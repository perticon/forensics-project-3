
void** g28;

void** safe_read(void** rdi, void** rsi, void** rdx, void** rcx, ...);

/* first_file.2 */
signed char first_file_2 = 1;

void fun_2880(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12, void** a13, void** a14, int64_t a15, int64_t a16, void** a17, void** a18, void** a19, void** a20);

/* xwrite_stdout.part.0 */
void xwrite_stdout_part_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_2530();

void** quotearg_style(int64_t rdi, void** rsi, ...);

void** fun_2600();

void fun_28a0();

void fun_2630();

void** fun_26b0(int64_t rdi, ...);

/* xlseek.part.0 */
signed char xlseek_part_0(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_2980();

void close_fd(int64_t rdi, void** rsi, ...);

void** __cxa_finalize;

unsigned char g36;

void** g3c;

void** open_safer(void** rdi, ...);

void** g38;

void fun_26c0(int64_t rdi, void** rsi, int64_t rdx, void* rcx);

unsigned char reopen_inaccessible_files = 0;

unsigned char disable_inotify = 0;

int32_t fun_2720(void** rdi, void** rsi, ...);

void** quotearg_n_style_colon();

int16_t g34;

int32_t follow_mode = 2;

signed char fremote(int64_t rdi, void** rsi, void** rdx);

int64_t g8;

int64_t g10;

uint32_t g40;

int64_t g18;

int64_t g58;

int64_t g20;

int32_t g30;

signed char print_headers = 0;

struct s0 {
    signed char[40] pad40;
    signed char* f28;
    signed char* f30;
};

struct s0* stdout = reinterpret_cast<struct s0*>(0);

int32_t fun_2940(struct s0* rdi, void** rsi, ...);

signed char line_end = 0;

struct s1 {
    signed char[1] pad1;
    void** f1;
};

struct s1* fun_28b0(void** rdi, int64_t rsi);

void** dump_remainder(void** edi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void** v7;
    void** v8;
    void*** rsp9;
    void** r12_10;
    void** v11;
    void** r15_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r13_16;
    void** rax17;
    void** v18;
    void** rdx19;
    void** rsi20;
    void** rdi21;
    void** rax22;
    int1_t zf23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    void** v28;
    int64_t v29;
    int64_t v30;
    void** v31;
    void** v32;
    void** v33;
    void** v34;
    void** rax35;
    void** rax36;
    void** rax37;
    void* rax38;
    void*** rsp39;
    void** v40;
    void** v41;
    void** v42;
    void** v43;
    void** rax44;
    void** v45;
    void** r14_46;
    void* rbx47;
    void* rax48;
    void* rbx49;
    void* rbx50;
    void** rdi51;
    int64_t rdi52;
    void** rax53;
    void* rsp54;
    void* rdx55;
    void* rsp56;
    void** rbx57;
    void* rsp58;
    void** edi59;
    void** rax60;
    void** v61;
    void* rax62;
    void** rax63;
    void** eax64;
    void** eax65;
    void** rax66;
    int64_t rdi67;
    void** rdx68;
    void* rsp69;
    void** rdi70;
    void** rax71;
    void** v72;
    uint32_t ebp73;
    uint32_t eax74;
    void** r13_75;
    void** eax76;
    void** r12d77;
    void** eax78;
    uint1_t zf79;
    void** rsi80;
    void** rax81;
    void** rax82;
    void** rsi83;
    void** rax84;
    int64_t rdi85;
    void** rsi86;
    void** rax87;
    int64_t rdi88;
    void* rax89;
    int64_t v90;
    uint32_t r14d91;
    uint32_t eax92;
    void** r15_93;
    void** rsi94;
    int32_t eax95;
    uint32_t v96;
    void** rax97;
    uint32_t r14d98;
    void** edx99;
    uint32_t eax100;
    void** rax101;
    void** rsi102;
    void** rdi103;
    int32_t eax104;
    uint32_t v105;
    void** eax106;
    void** rdi107;
    void** rsi108;
    int32_t eax109;
    uint32_t v110;
    uint32_t eax111;
    void** rax112;
    void** rax113;
    void** rsi114;
    void** rax115;
    void** rax116;
    uint32_t v117;
    int1_t zf118;
    void** rsi119;
    void** rax120;
    int64_t rdi121;
    signed char al122;
    void* rsp123;
    int1_t zf124;
    void** rsi125;
    void** rax126;
    void** rax127;
    void** eax128;
    void** rsi129;
    uint32_t edx130;
    void** rax131;
    void** rax132;
    void* rsp133;
    void** r13_134;
    void** rcx135;
    void* rsp136;
    uint32_t r15d137;
    int64_t v138;
    int64_t v139;
    int64_t v140;
    void** v141;
    int32_t v142;
    void** rax143;
    int64_t rdi144;
    void** rax145;
    int1_t zf146;
    void** v147;
    int1_t zf148;
    int64_t v149;
    void** rax150;
    void** rax151;
    void* rsp152;
    void** rsi153;
    void** rax154;
    int64_t rdi155;
    void** rax156;
    int64_t rdi157;
    void** rsi158;
    void** rax159;
    void** rax160;
    void** v161;
    void** rax162;
    int64_t rdi163;
    void** rax164;
    int1_t zf165;
    int1_t zf166;
    void** edx167;
    void** rax168;
    struct s0* rdi169;
    int32_t eax170;
    void** rax171;
    void** v172;
    int32_t edx173;
    void** v174;
    int64_t rdx175;
    void** v176;
    void** v177;
    int64_t rax178;
    void** rdi179;
    void** rax180;
    void** rax181;
    void** rdx182;
    int64_t rsi183;
    struct s1* rax184;
    int64_t rdi185;
    void** rax186;
    void** rdi187;
    void** rax188;
    void** rsi189;
    void** rax190;
    int64_t rdi191;
    void** rax192;
    void** rax193;

    v7 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(v8)));
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 40);
    r12_10 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
    v11 = rsi;
    r15_12 = edx;
    rbp13 = edi;
    *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    rbx14 = rcx;
    v15 = rcx;
    r13_16 = reinterpret_cast<void**>(rsp9 + 16);
    rax17 = g28;
    v18 = rax17;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdx19) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
        rsi20 = r13_16;
        rdi21 = r15_12;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx14) <= reinterpret_cast<unsigned char>(0x2000)) {
            rdx19 = rbx14;
        }
        rax22 = safe_read(rdi21, rsi20, rdx19, rcx);
        rsp9 = rsp9 - 8 + 8;
        if (rax22 == 0xffffffffffffffff) 
            break;
        if (!rax22) 
            goto addr_607d_6;
        if (*reinterpret_cast<signed char*>(&rbp13)) {
            zf23 = first_file_2 == 0;
            rcx = v11;
            rdx19 = reinterpret_cast<void**>("\n");
            if (!zf23) {
                rdx19 = reinterpret_cast<void**>(0xea41);
            }
            fun_2880(1, "%s==> %s <==\n", rdx19, rcx, r8, r9, v15, v11, v24, v25, v26, v7, v27, v28, v29, v30, v31, v32, v33, v34);
            rsp9 = rsp9 - 8 + 8;
            first_file_2 = 0;
        }
        rsi20 = rax22;
        rdi21 = r13_16;
        r12_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_10) + reinterpret_cast<unsigned char>(rax22));
        xwrite_stdout_part_0(rdi21, rsi20, rdx19, rcx, r8, r9);
        rsp9 = rsp9 - 8 + 8;
        if (v15 == 0xffffffffffffffff) 
            goto addr_6065_12;
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rax22));
        if (!rbx14) 
            goto addr_607d_6;
        if (v15 == 0xfffffffffffffffe) 
            goto addr_607d_6;
        addr_6065_12:
        rbp13 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    }
    rax35 = fun_2530();
    rsp9 = rsp9 - 8 + 8;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax35) == 11)) {
        rax36 = quotearg_style(4, v11);
        rax37 = fun_2600();
        rsi20 = *reinterpret_cast<void***>(rax35);
        rcx = rax36;
        rdi21 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx19 = rax37;
        fun_28a0();
        rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
    } else {
        addr_607d_6:
        rax38 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
        if (!rax38) {
            return r12_10;
        }
    }
    fun_2630();
    rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp9 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8) - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 56);
    v40 = rdi21;
    v41 = rcx;
    v42 = r8;
    v43 = r9;
    rax44 = g28;
    v45 = rax44;
    if (!rdx19) {
        addr_6330_20:
    } else {
        r12_10 = rsi20;
        r14_46 = rdx19;
        rbx47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
        rax48 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx47) >> 63) >> 51);
        rbx49 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx47) + reinterpret_cast<uint64_t>(rax48));
        *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<uint32_t*>(&rbx49) & 0x1fff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx50) - reinterpret_cast<uint64_t>(rax48));
        if (!rbx14) {
            rbx14 = reinterpret_cast<void**>(0x2000);
        }
        rdi51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rbx14));
        rbp13 = rdi51;
        r13_16 = rdi51;
        *reinterpret_cast<void***>(&rdi52) = r12_10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
        rax53 = fun_26b0(rdi52, rdi52);
        rsp54 = reinterpret_cast<void*>(rsp39 - 8 + 8);
        if (reinterpret_cast<signed char>(rax53) < reinterpret_cast<signed char>(0)) 
            goto addr_63c7_24; else 
            goto addr_61d3_25;
    }
    addr_6300_26:
    rdx55 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v45) - reinterpret_cast<unsigned char>(g28));
    if (rdx55) {
        fun_2630();
        rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
        goto addr_63b8_28;
    } else {
        goto v15;
    }
    addr_63c7_24:
    xlseek_part_0(rbp13, 0, v40, rcx, r8, r9);
    rbx57 = rbp13;
    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi59 = *reinterpret_cast<void***>(rbp13 + 56);
    rax60 = g28;
    v61 = rax60;
    if (edi59 == 0xffffffff) {
        addr_647a_31:
        rax62 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(g28));
        if (rax62) {
            fun_2630();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
        } else {
            goto v43;
        }
    } else {
        r13_16 = *reinterpret_cast<void***>(rbx57);
        if (*reinterpret_cast<void***>(r13_16) == 45 && !*reinterpret_cast<void***>(r13_16 + 1)) {
            rax63 = fun_2600();
            r13_16 = rax63;
            eax64 = fun_2980();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8);
            r12_10 = eax64;
            if (!eax64) 
                goto addr_6431_36; else 
                goto addr_64df_37;
        }
        eax65 = fun_2980();
        rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
        r12_10 = eax65;
        if (eax65) {
            addr_64df_37:
            rax66 = fun_2530();
            *reinterpret_cast<void***>(&rdi67) = *reinterpret_cast<void***>(rbx57 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
            *reinterpret_cast<void***>(rbx57 + 60) = *reinterpret_cast<void***>(rax66);
            close_fd(rdi67, r13_16, rdi67, r13_16);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx57 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_31;
        } else {
            addr_6431_36:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx57 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_39; else 
                goto addr_6444_40;
        }
    }
    addr_65f9_41:
    rdx68 = r13_16;
    xlseek_part_0(0, 0, rdx68, rcx, r8, r9);
    rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi70 = __cxa_finalize;
    rax71 = g28;
    v72 = rax71;
    ebp73 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi70) - 45);
    if (!ebp73) {
        ebp73 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi70 + 1));
    }
    eax74 = g36;
    r13_75 = g3c;
    if (!ebp73) {
        eax76 = r13_75;
        r12d77 = reinterpret_cast<void**>(0);
    } else {
        eax78 = open_safer(rdi70, rdi70);
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
        r12d77 = eax78;
        eax76 = g3c;
    }
    zf79 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx68) = zf79;
    if (*reinterpret_cast<unsigned char*>(&rdx68) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax76 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_50:
            ++r13_75;
            if (r13_75) {
                addr_6b83_51:
                rsi80 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi80) == 45) && !*reinterpret_cast<void***>(rsi80 + 1)) {
                    rax81 = fun_2600();
                    rsi80 = rax81;
                }
                rax82 = quotearg_style(4, rsi80, 4, rsi80);
                r13_75 = rax82;
                fun_2600();
                fun_28a0();
            }
            addr_6941_55:
            rsi83 = __cxa_finalize;
            addr_66f0_56:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi83) == 45) && !*reinterpret_cast<void***>(rsi83 + 1)) {
                rax84 = fun_2600();
                rsi83 = rax84;
            }
            *reinterpret_cast<void***>(&rdi85) = r12d77;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi85) + 4) = 0;
            close_fd(rdi85, rsi83, rdi85, rsi83);
            rsi86 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi86) == 45) && !*reinterpret_cast<void***>(rsi86 + 1)) {
                rax87 = fun_2600();
                rsi86 = rax87;
            }
            *reinterpret_cast<void***>(&rdi88) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi88) + 4) = 0;
            close_fd(rdi88, rsi86, rdi88, rsi86);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_61:
            rax89 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v72) - reinterpret_cast<unsigned char>(g28));
        } while (rax89);
        goto v90;
    }
    r14d91 = reopen_inaccessible_files;
    eax92 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d91)) {
        if (r12d77 == 0xffffffff) {
            addr_6918_65:
            g36 = 0;
            r15_93 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax92)) {
                rsi94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16);
                eax95 = fun_2720(r15_93, rsi94, r15_93, rsi94);
                if (eax95 || (v96 & 0xf000) != 0xa000) {
                    addr_66b9_67:
                    rax97 = fun_2530();
                    r14d98 = g36;
                    rsi83 = __cxa_finalize;
                    edx99 = *reinterpret_cast<void***>(rax97);
                    r15_93 = rsi83;
                    g3c = edx99;
                    if (*reinterpret_cast<signed char*>(&r14d98)) {
                        eax100 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi83) - 45);
                        if (!eax100) {
                            eax100 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi83 + 1));
                            if (edx99 == r13_75) 
                                goto addr_66f0_56;
                        } else {
                            if (edx99 == r13_75) {
                                goto addr_66f0_56;
                            }
                        }
                        if (!eax100) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi83 = __cxa_finalize;
                        goto addr_66f0_56;
                    }
                } else {
                    goto addr_6c10_76;
                }
            } else {
                rax101 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax101);
            }
        } else {
            g36 = 1;
            rsi102 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16);
            if (*reinterpret_cast<signed char*>(&eax92) || ((rdi103 = __cxa_finalize, eax104 = fun_2720(rdi103, rsi102, rdi103, rsi102), rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8), !!eax104) || (v105 & 0xf000) != 0xa000)) {
                addr_67a3_79:
                eax106 = fun_2980();
                rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
                if (reinterpret_cast<signed char>(eax106) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_67; else 
                    goto addr_67b3_80;
            } else {
                goto addr_6c10_76;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax92) || ((rdi107 = __cxa_finalize, rsi108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16), eax109 = fun_2720(rdi107, rsi108, rdi107, rsi108), rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8), !!eax109) || (v110 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d77 == 0xffffffff)) 
                goto addr_66b9_67;
            goto addr_67a3_79;
        } else {
            goto addr_6c10_76;
        }
    }
    eax111 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_93) - 45);
    if (!eax111) {
        eax111 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_93 + 1));
        if (!*reinterpret_cast<signed char*>(&eax74)) 
            goto addr_6941_55;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax74)) 
            goto addr_6941_55;
    }
    if (!eax111) {
        rax112 = fun_2600();
        r15_93 = rax112;
    }
    rax113 = quotearg_style(4, r15_93, 4, r15_93);
    r13_75 = rax113;
    fun_2600();
    fun_28a0();
    rsi83 = __cxa_finalize;
    goto addr_66f0_56;
    addr_6c10_76:
    rsi114 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi114) == 45) && !*reinterpret_cast<void***>(rsi114 + 1)) {
        rax115 = fun_2600();
        rsi114 = rax115;
    }
    rax116 = quotearg_style(4, rsi114, 4, rsi114);
    r13_75 = rax116;
    fun_2600();
    fun_28a0();
    rsi83 = __cxa_finalize;
    goto addr_66f0_56;
    addr_67b3_80:
    if ((v117 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v117 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d91) || (zf118 = follow_mode == 1, !zf118)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax74)) 
                goto addr_6b6d_96;
            if (r13_75 == 0xffffffff) 
                goto addr_6941_55;
            addr_6b6d_96:
            fun_2600();
            goto addr_6b83_51;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax74)) 
                goto addr_6e01_50;
            goto addr_6b83_51;
        }
    }
    rsi119 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi119) == 45) && !*reinterpret_cast<void***>(rsi119 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx68) = 5;
        *reinterpret_cast<int32_t*>(&rdx68 + 4) = 0;
        rax120 = fun_2600();
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
        rsi119 = rax120;
    }
    *reinterpret_cast<void***>(&rdi121) = r12d77;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi121) + 4) = 0;
    al122 = fremote(rdi121, rsi119, rdx68);
    rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al122;
    if (al122 && (zf124 = disable_inotify == 0, zf124)) {
        rsi125 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi125) == 45) && !*reinterpret_cast<void***>(rsi125 + 1)) {
            rax126 = fun_2600();
            rsi125 = rax126;
        }
        rax127 = quotearg_style(4, rsi125, 4, rsi125);
        r13_75 = rax127;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_55;
    }
    r13_75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_75) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax128 = g38;
    if (r13_75) 
        goto addr_6830_107;
    rsi129 = __cxa_finalize;
    edx130 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi129) - 45);
    if (!edx130) {
        edx130 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi129 + 1));
    }
    if (eax128 != 0xffffffff) 
        goto addr_6a80_111;
    if (!edx130) {
        rax131 = fun_2600();
        rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
        rsi129 = rax131;
    }
    rax132 = quotearg_style(4, rsi129, 4, rsi129);
    rsp133 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
    r13_134 = rax132;
    addr_685e_115:
    fun_2600();
    rcx135 = r13_134;
    fun_28a0();
    rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp133) - 8 + 8 - 8 + 8);
    addr_6876_116:
    r15d137 = 0;
    r13_75 = __cxa_finalize;
    if (!ebp73) {
        r15d137 = 0xffffffff;
    }
    g38 = r12d77;
    g8 = 0;
    g10 = v138;
    g40 = r15d137;
    g18 = v139;
    g58 = 0;
    g20 = v140;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v141;
    g30 = v142;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_75) == 45) && !*reinterpret_cast<void***>(r13_75 + 1)) {
        rax143 = fun_2600();
        rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp136) - 8 + 8);
        r13_75 = rax143;
    }
    *reinterpret_cast<void***>(&rdi144) = r12d77;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi144) + 4) = 0;
    rax145 = fun_26b0(rdi144);
    if (reinterpret_cast<signed char>(rax145) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_61;
    *reinterpret_cast<signed char*>(&eax92) = xlseek_part_0(0, 0, r13_75, rcx135, r8, r9);
    rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp136) - 8 + 8 - 8 + 8);
    goto addr_6918_65;
    addr_6a80_111:
    zf146 = g28 == v147;
    if (!zf146 || (zf148 = g20 == v149, !zf148)) {
        if (!edx130) {
            rax150 = fun_2600();
            rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
            rsi129 = rax150;
        }
        rax151 = quotearg_style(4, rsi129, 4, rsi129);
        fun_2600();
        rcx135 = rax151;
        fun_28a0();
        rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi153 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi153) == 45) && !*reinterpret_cast<void***>(rsi153 + 1)) {
            rax154 = fun_2600();
            rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8);
            rsi153 = rax154;
        }
        *reinterpret_cast<void***>(&rdi155) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi155) + 4) = 0;
        close_fd(rdi155, rsi153, rdi155, rsi153);
        rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8);
        goto addr_6876_116;
    } else {
        if (!edx130) {
            rax156 = fun_2600();
            rsi129 = rax156;
        }
        *reinterpret_cast<void***>(&rdi157) = r12d77;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi157) + 4) = 0;
        close_fd(rdi157, rsi129, rdi157, rsi129);
        goto addr_674c_61;
    }
    addr_6830_107:
    if (!reinterpret_cast<int1_t>(eax128 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi158 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi158) == 45 && !*reinterpret_cast<void***>(rsi158 + 1)) {
            rax159 = fun_2600();
            rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
            rsi158 = rax159;
        }
        rax160 = quotearg_style(4, rsi158, 4, rsi158);
        rsp133 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
        r13_134 = rax160;
        goto addr_685e_115;
    }
    addr_6500_39:
    if (reinterpret_cast<signed char>(v161) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 8))) {
        while (rax162 = quotearg_n_style_colon(), fun_2600(), rcx = rax162, fun_28a0(), *reinterpret_cast<void***>(&rdi163) = *reinterpret_cast<void***>(rbx57 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi163) + 4) = 0, rax164 = fun_26b0(rdi163), rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax164) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx57 + 8) = reinterpret_cast<void**>(0);
            addr_6444_40:
            zf165 = print_headers == 0;
            if (!zf165) {
                r12_10 = reinterpret_cast<void**>(0);
                zf166 = __cxa_finalize == rbx57;
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!zf166);
            }
            edx167 = *reinterpret_cast<void***>(rbx57 + 56);
            rcx = reinterpret_cast<void**>(0xffffffffffffffff);
            rax168 = dump_remainder(r12_10, r13_16, edx167, 0xffffffffffffffff, r8, r9);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
            *reinterpret_cast<void***>(rbx57 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx57 + 8)) + reinterpret_cast<unsigned char>(rax168));
            if (!rax168) 
                goto addr_647a_31;
            __cxa_finalize = rbx57;
            rdi169 = stdout;
            eax170 = fun_2940(rdi169, r13_16, rdi169, r13_16);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
            if (!eax170) 
                goto addr_647a_31;
            rax171 = fun_2600();
            r12_10 = rax171;
            fun_2530();
            fun_28a0();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_41;
    } else {
        if (v172 != *reinterpret_cast<void***>(rbx57 + 8)) 
            goto addr_6444_40;
        edx173 = 0;
        *reinterpret_cast<unsigned char*>(&edx173) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v174) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx175) = edx173 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v176) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx175) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 24)) < reinterpret_cast<signed char>(v177));
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax178) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 24)) > reinterpret_cast<signed char>(v177))) - *reinterpret_cast<uint32_t*>(&rcx);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax178) + 4) = 0;
        if (static_cast<int32_t>(rax178 + rdx175 * 2)) 
            goto addr_6444_40;
        goto addr_647a_31;
    }
    addr_61d3_25:
    r15_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 32);
    rdi179 = r12_10;
    *reinterpret_cast<int32_t*>(&rdi179 + 4) = 0;
    rax180 = safe_read(rdi179, r15_12, rbx14, rcx);
    rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
    rbp13 = rax180;
    if (rax180 == 0xffffffffffffffff) {
        addr_6337_144:
        rax181 = quotearg_style(4, v40, 4, v40);
        r13_16 = rax181;
        fun_2600();
        fun_2530();
        rcx = r13_16;
        fun_28a0();
        rsp39 = rsp39 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_6300_26;
    } else {
        rcx = v43;
        *reinterpret_cast<int32_t*>(&rbx14) = line_end;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax180));
        if (rbp13) {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp39) + reinterpret_cast<unsigned char>(rbp13) + 31) != *reinterpret_cast<signed char*>(&rbx14)) {
                --r14_46;
            }
        }
        while (1) {
            rdx182 = rbp13;
            while (rdx182 && (*reinterpret_cast<int32_t*>(&rsi183) = *reinterpret_cast<int32_t*>(&rbx14), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi183) + 4) = 0, rax184 = fun_28b0(r15_12, rsi183), rsp39 = rsp39 - 8 + 8, !!rax184)) {
                rdx182 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax184) - reinterpret_cast<unsigned char>(r15_12));
                if (!r14_46) 
                    goto addr_62c8_152;
                --r14_46;
            }
            if (r13_16 == v41) 
                goto addr_6379_155;
            r13_16 = r13_16 - 0x2000;
            *reinterpret_cast<void***>(&rdi185) = r12_10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi185) + 4) = 0;
            rax186 = fun_26b0(rdi185, rdi185);
            rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
            if (reinterpret_cast<signed char>(rax186) < reinterpret_cast<signed char>(0)) 
                goto addr_63b8_28;
            rdi187 = r12_10;
            *reinterpret_cast<int32_t*>(&rdi187 + 4) = 0;
            rax188 = safe_read(rdi187, r15_12, 0x2000, rcx);
            rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rbp13 = rax188;
            if (rax188 == 0xffffffffffffffff) 
                goto addr_6337_144;
            rcx = v43;
            *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax188));
            if (!rbp13) 
                goto addr_6330_20;
            *reinterpret_cast<int32_t*>(&rbx14) = line_end;
        }
    }
    addr_62c8_152:
    rsi189 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(rdx182));
    if (rsi189) {
        xwrite_stdout_part_0(&rax184->f1, rsi189, rdx182, rcx, r8, r9);
        rsp39 = rsp39 - 8 + 8;
    }
    rax190 = dump_remainder(0, v40, r12_10, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v42) - reinterpret_cast<unsigned char>(rbp13)) - reinterpret_cast<unsigned char>(r13_16), r8, r9);
    rsp39 = rsp39 - 8 + 8;
    rcx = v43;
    *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)) + reinterpret_cast<unsigned char>(rax190));
    goto addr_6300_26;
    addr_6379_155:
    *reinterpret_cast<void***>(&rdi191) = r12_10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi191) + 4) = 0;
    rax192 = fun_26b0(rdi191, rdi191);
    rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
    if (reinterpret_cast<signed char>(rax192) < reinterpret_cast<signed char>(0)) {
        addr_63b8_28:
        xlseek_part_0(r13_16, 0, v40, rcx, r8, r9);
        rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        goto addr_63c7_24;
    } else {
        rax193 = dump_remainder(0, v40, r12_10, v42, r8, r9);
        rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        rcx = v43;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax193) + reinterpret_cast<unsigned char>(r13_16));
        goto addr_6300_26;
    }
}

int32_t fun_28f0();

int32_t fun_26f0();

signed char fremote(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rsi5;
    int32_t eax6;
    int64_t rcx7;
    int64_t v8;
    void** rax9;
    int64_t r12_10;
    void* rax11;
    uint32_t eax12;
    int32_t eax13;
    int64_t v14;
    int64_t v15;
    uint64_t r12_16;
    int64_t rcx17;
    uint64_t r12_18;

    rax4 = g28;
    rsi5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x88);
    eax6 = fun_28f0();
    if (!eax6) {
        rcx7 = v8;
        if (rcx7 == 0x2bad1dea) 
            goto addr_5b68_3;
        if (rcx7 <= 0x2bad1dea) 
            goto addr_5493_5;
    } else {
        rax9 = fun_2530();
        *reinterpret_cast<uint32_t*>(&r12_10) = 1;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 38)) {
            quotearg_style(4, rsi);
            fun_2600();
            rsi5 = *reinterpret_cast<void***>(rax9);
            *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            fun_28a0();
            goto addr_5458_8;
        } else {
            goto addr_5458_8;
        }
    }
    if (rcx7 == 0x62656570) 
        goto addr_5b68_3;
    if (rcx7 > 0x62656570) 
        goto addr_5517_12;
    if (rcx7 == 0x53464846) 
        goto addr_5b68_3;
    if (rcx7 > 0x53464846) 
        goto addr_558b_15;
    if (rcx7 == 0x453dcd28) 
        goto addr_5b68_3;
    if (rcx7 > 0x453dcd28) 
        goto addr_568b_18;
    if (rcx7 == 0x42494e4d) 
        goto addr_5b68_3;
    if (rcx7 > 0x42494e4d) 
        goto addr_5833_21;
    if (rcx7 == 0x3153464a || rcx7 == 0x42465331) {
        addr_5b68_3:
        *reinterpret_cast<uint32_t*>(&r12_10) = 0;
    } else {
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x2fc12fc1);
    }
    addr_5458_8:
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rax11) {
        eax12 = *reinterpret_cast<uint32_t*>(&r12_10);
        return *reinterpret_cast<signed char*>(&eax12);
    }
    fun_2630();
    if (static_cast<uint32_t>(rdi + 1) <= 1) 
        goto addr_5b88_27;
    eax13 = fun_26f0();
    if (eax13) {
        quotearg_style(4, rsi5, 4, rsi5);
        fun_2600();
        fun_2530();
    } else {
        goto v14;
    }
    addr_5b88_27:
    goto v15;
    addr_5833_21:
    if (rcx7 == 0x43415d53) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x444d4142);
    goto addr_5458_8;
    addr_568b_18:
    if (rcx7 == 0x52654973) 
        goto addr_5b68_3;
    if (rcx7 > 0x52654973) 
        goto addr_569e_34;
    if (rcx7 == 0x454d444d) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x45584653);
    goto addr_5458_8;
    addr_569e_34:
    if (rcx7 == 0x5345434d) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x5346314d);
    goto addr_5458_8;
    addr_558b_15:
    if (rcx7 == 0x58465342) 
        goto addr_5b68_3;
    if (rcx7 > 0x58465342) 
        goto addr_559e_39;
    if (rcx7 == 0x54190100) 
        goto addr_5b68_3;
    if (rcx7 > 0x54190100) 
        goto addr_589b_42;
    if (rcx7 == 0x5346544e) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x534f434b);
    goto addr_5458_8;
    addr_589b_42:
    if (rcx7 == 0x565a4653) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x58295829);
    goto addr_5458_8;
    addr_559e_39:
    if (rcx7 == 0x5dca2df5) 
        goto addr_5b68_3;
    if (rcx7 > 0x5dca2df5) 
        goto addr_55b1_47;
    if (rcx7 == 0x5a3c69f0) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x5a4f4653);
    goto addr_5458_8;
    addr_55b1_47:
    if (rcx7 == 0x6165676c) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x62646576);
    goto addr_5458_8;
    addr_5517_12:
    if (rcx7 == 0x858458f6) 
        goto addr_5b68_3;
    if (rcx7 > 0x858458f6) 
        goto addr_552b_52;
    if (rcx7 == 0x6c6f6f70) 
        goto addr_5b68_3;
    if (rcx7 > 0x6c6f6f70) 
        goto addr_56d3_55;
    if (rcx7 == 0x64646178) 
        goto addr_5b68_3;
    if (rcx7 > 0x64646178) 
        goto addr_57cb_58;
    if (rcx7 == 0x63677270) 
        goto addr_5b68_3;
    if (rcx7 == 0x64626720) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x62656572);
    goto addr_5458_8;
    addr_57cb_58:
    if (rcx7 == 0x67596969) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x68191122);
    goto addr_5458_8;
    addr_56d3_55:
    if (rcx7 == 0x73717368) 
        goto addr_5b68_3;
    if (rcx7 > 0x73717368) 
        goto addr_56e6_64;
    if (rcx7 == 0x6e736673) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x73636673);
    goto addr_5458_8;
    addr_56e6_64:
    if (rcx7 == 0x73727279) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x74726163);
    goto addr_5458_8;
    addr_552b_52:
    if (rcx7 == 0xcafe4a11) 
        goto addr_5b68_3;
    if (rcx7 > 0xcafe4a11) 
        goto addr_553f_69;
    if (rcx7 == 0xabba1974) 
        goto addr_5b68_3;
    if (rcx7 > 0xabba1974) 
        goto addr_5865_72;
    if (rcx7 == 0x9123683e) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x958458f6);
    goto addr_5458_8;
    addr_5865_72:
    if (rcx7 == 0xc7571590) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xc97e8168);
    goto addr_5458_8;
    addr_553f_69:
    if (rcx7 == 0xf2f52010) 
        goto addr_5b68_3;
    if (rcx7 > 0xf2f52010) 
        goto addr_5553_77;
    if (rcx7 == 0xde5e81e4) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xe0f5e1e2);
    goto addr_5458_8;
    addr_5553_77:
    if (rcx7 == 0xf97cff8c) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xf995e849);
    goto addr_5458_8;
    addr_5493_5:
    if (rcx7 > 0x9fa2) {
        if (rcx7 > 0x12ff7b7) {
            if (rcx7 == 0x15013346) 
                goto addr_5b68_3;
            if (rcx7 > 0x15013346) 
                goto addr_5643_84;
        } else {
            if (rcx7 > 0x12ff7b3) 
                goto addr_5b68_3;
            if (rcx7 == 0x27e0eb) 
                goto addr_5b68_3;
            if (rcx7 <= 0x27e0eb) 
                goto addr_57f0_88; else 
                goto addr_55f9_89;
        }
    } else {
        if (rcx7 > 0x9f9f) 
            goto addr_5b68_3;
        if (rcx7 > 0x4006) 
            goto addr_5708_92; else 
            goto addr_54ba_93;
    }
    if (rcx7 == 0xbad1dea) 
        goto addr_5b68_3;
    if (rcx7 > 0xbad1dea) 
        goto addr_58d3_96;
    if (rcx7 == 0x7655821) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x9041934);
    goto addr_5458_8;
    addr_58d3_96:
    if (rcx7 == 0x11307854) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x13661366);
    goto addr_5458_8;
    addr_5643_84:
    if (rcx7 == 0x2011bab0) 
        goto addr_5b68_3;
    if (rcx7 > 0x2011bab0) 
        goto addr_5656_101;
    if (rcx7 == 0x19800202) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x1badface);
    goto addr_5458_8;
    addr_5656_101:
    if (rcx7 == 0x24051905) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x28cd3d45);
    goto addr_5458_8;
    addr_57f0_88:
    if (rcx7 == 0xef53) 
        goto addr_5b68_3;
    if (rcx7 > 0xef53) 
        goto addr_5803_106;
    if (rcx7 == 0xadff) 
        goto addr_5b68_3;
    if (rcx7 == 0xef51) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(rcx7 == "%("));
    goto addr_5458_8;
    addr_5803_106:
    if (rcx7 == 0xf15f) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x11954);
    goto addr_5458_8;
    addr_55f9_89:
    if (rcx7 == 0x1021994) 
        goto addr_5b68_3;
    if (rcx7 > 0x1021994) 
        goto addr_560c_112;
    if (rcx7 == 0x414a53) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xc0ffee);
    goto addr_5458_8;
    addr_560c_112:
    if (rcx7 == 0x1021997) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x12fd16d);
    goto addr_5458_8;
    addr_5708_92:
    if (rcx7 == 0x4d5a) 
        goto addr_5b68_3;
    if (rcx7 > 0x4d5a) 
        goto addr_571b_117;
    if (rcx7 == 0x4858) 
        goto addr_5b68_3;
    if (rcx7 > 0x4858) 
        goto addr_58ff_120;
    if (rcx7 == 0x4244) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x482b);
    goto addr_5458_8;
    addr_58ff_120:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x4d44);
    goto addr_5458_8;
    addr_571b_117:
    if (rcx7 == 0x72b6) 
        goto addr_5b68_3;
    if (rcx7 > 0x72b6) 
        goto addr_572a_124;
    if (rcx7 == 0x5df5) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x7275);
    goto addr_5458_8;
    addr_572a_124:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x9660);
    goto addr_5458_8;
    addr_54ba_93:
    if (rcx7 > 0x3fff) {
        r12_16 = 81 >> *reinterpret_cast<signed char*>(&rcx7);
        *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_16) & 1 ^ 1;
        goto addr_5458_8;
    } else {
        if (rcx7 > 0x138f) {
            if (rcx7 == 0x2478) 
                goto addr_5b68_3;
            if (rcx7 > 0x2478) 
                goto addr_574f_131;
        } else {
            if (rcx7 > 0x1372) {
                rcx17 = rcx7 - 0x1373;
                r12_18 = 0x10001401 >> *reinterpret_cast<signed char*>(&rcx17);
                *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_18) & 1 ^ 1;
                goto addr_5458_8;
            } else {
                if (rcx7 == 0x187) 
                    goto addr_5b68_3;
                if (rcx7 <= 0x187) {
                    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!!(rcx7 - 47 & 0xfffffffffffffffb));
                    goto addr_5458_8;
                } else {
                    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x7c0);
                    goto addr_5458_8;
                }
            }
        }
    }
    if (rcx7 == 0x1cd1) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x2468);
    goto addr_5458_8;
    addr_574f_131:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x3434);
    goto addr_5458_8;
}

void close_fd(int64_t rdi, void** rsi, ...) {
    int32_t eax3;

    if (static_cast<uint32_t>(rdi + 1) > 1) {
        eax3 = fun_26f0();
        if (eax3) {
            quotearg_style(4, rsi);
            fun_2600();
            fun_2530();
        } else {
            return;
        }
    } else {
        return;
    }
}

void** fun_2850();

void fun_25b0(struct s0* rdi, int64_t rsi, void** rdx, struct s0* rcx);

int64_t offtostr(void** rdi, void* rsi);

int32_t fun_2910();

struct s2 {
    signed char[1] pad1;
    void** f1;
};

struct s2* fun_2700();

/* xwrite_stdout.part.0 */
void xwrite_stdout_part_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    struct s0* rcx7;
    int64_t v8;
    int64_t rbx9;
    void** rax10;
    struct s0* rdi11;
    void** rax12;
    void** rax13;
    void** rax14;
    void** rcx15;
    void** rdi16;
    void** rsi17;
    void* rsp18;
    void** rax19;
    void** v20;
    void** rdx21;
    void** rsp22;
    void** rax23;
    void** v24;
    void* rdx25;
    int64_t v26;
    void** r13_27;
    void** rbp28;
    void** r14_29;
    void** rbx30;
    void** r12_31;
    void** rax32;
    void** rax33;
    void** rax34;
    void** r15d35;
    void** r14_36;
    void** rax37;
    void** rdx38;
    struct s2* rax39;
    void** rax40;
    void** rax41;

    rcx7 = stdout;
    v8 = rbx9;
    rax10 = fun_2850();
    if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(rax10)) {
        return;
    }
    rdi11 = stdout;
    fun_25b0(rdi11, 1, rsi, rcx7);
    rax12 = quotearg_style(4, "standard output", 4, "standard output");
    rax13 = fun_2600();
    rax14 = fun_2530();
    rcx15 = rax12;
    rdi16 = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
    rsi17 = *reinterpret_cast<void***>(rax14);
    fun_28a0();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 40);
    rax19 = g28;
    v20 = rax19;
    if (rax13) 
        goto addr_5cd3_5;
    addr_5d44_6:
    rdx21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(g28));
    if (!rdx21) {
        goto v8;
    }
    fun_2630();
    rsp22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 16);
    rax23 = g28;
    v24 = rax23;
    if (rdx21) 
        goto addr_5e1c_10;
    addr_5e68_11:
    addr_5e6a_12:
    rdx25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        goto v26;
    }
    fun_2630();
    offtostr(rdi16, reinterpret_cast<uint64_t>(rsp22 - 8) + 8 - 8 - 8 - 8 - 8 - 40);
    fun_2530();
    if (rsi17 != 1) 
        goto addr_5f31_16;
    while (1) {
        quotearg_n_style_colon();
        addr_5f45_18:
        fun_2600();
        fun_28a0();
        fun_2910();
    }
    addr_5f31_16:
    quotearg_n_style_colon();
    goto addr_5f45_18;
    addr_5e1c_10:
    r13_27 = rdi16;
    rbp28 = rsi17;
    r14_29 = rdx21;
    rbx30 = rcx15;
    r12_31 = rsp22;
    do {
        rsi17 = r12_31;
        rdi16 = rbp28;
        *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
        rax32 = safe_read(rdi16, rsi17, 0x2000, rcx15);
        rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8);
        if (!rax32) 
            break;
        if (rax32 == 0xffffffffffffffff) 
            goto addr_5e90_21;
        *reinterpret_cast<void***>(rbx30) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30)) + reinterpret_cast<unsigned char>(rax32));
        if (reinterpret_cast<unsigned char>(rax32) > reinterpret_cast<unsigned char>(r14_29)) 
            goto addr_5e60_23;
        r14_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_29) - reinterpret_cast<unsigned char>(rax32));
    } while (r14_29);
    goto addr_5e68_11;
    goto addr_5e6a_12;
    addr_5e90_21:
    quotearg_style(4, r13_27, 4, r13_27);
    fun_2600();
    rax33 = fun_2530();
    rdi16 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
    rsi17 = *reinterpret_cast<void***>(rax33);
    fun_28a0();
    rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_5e6a_12;
    addr_5e60_23:
    rax34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax32) - reinterpret_cast<unsigned char>(r14_29));
    rsi17 = rax34;
    if (rax34) {
        rdi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_31) + reinterpret_cast<unsigned char>(r14_29));
        xwrite_stdout_part_0(rdi16, rsi17, 0x2000, rcx15, r8, r9);
        rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8);
        goto addr_5e68_11;
    }
    addr_5cd3_5:
    r15d35 = rsi17;
    rbx30 = rax13;
    r14_36 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 16);
    r13_27 = rcx15;
    while (rsi17 = r14_36, rdi16 = r15d35, *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0, rax37 = safe_read(rdi16, rsi17, 0x2000, rcx15), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), !!rax37) {
        if (rax37 == 0xffffffffffffffff) 
            goto addr_5d8e_29;
        *reinterpret_cast<void***>(r13_27) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_27)) + reinterpret_cast<unsigned char>(rax37));
        r12_31 = reinterpret_cast<void**>(static_cast<int32_t>(line_end));
        rbp28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_36) + reinterpret_cast<unsigned char>(rax37));
        rdi16 = r14_36;
        do {
            rsi17 = r12_31;
            rdx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp28) - reinterpret_cast<unsigned char>(rdi16));
            rax39 = fun_2700();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            if (!rax39) 
                break;
            rdi16 = reinterpret_cast<void**>(&rax39->f1);
            --rbx30;
        } while (rbx30);
        goto addr_5d3d_33;
    }
    goto addr_5d44_6;
    addr_5d8e_29:
    rax40 = quotearg_style(4, 1, 4, 1);
    fun_2600();
    rax41 = fun_2530();
    rcx15 = rax40;
    rdi16 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
    rsi17 = *reinterpret_cast<void***>(rax41);
    fun_28a0();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_5d44_6;
    addr_5d3d_33:
    if (reinterpret_cast<unsigned char>(rbp28) > reinterpret_cast<unsigned char>(rdi16) && (rbp28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp28) - reinterpret_cast<unsigned char>(rdi16)), !!rbp28)) {
        rsi17 = rbp28;
        xwrite_stdout_part_0(rdi16, rsi17, rdx38, rcx15, r8, r9);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        goto addr_5d44_6;
    }
}

unsigned char file_lines(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void*** rsp7;
    void** v8;
    void** v9;
    void** v10;
    void** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void** r12_15;
    void** r14_16;
    void* rbx17;
    void* rax18;
    void* rbx19;
    void* rbx20;
    void** rbx21;
    void** rdi22;
    void** rbp23;
    void** r13_24;
    int64_t rdi25;
    void** rax26;
    void* rsp27;
    void* rdx28;
    void* rsp29;
    void** rbx30;
    void* rsp31;
    void** edi32;
    void** rax33;
    void** v34;
    void* rax35;
    void** rax36;
    void** eax37;
    void** eax38;
    void** rax39;
    int64_t rdi40;
    void** rdx41;
    void* rsp42;
    void** rdi43;
    void** rax44;
    void** v45;
    uint32_t ebp46;
    uint32_t eax47;
    void** r13_48;
    void** eax49;
    void** r12d50;
    void** eax51;
    uint1_t zf52;
    void** rsi53;
    void** rax54;
    void** rax55;
    void** rsi56;
    void** rax57;
    int64_t rdi58;
    void** rsi59;
    void** rax60;
    int64_t rdi61;
    void* rax62;
    int64_t v63;
    uint32_t r14d64;
    uint32_t eax65;
    void** r15_66;
    void** rsi67;
    int32_t eax68;
    uint32_t v69;
    void** rax70;
    uint32_t r14d71;
    void** edx72;
    uint32_t eax73;
    void** rax74;
    void** rsi75;
    void** rdi76;
    int32_t eax77;
    uint32_t v78;
    void** eax79;
    void** rdi80;
    void** rsi81;
    int32_t eax82;
    uint32_t v83;
    uint32_t eax84;
    void** rax85;
    void** rax86;
    void** rsi87;
    void** rax88;
    void** rax89;
    uint32_t v90;
    int1_t zf91;
    void** rsi92;
    void** rax93;
    int64_t rdi94;
    signed char al95;
    void* rsp96;
    int1_t zf97;
    void** rsi98;
    void** rax99;
    void** rax100;
    void** eax101;
    void** rsi102;
    uint32_t edx103;
    void** rax104;
    void** rax105;
    void* rsp106;
    void** r13_107;
    void** rcx108;
    void* rsp109;
    uint32_t r15d110;
    int64_t v111;
    int64_t v112;
    int64_t v113;
    void** v114;
    int32_t v115;
    void** rax116;
    int64_t rdi117;
    void** rax118;
    int1_t zf119;
    void** v120;
    int1_t zf121;
    int64_t v122;
    void** rax123;
    void** rax124;
    void* rsp125;
    void** rsi126;
    void** rax127;
    int64_t rdi128;
    void** rax129;
    int64_t rdi130;
    void** rsi131;
    void** rax132;
    void** rax133;
    void** v134;
    void** rax135;
    int64_t rdi136;
    void** rax137;
    int1_t zf138;
    int1_t zf139;
    void** edx140;
    void** rax141;
    struct s0* rdi142;
    int32_t eax143;
    void** rax144;
    void** v145;
    int32_t edx146;
    void** v147;
    int64_t rdx148;
    void** v149;
    void** v150;
    int64_t rax151;
    void** r15_152;
    void** rdi153;
    void** rax154;
    void** rax155;
    void** rdx156;
    int64_t rsi157;
    struct s1* rax158;
    int64_t rdi159;
    void** rax160;
    void** rdi161;
    void** rax162;
    void** rsi163;
    void** rax164;
    int64_t rdi165;
    void** rax166;
    void** rax167;

    rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 56);
    v8 = rdi;
    v9 = rcx;
    v10 = r8;
    v11 = r9;
    rax12 = g28;
    v13 = rax12;
    if (!rdx) {
        addr_6330_2:
        eax14 = 1;
    } else {
        r12_15 = esi;
        r14_16 = rdx;
        rbx17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
        rax18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx17) >> 63) >> 51);
        rbx19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx17) + reinterpret_cast<uint64_t>(rax18));
        *reinterpret_cast<uint32_t*>(&rbx20) = *reinterpret_cast<uint32_t*>(&rbx19) & 0x1fff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
        rbx21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx20) - reinterpret_cast<uint64_t>(rax18));
        if (!rbx21) {
            rbx21 = reinterpret_cast<void**>(0x2000);
        }
        rdi22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rbx21));
        rbp23 = rdi22;
        r13_24 = rdi22;
        *reinterpret_cast<void***>(&rdi25) = r12_15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        rax26 = fun_26b0(rdi25, rdi25);
        rsp27 = reinterpret_cast<void*>(rsp7 - 8 + 8);
        if (reinterpret_cast<signed char>(rax26) < reinterpret_cast<signed char>(0)) 
            goto addr_63c7_6; else 
            goto addr_61d3_7;
    }
    addr_6300_8:
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rdx28) {
        fun_2630();
        rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
        goto addr_63b8_10;
    } else {
        return *reinterpret_cast<unsigned char*>(&eax14);
    }
    addr_63c7_6:
    xlseek_part_0(rbp23, 0, v8, rcx, r8, r9);
    rbx30 = rbp23;
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi32 = *reinterpret_cast<void***>(rbp23 + 56);
    rax33 = g28;
    v34 = rax33;
    if (edi32 == 0xffffffff) {
        addr_647a_13:
        rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
        if (rax35) {
            fun_2630();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
        } else {
            goto v11;
        }
    } else {
        r13_24 = *reinterpret_cast<void***>(rbx30);
        if (*reinterpret_cast<void***>(r13_24) == 45 && !*reinterpret_cast<void***>(r13_24 + 1)) {
            rax36 = fun_2600();
            r13_24 = rax36;
            eax37 = fun_2980();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            r12_15 = eax37;
            if (!eax37) 
                goto addr_6431_18; else 
                goto addr_64df_19;
        }
        eax38 = fun_2980();
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
        r12_15 = eax38;
        if (eax38) {
            addr_64df_19:
            rax39 = fun_2530();
            *reinterpret_cast<void***>(&rdi40) = *reinterpret_cast<void***>(rbx30 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi40) + 4) = 0;
            *reinterpret_cast<void***>(rbx30 + 60) = *reinterpret_cast<void***>(rax39);
            close_fd(rdi40, r13_24, rdi40, r13_24);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx30 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_13;
        } else {
            addr_6431_18:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_21; else 
                goto addr_6444_22;
        }
    }
    addr_65f9_23:
    rdx41 = r13_24;
    xlseek_part_0(0, 0, rdx41, rcx, r8, r9);
    rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi43 = __cxa_finalize;
    rax44 = g28;
    v45 = rax44;
    ebp46 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi43) - 45);
    if (!ebp46) {
        ebp46 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi43 + 1));
    }
    eax47 = g36;
    r13_48 = g3c;
    if (!ebp46) {
        eax49 = r13_48;
        r12d50 = reinterpret_cast<void**>(0);
    } else {
        eax51 = open_safer(rdi43, rdi43);
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
        r12d50 = eax51;
        eax49 = g3c;
    }
    zf52 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx41) = zf52;
    if (*reinterpret_cast<unsigned char*>(&rdx41) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax49 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_32:
            ++r13_48;
            if (r13_48) {
                addr_6b83_33:
                rsi53 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi53) == 45) && !*reinterpret_cast<void***>(rsi53 + 1)) {
                    rax54 = fun_2600();
                    rsi53 = rax54;
                }
                rax55 = quotearg_style(4, rsi53, 4, rsi53);
                r13_48 = rax55;
                fun_2600();
                fun_28a0();
            }
            addr_6941_37:
            rsi56 = __cxa_finalize;
            addr_66f0_38:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi56) == 45) && !*reinterpret_cast<void***>(rsi56 + 1)) {
                rax57 = fun_2600();
                rsi56 = rax57;
            }
            *reinterpret_cast<void***>(&rdi58) = r12d50;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi58) + 4) = 0;
            close_fd(rdi58, rsi56, rdi58, rsi56);
            rsi59 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi59) == 45) && !*reinterpret_cast<void***>(rsi59 + 1)) {
                rax60 = fun_2600();
                rsi59 = rax60;
            }
            *reinterpret_cast<void***>(&rdi61) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
            close_fd(rdi61, rsi59, rdi61, rsi59);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_43:
            rax62 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v45) - reinterpret_cast<unsigned char>(g28));
        } while (rax62);
        goto v63;
    }
    r14d64 = reopen_inaccessible_files;
    eax65 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d64)) {
        if (r12d50 == 0xffffffff) {
            addr_6918_47:
            g36 = 0;
            r15_66 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax65)) {
                rsi67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16);
                eax68 = fun_2720(r15_66, rsi67, r15_66, rsi67);
                if (eax68 || (v69 & 0xf000) != 0xa000) {
                    addr_66b9_49:
                    rax70 = fun_2530();
                    r14d71 = g36;
                    rsi56 = __cxa_finalize;
                    edx72 = *reinterpret_cast<void***>(rax70);
                    r15_66 = rsi56;
                    g3c = edx72;
                    if (*reinterpret_cast<signed char*>(&r14d71)) {
                        eax73 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi56) - 45);
                        if (!eax73) {
                            eax73 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi56 + 1));
                            if (edx72 == r13_48) 
                                goto addr_66f0_38;
                        } else {
                            if (edx72 == r13_48) {
                                goto addr_66f0_38;
                            }
                        }
                        if (!eax73) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi56 = __cxa_finalize;
                        goto addr_66f0_38;
                    }
                } else {
                    goto addr_6c10_58;
                }
            } else {
                rax74 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax74);
            }
        } else {
            g36 = 1;
            rsi75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16);
            if (*reinterpret_cast<signed char*>(&eax65) || ((rdi76 = __cxa_finalize, eax77 = fun_2720(rdi76, rsi75, rdi76, rsi75), rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8), !!eax77) || (v78 & 0xf000) != 0xa000)) {
                addr_67a3_61:
                eax79 = fun_2980();
                rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                if (reinterpret_cast<signed char>(eax79) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_49; else 
                    goto addr_67b3_62;
            } else {
                goto addr_6c10_58;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax65) || ((rdi80 = __cxa_finalize, rsi81 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16), eax82 = fun_2720(rdi80, rsi81, rdi80, rsi81), rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8), !!eax82) || (v83 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d50 == 0xffffffff)) 
                goto addr_66b9_49;
            goto addr_67a3_61;
        } else {
            goto addr_6c10_58;
        }
    }
    eax84 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_66) - 45);
    if (!eax84) {
        eax84 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_66 + 1));
        if (!*reinterpret_cast<signed char*>(&eax47)) 
            goto addr_6941_37;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax47)) 
            goto addr_6941_37;
    }
    if (!eax84) {
        rax85 = fun_2600();
        r15_66 = rax85;
    }
    rax86 = quotearg_style(4, r15_66, 4, r15_66);
    r13_48 = rax86;
    fun_2600();
    fun_28a0();
    rsi56 = __cxa_finalize;
    goto addr_66f0_38;
    addr_6c10_58:
    rsi87 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi87) == 45) && !*reinterpret_cast<void***>(rsi87 + 1)) {
        rax88 = fun_2600();
        rsi87 = rax88;
    }
    rax89 = quotearg_style(4, rsi87, 4, rsi87);
    r13_48 = rax89;
    fun_2600();
    fun_28a0();
    rsi56 = __cxa_finalize;
    goto addr_66f0_38;
    addr_67b3_62:
    if ((v90 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v90 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d64) || (zf91 = follow_mode == 1, !zf91)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax47)) 
                goto addr_6b6d_78;
            if (r13_48 == 0xffffffff) 
                goto addr_6941_37;
            addr_6b6d_78:
            fun_2600();
            goto addr_6b83_33;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax47)) 
                goto addr_6e01_32;
            goto addr_6b83_33;
        }
    }
    rsi92 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi92) == 45) && !*reinterpret_cast<void***>(rsi92 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx41) = 5;
        *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
        rax93 = fun_2600();
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
        rsi92 = rax93;
    }
    *reinterpret_cast<void***>(&rdi94) = r12d50;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0;
    al95 = fremote(rdi94, rsi92, rdx41);
    rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al95;
    if (al95 && (zf97 = disable_inotify == 0, zf97)) {
        rsi98 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi98) == 45) && !*reinterpret_cast<void***>(rsi98 + 1)) {
            rax99 = fun_2600();
            rsi98 = rax99;
        }
        rax100 = quotearg_style(4, rsi98, 4, rsi98);
        r13_48 = rax100;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_37;
    }
    r13_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_48) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax101 = g38;
    if (r13_48) 
        goto addr_6830_89;
    rsi102 = __cxa_finalize;
    edx103 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi102) - 45);
    if (!edx103) {
        edx103 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi102 + 1));
    }
    if (eax101 != 0xffffffff) 
        goto addr_6a80_93;
    if (!edx103) {
        rax104 = fun_2600();
        rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
        rsi102 = rax104;
    }
    rax105 = quotearg_style(4, rsi102, 4, rsi102);
    rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
    r13_107 = rax105;
    addr_685e_97:
    fun_2600();
    rcx108 = r13_107;
    fun_28a0();
    rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8 - 8 + 8);
    addr_6876_98:
    r15d110 = 0;
    r13_48 = __cxa_finalize;
    if (!ebp46) {
        r15d110 = 0xffffffff;
    }
    g38 = r12d50;
    g8 = 0;
    g10 = v111;
    g40 = r15d110;
    g18 = v112;
    g58 = 0;
    g20 = v113;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v114;
    g30 = v115;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_48) == 45) && !*reinterpret_cast<void***>(r13_48 + 1)) {
        rax116 = fun_2600();
        rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8);
        r13_48 = rax116;
    }
    *reinterpret_cast<void***>(&rdi117) = r12d50;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi117) + 4) = 0;
    rax118 = fun_26b0(rdi117);
    if (reinterpret_cast<signed char>(rax118) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_43;
    *reinterpret_cast<signed char*>(&eax65) = xlseek_part_0(0, 0, r13_48, rcx108, r8, r9);
    rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8 - 8 + 8);
    goto addr_6918_47;
    addr_6a80_93:
    zf119 = g28 == v120;
    if (!zf119 || (zf121 = g20 == v122, !zf121)) {
        if (!edx103) {
            rax123 = fun_2600();
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
            rsi102 = rax123;
        }
        rax124 = quotearg_style(4, rsi102, 4, rsi102);
        fun_2600();
        rcx108 = rax124;
        fun_28a0();
        rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi126 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi126) == 45) && !*reinterpret_cast<void***>(rsi126 + 1)) {
            rax127 = fun_2600();
            rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp125) - 8 + 8);
            rsi126 = rax127;
        }
        *reinterpret_cast<void***>(&rdi128) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi128) + 4) = 0;
        close_fd(rdi128, rsi126, rdi128, rsi126);
        rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp125) - 8 + 8);
        goto addr_6876_98;
    } else {
        if (!edx103) {
            rax129 = fun_2600();
            rsi102 = rax129;
        }
        *reinterpret_cast<void***>(&rdi130) = r12d50;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi130) + 4) = 0;
        close_fd(rdi130, rsi102, rdi130, rsi102);
        goto addr_674c_43;
    }
    addr_6830_89:
    if (!reinterpret_cast<int1_t>(eax101 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi131 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi131) == 45 && !*reinterpret_cast<void***>(rsi131 + 1)) {
            rax132 = fun_2600();
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
            rsi131 = rax132;
        }
        rax133 = quotearg_style(4, rsi131, 4, rsi131);
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
        r13_107 = rax133;
        goto addr_685e_97;
    }
    addr_6500_21:
    if (reinterpret_cast<signed char>(v134) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 8))) {
        while (rax135 = quotearg_n_style_colon(), fun_2600(), rcx = rax135, fun_28a0(), *reinterpret_cast<void***>(&rdi136) = *reinterpret_cast<void***>(rbx30 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi136) + 4) = 0, rax137 = fun_26b0(rdi136), rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax137) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx30 + 8) = reinterpret_cast<void**>(0);
            addr_6444_22:
            zf138 = print_headers == 0;
            if (!zf138) {
                r12_15 = reinterpret_cast<void**>(0);
                zf139 = __cxa_finalize == rbx30;
                *reinterpret_cast<unsigned char*>(&r12_15) = reinterpret_cast<uint1_t>(!zf139);
            }
            edx140 = *reinterpret_cast<void***>(rbx30 + 56);
            rcx = reinterpret_cast<void**>(0xffffffffffffffff);
            rax141 = dump_remainder(r12_15, r13_24, edx140, 0xffffffffffffffff, r8, r9);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            *reinterpret_cast<void***>(rbx30 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30 + 8)) + reinterpret_cast<unsigned char>(rax141));
            if (!rax141) 
                goto addr_647a_13;
            __cxa_finalize = rbx30;
            rdi142 = stdout;
            eax143 = fun_2940(rdi142, r13_24, rdi142, r13_24);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            if (!eax143) 
                goto addr_647a_13;
            rax144 = fun_2600();
            r12_15 = rax144;
            fun_2530();
            fun_28a0();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_23;
    } else {
        if (v145 != *reinterpret_cast<void***>(rbx30 + 8)) 
            goto addr_6444_22;
        edx146 = 0;
        *reinterpret_cast<unsigned char*>(&edx146) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v147) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx148) = edx146 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v149) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx148) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 24)) < reinterpret_cast<signed char>(v150));
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax151) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 24)) > reinterpret_cast<signed char>(v150))) - *reinterpret_cast<uint32_t*>(&rcx);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax151) + 4) = 0;
        if (static_cast<int32_t>(rax151 + rdx148 * 2)) 
            goto addr_6444_22;
        goto addr_647a_13;
    }
    addr_61d3_7:
    r15_152 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp27) + 32);
    rdi153 = r12_15;
    *reinterpret_cast<int32_t*>(&rdi153 + 4) = 0;
    rax154 = safe_read(rdi153, r15_152, rbx21, rcx);
    rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
    rbp23 = rax154;
    if (rax154 == 0xffffffffffffffff) {
        addr_6337_126:
        rax155 = quotearg_style(4, v8, 4, v8);
        r13_24 = rax155;
        fun_2600();
        fun_2530();
        rcx = r13_24;
        fun_28a0();
        rsp7 = rsp7 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        eax14 = 0;
        goto addr_6300_8;
    } else {
        rcx = v11;
        *reinterpret_cast<int32_t*>(&rbx21) = line_end;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(rax154));
        if (rbp23) {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp7) + reinterpret_cast<unsigned char>(rbp23) + 31) != *reinterpret_cast<signed char*>(&rbx21)) {
                --r14_16;
            }
        }
        while (1) {
            rdx156 = rbp23;
            while (rdx156 && (*reinterpret_cast<int32_t*>(&rsi157) = *reinterpret_cast<int32_t*>(&rbx21), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi157) + 4) = 0, rax158 = fun_28b0(r15_152, rsi157), rsp7 = rsp7 - 8 + 8, !!rax158)) {
                rdx156 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax158) - reinterpret_cast<unsigned char>(r15_152));
                if (!r14_16) 
                    goto addr_62c8_134;
                --r14_16;
            }
            if (r13_24 == v9) 
                goto addr_6379_137;
            r13_24 = r13_24 - 0x2000;
            *reinterpret_cast<void***>(&rdi159) = r12_15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi159) + 4) = 0;
            rax160 = fun_26b0(rdi159, rdi159);
            rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
            if (reinterpret_cast<signed char>(rax160) < reinterpret_cast<signed char>(0)) 
                goto addr_63b8_10;
            rdi161 = r12_15;
            *reinterpret_cast<int32_t*>(&rdi161 + 4) = 0;
            rax162 = safe_read(rdi161, r15_152, 0x2000, rcx);
            rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
            rbp23 = rax162;
            if (rax162 == 0xffffffffffffffff) 
                goto addr_6337_126;
            rcx = v11;
            *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(rax162));
            if (!rbp23) 
                goto addr_6330_2;
            *reinterpret_cast<int32_t*>(&rbx21) = line_end;
        }
    }
    addr_62c8_134:
    rsi163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp23 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(rdx156));
    if (rsi163) {
        xwrite_stdout_part_0(&rax158->f1, rsi163, rdx156, rcx, r8, r9);
        rsp7 = rsp7 - 8 + 8;
    }
    rax164 = dump_remainder(0, v8, r12_15, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(rbp23)) - reinterpret_cast<unsigned char>(r13_24), r8, r9);
    rsp7 = rsp7 - 8 + 8;
    rcx = v11;
    *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)) + reinterpret_cast<unsigned char>(rax164));
    eax14 = 1;
    goto addr_6300_8;
    addr_6379_137:
    *reinterpret_cast<void***>(&rdi165) = r12_15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi165) + 4) = 0;
    rax166 = fun_26b0(rdi165, rdi165);
    rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
    if (reinterpret_cast<signed char>(rax166) < reinterpret_cast<signed char>(0)) {
        addr_63b8_10:
        xlseek_part_0(r13_24, 0, v8, rcx, r8, r9);
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
        goto addr_63c7_6;
    } else {
        rax167 = dump_remainder(0, v8, r12_15, v10, r8, r9);
        rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
        rcx = v11;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax167) + reinterpret_cast<unsigned char>(r13_24));
        eax14 = 1;
        goto addr_6300_8;
    }
}

void recheck(void** rdi, void** esi, ...) {
    void** r15d3;
    void** rbx4;
    void* rsp5;
    void** rdi6;
    void** rax7;
    void** v8;
    uint32_t ebp9;
    uint32_t eax10;
    void** r13_11;
    void** eax12;
    void** r12d13;
    void** eax14;
    void** rdx15;
    void** rsi16;
    void** rax17;
    void** rax18;
    void** rsi19;
    void** rax20;
    int64_t rdi21;
    void** rsi22;
    void** rax23;
    int64_t rdi24;
    void* rax25;
    uint32_t r14d26;
    uint32_t eax27;
    void** r15_28;
    void** rsi29;
    int32_t eax30;
    uint32_t v31;
    void** rax32;
    uint32_t r14d33;
    void** edx34;
    uint32_t eax35;
    void** rax36;
    void** rdi37;
    int32_t eax38;
    uint32_t v39;
    void** eax40;
    void** rdi41;
    int32_t eax42;
    uint32_t v43;
    uint32_t eax44;
    void** rax45;
    void** rax46;
    void** rsi47;
    void** rax48;
    void** rax49;
    uint32_t v50;
    int1_t zf51;
    void** rsi52;
    void** rax53;
    int64_t rdi54;
    signed char al55;
    void* rsp56;
    int1_t zf57;
    void** rsi58;
    void** rax59;
    void** rax60;
    void** rsi61;
    uint32_t edx62;
    void** rax63;
    void** rax64;
    void* rsp65;
    void** r13_66;
    void** rcx67;
    void* rsp68;
    void** r15d69;
    void** v70;
    void** v71;
    int64_t v72;
    void** v73;
    void** v74;
    void** rax75;
    int64_t rdi76;
    void** rax77;
    void** r8_78;
    void** r9_79;
    void** v80;
    int64_t v81;
    void** rax82;
    void** rax83;
    void* rsp84;
    void** rsi85;
    void** rax86;
    int64_t rdi87;
    void** rax88;
    int64_t rdi89;
    void** rsi90;
    void** rax91;
    void** rax92;

    r15d3 = esi;
    rbx4 = rdi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi6 = *reinterpret_cast<void***>(rdi);
    rax7 = g28;
    v8 = rax7;
    ebp9 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi6) - 45);
    if (!ebp9) {
        ebp9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 1));
    }
    eax10 = *reinterpret_cast<unsigned char*>(rbx4 + 54);
    r13_11 = *reinterpret_cast<void***>(rbx4 + 60);
    if (!ebp9) {
        eax12 = r13_11;
        r12d13 = reinterpret_cast<void**>(0);
    } else {
        eax14 = open_safer(rdi6);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        r12d13 = eax14;
        eax12 = *reinterpret_cast<void***>(rbx4 + 60);
    }
    *reinterpret_cast<unsigned char*>(&rdx15) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx4 + 56) == 0xffffffff);
    if (*reinterpret_cast<unsigned char*>(&rdx15) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax12 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_9:
            ++r13_11;
            if (r13_11) {
                addr_6b83_10:
                rsi16 = *reinterpret_cast<void***>(rbx4);
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi16) == 45) && !*reinterpret_cast<void***>(rsi16 + 1)) {
                    rax17 = fun_2600();
                    rsi16 = rax17;
                }
                rax18 = quotearg_style(4, rsi16, 4, rsi16);
                r13_11 = rax18;
                fun_2600();
                fun_28a0();
            }
            addr_6941_14:
            rsi19 = *reinterpret_cast<void***>(rbx4);
            addr_66f0_15:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi19) == 45) && !*reinterpret_cast<void***>(rsi19 + 1)) {
                rax20 = fun_2600();
                rsi19 = rax20;
            }
            *reinterpret_cast<void***>(&rdi21) = r12d13;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
            close_fd(rdi21, rsi19, rdi21, rsi19);
            rsi22 = *reinterpret_cast<void***>(rbx4);
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi22) == 45) && !*reinterpret_cast<void***>(rsi22 + 1)) {
                rax23 = fun_2600();
                rsi22 = rax23;
            }
            *reinterpret_cast<void***>(&rdi24) = *reinterpret_cast<void***>(rbx4 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
            close_fd(rdi24, rsi22, rdi24, rsi22);
            *reinterpret_cast<void***>(rbx4 + 56) = reinterpret_cast<void**>(0xffffffff);
            addr_674c_20:
            rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax25);
        return;
    }
    r14d26 = reopen_inaccessible_files;
    eax27 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d26)) {
        if (r12d13 == 0xffffffff) {
            addr_6918_24:
            *reinterpret_cast<unsigned char*>(rbx4 + 54) = 0;
            r15_28 = *reinterpret_cast<void***>(rbx4);
            if (!*reinterpret_cast<signed char*>(&eax27)) {
                rsi29 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 16);
                eax30 = fun_2720(r15_28, rsi29, r15_28, rsi29);
                if (eax30 || (v31 & 0xf000) != 0xa000) {
                    addr_66b9_26:
                    rax32 = fun_2530();
                    r14d33 = *reinterpret_cast<unsigned char*>(rbx4 + 54);
                    rsi19 = *reinterpret_cast<void***>(rbx4);
                    edx34 = *reinterpret_cast<void***>(rax32);
                    r15_28 = rsi19;
                    *reinterpret_cast<void***>(rbx4 + 60) = edx34;
                    if (*reinterpret_cast<signed char*>(&r14d33)) {
                        eax35 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi19) - 45);
                        if (!eax35) {
                            eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi19 + 1));
                            if (edx34 == r13_11) 
                                goto addr_66f0_15;
                        } else {
                            if (edx34 == r13_11) {
                                goto addr_66f0_15;
                            }
                        }
                        if (!eax35) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi19 = *reinterpret_cast<void***>(rbx4);
                        goto addr_66f0_15;
                    }
                } else {
                    goto addr_6c10_35;
                }
            } else {
                rax36 = fun_2530();
                *reinterpret_cast<void***>(rbx4 + 60) = *reinterpret_cast<void***>(rax36);
            }
        } else {
            *reinterpret_cast<unsigned char*>(rbx4 + 54) = 1;
            if (*reinterpret_cast<signed char*>(&eax27) || ((rdi37 = *reinterpret_cast<void***>(rbx4), eax38 = fun_2720(rdi37, reinterpret_cast<int64_t>(rsp5) + 16), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8), !!eax38) || (v39 & 0xf000) != 0xa000)) {
                addr_67a3_38:
                eax40 = fun_2980();
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                if (reinterpret_cast<signed char>(eax40) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_26; else 
                    goto addr_67b3_39;
            } else {
                goto addr_6c10_35;
            }
        }
    } else {
        *reinterpret_cast<unsigned char*>(rbx4 + 54) = 1;
        if (*reinterpret_cast<signed char*>(&eax27) || ((rdi41 = *reinterpret_cast<void***>(rbx4), eax42 = fun_2720(rdi41, reinterpret_cast<int64_t>(rsp5) + 16), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8), !!eax42) || (v43 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d13 == 0xffffffff)) 
                goto addr_66b9_26;
            goto addr_67a3_38;
        } else {
            goto addr_6c10_35;
        }
    }
    eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_28) - 45);
    if (!eax44) {
        eax44 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_28 + 1));
        if (!*reinterpret_cast<signed char*>(&eax10)) 
            goto addr_6941_14;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax10)) 
            goto addr_6941_14;
    }
    if (!eax44) {
        rax45 = fun_2600();
        r15_28 = rax45;
    }
    rax46 = quotearg_style(4, r15_28, 4, r15_28);
    r13_11 = rax46;
    fun_2600();
    fun_28a0();
    rsi19 = *reinterpret_cast<void***>(rbx4);
    goto addr_66f0_15;
    addr_6c10_35:
    rsi47 = *reinterpret_cast<void***>(rbx4);
    *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(1);
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi47) == 45) && !*reinterpret_cast<void***>(rsi47 + 1)) {
        rax48 = fun_2600();
        rsi47 = rax48;
    }
    rax49 = quotearg_style(4, rsi47, 4, rsi47);
    r13_11 = rax49;
    fun_2600();
    fun_28a0();
    rsi19 = *reinterpret_cast<void***>(rbx4);
    goto addr_66f0_15;
    addr_67b3_39:
    if ((v50 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v50 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
        *reinterpret_cast<unsigned char*>(rbx4 + 54) = 0;
        if (!*reinterpret_cast<signed char*>(&r14d26) || (zf51 = follow_mode == 1, !zf51)) {
            *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(1);
            if (*reinterpret_cast<signed char*>(&eax10)) 
                goto addr_6b6d_55;
            if (r13_11 == 0xffffffff) 
                goto addr_6941_14;
            addr_6b6d_55:
            fun_2600();
            goto addr_6b83_10;
        } else {
            *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0);
            if (!*reinterpret_cast<signed char*>(&eax10)) 
                goto addr_6e01_9;
            goto addr_6b83_10;
        }
    }
    rsi52 = *reinterpret_cast<void***>(rbx4);
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi52) == 45) && !*reinterpret_cast<void***>(rsi52 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx15) = 5;
        *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
        rax53 = fun_2600();
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        rsi52 = rax53;
    }
    *reinterpret_cast<void***>(&rdi54) = r12d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
    al55 = fremote(rdi54, rsi52, rdx15);
    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    *reinterpret_cast<signed char*>(rbx4 + 53) = al55;
    if (al55 && (zf57 = disable_inotify == 0, zf57)) {
        rsi58 = *reinterpret_cast<void***>(rbx4);
        *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi58) == 45) && !*reinterpret_cast<void***>(rsi58 + 1)) {
            rax59 = fun_2600();
            rsi58 = rax59;
        }
        rax60 = quotearg_style(4, rsi58, 4, rsi58);
        r13_11 = rax60;
        fun_2600();
        fun_28a0();
        *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0x101);
        goto addr_6941_14;
    }
    r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) & 0xfffffffd);
    *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0);
    if (r13_11) 
        goto addr_6830_66;
    rsi61 = *reinterpret_cast<void***>(rbx4);
    edx62 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi61) - 45);
    if (!edx62) {
        edx62 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi61 + 1));
    }
    if (*reinterpret_cast<void***>(rbx4 + 56) != 0xffffffff) 
        goto addr_6a80_70;
    if (!edx62) {
        rax63 = fun_2600();
        rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        rsi61 = rax63;
    }
    rax64 = quotearg_style(4, rsi61, 4, rsi61);
    rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
    r13_66 = rax64;
    addr_685e_74:
    fun_2600();
    rcx67 = r13_66;
    fun_28a0();
    rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8 - 8 + 8);
    addr_6876_75:
    r15d69 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r15d3)));
    r13_11 = *reinterpret_cast<void***>(rbx4);
    if (!ebp9) {
        r15d69 = reinterpret_cast<void**>(0xffffffff);
    }
    *reinterpret_cast<void***>(rbx4 + 56) = r12d13;
    *reinterpret_cast<void***>(rbx4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx4 + 16) = v70;
    *reinterpret_cast<void***>(rbx4 + 64) = r15d69;
    *reinterpret_cast<void***>(rbx4 + 24) = v71;
    *reinterpret_cast<void***>(rbx4 + 88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int64_t*>(rbx4 + 32) = v72;
    *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx4 + 40) = v73;
    *reinterpret_cast<void***>(rbx4 + 48) = v74;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_11) == 45) && !*reinterpret_cast<void***>(r13_11 + 1)) {
        rax75 = fun_2600();
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8);
        r13_11 = rax75;
    }
    *reinterpret_cast<void***>(&rdi76) = r12d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi76) + 4) = 0;
    rax77 = fun_26b0(rdi76);
    if (reinterpret_cast<signed char>(rax77) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_20;
    *reinterpret_cast<signed char*>(&eax27) = xlseek_part_0(0, 0, r13_11, rcx67, r8_78, r9_79);
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8);
    goto addr_6918_24;
    addr_6a80_70:
    if (*reinterpret_cast<void***>(rbx4 + 40) != v80 || *reinterpret_cast<int64_t*>(rbx4 + 32) != v81) {
        if (!edx62) {
            rax82 = fun_2600();
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rsi61 = rax82;
        }
        rax83 = quotearg_style(4, rsi61, 4, rsi61);
        fun_2600();
        rcx67 = rax83;
        fun_28a0();
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi85 = *reinterpret_cast<void***>(rbx4);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi85) == 45) && !*reinterpret_cast<void***>(rsi85 + 1)) {
            rax86 = fun_2600();
            rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
            rsi85 = rax86;
        }
        *reinterpret_cast<void***>(&rdi87) = *reinterpret_cast<void***>(rbx4 + 56);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi87) + 4) = 0;
        close_fd(rdi87, rsi85, rdi87, rsi85);
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
        goto addr_6876_75;
    } else {
        if (!edx62) {
            rax88 = fun_2600();
            rsi61 = rax88;
        }
        *reinterpret_cast<void***>(&rdi89) = r12d13;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi89) + 4) = 0;
        close_fd(rdi89, rsi61, rdi89, rsi61);
        goto addr_674c_20;
    }
    addr_6830_66:
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx4 + 56) == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi90 = *reinterpret_cast<void***>(rbx4);
        if (*reinterpret_cast<void***>(rsi90) == 45 && !*reinterpret_cast<void***>(rsi90 + 1)) {
            rax91 = fun_2600();
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rsi90 = rax91;
        }
        rax92 = quotearg_style(4, rsi90, 4, rsi90);
        rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        r13_66 = rax92;
        goto addr_685e_74;
    }
}

/* xlseek.part.0 */
signed char xlseek_part_0(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    offtostr(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 40);
    fun_2530();
    if (esi == 1) {
        while (1) {
            quotearg_n_style_colon();
            addr_5f45_3:
            fun_2600();
            fun_28a0();
            fun_2910();
        }
    } else {
        quotearg_n_style_colon();
        goto addr_5f45_3;
    }
}

void check_fspec(void** rdi, void** rsi) {
    void** rbx3;
    void* rsp4;
    void** edi5;
    void** rax6;
    void** v7;
    void* rax8;
    void** r13_9;
    void** rbp10;
    void** rax11;
    void** eax12;
    void** r12_13;
    void** eax14;
    void** rax15;
    int64_t rdi16;
    void** rdx17;
    void** rcx18;
    void** r8_19;
    void** r9_20;
    void* rsp21;
    void** rdi22;
    void** rax23;
    void** v24;
    uint32_t ebp25;
    uint32_t eax26;
    void** r13_27;
    void** eax28;
    void** r12d29;
    void** eax30;
    uint1_t zf31;
    void** rsi32;
    void** rax33;
    void** rax34;
    void** rsi35;
    void** rax36;
    int64_t rdi37;
    void** rsi38;
    void** rax39;
    int64_t rdi40;
    void* rax41;
    int64_t v42;
    uint32_t r14d43;
    uint32_t eax44;
    void** r15_45;
    void** rsi46;
    int32_t eax47;
    uint32_t v48;
    void** rax49;
    uint32_t r14d50;
    void** edx51;
    uint32_t eax52;
    void** rax53;
    void** rsi54;
    void** rdi55;
    int32_t eax56;
    uint32_t v57;
    void** eax58;
    void** rdi59;
    void** rsi60;
    int32_t eax61;
    uint32_t v62;
    uint32_t eax63;
    void** rax64;
    void** rax65;
    void** rsi66;
    void** rax67;
    void** rax68;
    uint32_t v69;
    int1_t zf70;
    void** rsi71;
    void** rax72;
    int64_t rdi73;
    signed char al74;
    void* rsp75;
    int1_t zf76;
    void** rsi77;
    void** rax78;
    void** rax79;
    void** eax80;
    void** rsi81;
    uint32_t edx82;
    void** rax83;
    void** rax84;
    void* rsp85;
    void** r13_86;
    void** rcx87;
    void* rsp88;
    uint32_t r15d89;
    int64_t v90;
    int64_t v91;
    int64_t v92;
    void** v93;
    int32_t v94;
    void** rax95;
    int64_t rdi96;
    void** rax97;
    void** r8_98;
    void** r9_99;
    int1_t zf100;
    void** v101;
    int1_t zf102;
    int64_t v103;
    void** rax104;
    void** rax105;
    void* rsp106;
    void** rsi107;
    void** rax108;
    int64_t rdi109;
    void** rax110;
    int64_t rdi111;
    void** rsi112;
    void** rax113;
    void** rax114;
    void** v115;
    void** rax116;
    int64_t rdi117;
    void** rax118;
    int1_t zf119;
    void** edx120;
    void** r8_121;
    void** r9_122;
    void** rax123;
    struct s0* rdi124;
    int32_t eax125;
    void** rax126;
    void** v127;
    int32_t edx128;
    void** v129;
    int64_t rdx130;
    void** v131;
    void** v132;
    int64_t rax133;

    rbx3 = rdi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi5 = *reinterpret_cast<void***>(rdi + 56);
    rax6 = g28;
    v7 = rax6;
    if (edi5 == 0xffffffff) {
        addr_647a_2:
        rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (rax8) {
            fun_2630();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        } else {
            return;
        }
    } else {
        r13_9 = *reinterpret_cast<void***>(rbx3);
        rbp10 = rsi;
        if (*reinterpret_cast<void***>(r13_9) == 45 && !*reinterpret_cast<void***>(r13_9 + 1)) {
            rax11 = fun_2600();
            r13_9 = rax11;
            eax12 = fun_2980();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            r12_13 = eax12;
            if (!eax12) 
                goto addr_6431_7; else 
                goto addr_64df_8;
        }
        eax14 = fun_2980();
        rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        r12_13 = eax14;
        if (eax14) {
            addr_64df_8:
            rax15 = fun_2530();
            *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbx3 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
            *reinterpret_cast<void***>(rbx3 + 60) = *reinterpret_cast<void***>(rax15);
            close_fd(rdi16, r13_9, rdi16, r13_9);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx3 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_2;
        } else {
            addr_6431_7:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx3 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_10; else 
                goto addr_6444_11;
        }
    }
    addr_65f9_12:
    rdx17 = r13_9;
    xlseek_part_0(0, 0, rdx17, rcx18, r8_19, r9_20);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi22 = __cxa_finalize;
    rax23 = g28;
    v24 = rax23;
    ebp25 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi22) - 45);
    if (!ebp25) {
        ebp25 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 1));
    }
    eax26 = g36;
    r13_27 = g3c;
    if (!ebp25) {
        eax28 = r13_27;
        r12d29 = reinterpret_cast<void**>(0);
    } else {
        eax30 = open_safer(rdi22, rdi22);
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
        r12d29 = eax30;
        eax28 = g3c;
    }
    zf31 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx17) = zf31;
    if (*reinterpret_cast<unsigned char*>(&rdx17) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax28 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_21:
            ++r13_27;
            if (r13_27) {
                addr_6b83_22:
                rsi32 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi32) == 45) && !*reinterpret_cast<void***>(rsi32 + 1)) {
                    rax33 = fun_2600();
                    rsi32 = rax33;
                }
                rax34 = quotearg_style(4, rsi32, 4, rsi32);
                r13_27 = rax34;
                fun_2600();
                fun_28a0();
            }
            addr_6941_26:
            rsi35 = __cxa_finalize;
            addr_66f0_27:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi35) == 45) && !*reinterpret_cast<void***>(rsi35 + 1)) {
                rax36 = fun_2600();
                rsi35 = rax36;
            }
            *reinterpret_cast<void***>(&rdi37) = r12d29;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0;
            close_fd(rdi37, rsi35, rdi37, rsi35);
            rsi38 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi38) == 45) && !*reinterpret_cast<void***>(rsi38 + 1)) {
                rax39 = fun_2600();
                rsi38 = rax39;
            }
            *reinterpret_cast<void***>(&rdi40) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi40) + 4) = 0;
            close_fd(rdi40, rsi38, rdi40, rsi38);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_32:
            rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
        } while (rax41);
        goto v42;
    }
    r14d43 = reopen_inaccessible_files;
    eax44 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d43)) {
        if (r12d29 == 0xffffffff) {
            addr_6918_36:
            g36 = 0;
            r15_45 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax44)) {
                rsi46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16);
                eax47 = fun_2720(r15_45, rsi46, r15_45, rsi46);
                if (eax47 || (v48 & 0xf000) != 0xa000) {
                    addr_66b9_38:
                    rax49 = fun_2530();
                    r14d50 = g36;
                    rsi35 = __cxa_finalize;
                    edx51 = *reinterpret_cast<void***>(rax49);
                    r15_45 = rsi35;
                    g3c = edx51;
                    if (*reinterpret_cast<signed char*>(&r14d50)) {
                        eax52 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi35) - 45);
                        if (!eax52) {
                            eax52 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi35 + 1));
                            if (edx51 == r13_27) 
                                goto addr_66f0_27;
                        } else {
                            if (edx51 == r13_27) {
                                goto addr_66f0_27;
                            }
                        }
                        if (!eax52) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi35 = __cxa_finalize;
                        goto addr_66f0_27;
                    }
                } else {
                    goto addr_6c10_47;
                }
            } else {
                rax53 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax53);
            }
        } else {
            g36 = 1;
            rsi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16);
            if (*reinterpret_cast<signed char*>(&eax44) || ((rdi55 = __cxa_finalize, eax56 = fun_2720(rdi55, rsi54, rdi55, rsi54), rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8), !!eax56) || (v57 & 0xf000) != 0xa000)) {
                addr_67a3_50:
                eax58 = fun_2980();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (reinterpret_cast<signed char>(eax58) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_38; else 
                    goto addr_67b3_51;
            } else {
                goto addr_6c10_47;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax44) || ((rdi59 = __cxa_finalize, rsi60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16), eax61 = fun_2720(rdi59, rsi60, rdi59, rsi60), rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8), !!eax61) || (v62 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d29 == 0xffffffff)) 
                goto addr_66b9_38;
            goto addr_67a3_50;
        } else {
            goto addr_6c10_47;
        }
    }
    eax63 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_45) - 45);
    if (!eax63) {
        eax63 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_45 + 1));
        if (!*reinterpret_cast<signed char*>(&eax26)) 
            goto addr_6941_26;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax26)) 
            goto addr_6941_26;
    }
    if (!eax63) {
        rax64 = fun_2600();
        r15_45 = rax64;
    }
    rax65 = quotearg_style(4, r15_45, 4, r15_45);
    r13_27 = rax65;
    fun_2600();
    fun_28a0();
    rsi35 = __cxa_finalize;
    goto addr_66f0_27;
    addr_6c10_47:
    rsi66 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi66) == 45) && !*reinterpret_cast<void***>(rsi66 + 1)) {
        rax67 = fun_2600();
        rsi66 = rax67;
    }
    rax68 = quotearg_style(4, rsi66, 4, rsi66);
    r13_27 = rax68;
    fun_2600();
    fun_28a0();
    rsi35 = __cxa_finalize;
    goto addr_66f0_27;
    addr_67b3_51:
    if ((v69 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v69 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d43) || (zf70 = follow_mode == 1, !zf70)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax26)) 
                goto addr_6b6d_67;
            if (r13_27 == 0xffffffff) 
                goto addr_6941_26;
            addr_6b6d_67:
            fun_2600();
            goto addr_6b83_22;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax26)) 
                goto addr_6e01_21;
            goto addr_6b83_22;
        }
    }
    rsi71 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi71) == 45) && !*reinterpret_cast<void***>(rsi71 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx17) = 5;
        *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
        rax72 = fun_2600();
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
        rsi71 = rax72;
    }
    *reinterpret_cast<void***>(&rdi73) = r12d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
    al74 = fremote(rdi73, rsi71, rdx17);
    rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al74;
    if (al74 && (zf76 = disable_inotify == 0, zf76)) {
        rsi77 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi77) == 45) && !*reinterpret_cast<void***>(rsi77 + 1)) {
            rax78 = fun_2600();
            rsi77 = rax78;
        }
        rax79 = quotearg_style(4, rsi77, 4, rsi77);
        r13_27 = rax79;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_26;
    }
    r13_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_27) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax80 = g38;
    if (r13_27) 
        goto addr_6830_78;
    rsi81 = __cxa_finalize;
    edx82 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi81) - 45);
    if (!edx82) {
        edx82 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi81 + 1));
    }
    if (eax80 != 0xffffffff) 
        goto addr_6a80_82;
    if (!edx82) {
        rax83 = fun_2600();
        rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
        rsi81 = rax83;
    }
    rax84 = quotearg_style(4, rsi81, 4, rsi81);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
    r13_86 = rax84;
    addr_685e_86:
    fun_2600();
    rcx87 = r13_86;
    fun_28a0();
    rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp85) - 8 + 8 - 8 + 8);
    addr_6876_87:
    r15d89 = 0;
    r13_27 = __cxa_finalize;
    if (!ebp25) {
        r15d89 = 0xffffffff;
    }
    g38 = r12d29;
    g8 = 0;
    g10 = v90;
    g40 = r15d89;
    g18 = v91;
    g58 = 0;
    g20 = v92;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v93;
    g30 = v94;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_27) == 45) && !*reinterpret_cast<void***>(r13_27 + 1)) {
        rax95 = fun_2600();
        rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8);
        r13_27 = rax95;
    }
    *reinterpret_cast<void***>(&rdi96) = r12d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
    rax97 = fun_26b0(rdi96);
    if (reinterpret_cast<signed char>(rax97) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_32;
    *reinterpret_cast<signed char*>(&eax44) = xlseek_part_0(0, 0, r13_27, rcx87, r8_98, r9_99);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8 - 8 + 8);
    goto addr_6918_36;
    addr_6a80_82:
    zf100 = g28 == v101;
    if (!zf100 || (zf102 = g20 == v103, !zf102)) {
        if (!edx82) {
            rax104 = fun_2600();
            rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
            rsi81 = rax104;
        }
        rax105 = quotearg_style(4, rsi81, 4, rsi81);
        fun_2600();
        rcx87 = rax105;
        fun_28a0();
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi107 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi107) == 45) && !*reinterpret_cast<void***>(rsi107 + 1)) {
            rax108 = fun_2600();
            rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
            rsi107 = rax108;
        }
        *reinterpret_cast<void***>(&rdi109) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0;
        close_fd(rdi109, rsi107, rdi109, rsi107);
        rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
        goto addr_6876_87;
    } else {
        if (!edx82) {
            rax110 = fun_2600();
            rsi81 = rax110;
        }
        *reinterpret_cast<void***>(&rdi111) = r12d29;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi111) + 4) = 0;
        close_fd(rdi111, rsi81, rdi111, rsi81);
        goto addr_674c_32;
    }
    addr_6830_78:
    if (!reinterpret_cast<int1_t>(eax80 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi112 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi112) == 45 && !*reinterpret_cast<void***>(rsi112 + 1)) {
            rax113 = fun_2600();
            rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
            rsi112 = rax113;
        }
        rax114 = quotearg_style(4, rsi112, 4, rsi112);
        rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
        r13_86 = rax114;
        goto addr_685e_86;
    }
    addr_6500_10:
    if (reinterpret_cast<signed char>(v115) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 8))) {
        while (rax116 = quotearg_n_style_colon(), fun_2600(), rcx18 = rax116, fun_28a0(), *reinterpret_cast<void***>(&rdi117) = *reinterpret_cast<void***>(rbx3 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi117) + 4) = 0, rax118 = fun_26b0(rdi117), rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax118) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx3 + 8) = reinterpret_cast<void**>(0);
            addr_6444_11:
            zf119 = print_headers == 0;
            if (!zf119) {
                r12_13 = reinterpret_cast<void**>(0);
                *reinterpret_cast<unsigned char*>(&r12_13) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbp10) != rbx3);
            }
            edx120 = *reinterpret_cast<void***>(rbx3 + 56);
            rcx18 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax123 = dump_remainder(r12_13, r13_9, edx120, 0xffffffffffffffff, r8_121, r9_122);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            *reinterpret_cast<void***>(rbx3 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx3 + 8)) + reinterpret_cast<unsigned char>(rax123));
            if (!rax123) 
                goto addr_647a_2;
            *reinterpret_cast<void***>(rbp10) = rbx3;
            rdi124 = stdout;
            eax125 = fun_2940(rdi124, r13_9, rdi124, r13_9);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            if (!eax125) 
                goto addr_647a_2;
            rax126 = fun_2600();
            r12_13 = rax126;
            fun_2530();
            fun_28a0();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_12;
    } else {
        if (v127 != *reinterpret_cast<void***>(rbx3 + 8)) 
            goto addr_6444_11;
        edx128 = 0;
        *reinterpret_cast<unsigned char*>(&edx128) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v129) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx130) = edx128 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v131) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx130) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 24)) < reinterpret_cast<signed char>(v132));
        *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax133) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 24)) > reinterpret_cast<signed char>(v132))) - *reinterpret_cast<uint32_t*>(&rcx18);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax133) + 4) = 0;
        if (static_cast<int32_t>(rax133 + rdx130 * 2)) 
            goto addr_6444_11;
        goto addr_647a_2;
    }
}

/* pretty_name.isra.0 */
void** pretty_name_isra_0(void** rdi, void** rsi, ...) {
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || *reinterpret_cast<void***>(rdi + 1)) {
        return rdi;
    }
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xe590);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xe590);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xe590) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x67d8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xe590);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x663f]");
        if (1) 
            goto addr_805c_6;
        __asm__("comiss xmm1, [rip+0x6636]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x6628]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_803c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_8032_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_803c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_8032_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_8032_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_803c_13:
        return 0;
    } else {
        addr_805c_6:
        return r8_3;
    }
}

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_2800(void** rdi, void** rsi, void** rdx);

int64_t fun_2520(void** rdi, ...);

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s3* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s4* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_80c6_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_80b8_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_80c6_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_813e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_813e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_29a5_12;
                    addr_813e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_80b8_3;
            }
            rsi16 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_29a5_12;
            r13_18 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_18->f0) 
                goto addr_8178_16;
            r13_18->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_8178_16:
            rax19 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax19) {
                rax19 = fun_2800(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_81ea_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_29a5_12:
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    fun_2520(rdi12, rdi12);
    addr_81ea_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
        fun_2520(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_7eef_22:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_7e94_25;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_7f00_29;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_7f00_29;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_7eef_22;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_7e94_25;
            }
        }
    }
    addr_7ef1_33:
    return rax11;
    addr_7e94_25:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_7ef1_33;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_7f00_29:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_2610();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2610();
    if (r8d > 10) {
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xe680 + rax11 * 4) + 0xe680;
    }
}

struct s5 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0xb0);

uint32_t nslots = 1;

void** xpalloc();

void fun_26d0();

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2510(void** rdi, void** rsi, ...);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s5* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s6* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0xa65f;
    rax8 = fun_2530();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
        fun_2520(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x130b0) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x88b1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0xa6eb;
            fun_26d0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s6*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x13140) {
                fun_2510(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0xa77a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2630();
        } else {
            return r14_19;
        }
    }
}

uint32_t start_lines(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rsi2;
    void* rsp7;
    void** v8;
    void** rax9;
    void** v10;
    uint32_t eax11;
    void** rdx12;
    void** r15d13;
    void** rbx14;
    void** r14_15;
    void** r13_16;
    void** rax17;
    void** r12_18;
    void** rbp19;
    void** rdx20;
    struct s2* rax21;
    void** rsp22;
    void** rax23;
    void** v24;
    void* rdx25;
    int64_t v26;
    void** r14_27;
    void** rax28;
    void** rax29;
    void** rax30;
    void** rax31;
    void** rax32;

    rsi2 = esi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 40);
    v8 = rdi;
    rax9 = g28;
    v10 = rax9;
    eax11 = 0;
    if (!rdx) {
        addr_5d44_2:
        rdx12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
        if (!rdx12) {
            return eax11;
        }
    } else {
        r15d13 = rsi2;
        rbx14 = rdx;
        r14_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        r13_16 = rcx;
        while (rsi2 = r14_15, rdi = r15d13, *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax17 = safe_read(rdi, rsi2, 0x2000, rcx), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), !!rax17) {
            if (rax17 == 0xffffffffffffffff) 
                goto addr_5d8e_7;
            *reinterpret_cast<void***>(r13_16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_16)) + reinterpret_cast<unsigned char>(rax17));
            r12_18 = reinterpret_cast<void**>(static_cast<int32_t>(line_end));
            rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_15) + reinterpret_cast<unsigned char>(rax17));
            rdi = r14_15;
            do {
                rsi2 = r12_18;
                rdx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) - reinterpret_cast<unsigned char>(rdi));
                rax21 = fun_2700();
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                if (!rax21) 
                    break;
                rdi = reinterpret_cast<void**>(&rax21->f1);
                --rbx14;
            } while (rbx14);
            goto addr_5d3d_11;
        }
        goto addr_5d87_12;
    }
    fun_2630();
    rsp22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 16);
    rax23 = g28;
    v24 = rax23;
    if (rdx12) 
        goto addr_5e1c_15;
    addr_5e68_16:
    addr_5e6a_17:
    rdx25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        goto v26;
    }
    fun_2630();
    offtostr(rdi, reinterpret_cast<uint64_t>(rsp22 - 8) + 8 - 8 - 8 - 8 - 8 - 40);
    fun_2530();
    if (rsi2 != 1) 
        goto addr_5f31_21;
    while (1) {
        quotearg_n_style_colon();
        addr_5f45_23:
        fun_2600();
        fun_28a0();
        fun_2910();
    }
    addr_5f31_21:
    quotearg_n_style_colon();
    goto addr_5f45_23;
    addr_5e1c_15:
    r13_16 = rdi;
    rbp19 = rsi2;
    r14_27 = rdx12;
    rbx14 = rcx;
    r12_18 = rsp22;
    do {
        rsi2 = r12_18;
        rdi = rbp19;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        rax28 = safe_read(rdi, rsi2, 0x2000, rcx);
        rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8);
        if (!rax28) 
            break;
        if (rax28 == 0xffffffffffffffff) 
            goto addr_5e90_26;
        *reinterpret_cast<void***>(rbx14) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14)) + reinterpret_cast<unsigned char>(rax28));
        if (reinterpret_cast<unsigned char>(rax28) > reinterpret_cast<unsigned char>(r14_27)) 
            goto addr_5e60_28;
        r14_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_27) - reinterpret_cast<unsigned char>(rax28));
    } while (r14_27);
    goto addr_5e68_16;
    goto addr_5e6a_17;
    addr_5e90_26:
    quotearg_style(4, r13_16, 4, r13_16);
    fun_2600();
    rax29 = fun_2530();
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi2 = *reinterpret_cast<void***>(rax29);
    fun_28a0();
    rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_5e6a_17;
    addr_5e60_28:
    rax30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax28) - reinterpret_cast<unsigned char>(r14_27));
    rsi2 = rax30;
    if (rax30) {
        rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r14_27));
        xwrite_stdout_part_0(rdi, rsi2, 0x2000, rcx, r8, r9);
        rsp22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22 - 8) + 8);
        goto addr_5e68_16;
    }
    addr_5d87_12:
    eax11 = 0xffffffff;
    goto addr_5d44_2;
    addr_5d8e_7:
    rax31 = quotearg_style(4, v8, 4, v8);
    fun_2600();
    rax32 = fun_2530();
    rcx = rax31;
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi2 = *reinterpret_cast<void***>(rax32);
    fun_28a0();
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax11 = 1;
    goto addr_5d44_2;
    addr_5d3d_11:
    eax11 = 0;
    if (reinterpret_cast<unsigned char>(rbp19) > reinterpret_cast<unsigned char>(rdi) && (rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) - reinterpret_cast<unsigned char>(rdi)), !!rbp19)) {
        rsi2 = rbp19;
        xwrite_stdout_part_0(rdi, rsi2, rdx20, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        eax11 = 0;
        goto addr_5d44_2;
    }
}

uint32_t start_bytes(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rsi2;
    void** rsp7;
    void** rax8;
    void** v9;
    uint32_t eax10;
    void** r13_11;
    int64_t rbp12;
    void** r14_13;
    void** rbx14;
    void** r12_15;
    void** rax16;
    void* rdx17;
    void** rax18;
    void** rax19;

    rsi2 = esi;
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 16);
    rax8 = g28;
    v9 = rax8;
    if (!rdx) {
        addr_5e68_2:
        eax10 = 0;
    } else {
        r13_11 = rdi;
        *reinterpret_cast<void***>(&rbp12) = rsi2;
        r14_13 = rdx;
        rbx14 = rcx;
        r12_15 = rsp7;
        do {
            rsi2 = r12_15;
            rdi = *reinterpret_cast<void***>(&rbp12);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rax16 = safe_read(rdi, rsi2, 0x2000, rcx);
            rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
            if (!rax16) 
                goto addr_5e57_5;
            if (rax16 == 0xffffffffffffffff) 
                goto addr_5e90_7;
            *reinterpret_cast<void***>(rbx14) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14)) + reinterpret_cast<unsigned char>(rax16));
            if (reinterpret_cast<unsigned char>(rax16) > reinterpret_cast<unsigned char>(r14_13)) 
                goto addr_5e60_9;
            r14_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_13) - reinterpret_cast<unsigned char>(rax16));
        } while (r14_13);
        goto addr_5e68_2;
    }
    addr_5e6a_11:
    rdx17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (!rdx17) {
        return eax10;
    }
    fun_2630();
    offtostr(rdi, reinterpret_cast<uint64_t>(rsp7 - 8) + 8 - 8 - 8 - 8 - 8 - 40);
    fun_2530();
    if (rsi2 != 1) 
        goto addr_5f31_15;
    while (1) {
        quotearg_n_style_colon();
        addr_5f45_17:
        fun_2600();
        fun_28a0();
        fun_2910();
    }
    addr_5f31_15:
    quotearg_n_style_colon();
    goto addr_5f45_17;
    addr_5e57_5:
    eax10 = 0xffffffff;
    goto addr_5e6a_11;
    addr_5e90_7:
    quotearg_style(4, r13_11, 4, r13_11);
    fun_2600();
    rax18 = fun_2530();
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi2 = *reinterpret_cast<void***>(rax18);
    fun_28a0();
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax10 = 1;
    goto addr_5e6a_11;
    addr_5e60_9:
    rax19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) - reinterpret_cast<unsigned char>(r14_13));
    rsi2 = rax19;
    if (rax19) {
        rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r14_13));
        xwrite_stdout_part_0(rdi, rsi2, 0x2000, rcx, r8, r9);
        rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
        goto addr_5e68_2;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x130c0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s7 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s7* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s7* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xe623);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xe61c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xe627);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xe618);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g12d78 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g12d78;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_24e3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_24f3() {
    __asm__("cli ");
    goto getenv;
}

int64_t raise = 0x2040;

void fun_2503() {
    __asm__("cli ");
    goto raise;
}

int64_t free = 0x2050;

void fun_2513() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2523() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2533() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2543() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2553() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_2563() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x20b0;

void fun_2573() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x20c0;

void fun_2583() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t strtod = 0x20d0;

void fun_2593() {
    __asm__("cli ");
    goto strtod;
}

int64_t fcntl = 0x20e0;

void fun_25a3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clearerr_unlocked = 0x20f0;

void fun_25b3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x2100;

void fun_25c3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t strtod_l = 0x2110;

void fun_25d3() {
    __asm__("cli ");
    goto strtod_l;
}

int64_t fclose = 0x2120;

void fun_25e3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2130;

void fun_25f3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2140;

void fun_2603() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2150;

void fun_2613() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2160;

void fun_2623() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2170;

void fun_2633() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2180;

void fun_2643() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2190;

void fun_2653() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21a0;

void fun_2663() {
    __asm__("cli ");
    goto strchr;
}

int64_t newlocale = 0x21b0;

void fun_2673() {
    __asm__("cli ");
    goto newlocale;
}

int64_t nanosleep = 0x21c0;

void fun_2683() {
    __asm__("cli ");
    goto nanosleep;
}

int64_t __overflow = 0x21d0;

void fun_2693() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21e0;

void fun_26a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21f0;

void fun_26b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2200;

void fun_26c3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2210;

void fun_26d3() {
    __asm__("cli ");
    goto memset;
}

int64_t __poll_chk = 0x2220;

void fun_26e3() {
    __asm__("cli ");
    goto __poll_chk;
}

int64_t close = 0x2230;

void fun_26f3() {
    __asm__("cli ");
    goto close;
}

int64_t memchr = 0x2240;

void fun_2703() {
    __asm__("cli ");
    goto memchr;
}

int64_t read = 0x2250;

void fun_2713() {
    __asm__("cli ");
    goto read;
}

int64_t lstat = 0x2260;

void fun_2723() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x2270;

void fun_2733() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2280;

void fun_2743() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x2290;

void fun_2753() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x22a0;

void fun_2763() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x22b0;

void fun_2773() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x22c0;

void fun_2783() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x22d0;

void fun_2793() {
    __asm__("cli ");
    goto stat;
}

int64_t strtol = 0x22e0;

void fun_27a3() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x22f0;

void fun_27b3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t kill = 0x2300;

void fun_27c3() {
    __asm__("cli ");
    goto kill;
}

int64_t inotify_init = 0x2310;

void fun_27d3() {
    __asm__("cli ");
    goto inotify_init;
}

int64_t fileno = 0x2320;

void fun_27e3() {
    __asm__("cli ");
    goto fileno;
}

int64_t pause = 0x2330;

void fun_27f3() {
    __asm__("cli ");
    goto pause;
}

int64_t malloc = 0x2340;

void fun_2803() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2350;

void fun_2813() {
    __asm__("cli ");
    goto fflush;
}

int64_t inotify_add_watch = 0x2360;

void fun_2823() {
    __asm__("cli ");
    goto inotify_add_watch;
}

int64_t nl_langinfo = 0x2370;

void fun_2833() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2380;

void fun_2843() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2390;

void fun_2853() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x23a0;

void fun_2863() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x23b0;

void fun_2873() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x23c0;

void fun_2883() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t poll = 0x23d0;

void fun_2893() {
    __asm__("cli ");
    goto poll;
}

int64_t error = 0x23e0;

void fun_28a3() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x23f0;

void fun_28b3() {
    __asm__("cli ");
    goto memrchr;
}

int64_t open = 0x2400;

void fun_28c3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2410;

void fun_28d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2420;

void fun_28e3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t fstatfs = 0x2430;

void fun_28f3() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x2440;

void fun_2903() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2450;

void fun_2913() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2460;

void fun_2923() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2470;

void fun_2933() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t fflush_unlocked = 0x2480;

void fun_2943() {
    __asm__("cli ");
    goto fflush_unlocked;
}

int64_t mbsinit = 0x2490;

void fun_2953() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x24a0;

void fun_2963() {
    __asm__("cli ");
    goto iswprint;
}

int64_t inotify_rm_watch = 0x24b0;

void fun_2973() {
    __asm__("cli ");
    goto inotify_rm_watch;
}

int64_t fstat = 0x24c0;

void fun_2983() {
    __asm__("cli ");
    goto fstat;
}

int64_t __ctype_b_loc = 0x24d0;

void fun_2993() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2870(int64_t rdi, ...);

void fun_25f0(int64_t rdi);

void fun_25c0(int64_t rdi);

void atexit();

signed char have_read_stdin = 0;

void** count_lines = reinterpret_cast<void**>(0);

unsigned char from_start = 0;

unsigned char forever = 0;

int32_t fun_2770(void** rdi, void** rsi, ...);

int32_t posix2_version();

uint32_t xstrtoumax();

int64_t fun_2640(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

int64_t Version = 0xe4b0;

void version_etc(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10);

int32_t usage();

void** optarg = reinterpret_cast<void**>(0);

void** xdectoumax(void** rdi);

void** pid = reinterpret_cast<void**>(0);

unsigned char monitor_output = 0;

int32_t fun_2890(void* rdi);

void** quote(void** rdi, ...);

void fun_2500(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t rpl_fcntl(int64_t rdi, int64_t rsi, ...);

void** max_n_unchanged_stats_between_opens = reinterpret_cast<void**>(5);

unsigned char presume_input_pipe = 0;

int32_t fun_27c0();

int32_t optind = 0;

void** xnmalloc(void** rdi, int64_t rsi, void** rdx);

int32_t fun_2570();

void** xmalloc(void** rdi, void** rsi, ...);

signed char* fun_27b0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s8 {
    signed char[1] pad1;
    void** f1;
};

struct s8* fun_2750();

int32_t xnanosleep(void* rdi);

void** fun_27d0(void** rdi);

void** hash_initialize(void** rdi);

void** fun_2620(void** rdi, ...);

void** dir_len(void** rdi, void** rsi);

void* last_component(void** rdi, void** rsi);

void** fun_2820(int64_t rdi, void** rsi, ...);

int64_t hash_insert(void** rdi);

void hash_free(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_2790(void** rdi, void** rsi);

int64_t hash_get_n_entries(void** rdi, void** rsi);

struct s9 {
    void** f0;
    signed char[3] pad4;
    void** f4;
};

struct s10 {
    signed char[1] pad1;
    void** f1;
};

int32_t fun_26e0(void** rdi, void** rsi);

struct s11 {
    void** f0;
    signed char[3] pad4;
    int32_t f4;
};

void** xrealloc(void** rdi, void** rsi);

void** hash_lookup(void** rdi, void** rsi);

void fun_2970(int64_t rdi, ...);

void** hash_remove(void** rdi, void** rsi);

void xalloc_die();

int64_t fun_2a13(void* edi, void** rsi) {
    void** r15_3;
    void** rbp4;
    void* ebx5;
    void** rdi6;
    void** rax7;
    void** v8;
    void** v9;
    void** rsi10;
    void** rdi11;
    void* rsp12;
    int32_t eax13;
    int32_t eax14;
    uint32_t ecx15;
    int32_t r12d16;
    void* rax17;
    int32_t eax18;
    void** rdx19;
    int32_t ecx20;
    int32_t eax21;
    void** r13_22;
    int32_t ecx23;
    void** rax24;
    void** r14_25;
    int64_t rax26;
    void** rax27;
    uint32_t eax28;
    void** v29;
    void** r12_30;
    void** rbx31;
    void** r8_32;
    int64_t rdi33;
    int64_t rax34;
    void* rsp35;
    struct s0* rdi36;
    int64_t rcx37;
    void** r9_38;
    void** rdi39;
    void** rax40;
    int1_t zf41;
    int1_t zf42;
    int1_t zf43;
    void** edi44;
    int32_t eax45;
    void** v46;
    void** rax47;
    void** rcx48;
    void** rdi49;
    void** rdx50;
    void* rsp51;
    int64_t rdx52;
    uint32_t edx53;
    int1_t zf54;
    void** rcx55;
    void** v56;
    void** rax57;
    void** v58;
    void** rsi59;
    void** v60;
    int1_t zf61;
    void** rdx62;
    int32_t eax63;
    struct s0* rdi64;
    int32_t eax65;
    void** rdi66;
    void** rax67;
    void** rax68;
    void** rax69;
    void** rdi70;
    void** rax71;
    void** eax72;
    void** r12d73;
    int64_t rdi74;
    int32_t eax75;
    int64_t rdi76;
    int32_t eax77;
    void** rax78;
    void** eax79;
    void** rax80;
    void** rax81;
    void** v82;
    void** v83;
    void** v84;
    void** v85;
    int32_t edx86;
    void** v87;
    void** v88;
    int64_t rdx89;
    int1_t cf90;
    int1_t zf91;
    uint32_t r12d92;
    void** v93;
    void** v94;
    void** v95;
    void** rax96;
    void** v97;
    int64_t rdi98;
    void** rax99;
    void** v100;
    int1_t zf101;
    int1_t zf102;
    void** rdx103;
    void** v104;
    void** v105;
    int64_t v106;
    int64_t v107;
    void** v108;
    void** v109;
    void** v110;
    void** edi111;
    void* v112;
    int1_t zf113;
    int1_t zf114;
    int32_t eax115;
    void* rsp116;
    void** rax117;
    void** rcx118;
    void** eax119;
    void** eax120;
    void** eax121;
    int1_t zf122;
    int32_t eax123;
    void* rsp124;
    void** rax125;
    int1_t zf126;
    void** rdx127;
    void** v128;
    int64_t v129;
    int1_t zf130;
    void** rax131;
    uint32_t eax132;
    void** v133;
    void** rax134;
    void** rsi135;
    void** eax136;
    uint32_t eax137;
    void** eax138;
    void* rsp139;
    uint32_t eax140;
    uint32_t v141;
    int1_t zf142;
    uint32_t v143;
    int64_t rdi144;
    void** rax145;
    int64_t rdi146;
    void** rax147;
    uint32_t v148;
    int64_t rdi149;
    void** rax150;
    void** rax151;
    void** v152;
    int64_t rdi153;
    void** rax154;
    int64_t rdi155;
    void** rax156;
    int64_t rdi157;
    void** rax158;
    void** edi159;
    int1_t zf160;
    int32_t eax161;
    void** rax162;
    int1_t zf163;
    int64_t rax164;
    void** rcx165;
    void** rax166;
    void** rdx167;
    int1_t zf168;
    void** rax169;
    int1_t zf170;
    int32_t eax171;
    int1_t zf172;
    void** rax173;
    int1_t zf174;
    void** eax175;
    uint32_t v176;
    int32_t eax177;
    void** rax178;
    void** v179;
    int64_t rdi180;
    uint32_t v181;
    void** rax182;
    void** rax183;
    int1_t zf184;
    void** eax185;
    void* rsp186;
    void** rax187;
    void** rbp188;
    int32_t eax189;
    void* rsp190;
    void** rax191;
    void** rdx192;
    void** v193;
    uint32_t eax194;
    void** v195;
    void** v196;
    int64_t v197;
    int32_t eax198;
    void* rsp199;
    void** rax200;
    int64_t rdi201;
    signed char al202;
    uint32_t eax203;
    void** rbx204;
    uint32_t eax205;
    void** rax206;
    void** rdi207;
    int32_t eax208;
    void* rsp209;
    uint32_t eax210;
    uint32_t eax211;
    int32_t eax212;
    void* rsp213;
    void** rax214;
    int64_t rdi215;
    int32_t eax216;
    int32_t eax217;
    void* rsp218;
    void** rax219;
    void** rax220;
    void** rax221;
    void** rax222;
    void* rsp223;
    void** r12_224;
    void** rbp225;
    void** rdi226;
    void** rax227;
    void** rax228;
    void** rax229;
    void** rax230;
    void** rdx231;
    void** rax232;
    void** rcx233;
    void* rax234;
    void** rax235;
    void** rax236;
    void** rsi237;
    void** rdi238;
    void** rax239;
    void** rax240;
    void* rsp241;
    void** r13_242;
    void** r14_243;
    void** rsi244;
    void** rdi245;
    void** rax246;
    void** r12_247;
    int32_t ebx248;
    void** rsi249;
    struct s2* rax250;
    void** rdi251;
    void** rax252;
    void** rax253;
    void** rax254;
    void** rax255;
    void** rax256;
    unsigned char dl257;
    void** rbx258;
    void** rax259;
    void* r12_260;
    void* rbx261;
    struct s8* rax262;
    uint32_t eax263;
    uint32_t eax264;
    void** rdi265;
    void** rax266;
    uint32_t eax267;
    void** rax268;
    void** rax269;
    void** rax270;
    uint32_t eax271;
    void** rax272;
    uint32_t eax273;
    unsigned char al274;
    void** rax275;
    int1_t zf276;
    void** eax277;
    int32_t eax278;
    uint32_t eax279;
    void** r12_280;
    void** rdi281;
    int32_t eax282;
    void*** rdi283;
    int32_t eax284;
    void** rax285;
    int32_t eax286;
    uint32_t eax287;
    void* rax288;
    void** eax289;
    uint32_t v290;
    int1_t zf291;
    void** r12_292;
    void** rdi293;
    int32_t eax294;
    void** rdx295;
    void** rcx296;
    void** rax297;
    void** rdx298;
    void** rax299;
    uint32_t ecx300;
    void** rdi301;
    int32_t eax302;
    uint32_t v303;
    void** rcx304;
    void** rax305;
    uint32_t edx306;
    int1_t zf307;
    void** eax308;
    void* rsp309;
    void** rax310;
    struct s0* rdi311;
    int32_t eax312;
    void* rsp313;
    void** rax314;
    void** r12_315;
    void** rax316;
    void** rsi317;
    void** rsi318;
    void** rax319;
    void** eax320;
    int1_t zf321;
    int1_t zf322;
    void** eax323;
    void** rdi324;
    void** rax325;
    void** rax326;
    void** rax327;
    void** rax328;
    int32_t r12d329;
    void** rax330;
    void* rsp331;
    void** rax332;
    void** rdx333;
    void* rax334;
    void** rsi335;
    int64_t rdi336;
    void** eax337;
    int64_t rdi338;
    void** eax339;
    void* rsp340;
    void** eax341;
    void** rax342;
    void** rsi343;
    void** rax344;
    void** rax345;
    int64_t rax346;
    uint32_t eax347;
    int1_t zf348;
    void** rax349;
    void** rbx350;
    int1_t zf351;
    void** rdi352;
    int32_t eax353;
    void** rax354;
    int1_t zf355;
    int1_t zf356;
    int64_t rax357;
    struct s9* rdi358;
    int32_t eax359;
    void** rax360;
    struct s10* rax361;
    int32_t eax362;
    struct s11* rdi363;
    void* rdx364;
    void* rax365;
    void** rax366;
    void** rdi367;
    void** rax368;
    void** rax369;
    void** rax370;
    void** rax371;
    void** rdi372;
    void** rbx373;
    void** r12_374;
    void** r14d375;
    void** r13d376;
    void** rax377;
    int32_t eax378;
    void** rsi379;
    int64_t rdi380;
    void** eax381;
    void** rax382;
    void** rsi383;
    void** rax384;
    int1_t zf385;
    int64_t rdi386;
    void** rsi387;
    void** rax388;
    void* rsp389;
    int1_t zf390;
    void** rdi391;
    void** rax392;
    int64_t rdi393;
    int64_t rax394;
    void** eax395;
    int64_t rdi396;
    void** rax397;
    void** rsi398;
    void** rax399;
    void** rax400;
    void** rax401;
    uint32_t eax402;
    int32_t eax403;

    __asm__("cli ");
    r15_3 = reinterpret_cast<void**>(0xea41);
    rbp4 = rsi;
    ebx5 = edi;
    rdi6 = *reinterpret_cast<void***>(rsi);
    rax7 = g28;
    v8 = rax7;
    v9 = reinterpret_cast<void**>(10);
    set_program_name(rdi6);
    fun_2870(6, 6);
    rsi10 = reinterpret_cast<void**>("/usr/local/share/locale");
    fun_25f0("coreutils");
    fun_25c0("coreutils");
    rdi11 = reinterpret_cast<void**>(0x78e0);
    atexit();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x228 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    have_read_stdin = 0;
    count_lines = reinterpret_cast<void**>(1);
    print_headers = 0;
    from_start = 0;
    forever = 0;
    line_end = 10;
    if (ebx5 != 2) {
        if (ebx5 == 3) {
            rdi11 = *reinterpret_cast<void***>(rbp4 + 16);
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi11) == 45)) {
                if (*reinterpret_cast<void***>(rdi11 + 1)) {
                    addr_2ad0_5:
                    rsi10 = reinterpret_cast<void**>("--");
                    eax13 = fun_2770(rdi11, "--");
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                    if (eax13) 
                        goto addr_2b02_6;
                }
            }
        } else {
            if (!reinterpret_cast<int1_t>(ebx5 == 4)) 
                goto addr_2b02_6;
            rdi11 = *reinterpret_cast<void***>(rbp4 + 16);
            goto addr_2ad0_5;
        }
    }
    eax14 = posix2_version();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    ecx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 8)));
    rdi11 = *reinterpret_cast<void***>(rbp4 + 8) + 1;
    if (*reinterpret_cast<signed char*>(&ecx15) == 43) {
        r12d16 = 1;
        if (eax14 - 0x30db0 <= 0x2b8) {
            goto addr_2b02_6;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&ecx15) != 45) 
            goto addr_2b02_6;
        if (eax14 <= 0x30daf) 
            goto addr_2e4b_15;
        *reinterpret_cast<int32_t*>(&rax17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rax17) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 8) + 1) == 99);
        if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 8)) + reinterpret_cast<uint64_t>(rax17) + 1)) 
            goto addr_2b02_6;
        addr_2e4b_15:
        r12d16 = 0;
    }
    eax18 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp4 + 8) + 1));
    rdx19 = rdi11;
    ecx20 = eax18;
    if (eax18 - 48 <= 9) {
        do {
            eax21 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx19 + 1));
            ++rdx19;
            ecx20 = eax21;
        } while (eax21 - 48 <= 9);
    }
    if (*reinterpret_cast<signed char*>(&ecx20) == 99) {
        r13_22 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
        ecx23 = 10;
    } else {
        if (*reinterpret_cast<signed char*>(&ecx20) == 0x6c) {
            r13_22 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
            ecx23 = 10;
        } else {
            if (*reinterpret_cast<signed char*>(&ecx20) == 98) {
                r13_22 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
                ecx23 = 0x1400;
            } else {
                rax24 = rdx19;
                r13_22 = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
                ecx23 = 10;
                goto addr_2e9b_27;
            }
        }
    }
    rax24 = rdx19 + 1;
    addr_2e9b_27:
    *reinterpret_cast<int32_t*>(&r14_25) = 0;
    *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax24) == 0x66)) {
        ++rax24;
        *reinterpret_cast<int32_t*>(&r14_25) = 1;
        *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    }
    if (*reinterpret_cast<void***>(rax24)) {
        addr_2b02_6:
        *reinterpret_cast<int32_t*>(&rax26) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    } else {
        if (rdi11 == rdx19) {
            *reinterpret_cast<int32_t*>(&rax27) = ecx23;
            *reinterpret_cast<int32_t*>(&rax27 + 4) = 0;
            v9 = rax27;
        } else {
            *reinterpret_cast<int32_t*>(&rsi10) = 0;
            *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
            eax28 = xstrtoumax();
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            if (eax28 & 0xfffffffd) 
                goto addr_4b97_34;
        }
        from_start = *reinterpret_cast<unsigned char*>(&r12d16);
        *reinterpret_cast<int32_t*>(&rax26) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
        count_lines = r13_22;
        forever = *reinterpret_cast<unsigned char*>(&r14_25);
    }
    *reinterpret_cast<void***>(rdi11) = *reinterpret_cast<void***>(rsi10);
    v29 = reinterpret_cast<void**>(0);
    r12_30 = rbp4 + rax26 * 8;
    rbx31 = reinterpret_cast<void**>(reinterpret_cast<int32_t>(ebx5) - *reinterpret_cast<unsigned char*>(&rax26));
    r14_25 = reinterpret_cast<void**>(0x12900);
    r13_22 = reinterpret_cast<void**>("c:n:fFqs:vz0123456789");
    *reinterpret_cast<void***>(rdi11 + 4) = *reinterpret_cast<void***>(rsi10 + 4);
    rbp4 = reinterpret_cast<void**>(0xe330);
    while (*reinterpret_cast<int32_t*>(&r8_32) = 0, *reinterpret_cast<int32_t*>(&r8_32 + 4) = 0, *reinterpret_cast<void***>(&rdi33) = rbx31, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0, rax34 = fun_2640(rdi33, r12_30, "c:n:fFqs:vz0123456789", 0x12900), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8), *reinterpret_cast<int32_t*>(&rax34) != -1) {
        if (*reinterpret_cast<int32_t*>(&rax34) > 0x85) 
            goto addr_4124_45;
        if (*reinterpret_cast<int32_t*>(&rax34) > 47) 
            goto addr_2b70_47;
        if (*reinterpret_cast<int32_t*>(&rax34) == 0xffffff7d) {
            rdi36 = stdout;
            rcx37 = Version;
            r9_38 = reinterpret_cast<void**>("David MacKenzie");
            r8_32 = reinterpret_cast<void**>("Paul Rubin");
            version_etc(rdi36, "tail", "GNU coreutils", rcx37, "Paul Rubin", "David MacKenzie", "Ian Lance Taylor", "Jim Meyering", 0, 0x2b53);
            *reinterpret_cast<int32_t*>(&rax34) = fun_2910();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
        }
        if (*reinterpret_cast<int32_t*>(&rax34) != 0xffffff7e) 
            goto addr_4124_45;
        usage();
        fun_2600();
        rdi39 = optarg;
        *reinterpret_cast<int32_t*>(&r9_38) = 0;
        *reinterpret_cast<int32_t*>(&r9_38 + 4) = 0;
        rax40 = xdectoumax(rdi39);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8);
        v9 = rax40;
    }
    zf41 = reopen_inaccessible_files == 0;
    if (zf41) 
        goto addr_3452_55;
    zf42 = forever == 0;
    if (zf42) {
        reopen_inaccessible_files = 0;
        goto addr_343d_58;
    } else {
        zf43 = follow_mode == 2;
        if (zf43) 
            goto addr_47ad_60;
        edi44 = pid;
        if (!edi44) 
            goto addr_2f85_62; else 
            goto addr_2f76_63;
    }
    addr_4124_45:
    eax45 = usage();
    *reinterpret_cast<int32_t*>(&v46) = eax45;
    rax47 = fun_2600();
    *reinterpret_cast<int32_t*>(&rcx48) = *reinterpret_cast<int32_t*>(&v46);
    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi49) = 1;
    *reinterpret_cast<int32_t*>(&rdi49 + 4) = 0;
    rdx50 = rax47;
    fun_28a0();
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_415a_64;
    addr_2b70_47:
    *reinterpret_cast<uint32_t*>(&rdx52) = static_cast<uint32_t>(rax34 - 48);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rdx52) <= 85) {
        goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xe330) + reinterpret_cast<uint64_t>(rdx52 * 4)))) + reinterpret_cast<unsigned char>(0xe330);
    }
    addr_3d10_66:
    return 0;
    while (1) {
        addr_4016_67:
        edx53 = reopen_inaccessible_files;
        if (!*reinterpret_cast<signed char*>(&edx53) || (zf54 = follow_mode == 1, !zf54)) {
            rcx55 = v56;
            rax57 = v58 + 52;
            do {
                rsi59 = *reinterpret_cast<void***>(rax57 + 4);
                *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                if (reinterpret_cast<signed char>(rsi59) >= reinterpret_cast<signed char>(0)) 
                    goto addr_441d_70;
                if (*reinterpret_cast<void***>(rax57) == 1) 
                    continue;
                if (*reinterpret_cast<signed char*>(&edx53)) 
                    goto addr_441d_70;
                rax57 = rax57 + 96;
            } while (rax57 != rcx55);
        } else {
            addr_441d_70:
            if (*reinterpret_cast<unsigned char*>(&v29) == 1 && !*reinterpret_cast<signed char*>(&v60)) {
                zf61 = monitor_output == 0;
                if (zf61) 
                    goto addr_3f20_75;
                rdx62 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                rsi59 = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                eax63 = fun_2890(reinterpret_cast<int64_t>(rsp51) + 0x90);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                if (eax63 < 0) 
                    goto addr_3f20_75; else 
                    goto addr_4468_77;
            }
        }
        fun_2600();
        fun_28a0();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
        goto addr_3a0f_79;
        rdi64 = stdout;
        eax65 = fun_2940(rdi64, rsi59);
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
        if (eax65) {
            while (1) {
                fun_2600();
                fun_2530();
                fun_28a0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_4b42_82:
                fun_2530();
                fun_28a0();
                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                addr_4b5c_83:
                quotearg_style(4, "-", 4, "-");
                fun_2600();
                fun_28a0();
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_4b97_34:
                rdi66 = *reinterpret_cast<void***>(rbp4 + 8);
                rax67 = quote(rdi66);
                rbx31 = rax67;
                rax68 = fun_2600();
                fun_2530();
                r8_32 = rbx31;
                rcx55 = rax68;
                fun_28a0();
                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                addr_4bd9_84:
                fun_2630();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                while (1) {
                    addr_4bde_85:
                    rdx62 = r14_25;
                    rsi59 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                    xlseek_part_0(0, 0, rdx62, rcx55, r8_32, r9_38);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    addr_4bea_86:
                    fun_2500(13, rsi59, rdx62, rcx55, r8_32, r9_38);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    addr_4bf4_87:
                    fun_2910();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    while (1) {
                        rcx55 = reinterpret_cast<void**>(0xfffffffffffffffe);
                        while (1) {
                            addr_3fbf_89:
                            rsi59 = r14_25;
                            rax69 = dump_remainder(0, rsi59, r13_22, rcx55, r8_32, r9_38);
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                            *reinterpret_cast<void***>(r15_3 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_3 + 8)) + reinterpret_cast<unsigned char>(rax69));
                            *reinterpret_cast<unsigned char*>(&v29) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&v29) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax69)));
                            while (1) {
                                addr_3fda_90:
                                ++rbx31;
                                r15_3 = r15_3 + 96;
                                if (v46 == rbx31) 
                                    goto addr_4016_67;
                                do {
                                    addr_3fe9_91:
                                    if (*reinterpret_cast<void***>(r15_3 + 52)) 
                                        goto addr_3fda_90;
                                    r13_22 = *reinterpret_cast<void***>(r15_3 + 56);
                                    *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
                                    if (reinterpret_cast<signed char>(r13_22) >= reinterpret_cast<signed char>(0)) 
                                        break;
                                    rdi70 = r15_3;
                                    rsi59 = rbp4;
                                    *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                                    ++rbx31;
                                    r15_3 = r15_3 + 96;
                                    recheck(rdi70, rsi59, rdi70, rsi59);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                } while (v46 != rbx31);
                                goto addr_4016_67;
                                r14_25 = *reinterpret_cast<void***>(r15_3);
                                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_25) == 45) && !*reinterpret_cast<void***>(r14_25 + 1)) {
                                    rax71 = fun_2600();
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    r14_25 = rax71;
                                }
                                eax72 = *reinterpret_cast<void***>(r15_3 + 64);
                                r12d73 = *reinterpret_cast<void***>(r15_3 + 48);
                                if (rbp4 == eax72) {
                                    addr_3fb0_97:
                                    rcx55 = reinterpret_cast<void**>(0xfffffffffffffffe);
                                    if (eax72) 
                                        goto addr_3fbf_89;
                                } else {
                                    *reinterpret_cast<void***>(&rdi74) = r13_22;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi74) + 4) = 0;
                                    eax75 = rpl_fcntl(rdi74, 3, rdi74, 3);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    if (!rbp4) {
                                        if (eax75 < 0) 
                                            goto addr_42e8_100;
                                        if (0) 
                                            goto addr_3faa_102;
                                        *reinterpret_cast<void***>(&rdi76) = r13_22;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi76) + 4) = 0;
                                        eax77 = rpl_fcntl(rdi76, 4);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        if (eax77 == -1) 
                                            goto addr_42e8_100; else 
                                            goto addr_3faa_102;
                                    }
                                    if (eax75 >= 0) {
                                        addr_3faa_102:
                                        *reinterpret_cast<void***>(r15_3 + 64) = rbp4;
                                        eax72 = rbp4;
                                        goto addr_3fb0_97;
                                    } else {
                                        addr_42e8_100:
                                        rax78 = fun_2530();
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_3 + 48)) & 0xf000) != 0x8000) 
                                            goto addr_4aa6_105;
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax78) == 1)) 
                                            goto addr_4aa6_105; else 
                                            goto addr_430c_107;
                                    }
                                }
                                eax79 = fun_2980();
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                if (eax79) {
                                    addr_44d9_109:
                                    *reinterpret_cast<void***>(r15_3 + 56) = reinterpret_cast<void**>(0xffffffff);
                                    rax80 = fun_2530();
                                    *reinterpret_cast<void***>(r15_3 + 60) = *reinterpret_cast<void***>(rax80);
                                    rax81 = quotearg_n_style_colon();
                                    rsi59 = *reinterpret_cast<void***>(rax80);
                                    *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                                    rcx55 = rax81;
                                    fun_28a0();
                                    fun_26f0();
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    continue;
                                }
                                if (*reinterpret_cast<void***>(r15_3 + 48) != v82) 
                                    break;
                                if ((reinterpret_cast<unsigned char>(v82) & 0xf000) != 0x8000) 
                                    goto addr_4329_112;
                                if (*reinterpret_cast<void***>(r15_3 + 8) != v83) 
                                    break;
                                addr_4329_112:
                                rcx55 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v84) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_3 + 16)))) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v85) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_3 + 16)))));
                                *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                                edx86 = 0;
                                *reinterpret_cast<unsigned char*>(&edx86) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v87) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_3 + 24)));
                                rsi59 = reinterpret_cast<void**>(static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v88) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_3 + 24))))));
                                *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx89) = edx86 - reinterpret_cast<unsigned char>(rsi59);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx89) + 4) = 0;
                                if (static_cast<int32_t>(rdx89 + reinterpret_cast<unsigned char>(rcx55) * 2)) 
                                    break;
                                cf90 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_3 + 88)) < reinterpret_cast<unsigned char>(max_n_unchanged_stats_between_opens);
                                *reinterpret_cast<void***>(r15_3 + 88) = *reinterpret_cast<void***>(r15_3 + 88) + 1;
                                if (cf90) 
                                    continue;
                                zf91 = follow_mode == 1;
                                if (!zf91) 
                                    continue;
                                rsi59 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rsi59) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(r15_3 + 64));
                                recheck(r15_3, rsi59, r15_3, rsi59);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                *reinterpret_cast<void***>(r15_3 + 88) = reinterpret_cast<void**>(0);
                                continue;
                                addr_430c_107:
                                eax72 = *reinterpret_cast<void***>(r15_3 + 64);
                                goto addr_3fb0_97;
                            }
                            r12d92 = reinterpret_cast<unsigned char>(r12d73) & 0xf000;
                            *reinterpret_cast<void***>(r15_3 + 48) = v82;
                            *reinterpret_cast<void***>(r15_3 + 88) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r15_3 + 16) = v93;
                            *reinterpret_cast<void***>(r15_3 + 24) = v94;
                            if (r12d92 == 0x8000 && reinterpret_cast<signed char>(v95) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_3 + 8))) {
                                rax96 = quotearg_n_style_colon();
                                v97 = rax96;
                                fun_2600();
                                rcx55 = v97;
                                fun_28a0();
                                *reinterpret_cast<void***>(&rdi98) = r13_22;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi98) + 4) = 0;
                                rax99 = fun_26b0(rdi98);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                if (reinterpret_cast<signed char>(rax99) < reinterpret_cast<signed char>(0)) 
                                    goto addr_4bde_85;
                                *reinterpret_cast<void***>(r15_3 + 8) = reinterpret_cast<void**>(0);
                            }
                            if (rbx31 != v100 && (zf101 = print_headers == 0, !zf101)) {
                                zf102 = first_file_2 == 0;
                                rdx103 = reinterpret_cast<void**>("\n");
                                if (!zf102) {
                                    rdx103 = reinterpret_cast<void**>(0xea41);
                                }
                                fun_2880(1, "%s==> %s <==\n", rdx103, r14_25, r8_32, r9_38, v104, v46, v29, v100, v56, v60, v97, v105, v106, v107, v108, v58, v109, v110);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                first_file_2 = 0;
                            }
                            edi111 = *reinterpret_cast<void***>(r15_3 + 64);
                            v100 = rbx31;
                            if (edi111) 
                                break;
                            rcx55 = reinterpret_cast<void**>(0xffffffffffffffff);
                            if (r12d92 != 0x8000) 
                                continue;
                            if (!*reinterpret_cast<signed char*>(r15_3 + 53)) 
                                continue;
                            rcx55 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v112) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_3 + 8)));
                        }
                    }
                }
                addr_4aa6_105:
                v46 = rax78;
                quotearg_n_style_colon();
                fun_2600();
                fun_28a0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_4aea_129:
                fun_2600();
                fun_2530();
                fun_28a0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
            }
        }
        zf113 = monitor_output == 0;
        if (!zf113) 
            goto addr_457e_131;
        addr_4476_132:
        if (*reinterpret_cast<unsigned char*>(&v29)) {
            addr_3f20_75:
            *reinterpret_cast<unsigned char*>(&v29) = 0;
            r15_3 = v58;
            rbx31 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rbx31 + 4) = 0;
            goto addr_3fe9_91;
        } else {
            if (*reinterpret_cast<signed char*>(&v107)) {
                do {
                    addr_3a0f_79:
                    zf114 = have_read_stdin == 0;
                    if (zf114) 
                        goto addr_3a27_134;
                    eax115 = fun_26f0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    if (eax115 < 0) 
                        goto addr_4b42_82;
                    addr_3a27_134:
                    fun_2910();
                    rsp116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    while (1) {
                        rax117 = fun_2600();
                        rsp116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp116) - 8 + 8);
                        rbp4 = rax117;
                        do {
                            quotearg_style(4, rbp4, 4, rbp4);
                            fun_2600();
                            fun_28a0();
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp116) - 8 + 8 - 8 + 8 - 8 + 8);
                            *reinterpret_cast<unsigned char*>(&v56) = 0;
                            while (v29 = v29 + 96, *reinterpret_cast<uint32_t*>(&rcx118) = *reinterpret_cast<unsigned char*>(&v56), *reinterpret_cast<int32_t*>(&rcx118 + 4) = 0, *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v107) + 7) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v107) + 7) & *reinterpret_cast<unsigned char*>(&rcx118)), v108 != v29) {
                                while (1) {
                                    v97 = v9;
                                    rbp4 = *reinterpret_cast<void***>(v29);
                                    r12_30 = rbp4;
                                    eax119 = *reinterpret_cast<void***>(rbp4) - 45;
                                    *reinterpret_cast<void***>(&v107) = eax119;
                                    if (eax119 || (eax120 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 1)))), *reinterpret_cast<void***>(&v107) = eax120, !!eax120)) {
                                        eax121 = open_safer(rbp4);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        r13_22 = reinterpret_cast<void**>(static_cast<uint32_t>(reopen_inaccessible_files));
                                        *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
                                        r15_3 = eax121;
                                        *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
                                        if (!r13_22) {
                                            rbp4 = *reinterpret_cast<void***>(v29);
                                            *reinterpret_cast<unsigned char*>(v29 + 54) = 1;
                                            r12_30 = rbp4;
                                            if (r15_3 == 0xffffffff) 
                                                goto addr_313c_142;
                                        } else {
                                            rbp4 = *reinterpret_cast<void***>(v29);
                                            r12_30 = rbp4;
                                            if (!reinterpret_cast<int1_t>(r15_3 == 0xffffffff)) 
                                                goto addr_3228_145; else 
                                                goto addr_3133_146;
                                        }
                                    } else {
                                        have_read_stdin = 1;
                                        r15_3 = reinterpret_cast<void**>(0);
                                        *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
                                        goto addr_3228_145;
                                    }
                                    addr_3231_148:
                                    zf122 = print_headers == 0;
                                    if (!zf122) {
                                        eax123 = fun_2770(rbp4, "-", rbp4, "-");
                                        rsp124 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        if (!eax123) {
                                            rax125 = fun_2600();
                                            rsp124 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp124) - 8 + 8);
                                            r12_30 = rax125;
                                        }
                                        zf126 = first_file_2 == 0;
                                        rcx118 = r12_30;
                                        rdx127 = reinterpret_cast<void**>("\n");
                                        if (!zf126) {
                                            rdx127 = reinterpret_cast<void**>(0xea41);
                                        }
                                        fun_2880(1, "%s==> %s <==\n", rdx127, rcx118, r8_32, r9_38, v128, v46, v29, v100, v56, v60, v97, v105, v129, v107, v108, v58, v109, v110);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp124) - 8 + 8);
                                        first_file_2 = 0;
                                        rbp4 = *reinterpret_cast<void***>(v29);
                                    }
                                    zf130 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4) == 45);
                                    v105 = rbp4;
                                    if (zf130 && !*reinterpret_cast<void***>(rbp4 + 1)) {
                                        rax131 = fun_2600();
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        v105 = rax131;
                                    }
                                    eax132 = reinterpret_cast<unsigned char>(count_lines);
                                    v133 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<unsigned char*>(&v56) = *reinterpret_cast<unsigned char*>(&eax132);
                                    rax134 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x180);
                                    v109 = rax134;
                                    rsi135 = rax134;
                                    if (!*reinterpret_cast<unsigned char*>(&eax132)) {
                                        eax136 = fun_2980();
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        if (eax136) 
                                            break;
                                        rbx31 = reinterpret_cast<void**>(static_cast<uint32_t>(from_start));
                                        *reinterpret_cast<int32_t*>(&rbx31 + 4) = 0;
                                        eax137 = presume_input_pipe;
                                        *reinterpret_cast<unsigned char*>(&v56) = *reinterpret_cast<unsigned char*>(&rbx31);
                                        if (*reinterpret_cast<unsigned char*>(&rbx31)) 
                                            goto addr_3543_159;
                                    } else {
                                        eax138 = fun_2980();
                                        rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        if (eax138) 
                                            goto addr_3acf_161;
                                        eax140 = from_start;
                                        *reinterpret_cast<unsigned char*>(&v141) = *reinterpret_cast<unsigned char*>(&eax140);
                                        if (*reinterpret_cast<unsigned char*>(&eax140)) 
                                            goto addr_3a53_163;
                                        zf142 = presume_input_pipe == 0;
                                        if (!zf142) 
                                            goto addr_32d9_165;
                                        if ((v143 & 0xf000) != 0x8000) 
                                            goto addr_32d9_165;
                                        *reinterpret_cast<int32_t*>(&rsi135) = 0;
                                        *reinterpret_cast<int32_t*>(&rsi135 + 4) = 0;
                                        *reinterpret_cast<void***>(&rdi144) = r15_3;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi144) + 4) = 0;
                                        rax145 = fun_26b0(rdi144, rdi144);
                                        rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                                        r12_30 = rax145;
                                        if (rax145 == 0xffffffffffffffff) 
                                            goto addr_32d9_165;
                                        *reinterpret_cast<void***>(&rdi146) = r15_3;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi146) + 4) = 0;
                                        rax147 = fun_26b0(rdi146, rdi146);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                                        r8_32 = rax147;
                                        if (reinterpret_cast<signed char>(r12_30) >= reinterpret_cast<signed char>(rax147)) 
                                            goto addr_4788_169; else 
                                            goto addr_3d5b_170;
                                    }
                                    if (*reinterpret_cast<signed char*>(&eax137)) 
                                        goto addr_391f_172;
                                    if (reinterpret_cast<signed char>(v97) < reinterpret_cast<signed char>(0)) 
                                        goto addr_391f_172;
                                    if ((v148 & 0xd000) == 0x8000) 
                                        goto addr_3b7d_175;
                                    *reinterpret_cast<void***>(&rdi149) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi149) + 4) = 0;
                                    rsi135 = reinterpret_cast<void**>(-reinterpret_cast<unsigned char>(v97));
                                    rax150 = fun_26b0(rdi149, rdi149);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    rbp4 = rax150;
                                    if (rax150 == 0xffffffffffffffff) 
                                        goto addr_391f_172;
                                    rbx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v97) + reinterpret_cast<unsigned char>(rax150));
                                    addr_3b89_178:
                                    rax151 = v152;
                                    rcx118 = rax151 + 0xffffffffffffffff;
                                    if (reinterpret_cast<unsigned char>(rcx118) > reinterpret_cast<unsigned char>(0x1fffffffffffffff)) {
                                        rax151 = reinterpret_cast<void**>(0x200);
                                    }
                                    if (reinterpret_cast<signed char>(rbx31) <= reinterpret_cast<signed char>(rax151)) 
                                        goto addr_391f_172;
                                    if (rbp4 != 0xffffffffffffffff) 
                                        goto addr_3bbe_182;
                                    *reinterpret_cast<void***>(&rdi153) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi153) + 4) = 0;
                                    rax154 = fun_26b0(rdi153, rdi153);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    rbp4 = rax154;
                                    if (reinterpret_cast<signed char>(rax154) < reinterpret_cast<signed char>(0)) 
                                        goto addr_46ca_184;
                                    addr_3bbe_182:
                                    if (reinterpret_cast<signed char>(rbx31) <= reinterpret_cast<signed char>(rbp4)) 
                                        goto addr_3bd4_185;
                                    if (reinterpret_cast<unsigned char>(v97) >= reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(rbp4))) 
                                        goto addr_3bd4_185;
                                    addr_475b_187:
                                    rbx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(v97));
                                    *reinterpret_cast<void***>(&rdi155) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi155) + 4) = 0;
                                    rbp4 = rbx31;
                                    rax156 = fun_26b0(rdi155, rdi155);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    if (reinterpret_cast<signed char>(rax156) >= reinterpret_cast<signed char>(0)) 
                                        goto addr_3bd4_185;
                                    xlseek_part_0(rbx31, 0, v105, rcx118, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    addr_4788_169:
                                    rsi135 = r12_30;
                                    *reinterpret_cast<void***>(&rdi157) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi157) + 4) = 0;
                                    rax158 = fun_26b0(rdi157, rdi157);
                                    rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    if (reinterpret_cast<signed char>(rax158) >= reinterpret_cast<signed char>(0)) 
                                        goto addr_32d9_165;
                                    xlseek_part_0(r12_30, 0, v105, rcx118, r8_32, r9_38);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                                    addr_47ad_60:
                                    addr_343d_58:
                                    fun_2600();
                                    fun_28a0();
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
                                    addr_3452_55:
                                    edi159 = pid;
                                    if (edi159) {
                                        zf160 = forever == 0;
                                        if (!zf160) {
                                            addr_2f76_63:
                                            eax161 = fun_27c0();
                                            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                            if (eax161 && (rax162 = fun_2530(), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax162) == 38))) {
                                                fun_2600();
                                                fun_28a0();
                                                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
                                                pid = reinterpret_cast<void**>(0);
                                            }
                                        } else {
                                            fun_2600();
                                            fun_28a0();
                                            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
                                        }
                                    }
                                    addr_2f85_62:
                                    zf163 = from_start == 0;
                                    if (!zf163 && v9) {
                                        --v9;
                                    }
                                    rax164 = optind;
                                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax164)) >= reinterpret_cast<signed char>(rbx31)) {
                                        v46 = reinterpret_cast<void**>(1);
                                        rbx31 = reinterpret_cast<void**>(0x13028);
                                    } else {
                                        rcx165 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax164)))));
                                        rbx31 = r12_30 + rax164 * 8;
                                        v46 = rcx165;
                                    }
                                    rcx55 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                                    rbp4 = rbx31 + reinterpret_cast<unsigned char>(v46) * 8;
                                    rax166 = rbx31;
                                    do {
                                        rdx167 = *reinterpret_cast<void***>(rax166);
                                        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx167) == 45) && !*reinterpret_cast<void***>(rdx167 + 1)) {
                                            rcx55 = reinterpret_cast<void**>(1);
                                            *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                                        }
                                        rax166 = rax166 + 8;
                                    } while (rbp4 != rax166);
                                    if (!*reinterpret_cast<signed char*>(&rcx55)) {
                                        addr_306b_202:
                                        if (v9 || (zf168 = forever == 0, !zf168)) {
                                            addr_3083_203:
                                            rax169 = xnmalloc(v46, 96, rdx167);
                                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                            v58 = rax169;
                                        } else {
                                            addr_3cec_204:
                                            zf170 = from_start == 0;
                                            if (!zf170) 
                                                goto addr_3083_203; else 
                                                goto addr_3cf9_205;
                                        }
                                    } else {
                                        eax171 = follow_mode;
                                        if (eax171 == 1) 
                                            goto addr_4b5c_83;
                                        zf172 = forever == 0;
                                        if (zf172) 
                                            goto addr_3cdd_208; else 
                                            goto addr_3018_209;
                                    }
                                    do {
                                        rbx31 = rbx31 + 8;
                                        rax169 = rax169 + 96;
                                        *reinterpret_cast<void***>(rax169 + 0xffffffffffffffa0) = *reinterpret_cast<void***>(rbx31);
                                    } while (rbx31 != rbp4);
                                    if (v29 == 1 || !v29 && !reinterpret_cast<int1_t>(v46 == 1)) {
                                        print_headers = 1;
                                    }
                                    rax173 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v46) * 96);
                                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v107) + 7) = 1;
                                    rbx31 = rax173;
                                    v110 = rax173;
                                    v29 = v58;
                                    rcx118 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(rbx31));
                                    v108 = rcx118;
                                    continue;
                                    addr_3cdd_208:
                                    if (v9) 
                                        goto addr_3083_203; else 
                                        goto addr_3cec_204;
                                    addr_3018_209:
                                    zf174 = pid == 0;
                                    if ((!zf174 || (!reinterpret_cast<int1_t>(v46 == 1) || (eax171 != 2 || ((eax175 = fun_2980(), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!eax175) || (v176 & 0xf000) == 0x8000)))) && (eax177 = fun_2570(), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!eax177)) {
                                        rax178 = fun_2600();
                                        rdx167 = rax178;
                                        fun_28a0();
                                        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
                                        goto addr_306b_202;
                                    }
                                    addr_3b7d_175:
                                    rbx31 = v179;
                                    rbp4 = reinterpret_cast<void**>(0xffffffffffffffff);
                                    goto addr_3b89_178;
                                    addr_3543_159:
                                    if (*reinterpret_cast<signed char*>(&eax137)) 
                                        goto addr_364e_215;
                                    if (reinterpret_cast<signed char>(v97) < reinterpret_cast<signed char>(0)) 
                                        goto addr_364e_215;
                                    *reinterpret_cast<void***>(&rdi180) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi180) + 4) = 0;
                                    if ((v181 & 0xf000) != 0x8000) 
                                        goto addr_357b_218;
                                    rax182 = fun_26b0(rdi180, rdi180);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    if (reinterpret_cast<signed char>(rax182) >= reinterpret_cast<signed char>(0)) 
                                        goto addr_358a_220;
                                    xlseek_part_0(v97, 1, v105, rcx118, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    goto addr_475b_187;
                                    addr_3228_145:
                                    *reinterpret_cast<unsigned char*>(v29 + 54) = 1;
                                    goto addr_3231_148;
                                }
                                rax183 = quotearg_style(4, v105, 4, v105);
                                r13_22 = rax183;
                                fun_2600();
                                fun_2530();
                                fun_28a0();
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                addr_35c1_223:
                                zf184 = forever == 0;
                                if (!zf184) {
                                    *reinterpret_cast<void***>(v29 + 60) = reinterpret_cast<void**>(*reinterpret_cast<unsigned char*>(&v56) - 1);
                                    eax185 = fun_2980();
                                    rsp186 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    if (reinterpret_cast<signed char>(eax185) < reinterpret_cast<signed char>(0)) {
                                        rax187 = fun_2530();
                                        rbp188 = *reinterpret_cast<void***>(v29);
                                        *reinterpret_cast<void***>(v29 + 60) = *reinterpret_cast<void***>(rax187);
                                        eax189 = fun_2770(rbp188, "-", rbp188, "-");
                                        rsp190 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp186) - 8 + 8 - 8 + 8);
                                        if (!eax189) {
                                            rax191 = fun_2600();
                                            rsp190 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp190) - 8 + 8);
                                            rbp188 = rax191;
                                        }
                                        quotearg_style(4, rbp188, 4, rbp188);
                                        fun_2600();
                                        fun_28a0();
                                        rsp186 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp190) - 8 + 8 - 8 + 8 - 8 + 8);
                                    } else {
                                        rdx192 = v193;
                                        *reinterpret_cast<int32_t*>(&rdx192 + 4) = 0;
                                        eax194 = (reinterpret_cast<unsigned char>(rdx192) & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable");
                                        if (!(eax194 & 0xffffe000) || (eax194 = reinterpret_cast<unsigned char>(rdx192) & reinterpret_cast<uint32_t>("("), eax194 == 0x8000)) {
                                            if (*reinterpret_cast<unsigned char*>(&v56)) {
                                                rbx31 = v29;
                                                *reinterpret_cast<void***>(rbx31 + 8) = v133;
                                                rbp4 = *reinterpret_cast<void***>(rbx31);
                                                *reinterpret_cast<void***>(rbx31 + 56) = r15_3;
                                                *reinterpret_cast<void***>(rbx31 + 16) = v195;
                                                *reinterpret_cast<void***>(rbx31 + 48) = rdx192;
                                                *reinterpret_cast<void***>(rbx31 + 24) = v196;
                                                *reinterpret_cast<void***>(rbx31 + 64) = reinterpret_cast<void**>(eax194 - (eax194 + reinterpret_cast<uint1_t>(eax194 < eax194 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&v107)) < reinterpret_cast<unsigned char>(1)))) | 1);
                                                *reinterpret_cast<int64_t*>(rbx31 + 32) = v197;
                                                *reinterpret_cast<void***>(rbx31 + 88) = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<void***>(rbx31 + 40) = reinterpret_cast<void**>(1);
                                                *reinterpret_cast<void***>(rbx31 + 52) = reinterpret_cast<void**>(0);
                                                eax198 = fun_2770(rbp4, "-", rbp4, "-");
                                                rsp199 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp186) - 8 + 8);
                                                if (!eax198) {
                                                    rdx192 = reinterpret_cast<void**>(5);
                                                    *reinterpret_cast<int32_t*>(&rdx192 + 4) = 0;
                                                    rax200 = fun_2600();
                                                    rsp199 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp199) - 8 + 8);
                                                    rbp4 = rax200;
                                                }
                                                *reinterpret_cast<void***>(&rdi201) = r15_3;
                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi201) + 4) = 0;
                                                al202 = fremote(rdi201, rbp4, rdx192);
                                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp199) - 8 + 8);
                                                *reinterpret_cast<signed char*>(v29 + 53) = al202;
                                                continue;
                                            }
                                        } else {
                                            eax203 = reopen_inaccessible_files;
                                            rbx204 = reinterpret_cast<void**>(0xea41);
                                            eax205 = eax203 ^ 1;
                                            *reinterpret_cast<void***>(v29 + 60) = reinterpret_cast<void**>(0xffffffff);
                                            *reinterpret_cast<unsigned char*>(v29 + 54) = 0;
                                            *reinterpret_cast<void***>(v29 + 52) = *reinterpret_cast<void***>(&eax205);
                                            if (*reinterpret_cast<void***>(&eax205)) {
                                                rax206 = fun_2600();
                                                rsp186 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp186) - 8 + 8);
                                                rbx204 = rax206;
                                            }
                                            rdi207 = *reinterpret_cast<void***>(v29);
                                            eax208 = fun_2770(rdi207, "-", rdi207, "-");
                                            rsp209 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp186) - 8 + 8);
                                            if (!eax208) {
                                                fun_2600();
                                                rsp209 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp209) - 8 + 8);
                                            }
                                            quotearg_n_style_colon();
                                            fun_2600();
                                            r8_32 = rbx204;
                                            fun_28a0();
                                            rsp186 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp209) - 8 + 8 - 8 + 8 - 8 + 8);
                                        }
                                    }
                                    eax210 = reopen_inaccessible_files;
                                    rbx31 = v29;
                                    eax211 = eax210 ^ 1;
                                    rbp4 = *reinterpret_cast<void***>(rbx31);
                                    *reinterpret_cast<void***>(rbx31 + 52) = *reinterpret_cast<void***>(&eax211);
                                    eax212 = fun_2770(rbp4, "-", rbp4, "-");
                                    rsp213 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp186) - 8 + 8);
                                    if (!eax212) {
                                        rax214 = fun_2600();
                                        rsp213 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp213) - 8 + 8);
                                        rbp4 = rax214;
                                    }
                                    *reinterpret_cast<void***>(&rdi215) = r15_3;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi215) + 4) = 0;
                                    close_fd(rdi215, rbp4, rdi215, rbp4);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp213) - 8 + 8);
                                    *reinterpret_cast<unsigned char*>(&v56) = 0;
                                    *reinterpret_cast<void***>(v29 + 56) = reinterpret_cast<void**>(0xffffffff);
                                    continue;
                                }
                                if (!*reinterpret_cast<void***>(&v107)) 
                                    continue;
                                eax216 = fun_26f0();
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                if (!eax216) 
                                    continue;
                                rbp4 = *reinterpret_cast<void***>(v29);
                                eax217 = fun_2770(rbp4, "-", rbp4, "-");
                                rsp218 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                if (eax217) 
                                    goto addr_3608_244;
                                rax219 = fun_2600();
                                rsp218 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp218) - 8 + 8);
                                rbp4 = rax219;
                                addr_3608_244:
                                rax220 = quotearg_style(4, rbp4, 4, rbp4);
                                r13_22 = rax220;
                                fun_2600();
                                fun_2530();
                                fun_28a0();
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp218) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                *reinterpret_cast<unsigned char*>(&v56) = 0;
                                continue;
                                addr_391f_172:
                                rbx31 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rbx31 + 4) = 0;
                                rax221 = xmalloc(0x2010, rsi135, 0x2010, rsi135);
                                *reinterpret_cast<void***>(rax221 + 0x2000) = reinterpret_cast<void**>(0);
                                r13_22 = rax221;
                                *reinterpret_cast<void***>(rax221 + 0x2008) = reinterpret_cast<void**>(0);
                                r14_25 = r13_22;
                                rax222 = xmalloc(0x2010, rsi135, 0x2010, rsi135);
                                rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                                r12_224 = v97;
                                rbp225 = rax222;
                                while (rdi226 = r15_3, *reinterpret_cast<int32_t*>(&rdi226 + 4) = 0, rax227 = safe_read(rdi226, rbp225, 0x2000, rcx118, rdi226, rbp225, 0x2000, rcx118), rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp223) - 8 + 8), reinterpret_cast<unsigned char>(rax227 + 0xffffffffffffffff) <= reinterpret_cast<unsigned char>(0xfffffffffffffffd)) {
                                    v133 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v133) + reinterpret_cast<unsigned char>(rax227));
                                    rbx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) + reinterpret_cast<unsigned char>(rax227));
                                    *reinterpret_cast<void***>(rbp225 + 0x2000) = rax227;
                                    rax228 = *reinterpret_cast<void***>(r14_25 + 0x2000);
                                    *reinterpret_cast<void***>(rbp225 + 0x2008) = reinterpret_cast<void**>(0);
                                    rcx118 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax227) + reinterpret_cast<unsigned char>(rax228));
                                    if (reinterpret_cast<unsigned char>(rcx118) > reinterpret_cast<unsigned char>(0x1fff)) {
                                        rax229 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_22 + 0x2000)));
                                        *reinterpret_cast<void***>(r14_25 + 0x2008) = rbp225;
                                        if (reinterpret_cast<unsigned char>(r12_224) >= reinterpret_cast<unsigned char>(rax229)) {
                                            rax230 = xmalloc(0x2010, rbp225, 0x2010, rbp225);
                                            rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp223) - 8 + 8);
                                        } else {
                                            rbx31 = rax229;
                                            rax230 = r13_22;
                                            r13_22 = *reinterpret_cast<void***>(r13_22 + 0x2008);
                                        }
                                    } else {
                                        fun_27b0(reinterpret_cast<unsigned char>(r14_25) + reinterpret_cast<unsigned char>(rax228), rbp225, rax227, rcx118, r8_32, r9_38);
                                        rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp223) - 8 + 8);
                                        *reinterpret_cast<void***>(r14_25 + 0x2000) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_25 + 0x2000)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp225 + 0x2000)));
                                        rax230 = rbp225;
                                        rbp225 = r14_25;
                                    }
                                    r14_25 = rbp225;
                                    rbp225 = rax230;
                                }
                                rbp4 = r13_22;
                                fun_2510(rbp225, rbp225);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp223) - 8 + 8);
                                rdx231 = v97;
                                if (rax227 + 1) {
                                    while (rax232 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 0x2000))), reinterpret_cast<unsigned char>(rdx231) < reinterpret_cast<unsigned char>(rax232)) {
                                        rbp4 = *reinterpret_cast<void***>(rbp4 + 0x2008);
                                        rbx31 = rax232;
                                    }
                                    rcx233 = v97;
                                    *reinterpret_cast<int32_t*>(&rax234) = 0;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax234) + 4) = 0;
                                    if (reinterpret_cast<unsigned char>(rcx233) < reinterpret_cast<unsigned char>(rbx31)) 
                                        goto addr_4633_257;
                                } else {
                                    rax235 = quotearg_style(4, v105, 4, v105);
                                    r14_25 = rax235;
                                    fun_2600();
                                    rax236 = fun_2530();
                                    rsi237 = *reinterpret_cast<void***>(rax236);
                                    *reinterpret_cast<int32_t*>(&rsi237 + 4) = 0;
                                    fun_28a0();
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    rdi238 = r13_22;
                                    goto addr_468a_260;
                                }
                                addr_4639_261:
                                rsi237 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 0x2000)) - reinterpret_cast<uint64_t>(rax234));
                                if (rsi237) {
                                    xwrite_stdout_part_0(reinterpret_cast<unsigned char>(rbp4) + reinterpret_cast<uint64_t>(rax234), rsi237, rdx231, rcx233, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                }
                                rbx31 = *reinterpret_cast<void***>(rbp4 + 0x2008);
                                while (rbx31) {
                                    rsi237 = *reinterpret_cast<void***>(rbx31 + 0x2000);
                                    if (rsi237) {
                                        xwrite_stdout_part_0(rbx31, rsi237, rdx231, rcx233, r8_32, r9_38);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    }
                                    rbx31 = *reinterpret_cast<void***>(rbx31 + 0x2008);
                                }
                                *reinterpret_cast<unsigned char*>(&v56) = 1;
                                rdi238 = r13_22;
                                addr_468a_260:
                                while (rdi238) {
                                    rbx31 = *reinterpret_cast<void***>(rdi238 + 0x2008);
                                    fun_2510(rdi238, rsi237, rdi238, rsi237);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    rdi238 = rbx31;
                                }
                                goto addr_35c1_223;
                                addr_4633_257:
                                rax234 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(rcx233));
                                goto addr_4639_261;
                                addr_46ca_184:
                                xlseek_part_0(0, 1, v105, rcx118, r8_32, r9_38);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                goto addr_46db_271;
                                addr_3bd4_185:
                                v133 = rbp4;
                                goto addr_35a0_272;
                                addr_32d9_165:
                                rax239 = xmalloc(0x2018, rsi135, 0x2018, rsi135);
                                *reinterpret_cast<void***>(rax239 + 0x2008) = reinterpret_cast<void**>(0);
                                *reinterpret_cast<void***>(rax239 + 0x2000) = reinterpret_cast<void**>(0);
                                *reinterpret_cast<void***>(rax239 + 0x2010) = reinterpret_cast<void**>(0);
                                v100 = rax239;
                                rax240 = xmalloc(0x2018, rsi135, 0x2018, rsi135);
                                rsp241 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8 - 8 + 8);
                                v60 = reinterpret_cast<void**>(0);
                                r13_242 = rax240;
                                r14_243 = rax239;
                                while (rsi244 = r13_242, rdi245 = r15_3, *reinterpret_cast<int32_t*>(&rdi245 + 4) = 0, rax246 = safe_read(rdi245, rsi244, 0x2000, rcx118, rdi245, rsi244, 0x2000, rcx118), rsp241 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp241) - 8 + 8), r12_247 = rax246, reinterpret_cast<unsigned char>(rax246 + 0xffffffffffffffff) <= reinterpret_cast<unsigned char>(0xfffffffffffffffd)) {
                                    *reinterpret_cast<void***>(r13_242 + 0x2000) = r12_247;
                                    ebx248 = line_end;
                                    rbp4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_242) + reinterpret_cast<unsigned char>(r12_247));
                                    v133 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v133) + reinterpret_cast<unsigned char>(r12_247));
                                    *reinterpret_cast<void***>(r13_242 + 0x2008) = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<void***>(r13_242 + 0x2010) = reinterpret_cast<void**>(0);
                                    while (*reinterpret_cast<int32_t*>(&rsi249) = ebx248, *reinterpret_cast<int32_t*>(&rsi249 + 4) = 0, rax250 = fun_2700(), rsp241 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp241) - 8 + 8), !!rax250) {
                                        *reinterpret_cast<void***>(r13_242 + 0x2008) = *reinterpret_cast<void***>(r13_242 + 0x2008) + 1;
                                    }
                                    rbx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v60) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_242 + 0x2008)));
                                    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v100 + 0x2000))) <= 0x1fff) {
                                        rbp4 = v100;
                                        rdi251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v100 + 0x2000)) + reinterpret_cast<unsigned char>(rbp4));
                                        fun_27b0(rdi251, r13_242, r12_247, rcx118, r8_32, r9_38);
                                        rsp241 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp241) - 8 + 8);
                                        rax252 = *reinterpret_cast<void***>(r13_242 + 0x2000);
                                        v60 = rbx31;
                                        *reinterpret_cast<void***>(rbp4 + 0x2000) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 0x2000)) + reinterpret_cast<unsigned char>(rax252));
                                        *reinterpret_cast<void***>(rbp4 + 0x2008) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 0x2008)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_242 + 0x2008)));
                                    } else {
                                        *reinterpret_cast<void***>(v100 + 0x2010) = r13_242;
                                        rax253 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_243 + 0x2008)));
                                        v60 = rax253;
                                        if (reinterpret_cast<unsigned char>(v97) >= reinterpret_cast<unsigned char>(rax253)) {
                                            rax254 = xmalloc(0x2018, rsi249, 0x2018, rsi249);
                                            rsp241 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp241) - 8 + 8);
                                            v100 = r13_242;
                                            v60 = rbx31;
                                            r13_242 = rax254;
                                        } else {
                                            v100 = r13_242;
                                            r13_242 = r14_243;
                                            r14_243 = *reinterpret_cast<void***>(r14_243 + 0x2010);
                                        }
                                    }
                                }
                                r14_25 = r13_242;
                                r13_22 = r14_243;
                                fun_2510(r14_25, rsi244, r14_25, rsi244);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp241) - 8 + 8);
                                if (!(r12_247 + 1)) {
                                    addr_46db_271:
                                    rax255 = quotearg_style(4, v105, 4, v105);
                                    r14_25 = rax255;
                                    fun_2600();
                                    rax256 = fun_2530();
                                    rsi244 = *reinterpret_cast<void***>(rax256);
                                    *reinterpret_cast<int32_t*>(&rsi244 + 4) = 0;
                                    fun_28a0();
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_383f_283;
                                } else {
                                    rbx31 = v100;
                                    rcx48 = *reinterpret_cast<void***>(rbx31 + 0x2000);
                                    dl257 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rcx48 == 0)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(v97 == 0)));
                                    *reinterpret_cast<unsigned char*>(&v141) = dl257;
                                    if (!dl257) {
                                        rbp4 = reinterpret_cast<void**>(static_cast<int32_t>(line_end));
                                        *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                                        if (*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx31) + reinterpret_cast<unsigned char>(rcx48) + 0xffffffffffffffff) != *reinterpret_cast<unsigned char*>(&rbp4)) {
                                            *reinterpret_cast<void***>(rbx31 + 0x2008) = *reinterpret_cast<void***>(rbx31 + 0x2008) + 1;
                                            ++v60;
                                        }
                                        rdx50 = v97;
                                        rbx258 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v60) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_22 + 0x2008)));
                                        if (reinterpret_cast<unsigned char>(rdx50) >= reinterpret_cast<unsigned char>(rbx258)) {
                                            rbx258 = v60;
                                            r14_25 = r13_22;
                                        } else {
                                            r14_25 = r13_22;
                                            while (r14_25 = *reinterpret_cast<void***>(r14_25 + 0x2010), rax259 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx258) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_25 + 0x2008))), reinterpret_cast<unsigned char>(rdx50) < reinterpret_cast<unsigned char>(rax259)) {
                                                rbx258 = rax259;
                                            }
                                        }
                                        rdi49 = r14_25;
                                        r12_260 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_25 + 0x2000)) + reinterpret_cast<unsigned char>(r14_25));
                                        if (reinterpret_cast<unsigned char>(v97) < reinterpret_cast<unsigned char>(rbx258)) {
                                            rbx261 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx258) - reinterpret_cast<unsigned char>(v97));
                                            do {
                                                rsi244 = rbp4;
                                                *reinterpret_cast<int32_t*>(&rsi244 + 4) = 0;
                                                rax262 = fun_2750();
                                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                                rdi49 = reinterpret_cast<void**>(&rax262->f1);
                                                rbx261 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx261) - 1);
                                            } while (rbx261);
                                        }
                                        r12_30 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r12_260) - reinterpret_cast<unsigned char>(rdi49));
                                        if (r12_30) {
                                            addr_415a_64:
                                            rsi244 = r12_30;
                                            xwrite_stdout_part_0(rdi49, rsi244, rdx50, rcx48, r8_32, r9_38);
                                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        }
                                        rbx31 = *reinterpret_cast<void***>(r14_25 + 0x2010);
                                        if (rbx31) {
                                            do {
                                                rsi244 = *reinterpret_cast<void***>(rbx31 + 0x2000);
                                                if (rsi244) {
                                                    xwrite_stdout_part_0(rbx31, rsi244, rdx50, rcx48, r8_32, r9_38);
                                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                                }
                                                rbx31 = *reinterpret_cast<void***>(rbx31 + 0x2010);
                                            } while (rbx31);
                                        }
                                        eax263 = *reinterpret_cast<unsigned char*>(&v56);
                                        *reinterpret_cast<unsigned char*>(&v141) = *reinterpret_cast<unsigned char*>(&eax263);
                                    } else {
                                        addr_383f_283:
                                        if (!r13_22) {
                                            addr_385c_302:
                                            eax264 = *reinterpret_cast<unsigned char*>(&v141);
                                            *reinterpret_cast<unsigned char*>(&v56) = *reinterpret_cast<unsigned char*>(&eax264);
                                            goto addr_35c1_223;
                                        }
                                    }
                                }
                                do {
                                    rdi265 = r13_22;
                                    r13_22 = *reinterpret_cast<void***>(r13_22 + 0x2010);
                                    fun_2510(rdi265, rsi244, rdi265, rsi244);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                } while (r13_22);
                                goto addr_385c_302;
                                addr_357b_218:
                                rax266 = fun_26b0(rdi180, rdi180);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                if (!(rax266 + 1)) {
                                    addr_364e_215:
                                    eax267 = start_bytes(v105, r15_3, v97, reinterpret_cast<int64_t>(rsp51) + 0x90, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    v97 = reinterpret_cast<void**>(0xffffffffffffffff);
                                    if (!eax267) {
                                        addr_35a0_272:
                                        rax268 = dump_remainder(0, v105, r15_3, v97, r8_32, r9_38);
                                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                        v133 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v133) + reinterpret_cast<unsigned char>(rax268));
                                        *reinterpret_cast<unsigned char*>(&v56) = 1;
                                        goto addr_35c1_223;
                                    } else {
                                        *reinterpret_cast<uint32_t*>(&v56) = eax267 >> 31;
                                        goto addr_35c1_223;
                                    }
                                } else {
                                    addr_358a_220:
                                    rax269 = v97;
                                    v97 = reinterpret_cast<void**>(0xffffffffffffffff);
                                    v133 = rax269;
                                    goto addr_35a0_272;
                                }
                                addr_3acf_161:
                                rax270 = quotearg_style(4, v105, 4, v105);
                                r13_22 = rax270;
                                fun_2600();
                                fun_2530();
                                fun_28a0();
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                *reinterpret_cast<unsigned char*>(&v141) = 0;
                                goto addr_385c_302;
                                addr_3a53_163:
                                eax271 = start_lines(v105, r15_3, v97, reinterpret_cast<int64_t>(rsp139) + 0x90, r8_32, r9_38);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                                if (!eax271) {
                                    rax272 = dump_remainder(0, v105, r15_3, 0xffffffffffffffff, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    v133 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rax272)));
                                    goto addr_385c_302;
                                } else {
                                    v141 = eax271 >> 31;
                                    goto addr_385c_302;
                                }
                                addr_3d5b_170:
                                v133 = rax147;
                                eax273 = *reinterpret_cast<unsigned char*>(&v56);
                                *reinterpret_cast<unsigned char*>(&v141) = *reinterpret_cast<unsigned char*>(&eax273);
                                if (r8_32) {
                                    r9_38 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x90);
                                    al274 = file_lines(v105, r15_3, v97, r12_30, r8_32, r9_38);
                                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                    *reinterpret_cast<unsigned char*>(&v141) = al274;
                                    goto addr_385c_302;
                                }
                            }
                            goto addr_39fc_309;
                            addr_313c_142:
                            rax275 = fun_2530();
                            zf276 = forever == 0;
                            rbx31 = rax275;
                            if (!zf276) {
                                eax277 = *reinterpret_cast<void***>(rax275);
                                r13_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_22) ^ 1);
                                *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
                                *reinterpret_cast<void***>(v29 + 56) = reinterpret_cast<void**>(0xffffffff);
                                *reinterpret_cast<void***>(v29 + 60) = eax277;
                                *reinterpret_cast<void***>(v29 + 52) = r13_22;
                                *reinterpret_cast<void***>(v29 + 40) = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int64_t*>(v29 + 32) = 0;
                                continue;
                            }
                            addr_3133_146:
                            *reinterpret_cast<unsigned char*>(v29 + 54) = 0;
                            goto addr_313c_142;
                            eax278 = fun_2770(rbp4, "-", rbp4, "-");
                            rsp116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                        } while (eax278);
                    }
                    addr_39fc_309:
                    eax279 = forever;
                    *reinterpret_cast<unsigned char*>(&v105) = *reinterpret_cast<unsigned char*>(&eax279);
                    if (!*reinterpret_cast<unsigned char*>(&eax279)) 
                        goto addr_3a0f_79;
                    r12_280 = v58;
                    r13_22 = v108;
                    rbp4 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                    rbx31 = reinterpret_cast<void**>("-");
                    do {
                        rdi281 = *reinterpret_cast<void***>(r12_280);
                        eax282 = fun_2770(rdi281, "-", rdi281, "-");
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                        if (eax282 || (*reinterpret_cast<void***>(r12_280 + 52) || (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_280 + 56)) < reinterpret_cast<signed char>(0) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_280 + 48)) & 0xf000) != "sterTMCloneTable"))) {
                            ++rbp4;
                        } else {
                            *reinterpret_cast<void***>(r12_280 + 56) = reinterpret_cast<void**>(0xffffffff);
                            *reinterpret_cast<void***>(r12_280 + 52) = reinterpret_cast<void**>(1);
                        }
                        r12_280 = r12_280 + 96;
                    } while (r13_22 != r12_280);
                } while (!rbp4);
                goto addr_3e19_318;
            } else {
                *reinterpret_cast<void***>(&rdi283) = pid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi283) + 4) = 0;
                if (!*reinterpret_cast<void***>(&rdi283) || ((rsi59 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0, eax284 = fun_27c0(), rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8), eax284 == 0) || (rax285 = fun_2530(), rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8), *reinterpret_cast<void***>(rax285) == 1))) {
                    *rdi283 = *reinterpret_cast<void***>(rsi59);
                    rsi59 = rsi59 + 4;
                    eax286 = xnanosleep(rdi283 + 4);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    if (!eax286) 
                        goto addr_3f20_75; else 
                        goto addr_44ad_324;
                } else {
                    eax287 = *reinterpret_cast<unsigned char*>(&v105);
                    *reinterpret_cast<signed char*>(&v107) = *reinterpret_cast<signed char*>(&eax287);
                    goto addr_3f20_75;
                }
            }
        }
        addr_3cf9_205:
        rax288 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        if (rax288) 
            goto addr_4bd9_84; else 
            goto addr_3d10_66;
        addr_3e19_318:
        rsi59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0xf0);
        eax289 = fun_2980();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
        if (reinterpret_cast<signed char>(eax289) < reinterpret_cast<signed char>(0)) 
            goto addr_4aea_129;
        monitor_output = reinterpret_cast<uint1_t>((v290 & 0xf000) == "sterTMCloneTable");
        zf291 = disable_inotify == 0;
        if (!zf291) 
            goto addr_3ec6_327;
        rbp4 = v58;
        r12_292 = v108;
        do {
            if (*reinterpret_cast<void***>(rbp4 + 52)) 
                continue;
            rdi293 = *reinterpret_cast<void***>(rbp4);
            rsi59 = reinterpret_cast<void**>("-");
            eax294 = fun_2770(rdi293, "-", rdi293, "-");
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            if (!eax294) 
                goto addr_3ec6_327;
            rbp4 = rbp4 + 96;
        } while (rbp4 != r12_292);
        rdx295 = v58;
        rcx296 = v110;
        *reinterpret_cast<int32_t*>(&rax297) = 0;
        *reinterpret_cast<int32_t*>(&rax297 + 4) = 0;
        do {
            if (*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rdx295) + reinterpret_cast<unsigned char>(rax297) + 56) < 0) 
                continue;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx295) + reinterpret_cast<unsigned char>(rax297) + 53)) 
                goto addr_3ec6_327;
            rax297 = rax297 + 96;
        } while (rax297 != rcx296);
        rdx298 = v58;
        *reinterpret_cast<int32_t*>(&rax299) = 0;
        *reinterpret_cast<int32_t*>(&rax299 + 4) = 0;
        do {
            if (*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rdx298) + reinterpret_cast<unsigned char>(rax299) + 56) < 0) 
                continue;
            ecx300 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx298) + reinterpret_cast<unsigned char>(rax299) + 53);
            if (!*reinterpret_cast<unsigned char*>(&ecx300)) 
                break;
            rax299 = rax299 + 96;
        } while (rax299 != v110);
        goto addr_3ec6_327;
        *reinterpret_cast<unsigned char*>(&v141) = *reinterpret_cast<unsigned char*>(&ecx300);
        rbx31 = v58;
        v60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x180);
        do {
            rsi59 = v60;
            rdi301 = *reinterpret_cast<void***>(rbx31);
            eax302 = fun_2720(rdi301, rsi59);
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            if (eax302) 
                continue;
            if ((v303 & 0xf000) == 0xa000) 
                goto addr_3ec6_327;
            rbx31 = rbx31 + 96;
        } while (rbx31 != v108);
        rcx304 = v58;
        *reinterpret_cast<int32_t*>(&rax305) = 0;
        *reinterpret_cast<int32_t*>(&rax305 + 4) = 0;
        do {
            if (*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rcx304) + reinterpret_cast<unsigned char>(rax305) + 56) < 0) 
                continue;
            edx306 = *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rcx304) + reinterpret_cast<unsigned char>(rax305) + 48) & 0xf000;
            if (edx306 == 0x8000) 
                continue;
            if (!reinterpret_cast<int1_t>(edx306 == "sterTMCloneTable")) 
                goto addr_3ec6_327;
            rax305 = rax305 + 96;
        } while (rax305 != v110);
        if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v107) + 7) || (zf307 = follow_mode == 2, !zf307)) {
            eax308 = fun_27d0(rdi301);
            rsp309 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            r13_22 = eax308;
            *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
            if (reinterpret_cast<signed char>(eax308) < reinterpret_cast<signed char>(0)) {
                addr_4c46_351:
                fun_2600();
                rax310 = fun_2530();
                rsi59 = *reinterpret_cast<void***>(rax310);
                *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
                fun_28a0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp309) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_3ec6_327;
            } else {
                rdi311 = stdout;
                eax312 = fun_2940(rdi311, rsi59);
                rsp313 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp309) - 8 + 8);
                if (eax312) {
                    rax314 = fun_2600();
                    r12_315 = rax314;
                    rax316 = fun_2530();
                    rsi317 = *reinterpret_cast<void***>(rax316);
                    *reinterpret_cast<int32_t*>(&rsi317 + 4) = 0;
                    fun_28a0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp313) - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    *reinterpret_cast<int32_t*>(&r8_32) = 0;
                    *reinterpret_cast<int32_t*>(&r8_32 + 4) = 0;
                    rcx55 = reinterpret_cast<void**>(0x5400);
                    rsi318 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
                    rax319 = hash_initialize(v46);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp313) - 8 + 8);
                    r14_25 = rax319;
                    if (!rax319) 
                        goto addr_510e_355;
                    rdx62 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    eax320 = reinterpret_cast<void**>(0xc06);
                    zf321 = follow_mode == 1;
                    if (!zf321) 
                        goto label_357; else 
                        goto addr_493d_358;
                }
            }
        } else {
            addr_3ec6_327:
            rbp4 = pid;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            disable_inotify = 1;
            if (rbp4) {
                rbp4 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                goto addr_3ef0_360;
            } else {
                zf322 = follow_mode == 2;
                if (zf322 && (v46 == 1 && *reinterpret_cast<void***>(v58 + 56) != 0xffffffff)) {
                    eax323 = *reinterpret_cast<void***>(v58 + 48);
                    rbp4 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                    v29 = eax323;
                    *reinterpret_cast<unsigned char*>(&rbp4) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(eax323) & 0xf000) != 0x8000);
                    goto addr_3ef0_360;
                }
            }
        }
        addr_5299_363:
        rdi324 = *reinterpret_cast<void***>(r12_315);
        rax325 = pretty_name_isra_0(rdi324, rsi317, rdi324, rsi317);
        rax326 = quotearg_style(4, rax325, 4, rax325);
        rax327 = fun_2600();
        rax328 = fun_2530();
        rcx55 = rax326;
        rdx62 = rax327;
        rsi318 = *reinterpret_cast<void***>(rax328);
        *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
        fun_28a0();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_4c2b_364;
        label_357:
        eax320 = reinterpret_cast<void**>(2);
        addr_493d_358:
        *reinterpret_cast<unsigned char*>(&v100) = 0;
        *reinterpret_cast<unsigned char*>(&v56) = 0;
        rbp4 = v58;
        *reinterpret_cast<void***>(&v107) = eax320;
        v29 = reinterpret_cast<void**>(0);
        do {
            r12d329 = follow_mode;
            if (!*reinterpret_cast<void***>(rbp4 + 52)) {
                rbx31 = *reinterpret_cast<void***>(rbp4);
                rax330 = fun_2620(rbx31, rbx31);
                rsp331 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                rcx55 = v29;
                *reinterpret_cast<void***>(rbp4 + 68) = reinterpret_cast<void**>(0xffffffff);
                if (reinterpret_cast<unsigned char>(rcx55) >= reinterpret_cast<unsigned char>(rax330)) {
                    rax330 = rcx55;
                }
                *reinterpret_cast<uint32_t*>(&r12_292) = reinterpret_cast<uint32_t>(r12d329 - 1);
                *reinterpret_cast<int32_t*>(&r12_292 + 4) = 0;
                v29 = rax330;
                if (!*reinterpret_cast<uint32_t*>(&r12_292)) {
                    rax332 = dir_len(rbx31, rsi318);
                    rdx333 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx31) + reinterpret_cast<unsigned char>(rax332));
                    r15_3 = rax332;
                    *reinterpret_cast<uint32_t*>(&r12_292) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx333));
                    *reinterpret_cast<int32_t*>(&r12_292 + 4) = 0;
                    v97 = rdx333;
                    rax334 = last_component(rbx31, rsi318);
                    rsi335 = reinterpret_cast<void**>(".");
                    *reinterpret_cast<void**>(rbp4 + 80) = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax334) - reinterpret_cast<unsigned char>(rbx31));
                    *reinterpret_cast<void***>(v97) = reinterpret_cast<void**>(0);
                    if (r15_3) {
                        rsi335 = *reinterpret_cast<void***>(rbp4);
                    }
                    *reinterpret_cast<void***>(&rdi336) = r13_22;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi336) + 4) = 0;
                    eax337 = fun_2820(rdi336, rsi335, rdi336, rsi335);
                    rsp331 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp331) - 8 + 8 - 8 + 8 - 8 + 8);
                    *reinterpret_cast<void***>(rbp4 + 72) = eax337;
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4)) + reinterpret_cast<unsigned char>(r15_3)) = *reinterpret_cast<signed char*>(&r12_292);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp4 + 72)) < reinterpret_cast<signed char>(0)) 
                        goto addr_5222_372;
                }
                rdx62 = *reinterpret_cast<void***>(&v107);
                *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                rsi318 = *reinterpret_cast<void***>(rbp4);
                *reinterpret_cast<void***>(&rdi338) = r13_22;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi338) + 4) = 0;
                eax339 = fun_2820(rdi338, rsi318);
                rsp340 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp331) - 8 + 8);
                *reinterpret_cast<void***>(rbp4 + 68) = eax339;
                if (reinterpret_cast<signed char>(eax339) < reinterpret_cast<signed char>(0)) {
                    eax341 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v100)));
                    rcx55 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v105)));
                    *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4 + 56) == 0xffffffff)) {
                        eax341 = rcx55;
                    }
                    *reinterpret_cast<unsigned char*>(&v100) = *reinterpret_cast<unsigned char*>(&eax341);
                    rax342 = fun_2530();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp340) - 8 + 8);
                    rdx62 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax342)) & 0xffffffef);
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    if (rdx62 == 12) 
                        goto addr_4c0a_377;
                    if (*reinterpret_cast<void***>(rax342) != *reinterpret_cast<void***>(rbp4 + 60)) {
                        rsi343 = *reinterpret_cast<void***>(rbp4);
                        rax344 = quotearg_style(4, rsi343);
                        r15_3 = rax344;
                        rax345 = fun_2600();
                        rsi318 = *reinterpret_cast<void***>(rax342);
                        *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
                        rcx55 = r15_3;
                        rdx62 = rax345;
                        fun_28a0();
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                    }
                } else {
                    rsi318 = rbp4;
                    rax346 = hash_insert(r14_25);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp340) - 8 + 8);
                    if (!rax346) 
                        goto addr_510e_355;
                    eax347 = *reinterpret_cast<unsigned char*>(&v105);
                    *reinterpret_cast<unsigned char*>(&v56) = *reinterpret_cast<unsigned char*>(&eax347);
                }
            }
            rbp4 = rbp4 + 96;
        } while (v108 != rbp4);
        zf348 = follow_mode == 2;
        if (zf348) {
            if (*reinterpret_cast<unsigned char*>(&v100)) {
                addr_4c2b_364:
                hash_free(r14_25, rsi318, rdx62, rcx55, r8_32, r9_38);
                fun_26f0();
                rax349 = fun_2530();
                rsp309 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                *reinterpret_cast<void***>(rax349) = reinterpret_cast<void**>(0);
                goto addr_4c46_351;
            } else {
                if (!*reinterpret_cast<unsigned char*>(&v56)) {
                    goto addr_4bf4_87;
                }
            }
        } else {
            r12_315 = v58;
            rbx350 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x88);
            rbp4 = v108;
        }
        do {
            if (!*reinterpret_cast<void***>(r12_315 + 52)) {
                zf351 = follow_mode == 1;
                if (!zf351) {
                    if (*reinterpret_cast<void***>(r12_315 + 56) == 0xffffffff) 
                        goto addr_4ce7_391;
                    rsi317 = v60;
                    rdi352 = *reinterpret_cast<void***>(r12_315);
                    eax353 = fun_2790(rdi352, rsi317);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    if (eax353) 
                        goto addr_4ce7_391;
                    if (1) 
                        goto addr_5299_363;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_315 + 40) == 1)) 
                        goto addr_5299_363;
                } else {
                    recheck(r12_315, 0);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                }
                addr_4ce7_391:
                rsi318 = rbx350;
                check_fspec(r12_315, rsi318);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            }
            r12_315 = r12_315 + 96;
        } while (rbp4 != r12_315);
        r12_292 = v29 + 17;
        rax354 = xmalloc(r12_292, rsi318);
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
        *reinterpret_cast<int32_t*>(&v97) = 3;
        v100 = rax354;
        v56 = reinterpret_cast<void**>(0);
        v29 = reinterpret_cast<void**>(0);
        while (1) {
            addr_4d48_398:
            zf355 = follow_mode == 1;
            if (!zf355 || ((zf356 = reopen_inaccessible_files == 0, !zf356) || (rax357 = hash_get_n_entries(r14_25, rsi318), rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8), !!rax357))) {
                rcx55 = v56;
                if (reinterpret_cast<unsigned char>(v29) >= reinterpret_cast<unsigned char>(rcx55)) {
                    rbp4 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v141)));
                    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                    rbx31 = v60;
                    while (1) {
                        *reinterpret_cast<void***>(&rdi358) = pid;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi358) + 4) = 0;
                        if (*reinterpret_cast<void***>(&rdi358)) {
                            if (*reinterpret_cast<unsigned char*>(&rbp4)) 
                                break;
                            eax359 = fun_27c0();
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                            if (!eax359) 
                                goto addr_4f51_404;
                            rax360 = fun_2530();
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                            if (*reinterpret_cast<void***>(rax360) != 1) 
                                goto addr_4f01_406;
                        } else {
                            addr_4f4c_407:
                            rdx62 = reinterpret_cast<void**>(0xffffffff);
                            *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                            goto addr_4f08_408;
                        }
                        addr_4f51_404:
                        rdx62 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        __asm__("pxor xmm7, xmm7");
                        __asm__("comisd xmm7, [rsp+0x40]");
                        if (1) {
                            addr_4f08_408:
                            *reinterpret_cast<uint32_t*>(&rax361) = monitor_output;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax361) + 4) = 0;
                            rcx55 = reinterpret_cast<void**>(16);
                            *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                            *reinterpret_cast<void***>(&v197) = r13_22;
                            *reinterpret_cast<int16_t*>(reinterpret_cast<int64_t>(&v197) + 4) = 1;
                            rsi59 = reinterpret_cast<void**>(&rax361->f1);
                            eax362 = fun_26e0(rbx31, rsi59);
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                            if (eax362) 
                                goto addr_4fab_409; else 
                                continue;
                        } else {
                            rdi358->f0 = __cxa_finalize;
                            rdi363 = reinterpret_cast<struct s11*>(&rdi358->f4);
                            rdi363->f0 = (&__cxa_finalize)[4];
                            __asm__("comisd xmm6, xmm5");
                            if (1) 
                                goto addr_4f4c_407;
                        }
                        rdi363->f4 = *reinterpret_cast<int32_t*>(&g8);
                        __asm__("pxor xmm1, xmm1");
                        __asm__("mulsd xmm0, xmm5");
                        __asm__("cvttsd2si edx, xmm0");
                        __asm__("cvtsi2sd xmm1, edx");
                        __asm__("comisd xmm0, xmm1");
                        rdx62 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        goto addr_4f08_408;
                        addr_4f01_406:
                        rbp4 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v105)));
                        *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                        rdx62 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        goto addr_4f08_408;
                    }
                } else {
                    r15_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v100) + reinterpret_cast<unsigned char>(v29));
                    *reinterpret_cast<int32_t*>(&rdx364) = *reinterpret_cast<int32_t*>(r15_3 + 12);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx364) + 4) = 0;
                    rax365 = rdx364;
                    rdx62 = *reinterpret_cast<void***>(r15_3);
                    v29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v29) + reinterpret_cast<uint64_t>(rdx364) + 16);
                    rcx55 = *reinterpret_cast<void***>(r15_3 + 4);
                    if (*reinterpret_cast<unsigned char*>(&rcx55 + 1) & 4) {
                        if (*reinterpret_cast<int32_t*>(&rax365)) 
                            goto addr_4d9b_423;
                        *reinterpret_cast<int32_t*>(&rax366) = 0;
                        *reinterpret_cast<int32_t*>(&rax366 + 4) = 0;
                        do {
                            rcx55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax366) * 96);
                            if (*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(rcx55) + 72) == rdx62) 
                                goto addr_5169_426;
                            ++rax366;
                        } while (v46 != rax366);
                        goto addr_4e5c_428;
                    }
                }
            } else {
                fun_2600();
                fun_28a0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                goto addr_4ec2_430;
            }
            fun_2910();
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            goto addr_51ab_432;
            addr_4fab_409:
            *reinterpret_cast<unsigned char*>(&v141) = *reinterpret_cast<unsigned char*>(&rbp4);
            if (eax362 < 0) 
                break;
            if (0) 
                goto addr_4bea_86;
            rsi318 = v100;
            rdi367 = r13_22;
            *reinterpret_cast<int32_t*>(&rdi367 + 4) = 0;
            rax368 = safe_read(rdi367, rsi318, r12_292, 16);
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            v56 = rax368;
            if (!rax368) 
                goto addr_51ab_432;
            v29 = reinterpret_cast<void**>(0);
            if (!(rax368 + 1)) {
                rax369 = fun_2530();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                if (*reinterpret_cast<void***>(rax369) == 22) {
                    addr_51ab_432:
                    if (*reinterpret_cast<int32_t*>(&v97)) {
                        r12_292 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_292) + reinterpret_cast<unsigned char>(r12_292));
                        *reinterpret_cast<int32_t*>(&v97) = *reinterpret_cast<int32_t*>(&v97) - 1;
                        rsi318 = r12_292;
                        rax370 = xrealloc(v100, rsi318);
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                        v56 = reinterpret_cast<void**>(0);
                        v100 = rax370;
                        v29 = reinterpret_cast<void**>(0);
                        continue;
                    }
                } else {
                    rax371 = fun_2600();
                    r12_292 = rax371;
                    fun_2530();
                    fun_28a0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_5030_439;
                }
            }
            if (*reinterpret_cast<int32_t*>(&rax365)) {
                addr_4d9b_423:
                v108 = r12_292;
                rdi372 = r15_3 + 16;
                rbp4 = v58;
                *reinterpret_cast<int32_t*>(&rbx373) = 0;
                *reinterpret_cast<int32_t*>(&rbx373 + 4) = 0;
                v109 = r15_3;
                r12_374 = v46;
                r15_3 = r14_25;
                r14d375 = r13_22;
                r13d376 = rdx62;
            } else {
                addr_4e5c_428:
                rsi318 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x90);
                rax377 = hash_lookup(r14_25, rsi318);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                rbp4 = rax377;
                if (!rax377) {
                    continue;
                }
            }
            do {
                if (*reinterpret_cast<void***>(rbp4 + 72) != r13d376) 
                    continue;
                rsi318 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbp4 + 80)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4)));
                eax378 = fun_2770(rdi372, rsi318);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                rdi372 = rdi372;
                rcx55 = rcx55;
                *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                if (!eax378) 
                    break;
                ++rbx373;
                rbp4 = rbp4 + 96;
            } while (r12_374 != rbx373);
            goto addr_4f9b_445;
            r13_22 = r14d375;
            *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
            r12_292 = v108;
            r14_25 = r15_3;
            r15_3 = v109;
            rbp4 = v58 + reinterpret_cast<unsigned char>(rbx373) * 96;
            *reinterpret_cast<unsigned char*>(&rcx55 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx55 + 1) & 2);
            if (!*reinterpret_cast<unsigned char*>(&rcx55 + 1)) {
                addr_5030_439:
                rsi379 = *reinterpret_cast<void***>(rbp4);
                *reinterpret_cast<void***>(&rdi380) = r13_22;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi380) + 4) = 0;
                eax381 = fun_2820(rdi380, rsi379);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                rbx31 = eax381;
                if (reinterpret_cast<signed char>(eax381) < reinterpret_cast<signed char>(0)) {
                    rax382 = fun_2530();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    r8_32 = rax382;
                    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax382)) & 0xffffffef) == 12) 
                        goto addr_4c0a_377;
                    rsi383 = *reinterpret_cast<void***>(rbp4);
                    v109 = r8_32;
                    rax384 = quotearg_style(4, rsi383, 4, rsi383);
                    v108 = rax384;
                    fun_2600();
                    r8_32 = v109;
                    fun_28a0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
                }
            } else {
                addr_4e20_449:
                zf385 = follow_mode == 1;
                if (zf385) {
                    recheck(rbp4, 0);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    goto addr_4e2d_451;
                }
            }
            rsi318 = *reinterpret_cast<void***>(rbp4 + 68);
            *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
            if (rbx31 != rsi318) {
                while (1) {
                    if (reinterpret_cast<signed char>(rsi318) >= reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(&rdi386) = r13_22;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi386) + 4) = 0;
                        fun_2970(rdi386);
                        rsi318 = rbp4;
                        hash_remove(r14_25, rsi318);
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                    }
                    addr_50af_455:
                    *reinterpret_cast<void***>(rbp4 + 68) = rbx31;
                    if (!(rbx31 + 1)) 
                        goto addr_4d48_398;
                    rsi387 = rbp4;
                    rax388 = hash_remove(r14_25, rsi387);
                    rsp389 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    rbx31 = rax388;
                    if (rax388 && rbp4 != rax388) {
                        zf390 = follow_mode == 1;
                        if (zf390) {
                            rsi387 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rsi387 + 4) = 0;
                            recheck(rax388, 0);
                            rsp389 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp389) - 8 + 8);
                        }
                        *reinterpret_cast<void***>(rbx31 + 68) = reinterpret_cast<void**>(0xffffffff);
                        rdi391 = *reinterpret_cast<void***>(rbx31);
                        rax392 = pretty_name_isra_0(rdi391, rsi387);
                        *reinterpret_cast<void***>(&rdi393) = *reinterpret_cast<void***>(rbx31 + 56);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi393) + 4) = 0;
                        close_fd(rdi393, rax392, rdi393, rax392);
                        rsp389 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp389) - 8 + 8 - 8 + 8);
                    }
                    rsi318 = rbp4;
                    rax394 = hash_insert(r14_25);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp389) - 8 + 8);
                    if (rax394) 
                        goto addr_4e20_449;
                    addr_510e_355:
                    xalloc_die();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                }
            } else {
                if (reinterpret_cast<signed char>(rsi318) >= reinterpret_cast<signed char>(0)) 
                    goto addr_4e20_449; else 
                    goto addr_50af_455;
            }
            addr_4e2d_451:
            eax395 = *reinterpret_cast<void***>(r15_3 + 4);
            if (reinterpret_cast<unsigned char>(eax395) & reinterpret_cast<uint32_t>("flush")) {
                if (*reinterpret_cast<unsigned char*>(&eax395 + 1) & 4) {
                    *reinterpret_cast<void***>(&rdi396) = r13_22;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi396) + 4) = 0;
                    fun_2970(rdi396, rdi396);
                    hash_remove(r14_25, rbp4);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                }
                rsi318 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
                recheck(rbp4, 0);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                continue;
            }
            addr_4ec2_430:
            rsi318 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x88);
            check_fspec(rbp4, rsi318);
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            continue;
            addr_4f9b_445:
            r13_22 = r14d375;
            *reinterpret_cast<int32_t*>(&r13_22 + 4) = 0;
            r12_292 = v108;
            r14_25 = r15_3;
        }
        fun_2600();
        fun_2530();
        fun_28a0();
        rsp331 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
        addr_5222_372:
        rax397 = fun_2530();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp331) - 8 + 8);
        if (*reinterpret_cast<void***>(rax397) == 28) {
            addr_4c0a_377:
        } else {
            rsi398 = *reinterpret_cast<void***>(rbp4);
            rax399 = quotearg_style(4, rsi398, 4, rsi398);
            rax400 = fun_2600();
            rsi318 = *reinterpret_cast<void***>(rax397);
            *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
            rcx55 = rax399;
            rdx62 = rax400;
            fun_28a0();
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_4c2b_364;
        }
        addr_4c16_467:
        rax401 = fun_2600();
        rsi318 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi318 + 4) = 0;
        rdx62 = rax401;
        fun_28a0();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
        goto addr_4c2b_364;
        addr_5169_426:
        goto addr_4c16_467;
        addr_3ef0_360:
        rcx55 = v110;
        *reinterpret_cast<signed char*>(&v107) = 0;
        eax402 = reinterpret_cast<unsigned char>(rbp4) & 1;
        *reinterpret_cast<signed char*>(&v60) = *reinterpret_cast<signed char*>(&eax402);
        v100 = v46 - 1;
        v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(rcx55) + 52);
        goto addr_3f20_75;
        addr_44ad_324:
        fun_2600();
        fun_2530();
        fun_28a0();
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_44d9_109;
        addr_457e_131:
        rdx62 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
        rsi59 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rsi59 + 4) = 0;
        eax403 = fun_2890(reinterpret_cast<int64_t>(rsp51) + 0x90);
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
        if (eax403 >= 0) {
            addr_4468_77:
            if (0) 
                goto addr_4bea_86; else 
                goto addr_4476_132;
        } else {
            goto addr_4476_132;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_52f3() {
    __asm__("cli ");
    __libc_start_main(0x2a10, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x13008;

void fun_24e0(int64_t rdi);

int64_t fun_5393() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_24e0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_53d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s12 {
    signed char[68] pad68;
    int32_t f44;
};

uint64_t fun_53e3(struct s12* rdi, uint64_t rsi) {
    __asm__("cli ");
    return static_cast<int64_t>(rdi->f44) % rsi;
}

struct s13 {
    signed char[68] pad68;
    int32_t f44;
};

struct s14 {
    signed char[68] pad68;
    int32_t f44;
};

int64_t fun_5403(struct s13* rdi, struct s14* rsi) {
    int64_t rax3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = rsi->f44;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f44 == *reinterpret_cast<int32_t*>(&rax3));
    return rax3;
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2740(void** rdi, struct s0* rsi, int64_t rdx, void** rcx);

int32_t fun_2540(void** rdi, void** rsi, void** rdx, ...);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_2930(struct s0* rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void*** a8, int64_t a9, int64_t a10, int64_t a11, void*** a12);

void fun_6e43(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r8_7;
    void** r9_8;
    void** rax9;
    void** r8_10;
    void** r9_11;
    struct s0* r12_12;
    void** rax13;
    struct s0* r12_14;
    void** rax15;
    struct s0* r12_16;
    void** rax17;
    struct s0* r12_18;
    void** rax19;
    void** rax20;
    void** r8_21;
    void** r9_22;
    struct s0* r12_23;
    void** rax24;
    struct s0* r12_25;
    void** rax26;
    struct s0* r12_27;
    void** rax28;
    struct s0* r12_29;
    void** rax30;
    struct s0* r12_31;
    void** rax32;
    struct s0* r12_33;
    void** rax34;
    struct s0* r12_35;
    void** rax36;
    int32_t eax37;
    void** r13_38;
    void** rax39;
    void** r8_40;
    void** r9_41;
    void** rax42;
    int32_t eax43;
    void** rax44;
    void** r8_45;
    void** r9_46;
    void** rax47;
    void** r8_48;
    void** r9_49;
    void** rax50;
    int32_t eax51;
    void** rax52;
    void** r8_53;
    void** r9_54;
    struct s0* r15_55;
    void** rax56;
    void** rax57;
    void** r8_58;
    void** r9_59;
    void** rax60;
    struct s0* rdi61;
    void** r8_62;
    void** r9_63;
    int64_t v64;
    void*** v65;
    int64_t v66;
    int64_t v67;
    int64_t v68;
    void*** v69;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2600();
            fun_2880(1, rax5, r12_2, rcx6, r8_7, r9_8, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            rax9 = fun_2600();
            fun_2880(1, rax9, 10, rcx6, r8_10, r9_11, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_12 = stdout;
            rax13 = fun_2600();
            fun_2740(rax13, r12_12, 5, rcx6);
            r12_14 = stdout;
            rax15 = fun_2600();
            fun_2740(rax15, r12_14, 5, rcx6);
            r12_16 = stdout;
            rax17 = fun_2600();
            fun_2740(rax17, r12_16, 5, rcx6);
            r12_18 = stdout;
            rax19 = fun_2600();
            fun_2740(rax19, r12_18, 5, rcx6);
            rax20 = fun_2600();
            fun_2880(1, rax20, 10, 5, r8_21, r9_22, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_23 = stdout;
            rax24 = fun_2600();
            fun_2740(rax24, r12_23, 5, 5);
            r12_25 = stdout;
            rax26 = fun_2600();
            fun_2740(rax26, r12_25, 5, 5);
            r12_27 = stdout;
            rax28 = fun_2600();
            fun_2740(rax28, r12_27, 5, 5);
            r12_29 = stdout;
            rax30 = fun_2600();
            fun_2740(rax30, r12_29, 5, 5);
            r12_31 = stdout;
            rax32 = fun_2600();
            fun_2740(rax32, r12_31, 5, 5);
            r12_33 = stdout;
            rax34 = fun_2600();
            fun_2740(rax34, r12_33, 5, 5);
            r12_35 = stdout;
            rax36 = fun_2600();
            fun_2740(rax36, r12_35, 5, 5);
            do {
                if (1) 
                    break;
                eax37 = fun_2770("tail", 0, "tail", 0);
            } while (eax37);
            r13_38 = v4;
            if (!r13_38) {
                rax39 = fun_2600();
                fun_2880(1, rax39, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_40, r9_41, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax42 = fun_2870(5);
                if (!rax42 || (eax43 = fun_2540(rax42, "en_", 3, rax42, "en_", 3), !eax43)) {
                    rax44 = fun_2600();
                    r13_38 = reinterpret_cast<void**>("tail");
                    fun_2880(1, rax44, "https://www.gnu.org/software/coreutils/", "tail", r8_45, r9_46, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_38 = reinterpret_cast<void**>("tail");
                    goto addr_72c8_9;
                }
            } else {
                rax47 = fun_2600();
                fun_2880(1, rax47, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_48, r9_49, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax50 = fun_2870(5);
                if (!rax50 || (eax51 = fun_2540(rax50, "en_", 3, rax50, "en_", 3), !eax51)) {
                    addr_71ce_11:
                    rax52 = fun_2600();
                    fun_2880(1, rax52, "https://www.gnu.org/software/coreutils/", "tail", r8_53, r9_54, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_38 == "tail")) {
                        r12_2 = reinterpret_cast<void**>(0xea41);
                    }
                } else {
                    addr_72c8_9:
                    r15_55 = stdout;
                    rax56 = fun_2600();
                    fun_2740(rax56, r15_55, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_71ce_11;
                }
            }
            rax57 = fun_2600();
            rcx6 = r12_2;
            fun_2880(1, rax57, r13_38, rcx6, r8_58, r9_59, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_6e9e_14:
            fun_2910();
        }
    } else {
        rax60 = fun_2600();
        rdi61 = stderr;
        rcx6 = r12_2;
        fun_2930(rdi61, 1, rax60, rcx6, r8_62, r9_63, v64, v65, v66, v67, v68, v69);
        goto addr_6e9e_14;
    }
}

void fun_7303() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_2730(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_7313(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2620(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2540(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_7393_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_73e0_6; else 
                    continue;
            } else {
                rax18 = fun_2620(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_7410_8;
                if (v16 == -1) 
                    goto addr_73ce_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_7393_5;
            } else {
                eax19 = fun_2730(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_7393_5;
            }
            addr_73ce_10:
            v16 = rbx15;
            goto addr_7393_5;
        }
    }
    addr_73f5_16:
    return v12;
    addr_73e0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_73f5_16;
    addr_7410_8:
    v12 = rbx15;
    goto addr_73f5_16;
}

int64_t fun_7423(void** rdi, void*** rsi) {
    void** r12_3;
    void** rdi4;
    void*** rbp5;
    int64_t rbx6;
    int32_t eax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_7468_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            eax7 = fun_2770(rdi4, r12_3);
            if (!eax7) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6 * 8];
        } while (rdi4);
        goto addr_7468_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_7483(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2600();
    } else {
        fun_2600();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_28a0;
}

void fun_7513(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void*** v8;
    void*** r12_9;
    void** r12_10;
    int64_t v11;
    int64_t rbp12;
    void** rbp13;
    int64_t v14;
    int64_t rbx15;
    struct s0* r14_16;
    void*** v17;
    void** rax18;
    void** r15_19;
    int64_t rbx20;
    uint32_t eax21;
    void** rax22;
    struct s0* rdi23;
    int64_t v24;
    int64_t v25;
    void** rax26;
    struct s0* rdi27;
    int64_t v28;
    int64_t v29;
    struct s0* rdi30;
    signed char* rax31;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    v11 = rbp12;
    rbp13 = rsi;
    v14 = rbx15;
    r14_16 = stderr;
    v17 = rdi;
    rax18 = fun_2600();
    fun_2740(rax18, r14_16, 5, rcx);
    r15_19 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
    if (r15_19) {
        do {
            if (!rbx20 || (eax21 = fun_2730(r13_7, rbp13, r12_10, r13_7, rbp13, r12_10), !!eax21)) {
                r13_7 = rbp13;
                rax22 = quote(r15_19, r15_19);
                rdi23 = stderr;
                fun_2930(rdi23, 1, "\n  - %s", rax22, r8, r9, v24, v17, v25, v14, v11, v8);
            } else {
                rax26 = quote(r15_19, r15_19);
                rdi27 = stderr;
                fun_2930(rdi27, 1, ", %s", rax26, r8, r9, v28, v17, v29, v14, v11, v8);
            }
            ++rbx20;
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r12_10));
            r15_19 = v17[rbx20 * 8];
        } while (r15_19);
    }
    rdi30 = stderr;
    rax31 = rdi30->f28;
    if (reinterpret_cast<uint64_t>(rax31) < reinterpret_cast<uint64_t>(rdi30->f30)) {
        rdi30->f28 = rax31 + 1;
        *rax31 = 10;
        return;
    }
}

int64_t argmatch(void** rdi, void*** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void*** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_7643(int64_t rdi, void** rsi, void*** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void*** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_7687_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_76fe_4;
        } else {
            addr_76fe_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_2770(rdi15, r14_9);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16 * 8];
        } while (rdi15);
        goto addr_7680_8;
    } else {
        goto addr_7680_8;
    }
    return rbx16;
    addr_7680_8:
    rax14 = -1;
    goto addr_7687_3;
}

struct s15 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_7713(void** rdi, struct s15* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2730(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

struct s16 {
    unsigned char f0;
    unsigned char f1;
};

struct s16* fun_7773(struct s16* rdi) {
    uint32_t edx2;
    struct s16* rax3;
    struct s16* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s16*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s16*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s16*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_77d3(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2620(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

void fun_2590();

void c_strtod();

void fun_7803(int32_t* rdi, uint64_t* rsi) {
    void** rax3;
    signed char* v4;
    int32_t v5;
    void** rax6;
    void** r13d7;
    int32_t v8;
    uint64_t v9;
    uint64_t v10;
    uint64_t v11;
    void* rax12;

    __asm__("cli ");
    rax3 = g28;
    fun_2590();
    __asm__("movapd xmm1, xmm0");
    if (*v4) {
        *rdi = v5;
        rax6 = fun_2530();
        r13d7 = *reinterpret_cast<void***>(rax6);
        c_strtod();
        *rdi = v8;
        if (v9 < v10) 
            goto addr_78a0_9;
    } else {
        addr_783c_10:
        if (rsi) {
            *rsi = v11;
            goto addr_7849_12;
        }
    }
    *reinterpret_cast<void***>(rax6) = r13d7;
    goto addr_783c_10;
    addr_78a0_9:
    v11 = v10;
    __asm__("movapd xmm1, xmm0");
    goto addr_783c_10;
    addr_7849_12:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2630();
    } else {
        __asm__("movapd xmm0, xmm1");
        return;
    }
}

int64_t file_name = 0;

void fun_78c3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_78d3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2550(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_78e3() {
    struct s0* rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    struct s0* rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2530(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2600();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_7973_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_28a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2550(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_7973_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_28a0();
    }
}

void fun_7993(void** rdi, void** rsi) {
    void* rbp3;
    void** rbx4;
    void* rax5;
    void* rax6;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rbx4 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp3) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax5 = last_component(rdi, rsi);
    rax6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<unsigned char>(rbx4));
    while (reinterpret_cast<uint64_t>(rax6) > reinterpret_cast<uint64_t>(rbp3) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<uint64_t>(rax6) + 0xffffffffffffffff) == 47) {
        rax6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax6) + 0xffffffffffffffff);
    }
    return;
}

signed char* fun_79d3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rbx8;
    void* rax9;
    void** r12_10;
    void* rax11;
    uint32_t ebx12;
    void** rax13;
    signed char* r8_14;
    signed char* rax15;
    void** rax16;
    signed char* rax17;

    __asm__("cli ");
    rbp7 = rdi;
    *reinterpret_cast<int32_t*>(&rbx8) = 0;
    *reinterpret_cast<int32_t*>(&rbx8 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx8) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax9 = last_component(rdi, rsi);
    r12_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax9) - reinterpret_cast<unsigned char>(rbp7));
    while (reinterpret_cast<unsigned char>(rbx8) < reinterpret_cast<unsigned char>(r12_10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r12_10) + 0xffffffffffffffff) != 47) 
            goto addr_7a50_4;
        --r12_10;
    }
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_10) ^ 1);
    ebx12 = *reinterpret_cast<uint32_t*>(&rax11) & 1;
    rax13 = fun_2800(reinterpret_cast<unsigned char>(r12_10) + reinterpret_cast<uint64_t>(rax11) + 1, rsi, rdx);
    if (!rax13) {
        addr_7a7d_7:
        *reinterpret_cast<int32_t*>(&r8_14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_14) + 4) = 0;
    } else {
        rax15 = fun_27b0(rax13, rbp7, r12_10, rcx, r8, r9);
        r8_14 = rax15;
        if (!*reinterpret_cast<signed char*>(&ebx12)) {
            *reinterpret_cast<int32_t*>(&r12_10) = 1;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            goto addr_7a3e_10;
        } else {
            *rax15 = 46;
            *reinterpret_cast<int32_t*>(&r12_10) = 1;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            goto addr_7a3e_10;
        }
    }
    addr_7a43_12:
    return r8_14;
    addr_7a3e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_14) + reinterpret_cast<unsigned char>(r12_10)) = 0;
    goto addr_7a43_12;
    addr_7a50_4:
    rax16 = fun_2800(r12_10 + 1, rsi, rdx);
    if (!rax16) 
        goto addr_7a7d_7;
    rax17 = fun_27b0(rax16, rbp7, r12_10, rcx, rax16, r9);
    r8_14 = rax17;
    goto addr_7a3e_10;
}

uint32_t fun_25a0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_7a93(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_25a0(rdi);
        r12d10 = eax9;
        goto addr_7b94_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_25a0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_7b94_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2630();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_7c49_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_25a0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_25a0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2530();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_26f0();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_7b94_3;
                }
            }
        } else {
            eax22 = fun_25a0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2530(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_7b94_3;
            } else {
                eax25 = fun_25a0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_7b94_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_7c49_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_7af9_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_7afd_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_7afd_18:
            if (0) {
            }
        } else {
            addr_7b45_23:
            eax28 = fun_25a0(rdi);
            r12d10 = eax28;
            goto addr_7b94_3;
        }
        eax29 = fun_25a0(rdi);
        r12d10 = eax29;
        goto addr_7b94_3;
    }
    if (0) {
    }
    eax30 = fun_25a0(rdi);
    r12d10 = eax30;
    goto addr_7b94_3;
    addr_7af9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_7afd_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_7b45_23;
        goto addr_7afd_18;
    }
}

int32_t fun_28c0();

void fd_safer(int64_t rdi);

void fun_7d03() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_28c0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2630();
    } else {
        return;
    }
}

uint64_t fun_7d83(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_7da3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s17 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_8203(struct s17* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s18 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_8213(struct s18* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s19 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_8223(struct s19* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s22 {
    signed char[8] pad8;
    struct s22* f8;
};

struct s21 {
    int64_t f0;
    struct s22* f8;
};

struct s20 {
    struct s21* f0;
    struct s21* f8;
};

uint64_t fun_8233(struct s20* rdi) {
    struct s21* rcx2;
    struct s21* rsi3;
    uint64_t r8_4;
    struct s22* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s25 {
    signed char[8] pad8;
    struct s25* f8;
};

struct s24 {
    int64_t f0;
    struct s25* f8;
};

struct s23 {
    struct s24* f0;
    struct s24* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_8293(struct s23* rdi) {
    struct s24* rcx2;
    struct s24* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s25* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s28 {
    signed char[8] pad8;
    struct s28* f8;
};

struct s27 {
    int64_t f0;
    struct s28* f8;
};

struct s26 {
    struct s27* f0;
    struct s27* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_8303(struct s26* rdi, struct s0* rsi) {
    void*** v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    struct s0* rbp11;
    void*** v12;
    void*** rbx13;
    struct s27* rcx14;
    struct s27* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s28* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<void***>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_2930(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_2930(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x618c]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_83ba_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_8439_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2930(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_2930;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x620b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_8439_14; else 
            goto addr_83ba_13;
    }
}

struct s29 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s30 {
    int64_t f0;
    struct s30* f8;
};

int64_t fun_8463(struct s29* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s29* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s30* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x29aa;
    rbx7 = reinterpret_cast<struct s30*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_84c8_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_84bb_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_84bb_7;
    }
    addr_84cb_10:
    return r12_3;
    addr_84c8_5:
    r12_3 = rbx7->f0;
    goto addr_84cb_10;
    addr_84bb_7:
    return 0;
}

struct s31 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_84e3(struct s31* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x29af;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_851f_7;
    return *rax2;
    addr_851f_7:
    goto 0x29af;
}

struct s33 {
    int64_t f0;
    struct s33* f8;
};

struct s32 {
    int64_t* f0;
    struct s33* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_8533(struct s32* rdi, int64_t rsi) {
    struct s32* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s33* rax7;
    struct s33* rdx8;
    struct s33* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x29b5;
    rax7 = reinterpret_cast<struct s33*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_857e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_857e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_859c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_859c_10:
    return r8_10;
}

struct s35 {
    int64_t f0;
    struct s35* f8;
};

struct s34 {
    struct s35* f0;
    struct s35* f8;
};

void fun_85c3(struct s34* rdi, int64_t rsi, uint64_t rdx) {
    struct s35* r9_4;
    uint64_t rax5;
    struct s35* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_8602_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_8602_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s37 {
    int64_t f0;
    struct s37* f8;
};

struct s36 {
    struct s37* f0;
    struct s37* f8;
};

int64_t fun_8613(struct s36* rdi, int64_t rsi, int64_t rdx) {
    struct s37* r14_4;
    int64_t r12_5;
    struct s36* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s37* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_863f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_8681_10;
            }
            addr_863f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_8649_11:
    return r12_5;
    addr_8681_10:
    goto addr_8649_11;
}

uint64_t fun_8693(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s38 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_86d3(struct s38* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_2760(void** rdi, void** rsi);

void** fun_8703(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x7d80);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x7da0);
    }
    rax9 = fun_2800(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xe590);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_2760(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2510(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int64_t*>(r12_10 + 32) = 0;
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s41 {
    int64_t f0;
    struct s41* f8;
};

struct s40 {
    int64_t f0;
    struct s41* f8;
};

struct s39 {
    struct s40* f0;
    struct s40* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s41* f48;
};

void fun_8803(struct s39* rdi) {
    struct s39* rbp2;
    struct s40* r12_3;
    struct s41* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s41* rax7;
    struct s41* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s42 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_88b3(struct s42* rdi, void** rsi) {
    struct s42* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_8923_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2510(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_8923_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2510(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2510(rdi13, rsi);
    goto fun_2510;
}

int64_t fun_89a3(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_8ae0_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2760(rax6, 16);
        if (!rax8) {
            addr_8ae0_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2510(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x29ba;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x29ba;
                fun_2510(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_2630();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s43 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_8b33(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s43* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x29bf;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_8c4e_5; else 
                goto addr_8bbf_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_8bbf_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_8c4e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x5911]");
            if (!cf14) 
                goto addr_8ca5_12;
            __asm__("comiss xmm4, [rip+0x58c1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x5880]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_8ca5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x29bf;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<int64_t*>(rdi + 32) = *reinterpret_cast<int64_t*>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2800(16, rsi, rdx6);
                if (!rax19) {
                    addr_8ca5_12:
                    r8d18 = -1;
                } else {
                    goto addr_8c02_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_8c02_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_8b7e_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2630();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_8c02_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<int64_t*>(rdi + 32) = *reinterpret_cast<int64_t*>(rdi + 32) + 1;
    goto addr_8b7e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_8d53() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2630();
    } else {
        return rax3;
    }
}

void** fun_8db3(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<int64_t*>(rbx3 + 32) = *reinterpret_cast<int64_t*>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_8e40_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_8ef6_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x572e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x5698]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2510(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_8ef6_5; else 
                goto addr_8e40_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2630();
    } else {
        return r12_7;
    }
}

void fun_8f43() {
    __asm__("cli ");
    goto hash_remove;
}

struct s44 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_8f53(uint64_t rdi, struct s44* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

signed char* fun_24f0(int64_t rdi);

int64_t fun_27a0(signed char* rdi, signed char** rsi, int64_t rdx);

int64_t fun_8ff3() {
    int64_t r12_1;
    void** rax2;
    signed char* rax3;
    int64_t rax4;
    signed char* v5;
    void* rax6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_1) = 0x31069;
    rax2 = g28;
    rax3 = fun_24f0("_POSIX2_VERSION");
    if (rax3 && (*rax3 && (rax4 = fun_27a0(rax3, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8, 10), !*v5))) {
        if (rax4 < 0xffffffff80000000) {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x80000000;
        } else {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x7fffffff;
            if (rax4 <= 0x7fffffff) {
                r12_1 = rax4;
            }
        }
    }
    rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax6) {
        fun_2630();
    } else {
        *reinterpret_cast<int32_t*>(&rax7) = *reinterpret_cast<int32_t*>(&r12_1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

void fun_2920(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s45 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s45* fun_26a0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_9083(void** rdi) {
    struct s0* rcx2;
    void** rbx3;
    struct s45* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2920("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2520("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_26a0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2540(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_a823(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2530();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x13240;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_a863(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13240);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_a883(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13240);
    }
    *rdi = esi;
    return 0x13240;
}

int64_t fun_a8a3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x13240);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s46 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_a8e3(struct s46* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s46*>(0x13240);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s47 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s47* fun_a903(struct s47* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s47*>(0x13240);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x29ce;
    if (!rdx) 
        goto 0x29ce;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x13240;
}

struct s48 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_a943(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s48* r8) {
    struct s48* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s48*>(0x13240);
    }
    rax7 = fun_2530();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xa976);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s49 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_a9c3(int64_t rdi, int64_t rsi, void*** rdx, struct s49* rcx) {
    struct s49* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s49*>(0x13240);
    }
    rax6 = fun_2530();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xa9f1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xaa4c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_aab3() {
    __asm__("cli ");
}

void** g130b8 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_aac3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2510(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x13140) {
        fun_2510(rdi8, rsi9);
        g130b8 = reinterpret_cast<void**>(0x13140);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x130b0) {
        fun_2510(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x130b0);
    }
    nslots = 1;
    return;
}

void fun_ab63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_ab83() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_ab93(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_abb3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_abd3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29d4;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2630();
    } else {
        return rax6;
    }
}

void** fun_ac63(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x29d9;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2630();
    } else {
        return rax7;
    }
}

void** fun_acf3(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s5* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x29de;
    rcx4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2630();
    } else {
        return rax5;
    }
}

void** fun_ad83(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x29e3;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2630();
    } else {
        return rax6;
    }
}

void** fun_ae13(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s5* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8420]");
    __asm__("movdqa xmm1, [rip+0x8428]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8411]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2630();
    } else {
        return rax10;
    }
}

void** fun_aeb3(int64_t rdi, uint32_t esi) {
    struct s5* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8380]");
    __asm__("movdqa xmm1, [rip+0x8388]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8371]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2630();
    } else {
        return rax9;
    }
}

void** fun_af53(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x82e0]");
    __asm__("movdqa xmm1, [rip+0x82e8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x82c9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2630();
    } else {
        return rax3;
    }
}

void** fun_afe3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8250]");
    __asm__("movdqa xmm1, [rip+0x8258]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8246]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2630();
    } else {
        return rax4;
    }
}

void** fun_b073(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29e8;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2630();
    } else {
        return rax6;
    }
}

void** fun_b113(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x811a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8112]");
    __asm__("movdqa xmm2, [rip+0x811a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29ed;
    if (!rdx) 
        goto 0x29ed;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2630();
    } else {
        return rax7;
    }
}

void** fun_b1b3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s5* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x807a]");
    __asm__("movdqa xmm1, [rip+0x8082]");
    __asm__("movdqa xmm2, [rip+0x808a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29f2;
    if (!rdx) 
        goto 0x29f2;
    rcx7 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2630();
    } else {
        return rax9;
    }
}

void** fun_b263(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7fca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7fc2]");
    __asm__("movdqa xmm2, [rip+0x7fca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29f7;
    if (!rsi) 
        goto 0x29f7;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2630();
    } else {
        return rax6;
    }
}

void** fun_b303(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7f2a]");
    __asm__("movdqa xmm1, [rip+0x7f32]");
    __asm__("movdqa xmm2, [rip+0x7f3a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29fc;
    if (!rsi) 
        goto 0x29fc;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2630();
    } else {
        return rax7;
    }
}

void fun_b3a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b3b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b3d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b3f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_2710(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_b413(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2710(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_2530();
        if (*reinterpret_cast<void***>(rax9) == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 22)) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int32_t dup_safer();

int64_t fun_b483(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_2530();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_26f0();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s50 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void*** f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    void*** f40;
};

void fun_2780(int64_t rdi, struct s0* rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_b4e3(struct s0* rdi, void** rsi, void** rdx, void** rcx, struct s50* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void*** v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;
    void*** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    void*** v19;
    void** rax20;
    int64_t v21;
    void*** v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    void*** v26;
    void** rax27;
    int64_t v28;
    void*** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    void*** v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    void*** rcx37;
    int64_t r15_38;
    void*** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2930(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2930(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2600();
    fun_2930(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2780(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2600();
    fun_2930(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2780(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2600();
        fun_2930(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xece8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xece8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_b953() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s51 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_b973(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s51* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s51* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2630();
    } else {
        return;
    }
}

void fun_ba13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_bab6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_bac0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2630();
    } else {
        return;
    }
    addr_bab6_5:
    goto addr_bac0_7;
}

void fun_baf3() {
    struct s0* rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    int64_t v17;
    int64_t v18;
    void** v19;
    void** v20;
    void** v21;
    void** v22;
    void** rax23;
    void** r8_24;
    void** r9_25;
    void** v26;
    void** v27;
    void** v28;
    void** v29;
    void** v30;
    void** v31;
    void** v32;
    int64_t v33;
    int64_t v34;
    void** v35;
    void** v36;
    void** v37;
    void** v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2780(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2600();
    fun_2880(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2600();
    fun_2880(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2600();
    goto fun_2880;
}

int64_t fun_2580();

void fun_bb93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2580();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_bbd3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bbf3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bc13(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2860();

void fun_bc33(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2860();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_bc63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2860();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bc93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2580();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_bcd3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2580();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bd13(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2580();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bd43(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2580();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bd93(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2580();
            if (rax5) 
                break;
            addr_bddd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_bddd_5;
        rax8 = fun_2580();
        if (rax8) 
            goto addr_bdc6_9;
        if (rbx4) 
            goto addr_bddd_5;
        addr_bdc6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_be23(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2580();
            if (rax8) 
                break;
            addr_be6a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_be6a_5;
        rax11 = fun_2580();
        if (rax11) 
            goto addr_be52_9;
        if (!rbx6) 
            goto addr_be52_9;
        if (r12_4) 
            goto addr_be6a_5;
        addr_be52_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_beb3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_bf5d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_bf70_10:
                *r12_8 = 0;
            }
            addr_bf10_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_bf36_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_bf84_14;
            if (rcx10 <= rsi9) 
                goto addr_bf2d_16;
            if (rsi9 >= 0) 
                goto addr_bf84_14;
            addr_bf2d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_bf84_14;
            addr_bf36_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2860();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_bf84_14;
            if (!rbp13) 
                break;
            addr_bf84_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_bf5d_9;
        } else {
            if (!r13_6) 
                goto addr_bf70_10;
            goto addr_bf10_11;
        }
    }
}

void fun_bfb3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2760(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_bfe3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2760(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c013(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2760(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c033(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2760(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c053(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_27b0;
    }
}

void fun_c093(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_27b0;
    }
}

void fun_c0d3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2800(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_27b0;
    }
}

void fun_c113(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_2620(rdi);
    rax5 = fun_2800(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_27b0;
    }
}

void fun_c153() {
    void** rdi1;

    __asm__("cli ");
    fun_2600();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_28a0();
    fun_2520(rdi1);
}

uint64_t fun_c193(void** rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void** rax5;
    uint32_t eax6;
    void** rax7;
    void** r12_8;
    uint64_t v9;
    void* rax10;
    void** rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax();
    if (eax6) {
        rax7 = fun_2530();
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_c230_3:
            *reinterpret_cast<void***>(r12_8) = reinterpret_cast<void**>(75);
        } else {
            if (eax6 == 3) {
                *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2630();
            } else {
                return v9;
            }
        }
        rax11 = fun_2530();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_c230_3;
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(34);
    }
    quote(rdi);
    if (*reinterpret_cast<void***>(r12_8) != 22) 
        goto addr_c24c_13;
    while (1) {
        addr_c24c_13:
        if (!1) {
        }
        fun_28a0();
    }
}

void xnumtoumax();

void fun_c2a3() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void fun_27f0();

int64_t dtotimespec();

int64_t rpl_nanosleep(void* rdi, void* rsi);

int64_t fun_c2d3() {
    void** rax1;
    void** v2;
    void** rax3;
    void* rsp4;
    void** rbx5;
    void* rbp6;
    int64_t rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movd rbp, xmm0");
    rax1 = g28;
    v2 = rax1;
    rax3 = fun_2530();
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 - 8 + 8);
    __asm__("movd xmm1, rbp");
    __asm__("comisd xmm1, [rip+0x2a5f]");
    rbx5 = rax3;
    if (1) {
        do {
            fun_27f0();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx5) == 4)) 
                goto addr_c306_3;
            fun_27f0();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        } while (*reinterpret_cast<void***>(rbx5) == 4);
    } else {
        addr_c306_3:
        __asm__("movd xmm0, rbp");
        rbp6 = rsp4;
        dtotimespec();
        goto addr_c328_5;
    }
    goto addr_c306_3;
    do {
        addr_c328_5:
        *reinterpret_cast<void***>(rbx5) = reinterpret_cast<void**>(0);
        rax7 = rpl_nanosleep(rbp6, rbp6);
        if (!*reinterpret_cast<int32_t*>(&rax7)) 
            break;
    } while (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx5)) & 0xfffffffb));
    goto addr_c370_8;
    addr_c33d_9:
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v2) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2630();
    } else {
        return rax7;
    }
    addr_c370_8:
    *reinterpret_cast<int32_t*>(&rax7) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    goto addr_c33d_9;
}

int64_t fun_c383(signed char* rdi, signed char** rsi, int64_t rdx, int64_t rcx) {
    void** rax5;
    void** rax6;
    signed char* v7;
    int32_t r8d8;
    int1_t zf9;
    int1_t zf10;
    signed char v11;
    void* rax12;
    int64_t rax13;

    __asm__("cli ");
    rax5 = g28;
    rax6 = fun_2530();
    *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(0);
    rcx();
    if (v7 == rdi) {
        r8d8 = 0;
    } else {
        zf9 = rsi == 0;
        if (zf9) {
            r8d8 = 0;
            zf10 = *v7 == 0;
            if (!zf10) 
                goto addr_c3f5_5;
            __asm__("ucomisd xmm0, [rip+0x2938]");
            if (__intrinsic()) 
                goto addr_c3e3_7; else 
                goto addr_c432_8;
        } else {
            __asm__("ucomisd xmm0, [rip+0x298f]");
            if (__intrinsic()) 
                goto addr_c3e3_7;
            r8d8 = 1;
            if (zf9) 
                goto addr_c3f1_11; else 
                goto addr_c3e3_7;
        }
    }
    addr_c3ec_12:
    if (!rsi) {
        addr_c3f5_5:
    } else {
        addr_c3f1_11:
        *rsi = v7;
        goto addr_c3f5_5;
    }
    *rdi = v11;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2630();
    } else {
        *reinterpret_cast<int32_t*>(&rax13) = r8d8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
        return rax13;
    }
    addr_c3e3_7:
    *reinterpret_cast<unsigned char*>(&r8d8) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 34));
    goto addr_c3ec_12;
    addr_c432_8:
    r8d8 = 1;
    if (zf10) 
        goto addr_c3f5_5;
    goto addr_c3e3_7;
}

void** fun_2990(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_28e0(void** rdi);

int64_t fun_2660(void** rdi);

int64_t fun_c453(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_26c0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_2630();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_c7c4_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_c7c4_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_c50d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_c515_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2530();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2990(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_c54b_21;
    rsi = r15_14;
    rax24 = fun_28e0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2660(r13_18), r8 = r8, rax26 == 0))) {
            addr_c54b_21:
            r12d11 = 4;
            goto addr_c515_13;
        } else {
            addr_c589_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2660(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xeda8 + rbp33 * 4) + 0xeda8;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_c54b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_c50d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_c50d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2660(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_c589_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_c515_13;
}

int64_t c_locale_cache = 0;

int64_t fun_2670(int64_t rdi, int64_t rsi);

void fun_c883(int64_t rdi, int64_t* rsi) {
    int64_t rax3;
    int64_t rax4;
    int64_t rdx5;

    __asm__("cli ");
    rax3 = c_locale_cache;
    if (!rax3) {
        rax4 = fun_2670(0x1fbf, "C");
        c_locale_cache = rax4;
    }
    rdx5 = c_locale_cache;
    if (!rdx5) {
        if (rsi) {
            *rsi = rdi;
        }
        __asm__("pxor xmm0, xmm0");
        return;
    }
}

int64_t fun_2560();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_c903(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_2560();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_c95e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2530();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_c95e_3;
            rax6 = fun_2530();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int64_t fun_c973(int32_t* rdi, int32_t* rsi) {
    int64_t r8_3;
    int64_t rcx4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8_3 = 0x8000000000000000;
    __asm__("comisd xmm0, [rip+0x2508]");
    if (1) {
        addr_ca13_2:
        return r8_3;
    } else {
        *rdi = *rsi;
        r8_3 = 0x7fffffffffffffff;
        __asm__("comisd xmm1, xmm0");
        if (1) 
            goto addr_ca13_2;
        __asm__("cvttsd2si r8, xmm0");
        __asm__("pxor xmm1, xmm1");
        __asm__("cvtsi2sd xmm1, r8");
        __asm__("subsd xmm0, xmm1");
        __asm__("mulsd xmm0, [rip+0x24d1]");
        __asm__("pxor xmm1, xmm1");
        __asm__("cvttsd2si rax, xmm0");
        __asm__("cvtsi2sd xmm1, rax");
        __asm__("comisd xmm0, xmm1");
        rcx4 = rax5;
        rax6 = (__intrinsic() >> 26) - (rcx4 >> 63);
        r8_3 = 0x7fffffffffffffff + rax6;
        if (rcx4 - rax6 * 0x3b9aca00 >= 0) 
            goto addr_ca13_2;
    }
    return r8_3 - 1;
}

struct s52 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    void** f90;
};

int32_t fun_27e0(struct s52* rdi);

int32_t fun_2840(struct s52* rdi);

int32_t rpl_fflush(struct s52* rdi);

int64_t fun_25e0(struct s52* rdi);

int64_t fun_ca33(struct s52* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    void** rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_27e0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2840(rdi);
        if (!(eax3 && (eax4 = fun_27e0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_26b0(rdi5, rdi5), reinterpret_cast<int1_t>(rax6 == 0xffffffffffffffff)) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2530();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_25e0(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_25e0;
}

void rpl_fseeko(struct s52* rdi);

void fun_cac3(struct s52* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2840(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_cb13(struct s52* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_27e0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_26b0(rdi5, rdi5);
        if (rax6 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2830(int64_t rdi);

signed char* fun_cb93() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2830(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2650(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_cbd3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2650(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2630();
    } else {
        return r12_7;
    }
}

struct s53 {
    int64_t f0;
    uint64_t f8;
};

int64_t fun_2680(void* rdi, int64_t* rsi);

int64_t fun_cc63(struct s53* rdi, int64_t* rsi) {
    void** rax3;
    void** v4;
    void** rax5;
    int64_t rax6;
    int64_t rbx7;
    int64_t* rbp8;
    void* r12_9;
    void* rdx10;

    __asm__("cli ");
    rax3 = g28;
    v4 = rax3;
    if (rdi->f8 > 0x3b9ac9ff) {
        rax5 = fun_2530();
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rbx7 = rdi->f0;
        rbp8 = rsi;
        r12_9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 32);
        if (rbx7 > 0x1fa400) {
            do {
                rbx7 = rbx7 - 0x1fa400;
                rax6 = fun_2680(r12_9, rbp8);
                if (*reinterpret_cast<int32_t*>(&rax6)) 
                    goto addr_ccd8_5;
            } while (rbx7 > 0x1fa400);
            goto addr_cce8_7;
        } else {
            goto addr_cce8_7;
        }
    }
    addr_ccf7_9:
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2630();
    } else {
        return rax6;
    }
    addr_ccd8_5:
    if (rbp8) {
        *rbp8 = *rbp8 + rbx7;
        goto addr_ccf7_9;
    }
    addr_cce8_7:
    rax6 = fun_2680(r12_9, rbp8);
    goto addr_ccf7_9;
}

void fun_cd33() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_cd53() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2630();
    } else {
        return rax3;
    }
}

int64_t fun_cdd3(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    int32_t r13d8;
    void** rax9;
    int64_t rax10;

    __asm__("cli ");
    rax7 = fun_2870(rdi);
    if (!rax7) {
        r13d8 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax9 = fun_2620(rax7);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax9)) {
            fun_27b0(rsi, rax7, rax9 + 1, rcx, r8, r9);
            return 0;
        } else {
            r13d8 = 34;
            if (rdx) {
                fun_27b0(rsi, rax7, rdx + 0xffffffffffffffff, rcx, r8, r9);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax10) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    return rax10;
}

void fun_ce83() {
    __asm__("cli ");
    goto fun_2870;
}

void fun_ce93() {
    __asm__("cli ");
}

void fun_cea7() {
    __asm__("cli ");
    return;
}

int64_t __xargmatch_internal(int64_t rdi);

void fun_2b90() {
    void** rsi1;
    int64_t rax2;

    rsi1 = optarg;
    forever = 1;
    if (!rsi1) {
        follow_mode = 2;
        goto 0x2b40;
    } else {
        rax2 = __xargmatch_internal("--follow");
        follow_mode = *reinterpret_cast<int32_t*>(0xe490 + rax2 * 4);
        goto 0x2b40;
    }
}

void fun_2bf0() {
    void** rdx1;
    int32_t eax2;
    uint32_t ecx3;
    int32_t eax4;

    rdx1 = optarg;
    count_lines = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax2 == 0x6e)));
    ecx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx1));
    if (*reinterpret_cast<signed char*>(&ecx3) == 43) {
        from_start = 1;
    } else {
        if (*reinterpret_cast<signed char*>(&ecx3) == 45) {
            optarg = rdx1 + 1;
        }
    }
    if (eax4 == 0x6e) 
        goto 0x2e0b;
    fun_2600();
}

void fun_2c61() {
    forever = 1;
    follow_mode = 1;
    reopen_inaccessible_files = 1;
    goto 0x2b40;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2960(int64_t rdi, void** rsi);

uint32_t fun_2950(void** rdi, void** rsi);

void fun_92b5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2600();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2600();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2620(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_95b3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_95b3_22; else 
                            goto addr_99ad_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_9a6d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_9dc0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_95b0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_95b0_30; else 
                                goto addr_9dd9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2620(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_9dc0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2730(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_9dc0_28; else 
                            goto addr_945c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_9f20_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_9da0_40:
                        if (r11_27 == 1) {
                            addr_992d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_9ee8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_9567_44;
                            }
                        } else {
                            goto addr_9db0_46;
                        }
                    } else {
                        addr_9f2f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_992d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_95b3_22:
                                if (v47 != 1) {
                                    addr_9b09_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2620(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_9b54_54;
                                    }
                                } else {
                                    goto addr_95c0_56;
                                }
                            } else {
                                addr_9565_57:
                                ebp36 = 0;
                                goto addr_9567_44;
                            }
                        } else {
                            addr_9d94_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_9f2f_47; else 
                                goto addr_9d9e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_992d_41;
                        if (v47 == 1) 
                            goto addr_95c0_56; else 
                            goto addr_9b09_52;
                    }
                }
                addr_9621_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_94b8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_94dd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_97e0_65;
                    } else {
                        addr_9649_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_9e98_67;
                    }
                } else {
                    goto addr_9640_69;
                }
                addr_94f1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_953c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_9e98_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_953c_81;
                }
                addr_9640_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_94dd_64; else 
                    goto addr_9649_66;
                addr_9567_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_961f_91;
                if (v22) 
                    goto addr_957f_93;
                addr_961f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_9621_62;
                addr_9b54_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_a2db_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_a34b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_a14f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2960(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2950(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_9c4e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_960c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_9c58_112;
                    }
                } else {
                    addr_9c58_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_9d29_114;
                }
                addr_9618_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_961f_91;
                while (1) {
                    addr_9d29_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_a237_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_9c96_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_a245_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_9d17_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_9d17_128;
                        }
                    }
                    addr_9cc5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_9d17_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_9c96_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_9cc5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_953c_81;
                addr_a245_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_9e98_67;
                addr_a2db_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_9c4e_109;
                addr_a34b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_9c4e_109;
                addr_95c0_56:
                rax93 = fun_2990(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_960c_110;
                addr_9d9e_59:
                goto addr_9da0_40;
                addr_9a6d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_95b3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_9618_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_9565_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_95b3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_9ab2_160;
                if (!v22) 
                    goto addr_9e87_162; else 
                    goto addr_a093_163;
                addr_9ab2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_9e87_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_9e98_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_995b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_97c3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_94f1_70; else 
                    goto addr_97d7_169;
                addr_995b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_94b8_63;
                goto addr_9640_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_9d94_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_9ecf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_95b0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_94a8_178; else 
                        goto addr_9e52_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9d94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_95b3_22;
                }
                addr_9ecf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_95b0_30:
                    r8d42 = 0;
                    goto addr_95b3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_9621_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_9ee8_42;
                    }
                }
                addr_94a8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_94b8_63;
                addr_9e52_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_9db0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_9621_62;
                } else {
                    addr_9e62_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_95b3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_a612_188;
                if (v28) 
                    goto addr_9e87_162;
                addr_a612_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_97c3_168;
                addr_945c_37:
                if (v22) 
                    goto addr_a453_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_9473_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_9f20_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_9fab_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_95b3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_94a8_178; else 
                        goto addr_9f87_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9d94_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_95b3_22;
                }
                addr_9fab_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_95b3_22;
                }
                addr_9f87_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_9db0_46;
                goto addr_9e62_186;
                addr_9473_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_95b3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_95b3_22; else 
                    goto addr_9484_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_a55e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_a3e4_210;
            if (1) 
                goto addr_a3e2_212;
            if (!v29) 
                goto addr_a01e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2610();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_a551_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_97e0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_959b_219; else 
            goto addr_97fa_220;
        addr_957f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_9593_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_97fa_220; else 
            goto addr_959b_219;
        addr_a14f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_97fa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2610();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_a16d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2610();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_a5e0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_a046_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_a237_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_9593_221;
        addr_a093_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_9593_221;
        addr_97d7_169:
        goto addr_97e0_65;
        addr_a55e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_97fa_220;
        goto addr_a16d_222;
        addr_a3e4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_a43e_236;
        fun_2630();
        rsp25 = rsp25 - 8 + 8;
        goto addr_a5e0_225;
        addr_a3e2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_a3e4_210;
        addr_a01e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_a3e4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_a046_226;
        }
        addr_a551_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_99ad_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe7ac + rax113 * 4) + 0xe7ac;
    addr_9dd9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe8ac + rax114 * 4) + 0xe8ac;
    addr_a453_190:
    addr_959b_219:
    goto 0x9280;
    addr_9484_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe6ac + rax115 * 4) + 0xe6ac;
    addr_a43e_236:
    goto v116;
}

void fun_94a0() {
}

void fun_9658() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x9352;
}

void fun_96b1() {
    goto 0x9352;
}

void fun_979e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x9621;
    }
    if (v2) 
        goto 0xa093;
    if (!r10_3) 
        goto addr_a1fe_5;
    if (!v4) 
        goto addr_a0ce_7;
    addr_a1fe_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_a0ce_7:
    goto 0x94d4;
}

void fun_97bc() {
}

void fun_9867() {
    signed char v1;

    if (v1) {
        goto 0x97ef;
    } else {
        goto 0x952a;
    }
}

void fun_9881() {
    signed char v1;

    if (!v1) 
        goto 0x987a; else 
        goto "???";
}

void fun_98a8() {
    goto 0x97c3;
}

void fun_9928() {
}

void fun_9940() {
}

void fun_996f() {
    goto 0x97c3;
}

void fun_99c1() {
    goto 0x9950;
}

void fun_99f0() {
    goto 0x9950;
}

void fun_9a23() {
    goto 0x9950;
}

void fun_9df0() {
    goto 0x94a8;
}

void fun_a0ee() {
    signed char v1;

    if (v1) 
        goto 0xa093;
    goto 0x94d4;
}

void fun_a195() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x94d4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x94b8;
        goto 0x94d4;
    }
}

void fun_a5b2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x9820;
    } else {
        goto 0x9352;
    }
}

void fun_b5b8() {
    fun_2600();
}

void fun_c5fc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xc60b;
}

void fun_c6cc() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xc6d9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xc60b;
}

void fun_c6f0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_c71c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xc60b;
}

void fun_c73d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xc60b;
}

void fun_c761() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xc60b;
}

void fun_c785() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xc714;
}

void fun_c7a9() {
}

void fun_c7c9() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xc714;
}

void fun_c7e5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xc714;
}

void fun_3410() {
    goto 0x2b40;
}

void fun_2c7e() {
    disable_inotify = 1;
    goto 0x2b40;
}

void fun_96de() {
    goto 0x9352;
}

void fun_98b4() {
    goto 0x986c;
}

void fun_997b() {
    goto 0x94a8;
}

void fun_99cd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x9950;
    goto 0x957f;
}

void fun_99ff() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x995b;
        goto 0x9380;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x97fa;
        goto 0x959b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xa198;
    if (r10_8 > r15_9) 
        goto addr_98e5_9;
    addr_98ea_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xa1a3;
    goto 0x94d4;
    addr_98e5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_98ea_10;
}

void fun_9a32() {
    goto 0x9567;
}

void fun_9e00() {
    goto 0x9567;
}

void fun_a59f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x96bc;
    } else {
        goto 0x9820;
    }
}

void fun_b670() {
}

void fun_c69f() {
    if (__intrinsic()) 
        goto 0xc6d9; else 
        goto "???";
}

void fun_341d() {
    goto 0x2b40;
}

void fun_2c8a() {
    presume_input_pipe = 1;
    goto 0x2b40;
}

void fun_9a3c() {
    goto 0x99d7;
}

void fun_9e0a() {
    goto 0x992d;
}

void fun_b6d0() {
    fun_2600();
    goto fun_2930;
}

void fun_2c96() {
    void** rdi1;
    void** rax2;

    fun_2600();
    rdi1 = optarg;
    rax2 = xdectoumax(rdi1);
    pid = rax2;
    goto 0x2b40;
}

void fun_970d() {
    goto 0x9352;
}

void fun_9a48() {
    goto 0x99d7;
}

void fun_9e17() {
    goto 0x997e;
}

void fun_b710() {
    fun_2600();
    goto fun_2930;
}

void fun_2cd0() {
    void** rdi1;
    void** rax2;

    fun_2600();
    rdi1 = optarg;
    rax2 = xdectoumax(rdi1);
    max_n_unchanged_stats_between_opens = rax2;
    goto 0x2b40;
}

void fun_973a() {
    goto 0x9352;
}

void fun_9a54() {
    goto 0x9950;
}

void fun_b750() {
    fun_2600();
    goto fun_2930;
}

void fun_2d0d() {
    reopen_inaccessible_files = 1;
    goto 0x2b40;
}

void fun_975c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xa0f0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x9621;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x9621;
    }
    if (v11) 
        goto 0xa453;
    if (r10_12 > r15_13) 
        goto addr_a4a3_8;
    addr_a4a8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xa1e1;
    addr_a4a3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_a4a8_9;
}

struct s54 {
    signed char[24] pad24;
    int64_t f18;
};

struct s55 {
    signed char[16] pad16;
    void** f10;
};

struct s56 {
    signed char[8] pad8;
    void** f8;
};

void fun_b7a0() {
    int64_t r15_1;
    struct s54* rbx2;
    void** r14_3;
    struct s55* rbx4;
    void** r13_5;
    struct s56* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2600();
    fun_2930(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xb7c2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2d19() {
    line_end = 0;
    goto 0x2b40;
}

void fun_b7f8() {
    fun_2600();
    goto 0xb7c9;
}

signed char xstrtod();

void fun_2d25() {
    void** rdi1;
    signed char al2;
    void** rdi3;

    rdi1 = optarg;
    al2 = xstrtod();
    if (al2) {
        *reinterpret_cast<void***>(rdi1) = __cxa_finalize;
        __asm__("pxor xmm4, xmm4");
        __asm__("comisd xmm2, xmm4");
        *reinterpret_cast<void***>(rdi1 + 4) = (&__cxa_finalize)[4];
        if (1) 
            goto 0x2b40;
    }
    rdi3 = optarg;
    quote(rdi3, rdi3);
    fun_2600();
    fun_28a0();
}

struct s57 {
    signed char[32] pad32;
    void*** f20;
};

struct s58 {
    signed char[24] pad24;
    int64_t f18;
};

struct s59 {
    signed char[16] pad16;
    void** f10;
};

struct s60 {
    signed char[8] pad8;
    void** f8;
};

struct s61 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_b830() {
    void*** rcx1;
    struct s57* rbx2;
    int64_t r15_3;
    struct s58* rbx4;
    void** r14_5;
    struct s59* rbx6;
    void** r13_7;
    struct s60* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s61* rbx12;
    void** rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2600();
    fun_2930(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xb864, __return_address(), rcx1);
    goto v15;
}

void fun_b8a8() {
    fun_2600();
    goto 0xb86b;
}
