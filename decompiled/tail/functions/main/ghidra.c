undefined8 main(int param_1,uint *param_2)

{
  bool bVar1;
  byte bVar2;
  char cVar3;
  undefined uVar4;
  byte bVar5;
  byte bVar6;
  int iVar7;
  uint uVar8;
  int iVar9;
  uint uVar10;
  uint32_t __mask;
  int iVar11;
  char **ppcVar12;
  char *pcVar13;
  pollfd pVar14;
  char *pcVar15;
  void *pvVar16;
  pollfd pVar17;
  int *piVar18;
  pollfd __ptr;
  pollfd pVar19;
  __off_t _Var20;
  short *psVar21;
  undefined8 uVar22;
  __off_t _Var23;
  size_t sVar24;
  pollfd *ppVar25;
  uint *puVar26;
  pollfd *ppVar27;
  long lVar28;
  undefined8 extraout_RDX;
  undefined **ppuVar29;
  long lVar30;
  ulong uVar31;
  byte bVar32;
  uint uVar33;
  char *pcVar34;
  pollfd *ppVar35;
  undefined1 *puVar36;
  pollfd pVar37;
  pollfd pVar38;
  long in_FS_OFFSET;
  bool bVar39;
  undefined8 uVar40;
  ulong local_250;
  undefined8 local_248;
  pollfd local_240;
  ulong local_238;
  pollfd local_230;
  pollfd local_220;
  pollfd local_218;
  uint local_210;
  byte local_209;
  pollfd *local_208;
  pollfd *local_200;
  stat *local_1f8;
  long local_1f0;
  uint local_1e0;
  pollfd local_1d8;
  long local_1d0;
  pollfd local_1c8 [8];
  int local_184;
  stat local_168;
  uint local_d8;
  undefined2 uStack212;
  undefined2 uStack210;
  pollfd local_d0;
  uint local_c0;
  pollfd local_a8;
  long local_a0;
  pollfd local_80;
  pollfd local_78;
  long local_40;
  
  pVar37 = (pollfd)((long)"Written by %s, %s, and %s.\n" + 0x1b);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_1d8 = (pollfd)0xa;
  set_program_name(*(char **)param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  have_read_stdin = '\0';
  count_lines = 1;
  print_headers = '\0';
  from_start = 0;
  forever = 0;
  line_end = '\n';
  if (param_1 != 2) {
    if (param_1 == 3) {
      pcVar34 = *(char **)((long)param_2 + 0x10);
      if ((*pcVar34 == '-') && (pcVar34[1] != '\0')) goto LAB_00102ad0;
      goto LAB_00102ae0;
    }
    if (param_1 == 4) {
      pcVar34 = *(char **)((long)param_2 + 0x10);
LAB_00102ad0:
      iVar7 = strcmp(pcVar34,"--");
      if (iVar7 == 0) goto LAB_00102ae0;
    }
LAB_00102b02:
    lVar30 = 0;
LAB_00102b04:
    local_248 = (pollfd *)((ulong)local_248 & 0xffffffff00000000);
    ppcVar12 = (char **)((long)param_2 + lVar30 * 8);
    param_1 = param_1 - (int)lVar30;
    puVar36 = long_options;
    pcVar34 = "c:n:fFqs:vz0123456789";
    local_218 = DAT_0010e498;
    param_2 = &switchD_00102b84::switchdataD_0010e330;
LAB_00102b40:
    uVar22 = 0;
    uVar40 = 0x102b53;
    uVar8 = getopt_long(param_1,ppcVar12,"c:n:fFqs:vz0123456789");
    if (uVar8 == 0xffffffff) goto LAB_00102f45;
    if ((int)uVar8 < 0x86) {
      if ((int)uVar8 < 0x30) {
LAB_00102da0:
        if (uVar8 == 0xffffff7d) {
          version_etc(stdout,&DAT_0010e039,"GNU coreutils",Version,"Paul Rubin","David MacKenzie",
                      "Ian Lance Taylor","Jim Meyering",0,uVar40);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (uVar8 != 0xffffff7e) goto switchD_00102b84_caseD_3a;
        usage(0);
        uVar22 = extraout_RDX;
LAB_00102e0b:
        uVar22 = dcgettext(0,"invalid number of lines",uVar22);
LAB_00102c35:
        local_1d8 = (pollfd)xdectoumax(optarg,0,0xffffffffffffffff,"bkKmMGTPEZY0",uVar22,0);
        goto LAB_00102b40;
      }
      switch(uVar8) {
      case 0x30:
      case 0x31:
      case 0x32:
      case 0x33:
      case 0x34:
      case 0x35:
      case 0x36:
      case 0x37:
      case 0x38:
      case 0x39:
        goto switchD_00102b84_caseD_30;
      default:
        goto switchD_00102b84_caseD_3a;
      case 0x46:
        forever = 1;
        follow_mode = 1;
        reopen_inaccessible_files = 1;
        break;
      case 99:
      case 0x6e:
        count_lines = uVar8 == 0x6e;
        if (*optarg == '+') {
          from_start = 1;
        }
        else if (*optarg == '-') {
          optarg = optarg + 1;
        }
        uVar22 = 5;
        if (uVar8 == 0x6e) goto LAB_00102e0b;
        uVar22 = dcgettext(0,"invalid number of bytes",5);
        goto LAB_00102c35;
      case 0x66:
      case 0x84:
        forever = 1;
        if (optarg == (char *)0x0) {
          follow_mode = 2;
        }
        else {
          lVar30 = __xargmatch_internal
                             ("--follow",optarg,follow_mode_string,&follow_mode_map,4,argmatch_die,1
                              ,uVar22);
          follow_mode = *(int *)((long)&follow_mode_map + lVar30 * 4);
        }
        break;
      case 0x71:
        local_248 = (pollfd *)CONCAT44(local_248._4_4_,2);
        break;
      case 0x73:
        cVar3 = xstrtod(optarg,0,local_1c8,cl_strtod);
        if (cVar3 != '\0') {
          local_218 = local_1c8[0];
          if (0.0 <= (double)local_1c8[0]) break;
        }
        ppcVar12 = (char **)quote(optarg);
        uVar22 = dcgettext(0,"invalid number of seconds: %s",5);
        uVar40 = 0x102d99;
        uVar8 = error(1,0,uVar22);
        goto LAB_00102da0;
      case 0x76:
        local_248 = (pollfd *)CONCAT44(local_248._4_4_,1);
        break;
      case 0x7a:
        line_end = '\0';
        break;
      case 0x80:
        reopen_inaccessible_files = 1;
        break;
      case 0x81:
        uVar22 = dcgettext(0,"invalid maximum number of unchanged stats between opens",5);
        max_n_unchanged_stats_between_opens =
             (pollfd)xdectoumax(optarg,0,0xffffffffffffffff,"",uVar22,0);
        break;
      case 0x82:
        uVar22 = dcgettext(0,"invalid PID",5);
        pid = xdectoumax(optarg,0,0x7fffffff,"",uVar22,0);
        break;
      case 0x83:
        presume_input_pipe = '\x01';
        break;
      case 0x85:
        disable_inotify = '\x01';
      }
      goto LAB_00102b40;
    }
switchD_00102b84_caseD_3a:
    uVar8 = usage(1);
switchD_00102b84_caseD_30:
    local_250 = local_250 & 0xffffffff00000000 | (ulong)uVar8;
    uVar22 = dcgettext(0,"option used in invalid context -- %c",5);
    error(1,0,uVar22);
LAB_0010415a:
    xwrite_stdout_part_0();
LAB_00103c69:
    for (pcVar15 = *(char **)((long)puVar36 + 0x2010); pcVar15 != (char *)0x0;
        pcVar15 = *(char **)(pcVar15 + 0x2010)) {
      if (*(long *)(pcVar15 + 0x2000) != 0) {
        xwrite_stdout_part_0(pcVar15);
      }
    }
    local_1e0 = (uint)local_238 & 0xff;
LAB_00103848:
    pVar38 = *(pollfd *)((long)pcVar34 + 0x2010);
    free(pcVar34);
    pcVar34 = (char *)pVar38;
joined_r0x0010385a:
    if ((pollfd)pcVar34 != (pollfd)0x0) goto LAB_00103848;
LAB_001035c1:
    iVar7 = SUB84(pVar37,0);
    if (forever == 0) {
      if ((local_210 != 0) && (iVar7 = close(iVar7), iVar7 != 0)) {
        param_2 = *(uint **)local_248;
        iVar7 = strcmp((char *)param_2,"-");
        if (iVar7 == 0) {
          param_2 = (uint *)dcgettext(0,"standard input",5);
        }
        quotearg_style(4,param_2);
        uVar22 = dcgettext(0,"error reading %s",5);
        piVar18 = __errno_location();
        error(0,*piVar18,uVar22);
        local_1e0 = 0;
      }
    }
    else {
      local_238._0_1_ = (byte)local_1e0;
      *(uint *)&local_248[7].events = local_1e0 - 1;
      iVar9 = fstat(iVar7,local_1f8);
      if (iVar9 < 0) {
        piVar18 = __errno_location();
        pVar38 = *local_248;
        *(int *)&local_248[7].events = *piVar18;
        iVar7 = strcmp((char *)pVar38,"-");
        if (iVar7 == 0) {
          pVar38 = (pollfd)dcgettext(0,"standard input",5);
        }
        quotearg_style(4,pVar38);
        uVar22 = dcgettext(0,"error reading %s",5);
        error(0,*piVar18,uVar22);
      }
      else if ((((local_c0 & 0xf000) - 0x1000 & 0xffffe000) == 0) || ((local_c0 & 0xb000) == 0x8000)
              ) {
        if ((byte)local_238 != '\0') {
          local_248[1] = local_1c8[0];
          param_2 = *(uint **)local_248;
          local_248[7].fd = iVar7;
          local_248[2] = local_80;
          local_248[6].fd = local_c0;
          local_248[3] = local_78;
          local_248[8].fd = -(uint)(local_210 == 0) | 1;
          local_248[4] = (pollfd)CONCAT26(uStack210,CONCAT24(uStack212,local_d8));
          local_248[0xb] = (pollfd)0x0;
          local_248[5] = local_d0;
          *(undefined *)&local_248[6].events = 0;
          iVar7 = strcmp((char *)param_2,"-");
          if (iVar7 == 0) {
            param_2 = (uint *)dcgettext(0,"standard input",5);
          }
          uVar4 = fremote(pVar37,param_2);
          *(undefined *)((long)&local_248[6].events + 1) = uVar4;
          goto LAB_001031c6;
        }
      }
      else {
        pcVar34 = "";
        bVar5 = reopen_inaccessible_files ^ 1;
        *(undefined4 *)&local_248[7].events = 0xffffffff;
        *(undefined *)&local_248[6].revents = 0;
        *(byte *)&local_248[6].events = bVar5;
        if (bVar5 != 0) {
          pcVar34 = (char *)dcgettext(0,"; giving up on this name",5);
        }
        pVar38 = *local_248;
        iVar7 = strcmp((char *)pVar38,"-");
        if (iVar7 == 0) {
          pVar38 = (pollfd)dcgettext(0,"standard input",5);
        }
        uVar22 = quotearg_n_style_colon(0,3,pVar38);
        uVar40 = dcgettext(0,"%s: cannot follow end of this type of file%s",5);
        error(0,0,uVar40,uVar22,pcVar34);
      }
      param_2 = *(uint **)local_248;
      *(byte *)&local_248[6].events = reopen_inaccessible_files ^ 1;
      iVar7 = strcmp((char *)param_2,"-");
      if (iVar7 == 0) {
        param_2 = (uint *)dcgettext(0,"standard input",5);
      }
      close_fd(pVar37,param_2);
      local_1e0 = 0;
      local_248[7].fd = -1;
    }
LAB_001031c6:
    bVar5 = forever;
    local_248 = local_248 + 0xc;
    local_238._0_1_ = (byte)local_1e0;
    local_209 = local_209 & (byte)local_238;
    if (local_208 != local_248) {
LAB_001031e5:
      pVar38 = local_1d8;
      param_2 = *(uint **)local_248;
      local_210 = *(byte *)param_2 - 0x2d;
      if ((local_210 != 0) || (local_210 = (uint)*(byte *)((long)param_2 + 1), local_210 != 0)) {
        uVar8 = open_safer(param_2,0);
        bVar5 = reopen_inaccessible_files;
        pVar37 = (pollfd)(ulong)uVar8;
        if (reopen_inaccessible_files == 0) {
          param_2 = *(uint **)local_248;
          *(undefined *)&local_248[6].revents = 1;
          if (uVar8 == 0xffffffff) goto LAB_0010313c;
          goto LAB_00103231;
        }
        param_2 = *(uint **)local_248;
        if (uVar8 != 0xffffffff) goto LAB_00103228;
        *(undefined *)&local_248[6].revents = 0;
LAB_0010313c:
        piVar18 = __errno_location();
        if (forever != 0) {
          iVar7 = *piVar18;
          local_248[7].fd = -1;
          *(int *)&local_248[7].events = iVar7;
          *(byte *)&local_248[6].events = bVar5 ^ 1;
          local_248[5] = (pollfd)0x0;
          local_248[4] = (pollfd)0x0;
        }
        iVar7 = strcmp((char *)param_2,"-");
        if (iVar7 == 0) {
          param_2 = (uint *)dcgettext(0,"standard input",5);
        }
        quotearg_style(4,param_2);
        uVar22 = dcgettext(0,"cannot open %s for reading",5);
        error(0,*piVar18,uVar22);
        local_1e0 = 0;
        goto LAB_001031c6;
      }
      have_read_stdin = '\x01';
      pVar37 = (pollfd)0x0;
LAB_00103228:
      *(undefined *)&local_248[6].revents = 1;
LAB_00103231:
      if (print_headers != '\0') {
        iVar7 = strcmp((char *)param_2,"-");
        if (iVar7 == 0) {
          dcgettext(0,"standard input",5);
        }
        pcVar34 = "\n";
        if (first_file_2 != '\0') {
          pcVar34 = "";
        }
        __printf_chk(1,"%s==> %s <==\n",pcVar34);
        first_file_2 = '\0';
        param_2 = *(uint **)local_248;
      }
      local_220 = (pollfd)param_2;
      if ((*(char *)param_2 == '-') && (*(char *)((long)param_2 + 1) == '\0')) {
        local_220 = (pollfd)dcgettext(0,"standard input",5);
      }
      bVar5 = count_lines;
      local_1c8[0] = (pollfd)0x0;
      local_238._0_4_ = (uint)count_lines;
      local_1f8 = (stat *)&local_d8;
      iVar7 = SUB84(pVar37,0);
      if (count_lines == 0) {
        iVar9 = fstat(iVar7,local_1f8);
        if (iVar9 != 0) {
          quotearg_style(4,local_220);
          uVar22 = dcgettext(0,"cannot fstat %s",5);
          piVar18 = __errno_location();
          error(0,*piVar18,uVar22);
          local_1e0 = (uint)local_238;
          goto LAB_001035c1;
        }
        local_1e0 = (uint)from_start;
        if (from_start != 0) {
          if ((presume_input_pipe == '\0') && (-1 < (long)pVar38)) {
            if ((local_c0 & 0xf000) == 0x8000) {
              _Var20 = lseek(iVar7,(__off_t)pVar38,1);
              if (_Var20 < 0) {
                    /* WARNING: Subroutine does not return */
                xlseek_part_0(pVar38,1,local_220);
              }
            }
            else {
              _Var20 = lseek(iVar7,(__off_t)pVar38,1);
              if (_Var20 == -1) goto LAB_0010364e;
            }
            local_1c8[0] = pVar38;
            pVar17 = local_1c8[0];
          }
          else {
LAB_0010364e:
            uVar8 = start_bytes(local_220,pVar37,pVar38);
            pVar17 = local_1c8[0];
            if (uVar8 != 0) {
              local_1e0 = uVar8 >> 0x1f;
              goto LAB_001035c1;
            }
          }
LAB_001035a0:
          local_1c8[0] = pVar17;
          lVar30 = dump_remainder(0,local_220,pVar37);
          local_1c8[0] = (pollfd)((long)local_1c8[0] + lVar30);
          local_1e0 = 1;
          goto LAB_001035c1;
        }
        if ((presume_input_pipe == '\0') && (-1 < (long)pVar38)) {
          if ((local_c0 & 0xd000) == 0x8000) {
            param_2 = (uint *)0xffffffffffffffff;
            pVar14 = local_a8;
          }
          else {
            param_2 = (uint *)lseek(iVar7,-(long)pVar38,2);
            if ((pollfd)param_2 == (pollfd)0xffffffffffffffff) goto LAB_0010391f;
            pVar14 = (pollfd)((long)pVar38 + (long)param_2);
          }
          lVar30 = local_a0;
          if (0x1fffffffffffffff < local_a0 - 1U) {
            lVar30 = 0x200;
          }
          if (lVar30 < (long)pVar14) {
            if (((pollfd)param_2 == (pollfd)0xffffffffffffffff) &&
               (param_2 = (uint *)lseek(iVar7,0,1), (long)param_2 < 0)) {
                    /* WARNING: Subroutine does not return */
              xlseek_part_0(0,1,local_220);
            }
            pVar17 = (pollfd)param_2;
            if (((long)param_2 < (long)pVar14) &&
               (pVar17 = (pollfd)param_2, (ulong)pVar38 < (ulong)((long)pVar14 - (long)param_2))) {
              param_2 = (uint *)((long)pVar14 - (long)pVar38);
              _Var20 = lseek(iVar7,(__off_t)param_2,0);
              pVar17 = (pollfd)param_2;
              if (_Var20 < 0) {
                    /* WARNING: Subroutine does not return */
                xlseek_part_0(param_2,0,local_220);
              }
            }
            goto LAB_001035a0;
          }
        }
LAB_0010391f:
        __ptr = (pollfd)xmalloc(0x2010);
        *(char **)((long)__ptr + 0x2000) = (char *)0x0;
        *(char **)((long)__ptr + 0x2008) = (char *)0x0;
        pVar17 = (pollfd)xmalloc(0x2010);
        pVar14 = (pollfd)0x0;
        puVar36 = (undefined1 *)__ptr;
        while (pcVar34 = (char *)safe_read(pVar37,pVar17,0x2000),
              pcVar34 + -1 < (char *)0xfffffffffffffffe) {
          local_1c8[0] = (pollfd)((long)local_1c8[0] + (long)pcVar34);
          pVar14 = (pollfd)((long)pVar14 + (long)pcVar34);
          *(char **)((long)pVar17 + 0x2000) = pcVar34;
          pcVar15 = *(char **)((long)puVar36 + 0x2000);
          *(char **)((long)pVar17 + 0x2008) = (char *)0x0;
          if (pcVar34 + (long)pcVar15 < (char *)0x2000) {
            memcpy(pcVar15 + (long)puVar36,(void *)pVar17,(size_t)pcVar34);
            *(char **)((long)puVar36 + 0x2000) =
                 *(char **)((long)puVar36 + 0x2000) + (long)*(char **)((long)pVar17 + 0x2000);
          }
          else {
            pVar19 = (pollfd)((long)pVar14 - (long)*(char **)((long)__ptr + 0x2000));
            *(pollfd *)((long)puVar36 + 0x2008) = pVar17;
            puVar36 = (undefined1 *)pVar17;
            if ((ulong)pVar38 < (ulong)pVar19) {
              pVar14 = pVar19;
              pVar17 = __ptr;
              __ptr = *(pollfd *)((long)__ptr + 0x2008);
            }
            else {
              pVar17 = (pollfd)xmalloc();
            }
          }
        }
        free((void *)pVar17);
        param_2 = (uint *)__ptr;
        if (pcVar34 == (char *)0xffffffffffffffff) {
          puVar36 = (undefined1 *)quotearg_style(4,local_220);
          uVar22 = dcgettext(0,"error reading %s",5);
          piVar18 = __errno_location();
          error(0,*piVar18,uVar22);
        }
        else {
          while( true ) {
            pVar17 = (pollfd)((long)pVar14 - (long)*(char **)((long)param_2 + 0x2000));
            if ((ulong)pVar17 <= (ulong)pVar38) break;
            param_2 = *(uint **)((long)param_2 + 0x2008);
            pVar14 = pVar17;
          }
          pcVar34 = (char *)0x0;
          if ((ulong)pVar38 < (ulong)pVar14) {
            pcVar34 = (char *)((long)pVar14 - (long)pVar38);
          }
          if (*(char **)((long)param_2 + 0x2000) != pcVar34) {
            xwrite_stdout_part_0(pcVar34 + (long)param_2);
          }
          for (pcVar34 = *(char **)((long)param_2 + 0x2008); pcVar34 != (char *)0x0;
              pcVar34 = *(char **)(pcVar34 + 0x2008)) {
            if (*(long *)(pcVar34 + 0x2000) != 0) {
              xwrite_stdout_part_0(pcVar34);
            }
          }
          local_1e0 = 1;
        }
        while (__ptr != (pollfd)0x0) {
          pVar38 = *(pollfd *)((long)__ptr + 0x2008);
          free((void *)__ptr);
          __ptr = pVar38;
        }
      }
      else {
        iVar9 = fstat(iVar7,local_1f8);
        if (iVar9 == 0) {
          local_1e0 = (uint)from_start;
          if (from_start == 0) {
            if (((presume_input_pipe != '\0') || ((local_c0 & 0xf000) != 0x8000)) ||
               (_Var20 = lseek(iVar7,0,1), _Var20 == -1)) goto LAB_001032d9;
            pVar14 = (pollfd)lseek(iVar7,0,2);
            if ((long)pVar14 <= _Var20) goto LAB_00104788;
            local_1e0 = (uint)bVar5;
            local_1c8[0] = pVar14;
            if (pVar14 != (pollfd)0x0) {
              bVar5 = file_lines(local_220,pVar37,pVar38,_Var20,pVar14,local_1c8);
              local_1e0 = (uint)bVar5;
            }
          }
          else {
            uVar8 = start_lines(local_220,pVar37,pVar38);
            if (uVar8 == 0) {
              lVar30 = dump_remainder(0,local_220,pVar37);
              local_1c8[0] = (pollfd)((long)local_1c8[0] + lVar30);
            }
            else {
              local_1e0 = uVar8 >> 0x1f;
            }
          }
        }
        else {
          quotearg_style(4,local_220);
          uVar22 = dcgettext(0,"cannot fstat %s",5);
          piVar18 = __errno_location();
          error(0,*piVar18,uVar22);
          local_1e0 = 0;
        }
      }
      goto LAB_001035c1;
    }
    if (forever == 0) goto LAB_00103a0f;
    param_2 = (uint *)0x0;
    ppVar25 = local_200;
    do {
      iVar7 = strcmp((char *)*ppVar25,"-");
      if ((((iVar7 == 0) && (*(char *)&ppVar25[6].events == '\0')) && (-1 < ppVar25[7].fd)) &&
         ((ppVar25[6].fd & 0xf000U) == 0x1000)) {
        ppVar25[7].fd = -1;
        *(undefined *)&ppVar25[6].events = 1;
      }
      else {
        param_2 = (uint *)((long)param_2 + 1);
      }
      ppVar25 = ppVar25 + 0xc;
    } while (local_208 != ppVar25);
    if ((pollfd)param_2 == (pollfd)0x0) {
LAB_00103a0f:
      if ((have_read_stdin == '\0') || (iVar7 = close(0), -1 < iVar7)) {
                    /* WARNING: Subroutine does not return */
        exit((uint)(local_209 ^ 1));
      }
LAB_00104b42:
      piVar18 = __errno_location();
      error(1,*piVar18,&DAT_0010e0fc);
      goto LAB_00104b5c;
    }
    iVar7 = fstat(1,&local_168);
    if (iVar7 < 0) {
LAB_00104aea:
      uVar22 = dcgettext(0,"standard output",5);
      piVar18 = __errno_location();
      error(1,*piVar18,uVar22);
LAB_00104b16:
      uVar22 = dcgettext(0,"write error",5);
      piVar18 = __errno_location();
      error(1,*piVar18,uVar22);
      goto LAB_00104b42;
    }
    monitor_output = (local_168.st_mode & 0xf000) == 0x1000;
    ppVar25 = local_200;
    ppVar35 = local_208;
    if (disable_inotify != '\0') {
LAB_00103ec6:
      param_2 = (uint *)(ulong)pid;
      disable_inotify = '\x01';
      if (pid == 0) {
        if (((follow_mode == 2) && (local_250 == 1)) && (local_200[7].fd != -1)) {
          param_2 = (uint *)(ulong)((local_200[6].fd & 0xf000U) != 0x8000);
        }
      }
      else {
        param_2 = (uint *)0x0;
      }
      iVar7 = (int)param_2;
      local_240 = (pollfd)(local_250 - 1);
      bVar32 = 0;
LAB_00103f20:
      bVar2 = bVar32;
      bVar1 = false;
      uVar31 = 0;
      ppVar25 = local_200;
      do {
        do {
          if (*(char *)&ppVar25[6].events != '\0') goto LAB_00103fda;
          uVar8 = ppVar25[7].fd;
          ppVar35 = (pollfd *)(ulong)uVar8;
          if (-1 < (int)uVar8) {
            puVar36 = *(undefined1 **)ppVar25;
            if ((*puVar36 == '-') && (*(char *)((long)puVar36 + 1) == '\0')) {
              puVar36 = (undefined1 *)dcgettext(0,"standard input",5);
            }
            uVar33 = ppVar25[6].fd;
            iVar9 = ppVar25[8].fd;
            if (iVar7 == ppVar25[8].fd) goto LAB_00103fb0;
            uVar10 = rpl_fcntl(ppVar35,3);
            if (iVar7 == 0) {
              if ((-1 < (int)uVar10) &&
                 ((uVar10 == (uVar10 | 0x800) || (iVar9 = rpl_fcntl(ppVar35,4), iVar9 != -1)))) {
LAB_00103faa:
                ppVar25[8].fd = iVar7;
                iVar9 = iVar7;
                goto LAB_00103fb0;
              }
            }
            else if (-1 < (int)uVar10) goto LAB_00103faa;
            piVar18 = __errno_location();
            if (((ppVar25[6].fd & 0xf000U) == 0x8000) && (*piVar18 == 1)) {
              iVar9 = ppVar25[8].fd;
LAB_00103fb0:
              lVar30 = -2;
              if (iVar9 == 0) {
                iVar9 = fstat(uVar8,(stat *)&local_d8);
                if (iVar9 != 0) goto LAB_001044d9;
                if ((ppVar25[6].fd == local_c0) &&
                   ((((local_c0 & 0xf000) != 0x8000 || (ppVar25[1] == local_a8)) &&
                    (((uint)((long)local_78 < (long)ppVar25[3]) -
                     (uint)((long)ppVar25[3] < (long)local_78)) +
                     ((uint)((long)local_80 < (long)ppVar25[2]) -
                     (uint)((long)ppVar25[2] < (long)local_80)) * 2 == 0)))) {
                  bVar39 = (ulong)ppVar25[0xb] < (ulong)max_n_unchanged_stats_between_opens;
                  ppVar25[0xb] = (pollfd)((long)ppVar25[0xb] + 1);
                  if (bVar39) goto LAB_00103fda;
                  if (follow_mode != 1) goto LAB_00103fda;
                  recheck();
                  ppVar25[0xb] = (pollfd)0x0;
                  goto LAB_00103fda;
                }
                uVar33 = uVar33 & 0xf000;
                ppVar25[6].fd = local_c0;
                ppVar25[0xb] = (pollfd)0x0;
                ppVar25[2] = local_80;
                ppVar25[3] = local_78;
                if ((uVar33 == 0x8000) && ((long)local_a8 < (long)ppVar25[1])) {
                  uVar22 = quotearg_n_style_colon(0,3,puVar36);
                  uVar40 = dcgettext(0,"%s: file truncated",5);
                  error(0,0,uVar40,uVar22);
                  _Var20 = lseek(uVar8,0,0);
                  if (_Var20 < 0) {
                    /* WARNING: Subroutine does not return */
                    xlseek_part_0(0,0,puVar36);
                  }
                  ppVar25[1] = (pollfd)0x0;
                }
                if (((pollfd)uVar31 != local_240) && (print_headers != '\0')) {
                  pcVar34 = "\n";
                  if (first_file_2 != '\0') {
                    pcVar34 = "";
                  }
                  __printf_chk(1,"%s==> %s <==\n",pcVar34,puVar36);
                  first_file_2 = '\0';
                }
                local_240 = (pollfd)uVar31;
                if (ppVar25[8].fd == 0) {
                  lVar30 = -1;
                  if ((uVar33 == 0x8000) && (*(char *)((long)&ppVar25[6].events + 1) != '\0')) {
                    lVar30 = (long)local_a8 - (long)ppVar25[1];
                  }
                }
                else {
                  lVar30 = -2;
                }
              }
              lVar30 = dump_remainder(0,puVar36,ppVar35,lVar30);
              ppVar25[1] = (pollfd)((long)ppVar25[1] + lVar30);
              bVar1 = (bool)(bVar1 | lVar30 != 0);
              goto LAB_00103fda;
            }
            uVar22 = quotearg_n_style_colon(0,3,puVar36);
            uVar40 = dcgettext(0,"%s: cannot change nonblocking mode",5);
            error(1,*piVar18,uVar40,uVar22);
            goto LAB_00104aea;
          }
          uVar31 = uVar31 + 1;
          ppVar25 = ppVar25 + 0xc;
          recheck();
        } while (local_250 != uVar31);
        do {
          if ((reopen_inaccessible_files == 0) || (follow_mode != 1)) {
            psVar21 = &local_200[6].events;
            while ((*(int *)(psVar21 + 2) < 0 &&
                   ((*(char *)psVar21 == '\x01' || (reopen_inaccessible_files == 0))))) {
              psVar21 = psVar21 + 0x30;
              if (psVar21 == (short *)((long)&local_200[6].events + local_1f0)) {
                uVar22 = dcgettext(0,"no files remaining",5);
                error(0,0,uVar22);
                goto LAB_00103a0f;
              }
            }
          }
          bVar32 = bVar2;
          if ((bVar1) && (((ulong)param_2 & 1) == 0)) {
            if (monitor_output == 0) goto LAB_00103f20;
            local_1c8[0] = (pollfd)0x1;
            iVar9 = poll(local_1c8,1,0);
            if (iVar9 < 0) goto LAB_00103f20;
LAB_00104468:
            if (((ulong)local_1c8[0] & 0x18000000000000) != 0) goto LAB_00104bea;
          }
          else {
            iVar9 = fflush_unlocked(stdout);
            if (iVar9 != 0) goto LAB_00104b16;
            if (monitor_output != 0) {
              local_1c8[0] = (pollfd)0x1;
              iVar9 = poll(local_1c8,1,0);
              if (-1 < iVar9) goto LAB_00104468;
            }
          }
          if (bVar1) goto LAB_00103f20;
          if (bVar2 != 0) goto LAB_00103a0f;
          if ((((pid != 0) && (iVar9 = kill(pid,0), iVar9 != 0)) &&
              (piVar18 = __errno_location(), bVar32 = bVar5, *piVar18 != 1)) ||
             (iVar9 = xnanosleep(local_218), bVar32 = bVar2, iVar9 == 0)) goto LAB_00103f20;
          uVar22 = dcgettext(0,"cannot read realtime clock",5);
          piVar18 = __errno_location();
          error(1,*piVar18,uVar22);
LAB_001044d9:
          ppVar25[7].fd = -1;
          piVar18 = __errno_location();
          *(int *)&ppVar25[7].events = *piVar18;
          uVar22 = quotearg_n_style_colon(0,3,puVar36);
          error(0,*piVar18,&DAT_0010e514,uVar22);
          close((int)ppVar35);
LAB_00103fda:
          uVar31 = uVar31 + 1;
          ppVar25 = ppVar25 + 0xc;
        } while (local_250 == uVar31);
      } while( true );
    }
    do {
      if ((*(char *)&ppVar25[6].events == '\0') &&
         (iVar7 = strcmp((char *)*ppVar25,"-"), iVar7 == 0)) goto LAB_00103ec6;
      ppVar25 = ppVar25 + 0xc;
    } while (ppVar25 != local_208);
    lVar30 = 0;
    do {
      if ((-1 < *(int *)((long)&local_200[7].fd + lVar30)) &&
         (*(char *)((long)&local_200[6].events + lVar30 + 1) != '\0')) goto LAB_00103ec6;
      lVar30 = lVar30 + 0x60;
    } while (lVar30 != local_1f0);
    lVar30 = 0;
    while ((*(int *)((long)&local_200[7].fd + lVar30) < 0 ||
           (*(char *)((long)&local_200[6].events + lVar30 + 1) != '\0'))) {
      lVar30 = lVar30 + 0x60;
      if (lVar30 == local_1f0) goto LAB_00103ec6;
    }
    bVar32 = 0;
    ppVar25 = local_200;
    do {
      iVar7 = lstat((char *)*ppVar25,(stat *)&local_d8);
      if ((iVar7 == 0) && ((local_c0 & 0xf000) == 0xa000)) goto LAB_00103ec6;
      ppVar25 = ppVar25 + 0xc;
    } while (ppVar25 != local_208);
    lVar30 = 0;
    do {
      if (((-1 < *(int *)((long)&local_200[7].fd + lVar30)) &&
          (uVar8 = *(uint *)((long)&local_200[6].fd + lVar30) & 0xf000, uVar8 != 0x8000)) &&
         (uVar8 != 0x1000)) goto LAB_00103ec6;
      lVar30 = lVar30 + 0x60;
    } while (lVar30 != local_1f0);
    if ((local_209 == 0) && (follow_mode == 2)) goto LAB_00103ec6;
    uVar8 = inotify_init();
    ppVar35 = (pollfd *)(ulong)uVar8;
    if ((int)uVar8 < 0) {
LAB_00104c46:
      uVar22 = dcgettext(0,"inotify cannot be used, reverting to polling",5);
      piVar18 = __errno_location();
      error(0,*piVar18,uVar22);
      goto LAB_00103ec6;
    }
    iVar7 = fflush_unlocked(stdout);
    if (iVar7 != 0) {
      ppVar25 = (pollfd *)dcgettext(0,"write error",5);
      piVar18 = __errno_location();
      error(1,*piVar18,ppVar25);
LAB_00105299:
      uVar22 = pretty_name_isra_0(*ppVar25);
      uVar22 = quotearg_style(4,uVar22);
      uVar40 = dcgettext(0,"%s was replaced",5);
      piVar18 = __errno_location();
      error(0,*piVar18,uVar40,uVar22);
LAB_00104c2b:
      hash_free();
      close(uVar8);
      piVar18 = __errno_location();
      *piVar18 = 0;
      goto LAB_00104c46;
    }
    puVar36 = (undefined1 *)hash_initialize(local_250,0,wd_hasher,wd_comparator,0);
    if ((pollfd)puVar36 == (pollfd)0x0) goto LAB_0010510e;
    __mask = 0xc06;
    if (follow_mode != 1) {
      __mask = 2;
    }
    bVar6 = 0;
    bVar2 = 0;
    local_248 = (pollfd *)0x0;
    ppVar25 = local_200;
    do {
      iVar7 = follow_mode;
      if (*(char *)&ppVar25[6].events != '\0') goto LAB_001049c2;
      pVar38 = *ppVar25;
      sVar24 = strlen((char *)pVar38);
      *(undefined4 *)&ppVar25[8].events = 0xffffffff;
      if (sVar24 <= local_248) {
        sVar24 = (size_t)local_248;
      }
      if (iVar7 == 1) {
        pVar37 = (pollfd)dir_len(pVar38);
        cVar3 = *(char *)((long)pVar38 + (long)pVar37);
        lVar30 = last_component();
        pVar14 = (pollfd)&DAT_0010e0e1;
        ppVar25[10] = (pollfd)(lVar30 - (long)pVar38);
        *(char *)((long)pVar38 + (long)pVar37) = '\0';
        if (pVar37 != (pollfd)0x0) {
          pVar14 = *ppVar25;
        }
        iVar7 = inotify_add_watch(uVar8,(char *)pVar14,0x784);
        ppVar25[9].fd = iVar7;
        *(char *)((long)*ppVar25 + (long)pVar37) = cVar3;
        if (-1 < ppVar25[9].fd) goto LAB_00104a69;
LAB_00105222:
        piVar18 = __errno_location();
        if (*piVar18 != 0x1c) {
          uVar22 = quotearg_style(4,*ppVar25);
          uVar40 = dcgettext(0,"cannot watch parent directory of %s",5);
          error(0,*piVar18,uVar40,uVar22);
          goto LAB_00104c2b;
        }
LAB_00104c0a:
        pcVar34 = "inotify resources exhausted";
LAB_00104c16:
        uVar22 = dcgettext(0,pcVar34,5);
        error(0,0,uVar22);
        goto LAB_00104c2b;
      }
LAB_00104a69:
      iVar7 = inotify_add_watch(uVar8,(char *)*ppVar25,__mask);
      *(int *)&ppVar25[8].events = iVar7;
      local_248 = (pollfd *)sVar24;
      if (iVar7 < 0) {
        if (ppVar25[7].fd != -1) {
          bVar6 = bVar5;
        }
        puVar26 = (uint *)__errno_location();
        if ((*puVar26 & 0xffffffef) != 0xc) {
          if (*puVar26 != *(uint *)&ppVar25[7].events) {
            pVar37 = (pollfd)quotearg_style(4,*ppVar25);
            uVar22 = dcgettext(0,"cannot watch %s",5);
            error(0,*puVar26,uVar22,pVar37);
          }
          goto LAB_001049c2;
        }
        goto LAB_00104c0a;
      }
      lVar30 = hash_insert(puVar36);
      bVar2 = bVar5;
      if (lVar30 == 0) goto LAB_0010510e;
LAB_001049c2:
      ppVar25 = ppVar25 + 0xc;
    } while (local_208 != ppVar25);
    if (follow_mode == 2) {
      if (bVar6 == 0) {
        if (bVar2 == 0) goto LAB_00104bf4;
        goto LAB_00104c81;
      }
      goto LAB_00104c2b;
    }
LAB_00104c81:
    local_1d0 = (long)&local_200[-0xc].fd + local_1f0;
    ppVar25 = local_200;
    do {
      if (*(char *)&ppVar25[6].events == '\0') {
        if (follow_mode == 1) {
          recheck(ppVar25,0);
        }
        else if (((ppVar25[7].fd != -1) &&
                 (iVar7 = stat((char *)*ppVar25,(stat *)&local_d8), iVar7 == 0)) &&
                ((ppVar25[4] != (pollfd)CONCAT26(uStack210,CONCAT24(uStack212,local_d8)) ||
                 (ppVar25[5] != local_d0)))) goto LAB_00105299;
        check_fspec(ppVar25);
      }
      ppVar25 = ppVar25 + 0xc;
    } while (local_208 != ppVar25);
    lVar30 = (long)local_248 + 0x11;
    local_240 = (pollfd)xmalloc();
    iVar7 = 3;
    local_238 = 0;
    local_248 = (pollfd *)0x0;
LAB_00104d48:
    while (((pVar38 = pVar37, follow_mode == 1 && (reopen_inaccessible_files == 0)) &&
           (lVar28 = hash_get_n_entries(), lVar28 == 0))) {
      uVar22 = dcgettext(0,"no files remaining",5);
      error(1,0,uVar22);
LAB_00104ec2:
      check_fspec();
      pVar37 = pVar38;
    }
    if (local_248 < local_238) {
LAB_00104d65:
      pVar38 = (pollfd)((long)local_240 + (long)local_248);
      uVar33 = *(uint *)((long)pVar38 + 0xc);
      local_248 = (pollfd *)((long)local_248 + 0x10U + (ulong)uVar33);
      iVar9 = *(int *)pVar38;
      uVar10 = *(uint *)((long)pVar38 + 4);
      if ((uVar10 & 0x400) == 0) {
        if (uVar33 != 0) goto LAB_00104d9b;
LAB_00104e5c:
        local_184 = iVar9;
        ppVar25 = (pollfd *)hash_lookup();
        pVar37 = pVar38;
        if (ppVar25 != (pollfd *)0x0) {
LAB_00104e2d:
          if ((*(uint *)((long)pVar38 + 4) & 0xe04) == 0) goto LAB_00104ec2;
          if ((*(uint *)((long)pVar38 + 4) & 0x400) != 0) {
            inotify_rm_watch(uVar8,*(int *)&ppVar25[8].events);
            hash_remove(puVar36);
          }
          recheck();
          pVar37 = pVar38;
        }
      }
      else {
        if (uVar33 == 0) {
          uVar31 = 0;
          while (local_200[uVar31 * 0xc + 9].fd != iVar9) {
            uVar31 = uVar31 + 1;
            if (local_250 == uVar31) goto LAB_00104e5c;
          }
          pcVar34 = "directory containing watched file was removed";
          goto LAB_00104c16;
        }
LAB_00104d9b:
        uVar31 = 0;
        ppVar25 = local_200;
        do {
          if ((ppVar25[9].fd == iVar9) &&
             (iVar11 = strcmp((char *)((long)pVar38 + 0x10),
                              (char *)((long)ppVar25[10] + (long)*ppVar25)), iVar11 == 0)) {
            ppVar25 = local_200 + uVar31 * 0xc;
            if ((uVar10 & 0x200) == 0) goto LAB_00105030;
LAB_00104e20:
            if (follow_mode == 1) {
              recheck();
            }
            goto LAB_00104e2d;
          }
          uVar31 = uVar31 + 1;
          ppVar25 = ppVar25 + 0xc;
          pVar37 = (pollfd)puVar36;
        } while (local_250 != uVar31);
      }
      goto LAB_00104d48;
    }
    ppVar25 = (pollfd *)(ulong)bVar32;
    do {
      if (pid == 0) {
LAB_00104f4c:
        iVar9 = -1;
      }
      else {
        if ((char)ppVar25 != '\0') {
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        iVar9 = kill(pid,0);
        if ((iVar9 == 0) || (piVar18 = __errno_location(), *piVar18 == 1)) {
          iVar9 = 0;
          if (0.0 < (double)local_218) {
            if (DAT_0010e4a0 <= (double)local_218) goto LAB_00104f4c;
            iVar9 = (int)(DAT_0010e4a8 * (double)local_218);
            iVar9 = iVar9 + (uint)((double)iVar9 < DAT_0010e4a8 * (double)local_218);
          }
        }
        else {
          ppVar25 = (pollfd *)(ulong)bVar5;
          iVar9 = 0;
        }
      }
      uStack212 = 1;
      local_d0 = (pollfd)0x1;
      local_d8 = uVar8;
      iVar9 = __poll_chk((stat *)&local_d8,(ulong)monitor_output + 1,iVar9,0x10);
    } while (iVar9 == 0);
    bVar32 = (byte)ppVar25;
    if (iVar9 < 0) {
      uVar22 = dcgettext(0,"error waiting for inotify and output events",5);
      piVar18 = __errno_location();
      error(1,*piVar18,uVar22);
      goto LAB_00105222;
    }
    if (local_d0.revents != 0) {
LAB_00104bea:
      raise(0xd);
LAB_00104bf4:
                    /* WARNING: Subroutine does not return */
      exit(1);
    }
    local_238 = safe_read(ppVar35,local_240,lVar30);
    if (local_238 == 0) {
LAB_001051ab:
      if (iVar7 != 0) {
        lVar30 = lVar30 * 2;
        iVar7 = iVar7 + -1;
        local_240 = (pollfd)xrealloc();
        local_238 = 0;
        local_248 = (pollfd *)0x0;
        pVar37 = pVar38;
        goto LAB_00104d48;
      }
    }
    else {
      local_248 = (pollfd *)0x0;
      if (local_238 != 0xffffffffffffffff) goto LAB_00104d65;
      piVar18 = __errno_location();
      if (*piVar18 == 0x16) goto LAB_001051ab;
    }
    lVar30 = dcgettext(0,"error reading inotify event",5);
    piVar18 = __errno_location();
    error(1,*piVar18,lVar30);
LAB_00105030:
    iVar9 = inotify_add_watch(uVar8,(char *)*ppVar25,__mask);
    if (iVar9 < 0) {
      puVar26 = (uint *)__errno_location();
      if ((*puVar26 & 0xffffffef) == 0xc) goto LAB_00104c0a;
      uVar22 = quotearg_style(4,*ppVar25);
      uVar40 = dcgettext(0,"cannot watch %s",5);
      error(0,*puVar26,uVar40,uVar22);
    }
    iVar11 = *(int *)&ppVar25[8].events;
    if (iVar9 == iVar11) {
      if (-1 < iVar11) goto LAB_00104e20;
    }
    else if (-1 < iVar11) {
      inotify_rm_watch(uVar8,iVar11);
      hash_remove();
    }
    *(int *)&ppVar25[8].events = iVar9;
    pVar37 = pVar38;
    if (iVar9 != -1) {
      ppVar27 = (pollfd *)hash_remove(puVar36,ppVar25);
      if ((ppVar27 != (pollfd *)0x0) && (ppVar25 != ppVar27)) {
        if (follow_mode == 1) {
          recheck(ppVar27,0);
        }
        *(undefined4 *)&ppVar27[8].events = 0xffffffff;
        uVar22 = pretty_name_isra_0(*ppVar27);
        close_fd(ppVar27[7].fd,uVar22);
      }
      lVar28 = hash_insert();
      if (lVar28 == 0) {
LAB_0010510e:
                    /* WARNING: Subroutine does not return */
        xalloc_die();
      }
      goto LAB_00104e20;
    }
    goto LAB_00104d48;
  }
LAB_00102ae0:
  iVar7 = posix2_version();
  pcVar15 = *(char **)((long)param_2 + 8);
  pcVar34 = pcVar15 + 1;
  if (*pcVar15 == '+') {
    bVar5 = 1;
    if (0x2b8 < iVar7 - 0x30db0U) goto LAB_00102e4e;
    goto LAB_00102b02;
  }
  if ((*pcVar15 != '-') || ((0x30daf < iVar7 && (pcVar15[(ulong)(pcVar15[1] == 'c') + 1] == '\0'))))
  goto LAB_00102b02;
  bVar5 = 0;
LAB_00102e4e:
  cVar3 = pcVar15[1];
  pcVar15 = pcVar34;
  while ((int)cVar3 - 0x30U < 10) {
    pcVar13 = pcVar15 + 1;
    pcVar15 = pcVar15 + 1;
    cVar3 = *pcVar13;
  }
  if (cVar3 == 'c') {
    bVar32 = 0;
    pVar38 = (pollfd)0xa;
LAB_00103a89:
    pcVar13 = pcVar15 + 1;
  }
  else {
    if (cVar3 == 'l') {
      bVar32 = 1;
      pVar38 = (pollfd)0xa;
      goto LAB_00103a89;
    }
    if (cVar3 == 'b') {
      bVar32 = 0;
      pVar38 = (pollfd)0x1400;
      goto LAB_00103a89;
    }
    bVar32 = 1;
    pVar38 = (pollfd)0xa;
    pcVar13 = pcVar15;
  }
  cVar3 = *pcVar13;
  if (cVar3 == 'f') {
    pcVar13 = pcVar13 + 1;
  }
  if (*pcVar13 != '\0') goto LAB_00102b02;
  if ((pcVar34 == pcVar15) ||
     (uVar8 = xstrtoumax(pcVar34,0,10,&local_1d8), pVar38 = local_1d8, (uVar8 & 0xfffffffd) == 0)) {
    local_1d8 = pVar38;
    lVar30 = 1;
    from_start = bVar5;
    forever = cVar3 == 'f';
    count_lines = bVar32;
    goto LAB_00102b04;
  }
LAB_00104b97:
  uVar22 = quote(*(char **)((long)param_2 + 8));
  uVar40 = dcgettext(0,"invalid number",5);
  piVar18 = __errno_location();
  error(1,*piVar18,"%s: %s",uVar40,uVar22);
  goto LAB_00104bd9;
LAB_00104788:
  _Var23 = lseek(iVar7,_Var20,0);
  if (_Var23 < 0) {
                    /* WARNING: Subroutine does not return */
    xlseek_part_0(_Var20,0,local_220);
  }
LAB_001032d9:
  pcVar34 = (char *)xmalloc(0x2018);
  *(char **)((long)pcVar34 + 0x2008) = (char *)0x0;
  *(char **)((long)pcVar34 + 0x2000) = (char *)0x0;
  *(char **)((long)pcVar34 + 0x2010) = (char *)0x0;
  pVar14 = (pollfd)xmalloc(0x2018);
  local_230 = (pollfd)0x0;
  local_240 = (pollfd)pcVar34;
  while (puVar36 = (undefined1 *)pVar14, pcVar15 = (char *)safe_read(pVar37,puVar36,0x2000),
        pcVar15 + -1 < (char *)0xfffffffffffffffe) {
    *(char **)((long)puVar36 + 0x2000) = pcVar15;
    iVar7 = (int)line_end;
    param_2 = (uint *)((long)puVar36 + (long)pcVar15);
    local_1c8[0] = (pollfd)((long)local_1c8[0] + (long)pcVar15);
    *(char **)((long)puVar36 + 0x2008) = (char *)0x0;
    *(char **)((long)puVar36 + 0x2010) = (char *)0x0;
    pVar14 = (pollfd)puVar36;
    while (pvVar16 = memchr((void *)pVar14,iVar7,(long)param_2 - (long)pVar14),
          pvVar16 != (void *)0x0) {
      *(char **)((long)puVar36 + 0x2008) = *(char **)((long)puVar36 + 0x2008) + 1;
      pVar14 = (pollfd)((long)pvVar16 + 1);
    }
    local_230 = (pollfd)((long)local_230 + (long)*(char **)((long)puVar36 + 0x2008));
    if (pcVar15 + (long)*(char **)((long)local_240 + 0x2000) < (char *)0x2000) {
      memcpy(*(char **)((long)local_240 + 0x2000) + (long)local_240,puVar36,(size_t)pcVar15);
      *(char **)((long)local_240 + 0x2000) =
           *(char **)((long)local_240 + 0x2000) + (long)*(char **)((long)puVar36 + 0x2000);
      *(char **)((long)local_240 + 0x2008) =
           *(char **)((long)local_240 + 0x2008) + (long)*(char **)((long)puVar36 + 0x2008);
      param_2 = (uint *)local_240;
      pVar14 = (pollfd)puVar36;
    }
    else {
      *(undefined1 **)((long)local_240 + 0x2010) = puVar36;
      pVar17 = (pollfd)((long)local_230 - (long)*(char **)((long)pcVar34 + 0x2008));
      local_240 = (pollfd)puVar36;
      if ((ulong)pVar38 < (ulong)pVar17) {
        pVar14 = (pollfd)pcVar34;
        pcVar34 = (char *)*(pollfd *)((long)pcVar34 + 0x2010);
        local_230 = pVar17;
      }
      else {
        pVar14 = (pollfd)xmalloc();
      }
    }
  }
  free(puVar36);
  if (pcVar15 == (char *)0xffffffffffffffff) {
    puVar36 = (undefined1 *)quotearg_style(4,local_220);
    uVar22 = dcgettext(0,"error reading %s",5);
    piVar18 = __errno_location();
    error(0,*piVar18,uVar22);
    goto joined_r0x0010385a;
  }
  pcVar15 = *(char **)((long)local_240 + 0x2000);
  local_1e0 = (uint)(pcVar15 == (char *)0x0 || pVar38 == (pollfd)0x0);
  if (pcVar15 == (char *)0x0 || pVar38 == (pollfd)0x0) goto joined_r0x0010385a;
  uVar8 = (uint)line_end;
  param_2 = (uint *)(ulong)uVar8;
  if (((char *)((long)local_240 + -1))[(long)pcVar15] != line_end) {
    *(char **)((long)local_240 + 0x2008) = *(char **)((long)local_240 + 0x2008) + 1;
    local_230 = (pollfd)((long)local_230 + 1);
  }
  pVar14 = (pollfd)((long)local_230 - (long)*(char **)((long)pcVar34 + 0x2008));
  puVar36 = pcVar34;
  while (pVar17 = pVar14, (ulong)pVar38 < (ulong)pVar17) {
    puVar36 = *(undefined1 **)((long)puVar36 + 0x2010);
    local_230 = pVar17;
    pVar14 = (pollfd)((long)pVar17 - (long)*(char **)((long)puVar36 + 0x2008));
  }
  pcVar15 = *(char **)((long)puVar36 + 0x2000);
  pVar14 = (pollfd)puVar36;
  if ((ulong)pVar38 < (ulong)local_230) {
    lVar30 = (long)local_230 - (long)pVar38;
    do {
      pvVar16 = rawmemchr((void *)pVar14,uVar8);
      pVar14 = (pollfd)((long)pvVar16 + 1);
      lVar30 = lVar30 + -1;
    } while (lVar30 != 0);
  }
  if ((pollfd)((long)pcVar15 + (long)puVar36) != pVar14) goto LAB_0010415a;
  goto LAB_00103c69;
LAB_00102f45:
  if (reopen_inaccessible_files == 0) {
LAB_00103452:
    if (pid != 0) {
      if (forever == 0) {
        uVar22 = dcgettext(0,"warning: PID ignored; --pid=PID is useful only when following",5);
        error(0,0,uVar22);
      }
      else {
LAB_00102f76:
        iVar7 = kill(pid,0);
        if ((iVar7 != 0) && (piVar18 = __errno_location(), *piVar18 == 0x26)) {
          uVar22 = dcgettext(0,"warning: --pid=PID is not supported on this system",5);
          error(0,0,uVar22);
          pid = 0;
        }
      }
    }
  }
  else {
    if (forever == 0) {
      reopen_inaccessible_files = 0;
      pcVar34 = "warning: --retry ignored; --retry is useful only when following";
LAB_0010343d:
      uVar22 = dcgettext(0,pcVar34,5);
      error(0,0,uVar22);
      goto LAB_00103452;
    }
    if (follow_mode == 2) {
      pcVar34 = "warning: --retry only effective for the initial open";
      goto LAB_0010343d;
    }
    if (pid != 0) goto LAB_00102f76;
  }
  if ((from_start != 0) && (local_1d8 != (pollfd)0x0)) {
    local_1d8 = (pollfd)((long)local_1d8 + -1);
  }
  if (optind < param_1) {
    local_250 = (ulong)(param_1 - optind);
    ppuVar29 = ppcVar12 + optind;
  }
  else {
    local_250 = 1;
    ppuVar29 = &dummy_stdin_3;
  }
  bVar1 = false;
  param_2 = (uint *)((long)ppuVar29 + local_250 * 8);
  pVar37 = (pollfd)ppuVar29;
  do {
    if ((**(char **)pVar37 == '-') && ((*(char **)pVar37)[1] == '\0')) {
      bVar1 = true;
    }
    pVar37 = (pollfd)((long)pVar37 + 8);
  } while ((pollfd)param_2 != pVar37);
  if (bVar1) {
    if (follow_mode == 1) {
LAB_00104b5c:
      uVar22 = quotearg_style(4,&DAT_0010e0fc);
      uVar40 = dcgettext(0,"cannot follow %s by name",5);
      error(1,0,uVar40,uVar22);
      goto LAB_00104b97;
    }
    if (forever != 0) {
      if (((((pid != 0) || (local_250 != 1)) || (follow_mode != 2)) ||
          ((iVar7 = fstat(0,(stat *)&local_d8), iVar7 != 0 || ((local_c0 & 0xf000) == 0x8000)))) &&
         (iVar7 = isatty(0), iVar7 != 0)) {
        uVar22 = dcgettext(0,"warning: following standard input indefinitely is ineffective",5);
        error(0,0,uVar22);
      }
      goto LAB_0010306b;
    }
    if (local_1d8 != (pollfd)0x0) goto LAB_00103083;
  }
  else {
LAB_0010306b:
    if ((local_1d8 != (pollfd)0x0) || (forever != 0)) goto LAB_00103083;
  }
  if (from_start != 0) {
LAB_00103083:
    local_200 = (pollfd *)xnmalloc(local_250,0x60);
    ppVar25 = local_200;
    do {
      pVar37 = (pollfd)*ppuVar29;
      ppuVar29 = (undefined **)((long)ppuVar29 + 8);
      *ppVar25 = pVar37;
      ppVar25 = ppVar25 + 0xc;
    } while (ppuVar29 != (undefined **)param_2);
    if (((int)local_248 == 1) || (((int)local_248 == 0 && (local_250 != 1)))) {
      print_headers = '\x01';
    }
    local_1f0 = local_250 * 0x60;
    local_209 = 1;
    local_208 = local_200 + local_250 * 0xc;
    local_248 = local_200;
    goto LAB_001031e5;
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
LAB_00104bd9:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}