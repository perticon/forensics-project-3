void recheck(int32_t * f, bool blocking) {
    int64_t v1 = (int64_t)f;
    int64_t v2 = v1; // 0x6627
    int64_t v3 = __readfsqword(40); // 0x662a
    uint32_t v4 = (int32_t)*(char *)&v2 - 45; // 0x6640
    int64_t v5 = v4; // 0x6643
    if (v4 == 0) {
        // 0x6645
        v5 = (int64_t)*(char *)(v2 + 1);
    }
    int64_t v6 = blocking ? 0xffffffff : 0; // 0x6612
    char * v7 = (char *)(v1 + 54); // 0x6649
    char v8 = *v7; // 0x6649
    int32_t * v9 = (int32_t *)(v1 + 60); // 0x664d
    uint32_t v10 = *v9; // 0x664d
    int32_t v11 = v10; // 0x6657
    int64_t v12 = 0; // 0x6657
    if (v5 != 0) {
        uint32_t v13 = open_safer((char *)v2, 2048 * (int32_t)v6 & 0x7f800 ^ 2048); // 0x666c
        v11 = *v9;
        v12 = v13;
    }
    int64_t v14 = v10; // 0x664d
    int32_t * v15 = (int32_t *)(v1 + 56); // 0x6677
    int64_t v16; // 0x6610
    int64_t v17; // 0x6610
    int64_t v18; // 0x6610
    char v19; // 0x668b
    int32_t v20; // 0x6610
    if (*v15 == -1 == v11 == 0) {
        // 0x6ddd
        v2 = (int64_t)"valid_file_spec (f)";
        function_26c0();
        v17 = v14;
        goto lab_0x6dfc;
    } else {
        // 0x668b
        v19 = *(char *)&reopen_inaccessible_files;
        unsigned char v21 = *(char *)&disable_inotify; // 0x6693
        if (v19 != 0) {
            // 0x6788
            v18 = v21;
            v16 = v14;
            if (v12 == 0xffffffff) {
                goto lab_0x6918;
            } else {
                // 0x6792
                *v7 = 1;
                if (v21 == 0) {
                    // 0x6a30
                    if ((int32_t)function_2720() != 0) {
                        goto lab_0x67a3;
                    } else {
                        if ((v20 & (int32_t)&g58) != 0xa000) {
                            goto lab_0x67a3;
                        } else {
                            goto lab_0x6c10;
                        }
                    }
                } else {
                    goto lab_0x67a3;
                }
            }
        } else {
            // 0x66a3
            *v7 = 1;
            if (v21 == 0) {
                // 0x69f0
                if ((int32_t)function_2720() != 0) {
                    goto lab_0x66af;
                } else {
                    if ((v20 & (int32_t)&g58) != 0xa000) {
                        goto lab_0x66af;
                    } else {
                        goto lab_0x6c10;
                    }
                }
            } else {
                goto lab_0x66af;
            }
        }
    }
  lab_0x6dfc:
    // 0x6dfc
    function_2630();
    int64_t v22 = v17; // 0x6dfc
    goto lab_0x6e01;
  lab_0x6e01:;
    uint32_t v23 = (int32_t)v22 + 1; // 0x6e01
    int64_t v24 = v23; // 0x6e0c
    if (v23 != 0) {
        goto lab_0x6b83;
    } else {
        goto lab_0x6941;
    }
  lab_0x6918:
    // 0x6918
    *v7 = 0;
    int64_t v25; // 0x6610
    int64_t v26; // 0x6610
    int64_t v27; // 0x6610
    if ((char)v18 == 0) {
        // 0x6be0
        v25 = v16;
        if ((int32_t)function_2720() != 0) {
            goto lab_0x66b9;
        } else {
            // 0x6bf5
            v25 = v16;
            if ((v20 & (int32_t)&g58) != 0xa000) {
                goto lab_0x66b9;
            } else {
                goto lab_0x6c10;
            }
        }
    } else {
        // 0x6927
        *v9 = *(int32_t *)function_2530();
        v26 = v16;
        v27 = v2;
        goto lab_0x6931;
    }
  lab_0x66af:
    // 0x66af
    v25 = v14;
    if (v12 != 0xffffffff) {
        goto lab_0x67a3;
    } else {
        goto lab_0x66b9;
    }
  lab_0x6b83:
    // 0x6b83
    if (*(char *)&v2 == 45) {
        // 0x6b8b
        if (*(char *)(v2 + 1) == 0) {
            // 0x6b91
            function_2600();
        }
    }
    int64_t v28 = quotearg_style(); // 0x6bac
    function_2600();
    v2 = 0;
    function_28a0();
    v24 = v28;
    goto lab_0x6941;
  lab_0x6941:;
    int64_t v29 = v2; // 0x6944
    int64_t v30 = v24; // 0x6944
    goto lab_0x66f0;
  lab_0x67a3:
    // 0x67a3
    v2 = v12;
    v25 = v14;
    if ((int32_t)function_2980() < 0) {
        goto lab_0x66b9;
    } else {
        if ((v20 & (int32_t)&g58) - (int32_t)"sterTMCloneTable" < 0x2000) {
            goto lab_0x67dc;
        } else {
            if ((v20 & (int32_t)&g30) != 0x8000) {
                // 0x6b20
                *v9 = -1;
                *v7 = 0;
                if (v19 == 0) {
                    goto lab_0x6b58;
                } else {
                    // 0x6b30
                    if (follow_mode != 1) {
                        goto lab_0x6b58;
                    } else {
                        // 0x6b39
                        *(char *)(v1 + 52) = 0;
                        v22 = v14;
                        if (v8 == 0) {
                            goto lab_0x6e01;
                        } else {
                            goto lab_0x6b83;
                        }
                    }
                }
            } else {
                goto lab_0x67dc;
            }
        }
    }
  lab_0x66b9:;
    int64_t v58 = function_2530(); // 0x66b9
    int64_t v59 = v2; // 0x66c3
    int32_t v60 = *(int32_t *)v58; // 0x66c6
    *v9 = v60;
    v26 = v25;
    v27 = v59;
    int32_t v50; // 0x6610
    if (*v7 == 0) {
        goto lab_0x6931;
    } else {
        int32_t v61 = (int32_t)*(char *)&v2 - 45; // 0x66dd
        if (v61 == 0) {
            // 0x69a8
            v29 = v59;
            v30 = v25;
            v50 = (int32_t)*(char *)(v59 + 1);
            if (v60 == (int32_t)v25) {
                goto lab_0x66f0;
            } else {
                goto lab_0x69b5;
            }
        } else {
            // 0x66e6
            v29 = v59;
            v30 = v25;
            v50 = v61;
            if (v60 != (int32_t)v25) {
                goto lab_0x69b5;
            } else {
                goto lab_0x66f0;
            }
        }
    }
  lab_0x66f0:;
    int64_t v31 = v29;
    int64_t v32 = v31; // 0x66f3
    if (*(char *)v31 == 45) {
        // 0x66f5
        v32 = v31;
        if (*(char *)(v31 + 1) == 0) {
            // 0x66fb
            v32 = function_2600();
        }
    }
    // 0x6711
    v2 = v12;
    close_fd(v12, v32);
    int64_t v33 = v12; // 0x671f
    if (*(char *)&v2 == 45) {
        // 0x6721
        v33 = v12;
        if (*(char *)(v12 + 1) == 0) {
            // 0x6727
            v33 = function_2600();
        }
    }
    int64_t v34 = (int64_t)*v15; // 0x673d
    v2 = v34;
    close_fd(v34, v33);
    *v15 = -1;
    int64_t v35 = v30; // 0x6745
    goto lab_0x674c;
  lab_0x6931:;
    int32_t v55 = (int32_t)*(char *)&v2 - 45; // 0x6935
    int32_t v48; // 0x6610
    if (v55 == 0) {
        // 0x6950
        v24 = v26;
        v48 = (int32_t)*(char *)(v27 + 1);
        if (v8 == 0) {
            goto lab_0x6941;
        } else {
            goto lab_0x695c;
        }
    } else {
        // 0x693a
        v24 = v26;
        v48 = v55;
        if (v8 != 0) {
            goto lab_0x695c;
        } else {
            goto lab_0x6941;
        }
    }
  lab_0x6c10:;
    int64_t v56 = v2; // 0x6c10
    *v9 = -1;
    *(char *)(v1 + 52) = 1;
    if (*(char *)&v2 == 45) {
        // 0x6c23
        if (*(char *)(v56 + 1) == 0) {
            // 0x6c29
            function_2600();
        }
    }
    int64_t v57 = quotearg_style(); // 0x6c44
    function_2600();
    v2 = 0;
    function_28a0();
    v29 = 0;
    v30 = v57;
    goto lab_0x66f0;
  lab_0x674c:
    // 0x674c
    v17 = v35;
    if (v3 == __readfsqword(40)) {
        // 0x6763
        return;
    }
    goto lab_0x6dfc;
  lab_0x67dc:;
    int64_t v36 = v2; // 0x67dc
    int64_t v37 = v36; // 0x67e2
    if ((char)v36 == 45) {
        // 0x67e4
        v37 = v36;
        if (*(char *)(v36 + 1) == 0) {
            // 0x67ea
            v37 = function_2600();
        }
    }
    // 0x6800
    v2 = v12;
    int32_t v38 = v12; // 0x6803
    bool v39 = fremote(v38, (char *)v37); // 0x6803
    *(char *)(v1 + 53) = (char)v39;
    int64_t v40; // 0x6610
    if (v39 == *(char *)&disable_inotify == 0) {
        int64_t v41 = v2; // 0x6d18
        *v9 = -1;
        if (*(char *)&v2 == 45) {
            // 0x6d27
            if (*(char *)(v41 + 1) == 0) {
                // 0x6d2d
                function_2600();
            }
        }
        int64_t v42 = quotearg_style(); // 0x6d48
        function_2600();
        v2 = 0;
        function_28a0();
        *(int16_t *)(v1 + 52) = 257;
        v24 = v42;
        goto lab_0x6941;
    } else {
        // 0x681c
        *v9 = 0;
        int32_t v43 = *v15; // 0x6827
        if ((v10 & -3) == 0) {
            int64_t v44 = v2; // 0x6a68
            uint32_t v45 = (int32_t)v44 % 256 - 45; // 0x6a6e
            v40 = v45;
            if (v45 == 0) {
                // 0x6ca0
                v40 = (int64_t)*(char *)(v44 + 1);
            }
            // 0x6a77
            if (v43 == -1) {
                if (v40 == 0) {
                    // 0x6daa
                    function_2600();
                }
                // 0x6d8c
                v2 = 4;
                quotearg_style();
                goto lab_0x685e;
            } else {
                // 0x6a80
                int64_t v46; // 0x6610
                if (*(int64_t *)(v1 + 40) != v46) {
                    goto lab_0x6ab0;
                } else {
                    // 0x6a8b
                    if (*(int64_t *)(v1 + 32) != v46) {
                        goto lab_0x6ab0;
                    } else {
                        int64_t v47 = v44; // 0x6a98
                        if (v40 == 0) {
                            // 0x6dc2
                            v47 = function_2600();
                        }
                        // 0x6a9e
                        v2 = v12;
                        close_fd(v12, v47);
                        v35 = 0;
                        goto lab_0x674c;
                    }
                }
            }
        } else {
            if (v43 != -1) {
                // 0x6e17
                function_26c0();
                return;
            }
            // 0x6839
            if (*(char *)&v2 == 45) {
                // 0x6cb0
                if (*(char *)(v2 + 1) == 0) {
                    // 0x6cba
                    function_2600();
                }
            }
            // 0x6845
            v2 = 4;
            quotearg_style();
            goto lab_0x685e;
        }
    }
  lab_0x695c:
    // 0x695c
    if (v48 == 0) {
        // 0x6cf8
        function_2600();
    }
    int64_t v49 = quotearg_style(); // 0x696c
    function_2600();
    v2 = 0;
    function_28a0();
    v29 = 0;
    v30 = v49;
    goto lab_0x66f0;
  lab_0x69b5:
    // 0x69b5
    if (v50 == 0) {
        // 0x6c80
        v2 = 0;
        function_2600();
    }
    // 0x69bd
    quotearg_n_style_colon();
    v2 = 0;
    function_28a0();
    v29 = 0;
    v30 = v25;
    goto lab_0x66f0;
  lab_0x6b58:
    // 0x6b58
    *(char *)(v1 + 52) = 1;
    v24 = 0xffffffff;
    if (v8 == 0 == v10 == -1) {
        goto lab_0x6941;
    } else {
        // 0x6b6d
        v2 = 0;
        function_2600();
        goto lab_0x6b83;
    }
  lab_0x685e:
    // 0x685e
    function_2600();
    v2 = 0;
    function_28a0();
    goto lab_0x6876;
  lab_0x6ab0:
    if (v40 == 0) {
        // 0x6cd8
        function_2600();
    }
    // 0x6ab8
    quotearg_style();
    function_2600();
    v2 = 0;
    function_28a0();
    int64_t v53 = 0; // 0x6aef
    if (*(char *)&v2 == 45) {
        // 0x6af1
        v53 = 0;
        if (*(char *)1 == 0) {
            // 0x6af7
            v53 = function_2600();
        }
    }
    int64_t v54 = (int64_t)*v15; // 0x6b0d
    v2 = v54;
    close_fd(v54, v53);
    goto lab_0x6876;
  lab_0x6876:;
    int64_t v51 = v2; // 0x6881
    *v15 = v38;
    *(int64_t *)(v1 + 8) = 0;
    *(int32_t *)(v1 + 64) = v5 == 0 ? -1 : (int32_t)v6 % 256;
    *(int64_t *)(v1 + 88) = 0;
    *(char *)(v1 + 52) = 0;
    int64_t v52 = v51; // 0x68d4
    if (*(char *)&v2 == 45) {
        // 0x68d6
        v52 = v51;
        if (*(char *)(v51 + 1) == 0) {
            // 0x68dd
            v52 = function_2600();
        }
    }
    // 0x68f3
    v2 = v12;
    v35 = v52;
    if (function_26b0() >= 0) {
        goto lab_0x674c;
    } else {
        // 0x6908
        v2 = 0;
        v18 = xlseek_part_0(0, 0, v52);
        v16 = v52;
        goto lab_0x6918;
    }
}