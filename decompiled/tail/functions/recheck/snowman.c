void recheck(void** rdi, void** esi, ...) {
    void** r15d3;
    void** rbx4;
    void* rsp5;
    void** rdi6;
    void** rax7;
    void** v8;
    uint32_t ebp9;
    uint32_t eax10;
    void** r13_11;
    void** eax12;
    void** r12d13;
    void** eax14;
    void** rdx15;
    void** rsi16;
    void** rax17;
    void** rax18;
    void** rsi19;
    void** rax20;
    int64_t rdi21;
    void** rsi22;
    void** rax23;
    int64_t rdi24;
    void* rax25;
    uint32_t r14d26;
    uint32_t eax27;
    void** r15_28;
    void** rsi29;
    int32_t eax30;
    uint32_t v31;
    void** rax32;
    uint32_t r14d33;
    void** edx34;
    uint32_t eax35;
    void** rax36;
    void** rdi37;
    int32_t eax38;
    uint32_t v39;
    void** eax40;
    void** rdi41;
    int32_t eax42;
    uint32_t v43;
    uint32_t eax44;
    void** rax45;
    void** rax46;
    void** rsi47;
    void** rax48;
    void** rax49;
    uint32_t v50;
    int1_t zf51;
    void** rsi52;
    void** rax53;
    int64_t rdi54;
    signed char al55;
    void* rsp56;
    int1_t zf57;
    void** rsi58;
    void** rax59;
    void** rax60;
    void** rsi61;
    uint32_t edx62;
    void** rax63;
    void** rax64;
    void* rsp65;
    void** r13_66;
    void** rcx67;
    void* rsp68;
    void** r15d69;
    void** v70;
    void** v71;
    int64_t v72;
    void** v73;
    void** v74;
    void** rax75;
    int64_t rdi76;
    void** rax77;
    void** r8_78;
    void** r9_79;
    void** v80;
    int64_t v81;
    void** rax82;
    void** rax83;
    void* rsp84;
    void** rsi85;
    void** rax86;
    int64_t rdi87;
    void** rax88;
    int64_t rdi89;
    void** rsi90;
    void** rax91;
    void** rax92;

    r15d3 = esi;
    rbx4 = rdi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi6 = *reinterpret_cast<void***>(rdi);
    rax7 = g28;
    v8 = rax7;
    ebp9 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi6) - 45);
    if (!ebp9) {
        ebp9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 1));
    }
    eax10 = *reinterpret_cast<unsigned char*>(rbx4 + 54);
    r13_11 = *reinterpret_cast<void***>(rbx4 + 60);
    if (!ebp9) {
        eax12 = r13_11;
        r12d13 = reinterpret_cast<void**>(0);
    } else {
        eax14 = open_safer(rdi6);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        r12d13 = eax14;
        eax12 = *reinterpret_cast<void***>(rbx4 + 60);
    }
    *reinterpret_cast<unsigned char*>(&rdx15) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx4 + 56) == 0xffffffff);
    if (*reinterpret_cast<unsigned char*>(&rdx15) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax12 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_9:
            ++r13_11;
            if (r13_11) {
                addr_6b83_10:
                rsi16 = *reinterpret_cast<void***>(rbx4);
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi16) == 45) && !*reinterpret_cast<void***>(rsi16 + 1)) {
                    rax17 = fun_2600();
                    rsi16 = rax17;
                }
                rax18 = quotearg_style(4, rsi16, 4, rsi16);
                r13_11 = rax18;
                fun_2600();
                fun_28a0();
            }
            addr_6941_14:
            rsi19 = *reinterpret_cast<void***>(rbx4);
            addr_66f0_15:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi19) == 45) && !*reinterpret_cast<void***>(rsi19 + 1)) {
                rax20 = fun_2600();
                rsi19 = rax20;
            }
            *reinterpret_cast<void***>(&rdi21) = r12d13;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
            close_fd(rdi21, rsi19, rdi21, rsi19);
            rsi22 = *reinterpret_cast<void***>(rbx4);
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi22) == 45) && !*reinterpret_cast<void***>(rsi22 + 1)) {
                rax23 = fun_2600();
                rsi22 = rax23;
            }
            *reinterpret_cast<void***>(&rdi24) = *reinterpret_cast<void***>(rbx4 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
            close_fd(rdi24, rsi22, rdi24, rsi22);
            *reinterpret_cast<void***>(rbx4 + 56) = reinterpret_cast<void**>(0xffffffff);
            addr_674c_20:
            rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax25);
        return;
    }
    r14d26 = reopen_inaccessible_files;
    eax27 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d26)) {
        if (r12d13 == 0xffffffff) {
            addr_6918_24:
            *reinterpret_cast<unsigned char*>(rbx4 + 54) = 0;
            r15_28 = *reinterpret_cast<void***>(rbx4);
            if (!*reinterpret_cast<signed char*>(&eax27)) {
                rsi29 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 16);
                eax30 = fun_2720(r15_28, rsi29, r15_28, rsi29);
                if (eax30 || (v31 & 0xf000) != 0xa000) {
                    addr_66b9_26:
                    rax32 = fun_2530();
                    r14d33 = *reinterpret_cast<unsigned char*>(rbx4 + 54);
                    rsi19 = *reinterpret_cast<void***>(rbx4);
                    edx34 = *reinterpret_cast<void***>(rax32);
                    r15_28 = rsi19;
                    *reinterpret_cast<void***>(rbx4 + 60) = edx34;
                    if (*reinterpret_cast<signed char*>(&r14d33)) {
                        eax35 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi19) - 45);
                        if (!eax35) {
                            eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi19 + 1));
                            if (edx34 == r13_11) 
                                goto addr_66f0_15;
                        } else {
                            if (edx34 == r13_11) {
                                goto addr_66f0_15;
                            }
                        }
                        if (!eax35) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi19 = *reinterpret_cast<void***>(rbx4);
                        goto addr_66f0_15;
                    }
                } else {
                    goto addr_6c10_35;
                }
            } else {
                rax36 = fun_2530();
                *reinterpret_cast<void***>(rbx4 + 60) = *reinterpret_cast<void***>(rax36);
            }
        } else {
            *reinterpret_cast<unsigned char*>(rbx4 + 54) = 1;
            if (*reinterpret_cast<signed char*>(&eax27) || ((rdi37 = *reinterpret_cast<void***>(rbx4), eax38 = fun_2720(rdi37, reinterpret_cast<int64_t>(rsp5) + 16), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8), !!eax38) || (v39 & 0xf000) != 0xa000)) {
                addr_67a3_38:
                eax40 = fun_2980();
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                if (reinterpret_cast<signed char>(eax40) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_26; else 
                    goto addr_67b3_39;
            } else {
                goto addr_6c10_35;
            }
        }
    } else {
        *reinterpret_cast<unsigned char*>(rbx4 + 54) = 1;
        if (*reinterpret_cast<signed char*>(&eax27) || ((rdi41 = *reinterpret_cast<void***>(rbx4), eax42 = fun_2720(rdi41, reinterpret_cast<int64_t>(rsp5) + 16), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8), !!eax42) || (v43 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d13 == 0xffffffff)) 
                goto addr_66b9_26;
            goto addr_67a3_38;
        } else {
            goto addr_6c10_35;
        }
    }
    eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_28) - 45);
    if (!eax44) {
        eax44 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_28 + 1));
        if (!*reinterpret_cast<signed char*>(&eax10)) 
            goto addr_6941_14;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax10)) 
            goto addr_6941_14;
    }
    if (!eax44) {
        rax45 = fun_2600();
        r15_28 = rax45;
    }
    rax46 = quotearg_style(4, r15_28, 4, r15_28);
    r13_11 = rax46;
    fun_2600();
    fun_28a0();
    rsi19 = *reinterpret_cast<void***>(rbx4);
    goto addr_66f0_15;
    addr_6c10_35:
    rsi47 = *reinterpret_cast<void***>(rbx4);
    *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(1);
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi47) == 45) && !*reinterpret_cast<void***>(rsi47 + 1)) {
        rax48 = fun_2600();
        rsi47 = rax48;
    }
    rax49 = quotearg_style(4, rsi47, 4, rsi47);
    r13_11 = rax49;
    fun_2600();
    fun_28a0();
    rsi19 = *reinterpret_cast<void***>(rbx4);
    goto addr_66f0_15;
    addr_67b3_39:
    if ((v50 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v50 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
        *reinterpret_cast<unsigned char*>(rbx4 + 54) = 0;
        if (!*reinterpret_cast<signed char*>(&r14d26) || (zf51 = follow_mode == 1, !zf51)) {
            *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(1);
            if (*reinterpret_cast<signed char*>(&eax10)) 
                goto addr_6b6d_55;
            if (r13_11 == 0xffffffff) 
                goto addr_6941_14;
            addr_6b6d_55:
            fun_2600();
            goto addr_6b83_10;
        } else {
            *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0);
            if (!*reinterpret_cast<signed char*>(&eax10)) 
                goto addr_6e01_9;
            goto addr_6b83_10;
        }
    }
    rsi52 = *reinterpret_cast<void***>(rbx4);
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi52) == 45) && !*reinterpret_cast<void***>(rsi52 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx15) = 5;
        *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
        rax53 = fun_2600();
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        rsi52 = rax53;
    }
    *reinterpret_cast<void***>(&rdi54) = r12d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
    al55 = fremote(rdi54, rsi52, rdx15);
    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    *reinterpret_cast<signed char*>(rbx4 + 53) = al55;
    if (al55 && (zf57 = disable_inotify == 0, zf57)) {
        rsi58 = *reinterpret_cast<void***>(rbx4);
        *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi58) == 45) && !*reinterpret_cast<void***>(rsi58 + 1)) {
            rax59 = fun_2600();
            rsi58 = rax59;
        }
        rax60 = quotearg_style(4, rsi58, 4, rsi58);
        r13_11 = rax60;
        fun_2600();
        fun_28a0();
        *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0x101);
        goto addr_6941_14;
    }
    r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) & 0xfffffffd);
    *reinterpret_cast<void***>(rbx4 + 60) = reinterpret_cast<void**>(0);
    if (r13_11) 
        goto addr_6830_66;
    rsi61 = *reinterpret_cast<void***>(rbx4);
    edx62 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi61) - 45);
    if (!edx62) {
        edx62 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi61 + 1));
    }
    if (*reinterpret_cast<void***>(rbx4 + 56) != 0xffffffff) 
        goto addr_6a80_70;
    if (!edx62) {
        rax63 = fun_2600();
        rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        rsi61 = rax63;
    }
    rax64 = quotearg_style(4, rsi61, 4, rsi61);
    rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
    r13_66 = rax64;
    addr_685e_74:
    fun_2600();
    rcx67 = r13_66;
    fun_28a0();
    rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8 - 8 + 8);
    addr_6876_75:
    r15d69 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r15d3)));
    r13_11 = *reinterpret_cast<void***>(rbx4);
    if (!ebp9) {
        r15d69 = reinterpret_cast<void**>(0xffffffff);
    }
    *reinterpret_cast<void***>(rbx4 + 56) = r12d13;
    *reinterpret_cast<void***>(rbx4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx4 + 16) = v70;
    *reinterpret_cast<void***>(rbx4 + 64) = r15d69;
    *reinterpret_cast<void***>(rbx4 + 24) = v71;
    *reinterpret_cast<void***>(rbx4 + 88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int64_t*>(rbx4 + 32) = v72;
    *reinterpret_cast<void***>(rbx4 + 52) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx4 + 40) = v73;
    *reinterpret_cast<void***>(rbx4 + 48) = v74;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_11) == 45) && !*reinterpret_cast<void***>(r13_11 + 1)) {
        rax75 = fun_2600();
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8);
        r13_11 = rax75;
    }
    *reinterpret_cast<void***>(&rdi76) = r12d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi76) + 4) = 0;
    rax77 = fun_26b0(rdi76);
    if (reinterpret_cast<signed char>(rax77) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_20;
    *reinterpret_cast<signed char*>(&eax27) = xlseek_part_0(0, 0, r13_11, rcx67, r8_78, r9_79);
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8);
    goto addr_6918_24;
    addr_6a80_70:
    if (*reinterpret_cast<void***>(rbx4 + 40) != v80 || *reinterpret_cast<int64_t*>(rbx4 + 32) != v81) {
        if (!edx62) {
            rax82 = fun_2600();
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rsi61 = rax82;
        }
        rax83 = quotearg_style(4, rsi61, 4, rsi61);
        fun_2600();
        rcx67 = rax83;
        fun_28a0();
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi85 = *reinterpret_cast<void***>(rbx4);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi85) == 45) && !*reinterpret_cast<void***>(rsi85 + 1)) {
            rax86 = fun_2600();
            rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
            rsi85 = rax86;
        }
        *reinterpret_cast<void***>(&rdi87) = *reinterpret_cast<void***>(rbx4 + 56);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi87) + 4) = 0;
        close_fd(rdi87, rsi85, rdi87, rsi85);
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
        goto addr_6876_75;
    } else {
        if (!edx62) {
            rax88 = fun_2600();
            rsi61 = rax88;
        }
        *reinterpret_cast<void***>(&rdi89) = r12d13;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi89) + 4) = 0;
        close_fd(rdi89, rsi61, rdi89, rsi61);
        goto addr_674c_20;
    }
    addr_6830_66:
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx4 + 56) == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi90 = *reinterpret_cast<void***>(rbx4);
        if (*reinterpret_cast<void***>(rsi90) == 45 && !*reinterpret_cast<void***>(rsi90 + 1)) {
            rax91 = fun_2600();
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rsi90 = rax91;
        }
        rax92 = quotearg_style(4, rsi90, 4, rsi90);
        rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        r13_66 = rax92;
        goto addr_685e_74;
    }
}