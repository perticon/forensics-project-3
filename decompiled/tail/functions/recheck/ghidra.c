void recheck(byte **param_1,uint param_2)

{
  char cVar1;
  char cVar2;
  char cVar3;
  int __fd;
  uint uVar4;
  int iVar5;
  uint *puVar6;
  byte *pbVar7;
  __off_t _Var8;
  int *piVar9;
  undefined8 uVar10;
  undefined8 uVar11;
  uint uVar12;
  uint uVar13;
  char *pcVar14;
  long in_FS_OFFSET;
  stat local_d8;
  long local_40;
  
  pbVar7 = *param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar13 = *pbVar7 - 0x2d;
  if (uVar13 == 0) {
    uVar13 = (uint)pbVar7[1];
  }
  cVar3 = *(char *)((long)param_1 + 0x36);
  uVar12 = *(uint *)((long)param_1 + 0x3c);
  if (uVar13 == 0) {
    __fd = 0;
    uVar4 = uVar12;
  }
  else {
    __fd = open_safer(pbVar7,((param_2 ^ 1) & 0xff) << 0xb);
    uVar4 = *(uint *)((long)param_1 + 0x3c);
  }
  cVar2 = reopen_inaccessible_files;
  cVar1 = disable_inotify;
  if ((*(int *)(param_1 + 7) == -1) == (uVar4 == 0)) {
                    /* WARNING: Subroutine does not return */
    __assert_fail("valid_file_spec (f)","src/tail.c",0x3e2,"recheck");
  }
  if (reopen_inaccessible_files == '\0') {
    *(undefined *)((long)param_1 + 0x36) = 1;
    if ((cVar1 == '\0') &&
       ((iVar5 = lstat((char *)*param_1,&local_d8), iVar5 == 0 &&
        ((local_d8.st_mode & 0xf000) == 0xa000)))) goto LAB_00106c10;
    if (__fd == -1) {
LAB_001066b9:
      puVar6 = (uint *)__errno_location();
      pbVar7 = *param_1;
      uVar13 = *puVar6;
      *(uint *)((long)param_1 + 0x3c) = uVar13;
      if (*(char *)((long)param_1 + 0x36) == '\0') goto LAB_00106931;
      uVar4 = *pbVar7 - 0x2d;
      if (uVar4 == 0) {
        uVar4 = (uint)pbVar7[1];
      }
      if (uVar13 != uVar12) {
        if (uVar4 == 0) {
          pbVar7 = (byte *)dcgettext(0,"standard input",5);
        }
        uVar11 = quotearg_n_style_colon(0,3,pbVar7);
        error(0,*puVar6,&DAT_0010e514,uVar11);
        pbVar7 = *param_1;
      }
    }
    else {
LAB_001067a3:
      iVar5 = fstat(__fd,&local_d8);
      if (iVar5 < 0) goto LAB_001066b9;
      if ((((local_d8.st_mode & 0xf000) - 0x1000 & 0xffffe000) == 0) ||
         ((local_d8.st_mode & 0xb000) == 0x8000)) {
        pbVar7 = *param_1;
        if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
          pbVar7 = (byte *)dcgettext(0,"standard input",5);
        }
        cVar3 = fremote(__fd,pbVar7);
        *(char *)((long)param_1 + 0x35) = cVar3;
        if ((cVar3 == '\0') || (disable_inotify != '\0')) {
          *(undefined4 *)((long)param_1 + 0x3c) = 0;
          if ((uVar12 & 0xfffffffd) == 0) {
            pbVar7 = *param_1;
            uVar12 = *pbVar7 - 0x2d;
            if (uVar12 == 0) {
              uVar12 = (uint)pbVar7[1];
            }
            if (*(int *)(param_1 + 7) == -1) {
              if (uVar12 == 0) {
                pbVar7 = (byte *)dcgettext(0,"standard input",5);
              }
              uVar11 = quotearg_style(4,pbVar7);
              pcVar14 = "%s has appeared;  following new file";
              goto LAB_0010685e;
            }
            if ((param_1[5] == (byte *)local_d8.st_ino) && (param_1[4] == (byte *)local_d8.st_dev))
            {
              if (uVar12 == 0) {
                pbVar7 = (byte *)dcgettext(0,"standard input",5);
              }
              close_fd(__fd,pbVar7);
              goto LAB_0010674c;
            }
            if (uVar12 == 0) {
              pbVar7 = (byte *)dcgettext(0,"standard input",5);
            }
            uVar11 = quotearg_style(4,pbVar7);
            uVar10 = dcgettext(0,"%s has been replaced;  following new file",5);
            error(0,0,uVar10,uVar11);
            if ((**param_1 == 0x2d) && ((*param_1)[1] == 0)) {
              dcgettext(0,"standard input",5);
            }
            close_fd();
          }
          else {
            if (*(int *)(param_1 + 7) != -1) {
                    /* WARNING: Subroutine does not return */
              __assert_fail("f->fd == -1","src/tail.c",0x42f,"recheck");
            }
            pbVar7 = *param_1;
            if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
              pbVar7 = (byte *)dcgettext(0,"standard input",5);
            }
            uVar11 = quotearg_style(4,pbVar7);
            pcVar14 = "%s has become accessible";
LAB_0010685e:
            uVar10 = dcgettext(0,pcVar14,5);
            error(0,0,uVar10,uVar11);
          }
          param_2 = param_2 & 0xff;
          pbVar7 = *param_1;
          if (uVar13 == 0) {
            param_2 = 0xffffffff;
          }
          *(int *)(param_1 + 7) = __fd;
          param_1[1] = (byte *)0x0;
          param_1[2] = (byte *)local_d8.st_mtim.tv_sec;
          *(uint *)(param_1 + 8) = param_2;
          param_1[3] = (byte *)local_d8.st_mtim.tv_nsec;
          param_1[0xb] = (byte *)0x0;
          param_1[4] = (byte *)local_d8.st_dev;
          *(undefined *)((long)param_1 + 0x34) = 0;
          param_1[5] = (byte *)local_d8.st_ino;
          *(__mode_t *)(param_1 + 6) = local_d8.st_mode;
          if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
            pbVar7 = (byte *)dcgettext(0,"standard input",5);
          }
          _Var8 = lseek(__fd,0,0);
          if (_Var8 < 0) {
                    /* WARNING: Subroutine does not return */
            xlseek_part_0(0,0,pbVar7);
          }
          goto LAB_0010674c;
        }
        pbVar7 = *param_1;
        *(undefined4 *)((long)param_1 + 0x3c) = 0xffffffff;
        if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
          pbVar7 = (byte *)dcgettext(0,"standard input",5);
        }
        uVar11 = quotearg_style(4,pbVar7);
        uVar10 = dcgettext(0,"%s has been replaced with an untailable remote file",5);
        error(0,0,uVar10,uVar11);
        *(undefined2 *)((long)param_1 + 0x34) = 0x101;
      }
      else {
        *(undefined4 *)((long)param_1 + 0x3c) = 0xffffffff;
        *(undefined *)((long)param_1 + 0x36) = 0;
        if ((cVar2 == '\0') || (follow_mode != 1)) {
          *(undefined *)((long)param_1 + 0x34) = 1;
          if ((cVar3 == '\0') && (uVar12 == 0xffffffff)) goto LAB_00106941;
          pcVar14 = (char *)dcgettext(0,"; giving up on this name",5);
        }
        else {
          *(undefined *)((long)param_1 + 0x34) = 0;
          if (cVar3 == '\0') {
            pcVar14 = "";
            if (uVar12 == 0xffffffff) goto LAB_00106941;
          }
          else {
            pcVar14 = "";
          }
        }
        pbVar7 = *param_1;
        if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
          pbVar7 = (byte *)dcgettext(0,"standard input",5);
        }
        uVar11 = quotearg_style(4,pbVar7);
        uVar10 = dcgettext(0,"%s has been replaced with an untailable file%s",5);
        error(0,0,uVar10,uVar11,pcVar14);
      }
LAB_00106941:
      pbVar7 = *param_1;
    }
  }
  else {
    if (__fd == -1) {
      *(undefined *)((long)param_1 + 0x36) = 0;
      pbVar7 = *param_1;
      if (cVar1 != '\0') {
        piVar9 = __errno_location();
        *(int *)((long)param_1 + 0x3c) = *piVar9;
LAB_00106931:
        uVar13 = *pbVar7 - 0x2d;
        if (uVar13 == 0) {
          uVar13 = (uint)pbVar7[1];
        }
        if (cVar3 == '\0') goto LAB_00106941;
        if (uVar13 == 0) {
          pbVar7 = (byte *)dcgettext(0,"standard input",5);
        }
        uVar11 = quotearg_style(4,pbVar7);
        uVar10 = dcgettext(0,"%s has become inaccessible",5);
        error(0,*(undefined4 *)((long)param_1 + 0x3c),uVar10,uVar11);
        pbVar7 = *param_1;
        goto LAB_001066f0;
      }
      iVar5 = lstat((char *)pbVar7,&local_d8);
      if ((iVar5 != 0) || ((local_d8.st_mode & 0xf000) != 0xa000)) goto LAB_001066b9;
    }
    else {
      *(undefined *)((long)param_1 + 0x36) = 1;
      if ((cVar1 != '\0') ||
         ((iVar5 = lstat((char *)*param_1,&local_d8), iVar5 != 0 ||
          ((local_d8.st_mode & 0xf000) != 0xa000)))) goto LAB_001067a3;
    }
LAB_00106c10:
    pbVar7 = *param_1;
    *(undefined4 *)((long)param_1 + 0x3c) = 0xffffffff;
    *(undefined *)((long)param_1 + 0x34) = 1;
    if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
      pbVar7 = (byte *)dcgettext(0,"standard input",5);
    }
    uVar11 = quotearg_style(4,pbVar7);
    uVar10 = dcgettext(0,"%s has been replaced with an untailable symbolic link",5);
    error(0,0,uVar10,uVar11);
    pbVar7 = *param_1;
  }
LAB_001066f0:
  if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
    pbVar7 = (byte *)dcgettext(0,"standard input",5);
  }
  close_fd(__fd,pbVar7);
  pbVar7 = *param_1;
  if ((*pbVar7 == 0x2d) && (pbVar7[1] == 0)) {
    pbVar7 = (byte *)dcgettext(0,"standard input",5);
  }
  close_fd(*(undefined4 *)(param_1 + 7),pbVar7);
  *(undefined4 *)(param_1 + 7) = 0xffffffff;
LAB_0010674c:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}