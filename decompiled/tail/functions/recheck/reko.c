word32 recheck(word32 esi, Eq_24 rdi, struct Eq_542 * fs, union Eq_1044 & rdxOut)
{
	ptr64 fp;
	ui32 dwLocC0;
	word64 qwLocD0;
	Eq_77 qwLocD8;
	word64 qwLoc80;
	word64 qwLoc78;
	Eq_24 rsi;
	Eq_24 rdi_28 = *rdi;
	uint64 r15_9 = (uint64) esi;
	uint64 rbp_1832 = (uint64) ((word32) *rdi_28 - 0x2D);
	word32 r15d_67 = (word32) r15_9;
	word64 rax_30 = fs->qw0028;
	if ((word32) rbp_1832 == 0x00)
		rbp_1832 = (uint64) *((word128) rdi_28 + 1);
	uint64 r12_1833;
	Eq_24 eax_103;
	Eq_24 r13_49 = (uint64) *((word128) rdi + 60);
	word32 ebp_53 = (word32) rbp_1832;
	byte al_50 = *((word128) rdi + 54);
	Eq_24 r13d_58 = (word32) r13_49;
	byte r15b_700 = (byte) r15_9;
	if (ebp_53 != 0x00)
	{
		r12_1833 = (uint64) open_safer((uint64) ((word32) ((byte) r15d_67 ^ 0x01) << 11), rdi_28, fs);
		eax_103 = *((word128) rdi + 60);
	}
	else
	{
		eax_103 = r13d_58;
		r12_1833 = 0x00;
	}
	Eq_77 r12d_124 = (word32) r12_1833;
	Eq_24 r13_284 = r13_49;
	if ((int8) (*((word128) rdi + 56) == ~0x00) == (int8) (eax_103 == 0x00))
		fn00000000000026C0("recheck", 994, "src/tail.c", "valid_file_spec (f)");
	Eq_1044 rdx_1201;
	Eq_24 rsi_1040;
	Eq_24 r15_1070;
	Eq_5732 eax_1837 = (uint64) g_b1310C;
	byte r14b_120 = g_b1311A;
	byte al_131 = (byte) eax_1837;
	word32 eax_138 = (word32) eax_1837;
	if (r14b_120 == 0x00)
	{
		*((word128) rdi + 54) = 0x01;
		if (al_131 == 0x00)
		{
			fn0000000000002720();
			if (eax_138 == 0x00 && (dwLocC0 & 0xF000) == 0xA000)
				goto l0000000000006C10;
		}
		r13_284 = r13_49;
		if (r12d_124 == ~0x00)
			goto l00000000000066B9;
	}
	else
	{
		if (r12d_124 == ~0x00)
			goto l0000000000006918;
		*((word128) rdi + 54) = 0x01;
		if (al_131 == 0x00)
		{
			fn0000000000002720();
			if (eax_138 == 0x00 && (dwLocC0 & 0xF000) == 0xA000)
				goto l0000000000006C10;
		}
	}
	r13_284 = r13_49;
	ui32 r13d_393 = (word32) r13_49;
	if (fn0000000000002980(fp - 0xD8, r12d_124) < 0x00)
		goto l00000000000066B9;
	if (((dwLocC0 & 0xF000) - 0x1000 & ~0x1FFF) != 0x00 && (dwLocC0 & 0xB000) != 0x8000)
	{
		((word128) rdi + 60)->u0 = ~0x00;
		*((word128) rdi + 54) = 0x00;
		if (r14b_120 != 0x00 && g_dw13020 == 0x01)
		{
			*((word128) rdi + 52) = 0x00;
			if (al_50 == 0x00 && r13d_58 == 0x01)
				goto l0000000000006941;
		}
		else
		{
			*((word128) rdi + 52) = 0x01;
			if (al_50 == 0x00 && r13d_58 == ~0x00)
				goto l0000000000006941;
			fn0000000000002600(0x05, "; giving up on this name", null);
		}
		Eq_24 rsi_1225 = *rdi;
		if (*rsi_1225 == 0x2D && *((word128) rsi_1225 + 1) == 0x00)
			fn0000000000002600(0x05, "standard input", null);
		quotearg_style(rsi, 0x04, fs);
		fn00000000000028A0(fn0000000000002600(0x05, "%s has been replaced with an untailable file%s", null), 0x00, 0x00);
l0000000000006941:
		rsi_1040 = *rdi;
		goto l00000000000066F0;
	}
	Eq_24 rsi_242 = *rdi;
	if (*rsi_242 == 0x2D && *((word128) rsi_242 + 1) == 0x00)
		fn0000000000002600(0x05, "standard input", null);
	word64 rdx_1844;
	byte al_323 = fremote((word32) r12_1833, fs, out rdx_1844);
	*((word128) rdi + 53) = al_323;
	if (al_323 != 0x00 && g_b1310C == 0x00)
	{
		Eq_24 rsi_330 = *rdi;
		((word128) rdi + 60)->u0 = ~0x00;
		if (*rsi_330 == 0x2D && *((word128) rsi_330 + 1) == 0x00)
			fn0000000000002600(0x05, "standard input", null);
		quotearg_style(rsi, 0x04, fs);
		fn00000000000028A0(fn0000000000002600(0x05, "%s has been replaced with an untailable remote file", null), 0x00, 0x00);
		*((word128) rdi + 52) = 0x0101;
		goto l0000000000006941;
	}
	char * rsi_474;
	((word128) rdi + 60)->u0 = 0x00;
	Eq_77 eax_416 = *((word128) rdi + 56);
	if ((r13d_393 & ~0x02) != 0x00)
	{
		if (eax_416 != ~0x00)
			fn00000000000026C0("recheck", 1071, "src/tail.c", "f->fd == -1");
		Eq_24 rsi_631 = *rdi;
		if (*rsi_631 == 0x2D && *((word128) rsi_631 + 1) == 0x00)
			fn0000000000002600(0x05, "standard input", null);
		quotearg_style(rsi, 0x04, fs);
		rsi_474 = (char *) "%s has become accessible";
	}
	else
	{
		Eq_24 rsi_404 = *rdi;
		word32 edx_410 = (word32) *rsi_404 - 0x2D;
		if (edx_410 == 0x00)
			edx_410 = (word32) *((word128) rsi_404 + 1);
		if (eax_416 != ~0x00)
		{
			if (*((word128) rdi + 40) == qwLocD0 && *((word128) rdi + 32) == qwLocD8)
			{
				if (edx_410 == 0x00)
					fn0000000000002600(0x05, "standard input", null);
				rdx_1201 = close_fd((uint64) r12d_124);
l000000000000674C:
				word64 rax_1212 = rax_30 - fs->qw0028;
				if (rax_1212 != 0x00)
					fn0000000000002630();
				else
				{
					rdxOut = rdx_1201;
					return (word32) rax_1212;
				}
			}
			if (edx_410 == 0x00)
				fn0000000000002600(0x05, "standard input", null);
			quotearg_style(rsi, 0x04, fs);
			fn00000000000028A0(fn0000000000002600(0x05, "%s has been replaced;  following new file", null), 0x00, 0x00);
			Eq_24 rsi_550 = *rdi;
			if (*rsi_550 == 0x2D && *((word128) rsi_550 + 1) == 0x00)
				fn0000000000002600(0x05, "standard input", null);
			close_fd((uint64) *((word128) rdi + 56));
l0000000000006876:
			word32 r15d_712 = (word32) r15b_700;
			r13_284 = *rdi;
			if (ebp_53 == 0x00)
				r15d_712 = ~0x00;
			*((word128) rdi + 56) = r12d_124;
			*((word128) rdi + 8) = 0x00;
			*((word128) rdi + 16) = qwLoc80;
			*((word128) rdi + 64) = r15d_712;
			*((word128) rdi + 24) = qwLoc78;
			*((word128) rdi + 88) = 0x00;
			*((word128) rdi + 32) = qwLocD8;
			*((word128) rdi + 52) = 0x00;
			*((word128) rdi + 40) = qwLocD0;
			*((word128) rdi + 48) = dwLocC0;
			if (*r13_284 == 0x2D && *((word128) r13_284 + 1) == 0x00)
				r13_284 = fn0000000000002600(0x05, "standard input", null);
			rdx_1201.u0 = 0x00;
			if (fn00000000000026B0(0x00, 0x00, r12d_124) < 0x00)
			{
				word64 rdi_1846;
				word32 esi_1845;
				eax_1837.u0 = (uint64) xlseek.part.0(0x00, 0x00, out esi_1845, out rdi_1846);
l0000000000006918:
				*((word128) rdi + 54) = 0x00;
				r15_1070 = *rdi;
				if ((byte) eax_1837 != 0x00)
				{
					*((word128) rdi + 60) = *fn0000000000002530();
l0000000000006931:
					word32 eax_949 = (word32) *r15_1070 - 0x2D;
					if (eax_949 != 0x00)
					{
						if (al_50 == 0x00)
							goto l0000000000006941;
					}
					else
					{
						eax_949 = (word32) *((word128) r15_1070 + 1);
						if (al_50 == 0x00)
							goto l0000000000006941;
					}
					if (eax_949 == 0x00)
						r15_1070 = fn0000000000002600(0x05, "standard input", null);
					quotearg_style(r15_1070, 0x04, fs);
					fn00000000000028A0(fn0000000000002600(0x05, "%s has become inaccessible", null), *((word128) rdi + 60), 0x00);
					rsi_1040 = *rdi;
					goto l00000000000066F0;
				}
				fn0000000000002720();
				if (eax_1837 == 0x00 && (dwLocC0 & 0xF000) == 0xA000)
				{
l0000000000006C10:
					Eq_24 rsi_842 = *rdi;
					((word128) rdi + 60)->u0 = ~0x00;
					*((word128) rdi + 52) = 0x01;
					if (*rsi_842 == 0x2D && *((word128) rsi_842 + 1) == 0x00)
						fn0000000000002600(0x05, "standard input", null);
					quotearg_style(rsi, 0x04, fs);
					fn00000000000028A0(fn0000000000002600(0x05, "%s has been replaced with an untailable symbolic link", null), 0x00, 0x00);
					rsi_1040 = *rdi;
					goto l00000000000066F0;
				}
l00000000000066B9:
				Eq_24 rax_926 = fn0000000000002530();
				rsi_1040 = *rdi;
				byte r14b_938 = *((word128) rdi + 54);
				Eq_24 edx_936 = *rax_926;
				*((word128) rdi + 60) = edx_936;
				Eq_24 r13d_1051 = (word32) r13_284;
				r15_1070 = rsi_1040;
				if (r14b_938 != 0x00)
				{
					word32 eax_1046 = (word32) *rsi_1040 - 0x2D;
					if (eax_1046 != 0x00)
					{
						if (edx_936 == r13d_1051)
							goto l00000000000066F0;
					}
					else
					{
						eax_1046 = (word32) *((word128) rsi_1040 + 1);
						if (edx_936 == r13d_1051)
							goto l00000000000066F0;
					}
					if (eax_1046 == 0x00)
						r15_1070 = fn0000000000002600(0x05, "standard input", null);
					quotearg_n_style_colon(r15_1070, 0x03, 0x00, fs);
					fn00000000000028A0(58644, *rax_926, 0x00);
					rsi_1040 = *rdi;
l00000000000066F0:
					word32 r12d_1135 = (word32) r12_1833;
					if (*rsi_1040 == 0x2D && *((word128) rsi_1040 + 1) == 0x00)
						fn0000000000002600(0x05, "standard input", null);
					close_fd((uint64) r12d_1135);
					Eq_24 rsi_1168 = *rdi;
					if (*rsi_1168 == 0x2D && *((word128) rsi_1168 + 1) == 0x00)
						fn0000000000002600(0x05, "standard input", null);
					rdx_1201 = close_fd((uint64) *((word128) rdi + 56));
					*((word128) rdi + 56) = ~0x00;
					goto l000000000000674C;
				}
				goto l0000000000006931;
			}
			goto l000000000000674C;
		}
		if (edx_410 == 0x00)
			fn0000000000002600(0x05, "standard input", null);
		quotearg_style(rsi, 0x04, fs);
		rsi_474 = (char *) "%s has appeared;  following new file";
	}
	fn00000000000028A0(fn0000000000002600(0x05, rsi_474, null), 0x00, 0x00);
	goto l0000000000006876;
}