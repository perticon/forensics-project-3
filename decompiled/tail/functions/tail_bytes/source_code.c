tail_bytes (char const *pretty_filename, int fd, uintmax_t n_bytes,
            uintmax_t *read_pos)
{
  struct stat stats;

  if (fstat (fd, &stats))
    {
      error (0, errno, _("cannot fstat %s"), quoteaf (pretty_filename));
      return false;
    }

  if (from_start)
    {
      if (! presume_input_pipe && n_bytes <= OFF_T_MAX
          && ((S_ISREG (stats.st_mode)
               && xlseek (fd, n_bytes, SEEK_CUR, pretty_filename) >= 0)
              || lseek (fd, n_bytes, SEEK_CUR) != -1))
        *read_pos += n_bytes;
      else
        {
          int t = start_bytes (pretty_filename, fd, n_bytes, read_pos);
          if (t)
            return t < 0;
        }
      n_bytes = COPY_TO_EOF;
    }
  else
    {
      off_t end_pos = -1;
      off_t current_pos = -1;

      if (! presume_input_pipe && n_bytes <= OFF_T_MAX)
        {
          if (usable_st_size (&stats))
            end_pos = stats.st_size;
          else if ((current_pos = lseek (fd, -n_bytes, SEEK_END)) != -1)
            end_pos = current_pos + n_bytes;
        }
      if (end_pos <= (off_t) ST_BLKSIZE (stats))
        return pipe_bytes (pretty_filename, fd, n_bytes, read_pos);
      if (current_pos == -1)
        current_pos = xlseek (fd, 0, SEEK_CUR, pretty_filename);
      if (current_pos < end_pos)
        {
          off_t bytes_remaining = end_pos - current_pos;

          if (n_bytes < bytes_remaining)
            {
              current_pos = end_pos - n_bytes;
              xlseek (fd, current_pos, SEEK_SET, pretty_filename);
            }
        }
      *read_pos = current_pos;
    }

  *read_pos += dump_remainder (false, pretty_filename, fd, n_bytes);
  return true;
}