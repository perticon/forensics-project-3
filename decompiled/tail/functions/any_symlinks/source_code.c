any_symlinks (const struct File_spec *f, size_t n_files)
{
  struct stat st;
  for (size_t i = 0; i < n_files; i++)
    if (lstat (f[i].name, &st) == 0 && S_ISLNK (st.st_mode))
      return true;
  return false;
}