parse_obsolete_option (int argc, char * const *argv, uintmax_t *n_units)
{
  char const *p;
  char const *n_string;
  char const *n_string_end;
  int default_count = DEFAULT_N_LINES;
  bool t_from_start;
  bool t_count_lines = true;
  bool t_forever = false;

  /* With the obsolete form, there is one option string and at most
     one file argument.  Watch out for "-" and "--", though.  */
  if (! (argc == 2
         || (argc == 3 && ! (argv[2][0] == '-' && argv[2][1]))
         || (3 <= argc && argc <= 4 && STREQ (argv[2], "--"))))
    return false;

  int posix_ver = posix2_version ();
  bool obsolete_usage = posix_ver < 200112;
  bool traditional_usage = obsolete_usage || 200809 <= posix_ver;
  p = argv[1];

  switch (*p++)
    {
    default:
      return false;

    case '+':
      /* Leading "+" is a file name in the standard form.  */
      if (!traditional_usage)
        return false;

      t_from_start = true;
      break;

    case '-':
      /* In the non-obsolete form, "-" is standard input and "-c"
         requires an option-argument.  The obsolete multidigit options
         are supported as a GNU extension even when conforming to
         POSIX 1003.1-2001 or later, so don't complain about them.  */
      if (!obsolete_usage && !p[p[0] == 'c'])
        return false;

      t_from_start = false;
      break;
    }

  n_string = p;
  while (ISDIGIT (*p))
    p++;
  n_string_end = p;

  switch (*p)
    {
    case 'b': default_count *= 512; FALLTHROUGH;
    case 'c': t_count_lines = false; FALLTHROUGH;
    case 'l': p++; break;
    }

  if (*p == 'f')
    {
      t_forever = true;
      ++p;
    }

  if (*p)
    return false;

  if (n_string == n_string_end)
    *n_units = default_count;
  else if ((xstrtoumax (n_string, NULL, 10, n_units, "b")
            & ~LONGINT_INVALID_SUFFIX_CHAR)
           != LONGINT_OK)
    {
      die (EXIT_FAILURE, errno, "%s: %s", _("invalid number"),
           quote (argv[1]));
    }

  /* Set globals.  */
  from_start = t_from_start;
  count_lines = t_count_lines;
  forever = t_forever;

  return true;
}