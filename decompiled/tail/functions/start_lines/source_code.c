start_lines (char const *pretty_filename, int fd, uintmax_t n_lines,
             uintmax_t *read_pos)
{
  if (n_lines == 0)
    return 0;

  while (true)
    {
      char buffer[BUFSIZ];
      size_t bytes_read = safe_read (fd, buffer, BUFSIZ);
      if (bytes_read == 0) /* EOF */
        return -1;
      if (bytes_read == SAFE_READ_ERROR) /* error */
        {
          error (0, errno, _("error reading %s"), quoteaf (pretty_filename));
          return 1;
        }

      char *buffer_end = buffer + bytes_read;

      *read_pos += bytes_read;

      char *p = buffer;
      while ((p = memchr (p, line_end, buffer_end - p)))
        {
          ++p;
          if (--n_lines == 0)
            {
              if (p < buffer_end)
                xwrite_stdout (p, buffer_end - p);
              return 0;
            }
        }
    }
}