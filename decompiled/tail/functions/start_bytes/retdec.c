int32_t start_bytes(char * pretty_filename, int32_t fd, int64_t n_bytes, int64_t * read_pos) {
    int64_t v1 = __readfsqword(40); // 0x5e04
    int32_t result = 0; // 0x5e1a
    if (n_bytes != 0) {
        int64_t v2 = n_bytes; // 0x5e2a
        int64_t v3; // bp-8248, 0x5de0
        uint64_t v4 = safe_read(fd, (char *)&v3, 0x2000); // 0x5e4d
        result = -1;
        while (v4 != 0) {
            if (v4 == -1) {
                // 0x5e90
                quotearg_style();
                function_2600();
                function_2530();
                function_28a0();
                result = 1;
                goto lab_0x5e6a;
            }
            uint64_t v5 = v2;
            *read_pos = v4 + (int64_t)read_pos;
            if (v5 < v4) {
                int64_t v6 = v4 - v5; // 0x5e60
                result = 0;
                if (v6 != 0) {
                    // 0x5ed8
                    xwrite_stdout_part_0(v5 + (int64_t)&v3, v6);
                    result = 0;
                    goto lab_0x5e6a;
                } else {
                    goto lab_0x5e6a;
                }
            }
            // 0x5e3e
            v2 = v5 - v4;
            result = 0;
            if (v2 == 0) {
                // break -> 0x5e6a
                break;
            }
            v4 = safe_read(fd, (char *)&v3, 0x2000);
            result = -1;
        }
    }
    goto lab_0x5e6a;
  lab_0x5e6a:
    // 0x5e6a
    if (v1 != __readfsqword(40)) {
        // 0x5ee3
        return function_2630();
    }
    // 0x5e7d
    return result;
}