undefined8 start_bytes(undefined8 param_1,undefined4 param_2,ulong param_3,long *param_4)

{
  ulong uVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  long in_FS_OFFSET;
  undefined auStack8248 [8200];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 != 0) {
    do {
      uVar1 = safe_read(param_2,auStack8248,0x2000);
      if (uVar1 == 0) {
        uVar2 = 0xffffffff;
        goto LAB_00105e6a;
      }
      if (uVar1 == 0xffffffffffffffff) {
        uVar2 = quotearg_style(4,param_1);
        uVar3 = dcgettext(0,"error reading %s",5);
        piVar4 = __errno_location();
        error(0,*piVar4,uVar3,uVar2);
        uVar2 = 1;
        goto LAB_00105e6a;
      }
      *param_4 = *param_4 + uVar1;
      if (param_3 < uVar1) {
        if (uVar1 - param_3 != 0) {
          xwrite_stdout_part_0(auStack8248 + param_3,uVar1 - param_3);
        }
        break;
      }
      param_3 = param_3 - uVar1;
    } while (param_3 != 0);
  }
  uVar2 = 0;
LAB_00105e6a:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar2;
}