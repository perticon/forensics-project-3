uint32_t start_bytes(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rsi2;
    void** rsp7;
    void** rax8;
    void** v9;
    uint32_t eax10;
    void** r13_11;
    int64_t rbp12;
    void** r14_13;
    void** rbx14;
    void** r12_15;
    void** rax16;
    void* rdx17;
    void** rax18;
    void** rax19;

    rsi2 = esi;
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 16);
    rax8 = g28;
    v9 = rax8;
    if (!rdx) {
        addr_5e68_2:
        eax10 = 0;
    } else {
        r13_11 = rdi;
        *reinterpret_cast<void***>(&rbp12) = rsi2;
        r14_13 = rdx;
        rbx14 = rcx;
        r12_15 = rsp7;
        do {
            rsi2 = r12_15;
            rdi = *reinterpret_cast<void***>(&rbp12);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rax16 = safe_read(rdi, rsi2, 0x2000, rcx);
            rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
            if (!rax16) 
                goto addr_5e57_5;
            if (rax16 == 0xffffffffffffffff) 
                goto addr_5e90_7;
            *reinterpret_cast<void***>(rbx14) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14)) + reinterpret_cast<unsigned char>(rax16));
            if (reinterpret_cast<unsigned char>(rax16) > reinterpret_cast<unsigned char>(r14_13)) 
                goto addr_5e60_9;
            r14_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_13) - reinterpret_cast<unsigned char>(rax16));
        } while (r14_13);
        goto addr_5e68_2;
    }
    addr_5e6a_11:
    rdx17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (!rdx17) {
        return eax10;
    }
    fun_2630();
    offtostr(rdi, reinterpret_cast<uint64_t>(rsp7 - 8) + 8 - 8 - 8 - 8 - 8 - 40);
    fun_2530();
    if (rsi2 != 1) 
        goto addr_5f31_15;
    while (1) {
        quotearg_n_style_colon();
        addr_5f45_17:
        fun_2600();
        fun_28a0();
        fun_2910();
    }
    addr_5f31_15:
    quotearg_n_style_colon();
    goto addr_5f45_17;
    addr_5e57_5:
    eax10 = 0xffffffff;
    goto addr_5e6a_11;
    addr_5e90_7:
    quotearg_style(4, r13_11, 4, r13_11);
    fun_2600();
    rax18 = fun_2530();
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi2 = *reinterpret_cast<void***>(rax18);
    fun_28a0();
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax10 = 1;
    goto addr_5e6a_11;
    addr_5e60_9:
    rax19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) - reinterpret_cast<unsigned char>(r14_13));
    rsi2 = rax19;
    if (rax19) {
        rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r14_13));
        xwrite_stdout_part_0(rdi, rsi2, 0x2000, rcx, r8, r9);
        rsp7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
        goto addr_5e68_2;
    }
}