start_bytes (char const *pretty_filename, int fd, uintmax_t n_bytes,
             uintmax_t *read_pos)
{
  char buffer[BUFSIZ];

  while (0 < n_bytes)
    {
      size_t bytes_read = safe_read (fd, buffer, BUFSIZ);
      if (bytes_read == 0)
        return -1;
      if (bytes_read == SAFE_READ_ERROR)
        {
          error (0, errno, _("error reading %s"), quoteaf (pretty_filename));
          return 1;
        }
      *read_pos += bytes_read;
      if (bytes_read <= n_bytes)
        n_bytes -= bytes_read;
      else
        {
          size_t n_remaining = bytes_read - n_bytes;
          /* Print extra characters if there are any.  */
          xwrite_stdout (&buffer[n_bytes], n_remaining);
          break;
        }
    }

  return 0;
}