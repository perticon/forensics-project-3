void close_fd(int param_1,undefined8 param_2)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  
  if (param_1 + 1U < 2) {
    return;
  }
  iVar1 = close(param_1);
  if (iVar1 == 0) {
    return;
  }
  uVar2 = quotearg_style(4,param_2);
  uVar3 = dcgettext(0,"closing %s (fd=%d)",5);
  piVar4 = __errno_location();
  error(0,*piVar4,uVar3,uVar2,param_1);
  return;
}