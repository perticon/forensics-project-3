Eq_24 close_fd(uint64 rdi)
{
	Eq_24 rsi;
	Eq_24 rdx;
	struct Eq_542 * fs;
	if ((word32) rdi <= 0x00)
		return rdx;
	if (fn00000000000026F0((word32) rdi) == 0x00)
		return rdx;
	quotearg_style(rsi, 0x04, fs);
	Eq_24 rax_52 = fn0000000000002600(0x05, "closing %s (fd=%d)", null);
	uint64 rsi_61 = (uint64) *fn0000000000002530();
	fn00000000000028A0(rax_52, (word32) rsi_61, 0x00);
	return rax_52;
}