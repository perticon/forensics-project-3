close_fd (int fd, char const *filename)
{
  if (fd != -1 && fd != STDIN_FILENO && close (fd))
    {
      error (0, errno, _("closing %s (fd=%d)"), quoteaf (filename), fd);
    }
}