uint64_t close_fd (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    eax = rdi + 1;
    if (eax <= 1) {
        return eax;
    }
    ebx = edi;
    eax = close (rdi);
    if (eax == 0) {
        return eax;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "closing %s (fd=%d)");
    r12 = rax;
    rax = errno_location ();
    r8d = ebx;
    rcx = r13;
    rdx = r12;
    esi = *(rax);
    edi = 0;
    eax = 0;
    return error ();
}