unsigned char file_lines(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void*** rsp7;
    void** v8;
    void** v9;
    void** v10;
    void** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void** r12_15;
    void** r14_16;
    void* rbx17;
    void* rax18;
    void* rbx19;
    void* rbx20;
    void** rbx21;
    void** rdi22;
    void** rbp23;
    void** r13_24;
    int64_t rdi25;
    void** rax26;
    void* rsp27;
    void* rdx28;
    void* rsp29;
    void** rbx30;
    void* rsp31;
    void** edi32;
    void** rax33;
    void** v34;
    void* rax35;
    void** rax36;
    void** eax37;
    void** eax38;
    void** rax39;
    int64_t rdi40;
    void** rdx41;
    void* rsp42;
    void** rdi43;
    void** rax44;
    void** v45;
    uint32_t ebp46;
    uint32_t eax47;
    void** r13_48;
    void** eax49;
    void** r12d50;
    void** eax51;
    uint1_t zf52;
    void** rsi53;
    void** rax54;
    void** rax55;
    void** rsi56;
    void** rax57;
    int64_t rdi58;
    void** rsi59;
    void** rax60;
    int64_t rdi61;
    void* rax62;
    int64_t v63;
    uint32_t r14d64;
    uint32_t eax65;
    void** r15_66;
    void** rsi67;
    int32_t eax68;
    uint32_t v69;
    void** rax70;
    uint32_t r14d71;
    void** edx72;
    uint32_t eax73;
    void** rax74;
    void** rsi75;
    void** rdi76;
    int32_t eax77;
    uint32_t v78;
    void** eax79;
    void** rdi80;
    void** rsi81;
    int32_t eax82;
    uint32_t v83;
    uint32_t eax84;
    void** rax85;
    void** rax86;
    void** rsi87;
    void** rax88;
    void** rax89;
    uint32_t v90;
    int1_t zf91;
    void** rsi92;
    void** rax93;
    int64_t rdi94;
    signed char al95;
    void* rsp96;
    int1_t zf97;
    void** rsi98;
    void** rax99;
    void** rax100;
    void** eax101;
    void** rsi102;
    uint32_t edx103;
    void** rax104;
    void** rax105;
    void* rsp106;
    void** r13_107;
    void** rcx108;
    void* rsp109;
    uint32_t r15d110;
    int64_t v111;
    int64_t v112;
    int64_t v113;
    void** v114;
    int32_t v115;
    void** rax116;
    int64_t rdi117;
    void** rax118;
    int1_t zf119;
    void** v120;
    int1_t zf121;
    int64_t v122;
    void** rax123;
    void** rax124;
    void* rsp125;
    void** rsi126;
    void** rax127;
    int64_t rdi128;
    void** rax129;
    int64_t rdi130;
    void** rsi131;
    void** rax132;
    void** rax133;
    void** v134;
    void** rax135;
    int64_t rdi136;
    void** rax137;
    int1_t zf138;
    int1_t zf139;
    void** edx140;
    void** rax141;
    struct s0* rdi142;
    int32_t eax143;
    void** rax144;
    void** v145;
    int32_t edx146;
    void** v147;
    int64_t rdx148;
    void** v149;
    void** v150;
    int64_t rax151;
    void** r15_152;
    void** rdi153;
    void** rax154;
    void** rax155;
    void** rdx156;
    int64_t rsi157;
    struct s1* rax158;
    int64_t rdi159;
    void** rax160;
    void** rdi161;
    void** rax162;
    void** rsi163;
    void** rax164;
    int64_t rdi165;
    void** rax166;
    void** rax167;

    rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 56);
    v8 = rdi;
    v9 = rcx;
    v10 = r8;
    v11 = r9;
    rax12 = g28;
    v13 = rax12;
    if (!rdx) {
        addr_6330_2:
        eax14 = 1;
    } else {
        r12_15 = esi;
        r14_16 = rdx;
        rbx17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
        rax18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx17) >> 63) >> 51);
        rbx19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx17) + reinterpret_cast<uint64_t>(rax18));
        *reinterpret_cast<uint32_t*>(&rbx20) = *reinterpret_cast<uint32_t*>(&rbx19) & 0x1fff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
        rbx21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx20) - reinterpret_cast<uint64_t>(rax18));
        if (!rbx21) {
            rbx21 = reinterpret_cast<void**>(0x2000);
        }
        rdi22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rbx21));
        rbp23 = rdi22;
        r13_24 = rdi22;
        *reinterpret_cast<void***>(&rdi25) = r12_15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        rax26 = fun_26b0(rdi25, rdi25);
        rsp27 = reinterpret_cast<void*>(rsp7 - 8 + 8);
        if (reinterpret_cast<signed char>(rax26) < reinterpret_cast<signed char>(0)) 
            goto addr_63c7_6; else 
            goto addr_61d3_7;
    }
    addr_6300_8:
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rdx28) {
        fun_2630();
        rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
        goto addr_63b8_10;
    } else {
        return *reinterpret_cast<unsigned char*>(&eax14);
    }
    addr_63c7_6:
    xlseek_part_0(rbp23, 0, v8, rcx, r8, r9);
    rbx30 = rbp23;
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi32 = *reinterpret_cast<void***>(rbp23 + 56);
    rax33 = g28;
    v34 = rax33;
    if (edi32 == 0xffffffff) {
        addr_647a_13:
        rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
        if (rax35) {
            fun_2630();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
        } else {
            goto v11;
        }
    } else {
        r13_24 = *reinterpret_cast<void***>(rbx30);
        if (*reinterpret_cast<void***>(r13_24) == 45 && !*reinterpret_cast<void***>(r13_24 + 1)) {
            rax36 = fun_2600();
            r13_24 = rax36;
            eax37 = fun_2980();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            r12_15 = eax37;
            if (!eax37) 
                goto addr_6431_18; else 
                goto addr_64df_19;
        }
        eax38 = fun_2980();
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
        r12_15 = eax38;
        if (eax38) {
            addr_64df_19:
            rax39 = fun_2530();
            *reinterpret_cast<void***>(&rdi40) = *reinterpret_cast<void***>(rbx30 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi40) + 4) = 0;
            *reinterpret_cast<void***>(rbx30 + 60) = *reinterpret_cast<void***>(rax39);
            close_fd(rdi40, r13_24, rdi40, r13_24);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx30 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_13;
        } else {
            addr_6431_18:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_21; else 
                goto addr_6444_22;
        }
    }
    addr_65f9_23:
    rdx41 = r13_24;
    xlseek_part_0(0, 0, rdx41, rcx, r8, r9);
    rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi43 = __cxa_finalize;
    rax44 = g28;
    v45 = rax44;
    ebp46 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi43) - 45);
    if (!ebp46) {
        ebp46 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi43 + 1));
    }
    eax47 = g36;
    r13_48 = g3c;
    if (!ebp46) {
        eax49 = r13_48;
        r12d50 = reinterpret_cast<void**>(0);
    } else {
        eax51 = open_safer(rdi43, rdi43);
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
        r12d50 = eax51;
        eax49 = g3c;
    }
    zf52 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx41) = zf52;
    if (*reinterpret_cast<unsigned char*>(&rdx41) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax49 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_32:
            ++r13_48;
            if (r13_48) {
                addr_6b83_33:
                rsi53 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi53) == 45) && !*reinterpret_cast<void***>(rsi53 + 1)) {
                    rax54 = fun_2600();
                    rsi53 = rax54;
                }
                rax55 = quotearg_style(4, rsi53, 4, rsi53);
                r13_48 = rax55;
                fun_2600();
                fun_28a0();
            }
            addr_6941_37:
            rsi56 = __cxa_finalize;
            addr_66f0_38:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi56) == 45) && !*reinterpret_cast<void***>(rsi56 + 1)) {
                rax57 = fun_2600();
                rsi56 = rax57;
            }
            *reinterpret_cast<void***>(&rdi58) = r12d50;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi58) + 4) = 0;
            close_fd(rdi58, rsi56, rdi58, rsi56);
            rsi59 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi59) == 45) && !*reinterpret_cast<void***>(rsi59 + 1)) {
                rax60 = fun_2600();
                rsi59 = rax60;
            }
            *reinterpret_cast<void***>(&rdi61) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
            close_fd(rdi61, rsi59, rdi61, rsi59);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_43:
            rax62 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v45) - reinterpret_cast<unsigned char>(g28));
        } while (rax62);
        goto v63;
    }
    r14d64 = reopen_inaccessible_files;
    eax65 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d64)) {
        if (r12d50 == 0xffffffff) {
            addr_6918_47:
            g36 = 0;
            r15_66 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax65)) {
                rsi67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16);
                eax68 = fun_2720(r15_66, rsi67, r15_66, rsi67);
                if (eax68 || (v69 & 0xf000) != 0xa000) {
                    addr_66b9_49:
                    rax70 = fun_2530();
                    r14d71 = g36;
                    rsi56 = __cxa_finalize;
                    edx72 = *reinterpret_cast<void***>(rax70);
                    r15_66 = rsi56;
                    g3c = edx72;
                    if (*reinterpret_cast<signed char*>(&r14d71)) {
                        eax73 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi56) - 45);
                        if (!eax73) {
                            eax73 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi56 + 1));
                            if (edx72 == r13_48) 
                                goto addr_66f0_38;
                        } else {
                            if (edx72 == r13_48) {
                                goto addr_66f0_38;
                            }
                        }
                        if (!eax73) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi56 = __cxa_finalize;
                        goto addr_66f0_38;
                    }
                } else {
                    goto addr_6c10_58;
                }
            } else {
                rax74 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax74);
            }
        } else {
            g36 = 1;
            rsi75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16);
            if (*reinterpret_cast<signed char*>(&eax65) || ((rdi76 = __cxa_finalize, eax77 = fun_2720(rdi76, rsi75, rdi76, rsi75), rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8), !!eax77) || (v78 & 0xf000) != 0xa000)) {
                addr_67a3_61:
                eax79 = fun_2980();
                rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                if (reinterpret_cast<signed char>(eax79) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_49; else 
                    goto addr_67b3_62;
            } else {
                goto addr_6c10_58;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax65) || ((rdi80 = __cxa_finalize, rsi81 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 16), eax82 = fun_2720(rdi80, rsi81, rdi80, rsi81), rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8), !!eax82) || (v83 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d50 == 0xffffffff)) 
                goto addr_66b9_49;
            goto addr_67a3_61;
        } else {
            goto addr_6c10_58;
        }
    }
    eax84 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_66) - 45);
    if (!eax84) {
        eax84 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_66 + 1));
        if (!*reinterpret_cast<signed char*>(&eax47)) 
            goto addr_6941_37;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax47)) 
            goto addr_6941_37;
    }
    if (!eax84) {
        rax85 = fun_2600();
        r15_66 = rax85;
    }
    rax86 = quotearg_style(4, r15_66, 4, r15_66);
    r13_48 = rax86;
    fun_2600();
    fun_28a0();
    rsi56 = __cxa_finalize;
    goto addr_66f0_38;
    addr_6c10_58:
    rsi87 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi87) == 45) && !*reinterpret_cast<void***>(rsi87 + 1)) {
        rax88 = fun_2600();
        rsi87 = rax88;
    }
    rax89 = quotearg_style(4, rsi87, 4, rsi87);
    r13_48 = rax89;
    fun_2600();
    fun_28a0();
    rsi56 = __cxa_finalize;
    goto addr_66f0_38;
    addr_67b3_62:
    if ((v90 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v90 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d64) || (zf91 = follow_mode == 1, !zf91)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax47)) 
                goto addr_6b6d_78;
            if (r13_48 == 0xffffffff) 
                goto addr_6941_37;
            addr_6b6d_78:
            fun_2600();
            goto addr_6b83_33;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax47)) 
                goto addr_6e01_32;
            goto addr_6b83_33;
        }
    }
    rsi92 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi92) == 45) && !*reinterpret_cast<void***>(rsi92 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx41) = 5;
        *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
        rax93 = fun_2600();
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
        rsi92 = rax93;
    }
    *reinterpret_cast<void***>(&rdi94) = r12d50;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0;
    al95 = fremote(rdi94, rsi92, rdx41);
    rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al95;
    if (al95 && (zf97 = disable_inotify == 0, zf97)) {
        rsi98 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi98) == 45) && !*reinterpret_cast<void***>(rsi98 + 1)) {
            rax99 = fun_2600();
            rsi98 = rax99;
        }
        rax100 = quotearg_style(4, rsi98, 4, rsi98);
        r13_48 = rax100;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_37;
    }
    r13_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_48) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax101 = g38;
    if (r13_48) 
        goto addr_6830_89;
    rsi102 = __cxa_finalize;
    edx103 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi102) - 45);
    if (!edx103) {
        edx103 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi102 + 1));
    }
    if (eax101 != 0xffffffff) 
        goto addr_6a80_93;
    if (!edx103) {
        rax104 = fun_2600();
        rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
        rsi102 = rax104;
    }
    rax105 = quotearg_style(4, rsi102, 4, rsi102);
    rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
    r13_107 = rax105;
    addr_685e_97:
    fun_2600();
    rcx108 = r13_107;
    fun_28a0();
    rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8 - 8 + 8);
    addr_6876_98:
    r15d110 = 0;
    r13_48 = __cxa_finalize;
    if (!ebp46) {
        r15d110 = 0xffffffff;
    }
    g38 = r12d50;
    g8 = 0;
    g10 = v111;
    g40 = r15d110;
    g18 = v112;
    g58 = 0;
    g20 = v113;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v114;
    g30 = v115;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_48) == 45) && !*reinterpret_cast<void***>(r13_48 + 1)) {
        rax116 = fun_2600();
        rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8);
        r13_48 = rax116;
    }
    *reinterpret_cast<void***>(&rdi117) = r12d50;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi117) + 4) = 0;
    rax118 = fun_26b0(rdi117);
    if (reinterpret_cast<signed char>(rax118) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_43;
    *reinterpret_cast<signed char*>(&eax65) = xlseek_part_0(0, 0, r13_48, rcx108, r8, r9);
    rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8 - 8 + 8);
    goto addr_6918_47;
    addr_6a80_93:
    zf119 = g28 == v120;
    if (!zf119 || (zf121 = g20 == v122, !zf121)) {
        if (!edx103) {
            rax123 = fun_2600();
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
            rsi102 = rax123;
        }
        rax124 = quotearg_style(4, rsi102, 4, rsi102);
        fun_2600();
        rcx108 = rax124;
        fun_28a0();
        rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi126 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi126) == 45) && !*reinterpret_cast<void***>(rsi126 + 1)) {
            rax127 = fun_2600();
            rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp125) - 8 + 8);
            rsi126 = rax127;
        }
        *reinterpret_cast<void***>(&rdi128) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi128) + 4) = 0;
        close_fd(rdi128, rsi126, rdi128, rsi126);
        rsp109 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp125) - 8 + 8);
        goto addr_6876_98;
    } else {
        if (!edx103) {
            rax129 = fun_2600();
            rsi102 = rax129;
        }
        *reinterpret_cast<void***>(&rdi130) = r12d50;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi130) + 4) = 0;
        close_fd(rdi130, rsi102, rdi130, rsi102);
        goto addr_674c_43;
    }
    addr_6830_89:
    if (!reinterpret_cast<int1_t>(eax101 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi131 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi131) == 45 && !*reinterpret_cast<void***>(rsi131 + 1)) {
            rax132 = fun_2600();
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
            rsi131 = rax132;
        }
        rax133 = quotearg_style(4, rsi131, 4, rsi131);
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8);
        r13_107 = rax133;
        goto addr_685e_97;
    }
    addr_6500_21:
    if (reinterpret_cast<signed char>(v134) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 8))) {
        while (rax135 = quotearg_n_style_colon(), fun_2600(), rcx = rax135, fun_28a0(), *reinterpret_cast<void***>(&rdi136) = *reinterpret_cast<void***>(rbx30 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi136) + 4) = 0, rax137 = fun_26b0(rdi136), rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax137) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx30 + 8) = reinterpret_cast<void**>(0);
            addr_6444_22:
            zf138 = print_headers == 0;
            if (!zf138) {
                r12_15 = reinterpret_cast<void**>(0);
                zf139 = __cxa_finalize == rbx30;
                *reinterpret_cast<unsigned char*>(&r12_15) = reinterpret_cast<uint1_t>(!zf139);
            }
            edx140 = *reinterpret_cast<void***>(rbx30 + 56);
            rcx = reinterpret_cast<void**>(0xffffffffffffffff);
            rax141 = dump_remainder(r12_15, r13_24, edx140, 0xffffffffffffffff, r8, r9);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            *reinterpret_cast<void***>(rbx30 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx30 + 8)) + reinterpret_cast<unsigned char>(rax141));
            if (!rax141) 
                goto addr_647a_13;
            __cxa_finalize = rbx30;
            rdi142 = stdout;
            eax143 = fun_2940(rdi142, r13_24, rdi142, r13_24);
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            if (!eax143) 
                goto addr_647a_13;
            rax144 = fun_2600();
            r12_15 = rax144;
            fun_2530();
            fun_28a0();
            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_23;
    } else {
        if (v145 != *reinterpret_cast<void***>(rbx30 + 8)) 
            goto addr_6444_22;
        edx146 = 0;
        *reinterpret_cast<unsigned char*>(&edx146) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v147) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx148) = edx146 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v149) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx148) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 24)) < reinterpret_cast<signed char>(v150));
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax151) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx30 + 24)) > reinterpret_cast<signed char>(v150))) - *reinterpret_cast<uint32_t*>(&rcx);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax151) + 4) = 0;
        if (static_cast<int32_t>(rax151 + rdx148 * 2)) 
            goto addr_6444_22;
        goto addr_647a_13;
    }
    addr_61d3_7:
    r15_152 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp27) + 32);
    rdi153 = r12_15;
    *reinterpret_cast<int32_t*>(&rdi153 + 4) = 0;
    rax154 = safe_read(rdi153, r15_152, rbx21, rcx);
    rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
    rbp23 = rax154;
    if (rax154 == 0xffffffffffffffff) {
        addr_6337_126:
        rax155 = quotearg_style(4, v8, 4, v8);
        r13_24 = rax155;
        fun_2600();
        fun_2530();
        rcx = r13_24;
        fun_28a0();
        rsp7 = rsp7 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        eax14 = 0;
        goto addr_6300_8;
    } else {
        rcx = v11;
        *reinterpret_cast<int32_t*>(&rbx21) = line_end;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(rax154));
        if (rbp23) {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp7) + reinterpret_cast<unsigned char>(rbp23) + 31) != *reinterpret_cast<signed char*>(&rbx21)) {
                --r14_16;
            }
        }
        while (1) {
            rdx156 = rbp23;
            while (rdx156 && (*reinterpret_cast<int32_t*>(&rsi157) = *reinterpret_cast<int32_t*>(&rbx21), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi157) + 4) = 0, rax158 = fun_28b0(r15_152, rsi157), rsp7 = rsp7 - 8 + 8, !!rax158)) {
                rdx156 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax158) - reinterpret_cast<unsigned char>(r15_152));
                if (!r14_16) 
                    goto addr_62c8_134;
                --r14_16;
            }
            if (r13_24 == v9) 
                goto addr_6379_137;
            r13_24 = r13_24 - 0x2000;
            *reinterpret_cast<void***>(&rdi159) = r12_15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi159) + 4) = 0;
            rax160 = fun_26b0(rdi159, rdi159);
            rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
            if (reinterpret_cast<signed char>(rax160) < reinterpret_cast<signed char>(0)) 
                goto addr_63b8_10;
            rdi161 = r12_15;
            *reinterpret_cast<int32_t*>(&rdi161 + 4) = 0;
            rax162 = safe_read(rdi161, r15_152, 0x2000, rcx);
            rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
            rbp23 = rax162;
            if (rax162 == 0xffffffffffffffff) 
                goto addr_6337_126;
            rcx = v11;
            *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(rax162));
            if (!rbp23) 
                goto addr_6330_2;
            *reinterpret_cast<int32_t*>(&rbx21) = line_end;
        }
    }
    addr_62c8_134:
    rsi163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp23 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(rdx156));
    if (rsi163) {
        xwrite_stdout_part_0(&rax158->f1, rsi163, rdx156, rcx, r8, r9);
        rsp7 = rsp7 - 8 + 8;
    }
    rax164 = dump_remainder(0, v8, r12_15, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(rbp23)) - reinterpret_cast<unsigned char>(r13_24), r8, r9);
    rsp7 = rsp7 - 8 + 8;
    rcx = v11;
    *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)) + reinterpret_cast<unsigned char>(rax164));
    eax14 = 1;
    goto addr_6300_8;
    addr_6379_137:
    *reinterpret_cast<void***>(&rdi165) = r12_15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi165) + 4) = 0;
    rax166 = fun_26b0(rdi165, rdi165);
    rsp29 = reinterpret_cast<void*>(rsp7 - 8 + 8);
    if (reinterpret_cast<signed char>(rax166) < reinterpret_cast<signed char>(0)) {
        addr_63b8_10:
        xlseek_part_0(r13_24, 0, v8, rcx, r8, r9);
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
        goto addr_63c7_6;
    } else {
        rax167 = dump_remainder(0, v8, r12_15, v10, r8, r9);
        rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
        rcx = v11;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax167) + reinterpret_cast<unsigned char>(r13_24));
        eax14 = 1;
        goto addr_6300_8;
    }
}