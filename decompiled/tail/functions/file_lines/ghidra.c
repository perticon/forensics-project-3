file_lines(undefined8 param_1,int param_2,long param_3,long param_4,long param_5,long *param_6)

{
  char cVar1;
  __off_t _Var2;
  size_t __n;
  void *pvVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  int *piVar6;
  int __c;
  long lVar7;
  size_t sVar8;
  long in_FS_OFFSET;
  bool bVar9;
  undefined8 local_2050;
  undefined local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_2050 = param_5;
  if (param_3 == 0) {
LAB_00106330:
    uVar4 = 1;
  }
  else {
    lVar7 = param_5 - param_4;
    lVar7 = (ulong)((int)lVar7 + ((uint)(lVar7 >> 0x5f) >> 0x13) & 0x1fff) -
            ((ulong)(lVar7 >> 0x3f) >> 0x33);
    if (lVar7 == 0) {
      lVar7 = 0x2000;
    }
    param_5 = param_5 - lVar7;
    _Var2 = lseek(param_2,param_5,0);
    if (_Var2 < 0) {
                    /* WARNING: Subroutine does not return */
      xlseek_part_0(param_5,0,param_1);
    }
    __n = safe_read(param_2,local_2048,lVar7);
    cVar1 = line_end;
    if (__n == 0xffffffffffffffff) {
LAB_00106337:
      uVar4 = quotearg_style(4,param_1);
      uVar5 = dcgettext(0,"error reading %s",5);
      piVar6 = __errno_location();
      error(0,*piVar6,uVar5,uVar4);
      uVar4 = 0;
    }
    else {
      __c = (int)line_end;
      *param_6 = param_5 + __n;
      sVar8 = __n;
      if ((__n != 0) && (local_2048[__n - 1] != cVar1)) {
        param_3 = param_3 + -1;
      }
LAB_0010623d:
      do {
        if ((__n == 0) || (pvVar3 = memrchr(local_2048,__c,__n), pvVar3 == (void *)0x0)) {
          if (param_5 == param_4) {
            _Var2 = lseek(param_2,param_5,0);
            if (-1 < _Var2) {
              lVar7 = dump_remainder(0,param_1,param_2,local_2050);
              *param_6 = lVar7 + param_5;
              uVar4 = 1;
              goto LAB_00106300;
            }
          }
          else {
            param_5 = param_5 + -0x2000;
            _Var2 = lseek(param_2,param_5,0);
            if (-1 < _Var2) {
              __n = safe_read(param_2,local_2048,0x2000);
              if (__n == 0xffffffffffffffff) goto LAB_00106337;
              *param_6 = param_5 + __n;
              if (__n == 0) goto LAB_00106330;
              __c = (int)line_end;
              sVar8 = __n;
              goto LAB_0010623d;
            }
          }
                    /* WARNING: Subroutine does not return */
          xlseek_part_0(param_5,0,param_1);
        }
        __n = (long)pvVar3 - (long)local_2048;
        bVar9 = param_3 != 0;
        param_3 = param_3 + -1;
      } while (bVar9);
      if (sVar8 - 1 != __n) {
        xwrite_stdout_part_0((long)pvVar3 + 1);
      }
      lVar7 = dump_remainder(0,param_1,param_2,(local_2050 - sVar8) - param_5);
      *param_6 = *param_6 + lVar7;
      uVar4 = 1;
    }
  }
LAB_00106300:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar4;
}