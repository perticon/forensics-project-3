bool file_lines(char * pretty_filename, int32_t fd, int64_t n_lines, int64_t start_pos, int64_t end_pos, int64_t * read_pos) {
    char buffer[8192]; // bp-8272, 0x6130
    char v1[8192]; // 0x6160
    int64_t v2 = (int64_t)read_pos;
    v1[0] = end_pos;
    buffer = v1;
    int64_t v3 = __readfsqword(40); // 0x6169
    int64_t v4 = 1; // 0x617f
    int64_t v5; // 0x6130
    int64_t v6; // 0x6130
    int64_t v7; // 0x6130
    int64_t v8; // 0x6130
    int64_t v9; // 0x6130
    int64_t v10; // 0x6130
    if (n_lines == 0) {
        goto lab_0x6300;
    } else {
        int64_t v11 = end_pos - start_pos; // 0x6191
        uint64_t v12 = (v11 >> 63) / 0x8000000000000; // 0x619b
        int64_t v13 = (v12 + v11) % 0x2000 - v12; // 0x61a8
        int64_t v14 = v13 == 0 ? 0x2000 : v13; // 0x61b0
        int64_t v15 = end_pos - v14; // 0x61b6
        if (function_26b0() < 0) {
            // 0x63c7
            return xlseek_part_0((int32_t)v15, 0, (int64_t)pretty_filename) % 2 != 0;
        }
        // 0x61d3
        int64_t v16; // bp-8264, 0x6130
        int64_t v17 = safe_read(fd, (char *)&v16, v14); // 0x61e1
        if (v17 == -1) {
            goto lab_0x6337;
        } else {
            // 0x61f3
            *(int64_t *)v2 = v17 + v15;
            if (v17 != 0) {
                // branch -> 0x6210
            }
            // 0x6210
            v5 = v15;
            v8 = v17;
            int64_t v18 = n_lines; // 0x6240
            int64_t v19; // 0x6130
            int64_t v20; // 0x6130
            int64_t v21; // 0x621d
            int64_t v22; // 0x622e
            int64_t v23; // 0x62cc
            int64_t v24; // 0x622a
            if (v8 != 0) {
                v20 = n_lines;
                v21 = function_28b0();
                v18 = v20;
                while (v21 != 0) {
                    // 0x6227
                    v22 = v21 - (int64_t)&v16;
                    if (v20 == 0) {
                        // 0x62c8
                        v23 = v8 + -1 - v22;
                        if (v23 == 0) {
                            goto lab_0x62da;
                        } else {
                            // 0x62d1
                            xwrite_stdout_part_0(v21 + 1, v23);
                            goto lab_0x62da;
                        }
                    }
                    // 0x623a
                    v24 = v20 - 1;
                    v19 = v24;
                    v18 = v24;
                    if (v22 == 0) {
                        // break -> 0x6242
                        break;
                    }
                    v20 = v19;
                    v21 = function_28b0();
                    v18 = v20;
                }
            }
            while (v5 != start_pos) {
                int64_t v25 = v5 - 0x2000; // 0x624d
                v10 = v8;
                v7 = v25;
                if (function_26b0() < 0) {
                    goto lab_0x63b8;
                }
                int64_t v26 = safe_read(fd, (char *)&v16, 0x2000); // 0x6275
                if (v26 == -1) {
                    goto lab_0x6337;
                }
                // 0x6287
                *(int64_t *)v2 = v26 + v25;
                v4 = 1;
                v9 = 0;
                v6 = v25;
                if (v26 == 0) {
                    goto lab_0x6300;
                }
                v5 = v25;
                v8 = v26;
                v19 = v18;
                if (v8 != 0) {
                    v20 = v19;
                    v21 = function_28b0();
                    v18 = v20;
                    while (v21 != 0) {
                        // 0x6227
                        v22 = v21 - (int64_t)&v16;
                        if (v20 == 0) {
                            // 0x62c8
                            v23 = v8 + -1 - v22;
                            if (v23 == 0) {
                                goto lab_0x62da;
                            } else {
                                // 0x62d1
                                xwrite_stdout_part_0(v21 + 1, v23);
                                goto lab_0x62da;
                            }
                        }
                        // 0x623a
                        v24 = v20 - 1;
                        v19 = v24;
                        v18 = v24;
                        if (v22 == 0) {
                            // break -> 0x6242
                            break;
                        }
                        v20 = v19;
                        v21 = function_28b0();
                        v18 = v20;
                    }
                }
            }
            // 0x6379
            v10 = v8;
            v7 = start_pos;
            if (function_26b0() < 0) {
                goto lab_0x63b8;
            } else {
                int64_t v27 = dump_remainder(false, pretty_filename, fd, *(int64_t *)&buffer); // 0x639a
                *(int64_t *)v2 = v27 + start_pos;
                v4 = 1;
                v9 = v8;
                v6 = start_pos;
                goto lab_0x6300;
            }
        }
    }
  lab_0x63b8:;
    int64_t v28 = (int64_t)pretty_filename;
    xlseek_part_0((int32_t)v7, 0, v28);
    // 0x63c7
    return xlseek_part_0((int32_t)v10, 0, v28) % 2 != 0;
  lab_0x6337:;
    int64_t v29 = quotearg_style(); // 0x6341
    function_2600();
    function_2530();
    function_28a0();
    v4 = 0;
    v9 = -1;
    v6 = v29;
    goto lab_0x6300;
  lab_0x6300:
    // 0x6300
    if (v3 == __readfsqword(40)) {
        // 0x6317
        return v4 != 0;
    }
    // 0x63b3
    function_2630();
    v10 = v9;
    v7 = v6;
    goto lab_0x63b8;
  lab_0x62da:;
    int64_t v30 = *(int64_t *)&buffer; // 0x62da
    int64_t v31 = dump_remainder(false, pretty_filename, fd, v30 - (v5 + v8)); // 0x62ef
    *(int64_t *)v2 = v31 + v2;
    v4 = 1;
    v9 = v8;
    v6 = v5;
    goto lab_0x6300;
}