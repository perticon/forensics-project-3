byte file_lines(Eq_24 rcx, Eq_24 rdx, Eq_77 esi, Eq_24 r8, void ** r9, struct Eq_542 * fs, ptr64 & rdxOut)
{
	Eq_24 rsi;
	Eq_5247 fp;
	Eq_24 rdi;
	Eq_77 esi = (word32) rsi;
	uint64 rax_200;
	word64 rax_40 = fs->qw0028;
	if (rdx != 0x00)
	{
		int64 rbx_54 = r8 - rcx;
		uint64 r12_52 = (uint64) esi;
		uint64 rax_57 = rbx_54 >> 0x3F >> 0x33;
		Eq_77 r12d_78 = (word32) r12_52;
		Eq_24 r14_112 = rdx;
		Eq_24 rbx_62 = (uint64) ((word32) (rbx_54 + rax_57) & 0x1FFF) - rax_57;
		if (rbx_62 == 0x00)
			rbx_62.u0 = 0x2000;
		Eq_24 rdi_72 = r8 - rbx_62;
		Eq_24 rbp_114 = rdi_72;
		Eq_24 r13_254 = rdi_72;
		Eq_24 rax_81 = fn00000000000026B0(0x00, rdi_72, r12d_78);
		if (rax_81 < 0x00)
		{
l00000000000063C7:
			word32 esi_618;
			Eq_24 rdi_410;
			xlseek.part.0(0x00, rbp_114, out esi_618, out rdi_410);
			ptr64 rdx_424;
			uint64 rax_423 = (uint64) check_fspec(rsi, rdi_410, fs, out rdx_424);
			rdxOut = rdx_424;
			return (byte) rax_423;
		}
		safe_read(rbx_62, fp - 0x2048, r12d_78);
		rbp_114 = rax_81;
		if (rax_81 == ~0x00)
		{
l0000000000006337:
			quotearg_style(rdi, 0x04, fs);
			fn00000000000028A0(fn0000000000002600(0x05, "error reading %s", null), *fn0000000000002530(), 0x00);
			rax_200 = 0x00;
l0000000000006300:
			ptr64 rdx_358 = rax_40 - fs->qw0028;
			if (rdx_358 != 0x00)
				fn0000000000002630();
			else
			{
				rdxOut = rdx_358;
				return (byte) rax_200;
			}
		}
		word64 rax_102 = rdi_72 + rax_81;
		Eq_24 bl_108 = g_t13114;
		*r9 = rax_102;
		if (rax_81 != 0x00 && Mem104[(fp - 0x2049) + rax_81:byte] != bl_108)
			r14_112 = rdx - 0x01;
		do
		{
			Eq_24 rdx_115 = rbp_114;
			while (true)
			{
				Eq_77 r12d_167 = (word32) r12_52;
				if (rdx_115 == 0x00)
					break;
				fn00000000000028B0();
				if (rax_102 == 0x00)
					break;
				Eq_24 rsi_131 = r14_112 - 1;
				rdx_115 = rax_102 - (fp - 0x2048);
				if (r14_112 == 0x00)
				{
					if (rbp_114 - 1 - rdx_115 != 0x00)
						xwrite_stdout.part.0(rsi, rax_102 + 1);
					word64 rdx_622;
					*r9 = (void **) ((char *) *r9 + dump_remainder((r8 - rbp_114) - r13_254, r12d_167, 0x00, fs, out rdx_622));
					rax_200 = 0x01;
					goto l0000000000006300;
				}
				r14_112 = rsi_131;
			}
			if (r13_254 == rcx)
			{
				if (fn00000000000026B0(0x00, r13_254, r12d_167) >= 0x00)
				{
					word64 rdx_621;
					*r9 = (void **) ((word128) r13_254 + dump_remainder(r8, r12d_167, 0x00, fs, out rdx_621));
					rax_200 = 0x01;
					goto l0000000000006300;
				}
l00000000000063B8:
				word64 rdi_620;
				word32 esi_619;
				xlseek.part.0(0x00, r13_254, out esi_619, out rdi_620);
				goto l00000000000063C7;
			}
			r13_254 -= 0x2000;
			Eq_24 rax_263 = fn00000000000026B0(0x00, r13_254, r12d_167);
			if (rax_263 < 0x00)
				goto l00000000000063B8;
			safe_read(0x2000, fp - 0x2048, r12d_167);
			rbp_114 = rax_263;
			if (rax_263 == ~0x00)
				goto l0000000000006337;
			rax_102 = r13_254 + rax_263;
			*r9 = rax_102;
		} while (rax_263 != 0x00);
	}
	rax_200 = 0x01;
	goto l0000000000006300;
}