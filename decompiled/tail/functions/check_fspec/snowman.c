void check_fspec(void** rdi, void** rsi) {
    void** rbx3;
    void* rsp4;
    void** edi5;
    void** rax6;
    void** v7;
    void* rax8;
    void** r13_9;
    void** rbp10;
    void** rax11;
    void** eax12;
    void** r12_13;
    void** eax14;
    void** rax15;
    int64_t rdi16;
    void** rdx17;
    void** rcx18;
    void** r8_19;
    void** r9_20;
    void* rsp21;
    void** rdi22;
    void** rax23;
    void** v24;
    uint32_t ebp25;
    uint32_t eax26;
    void** r13_27;
    void** eax28;
    void** r12d29;
    void** eax30;
    uint1_t zf31;
    void** rsi32;
    void** rax33;
    void** rax34;
    void** rsi35;
    void** rax36;
    int64_t rdi37;
    void** rsi38;
    void** rax39;
    int64_t rdi40;
    void* rax41;
    int64_t v42;
    uint32_t r14d43;
    uint32_t eax44;
    void** r15_45;
    void** rsi46;
    int32_t eax47;
    uint32_t v48;
    void** rax49;
    uint32_t r14d50;
    void** edx51;
    uint32_t eax52;
    void** rax53;
    void** rsi54;
    void** rdi55;
    int32_t eax56;
    uint32_t v57;
    void** eax58;
    void** rdi59;
    void** rsi60;
    int32_t eax61;
    uint32_t v62;
    uint32_t eax63;
    void** rax64;
    void** rax65;
    void** rsi66;
    void** rax67;
    void** rax68;
    uint32_t v69;
    int1_t zf70;
    void** rsi71;
    void** rax72;
    int64_t rdi73;
    signed char al74;
    void* rsp75;
    int1_t zf76;
    void** rsi77;
    void** rax78;
    void** rax79;
    void** eax80;
    void** rsi81;
    uint32_t edx82;
    void** rax83;
    void** rax84;
    void* rsp85;
    void** r13_86;
    void** rcx87;
    void* rsp88;
    uint32_t r15d89;
    int64_t v90;
    int64_t v91;
    int64_t v92;
    void** v93;
    int32_t v94;
    void** rax95;
    int64_t rdi96;
    void** rax97;
    void** r8_98;
    void** r9_99;
    int1_t zf100;
    void** v101;
    int1_t zf102;
    int64_t v103;
    void** rax104;
    void** rax105;
    void* rsp106;
    void** rsi107;
    void** rax108;
    int64_t rdi109;
    void** rax110;
    int64_t rdi111;
    void** rsi112;
    void** rax113;
    void** rax114;
    void** v115;
    void** rax116;
    int64_t rdi117;
    void** rax118;
    int1_t zf119;
    void** edx120;
    void** r8_121;
    void** r9_122;
    void** rax123;
    struct s0* rdi124;
    int32_t eax125;
    void** rax126;
    void** v127;
    int32_t edx128;
    void** v129;
    int64_t rdx130;
    void** v131;
    void** v132;
    int64_t rax133;

    rbx3 = rdi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi5 = *reinterpret_cast<void***>(rdi + 56);
    rax6 = g28;
    v7 = rax6;
    if (edi5 == 0xffffffff) {
        addr_647a_2:
        rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (rax8) {
            fun_2630();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        } else {
            return;
        }
    } else {
        r13_9 = *reinterpret_cast<void***>(rbx3);
        rbp10 = rsi;
        if (*reinterpret_cast<void***>(r13_9) == 45 && !*reinterpret_cast<void***>(r13_9 + 1)) {
            rax11 = fun_2600();
            r13_9 = rax11;
            eax12 = fun_2980();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            r12_13 = eax12;
            if (!eax12) 
                goto addr_6431_7; else 
                goto addr_64df_8;
        }
        eax14 = fun_2980();
        rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        r12_13 = eax14;
        if (eax14) {
            addr_64df_8:
            rax15 = fun_2530();
            *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbx3 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
            *reinterpret_cast<void***>(rbx3 + 60) = *reinterpret_cast<void***>(rax15);
            close_fd(rdi16, r13_9, rdi16, r13_9);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx3 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_2;
        } else {
            addr_6431_7:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx3 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_10; else 
                goto addr_6444_11;
        }
    }
    addr_65f9_12:
    rdx17 = r13_9;
    xlseek_part_0(0, 0, rdx17, rcx18, r8_19, r9_20);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi22 = __cxa_finalize;
    rax23 = g28;
    v24 = rax23;
    ebp25 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi22) - 45);
    if (!ebp25) {
        ebp25 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 1));
    }
    eax26 = g36;
    r13_27 = g3c;
    if (!ebp25) {
        eax28 = r13_27;
        r12d29 = reinterpret_cast<void**>(0);
    } else {
        eax30 = open_safer(rdi22, rdi22);
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
        r12d29 = eax30;
        eax28 = g3c;
    }
    zf31 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx17) = zf31;
    if (*reinterpret_cast<unsigned char*>(&rdx17) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax28 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_21:
            ++r13_27;
            if (r13_27) {
                addr_6b83_22:
                rsi32 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi32) == 45) && !*reinterpret_cast<void***>(rsi32 + 1)) {
                    rax33 = fun_2600();
                    rsi32 = rax33;
                }
                rax34 = quotearg_style(4, rsi32, 4, rsi32);
                r13_27 = rax34;
                fun_2600();
                fun_28a0();
            }
            addr_6941_26:
            rsi35 = __cxa_finalize;
            addr_66f0_27:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi35) == 45) && !*reinterpret_cast<void***>(rsi35 + 1)) {
                rax36 = fun_2600();
                rsi35 = rax36;
            }
            *reinterpret_cast<void***>(&rdi37) = r12d29;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0;
            close_fd(rdi37, rsi35, rdi37, rsi35);
            rsi38 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi38) == 45) && !*reinterpret_cast<void***>(rsi38 + 1)) {
                rax39 = fun_2600();
                rsi38 = rax39;
            }
            *reinterpret_cast<void***>(&rdi40) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi40) + 4) = 0;
            close_fd(rdi40, rsi38, rdi40, rsi38);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_32:
            rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
        } while (rax41);
        goto v42;
    }
    r14d43 = reopen_inaccessible_files;
    eax44 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d43)) {
        if (r12d29 == 0xffffffff) {
            addr_6918_36:
            g36 = 0;
            r15_45 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax44)) {
                rsi46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16);
                eax47 = fun_2720(r15_45, rsi46, r15_45, rsi46);
                if (eax47 || (v48 & 0xf000) != 0xa000) {
                    addr_66b9_38:
                    rax49 = fun_2530();
                    r14d50 = g36;
                    rsi35 = __cxa_finalize;
                    edx51 = *reinterpret_cast<void***>(rax49);
                    r15_45 = rsi35;
                    g3c = edx51;
                    if (*reinterpret_cast<signed char*>(&r14d50)) {
                        eax52 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi35) - 45);
                        if (!eax52) {
                            eax52 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi35 + 1));
                            if (edx51 == r13_27) 
                                goto addr_66f0_27;
                        } else {
                            if (edx51 == r13_27) {
                                goto addr_66f0_27;
                            }
                        }
                        if (!eax52) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi35 = __cxa_finalize;
                        goto addr_66f0_27;
                    }
                } else {
                    goto addr_6c10_47;
                }
            } else {
                rax53 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax53);
            }
        } else {
            g36 = 1;
            rsi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16);
            if (*reinterpret_cast<signed char*>(&eax44) || ((rdi55 = __cxa_finalize, eax56 = fun_2720(rdi55, rsi54, rdi55, rsi54), rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8), !!eax56) || (v57 & 0xf000) != 0xa000)) {
                addr_67a3_50:
                eax58 = fun_2980();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (reinterpret_cast<signed char>(eax58) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_38; else 
                    goto addr_67b3_51;
            } else {
                goto addr_6c10_47;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax44) || ((rdi59 = __cxa_finalize, rsi60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) + 16), eax61 = fun_2720(rdi59, rsi60, rdi59, rsi60), rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8), !!eax61) || (v62 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d29 == 0xffffffff)) 
                goto addr_66b9_38;
            goto addr_67a3_50;
        } else {
            goto addr_6c10_47;
        }
    }
    eax63 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_45) - 45);
    if (!eax63) {
        eax63 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_45 + 1));
        if (!*reinterpret_cast<signed char*>(&eax26)) 
            goto addr_6941_26;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax26)) 
            goto addr_6941_26;
    }
    if (!eax63) {
        rax64 = fun_2600();
        r15_45 = rax64;
    }
    rax65 = quotearg_style(4, r15_45, 4, r15_45);
    r13_27 = rax65;
    fun_2600();
    fun_28a0();
    rsi35 = __cxa_finalize;
    goto addr_66f0_27;
    addr_6c10_47:
    rsi66 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi66) == 45) && !*reinterpret_cast<void***>(rsi66 + 1)) {
        rax67 = fun_2600();
        rsi66 = rax67;
    }
    rax68 = quotearg_style(4, rsi66, 4, rsi66);
    r13_27 = rax68;
    fun_2600();
    fun_28a0();
    rsi35 = __cxa_finalize;
    goto addr_66f0_27;
    addr_67b3_51:
    if ((v69 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v69 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d43) || (zf70 = follow_mode == 1, !zf70)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax26)) 
                goto addr_6b6d_67;
            if (r13_27 == 0xffffffff) 
                goto addr_6941_26;
            addr_6b6d_67:
            fun_2600();
            goto addr_6b83_22;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax26)) 
                goto addr_6e01_21;
            goto addr_6b83_22;
        }
    }
    rsi71 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi71) == 45) && !*reinterpret_cast<void***>(rsi71 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx17) = 5;
        *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
        rax72 = fun_2600();
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
        rsi71 = rax72;
    }
    *reinterpret_cast<void***>(&rdi73) = r12d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
    al74 = fremote(rdi73, rsi71, rdx17);
    rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al74;
    if (al74 && (zf76 = disable_inotify == 0, zf76)) {
        rsi77 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi77) == 45) && !*reinterpret_cast<void***>(rsi77 + 1)) {
            rax78 = fun_2600();
            rsi77 = rax78;
        }
        rax79 = quotearg_style(4, rsi77, 4, rsi77);
        r13_27 = rax79;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_26;
    }
    r13_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_27) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax80 = g38;
    if (r13_27) 
        goto addr_6830_78;
    rsi81 = __cxa_finalize;
    edx82 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi81) - 45);
    if (!edx82) {
        edx82 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi81 + 1));
    }
    if (eax80 != 0xffffffff) 
        goto addr_6a80_82;
    if (!edx82) {
        rax83 = fun_2600();
        rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
        rsi81 = rax83;
    }
    rax84 = quotearg_style(4, rsi81, 4, rsi81);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
    r13_86 = rax84;
    addr_685e_86:
    fun_2600();
    rcx87 = r13_86;
    fun_28a0();
    rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp85) - 8 + 8 - 8 + 8);
    addr_6876_87:
    r15d89 = 0;
    r13_27 = __cxa_finalize;
    if (!ebp25) {
        r15d89 = 0xffffffff;
    }
    g38 = r12d29;
    g8 = 0;
    g10 = v90;
    g40 = r15d89;
    g18 = v91;
    g58 = 0;
    g20 = v92;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v93;
    g30 = v94;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_27) == 45) && !*reinterpret_cast<void***>(r13_27 + 1)) {
        rax95 = fun_2600();
        rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8);
        r13_27 = rax95;
    }
    *reinterpret_cast<void***>(&rdi96) = r12d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
    rax97 = fun_26b0(rdi96);
    if (reinterpret_cast<signed char>(rax97) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_32;
    *reinterpret_cast<signed char*>(&eax44) = xlseek_part_0(0, 0, r13_27, rcx87, r8_98, r9_99);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8 - 8 + 8);
    goto addr_6918_36;
    addr_6a80_82:
    zf100 = g28 == v101;
    if (!zf100 || (zf102 = g20 == v103, !zf102)) {
        if (!edx82) {
            rax104 = fun_2600();
            rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
            rsi81 = rax104;
        }
        rax105 = quotearg_style(4, rsi81, 4, rsi81);
        fun_2600();
        rcx87 = rax105;
        fun_28a0();
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi107 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi107) == 45) && !*reinterpret_cast<void***>(rsi107 + 1)) {
            rax108 = fun_2600();
            rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
            rsi107 = rax108;
        }
        *reinterpret_cast<void***>(&rdi109) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0;
        close_fd(rdi109, rsi107, rdi109, rsi107);
        rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
        goto addr_6876_87;
    } else {
        if (!edx82) {
            rax110 = fun_2600();
            rsi81 = rax110;
        }
        *reinterpret_cast<void***>(&rdi111) = r12d29;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi111) + 4) = 0;
        close_fd(rdi111, rsi81, rdi111, rsi81);
        goto addr_674c_32;
    }
    addr_6830_78:
    if (!reinterpret_cast<int1_t>(eax80 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi112 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi112) == 45 && !*reinterpret_cast<void***>(rsi112 + 1)) {
            rax113 = fun_2600();
            rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
            rsi112 = rax113;
        }
        rax114 = quotearg_style(4, rsi112, 4, rsi112);
        rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
        r13_86 = rax114;
        goto addr_685e_86;
    }
    addr_6500_10:
    if (reinterpret_cast<signed char>(v115) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 8))) {
        while (rax116 = quotearg_n_style_colon(), fun_2600(), rcx18 = rax116, fun_28a0(), *reinterpret_cast<void***>(&rdi117) = *reinterpret_cast<void***>(rbx3 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi117) + 4) = 0, rax118 = fun_26b0(rdi117), rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax118) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx3 + 8) = reinterpret_cast<void**>(0);
            addr_6444_11:
            zf119 = print_headers == 0;
            if (!zf119) {
                r12_13 = reinterpret_cast<void**>(0);
                *reinterpret_cast<unsigned char*>(&r12_13) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbp10) != rbx3);
            }
            edx120 = *reinterpret_cast<void***>(rbx3 + 56);
            rcx18 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax123 = dump_remainder(r12_13, r13_9, edx120, 0xffffffffffffffff, r8_121, r9_122);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            *reinterpret_cast<void***>(rbx3 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx3 + 8)) + reinterpret_cast<unsigned char>(rax123));
            if (!rax123) 
                goto addr_647a_2;
            *reinterpret_cast<void***>(rbp10) = rbx3;
            rdi124 = stdout;
            eax125 = fun_2940(rdi124, r13_9, rdi124, r13_9);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            if (!eax125) 
                goto addr_647a_2;
            rax126 = fun_2600();
            r12_13 = rax126;
            fun_2530();
            fun_28a0();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_12;
    } else {
        if (v127 != *reinterpret_cast<void***>(rbx3 + 8)) 
            goto addr_6444_11;
        edx128 = 0;
        *reinterpret_cast<unsigned char*>(&edx128) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v129) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx130) = edx128 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v131) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx130) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 24)) < reinterpret_cast<signed char>(v132));
        *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax133) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx3 + 24)) > reinterpret_cast<signed char>(v132))) - *reinterpret_cast<uint32_t*>(&rcx18);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax133) + 4) = 0;
        if (static_cast<int32_t>(rax133 + rdx130 * 2)) 
            goto addr_6444_11;
        goto addr_647a_2;
    }
}