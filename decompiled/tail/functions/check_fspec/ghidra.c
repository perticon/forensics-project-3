void check_fspec(char **param_1,undefined8 *param_2)

{
  char *pcVar1;
  int iVar2;
  long lVar3;
  char *pcVar4;
  ulong uVar5;
  int *piVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  __off_t _Var9;
  long in_FS_OFFSET;
  stat sStack200;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(int *)(param_1 + 7) != -1) {
    pcVar4 = *param_1;
    if ((*pcVar4 == '-') && (pcVar4[1] == '\0')) {
      pcVar4 = (char *)dcgettext(0,"standard input",5);
      iVar2 = fstat(*(int *)(param_1 + 7),&sStack200);
    }
    else {
      iVar2 = fstat(*(int *)(param_1 + 7),&sStack200);
    }
    if (iVar2 == 0) {
      uVar5 = 0;
      if ((*(uint *)(param_1 + 6) & 0xf000) == 0x8000) {
        if (sStack200.st_size < (long)param_1[1]) goto LAB_001065a0;
        if (((char *)sStack200.st_size == param_1[1]) &&
           (pcVar1 = param_1[3],
           ((uint)(param_1[3] != (char *)sStack200.st_mtim.tv_nsec &&
                  sStack200.st_mtim.tv_nsec <= (long)pcVar1) -
           (uint)((long)pcVar1 < sStack200.st_mtim.tv_nsec)) +
           ((uint)(sStack200.st_mtim.tv_sec < (long)param_1[2]) -
           (uint)((long)param_1[2] < sStack200.st_mtim.tv_sec)) * 2 == 0)) goto LAB_0010647a;
      }
      while( true ) {
        if (print_headers != '\0') {
          uVar5 = (ulong)((char **)*param_2 != param_1);
        }
        lVar3 = dump_remainder(uVar5 & 0xffffffff,pcVar4,*(undefined4 *)(param_1 + 7),
                               0xffffffffffffffff);
        param_1[1] = param_1[1] + lVar3;
        if (lVar3 == 0) break;
        *param_2 = param_1;
        iVar2 = fflush_unlocked(stdout);
        if (iVar2 == 0) break;
        uVar5 = dcgettext(0,"write error",5);
        piVar6 = __errno_location();
        error(1,*piVar6,uVar5);
LAB_001065a0:
        uVar7 = quotearg_n_style_colon(0,3,pcVar4);
        uVar8 = dcgettext(0,"%s: file truncated",5);
        error(0,0,uVar8,uVar7);
        _Var9 = lseek(*(int *)(param_1 + 7),0,0);
        if (_Var9 < 0) {
                    /* WARNING: Subroutine does not return */
          xlseek_part_0(0,0,pcVar4);
        }
        param_1[1] = (char *)0x0;
      }
    }
    else {
      piVar6 = __errno_location();
      *(int *)((long)param_1 + 0x3c) = *piVar6;
      close_fd(*(undefined4 *)(param_1 + 7),pcVar4);
      *(undefined4 *)(param_1 + 7) = 0xffffffff;
    }
  }
LAB_0010647a:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}