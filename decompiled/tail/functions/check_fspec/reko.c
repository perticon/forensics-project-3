word32 check_fspec(Eq_24 rsi, Eq_24 rdi, struct Eq_542 * fs, union Eq_897 & rdxOut)
{
	ptr64 fp;
	Eq_24 qwLoc98;
	Eq_2029 qwLoc70;
	Eq_2054 qwLoc68;
	uint64 rdi_23 = (uint64) *((word128) rdi + 56);
	word64 rax_25 = fs->qw0028;
	if ((word32) rdi_23 == ~0x00)
	{
l000000000000647A:
		word64 rax_291 = rax_25 - fs->qw0028;
		if (rax_291 != 0x00)
			fn0000000000002630();
		else
		{
			rdxOut.u1 = <invalid>;
			return (word32) rax_291;
		}
	}
	Eq_24 r12_211;
	Eq_24 r13_229 = *rdi;
	if (*r13_229 != 0x2D || *((word128) r13_229 + 1) != 0x00)
	{
		int32 eax_61 = fn0000000000002980(fp - 200, (word32) rdi_23);
		r12_211 = (uint64) eax_61;
		if (eax_61 == 0x00)
			goto l0000000000006431;
	}
	else
	{
		Eq_24 rax_47 = fn0000000000002600(0x05, "standard input", null);
		int32 eax_52 = fn0000000000002980(fp - 200, *((word128) rdi + 56));
		r13_229 = rax_47;
		r12_211 = (uint64) eax_52;
		if (eax_52 == 0x00)
		{
l0000000000006431:
			if ((*((word128) rdi + 48) & 0xF000) == 0x8000)
			{
				Eq_24 rax_108 = *((word128) rdi + 8);
				if (qwLoc98 < rax_108)
					goto l00000000000065A0;
				if (qwLoc98 == rax_108)
				{
					Eq_2029 rax_113 = *((word128) rdi + 16);
					if ((word32) (((uint32) (int8) (qwLoc70 < rax_113) - (word32) (qwLoc70 > rax_113)) * 0x02) + (bool) ((*((word128) rdi + 24) > qwLoc68) - (*((word128) rdi + 24) < qwLoc68)) == 0x00)
						goto l000000000000647A;
				}
			}
l0000000000006444:
			Eq_24 r12_433 = r12_211;
			if (g_b13115 != 0x00)
				r12_433 = (uint64) (uint32) (int8) (*rsi != rdi);
			word64 rdx_438;
			int64 rax_186 = dump_remainder(~0x00, *((word128) rdi + 56), (word32) r12_433, fs, out rdx_438);
			((word128) rdi + 8)->u2 = (word128) *((word128) rdi + 8) + rax_186;
			if (rax_186 == 0x00)
				goto l000000000000647A;
			*rsi = rdi;
			if (fn0000000000002940(stdout) == 0x00)
				goto l000000000000647A;
			Eq_24 rax_210 = fn0000000000002600(0x05, "write error", null);
			fn00000000000028A0(rax_210, *fn0000000000002530(), 0x01);
			r12_211 = rax_210;
l00000000000065A0:
			quotearg_n_style_colon(r13_229, 0x03, 0x00, fs);
			fn00000000000028A0(fn0000000000002600(0x05, "%s: file truncated", null), 0x00, 0x00);
			if (fn00000000000026B0(0x00, 0x00, *((word128) rdi + 56)) < 0x00)
			{
				Eq_24 rdi_302;
				word32 esi_440;
				xlseek.part.0(0x00, 0x00, out esi_440, out rdi_302);
				struct Eq_598 * rsi_304 = (struct Eq_598 *) <invalid>;
				word64 rdx_439;
				uint64 rax_309 = (uint64) recheck((word32) rsi_304, rdi_302, fs, out rdx_439);
				rdxOut.u1 = <invalid>;
				return (word32) rax_309;
			}
			((word128) rdi + 8)->u0 = 0x00;
			goto l0000000000006444;
		}
	}
	Eq_24 rax_67 = fn0000000000002530();
	uint64 rdi_73 = (uint64) *((word128) rdi + 56);
	*((word128) rdi + 60) = *rax_67;
	close_fd(rdi_73);
	*((word128) rdi + 56) = ~0x00;
	goto l000000000000647A;
}