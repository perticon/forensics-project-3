void check_fspec(int32_t * fspec, int32_t ** prev_fspec) {
    int64_t v1 = (int64_t)fspec;
    int64_t v2; // 0x63e0
    int64_t v3 = v2;
    int64_t v4 = v2;
    int32_t * v5 = (int32_t *)(v1 + 56); // 0x63f2
    uint32_t v6 = *v5; // 0x63f2
    int64_t v7 = __readfsqword(40); // 0x63f5
    int64_t v8; // 0x63e0
    int64_t v9; // 0x63e0
    int64_t v10; // 0x63e0
    int64_t v11; // 0x63f2
    if (v6 == -1) {
        goto lab_0x647a;
    } else {
        // 0x640d
        v11 = v6;
        if ((char)v6 == 45) {
            // 0x64a8
            if (*(char *)(v11 + 1) != 0) {
                goto lab_0x641e;
            } else {
                int64_t v12 = function_2600(); // 0x64c1
                int64_t v13 = function_2980(); // 0x64cf
                v8 = v13;
                v9 = v12;
                v10 = v12;
                if ((int32_t)v13 == 0) {
                    goto lab_0x6431;
                } else {
                    goto lab_0x64df;
                }
            }
        } else {
            goto lab_0x641e;
        }
    }
  lab_0x647a:
    // 0x647a
    if (v7 == __readfsqword(40)) {
        // 0x6491
        return;
    }
    // 0x65f4
    function_2630();
    // 0x65f9
    int64_t v14; // 0x63e0
    xlseek_part_0(0, 0, v14);
  lab_0x641e:;
    int64_t v15 = function_2980(); // 0x6421
    v8 = v15;
    v9 = v11;
    v10 = v11;
    if ((int32_t)v15 != 0) {
        goto lab_0x64df;
    } else {
        goto lab_0x6431;
    }
  lab_0x64df:
    // 0x64df
    *(int32_t *)(v1 + 60) = *(int32_t *)function_2530();
    close_fd((int64_t)*v5, v10);
    *v5 = -1;
    v14 = v10;
    goto lab_0x647a;
  lab_0x6431:;
    int64_t v16 = v8 & 0xffffffff;
    int64_t v17; // bp-200, 0x63e0
    int64_t v18 = &v17;
    int64_t * v19 = (int64_t *)(v1 + 8);
    int64_t v20 = v18; // 0x643e
    int64_t v21 = v16; // 0x643e
    int64_t v22; // 0x63e0
    if ((*(int32_t *)(v1 + 48) & (int32_t)&g58) == 0x8000) {
        int64_t v23 = *v19; // 0x6500
        v22 = v16;
        if (v2 < v23) {
            goto lab_0x65a0;
        } else {
            // 0x650f
            v20 = v18;
            v21 = v16;
            if (v2 != v23) {
                goto lab_0x6444;
            } else {
                int64_t v24 = *(int64_t *)(v1 + 16); // 0x6515
                int64_t v25 = v4 - v24; // 0x651b
                int64_t v26 = *(int64_t *)(v1 + 24); // 0x6530
                int64_t v27 = v26 - v3; // 0x6530
                v20 = v18;
                v21 = v16;
                v14 = v9;
                if ((int32_t)(v27 < 0 == ((v27 ^ v26) & (v26 ^ v3)) < 0 == (v27 != 0)) - (int32_t)(v27 < 0 != ((v27 ^ v26) & (v26 ^ v3)) < 0) != -((2 * ((int32_t)(v25 < 0 != ((v25 ^ v4) & (v24 ^ v4)) < 0) - (int32_t)(v25 < 0 == ((v25 ^ v4) & (v24 ^ v4)) < 0 == (v25 != 0)))))) {
                    goto lab_0x6444;
                } else {
                    goto lab_0x647a;
                }
            }
        }
    } else {
        goto lab_0x6444;
    }
  lab_0x6444:;
    uint64_t v28 = *(char *)&print_headers == 0 ? v21 : (int64_t)(v20 != v1);
    int64_t v29 = dump_remainder(v28 % 2 != 0, (char *)v9, *v5, -1); // 0x6468
    *v19 = *v19 + v29;
    v14 = v9;
    if (v29 != 0) {
        // 0x6558
        *(int64_t *)prev_fspec = v1;
        v14 = v9;
        if ((int32_t)function_2940() == 0) {
            goto lab_0x647a;
        } else {
            int64_t v30 = function_2600(); // 0x657e
            function_2530();
            function_28a0();
            v22 = v30;
            goto lab_0x65a0;
        }
    } else {
        goto lab_0x647a;
    }
  lab_0x65a0:
    // 0x65a0
    quotearg_n_style_colon();
    function_2600();
    function_28a0();
    if (function_26b0() < 0) {
        // 0x65f9
        xlseek_part_0(0, 0, v9);
        return;
    }
    // 0x65e7
    *v19 = 0;
    v20 = (int32_t)"%s: file truncated" ^ (int32_t)"%s: file truncated" ^ (int32_t)"%s: file truncated" ^ (int32_t)"%s: file truncated";
    v21 = v22;
    goto lab_0x6444;
}