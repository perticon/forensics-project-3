check_output_alive (void)
{
  if (! monitor_output)
    return;

  /* Use 'poll' on AIX (where 'select' was seen to give a readable
     event immediately) or if using inotify (which relies on 'poll'
     anyway).  Otherwise, use 'select' as it's more portable;
     'poll' doesn't work for this application on macOS.  */
#if defined _AIX || defined __sun || HAVE_INOTIFY
  struct pollfd pfd;
  pfd.fd = STDOUT_FILENO;
  pfd.events = pfd.revents = 0;

  if (poll (&pfd, 1, 0) >= 0 && (pfd.revents & (POLLERR | POLLHUP)))
    die_pipe ();
#else
  struct timeval delay;
  delay.tv_sec = delay.tv_usec = 0;

  fd_set rfd;
  FD_ZERO (&rfd);
  FD_SET (STDOUT_FILENO, &rfd);

  /* readable event on STDOUT is equivalent to POLLERR,
     and implies an error condition on output like broken pipe.  */
  if (select (STDOUT_FILENO + 1, &rfd, NULL, NULL, &delay) == 1)
    die_pipe ();
#endif

}