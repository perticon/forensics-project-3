die_pipe (void)
{
  raise (SIGPIPE);
  exit (EXIT_FAILURE);
}