any_live_files (const struct File_spec *f, size_t n_files)
{
  /* In inotify mode, ignore may be set for files
     which may later be replaced with new files.
     So always consider files live in -F mode.  */
  if (reopen_inaccessible_files && follow_mode == Follow_name)
    return true;

  for (size_t i = 0; i < n_files; i++)
    {
      if (0 <= f[i].fd)
        return true;
      else
        {
          if (! f[i].ignore && reopen_inaccessible_files)
            return true;
        }
    }

  return false;
}