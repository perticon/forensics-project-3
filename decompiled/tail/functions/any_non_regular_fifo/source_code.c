any_non_regular_fifo (const struct File_spec *f, size_t n_files)
{
  for (size_t i = 0; i < n_files; i++)
    if (0 <= f[i].fd && ! S_ISREG (f[i].mode) && ! S_ISFIFO (f[i].mode))
      return true;
  return false;
}