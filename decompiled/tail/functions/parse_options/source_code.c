parse_options (int argc, char **argv,
               uintmax_t *n_units, enum header_mode *header_mode,
               double *sleep_interval)
{
  int c;

  while ((c = getopt_long (argc, argv, "c:n:fFqs:vz0123456789",
                           long_options, NULL))
         != -1)
    {
      switch (c)
        {
        case 'F':
          forever = true;
          follow_mode = Follow_name;
          reopen_inaccessible_files = true;
          break;

        case 'c':
        case 'n':
          count_lines = (c == 'n');
          if (*optarg == '+')
            from_start = true;
          else if (*optarg == '-')
            ++optarg;

          *n_units = xdectoumax (optarg, 0, UINTMAX_MAX, "bkKmMGTPEZY0",
                                 count_lines
                                 ? _("invalid number of lines")
                                 : _("invalid number of bytes"), 0);
          break;

        case 'f':
        case LONG_FOLLOW_OPTION:
          forever = true;
          if (optarg == NULL)
            follow_mode = DEFAULT_FOLLOW_MODE;
          else
            follow_mode = XARGMATCH ("--follow", optarg,
                                     follow_mode_string, follow_mode_map);
          break;

        case RETRY_OPTION:
          reopen_inaccessible_files = true;
          break;

        case MAX_UNCHANGED_STATS_OPTION:
          /* --max-unchanged-stats=N */
          max_n_unchanged_stats_between_opens =
            xdectoumax (optarg, 0, UINTMAX_MAX, "",
              _("invalid maximum number of unchanged stats between opens"), 0);
          break;

        case DISABLE_INOTIFY_OPTION:
          disable_inotify = true;
          break;

        case PID_OPTION:
          pid = xdectoumax (optarg, 0, PID_T_MAX, "", _("invalid PID"), 0);
          break;

        case PRESUME_INPUT_PIPE_OPTION:
          presume_input_pipe = true;
          break;

        case 'q':
          *header_mode = never;
          break;

        case 's':
          {
            double s;
            if (! (xstrtod (optarg, NULL, &s, cl_strtod) && 0 <= s))
              die (EXIT_FAILURE, 0,
                   _("invalid number of seconds: %s"), quote (optarg));
            *sleep_interval = s;
          }
          break;

        case 'v':
          *header_mode = always;
          break;

        case 'z':
          line_end = '\0';
          break;

        case_GETOPT_HELP_CHAR;

        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
          die (EXIT_FAILURE, 0, _("option used in invalid context -- %c"), c);

        default:
          usage (EXIT_FAILURE);
        }
    }

  if (reopen_inaccessible_files)
    {
      if (!forever)
        {
          reopen_inaccessible_files = false;
          error (0, 0, _("warning: --retry ignored; --retry is useful"
                         " only when following"));
        }
      else if (follow_mode == Follow_descriptor)
        error (0, 0, _("warning: --retry only effective for the initial open"));
    }

  if (pid && !forever)
    error (0, 0,
           _("warning: PID ignored; --pid=PID is useful only when following"));
  else if (pid && kill (pid, 0) != 0 && errno == ENOSYS)
    {
      error (0, 0, _("warning: --pid=PID is not supported on this system"));
      pid = 0;
    }
}