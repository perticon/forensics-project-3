void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002930(fn0000000000002600(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000006E9E;
	}
	fn0000000000002880(fn0000000000002600(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002880(fn0000000000002600(0x05, "Print the last %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n", null), 0x01);
	fn0000000000002740(stdout, fn0000000000002600(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "  -c, --bytes=[+]NUM       output the last NUM bytes; or use -c +NUM to\n                             output starting with byte NUM of each file\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "  -f, --follow[={name|descriptor}]\n                           output appended data as the file grows;\n                             an absent option argument means 'descriptor'\n  -F                       same as --follow=name --retry\n", null));
	fn0000000000002880(fn0000000000002600(0x05, "  -n, --lines=[+]NUM       output the last NUM lines, instead of the last %d;\n                             or use -n +NUM to output starting with line NUM\n      --max-unchanged-stats=N\n                           with --follow=name, reopen a FILE which has not\n                             changed size after N (default %d) iterations\n                             to see if it has been unlinked or renamed\n                             (this is the usual case of rotated log files);\n                             with inotify, this option is rarely useful\n", null), 0x01);
	fn0000000000002740(stdout, fn0000000000002600(0x05, "      --pid=PID            with -f, terminate after process ID, PID dies\n  -q, --quiet, --silent    never output headers giving file names\n      --retry              keep trying to open a file if it is inaccessible\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "  -s, --sleep-interval=N   with -f, sleep for approximately N seconds\n                             (default 1.0) between iterations;\n                             with inotify and --pid=P, check process P at\n                             least once every N seconds\n  -v, --verbose            always output headers giving file names\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "  -z, --zero-terminated    line delimiter is NUL, not newline\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n\n", null));
	fn0000000000002740(stdout, fn0000000000002600(0x05, "With --follow (-f), tail defaults to following the file descriptor, which\nmeans that even if a tail'ed file is renamed, tail will continue to track\nits end.  This default behavior is not desirable when you really want to\ntrack the actual name of the file, not the file descriptor (e.g., log\nrotation).  Use --follow=name in that case.  That causes tail to track the\nnamed file in a way that accommodates renaming, removal and creation.\n", null));
	struct Eq_6519 * rbx_285 = fp - 0xB8 + 16;
	do
	{
		Eq_24 rsi_287 = rbx_285->qw0000;
		++rbx_285;
	} while (rsi_287 != 0x00 && fn0000000000002770(rsi_287, 0xE039) != 0x00);
	ptr64 r13_300 = rbx_285->qw0008;
	if (r13_300 != 0x00)
	{
		fn0000000000002880(fn0000000000002600(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_24 rax_387 = fn0000000000002870(null, 0x05);
		if (rax_387 == 0x00 || fn0000000000002540(0x03, 0xE0C2, rax_387) == 0x00)
			goto l00000000000071CE;
	}
	else
	{
		fn0000000000002880(fn0000000000002600(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_24 rax_329 = fn0000000000002870(null, 0x05);
		if (rax_329 == 0x00 || fn0000000000002540(0x03, 0xE0C2, rax_329) == 0x00)
		{
			fn0000000000002880(fn0000000000002600(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000720B:
			fn0000000000002880(fn0000000000002600(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000006E9E:
			fn0000000000002910(edi);
		}
		r13_300 = 0xE039;
	}
	fn0000000000002740(stdout, fn0000000000002600(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000071CE:
	fn0000000000002880(fn0000000000002600(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000720B;
}