dump_remainder (bool want_header, char const *pretty_filename, int fd,
                uintmax_t n_bytes)
{
  uintmax_t n_written;
  uintmax_t n_remaining = n_bytes;

  n_written = 0;
  while (true)
    {
      char buffer[BUFSIZ];
      size_t n = MIN (n_remaining, BUFSIZ);
      size_t bytes_read = safe_read (fd, buffer, n);
      if (bytes_read == SAFE_READ_ERROR)
        {
          if (errno != EAGAIN)
            die (EXIT_FAILURE, errno, _("error reading %s"),
                 quoteaf (pretty_filename));
          break;
        }
      if (bytes_read == 0)
        break;
      if (want_header)
        {
          write_header (pretty_filename);
          want_header = false;
        }
      xwrite_stdout (buffer, bytes_read);
      n_written += bytes_read;
      if (n_bytes != COPY_TO_EOF)
        {
          n_remaining -= bytes_read;
          if (n_remaining == 0 || n_bytes == COPY_A_BUFFER)
            break;
        }
    }

  return n_written;
}