int64_t dump_remainder(bool want_header, char * pretty_filename, int32_t fd, int64_t n_bytes) {
    int64_t v1 = __readfsqword(40); // 0x5fff
    int64_t v2 = n_bytes; // 0x6012
    char v3 = want_header; // 0x6012
    int64_t v4 = 0; // 0x6012
    int64_t v5; // 0x5fc0
    int64_t result; // 0x5fc0
    while (true) {
      lab_0x6018:
        // 0x6018
        v5 = v4;
        int64_t v6 = v2;
        int64_t v7; // bp-8264, 0x5fc0
        int64_t v8 = safe_read(fd, (char *)&v7, v6 < 0x2000 ? v6 : 0x2000); // 0x602a
        result = v5;
        switch (v8) {
            case -1: {
                goto lab_0x6070;
            }
            case 0: {
                goto lab_0x607d;
            }
            default: {
                // 0x603d
                if (v3 != 0) {
                    // 0x60b0
                    function_2880();
                    *(char *)&g2 = 0;
                }
                // 0x6042
                v4 = v8 + v5;
                xwrite_stdout_part_0((int64_t)&v7, v8);
                v2 = v6;
                if (n_bytes != -1) {
                    // 0x605a
                    v2 = v6 - v8;
                    result = v4;
                    if (n_bytes == -2 || v2 == 0) {
                        goto lab_0x607d;
                    }
                }
                // 0x6065
                v3 = 0;
                goto lab_0x6018;
            }
        }
    }
  lab_0x6070:;
    int32_t v9 = *(int32_t *)function_2530(); // 0x6075
    result = v5;
    if (v9 == 11) {
      lab_0x607d:
        // 0x607d
        if (v1 == __readfsqword(40)) {
            // 0x6094
            return result;
        }
        // 0x6126
        return function_2630();
    }
    // 0x60ed
    quotearg_style();
    function_2600();
    function_28a0();
    // 0x6126
    return function_2630();
}