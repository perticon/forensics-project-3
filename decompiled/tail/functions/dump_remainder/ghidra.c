long dump_remainder(char param_1,undefined8 param_2,undefined4 param_3,ulong param_4)

{
  long lVar1;
  int *piVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  ulong uVar5;
  char *pcVar6;
  ulong uVar7;
  long lVar8;
  long in_FS_OFFSET;
  undefined local_2048 [8200];
  long local_40;
  
  lVar8 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar7 = param_4;
  while( true ) {
    uVar5 = 0x2000;
    if (uVar7 < 0x2001) {
      uVar5 = uVar7;
    }
    lVar1 = safe_read(param_3,local_2048,uVar5);
    if (lVar1 == -1) break;
    if (lVar1 == 0) goto LAB_0010607d;
    if (param_1 != '\0') {
      pcVar6 = "\n";
      if (first_file_2 != '\0') {
        pcVar6 = "";
      }
      __printf_chk(1,"%s==> %s <==\n",pcVar6,param_2);
      first_file_2 = '\0';
    }
    lVar8 = lVar8 + lVar1;
    xwrite_stdout_part_0(local_2048,lVar1);
    if ((param_4 != 0xffffffffffffffff) &&
       ((uVar7 = uVar7 - lVar1, uVar7 == 0 || (param_4 == 0xfffffffffffffffe)))) goto LAB_0010607d;
    param_1 = '\0';
  }
  piVar2 = __errno_location();
  if (*piVar2 != 0xb) {
    uVar3 = quotearg_style(4,param_2);
    uVar4 = dcgettext(0,"error reading %s",5);
    error(1,*piVar2,uVar4,uVar3);
    goto LAB_00106126;
  }
LAB_0010607d:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar8;
  }
LAB_00106126:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}