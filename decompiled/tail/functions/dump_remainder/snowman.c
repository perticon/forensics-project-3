void** dump_remainder(void** edi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void** v7;
    void** v8;
    void*** rsp9;
    void** r12_10;
    void** v11;
    void** r15_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r13_16;
    void** rax17;
    void** v18;
    void** rdx19;
    void** rsi20;
    void** rdi21;
    void** rax22;
    int1_t zf23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    void** v28;
    int64_t v29;
    int64_t v30;
    void** v31;
    void** v32;
    void** v33;
    void** v34;
    void** rax35;
    void** rax36;
    void** rax37;
    void* rax38;
    void*** rsp39;
    void** v40;
    void** v41;
    void** v42;
    void** v43;
    void** rax44;
    void** v45;
    void** r14_46;
    void* rbx47;
    void* rax48;
    void* rbx49;
    void* rbx50;
    void** rdi51;
    int64_t rdi52;
    void** rax53;
    void* rsp54;
    void* rdx55;
    void* rsp56;
    void** rbx57;
    void* rsp58;
    void** edi59;
    void** rax60;
    void** v61;
    void* rax62;
    void** rax63;
    void** eax64;
    void** eax65;
    void** rax66;
    int64_t rdi67;
    void** rdx68;
    void* rsp69;
    void** rdi70;
    void** rax71;
    void** v72;
    uint32_t ebp73;
    uint32_t eax74;
    void** r13_75;
    void** eax76;
    void** r12d77;
    void** eax78;
    uint1_t zf79;
    void** rsi80;
    void** rax81;
    void** rax82;
    void** rsi83;
    void** rax84;
    int64_t rdi85;
    void** rsi86;
    void** rax87;
    int64_t rdi88;
    void* rax89;
    int64_t v90;
    uint32_t r14d91;
    uint32_t eax92;
    void** r15_93;
    void** rsi94;
    int32_t eax95;
    uint32_t v96;
    void** rax97;
    uint32_t r14d98;
    void** edx99;
    uint32_t eax100;
    void** rax101;
    void** rsi102;
    void** rdi103;
    int32_t eax104;
    uint32_t v105;
    void** eax106;
    void** rdi107;
    void** rsi108;
    int32_t eax109;
    uint32_t v110;
    uint32_t eax111;
    void** rax112;
    void** rax113;
    void** rsi114;
    void** rax115;
    void** rax116;
    uint32_t v117;
    int1_t zf118;
    void** rsi119;
    void** rax120;
    int64_t rdi121;
    signed char al122;
    void* rsp123;
    int1_t zf124;
    void** rsi125;
    void** rax126;
    void** rax127;
    void** eax128;
    void** rsi129;
    uint32_t edx130;
    void** rax131;
    void** rax132;
    void* rsp133;
    void** r13_134;
    void** rcx135;
    void* rsp136;
    uint32_t r15d137;
    int64_t v138;
    int64_t v139;
    int64_t v140;
    void** v141;
    int32_t v142;
    void** rax143;
    int64_t rdi144;
    void** rax145;
    int1_t zf146;
    void** v147;
    int1_t zf148;
    int64_t v149;
    void** rax150;
    void** rax151;
    void* rsp152;
    void** rsi153;
    void** rax154;
    int64_t rdi155;
    void** rax156;
    int64_t rdi157;
    void** rsi158;
    void** rax159;
    void** rax160;
    void** v161;
    void** rax162;
    int64_t rdi163;
    void** rax164;
    int1_t zf165;
    int1_t zf166;
    void** edx167;
    void** rax168;
    struct s0* rdi169;
    int32_t eax170;
    void** rax171;
    void** v172;
    int32_t edx173;
    void** v174;
    int64_t rdx175;
    void** v176;
    void** v177;
    int64_t rax178;
    void** rdi179;
    void** rax180;
    void** rax181;
    void** rdx182;
    int64_t rsi183;
    struct s1* rax184;
    int64_t rdi185;
    void** rax186;
    void** rdi187;
    void** rax188;
    void** rsi189;
    void** rax190;
    int64_t rdi191;
    void** rax192;
    void** rax193;

    v7 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(v8)));
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 40);
    r12_10 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
    v11 = rsi;
    r15_12 = edx;
    rbp13 = edi;
    *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    rbx14 = rcx;
    v15 = rcx;
    r13_16 = reinterpret_cast<void**>(rsp9 + 16);
    rax17 = g28;
    v18 = rax17;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdx19) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
        rsi20 = r13_16;
        rdi21 = r15_12;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx14) <= reinterpret_cast<unsigned char>(0x2000)) {
            rdx19 = rbx14;
        }
        rax22 = safe_read(rdi21, rsi20, rdx19, rcx);
        rsp9 = rsp9 - 8 + 8;
        if (rax22 == 0xffffffffffffffff) 
            break;
        if (!rax22) 
            goto addr_607d_6;
        if (*reinterpret_cast<signed char*>(&rbp13)) {
            zf23 = first_file_2 == 0;
            rcx = v11;
            rdx19 = reinterpret_cast<void**>("\n");
            if (!zf23) {
                rdx19 = reinterpret_cast<void**>(0xea41);
            }
            fun_2880(1, "%s==> %s <==\n", rdx19, rcx, r8, r9, v15, v11, v24, v25, v26, v7, v27, v28, v29, v30, v31, v32, v33, v34);
            rsp9 = rsp9 - 8 + 8;
            first_file_2 = 0;
        }
        rsi20 = rax22;
        rdi21 = r13_16;
        r12_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_10) + reinterpret_cast<unsigned char>(rax22));
        xwrite_stdout_part_0(rdi21, rsi20, rdx19, rcx, r8, r9);
        rsp9 = rsp9 - 8 + 8;
        if (v15 == 0xffffffffffffffff) 
            goto addr_6065_12;
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rax22));
        if (!rbx14) 
            goto addr_607d_6;
        if (v15 == 0xfffffffffffffffe) 
            goto addr_607d_6;
        addr_6065_12:
        rbp13 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    }
    rax35 = fun_2530();
    rsp9 = rsp9 - 8 + 8;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax35) == 11)) {
        rax36 = quotearg_style(4, v11);
        rax37 = fun_2600();
        rsi20 = *reinterpret_cast<void***>(rax35);
        rcx = rax36;
        rdi21 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx19 = rax37;
        fun_28a0();
        rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
    } else {
        addr_607d_6:
        rax38 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
        if (!rax38) {
            return r12_10;
        }
    }
    fun_2630();
    rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp9 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8) - reinterpret_cast<int64_t>("sterTMCloneTable") - reinterpret_cast<int64_t>("sterTMCloneTable") - 56);
    v40 = rdi21;
    v41 = rcx;
    v42 = r8;
    v43 = r9;
    rax44 = g28;
    v45 = rax44;
    if (!rdx19) {
        addr_6330_20:
    } else {
        r12_10 = rsi20;
        r14_46 = rdx19;
        rbx47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
        rax48 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbx47) >> 63) >> 51);
        rbx49 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx47) + reinterpret_cast<uint64_t>(rax48));
        *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<uint32_t*>(&rbx49) & 0x1fff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx50) - reinterpret_cast<uint64_t>(rax48));
        if (!rbx14) {
            rbx14 = reinterpret_cast<void**>(0x2000);
        }
        rdi51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rbx14));
        rbp13 = rdi51;
        r13_16 = rdi51;
        *reinterpret_cast<void***>(&rdi52) = r12_10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
        rax53 = fun_26b0(rdi52, rdi52);
        rsp54 = reinterpret_cast<void*>(rsp39 - 8 + 8);
        if (reinterpret_cast<signed char>(rax53) < reinterpret_cast<signed char>(0)) 
            goto addr_63c7_24; else 
            goto addr_61d3_25;
    }
    addr_6300_26:
    rdx55 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v45) - reinterpret_cast<unsigned char>(g28));
    if (rdx55) {
        fun_2630();
        rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
        goto addr_63b8_28;
    } else {
        goto v15;
    }
    addr_63c7_24:
    xlseek_part_0(rbp13, 0, v40, rcx, r8, r9);
    rbx57 = rbp13;
    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0xa0);
    edi59 = *reinterpret_cast<void***>(rbp13 + 56);
    rax60 = g28;
    v61 = rax60;
    if (edi59 == 0xffffffff) {
        addr_647a_31:
        rax62 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(g28));
        if (rax62) {
            fun_2630();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
        } else {
            goto v43;
        }
    } else {
        r13_16 = *reinterpret_cast<void***>(rbx57);
        if (*reinterpret_cast<void***>(r13_16) == 45 && !*reinterpret_cast<void***>(r13_16 + 1)) {
            rax63 = fun_2600();
            r13_16 = rax63;
            eax64 = fun_2980();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8);
            r12_10 = eax64;
            if (!eax64) 
                goto addr_6431_36; else 
                goto addr_64df_37;
        }
        eax65 = fun_2980();
        rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
        r12_10 = eax65;
        if (eax65) {
            addr_64df_37:
            rax66 = fun_2530();
            *reinterpret_cast<void***>(&rdi67) = *reinterpret_cast<void***>(rbx57 + 56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
            *reinterpret_cast<void***>(rbx57 + 60) = *reinterpret_cast<void***>(rax66);
            close_fd(rdi67, r13_16, rdi67, r13_16);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rbx57 + 56) = reinterpret_cast<void**>(0xffffffff);
            goto addr_647a_31;
        } else {
            addr_6431_36:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx57 + 48)) & 0xf000) == 0x8000) 
                goto addr_6500_39; else 
                goto addr_6444_40;
        }
    }
    addr_65f9_41:
    rdx68 = r13_16;
    xlseek_part_0(0, 0, rdx68, rcx, r8, r9);
    rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rdi70 = __cxa_finalize;
    rax71 = g28;
    v72 = rax71;
    ebp73 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi70) - 45);
    if (!ebp73) {
        ebp73 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi70 + 1));
    }
    eax74 = g36;
    r13_75 = g3c;
    if (!ebp73) {
        eax76 = r13_75;
        r12d77 = reinterpret_cast<void**>(0);
    } else {
        eax78 = open_safer(rdi70, rdi70);
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
        r12d77 = eax78;
        eax76 = g3c;
    }
    zf79 = reinterpret_cast<uint1_t>(g38 == 0xffffffff);
    *reinterpret_cast<unsigned char*>(&rdx68) = zf79;
    if (*reinterpret_cast<unsigned char*>(&rdx68) == static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax76 == 0))) {
        fun_26c0("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
        do {
            fun_2630();
            addr_6e01_50:
            ++r13_75;
            if (r13_75) {
                addr_6b83_51:
                rsi80 = __cxa_finalize;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi80) == 45) && !*reinterpret_cast<void***>(rsi80 + 1)) {
                    rax81 = fun_2600();
                    rsi80 = rax81;
                }
                rax82 = quotearg_style(4, rsi80, 4, rsi80);
                r13_75 = rax82;
                fun_2600();
                fun_28a0();
            }
            addr_6941_55:
            rsi83 = __cxa_finalize;
            addr_66f0_56:
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi83) == 45) && !*reinterpret_cast<void***>(rsi83 + 1)) {
                rax84 = fun_2600();
                rsi83 = rax84;
            }
            *reinterpret_cast<void***>(&rdi85) = r12d77;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi85) + 4) = 0;
            close_fd(rdi85, rsi83, rdi85, rsi83);
            rsi86 = __cxa_finalize;
            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi86) == 45) && !*reinterpret_cast<void***>(rsi86 + 1)) {
                rax87 = fun_2600();
                rsi86 = rax87;
            }
            *reinterpret_cast<void***>(&rdi88) = g38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi88) + 4) = 0;
            close_fd(rdi88, rsi86, rdi88, rsi86);
            g38 = reinterpret_cast<void**>(0xffffffff);
            addr_674c_61:
            rax89 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v72) - reinterpret_cast<unsigned char>(g28));
        } while (rax89);
        goto v90;
    }
    r14d91 = reopen_inaccessible_files;
    eax92 = disable_inotify;
    if (*reinterpret_cast<signed char*>(&r14d91)) {
        if (r12d77 == 0xffffffff) {
            addr_6918_65:
            g36 = 0;
            r15_93 = __cxa_finalize;
            if (!*reinterpret_cast<signed char*>(&eax92)) {
                rsi94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16);
                eax95 = fun_2720(r15_93, rsi94, r15_93, rsi94);
                if (eax95 || (v96 & 0xf000) != 0xa000) {
                    addr_66b9_67:
                    rax97 = fun_2530();
                    r14d98 = g36;
                    rsi83 = __cxa_finalize;
                    edx99 = *reinterpret_cast<void***>(rax97);
                    r15_93 = rsi83;
                    g3c = edx99;
                    if (*reinterpret_cast<signed char*>(&r14d98)) {
                        eax100 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi83) - 45);
                        if (!eax100) {
                            eax100 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi83 + 1));
                            if (edx99 == r13_75) 
                                goto addr_66f0_56;
                        } else {
                            if (edx99 == r13_75) {
                                goto addr_66f0_56;
                            }
                        }
                        if (!eax100) {
                            fun_2600();
                        }
                        quotearg_n_style_colon();
                        fun_28a0();
                        rsi83 = __cxa_finalize;
                        goto addr_66f0_56;
                    }
                } else {
                    goto addr_6c10_76;
                }
            } else {
                rax101 = fun_2530();
                g3c = *reinterpret_cast<void***>(rax101);
            }
        } else {
            g36 = 1;
            rsi102 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16);
            if (*reinterpret_cast<signed char*>(&eax92) || ((rdi103 = __cxa_finalize, eax104 = fun_2720(rdi103, rsi102, rdi103, rsi102), rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8), !!eax104) || (v105 & 0xf000) != 0xa000)) {
                addr_67a3_79:
                eax106 = fun_2980();
                rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
                if (reinterpret_cast<signed char>(eax106) < reinterpret_cast<signed char>(0)) 
                    goto addr_66b9_67; else 
                    goto addr_67b3_80;
            } else {
                goto addr_6c10_76;
            }
        }
    } else {
        g36 = 1;
        if (*reinterpret_cast<signed char*>(&eax92) || ((rdi107 = __cxa_finalize, rsi108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp69) + 16), eax109 = fun_2720(rdi107, rsi108, rdi107, rsi108), rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8), !!eax109) || (v110 & 0xf000) != 0xa000)) {
            if (reinterpret_cast<int1_t>(r12d77 == 0xffffffff)) 
                goto addr_66b9_67;
            goto addr_67a3_79;
        } else {
            goto addr_6c10_76;
        }
    }
    eax111 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r15_93) - 45);
    if (!eax111) {
        eax111 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_93 + 1));
        if (!*reinterpret_cast<signed char*>(&eax74)) 
            goto addr_6941_55;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax74)) 
            goto addr_6941_55;
    }
    if (!eax111) {
        rax112 = fun_2600();
        r15_93 = rax112;
    }
    rax113 = quotearg_style(4, r15_93, 4, r15_93);
    r13_75 = rax113;
    fun_2600();
    fun_28a0();
    rsi83 = __cxa_finalize;
    goto addr_66f0_56;
    addr_6c10_76:
    rsi114 = __cxa_finalize;
    g3c = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&g34) = 1;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi114) == 45) && !*reinterpret_cast<void***>(rsi114 + 1)) {
        rax115 = fun_2600();
        rsi114 = rax115;
    }
    rax116 = quotearg_style(4, rsi114, 4, rsi114);
    r13_75 = rax116;
    fun_2600();
    fun_28a0();
    rsi83 = __cxa_finalize;
    goto addr_66f0_56;
    addr_67b3_80:
    if ((v117 & 0xf000) - reinterpret_cast<int32_t>("sterTMCloneTable") & 0xffffe000 && (v117 & reinterpret_cast<uint32_t>("(")) != 0x8000) {
        g3c = reinterpret_cast<void**>(0xffffffff);
        g36 = 0;
        if (!*reinterpret_cast<signed char*>(&r14d91) || (zf118 = follow_mode == 1, !zf118)) {
            *reinterpret_cast<signed char*>(&g34) = 1;
            if (*reinterpret_cast<signed char*>(&eax74)) 
                goto addr_6b6d_96;
            if (r13_75 == 0xffffffff) 
                goto addr_6941_55;
            addr_6b6d_96:
            fun_2600();
            goto addr_6b83_51;
        } else {
            *reinterpret_cast<signed char*>(&g34) = 0;
            if (!*reinterpret_cast<signed char*>(&eax74)) 
                goto addr_6e01_50;
            goto addr_6b83_51;
        }
    }
    rsi119 = __cxa_finalize;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi119) == 45) && !*reinterpret_cast<void***>(rsi119 + 1)) {
        *reinterpret_cast<int32_t*>(&rdx68) = 5;
        *reinterpret_cast<int32_t*>(&rdx68 + 4) = 0;
        rax120 = fun_2600();
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
        rsi119 = rax120;
    }
    *reinterpret_cast<void***>(&rdi121) = r12d77;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi121) + 4) = 0;
    al122 = fremote(rdi121, rsi119, rdx68);
    rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&g34) + 1) = al122;
    if (al122 && (zf124 = disable_inotify == 0, zf124)) {
        rsi125 = __cxa_finalize;
        g3c = reinterpret_cast<void**>(0xffffffff);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi125) == 45) && !*reinterpret_cast<void***>(rsi125 + 1)) {
            rax126 = fun_2600();
            rsi125 = rax126;
        }
        rax127 = quotearg_style(4, rsi125, 4, rsi125);
        r13_75 = rax127;
        fun_2600();
        fun_28a0();
        g34 = 0x101;
        goto addr_6941_55;
    }
    r13_75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_75) & 0xfffffffd);
    g3c = reinterpret_cast<void**>(0);
    eax128 = g38;
    if (r13_75) 
        goto addr_6830_107;
    rsi129 = __cxa_finalize;
    edx130 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi129) - 45);
    if (!edx130) {
        edx130 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi129 + 1));
    }
    if (eax128 != 0xffffffff) 
        goto addr_6a80_111;
    if (!edx130) {
        rax131 = fun_2600();
        rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
        rsi129 = rax131;
    }
    rax132 = quotearg_style(4, rsi129, 4, rsi129);
    rsp133 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
    r13_134 = rax132;
    addr_685e_115:
    fun_2600();
    rcx135 = r13_134;
    fun_28a0();
    rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp133) - 8 + 8 - 8 + 8);
    addr_6876_116:
    r15d137 = 0;
    r13_75 = __cxa_finalize;
    if (!ebp73) {
        r15d137 = 0xffffffff;
    }
    g38 = r12d77;
    g8 = 0;
    g10 = v138;
    g40 = r15d137;
    g18 = v139;
    g58 = 0;
    g20 = v140;
    *reinterpret_cast<signed char*>(&g34) = 0;
    g28 = v141;
    g30 = v142;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_75) == 45) && !*reinterpret_cast<void***>(r13_75 + 1)) {
        rax143 = fun_2600();
        rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp136) - 8 + 8);
        r13_75 = rax143;
    }
    *reinterpret_cast<void***>(&rdi144) = r12d77;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi144) + 4) = 0;
    rax145 = fun_26b0(rdi144);
    if (reinterpret_cast<signed char>(rax145) >= reinterpret_cast<signed char>(0)) 
        goto addr_674c_61;
    *reinterpret_cast<signed char*>(&eax92) = xlseek_part_0(0, 0, r13_75, rcx135, r8, r9);
    rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp136) - 8 + 8 - 8 + 8);
    goto addr_6918_65;
    addr_6a80_111:
    zf146 = g28 == v147;
    if (!zf146 || (zf148 = g20 == v149, !zf148)) {
        if (!edx130) {
            rax150 = fun_2600();
            rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
            rsi129 = rax150;
        }
        rax151 = quotearg_style(4, rsi129, 4, rsi129);
        fun_2600();
        rcx135 = rax151;
        fun_28a0();
        rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8 - 8 + 8 - 8 + 8);
        rsi153 = __cxa_finalize;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi153) == 45) && !*reinterpret_cast<void***>(rsi153 + 1)) {
            rax154 = fun_2600();
            rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8);
            rsi153 = rax154;
        }
        *reinterpret_cast<void***>(&rdi155) = g38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi155) + 4) = 0;
        close_fd(rdi155, rsi153, rdi155, rsi153);
        rsp136 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8);
        goto addr_6876_116;
    } else {
        if (!edx130) {
            rax156 = fun_2600();
            rsi129 = rax156;
        }
        *reinterpret_cast<void***>(&rdi157) = r12d77;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi157) + 4) = 0;
        close_fd(rdi157, rsi129, rdi157, rsi129);
        goto addr_674c_61;
    }
    addr_6830_107:
    if (!reinterpret_cast<int1_t>(eax128 == 0xffffffff)) {
        fun_26c0("f->fd == -1", "src/tail.c", 0x42f, "recheck");
    } else {
        rsi158 = __cxa_finalize;
        if (*reinterpret_cast<void***>(rsi158) == 45 && !*reinterpret_cast<void***>(rsi158 + 1)) {
            rax159 = fun_2600();
            rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
            rsi158 = rax159;
        }
        rax160 = quotearg_style(4, rsi158, 4, rsi158);
        rsp133 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8);
        r13_134 = rax160;
        goto addr_685e_115;
    }
    addr_6500_39:
    if (reinterpret_cast<signed char>(v161) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 8))) {
        while (rax162 = quotearg_n_style_colon(), fun_2600(), rcx = rax162, fun_28a0(), *reinterpret_cast<void***>(&rdi163) = *reinterpret_cast<void***>(rbx57 + 56), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi163) + 4) = 0, rax164 = fun_26b0(rdi163), rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8), reinterpret_cast<signed char>(rax164) >= reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<void***>(rbx57 + 8) = reinterpret_cast<void**>(0);
            addr_6444_40:
            zf165 = print_headers == 0;
            if (!zf165) {
                r12_10 = reinterpret_cast<void**>(0);
                zf166 = __cxa_finalize == rbx57;
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!zf166);
            }
            edx167 = *reinterpret_cast<void***>(rbx57 + 56);
            rcx = reinterpret_cast<void**>(0xffffffffffffffff);
            rax168 = dump_remainder(r12_10, r13_16, edx167, 0xffffffffffffffff, r8, r9);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
            *reinterpret_cast<void***>(rbx57 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx57 + 8)) + reinterpret_cast<unsigned char>(rax168));
            if (!rax168) 
                goto addr_647a_31;
            __cxa_finalize = rbx57;
            rdi169 = stdout;
            eax170 = fun_2940(rdi169, r13_16, rdi169, r13_16);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
            if (!eax170) 
                goto addr_647a_31;
            rax171 = fun_2600();
            r12_10 = rax171;
            fun_2530();
            fun_28a0();
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto addr_65f9_41;
    } else {
        if (v172 != *reinterpret_cast<void***>(rbx57 + 8)) 
            goto addr_6444_40;
        edx173 = 0;
        *reinterpret_cast<unsigned char*>(&edx173) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v174) < reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 16)));
        *reinterpret_cast<uint32_t*>(&rdx175) = edx173 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v176) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 16))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx175) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 24)) < reinterpret_cast<signed char>(v177));
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax178) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx57 + 24)) > reinterpret_cast<signed char>(v177))) - *reinterpret_cast<uint32_t*>(&rcx);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax178) + 4) = 0;
        if (static_cast<int32_t>(rax178 + rdx175 * 2)) 
            goto addr_6444_40;
        goto addr_647a_31;
    }
    addr_61d3_25:
    r15_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 32);
    rdi179 = r12_10;
    *reinterpret_cast<int32_t*>(&rdi179 + 4) = 0;
    rax180 = safe_read(rdi179, r15_12, rbx14, rcx);
    rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
    rbp13 = rax180;
    if (rax180 == 0xffffffffffffffff) {
        addr_6337_144:
        rax181 = quotearg_style(4, v40, 4, v40);
        r13_16 = rax181;
        fun_2600();
        fun_2530();
        rcx = r13_16;
        fun_28a0();
        rsp39 = rsp39 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_6300_26;
    } else {
        rcx = v43;
        *reinterpret_cast<int32_t*>(&rbx14) = line_end;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax180));
        if (rbp13) {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp39) + reinterpret_cast<unsigned char>(rbp13) + 31) != *reinterpret_cast<signed char*>(&rbx14)) {
                --r14_46;
            }
        }
        while (1) {
            rdx182 = rbp13;
            while (rdx182 && (*reinterpret_cast<int32_t*>(&rsi183) = *reinterpret_cast<int32_t*>(&rbx14), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi183) + 4) = 0, rax184 = fun_28b0(r15_12, rsi183), rsp39 = rsp39 - 8 + 8, !!rax184)) {
                rdx182 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax184) - reinterpret_cast<unsigned char>(r15_12));
                if (!r14_46) 
                    goto addr_62c8_152;
                --r14_46;
            }
            if (r13_16 == v41) 
                goto addr_6379_155;
            r13_16 = r13_16 - 0x2000;
            *reinterpret_cast<void***>(&rdi185) = r12_10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi185) + 4) = 0;
            rax186 = fun_26b0(rdi185, rdi185);
            rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
            if (reinterpret_cast<signed char>(rax186) < reinterpret_cast<signed char>(0)) 
                goto addr_63b8_28;
            rdi187 = r12_10;
            *reinterpret_cast<int32_t*>(&rdi187 + 4) = 0;
            rax188 = safe_read(rdi187, r15_12, 0x2000, rcx);
            rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            rbp13 = rax188;
            if (rax188 == 0xffffffffffffffff) 
                goto addr_6337_144;
            rcx = v43;
            *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax188));
            if (!rbp13) 
                goto addr_6330_20;
            *reinterpret_cast<int32_t*>(&rbx14) = line_end;
        }
    }
    addr_62c8_152:
    rsi189 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(rdx182));
    if (rsi189) {
        xwrite_stdout_part_0(&rax184->f1, rsi189, rdx182, rcx, r8, r9);
        rsp39 = rsp39 - 8 + 8;
    }
    rax190 = dump_remainder(0, v40, r12_10, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v42) - reinterpret_cast<unsigned char>(rbp13)) - reinterpret_cast<unsigned char>(r13_16), r8, r9);
    rsp39 = rsp39 - 8 + 8;
    rcx = v43;
    *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)) + reinterpret_cast<unsigned char>(rax190));
    goto addr_6300_26;
    addr_6379_155:
    *reinterpret_cast<void***>(&rdi191) = r12_10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi191) + 4) = 0;
    rax192 = fun_26b0(rdi191, rdi191);
    rsp56 = reinterpret_cast<void*>(rsp39 - 8 + 8);
    if (reinterpret_cast<signed char>(rax192) < reinterpret_cast<signed char>(0)) {
        addr_63b8_28:
        xlseek_part_0(r13_16, 0, v40, rcx, r8, r9);
        rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        goto addr_63c7_24;
    } else {
        rax193 = dump_remainder(0, v40, r12_10, v42, r8, r9);
        rsp39 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
        rcx = v43;
        *reinterpret_cast<void***>(rcx) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax193) + reinterpret_cast<unsigned char>(r13_16));
        goto addr_6300_26;
    }
}