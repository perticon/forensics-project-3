signed char fremote(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rsi5;
    int32_t eax6;
    int64_t rcx7;
    int64_t v8;
    void** rax9;
    int64_t r12_10;
    void* rax11;
    uint32_t eax12;
    int32_t eax13;
    int64_t v14;
    int64_t v15;
    uint64_t r12_16;
    int64_t rcx17;
    uint64_t r12_18;

    rax4 = g28;
    rsi5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x88);
    eax6 = fun_28f0();
    if (!eax6) {
        rcx7 = v8;
        if (rcx7 == 0x2bad1dea) 
            goto addr_5b68_3;
        if (rcx7 <= 0x2bad1dea) 
            goto addr_5493_5;
    } else {
        rax9 = fun_2530();
        *reinterpret_cast<uint32_t*>(&r12_10) = 1;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 38)) {
            quotearg_style(4, rsi);
            fun_2600();
            rsi5 = *reinterpret_cast<void***>(rax9);
            *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            fun_28a0();
            goto addr_5458_8;
        } else {
            goto addr_5458_8;
        }
    }
    if (rcx7 == 0x62656570) 
        goto addr_5b68_3;
    if (rcx7 > 0x62656570) 
        goto addr_5517_12;
    if (rcx7 == 0x53464846) 
        goto addr_5b68_3;
    if (rcx7 > 0x53464846) 
        goto addr_558b_15;
    if (rcx7 == 0x453dcd28) 
        goto addr_5b68_3;
    if (rcx7 > 0x453dcd28) 
        goto addr_568b_18;
    if (rcx7 == 0x42494e4d) 
        goto addr_5b68_3;
    if (rcx7 > 0x42494e4d) 
        goto addr_5833_21;
    if (rcx7 == 0x3153464a || rcx7 == 0x42465331) {
        addr_5b68_3:
        *reinterpret_cast<uint32_t*>(&r12_10) = 0;
    } else {
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x2fc12fc1);
    }
    addr_5458_8:
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rax11) {
        eax12 = *reinterpret_cast<uint32_t*>(&r12_10);
        return *reinterpret_cast<signed char*>(&eax12);
    }
    fun_2630();
    if (static_cast<uint32_t>(rdi + 1) <= 1) 
        goto addr_5b88_27;
    eax13 = fun_26f0();
    if (eax13) {
        quotearg_style(4, rsi5, 4, rsi5);
        fun_2600();
        fun_2530();
    } else {
        goto v14;
    }
    addr_5b88_27:
    goto v15;
    addr_5833_21:
    if (rcx7 == 0x43415d53) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x444d4142);
    goto addr_5458_8;
    addr_568b_18:
    if (rcx7 == 0x52654973) 
        goto addr_5b68_3;
    if (rcx7 > 0x52654973) 
        goto addr_569e_34;
    if (rcx7 == 0x454d444d) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x45584653);
    goto addr_5458_8;
    addr_569e_34:
    if (rcx7 == 0x5345434d) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x5346314d);
    goto addr_5458_8;
    addr_558b_15:
    if (rcx7 == 0x58465342) 
        goto addr_5b68_3;
    if (rcx7 > 0x58465342) 
        goto addr_559e_39;
    if (rcx7 == 0x54190100) 
        goto addr_5b68_3;
    if (rcx7 > 0x54190100) 
        goto addr_589b_42;
    if (rcx7 == 0x5346544e) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x534f434b);
    goto addr_5458_8;
    addr_589b_42:
    if (rcx7 == 0x565a4653) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x58295829);
    goto addr_5458_8;
    addr_559e_39:
    if (rcx7 == 0x5dca2df5) 
        goto addr_5b68_3;
    if (rcx7 > 0x5dca2df5) 
        goto addr_55b1_47;
    if (rcx7 == 0x5a3c69f0) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x5a4f4653);
    goto addr_5458_8;
    addr_55b1_47:
    if (rcx7 == 0x6165676c) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x62646576);
    goto addr_5458_8;
    addr_5517_12:
    if (rcx7 == 0x858458f6) 
        goto addr_5b68_3;
    if (rcx7 > 0x858458f6) 
        goto addr_552b_52;
    if (rcx7 == 0x6c6f6f70) 
        goto addr_5b68_3;
    if (rcx7 > 0x6c6f6f70) 
        goto addr_56d3_55;
    if (rcx7 == 0x64646178) 
        goto addr_5b68_3;
    if (rcx7 > 0x64646178) 
        goto addr_57cb_58;
    if (rcx7 == 0x63677270) 
        goto addr_5b68_3;
    if (rcx7 == 0x64626720) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x62656572);
    goto addr_5458_8;
    addr_57cb_58:
    if (rcx7 == 0x67596969) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x68191122);
    goto addr_5458_8;
    addr_56d3_55:
    if (rcx7 == 0x73717368) 
        goto addr_5b68_3;
    if (rcx7 > 0x73717368) 
        goto addr_56e6_64;
    if (rcx7 == 0x6e736673) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x73636673);
    goto addr_5458_8;
    addr_56e6_64:
    if (rcx7 == 0x73727279) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x74726163);
    goto addr_5458_8;
    addr_552b_52:
    if (rcx7 == 0xcafe4a11) 
        goto addr_5b68_3;
    if (rcx7 > 0xcafe4a11) 
        goto addr_553f_69;
    if (rcx7 == 0xabba1974) 
        goto addr_5b68_3;
    if (rcx7 > 0xabba1974) 
        goto addr_5865_72;
    if (rcx7 == 0x9123683e) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x958458f6);
    goto addr_5458_8;
    addr_5865_72:
    if (rcx7 == 0xc7571590) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xc97e8168);
    goto addr_5458_8;
    addr_553f_69:
    if (rcx7 == 0xf2f52010) 
        goto addr_5b68_3;
    if (rcx7 > 0xf2f52010) 
        goto addr_5553_77;
    if (rcx7 == 0xde5e81e4) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xe0f5e1e2);
    goto addr_5458_8;
    addr_5553_77:
    if (rcx7 == 0xf97cff8c) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xf995e849);
    goto addr_5458_8;
    addr_5493_5:
    if (rcx7 > 0x9fa2) {
        if (rcx7 > 0x12ff7b7) {
            if (rcx7 == 0x15013346) 
                goto addr_5b68_3;
            if (rcx7 > 0x15013346) 
                goto addr_5643_84;
        } else {
            if (rcx7 > 0x12ff7b3) 
                goto addr_5b68_3;
            if (rcx7 == 0x27e0eb) 
                goto addr_5b68_3;
            if (rcx7 <= 0x27e0eb) 
                goto addr_57f0_88; else 
                goto addr_55f9_89;
        }
    } else {
        if (rcx7 > 0x9f9f) 
            goto addr_5b68_3;
        if (rcx7 > 0x4006) 
            goto addr_5708_92; else 
            goto addr_54ba_93;
    }
    if (rcx7 == 0xbad1dea) 
        goto addr_5b68_3;
    if (rcx7 > 0xbad1dea) 
        goto addr_58d3_96;
    if (rcx7 == 0x7655821) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x9041934);
    goto addr_5458_8;
    addr_58d3_96:
    if (rcx7 == 0x11307854) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x13661366);
    goto addr_5458_8;
    addr_5643_84:
    if (rcx7 == 0x2011bab0) 
        goto addr_5b68_3;
    if (rcx7 > 0x2011bab0) 
        goto addr_5656_101;
    if (rcx7 == 0x19800202) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x1badface);
    goto addr_5458_8;
    addr_5656_101:
    if (rcx7 == 0x24051905) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x28cd3d45);
    goto addr_5458_8;
    addr_57f0_88:
    if (rcx7 == 0xef53) 
        goto addr_5b68_3;
    if (rcx7 > 0xef53) 
        goto addr_5803_106;
    if (rcx7 == 0xadff) 
        goto addr_5b68_3;
    if (rcx7 == 0xef51) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(rcx7 == "%("));
    goto addr_5458_8;
    addr_5803_106:
    if (rcx7 == 0xf15f) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x11954);
    goto addr_5458_8;
    addr_55f9_89:
    if (rcx7 == 0x1021994) 
        goto addr_5b68_3;
    if (rcx7 > 0x1021994) 
        goto addr_560c_112;
    if (rcx7 == 0x414a53) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0xc0ffee);
    goto addr_5458_8;
    addr_560c_112:
    if (rcx7 == 0x1021997) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x12fd16d);
    goto addr_5458_8;
    addr_5708_92:
    if (rcx7 == 0x4d5a) 
        goto addr_5b68_3;
    if (rcx7 > 0x4d5a) 
        goto addr_571b_117;
    if (rcx7 == 0x4858) 
        goto addr_5b68_3;
    if (rcx7 > 0x4858) 
        goto addr_58ff_120;
    if (rcx7 == 0x4244) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x482b);
    goto addr_5458_8;
    addr_58ff_120:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x4d44);
    goto addr_5458_8;
    addr_571b_117:
    if (rcx7 == 0x72b6) 
        goto addr_5b68_3;
    if (rcx7 > 0x72b6) 
        goto addr_572a_124;
    if (rcx7 == 0x5df5) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x7275);
    goto addr_5458_8;
    addr_572a_124:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x9660);
    goto addr_5458_8;
    addr_54ba_93:
    if (rcx7 > 0x3fff) {
        r12_16 = 81 >> *reinterpret_cast<signed char*>(&rcx7);
        *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_16) & 1 ^ 1;
        goto addr_5458_8;
    } else {
        if (rcx7 > 0x138f) {
            if (rcx7 == 0x2478) 
                goto addr_5b68_3;
            if (rcx7 > 0x2478) 
                goto addr_574f_131;
        } else {
            if (rcx7 > 0x1372) {
                rcx17 = rcx7 - 0x1373;
                r12_18 = 0x10001401 >> *reinterpret_cast<signed char*>(&rcx17);
                *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_18) & 1 ^ 1;
                goto addr_5458_8;
            } else {
                if (rcx7 == 0x187) 
                    goto addr_5b68_3;
                if (rcx7 <= 0x187) {
                    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!!(rcx7 - 47 & 0xfffffffffffffffb));
                    goto addr_5458_8;
                } else {
                    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x7c0);
                    goto addr_5458_8;
                }
            }
        }
    }
    if (rcx7 == 0x1cd1) 
        goto addr_5b68_3;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x2468);
    goto addr_5458_8;
    addr_574f_131:
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(rcx7 != 0x3434);
    goto addr_5458_8;
}