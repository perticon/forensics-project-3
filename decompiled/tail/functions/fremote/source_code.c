fremote (int fd, char const *name)
{
  bool remote = true;           /* be conservative (poll by default).  */

#if HAVE_FSTATFS && HAVE_STRUCT_STATFS_F_TYPE \
 && (defined __linux__ || defined __ANDROID__)
  struct statfs buf;
  int err = fstatfs (fd, &buf);
  if (err != 0)
    {
      /* On at least linux-2.6.38, fstatfs fails with ENOSYS when FD
         is open on a pipe.  Treat that like a remote file.  */
      if (errno != ENOSYS)
        error (0, errno, _("cannot determine location of %s. "
                           "reverting to polling"), quoteaf (name));
    }
  else
    {
      switch (is_local_fs_type (buf.f_type))
        {
        case 0:
          break;
        case -1:
          /* Treat unrecognized file systems as "remote", so caller polls.
             Note README-release has instructions for syncing the internal
             list with the latest Linux kernel file system constants.  */
          break;
        case 1:
          remote = false;
          break;
        default:
          assert (!"unexpected return value from is_local_fs_type");
        }
    }
#endif

  return remote;
}