uint fremote(int param_1,undefined8 param_2)

{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  uint unaff_R12D;
  uint uVar5;
  long in_FS_OFFSET;
  statfs local_a8;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = fstatfs(param_1,&local_a8);
  if (iVar1 != 0) {
    piVar2 = __errno_location();
    uVar5 = 1;
    if (*piVar2 != 0x26) {
      uVar3 = quotearg_style(4,param_2);
      uVar4 = dcgettext(0,"cannot determine location of %s. reverting to polling",5);
      error(0,*piVar2,uVar4,uVar3);
    }
    goto LAB_00105458;
  }
  if (local_a8.f_type != 0x2bad1dea) {
    if (local_a8.f_type < 0x2bad1deb) {
      if (local_a8.f_type < 0x9fa3) {
        if (local_a8.f_type < 0x9fa0) {
          if (local_a8.f_type < 0x4007) {
            if (0x3fff < local_a8.f_type) {
              uVar5 = (uint)(0x51 >> ((byte)local_a8.f_type & 0x3f)) & 1 ^ 1;
              goto LAB_00105458;
            }
            if (local_a8.f_type < 0x1390) {
              if (0x1372 < local_a8.f_type) {
                uVar5 = (uint)(0x10001401 >> ((byte)local_a8.f_type + 0x8d & 0x3f)) & 1 ^ 1;
                goto LAB_00105458;
              }
              if (local_a8.f_type != 0x187) {
                if (local_a8.f_type < 0x188) {
                  uVar5 = unaff_R12D & 0xffffff00 |
                          (uint)((local_a8.f_type - 0x2fU & 0xfffffffffffffffb) != 0);
                }
                else {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x7c0);
                }
                goto LAB_00105458;
              }
            }
            else if (local_a8.f_type != 0x2478) {
              if (0x2478 < local_a8.f_type) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x3434);
                goto LAB_00105458;
              }
              if (local_a8.f_type != 0x1cd1) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x2468);
                goto LAB_00105458;
              }
            }
          }
          else if (local_a8.f_type != 0x4d5a) {
            if (local_a8.f_type < 0x4d5b) {
              if (local_a8.f_type != 0x4858) {
                if (0x4858 < local_a8.f_type) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x4d44);
                  goto LAB_00105458;
                }
                if (local_a8.f_type != 0x4244) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x482b);
                  goto LAB_00105458;
                }
              }
            }
            else if (local_a8.f_type != 0x72b6) {
              if (0x72b6 < local_a8.f_type) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x9660);
                goto LAB_00105458;
              }
              if (local_a8.f_type != 0x5df5) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x7275);
                goto LAB_00105458;
              }
            }
          }
        }
      }
      else if (local_a8.f_type < 0x12ff7b8) {
        if ((local_a8.f_type < 0x12ff7b4) && (local_a8.f_type != 0x27e0eb)) {
          if (local_a8.f_type < 0x27e0ec) {
            if (local_a8.f_type != 0xef53) {
              if (local_a8.f_type < 0xef54) {
                if ((local_a8.f_type != 0xadff) && (local_a8.f_type != 0xef51)) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0xadf5);
                  goto LAB_00105458;
                }
              }
              else if (local_a8.f_type != 0xf15f) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x11954);
                goto LAB_00105458;
              }
            }
          }
          else if (local_a8.f_type != 0x1021994) {
            if (local_a8.f_type < 0x1021995) {
              if (local_a8.f_type != 0x414a53) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0xc0ffee);
                goto LAB_00105458;
              }
            }
            else if (local_a8.f_type != 0x1021997) {
              uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x12fd16d);
              goto LAB_00105458;
            }
          }
        }
      }
      else if (local_a8.f_type != 0x15013346) {
        if (local_a8.f_type < 0x15013347) {
          if (local_a8.f_type != 0xbad1dea) {
            if (local_a8.f_type < 0xbad1deb) {
              if (local_a8.f_type != 0x7655821) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x9041934);
                goto LAB_00105458;
              }
            }
            else if (local_a8.f_type != 0x11307854) {
              uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x13661366);
              goto LAB_00105458;
            }
          }
        }
        else if (local_a8.f_type != 0x2011bab0) {
          if (local_a8.f_type < 0x2011bab1) {
            if (local_a8.f_type != 0x19800202) {
              uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x1badface);
              goto LAB_00105458;
            }
          }
          else if (local_a8.f_type != 0x24051905) {
            uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x28cd3d45);
            goto LAB_00105458;
          }
        }
      }
    }
    else if (local_a8.f_type != 0x62656570) {
      if (local_a8.f_type < 0x62656571) {
        if (local_a8.f_type != 0x53464846) {
          if (local_a8.f_type < 0x53464847) {
            if (local_a8.f_type != 0x453dcd28) {
              if (local_a8.f_type < 0x453dcd29) {
                if (local_a8.f_type != 0x42494e4d) {
                  if (local_a8.f_type < 0x42494e4e) {
                    if ((local_a8.f_type != 0x3153464a) && (local_a8.f_type != 0x42465331)) {
                      uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x2fc12fc1);
                      goto LAB_00105458;
                    }
                  }
                  else if (local_a8.f_type != 0x43415d53) {
                    uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x444d4142);
                    goto LAB_00105458;
                  }
                }
              }
              else if (local_a8.f_type != 0x52654973) {
                if (local_a8.f_type < 0x52654974) {
                  if (local_a8.f_type != 0x454d444d) {
                    uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x45584653);
                    goto LAB_00105458;
                  }
                }
                else if (local_a8.f_type != 0x5345434d) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x5346314d);
                  goto LAB_00105458;
                }
              }
            }
          }
          else if (local_a8.f_type != 0x58465342) {
            if (local_a8.f_type < 0x58465343) {
              if (local_a8.f_type != 0x54190100) {
                if (local_a8.f_type < 0x54190101) {
                  if (local_a8.f_type != 0x5346544e) {
                    uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x534f434b);
                    goto LAB_00105458;
                  }
                }
                else if (local_a8.f_type != 0x565a4653) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x58295829);
                  goto LAB_00105458;
                }
              }
            }
            else if (local_a8.f_type != 0x5dca2df5) {
              if (local_a8.f_type < 0x5dca2df6) {
                if (local_a8.f_type != 0x5a3c69f0) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x5a4f4653);
                  goto LAB_00105458;
                }
              }
              else if (local_a8.f_type != 0x6165676c) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x62646576);
                goto LAB_00105458;
              }
            }
          }
        }
      }
      else if (local_a8.f_type != 0x858458f6) {
        if (local_a8.f_type < 0x858458f7) {
          if (local_a8.f_type != 0x6c6f6f70) {
            if (local_a8.f_type < 0x6c6f6f71) {
              if (local_a8.f_type != 0x64646178) {
                if (local_a8.f_type < 0x64646179) {
                  if ((local_a8.f_type != 0x63677270) && (local_a8.f_type != 0x64626720)) {
                    uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x62656572);
                    goto LAB_00105458;
                  }
                }
                else if (local_a8.f_type != 0x67596969) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x68191122);
                  goto LAB_00105458;
                }
              }
            }
            else if (local_a8.f_type != 0x73717368) {
              if (local_a8.f_type < 0x73717369) {
                if (local_a8.f_type != 0x6e736673) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x73636673);
                  goto LAB_00105458;
                }
              }
              else if (local_a8.f_type != 0x73727279) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x74726163);
                goto LAB_00105458;
              }
            }
          }
        }
        else if (local_a8.f_type != 0xcafe4a11) {
          if (local_a8.f_type < 0xcafe4a12) {
            if (local_a8.f_type != 0xabba1974) {
              if (local_a8.f_type < 0xabba1975) {
                if (local_a8.f_type != 0x9123683e) {
                  uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0x958458f6);
                  goto LAB_00105458;
                }
              }
              else if (local_a8.f_type != 0xc7571590) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0xc97e8168);
                goto LAB_00105458;
              }
            }
          }
          else if (local_a8.f_type != 0xf2f52010) {
            if (local_a8.f_type < 0xf2f52011) {
              if (local_a8.f_type != 0xde5e81e4) {
                uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0xe0f5e1e2);
                goto LAB_00105458;
              }
            }
            else if (local_a8.f_type != 0xf97cff8c) {
              uVar5 = unaff_R12D & 0xffffff00 | (uint)(local_a8.f_type != 0xf995e849);
              goto LAB_00105458;
            }
          }
        }
      }
    }
  }
  uVar5 = 0;
LAB_00105458:
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar5;
}