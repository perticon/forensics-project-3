byte fremote(word32 r12d, struct Eq_542 * fs, union Eq_897 & rdxOut)
{
	word64 r12;
	Eq_24 rsi;
	Eq_4571 qwLocA8;
	word24 r12d_24_8_384 = SLICE(r12, word24, 8);
	ui32 r12d_288 = (word32) r12;
	word64 rax_20 = fs->qw0028;
	fn00000000000028F0();
	if (false)
	{
		Eq_24 rax_334 = fn0000000000002530();
		r12d_288 = 0x01;
		if (*rax_334 != 0x26)
		{
			quotearg_style(rsi, 0x04, fs);
			fn00000000000028A0(fn0000000000002600(0x05, "cannot determine location of %s. reverting to polling", null), *rax_334, 0x00);
		}
		goto l0000000000005458;
	}
	byte cl_280 = (byte) qwLocA8;
	if (qwLocA8 == 732765674)
	{
l0000000000005B68:
		r12d_288 = 0x00;
		goto l0000000000005458;
	}
	else
	{
		if (qwLocA8 <= 732765674)
		{
			if (qwLocA8 <= 40866)
			{
				if (qwLocA8 > 0x9F9F)
					goto l0000000000005B68;
				if (qwLocA8 <= 0x4006)
				{
					if (qwLocA8 > 0x3FFF)
					{
						r12d_288 = (word32) (0x51 >> cl_280) & 0x01 ^ 0x01;
						goto l0000000000005458;
					}
					if (qwLocA8 <= 5007)
					{
						if (qwLocA8 > 0x1372)
						{
							r12d_288 = (word32) (0x10001401 >> (byte) qwLocA8 - 115) & 0x01 ^ 0x01;
							goto l0000000000005458;
						}
						if (qwLocA8 != 0x0187)
						{
							if (qwLocA8 > 0x0187)
								r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x07C0));
							goto l0000000000005458;
						}
						goto l0000000000005B68;
					}
					if (qwLocA8 == 9336)
						goto l0000000000005B68;
					if (qwLocA8 > 9336)
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x3434));
					else
					{
						if (qwLocA8 == 7377)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x2468));
					}
				}
				else
				{
					if (qwLocA8 == 0x4D5A)
						goto l0000000000005B68;
					if (qwLocA8 > 0x4D5A)
					{
						if (qwLocA8 == 29366)
							goto l0000000000005B68;
						if (qwLocA8 > 29366)
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x9660));
						else
						{
							if (qwLocA8 == 0x5DF5)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x7275));
						}
					}
					else
					{
						if (qwLocA8 == 0x4858)
							goto l0000000000005B68;
						if (qwLocA8 > 0x4858)
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x4D44));
						else
						{
							if (qwLocA8 == 0x4244)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x482B));
						}
					}
				}
			}
			else if (qwLocA8 <= 0x012FF7B7)
			{
				if (qwLocA8 > 19920819 || qwLocA8 == 0x0027E0EB)
					goto l0000000000005B68;
				if (qwLocA8 > 0x0027E0EB)
				{
					if (qwLocA8 == 0x01021994)
						goto l0000000000005B68;
					if (qwLocA8 > 0x01021994)
					{
						if (qwLocA8 == 0x01021997)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 19911021));
					}
					else
					{
						if (qwLocA8 == 4278867)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x00C0FFEE));
					}
				}
				else
				{
					if (qwLocA8 == 61267)
						goto l0000000000005B68;
					if (qwLocA8 > 61267)
					{
						if (qwLocA8 == 0xF15F)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 72020));
					}
					else
					{
						if (qwLocA8 == 44543 || qwLocA8 == 61265)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 44533));
					}
				}
			}
			else
			{
				if (qwLocA8 == 0x15013346)
					goto l0000000000005B68;
				if (qwLocA8 > 0x15013346)
				{
					if (qwLocA8 == 0x2011BAB0)
						goto l0000000000005B68;
					if (qwLocA8 > 0x2011BAB0)
					{
						if (qwLocA8 == 0x24051905)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x28CD3D45));
					}
					else
					{
						if (qwLocA8 == 0x19800202)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 464386766));
					}
				}
				else
				{
					if (qwLocA8 == 0x0BAD1DEA)
						goto l0000000000005B68;
					if (qwLocA8 > 0x0BAD1DEA)
					{
						if (qwLocA8 == 288389204)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x13661366));
					}
					else
					{
						if (qwLocA8 == 124082209)
							goto l0000000000005B68;
						r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x09041934));
					}
				}
			}
		}
		else
		{
			if (qwLocA8 == 0x62656570)
				goto l0000000000005B68;
			if (qwLocA8 > 0x62656570)
			{
				if (qwLocA8 == 2240043254)
					goto l0000000000005B68;
				if (qwLocA8 > 2240043254)
				{
					if (qwLocA8 == 0xCAFE4A11)
						goto l0000000000005B68;
					if (qwLocA8 > 0xCAFE4A11)
					{
						if (qwLocA8 == 0xF2F52010)
							goto l0000000000005B68;
						if (qwLocA8 > 0xF2F52010)
						{
							if (qwLocA8 == 0xF97CFF8C)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 4187351113));
						}
						else
						{
							if (qwLocA8 == 3730735588)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0xE0F5E1E2));
						}
					}
					else
					{
						if (qwLocA8 == 2881100148)
							goto l0000000000005B68;
						if (qwLocA8 > 2881100148)
						{
							if (qwLocA8 == 3344373136)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 3380511080));
						}
						else
						{
							if (qwLocA8 == 2435016766)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x958458F6));
						}
					}
				}
				else
				{
					if (qwLocA8 == 0x6C6F6F70)
						goto l0000000000005B68;
					if (qwLocA8 > 0x6C6F6F70)
					{
						if (qwLocA8 == 0x73717368)
							goto l0000000000005B68;
						if (qwLocA8 > 0x73717368)
						{
							if (qwLocA8 == 0x73727279)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 1953653091));
						}
						else
						{
							if (qwLocA8 == 0x6E736673)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x73636673));
						}
					}
					else
					{
						if (qwLocA8 == 0x64646178)
							goto l0000000000005B68;
						if (qwLocA8 > 0x64646178)
						{
							if (qwLocA8 == 0x67596969)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x68191122));
						}
						else
						{
							if (qwLocA8 == 0x63677270 || qwLocA8 == 0x64626720)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x62656572));
						}
					}
				}
			}
			else
			{
				if (qwLocA8 == 0x53464846)
					goto l0000000000005B68;
				if (qwLocA8 > 0x53464846)
				{
					if (qwLocA8 == 1481003842)
						goto l0000000000005B68;
					if (qwLocA8 > 1481003842)
					{
						if (qwLocA8 == 1573531125)
							goto l0000000000005B68;
						if (qwLocA8 > 1573531125)
						{
							if (qwLocA8 == 0x6165676C)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x62646576));
						}
						else
						{
							if (qwLocA8 == 0x5A3C69F0)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 1515144787));
						}
					}
					else
					{
						if (qwLocA8 == 0x54190100)
							goto l0000000000005B68;
						if (qwLocA8 > 0x54190100)
						{
							if (qwLocA8 == 0x565A4653)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x58295829));
						}
						else
						{
							if (qwLocA8 == 0x5346544E)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x534F434B));
						}
					}
				}
				else
				{
					if (qwLocA8 == 1161678120)
						goto l0000000000005B68;
					if (qwLocA8 > 1161678120)
					{
						if (qwLocA8 == 1382369651)
							goto l0000000000005B68;
						if (qwLocA8 > 1382369651)
						{
							if (qwLocA8 == 0x5345434D)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 1397109069));
						}
						else
						{
							if (qwLocA8 == 1162691661)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x45584653));
						}
					}
					else
					{
						if (qwLocA8 == 1112100429)
							goto l0000000000005B68;
						if (qwLocA8 > 1112100429)
						{
							if (qwLocA8 == 0x43415D53)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x444D4142));
						}
						else
						{
							if (qwLocA8 == 0x3153464A || qwLocA8 == 1111905073)
								goto l0000000000005B68;
							r12d_288 = SEQ(r12d_24_8_384, (int8) (qwLocA8 != 0x2FC12FC1));
						}
					}
				}
			}
		}
l0000000000005458:
		if (rax_20 - fs->qw0028 != 0x00)
			fn0000000000002630();
		else
		{
			rdxOut.u1 = <invalid>;
			return (byte) r12d_288;
		}
	}
}