#include <stdint.h>

/* /tmp/tmp1cbccfqz @ 0x52f0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp1cbccfqz @ 0x7d80 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x7da0 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* _Bool raw_comparator( const * a, const * b); */
    __asm ("in eax, 0");
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmp1cbccfqz @ 0x7db0 */
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    /* _Bool check_tuning(Hash_table * table); */
    __asm ("in eax, 0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("in eax, 0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) < 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    ah += cl;
    *(eax) += al;
    if (*(eax) >= 0) {
        __asm ("addss xmm1, dword [0x0000e5a4]");
        xmm2 = *((rax + 4));
        __asm ("comiss xmm2, xmm1");
        if (*(eax) <= 0) {
            goto label_0;
        }
        xmm3 = *(0x0000e5b0);
        __asm ("comiss xmm3, xmm2");
        if (*(eax) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*(eax) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x7e40 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29a0)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmp1cbccfqz @ 0x29a0 */
 
void hash_find_entry_cold (void) {
    /* [16] -r-x section size 42242 named .text */
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x7f50 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x0000e5b4]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x0000e5b8]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x0000e5b8]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmp1cbccfqz @ 0x8090 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x29a5)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x29a5)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    rax = malloc (0x10);
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x29a5 */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29aa */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x2520 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp1cbccfqz @ 0x29af */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29b5 */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29ba */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29bf */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x9130 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000e627;
        rdx = 0x0000e618;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000e61f;
        rdx = 0x0000e621;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000e623;
    rdx = 0x0000e61c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xcb90 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = 0x0000eea0;
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = 0x0000eea0;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x2830 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp1cbccfqz @ 0x9210 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x29c4)() ();
    }
    rdx = 0x0000e680;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xe680 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000e62b;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000e621;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000e6ac;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xe6ac */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000e61f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000e621;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000e61f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000e621;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000e7ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xe7ac */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000e8ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xe8ac */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000e621;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000e61f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000e61f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000e621;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp1cbccfqz @ 0x29c4 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0xa630 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x29c9)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x29c9 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29ce */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29d4 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29d9 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29de */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29e3 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29e8 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29ed */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29f2 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29f7 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x29fc */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x53e0 */
 
int64_t dbg_wd_hasher ( const * entry, size_t tabsize) {
    rdi = entry;
    rsi = tabsize;
    /* size_t wd_hasher( const * entry,size_t tabsize); */
    rax = *((rdi + 0x44));
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x5400 */
 
int32_t dbg_wd_comparator ( const * e1,  const * e2) {
    rdi = e1;
    rsi = e2;
    /* _Bool wd_comparator( const * e1, const * e2); */
    eax = *((rsi + 0x44));
    al = (*((rdi + 0x44)) == eax) ? 1 : 0;
    return eax;
}

/* /tmp/tmp1cbccfqz @ 0x5410 */
 
int64_t dbg_fremote (int64_t arg2) {
    statfs buf;
    int64_t var_78h;
    rsi = arg2;
    /* _Bool fremote(int fd,char const * name); */
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rsi = rsp;
    eax = fstatfs ();
    if (eax == 0) {
        goto label_1;
    }
    rax = errno_location ();
    r12d = 1;
    rbx = rax;
    if (*(rax) != 0x26) {
        goto label_2;
    }
    do {
label_0:
        rax = *((rsp + 0x78));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        eax = r12d;
        return rax;
label_1:
        rcx = *(rsp);
        if (rcx == 0x2bad1dea) {
            goto label_4;
        }
        if (rcx > 0x2bad1dea) {
            goto label_5;
        }
        if (rcx > 0x9fa2) {
            goto label_6;
        }
        if (rcx > 0x9f9f) {
            goto label_4;
        }
        if (rcx > 0x4006) {
            goto label_7;
        }
        if (rcx > 0x3fff) {
            goto label_8;
        }
        if (rcx > 0x138f) {
            goto label_9;
        }
        if (rcx > 0x1372) {
            goto label_10;
        }
        if (rcx == 0x187) {
            goto label_4;
        }
        if (rcx <= 0x187) {
            goto label_11;
        }
        r12b = (rcx != 0x7c0) ? 1 : 0;
    } while (1);
label_5:
    if (rcx != 0x62656570) {
        if (rcx > 0x62656570) {
            eax = 0x858458f6;
            if (rcx == rax) {
                goto label_4;
            }
            if (rcx <= rax) {
                goto label_12;
            }
            eax = 0xcafe4a11;
            if (rcx == rax) {
                goto label_4;
            }
            if (rcx <= rax) {
                goto label_13;
            }
            eax = 0xf2f52010;
            if (rcx == rax) {
                goto label_4;
            }
            if (rcx <= rax) {
                goto label_14;
            }
            eax = 0xf97cff8c;
            if (rcx == rax) {
                goto label_4;
            }
            rax += 0x18e8bd;
            r12b = (rcx != rax) ? 1 : 0;
            goto label_0;
        }
        if (rcx == 0x53464846) {
            goto label_4;
        }
        if (rcx > 0x53464846) {
            if (rcx == 0x58465342) {
                goto label_4;
            }
            if (rcx <= 0x58465342) {
                goto label_15;
            }
            if (rcx == 0x5dca2df5) {
                goto label_4;
            }
            if (rcx <= 0x5dca2df5) {
                goto label_16;
            }
            if (rcx == 0x6165676c) {
                goto label_4;
            }
            r12b = (rcx != 0x62646576) ? 1 : 0;
            goto label_0;
label_6:
            if (rcx <= 0x12ff7b7) {
                if (rcx > 0x12ff7b3) {
                    goto label_4;
                }
                if (rcx == 0x27e0eb) {
                    goto label_4;
                }
                if (rcx <= 0x27e0eb) {
                    goto label_17;
                }
                if (rcx == 0x1021994) {
                    goto label_4;
                }
                if (rcx <= 0x1021994) {
                    goto label_18;
                }
                if (rcx == 0x1021997) {
                    goto label_4;
                }
                r12b = (rcx != 0x12fd16d) ? 1 : 0;
                goto label_0;
            }
            if (rcx == 0x15013346) {
                goto label_4;
            }
            if (rcx <= 0x15013346) {
                goto label_19;
            }
            if (rcx == 0x2011bab0) {
                goto label_4;
            }
            if (rcx <= 0x2011bab0) {
                goto label_20;
            }
            if (rcx == 0x24051905) {
                goto label_4;
            }
            r12b = (rcx != 0x28cd3d45) ? 1 : 0;
            goto label_0;
        }
        if (rcx == 0x453dcd28) {
            goto label_4;
        }
        if (rcx > 0x453dcd28) {
            if (rcx == 0x52654973) {
                goto label_4;
            }
            if (rcx <= 0x52654973) {
                goto label_21;
            }
            if (rcx == 0x5345434d) {
                goto label_4;
            }
            r12b = (rcx != 0x5346314d) ? 1 : 0;
            goto label_0;
label_12:
            if (rcx == 0x6c6f6f70) {
                goto label_4;
            }
            if (rcx > 0x6c6f6f70) {
                if (rcx == 0x73717368) {
                    goto label_4;
                }
                if (rcx <= 0x73717368) {
                    goto label_22;
                }
                if (rcx == 0x73727279) {
                    goto label_4;
                }
                r12b = (rcx != 0x74726163) ? 1 : 0;
                goto label_0;
label_7:
                if (rcx == 0x4d5a) {
                    goto label_4;
                }
                if (rcx <= 0x4d5a) {
                    goto label_23;
                }
                if (rcx == 0x72b6) {
                    goto label_4;
                }
                if (rcx > 0x72b6) {
                    r12b = (rcx != 0x9660) ? 1 : 0;
                    goto label_0;
label_9:
                    if (rcx == 0x2478) {
                        goto label_4;
                    }
                    if (rcx > 0x2478) {
                        r12b = (rcx != 0x3434) ? 1 : 0;
                        goto label_0;
                    }
                    if (rcx == 0x1cd1) {
                        goto label_4;
                    }
                    r12b = (rcx != 0x2468) ? 1 : 0;
                    goto label_0;
                }
                if (rcx == 0x5df5) {
                    goto label_4;
                }
                r12b = (rcx != 0x7275) ? 1 : 0;
                goto label_0;
label_11:
                rcx -= 0x2f;
                r12b = ((rcx & 0xfffffffffffffffb) != 0) ? 1 : 0;
                goto label_0;
            }
            if (rcx == 0x64646178) {
                goto label_4;
            }
            if (rcx <= 0x64646178) {
                goto label_24;
            }
            if (rcx == 0x67596969) {
                goto label_4;
            }
            r12b = (rcx != 0x68191122) ? 1 : 0;
            goto label_0;
label_17:
            if (rcx == 0xef53) {
                goto label_4;
            }
            if (rcx <= 0xef53) {
                goto label_25;
            }
            if (rcx == 0xf15f) {
                goto label_4;
            }
            r12b = (rcx != 0x11954) ? 1 : 0;
            goto label_0;
        }
        if (rcx == 0x42494e4d) {
            goto label_4;
        }
        if (rcx > 0x42494e4d) {
            if (rcx == 0x43415d53) {
                goto label_4;
            }
            r12b = (rcx != 0x444d4142) ? 1 : 0;
            goto label_0;
label_13:
            rax -= 0x1f44309d;
            if (rcx == rax) {
                goto label_4;
            }
            if (rcx <= rax) {
                goto label_26;
            }
            eax = 0xc7571590;
            if (rcx == rax) {
                goto label_4;
            }
            rax += 0x2276bd8;
            r12b = (rcx != rax) ? 1 : 0;
            goto label_0;
label_15:
            if (rcx == 0x54190100) {
                goto label_4;
            }
            if (rcx <= 0x54190100) {
                goto label_27;
            }
            if (rcx == 0x565a4653) {
                goto label_4;
            }
            r12b = (rcx != 0x58295829) ? 1 : 0;
            goto label_0;
label_19:
            if (rcx == 0xbad1dea) {
                goto label_4;
            }
            if (rcx <= 0xbad1dea) {
                goto label_28;
            }
            if (rcx == 0x11307854) {
                goto label_4;
            }
            r12b = (rcx != 0x13661366) ? 1 : 0;
            goto label_0;
label_23:
            if (rcx == 0x4858) {
                goto label_4;
            }
            if (rcx > 0x4858) {
                r12b = (rcx != 0x4d44) ? 1 : 0;
                goto label_0;
            }
            if (rcx == 0x4244) {
                goto label_4;
            }
            r12b = (rcx != 0x482b) ? 1 : 0;
            goto label_0;
        }
        if (rcx == 0x3153464a) {
            goto label_4;
        }
        if (rcx == 0x42465331) {
            goto label_4;
        }
        r12b = (rcx != 0x2fc12fc1) ? 1 : 0;
        goto label_0;
label_26:
        rax -= 0x1a96b136;
        if (rcx == rax) {
            goto label_4;
        }
        rax += 0x460f0b8;
        r12b = (rcx != rax) ? 1 : 0;
        goto label_0;
label_16:
        if (rcx == 0x5a3c69f0) {
            goto label_4;
        }
        r12b = (rcx != 0x5a4f4653) ? 1 : 0;
        goto label_0;
label_20:
        if (rcx == 0x19800202) {
            goto label_4;
        }
        r12b = (rcx != 0x1badface) ? 1 : 0;
        goto label_0;
label_14:
        rax -= 0x14969e2c;
        if (rcx == rax) {
            goto label_4;
        }
        rax += 0x2975ffe;
        r12b = (rcx != rax) ? 1 : 0;
        goto label_0;
label_28:
        if (rcx == 0x7655821) {
            goto label_4;
        }
        r12b = (rcx != 0x9041934) ? 1 : 0;
        goto label_0;
label_24:
        if (rcx == 0x63677270) {
            goto label_4;
        }
        if (rcx == 0x64626720) {
            goto label_4;
        }
        r12b = (rcx != 0x62656572) ? 1 : 0;
        goto label_0;
label_18:
        if (rcx == 0x414a53) {
            goto label_4;
        }
        r12b = (rcx != 0xc0ffee) ? 1 : 0;
        goto label_0;
label_27:
        if (rcx == 0x5346544e) {
            goto label_4;
        }
        r12b = (rcx != 0x534f434b) ? 1 : 0;
        goto label_0;
label_22:
        if (rcx == 0x6e736673) {
            goto label_4;
        }
        r12b = (rcx != 0x73636673) ? 1 : 0;
        goto label_0;
label_25:
        if (rcx == 0xadff) {
            goto label_4;
        }
        if (rcx == 0xef51) {
            goto label_4;
        }
        r12b = (rcx != 0xadf5) ? 1 : 0;
        goto label_0;
label_21:
        if (rcx == 0x454d444d) {
            goto label_4;
        }
        r12b = (rcx != 0x45584653) ? 1 : 0;
        goto label_0;
label_10:
        rcx -= 0x1373;
        r12d = 0x10001401;
        r12 >>= cl;
        r12d &= 1;
        r12d ^= 1;
        goto label_0;
label_8:
        r12d = 0x51;
        r12 >>= cl;
        r12d &= 1;
        r12d ^= 1;
        goto label_0;
label_2:
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r13 = rax;
        rax = dcgettext (0, "cannot determine location of %s. reverting to polling");
        rcx = r13;
        eax = 0;
        error (0, *(rbx), rax);
        goto label_0;
    }
label_4:
    r12d = 0;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x5b80 */
 
uint64_t close_fd (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    eax = rdi + 1;
    if (eax <= 1) {
        return eax;
    }
    ebx = edi;
    eax = close (rdi);
    if (eax == 0) {
        return eax;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "closing %s (fd=%d)");
    r12 = rax;
    rax = errno_location ();
    r8d = ebx;
    rcx = r13;
    rdx = r12;
    esi = *(rax);
    edi = 0;
    eax = 0;
    return error ();
}

/* /tmp/tmp1cbccfqz @ 0x26f0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmp1cbccfqz @ 0xacf0 */
 
int64_t quotearg_style (uint32_t arg1, void * arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29de)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x2630 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp1cbccfqz @ 0x2600 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp1cbccfqz @ 0x2530 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp1cbccfqz @ 0x5c10 */
 
uint64_t xwrite_stdout_part_0 (int64_t arg_8h, int64_t arg_10h, int64_t arg2) {
    int64_t var_2018h;
    rsi = arg2;
    rcx = stdout;
    rdx = rsi;
    rbx = rsi;
    esi = 1;
    rax = fwrite_unlocked ();
    if (rbx <= rax) {
        return rax;
    }
    rdi = stdout;
    clearerr_unlocked ();
    rsi = "standard output";
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error writing %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmp1cbccfqz @ 0x2850 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmp1cbccfqz @ 0x25b0 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmp1cbccfqz @ 0x28a0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp1cbccfqz @ 0xb410 */
 
uint64_t dbg_safe_read (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_read(int fd,void * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = read (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (rbx > 0x7ff00000) {
        if (eax != 0x16) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x2700 */
 
void memchr (void) {
    __asm ("bnd jmp qword [reloc.memchr]");
}

/* /tmp/tmp1cbccfqz @ 0x5c90 */
 
int64_t dbg_start_lines (void * arg_8h, int64_t arg_10h, void ** arg1, int64_t arg2, void ** arg3, char ** arg4) {
    char[8192] buffer;
    int64_t var_2018h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int start_lines(char const * pretty_filename,int fd,uintmax_t n_lines,uintmax_t * read_pos); */
    *((rsp + 8)) = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x2018)) = rax;
    eax = 0;
    if (rdx == 0) {
        goto label_1;
    }
    r15d = esi;
    rbx = rdx;
    r14 = rsp + 0x10;
    r13 = rcx;
    do {
        rax = safe_read (r15d, r14, sym._init);
        if (rax == 0) {
            goto label_2;
        }
        if (rax == -1) {
            goto label_3;
        }
        *(r13) += rax;
        r12d = *(obj.line_end);
        rbp = r14 + rax;
label_0:
        rdx -= rdi;
        rax = memchr (r14, r12d, rbp);
    } while (rax == 0);
    rdi = rax + 1;
    rbx--;
    if (rbx != 0) {
        goto label_0;
    }
    eax = 0;
    while (rbp == 0) {
label_1:
        rdx = *((rsp + 0x2018));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_4;
        }
        return rax;
        rbp -= rdi;
    }
    rsi = rbp;
    *((rsp + 8)) = eax;
    xwrite_stdout_part_0 ();
    eax = *((rsp + 8));
    goto label_1;
label_2:
    eax = 0xffffffff;
    goto label_1;
label_3:
    rsi = *((rsp + 8));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error reading %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    eax = 1;
    goto label_1;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x5de0 */
 
int64_t dbg_start_bytes (int32_t arg1, int64_t arg2, int32_t arg3, int64_t arg4) {
    char[8192] buffer;
    int64_t var_2008h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int start_bytes(char const * pretty_filename,int fd,uintmax_t n_bytes,uintmax_t * read_pos); */
    rax = *(fs:0x28);
    *((rsp + 0x2008)) = rax;
    eax = 0;
    if (rdx == 0) {
        goto label_0;
    }
    r13 = rdi;
    r14 = rdx;
    rbx = rcx;
    r12 = rsp;
    while (rax != 0) {
        if (rax == -1) {
            goto label_1;
        }
        *(rbx) += rax;
        if (rax > r14) {
            goto label_2;
        }
        r14 -= rax;
        if (r14 == 0) {
            goto label_0;
        }
        rax = safe_read (ebp, r12, sym._init);
    }
    eax = 0xffffffff;
    goto label_3;
label_2:
    rax -= r14;
    rsi = rax;
    if (rax != 0) {
        goto label_4;
    }
label_0:
    eax = 0;
    do {
label_3:
        rdx = *((rsp + 0x2008));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
label_1:
        rsi = r13;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r13 = rax;
        rax = dcgettext (0, "error reading %s");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        error (0, *(rax), r12);
        eax = 1;
    } while (1);
label_4:
    rdi = r12 + r14;
    xwrite_stdout_part_0 ();
    goto label_0;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x5ef0 */
 
int64_t xlseek_part_0 (void ** arg1, int64_t arg2, int64_t arg3) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    ebx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = offtostr (rdi, rsp, rdx);
    r13 = rax;
    rax = errno_location ();
    rdx = r12;
    esi = 3;
    edi = 0;
    if (ebx == 1) {
        goto label_0;
    }
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    do {
        rax = dcgettext (0, "%s: cannot seek to offset %s");
        r8 = r13;
        rcx = r12;
        eax = 0;
        error (0, *(rbp), rax);
        exit (1);
label_0:
        rax = quotearg_n_style_colon ();
        edx = 5;
        rsi = "%s: cannot seek to relative offset %s";
        r12 = rax;
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0x8f50 */
 
int64_t dbg_offtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * offtostr(off_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    r8 = rsi + 0x14;
    rcx = rdi;
    rsi = 0xcccccccccccccccd;
    if (rdi < 0) {
        goto label_0;
    }
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rsi;
        rdx >>= 3;
        rax = rdx * 5;
        rax += rax;
        rcx -= rax;
        ecx += 0x30;
        *(r8) = cl;
        rcx = rdx;
    } while (rdx != 0);
    rax = r8;
    return rax;
label_0:
    r9 = 0x6666666666666667;
    edi = 0x30;
    do {
        rax = rcx;
        rsi = r8;
        r8--;
        rdx:rax = rax * r9;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rax = rdx * 5;
        eax = rdi + rax*2;
        eax -= ecx;
        rcx = rdx;
        *(r8) = al;
    } while (rdx != 0);
    *((r8 - 1)) = 0x2d;
    r8 = rsi - 2;
    rax = r8;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xb070 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x29e8)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x2910 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp1cbccfqz @ 0x5f90 */
 
int64_t pretty_name_isra_0 (uint32_t arg1) {
    rdi = arg1;
    rax = rdi;
    if (*(rdi) == 0x2d) {
        if (*((rdi + 1)) != 0) {
            goto label_0;
        }
        edx = 5;
        rsi = "standard input";
        edi = 0;
        void (*0x2600)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x5fc0 */
 
int64_t dbg_dump_remainder (int64_t arg1, void ** arg2, int64_t arg3, int32_t arg4) {
    char[8192] buffer;
    int64_t var_2018h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* uintmax_t dump_remainder(_Bool want_header,char const * pretty_filename,int fd,uintmax_t n_bytes); */
    r12d = 0;
    *((rsp + 8)) = rsi;
    r15d = edx;
    rbx = rcx;
    *(rsp) = rcx;
    r13 = rsp + 0x10;
    rax = *(fs:0x28);
    *((rsp + 0x2018)) = rax;
    eax = 0;
    do {
        edx = _init;
        rsi = r13;
        edi = r15d;
        if (rbx <= rdx) {
            rdx = rbx;
        }
        rax = safe_read (rdi, rsi, rdx);
        r14 = rax;
        if (rax == -1) {
            goto label_1;
        }
        if (rax == 0) {
            goto label_2;
        }
        if (bpl != 0) {
            goto label_3;
        }
label_0:
        rsi = r14;
        rdi = r13;
        r12 += r14;
        xwrite_stdout_part_0 ();
        rax = *(rsp);
        if (rax != -1) {
            rbx -= r14;
            if (rbx == 0) {
                goto label_2;
            }
            if (rax == 0xfffffffffffffffe) {
                goto label_2;
            }
        }
        ebp = 0;
    } while (1);
label_1:
    rax = errno_location ();
    rbx = rax;
    if (*(rax) == 0xb) {
label_2:
        rax = *((rsp + 0x2018));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
label_3:
        rcx = *((rsp + 8));
        rax = 0x0000ea41;
        rdx = 0x0000ea40;
        if (*(obj.first_file.2) != 0) {
            rdx = rax;
        }
        rsi = "%s==> %s <==\n";
        edi = 1;
        eax = 0;
        printf_chk ();
        *(obj.first_file.2) = 0;
        goto label_0;
    }
    rsi = *((rsp + 8));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "error reading %s");
    rcx = r12;
    eax = 0;
    error (1, *(rbx), rax);
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x6130 */
 
int64_t dbg_file_lines (int64_t arg_7h, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_98h, int64_t arg_a8h, void ** arg1, int64_t arg2, void ** arg3, int64_t arg4, int64_t arg5, char ** arg6) {
    char[8192] buffer;
    int64_t var_1h;
    int64_t var_2028h;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool file_lines(char const * pretty_filename,int fd,uintmax_t n_lines,off_t start_pos,off_t end_pos,uintmax_t * read_pos); */
    *((rsp + 0x10)) = rdi;
    *((rsp + 8)) = rcx;
    *((rsp + 0x18)) = r8;
    *(rsp) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x2028)) = rax;
    eax = 0;
    if (rdx == 0) {
        goto label_2;
    }
    rbx = r8;
    rdi = r8;
    r12d = esi;
    r14 = rdx;
    rbx -= rcx;
    rax = rbx;
    rax >>= 0x3f;
    rax >>= 0x33;
    rbx += rax;
    ebx &= 0x1fff;
    rbx -= rax;
    eax = _init;
    if (rbx == 0) {
        rbx = rax;
    }
    edx = 0;
    rdi -= rbx;
    r13 = rdi;
    rsi = rdi;
    edi = r12d;
    rax = lseek ();
    if (rax < 0) {
        goto label_3;
    }
    r15 = rsp + 0x20;
    rax = safe_read (r12d, r15, rbx);
    if (rax == -1) {
        goto label_4;
    }
    rcx = *(rsp);
    rax = r13 + rax;
    ebx = *(obj.line_end);
    *(rcx) = rax;
    if (rbp != 0) {
        goto label_5;
    }
label_0:
    rdx = rbp;
    while (rdx != 0) {
        esi = ebx;
        rdi = r15;
        rax = memrchr ();
        if (rax == 0) {
            goto label_6;
        }
        rdx = rax;
        rsi = r14 - 1;
        rdx -= r15;
        if (r14 == 0) {
            goto label_7;
        }
        r14 = rsi;
    }
label_6:
    if (r13 == *((rsp + 8))) {
        goto label_8;
    }
    r13 -= sym._init;
    edx = 0;
    edi = r12d;
    rsi = r13;
    rax = lseek ();
    if (rax < 0) {
        goto label_9;
    }
    rax = safe_read (r12d, r15, sym._init);
    if (rax == -1) {
        goto label_4;
    }
    rcx = *(rsp);
    rax = r13 + rax;
    *(rcx) = rax;
    if (rbp == 0) {
        goto label_2;
    }
    ebx = *(obj.line_end);
    goto label_0;
label_5:
    if (*((rsp + rbp + 0x1f)) == bl) {
        goto label_0;
    }
    r14--;
    goto label_0;
label_7:
    rsi = rbp - 1;
    rsi -= rdx;
    if (rsi != 0) {
        rdi = rax + 1;
        xwrite_stdout_part_0 ();
    }
    rcx -= rbp;
    rcx -= r13;
    rax = dump_remainder (0, *((rsp + 0x10)), r12d, *((rsp + 0x18)));
    rcx = *(rsp);
    *(rcx) += rax;
    eax = 1;
    do {
label_1:
        rdx = *((rsp + 0x2028));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_10;
        }
        return rax;
label_2:
        eax = 1;
    } while (1);
label_4:
    rsi = *((rsp + 0x10));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error reading %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    eax = error (0, *(rax), r12);
    eax = 0;
    goto label_1;
label_8:
    edx = 0;
    rsi = r13;
    edi = r12d;
    rax = lseek ();
    if (rax >= 0) {
        rax = dump_remainder (0, *((rsp + 0x10)), r12d, *((rsp + 0x18)));
        rcx = *(rsp);
        rax += r13;
        *(rcx) = rax;
        eax = 1;
        goto label_1;
label_10:
        stack_chk_fail ();
    }
label_9:
    rdx = *((rsp + 0x10));
    esi = 0;
    rdi = r13;
    xlseek_part_0 ();
label_3:
    rdx = *((rsp + 0x10));
    esi = 0;
    rdi = rbp;
    return xlseek_part_0 ();
}

/* /tmp/tmp1cbccfqz @ 0x63e0 */
 
int64_t dbg_check_fspec (int64_t arg_7h, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_28h, signed int64_t arg_30h, uint32_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_98h, int64_t arg_a8h, uint32_t arg1, int64_t arg2) {
    stat stats;
    int64_t var_a8h;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    /* void check_fspec(File_spec * fspec,File_spec ** prev_fspec); */
    rbx = rdi;
    edi = *((rdi + 0x38));
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    if (edi == 0xffffffff) {
        goto label_1;
    }
    r13 = *(rbx);
    while (*((r13 + 1)) != 0) {
        eax = fstat (rdi, rsp);
        r12d = eax;
        if (eax != 0) {
            goto label_3;
        }
label_0:
        eax = *((rbx + 0x30));
        eax &= 0xf000;
        if (eax == 0x8000) {
            goto label_4;
        }
label_2:
        if (*(obj.print_headers) != 0) {
            r12d = 0;
            r12b = (*(rbp) != rbx) ? 1 : 0;
        }
        rax = dump_remainder (r12d, r13, *((rbx + 0x38)), 0xffffffffffffffff);
        *((rbx + 8)) += rax;
        if (rax != 0) {
            goto label_5;
        }
label_1:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        return rax;
    }
    edx = 5;
    rax = dcgettext (0, "standard input");
    r13 = rax;
    eax = fstat (*((rbx + 0x38)), rsp);
    r12d = eax;
    if (eax == 0) {
        goto label_0;
    }
label_3:
    rax = errno_location ();
    edi = *((rbx + 0x38));
    rsi = r13;
    eax = *(rax);
    *((rbx + 0x3c)) = eax;
    close_fd ();
    *((rbx + 0x38)) = 0xffffffff;
    goto label_1;
label_4:
    rax = *((rbx + 8));
    if (*((rsp + 0x30)) < rax) {
        goto label_7;
    }
    if (*((rsp + 0x30)) != rax) {
        goto label_2;
    }
    rax = *((rbx + 0x10));
    edx = 0;
    al = (*((rsp + 0x58)) > rax) ? 1 : 0;
    dl = (*((rsp + 0x58)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsp + 0x60));
    al = (*((rbx + 0x18)) > rax) ? 1 : 0;
    cl = (*((rbx + 0x18)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax != 0) {
        goto label_2;
    }
    goto label_1;
label_5:
    *(rbp) = rbx;
    rdi = stdout;
    eax = fflush_unlocked ();
    if (eax == 0) {
        goto label_1;
    }
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    rax = error (1, *(rax), r12);
label_7:
    edi = 0;
    rdx = r13;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "%s: file truncated");
    rcx = r14;
    eax = 0;
    error (0, 0, rax);
    edi = *((rbx + 0x38));
    edx = 0;
    esi = 0;
    rax = lseek ();
    if (rax >= 0) {
        *((rbx + 8)) = 0;
        goto label_2;
label_6:
        stack_chk_fail ();
    }
    rdx = r13;
    esi = 0;
    edi = 0;
    return xlseek_part_0 ();
}

/* /tmp/tmp1cbccfqz @ 0x6610 */
 
int64_t dbg_recheck (uint32_t arg_7h, int64_t arg_8h, void * buf, int64_t arg_18h, int64_t arg_28h, int64_t arg_68h, int64_t arg_70h, int64_t arg_a8h, int64_t arg1, int64_t arg2, int64_t arg3) {
    stat new_stats;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void recheck(File_spec * f,_Bool blocking); */
    r15d = esi;
    rbx = rdi;
    rdi = *(rdi);
    rax = *(fs:0x28);
    *((rsp + 0xa8)) = rax;
    eax = 0;
    ebp = *(rdi);
    ebp -= 0x2d;
    if (ebp == 0) {
        ebp = *((rdi + 1));
    }
    eax = *((rbx + 0x36));
    r13d = *((rbx + 0x3c));
    *((rsp + 7)) = al;
    if (ebp == 0) {
        goto label_15;
    }
    esi = r15d;
    eax = 0;
    esi = (int32_t) sil;
    esi <<= 0xb;
    eax = open_safer (rdi, 1, rdx, rcx);
    r12d = eax;
    eax = *((rbx + 0x3c));
    do {
        dl = (*((rbx + 0x38)) == 0xffffffff) ? 1 : 0;
        al = (eax == 0) ? 1 : 0;
        if (dl == al) {
            goto label_16;
        }
        r14d = *(obj.reopen_inaccessible_files);
        eax = *(obj.disable_inotify);
        if (r14b != 0) {
            goto label_17;
        }
        *((rbx + 0x36)) = 1;
        if (al == 0) {
            goto label_18;
        }
label_3:
        if (r12d != 0xffffffff) {
            goto label_19;
        }
label_0:
        rax = errno_location ();
        r14d = *((rbx + 0x36));
        rsi = *(rbx);
        edx = *(rax);
        r15 = rsi;
        *((rbx + 0x3c)) = edx;
        if (r14b == 0) {
            goto label_20;
        }
        eax = *(rsi);
        eax -= 0x2d;
        if (eax == 0) {
            goto label_21;
        }
        if (edx != r13d) {
            goto label_22;
        }
label_2:
        if (*(rsi) == 0x2d) {
            if (*((rsi + 1)) != 0) {
                goto label_23;
            }
            edx = 5;
            rax = dcgettext (0, "standard input");
            rsi = rax;
        }
label_23:
        edi = r12d;
        close_fd ();
        rsi = *(rbx);
        if (*(rsi) == 0x2d) {
            if (*((rsi + 1)) != 0) {
                goto label_24;
            }
            edx = 5;
            rax = dcgettext (0, "standard input");
            rsi = rax;
        }
label_24:
        edi = *((rbx + 0x38));
        close_fd ();
        *((rbx + 0x38)) = 0xffffffff;
label_1:
        rax = *((rsp + 0xa8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_25;
        }
        return rax;
label_15:
        eax = r13d;
        r12d = 0;
    } while (1);
label_17:
    if (r12d == 0xffffffff) {
        goto label_26;
    }
    *((rbx + 0x36)) = 1;
    rsi = rsp + 0x10;
    if (al == 0) {
        goto label_27;
    }
label_4:
    eax = fstat (r12d, rsi);
    if (eax < 0) {
        goto label_0;
    }
    ecx = *((rsp + 0x28));
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    eax -= 0x1000;
    if ((eax & 0xffffe000) != 0) {
        ecx &= 0xb000;
        if (ecx != 0x8000) {
            goto label_28;
        }
    }
    rsi = *(rbx);
    if (*(rsi) == 0x2d) {
        if (*((rsi + 1)) != 0) {
            goto label_29;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_29:
    al = fremote (r12d);
    *((rbx + 0x35)) = al;
    if (al != 0) {
        if (*(obj.disable_inotify) == 0) {
            goto label_30;
        }
    }
    r13d &= 0xfffffffd;
    *((rbx + 0x3c)) = 0;
    eax = *((rbx + 0x38));
    if (r13d == 0) {
        goto label_31;
    }
    if (eax != 0xffffffff) {
        goto label_32;
    }
    rsi = *(rbx);
    if (*(rsi) == 0x2d) {
        goto label_33;
    }
label_9:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_12:
    rax = dcgettext (0, "%s has become accessible");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
label_5:
    eax = 0xffffffff;
    r15d = (int32_t) r15b;
    r13 = *(rbx);
    if (ebp == 0) {
        r15d = eax;
    }
    rax = *((rsp + 0x68));
    *((rbx + 0x38)) = r12d;
    *((rbx + 8)) = 0;
    *((rbx + 0x10)) = rax;
    rax = *((rsp + 0x70));
    *((rbx + 0x40)) = r15d;
    *((rbx + 0x18)) = rax;
    rax = *((rsp + 0x10));
    *((rbx + 0x58)) = 0;
    *((rbx + 0x20)) = rax;
    rax = *((rsp + 0x18));
    *((rbx + 0x34)) = 0;
    *((rbx + 0x28)) = rax;
    eax = *((rsp + 0x28));
    *((rbx + 0x30)) = eax;
    if (*(r13) == 0x2d) {
        if (*((r13 + 1)) != 0) {
            goto label_34;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        r13 = rax;
    }
label_34:
    edx = 0;
    esi = 0;
    edi = r12d;
    rax = lseek ();
    if (rax >= 0) {
        goto label_1;
    }
    rdx = r13;
    esi = 0;
    edi = 0;
    al = xlseek_part_0 ();
label_26:
    *((rbx + 0x36)) = 0;
    r15 = *(rbx);
    if (al == 0) {
        goto label_35;
    }
    rax = errno_location ();
    eax = *(rax);
    *((rbx + 0x3c)) = eax;
label_20:
    eax = *(r15);
    eax -= 0x2d;
    if (eax == 0) {
        goto label_36;
    }
    if (*((rsp + 7)) != 0) {
        goto label_37;
    }
    do {
label_6:
        rsi = *(rbx);
        goto label_2;
label_36:
        eax = *((r15 + 1));
    } while (*((rsp + 7)) == 0);
label_37:
    if (eax == 0) {
        goto label_38;
    }
label_11:
    rsi = r15;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s has become inaccessible");
    rcx = r13;
    eax = 0;
    error (0, *((rbx + 0x3c)), rax);
    rsi = *(rbx);
    goto label_2;
label_21:
    eax = *((rsi + 1));
    if (edx == r13d) {
        goto label_2;
    }
label_22:
    if (eax == 0) {
        goto label_39;
    }
label_7:
    rdx = r15;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    error (0, *(rbp), 0x0000e514);
    rsi = *(rbx);
    goto label_2;
label_18:
    eax = lstat (*(rbx), rsp + 0x10);
    if (eax != 0) {
        goto label_3;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax != segment.PHDR) {
        goto label_3;
    }
    goto label_40;
label_19:
    rsi = rsp + 0x10;
    goto label_4;
label_27:
    *((rsp + 8)) = rsi;
    eax = lstat (*(rbx), rsi);
    rsi = *((rsp + 8));
    if (eax != 0) {
        goto label_4;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax != segment.PHDR) {
        goto label_4;
    }
    goto label_40;
label_31:
    rsi = *(rbx);
    edx = *(rsi);
    edx -= 0x2d;
    if (edx == 0) {
        goto label_41;
    }
label_8:
    if (eax == 0xffffffff) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    if (*((rbx + 0x28)) == rax) {
        rax = *((rsp + 0x10));
        if (*((rbx + 0x20)) != rax) {
            goto label_43;
        }
        if (edx == 0) {
            goto label_44;
        }
label_13:
        edi = r12d;
        close_fd ();
        goto label_1;
    }
label_43:
    if (edx == 0) {
        goto label_45;
    }
label_10:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s has been replaced;  following new file");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    rsi = *(rbx);
    if (*(rsi) == 0x2d) {
        if (*((rsi + 1)) != 0) {
            goto label_46;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_46:
    edi = *((rbx + 0x38));
    close_fd ();
    goto label_5;
label_28:
    *((rbx + 0x3c)) = 0xffffffff;
    *((rbx + 0x36)) = 0;
    if (r14b != 0) {
        if (*(obj.follow_mode) != 1) {
            goto label_47;
        }
        *((rbx + 0x34)) = 0;
        if (*((rsp + 7)) == 0) {
            goto label_48;
        }
        rbp = 0x0000ea41;
        goto label_14;
    }
label_47:
    *((rbx + 0x34)) = 1;
    if (*((rsp + 7)) != 0) {
        goto label_49;
    }
    if (r13d == 0xffffffff) {
        goto label_6;
    }
label_49:
    edx = 5;
    rax = dcgettext (0, "; giving up on this name");
label_14:
    rsi = *(rbx);
    if (*(rsi) == 0x2d) {
        if (*((rsi + 1)) != 0) {
            goto label_50;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_50:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s has been replaced with an untailable file%s");
    r8 = rbp;
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    goto label_6;
label_35:
    eax = lstat (r15, rsp + 0x10);
    if (eax != 0) {
        goto label_0;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax != segment.PHDR) {
        goto label_0;
    }
label_40:
    rsi = *(rbx);
    *((rbx + 0x3c)) = 0xffffffff;
    *((rbx + 0x34)) = 1;
    if (*(rsi) == 0x2d) {
        if (*((rsi + 1)) != 0) {
            goto label_51;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_51:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s has been replaced with an untailable symbolic link");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    rsi = *(rbx);
    goto label_2;
label_39:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r15 = rax;
    goto label_7;
label_41:
    edx = *((rsi + 1));
    goto label_8;
label_33:
    if (*((rsi + 1)) != 0) {
        goto label_9;
    }
    edx = 5;
    rax = dcgettext (0, "standard input");
    rsi = rax;
    goto label_9;
label_45:
    edx = 5;
    rax = dcgettext (0, "standard input");
    rsi = rax;
    goto label_10;
label_38:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r15 = rax;
    goto label_11;
label_30:
    rsi = *(rbx);
    *((rbx + 0x3c)) = 0xffffffff;
    if (*(rsi) == 0x2d) {
        if (*((rsi + 1)) != 0) {
            goto label_52;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_52:
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s has been replaced with an untailable remote file");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    eax = 0x101;
    *((rbx + 0x34)) = ax;
    goto label_6;
label_42:
    while (1) {
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        rsi = "%s has appeared;  following new file";
        r13 = rax;
        goto label_12;
        edx = 5;
        rax = dcgettext (0, "standard input");
        rsi = rax;
    }
label_44:
    edx = 5;
    rax = dcgettext (0, "standard input");
    rsi = rax;
    goto label_13;
label_16:
    assert_fail ("valid_file_spec (f)", "src/tail.c", 0x3e2, "recheck");
label_25:
    stack_chk_fail ();
label_48:
    r13d++;
    rbp = 0x0000ea41;
    if (r13d != 0) {
        goto label_14;
    }
    goto label_6;
label_32:
    return assert_fail ("f->fd == -1", "src/tail.c", 0x42f, "recheck");
}

/* /tmp/tmp1cbccfqz @ 0x5320 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x5350 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x5390 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000024e0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp1cbccfqz @ 0x24e0 */
 
void fcn_000024e0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp1cbccfqz @ 0x53d0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp1cbccfqz @ 0x7300 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x6e40)() ();
}

/* /tmp/tmp1cbccfqz @ 0x2930 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp1cbccfqz @ 0x2880 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp1cbccfqz @ 0x2740 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp1cbccfqz @ 0x2770 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp1cbccfqz @ 0x2870 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp1cbccfqz @ 0x2540 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp1cbccfqz @ 0xce90 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp1cbccfqz @ 0xab80 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0x77d0 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0xaeb0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xba10 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xa9c0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xbc10 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x2800 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp1cbccfqz @ 0xc150 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000e514);
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x2860 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp1cbccfqz @ 0x2580 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp1cbccfqz @ 0xa8e0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xcbd0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xabd0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x29d4)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xa940 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x8300 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x0000e5c0]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x2930)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x0000e5c0]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmp1cbccfqz @ 0x9080 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp1cbccfqz @ 0x26a0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp1cbccfqz @ 0x2920 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp1cbccfqz @ 0x7480 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x28a0)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0xb3d0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0xb110 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x29ed)() ();
    }
    if (rdx == 0) {
        void (*0x29ed)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8d50 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xb1b0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29f2)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x29f2)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xcc60 */
 
int64_t dbg_rpl_nanosleep (int64_t arg1, int64_t arg2) {
    timespec intermediate;
    int64_t var_8h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* int rpl_nanosleep(timespec const * requested_delay,timespec * remaining_delay); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    rax = *((rdi + 8));
    if (rax > 0x3b9ac9ff) {
        goto label_0;
    }
    rbx = *(rdi);
    *((rsp + 8)) = rax;
    r12 = rsp;
    if (rbx > 0x1fa400) {
        goto label_1;
    }
    goto label_2;
    do {
        *((rsp + 8)) = 0;
        if (rbx <= 0x1fa400) {
            goto label_2;
        }
label_1:
        *(rsp) = 0x1fa400;
        rbx -= 0x1fa400;
        eax = nanosleep (r12, rbp);
    } while (eax == 0);
    if (rbp == 0) {
        goto label_3;
    }
    *(rbp) += rbx;
    goto label_3;
label_2:
    *(rsp) = rbx;
    nanosleep (r12, rbp);
    do {
label_3:
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_4;
        }
        return rax;
label_0:
        errno_location ();
        *(rax) = 0x16;
        eax = 0xffffffff;
    } while (1);
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x79d0 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmp1cbccfqz @ 0xcea4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp1cbccfqz @ 0x88b0 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rbx = *((rbx + 8));
        free (rbx);
    } while (rbx != 0);
label_6:
    free (*(r12));
    rdi = r12;
    return free ();
}

/* /tmp/tmp1cbccfqz @ 0xbc90 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xbd90 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xbc30 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xcac0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2810)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp1cbccfqz @ 0x8690 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xbbd0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xabb0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0xc110 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27b0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x2620 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp1cbccfqz @ 0xbbf0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x7310 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp1cbccfqz @ 0xb950 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xb4e0)() ();
}

/* /tmp/tmp1cbccfqz @ 0x78e0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000e514);
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0x7770 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xcdd0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp1cbccfqz @ 0xbb90 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x8290 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xcd50 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x7640 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xb260 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29f7)() ();
    }
    if (rax == 0) {
        void (*0x29f7)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8b30 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x29bf)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x0000e5b4]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        rax = malloc (0x10);
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x0000e5b8]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x29bf)() ();
label_17:
    __asm ("subss xmm4, dword [0x0000e5b8]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8210 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xafe0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8ff0 */
 
int64_t dbg_posix2_version (void) {
    char * e;
    int64_t var_8h;
    /* int posix2_version(); */
    r12d = 0x31069;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = getenv (0x0000e5c8);
    if (rax == 0) {
        goto label_0;
    }
    while (*(rdx) != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        rax = strtol (rax, rsp, 0xa);
        rdx = *(rsp);
    }
    if (rax >= 0xffffffff80000000) {
        r12d = 0x7fffffff;
        if (rax <= r12) {
            r12 = rax;
        }
        goto label_0;
    }
    r12d = 0x80000000;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xbfe0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x2760 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp1cbccfqz @ 0xa880 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x8700 */
 
uint64_t dbg_hash_initialize (int32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        r12d = 0;
        free (r12);
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0x2510 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp1cbccfqz @ 0xb3a0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0xa820 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xc050 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27b0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xaac0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x8460 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, char ** arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29aa)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xc2a0 */
 
void dbg_xdectoumax (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* uintmax_t xdectoumax(char const * n_str,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoumax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmp1cbccfqz @ 0xc190 */
 
int64_t dbg_xnumtoumax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    uintmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* uintmax_t xnumtoumax(char const * n_str,int base,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xc450 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000eda8;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xeda8 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmp1cbccfqz @ 0xb3f0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0x8800 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x86d0 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x85c0 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x78d0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp1cbccfqz @ 0xbc60 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xbe20 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x7420 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x89a0 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x29ba)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x29ba)() ();
    }
    free (*(rsp));
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    free (*(rbp));
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xca30 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x25e0)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp1cbccfqz @ 0xbd40 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xb3b0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0xb300 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x29fc)() ();
    }
    if (rax == 0) {
        void (*0x29fc)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8230 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x7510 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp1cbccfqz @ 0xc090 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27b0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x7990 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xcd30 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x7a90)() ();
}

/* /tmp/tmp1cbccfqz @ 0x8db0 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x0000e5b8]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rbp = *((rbp + 8));
        rax = free (rbp);
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x0000e5b8]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x7800 */
 
int64_t dbg_cl_strtod (int64_t arg1, int64_t arg2) {
    char * end;
    char * c_end;
    int64_t var_8h;
    char * * endptr;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* double cl_strtod(char const * nptr,char ** restrict endptr); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    strtod (rdi, rsp + 0x18);
    rax = *((rsp + 0x18));
    __asm ("movapd xmm1, xmm0");
    while (1) {
label_0:
        if (rbx != 0) {
            rax = *((rsp + 0x18));
            *(rbx) = rax;
        }
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        __asm ("movapd xmm0, xmm1");
        return rax;
        *((rsp + 8)) = xmm0;
        rax = errno_location ();
        r13d = *(rax);
        r12 = rax;
        c_strtod (rbp, rsp + 0x20);
        rax = *((rsp + 0x20));
        xmm1 = *((rsp + 8));
        if (*((rsp + 0x18)) >= rax) {
            goto label_2;
        }
        *((rsp + 0x18)) = rax;
        __asm ("movapd xmm1, xmm0");
    }
label_2:
    *(r12) = r13d;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xaab0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0xa9c0)() ();
}

/* /tmp/tmp1cbccfqz @ 0x8200 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xab90 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0x8530 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x29b5)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xc380 */
 
int64_t dbg_xstrtod (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    char * terminator;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool xstrtod(char const * str,char const ** ptr,double * result,double (*)() convert); */
    r14 = rcx;
    r13 = rdx;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    errno_location ();
    rsi = rsp;
    rdi = rbx;
    *(rax) = 0;
    r12 = rax;
    void (*r14)() ();
    rax = *(rsp);
    if (rax == rbx) {
        goto label_3;
    }
    if (rbp == 0) {
        goto label_4;
    }
    __asm ("ucomisd xmm0, qword [0x0000ed68]");
    if (rbp != 0) {
        r8d = 1;
        if (rbp == 0) {
            goto label_5;
        }
    }
label_0:
    r8b = (*(r12) != 0x22) ? 1 : 0;
label_2:
    if (rbp == 0) {
        goto label_1;
    }
label_5:
    *(rbp) = rax;
    do {
label_1:
        *(r13) = xmm0;
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r8d;
        return rax;
label_4:
        r8d = 0;
    } while (*(rax) != 0);
    __asm ("ucomisd xmm0, qword [0x0000ed68]");
    if (*(rax) == 0) {
        goto label_0;
    }
    r8d = 1;
    if (*(rax) == 0) {
        goto label_1;
    }
    goto label_0;
label_3:
    r8d = 0;
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x2a10 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    uintmax_t n_units;
    File_spec * prev_fspec;
    uintmax_t read_pos;
    stat out_stat;
    stat stats;
    int32_t errname;
    char ** size;
    char ** s1;
    char ** var_20h;
    void ** var_28h;
    void ** var_30h;
    void * var_38h;
    int64_t var_40h;
    int32_t var_48h;
    uint32_t status;
    char * s2;
    int64_t var_58h;
    void * var_60h;
    uint32_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7ch;
    uint32_t var_80h;
    int64_t var_88h;
    char ** var_90h;
    int64_t var_96h;
    int64_t var_d4h;
    void * var_f0h;
    int64_t var_108h;
    void * buf;
    int64_t var_184h;
    int64_t var_188h;
    uint32_t var_18eh;
    int64_t var_198h;
    signed int64_t var_1b0h;
    int64_t var_1b8h;
    uint32_t var_1d8h;
    uint32_t var_1e0h;
    int64_t var_218h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = 0x0000ea41;
    r12 = 0x0000e0b8;
    ebx = edi;
    rax = *(fs:0x28);
    *((rsp + 0x218)) = rax;
    eax = 0;
    *((rsp + 0x80)) = 0xa;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, r15);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    *(obj.have_read_stdin) = 0;
    *(obj.count_lines) = 1;
    *(obj.print_headers) = 0;
    *(obj.from_start) = 0;
    *(obj.forever) = 0;
    *(obj.line_end) = 0xa;
    if (ebx != 2) {
        if (ebx == 3) {
            goto label_87;
        }
        if (ebx != 4) {
            goto label_2;
        }
label_6:
        eax = strcmp (*((rbp + 0x10)), 0x0000e0fb);
        if (eax != 0) {
            goto label_2;
        }
    }
label_5:
    eax = posix2_version ();
    rdx = *((rbp + 8));
    ecx = *(rdx);
    rdi = rdx + 1;
    if (cl == 0x2b) {
        goto label_88;
    }
    if (cl == 0x2d) {
        goto label_89;
    }
label_2:
    eax = 0;
label_3:
    xmm3 = *(0x0000e498);
    *((rsp + 0x10)) = 0;
    edx = (int32_t) al;
    r12 = rbp + rax*8;
    ebx -= edx;
    r14 = obj_long_options;
    r13 = "c:n:fFqs:vz0123456789";
    *((rsp + 0x40)) = xmm3;
    rbp = 0x0000e330;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = r12;
        edi = ebx;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_90;
        }
        if (eax > 0x85) {
            goto label_91;
        }
        if (eax <= 0x2f) {
            goto label_92;
        }
        edx = rax - 0x30;
        if (edx > 0x55) {
            goto label_91;
        }
        rdx = *((rbp + rdx*4));
        rdx += rbp;
        /* switch table (86 cases) at 0xe330 */
        void (*rdx)() ();
        rsi = optarg;
        *(obj.forever) = 1;
        if (rsi == 0) {
            goto label_93;
        }
        _xargmatch_internal ("--follow", rsi, obj.follow_mode_string, obj.follow_mode_map, 4, *(obj.argmatch_die));
        rcx = obj_follow_mode_map;
        eax = *((rcx + rax*4));
        *(obj.follow_mode) = eax;
    } while (1);
    rdx = optarg;
    obj.count_lines = (eax == 0x6e) ? 1 : 0;
    ecx = *(rdx);
    if (cl == 0x2b) {
        goto label_94;
    }
    if (cl == 0x2d) {
        goto label_95;
    }
label_7:
    edx = 5;
    if (eax == 0x6e) {
        goto label_96;
    }
    rax = dcgettext (0, "invalid number of bytes");
label_1:
    rax = xdectoumax (*(obj.optarg), 0, 0xffffffffffffffff, "bkKmMGTPEZY0", rax, 0);
    *((rsp + 0x80)) = rax;
    goto label_0;
    *(obj.forever) = 1;
    *(obj.follow_mode) = 1;
    *(obj.reopen_inaccessible_files) = 1;
    goto label_0;
    *(obj.disable_inotify) = 1;
    goto label_0;
    *(obj.presume_input_pipe) = 1;
    goto label_0;
    edx = 5;
    rax = dcgettext (0, "invalid PID");
    eax = xdectoumax (*(obj.optarg), 0, 0x7fffffff, r15, rax, 0);
    *(obj.pid) = eax;
    goto label_0;
    edx = 5;
    rax = dcgettext (0, "invalid maximum number of unchanged stats between opens");
    rax = xdectoumax (*(obj.optarg), 0, 0xffffffffffffffff, r15, rax, 0);
    *(obj.max_n_unchanged_stats_between_opens) = rax;
    goto label_0;
    *(obj.reopen_inaccessible_files) = 1;
    goto label_0;
    *(obj.line_end) = 0;
    goto label_0;
    al = xstrtod (*(obj.optarg), 0, rsp + 0x90, dbg.cl_strtod);
    if (al == 0) {
        goto label_97;
    }
    xmm2 = *((rsp + 0x90));
    xmm4 = 0;
    __asm ("comisd xmm2, xmm4");
    *((rsp + 0x40)) = xmm2;
    if (al >= 0) {
        goto label_0;
    }
label_97:
    rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid number of seconds: %s");
    rcx = r12;
    eax = 0;
    rax = error (1, 0, rax);
label_92:
    if (eax == 0xffffff7d) {
        rax = "Jim Meyering";
        rax = "Ian Lance Taylor";
        eax = 0;
        version_etc (*(obj.stdout), "tail", "GNU coreutils", *(obj.Version), "Paul Rubin", "David MacKenzie");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_91;
    }
    usage (0);
label_96:
    rax = dcgettext (0, "invalid number of lines");
    r8 = rax;
    goto label_1;
label_93:
    *(obj.follow_mode) = 2;
    goto label_0;
label_89:
    if (eax <= 0x30daf) {
        goto label_98;
    }
    eax = 0;
    al = (*((rdx + 1)) == 0x63) ? 1 : 0;
    if (*((rdx + rax + 1)) == 0) {
        goto label_2;
    }
label_98:
    r12d = 0;
label_4:
    eax = *((rdx + 1));
    rdx = rdi;
    ecx = eax;
    eax -= 0x30;
    if (eax > 9) {
        goto label_99;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        ecx = eax;
        eax -= 0x30;
    } while (eax <= 9);
label_99:
    if (cl == 0x63) {
        goto label_100;
    }
    if (cl == 0x6c) {
        goto label_101;
    }
    if (cl == 0x62) {
        goto label_102;
    }
    rax = rdx;
    r13d = 1;
    ecx = 0xa;
label_24:
    r14d = 0;
    if (*(rax) == 0x66) {
        rax++;
        r14d = 1;
    }
    if (*(rax) != 0) {
        goto label_2;
    }
    if (rdi == rdx) {
        goto label_103;
    }
    eax = xstrtoumax (rdi, 0, 0xa, rsp + 0x80, 0x0000e0fe);
    eax &= 0xfffffffd;
    if (eax != 0) {
        goto label_104;
    }
label_12:
    *(obj.from_start) = r12b;
    eax = 1;
    *(obj.count_lines) = r13b;
    *(obj.forever) = r14b;
    goto label_3;
label_88:
    eax -= 0x30db0;
    r12d = 1;
    if (eax > 0x2b8) {
        goto label_4;
    }
    goto label_2;
label_87:
    rdi = *((rbp + 0x10));
    if (*(rdi) != 0x2d) {
        goto label_5;
    }
    if (*((rdi + 1)) != 0) {
        goto label_6;
    }
    goto label_5;
label_94:
    *(obj.from_start) = 1;
    goto label_7;
label_90:
    if (*(obj.reopen_inaccessible_files) == 0) {
        goto label_105;
    }
    if (*(obj.forever) == 0) {
        goto label_106;
    }
    if (*(obj.follow_mode) == 2) {
        goto label_107;
    }
    edi = pid;
    if (edi != 0) {
label_11:
        eax = kill (rdi, 0);
        if (eax != 0) {
            goto label_108;
        }
    }
label_10:
    if (*(obj.from_start) != 0) {
        rax = *((rsp + 0x80));
        if (rax == 0) {
            goto label_109;
        }
        rax--;
        *((rsp + 0x80)) = rax;
    }
label_109:
    rax = *(obj.optind);
    if (eax >= ebx) {
        goto label_110;
    }
    ebx -= eax;
    rcx = (int64_t) ebx;
    rbx = r12 + rax*8;
    *((rsp + 8)) = rcx;
label_26:
    rax = *((rsp + 8));
    ecx = 0;
    esi = 1;
    rbp = rbx + rax*8;
    rax = rbx;
    do {
        rdx = *(rax);
        if (*(rdx) == 0x2d) {
            if (*((rdx + 1)) != 0) {
                ecx = esi;
                goto label_111;
            }
        }
label_111:
        rax += 8;
    } while (rbp != rax);
    if (cl != 0) {
        eax = follow_mode;
        if (eax == 1) {
            goto label_112;
        }
        if (*(obj.forever) == 0) {
            goto label_113;
        }
        if (*(obj.pid) != 0) {
            goto label_114;
        }
        if (*((rsp + 8)) != 1) {
            goto label_114;
        }
        if (eax != 2) {
            goto label_114;
        }
        rax = rsp + 0x180;
        eax = fstat (0, rax);
        if (eax != 0) {
            goto label_114;
        }
        eax = *((rsp + 0x198));
        eax &= 0xf000;
        if (eax == 0x8000) {
            goto label_114;
        }
    }
label_13:
    if (*((rsp + 0x80)) == 0) {
        if (*(obj.forever) == 0) {
            goto label_115;
        }
    }
label_31:
    rax = xnmalloc (*((rsp + 8)), 0x60);
    *((rsp + 0x58)) = rax;
    do {
        rdx = *(rbx);
        rbx += 8;
        rax += 0x60;
        *((rax - 0x60)) = rdx;
    } while (rbx != rbp);
    eax = *((rsp + 0x10));
    if (eax == 1) {
        goto label_116;
    }
    if (eax == 0) {
        if (*((rsp + 8)) != 1) {
            goto label_116;
        }
    }
label_14:
    rax = *((rsp + 8)) * 0x60;
    *((rsp + 0x4f)) = 1;
    rbx = rax;
    *((rsp + 0x68)) = rax;
    rax = *((rsp + 0x58));
    rcx = *((rsp + 0x58));
    *((rsp + 0x10)) = rax;
    rcx += rbx;
    *((rsp + 0x50)) = rcx;
    while (eax != 0) {
label_8:
        eax = 0;
        eax = open_safer (rbp, 0, rdx, rcx);
        r13d = *(obj.reopen_inaccessible_files);
        r15d = eax;
        rax = *((rsp + 0x10));
        if (r13b == 0) {
            goto label_117;
        }
        rbp = *(rax);
        r12 = *(rax);
        if (r15d != 0xffffffff) {
            goto label_118;
        }
        rax = *((rsp + 0x10));
        *((rax + 0x36)) = 0;
label_19:
        rax = errno_location ();
        rbx = rax;
        if (*(obj.forever) != 0) {
            rcx = *((rsp + 0x10));
            eax = *(rax);
            r13d ^= 1;
            *((rcx + 0x38)) = 0xffffffff;
            *((rcx + 0x3c)) = eax;
            *((rcx + 0x34)) = r13b;
            *((rcx + 0x28)) = 0;
            *((rcx + 0x20)) = 0;
        }
        eax = strcmp (rbp, 0x0000e0fc);
        if (eax == 0) {
            goto label_119;
        }
label_22:
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot open %s for reading");
        rcx = r12;
        eax = 0;
        error (0, *(rbx), rax);
        *((rsp + 0x20)) = 0;
label_15:
        ecx = *((rsp + 0x20));
        rax = *((rsp + 0x10));
        if (*((rsp + 0x50)) == rax) {
            goto label_120;
        }
        rax = *((rsp + 0x80));
        *((rsp + 0x30)) = rax;
        rax = *((rsp + 0x10));
        rbp = *(rax);
        eax = *(rbp);
        r12 = rbp;
        eax -= 0x2d;
        *((rsp + 0x48)) = eax;
    }
    eax = *((rbp + 1));
    *((rsp + 0x48)) = eax;
    if (eax != 0) {
        goto label_8;
    }
    *(obj.have_read_stdin) = 1;
    r15d = 0;
label_118:
    rax = *((rsp + 0x10));
    *((rax + 0x36)) = 1;
label_20:
    if (*(obj.print_headers) != 0) {
        goto label_121;
    }
label_18:
    *((rsp + 0x38)) = rbp;
    if (*(rbp) == 0x2d) {
        if (*((rbp + 1)) != 0) {
            goto label_122;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        *((rsp + 0x38)) = rax;
    }
label_122:
    eax = *(obj.count_lines);
    edi = r15d;
    *((rsp + 0x90)) = 0;
    *((rsp + 0x20)) = al;
    rax = rsp + 0x180;
    *((rsp + 0x60)) = rax;
    rsi = rax;
    if (al == 0) {
        goto label_123;
    }
    eax = fstat (rdi, rsi);
    if (eax != 0) {
        goto label_124;
    }
    eax = *(obj.from_start);
    *((rsp + 0x78)) = al;
    if (al != 0) {
        goto label_125;
    }
    if (*(obj.presume_input_pipe) == 0) {
        eax = *((rsp + 0x198));
        eax &= 0xf000;
        if (eax == 0x8000) {
            goto label_126;
        }
    }
label_32:
    rax = xmalloc (0x2018);
    *((rax + 0x2008)) = 0;
    r13 = rax;
    *((rax + sym._init)) = 0;
    *((rax + 0x2010)) = 0;
    *((rsp + 0x18)) = rax;
    rax = xmalloc (0x2018);
    *((rsp + 0x28)) = 0;
    r14 = rax;
    tmp_0 = r13;
    r13 = r14;
    r14 = tmp_0;
label_9:
    rax = safe_read (r15d, r13, sym._init);
    r12 = rax;
    rax = rax - 1;
    if (rax > 0xfffffffffffffffd) {
        goto label_127;
    }
    *((r13 + sym._init)) = r12;
    ebx = *(obj.line_end);
    rbp = r13 + r12;
    rdi = r13;
    *((r13 + 0x2008)) = 0;
    *((r13 + 0x2010)) = 0;
    while (rax != 0) {
        *((r13 + 0x2008))++;
        rdx -= rdi;
        rax = memchr (rax + 1, ebx, rbp);
    }
    rax = *((rsp + 0x18));
    rbx = *((rsp + 0x28));
    rbx += *((r13 + 0x2008));
    rdi = *((rax + sym._init));
    rax = r12 + rdi;
    if (rax <= 0x1fff) {
        goto label_128;
    }
    rax = *((rsp + 0x18));
    *((rax + 0x2010)) = r13;
    rax = rbx;
    rax -= *((r14 + 0x2008));
    *((rsp + 0x28)) = rax;
    if (*((rsp + 0x30)) >= rax) {
        goto label_129;
    }
    *((rsp + 0x18)) = r13;
    r13 = r14;
    r14 = *((r14 + 0x2010));
    goto label_9;
label_95:
    rdx++;
    *(obj.optarg) = rdx;
    goto label_7;
    *((rsp + 0x10)) = 2;
    goto label_0;
    *((rsp + 0x10)) = 1;
    goto label_0;
label_106:
    *(obj.reopen_inaccessible_files) = 0;
    edx = 5;
label_61:
    rax = dcgettext (0, "warning: --retry ignored; --retry is useful only when following");
    eax = 0;
    error (0, 0, rax);
label_105:
    edi = pid;
    if (edi == 0) {
        goto label_10;
    }
    if (*(obj.forever) != 0) {
        goto label_11;
    }
    edx = 5;
    rax = dcgettext (0, "warning: PID ignored; --pid=PID is useful only when following");
    eax = 0;
    error (0, 0, rax);
    goto label_10;
label_103:
    eax = ecx;
    *((rsp + 0x80)) = rax;
    goto label_12;
label_114:
    eax = isatty (0);
    if (eax == 0) {
        goto label_13;
    }
    edx = 5;
    rax = dcgettext (0, "warning: following standard input indefinitely is ineffective");
    eax = 0;
    error (0, 0, rax);
    goto label_13;
label_116:
    *(obj.print_headers) = 1;
    goto label_14;
label_128:
    rbp = *((rsp + 0x18));
    rdi += rbp;
    memcpy (rdi, r13, r12);
    rax = *((r13 + sym._init));
    *((rsp + 0x28)) = rbx;
    *((rbp + sym._init)) += rax;
    rax = *((r13 + 0x2008));
    *((rbp + 0x2008)) += rax;
    goto label_9;
label_123:
    eax = fstat (rdi, rsi);
    if (eax != 0) {
        goto label_130;
    }
    ebx = *(obj.from_start);
    eax = *(obj.presume_input_pipe);
    *((rsp + 0x20)) = bl;
    if (bl == 0) {
        goto label_131;
    }
    if (al != 0) {
        goto label_132;
    }
    if (*((rsp + 0x30)) < 0) {
        goto label_132;
    }
    eax = *((rsp + 0x198));
    rsi = *((rsp + 0x30));
    edx = 1;
    edi = r15d;
    eax &= 0xf000;
    if (eax == 0x8000) {
        goto label_133;
    }
    rax = lseek ();
    rax++;
    if (rax == 0) {
        goto label_132;
    }
label_59:
    rax = *((rsp + 0x30));
    *((rsp + 0x30)) = 0xffffffffffffffff;
    *((rsp + 0x90)) = rax;
label_16:
    rax = dump_remainder (0, *((rsp + 0x38)), r15d, *((rsp + 0x30)));
    *((rsp + 0x20)) = 1;
label_17:
    if (*(obj.forever) != 0) {
        goto label_134;
    }
    if (*((rsp + 0x48)) == 0) {
        goto label_15;
    }
    eax = close (r15d);
    if (eax == 0) {
        goto label_15;
    }
    rax = *((rsp + 0x10));
    rbp = *(rax);
    eax = strcmp (*(rax), 0x0000e0fc);
    if (eax == 0) {
        goto label_135;
    }
label_39:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "error reading %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    *((rsp + 0x20)) = 0;
    goto label_15;
label_132:
    eax = start_bytes (*((rsp + 0x38)), r15d, *((rsp + 0x30)), rsp + 0x90);
    *((rsp + 0x30)) = 0xffffffffffffffff;
    if (eax == 0) {
        goto label_16;
    }
    eax >>= 0x1f;
    *((rsp + 0x20)) = eax;
    goto label_17;
label_129:
    rax = xmalloc (0x2018);
    *((rsp + 0x18)) = r13;
    *((rsp + 0x28)) = rbx;
    r13 = rax;
    goto label_9;
label_134:
    eax = *((rsp + 0x20));
    rcx = *((rsp + 0x10));
    eax--;
    *((rcx + 0x3c)) = eax;
    eax = fstat (r15d, *((rsp + 0x60)));
    if (eax < 0) {
        goto label_136;
    }
    edx = *((rsp + 0x198));
    eax = *((rsp + 0x198));
    eax &= 0xf000;
    eax -= 0x1000;
    if ((eax & 0xffffe000) != 0) {
        eax = edx;
        eax &= 0xb000;
        if (eax != 0x8000) {
            goto label_137;
        }
    }
    if (*((rsp + 0x20)) == 0) {
        goto label_37;
    }
    rbx = *((rsp + 0x10));
    rcx = *((rsp + 0x90));
    *((rbx + 8)) = rcx;
    rcx = *((rsp + 0x1d8));
    eax -= eax;
    eax |= 1;
    rbp = *(rbx);
    *((rbx + 0x38)) = r15d;
    *((rbx + 0x10)) = rcx;
    rcx = *((rsp + 0x1e0));
    *((rbx + 0x30)) = edx;
    *((rbx + 0x18)) = rcx;
    rcx = *((rsp + 0x180));
    *((rbx + 0x40)) = eax;
    *((rbx + 0x20)) = rcx;
    rcx = *((rsp + 0x188));
    *((rbx + 0x58)) = 0;
    *((rbx + 0x28)) = rcx;
    *((rbx + 0x34)) = 0;
    eax = strcmp (rbp, 0x0000e0fc);
    if (eax == 0) {
        goto label_138;
    }
label_55:
    rsi = rbp;
    al = fremote (r15d);
    rcx = *((rsp + 0x10));
    *((rcx + 0x35)) = al;
    goto label_15;
label_121:
    eax = strcmp (rbp, 0x0000e0fc);
    if (eax == 0) {
        goto label_139;
    }
label_25:
    rax = 0x0000ea41;
    rcx = r12;
    edi = 1;
    rdx = 0x0000ea40;
    rsi = "%s==> %s <==\n";
    if (*(obj.first_file.2) != 0) {
        rdx = rax;
    }
    eax = 0;
    printf_chk ();
    rax = *((rsp + 0x10));
    *(obj.first_file.2) = 0;
    rbp = *(rax);
    goto label_18;
label_117:
    rbp = *(rax);
    *((rax + 0x36)) = 1;
    r12 = rbp;
    if (r15d == 0xffffffff) {
        goto label_19;
    }
    goto label_20;
label_127:
    tmp_1 = r14;
    r14 = r13;
    r13 = tmp_1;
    free (r14);
    r12++;
    if (r12 == 0) {
        goto label_140;
    }
    rbx = *((rsp + 0x18));
    rcx = *((rbx + sym._init));
    dl = (rcx == 0) ? 1 : 0;
    al = (*((rsp + 0x30)) == 0) ? 1 : 0;
    dl |= al;
    *((rsp + 0x78)) = dl;
    if (dl == 0) {
        goto label_141;
    }
label_57:
    if (r13 == 0) {
        goto label_23;
    }
    do {
label_28:
        r13 = *((r13 + 0x2010));
        free (r13);
    } while (r13 != 0);
label_23:
    eax = *((rsp + 0x78));
    *((rsp + 0x20)) = al;
    goto label_17;
label_136:
    rax = errno_location ();
    rcx = *((rsp + 0x10));
    rbx = rax;
    eax = *(rax);
    rbp = *(rcx);
    *((rcx + 0x3c)) = eax;
    eax = strcmp (rbp, 0x0000e0fc);
    if (eax == 0) {
        goto label_142;
    }
label_58:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "error reading %s");
    rcx = r12;
    eax = 0;
    error (0, *(rbx), rax);
label_37:
    eax = *(obj.reopen_inaccessible_files);
    rbx = *((rsp + 0x10));
    eax ^= 1;
    rbp = *(rbx);
    *((rbx + 0x34)) = al;
    eax = strcmp (rbp, 0x0000e0fc);
    if (eax == 0) {
        goto label_143;
    }
label_30:
    rsi = rbp;
    edi = r15d;
    close_fd ();
    rax = *((rsp + 0x10));
    *((rsp + 0x20)) = 0;
    *((rax + 0x38)) = 0xffffffff;
    goto label_15;
label_131:
    if (al == 0) {
        goto label_144;
    }
label_27:
    ebx = 0;
    rax = xmalloc (0x2010);
    *((rax + sym._init)) = 0;
    r13 = rax;
    *((rax + 0x2008)) = 0;
    r14 = r13;
    rax = xmalloc (0x2010);
    r12 = *((rsp + 0x30));
    while (rcx > 0x1fff) {
        rax = rbx;
        rax -= *((r13 + sym._init));
        *((r14 + 0x2008)) = rbp;
        if (r12 >= rax) {
            goto label_145;
        }
        rbx = rax;
        rax = r13;
        r13 = *((r13 + 0x2008));
label_21:
        r14 = rbp;
        rax = safe_read (r15d, rbp, sym._init);
        rdx = rax;
        rax = rax - 1;
        if (rax > 0xfffffffffffffffd) {
            goto label_146;
        }
        rbx += rdx;
        *((rbp + sym._init)) = rdx;
        rax = *((r14 + sym._init));
        *((rbp + 0x2008)) = 0;
        rcx = rdx + rax;
    }
    memcpy (r14 + rax, rbp, rdx);
    rax = *((rbp + sym._init));
    *((r14 + sym._init)) += rax;
    rax = rbp;
    goto label_21;
label_120:
    eax = *(obj.forever);
    *((rsp + 0x38)) = al;
    if (al != 0) {
        goto label_147;
    }
label_33:
    if (*(obj.have_read_stdin) != 0) {
        eax = close (0);
        if (eax < 0) {
            goto label_148;
        }
    }
    edi = *((rsp + 0x4f));
    edi = (int32_t) dil;
    exit (1);
label_119:
    edx = 5;
    rax = dcgettext (0, "standard input");
    goto label_22;
label_125:
    eax = start_lines (*((rsp + 0x38)), r15d, *((rsp + 0x30)), rsp + 0x90, r8, r9);
    if (eax == 0) {
        goto label_149;
    }
    eax >>= 0x1f;
    *((rsp + 0x78)) = eax;
    goto label_23;
label_102:
    r13d = 0;
    ecx = 0x1400;
    do {
label_29:
        rax = rdx + 1;
        goto label_24;
label_101:
        r13d = 1;
        ecx = 0xa;
    } while (1);
label_139:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    goto label_25;
label_110:
    *((rsp + 8)) = 1;
    rbx = 0x00013028;
    goto label_26;
label_124:
    rsi = *((rsp + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot fstat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    *((rsp + 0x78)) = 0;
    goto label_23;
label_130:
    rsi = *((rsp + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot fstat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    goto label_17;
label_144:
    if (*((rsp + 0x30)) < 0) {
        goto label_27;
    }
    eax = *((rsp + 0x198));
    eax &= obj._IO_stdin_used;
    if (eax != 0x8000) {
        goto label_150;
    }
    rbx = *((rsp + 0x1b0));
    rbp |= 0xffffffffffffffff;
label_40:
    rax = *((rsp + 0x1b8));
    rdx = 0x1fffffffffffffff;
    rcx = rax - 1;
    edx = 0x200;
    if (rcx > rdx) {
        rax = rdx;
    }
    if (rbx <= rax) {
        goto label_27;
    }
    if (rbp == -1) {
        goto label_151;
    }
label_56:
    if (rbx > rbp) {
        rax = rbx;
        rax -= rbp;
        if (*((rsp + 0x30)) < rax) {
            goto label_152;
        }
    }
label_60:
    *((rsp + 0x90)) = rbp;
    goto label_16;
label_141:
    ebp = *(obj.line_end);
    if (*((rbx + rcx - 1)) != bpl) {
        *((rbx + 0x2008))++;
    }
    rdx = *((rsp + 0x30));
    rbx = *((rsp + 0x28));
    rbx -= *((r13 + 0x2008));
    if (rdx >= rbx) {
        goto label_153;
    }
    r14 = r13;
    while (rdx < rax) {
        rbx = rax;
        r14 = *((r14 + 0x2010));
        rax = rbx;
        rax -= *((r14 + 0x2008));
    }
label_62:
    r12 = *((r14 + sym._init));
    rax = *((rsp + 0x30));
    rdi = r14;
    r12 += r14;
    if (rax >= rbx) {
        goto label_154;
    }
    rbx -= rax;
    do {
        esi = ebp;
        rax = rawmemchr ();
        rdi = rax + 1;
        rbx--;
    } while (rbx != 0);
label_154:
    r12 -= rdi;
    if (r12 != 0) {
        goto label_155;
    }
label_38:
    rbx = *((r14 + 0x2010));
    if (rbx == 0) {
        goto label_156;
    }
    do {
        rsi = *((rbx + sym._init));
        if (rsi != 0) {
            rdi = rbx;
            xwrite_stdout_part_0 ();
        }
        rbx = *((rbx + 0x2010));
    } while (rbx != 0);
label_156:
    eax = *((rsp + 0x20));
    *((rsp + 0x78)) = al;
    goto label_28;
label_100:
    r13d = 0;
    ecx = 0xa;
    goto label_29;
label_143:
    edx = 5;
    rax = dcgettext (0, "standard input");
    goto label_30;
label_145:
    xmalloc (0x2010);
    goto label_21;
label_113:
    if (*((rsp + 0x80)) != 0) {
        goto label_31;
    }
label_115:
    if (*(obj.from_start) != 0) {
        goto label_31;
    }
    rax = *((rsp + 0x218));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_157;
    }
    eax = 0;
    return rax;
label_126:
    edx = 1;
    esi = 0;
    edi = r15d;
    rax = lseek ();
    r12 = rax;
    if (rax == -1) {
        goto label_32;
    }
    edx = 2;
    esi = 0;
    edi = r15d;
    rax = lseek ();
    r8 = rax;
    if (r12 >= rax) {
        goto label_158;
    }
    *((rsp + 0x90)) = rax;
    eax = *((rsp + 0x20));
    *((rsp + 0x78)) = al;
    if (r8 == 0) {
        goto label_23;
    }
    al = file_lines (*((rsp + 0x38)), r15d, *((rsp + 0x30)), r12, r8, rsp + 0x90);
    *((rsp + 0x78)) = al;
    goto label_23;
label_149:
    rcx |= 0xffffffffffffffff;
    rax = dump_remainder (0, *((rsp + 0x38)), r15d, rcx);
    goto label_23;
label_147:
    r12 = *((rsp + 0x58));
    r13 = *((rsp + 0x50));
    ebp = 0;
    rbx = 0x0000e0fc;
    do {
        eax = strcmp (*(r12), rbx);
        if (eax == 0) {
            if (*((r12 + 0x34)) != 0) {
                goto label_159;
            }
            if (*((r12 + 0x38)) < 0) {
                goto label_159;
            }
            eax = *((r12 + 0x30));
            eax &= 0xf000;
            if (eax == 0x1000) {
                goto label_160;
            }
        }
label_159:
        rbp++;
label_36:
        r12 += 0x60;
    } while (r13 != r12);
    if (rbp == 0) {
        goto label_33;
    }
    eax = fstat (1, rsp + 0xf0);
    if (eax < 0) {
        goto label_161;
    }
    eax = *((rsp + 0x108));
    eax &= 0xf000;
    obj.monitor_output = (eax == 0x1000) ? 1 : 0;
    if (*(obj.disable_inotify) != 0) {
        goto label_65;
    }
    rbp = *((rsp + 0x58));
    r12 = *((rsp + 0x50));
    do {
        if (*((rbp + 0x34)) == 0) {
            eax = strcmp (*(rbp), rbx);
            if (eax == 0) {
                goto label_65;
            }
        }
        rbp += 0x60;
    } while (rbp != r12);
    rdx = *((rsp + 0x58));
    rcx = *((rsp + 0x68));
    eax = 0;
    do {
        if (*((rdx + rax + 0x38)) >= 0) {
            if (*((rdx + rax + 0x35)) != 0) {
                goto label_65;
            }
        }
        rax += 0x60;
    } while (rax != rcx);
    rdx = *((rsp + 0x58));
    eax = 0;
    do {
        if (*((rdx + rax + 0x38)) >= 0) {
            ecx = *((rdx + rax + 0x35));
            if (cl == 0) {
                goto label_162;
            }
        }
        rax += 0x60;
    } while (rax != *((rsp + 0x68)));
label_65:
    ebp = pid;
    *(obj.disable_inotify) = 1;
    if (ebp != 0) {
        goto label_163;
    }
    if (*(obj.follow_mode) == 2) {
        if (*((rsp + 8)) == 1) {
            goto label_164;
        }
    }
label_63:
    eax = ebp;
    rcx = *((rsp + 0x68));
    *((rsp + 0x48)) = 0;
    eax &= 1;
    *((rsp + 0x28)) = al;
    rax = *((rsp + 8));
    rax--;
    *((rsp + 0x18)) = rax;
    rax = *((rsp + 0x58));
    rax = rax + rcx + 0x34;
    *((rsp + 0x20)) = rax;
label_49:
    *((rsp + 0x10)) = 0;
    r15 = *((rsp + 0x58));
    ebx = 0;
    goto label_35;
label_34:
    r14 = *(r15);
    if (*(r14) == 0x2d) {
        if (*((r14 + 1)) != 0) {
            goto label_165;
        }
        edx = 5;
        rax = dcgettext (0, "standard input");
        r14 = rax;
    }
label_165:
    eax = *((r15 + 0x40));
    r12d = *((r15 + 0x30));
    if (ebp != eax) {
        eax = 0;
        eax = rpl_fcntl (r13d, 3, rdx, rcx, r8, r9);
        if (ebp != 0) {
            goto label_166;
        }
        if (eax < 0) {
            goto label_167;
        }
        edx = eax;
        dh |= 8;
        if (eax != edx) {
            eax = 0;
            eax = rpl_fcntl (r13d, 4, rdx, rcx, r8, r9);
            if (eax == 0xffffffff) {
                goto label_167;
            }
        }
label_44:
        *((r15 + 0x40)) = ebp;
        eax = ebp;
    }
label_45:
    rcx = 0xfffffffffffffffe;
    if (eax == 0) {
        goto label_168;
    }
label_43:
    rax = dump_remainder (0, r14, r13d, rcx);
    dl = (rax != 0) ? 1 : 0;
    *((r15 + 8)) += rax;
    do {
label_47:
        rbx++;
        r15 += 0x60;
        if (*((rsp + 8)) == rbx) {
            goto label_169;
        }
label_35:
    } while (*((r15 + 0x34)) != 0);
    r13d = *((r15 + 0x38));
    if (r13d >= 0) {
        goto label_34;
    }
    rbx++;
    r15 += 0x60;
    recheck (r15, ebp, rdx, rcx, r8, r9);
    if (*((rsp + 8)) != rbx) {
        goto label_35;
    }
label_169:
    edx = *(obj.reopen_inaccessible_files);
    if (dl != 0) {
        if (*(obj.follow_mode) == 1) {
            goto label_170;
        }
    }
    rax = *((rsp + 0x58));
    rcx = *((rsp + 0x20));
    rax += 0x34;
    do {
        esi = *((rax + 4));
        if (esi >= 0) {
            goto label_170;
        }
        if (*(rax) != 1) {
            if (dl != 0) {
                goto label_170;
            }
        }
        rax += 0x60;
    } while (rax != rcx);
    edx = 5;
    rax = dcgettext (0, "no files remaining");
    eax = 0;
    error (0, 0, rax);
    goto label_33;
label_160:
    *((r12 + 0x38)) = 0xffffffff;
    *((r12 + 0x34)) = 1;
    goto label_36;
label_137:
    eax = *(obj.reopen_inaccessible_files);
    rcx = *((rsp + 0x10));
    rbx = 0x0000ea41;
    eax ^= 1;
    *((rcx + 0x3c)) = 0xffffffff;
    *((rcx + 0x36)) = 0;
    *((rcx + 0x34)) = al;
    if (al != 0) {
        goto label_171;
    }
label_41:
    rax = *((rsp + 0x10));
    r12 = *(rax);
    eax = strcmp (*(rax), 0x0000e0fc);
    if (eax == 0) {
        goto label_172;
    }
label_42:
    rdx = r12;
    edi = 0;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: cannot follow end of this type of file%s");
    r8 = rbx;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_37;
label_91:
    eax = usage (1);
    edx = 5;
    *((rsp + 8)) = eax;
    rax = dcgettext (0, "option used in invalid context -- %c");
    ecx = *((rsp + 8));
    eax = 0;
    error (1, 0, rax);
label_155:
    rsi = r12;
    xwrite_stdout_part_0 ();
    goto label_38;
label_108:
    rax = errno_location ();
    if (*(rax) != 0x26) {
        goto label_10;
    }
    edx = 5;
    rax = dcgettext (0, "warning: --pid=PID is not supported on this system");
    eax = 0;
    error (0, 0, rax);
    *(obj.pid) = 0;
    goto label_10;
label_135:
    edx = 5;
    rax = dcgettext (0, "standard input");
    goto label_39;
label_150:
    rbx = *((rsp + 0x30));
    edx = 2;
    edi = r15d;
    rsi = rbx;
    rsi = -rsi;
    rax = lseek ();
    if (rax == -1) {
        goto label_27;
    }
    rbx += rax;
    goto label_40;
label_171:
    edx = 5;
    rax = dcgettext (0, "; giving up on this name");
    rbx = rax;
    goto label_41;
label_172:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    goto label_42;
label_168:
    eax = fstat (r13d, rsp + 0x180);
    if (eax != 0) {
        goto label_173;
    }
    eax = *((rsp + 0x198));
    if (*((r15 + 0x30)) == eax) {
        goto label_174;
    }
label_46:
    rdx = *((rsp + 0x1d8));
    r12d &= 0xf000;
    *((r15 + 0x30)) = eax;
    *((r15 + 0x58)) = 0;
    *((r15 + 0x10)) = rdx;
    rdx = *((rsp + 0x1e0));
    *((r15 + 0x18)) = rdx;
    if (r12d == 0x8000) {
        goto label_175;
    }
label_48:
    if (rbx != *((rsp + 0x18))) {
        if (*(obj.print_headers) != 0) {
            goto label_176;
        }
    }
label_50:
    edi = *((r15 + 0x40));
    *((rsp + 0x18)) = rbx;
    if (edi != 0) {
        goto label_177;
    }
    rcx = 0xffffffffffffffff;
    if (r12d != 0x8000) {
        goto label_43;
    }
    if (*((r15 + 0x35)) == 0) {
        goto label_43;
    }
    rcx = *((rsp + 0x1b0));
    rcx -= *((r15 + 8));
    goto label_43;
label_166:
    if (eax >= 0) {
        goto label_44;
    }
label_167:
    rax = errno_location ();
    edx = *((r15 + 0x30));
    edx &= 0xf000;
    if (edx != 0x8000) {
        goto label_178;
    }
    if (*(rax) != 1) {
        goto label_178;
    }
    eax = *((r15 + 0x40));
    goto label_45;
label_174:
    edx = eax;
    edx &= 0xf000;
    if (edx == 0x8000) {
        goto label_179;
    }
label_53:
    rcx = *((r15 + 0x10));
    cl = (*((rsp + 0x1d8)) < rcx) ? 1 : 0;
    dl = (*((rsp + 0x1d8)) > rcx) ? 1 : 0;
    rsi = *((r15 + 0x18));
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    ecx -= edx;
    edx = 0;
    sil = (*((rsp + 0x1e0)) > rsi) ? 1 : 0;
    dl = (*((rsp + 0x1e0)) < rsi) ? 1 : 0;
    esi = (int32_t) sil;
    edx -= esi;
    edx = rdx + rcx*2;
    if (edx != 0) {
        goto label_46;
    }
    rax = *((r15 + 0x58));
    rdx = rax + 1;
    *((r15 + 0x58)) = rdx;
    if (rax < *(obj.max_n_unchanged_stats_between_opens)) {
        goto label_47;
    }
    if (*(obj.follow_mode) != 1) {
        goto label_47;
    }
    sil = (*((r15 + 0x40)) != 0) ? 1 : 0;
    recheck (r15, 0, rdx, rcx, r8, r9);
    *((r15 + 0x58)) = 0;
    goto label_47;
label_175:
    rax = *((r15 + 8));
    if (*((rsp + 0x1b0)) >= rax) {
        goto label_48;
    }
    edi = 0;
    rdx = r14;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    *((rsp + 0x30)) = rax;
    rax = dcgettext (0, "%s: file truncated");
    rcx = *((rsp + 0x30));
    eax = 0;
    error (0, 0, rax);
    edx = 0;
    esi = 0;
    edi = r13d;
    rax = lseek ();
    if (rax < 0) {
        goto label_180;
    }
    *((r15 + 8)) = 0;
    goto label_48;
label_170:
    if (*((rsp + 0x10)) != 1) {
        goto label_181;
    }
    if (*((rsp + 0x28)) != 0) {
        goto label_181;
    }
    if (*(obj.monitor_output) == 0) {
        goto label_49;
    }
    edx = 0;
    rdi = rsp + 0x90;
    esi = 1;
    *((rsp + 0x90)) = 1;
    eax = poll ();
    if (eax < 0) {
        goto label_49;
    }
label_52:
    if ((*((rsp + 0x96)) & 0x18) != 0) {
        goto label_79;
    }
label_51:
    if (*((rsp + 0x10)) != 0) {
        goto label_49;
    }
    if (*((rsp + 0x48)) != 0) {
        goto label_33;
    }
    edi = pid;
    if (edi != 0) {
        goto label_182;
    }
label_54:
    xmm0 = *((rsp + 0x40));
    eax = xnanosleep (rdi);
    if (eax == 0) {
        goto label_49;
    }
    edx = 5;
    rax = dcgettext (0, "cannot read realtime clock");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_173:
    *((r15 + 0x38)) = 0xffffffff;
    rax = errno_location ();
    rdx = r14;
    edi = 0;
    esi = 3;
    r12 = rax;
    eax = *(rax);
    *((r15 + 0x3c)) = eax;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    error (0, *(r12), 0x0000e514);
    close (r13d);
    goto label_47;
label_176:
    rax = 0x0000ea41;
    rcx = r14;
    edi = 1;
    rdx = 0x0000ea40;
    rsi = "%s==> %s <==\n";
    if (*(obj.first_file.2) != 0) {
        rdx = rax;
    }
    eax = 0;
    printf_chk ();
    *(obj.first_file.2) = 0;
    goto label_50;
label_181:
    rdi = stdout;
    eax = fflush_unlocked ();
    if (eax != 0) {
        goto label_183;
    }
    if (*(obj.monitor_output) == 0) {
        goto label_51;
    }
    edx = 0;
    rdi = rsp + 0x90;
    esi = 1;
    *((rsp + 0x90)) = 1;
    eax = poll ();
    if (eax >= 0) {
        goto label_52;
    }
    goto label_51;
label_179:
    rcx = *((rsp + 0x1b0));
    if (*((r15 + 8)) != rcx) {
        goto label_46;
    }
    goto label_53;
label_182:
    eax = kill (rdi, 0);
    if (eax == 0) {
        goto label_54;
    }
    rax = errno_location ();
    if (*(rax) == 1) {
        goto label_54;
    }
    eax = *((rsp + 0x38));
    *((rsp + 0x48)) = al;
    goto label_49;
label_146:
    r12 = rdx;
    rax = free (rbp);
    r12++;
    rdx = *((rsp + 0x30));
    if (r12 != 0) {
        goto label_184;
    }
    goto label_185;
    do {
        rbp = *((rbp + 0x2008));
        rbx = rax;
label_184:
        rsi = *((rbp + sym._init));
        rax = rbx;
        rax -= rsi;
    } while (rdx < rax);
    rcx = *((rsp + 0x30));
    eax = 0;
    if (rcx < rbx) {
        rax = rbx;
        rax -= rcx;
    }
    rsi -= rax;
    if (rsi != 0) {
        rdi = rbp + rax;
        xwrite_stdout_part_0 ();
    }
    rbx = *((rbp + 0x2008));
    while (rbx != 0) {
        rsi = *((rbx + sym._init));
        if (rsi != 0) {
            rdi = rbx;
            xwrite_stdout_part_0 ();
        }
        rbx = *((rbx + 0x2008));
    }
    *((rsp + 0x20)) = 1;
    rdi = r13;
    while (rdi != 0) {
        rbx = *((rdi + 0x2008));
        free (rdi);
        rdi = rbx;
label_64:
    }
    goto label_17;
label_138:
    edx = 5;
    rax = dcgettext (0, "standard input");
    goto label_55;
label_151:
    edx = 1;
    esi = 0;
    edi = r15d;
    rax = lseek ();
    if (rax >= 0) {
        goto label_56;
    }
    rdx = *((rsp + 0x38));
    esi = 1;
    edi = 0;
    xlseek_part_0 ();
label_140:
    rsi = *((rsp + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "error reading %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    error (0, *(rax), r12);
    goto label_57;
label_142:
    edx = 5;
    rax = dcgettext (0, "standard input");
    goto label_58;
label_133:
    rax = lseek ();
    if (rax >= 0) {
        goto label_59;
    }
    rdx = *((rsp + 0x38));
    rdi = *((rsp + 0x30));
    esi = 1;
    xlseek_part_0 ();
label_152:
    rbx -= *((rsp + 0x30));
    edx = 0;
    edi = r15d;
    rsi = rbx;
    rax = lseek ();
    if (rax >= 0) {
        goto label_60;
    }
    rdx = *((rsp + 0x38));
    esi = 0;
    rdi = rbx;
    xlseek_part_0 ();
label_158:
    edx = 0;
    rsi = r12;
    edi = r15d;
    rax = lseek ();
    if (rax >= 0) {
        goto label_32;
    }
    rdx = *((rsp + 0x38));
    esi = 0;
    rdi = r12;
    xlseek_part_0 ();
label_107:
    edx = 5;
    rsi = "warning: --retry only effective for the initial open";
    goto label_61;
label_153:
    rbx = *((rsp + 0x28));
    r14 = r13;
    goto label_62;
label_163:
    ebp = 0;
    goto label_63;
label_185:
    rsi = *((rsp + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "error reading %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    error (0, *(rax), r12);
    rdi = r13;
    goto label_64;
label_164:
    rax = *((rsp + 0x58));
    if (*((rax + 0x38)) == 0xffffffff) {
        goto label_63;
    }
    eax = *((rax + 0x30));
    ebp = 0;
    *((rsp + 0x10)) = eax;
    eax &= 0xf000;
    bpl = (eax != 0x8000) ? 1 : 0;
    goto label_63;
label_162:
    rax = rsp + 0x180;
    *((rsp + 0x78)) = cl;
    rbx = *((rsp + 0x58));
    *((rsp + 0x28)) = rax;
label_66:
    eax = lstat (*(rbx), *((rsp + 0x28)));
    if (eax != 0) {
        goto label_186;
    }
    eax = *((rsp + 0x198));
    eax &= 0xf000;
    if (eax == segment.PHDR) {
        goto label_65;
    }
label_186:
    rbx += 0x60;
    if (rbx != *((rsp + 0x50))) {
        goto label_66;
    }
    rcx = *((rsp + 0x58));
    eax = 0;
label_67:
    if (*((rcx + rax + 0x38)) < 0) {
        goto label_187;
    }
    edx = *((rcx + rax + 0x30));
    edx &= 0xf000;
    if (edx == 0x8000) {
        goto label_187;
    }
    if (edx != 0x1000) {
        goto label_65;
    }
label_187:
    rax += 0x60;
    if (rax != *((rsp + 0x68))) {
        goto label_67;
    }
    if (*((rsp + 0x4f)) != 0) {
        goto label_188;
    }
    if (*(obj.follow_mode) == 2) {
        goto label_65;
    }
label_188:
    eax = inotify_init ();
    r13d = eax;
    if (eax < 0) {
        goto label_189;
    }
    rdi = stdout;
    eax = fflush_unlocked ();
    if (eax != 0) {
        goto label_190;
    }
    rax = hash_initialize (*((rsp + 8)), 0, dbg.wd_hasher, dbg.wd_comparator, 0);
    r14 = rax;
    if (rax == 0) {
        goto label_191;
    }
    edx = 2;
    eax = 0xc06;
    r12 = *((rsp + 0x58));
    if (*(obj.follow_mode) != 1) {
        eax = edx;
    }
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x48)) = eax;
    *((rsp + 0x10)) = 0;
    goto label_192;
label_68:
    eax = *((rsp + 0x18));
    ecx = *((rsp + 0x38));
    if (*((rbp + 0x38)) != 0xffffffff) {
        eax = ecx;
    }
    *((rsp + 0x18)) = al;
    rax = errno_location ();
    r12 = rax;
    eax = *(rax);
    edx = *(rax);
    edx &= 0xffffffef;
    if (edx == 0xc) {
        goto label_80;
    }
    if (eax == *((rbp + 0x3c))) {
        goto label_69;
    }
    rsi = *(rbp);
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r15 = rax;
    rax = dcgettext (0, "cannot watch %s");
    rcx = r15;
    eax = 0;
    error (0, *(r12), rax);
    do {
label_69:
        rbp += 0x60;
        if (*((rsp + 0x50)) == rbp) {
            goto label_193;
        }
label_192:
        r12d = follow_mode;
    } while (*((rbp + 0x34)) != 0);
    rbx = *(rbp);
    rax = strlen (*(rbp));
    rcx = *((rsp + 0x10));
    *((rbp + 0x44)) = 0xffffffff;
    if (rcx >= rax) {
        rax = rcx;
    }
    r12d--;
    *((rsp + 0x10)) = rax;
    if (r12d == 0) {
        rdi = rbx;
        rax = dir_len ();
        rdi = rbx;
        rdx = rbx + rax;
        r15 = rax;
        r12d = *(rdx);
        *((rsp + 0x30)) = rdx;
        rax = last_component ();
        rdx = *((rsp + 0x30));
        rsi = 0x0000e0e1;
        rax -= rbx;
        *((rbp + 0x50)) = rax;
        *(rdx) = 0;
        if (r15 != 0) {
            rsi = *(rbp);
        }
        edx = 0x784;
        edi = r13d;
        eax = inotify_add_watch ();
        *((rbp + 0x48)) = eax;
        rax = *(rbp);
        *((rax + r15)) = r12b;
        if (*((rbp + 0x48)) < 0) {
            goto label_194;
        }
    }
    edx = *((rsp + 0x48));
    rsi = *(rbp);
    edi = r13d;
    eax = inotify_add_watch ();
    *((rbp + 0x44)) = eax;
    if (eax < 0) {
        goto label_68;
    }
    rsi = rbp;
    rax = hash_insert (r14);
    if (rax == 0) {
        goto label_191;
    }
    eax = *((rsp + 0x38));
    *((rsp + 0x20)) = al;
    goto label_69;
label_178:
    rdx = r14;
    esi = 3;
    edi = 0;
    *((rsp + 8)) = rax;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: cannot change nonblocking mode");
    r11 = *((rsp + 8));
    rcx = r12;
    eax = 0;
    error (1, *(r11), rax);
label_161:
    edx = 5;
    rax = dcgettext (0, "standard output");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_183:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_148:
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), 0x0000e0fc);
label_112:
    rsi = 0x0000e0fc;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot follow %s by name");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_104:
    rax = quote (*((rbp + 8)), rsi, rdx, rcx, r8);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "invalid number");
    r12 = rax;
    rax = errno_location ();
    r8 = rbx;
    rcx = r12;
    eax = 0;
    error (1, *(rax), "%s: %s");
label_157:
    stack_chk_fail ();
label_180:
    rdx = r14;
    esi = 0;
    edi = 0;
    xlseek_part_0 ();
label_79:
    raise (0xd);
label_86:
    exit (1);
label_177:
    rcx = 0xfffffffffffffffe;
    goto label_43;
label_80:
    edx = 5;
label_83:
    rax = dcgettext (0, "inotify resources exhausted");
    eax = 0;
    error (0, 0, rax);
label_84:
    hash_free (r14, rsi);
    close (r13d);
    errno_location ();
    *(rax) = 0;
label_189:
    edx = 5;
    rax = dcgettext (0, "inotify cannot be used, reverting to polling");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (0, *(rax), r12);
    goto label_65;
label_193:
    if (*(obj.follow_mode) == 2) {
        goto label_195;
    }
label_85:
    r12 = *((rsp + 0x58));
    rax = *((rsp + 0x68));
    rbx = rsp + 0x88;
    rbp = *((rsp + 0x50));
    rax = r12 + rax - 0x60;
    *((rsp + 0x88)) = rax;
    goto label_196;
label_70:
    if (*((r12 + 0x38)) != 0xffffffff) {
        rsi = *((rsp + 0x28));
        rdi = *(r12);
        eax = stat ();
        if (eax != 0) {
            goto label_71;
        }
        rax = *((rsp + 0x180));
        if (*((r12 + 0x20)) != rax) {
            goto label_197;
        }
        rax = *((rsp + 0x188));
        if (*((r12 + 0x28)) != rax) {
            goto label_197;
        }
    }
label_71:
    check_fspec (r12, rbx, rdx, rcx, r8, r9);
    do {
        r12 += 0x60;
        if (rbp == r12) {
            goto label_198;
        }
label_196:
    } while (*((r12 + 0x34)) != 0);
    if (*(obj.follow_mode) != 1) {
        goto label_70;
    }
    recheck (r12, 0, rdx, rcx, r8, r9);
    goto label_71;
label_198:
    r12 = *((rsp + 0x10));
    r12 += 0x11;
    rax = xmalloc (r12);
    *((rsp + 0x30)) = 3;
    *((rsp + 0x18)) = rax;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x10)) = 0;
label_75:
    if (*(obj.follow_mode) == 1) {
        goto label_199;
    }
label_76:
    rcx = *((rsp + 0x20));
    if (*((rsp + 0x10)) >= rcx) {
        goto label_200;
    }
    rcx = *((rsp + 0x10));
    r15 = *((rsp + 0x18));
    r15 += rcx;
    edx = *((r15 + 0xc));
    rcx = rcx + rdx + 0x10;
    rax = rdx;
    edx = *(r15);
    *((rsp + 0x10)) = rcx;
    ecx = *((r15 + 4));
    if ((ch & 4) == 0) {
        goto label_201;
    }
    if (eax == 0) {
        goto label_202;
    }
label_73:
    *((rsp + 0x50)) = r12;
    rdi = r15 + 0x10;
    rbp = *((rsp + 0x58));
    ebx = 0;
    *((rsp + 0x60)) = r15;
    r12 = *((rsp + 8));
    r15 = r14;
    r14d = r13d;
    r13d = edx;
    while (*((rbp + 0x48)) != r13d) {
label_72:
        rbx++;
        rbp += 0x60;
        if (r12 == rbx) {
            goto label_203;
        }
    }
    rsi += *(rbp);
    *((rsp + 0x7c)) = ecx;
    *((rsp + 0x70)) = rdi;
    eax = strcmp (rdi, *((rbp + 0x50)));
    rdi = *((rsp + 0x70));
    ecx = *((rsp + 0x7c));
    if (eax != 0) {
        goto label_72;
    }
    rbx *= 0x60;
    rax = *((rsp + 0x58));
    r13d = r14d;
    r12 = *((rsp + 0x50));
    r14 = r15;
    r15 = *((rsp + 0x60));
    rbp = rax + rbx;
    ch &= 2;
    if (ch == 0) {
        goto label_204;
    }
    if (*(obj.follow_mode) == 1) {
        goto label_205;
    }
label_74:
    eax = *((r15 + 4));
    if ((eax & 0xe04) == 0) {
        goto label_206;
    }
label_201:
    if (eax != 0) {
        goto label_73;
    }
label_81:
    rsi = rsp + 0x90;
    rdi = r14;
    *((rsp + 0xd4)) = edx;
    rax = hash_lookup ();
    if (rax != 0) {
        goto label_74;
    }
    goto label_75;
label_199:
    if (*(obj.reopen_inaccessible_files) != 0) {
        goto label_76;
    }
    rdi = r14;
    rax = hash_get_n_entries ();
    if (rax != 0) {
        goto label_76;
    }
    edx = 5;
    rax = dcgettext (0, "no files remaining");
    eax = 0;
    error (1, 0, rax);
label_206:
    check_fspec (rbp, rsp + 0x88, rdx, rcx, r8, r9);
    goto label_75;
label_200:
    ebp = *((rsp + 0x78));
    rbx = *((rsp + 0x28));
    while (edi != 0) {
        if (bpl != 0) {
            goto label_207;
        }
        eax = kill (rdi, 0);
        if (eax == 0) {
            goto label_208;
        }
        rax = errno_location ();
        if (*(rax) == 1) {
            goto label_208;
        }
        ebp = *((rsp + 0x38));
        edx = 0;
label_77:
        eax = *(obj.monitor_output);
        ecx = 0x10;
        rdi = rbx;
        *((rsp + 0x180)) = r13d;
        *((rsp + 0x184)) = 1;
        *((rsp + 0x188)) = 1;
        rsi = rax + 1;
        eax = poll_chk ();
        if (eax != 0) {
            goto label_209;
        }
        edi = pid;
    }
label_78:
    edx |= 0xffffffff;
    goto label_77;
label_208:
    edx = 0;
    xmm7 = 0;
    __asm ("comisd xmm7, xmmword [rsp + 0x40]");
    if (edx >= 0) {
        goto label_77;
    }
    xmm6 = *(0x0000e4a0);
    xmm5 = *((rsp + 0x40));
    __asm ("comisd xmm6, xmm5");
    if (edx <= 0) {
        goto label_78;
    }
    xmm0 = *(0x0000e4a8);
    xmm1 = 0;
    eax = 0;
    __asm ("mulsd xmm0, xmm5");
    __asm ("cvttsd2si edx, xmm0");
    __asm ("cvtsi2sd xmm1, edx");
    __asm ("comisd xmm0, xmm1");
    al = (edx > 0) ? 1 : 0;
    edx += eax;
    goto label_77;
label_203:
    r13d = r14d;
    r12 = *((rsp + 0x50));
    r14 = r15;
    goto label_75;
label_209:
    *((rsp + 0x78)) = bpl;
    if (edx < 0) {
        goto label_210;
    }
    if (*((rsp + 0x18e)) != 0) {
        goto label_79;
    }
label_204:
    edx = *((rsp + 0x48));
    rsi = *(rbp);
    edi = r13d;
    eax = inotify_add_watch ();
    ebx = eax;
    if (eax >= 0) {
        goto label_211;
    }
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    eax &= 0xffffffef;
    if (eax == 0xc) {
        goto label_80;
    }
label_211:
    esi = *((rbp + 0x44));
    if (ebx != esi) {
        void (*0x5113)() ();
    }
label_191:
    eax = xalloc_die ();
label_202:
    eax = 0;
    goto label_212;
label_82:
    rax++;
    if (*((rsp + 8)) == rax) {
        goto label_81;
    }
label_212:
    rcx = rax * 0x60;
    rbx = *((rsp + 0x58));
    if (*((rbx + rcx + 0x48)) != edx) {
        goto label_82;
    }
    edx = 5;
    rsi = "directory containing watched file was removed";
    goto label_83;
label_205:
    recheck (rbp, 0, rdx, rcx, r8, r9);
    goto label_74;
label_195:
    if (*((rsp + 0x18)) != 0) {
        goto label_84;
    }
    if (*((rsp + 0x20)) != 0) {
        goto label_85;
    }
    goto label_86;
label_207:
    exit (0);
label_210:
    edx = 5;
    rax = dcgettext (0, "error waiting for inotify and output events");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_194:
    rax = errno_location ();
    rbx = rax;
    if (*(rax) == 0x1c) {
        goto label_80;
    }
label_190:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_197:
    rdi = *(r12);
    rax = pretty_name_isra_0 ();
    edi = 4;
    rsi = rax;
    rax = quotearg_style ();
    edx = 5;
    r15 = rax;
    rax = dcgettext (0, "%s was replaced");
    r12 = rax;
    rax = errno_location ();
    rcx = r15;
    eax = 0;
    error (0, *(rax), r12);
    goto label_84;
}

/* /tmp/tmp1cbccfqz @ 0xc970 */
 
int64_t dbg_dtotimespec (void) {
    /* timespec dtotimespec(double sec); */
    r8 = 0x8000000000000000;
    edx = 0;
    __asm ("comisd xmm0, xmmword [0x0000ee90]");
    if (? > ?) {
        xmm1 = *(0x0000ed60);
        edx = 0x3b9ac9ff;
        r8 = 0x7fffffffffffffff;
        __asm ("comisd xmm1, xmm0");
        if (? <= ?) {
            goto label_0;
        }
        __asm ("cvttsd2si r8, xmm0");
        xmm1 = 0;
        edx = 0;
        __asm ("cvtsi2sd xmm1, r8");
        __asm ("subsd xmm0, xmm1");
        __asm ("mulsd xmm0, qword [0x0000ee98]");
        xmm1 = 0;
        __asm ("cvttsd2si rax, xmm0");
        __asm ("cvtsi2sd xmm1, rax");
        __asm ("comisd xmm0, xmm1");
        dl = (? > ?) ? 1 : 0;
        rcx = rdx + rax;
        rdx = 0x112e0be826d694b3;
        rax = rcx;
        rdx:rax = rax * rdx;
        rax = rdx;
        rdx = rcx;
        rdx >>= 0x3f;
        rax >>= 0x1a;
        rax -= rdx;
        r8 += rax;
        rax *= 0x3b9aca00;
        rcx -= rax;
        rdx = rcx;
        if (rcx < 0) {
            goto label_1;
        }
    }
label_0:
    rax = r8;
    return rax;
label_1:
    r8--;
    rdx += 0x3b9aca00;
    rax = r8;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x6e40 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "Print the last %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n");
    edx = 0xa;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -c, --bytes=[+]NUM       output the last NUM bytes; or use -c +NUM to\n                             output starting with byte NUM of each file\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -f, --follow[={name|descriptor}]\n                           output appended data as the file grows;\n                             an absent option argument means 'descriptor'\n  -F                       same as --follow=name --retry\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "  -n, --lines=[+]NUM       output the last NUM lines, instead of the last %d;\n                             or use -n +NUM to output starting with line NUM\n      --max-unchanged-stats=N\n                           with --follow=name, reopen a FILE which has not\n                             changed size after N (default %d) iterations\n                             to see if it has been unlinked or renamed\n                             (this is the usual case of rotated log files);\n                             with inotify, this option is rarely useful\n");
    ecx = 5;
    edx = 0xa;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --pid=PID            with -f, terminate after process ID, PID dies\n  -q, --quiet, --silent    never output headers giving file names\n      --retry              keep trying to open a file if it is inaccessible\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -s, --sleep-interval=N   with -f, sleep for approximately N seconds\n                             (default 1.0) between iterations;\n                             with inotify and --pid=P, check process P at\n                             least once every N seconds\n  -v, --verbose            always output headers giving file names\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -z, --zero-terminated    line delimiter is NUL, not newline\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "With --follow (-f), tail defaults to following the file descriptor, which\nmeans that even if a tail'ed file is renamed, tail will continue to track\nits end.  This default behavior is not desirable when you really want to\ntrack the actual name of the file, not the file descriptor (e.g., log\nrotation).  Use --follow=name in that case.  That causes tail to track the\nnamed file in a way that accommodates renaming, removal and creation.\n");
    rsi = r12;
    r12 = "tail";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000e03e;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000e0b8;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000e0c2, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000ea41;
    r12 = 0x0000e05a;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000e0c2, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "tail";
        printf_chk ();
        r12 = 0x0000e05a;
    }
label_5:
    r13 = "tail";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmp1cbccfqz @ 0xa860 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xaf50 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x7a90 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = rsi - 0x400;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xad80 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29e3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x84e0 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x29af)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmp1cbccfqz @ 0xac60 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x29d9)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8f40 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0x8db0)() ();
}

/* /tmp/tmp1cbccfqz @ 0xa900 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x29ce)() ();
    }
    if (rdx == 0) {
        void (*0x29ce)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xae10 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013250]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00013260]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xbfb0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xc030 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xab60 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp1cbccfqz @ 0xc880 */
 
int64_t dbg_c_strtod (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* double c_strtod(char const * nptr,char ** endptr); */
    r12 = rdi;
    rax = c_locale_cache;
    while (1) {
        rdx = c_locale_cache;
        if (rdx != 0) {
            rsi = rbp;
            rdi = r12;
            void (*0x25d0)() ();
        }
        if (rbp != 0) {
            *(rbp) = r12;
        }
        xmm0 = 0;
        return rax;
        edx = 0;
        rsi = 0x0000ee8b;
        edi = 0x1fbf;
        rax = newlocale ();
        *(obj.c_locale_cache) = rax;
    }
}

/* /tmp/tmp1cbccfqz @ 0xbcd0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x8610 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmp1cbccfqz @ 0xcb10 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x28d0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp1cbccfqz @ 0x7d00 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0xa8a0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xbeb0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp1cbccfqz @ 0xbaf0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp1cbccfqz @ 0x2780 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp1cbccfqz @ 0x78c0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp1cbccfqz @ 0xc900 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp1cbccfqz @ 0x2560 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp1cbccfqz @ 0xb4e0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000e9e8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000e9fb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000ece8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xece8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2930)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2930)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2930)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp1cbccfqz @ 0xb480 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xb970 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bl |= al;
}

/* /tmp/tmp1cbccfqz @ 0xc010 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xc2d0 */
 
int64_t dbg_xnanosleep (int64_t arg7) {
    timespec ts_sleep;
    int64_t var_8h;
    int64_t var_18h;
    xmm0 = arg7;
    /* int xnanosleep(double seconds); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = errno_location ();
    xmm1 = rbp;
    __asm ("comisd xmm1, xmmword [0x0000ed60]");
    rbx = rax;
    if (? >= ?) {
        goto label_1;
    }
label_0:
    xmm0 = rbp;
    rax = dtotimespec ();
    *(rsp) = rax;
    *((rsp + 8)) = rdx;
    while (eax != 0) {
        if ((*(rbx) & 0xfffffffb) != 0) {
            goto label_3;
        }
        *(rbx) = 0;
        rsi = rbp;
        eax = rpl_nanosleep (rbp, rsi);
    }
label_2:
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_4;
    }
    return rax;
label_1:
    pause ();
    if (*(rbx) != 4) {
        goto label_0;
    }
    pause ();
    if (*(rbx) == 4) {
        goto label_1;
    }
    goto label_0;
label_3:
    eax = 0xffffffff;
    goto label_2;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp1cbccfqz @ 0x8220 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0xce80 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp1cbccfqz @ 0xbd10 */
 
uint64_t dbg_xnmalloc (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0xc0d0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27b0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp1cbccfqz @ 0x7710 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp1cbccfqz @ 0x24f0 */
 
void getenv (void) {
    /* [15] -r-x section size 1200 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp1cbccfqz @ 0x2500 */
 
void raise (void) {
    __asm ("bnd jmp qword [reloc.raise]");
}

/* /tmp/tmp1cbccfqz @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax -= 0x3030;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x1001781)) += al;
    *(rax) += eax;
    bh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *((rax + 0x17)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    dh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(0x400000f1) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *((rcx + 0xae)) += dh;
    *(rax) += al;
    *((rcx + 0xae)) += dh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmp1cbccfqz @ 0x2550 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp1cbccfqz @ 0x2570 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmp1cbccfqz @ 0x2590 */
 
void strtod (void) {
    __asm ("bnd jmp qword [reloc.strtod]");
}

/* /tmp/tmp1cbccfqz @ 0x25a0 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmp1cbccfqz @ 0x25c0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp1cbccfqz @ 0x25d0 */
 
void strtod_l (void) {
    __asm ("bnd jmp qword [reloc.strtod_l]");
}

/* /tmp/tmp1cbccfqz @ 0x25e0 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp1cbccfqz @ 0x25f0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp1cbccfqz @ 0x2610 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp1cbccfqz @ 0x2640 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp1cbccfqz @ 0x2650 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp1cbccfqz @ 0x2660 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmp1cbccfqz @ 0x2670 */
 
void newlocale (void) {
    __asm ("bnd jmp qword [reloc.newlocale]");
}

/* /tmp/tmp1cbccfqz @ 0x2680 */
 
void nanosleep (void) {
    __asm ("bnd jmp qword [reloc.nanosleep]");
}

/* /tmp/tmp1cbccfqz @ 0x2690 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp1cbccfqz @ 0x26b0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp1cbccfqz @ 0x26c0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp1cbccfqz @ 0x26d0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp1cbccfqz @ 0x26e0 */
 
void poll_chk (void) {
    __asm ("bnd jmp qword [reloc.__poll_chk]");
}

/* /tmp/tmp1cbccfqz @ 0x2710 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmp1cbccfqz @ 0x2720 */
 
void lstat (void) {
    __asm ("bnd jmp qword [reloc.lstat]");
}

/* /tmp/tmp1cbccfqz @ 0x2730 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp1cbccfqz @ 0x2750 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmp1cbccfqz @ 0x2790 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmp1cbccfqz @ 0x27a0 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmp1cbccfqz @ 0x27b0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp1cbccfqz @ 0x27c0 */
 
void kill (void) {
    __asm ("bnd jmp qword [reloc.kill]");
}

/* /tmp/tmp1cbccfqz @ 0x27d0 */
 
void inotify_init (void) {
    __asm ("bnd jmp qword [reloc.inotify_init]");
}

/* /tmp/tmp1cbccfqz @ 0x27e0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp1cbccfqz @ 0x27f0 */
 
void pause (void) {
    __asm ("bnd jmp qword [reloc.pause]");
}

/* /tmp/tmp1cbccfqz @ 0x2810 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp1cbccfqz @ 0x2820 */
 
void inotify_add_watch (void) {
    __asm ("bnd jmp qword [reloc.inotify_add_watch]");
}

/* /tmp/tmp1cbccfqz @ 0x2840 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp1cbccfqz @ 0x2890 */
 
void poll (void) {
    __asm ("bnd jmp qword [reloc.poll]");
}

/* /tmp/tmp1cbccfqz @ 0x28b0 */
 
void memrchr (void) {
    __asm ("bnd jmp qword [reloc.memrchr]");
}

/* /tmp/tmp1cbccfqz @ 0x28c0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmp1cbccfqz @ 0x28d0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp1cbccfqz @ 0x28e0 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmp1cbccfqz @ 0x28f0 */
 
void fstatfs (void) {
    __asm ("bnd jmp qword [reloc.fstatfs]");
}

/* /tmp/tmp1cbccfqz @ 0x2900 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp1cbccfqz @ 0x2940 */
 
void fflush_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fflush_unlocked]");
}

/* /tmp/tmp1cbccfqz @ 0x2950 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp1cbccfqz @ 0x2960 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp1cbccfqz @ 0x2970 */
 
void inotify_rm_watch (void) {
    __asm ("bnd jmp qword [reloc.inotify_rm_watch]");
}

/* /tmp/tmp1cbccfqz @ 0x2980 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmp1cbccfqz @ 0x2990 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp1cbccfqz @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1216 named .plt */
    __asm ("bnd jmp qword [0x00012d78]");
}

/* /tmp/tmp1cbccfqz @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x24a0 */
 
void fcn_000024a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x24b0 */
 
void fcn_000024b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x24c0 */
 
void fcn_000024c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp1cbccfqz @ 0x24d0 */
 
void fcn_000024d0 (void) {
    return __asm ("bnd jmp section..plt");
}
