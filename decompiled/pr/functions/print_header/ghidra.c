void print_header(void)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  undefined auStack312 [280];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  output_position = 0;
  pad_across_to(chars_per_margin);
  print_white_space();
  if (page_number == 0) {
    uVar3 = dcgettext(0,"page number overflow",5);
    error(1,0,uVar3);
  }
  else {
    uVar3 = dcgettext(0,"Page %lu",5);
    __sprintf_chk(auStack312,1,0x114,uVar3);
    iVar2 = header_width_available;
    uVar3 = 0x1041b6;
    iVar1 = gnu_mbswidth(auStack312,0);
    iVar2 = iVar2 - iVar1;
    if (iVar2 < 0) {
      iVar2 = 0;
    }
    __printf_chk(1,"\n\n%*s%s%*s%s%*s%s\n\n\n",chars_per_margin,&DAT_0010e06b,date_text,iVar2 >> 1,
                 &DAT_0010e063,file_text,iVar2 - (iVar2 >> 1),&DAT_0010e063,auStack312,uVar3);
    print_a_header = 0;
    output_position = 0;
    if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
      output_position = 0;
      print_a_header = 0;
      return;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}