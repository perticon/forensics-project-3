void print_header(void) {
    int64_t v1 = __readfsqword(40); // 0x413f
    output_position = 0;
    pad_across_to(chars_per_margin);
    print_white_space();
    function_25d0();
    if (page_number == 0) {
        // 0x423c
        function_27f0();
        // 0x425b
        function_2600();
        return;
    }
    // 0x417b
    function_28d0();
    int64_t v2; // bp-312, 0x4130
    gnu_mbswidth((char *)&v2, 0);
    function_27d0();
    *(char *)&print_a_header = 0;
    output_position = 0;
    if (v1 == __readfsqword(40)) {
        // 0x4232
        return;
    }
    // 0x425b
    function_2600();
}