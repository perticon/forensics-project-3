void skip_read(_IO_FILE **param_1,int param_2)

{
  _IO_FILE *__stream;
  byte *pbVar1;
  char cVar2;
  uint uVar3;
  long lVar4;
  long lVar5;
  bool bVar6;
  char cVar7;
  
  __stream = *param_1;
  pbVar1 = (byte *)__stream->_IO_read_ptr;
  if (pbVar1 < __stream->_IO_read_end) {
    __stream->_IO_read_ptr = (char *)(pbVar1 + 1);
    uVar3 = (uint)*pbVar1;
  }
  else {
    uVar3 = __uflow(__stream);
  }
  cVar2 = last_line;
  cVar7 = false;
  if (uVar3 == 0xc) {
    if (*(char *)((long)param_1 + 0x39) == '\0') {
      if (last_line == '\0') {
        cVar7 = '\x01';
      }
      else {
        *(undefined *)((long)param_1 + 0x39) = 1;
        cVar7 = cVar2;
LAB_00103ca7:
        if (cVar2 != '\0') {
          if (parallel_files == '\0') {
            if (columns != 0) {
              lVar5 = (ulong)columns * 0x40 + column_vector;
              lVar4 = column_vector;
              do {
                *(undefined *)(lVar4 + 0x39) = 0;
                lVar4 = lVar4 + 0x40;
              } while (lVar5 != lVar4);
            }
          }
          else {
            *(undefined *)((long)param_1 + 0x39) = 0;
          }
        }
      }
      pbVar1 = (byte *)__stream->_IO_read_ptr;
      if (pbVar1 < __stream->_IO_read_end) {
        __stream->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar3 = (uint)*pbVar1;
      }
      else {
        uVar3 = __uflow(__stream);
      }
      if (uVar3 == 10) {
        hold_file(param_1);
      }
      else {
        ungetc(uVar3,__stream);
        hold_file(param_1);
      }
      goto LAB_00103bf0;
    }
    pbVar1 = (byte *)__stream->_IO_read_ptr;
    if (pbVar1 < __stream->_IO_read_end) {
      __stream->_IO_read_ptr = (char *)(pbVar1 + 1);
      uVar3 = (uint)*pbVar1;
    }
    else {
      uVar3 = __uflow(__stream);
    }
    if (uVar3 == 10) {
      pbVar1 = (byte *)__stream->_IO_read_ptr;
      if (pbVar1 < __stream->_IO_read_end) {
        __stream->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar3 = (uint)*pbVar1;
      }
      else {
        uVar3 = __uflow(__stream);
      }
    }
    cVar7 = uVar3 == 0xc;
  }
  bVar6 = last_line != '\0';
  *(undefined *)((long)param_1 + 0x39) = 0;
  cVar2 = last_line;
  if (bVar6) {
    *(undefined *)((long)param_1 + 0x39) = 1;
    cVar2 = last_line;
  }
  while (last_line = cVar2, uVar3 != 10) {
    if (uVar3 == 0xc) goto LAB_00103ca7;
    if (uVar3 == 0xffffffff) {
      close_file(param_1);
      break;
    }
    pbVar1 = (byte *)__stream->_IO_read_ptr;
    if (pbVar1 < __stream->_IO_read_end) {
      __stream->_IO_read_ptr = (char *)(pbVar1 + 1);
      uVar3 = (uint)*pbVar1;
      cVar2 = last_line;
    }
    else {
      uVar3 = __uflow(__stream);
      cVar2 = last_line;
    }
  }
LAB_00103bf0:
  if ((skip_count != '\0') && (((parallel_files != '\x01' || (param_2 == 1)) && (cVar7 == '\0')))) {
    line_count = line_count + 1;
  }
  return;
}