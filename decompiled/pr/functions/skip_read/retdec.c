void skip_read(int32_t * p, int32_t column_number) {
    int64_t v1 = (int64_t)p;
    int64_t * v2 = (int64_t *)(v1 + 8); // 0x3bb2
    uint64_t v3 = *v2; // 0x3bb2
    int64_t * v4 = (int64_t *)(v1 + 16); // 0x3bb6
    int64_t v5; // 0x3ba0
    if (v3 >= *v4) {
        // 0x3d48
        v5 = function_24b0();
    } else {
        // 0x3bc0
        *v2 = v3 + 1;
        v5 = (int64_t)*(char *)v3;
    }
    char * v6 = (char *)(v1 + 57);
    int64_t v7 = v5; // 0x3bd1
    int64_t v8 = 0; // 0x3bd1
    int64_t v9; // 0x3ba0
    int64_t v10; // 0x3ba0
    if ((int32_t)v5 == 12) {
        // 0x3c50
        if (*v6 == 0) {
            unsigned char v11 = *(char *)&last_line; // 0x3d10
            v10 = 1;
            if (v11 == 0) {
                goto lab_0x3cdd;
            } else {
                // 0x3ca7
                *v6 = 1;
                v9 = v11;
                goto lab_0x3cab;
            }
        } else {
            uint64_t v12 = *v2; // 0x3c5b
            int64_t v13; // 0x3ba0
            if (v12 >= *v4) {
                // 0x3d90
                v13 = function_24b0();
            } else {
                // 0x3c69
                *v2 = v12 + 1;
                v13 = (int64_t)*(char *)v12;
            }
            int64_t v14 = v13; // 0x3c77
            if ((int32_t)v13 == 10) {
                uint64_t v15 = *v2; // 0x3d67
                if (v15 >= *v4) {
                    // 0x3d9d
                    v14 = function_24b0();
                } else {
                    // 0x3d71
                    *v2 = v15 + 1;
                    v14 = (int64_t)*(char *)v15;
                }
            }
            // 0x3c7d
            v7 = v14;
            v8 = (int32_t)v7 == 12;
            goto lab_0x3bd3;
        }
    } else {
        goto lab_0x3bd3;
    }
  lab_0x3bd3:
    // 0x3bd3
    *v6 = 0;
    int64_t v16 = v7; // 0x3bdf
    int64_t v17; // 0x3ba0
    if (*(char *)&last_line == 0) {
        goto lab_0x3c2b;
    } else {
        // 0x3be1
        *v6 = 1;
        v17 = v8;
        if ((int32_t)v7 != 10) {
            goto lab_0x3c30;
        } else {
            goto lab_0x3bf0;
        }
    }
  lab_0x3c2b:
    // 0x3c2b
    v17 = v8;
    if ((int32_t)v16 == 10) {
        goto lab_0x3bf0;
    } else {
        goto lab_0x3c30;
    }
  lab_0x3cdd:;
    uint64_t v18 = *v2; // 0x3cdd
    int64_t v19; // 0x3ba0
    if (v18 >= *v4) {
        // 0x3d81
        v19 = function_24b0() & 0xffffffff;
    } else {
        // 0x3ceb
        *v2 = v18 + 1;
        v19 = (int64_t)*(char *)v18;
    }
    // 0x3cf6
    if (v19 != 10) {
        // 0x3d30
        function_2780();
        hold_file(p);
        v17 = v10;
    } else {
        // 0x3cfb
        hold_file(p);
        v17 = v10;
    }
    goto lab_0x3bf0;
  lab_0x3bf0:
    // 0x3bf0
    if (*(char *)&skip_count == 0 || column_number != 1 == *(char *)&parallel_files == 1) {
        // 0x3c13
        return;
    }
    // 0x3c07
    if ((char)v17 == 0) {
        // 0x3c0c
        line_count++;
    }
  lab_0x3c30:;
    // 0x3c30
    int64_t v20; // 0x3ba0
    switch ((int32_t)v20) {
        case 12: {
            // 0x3ca7
            v9 = v8;
            v10 = v8;
            if (*(char *)&last_line == 0) {
                goto lab_0x3cdd;
            } else {
                goto lab_0x3cab;
            }
        }
        case -1: {
            // 0x3c90
            close_file(p);
            v17 = v8;
            goto lab_0x3bf0;
        }
        default: {
            uint64_t v21 = *v2; // 0x3c3a
            if (v21 < *v4) {
                // 0x3c20
                *v2 = v21 + 1;
                v16 = (int64_t)*(char *)v21;
            } else {
                // 0x3c44
                v16 = function_24b0();
            }
            goto lab_0x3c2b;
        }
    }
  lab_0x3cab:
    // 0x3cab
    if (*(char *)&parallel_files != 0) {
        // 0x3d5d
        *v6 = 0;
        v10 = v9;
    } else {
        // 0x3cb8
        v10 = v9;
        if (columns != 0) {
            int64_t v22 = (int64_t)column_vector; // 0x3cbe
            int64_t v23 = v22; // 0x3ccd
            *(char *)(v23 + 57) = 0;
            v23 += 64;
            v10 = v9;
            while (64 * (int64_t)columns + v22 != v23) {
                // 0x3cd0
                *(char *)(v23 + 57) = 0;
                v23 += 64;
                v10 = v9;
            }
        }
    }
    goto lab_0x3cdd;
}