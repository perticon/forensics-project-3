void init_header(char * filename, int32_t desc) {
    int64_t v1 = (int64_t)filename;
    int64_t v2 = __readfsqword(40); // 0x3897
    int32_t v3; // 0x3880
    if ((char)v1 != 45) {
        goto lab_0x39d0;
    } else {
        // 0x38b3
        v3 = -1;
        if (*(char *)(v1 + 1) != 0) {
            goto lab_0x39d0;
        } else {
            goto lab_0x38c3;
        }
    }
  lab_0x39d0:
    // 0x39d0
    v3 = desc;
    int32_t v4; // 0x3880
    int64_t v5; // 0x3880
    if (desc < 0) {
        goto lab_0x38c3;
    } else {
        // 0x39d9
        v3 = desc;
        if ((int32_t)function_28a0() != 0) {
            goto lab_0x38c3;
        } else {
            // 0x39ee
            v4 = desc;
            int32_t v6; // 0x3880
            v5 = v6;
            goto lab_0x38e2;
        }
    }
  lab_0x38c3:;
    int128_t v7 = g19; // 0x38cb
    if ((int64_t)g19 == 0) {
        // 0x3a70
        gettime();
        v7 = g19;
    }
    // 0x38d1
    v4 = v3;
    goto lab_0x38e2;
  lab_0x38e2:;
    int64_t v8 = __asm_movaps(__asm_movdqa(v7));
    int64_t v9; // 0x3880
    int64_t v10; // bp-296, 0x3880
    if (localtime_rz(localtz, &v8, (struct tm *)&v10) == NULL) {
        int64_t v11 = xmalloc(); // 0x3a2d
        int64_t v12; // bp-88, 0x3880
        imaxtostr(v8, (char *)&v12);
        function_28d0();
        v9 = v11;
    } else {
        int32_t v13 = v5; // 0x3909
        int64_t v14 = nstrftime(NULL, -1, (char *)date_format, (struct tm *)&v10, localtz, v13); // 0x391f
        int64_t v15 = xmalloc(); // 0x392b
        nstrftime((char *)v15, v14 + 1, (char *)date_format, (struct tm *)&v10, localtz, v13);
        v9 = v15;
    }
    // 0x3952
    function_24d0();
    date_text = v9;
    int64_t v16 = custom_header; // 0x396f
    if (custom_header == 0) {
        // 0x3a10
        v16 = v4 >= 0 ? v1 : (int64_t)&g35;
    }
    // 0x3975
    file_text = v16;
    int32_t v17 = gnu_mbswidth((char *)v9, 0); // 0x3987
    header_width_available = chars_per_line - v17 - gnu_mbswidth((char *)file_text, 0);
    if (v2 == __readfsqword(40)) {
        // 0x39bb
        return;
    }
    // 0x3a81
    function_2600();
}