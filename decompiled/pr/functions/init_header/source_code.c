init_header (char const *filename, int desc)
{
  char *buf = NULL;
  struct stat st;
  struct timespec t;
  int ns;
  struct tm tm;

  /* If parallel files or standard input, use current date. */
  if (STREQ (filename, "-"))
    desc = -1;
  if (0 <= desc && fstat (desc, &st) == 0)
    t = get_stat_mtime (&st);
  else
    {
      static struct timespec timespec;
      if (! timespec.tv_sec)
        gettime (&timespec);
      t = timespec;
    }

  ns = t.tv_nsec;
  if (localtime_rz (localtz, &t.tv_sec, &tm))
    {
      size_t bufsize
        = nstrftime (NULL, SIZE_MAX, date_format, &tm, localtz, ns) + 1;
      buf = xmalloc (bufsize);
      nstrftime (buf, bufsize, date_format, &tm, localtz, ns);
    }
  else
    {
      char secbuf[INT_BUFSIZE_BOUND (intmax_t)];
      buf = xmalloc (sizeof secbuf + MAX (10, INT_BUFSIZE_BOUND (int)));
      sprintf (buf, "%s.%09d", timetostr (t.tv_sec, secbuf), ns);
    }

  free (date_text);
  date_text = buf;
  file_text = custom_header ? custom_header : desc < 0 ? "" : filename;
  header_width_available = (chars_per_line
                            - mbswidth (date_text, 0)
                            - mbswidth (file_text, 0));
}