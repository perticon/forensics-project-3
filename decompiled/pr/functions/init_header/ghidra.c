void init_header(char *param_1,int param_2)

{
  int iVar1;
  int iVar2;
  int iVar3;
  long lVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  long in_FS_OFFSET;
  long local_138;
  ulong uStack304;
  undefined local_128 [64];
  stat local_e8;
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*param_1 == '-') && (param_1[1] == '\0')) {
    param_2 = -1;
  }
  else if ((-1 < param_2) && (iVar3 = fstat(param_2,&local_e8), iVar3 == 0)) {
    local_138 = local_e8.st_mtim.tv_sec;
    goto LAB_001038e2;
  }
  if (timespec_0._0_8_ == 0) {
    gettime(timespec_0);
  }
  local_138 = timespec_0._0_8_;
  local_e8.st_mtim.tv_nsec = timespec_0._8_8_;
LAB_001038e2:
  uStack304 = local_e8.st_mtim.tv_nsec;
  lVar4 = localtime_rz(localtz,&local_138,local_128);
  if (lVar4 == 0) {
    uVar5 = xmalloc(0x21);
    uVar6 = imaxtostr(local_138,local_58);
    __sprintf_chk(uVar5,1,0x21,"%s.%09d",uVar6,local_e8.st_mtim.tv_nsec & 0xffffffff);
  }
  else {
    lVar4 = nstrftime(0,0xffffffffffffffff,date_format,local_128,localtz,
                      local_e8.st_mtim.tv_nsec & 0xffffffff);
    uVar5 = xmalloc(lVar4 + 1);
    nstrftime(uVar5,lVar4 + 1,date_format,local_128,localtz,local_e8.st_mtim.tv_nsec & 0xffffffff);
  }
  free(date_text);
  iVar3 = chars_per_line;
  file_text = custom_header;
  if ((custom_header == (char *)0x0) && (file_text = "", -1 < param_2)) {
    file_text = param_1;
  }
  date_text = (void *)uVar5;
  iVar1 = gnu_mbswidth(uVar5,0);
  iVar2 = gnu_mbswidth(file_text,0);
  header_width_available = (iVar3 - iVar1) - iVar2;
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}