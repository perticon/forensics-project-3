bool first_last_page(int32_t oi, char c, char * pages) {
    int64_t v1 = __readfsqword(40); // 0x3513
    int64_t v2 = -1; // bp-56, 0x352b
    int64_t first; // bp-72, 0x34f0
    int64_t last; // bp-64, 0x34f0
    int32_t v3 = xstrtoumax(pages, (char **)&first, 10, &last, (char *)&g35); // 0x3537
    if ((v3 & -3) != 0) {
        // 0x35f6
        return xstrtol_fatal() % 2 != 0;
    }
    int64_t v4 = 0; // 0x354e
    int64_t v5; // 0x34f0
    char v6; // 0x34f0
    if (first == (int64_t)pages || last == 0) {
        goto lab_0x358a;
    } else {
        char v7 = *(char *)first; // 0x3558
        v5 = -1;
        v6 = v7;
        if (v7 == 58) {
            int64_t v8 = first + 1; // 0x35b0
            int32_t v9 = xstrtoumax((char *)v8, (char **)&first, 10, &v2, (char *)&g35); // 0x35cb
            if (v9 != 0) {
                // 0x35f6
                return xstrtol_fatal() % 2 != 0;
            }
            // 0x35d4
            v4 = 0;
            if (first == v8) {
                goto lab_0x358a;
            } else {
                // 0x35dd
                v4 = 0;
                if (v2 < last) {
                    goto lab_0x358a;
                } else {
                    // 0x35e9
                    v5 = v2;
                    v6 = *(char *)first;
                    goto lab_0x3560;
                }
            }
        } else {
            goto lab_0x3560;
        }
    }
  lab_0x358a:
    // 0x358a
    if (v1 == __readfsqword(40)) {
        // 0x359a
        return v4 != 0;
    }
    // 0x35f1
    function_2600();
    // 0x35f6
    return xstrtol_fatal() % 2 != 0;
  lab_0x3560:
    // 0x3560
    v4 = 0;
    if (v6 == 0) {
        // 0x3564
        first_page_number = last;
        last_page_number = v5;
        v4 = 1;
    }
    goto lab_0x358a;
}