size_t first_last_page(undefined4 param_1,char param_2,char *param_3)

{
  uint uVar1;
  size_t sVar2;
  char cVar3;
  undefined4 *extraout_RDX;
  char *pcVar4;
  long in_FS_OFFSET;
  char *local_48;
  ulong local_40;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = 0xffffffffffffffff;
  uVar1 = xstrtoumax(param_3,&local_48,10,&local_40,&DAT_0010e06b);
  if ((uVar1 & 0xfffffffd) != 0) {
LAB_001035f6:
    pcVar4 = (char *)(ulong)uVar1;
    xstrtol_fatal(pcVar4,param_1,(int)param_2,long_options,param_3);
    sVar2 = strlen(pcVar4);
    if (sVar2 < 0x80000000) {
      col_sep_string = pcVar4;
      col_sep_length = (int)sVar2;
      return sVar2;
    }
    integer_overflow();
    sVar2 = xdectoimax();
    *extraout_RDX = (int)sVar2;
    return sVar2;
  }
  if ((local_48 != param_3) && (local_40 != 0)) {
    cVar3 = *local_48;
    if (cVar3 == ':') {
      pcVar4 = local_48 + 1;
      uVar1 = xstrtoumax(pcVar4,&local_48,10,&local_38,&DAT_0010e06b);
      if (uVar1 != 0) goto LAB_001035f6;
      if ((local_48 == pcVar4) || (local_38 < local_40)) goto LAB_00103588;
      cVar3 = *local_48;
    }
    if (cVar3 == '\0') {
      first_page_number = local_40;
      last_page_number = local_38;
      sVar2 = 1;
      goto LAB_0010358a;
    }
  }
LAB_00103588:
  sVar2 = 0;
LAB_0010358a:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return sVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}