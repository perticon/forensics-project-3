void add_line_number(int32_t * p) {
    int64_t v1 = function_28d0(); // 0x3791
    line_number++;
    int64_t v2 = number_buff + (int64_t)((int32_t)v1 - chars_per_number); // 0x37af
    int64_t v3 = v2; // 0x37bb
    if (chars_per_number >= 1) {
        v3++;
        while (v2 + (int64_t)chars_per_number != v3) {
            // 0x37c0
            v3++;
        }
    }
    // 0x37d0
    if (columns < 2) {
        if (number_separator == 9) {
            int32_t v4 = output_position; // 0x3854
            uint32_t v5 = chars_per_output_tab; // 0x385a
            output_position = v5 + v4 - (int32_t)((0x100000000 * (int64_t)(v4 >> 31) | (int64_t)v4) % (int64_t)v5);
        }
    } else {
        if (number_separator == 9) {
            uint32_t v6 = number_width - chars_per_number; // 0x3816
            if (v6 >= 1) {
                int64_t v7 = v6 - 1;
                while (v7 != 0) {
                    // 0x3835
                    if (v7 == 1) {
                        // break -> 0x37e9
                        break;
                    }
                    v7 = v7 + 0xfffffffe & 0xffffffff;
                }
            }
        }
    }
    // 0x37e9
    if (*(char *)&truncate_lines == 0) {
        // 0x3807
        return;
    }
    // 0x37f2
    if (*(char *)&parallel_files == 0) {
        // 0x37fb
        input_position += number_width;
    }
}