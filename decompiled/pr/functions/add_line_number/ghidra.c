void add_line_number(long param_1)

{
  int iVar1;
  long lVar2;
  long lVar3;
  bool bVar4;
  
  iVar1 = __sprintf_chk(number_buff,1,0xffffffffffffffff,&DAT_0010e024,chars_per_number,line_number)
  ;
  line_number = line_number + 1;
  lVar2 = (iVar1 - chars_per_number) + number_buff;
  lVar3 = chars_per_number + lVar2;
  if (0 < chars_per_number) {
    do {
      lVar2 = lVar2 + 1;
      (**(code **)(param_1 + 0x20))();
    } while (lVar3 != lVar2);
  }
  if (columns < 2) {
    (**(code **)(param_1 + 0x20))();
    if (number_separator == '\t') {
      output_position =
           (chars_per_output_tab - output_position % chars_per_output_tab) + output_position;
    }
  }
  else if (number_separator == '\t') {
    iVar1 = number_width - chars_per_number;
    if (0 < iVar1) {
      do {
        (**(code **)(param_1 + 0x20))(0x20);
        bVar4 = iVar1 == 1;
        iVar1 = iVar1 + -2;
        if (bVar4) break;
        (**(code **)(param_1 + 0x20))(0x20);
      } while (iVar1 != 0);
    }
  }
  else {
    (**(code **)(param_1 + 0x20))();
  }
  if ((truncate_lines != '\0') && (parallel_files == '\0')) {
    input_position = input_position + number_width;
  }
  return;
}