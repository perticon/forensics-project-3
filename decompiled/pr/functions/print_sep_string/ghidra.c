void print_sep_string(void)

{
  byte bVar1;
  byte *pbVar2;
  byte *pbVar3;
  byte *pbVar4;
  int iVar5;
  
  if (separators_not_printed < 1) {
    if (spaces_not_printed < 1) {
      return;
    }
    print_white_space();
    return;
  }
  pbVar4 = col_sep_string + col_sep_length;
  pbVar2 = col_sep_string;
  pbVar3 = col_sep_string;
  iVar5 = col_sep_length;
  if (0 < col_sep_length) goto LAB_0010404b;
  do {
    iVar5 = iVar5 + -1;
    while( true ) {
      if (0 < spaces_not_printed) {
        print_white_space();
      }
      separators_not_printed = separators_not_printed + -1;
      if (separators_not_printed < 1) {
        return;
      }
      pbVar4 = pbVar3 + iVar5;
      pbVar2 = pbVar3;
      if (iVar5 < 1) break;
LAB_0010404b:
      do {
        while( true ) {
          bVar1 = *pbVar2;
          pbVar3 = pbVar2 + 1;
          if (bVar1 != 0x20) break;
          spaces_not_printed = spaces_not_printed + 1;
          pbVar2 = pbVar3;
          if (pbVar4 == pbVar3) goto LAB_00104066;
        }
        if (0 < spaces_not_printed) {
          print_white_space();
          bVar1 = *pbVar2;
        }
        pbVar2 = (byte *)stdout->_IO_write_ptr;
        if (pbVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
          *pbVar2 = bVar1;
        }
        else {
          __overflow(stdout,(uint)bVar1);
        }
        output_position = output_position + 1;
        pbVar2 = pbVar3;
      } while (pbVar4 != pbVar3);
LAB_00104066:
      iVar5 = -1;
    }
  } while( true );
}