void print_sep_string(void) {
    int32_t v1 = spaces_not_printed; // 0x3fd6
    if (separators_not_printed < 1) {
        if (v1 >= 0 != v1 != 0) {
            // 0x40b3
            return;
        }
        // 0x40b8
        print_white_space();
        return;
    }
    int64_t v2 = v1; // 0x3fd6
    int32_t v3 = col_sep_length; // 0x3fde
    int64_t v4 = (int64_t)col_sep_string; // 0x3fe7
    int64_t v5 = v2; // 0x4004
    int64_t v6 = v4; // 0x4004
    int64_t v7 = v4 + (int64_t)v3; // 0x4004
    int64_t v8 = v2; // 0x4004
    int64_t v9 = v3 - 1; // 0x4004
    int64_t v10 = v4; // 0x4004
    if (v3 >= 0 == (v3 != 0)) {
        goto lab_0x404b;
    } else {
        goto lab_0x409e;
    }
  lab_0x404b:;
    int64_t v11 = v5; // 0x3fd0
    int64_t v12 = v6; // 0x3fd0
    int64_t v13; // 0x3fd0
    int64_t v14; // 0x3fd0
    while (true) {
        int64_t v15 = v12 + 1; // 0x404e
        int64_t v16; // 0x3fd0
        if (*(char *)v12 != 32) {
            // 0x4010
            char v17; // 0x404b
            char v18 = v17; // 0x4012
            int64_t v19; // 0x3fd0
            if ((int32_t)v19 >= 1) {
                // 0x4014
                print_white_space();
                char * v20; // 0x404b
                char v21 = *v20; // 0x4019
                v18 = v21;
            }
            struct _IO_FILE * v22 = g12; // 0x401d
            int64_t v23 = (int64_t)v22; // 0x401d
            int64_t * v24 = (int64_t *)(v23 + 40); // 0x4024
            uint64_t v25 = *v24; // 0x4024
            uint64_t v26 = *(int64_t *)(v23 + 48); // 0x4028
            if (v25 >= v26) {
                // 0x40a8
                function_2640();
            } else {
                char v27 = v18;
                *v24 = v25 + 1;
                *(char *)v25 = v27;
            }
            int32_t v28 = output_position; // 0x4039
            output_position = v28 + 1;
            uint32_t v29 = spaces_not_printed; // 0x4040
            int64_t v30 = v29; // 0x4040
            v16 = v30;
            int64_t v31; // 0x3fd0
            v13 = v31;
            v14 = v30;
            if (v31 == v15) {
                // break -> 0x4066
                break;
            }
        } else {
            int64_t v32 = v11 + 1; // 0x4058
            int64_t v33 = v32 & 0xffffffff; // 0x4058
            spaces_not_printed = v32;
            v16 = v33;
            v13 = v15;
            v14 = v33;
            if (v7 == v15) {
                // break -> 0x4066
                break;
            }
        }
        // 0x404b
        v11 = v16;
        v12 = v15;
    }
    int64_t v34 = v14; // 0x4066
    int64_t v35 = v13; // 0x4066
    int64_t v36 = 0xffffffff; // 0x4066
    goto lab_0x406c;
  lab_0x409e:
    // 0x409e
    v34 = v8;
    v35 = v10;
    v36 = v9;
    goto lab_0x406c;
  lab_0x406c:
    // 0x406c
    if ((int32_t)v34 >= 1) {
        // 0x4070
        print_white_space();
    }
    int32_t v37 = separators_not_printed - 1; // 0x407b
    separators_not_printed = v37;
    if (v37 < 1) {
        // 0x40b3
        return;
    }
    int64_t v38 = spaces_not_printed; // 0x408b
    int32_t v39 = v36; // 0x4099
    v5 = v38;
    v6 = v35;
    v7 = (0x100000000 * v36 >> 32) + v35;
    v8 = v38;
    v9 = v36 + 0xffffffff & 0xffffffff;
    v10 = v35;
    if (v39 >= 0 == (v39 != 0)) {
        goto lab_0x404b;
    } else {
        goto lab_0x409e;
    }
}