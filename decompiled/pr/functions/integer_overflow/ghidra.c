size_t integer_overflow(void)

{
  uint uVar1;
  undefined8 uVar2;
  size_t sVar3;
  char cVar4;
  char *extraout_RDX;
  undefined4 *extraout_RDX_00;
  char cVar5;
  ulong uVar6;
  char *pcVar7;
  long in_FS_OFFSET;
  char *pcStack80;
  ulong uStack72;
  ulong uStack64;
  long lStack56;
  
  uVar2 = dcgettext(0,"integer overflow",5);
  cVar5 = '\0';
  uVar6 = 1;
  error(1,0,uVar2);
  lStack56 = *(long *)(in_FS_OFFSET + 0x28);
  uStack64 = 0xffffffffffffffff;
  uVar1 = xstrtoumax(extraout_RDX,&pcStack80,10,&uStack72,&DAT_0010e06b);
  if ((uVar1 & 0xfffffffd) != 0) {
LAB_001035f6:
    pcVar7 = (char *)(ulong)uVar1;
    xstrtol_fatal(pcVar7,uVar6 & 0xffffffff,(int)cVar5,long_options,extraout_RDX);
    sVar3 = strlen(pcVar7);
    if (sVar3 < 0x80000000) {
      col_sep_string = pcVar7;
      col_sep_length = (int)sVar3;
      return sVar3;
    }
    integer_overflow();
    sVar3 = xdectoimax();
    *extraout_RDX_00 = (int)sVar3;
    return sVar3;
  }
  if ((pcStack80 != extraout_RDX) && (uStack72 != 0)) {
    cVar4 = *pcStack80;
    if (cVar4 == ':') {
      pcVar7 = pcStack80 + 1;
      uVar1 = xstrtoumax(pcVar7,&pcStack80,10,&uStack64,&DAT_0010e06b);
      if (uVar1 != 0) goto LAB_001035f6;
      if ((pcStack80 == pcVar7) || (uStack64 < uStack72)) goto LAB_00103588;
      cVar4 = *pcStack80;
    }
    if (cVar4 == '\0') {
      first_page_number = uStack72;
      last_page_number = uStack64;
      sVar3 = 1;
      goto LAB_0010358a;
    }
  }
LAB_00103588:
  sVar3 = 0;
LAB_0010358a:
  if (lStack56 == *(long *)(in_FS_OFFSET + 0x28)) {
    return sVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}