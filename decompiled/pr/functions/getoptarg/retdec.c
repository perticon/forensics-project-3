void getoptarg(char * arg, char switch_char, char * character, int32_t * number) {
    int64_t v1 = (int64_t)arg;
    char v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x5f86
    char v4 = v2; // 0x5f9e
    int64_t v5 = v1; // 0x5f9e
    if (v2 != 57 && (int32_t)v2 >= 57) {
        // 0x5fa0
        *character = v2;
        v5 = v1 + 1;
        v4 = *(char *)v5;
    }
    // 0x5faa
    char * v6; // 0x5f70
    if (v4 != 0) {
        char * v7 = (char *)v5;
        int64_t v8; // bp-56, 0x5f70
        if (xstrtol(v7, NULL, 10, &v8, (char *)&g35) != 0) {
            // 0x6009
            quote(v7);
            function_25d0();
            goto lab_0x6036;
        } else {
            // 0x5ff0
            if (v8 < 1) {
                // 0x6080
                quote(v7);
            } else {
                // 0x5ffd
                v6 = v7;
                if (v8 > 0x7fffffff) {
                    goto lab_0x6057;
                } else {
                    // 0x6005
                    *number = (int32_t)v8;
                    goto lab_0x5fae;
                }
            }
        }
    } else {
        goto lab_0x5fae;
    }
  lab_0x5fae:
    // 0x5fae
    if (v3 == __readfsqword(40)) {
        // 0x5fc2
        return;
    }
    // 0x6052
    function_2600();
    v6 = (char *)v5;
    goto lab_0x6057;
  lab_0x6036:
    // 0x6036
    function_27f0();
    usage(1);
    // 0x6052
    function_2600();
    v6 = (char *)v5;
    goto lab_0x6057;
  lab_0x6057:
    // 0x6057
    quote(v6);
    function_25d0();
    goto lab_0x6036;
}