void getoptarg(char *param_1,char param_2,char *param_3,undefined4 *param_4)

{
  char cVar1;
  uint uVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  long local_38;
  long local_30;
  
  cVar1 = *param_1;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (9 < (int)cVar1 - 0x30U) {
    *param_3 = cVar1;
    cVar1 = param_1[1];
    param_1 = param_1 + 1;
  }
  if (cVar1 == '\0') {
LAB_00105fae:
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
  }
  else {
    uVar2 = xstrtol(param_1,0,10,&local_38,&DAT_0010e06b);
    if (uVar2 == 0) {
      if (local_38 < 1) {
        uVar3 = quote(param_1);
        uVar4 = dcgettext(0,"\'-%c\' extra characters or invalid number in the argument: %s",5);
        uVar2 = 0;
      }
      else {
        if (local_38 < 0x80000000) {
          *param_4 = (int)local_38;
          goto LAB_00105fae;
        }
        uVar3 = quote(param_1);
        uVar4 = dcgettext(0,"\'-%c\' extra characters or invalid number in the argument: %s",5);
        uVar2 = 0x4b;
      }
    }
    else {
      uVar3 = quote(param_1);
      uVar4 = dcgettext(0,"\'-%c\' extra characters or invalid number in the argument: %s",5);
      uVar2 = -(uVar2 & 1) & 0x4b;
    }
    error(0,uVar2,uVar4,(int)param_2,uVar3);
    usage(1);
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}