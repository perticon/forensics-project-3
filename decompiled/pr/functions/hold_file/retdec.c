void hold_file(int32_t * p) {
    int64_t v1 = (int64_t)p;
    if (*(char *)&parallel_files != 0) {
        // 0x33d0
        files_ready_to_read--;
        *(int32_t *)(v1 + 16) = 2;
        *(int32_t *)(v1 + 48) = 0;
        return;
    }
    // 0x3389
    if (columns == 0) {
        // 0x33bc
        files_ready_to_read--;
        *(int32_t *)(v1 + 48) = 0;
        return;
    }
    int64_t v2 = (int64_t)column_vector; // 0x338f
    int64_t v3 = v2; // 0x33af
    *(int32_t *)(v3 + 16) = 2 - (int32_t)*(char *)&storing_columns;
    v3 += 64;
    while (v3 != 64 * (int64_t)columns + v2) {
        // 0x33b0
        *(int32_t *)(v3 + 16) = 2 - (int32_t)*(char *)&storing_columns;
        v3 += 64;
    }
    // 0x33bc
    files_ready_to_read--;
    *(int32_t *)(v1 + 48) = 0;
}