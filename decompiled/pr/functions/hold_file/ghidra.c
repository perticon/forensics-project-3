void hold_file(long param_1)

{
  long lVar1;
  long lVar2;
  uint uVar3;
  
  if (parallel_files == '\0') {
    if (columns != 0) {
      uVar3 = (uint)storing_columns;
      lVar2 = (ulong)columns * 0x40 + column_vector;
      lVar1 = column_vector;
      do {
        *(uint *)(lVar1 + 0x10) = 2 - uVar3;
        lVar1 = lVar1 + 0x40;
      } while (lVar1 != lVar2);
    }
    files_ready_to_read = files_ready_to_read + -1;
    *(undefined4 *)(param_1 + 0x30) = 0;
    return;
  }
  files_ready_to_read = files_ready_to_read + -1;
  *(undefined4 *)(param_1 + 0x10) = 2;
  *(undefined4 *)(param_1 + 0x30) = 0;
  return;
}