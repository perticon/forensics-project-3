undefined8 read_line(_IO_FILE **param_1)

{
  byte *pbVar1;
  int iVar2;
  uint uVar3;
  undefined8 uVar4;
  char *pcVar5;
  char *pcVar6;
  _IO_FILE *p_Var7;
  uint unaff_R12D;
  int iVar8;
  char *pcVar9;
  
  p_Var7 = *param_1;
  pbVar1 = (byte *)p_Var7->_IO_read_ptr;
  if (pbVar1 < p_Var7->_IO_read_end) {
    p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
    uVar3 = (uint)*pbVar1;
  }
  else {
    uVar3 = __uflow(p_Var7);
  }
  iVar2 = input_position;
  if (uVar3 == 0xc) {
    p_Var7 = *param_1;
    pbVar1 = (byte *)p_Var7->_IO_read_ptr;
    if (*(char *)((long)param_1 + 0x39) == '\0') {
LAB_00104700:
      pbVar1 = (byte *)p_Var7->_IO_read_ptr;
      if (pbVar1 < p_Var7->_IO_read_end) {
        p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar3 = (uint)*pbVar1;
      }
      else {
        uVar3 = __uflow(p_Var7);
      }
      if (uVar3 != 10) {
        ungetc(uVar3,*param_1);
      }
      FF_only = 1;
      if ((print_a_header == '\0') || (FF_only = 1, storing_columns != '\0')) {
LAB_0010473b:
        if (keep_FF != '\0') {
          print_a_FF = 1;
        }
      }
      else {
        pad_vertically = 1;
        print_header();
      }
      hold_file(param_1);
      return 1;
    }
    if (pbVar1 < p_Var7->_IO_read_end) {
      p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
      uVar3 = (uint)*pbVar1;
    }
    else {
      uVar3 = __uflow(p_Var7);
    }
    if (uVar3 == 10) {
      p_Var7 = *param_1;
      pbVar1 = (byte *)p_Var7->_IO_read_ptr;
      if (pbVar1 < p_Var7->_IO_read_end) {
        p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar3 = (uint)*pbVar1;
      }
      else {
        uVar3 = __uflow(p_Var7);
      }
      *(undefined *)((long)param_1 + 0x39) = 0;
      if (uVar3 == 10) goto LAB_001044ee;
    }
    else {
      *(undefined *)((long)param_1 + 0x39) = 0;
    }
    if (uVar3 == 0xc) {
      p_Var7 = *param_1;
      goto LAB_00104700;
    }
  }
  else {
    *(undefined *)((long)param_1 + 0x39) = 0;
    if (uVar3 == 10) goto LAB_001044ee;
  }
  if (uVar3 == 0xffffffff) {
LAB_001045c0:
    close_file(param_1);
    return 1;
  }
  unaff_R12D = char_to_clump();
LAB_001044ee:
  if ((truncate_lines == '\0') || (input_position <= chars_per_column)) {
    if (param_1[4] != (_IO_FILE *)store_char) {
      pad_vertically = 1;
      if ((print_a_header != '\0') && (storing_columns == '\0')) {
        print_header();
      }
      iVar2 = separators_not_printed;
      if ((parallel_files != '\0') && (align_empty_cols != '\0')) {
        separators_not_printed = 0;
        iVar8 = 1;
        if (0 < iVar2) {
          do {
            iVar8 = iVar8 + 1;
            align_column();
            separators_not_printed = separators_not_printed + 1;
          } while (iVar8 <= iVar2);
        }
        align_empty_cols = '\0';
        padding_not_printed = *(int *)((long)param_1 + 0x34);
        spaces_not_printed = chars_per_column;
        if (truncate_lines == '\0') {
          spaces_not_printed = 0;
        }
      }
      if (col_sep_length < padding_not_printed) {
        pad_across_to(padding_not_printed - col_sep_length);
        padding_not_printed = 0;
      }
      if (use_col_separator != '\0') {
        print_sep_string();
      }
    }
    if (*(char *)(param_1 + 7) != '\0') {
      add_line_number(param_1);
    }
    empty_line = 0;
    uVar4 = 1;
    if (uVar3 != 10) {
      pcVar9 = clump_buff + unaff_R12D;
      pcVar6 = clump_buff;
      if (unaff_R12D != 0) {
        do {
          pcVar5 = pcVar6 + 1;
          (*(code *)param_1[4])((int)*pcVar6);
          pcVar6 = pcVar5;
        } while (pcVar9 != pcVar5);
      }
      while( true ) {
        p_Var7 = *param_1;
        pbVar1 = (byte *)p_Var7->_IO_read_ptr;
        if (pbVar1 < p_Var7->_IO_read_end) {
          p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
          uVar3 = (uint)*pbVar1;
        }
        else {
          uVar3 = __uflow(p_Var7);
        }
        iVar2 = input_position;
        if (uVar3 == 10) {
          return 1;
        }
        if (uVar3 == 0xc) {
          p_Var7 = *param_1;
          pbVar1 = (byte *)p_Var7->_IO_read_ptr;
          if (pbVar1 < p_Var7->_IO_read_end) {
            p_Var7->_IO_read_ptr = (char *)(pbVar1 + 1);
            uVar3 = (uint)*pbVar1;
          }
          else {
            uVar3 = __uflow(p_Var7);
          }
          if (uVar3 != 10) {
            ungetc(uVar3,*param_1);
          }
          goto LAB_0010473b;
        }
        if (uVar3 == 0xffffffff) goto LAB_001045c0;
        uVar3 = char_to_clump((int)(char)uVar3);
        if ((truncate_lines != '\0') && (chars_per_column < input_position)) break;
        pcVar9 = clump_buff + uVar3;
        pcVar6 = clump_buff;
        if (uVar3 != 0) {
          do {
            pcVar5 = pcVar6 + 1;
            (*(code *)param_1[4])((int)*pcVar6);
            pcVar6 = pcVar5;
          } while (pcVar5 != pcVar9);
        }
      }
      uVar4 = 0;
      input_position = iVar2;
    }
  }
  else {
    uVar4 = 0;
    input_position = iVar2;
  }
  return uVar4;
}