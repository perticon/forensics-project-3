void align_column(int32_t * p) {
    int64_t v1 = (int64_t)p;
    uint32_t v2 = *(int32_t *)(v1 + 52); // 0x40da
    padding_not_printed = v2;
    if (v2 > col_sep_length) {
        // 0x4100
        pad_across_to(v2 - col_sep_length);
        padding_not_printed = 0;
        if (*(char *)&use_col_separator == 0) {
            goto lab_0x40f0;
        } else {
            goto lab_0x411a;
        }
    } else {
        // 0x40e7
        if (*(char *)&use_col_separator != 0) {
            goto lab_0x411a;
        } else {
            goto lab_0x40f0;
        }
    }
  lab_0x40f0:
    // 0x40f0
    if (*(char *)(v1 + 56) != 0) {
        // 0x4125
        add_line_number(p);
        return;
    }
  lab_0x411a:
    // 0x411a
    print_sep_string();
    if (*(char *)(v1 + 56) == 0) {
        // 0x40f6
        return;
    }
    // 0x4125
    add_line_number(p);
    return;
}