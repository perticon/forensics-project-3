void align_column(long param_1)

{
  char cVar1;
  
  padding_not_printed = *(int *)(param_1 + 0x34);
  if (col_sep_length < padding_not_printed) {
    pad_across_to(padding_not_printed - col_sep_length);
    padding_not_printed = 0;
    if (use_col_separator != '\0') goto LAB_0010411a;
  }
  else if (use_col_separator != '\0') {
LAB_0010411a:
    print_sep_string();
    cVar1 = *(char *)(param_1 + 0x38);
    goto joined_r0x00104123;
  }
  cVar1 = *(char *)(param_1 + 0x38);
joined_r0x00104123:
  if (cVar1 == '\0') {
    return;
  }
  add_line_number(param_1);
  return;
}