void separator_string(char * optarg_S) {
    uint64_t v1 = function_25f0(); // 0x3614
    if (v1 >= 0x80000000) {
        // 0x3630
        integer_overflow();
        return;
    }
    // 0x3621
    *(int64_t *)&col_sep_string = (int64_t)optarg_S;
    col_sep_length = v1;
}