separator_string (char const *optarg_S)
{
  size_t len = strlen (optarg_S);
  if (INT_MAX < len)
    integer_overflow ();
  col_sep_length = len;
  col_sep_string = optarg_S;
}