void separator_string(char *param_1)

{
  undefined4 uVar1;
  size_t sVar2;
  undefined4 *extraout_RDX;
  
  sVar2 = strlen(param_1);
  if (sVar2 < 0x80000000) {
    col_sep_string = param_1;
    col_sep_length = (int)sVar2;
    return;
  }
  integer_overflow();
  uVar1 = xdectoimax();
  *extraout_RDX = uVar1;
  return;
}