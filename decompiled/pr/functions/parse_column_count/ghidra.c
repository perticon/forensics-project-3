void parse_column_count(undefined8 param_1)

{
  undefined8 uVar1;
  
  uVar1 = dcgettext(0,"invalid number of columns",5);
  columns = xdectoimax(param_1,1,0x7fffffff,&DAT_0010e06b,uVar1,0);
  explicit_columns = 1;
  return;
}