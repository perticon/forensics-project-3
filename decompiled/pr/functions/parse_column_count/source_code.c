parse_column_count (char const *s)
{
  getoptnum (s, 1, &columns, _("invalid number of columns"));
  explicit_columns = true;
}