int64_t print_files(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6) {
    int32_t v1 = lines_per_page - 10; // 0x4b3a
    lines_per_body = v1;
    int32_t v2; // 0x4b20
    if (v1 < 1) {
        // 0x50a6
        *(char *)&extremities = 0;
        *(char *)&keep_FF = 1;
        // 0x50b4
        lines_per_body = lines_per_page;
        v2 = lines_per_page;
        goto lab_0x4b58;
    } else {
        // 0x4b4b
        v2 = v1;
        if (*(char *)&extremities == 0) {
            // 0x50b4
            lines_per_body = lines_per_page;
            v2 = lines_per_page;
            goto lab_0x4b58;
        } else {
            goto lab_0x4b58;
        }
    }
  lab_0x5310:;
    // 0x5310
    int32_t v3; // 0x4b20
    int32_t v4 = v3 - 1; // 0x5310
    int64_t v5; // 0x4b20
    int64_t v6 = v5; // 0x5314
    int64_t v7; // 0x4b20
    int64_t v8 = v7; // 0x5314
    int32_t v9 = v4; // 0x5314
    int64_t v10 = v5; // 0x5314
    int64_t v11 = v7; // 0x5314
    if (v4 == 0) {
        // break -> 0x5380
        goto lab_0x5380_2;
    }
    goto lab_0x5316;
  lab_0x5316:;
    int64_t v12 = v6; // 0x531e
    int64_t v13 = v8; // 0x531e
    int32_t v14 = v9; // 0x531e
    v10 = v6;
    v11 = v8;
    if (files_ready_to_read == 0) {
        // break -> 0x5380
        goto lab_0x5380_2;
    }
    goto lab_0x5320;
  lab_0x51c2:;
    // 0x51c2
    int64_t v24; // 0x4b20
    int64_t v25 = v24;
    int64_t v26; // 0x4b20
    int64_t v27 = v26;
    int64_t v28 = v27; // 0x51c5
    int64_t v29 = v25; // 0x51c5
    int32_t v30; // 0x4b20
    int64_t v31; // 0x4b20
    int32_t v32; // 0x4b20
    if (v30 != 0) {
        goto lab_0x51f7;
    } else {
        // 0x51c7
        v32 = columns;
        v31 = v27;
        v28 = v27;
        v29 = v25;
        if (columns < 1) {
            goto lab_0x51f7;
        } else {
            goto lab_0x51d9;
        }
    }
  lab_0x4fe0:;
    // 0x4fe0
    int64_t v33; // 0x4b20
    int32_t v34 = *(int32_t *)(v33 + 52); // 0x4fe0
    *(char *)&FF_only = 0;
    padding_not_printed = v34;
    if ((char)v34 == 0) {
        // 0x5408
        read_rest_of_line((int32_t *)v33);
    }
    unsigned char v35 = *(char *)&pad_vertically; // 0x5001
    int32_t * v36; // 0x506b
    int32_t v37 = *v36 - 1; // 0x5009
    int64_t v38; // 0x4b20
    int64_t v39 = v38 & 0xffffffff | (int64_t)v35; // 0x500c
    *v36 = v37;
    if (v37 < 1) {
        // 0x5140
        if (cols_ready_to_print() == 0) {
            // break -> 0x514d
            goto lab_0x514d;
        }
    }
    int64_t v40 = v39; // 0x5021
    int64_t v41; // 0x4b20
    int64_t v42; // 0x4b20
    if (*(char *)&parallel_files == 0) {
        goto lab_0x5048;
    } else {
        int32_t v43 = *(int32_t *)(v33 + 16); // 0x5023
        v40 = v39;
        if (v43 == 0) {
            goto lab_0x5048;
        } else {
            // 0x502a
            v42 = v39;
            if (*(char *)&empty_line != 0) {
                goto lab_0x509d;
            } else {
                // 0x5033
                v41 = v39;
                if (v43 == 3) {
                    goto lab_0x5428;
                } else {
                    // 0x503c
                    v41 = v39;
                    if (v43 != 2 | *(char *)&FF_only == 0) {
                        goto lab_0x5048;
                    } else {
                        goto lab_0x5428;
                    }
                }
            }
        }
    }
  lab_0x5048:;
    int64_t v44 = v40;
    if (*(char *)&use_col_separator != 0) {
        // 0x5051
        separators_not_printed++;
    }
    // 0x5058
    int64_t v45; // 0x4b20
    int64_t v46 = v45 + 1; // 0x5058
    int64_t v47 = v33 + 64; // 0x5065
    int64_t v48 = v46 & 0xffffffff; // 0x5065
    int64_t v49 = v44; // 0x5065
    int64_t v50; // 0x4b20
    int64_t v51; // 0x4b20
    int64_t v52; // 0x4b20
    int64_t v53; // 0x4b20
    if (columns < (int32_t)v46) {
        // 0x5438
        v50 = v44;
        v51 = v44;
        v53 = v52;
        if (*(char *)&pad_vertically != 0) {
            goto lab_0x5156;
        } else {
            goto lab_0x517a;
        }
    }
    goto lab_0x506b;
  lab_0x509d:
    // 0x509d
    *(char *)&align_empty_cols = 1;
    v40 = v42;
    goto lab_0x5048;
  lab_0x5428:
    // 0x5428
    align_column((int32_t *)v33);
    v40 = v41;
    goto lab_0x5048;
  lab_0x4f41:
    // 0x4f41
    if (cols_ready_to_print() == 0) {
        // 0x5107
        return 0;
    }
    // 0x4f4e
    if (*(char *)&extremities != 0) {
        // 0x4f57
        *(char *)&print_a_header = 1;
    }
    // 0x4f5e
    *(char *)&pad_vertically = 0;
    int64_t v55 = lines_per_body; // 0x4f6c
    int64_t v56 = *(char *)&double_space == 0 ? v55 : 2 * v55 & 0xfffffffe;
    int32_t v57 = v56; // 0x4f7b
    int64_t v58 = 0; // 0x4f7e
    int64_t v59 = v56; // 0x4f7e
    int64_t v60; // 0x4b20
    int64_t v61; // 0x4b20
    if (v57 < 1) {
        if (v57 != 0) {
            goto lab_0x5210;
        } else {
            int32_t v62 = columns; // 0x58ec
            if (v62 >= 0 == (v62 != 0)) {
                goto lab_0x51d9;
            } else {
                goto lab_0x5210;
            }
        }
    } else {
        while (true) {
          lab_0x4f88:
            // 0x4f88
            v52 = v59;
            int64_t v63 = v58;
            v28 = v63;
            v29 = v52;
            if (cols_ready_to_print() == 0) {
                // break -> 0x51f7
                break;
            }
            // 0x4f95
            output_position = 0;
            spaces_not_printed = 0;
            separators_not_printed = 0;
            *(char *)&pad_vertically = 0;
            *(char *)&align_empty_cols = 0;
            *(char *)&empty_line = 1;
            v51 = v63;
            v53 = v52;
            if (columns < 1) {
                goto lab_0x517a;
            } else {
                // 0x4fd4
                v47 = (int64_t)column_vector;
                v48 = 1;
                v49 = v63;
                while (true) {
                  lab_0x506b:
                    // 0x506b
                    v38 = v49;
                    v45 = v48;
                    v33 = v47;
                    v36 = (int32_t *)(v33 + 48);
                    int32_t v54 = *v36; // 0x506b
                    input_position = 0;
                    if (v54 >= 0 == (v54 != 0)) {
                        goto lab_0x4fe0;
                    } else {
                        // 0x507d
                        if (*(int32_t *)(v33 + 16) == 1) {
                            goto lab_0x4fe0;
                        } else {
                            // 0x5087
                            v40 = v38;
                            if (*(char *)&parallel_files == 0) {
                                goto lab_0x5048;
                            } else {
                                // 0x5090
                                v42 = v38;
                                v41 = v38;
                                if (*(char *)&empty_line == 0) {
                                    goto lab_0x5428;
                                } else {
                                    goto lab_0x509d;
                                }
                            }
                        }
                    }
                }
              lab_0x514d:
                // 0x514d
                v50 = v39;
                v60 = v39;
                v61 = v52;
                if (v35 == 0) {
                    goto lab_0x5450_2;
                } else {
                    goto lab_0x5156;
                }
            }
        }
        goto lab_0x51f7;
    }
  lab_0x517a:;
    int64_t v64 = v51; // 0x5181
    int64_t v65 = v53; // 0x5181
    v60 = v51;
    v61 = v53;
    if (cols_ready_to_print() == 0) {
        goto lab_0x5450_2;
    } else {
        goto lab_0x5187;
    }
  lab_0x5450_2:;
    int64_t v66 = v61;
    int64_t v67 = v60;
    v64 = v67;
    v65 = v66;
    if (*(char *)&extremities == 0) {
        // 0x5450
        v30 = v66;
        v26 = v67;
        v24 = v66;
        goto lab_0x51c2;
    }
    goto lab_0x5187;
  lab_0x5187:;
    int64_t v68 = v65;
    int64_t v69 = v68; // 0x518e
    if (!(((char)v64 == 0 | *(char *)&double_space == 0))) {
        int64_t v70 = (int64_t)g12; // 0x5195
        int64_t * v71 = (int64_t *)(v70 + 40); // 0x519c
        uint64_t v72 = *v71; // 0x519c
        if (v72 >= *(int64_t *)(v70 + 48)) {
            // 0x54a0
            function_2640();
        } else {
            // 0x51aa
            *v71 = v72 + 1;
            *(char *)v72 = 10;
        }
        // 0x51b5
        v69 = v68 + 0xffffffff & 0xffffffff;
    }
    int32_t v73 = v69;
    v58 = v64;
    v59 = v69;
    v30 = v73;
    v26 = v64;
    v24 = v69;
    if (v73 >= 0 != v73 != 0) {
        goto lab_0x51c2;
    }
    goto lab_0x4f88;
  lab_0x5156:;
    int64_t v74 = (int64_t)g12; // 0x5156
    int64_t * v75 = (int64_t *)(v74 + 40); // 0x515d
    uint64_t v76 = *v75; // 0x515d
    if (v76 >= *(int64_t *)(v74 + 48)) {
        // 0x5462
        function_2640();
    } else {
        // 0x516b
        *v75 = v76 + 1;
        *(char *)v76 = 10;
    }
    // 0x5176
    v51 = v50;
    v53 = v52 + 0xffffffff & 0xffffffff;
    goto lab_0x517a;
  lab_0x4b58:
    // 0x4b58
    if (*(char *)&double_space != 0) {
        int32_t v77 = v2; // 0x4b61
        lines_per_body = v77 + (int32_t)(v77 < 0) >> 1;
    }
    int64_t v78 = a1 & 0xffffffff; // 0x4b28
    int32_t v79 = a1; // 0x4b76
    char v80; // 0x4b20
    int64_t v81; // 0x4b20
    if (v79 != 0) {
        char v82 = *(char *)&parallel_files; // 0x5116
        if (v82 == 0) {
            // 0x5994
            v81 = columns;
            v80 = 0;
        } else {
            // 0x5127
            columns = v79;
            v81 = v78;
            v80 = v82;
        }
    } else {
        // 0x4b7f
        *(char *)&parallel_files = 0;
        v81 = columns;
        v80 = 0;
    }
    char v83 = v80;
    if (*(char *)&storing_columns != 0) {
        // 0x4b98
        *(char *)&balance_columns = 1;
    }
    char v84 = *(char *)&join_lines; // 0x4b9f
    int64_t v85; // 0x4b20
    int64_t v86; // 0x4b20
    int64_t v87; // 0x4b20
    if ((int32_t)v81 < 2) {
        // 0x5556
        *(char *)&storing_columns = 0;
        v86 = col_sep_length;
        goto lab_0x4c08;
    } else {
        // 0x4baf
        if (*(char *)&use_col_separator != 0) {
            uint32_t v88 = col_sep_length; // 0x50bf
            int64_t v89 = v88; // 0x50bf
            if (v84 != 0) {
                // 0x587e
                *(char *)&truncate_lines = 1;
                v85 = v89;
                if (v88 != 1) {
                    goto lab_0x4bfa;
                } else {
                    // 0x588f
                    v87 = (int64_t)col_sep_string;
                    goto lab_0x4bef;
                }
            } else {
                if (v88 == 1) {
                    int64_t v90 = (int64_t)col_sep_string;
                    int64_t v91 = v90; // 0x592a
                    if ((char)v90 == 9) {
                        // 0x5947
                        *(int64_t *)&col_sep_string = (int64_t)&g33;
                        v91 = &g33;
                    }
                    // 0x592c
                    *(char *)&truncate_lines = 1;
                    v87 = v91;
                    goto lab_0x4bef;
                } else {
                    // 0x50d8
                    *(char *)&truncate_lines = 1;
                    v85 = v89;
                    goto lab_0x4bfa;
                }
            }
        } else {
            // 0x4bbc
            col_sep_length = 1;
            int64_t v92 = v84 != 0 ? (int64_t)&g34 : (int64_t)&g33; // 0x4bd6
            *(char *)&use_col_separator = 1;
            *(char *)&truncate_lines = 1;
            *(int64_t *)&col_sep_string = v92;
            v87 = v92;
            goto lab_0x4bef;
        }
    }
  lab_0x4c08:
    // 0x4c08
    if (v84 != 0) {
        // 0x4c0c
        *(char *)&truncate_lines = 0;
    }
    unsigned char v93 = *(char *)&numbered_lines; // 0x4c13
    int64_t v94; // 0x4b20
    if (v93 == 0) {
        // 0x50e4
        v94 = 0;
        goto lab_0x4c64;
    } else {
        int32_t v95 = chars_per_number; // 0x4c29
        line_count = start_line_num;
        int32_t v96 = v95 + 1; // 0x4c3f
        if (number_separator == 9) {
            uint32_t v97 = (v95 >> 31) / 0x20000000 + v95;
            v96 = v97 + 8 - v97 % 8;
        }
        // 0x4c55
        number_width = v96;
        v94 = v96;
        if (v83 == 0) {
            // 0x50e4
            v94 = 0;
            goto lab_0x4c64;
        } else {
            goto lab_0x4c64;
        }
    }
  lab_0x4bef:
    // 0x4bef
    v85 = 1;
    int64_t v98 = 1; // 0x4bf8
    if (*(char *)v87 == 9) {
        goto lab_0x4c01;
    } else {
        goto lab_0x4bfa;
    }
  lab_0x4c64:;
    int64_t v99 = v93; // 0x4c13
    int64_t v100 = (0x100000000 * v86 >> 32) * (0x100000000 * v81 - 0x100000000 >> 32); // 0x4c67
    int64_t v101 = v81; // 0x4c6b
    int64_t v102 = a2; // 0x4c6b
    int64_t v103 = v99; // 0x4c6b
    int64_t v104 = v94; // 0x4c6b
    int64_t v105 = v78; // 0x4c6b
    int64_t v106 = v100 & 0xffffffff; // 0x4c6b
    int64_t v107 = v81; // 0x4c6b
    int64_t v108 = a2; // 0x4c6b
    int64_t v109 = v99; // 0x4c6b
    int64_t v110 = v94; // 0x4c6b
    int64_t v111 = v78; // 0x4c6b
    if (v100 > 0x7fffffff) {
        goto lab_0x598a;
    } else {
        goto lab_0x4c71;
    }
  lab_0x4bfa:
    // 0x4bfa
    *(char *)&untabify_input = 1;
    v98 = v85;
    goto lab_0x4c01;
  lab_0x4c01:
    // 0x4c01
    *(char *)&tabify_output = 1;
    v86 = v98;
    goto lab_0x4c08;
  lab_0x598a:
    // 0x598a
    v101 = v107;
    v102 = v108;
    v103 = v109;
    v104 = v110;
    v105 = v111;
    v106 = 0x7fffffff;
    goto lab_0x4c71;
  lab_0x4c71:;
    int64_t v112 = v105;
    int64_t v113 = v102;
    int64_t v114 = v101;
    int32_t v115 = chars_per_line - (int32_t)v104; // 0x4c77
    int32_t v116 = v106; // 0x4c79
    int32_t v117 = v115 - v116; // 0x4c79
    int32_t v118; // 0x4b20
    int64_t v119; // 0x4b20
    int64_t result; // 0x4b20
    int64_t v120; // 0x4b20
    int64_t v121; // 0x4b20
    int64_t v122; // 0x4b20
    if (((v117 ^ v115) & (v115 ^ v116)) < 0) {
        // 0x5957
        chars_per_column = 0;
        goto lab_0x5961;
    } else {
        int32_t v123 = (0x100000000 * (int64_t)(v117 >> 31) | (int64_t)v117) / (v114 & 0xffffffff); // 0x4c84
        chars_per_column = v123;
        if (v123 < 1) {
            goto lab_0x5961;
        } else {
            // 0x4c92
            if ((char)v103 != 0) {
                // 0x5569
                function_24d0();
                number_buff = xmalloc();
            }
            // 0x4c9b
            function_24d0();
            int64_t v124 = xmalloc(); // 0x4cba
            total_files = 0;
            clump_buff = v124;
            function_24d0();
            char * v125 = xnmalloc((int64_t)columns, 64); // 0x4ce8
            int64_t v126 = (int64_t)v125; // 0x4ce8
            *(int64_t *)&column_vector = v126;
            if (*(char *)&parallel_files == 0) {
                int64_t * v127; // 0x4b20
                int64_t v128; // 0x4b20
                if ((int32_t)v112 < 1) {
                    int64_t v129 = function_25d0(); // 0x57bb
                    *(char *)(v126 + 57) = 0;
                    int64_t * v130 = (int64_t *)(v126 + 8);
                    *v130 = v129;
                    *(int32_t *)(v126 + 16) = 0;
                    *(int64_t *)v125 = (int64_t)g13;
                    total_files++;
                    *(char *)&have_read_stdin = 1;
                    init_header((char *)&g35, -1);
                    v127 = v130;
                    v128 = &g35;
                } else {
                    int64_t * v131 = (int64_t *)v113; // 0x50f4
                    bool v132 = open_file((char *)*v131, (int32_t *)v125); // 0x50fa
                    result = v132;
                    if (!v132) {
                        // 0x5107
                        return result;
                    }
                    int64_t v133 = function_2730(); // 0x586a
                    int64_t v134 = *v131; // 0x586f
                    init_header((char *)v134, (int32_t)v133);
                    v127 = (int64_t *)(v126 + 8);
                    v128 = v134;
                }
                int64_t v135 = v128;
                uint32_t v136 = columns; // 0x57f9
                *(int32_t *)(v126 + 44) = 0;
                int64_t v137 = *v127; // 0x580a
                int64_t v138 = *(int64_t *)v125; // 0x580e
                v120 = v137;
                v122 = v138;
                v121 = v135;
                if (v136 != 1) {
                    int64_t v139 = v126 + 64;
                    *(int64_t *)(v139 + 8) = v137;
                    int64_t v140 = v139 + 64; // 0x582d
                    *(int64_t *)v139 = v138;
                    *(int32_t *)(v139 + 16) = 0;
                    *(char *)(v139 + 57) = 0;
                    *(int32_t *)(v139 + 44) = 0;
                    v120 = v137;
                    v122 = v138;
                    v121 = v135;
                    while (v140 != v126 + 128 + (64 * (int64_t)v136 + 0x3fffffff80 & 0x3fffffffc0)) {
                        // 0x5829
                        v139 = v140;
                        *(int64_t *)(v139 + 8) = v137;
                        v140 = v139 + 64;
                        *(int64_t *)v139 = v138;
                        *(int32_t *)(v139 + 16) = 0;
                        *(char *)(v139 + 57) = 0;
                        *(int32_t *)(v139 + 44) = 0;
                        v120 = v137;
                        v122 = v138;
                        v121 = v135;
                    }
                }
                goto lab_0x4d63;
            } else {
                int64_t v141 = v112 & 0xffffffff; // 0x4d04
                int64_t v142 = 8 * v141 + v113; // 0x4d07
                int64_t v143 = v113; // 0x4d0e
                int64_t v144 = v126; // 0x4d0e
                int64_t v145 = v141; // 0x4d0e
                if ((int32_t)v112 != 0) {
                    int64_t v146 = v144;
                    int64_t v147 = *(int64_t *)v143; // 0x4d25
                    bool v148 = open_file((char *)v147, (int32_t *)v146); // 0x4d2b
                    int64_t v149 = v143 + 8;
                    int64_t v150; // 0x4b20
                    int32_t v151; // 0x4d38
                    while (!v148) {
                        // 0x4d34
                        v151 = columns - 1;
                        columns = v151;
                        v150 = v149;
                        if (v149 == v142) {
                            // 0x4d44
                            v118 = v151;
                            v119 = v148;
                            goto lab_0x4d44_2;
                        }
                        v147 = *(int64_t *)v150;
                        v148 = open_file((char *)v147, (int32_t *)v146);
                        v149 = v150 + 8;
                    }
                    // 0x4d18
                    v144 = v146 + 64;
                    while (v149 != v142) {
                        // 0x4d25
                        v146 = v144;
                        v148 = open_file((char *)*(int64_t *)v149, (int32_t *)v146);
                        v149 += 8;
                        while (!v148) {
                            // 0x4d34
                            v151 = columns - 1;
                            columns = v151;
                            v150 = v149;
                            if (v149 == v142) {
                                // 0x4d44
                                v118 = v151;
                                v119 = v148;
                                goto lab_0x4d44_2;
                            }
                            v147 = *(int64_t *)v150;
                            v148 = open_file((char *)v147, (int32_t *)v146);
                            v149 = v150 + 8;
                        }
                        // 0x4d18
                        v144 = v146 + 64;
                    }
                    // 0x4d44
                    v145 = v148;
                }
                // 0x4d44
                v118 = columns;
                v119 = v145;
                goto lab_0x4d44_2;
            }
        }
    }
  lab_0x5961:
    // 0x5961
    function_25d0();
    function_27f0();
    int64_t v152 = v114; // 0x5980
    int64_t v153 = v113; // 0x5980
    int64_t v154 = (int32_t)"page width too narrow" ^ (int32_t)"page width too narrow"; // 0x5980
    int64_t v155 = 1; // 0x5980
    int64_t v156 = v112; // 0x5980
    goto lab_0x5985;
  lab_0x5985:
    // 0x5985
    integer_overflow();
    v107 = v152;
    v108 = v153;
    v109 = v154;
    v110 = v155;
    v111 = v156;
    goto lab_0x598a;
  lab_0x4d63:
    // 0x4d63
    files_ready_to_read = total_files;
    if (*(char *)&storing_columns != 0) {
        int64_t v157 = v121;
        int64_t v158 = v122;
        int64_t v159 = v120;
        int64_t v160 = (int64_t)columns * (int64_t)lines_per_body; // 0x56f7
        int64_t v161 = v160 & 0xffffffff; // 0x56f7
        v152 = v159;
        v153 = v161;
        v154 = v158;
        v155 = v157;
        v156 = v112;
        if (v160 > 0x7fffffff) {
            goto lab_0x5985;
        } else {
            int32_t v162 = v160; // 0x5704
            int32_t v163 = v162 + 1; // 0x5707
            int64_t v164 = v163; // 0x5707
            v152 = v159;
            v153 = v161;
            v154 = v158;
            v155 = v157;
            v156 = v164;
            if ((v163 & (v162 ^ -0x80000000)) < 0) {
                goto lab_0x5985;
            } else {
                int32_t v165 = chars_per_column + 1; // 0x5717
                v152 = v159;
                v153 = v161;
                v154 = v158;
                v155 = v157;
                v156 = v164;
                if ((v165 & (chars_per_column ^ -0x80000000)) < 0) {
                    goto lab_0x5985;
                } else {
                    int64_t v166 = 0x100000000 * v160 >> 32; // 0x5720
                    int64_t v167 = v166 * (int64_t)v165; // 0x5720
                    v152 = v159;
                    v153 = v161;
                    v154 = v158;
                    v155 = v157;
                    v156 = v164;
                    if (v167 > 0x7fffffff) {
                        goto lab_0x5985;
                    } else {
                        int64_t v168 = 0x100000000 * v167 >> 32; // 0x5723
                        function_24d0();
                        char * v169 = xnmalloc((int64_t)v163, 4); // 0x5740
                        *(int64_t *)&line_vector = (int64_t)v169;
                        function_24d0();
                        char * v170 = xnmalloc(v166, 4); // 0x5760
                        *(int64_t *)&end_vector = (int64_t)v170;
                        function_24d0();
                        buff = (int64_t)xnmalloc(v168, (int64_t)&ignore_failed_opens);
                        buff_allocated = v168 * (int64_t)&ignore_failed_opens;
                        goto lab_0x4d7c;
                    }
                }
            }
        }
    } else {
        goto lab_0x4d7c;
    }
  lab_0x4d44_2:
    // 0x4d44
    result = v119;
    if (v118 == 0) {
        // 0x5107
        return result;
    }
    // 0x4d52
    init_header((char *)&g35, -1);
    v120 = v114;
    v122 = 0xffffffff;
    v121 = &g35;
    goto lab_0x4d63;
  lab_0x4d7c:;
    int64_t v171 = 1; // 0x4d8c
    if (first_page_number >= 2) {
        int64_t v172 = 1; // 0x56d8
        int64_t v173 = (int64_t)column_vector; // 0x55d2
        int64_t v174 = columns; // 0x55df
        int32_t v175 = lines_per_body; // 0x55e6
        int64_t v176 = v174; // 0x55e6
        int64_t v177 = v173; // 0x55e6
        int64_t v178 = v174; // 0x55e6
        int64_t v179 = v173; // 0x55e6
        int32_t v180; // 0x4b20
        int64_t v181; // 0x4b20
        int64_t v182; // 0x4b20
        int64_t v183; // 0x4b20
        int64_t v184; // 0x4b20
        int64_t v185; // 0x4b20
        int64_t v186; // 0x4b20
        int64_t v187; // 0x4b20
        int64_t v188; // 0x4b20
        int64_t v189; // 0x5635
        int64_t v190; // 0x5600
        int64_t v191; // 0x5617
        int64_t v192; // 0x5623
        if (lines_per_body >= 2) {
            v182 = v176;
            v186 = v177;
            v180 = v175;
            v184 = v176;
            v188 = v177;
            if ((int32_t)v176 >= 1) {
                v183 = v182;
                v185 = 1;
                v187 = v186;
                while (*(int32_t *)(v187 + 16) != 0) {
                    // 0x5600
                    v190 = v185 + 1 & 0xffffffff;
                    if (v190 > v183) {
                        // break (via goto) -> 0x562e
                        goto lab_0x562e;
                    }
                    v185 = v190;
                    v187 += 64;
                }
                // 0x5612
                v191 = v185 + 1 & 0xffffffff;
                skip_read((int32_t *)v187, (int32_t)v185);
                v192 = columns;
                while (v191 <= v192) {
                    // 0x560b
                    v183 = v192;
                    v185 = v191;
                    v187 += 64;
                    while (*(int32_t *)(v187 + 16) != 0) {
                        // 0x5600
                        v190 = v185 + 1 & 0xffffffff;
                        if (v190 > v183) {
                            // break (via goto) -> 0x562e
                            goto lab_0x562e;
                        }
                        v185 = v190;
                        v187 += 64;
                    }
                    // 0x5612
                    v191 = v185 + 1 & 0xffffffff;
                    skip_read((int32_t *)v187, (int32_t)v185);
                    v192 = columns;
                }
                // 0x562e
                v180 = lines_per_body;
                v184 = v192;
                v188 = (int64_t)column_vector;
            }
            // 0x5635
            v189 = 2;
            v175 = v180;
            v176 = v184;
            v177 = v188;
            v181 = v189 & 0xffffffff;
            v178 = v184;
            v179 = v188;
            while (v180 > (int32_t)v189) {
                // 0x55f0
                v182 = v176;
                v186 = v177;
                v180 = v175;
                v184 = v176;
                v188 = v177;
                if ((int32_t)v176 >= 1) {
                    v183 = v182;
                    v185 = 1;
                    v187 = v186;
                    while (*(int32_t *)(v187 + 16) != 0) {
                        // 0x5600
                        v190 = v185 + 1 & 0xffffffff;
                        if (v190 > v183) {
                            // break (via goto) -> 0x562e
                            goto lab_0x562e;
                        }
                        v185 = v190;
                        v187 += 64;
                    }
                    // 0x5612
                    v191 = v185 + 1 & 0xffffffff;
                    skip_read((int32_t *)v187, (int32_t)v185);
                    v192 = columns;
                    while (v191 <= v192) {
                        // 0x560b
                        v183 = v192;
                        v185 = v191;
                        v187 += 64;
                        while (*(int32_t *)(v187 + 16) != 0) {
                            // 0x5600
                            v190 = v185 + 1 & 0xffffffff;
                            if (v190 > v183) {
                                // break (via goto) -> 0x562e
                                goto lab_0x562e;
                            }
                            v185 = v190;
                            v187 += 64;
                        }
                        // 0x5612
                        v191 = v185 + 1 & 0xffffffff;
                        skip_read((int32_t *)v187, (int32_t)v185);
                        v192 = columns;
                    }
                    // 0x562e
                    v180 = lines_per_body;
                    v184 = v192;
                    v188 = (int64_t)column_vector;
                }
                // 0x5635
                v189 = v181 + 1;
                v175 = v180;
                v176 = v184;
                v177 = v188;
                v181 = v189 & 0xffffffff;
                v178 = v184;
                v179 = v188;
            }
        }
        // 0x5642
        *(char *)&last_line = 1;
        int64_t v193 = v178; // 0x564b
        int64_t v194 = v179; // 0x564b
        int64_t v195; // 0x4b20
        int64_t v196; // 0x4b20
        int64_t v197; // 0x4b20
        int64_t v198; // 0x4b20
        int64_t v199; // 0x4b20
        int64_t v200; // 0x5658
        int64_t v201; // 0x566f
        int64_t v202; // 0x567b
        int64_t v203; // 0x568f
        int32_t * v204; // 0x56a8
        int64_t v205; // 0x56b5
        if ((int32_t)v178 >= 1) {
            v195 = v193;
            v198 = 1;
            v199 = v194;
            while (*(int32_t *)(v199 + 16) != 0) {
                // 0x5658
                v200 = v198 + 1 & 0xffffffff;
                v196 = v195;
                if (v200 > v195) {
                    // break (via goto) -> 0x5686
                    goto lab_0x5686;
                }
                v198 = v200;
                v199 += 64;
            }
            // 0x566a
            v201 = v198 + 1 & 0xffffffff;
            skip_read((int32_t *)v199, (int32_t)v198);
            v202 = columns;
            v196 = v202;
            while (v201 <= v202) {
                // 0x5663
                v195 = v202;
                v198 = v201;
                v199 += 64;
                while (*(int32_t *)(v199 + 16) != 0) {
                    // 0x5658
                    v200 = v198 + 1 & 0xffffffff;
                    v196 = v195;
                    if (v200 > v195) {
                        // break (via goto) -> 0x5686
                        goto lab_0x5686;
                    }
                    v198 = v200;
                    v199 += 64;
                }
                // 0x566a
                v201 = v198 + 1 & 0xffffffff;
                skip_read((int32_t *)v199, (int32_t)v198);
                v202 = columns;
                v196 = v202;
            }
            // 0x5686
            if (*(char *)&storing_columns != 0) {
                // 0x568f
                v197 = v196;
                if ((int32_t)v197 >= 1) {
                    // 0x569a
                    v203 = (int64_t)column_vector;
                    v205 = v203;
                    v204 = (int32_t *)(v205 + 16);
                    if (*v204 != 3) {
                        // 0x56ae
                        *v204 = 2;
                    }
                    // 0x56b5
                    v205 += 64;
                    while (v205 != 64 * v197 + v203) {
                        // 0x56a8
                        v204 = (int32_t *)(v205 + 16);
                        if (*v204 != 3) {
                            // 0x56ae
                            *v204 = 2;
                        }
                        // 0x56b5
                        v205 += 64;
                    }
                }
            }
        }
        // 0x56be
        reset_status();
        *(char *)&last_line = 0;
        while (files_ready_to_read >= 1) {
            // 0x56d8
            v172++;
            if (first_page_number == v172) {
                goto lab_0x56e5;
            }
            v173 = (int64_t)column_vector;
            v174 = columns;
            v175 = lines_per_body;
            v176 = v174;
            v177 = v173;
            v178 = v174;
            v179 = v173;
            if (lines_per_body >= 2) {
                v182 = v176;
                v186 = v177;
                v180 = v175;
                v184 = v176;
                v188 = v177;
                if ((int32_t)v176 >= 1) {
                    v183 = v182;
                    v185 = 1;
                    v187 = v186;
                    while (*(int32_t *)(v187 + 16) != 0) {
                        // 0x5600
                        v190 = v185 + 1 & 0xffffffff;
                        if (v190 > v183) {
                            // break (via goto) -> 0x562e
                            goto lab_0x562e;
                        }
                        v185 = v190;
                        v187 += 64;
                    }
                    // 0x5612
                    v191 = v185 + 1 & 0xffffffff;
                    skip_read((int32_t *)v187, (int32_t)v185);
                    v192 = columns;
                    while (v191 <= v192) {
                        // 0x560b
                        v183 = v192;
                        v185 = v191;
                        v187 += 64;
                        while (*(int32_t *)(v187 + 16) != 0) {
                            // 0x5600
                            v190 = v185 + 1 & 0xffffffff;
                            if (v190 > v183) {
                                // break (via goto) -> 0x562e
                                goto lab_0x562e;
                            }
                            v185 = v190;
                            v187 += 64;
                        }
                        // 0x5612
                        v191 = v185 + 1 & 0xffffffff;
                        skip_read((int32_t *)v187, (int32_t)v185);
                        v192 = columns;
                    }
                    // 0x562e
                    v180 = lines_per_body;
                    v184 = v192;
                    v188 = (int64_t)column_vector;
                }
                // 0x5635
                v189 = 2;
                v175 = v180;
                v176 = v184;
                v177 = v188;
                v181 = v189 & 0xffffffff;
                v178 = v184;
                v179 = v188;
                while (v180 > (int32_t)v189) {
                    // 0x55f0
                    v182 = v176;
                    v186 = v177;
                    v180 = v175;
                    v184 = v176;
                    v188 = v177;
                    if ((int32_t)v176 >= 1) {
                        v183 = v182;
                        v185 = 1;
                        v187 = v186;
                        while (*(int32_t *)(v187 + 16) != 0) {
                            // 0x5600
                            v190 = v185 + 1 & 0xffffffff;
                            if (v190 > v183) {
                                // break (via goto) -> 0x562e
                                goto lab_0x562e;
                            }
                            v185 = v190;
                            v187 += 64;
                        }
                        // 0x5612
                        v191 = v185 + 1 & 0xffffffff;
                        skip_read((int32_t *)v187, (int32_t)v185);
                        v192 = columns;
                        while (v191 <= v192) {
                            // 0x560b
                            v183 = v192;
                            v185 = v191;
                            v187 += 64;
                            while (*(int32_t *)(v187 + 16) != 0) {
                                // 0x5600
                                v190 = v185 + 1 & 0xffffffff;
                                if (v190 > v183) {
                                    // break (via goto) -> 0x562e
                                    goto lab_0x562e;
                                }
                                v185 = v190;
                                v187 += 64;
                            }
                            // 0x5612
                            v191 = v185 + 1 & 0xffffffff;
                            skip_read((int32_t *)v187, (int32_t)v185);
                            v192 = columns;
                        }
                        // 0x562e
                        v180 = lines_per_body;
                        v184 = v192;
                        v188 = (int64_t)column_vector;
                    }
                    // 0x5635
                    v189 = v181 + 1;
                    v175 = v180;
                    v176 = v184;
                    v177 = v188;
                    v181 = v189 & 0xffffffff;
                    v178 = v184;
                    v179 = v188;
                }
            }
            // 0x5642
            *(char *)&last_line = 1;
            v193 = v178;
            v194 = v179;
            if ((int32_t)v178 >= 1) {
                v195 = v193;
                v198 = 1;
                v199 = v194;
                while (*(int32_t *)(v199 + 16) != 0) {
                    // 0x5658
                    v200 = v198 + 1 & 0xffffffff;
                    v196 = v195;
                    if (v200 > v195) {
                        // break (via goto) -> 0x5686
                        goto lab_0x5686;
                    }
                    v198 = v200;
                    v199 += 64;
                }
                // 0x566a
                v201 = v198 + 1 & 0xffffffff;
                skip_read((int32_t *)v199, (int32_t)v198);
                v202 = columns;
                v196 = v202;
                while (v201 <= v202) {
                    // 0x5663
                    v195 = v202;
                    v198 = v201;
                    v199 += 64;
                    while (*(int32_t *)(v199 + 16) != 0) {
                        // 0x5658
                        v200 = v198 + 1 & 0xffffffff;
                        v196 = v195;
                        if (v200 > v195) {
                            // break (via goto) -> 0x5686
                            goto lab_0x5686;
                        }
                        v198 = v200;
                        v199 += 64;
                    }
                    // 0x566a
                    v201 = v198 + 1 & 0xffffffff;
                    skip_read((int32_t *)v199, (int32_t)v198);
                    v202 = columns;
                    v196 = v202;
                }
                // 0x5686
                if (*(char *)&storing_columns != 0) {
                    // 0x568f
                    v197 = v196;
                    if ((int32_t)v197 >= 1) {
                        // 0x569a
                        v203 = (int64_t)column_vector;
                        v205 = v203;
                        v204 = (int32_t *)(v205 + 16);
                        if (*v204 != 3) {
                            // 0x56ae
                            *v204 = 2;
                        }
                        // 0x56b5
                        v205 += 64;
                        while (v205 != 64 * v197 + v203) {
                            // 0x56a8
                            v204 = (int32_t *)(v205 + 16);
                            if (*v204 != 3) {
                                // 0x56ae
                                *v204 = 2;
                            }
                            // 0x56b5
                            v205 += 64;
                        }
                    }
                }
            }
            // 0x56be
            reset_status();
            *(char *)&last_line = 0;
        }
        // 0x58ac
        function_25d0();
        function_27f0();
        result = files_ready_to_read;
        if (files_ready_to_read < 1) {
            // 0x5107
            return result;
        }
      lab_0x56e5:
        // 0x56e5
        v171 = first_page_number;
    }
    // 0x4d92
    page_number = v171;
    char v206 = *(char *)&truncate_lines; // 0x4da2
    char v207 = *(char *)&numbered_lines; // 0x4daa
    int32_t v208 = 0; // 0x4db4
    if (v206 != 0) {
        int32_t v209 = chars_per_column + chars_per_margin; // 0x4dbd
        v208 = v209;
        if (!((v207 == 0 | *(char *)&parallel_files == 0))) {
            // 0x585a
            v208 = number_width + v209;
        }
    }
    uint32_t v210 = col_sep_length; // 0x4dcd
    int64_t v211 = (int64_t)column_vector; // 0x4dd4
    int32_t v212 = columns; // 0x4ddb
    char v213 = *(char *)&storing_columns; // 0x4de1
    int64_t v214 = v210 + chars_per_margin; // 0x4de9
    int64_t v215 = v211; // 0x4df6
    int64_t v216 = v214; // 0x4df6
    int32_t v217 = 1; // 0x4df6
    if (v212 >= 2) {
        int64_t v218 = v210; // 0x4dcd
        int32_t v219 = chars_per_column; // 0x4dfc
        int64_t v220 = v211; // 0x4e41
        int64_t v221 = 1; // 0x4e41
        int64_t v222 = v214; // 0x4e41
        int32_t v223 = v208; // 0x4e41
        int64_t v224; // 0x4b20
        while (true) {
            uint32_t v225 = v223;
            int64_t v226 = v221;
            int64_t v227 = v220;
            *(int64_t *)(v227 + 32) = v213 == 0 ? 0x48f0 : 0x3460;
            *(int64_t *)(v227 + 24) = v213 == 0 ? 0x4490 : 0x4990;
            char v228 = v207 == 0 ? 0 : *(char *)&parallel_files ^ 1 | (char)(v226 == 1);
            *(char *)(v227 + 56) = v228;
            *(int32_t *)(v227 + 52) = (int32_t)v222;
            int32_t v229; // 0x4b20
            int64_t v230; // 0x4b20
            int64_t v231; // 0x4b20
            if (v206 != 0) {
                int64_t v232 = (int64_t)v225 + v218; // 0x4e47
                int64_t v233 = v232 & 0xffffffff; // 0x4e47
                int64_t v234 = v226 + 1; // 0x4e4b
                v230 = v234;
                v231 = v233;
                v229 = v219 + (int32_t)v232;
                v224 = v233;
                if (v212 == (int32_t)v234) {
                    // break -> 0x4e8d
                    break;
                }
            } else {
                int64_t v235 = v226 + 1; // 0x4e7d
                v230 = v235;
                v231 = 0;
                v229 = 0;
                v224 = 0;
                if (v212 == (int32_t)v235) {
                    // break -> 0x4e8d
                    break;
                }
            }
            // 0x4e5a
            v220 = v227 + 64;
            v221 = v230 & 0xffffffff;
            v222 = v231;
            v223 = v229;
        }
        // 0x4e8d
        v215 = 64 * (int64_t)(v212 - 1) + v211;
        v216 = v224;
        v217 = v212;
    }
    int32_t v236 = v217;
    int64_t v237 = 0x48f0; // 0x4eae
    int64_t v238 = 0x4490; // 0x4eae
    if (v213 != 0) {
        char v239 = *(char *)&balance_columns; // 0x4eb0
        v237 = v239 != 0 ? 0x3460 : 0x48f0;
        v238 = v239 != 0 ? 0x4990 : 0x4490;
    }
    // 0x4ecd
    *(int64_t *)(v215 + 32) = v237;
    *(int64_t *)(v215 + 24) = v238;
    char v240 = 0; // 0x4ed8
    if (v207 != 0) {
        // 0x4eda
        v240 = *(char *)&parallel_files ^ 1 | (char)(v236 == 1);
    }
    // 0x4eed
    *(char *)(v215 + 56) = v240;
    *(int32_t *)(v215 + 52) = (int32_t)v216;
    line_number = line_count;
    int32_t v241 = v212; // 0x4f06
    int64_t v242 = v211; // 0x4f06
    int32_t v243 = v212; // 0x4f06
    int64_t v244 = v211; // 0x4f06
    if (v213 != 0) {
        goto lab_0x5292;
    } else {
        goto lab_0x4f0c;
    }
  lab_0x5292:;
    int64_t v245 = v244;
    char v246 = *(char *)&balance_columns; // 0x5292
    buff_current = 0;
    uint32_t v247 = v243 - (int32_t)(v246 == 0); // 0x52a4
    int64_t v248 = 0; // 0x52af
    int64_t v249 = v245; // 0x52af
    int64_t v250 = 0; // 0x52af
    char v251 = v246; // 0x52af
    int64_t v252 = 0; // 0x52af
    if (v247 >= 1) {
        int64_t v253 = v245; // 0x52c2
        *(int32_t *)(v253 + 44) = 0;
        v253 += 64;
        while ((0x100000000 * (int64_t)v247 >> 26) + v245 != v253) {
            // 0x52c8
            *(int32_t *)(v253 + 44) = 0;
            v253 += 64;
        }
        int64_t v254 = v247;
        int64_t v255 = v245; // 0x4b20
        int64_t v256 = 0;
        int64_t v257 = 0;
        int64_t v258 = 1; // 0x5380
        int64_t v259; // 0x4b20
        int64_t v260; // 0x4b20
        while (true) {
            int64_t v261 = v257;
            int64_t v262 = v256;
            v260 = v262;
            v259 = v261;
            if (files_ready_to_read == 0) {
                // break -> 0x5393
                break;
            }
            int64_t v263 = v255;
            int32_t v264 = lines_per_body; // 0x52f8
            *(int32_t *)(v263 + 40) = (int32_t)v262;
            v10 = v262;
            v11 = v261;
            if (v264 != 0) {
                int32_t * v15 = (int32_t *)(v263 + 16); // 0x5320
                int32_t * v16 = (int32_t *)v263;
                int32_t * v20 = (int32_t *)(v263 + 44);
                v12 = v262;
                v13 = v261;
                v14 = v264;
                while (true) {
                  lab_0x5320:
                    // 0x5320
                    v3 = v14;
                    v7 = v13;
                    v5 = v12;
                    if (*v15 != 0) {
                        goto lab_0x5310;
                    } else {
                        // 0x5327
                        input_position = 0;
                        if (!read_line(v16)) {
                            // 0x5478
                            read_rest_of_line(v16);
                        }
                        uint32_t v17 = buff_current; // 0x5341
                        int32_t v18 = v7; // 0x534b
                        if (*v15 != 0 == v17 == v18) {
                            goto lab_0x5310;
                        } else {
                            int64_t v19 = v17; // 0x5341
                            *v20 = *v20 + 1;
                            int64_t v21 = v5 + 1 & 0xffffffff; // 0x535d
                            int64_t v22 = 4 * v5 & 0x3fffffffc; // 0x5366
                            *(int32_t *)(v22 + (int64_t)line_vector) = v18;
                            *(int32_t *)(v22 + (int64_t)end_vector) = input_position;
                            int32_t v23 = v3 - 1; // 0x5377
                            v6 = v21;
                            v8 = v19;
                            v9 = v23;
                            v10 = v21;
                            v11 = v19;
                            if (v23 == 0) {
                                // break -> 0x5380
                                break;
                            }
                            goto lab_0x5316;
                        }
                    }
                }
            }
          lab_0x5380_2:
            // 0x5380
            v257 = v11;
            v256 = v10;
            v258 = v258 + 1 & 0xffffffff;
            v255 = v263 + 64;
            v260 = v256;
            v259 = v257;
            if (v258 > v254) {
                // break -> 0x5393
                break;
            }
        }
        // 0x5393
        v248 = 4 * v260 & 0x3fffffffc;
        v249 = (int64_t)column_vector;
        v250 = v260;
        v251 = *(char *)&balance_columns;
        v252 = v259;
    }
    char v265 = v251;
    int64_t v266 = v250;
    int64_t v267 = v249;
    *(int32_t *)(v248 + (int64_t)line_vector) = (int32_t)v252;
    int32_t v268 = columns; // 0x53b5
    int64_t v269; // 0x4b20
    int64_t v270; // 0x4b20
    int32_t v271; // 0x4b20
    if (v265 != 0) {
        if (v268 < 1) {
            // 0x54e9
            v271 = v268 - 1;
            goto lab_0x53d0;
        } else {
            int64_t v272 = v268; // 0x53b5
            int64_t v273 = 0x100000000 * (int64_t)((int32_t)v266 >> 31) | v266 & 0xffffffff; // 0x54c4
            int64_t v274 = v273 / v272; // 0x54c4
            int64_t v275 = v273 % v272; // 0x54c4
            int64_t v276 = 1; // 0x54c6
            int64_t v277 = v267; // 0x54c6
            int64_t v278 = 0; // 0x54c6
            int32_t v279 = v276; // 0x54ca
            int32_t v280 = v279 - (int32_t)v275; // 0x54ca
            *(int32_t *)(v277 + 40) = (int32_t)v278;
            int64_t v281 = v274 + (int64_t)(v280 == 0 | v280 < 0 != ((v280 ^ v279) & (int32_t)(v276 ^ v275)) < 0);
            int64_t v282 = v276 + 1; // 0x54d7
            *(int32_t *)(v277 + 44) = (int32_t)v281;
            v276 = v282 & 0xffffffff;
            v277 += 64;
            v278 = v281 + v278 & 0xffffffff;
            while (v268 + 1 != (int32_t)v282) {
                // 0x54ca
                v279 = v276;
                v280 = v279 - (int32_t)v275;
                *(int32_t *)(v277 + 40) = (int32_t)v278;
                v281 = v274 + (int64_t)(v280 == 0 | v280 < 0 != ((v280 ^ v279) & (int32_t)(v276 ^ v275)) < 0);
                v282 = v276 + 1;
                *(int32_t *)(v277 + 44) = (int32_t)v281;
                v276 = v282 & 0xffffffff;
                v277 += 64;
                v278 = v281 + v278 & 0xffffffff;
            }
            int32_t v283 = v268 - 1; // 0x54e9
            v271 = v283;
            v269 = v267;
            if (v283 != 0) {
                goto lab_0x53d0;
            } else {
                goto lab_0x53f8;
            }
        }
    } else {
        int32_t v284 = v268 - 1; // 0x53c4
        v271 = v284;
        v270 = v267;
        if (v284 == 0) {
            goto lab_0x5485;
        } else {
            goto lab_0x53d0;
        }
    }
  lab_0x4f0c:;
    int64_t v285 = v242;
    uint32_t v286 = v241;
    if (v286 != 0) {
        int64_t v287 = v285; // 0x4f25
        while (true) {
            int64_t v288 = v287;
            int32_t v289 = *(int32_t *)(v288 + 16); // 0x4f28
            v287 = v288 + 64;
            *(int32_t *)(v288 + 48) = v289 == 0 ? lines_per_body : 0;
            if (64 * (int64_t)v286 + v285 == v287) {
                goto lab_0x4f41;
            }
        }
    }
    goto lab_0x4f41;
  lab_0x5485:;
    int64_t v290 = v270;
    if (*(int32_t *)(v290 + 16) != 0) {
        // 0x554a
        *(int32_t *)(v290 + 48) = 0;
    } else {
        // 0x5492
        *(int32_t *)(v290 + 48) = lines_per_body;
    }
    goto lab_0x4f41;
  lab_0x53d0:;
    int64_t v291 = 64 * (int64_t)v271 + v267; // 0x53d6
    int64_t v292 = v267;
    int64_t v293 = v292 + 64; // 0x53e3
    *(int32_t *)(v292 + 48) = *(int32_t *)(v292 + 44);
    while (v293 != v291) {
        // 0x53e0
        v292 = v293;
        v293 = v292 + 64;
        *(int32_t *)(v292 + 48) = *(int32_t *)(v292 + 44);
    }
    // 0x53ef
    v269 = v291;
    v270 = v291;
    if (v265 == 0) {
        goto lab_0x5485;
    } else {
        goto lab_0x53f8;
    }
  lab_0x53f8:;
    int64_t v294 = v269;
    *(int32_t *)(v294 + 48) = *(int32_t *)(v294 + 44);
    goto lab_0x4f41;
  lab_0x5210:
    // 0x5210
    if (!((*(char *)&keep_FF == 0 | *(char *)&print_a_FF == 0))) {
        int64_t v295 = (int64_t)g12; // 0x5222
        int64_t * v296 = (int64_t *)(v295 + 40); // 0x5229
        uint64_t v297 = *v296; // 0x5229
        if (v297 >= *(int64_t *)(v295 + 48)) {
            // 0x5903
            function_2640();
        } else {
            // 0x5237
            *v296 = v297 + 1;
            *(char *)v297 = 12;
        }
        // 0x5242
        *(char *)&print_a_FF = 0;
    }
    goto lab_0x5249;
  lab_0x51f7:;
    char v303 = v28; // 0x51f7
    *(char *)&pad_vertically = v303;
    if (v303 == 0) {
        goto lab_0x5210;
    } else {
        // 0x5203
        if (*(char *)&extremities != 0) {
            // 0x54fa
            if (*(char *)&use_form_feed != 0) {
                struct _IO_FILE * v304 = g12; // 0x55a0
                int64_t v305 = (int64_t)v304; // 0x55a0
                int64_t * v306 = (int64_t *)(v305 + 40); // 0x55a7
                uint64_t v307 = *v306; // 0x55a7
                uint64_t v308 = *(int64_t *)(v305 + 48); // 0x55ab
                if (v307 >= v308) {
                    // 0x5938
                    function_2640();
                } else {
                    // 0x55b5
                    *v306 = v307 + 1;
                    *(char *)v307 = 12;
                }
            } else {
                int32_t v309 = (int32_t)v29 + 5; // 0x5507
                int32_t v310 = v309; // 0x550b
                if (v309 != 0) {
                    int64_t v311 = (int64_t)g12; // 0x552d
                    int64_t * v312 = (int64_t *)(v311 + 40); // 0x5534
                    uint64_t v313 = *v312; // 0x5534
                    if (v313 < *(int64_t *)(v311 + 48)) {
                        // 0x5518
                        *v312 = v313 + 1;
                        *(char *)v313 = 10;
                    } else {
                        // 0x553e
                        function_2640();
                    }
                    int32_t v314 = v310 - 1; // 0x5523
                    v310 = v314;
                    while (v314 != 0) {
                        // 0x552d
                        v311 = (int64_t)g12;
                        v312 = (int64_t *)(v311 + 40);
                        v313 = *v312;
                        if (v313 < *(int64_t *)(v311 + 48)) {
                            // 0x5518
                            *v312 = v313 + 1;
                            *(char *)v313 = 10;
                        } else {
                            // 0x553e
                            function_2640();
                        }
                        // 0x5523
                        v314 = v310 - 1;
                        v310 = v314;
                    }
                }
            }
            goto lab_0x5249;
        } else {
            goto lab_0x5210;
        }
    }
  lab_0x5249:;
    int64_t v298 = page_number + 1; // 0x5250
    page_number = v298;
    result = v298;
    if (v298 > last_page_number) {
        // 0x5107
        return result;
    }
    // 0x5268
    reset_status();
    int64_t v299 = (int64_t)column_vector; // 0x5273
    v241 = columns;
    v242 = v299;
    v243 = columns;
    v244 = v299;
    if (*(char *)&storing_columns == 0) {
        goto lab_0x4f0c;
    } else {
        goto lab_0x5292;
    }
  lab_0x51d9:;
    int64_t v300 = (int64_t)column_vector;
    int64_t v301 = v300;
    if (*(int32_t *)(v301 + 16) == 0) {
        // 0x51e7
        *(char *)(v301 + 57) = 1;
    }
    int64_t v302 = v301 + 64; // 0x51eb
    v28 = v31;
    v29 = 0;
    while (v302 != 64 * (int64_t)v32 + v300) {
        // 0x51e0
        v301 = v302;
        if (*(int32_t *)(v301 + 16) == 0) {
            // 0x51e7
            *(char *)(v301 + 57) = 1;
        }
        // 0x51eb
        v302 = v301 + 64;
        v28 = v31;
        v29 = 0;
    }
    goto lab_0x51f7;
}