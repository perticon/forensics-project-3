void print_files(ulong param_1,undefined8 *param_2)

{
  undefined8 *puVar1;
  char *pcVar2;
  undefined4 uVar3;
  char cVar4;
  void *pvVar5;
  char cVar6;
  uint uVar7;
  int iVar8;
  int iVar9;
  FILE **ppFVar10;
  ulong uVar11;
  long lVar12;
  FILE **ppFVar13;
  FILE *pFVar14;
  undefined8 uVar15;
  byte bVar16;
  uint uVar17;
  ulong uVar18;
  FILE **ppFVar19;
  FILE **ppFVar20;
  int iVar21;
  FILE *pFVar22;
  byte bVar23;
  undefined1 *puVar24;
  int iVar25;
  int iVar26;
  int iVar27;
  code *pcVar28;
  FILE *pFVar29;
  code *pcVar30;
  ulong uVar31;
  byte bVar32;
  uint local_40;
  
  pFVar29 = (FILE *)(param_1 & 0xffffffff);
  lines_per_body = lines_per_page + -10;
  if (lines_per_body < 1) {
    extremities = '\0';
    keep_FF = '\x01';
LAB_001050b4:
    lines_per_body = lines_per_page;
  }
  else if (extremities == '\0') goto LAB_001050b4;
  if (double_space != '\0') {
    lines_per_body = lines_per_body / 2;
  }
  if ((uint)pFVar29 == 0) {
    parallel_files = 0;
    pFVar14 = (FILE *)(ulong)columns;
    uVar7 = columns;
  }
  else {
    pFVar14 = pFVar29;
    uVar7 = (uint)pFVar29;
    if (parallel_files == 0) {
      pFVar14 = (FILE *)(ulong)columns;
      uVar7 = columns;
    }
  }
  columns = uVar7;
  if (storing_columns != '\0') {
    balance_columns = '\x01';
  }
  iVar8 = col_sep_length;
  if ((int)pFVar14 < 2) {
    storing_columns = '\0';
  }
  else {
    if (use_col_separator == 0) {
      col_sep_string = &DAT_0010e063;
      col_sep_length = 1;
      if (join_lines != '\0') {
        col_sep_string = &DAT_0010e06a;
      }
      use_col_separator = 1;
LAB_00104bef:
      iVar8 = 1;
      if (*col_sep_string != '\t') goto LAB_00104bfa;
    }
    else {
      if (join_lines == '\0') {
        if (col_sep_length == 1) {
          if (*col_sep_string == '\t') {
            col_sep_string = &DAT_0010e063;
          }
          goto LAB_00104bef;
        }
      }
      else if (col_sep_length == 1) goto LAB_00104bef;
LAB_00104bfa:
      untabify_input = 1;
    }
    truncate_lines = '\x01';
    tabify_output = 1;
  }
  if (join_lines != '\0') {
    truncate_lines = '\0';
  }
  pFVar22 = (FILE *)(ulong)numbered_lines;
  if (numbered_lines != 0) {
    line_count = start_line_num;
    number_width = chars_per_number + 1;
    if (number_separator == '\t') {
      uVar7 = (uint)((int)chars_per_number >> 0x1f) >> 0x1d;
      number_width = chars_per_number + 8 + (uVar7 - (chars_per_number + uVar7 & 7));
    }
    puVar24 = (undefined1 *)(ulong)number_width;
    if (parallel_files != 0) goto LAB_00104c64;
  }
  puVar24 = (undefined1 *)0x0;
LAB_00104c64:
  uVar11 = (long)((int)pFVar14 + -1) * (long)iVar8;
  uVar18 = uVar11 & 0xffffffff;
  if ((long)(int)uVar11 != uVar11) goto LAB_0010598a;
  do {
    iVar8 = chars_per_line - (int)puVar24;
    if (SBORROW4(iVar8,(int)uVar18)) {
      chars_per_column = 0;
LAB_00105961:
      uVar15 = dcgettext(0,"page width too narrow",5);
      pFVar22 = (FILE *)0x0;
      puVar24 = (undefined1 *)0x1;
      error(1,0,uVar15);
    }
    else {
      chars_per_column = (iVar8 - (int)uVar18) / (int)pFVar14;
      if (chars_per_column < 1) goto LAB_00105961;
      if ((char)pFVar22 != '\0') {
        free(number_buff);
        lVar12 = (long)(int)chars_per_number + 1;
        if (chars_per_number < 0xc) {
          lVar12 = 0xc;
        }
        number_buff = (void *)xmalloc(lVar12);
      }
      free(clump_buff);
      iVar8 = chars_per_input_tab;
      if (chars_per_input_tab < 8) {
        iVar8 = 8;
      }
      clump_buff = (void *)xmalloc((long)iVar8);
      total_files = 0;
      free(column_vector);
      ppFVar10 = (FILE **)xnmalloc((long)(int)columns);
      column_vector = ppFVar10;
      if (parallel_files == 0) {
        if ((int)pFVar29 < 1) {
          pFVar14 = (FILE *)dcgettext(0,"standard input",5);
          *(undefined *)((long)ppFVar10 + 0x39) = 0;
          puVar24 = &DAT_0010e06b;
          ppFVar10[1] = pFVar14;
          pFVar14 = stdin;
          *(undefined4 *)(ppFVar10 + 2) = 0;
          *ppFVar10 = pFVar14;
          total_files = total_files + 1;
          have_read_stdin = 1;
          init_header(&DAT_0010e06b,0xffffffff);
        }
        else {
          cVar6 = open_file(*param_2,ppFVar10);
          if (cVar6 == '\0') {
            return;
          }
          iVar8 = fileno(*ppFVar10);
          puVar24 = (undefined1 *)*param_2;
          init_header(puVar24,iVar8);
        }
        uVar7 = columns;
        *(undefined4 *)((long)ppFVar10 + 0x2c) = 0;
        pFVar14 = ppFVar10[1];
        pFVar22 = *ppFVar10;
        if (uVar7 != 1) {
          ppFVar10 = ppFVar10 + 8;
          do {
            ppFVar10[1] = pFVar14;
            ppFVar13 = ppFVar10 + 8;
            *ppFVar10 = pFVar22;
            *(undefined4 *)(ppFVar10 + 2) = 0;
            *(undefined *)((long)ppFVar10 + 0x39) = 0;
            *(undefined4 *)((long)ppFVar10 + 0x2c) = 0;
            ppFVar10 = ppFVar13;
          } while (ppFVar13 != ppFVar10 + ((ulong)(uVar7 - 2) + 2) * 8);
        }
      }
      else {
        puVar1 = param_2 + (long)pFVar29;
        if ((int)pFVar29 != 0) {
          do {
            while (cVar6 = open_file(*param_2), cVar6 == '\0') {
              param_2 = param_2 + 1;
              columns = columns - 1;
              if (param_2 == puVar1) goto LAB_00104d44;
            }
            param_2 = param_2 + 1;
          } while (param_2 != puVar1);
        }
LAB_00104d44:
        if (columns == 0) {
          return;
        }
        pFVar22 = (FILE *)0xffffffff;
        puVar24 = &DAT_0010e06b;
        init_header();
      }
      files_ready_to_read = total_files;
      if (storing_columns == '\0') goto LAB_00104d7c;
      uVar11 = (long)lines_per_body * (long)(int)columns;
      param_2 = (undefined8 *)(uVar11 & 0xffffffff);
      if ((long)(int)uVar11 == uVar11) {
        iVar8 = (int)param_2;
        pFVar29 = (FILE *)(ulong)(iVar8 + 1U);
        if (((!SCARRY4(iVar8,1)) && (!SCARRY4(chars_per_column,1))) &&
           (lVar12 = (long)(chars_per_column + 1) * (long)iVar8, iVar9 = (int)lVar12,
           iVar9 == lVar12)) break;
      }
    }
    integer_overflow();
LAB_0010598a:
    uVar18 = 0x7fffffff;
  } while( true );
  free(line_vector);
  line_vector = (void *)xnmalloc((long)(int)(iVar8 + 1U),4);
  free(end_vector);
  end_vector = (void *)xnmalloc((long)iVar8,4);
  free(buff);
  buff = (void *)xnmalloc();
  buff_allocated = ((ulong)use_col_separator + 1) * (long)iVar9;
LAB_00104d7c:
  uVar11 = first_page_number;
  uVar18 = 1;
  if (1 < first_page_number) {
    uVar31 = 1;
    do {
      iVar8 = 1;
      lVar12 = (long)(int)columns;
      ppFVar10 = column_vector;
      ppFVar13 = column_vector;
      if (1 < lines_per_body) {
        do {
          ppFVar10 = ppFVar13;
          if (0 < (int)lVar12) {
            iVar9 = 1;
            do {
              while (*(int *)(ppFVar13 + 2) != 0) {
                iVar9 = iVar9 + 1;
                ppFVar13 = ppFVar13 + 8;
                ppFVar10 = column_vector;
                if ((int)lVar12 < iVar9) goto LAB_00105635;
              }
              iVar9 = iVar9 + 1;
              ppFVar13 = ppFVar13 + 8;
              skip_read();
              lVar12 = (long)(int)columns;
              ppFVar10 = column_vector;
            } while (iVar9 <= (int)columns);
          }
LAB_00105635:
          iVar8 = iVar8 + 1;
          ppFVar13 = ppFVar10;
        } while (iVar8 < lines_per_body);
      }
      last_line = 1;
      if (0 < (int)lVar12) {
        iVar8 = 1;
        do {
          while (*(int *)(ppFVar10 + 2) != 0) {
            iVar8 = iVar8 + 1;
            ppFVar10 = ppFVar10 + 8;
            if ((int)lVar12 < iVar8) goto LAB_00105686;
          }
          iVar8 = iVar8 + 1;
          ppFVar10 = ppFVar10 + 8;
          skip_read();
          lVar12 = (long)(int)columns;
        } while (iVar8 <= (int)columns);
LAB_00105686:
        if ((storing_columns != '\0') && (0 < (int)lVar12)) {
          ppFVar13 = column_vector + lVar12 * 8;
          ppFVar10 = column_vector;
          do {
            if (*(int *)(ppFVar10 + 2) != 3) {
              *(undefined4 *)(ppFVar10 + 2) = 2;
            }
            ppFVar10 = ppFVar10 + 8;
          } while (ppFVar10 != ppFVar13);
        }
      }
      reset_status();
      last_line = 0;
      if (files_ready_to_read < 1) {
        uVar15 = dcgettext(0,"starting page number %lu exceeds page count %lu",5);
        error(0,0,uVar15);
        uVar18 = first_page_number;
        if (files_ready_to_read < 1) {
          return;
        }
        break;
      }
      uVar31 = uVar31 + 1;
      uVar18 = first_page_number;
    } while (uVar11 != uVar31);
  }
  ppFVar10 = column_vector;
  cVar4 = truncate_lines;
  iVar9 = chars_per_column;
  bVar23 = numbered_lines;
  iVar8 = col_sep_length;
  cVar6 = storing_columns;
  uVar7 = columns;
  iVar25 = 0;
  if (((truncate_lines != '\0') &&
      (iVar25 = chars_per_column + chars_per_margin, parallel_files != 0)) && (numbered_lines != 0))
  {
    iVar25 = iVar25 + number_width;
  }
  iVar21 = chars_per_margin + col_sep_length;
  local_40 = columns;
  page_number = uVar18;
  if ((int)columns < 2) {
    uVar7 = 1;
    ppFVar13 = column_vector;
  }
  else {
    pcVar30 = print_stored;
    pcVar28 = store_char;
    uVar17 = 1;
    bVar32 = parallel_files ^ 1;
    ppFVar13 = column_vector;
    iVar26 = iVar21;
    if (storing_columns == '\0') {
      pcVar30 = read_line;
      pcVar28 = print_char;
    }
    do {
      while( true ) {
        ppFVar13[4] = (FILE *)pcVar28;
        bVar16 = 0;
        ppFVar13[3] = (FILE *)pcVar30;
        if (bVar23 != 0) {
          bVar16 = uVar17 == 1 | bVar32;
        }
        *(byte *)(ppFVar13 + 7) = bVar16;
        *(int *)((long)ppFVar13 + 0x34) = iVar26;
        if (cVar4 != '\0') break;
        uVar17 = uVar17 + 1;
        iVar25 = 0;
        iVar21 = 0;
        ppFVar13 = ppFVar13 + 8;
        iVar26 = 0;
        if (uVar17 == uVar7) goto LAB_00104e8d;
      }
      iVar21 = iVar8 + iVar25;
      uVar17 = uVar17 + 1;
      iVar25 = iVar21 + iVar9;
      ppFVar13 = ppFVar13 + 8;
      iVar26 = iVar21;
    } while (uVar17 != uVar7);
LAB_00104e8d:
    ppFVar13 = ppFVar10 + (ulong)(uVar7 - 1) * 8;
  }
  pcVar30 = print_char;
  pcVar28 = read_line;
  if ((cVar6 != '\0') && (balance_columns != '\0')) {
    pcVar30 = store_char;
    pcVar28 = print_stored;
  }
  ppFVar13[4] = (FILE *)pcVar30;
  ppFVar13[3] = (FILE *)pcVar28;
  if (bVar23 != 0) {
    bVar23 = parallel_files ^ 1 | uVar7 == 1;
  }
  *(byte *)(ppFVar13 + 7) = bVar23;
  *(int *)((long)ppFVar13 + 0x34) = iVar21;
  line_number = line_count;
  if (cVar6 != '\0') goto LAB_00105292;
  do {
    iVar8 = lines_per_body;
    if (local_40 != 0) {
      ppFVar13 = ppFVar10;
      do {
        iVar9 = 0;
        if (*(int *)(ppFVar13 + 2) == 0) {
          iVar9 = iVar8;
        }
        ppFVar19 = ppFVar13 + 8;
        *(int *)(ppFVar13 + 6) = iVar9;
        ppFVar13 = ppFVar19;
      } while (ppFVar10 + (ulong)local_40 * 8 != ppFVar19);
    }
LAB_00104f41:
    iVar8 = cols_ready_to_print();
    if (iVar8 == 0) {
      return;
    }
    if (extremities != '\0') {
      print_a_header = 1;
    }
    pad_vertically = 0;
    iVar8 = lines_per_body;
    if (double_space != '\0') {
      iVar8 = lines_per_body * 2;
    }
    bVar23 = 0;
    if (iVar8 < 1) {
      if ((iVar8 == 0) && (0 < (int)columns)) goto LAB_001051d9;
LAB_00105210:
      if ((keep_FF != '\0') && (print_a_FF != '\0')) {
        pcVar2 = stdout->_IO_write_ptr;
        if (pcVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar2 + 1;
          *pcVar2 = '\f';
        }
        else {
          __overflow(stdout,0xc);
        }
        print_a_FF = '\0';
      }
    }
    else {
      do {
        iVar9 = cols_ready_to_print();
        pad_vertically = bVar23;
        if (iVar9 == 0) goto LAB_001051f7;
        output_position = 0;
        spaces_not_printed = 0;
        separators_not_printed = 0;
        pad_vertically = 0;
        align_empty_cols = 0;
        empty_line = '\x01';
        if (0 < (int)columns) {
          iVar9 = 1;
          ppFVar10 = column_vector;
          do {
            input_position = 0;
            if ((*(int *)(ppFVar10 + 6) < 1) && (*(int *)(ppFVar10 + 2) != 1)) {
              if (parallel_files != 0) {
                if (empty_line == '\0') {
LAB_00105428:
                  align_column();
                }
                else {
LAB_0010509d:
                  align_empty_cols = 1;
                }
              }
            }
            else {
              padding_not_printed = *(undefined4 *)((long)ppFVar10 + 0x34);
              FF_only = '\0';
              cVar6 = (*(code *)ppFVar10[3])();
              if (cVar6 == '\0') {
                read_rest_of_line();
              }
              bVar32 = pad_vertically;
              iVar25 = *(int *)(ppFVar10 + 6);
              bVar23 = bVar23 | pad_vertically;
              *(int *)(ppFVar10 + 6) = iVar25 + -1;
              if ((iVar25 + -1 < 1) && (iVar25 = cols_ready_to_print(), iVar25 == 0)) {
                if (bVar32 == 0) goto LAB_00105450;
                goto LAB_00105156;
              }
              if ((parallel_files != 0) && (iVar25 = *(int *)(ppFVar10 + 2), iVar25 != 0)) {
                if (empty_line != '\0') goto LAB_0010509d;
                if ((iVar25 == 3) || ((iVar25 == 2 && (FF_only != '\0')))) goto LAB_00105428;
              }
            }
            if (use_col_separator != 0) {
              separators_not_printed = separators_not_printed + 1;
            }
            iVar9 = iVar9 + 1;
            ppFVar10 = ppFVar10 + 8;
          } while (iVar9 <= (int)columns);
          if (pad_vertically != 0) {
LAB_00105156:
            pcVar2 = stdout->_IO_write_ptr;
            if (pcVar2 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar2 + 1;
              *pcVar2 = '\n';
            }
            else {
              __overflow(stdout,10);
            }
            iVar8 = iVar8 + -1;
          }
        }
        iVar9 = cols_ready_to_print();
        if (iVar9 == 0) {
LAB_00105450:
          if (extremities == '\0') break;
        }
        if ((double_space != '\0') && (bVar23 != 0)) {
          pcVar2 = stdout->_IO_write_ptr;
          if (pcVar2 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar2 + 1;
            *pcVar2 = '\n';
          }
          else {
            __overflow(stdout,10);
          }
          iVar8 = iVar8 + -1;
        }
      } while (0 < iVar8);
      pad_vertically = bVar23;
      if ((iVar8 == 0) && (ppFVar10 = column_vector, bVar23 = pad_vertically, 0 < (int)columns)) {
LAB_001051d9:
        pad_vertically = bVar23;
        ppFVar13 = ppFVar10 + (long)(int)columns * 8;
        do {
          if (*(int *)(ppFVar10 + 2) == 0) {
            *(undefined *)((long)ppFVar10 + 0x39) = 1;
          }
          ppFVar10 = ppFVar10 + 8;
        } while (ppFVar10 != ppFVar13);
        iVar8 = 0;
      }
LAB_001051f7:
      if ((pad_vertically == 0) || (extremities == '\0')) goto LAB_00105210;
      if (use_form_feed == '\0') {
        for (iVar8 = iVar8 + 5; iVar8 != 0; iVar8 = iVar8 + -1) {
          pcVar2 = stdout->_IO_write_ptr;
          if (pcVar2 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar2 + 1;
            *pcVar2 = '\n';
          }
          else {
            __overflow(stdout,10);
          }
        }
      }
      else {
        pcVar2 = stdout->_IO_write_ptr;
        if (pcVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar2 + 1;
          *pcVar2 = '\f';
        }
        else {
          __overflow(stdout,0xc);
        }
      }
    }
    page_number = page_number + 1;
    if (last_page_number < page_number) {
      return;
    }
    reset_status();
    local_40 = columns;
    ppFVar10 = column_vector;
  } while (storing_columns == '\0');
LAB_00105292:
  buff_current = 0;
  iVar8 = local_40 - (balance_columns == '\0');
  if (iVar8 < 1) {
    iVar9 = 0;
    lVar12 = 0;
    uVar7 = 0;
  }
  else {
    ppFVar13 = ppFVar10;
    do {
      *(undefined4 *)((long)ppFVar13 + 0x2c) = 0;
      ppFVar13 = ppFVar13 + 8;
    } while (ppFVar10 + (long)iVar8 * 8 != ppFVar13);
    iVar9 = 0;
    iVar25 = 1;
    uVar7 = 0;
    do {
      iVar21 = lines_per_body;
      if (files_ready_to_read == 0) break;
      *(uint *)(ppFVar10 + 5) = uVar7;
      iVar26 = iVar21;
      while (iVar26 != 0) {
        if (*(int *)(ppFVar10 + 2) == 0) {
          input_position = 0;
          cVar6 = read_line();
          if (cVar6 == '\0') {
            read_rest_of_line();
          }
          iVar26 = buff_current;
          pvVar5 = line_vector;
          if ((*(int *)(ppFVar10 + 2) == 0) || (iVar9 != buff_current)) {
            uVar11 = (ulong)uVar7;
            *(int *)((long)ppFVar10 + 0x2c) = *(int *)((long)ppFVar10 + 0x2c) + 1;
            uVar3 = input_position;
            uVar7 = uVar7 + 1;
            *(int *)((long)pvVar5 + uVar11 * 4) = iVar9;
            *(undefined4 *)((long)end_vector + uVar11 * 4) = uVar3;
            iVar9 = iVar26;
          }
        }
        if (iVar21 == 1) break;
        iVar21 = iVar21 + -1;
        iVar26 = files_ready_to_read;
      }
      iVar25 = iVar25 + 1;
      ppFVar10 = ppFVar10 + 8;
    } while (iVar25 <= iVar8);
    lVar12 = (ulong)uVar7 << 2;
    ppFVar10 = column_vector;
  }
  cVar6 = balance_columns;
  *(int *)((long)line_vector + lVar12) = iVar9;
  uVar17 = columns;
  ppFVar13 = ppFVar10;
  if (cVar6 == '\0') {
    if (columns != 1) {
LAB_001053d0:
      ppFVar13 = ppFVar10 + (ulong)(uVar17 - 1) * 8;
      ppFVar19 = ppFVar10;
      do {
        ppFVar20 = ppFVar19 + 8;
        *(undefined4 *)(ppFVar19 + 6) = *(undefined4 *)((long)ppFVar19 + 0x2c);
        ppFVar19 = ppFVar20;
      } while (ppFVar20 != ppFVar13);
      if (cVar6 != '\0') goto LAB_001053f8;
    }
    if (*(int *)(ppFVar13 + 2) == 0) {
      *(int *)(ppFVar13 + 6) = lines_per_body;
    }
    else {
      *(undefined4 *)(ppFVar13 + 6) = 0;
    }
  }
  else {
    if (0 < (int)columns) {
      iVar26 = 0;
      iVar8 = columns + 1;
      iVar21 = 1;
      iVar25 = (int)uVar7 / (int)columns;
      iVar9 = (int)uVar7 % (int)columns;
      ppFVar19 = ppFVar10;
      do {
        *(int *)(ppFVar19 + 5) = iVar26;
        iVar27 = iVar25;
        if (iVar21 <= iVar9) {
          iVar27 = iVar25 + 1;
        }
        iVar21 = iVar21 + 1;
        *(int *)((long)ppFVar19 + 0x2c) = iVar27;
        iVar26 = iVar26 + iVar27;
        ppFVar19 = ppFVar19 + 8;
      } while (iVar8 != iVar21);
    }
    if (uVar17 != 1) goto LAB_001053d0;
LAB_001053f8:
    *(undefined4 *)(ppFVar13 + 6) = *(undefined4 *)((long)ppFVar13 + 0x2c);
  }
  goto LAB_00104f41;
}