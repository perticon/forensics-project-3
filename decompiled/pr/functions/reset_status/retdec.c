void reset_status(void) {
    uint32_t v1 = columns; // 0x33f0
    if (v1 != 0) {
        int64_t v2 = (int64_t)column_vector; // 0x33f6
        int64_t v3 = v2; // 0x3413
        int64_t v4 = files_ready_to_read;
        int32_t * v5 = (int32_t *)(v3 + 16); // 0x3418
        int64_t v6 = v4; // 0x341c
        int64_t v7 = 0; // 0x341c
        if (*v5 == 2) {
            // 0x341e
            *v5 = 0;
            v6 = v4 + 1 & 0xffffffff;
            v7 = 1;
        }
        int64_t v8 = v7;
        int64_t v9 = v6;
        v3 += 64;
        while (v3 != 64 * (int64_t)v1 + v2) {
            // 0x3418
            v4 = v9;
            v5 = (int32_t *)(v3 + 16);
            v6 = v4;
            v7 = v8;
            if (*v5 == 2) {
                // 0x341e
                *v5 = 0;
                v6 = v4 + 1 & 0xffffffff;
                v7 = 1;
            }
            // 0x342d
            v8 = v7;
            v9 = v6;
            v3 += 64;
        }
        if ((char)v8 != 0) {
            // 0x343b
            files_ready_to_read = v9;
        }
    }
    // 0x3441
    if (*(char *)&storing_columns != 0) {
        // 0x344a
        files_ready_to_read = *(int32_t *)&file_name != 3;
    }
}