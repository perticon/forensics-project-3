void reset_status(void)

{
  bool bVar1;
  long lVar2;
  long lVar3;
  uint uVar4;
  long lVar5;
  
  lVar2 = column_vector;
  if (columns != 0) {
    bVar1 = false;
    lVar5 = (ulong)columns * 0x40 + column_vector;
    lVar3 = column_vector;
    uVar4 = files_ready_to_read;
    do {
      if (*(int *)(lVar3 + 0x10) == 2) {
        *(undefined4 *)(lVar3 + 0x10) = 0;
        uVar4 = uVar4 + 1;
        bVar1 = true;
      }
      lVar3 = lVar3 + 0x40;
    } while (lVar3 != lVar5);
    if (bVar1) {
      files_ready_to_read = uVar4;
    }
  }
  if (storing_columns != '\0') {
    files_ready_to_read = (uint)(*(int *)(lVar2 + 0x10) != 3);
  }
  return;
}