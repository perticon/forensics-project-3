void print_white_space(void) {
    int32_t v1 = output_position; // 0x3ee6
    int64_t v2 = v1; // 0x3ee6
    uint32_t v3 = spaces_not_printed; // 0x3eec
    int32_t v4 = v3 + v1; // 0x3ef2
    int64_t v5 = v2; // 0x3ef8
    if (v3 >= 2) {
        uint32_t v6 = chars_per_output_tab; // 0x3efe
        uint32_t v7 = v6 - (int32_t)((0x100000000 * (int64_t)(v1 >> 31) | v2) % (int64_t)v6); // 0x3f09
        v5 = v2;
        if (v3 >= v7) {
            int32_t v8 = v7 + v1;
            int64_t v9 = (int64_t)g12; // 0x3f49
            int64_t * v10 = (int64_t *)(v9 + 40); // 0x3f57
            uint64_t v11 = *v10; // 0x3f57
            if (v11 < *(int64_t *)(v9 + 48)) {
                // 0x3f20
                *v10 = v11 + 1;
                *(char *)v11 = output_tab_char;
            } else {
                // 0x3f61
                function_2640();
            }
            int64_t v12 = v8;
            v5 = v12;
            while (v4 - v8 >= 2) {
                uint32_t v13 = chars_per_output_tab; // 0x3f34
                int32_t v14 = v13 - (int32_t)((0x100000000 * (int64_t)(v8 >> 31) | v12) % (int64_t)v13) + v8; // 0x3f41
                v5 = v12;
                if (v4 < v14) {
                    // break -> 0x3f8c
                    break;
                }
                v8 = v14;
                v9 = (int64_t)g12;
                v10 = (int64_t *)(v9 + 40);
                v11 = *v10;
                if (v11 < *(int64_t *)(v9 + 48)) {
                    // 0x3f20
                    *v10 = v11 + 1;
                    *(char *)v11 = output_tab_char;
                } else {
                    // 0x3f61
                    function_2640();
                }
                // 0x3f2b
                v12 = v8;
                v5 = v12;
            }
        }
    }
    uint64_t v15 = (int64_t)v4; // 0x3ef2
    int64_t v16 = v5 + 1 & 0xffffffff; // 0x3f8c
    int64_t v17 = v16; // 0x3f91
    if (v16 > v15) {
        // 0x3f93
        output_position = v4;
        spaces_not_printed = 0;
        return;
    }
    int64_t v18 = (int64_t)g12; // 0x3f70
    int64_t * v19 = (int64_t *)(v18 + 40); // 0x3f77
    uint64_t v20 = *v19; // 0x3f77
    if (v20 >= *(int64_t *)(v18 + 48)) {
        // 0x3fb0
        function_2640();
    } else {
        // 0x3f81
        *v19 = v20 + 1;
        *(char *)v20 = 32;
    }
    // 0x3f8c
    v17 = v17 + 1 & 0xffffffff;
    while (v17 <= v15) {
        // 0x3f70
        v18 = (int64_t)g12;
        v19 = (int64_t *)(v18 + 40);
        v20 = *v19;
        if (v20 >= *(int64_t *)(v18 + 48)) {
            // 0x3fb0
            function_2640();
        } else {
            // 0x3f81
            *v19 = v20 + 1;
            *(char *)v20 = 32;
        }
        // 0x3f8c
        v17 = v17 + 1 & 0xffffffff;
    }
    // 0x3f93
    output_position = v4;
    spaces_not_printed = 0;
}