void print_white_space(void)

{
  int iVar1;
  byte *pbVar2;
  char *pcVar3;
  byte bVar4;
  int iVar5;
  int iVar6;
  int iVar7;
  
  iVar1 = spaces_not_printed + output_position;
  iVar7 = output_position;
  if ((1 < spaces_not_printed) &&
     (iVar5 = chars_per_output_tab - output_position % chars_per_output_tab,
     iVar6 = output_position + iVar5, iVar5 <= spaces_not_printed)) {
    do {
      iVar7 = iVar6;
      bVar4 = output_tab_char;
      pbVar2 = (byte *)stdout->_IO_write_ptr;
      if (pbVar2 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
        *pbVar2 = bVar4;
      }
      else {
        __overflow(stdout,(uint)output_tab_char);
      }
    } while ((1 < iVar1 - iVar7) &&
            (iVar6 = (chars_per_output_tab - iVar7 % chars_per_output_tab) + iVar7, iVar6 <= iVar1))
    ;
  }
  while (iVar7 = iVar7 + 1, iVar7 <= iVar1) {
    pcVar3 = stdout->_IO_write_ptr;
    if (pcVar3 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar3 + 1;
      *pcVar3 = ' ';
    }
    else {
      __overflow(stdout,0x20);
    }
  }
  output_position = iVar1;
  spaces_not_printed = 0;
  return;
}