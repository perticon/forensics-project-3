undefined8 print_stored(long param_1)

{
  long lVar1;
  long lVar2;
  char *pcVar3;
  long lVar4;
  char *pcVar5;
  long lVar6;
  
  lVar6 = (long)*(int *)(param_1 + 0x28);
  pad_vertically = 1;
  *(int *)(param_1 + 0x28) = *(int *)(param_1 + 0x28) + 1;
  pcVar5 = (char *)(*(int *)(line_vector + lVar6 * 4) + buff);
  pcVar3 = (char *)(*(int *)(line_vector + 4 + lVar6 * 4) + buff);
  if (print_a_header != '\0') {
    print_header();
  }
  lVar2 = column_vector;
  if (*(int *)(param_1 + 0x10) == 1) {
    if (0 < columns) {
      lVar1 = column_vector + (long)columns * 0x40;
      lVar4 = column_vector;
      do {
        *(undefined4 *)(lVar4 + 0x10) = 2;
        lVar4 = lVar4 + 0x40;
      } while (lVar1 != lVar4);
    }
    if (*(int *)(lVar2 + 0x30) < 1) {
      if (extremities != '\0') {
        return 1;
      }
      pad_vertically = 0;
      return 1;
    }
  }
  if (col_sep_length < padding_not_printed) {
    pad_across_to(padding_not_printed - col_sep_length);
    padding_not_printed = 0;
  }
  if (use_col_separator != '\0') {
    print_sep_string();
  }
  for (; pcVar5 != pcVar3; pcVar5 = pcVar5 + 1) {
    print_char((int)*pcVar5);
  }
  if ((spaces_not_printed == 0) &&
     (output_position = *(int *)(end_vector + lVar6 * 4) + *(int *)(param_1 + 0x34),
     *(int *)(param_1 + 0x34) - col_sep_length == chars_per_margin)) {
    output_position = output_position - col_sep_length;
    return 1;
  }
  return 1;
}