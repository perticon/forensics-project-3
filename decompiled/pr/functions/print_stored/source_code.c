print_stored (COLUMN *p)
{
  COLUMN *q;

  int line = p->current_line++;
  char *first = &buff[line_vector[line]];
  /* FIXME
     UMR: Uninitialized memory read:
     * This is occurring while in:
     print_stored   [pr.c:2239]
     * Reading 4 bytes from 0x5148c in the heap.
     * Address 0x5148c is 4 bytes into a malloc'd block at 0x51488 of 676 bytes
     * This block was allocated from:
     malloc         [rtlib.o]
     xmalloc        [xmalloc.c:94]
     init_store_cols [pr.c:1648]
     */
  char *last = &buff[line_vector[line + 1]];

  pad_vertically = true;

  if (print_a_header)
    print_header ();

  if (p->status == FF_FOUND)
    {
      int i;
      for (i = 1, q = column_vector; i <= columns; ++i, ++q)
        q->status = ON_HOLD;
      if (column_vector->lines_to_print <= 0)
        {
          if (!extremities)
            pad_vertically = false;
          return true;		/* print a header only */
        }
    }

  if (col_sep_length < padding_not_printed)
    {
      pad_across_to (padding_not_printed - col_sep_length);
      padding_not_printed = ANYWHERE;
    }

  if (use_col_separator)
    print_sep_string ();

  while (first != last)
    print_char (*first++);

  if (spaces_not_printed == 0)
    {
      output_position = p->start_position + end_vector[line];
      if (p->start_position - col_sep_length == chars_per_margin)
        output_position -= col_sep_length;
    }

  return true;
}