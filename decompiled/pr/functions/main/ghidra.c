void main(int param_1,undefined8 *param_2)

{
  long lVar1;
  void *__ptr;
  char cVar2;
  uint uVar3;
  int iVar4;
  char *pcVar5;
  undefined8 uVar6;
  int *piVar7;
  ulong uVar8;
  ulong uVar9;
  ulong extraout_RDX;
  ulong uVar10;
  long lVar11;
  long lVar12;
  long in_FS_OFFSET;
  undefined auVar13 [16];
  undefined8 uStack136;
  void *local_80;
  ulong local_78;
  uint local_6c;
  long local_68;
  uint local_60;
  char local_5b;
  char local_5a;
  char local_59;
  undefined4 local_50;
  undefined4 local_4c;
  ulong local_48;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_48 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_68 = 0;
  if (1 < param_1) {
    local_68 = xnmalloc((long)(param_1 + -1),8);
  }
  local_80 = (void *)0x0;
  local_5a = '\0';
  local_59 = '\0';
  local_5b = '\0';
  local_60 = 0;
  uVar10 = 0;
LAB_00102a00:
  local_50 = 0xffffffff;
  uVar3 = getopt_long(param_1,param_2,"-0123456789D:FJN:S::TW:abcde::fh:i::l:mn::o:rs::tvw:",
                      long_options,&local_50);
  __ptr = local_80;
  uVar9 = (ulong)uVar3;
  if (uVar3 != 0xffffffff) {
    if (uVar3 - 0x30 < 10) {
LAB_00102a80:
      uVar8 = uVar10 + 1;
      if (local_48 <= uVar8) {
        local_6c = (uint)uVar9;
        local_78 = uVar8;
        local_80 = (void *)x2realloc(local_80,&local_48);
        uVar9 = (ulong)local_6c;
        uVar8 = local_78;
      }
      *(char *)((long)local_80 + uVar10) = (char)uVar9;
      *(undefined *)((long)local_80 + uVar8) = 0;
      uVar10 = uVar8;
      goto LAB_00102a00;
    }
    if (0x81 < (int)uVar3) goto switchD_00102a58_caseD_2;
    switch(uVar9) {
    case 1:
      if (((first_page_number != 0) || (*optarg != '+')) ||
         (cVar2 = first_last_page(0xfffffffe,0x2b,optarg + 1), cVar2 == '\0')) {
        uVar10 = 0;
        *(char **)(local_68 + (ulong)local_60 * 8) = optarg;
        local_60 = local_60 + 1;
        goto LAB_00102a00;
      }
      break;
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 0xb:
    case 0xc:
    case 0xd:
    case 0xe:
    case 0xf:
    case 0x10:
    case 0x11:
    case 0x12:
    case 0x13:
    case 0x14:
    case 0x15:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19:
    case 0x1a:
    case 0x1b:
    case 0x1c:
    case 0x1d:
    case 0x1e:
    case 0x1f:
    case 0x20:
    case 0x21:
    case 0x22:
    case 0x23:
    case 0x24:
    case 0x25:
    case 0x26:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2a:
    case 0x2b:
    case 0x2c:
    case 0x2d:
    case 0x2e:
    case 0x2f:
    case 0x30:
    case 0x31:
    case 0x32:
    case 0x33:
    case 0x34:
    case 0x35:
    case 0x36:
    case 0x37:
    case 0x38:
    case 0x39:
    case 0x3a:
    case 0x3b:
    case 0x3c:
    case 0x3d:
    case 0x3e:
    case 0x3f:
    case 0x40:
    case 0x41:
    case 0x42:
    case 0x43:
    case 0x45:
    case 0x47:
    case 0x48:
    case 0x49:
    case 0x4b:
    case 0x4c:
    case 0x4d:
    case 0x4f:
    case 0x50:
    case 0x51:
    case 0x52:
    case 0x55:
    case 0x56:
    case 0x58:
    case 0x59:
    case 0x5a:
    case 0x5b:
    case 0x5c:
    case 0x5d:
    case 0x5e:
    case 0x5f:
    case 0x60:
    case 0x67:
    case 0x6a:
    case 0x6b:
    case 0x70:
    case 0x71:
    case 0x75:
    case 0x78:
    case 0x79:
    case 0x7a:
    case 0x7b:
    case 0x7c:
    case 0x7d:
    case 0x7e:
    case 0x7f:
      goto switchD_00102a58_caseD_2;
    case 0x44:
      date_format = optarg;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x46:
    case 0x66:
      goto switchD_00102a58_caseD_46;
    case 0x4a:
      join_lines = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x4e:
      uVar10 = 0;
      skip_count = 0;
      uVar6 = dcgettext(0,"\'-N NUMBER\' invalid starting line number",5);
      getoptnum(optarg,0x80000000,&start_line_num,uVar6);
      goto LAB_00102a00;
    case 0x53:
      col_sep_length = 0;
      col_sep_string = &DAT_0010e06b;
      use_col_separator = '\x01';
      local_5a = '\0';
      if (optarg != (char *)0x0) {
        separator_string();
        uVar10 = 0;
        goto LAB_00102a00;
      }
      break;
    case 0x54:
      extremities = 0;
      uVar10 = 0;
      keep_FF = 0;
      goto LAB_00102a00;
    case 0x57:
      uVar10 = 0;
      truncate_lines = '\x01';
      uVar6 = dcgettext(0,"\'-W PAGE_WIDTH\' invalid number of characters",5);
      getoptnum(optarg,1,&chars_per_line,uVar6);
      local_59 = '\0';
      goto LAB_00102a00;
    case 0x61:
      print_across_flag = '\x01';
      uVar10 = 0;
      storing_columns = 0;
      goto LAB_00102a00;
    case 0x62:
      balance_columns = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 99:
      use_cntrl_prefix = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 100:
      double_space = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x65:
      if (optarg != (char *)0x0) {
        getoptarg(optarg,0x65,&input_tab_char,&chars_per_input_tab);
      }
      untabify_input = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x68:
      custom_header = optarg;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x69:
      if (optarg != (char *)0x0) {
        getoptarg(optarg,0x69,&output_tab_char,&chars_per_output_tab);
      }
      tabify_output = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x6c:
      uVar10 = 0;
      uVar6 = dcgettext(0,"\'-l PAGE_LENGTH\' invalid number of lines",5);
      getoptnum(optarg,1,&lines_per_page,uVar6);
      goto LAB_00102a00;
    case 0x6d:
      parallel_files = '\x01';
      uVar10 = 0;
      storing_columns = 0;
      goto LAB_00102a00;
    case 0x6e:
      numbered_lines = 1;
      if (optarg != (char *)0x0) {
        uVar10 = 0;
        getoptarg(optarg,0x6e,&number_separator,&chars_per_number);
        goto LAB_00102a00;
      }
      break;
    case 0x6f:
      uVar10 = 0;
      uVar6 = dcgettext(0,"\'-o MARGIN\' invalid line offset",5);
      getoptnum(optarg,0,&chars_per_margin,uVar6);
      goto LAB_00102a00;
    case 0x72:
      ignore_failed_opens = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x73:
      local_5b = use_col_separator;
      local_5a = use_col_separator;
      if (use_col_separator == '\0') {
        if (optarg == (char *)0x0) {
          local_5a = '\x01';
          uVar10 = 0;
          local_5b = '\x01';
        }
        else {
          separator_string();
          uVar10 = 0;
          local_5a = '\x01';
          local_5b = '\x01';
        }
        goto LAB_00102a00;
      }
      break;
    case 0x74:
      extremities = 0;
      uVar10 = 0;
      keep_FF = 1;
      goto LAB_00102a00;
    case 0x76:
      use_esc_sequence = 1;
      uVar10 = 0;
      goto LAB_00102a00;
    case 0x77:
      uVar6 = dcgettext(0,"\'-w PAGE_WIDTH\' invalid number of characters",5);
      getoptnum(optarg,1,&local_4c,uVar6);
      if (truncate_lines == '\0') {
        chars_per_line = local_4c;
      }
      local_59 = '\x01';
      uVar10 = 0;
      local_5b = '\x01';
      goto LAB_00102a00;
    case 0x80:
      goto switchD_00102a58_caseD_80;
    case 0x81:
      if (optarg == (char *)0x0) goto LAB_00103211;
      cVar2 = first_last_page(local_50,0);
      if (cVar2 == '\0') {
        param_2 = (undefined8 *)quote(optarg);
        uVar6 = dcgettext(0,"invalid page range %s",5);
        error(1,0,uVar6,param_2);
        goto switchD_00102a58_caseD_80;
      }
      break;
    default:
      if (uVar3 == 0xffffff7d) {
        version_etc(stdout,&DAT_0010e09c,"GNU coreutils",Version,"Pete TerMaat","Roland Huebner",0,
                    uVar3 - 0x30);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (uVar3 == 0xffffff7e) {
        usage();
        uVar9 = extraout_RDX;
        goto LAB_00102a80;
      }
switchD_00102a58_caseD_2:
      usage();
switchD_00102a58_caseD_46:
      use_form_feed = 1;
    }
    uVar10 = 0;
    goto LAB_00102a00;
  }
  if (local_80 != (void *)0x0) {
    parse_column_count(local_80);
    free(__ptr);
  }
  if (date_format == (char *)0x0) {
    pcVar5 = getenv("POSIXLY_CORRECT");
    if ((pcVar5 == (char *)0x0) || (cVar2 = hard_locale(2), cVar2 != '\0')) {
      date_format = "%Y-%m-%d %H:%M";
    }
    else {
      date_format = "%b %e %H:%M %Y";
    }
  }
  getenv("TZ");
  localtz = tzalloc();
  cVar2 = parallel_files;
  if (first_page_number == 0) {
    first_page_number = 1;
  }
  if (parallel_files != '\0') {
    if (explicit_columns == '\0') {
      if (print_across_flag == '\0') goto LAB_00102f6e;
      uVar6 = dcgettext(0,"cannot specify both printing across and printing in parallel",5);
      error(1,0,uVar6);
    }
    uVar6 = dcgettext(0,"cannot specify number of columns when printing in parallel",5);
    error(1,0,uVar6);
    goto LAB_001031e5;
  }
LAB_00102f6e:
  if (local_5b != '\0') {
    if (local_59 == '\0') {
      if ((((use_col_separator != '\x01') && (local_5a != '\0')) &&
          ((parallel_files != '\0' || (explicit_columns != '\0')))) &&
         ((truncate_lines != '\0' || (join_lines = 1, 0 < col_sep_length)))) {
LAB_0010312f:
        use_col_separator = '\x01';
      }
    }
    else if ((parallel_files == '\0') && (explicit_columns == '\0')) {
      join_lines = 1;
    }
    else {
      truncate_lines = '\x01';
      if (local_5a != '\0') goto LAB_0010312f;
    }
  }
  if (optind < param_1) {
    param_2 = param_2 + optind;
    uVar3 = (param_1 - optind) + local_60;
    do {
      uVar6 = *param_2;
      uVar10 = (ulong)local_60;
      local_60 = local_60 + 1;
      param_2 = param_2 + 1;
      *(undefined8 *)(local_68 + uVar10 * 8) = uVar6;
      optind = param_1;
    } while (local_60 != uVar3);
  }
  if (local_60 == 0) {
    print_files(0,0);
  }
  else if (cVar2 == '\0') {
    uVar3 = local_60 - 1;
    lVar1 = local_68 + 8;
    lVar11 = local_68;
    do {
      lVar12 = lVar11 + 8;
      print_files(1,lVar11);
      lVar11 = lVar12;
    } while (lVar12 != lVar1 + (ulong)uVar3 * 8);
  }
  else {
    print_files(local_60,local_68);
  }
  free(number_buff);
  free(clump_buff);
  free(column_vector);
  free(line_vector);
  free(end_vector);
  free(buff);
  if ((have_read_stdin == '\0') || (iVar4 = rpl_fclose(), iVar4 != -1)) {
                    /* WARNING: Subroutine does not return */
    exit((uint)failed_opens);
  }
LAB_001031e5:
  uVar6 = dcgettext(0,"standard input",5);
  piVar7 = __errno_location();
  error(1,*piVar7,uVar6);
LAB_00103211:
  uVar6 = dcgettext(0,"\'--pages=FIRST_PAGE[:LAST_PAGE]\' missing argument",5);
  auVar13 = error(1,0,uVar6);
  uVar6 = uStack136;
  uStack136 = SUB168(auVar13,0);
  (*(code *)PTR___libc_start_main_00113fc0)
            (main,uVar6,&local_80,0,0,SUB168(auVar13 >> 0x40,0),&uStack136);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
switchD_00102a58_caseD_80:
  uVar10 = 0;
  parse_column_count(optarg);
  free(local_80);
  local_48 = 0;
  local_80 = (void *)0x0;
  goto LAB_00102a00;
}