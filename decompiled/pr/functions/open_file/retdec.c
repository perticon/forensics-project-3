bool open_file(char * name, int32_t * p) {
    int64_t v1 = (int64_t)p;
    int64_t v2 = (int64_t)name;
    int64_t v3; // 0x3670
    if ((char)v2 != 45) {
        goto lab_0x36e0;
    } else {
        // 0x3680
        if (*(char *)(v2 + 1) != 0) {
            goto lab_0x36e0;
        } else {
            int64_t v4 = function_25d0(); // 0x3694
            int64_t v5 = (int64_t)g13; // 0x3699
            *(char *)&have_read_stdin = 1;
            *(int64_t *)(v1 + 8) = v4;
            *(int64_t *)p = v5;
            v3 = v5;
            if (g13 == NULL) {
                goto lab_0x36fe;
            } else {
                goto lab_0x36b3;
            }
        }
    }
  lab_0x36e0:
    // 0x36e0
    *(int64_t *)(v1 + 8) = v2;
    struct _IO_FILE * v6 = fopen_safer(name, "r"); // 0x36ee
    int64_t v7 = (int64_t)v6; // 0x36ee
    *(int64_t *)p = v7;
    v3 = v7;
    if (v6 != NULL) {
        goto lab_0x36b3;
    } else {
        goto lab_0x36fe;
    }
  lab_0x36b3:
    // 0x36b3
    fadvise((struct _IO_FILE *)v3, 2);
    *(char *)(v1 + 57) = 0;
    *(int32_t *)(v1 + 16) = 0;
    total_files++;
    return true;
  lab_0x36fe:
    // 0x36fe
    *(char *)&failed_opens = 1;
    if (*(char *)&ignore_failed_opens != 0) {
        // 0x3712
        return false;
    }
    // 0x3720
    quotearg_n_style_colon();
    function_2500();
    function_27f0();
    return false;
}