undefined open_file(char *param_1,long *param_2)

{
  long lVar1;
  long lVar2;
  undefined8 uVar3;
  int *piVar4;
  
  if ((*param_1 == '-') && (param_1[1] == '\0')) {
    lVar1 = dcgettext(0,"standard input",5);
    lVar2 = stdin;
    have_read_stdin = 1;
    param_2[1] = lVar1;
    *param_2 = lVar2;
  }
  else {
    param_2[1] = (long)param_1;
    lVar2 = fopen_safer(param_1,"r");
    *param_2 = lVar2;
  }
  if (lVar2 != 0) {
    fadvise(lVar2,2);
    *(undefined *)((long)param_2 + 0x39) = 0;
    *(undefined4 *)(param_2 + 2) = 0;
    total_files = total_files + 1;
    return 1;
  }
  failed_opens = 1;
  if (ignore_failed_opens == '\0') {
    uVar3 = quotearg_n_style_colon(0,3,param_1);
    piVar4 = __errno_location();
    error(0,*piVar4,"%s",uVar3);
    return 0;
  }
  failed_opens = 1;
  return 0;
}