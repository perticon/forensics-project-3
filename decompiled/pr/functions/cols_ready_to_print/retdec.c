int32_t cols_ready_to_print(void) {
    uint32_t v1 = columns; // 0x3330
    if (v1 == 0) {
        // 0x337f
        return 0;
    }
    int64_t v2 = (int64_t)column_vector; // 0x3336
    int64_t v3 = 0; // 0x3352
    int64_t v4 = v2; // 0x3352
    int64_t v5; // 0x3330
    int64_t v6; // 0x3330
    while (true) {
      lab_0x3358:
        // 0x3358
        v6 = v4;
        int64_t v7 = v3;
        if (*(int32_t *)(v6 + 16) < 2) {
            // 0x3373
            v5 = v7 + 1 & 0xffffffff;
            goto lab_0x3376;
        } else {
            // 0x335e
            v5 = v7;
            if (*(char *)&storing_columns == 0) {
                goto lab_0x3376;
            } else {
                // 0x3363
                v5 = v7;
                if (*(int32_t *)(v6 + 44) < 1) {
                    goto lab_0x3376;
                } else {
                    // 0x336c
                    v5 = v7;
                    if (*(int32_t *)(v6 + 48) < 1) {
                        goto lab_0x3376;
                    } else {
                        // 0x3373
                        v5 = v7 + 1 & 0xffffffff;
                        goto lab_0x3376;
                    }
                }
            }
        }
    }
  lab_0x337f:;
    // 0x337f
    int64_t result; // 0x3330
    return result;
  lab_0x3376:
    // 0x3376
    v3 = v5;
    v4 = v6 + 64;
    result = v3;
    if (v4 == 64 * (int64_t)v1 + v2) {
        // break -> 0x337f
        goto lab_0x337f;
    }
    goto lab_0x3358;
}