ulong cols_ready_to_print(void)

{
  ulong uVar1;
  long lVar2;
  long lVar3;
  
  uVar1 = (ulong)columns;
  if (columns != 0) {
    lVar2 = uVar1 * 0x40;
    uVar1 = 0;
    lVar3 = column_vector;
    do {
      if ((*(uint *)(lVar3 + 0x10) < 2) ||
         (((storing_columns != '\0' && (0 < *(int *)(lVar3 + 0x2c))) && (0 < *(int *)(lVar3 + 0x30))
          ))) {
        uVar1 = (ulong)((int)uVar1 + 1);
      }
      lVar3 = lVar3 + 0x40;
    } while (lVar3 != column_vector + lVar2);
  }
  return uVar1;
}