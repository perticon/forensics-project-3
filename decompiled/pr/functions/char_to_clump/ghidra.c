int char_to_clump(byte param_1)

{
  byte *pbVar1;
  uint uVar2;
  int iVar3;
  ushort **ppuVar4;
  int iVar5;
  int iVar6;
  int iVar7;
  long in_FS_OFFSET;
  byte local_34;
  byte local_33;
  byte local_32;
  long local_30;
  
  iVar6 = input_position;
  pbVar1 = clump_buff;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (input_tab_char == param_1) {
    iVar7 = chars_per_input_tab - input_position % chars_per_input_tab;
    if (untabify_input != '\0') {
      if (iVar7 == 0) {
        iVar3 = 0;
        goto LAB_00104390;
      }
      goto LAB_0010431b;
    }
LAB_0010437f:
    *clump_buff = param_1;
    iVar3 = 1;
joined_r0x0010438b:
    if (-1 < iVar7) {
      input_position = iVar6 + iVar7;
      goto LAB_00104390;
    }
    if (iVar6 == 0) {
LAB_001043e0:
      iVar3 = 0;
      input_position = 0;
      goto LAB_00104390;
    }
    iVar5 = -iVar7;
  }
  else {
    if (param_1 == 9) {
      uVar2 = (uint)(input_position >> 0x1f) >> 0x1d;
      iVar7 = (uVar2 - (input_position + uVar2 & 7)) + 8;
      if (untabify_input == '\0') goto LAB_0010437f;
LAB_0010431b:
      memset(clump_buff,0x20,(ulong)(iVar7 - 1) + 1);
      iVar3 = iVar7;
      goto joined_r0x0010438b;
    }
    ppuVar4 = __ctype_b_loc();
    if ((*(byte *)((long)*ppuVar4 + (ulong)param_1 * 2 + 1) & 0x40) != 0) {
      *pbVar1 = param_1;
      iVar3 = 1;
      input_position = input_position + 1;
      goto LAB_00104390;
    }
    if (use_esc_sequence != '\0') {
LAB_001043f0:
      *pbVar1 = 0x5c;
      __sprintf_chk(&local_34,1,4,&DAT_0010e065,param_1);
      pbVar1[1] = local_34;
      pbVar1[2] = local_33;
      pbVar1[3] = local_32;
      iVar3 = 4;
      input_position = input_position + 4;
      goto LAB_00104390;
    }
    if (use_cntrl_prefix != '\0') {
      if (-1 < (char)param_1) {
        *pbVar1 = 0x5e;
        pbVar1[1] = param_1 ^ 0x40;
        iVar3 = 2;
        input_position = input_position + 2;
        goto LAB_00104390;
      }
      goto LAB_001043f0;
    }
    if (param_1 != 8) {
      *pbVar1 = param_1;
      iVar3 = 1;
      goto LAB_00104390;
    }
    *pbVar1 = 8;
    if (input_position == 0) goto LAB_001043e0;
    iVar5 = 1;
    iVar3 = 1;
    iVar7 = -1;
    iVar6 = input_position;
  }
  input_position = 0;
  if (iVar5 < iVar6) {
    input_position = iVar7 + iVar6;
  }
LAB_00104390:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}