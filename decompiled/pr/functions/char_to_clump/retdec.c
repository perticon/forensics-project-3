int32_t char_to_clump(char c) {
    int64_t v1 = __readfsqword(40); // 0x4273
    int64_t v2; // 0x4260
    int64_t result; // 0x4260
    int64_t v3; // 0x4260
    int64_t v4; // 0x4260
    int64_t v5; // 0x42f8
    int64_t v6; // 0x4315
    if (input_tab_char == c) {
        int64_t v7 = input_position; // 0x4360
        uint32_t v8 = chars_per_input_tab; // 0x4366
        uint32_t v9 = v8 - (int32_t)((0x100000000 * (int64_t)(input_position >> 31) | v7) % (int64_t)v8); // 0x4373
        int64_t v10 = v9; // 0x4373
        v5 = v7;
        v6 = v10;
        if (*(char *)&untabify_input != 0) {
            // 0x43d0
            v3 = v7;
            v2 = v10;
            result = 0;
            v4 = v7;
            if (v9 != 0) {
                goto lab_0x431b;
            } else {
                goto lab_0x4390;
            }
        } else {
            goto lab_0x437f;
        }
    } else {
        if (c == 9) {
            // 0x42f8
            v5 = input_position;
            uint32_t v11 = (input_position >> 31) / 0x20000000; // 0x4303
            v6 = (v11 | 8) - (v11 + input_position) % 8;
            v3 = v5;
            v2 = v6;
            if (*(char *)&untabify_input == 0) {
                goto lab_0x437f;
            } else {
                goto lab_0x431b;
            }
        } else {
            int64_t v12 = *(int64_t *)function_28c0(); // 0x42a2
            if ((*(char *)(v12 + (2 * (int64_t)c & 510 | 1)) & 64) != 0) {
                // 0x43b8
                *(char *)clump_buff = c;
                result = 1;
                v4 = input_position + 1;
                goto lab_0x4390;
            } else {
                // 0x42b0
                if (*(char *)&use_esc_sequence != 0) {
                    goto lab_0x43f0;
                } else {
                    // 0x42bd
                    if (*(char *)&use_cntrl_prefix == 0) {
                        // 0x4448
                    } else {
                        if (c < 0) {
                            goto lab_0x43f0;
                        } else {
                            // 0x42d3
                            *(char *)clump_buff = 94;
                            *(char *)(clump_buff + 1) = c ^ 64;
                            result = 2;
                            v4 = input_position + 2;
                            goto lab_0x4390;
                        }
                    }
                }
            }
        }
    }
  lab_0x437f:
    // 0x437f
    *(char *)clump_buff = c;
    int64_t v13 = 1; // 0x438b
    int64_t v14 = v5; // 0x438b
    int64_t v15 = v6; // 0x438b
    int64_t v16 = 1; // 0x438b
    int64_t v17 = v5; // 0x438b
    int64_t v18 = v6; // 0x438b
    if ((int32_t)v6 < 0) {
        goto lab_0x4339;
    } else {
        goto lab_0x438d;
    }
  lab_0x431b:
    // 0x431b
    function_2680();
    v13 = v2;
    v14 = v3;
    v15 = v2;
    v16 = v2;
    v17 = v3;
    v18 = v2;
    if ((int32_t)v2 >= 0) {
        goto lab_0x438d;
    } else {
        goto lab_0x4339;
    }
  lab_0x4390:
    // 0x4390
    input_position = v4;
    if (v1 != __readfsqword(40)) {
        // 0x4484
        return function_2600();
    }
    // 0x43aa
    return result;
  lab_0x4339:;
    int32_t v19 = v14;
    result = 0;
    v4 = 0;
    int32_t v20; // 0x4260
    int64_t v21; // 0x4260
    int64_t v22; // 0x4260
    int64_t v23; // 0x4260
    int64_t v24; // 0x4260
    if (v19 == 0) {
        goto lab_0x4390;
    } else {
        // 0x4341
        v20 = v19;
        v22 = v13;
        v24 = -v15 & 0xffffffff;
        v23 = v14;
        v21 = v15;
        goto lab_0x4346;
    }
  lab_0x438d:
    // 0x438d
    result = v16;
    v4 = v18 + v17 & 0xffffffff;
    goto lab_0x4390;
  lab_0x43f0:
    // 0x43f0
    *(char *)clump_buff = 92;
    function_28d0();
    result = 4;
    v4 = input_position + 4;
    goto lab_0x4390;
  lab_0x4346:;
    int64_t v25 = v23;
    int32_t v26 = v24; // 0x4349
    int32_t v27 = v26 - v20; // 0x4349
    int64_t v28 = v27 < 0 == ((v27 ^ v26) & (int32_t)(v25 ^ v24)) < 0 ? 0 : v21 + v25 & 0xffffffff; // 0x4350
    result = v22;
    v4 = v28;
    goto lab_0x4390;
}