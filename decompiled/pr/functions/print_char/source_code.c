print_char (char c)
{
  if (tabify_output)
    {
      if (c == ' ')
        {
          ++spaces_not_printed;
          return;
        }
      else if (spaces_not_printed > 0)
        print_white_space ();

      /* Nonprintables are assumed to have width 0, except '\b'. */
      if (! isprint (to_uchar (c)))
        {
          if (c == '\b')
            --output_position;
        }
      else
        ++output_position;
    }
  putchar (c);
}