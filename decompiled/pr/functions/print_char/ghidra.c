void print_char(byte param_1)

{
  byte *pbVar1;
  ushort **ppuVar2;
  
  if (tabify_output != '\0') {
    if (param_1 == 0x20) {
      spaces_not_printed = spaces_not_printed + 1;
      return;
    }
    if (0 < spaces_not_printed) {
      print_white_space();
    }
    ppuVar2 = __ctype_b_loc();
    if ((*(byte *)((long)*ppuVar2 + (ulong)param_1 * 2 + 1) & 0x40) == 0) {
      if (param_1 == 8) {
        output_position = output_position + -1;
      }
    }
    else {
      output_position = output_position + 1;
    }
  }
  pbVar1 = (byte *)stdout->_IO_write_ptr;
  if (pbVar1 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = (char *)(pbVar1 + 1);
    *pbVar1 = param_1;
    return;
  }
  __overflow(stdout,(uint)param_1);
  return;
}