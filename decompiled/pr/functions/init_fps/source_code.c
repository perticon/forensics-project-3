init_fps (int number_of_files, char **av)
{
  COLUMN *p;

  total_files = 0;

  free (column_vector);
  column_vector = xnmalloc (columns, sizeof (COLUMN));

  if (parallel_files)
    {
      int files_left = number_of_files;
      for (p = column_vector; files_left--; ++p, ++av)
        {
          if (! open_file (*av, p))
            {
              --p;
              --columns;
            }
        }
      if (columns == 0)
        return false;
      init_header ("", -1);
    }
  else
    {
      p = column_vector;
      if (number_of_files > 0)
        {
          if (! open_file (*av, p))
            return false;
          init_header (*av, fileno (p->fp));
          p->lines_stored = 0;
        }
      else
        {
          p->name = _("standard input");
          p->fp = stdin;
          have_read_stdin = true;
          p->status = OPEN;
          p->full_page_printed = false;
          ++total_files;
          init_header ("", -1);
          p->lines_stored = 0;
        }

      char const *firstname = p->name;
      FILE *firstfp = p->fp;
      int i;
      for (i = columns - 1, ++p; i; --i, ++p)
        {
          p->name = firstname;
          p->fp = firstfp;
          p->status = OPEN;
          p->full_page_printed = false;
          p->lines_stored = 0;
        }
    }
  files_ready_to_read = total_files;
  return true;
}