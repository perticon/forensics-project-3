balance (int total_stored)
{
  COLUMN *p;
  int i, lines;
  int first_line = 0;

  for (i = 1, p = column_vector; i <= columns; ++i, ++p)
    {
      lines = total_stored / columns;
      if (i <= total_stored % columns)
        ++lines;

      p->lines_stored = lines;
      p->current_line = first_line;

      first_line += lines;
    }
}