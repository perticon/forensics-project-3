void store_char(undefined param_1)

{
  if (buff_current < buff_allocated) {
    *(undefined *)(buff + (ulong)buff_current) = param_1;
    buff_current = buff_current + 1;
    return;
  }
  buff = x2realloc(buff,&buff_allocated);
  *(undefined *)(buff + (ulong)buff_current) = param_1;
  buff_current = buff_current + 1;
  return;
}