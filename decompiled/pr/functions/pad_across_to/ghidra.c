void pad_across_to(int param_1)

{
  char *pcVar1;
  int iVar2;
  
  if (tabify_output != '\0') {
    spaces_not_printed = param_1 - output_position;
    return;
  }
  for (iVar2 = output_position + 1; iVar2 <= param_1; iVar2 = iVar2 + 1) {
    pcVar1 = stdout->_IO_write_ptr;
    if (pcVar1 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = ' ';
    }
    else {
      __overflow(stdout,0x20);
    }
  }
  output_position = param_1;
  return;
}