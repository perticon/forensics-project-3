void pad_across_to(int32_t position) {
    int32_t v1 = output_position; // 0x3e7f
    if (*(char *)&tabify_output != 0) {
        // 0x3ec0
        spaces_not_printed = position - v1;
        return;
    }
    int64_t v2 = v1 + 1; // 0x3e87
    int64_t v3 = v2; // 0x3e8c
    if ((int64_t)position < v2) {
        // 0x3eb3
        output_position = position;
        return;
    }
    int64_t v4 = (int64_t)g12; // 0x3e90
    int64_t * v5 = (int64_t *)(v4 + 40); // 0x3e97
    uint64_t v6 = *v5; // 0x3e97
    if (v6 >= *(int64_t *)(v4 + 48)) {
        // 0x3ed0
        function_2640();
    } else {
        // 0x3ea1
        *v5 = v6 + 1;
        *(char *)v6 = 32;
    }
    // 0x3eac
    v3 = v3 + 1 & 0xffffffff;
    while (v3 <= (int64_t)position) {
        // 0x3e90
        v4 = (int64_t)g12;
        v5 = (int64_t *)(v4 + 40);
        v6 = *v5;
        if (v6 >= *(int64_t *)(v4 + 48)) {
            // 0x3ed0
            function_2640();
        } else {
            // 0x3ea1
            *v5 = v6 + 1;
            *(char *)v6 = 32;
        }
        // 0x3eac
        v3 = v3 + 1 & 0xffffffff;
    }
    // 0x3eb3
    output_position = position;
}