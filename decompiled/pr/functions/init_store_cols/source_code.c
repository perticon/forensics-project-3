init_store_cols (void)
{
  int total_lines, total_lines_1, chars_per_column_1, chars_if_truncate;
  if (INT_MULTIPLY_WRAPV (lines_per_body, columns, &total_lines)
      || INT_ADD_WRAPV (total_lines, 1, &total_lines_1)
      || INT_ADD_WRAPV (chars_per_column, 1, &chars_per_column_1)
      || INT_MULTIPLY_WRAPV (total_lines, chars_per_column_1,
                             &chars_if_truncate))
    integer_overflow ();

  free (line_vector);
  /* FIXME: here's where it was allocated.  */
  line_vector = xnmalloc (total_lines_1, sizeof *line_vector);

  free (end_vector);
  end_vector = xnmalloc (total_lines, sizeof *end_vector);

  free (buff);
  buff = xnmalloc (chars_if_truncate, use_col_separator + 1);
  buff_allocated = chars_if_truncate;  /* Tune this. */
  buff_allocated *= use_col_separator + 1;
}