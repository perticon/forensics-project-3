void close_file(FILE **param_1)

{
  int iVar1;
  int *piVar2;
  long lVar3;
  undefined8 uVar4;
  long lVar5;
  int iVar6;
  
  if (*(int *)(param_1 + 2) == 3) {
    return;
  }
  piVar2 = __errno_location();
  iVar6 = *piVar2;
  if ((*(byte *)&(*param_1)->_flags & 0x20) == 0) {
    iVar6 = 0;
  }
  iVar1 = fileno(*param_1);
  if (iVar1 == 0) {
    clearerr_unlocked(*param_1);
LAB_00103ac7:
    if (iVar6 == 0) {
      if (parallel_files == '\0') {
        lVar5 = (ulong)columns * 0x40 + column_vector;
        lVar3 = column_vector;
        if (columns != 0) {
          do {
            *(undefined4 *)(lVar3 + 0x10) = 3;
            if (*(int *)(lVar3 + 0x2c) == 0) {
              *(undefined4 *)(lVar3 + 0x30) = 0;
            }
            lVar3 = lVar3 + 0x40;
          } while (lVar3 != lVar5);
        }
        files_ready_to_read = files_ready_to_read + -1;
        return;
      }
      goto LAB_00103b60;
    }
  }
  else {
    iVar1 = rpl_fclose();
    if (iVar1 == 0) goto LAB_00103ac7;
    if (iVar6 == 0) {
      iVar6 = *piVar2;
      goto LAB_00103ac7;
    }
  }
  uVar4 = quotearg_n_style_colon(0,3,param_1[1]);
  error(1,iVar6,"%s",uVar4);
LAB_00103b60:
  *(undefined4 *)(param_1 + 2) = 3;
  *(undefined4 *)(param_1 + 6) = 0;
  files_ready_to_read = files_ready_to_read + -1;
  return;
}