void close_file(int32_t * p) {
    int64_t v1 = (int64_t)p;
    int32_t * v2 = (int32_t *)(v1 + 16);
    if (*v2 == 3) {
        // 0x3b80
        return;
    }
    int32_t * v3 = (int32_t *)function_2500(); // 0x3aa9
    int64_t v4 = (v1 & 32) == 0 ? 0 : (int64_t)*v3; // 0x3ab3
    int64_t v5; // 0x3a90
    if ((int32_t)function_2730() != 0) {
        // 0x3b28
        v5 = v4;
        if (rpl_fclose((struct _IO_FILE *)p) == 0) {
            goto lab_0x3ac7;
        } else {
            if (v4 == 0) {
                // 0x3b88
                v5 = (int64_t)*v3;
                goto lab_0x3ac7;
            } else {
                goto lab_0x3b35;
            }
        }
    } else {
        // 0x3ac2
        function_2590();
        v5 = v4;
        goto lab_0x3ac7;
    }
  lab_0x3ac7:
    // 0x3ac7
    if ((int32_t)v5 != 0) {
        goto lab_0x3b35;
      lab_0x3b35:
        // 0x3b35
        quotearg_n_style_colon();
        function_27f0();
        // 0x3b60
        *v2 = 3;
        *(int32_t *)(v1 + 48) = 0;
        files_ready_to_read--;
        return;
    }
    // 0x3acb
    if (*(char *)&parallel_files != 0) {
        // 0x3b60
        *v2 = 3;
        *(int32_t *)(v1 + 48) = 0;
        files_ready_to_read--;
        return;
    }
    int64_t v6 = (int64_t)column_vector; // 0x3ade
    if (columns == 0) {
        // 0x3b16
        files_ready_to_read--;
        return;
    }
    *(int32_t *)(v6 + 16) = 3;
    if (*(int32_t *)(v6 + 44) == 0) {
        // 0x3b06
        *(int32_t *)(v6 + 48) = 0;
    }
    int64_t v7 = v6 + 64; // 0x3b0d
    while (v7 != 64 * (int64_t)columns + v6) {
        int64_t v8 = v7;
        *(int32_t *)(v8 + 16) = 3;
        if (*(int32_t *)(v8 + 44) == 0) {
            // 0x3b06
            *(int32_t *)(v8 + 48) = 0;
        }
        // 0x3b0d
        v7 = v8 + 64;
    }
    // 0x3b16
    files_ready_to_read--;
}