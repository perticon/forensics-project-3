close_file (COLUMN *p)
{
  COLUMN *q;
  int i;

  if (p->status == CLOSED)
    return;

  int err = errno;
  if (!ferror (p->fp))
    err = 0;
  if (fileno (p->fp) == STDIN_FILENO)
    clearerr (p->fp);
  else if (fclose (p->fp) != 0 && !err)
    err = errno;
  if (err)
    die (EXIT_FAILURE, err, "%s", quotef (p->name));

  if (!parallel_files)
    {
      for (q = column_vector, i = columns; i; ++q, --i)
        {
          q->status = CLOSED;
          if (q->lines_stored == 0)
            {
              q->lines_to_print = 0;
            }
        }
    }
  else
    {
      p->status = CLOSED;
      p->lines_to_print = 0;
    }

  --files_ready_to_read;
}