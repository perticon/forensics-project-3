getoptnum (char const *n_str, int min, int *num, char const *err)
{
  intmax_t tnum = xdectoimax (n_str, min, INT_MAX, "", err, 0);
  *num = tnum;
}