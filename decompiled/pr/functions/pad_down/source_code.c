pad_down (unsigned int lines)
{
  if (use_form_feed)
    putchar ('\f');
  else
    for (unsigned int i = lines; i; --i)
      putchar ('\n');
}