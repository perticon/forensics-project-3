undefined8 main(int param_1,undefined8 *param_2)

{
  int iVar1;
  __mode_t __mask;
  uint __mode;
  undefined8 uVar2;
  void *__ptr;
  undefined8 uVar3;
  int *piVar4;
  char *pcVar5;
  long lVar6;
  undefined auVar7 [16];
  undefined8 uStack72;
  undefined8 local_40 [2];
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  lVar6 = 0;
  do {
    while( true ) {
      iVar1 = getopt_long(param_1,param_2,&DAT_001070d6,longopts,0);
      if (iVar1 == -1) {
        if (optind != param_1) {
          if (lVar6 == 0) {
            __mode = 0x1b6;
          }
          else {
            __ptr = (void *)mode_compile();
            if (__ptr == (void *)0x0) goto LAB_0010290d;
            __mask = umask(0);
            umask(__mask);
            __mode = mode_adjust(0x1b6,0,__mask,__ptr,0);
            free(__ptr);
            if ((__mode & 0xfffffe00) != 0) goto LAB_001028e9;
          }
          uVar2 = 0;
          goto joined_r0x001027e1;
        }
        uVar2 = dcgettext(0,"missing operand",5);
        error(0,0,uVar2);
        goto LAB_001028df;
      }
      if (iVar1 != 0x5a) break;
LAB_00102738:
      if (optarg != 0) {
        uVar2 = dcgettext(0,
                          "warning: ignoring --context; it requires an SELinux/SMACK-enabled kernel"
                          ,5);
        error(0,0,uVar2);
      }
    }
    if (iVar1 < 0x5b) {
      if (iVar1 == -0x83) {
        version_etc(stdout,"mkfifo","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar1 == -0x82) {
        usage();
        goto LAB_00102738;
      }
      break;
    }
    lVar6 = optarg;
  } while (iVar1 == 0x6d);
LAB_001028df:
  usage(1);
LAB_001028e9:
  uVar2 = dcgettext(0,"mode must specify only file permission bits",5);
  error(1,0,uVar2);
LAB_0010290d:
  uVar2 = dcgettext(0,"invalid mode",5);
  auVar7 = error(1,0,uVar2);
  uVar2 = uStack72;
  uStack72 = SUB168(auVar7,0);
  (*(code *)PTR___libc_start_main_0010afd0)
            (main,uVar2,local_40,0,0,SUB168(auVar7 >> 0x40,0),&uStack72);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
joined_r0x001027e1:
  if (param_1 <= optind) {
    return uVar2;
  }
  iVar1 = mkfifo((char *)param_2[optind],__mode);
  if (iVar1 == 0) {
    if ((lVar6 != 0) && (iVar1 = lchmod((char *)param_2[optind],__mode), iVar1 != 0)) {
      local_40[0] = quotearg_style(4,param_2[optind]);
      pcVar5 = "cannot set permissions of %s";
      goto LAB_0010284f;
    }
  }
  else {
    local_40[0] = quotearg_style(4,param_2[optind]);
    pcVar5 = "cannot create fifo %s";
LAB_0010284f:
    uVar3 = dcgettext(0,pcVar5,5);
    piVar4 = __errno_location();
    uVar2 = 1;
    error(0,*piVar4,uVar3,local_40[0]);
  }
  optind = optind + 1;
  goto joined_r0x001027e1;
}