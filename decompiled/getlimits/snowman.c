
struct s0 {
    unsigned char f0;
    unsigned char f1;
    signed char f2;
};

struct s1 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s0* fun_2430(struct s0* rdi, ...);

struct s0* decimal_absval_add_one(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8) {
    void* rax9;
    uint32_t ebp10;
    struct s0* rbx11;
    struct s0* rax12;
    void* rax13;
    struct s0* rdx14;
    uint32_t eax15;
    uint32_t eax16;
    struct s0* rax17;

    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    ebp10 = rdi->f1;
    *reinterpret_cast<unsigned char*>(&rax9) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&ebp10) == 45);
    rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rax9) + 1);
    rax12 = fun_2430(rbx11);
    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx11) + 0xffffffffffffffff) = 48;
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) + reinterpret_cast<unsigned char>(rbx11));
    rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax13) + 0xffffffffffffffff);
    eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax13) + 0xffffffffffffffff);
    if (*reinterpret_cast<signed char*>(&eax15) == 57) {
        do {
            *reinterpret_cast<unsigned char*>(&rdx14->f0) = 48;
            eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx14) + 0xffffffffffffffff);
            rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx14) - 1);
        } while (*reinterpret_cast<signed char*>(&eax15) == 57);
    }
    eax16 = eax15 + 1;
    *reinterpret_cast<unsigned char*>(&rdx14->f0) = *reinterpret_cast<unsigned char*>(&eax16);
    rax17 = rdx14;
    if (reinterpret_cast<unsigned char>(rbx11) <= reinterpret_cast<unsigned char>(rdx14)) {
        rax17 = rbx11;
    }
    if (*reinterpret_cast<signed char*>(&ebp10) == 45) {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) = 45;
        rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax17) - 1);
    }
    return rax17;
}

struct s0* g28;

void ftoastr(void* rdi, int64_t rsi, int64_t rdx);

void fun_23b0(void* rdi, int64_t rsi, int64_t rdx);

void fun_2440();

void dtoastr(void* rdi, int64_t rsi, int64_t rdx);

void ldtoastr(void* rdi, int64_t rsi, int64_t rdx);

void print_FLT(unsigned char* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8) {
    void* rsp9;
    struct s0* rax10;
    void* rax11;
    void* rsp12;
    struct s0* rax13;
    void* rax14;
    struct s0* rax15;
    void* rbp16;
    void* rax17;
    int64_t v18;
    int64_t v19;

    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 48);
    rax10 = g28;
    ftoastr(rsp9, 31, 1);
    fun_23b0(rsp9, 31, 1);
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<unsigned char>(g28));
    if (!rax11) {
        return;
    }
    fun_2440();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 48);
    rax13 = g28;
    dtoastr(rsp12, 40, 1);
    fun_23b0(rsp12, 40, 1);
    rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax13) - reinterpret_cast<unsigned char>(g28));
    if (!rax14) 
        goto addr_32d4_5;
    fun_2440();
    rax15 = g28;
    rbp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 64);
    ldtoastr(rbp16, 45, 1);
    fun_23b0(rbp16, 45, 1);
    rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax15) - reinterpret_cast<unsigned char>(g28));
    if (!rax17) 
        goto addr_332f_8;
    fun_2440();
    addr_332f_8:
    goto v18;
    addr_32d4_5:
    goto v19;
}

void print_DBL(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8) {
    void* rsp9;
    struct s0* rax10;
    void* rax11;
    struct s0* rax12;
    void* rbp13;
    void* rax14;
    int64_t v15;

    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 48);
    rax10 = g28;
    dtoastr(rsp9, 40, 1);
    fun_23b0(rsp9, 40, 1);
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<unsigned char>(g28));
    if (!rax11) {
        return;
    }
    fun_2440();
    rax12 = g28;
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 64);
    ldtoastr(rbp13, 45, 1);
    fun_23b0(rbp13, 45, 1);
    rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) - reinterpret_cast<unsigned char>(g28));
    if (!rax14) 
        goto addr_332f_5;
    fun_2440();
    addr_332f_5:
    goto v15;
}

void print_LDBL(unsigned char* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    struct s0* rax11;
    void* rbp12;
    void* rax13;

    rax11 = g28;
    rbp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 64);
    ldtoastr(rbp12, 45, 1);
    fun_23b0(rbp12, 45, 1);
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(g28));
    if (rax13) {
        fun_2440();
    } else {
        return;
    }
}

struct s2 {
    void* f0;
    signed char[4] pad8;
    struct s2* f8;
    struct s2* f10;
    struct s2* f18;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int64_t fun_2420();

int64_t fun_2360(struct s2* rdi, ...);

void* quotearg_buffer_restyled(struct s2* rdi, void* rsi, struct s2* rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2420();
    if (r8d > 10) {
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8660 + rax11 * 4) + 0x8660;
    }
}

struct s3 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_2370(struct s2* rdi);

struct s2* slotvec = reinterpret_cast<struct s2*>(0xc070);

uint32_t nslots = 1;

struct s2* xpalloc();

void fun_24a0();

struct s4 {
    void* f0;
    signed char[4] pad8;
    struct s2* f8;
};

void fun_2350(struct s2* rdi);

struct s2* xcharalloc(void* rdi, ...);

struct s2* quotearg_n_options(struct s2* rdi, struct s2* rsi, int64_t rdx, struct s3* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s2* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s2* rax12;
    struct s2* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s4* rbx16;
    uint32_t r15d17;
    void* rsi18;
    struct s2* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void* rax23;
    void* rsi24;
    struct s2* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x53cf;
    rax8 = fun_2370(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
        fun_2360(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6b01]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x545b;
            fun_24a0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s4*>((rbx5 << 4) + reinterpret_cast<int64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (reinterpret_cast<uint32_t>(rsi18) <= reinterpret_cast<uint32_t>(rax23)) {
            rsi24 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc100) {
                fun_2350(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x54ea);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2440();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s5 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s5* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s5* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x85eb);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s0*>(0x85e4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x85ef);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s0*>(0x85e0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbe38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbe38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2333() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_2343() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_2353() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2363() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2373() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2383() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2393() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23a3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x20a0;

void fun_23b3() {
    __asm__("cli ");
    goto puts;
}

int64_t reallocarray = 0x20b0;

void fun_23c3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t strtod = 0x20c0;

void fun_23d3() {
    __asm__("cli ");
    goto strtod;
}

int64_t textdomain = 0x20d0;

void fun_23e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_23f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_2403() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2413() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_2423() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_2433() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_2443() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_2453() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_2463() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2160;

void fun_2473() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_2483() {
    __asm__("cli ");
    goto lseek;
}

int64_t strtof = 0x2180;

void fun_2493() {
    __asm__("cli ");
    goto strtof;
}

int64_t memset = 0x2190;

void fun_24a3() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x21a0;

void fun_24b3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24c3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24d3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_24e3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_24f3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21f0;

void fun_2503() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2200;

void fun_2513() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2210;

void fun_2523() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2220;

void fun_2533() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2230;

void fun_2543() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2240;

void fun_2553() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2250;

void fun_2563() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2260;

void fun_2573() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2270;

void fun_2583() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strtold = 0x2280;

void fun_2593() {
    __asm__("cli ");
    goto strtold;
}

int64_t error = 0x2290;

void fun_25a3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22a0;

void fun_25b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22b0;

void fun_25c3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22c0;

void fun_25d3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22d0;

void fun_25e3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x22e0;

void fun_25f3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x22f0;

void fun_2603() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2300;

void fun_2613() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2310;

void fun_2623() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2320;

void fun_2633() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(int64_t rdi);

struct s0* fun_2570(int64_t rdi, ...);

void fun_2400(int64_t rdi, int64_t rsi);

void fun_23e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void parse_long_options(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

void fun_2630(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

void fun_2580(unsigned char* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, struct s0* a10, int64_t a11, struct s0* a12, int64_t a13, struct s0* a14, int64_t a15, struct s0* a16, int64_t a17, struct s0* a18, int64_t a19, int64_t a20);

unsigned char g1;

unsigned char g837e = 68;

unsigned char g8388 = 68;

int64_t fun_2683(int32_t edi, int64_t* rsi) {
    int64_t rdi3;
    struct s0* rax4;
    int64_t rdi5;
    void* rsp6;
    struct s0* r12_7;
    struct s1* rbp8;
    int64_t v9;
    struct s0* v10;
    int64_t v11;
    int64_t v12;
    struct s0* rbp13;
    int64_t r12_14;
    struct s0* r13_15;
    int64_t r14_16;
    int64_t v17;
    int64_t v18;
    struct s0* rax19;
    int64_t v20;
    struct s0* v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    struct s0* rax32;
    int64_t v33;
    struct s0* v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    struct s0* v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    struct s0* rax45;
    int64_t v46;
    struct s0* v47;
    int64_t v48;
    int64_t v49;
    int64_t v50;
    int64_t v51;
    int64_t v52;
    struct s0* v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    int64_t v57;
    struct s0* rax58;
    int64_t v59;
    struct s0* v60;
    int64_t v61;
    int64_t v62;
    int64_t v63;
    int64_t v64;
    int64_t v65;
    struct s0* v66;
    int64_t v67;
    int64_t v68;
    int64_t v69;
    int64_t v70;
    struct s0* rax71;
    int64_t v72;
    struct s0* v73;
    int64_t v74;
    int64_t v75;
    int64_t v76;
    int64_t v77;
    int64_t v78;
    struct s0* v79;
    int64_t v80;
    int64_t v81;
    int64_t v82;
    int64_t v83;
    struct s0* rax84;
    int64_t v85;
    struct s0* v86;
    int64_t v87;
    int64_t v88;
    int64_t v89;
    int64_t v90;
    int64_t v91;
    struct s0* v92;
    int64_t v93;
    int64_t v94;
    int64_t v95;
    int64_t v96;
    struct s0* rax97;
    int64_t v98;
    struct s0* v99;
    int64_t v100;
    int64_t v101;
    int64_t v102;
    int64_t v103;
    int64_t v104;
    struct s0* v105;
    int64_t v106;
    int64_t v107;
    int64_t v108;
    int64_t v109;
    struct s0* rax110;
    int64_t v111;
    struct s0* v112;
    int64_t v113;
    int64_t v114;
    int64_t v115;
    int64_t v116;
    int64_t v117;
    struct s0* v118;
    int64_t v119;
    int64_t v120;
    int64_t v121;
    int64_t v122;
    struct s0* rax123;
    int64_t v124;
    struct s0* v125;
    int64_t v126;
    int64_t v127;
    int64_t v128;
    int64_t v129;
    int64_t v130;
    struct s0* v131;
    int64_t v132;
    int64_t v133;
    int64_t v134;
    int64_t v135;
    struct s0* rax136;
    int64_t v137;
    struct s0* v138;
    int64_t v139;
    int64_t v140;
    int64_t v141;
    int64_t v142;
    int64_t v143;
    struct s0* v144;
    int64_t v145;
    int64_t v146;
    int64_t v147;
    int64_t v148;
    struct s0* rax149;
    int64_t v150;
    struct s0* v151;
    int64_t v152;
    int64_t v153;
    int64_t v154;
    int64_t v155;
    int64_t v156;
    struct s0* v157;
    int64_t v158;
    int64_t v159;
    int64_t v160;
    int64_t v161;
    struct s0* rax162;
    int64_t v163;
    struct s0* v164;
    int64_t v165;
    int64_t v166;
    int64_t v167;
    int64_t v168;
    int64_t v169;
    struct s0* v170;
    int64_t v171;
    int64_t v172;
    int64_t v173;
    int64_t v174;
    struct s0* rax175;
    int64_t v176;
    struct s0* v177;
    int64_t v178;
    int64_t v179;
    int64_t v180;
    int64_t v181;
    int64_t v182;
    struct s0* v183;
    int64_t v184;
    int64_t v185;
    int64_t v186;
    int64_t v187;
    struct s0* rax188;
    int64_t v189;
    struct s0* v190;
    int64_t v191;
    int64_t v192;
    int64_t v193;
    int64_t v194;
    int64_t v195;
    struct s0* v196;
    int64_t v197;
    int64_t v198;
    int64_t v199;
    int64_t v200;
    struct s0* rax201;
    int64_t v202;
    struct s0* v203;
    int64_t v204;
    int64_t v205;
    int64_t v206;
    int64_t v207;
    int64_t v208;
    struct s0* v209;
    int64_t v210;
    int64_t v211;
    int64_t v212;
    int64_t v213;
    struct s0* rax214;
    int64_t v215;
    struct s0* v216;
    int64_t v217;
    int64_t v218;
    int64_t v219;
    int64_t v220;
    int64_t v221;
    struct s0* v222;
    int64_t v223;
    int64_t v224;
    int64_t v225;
    int64_t v226;
    struct s0* rax227;
    int64_t v228;
    struct s0* v229;
    int64_t v230;
    int64_t v231;
    int64_t v232;
    int64_t v233;
    int64_t v234;
    struct s0* v235;
    int64_t v236;
    int64_t v237;
    int64_t v238;
    int64_t v239;
    struct s0* rax240;
    int64_t v241;
    struct s0* v242;
    int64_t v243;
    int64_t v244;
    int64_t v245;
    int64_t v246;
    int64_t v247;
    struct s0* v248;
    int64_t v249;
    int64_t v250;
    int64_t v251;
    int64_t v252;
    struct s0* rax253;
    int64_t v254;
    struct s0* v255;
    int64_t v256;
    int64_t v257;
    int64_t v258;
    int64_t v259;
    int64_t v260;
    struct s0* v261;
    int64_t v262;
    int64_t v263;
    int64_t v264;
    int64_t v265;
    struct s0* rax266;
    int64_t v267;
    struct s0* v268;
    int64_t v269;
    int64_t v270;
    int64_t v271;
    int64_t v272;
    int64_t v273;
    struct s0* v274;
    int64_t v275;
    int64_t v276;
    int64_t v277;
    int64_t v278;
    struct s0* rax279;
    int64_t v280;
    struct s0* v281;
    int64_t v282;
    int64_t v283;
    int64_t v284;
    int64_t v285;
    int64_t v286;
    struct s0* v287;
    int64_t v288;
    int64_t v289;
    int64_t v290;
    int64_t v291;
    struct s0* rax292;
    int64_t v293;
    struct s0* v294;
    int64_t v295;
    int64_t v296;
    int64_t v297;
    int64_t v298;
    int64_t v299;
    struct s0* v300;
    int64_t v301;
    int64_t v302;
    int64_t v303;
    int64_t v304;
    struct s0* rax305;
    int64_t v306;
    struct s0* v307;
    int64_t v308;
    int64_t v309;
    int64_t v310;
    int64_t v311;
    int64_t v312;
    struct s0* v313;
    int64_t v314;
    int64_t v315;
    int64_t v316;
    int64_t v317;
    struct s0* rax318;
    int64_t v319;
    struct s0* v320;
    int64_t v321;
    int64_t v322;
    int64_t v323;
    int64_t v324;
    int64_t v325;
    struct s0* v326;
    int64_t v327;
    int64_t v328;
    int64_t v329;
    int64_t v330;
    struct s0* rax331;
    int64_t v332;
    struct s0* v333;
    int64_t v334;
    int64_t v335;
    int64_t v336;
    int64_t v337;
    int64_t v338;
    struct s0* v339;
    int64_t v340;
    int64_t v341;
    int64_t v342;
    int64_t v343;
    struct s0* rax344;
    int64_t v345;
    struct s0* v346;
    int64_t v347;
    int64_t v348;
    int64_t v349;
    int64_t v350;
    int64_t v351;
    struct s0* v352;
    int64_t v353;
    int64_t v354;
    int64_t v355;
    int64_t v356;
    struct s0* rax357;
    int64_t v358;
    struct s0* v359;
    int64_t v360;
    int64_t v361;
    int64_t v362;
    int64_t v363;
    int64_t v364;
    struct s0* v365;
    int64_t v366;
    int64_t v367;
    int64_t v368;
    int64_t v369;
    int64_t v370;
    struct s0* v371;
    int64_t v372;
    int64_t v373;
    int64_t v374;
    int64_t v375;
    int64_t v376;
    struct s0* v377;
    int64_t v378;
    int64_t v379;
    int64_t v380;
    int64_t v381;
    int64_t v382;
    struct s0* v383;
    int64_t v384;
    int64_t v385;
    int64_t v386;
    int64_t v387;
    int64_t v388;
    struct s0* v389;
    int64_t v390;
    int64_t v391;
    int64_t v392;
    int64_t v393;
    int64_t v394;
    int64_t v395;
    int64_t v396;
    int64_t v397;
    int64_t v398;
    struct s0* v399;
    int64_t v400;
    int64_t v401;
    int64_t v402;
    int64_t v403;
    void* rax404;

    __asm__("cli ");
    rdi3 = *rsi;
    rax4 = g28;
    set_program_name(rdi3);
    fun_2570(6, 6);
    fun_2400("coreutils", "/usr/local/share/locale");
    fun_23e0("coreutils", "/usr/local/share/locale");
    atexit(0x36a0, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&rdi5) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
    parse_long_options(rdi5, rsi, "getlimits", "GNU coreutils", "9.1.17-a351f", 0x3340, "Padraig Brady", 0);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8);
    r12_7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp6) + 17);
    fun_2630(r12_7, 1, 21, "%lu", 0x7f, 0x3340, "Padraig Brady", 0);
    rbp8 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 + 16);
    fun_2580(1, "CHAR_MAX=%s\n", r12_7, "%lu", 0x7f, 0x3340, "Padraig Brady", 0, v9, v10, v11, rax4, v12, rbp13, r12_14, r13_15, r14_16, __return_address(), v17, v18);
    rax19 = decimal_absval_add_one(rbp8, "CHAR_MAX=%s\n", r12_7, "%lu", 0x7f, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "CHAR_OFLOW=%s\n", rax19, "%lu", 0x7f, 0x3340, "Padraig Brady", 0, v20, v21, v22, rax4, v23, rbp13, r12_14, r13_15, r14_16, __return_address(), v24, v25);
    fun_2630(r12_7, 1, 21, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "CHAR_MIN=%s\n", r12_7, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0, v26, v27, v28, rax4, v29, rbp13, r12_14, r13_15, r14_16, __return_address(), v30, v31);
    rax32 = decimal_absval_add_one(rbp8, "CHAR_MIN=%s\n", r12_7, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "CHAR_UFLOW=%s\n", rax32, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0, v33, v34, v35, rax4, v36, rbp13, r12_14, r13_15, r14_16, __return_address(), v37, v38);
    fun_2630(r12_7, 1, 21, "%lu", 0x7f, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SCHAR_MAX=%s\n", r12_7, "%lu", 0x7f, 0x3340, "Padraig Brady", 0, v39, v40, v41, rax4, v42, rbp13, r12_14, r13_15, r14_16, __return_address(), v43, v44);
    rax45 = decimal_absval_add_one(rbp8, "SCHAR_MAX=%s\n", r12_7, "%lu", 0x7f, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SCHAR_OFLOW=%s\n", rax45, "%lu", 0x7f, 0x3340, "Padraig Brady", 0, v46, v47, v48, rax4, v49, rbp13, r12_14, r13_15, r14_16, __return_address(), v50, v51);
    fun_2630(r12_7, 1, 21, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SCHAR_MIN=%s\n", r12_7, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0, v52, v53, v54, rax4, v55, rbp13, r12_14, r13_15, r14_16, __return_address(), v56, v57);
    rax58 = decimal_absval_add_one(rbp8, "SCHAR_MIN=%s\n", r12_7, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SCHAR_UFLOW=%s\n", rax58, "%ld", 0xffffffffffffff80, 0x3340, "Padraig Brady", 0, v59, v60, v61, rax4, v62, rbp13, r12_14, r13_15, r14_16, __return_address(), v63, v64);
    fun_2630(r12_7, 1, 21, "%lu", 0xff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UCHAR_MAX=%s\n", r12_7, "%lu", 0xff, 0x3340, "Padraig Brady", 0, v65, v66, v67, rax4, v68, rbp13, r12_14, r13_15, r14_16, __return_address(), v69, v70);
    rax71 = decimal_absval_add_one(rbp8, "UCHAR_MAX=%s\n", r12_7, "%lu", 0xff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UCHAR_OFLOW=%s\n", rax71, "%lu", 0xff, 0x3340, "Padraig Brady", 0, v72, v73, v74, rax4, v75, rbp13, r12_14, r13_15, r14_16, __return_address(), v76, v77);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SHRT_MAX=%s\n", r12_7, "%lu", 0x7fff, 0x3340, "Padraig Brady", 0, v78, v79, v80, rax4, v81, rbp13, r12_14, r13_15, r14_16, __return_address(), v82, v83);
    rax84 = decimal_absval_add_one(rbp8, "SHRT_MAX=%s\n", r12_7, "%lu", 0x7fff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SHRT_OFLOW=%s\n", rax84, "%lu", 0x7fff, 0x3340, "Padraig Brady", 0, v85, v86, v87, rax4, v88, rbp13, r12_14, r13_15, r14_16, __return_address(), v89, v90);
    fun_2630(r12_7, 1, 21, "%ld", 0xffffffffffff8000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SHRT_MIN=%s\n", r12_7, "%ld", 0xffffffffffff8000, 0x3340, "Padraig Brady", 0, v91, v92, v93, rax4, v94, rbp13, r12_14, r13_15, r14_16, __return_address(), v95, v96);
    rax97 = decimal_absval_add_one(rbp8, "SHRT_MIN=%s\n", r12_7, "%ld", 0xffffffffffff8000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SHRT_UFLOW=%s\n", rax97, "%ld", 0xffffffffffff8000, 0x3340, "Padraig Brady", 0, v98, v99, v100, rax4, v101, rbp13, r12_14, r13_15, r14_16, __return_address(), v102, v103);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INT_MAX=%s\n", r12_7, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0, v104, v105, v106, rax4, v107, rbp13, r12_14, r13_15, r14_16, __return_address(), v108, v109);
    rax110 = decimal_absval_add_one(rbp8, "INT_MAX=%s\n", r12_7, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INT_OFLOW=%s\n", rax110, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0, v111, v112, v113, rax4, v114, rbp13, r12_14, r13_15, r14_16, __return_address(), v115, v116);
    fun_2630(r12_7, 1, 21, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INT_MIN=%s\n", r12_7, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0, v117, v118, v119, rax4, v120, rbp13, r12_14, r13_15, r14_16, __return_address(), v121, v122);
    rax123 = decimal_absval_add_one(rbp8, "INT_MIN=%s\n", r12_7, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INT_UFLOW=%s\n", rax123, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0, v124, v125, v126, rax4, v127, rbp13, r12_14, r13_15, r14_16, __return_address(), v128, v129);
    fun_2630(r12_7, 1, 21, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UINT_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v130, v131, v132, rax4, v133, rbp13, r12_14, r13_15, r14_16, __return_address(), v134, v135);
    rax136 = decimal_absval_add_one(rbp8, "UINT_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UINT_OFLOW=%s\n", rax136, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v137, v138, v139, rax4, v140, rbp13, r12_14, r13_15, r14_16, __return_address(), v141, v142);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "LONG_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v143, v144, v145, rax4, v146, rbp13, r12_14, r13_15, r14_16, __return_address(), v147, v148);
    rax149 = decimal_absval_add_one(rbp8, "LONG_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "LONG_OFLOW=%s\n", rax149, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v150, v151, v152, rax4, v153, rbp13, r12_14, r13_15, r14_16, __return_address(), v154, v155);
    fun_2630(r12_7, 1, 21, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "LONG_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v156, v157, v158, rax4, v159, rbp13, r12_14, r13_15, r14_16, __return_address(), v160, v161);
    rax162 = decimal_absval_add_one(rbp8, "LONG_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "LONG_UFLOW=%s\n", rax162, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v163, v164, v165, rax4, v166, rbp13, r12_14, r13_15, r14_16, __return_address(), v167, v168);
    fun_2630(r12_7, 1, 21, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "ULONG_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0, v169, v170, v171, rax4, v172, rbp13, r12_14, r13_15, r14_16, __return_address(), v173, v174);
    rax175 = decimal_absval_add_one(rbp8, "ULONG_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "ULONG_OFLOW=%s\n", rax175, "%lu", -1, 0x3340, "Padraig Brady", 0, v176, v177, v178, rax4, v179, rbp13, r12_14, r13_15, r14_16, __return_address(), v180, v181);
    fun_2630(r12_7, 1, 21, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SIZE_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0, v182, v183, v184, rax4, v185, rbp13, r12_14, r13_15, r14_16, __return_address(), v186, v187);
    rax188 = decimal_absval_add_one(rbp8, "SIZE_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SIZE_OFLOW=%s\n", rax188, "%lu", -1, 0x3340, "Padraig Brady", 0, v189, v190, v191, rax4, v192, rbp13, r12_14, r13_15, r14_16, __return_address(), v193, v194);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SSIZE_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v195, v196, v197, rax4, v198, rbp13, r12_14, r13_15, r14_16, __return_address(), v199, v200);
    rax201 = decimal_absval_add_one(rbp8, "SSIZE_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SSIZE_OFLOW=%s\n", rax201, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v202, v203, v204, rax4, v205, rbp13, r12_14, r13_15, r14_16, __return_address(), v206, v207);
    fun_2630(r12_7, 1, 21, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SSIZE_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v208, v209, v210, rax4, v211, rbp13, r12_14, r13_15, r14_16, __return_address(), v212, v213);
    rax214 = decimal_absval_add_one(rbp8, "SSIZE_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "SSIZE_UFLOW=%s\n", rax214, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v215, v216, v217, rax4, v218, rbp13, r12_14, r13_15, r14_16, __return_address(), v219, v220);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "TIME_T_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v221, v222, v223, rax4, v224, rbp13, r12_14, r13_15, r14_16, __return_address(), v225, v226);
    rax227 = decimal_absval_add_one(rbp8, "TIME_T_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "TIME_T_OFLOW=%s\n", rax227, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v228, v229, v230, rax4, v231, rbp13, r12_14, r13_15, r14_16, __return_address(), v232, v233);
    fun_2630(r12_7, 1, 21, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "TIME_T_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v234, v235, v236, rax4, v237, rbp13, r12_14, r13_15, r14_16, __return_address(), v238, v239);
    rax240 = decimal_absval_add_one(rbp8, "TIME_T_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "TIME_T_UFLOW=%s\n", rax240, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v241, v242, v243, rax4, v244, rbp13, r12_14, r13_15, r14_16, __return_address(), v245, v246);
    fun_2630(r12_7, 1, 21, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UID_T_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v247, v248, v249, rax4, v250, rbp13, r12_14, r13_15, r14_16, __return_address(), v251, v252);
    rax253 = decimal_absval_add_one(rbp8, "UID_T_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UID_T_OFLOW=%s\n", rax253, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v254, v255, v256, rax4, v257, rbp13, r12_14, r13_15, r14_16, __return_address(), v258, v259);
    fun_2630(r12_7, 1, 21, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "GID_T_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v260, v261, v262, rax4, v263, rbp13, r12_14, r13_15, r14_16, __return_address(), v264, v265);
    rax266 = decimal_absval_add_one(rbp8, "GID_T_MAX=%s\n", r12_7, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "GID_T_OFLOW=%s\n", rax266, "%lu", 0xffffffff, 0x3340, "Padraig Brady", 0, v267, v268, v269, rax4, v270, rbp13, r12_14, r13_15, r14_16, __return_address(), v271, v272);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "PID_T_MAX=%s\n", r12_7, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0, v273, v274, v275, rax4, v276, rbp13, r12_14, r13_15, r14_16, __return_address(), v277, v278);
    rax279 = decimal_absval_add_one(rbp8, "PID_T_MAX=%s\n", r12_7, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "PID_T_OFLOW=%s\n", rax279, "%lu", 0x7fffffff, 0x3340, "Padraig Brady", 0, v280, v281, v282, rax4, v283, rbp13, r12_14, r13_15, r14_16, __return_address(), v284, v285);
    fun_2630(r12_7, 1, 21, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "PID_T_MIN=%s\n", r12_7, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0, v286, v287, v288, rax4, v289, rbp13, r12_14, r13_15, r14_16, __return_address(), v290, v291);
    rax292 = decimal_absval_add_one(rbp8, "PID_T_MIN=%s\n", r12_7, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "PID_T_UFLOW=%s\n", rax292, "%ld", 0xffffffff80000000, 0x3340, "Padraig Brady", 0, v293, v294, v295, rax4, v296, rbp13, r12_14, r13_15, r14_16, __return_address(), v297, v298);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "OFF_T_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v299, v300, v301, rax4, v302, rbp13, r12_14, r13_15, r14_16, __return_address(), v303, v304);
    rax305 = decimal_absval_add_one(rbp8, "OFF_T_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "OFF_T_OFLOW=%s\n", rax305, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v306, v307, v308, rax4, v309, rbp13, r12_14, r13_15, r14_16, __return_address(), v310, v311);
    fun_2630(r12_7, 1, 21, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "OFF_T_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v312, v313, v314, rax4, v315, rbp13, r12_14, r13_15, r14_16, __return_address(), v316, v317);
    rax318 = decimal_absval_add_one(rbp8, "OFF_T_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "OFF_T_UFLOW=%s\n", rax318, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v319, v320, v321, rax4, v322, rbp13, r12_14, r13_15, r14_16, __return_address(), v323, v324);
    fun_2630(r12_7, 1, 21, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INTMAX_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v325, v326, v327, rax4, v328, rbp13, r12_14, r13_15, r14_16, __return_address(), v329, v330);
    rax331 = decimal_absval_add_one(rbp8, "INTMAX_MAX=%s\n", r12_7, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INTMAX_OFLOW=%s\n", rax331, "%lu", 0x7fffffffffffffff, 0x3340, "Padraig Brady", 0, v332, v333, v334, rax4, v335, rbp13, r12_14, r13_15, r14_16, __return_address(), v336, v337);
    fun_2630(r12_7, 1, 21, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INTMAX_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v338, v339, v340, rax4, v341, rbp13, r12_14, r13_15, r14_16, __return_address(), v342, v343);
    rax344 = decimal_absval_add_one(rbp8, "INTMAX_MIN=%s\n", r12_7, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "INTMAX_UFLOW=%s\n", rax344, "%ld", 0x8000000000000000, 0x3340, "Padraig Brady", 0, v345, v346, v347, rax4, v348, rbp13, r12_14, r13_15, r14_16, __return_address(), v349, v350);
    fun_2630(r12_7, 1, 21, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UINTMAX_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0, v351, v352, v353, rax4, v354, rbp13, r12_14, r13_15, r14_16, __return_address(), v355, v356);
    rax357 = decimal_absval_add_one(rbp8, "UINTMAX_MAX=%s\n", r12_7, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "UINTMAX_OFLOW=%s\n", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v358, v359, v360, rax4, v361, rbp13, r12_14, r13_15, r14_16, __return_address(), v362, v363);
    fun_2580(1, "FLT_MIN=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v364, v365, v366, rax4, v367, rbp13, r12_14, r13_15, r14_16, __return_address(), v368, v369);
    __asm__("movss xmm0, [rip+0x551b]");
    print_FLT(1, "FLT_MIN=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "FLT_MAX=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v370, v371, v372, rax4, v373, rbp13, r12_14, r13_15, r14_16, __return_address(), v374, v375);
    __asm__("movss xmm0, [rip+0x54ff]");
    print_FLT(1, "FLT_MAX=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "DBL_MIN=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v376, v377, v378, rax4, v379, rbp13, r12_14, r13_15, r14_16, __return_address(), v380, v381);
    g1 = g837e;
    print_DBL(5, "MIN=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "DBL_MAX=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v382, v383, v384, rax4, v385, rbp13, r12_14, r13_15, r14_16, __return_address(), v386, v387);
    g1 = g8388;
    print_DBL(5, "MAX=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0);
    fun_2580(1, "LDBL_MIN=", rax357, "%lu", -1, 0x3340, "Padraig Brady", 0, v388, v389, v390, rax4, v391, rbp13, r12_14, r13_15, r14_16, __return_address(), v392, v393);
    print_LDBL(1, "LDBL_MIN=", rax357, "%lu", -1, 0x3340, 0x8000000000000000, 1, "Padraig Brady", 0);
    fun_2580(1, "LDBL_MAX=", rax357, "%lu", -1, 0x3340, v394, v395, v396, rax4, v397, rbp13, r12_14, r13_15, r14_16, __return_address(), v398, v399, v400, v401);
    print_LDBL(1, "LDBL_MAX=", rax357, "%lu", -1, 0x3340, 0xff, 0x7ffe, v402, v403);
    rax404 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax404) {
        fun_2440();
    } else {
        return 0;
    }
}

int64_t __libc_start_main = 0;

void fun_30e3() {
    __asm__("cli ");
    __libc_start_main(0x2680, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2330(int64_t rdi);

int64_t fun_3183() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2330(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_31c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

struct s0* fun_2410();

struct s2* stdout = reinterpret_cast<struct s2*>(0);

void fun_24c0(struct s0* rdi, struct s2* rsi, int64_t rdx, struct s0* rcx);

int32_t fun_24e0(int64_t rdi);

int32_t fun_2380(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

void fun_25d0();

struct s2* stderr = reinterpret_cast<struct s2*>(0);

void fun_25f0(struct s2* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3343(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r8_7;
    int64_t r9_8;
    struct s2* r12_9;
    struct s0* rax10;
    struct s2* r12_11;
    struct s0* rax12;
    struct s2* r12_13;
    struct s0* rax14;
    int32_t eax15;
    struct s0* r13_16;
    struct s0* rax17;
    int64_t r8_18;
    int64_t r9_19;
    struct s0* rax20;
    int32_t eax21;
    struct s0* rax22;
    int64_t r8_23;
    int64_t r9_24;
    struct s0* rax25;
    int64_t r8_26;
    int64_t r9_27;
    struct s0* rax28;
    int32_t eax29;
    struct s0* rax30;
    int64_t r8_31;
    int64_t r9_32;
    struct s2* r15_33;
    struct s0* rax34;
    struct s0* rax35;
    int64_t r8_36;
    int64_t r9_37;
    struct s0* rax38;
    struct s2* rdi39;
    struct s0* r8_40;
    struct s0* r9_41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t v47;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2410();
            fun_2580(1, rax5, r12_2, rcx6, r8_7, r9_8, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_9 = stdout;
            rax10 = fun_2410();
            fun_24c0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2410();
            fun_24c0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2410();
            fun_24c0(rax14, r12_13, 5, rcx6);
            do {
                if (1) 
                    break;
                eax15 = fun_24e0("getlimits");
            } while (eax15);
            r13_16 = v4;
            if (!r13_16) {
                rax17 = fun_2410();
                fun_2580(1, rax17, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_18, r9_19, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax20 = fun_2570(5);
                if (!rax20 || (eax21 = fun_2380(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    rax22 = fun_2410();
                    r13_16 = reinterpret_cast<struct s0*>("getlimits");
                    fun_2580(1, rax22, "https://www.gnu.org/software/coreutils/", "getlimits", r8_23, r9_24, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_16 = reinterpret_cast<struct s0*>("getlimits");
                    goto addr_3650_9;
                }
            } else {
                rax25 = fun_2410();
                fun_2580(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_26, r9_27, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax28 = fun_2570(5);
                if (!rax28 || (eax29 = fun_2380(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_3556_11:
                    rax30 = fun_2410();
                    fun_2580(1, rax30, "https://www.gnu.org/software/coreutils/", "getlimits", r8_31, r9_32, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_16 == "getlimits")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x8a21);
                    }
                } else {
                    addr_3650_9:
                    r15_33 = stdout;
                    rax34 = fun_2410();
                    fun_24c0(rax34, r15_33, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3556_11;
                }
            }
            rax35 = fun_2410();
            rcx6 = r12_2;
            fun_2580(1, rax35, r13_16, rcx6, r8_36, r9_37, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_339e_14:
            fun_25d0();
        }
    } else {
        rax38 = fun_2410();
        rdi39 = stderr;
        rcx6 = r12_2;
        fun_25f0(rdi39, 1, rax38, rcx6, r8_40, r9_41, v42, v43, v44, v45, v46, v47);
        goto addr_339e_14;
    }
}

int64_t file_name = 0;

void fun_3683(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3693(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s2* rdi);

struct s0* quotearg_colon();

void fun_25a0();

int32_t exit_failure = 1;

struct s0* fun_2390(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_36a3() {
    struct s2* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s2* rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2370(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2410();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3733_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2390(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3733_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25a0();
    }
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

struct s7 {
    signed char[4] pad4;
    int32_t f4;
};

struct s8 {
    int32_t f0;
    int32_t f4;
};

int32_t g2e2a = 0xe8ef8948;

struct s9 {
    int32_t f0;
    int32_t f4;
};

int32_t g2e2e = 0x39e;

struct s10 {
    int16_t f0;
    signed char f2;
    signed char f3;
    signed char f4;
};

int32_t fun_2340(struct s8* rdi, int32_t* rsi, int64_t rdx, int64_t rcx, void* r8, int64_t r9, int64_t a7);

void fun_23d0();

int64_t fun_3753(struct s6* rdi, struct s7* rsi, uint32_t edx, int32_t ecx) {
    int64_t r15_5;
    int32_t r13d6;
    struct s8* rdi7;
    struct s8* rbp8;
    int32_t* rbx9;
    void* rsp10;
    struct s9* rdi11;
    void* r14_12;
    struct s0* rax13;
    struct s0* v14;
    int64_t rax15;
    int64_t rcx16;
    signed char* rax17;
    struct s8* rdi18;
    int32_t* rsi19;
    signed char* rax20;
    int64_t rcx21;
    signed char* rax22;
    uint32_t edx23;
    int64_t rcx24;
    struct s10* rax25;
    uint32_t edx26;
    uint32_t tmp32_27;
    uint32_t edx28;
    int64_t r9_29;
    int32_t eax30;
    void* rax31;
    int64_t rax32;

    __asm__("cli ");
    __asm__("movapd xmm4, xmm0");
    __asm__("pxor xmm2, xmm2");
    *reinterpret_cast<int32_t*>(&r15_5) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    __asm__("movapd xmm1, xmm4");
    r13d6 = ecx;
    rdi7 = reinterpret_cast<struct s8*>(&rdi->f4);
    rbp8 = rdi7;
    rbx9 = &rsi->f4;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rdi7->f0 = g2e2a;
    rdi11 = reinterpret_cast<struct s9*>(&rdi7->f4);
    __asm__("xorpd xmm0, [rip+0x4dc6]");
    r14_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 29);
    rax13 = g28;
    v14 = rax13;
    *reinterpret_cast<uint32_t*>(&rax15) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    __asm__("andpd xmm0, xmm1");
    __asm__("andnpd xmm1, xmm4");
    *reinterpret_cast<uint32_t*>(&rcx16) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    rax17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp10) + rax15 + 30);
    __asm__("orpd xmm0, xmm1");
    rdi11->f0 = g2e2e;
    rdi18 = reinterpret_cast<struct s8*>(&rdi11->f4);
    rsi19 = reinterpret_cast<int32_t*>(0x2e32);
    *rax17 = 43;
    rax20 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax17) + rcx16);
    *rax20 = 32;
    *reinterpret_cast<uint32_t*>(&rcx21) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
    rax22 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax20) + rcx21);
    edx23 = edx & 16;
    *rax22 = 48;
    *reinterpret_cast<uint32_t*>(&rcx24) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
    rax25 = reinterpret_cast<struct s10*>(reinterpret_cast<int64_t>(rax22) + rcx24);
    rax25->f0 = 0x2e32;
    edx26 = edx23 - (edx23 + reinterpret_cast<uint1_t>(edx23 < edx23 + reinterpret_cast<uint1_t>(edx23 < 1))) & 32;
    rax25->f2 = 42;
    tmp32_27 = edx26 + 71;
    edx28 = tmp32_27;
    __asm__("comisd xmm1, xmm0");
    rax25->f4 = 0;
    rax25->f3 = *reinterpret_cast<signed char*>(&edx28);
    if (reinterpret_cast<uint1_t>(tmp32_27 < edx26) | reinterpret_cast<uint1_t>(edx28 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_5) = 15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&r9_29) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_29) + 4) = 0;
        rdi18->f0 = *rsi19;
        rsi19 = rbx9;
        rdi18 = rbp8;
        eax30 = fun_2340(rdi18, rsi19, 1, -1, r14_12, r9_29, r15_5);
        if (eax30 < 0) 
            break;
        if (*reinterpret_cast<int32_t*>(&r15_5) > 16) 
            break;
        if (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax30)) >= reinterpret_cast<uint64_t>(rbx9)) 
            goto addr_3830_19;
        *reinterpret_cast<int32_t*>(&rsi19) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi19) + 4) = 0;
        rdi18 = rbp8;
        fun_23d0();
        __asm__("ucomisd xmm0, [rsp+0x8]");
        if (__intrinsic()) 
            goto addr_3830_19;
        if (!0) 
            break;
        addr_3830_19:
        *reinterpret_cast<int32_t*>(&r15_5) = *reinterpret_cast<int32_t*>(&r15_5) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28));
    if (rax31) {
        fun_2440();
    } else {
        *reinterpret_cast<int32_t*>(&rax32) = eax30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
        return rax32;
    }
}

struct s11 {
    int16_t f0;
    signed char f2;
    signed char f3;
    signed char f4;
};

void fun_2490();

int64_t fun_38c3(struct s8* rdi, int32_t* rsi, uint32_t edx, int32_t ecx) {
    int64_t r15_5;
    int32_t r13d6;
    struct s8* rbp7;
    int32_t* rbx8;
    void* rsp9;
    void* r14_10;
    struct s0* rax11;
    struct s0* v12;
    int64_t rax13;
    int64_t rcx14;
    signed char* rax15;
    struct s8* rdi16;
    int32_t* rsi17;
    signed char* rax18;
    int64_t rcx19;
    signed char* rax20;
    uint32_t edx21;
    int64_t rcx22;
    struct s11* rax23;
    uint32_t edx24;
    uint32_t tmp32_25;
    uint32_t edx26;
    int64_t r9_27;
    int32_t eax28;
    void* rax29;
    int64_t rax30;

    __asm__("cli ");
    __asm__("movaps xmm3, xmm0");
    __asm__("pxor xmm2, xmm2");
    __asm__("pxor xmm5, xmm5");
    __asm__("movaps xmm1, xmm3");
    __asm__("cvtss2sd xmm5, xmm0");
    *reinterpret_cast<int32_t*>(&r15_5) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    r13d6 = ecx;
    __asm__("cmpss xmm1, xmm2, 0x1");
    rbp7 = rdi;
    rbx8 = rsi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    __asm__("movss [rsp+0xc], xmm0");
    __asm__("xorps xmm0, [rip+0x4c61]");
    r14_10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) + 29);
    rax11 = g28;
    v12 = rax11;
    *reinterpret_cast<uint32_t*>(&rax13) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    __asm__("andps xmm0, xmm1");
    __asm__("andnps xmm1, xmm3");
    *reinterpret_cast<uint32_t*>(&rcx14) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
    rax15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp9) + rax13 + 30);
    __asm__("orps xmm0, xmm1");
    rdi->f0 = g2e2a;
    rdi16 = reinterpret_cast<struct s8*>(&rdi->f4);
    rsi17 = reinterpret_cast<int32_t*>(0x2e2e);
    *rax15 = 43;
    rax18 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax15) + rcx14);
    __asm__("movss xmm1, [rip+0x4bcc]");
    *rax18 = 32;
    *reinterpret_cast<uint32_t*>(&rcx19) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
    rax20 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax18) + rcx19);
    edx21 = edx & 16;
    *rax20 = 48;
    *reinterpret_cast<uint32_t*>(&rcx22) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
    rax23 = reinterpret_cast<struct s11*>(reinterpret_cast<int64_t>(rax20) + rcx22);
    rax23->f0 = 0x2e2e;
    edx24 = edx21 - (edx21 + reinterpret_cast<uint1_t>(edx21 < edx21 + reinterpret_cast<uint1_t>(edx21 < 1))) & 32;
    rax23->f2 = 42;
    tmp32_25 = edx24 + 71;
    edx26 = tmp32_25;
    __asm__("comiss xmm1, xmm0");
    rax23->f4 = 0;
    rax23->f3 = *reinterpret_cast<signed char*>(&edx26);
    if (reinterpret_cast<uint1_t>(tmp32_25 < edx24) | reinterpret_cast<uint1_t>(edx26 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_5) = 6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&r9_27) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_27) + 4) = 0;
        rdi16->f0 = *rsi17;
        rsi17 = rbx8;
        rdi16 = rbp7;
        eax28 = fun_2340(rdi16, rsi17, 1, -1, r14_10, r9_27, r15_5);
        if (eax28 < 0) 
            break;
        if (*reinterpret_cast<int32_t*>(&r15_5) > 8) 
            break;
        if (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax28)) >= reinterpret_cast<uint64_t>(rbx8)) 
            goto addr_39a0_13;
        *reinterpret_cast<int32_t*>(&rsi17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        rdi16 = rbp7;
        fun_2490();
        __asm__("ucomiss xmm0, [rsp+0xc]");
        if (__intrinsic()) 
            goto addr_39a0_13;
        if (!0) 
            break;
        addr_39a0_13:
        *reinterpret_cast<int32_t*>(&r15_5) = *reinterpret_cast<int32_t*>(&r15_5) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rax29) {
        fun_2440();
    } else {
        *reinterpret_cast<int32_t*>(&rax30) = eax28;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
        return rax30;
    }
}

struct s12 {
    int32_t f0;
    signed char f4;
    signed char f5;
};

void fun_2590(struct s8* rdi);

int64_t fun_3a33(struct s8* rdi, int32_t* rsi, uint32_t edx, int32_t ecx) {
    int64_t r15_5;
    int32_t r13d6;
    struct s8* rbp7;
    int32_t* rbx8;
    void* rsp9;
    struct s0* rax10;
    struct s0* v11;
    void* r14_12;
    int64_t rax13;
    int64_t rcx14;
    signed char* rax15;
    signed char* rax16;
    int64_t rcx17;
    signed char* rax18;
    uint32_t edx19;
    int64_t rcx20;
    struct s12* rax21;
    uint32_t edx22;
    uint32_t tmp32_23;
    uint32_t edx24;
    int64_t r9_25;
    int32_t eax26;
    void* rax27;
    int64_t rax28;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_5) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    r13d6 = ecx;
    rbp7 = rdi;
    rbx8 = rsi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rax10 = g28;
    v11 = rax10;
    __asm__("fld tword [rsp+0x60]");
    r14_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) + 13);
    __asm__("fchs ");
    __asm__("fld tword [rsp+0x60]");
    __asm__("fldz ");
    __asm__("fcomip st0, st1");
    __asm__("fcmovnbe st0, st1");
    __asm__("fstp st1");
    *reinterpret_cast<uint32_t*>(&rax13) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rcx14) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
    rax15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp9) + rax13 + 14);
    *rax15 = 43;
    __asm__("fld tword [rip+0x4ae4]");
    rax16 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax15) + rcx14);
    *rax16 = 32;
    *reinterpret_cast<uint32_t*>(&rcx17) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    rax18 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax16) + rcx17);
    edx19 = edx & 16;
    *rax18 = 48;
    *reinterpret_cast<uint32_t*>(&rcx20) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
    rax21 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(rax18) + rcx20);
    rax21->f0 = 0x4c2a2e2a;
    edx22 = edx19 - (edx19 + reinterpret_cast<uint1_t>(edx19 < edx19 + reinterpret_cast<uint1_t>(edx19 < 1))) & 32;
    rax21->f5 = 0;
    tmp32_23 = edx22 + 71;
    edx24 = tmp32_23;
    __asm__("fcomip st0, st1");
    __asm__("fstp st0");
    rax21->f4 = *reinterpret_cast<signed char*>(&edx24);
    if (reinterpret_cast<uint1_t>(tmp32_23 < edx22) | reinterpret_cast<uint1_t>(edx24 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_5) = 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    while ((*reinterpret_cast<int32_t*>(&r9_25) = r13d6, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_25) + 4) = 0, eax26 = fun_2340(rbp7, rbx8, 1, -1, r14_12, r9_25, r15_5), eax26 >= 0) && (*reinterpret_cast<int32_t*>(&r15_5) <= 20 && (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax26)) >= reinterpret_cast<uint64_t>(rbx8) || (fun_2590(rbp7), __intrinsic())))) {
        *reinterpret_cast<int32_t*>(&r15_5) = *reinterpret_cast<int32_t*>(&r15_5) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    rax27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax27) {
        fun_2440();
    } else {
        *reinterpret_cast<int32_t*>(&rax28) = eax26;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        return rax28;
    }
}

int32_t opterr = 0;

int32_t fun_2450();

int32_t optind = 0;

void version_etc_va(struct s2* rdi, int64_t rsi, int64_t rdx, int64_t rcx, void* r8);

void fun_3b73(int32_t edi) {
    signed char al2;
    struct s0* rax3;
    int32_t r14d4;
    int32_t eax5;
    void* rax6;
    struct s2* rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    r14d4 = opterr;
    opterr = 0;
    if (edi != 2) 
        goto addr_3bf0_4;
    eax5 = fun_2450();
    if (eax5 == -1) 
        goto addr_3bf0_4;
    if (eax5 != 0x68) {
        if (eax5 != 0x76) {
            addr_3bf0_4:
            opterr = r14d4;
            optind = 0;
            rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
            if (rax6) {
                fun_2440();
            } else {
                return;
            }
        } else {
            rdi7 = stdout;
            version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
            fun_25d0();
        }
    }
    r9_11();
    goto addr_3bf0_4;
}

void fun_3cb3() {
    signed char al1;
    struct s0* rax2;
    signed char r9b3;
    int32_t ebx4;
    int32_t eax5;
    int64_t v6;
    struct s2* rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t rdi11;
    void* rax12;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    if (!r9b3) {
    }
    ebx4 = opterr;
    opterr = 1;
    eax5 = fun_2450();
    if (eax5 != -1) {
        if (eax5 == 0x68) {
            addr_3de0_7:
            v6();
        } else {
            if (eax5 == 0x76) {
                rdi7 = stdout;
                version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
                fun_25d0();
                goto addr_3de0_7;
            } else {
                *reinterpret_cast<int32_t*>(&rdi11) = exit_failure;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                v6(rdi11);
            }
        }
    }
    opterr = ebx4;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2440();
    } else {
        return;
    }
}

void fun_25e0(struct s2* rdi, int64_t rsi, int64_t rdx, struct s2* rcx);

struct s13 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s13* fun_2470();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_3df3(struct s0* rdi) {
    struct s2* rcx2;
    struct s0* rbx3;
    struct s13* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_25e0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2360("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2470();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2380(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(struct s2* rdi, int64_t rsi);

void fun_5593(struct s2* rdi) {
    struct s2* rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2370(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<struct s2*>(0xc200);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_55d3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_55f3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *rdi = esi;
    return 0xc200;
}

int64_t fun_5613(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s14 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5653(struct s14* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s14*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s15 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s15* fun_5673(struct s15* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s15*>(0xc200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x264a;
    if (!rdx) 
        goto 0x264a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc200;
}

struct s16 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void* fun_56b3(struct s2* rdi, void* rsi, struct s2* rdx, int64_t rcx, struct s16* r8) {
    struct s16* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s16*>(0xc200);
    }
    rax7 = fun_2370(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x56e6);
    *rax7 = r15d8;
    return rax13;
}

struct s17 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

struct s2* fun_5733(struct s2* rdi, int64_t rsi, void** rdx, struct s17* rcx) {
    struct s17* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void* rax14;
    void* rsi15;
    struct s2* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s17*>(0xc200);
    }
    rax6 = fun_2370(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void*>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5761);
    rsi15 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x57bc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5823() {
    __asm__("cli ");
}

struct s2* gc078 = reinterpret_cast<struct s2*>(0xc100);

int64_t slotvec0 = 0x100;

void fun_5833() {
    uint32_t eax1;
    struct s2* r12_2;
    int64_t rax3;
    struct s2** rbx4;
    struct s2** rbp5;
    struct s2* rdi6;
    struct s2* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2350(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xc100) {
        fun_2350(rdi7);
        gc078 = reinterpret_cast<struct s2*>(0xc100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc070) {
        fun_2350(r12_2);
        slotvec = reinterpret_cast<struct s2*>(0xc070);
    }
    nslots = 1;
    return;
}

void fun_58d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_58f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5903(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5923(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s2* fun_5943(struct s2* rdi, int32_t esi, struct s2* rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s2* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2650;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s2* fun_59d3(struct s2* rdi, int32_t esi, struct s2* rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s2* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2655;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

struct s2* fun_5a63(int32_t edi, struct s2* rsi) {
    struct s0* rax3;
    struct s3* rcx4;
    struct s2* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x265a;
    rcx4 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2440();
    } else {
        return rax5;
    }
}

struct s2* fun_5af3(int32_t edi, struct s2* rsi, int64_t rdx) {
    struct s0* rax4;
    struct s3* rcx5;
    struct s2* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x265f;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s2* fun_5b83(struct s2* rdi, int64_t rsi, uint32_t edx) {
    struct s3* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s2* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6670]");
    __asm__("movdqa xmm1, [rip+0x6678]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6661]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2440();
    } else {
        return rax10;
    }
}

struct s2* fun_5c23(struct s2* rdi, uint32_t esi) {
    struct s3* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s2* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x65d0]");
    __asm__("movdqa xmm1, [rip+0x65d8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x65c1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

struct s2* fun_5cc3(struct s2* rdi) {
    struct s0* rax2;
    struct s2* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6530]");
    __asm__("movdqa xmm1, [rip+0x6538]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6519]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2440();
    } else {
        return rax3;
    }
}

struct s2* fun_5d53(struct s2* rdi, int64_t rsi) {
    struct s0* rax3;
    struct s2* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x64a0]");
    __asm__("movdqa xmm1, [rip+0x64a8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6496]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2440();
    } else {
        return rax4;
    }
}

struct s2* fun_5de3(struct s2* rdi, int32_t esi, struct s2* rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s2* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2664;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s2* fun_5e83(struct s2* rdi, int64_t rsi, int64_t rdx, struct s2* rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s2* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x636a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6362]");
    __asm__("movdqa xmm2, [rip+0x636a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2669;
    if (!rdx) 
        goto 0x2669;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

struct s2* fun_5f23(int32_t edi, int64_t rsi, int64_t rdx, struct s2* rcx, int64_t r8) {
    struct s0* rcx6;
    struct s3* rcx7;
    struct s2* rdi8;
    struct s2* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x62ca]");
    __asm__("movdqa xmm1, [rip+0x62d2]");
    __asm__("movdqa xmm2, [rip+0x62da]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x266e;
    if (!rdx) 
        goto 0x266e;
    rcx7 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2440();
    } else {
        return rax9;
    }
}

struct s2* fun_5fd3(int64_t rdi, int64_t rsi, struct s2* rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s2* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x621a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6212]");
    __asm__("movdqa xmm2, [rip+0x621a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2673;
    if (!rsi) 
        goto 0x2673;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax6;
    }
}

struct s2* fun_6073(int64_t rdi, int64_t rsi, struct s2* rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s2* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x617a]");
    __asm__("movdqa xmm1, [rip+0x6182]");
    __asm__("movdqa xmm2, [rip+0x618a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2678;
    if (!rsi) 
        goto 0x2678;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2440();
    } else {
        return rax7;
    }
}

void fun_6113() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6123(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6143() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6163(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s18 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_24f0(int64_t rdi, struct s2* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_6183(struct s2* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s18* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_25f0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_25f0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2410();
    fun_25f0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_24f0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2410();
    fun_25f0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_24f0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2410();
        fun_25f0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8cc8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8cc8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_65f3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s19 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6613(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s19* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s19* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2440();
    } else {
        return;
    }
}

void fun_66b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6756_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6760_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2440();
    } else {
        return;
    }
    addr_6756_5:
    goto addr_6760_7;
}

void fun_6793() {
    struct s2* rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    int64_t r8_8;
    int64_t r9_9;
    int64_t v10;
    int64_t v11;
    struct s0* v12;
    int64_t v13;
    struct s0* v14;
    int64_t v15;
    struct s0* v16;
    int64_t v17;
    struct s0* v18;
    int64_t v19;
    struct s0* v20;
    int64_t v21;
    int64_t v22;
    struct s0* rax23;
    int64_t r8_24;
    int64_t r9_25;
    int64_t v26;
    int64_t v27;
    struct s0* v28;
    int64_t v29;
    struct s0* v30;
    int64_t v31;
    struct s0* v32;
    int64_t v33;
    struct s0* v34;
    int64_t v35;
    struct s0* v36;
    int64_t v37;
    int64_t v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_24f0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2410();
    fun_2580(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2410();
    fun_2580(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2410();
    goto fun_2580;
}

int64_t fun_23c0();

void xalloc_die();

void fun_6833(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2520(unsigned char* rdi);

void fun_6873(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6893(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_68b3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2560();

void fun_68d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2560();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6903() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6933(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6973() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_69b3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_69e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6a33(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23c0();
            if (rax5) 
                break;
            addr_6a7d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_6a7d_5;
        rax8 = fun_23c0();
        if (rax8) 
            goto addr_6a66_9;
        if (rbx4) 
            goto addr_6a7d_5;
        addr_6a66_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6ac3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23c0();
            if (rax8) 
                break;
            addr_6b0a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_6b0a_5;
        rax11 = fun_23c0();
        if (rax11) 
            goto addr_6af2_9;
        if (!rbx6) 
            goto addr_6af2_9;
        if (r12_4) 
            goto addr_6b0a_5;
        addr_6af2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6b53(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_6bfd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6c10_10:
                *r12_8 = 0;
            }
            addr_6bb0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6bd6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6c24_14;
            if (rcx10 <= rsi9) 
                goto addr_6bcd_16;
            if (rsi9 >= 0) 
                goto addr_6c24_14;
            addr_6bcd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6c24_14;
            addr_6bd6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2560();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6c24_14;
            if (!rbp13) 
                break;
            addr_6c24_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6bfd_9;
        } else {
            if (!r13_6) 
                goto addr_6c10_10;
            goto addr_6bb0_11;
        }
    }
}

int64_t fun_24d0();

void fun_6c53() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6cb3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6cd3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2500(signed char* rdi, struct s0* rsi, unsigned char* rdx);

void fun_6cf3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

void fun_6d33(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

struct s20 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6d73(int64_t rdi, struct s20* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2520(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2500;
    }
}

void fun_6db3(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2430(rdi);
    rax3 = fun_2520(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2500;
    }
}

void fun_6df3() {
    struct s2* rdi1;

    __asm__("cli ");
    fun_2410();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_25a0();
    fun_2360(rdi1);
}

int64_t fun_23a0();

int64_t rpl_fclose(struct s2* rdi);

int64_t fun_6e33(struct s2* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23a0();
    ebx3 = reinterpret_cast<uint32_t>(rdi->f0) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_6e8e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2370(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_6e8e_3;
            rax6 = fun_2370(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_2510(struct s2* rdi);

int32_t fun_2550(struct s2* rdi);

int64_t fun_2480(int64_t rdi, ...);

int32_t rpl_fflush(struct s2* rdi);

int64_t fun_23f0(struct s2* rdi);

int64_t fun_6ea3(struct s2* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2510(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2550(rdi);
        if (!(eax3 && (eax4 = fun_2510(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2480(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2370(rdi);
            r12d9 = *rax8;
            rax10 = fun_23f0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23f0;
}

void rpl_fseeko(struct s2* rdi);

void fun_6f33(struct s2* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2550(rdi), !eax2) || !(reinterpret_cast<uint32_t>(rdi->f0) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6f83(struct s2* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2510(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2480(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rdi->f0) & 0xffffffef);
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2540(int64_t rdi);

signed char* fun_7003() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2540(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2460(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7043(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2460(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2440();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_70d3() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2440();
    } else {
        return rax3;
    }
}

int64_t fun_7153(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2570(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2430(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2500(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2500(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7203() {
    __asm__("cli ");
    goto fun_2570;
}

void fun_7213() {
    __asm__("cli ");
}

void fun_7227() {
    __asm__("cli ");
    return;
}

uint32_t fun_24b0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2610(int64_t rdi, struct s0* rsi);

uint32_t fun_2600(struct s0* rdi, struct s0* rsi);

void** fun_2620(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_4025() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2410();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2410();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2430(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4323_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4323_22; else 
                            goto addr_471d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_47dd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4b30_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4320_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4320_30; else 
                                goto addr_4b49_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2430(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4b30_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24b0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4b30_28; else 
                            goto addr_41cc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4c90_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4b10_40:
                        if (r11_27 == 1) {
                            addr_469d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4c58_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_42d7_44;
                            }
                        } else {
                            goto addr_4b20_46;
                        }
                    } else {
                        addr_4c9f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_469d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4323_22:
                                if (v47 != 1) {
                                    addr_4879_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2430(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_48c4_54;
                                    }
                                } else {
                                    goto addr_4330_56;
                                }
                            } else {
                                addr_42d5_57:
                                ebp36 = 0;
                                goto addr_42d7_44;
                            }
                        } else {
                            addr_4b04_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4c9f_47; else 
                                goto addr_4b0e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_469d_41;
                        if (v47 == 1) 
                            goto addr_4330_56; else 
                            goto addr_4879_52;
                    }
                }
                addr_4391_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4228_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_424d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4550_65;
                    } else {
                        addr_43b9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4c08_67;
                    }
                } else {
                    goto addr_43b0_69;
                }
                addr_4261_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_42ac_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4c08_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_42ac_81;
                }
                addr_43b0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_424d_64; else 
                    goto addr_43b9_66;
                addr_42d7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_438f_91;
                if (v22) 
                    goto addr_42ef_93;
                addr_438f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4391_62;
                addr_48c4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_504b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_50bb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4ebf_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2610(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2600(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_49be_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_437c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_49c8_112;
                    }
                } else {
                    addr_49c8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4a99_114;
                }
                addr_4388_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_438f_91;
                while (1) {
                    addr_4a99_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4fa7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4a06_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4fb5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4a87_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4a87_128;
                        }
                    }
                    addr_4a35_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4a87_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_4a06_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4a35_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_42ac_81;
                addr_4fb5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4c08_67;
                addr_504b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_49be_109;
                addr_50bb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_49be_109;
                addr_4330_56:
                rax93 = fun_2620(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_437c_110;
                addr_4b0e_59:
                goto addr_4b10_40;
                addr_47dd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4323_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4388_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_42d5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4323_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4822_160;
                if (!v22) 
                    goto addr_4bf7_162; else 
                    goto addr_4e03_163;
                addr_4822_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4bf7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4c08_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_46cb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4533_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4261_70; else 
                    goto addr_4547_169;
                addr_46cb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4228_63;
                goto addr_43b0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4b04_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4c3f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4320_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4218_178; else 
                        goto addr_4bc2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4b04_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4323_22;
                }
                addr_4c3f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4320_30:
                    r8d42 = 0;
                    goto addr_4323_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4391_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4c58_42;
                    }
                }
                addr_4218_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4228_63;
                addr_4bc2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4b20_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4391_62;
                } else {
                    addr_4bd2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4323_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5382_188;
                if (v28) 
                    goto addr_4bf7_162;
                addr_5382_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4533_168;
                addr_41cc_37:
                if (v22) 
                    goto addr_51c3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_41e3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4c90_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4d1b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4323_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4218_178; else 
                        goto addr_4cf7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4b04_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4323_22;
                }
                addr_4d1b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4323_22;
                }
                addr_4cf7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4b20_46;
                goto addr_4bd2_186;
                addr_41e3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4323_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4323_22; else 
                    goto addr_41f4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_52ce_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5154_210;
            if (1) 
                goto addr_5152_212;
            if (!v29) 
                goto addr_4d8e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_52c1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4550_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_430b_219; else 
            goto addr_456a_220;
        addr_42ef_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4303_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_456a_220; else 
            goto addr_430b_219;
        addr_4ebf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_456a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4edd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2420();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_5350_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4db6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4fa7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4303_221;
        addr_4e03_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4303_221;
        addr_4547_169:
        goto addr_4550_65;
        addr_52ce_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_456a_220;
        goto addr_4edd_222;
        addr_5154_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_51ae_236;
        fun_2440();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5350_225;
        addr_5152_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5154_210;
        addr_4d8e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5154_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_4db6_226;
        }
        addr_52c1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_471d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x878c + rax113 * 4) + 0x878c;
    addr_4b49_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x888c + rax114 * 4) + 0x888c;
    addr_51c3_190:
    addr_430b_219:
    goto 0x3ff0;
    addr_41f4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x868c + rax115 * 4) + 0x868c;
    addr_51ae_236:
    goto v116;
}

void fun_4210() {
}

void fun_43c8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x40c2;
}

void fun_4421() {
    goto 0x40c2;
}

void fun_450e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4391;
    }
    if (v2) 
        goto 0x4e03;
    if (!r10_3) 
        goto addr_4f6e_5;
    if (!v4) 
        goto addr_4e3e_7;
    addr_4f6e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4e3e_7:
    goto 0x4244;
}

void fun_452c() {
}

void fun_45d7() {
    signed char v1;

    if (v1) {
        goto 0x455f;
    } else {
        goto 0x429a;
    }
}

void fun_45f1() {
    signed char v1;

    if (!v1) 
        goto 0x45ea; else 
        goto "???";
}

void fun_4618() {
    goto 0x4533;
}

void fun_4698() {
}

void fun_46b0() {
}

void fun_46df() {
    goto 0x4533;
}

void fun_4731() {
    goto 0x46c0;
}

void fun_4760() {
    goto 0x46c0;
}

void fun_4793() {
    goto 0x46c0;
}

void fun_4b60() {
    goto 0x4218;
}

void fun_4e5e() {
    signed char v1;

    if (v1) 
        goto 0x4e03;
    goto 0x4244;
}

void fun_4f05() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4244;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4228;
        goto 0x4244;
    }
}

void fun_5322() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4590;
    } else {
        goto 0x40c2;
    }
}

void fun_6258() {
    fun_2410();
}

void fun_444e() {
    goto 0x40c2;
}

void fun_4624() {
    goto 0x45dc;
}

void fun_46eb() {
    goto 0x4218;
}

void fun_473d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x46c0;
    goto 0x42ef;
}

void fun_476f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x46cb;
        goto 0x40f0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x456a;
        goto 0x430b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4f08;
    if (r10_8 > r15_9) 
        goto addr_4655_9;
    addr_465a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4f13;
    goto 0x4244;
    addr_4655_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_465a_10;
}

void fun_47a2() {
    goto 0x42d7;
}

void fun_4b70() {
    goto 0x42d7;
}

void fun_530f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x442c;
    } else {
        goto 0x4590;
    }
}

void fun_6310() {
}

void fun_47ac() {
    goto 0x4747;
}

void fun_4b7a() {
    goto 0x469d;
}

void fun_6370() {
    fun_2410();
    goto fun_25f0;
}

void fun_447d() {
    goto 0x40c2;
}

void fun_47b8() {
    goto 0x4747;
}

void fun_4b87() {
    goto 0x46ee;
}

void fun_63b0() {
    fun_2410();
    goto fun_25f0;
}

void fun_44aa() {
    goto 0x40c2;
}

void fun_47c4() {
    goto 0x46c0;
}

void fun_63f0() {
    fun_2410();
    goto fun_25f0;
}

void fun_44cc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4e60;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4391;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4391;
    }
    if (v11) 
        goto 0x51c3;
    if (r10_12 > r15_13) 
        goto addr_5213_8;
    addr_5218_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4f51;
    addr_5213_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5218_9;
}

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s23 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_6440() {
    int64_t r15_1;
    struct s21* rbx2;
    struct s0* r14_3;
    struct s22* rbx4;
    struct s0* r13_5;
    struct s23* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    struct s2* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2410();
    fun_25f0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6462, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6498() {
    fun_2410();
    goto 0x6469;
}

struct s24 {
    signed char[32] pad32;
    int64_t f20;
};

struct s25 {
    signed char[24] pad24;
    int64_t f18;
};

struct s26 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s27 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s28 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_64d0() {
    int64_t rcx1;
    struct s24* rbx2;
    int64_t r15_3;
    struct s25* rbx4;
    struct s0* r14_5;
    struct s26* rbx6;
    struct s0* r13_7;
    struct s27* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s28* rbx12;
    struct s0* rax13;
    struct s2* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2410();
    fun_25f0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6504, __return_address(), rcx1);
    goto v15;
}

void fun_6548() {
    fun_2410();
    goto 0x650b;
}
