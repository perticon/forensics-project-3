char * decimal_absval_add_one(char * buf) {
    int64_t v1 = (int64_t)buf;
    int64_t v2 = v1 + 1; // 0x31d8
    char v3 = *(char *)v2; // 0x31d8
    int64_t v4 = v3 == 45; // 0x31e0
    uint64_t v5 = v2 + v4; // 0x31e3
    int64_t v6 = function_2430(); // 0x31eb
    *(char *)(v4 + v1) = 48;
    int64_t v7 = v6 - 1 + v5; // 0x31f7
    char * v8 = (char *)v7;
    char v9 = *v8; // 0x31fb
    int64_t v10 = v7; // 0x3201
    char * v11 = v8; // 0x3201
    char v12 = v9; // 0x3201
    int64_t v13 = v7; // 0x3201
    if (v9 == 57) {
        *(char *)v10 = 48;
        v10--;
        char * v14 = (char *)v10;
        char v15 = *v14; // 0x320b
        v11 = v14;
        v12 = v15;
        v13 = v10;
        while (v15 == 57) {
            // 0x3208
            *(char *)v10 = 48;
            v10--;
            v14 = (char *)v10;
            v15 = *v14;
            v11 = v14;
            v12 = v15;
            v13 = v10;
        }
    }
    uint64_t v16 = v13;
    *v11 = v12 + 1;
    int64_t v17 = v5 > v16 ? v16 : v5; // 0x3222
    char * result; // 0x31d0
    if (v3 != 45) {
        // 0x3217
        result = (char *)v17;
    } else {
        char * v18 = (char *)(v17 - 1);
        *v18 = 45;
        result = v18;
    }
    // 0x3234
    return result;
}