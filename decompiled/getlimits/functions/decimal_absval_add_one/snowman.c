struct s0* decimal_absval_add_one(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8) {
    void* rax9;
    uint32_t ebp10;
    struct s0* rbx11;
    struct s0* rax12;
    void* rax13;
    struct s0* rdx14;
    uint32_t eax15;
    uint32_t eax16;
    struct s0* rax17;

    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    ebp10 = rdi->f1;
    *reinterpret_cast<unsigned char*>(&rax9) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&ebp10) == 45);
    rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdi) + reinterpret_cast<int64_t>(rax9) + 1);
    rax12 = fun_2430(rbx11);
    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx11) + 0xffffffffffffffff) = 48;
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) + reinterpret_cast<unsigned char>(rbx11));
    rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax13) + 0xffffffffffffffff);
    eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax13) + 0xffffffffffffffff);
    if (*reinterpret_cast<signed char*>(&eax15) == 57) {
        do {
            *reinterpret_cast<unsigned char*>(&rdx14->f0) = 48;
            eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx14) + 0xffffffffffffffff);
            rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx14) - 1);
        } while (*reinterpret_cast<signed char*>(&eax15) == 57);
    }
    eax16 = eax15 + 1;
    *reinterpret_cast<unsigned char*>(&rdx14->f0) = *reinterpret_cast<unsigned char*>(&eax16);
    rax17 = rdx14;
    if (reinterpret_cast<unsigned char>(rbx11) <= reinterpret_cast<unsigned char>(rdx14)) {
        rax17 = rbx11;
    }
    if (*reinterpret_cast<signed char*>(&ebp10) == 45) {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) = 45;
        rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax17) - 1);
    }
    return rax17;
}