char * decimal_absval_add_one(long param_1)

{
  char *pcVar1;
  char *__s;
  char cVar2;
  char cVar3;
  size_t sVar4;
  char *pcVar5;
  
  cVar2 = *(char *)(param_1 + 1);
  __s = (char *)(param_1 + 1 + (ulong)(cVar2 == '-'));
  sVar4 = strlen(__s);
  __s[-1] = '0';
  pcVar5 = __s + (sVar4 - 1);
  cVar3 = __s[sVar4 - 1];
  while (cVar3 == '9') {
    *pcVar5 = '0';
    pcVar1 = pcVar5 + -1;
    pcVar5 = pcVar5 + -1;
    cVar3 = *pcVar1;
  }
  *pcVar5 = cVar3 + '\x01';
  if (__s <= pcVar5) {
    pcVar5 = __s;
  }
  if (cVar2 == '-') {
    pcVar5[-1] = '-';
    pcVar5 = pcVar5 + -1;
  }
  return pcVar5;
}