void decimal_absval_add_one(struct Eq_460 * rdi)
{
	byte bpl_16 = rdi->b0001;
	Eq_43 rbx_22 = &rdi->b0001 + (uint64) ((int8) (bpl_16 == 0x2D));
	Eq_12 rax_24 = fn0000000000002430(rbx_22);
	*((word64) rbx_22 - 1) = 0x30;
	word64 rax_26 = rax_24 + rbx_22;
	uint64 rax_29 = (uint64) rax_26->bFFFFFFFF;
	Eq_43 rdx_27 = rax_26 - 1;
	word32 eax_41 = (word32) rax_29;
	if ((byte) rax_29 == 0x39)
	{
		do
		{
			*rdx_27 = 0x30;
			uint64 rax_36 = (uint64) *((word64) rdx_27 - 1);
			eax_41 = (word32) rax_36;
			--rdx_27;
		} while ((byte) rax_36 == 0x39);
	}
	*rdx_27 = (byte) eax_41 + 0x01;
	Eq_43 rax_52 = rdx_27;
	if (rbx_22 <= rdx_27)
		rax_52 = rbx_22;
	if (bpl_16 == 0x2D)
		*((word64) rax_52 - 1) = 0x2D;
}