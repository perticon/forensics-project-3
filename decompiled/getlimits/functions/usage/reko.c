void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000025F0(fn0000000000002410(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000339E;
	}
	fn0000000000002580(fn0000000000002410(0x05, "Usage: %s\n", null), 0x01);
	fn00000000000024C0(stdout, fn0000000000002410(0x05, "Output platform dependent limits in a format useful for shell scripts.\n\n", null));
	fn00000000000024C0(stdout, fn0000000000002410(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024C0(stdout, fn0000000000002410(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1246 * rbx_139 = fp - 0xB8 + 16;
	do
	{
		char * rsi_141 = rbx_139->qw0000;
		++rbx_139;
	} while (rsi_141 != null && fn00000000000024E0(rsi_141, "getlimits") != 0x00);
	ptr64 r13_154 = rbx_139->qw0008;
	if (r13_154 != 0x00)
	{
		fn0000000000002580(fn0000000000002410(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_43 rax_241 = fn0000000000002570(null, 0x05);
		if (rax_241 == 0x00 || fn0000000000002380(0x03, "en_", rax_241) == 0x00)
			goto l0000000000003556;
	}
	else
	{
		fn0000000000002580(fn0000000000002410(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_43 rax_183 = fn0000000000002570(null, 0x05);
		if (rax_183 == 0x00 || fn0000000000002380(0x03, "en_", rax_183) == 0x00)
		{
			fn0000000000002580(fn0000000000002410(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003593:
			fn0000000000002580(fn0000000000002410(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000339E:
			fn00000000000025D0(edi);
		}
		r13_154 = 0x8004;
	}
	fn00000000000024C0(stdout, fn0000000000002410(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003556:
	fn0000000000002580(fn0000000000002410(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003593;
}