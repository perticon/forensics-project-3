
int64_t g28;

unsigned char just_user = 0;

unsigned char just_group = 0;

unsigned char just_group_list = 0;

void** umaxtostr();

void** fun_24b0();

void fun_26c0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t ruid = 0;

void** fun_2490(int64_t rdi, void** rsi, ...);

uint32_t rgid = 0;

void*** fun_2530(int64_t rdi, void** rsi, ...);

int32_t euid = 0;

uint32_t egid = 0;

unsigned char print_group_list();

unsigned char ok = 1;

signed char use_real = 0;

unsigned char use_name = 0;

unsigned char print_group(int64_t rdi, void** rsi);

void* fun_26d0();

void** stdout = reinterpret_cast<void**>(0);

void fun_25b0(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t xgetgroups();

void fun_2540();

void*** fun_2420();

void fun_2400(void** rdi, void** rsi, ...);

signed char opt_zero = 0;

unsigned char multiple_users = 0;

void fun_24e0();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9);

void print_stuff(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9, ...) {
    int64_t rax7;
    int64_t v8;
    int1_t zf9;
    int1_t zf10;
    int1_t zf11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rax15;
    void* rsp16;
    void** r13_17;
    void** rdx18;
    void** rax19;
    void** rax20;
    int64_t rdi21;
    void*** rax22;
    void** rsp23;
    void** rdx24;
    int32_t edi25;
    int1_t zf26;
    void** rax27;
    void** rax28;
    int64_t rdi29;
    void** rax30;
    void** rdx31;
    uint32_t edi32;
    int1_t zf33;
    void** rax34;
    void** rax35;
    int64_t rdi36;
    void*** rax37;
    void** rdx38;
    unsigned char al39;
    int1_t zf40;
    void** rsi41;
    int64_t rdi42;
    unsigned char al43;
    int1_t zf44;
    int64_t rbp45;
    int1_t zf46;
    int64_t rdi47;
    void** rax48;
    void** rax49;
    void** rax50;
    void** rdi51;
    void** rax52;
    void** rsi53;
    int1_t zf54;
    void** rsi55;
    int32_t eax56;
    int32_t r12d57;
    void** rbp58;
    void** rdx59;
    void** rax60;
    int64_t r12_61;
    int64_t r12_62;
    int64_t rbp63;
    void** r14_64;
    void** rax65;
    int64_t rdi66;
    void* v67;
    void*** rax68;
    void** rdi69;
    void** rax70;
    void** v71;
    void** rdi72;
    int1_t zf73;
    void** rax74;
    int32_t esi75;
    int1_t zf76;
    int1_t zf77;
    void** rdi78;
    void** rax79;
    int64_t rax80;
    int64_t rax81;
    int64_t rax82;
    int32_t eax83;

    rax7 = g28;
    v8 = rax7;
    zf9 = just_user == 0;
    if (zf9) {
        zf10 = just_group == 0;
        if (zf10) {
            zf11 = just_group_list == 0;
            if (zf11) {
                rax12 = umaxtostr();
                rax13 = fun_24b0();
                fun_26c0(1, rax13, rax12, rcx);
                *reinterpret_cast<int32_t*>(&rdi14) = ruid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                rax15 = fun_2490(rdi14, rax13, rdi14, rax13);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                r13_17 = rax15;
                if (rax15) {
                    rdx18 = *reinterpret_cast<void***>(rax15);
                    fun_26c0(1, "(%s)", rdx18, rcx);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                }
                rax19 = umaxtostr();
                rax20 = fun_24b0();
                fun_26c0(1, rax20, rax19, rcx);
                *reinterpret_cast<uint32_t*>(&rdi21) = rgid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
                rax22 = fun_2530(rdi21, rax20, rdi21, rax20);
                rsp23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (rax22) {
                    rdx24 = *rax22;
                    fun_26c0(1, "(%s)", rdx24, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                edi25 = euid;
                zf26 = edi25 == ruid;
                if (!zf26 && (rax27 = umaxtostr(), rax28 = fun_24b0(), fun_26c0(1, rax28, rax27, rcx), *reinterpret_cast<int32_t*>(&rdi29) = euid, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0, rax30 = fun_2490(rdi29, rax28, rdi29, rax28), rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8), r13_17 = rax30, !!rax30)) {
                    rdx31 = *reinterpret_cast<void***>(rax30);
                    fun_26c0(1, "(%s)", rdx31, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                edi32 = egid;
                zf33 = edi32 == rgid;
                if (!zf33 && (rax34 = umaxtostr(), rax35 = fun_24b0(), fun_26c0(1, rax35, rax34, rcx), *reinterpret_cast<uint32_t*>(&rdi36) = egid, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi36) + 4) = 0, rax37 = fun_2530(rdi36, rax35, rdi36, rax35), rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8), !!rax37)) {
                    rdx38 = *rax37;
                    fun_26c0(1, "(%s)", rdx38, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                if (rdi) 
                    goto addr_3176_13;
            } else {
                al39 = print_group_list();
                ok = reinterpret_cast<unsigned char>(ok & al39);
                goto addr_2f0f_15;
            }
        } else {
            zf40 = use_real == 0;
            *reinterpret_cast<uint32_t*>(&rsi41) = use_name;
            *reinterpret_cast<int32_t*>(&rsi41 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi42) = egid;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
            if (!zf40) {
                *reinterpret_cast<uint32_t*>(&rdi42) = rgid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
            }
            al43 = print_group(rdi42, rsi41);
            ok = reinterpret_cast<unsigned char>(ok & al43);
            goto addr_2f0f_15;
        }
    } else {
        zf44 = use_real == 0;
        *reinterpret_cast<int32_t*>(&rbp45) = euid;
        if (!zf44) {
            zf46 = use_name == 0;
            *reinterpret_cast<int32_t*>(&rbp45) = ruid;
            if (!zf46) {
                addr_2ef0_21:
                *reinterpret_cast<int32_t*>(&rdi47) = *reinterpret_cast<int32_t*>(&rbp45);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi47) + 4) = 0;
                rax48 = fun_2490(rdi47, rsi);
                if (!rax48) {
                    rax49 = umaxtostr();
                    rax50 = fun_24b0();
                    rcx = rax49;
                    rdx = rax50;
                    fun_26d0();
                    ok = 0;
                    goto addr_2f9a_23;
                } else {
                    rdi51 = *reinterpret_cast<void***>(rax48);
                }
            } else {
                addr_2f9a_23:
                rax52 = umaxtostr();
                rdi51 = rax52;
            }
            rsi53 = stdout;
            fun_25b0(rdi51, rsi53, rdx, rcx);
            goto addr_2f0f_15;
        } else {
            zf54 = use_name == 0;
            if (zf54) 
                goto addr_2f9a_23; else 
                goto addr_2ef0_21;
        }
    }
    *reinterpret_cast<uint32_t*>(&rsi55) = egid;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    eax56 = xgetgroups();
    r12d57 = eax56;
    if (eax56 >= 0) {
        addr_319a_28:
        if (r12d57) {
            rbp58 = stdout;
            *reinterpret_cast<int32_t*>(&rdx59) = 5;
            *reinterpret_cast<int32_t*>(&rdx59 + 4) = 0;
            rax60 = fun_24b0();
            *reinterpret_cast<int32_t*>(&r12_61) = r12d57 - 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_61) + 4) = 0;
            r12_62 = r12_61 << 2;
            *reinterpret_cast<int32_t*>(&rbp63) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp63) + 4) = 0;
            fun_25b0(rax60, rbp58, 5, rcx);
            while (1) {
                r14_64 = stdout;
                rax65 = umaxtostr();
                rsi55 = r14_64;
                fun_25b0(rax65, rsi55, rdx59, rcx);
                *reinterpret_cast<int32_t*>(&rdi66) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(v67) + rbp63);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi66) + 4) = 0;
                rax68 = fun_2530(rdi66, rsi55, rdi66, rsi55);
                if (rax68) {
                    rdx59 = *rax68;
                    rsi55 = reinterpret_cast<void**>("(%s)");
                    fun_26c0(1, "(%s)", rdx59, rcx);
                }
                if (rbp63 == r12_62) 
                    break;
                rdi69 = stdout;
                rax70 = *reinterpret_cast<void***>(rdi69 + 40);
                if (reinterpret_cast<unsigned char>(rax70) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi69 + 48))) {
                    rdx59 = rax70 + 1;
                    *reinterpret_cast<void***>(rdi69 + 40) = rdx59;
                    *reinterpret_cast<void***>(rax70) = reinterpret_cast<void**>(44);
                } else {
                    fun_2540();
                }
                rbp63 = rbp63 + 4;
            }
        }
    } else {
        fun_24b0();
        fun_2420();
        fun_26d0();
        goto addr_32ec_38;
    }
    fun_2400(v71, rsi55, v71, rsi55);
    addr_2f0f_15:
    rdi72 = stdout;
    zf73 = opt_zero == 0;
    rax74 = *reinterpret_cast<void***>(rdi72 + 40);
    if (zf73) {
        esi75 = 10;
    } else {
        zf76 = just_group_list == 0;
        if (zf76 || (zf77 = multiple_users == 0, zf77)) {
            esi75 = 0;
        } else {
            if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
                fun_2540();
            } else {
                *reinterpret_cast<void***>(rdi72 + 40) = rax74 + 1;
                *reinterpret_cast<void***>(rax74) = reinterpret_cast<void**>(0);
            }
            rdi78 = stdout;
            rax79 = *reinterpret_cast<void***>(rdi78 + 40);
            if (reinterpret_cast<unsigned char>(rax79) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi78 + 48))) 
                goto addr_3400_47; else 
                goto addr_3071_48;
        }
    }
    if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
        rax80 = v8 - g28;
        if (rax80) {
            addr_346c_51:
            fun_24e0();
        } else {
            addr_31cc_52:
            goto addr_2540_53;
        }
    } else {
        *reinterpret_cast<void***>(rdi72 + 40) = rax74 + 1;
        *reinterpret_cast<void***>(rax74) = *reinterpret_cast<void***>(&esi75);
        goto addr_2f57_55;
    }
    addr_2540_53:
    addr_2f57_55:
    rax81 = v8 - g28;
    if (!rax81) {
        return;
    }
    addr_3400_47:
    rax82 = v8 - g28;
    if (rax82) 
        goto addr_346c_51;
    goto addr_31cc_52;
    addr_3071_48:
    *reinterpret_cast<void***>(rdi78 + 40) = rax79 + 1;
    *reinterpret_cast<void***>(rax79) = reinterpret_cast<void**>(0);
    goto addr_2f57_55;
    addr_32ec_38:
    ok = 0;
    goto addr_2f0f_15;
    addr_3176_13:
    *reinterpret_cast<uint32_t*>(&rsi55) = 0xffffffff;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    if (r13_17) {
        *reinterpret_cast<uint32_t*>(&rsi55) = *reinterpret_cast<uint32_t*>(r13_17 + 20);
        *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    }
    eax83 = xgetgroups();
    r12d57 = eax83;
    if (eax83 >= 0) 
        goto addr_319a_28;
    quote(rdi, rsi55, rsp23, rcx, r8, r9);
    fun_24b0();
    fun_2420();
    fun_26d0();
    goto addr_32ec_38;
}

int64_t fun_24c0();

int64_t fun_2410(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24c0();
    if (r8d > 10) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8900 + rax11 * 4) + 0x8900;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2580();

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s1* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x51df;
    rax8 = fun_2420();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6d11]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x526b;
            fun_2580();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc160) {
                fun_2400(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x52fa);
        }
        *rax8 = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_24e0();
        } else {
            return r14_19;
        }
    }
}

void** xstrdup(void** rdi, ...);

struct s2 {
    signed char[1] pad1;
    void** f1;
};

void** ximemdup();

void** fun_25e0(void** rdi, void** rsi);

void fun_2670(void** rdi, ...);

int32_t xstrtoul(void** rdi);

void fun_23e0(void** rdi, ...);

struct s3 {
    signed char[16] pad16;
    uint32_t f10;
};

struct s3* fun_2600(void** rdi, void** rsi, int32_t* rdx, uint32_t* rcx, void*** r8);

int64_t parse_with_separator(void** rdi, void** rsi, int32_t* rdx, uint32_t* rcx, void*** r8, void** r9) {
    void** r10_7;
    uint64_t r14_8;
    uint32_t* r12_9;
    void** rbp10;
    void*** rbx11;
    void* rsp12;
    int32_t* v13;
    int64_t rax14;
    int64_t v15;
    void** v16;
    void** r13_17;
    int64_t rax18;
    void** r15_19;
    void** rax20;
    void* rsp21;
    void** r10_22;
    uint32_t eax23;
    struct s2* r15_24;
    void** rax25;
    void** rax26;
    unsigned char v27;
    uint32_t eax28;
    int1_t zf29;
    int32_t eax30;
    void*** v31;
    int64_t rdi32;
    void*** rax33;
    void** rax34;
    void** rdi35;
    void** rax36;
    uint32_t eax37;
    int64_t rax38;
    struct s3* rax39;
    int32_t eax40;
    uint64_t v41;
    void** rax42;
    void** rax43;
    void** rax44;

    r10_7 = rsi;
    *reinterpret_cast<uint32_t*>(&r14_8) = 0xffffffff;
    r12_9 = rcx;
    rbp10 = r9;
    rbx11 = r8;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88);
    v13 = rdx;
    rax14 = g28;
    v15 = rax14;
    *reinterpret_cast<int32_t*>(&v16) = *rdx;
    if (rcx) {
        *reinterpret_cast<uint32_t*>(&r14_8) = *rcx;
    }
    if (rbx11) {
        *rbx11 = reinterpret_cast<void**>(0);
    }
    if (rbp10) {
        *reinterpret_cast<void***>(rbp10) = reinterpret_cast<void**>(0);
    }
    if (!r10_7) {
        *reinterpret_cast<int32_t*>(&r13_17) = 0;
        *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
        if (!*reinterpret_cast<void***>(rdi)) {
            while (1) {
                addr_6040_9:
                *v13 = *reinterpret_cast<int32_t*>(&v16);
                if (r12_9) {
                    *r12_9 = *reinterpret_cast<uint32_t*>(&r14_8);
                }
                if (rbx11) {
                    *rbx11 = r13_17;
                    *reinterpret_cast<int32_t*>(&r13_17) = 0;
                    *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                }
                addr_605f_13:
                if (rbp10) {
                    *reinterpret_cast<void***>(rbp10) = r10_7;
                    *reinterpret_cast<int32_t*>(&r10_7) = 0;
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                }
                v16 = r10_7;
                fun_2400(r13_17, rsi, r13_17, rsi);
                fun_2400(v16, rsi, v16, rsi);
                rax18 = v15 - g28;
                if (!rax18) 
                    break;
                addr_6312_16:
                fun_24e0();
                addr_6317_17:
                *reinterpret_cast<int32_t*>(&r10_7) = 0;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            }
        } else {
            v16 = r10_7;
            *reinterpret_cast<int32_t*>(&r15_19) = 0;
            *reinterpret_cast<int32_t*>(&r15_19 + 4) = 0;
            rax20 = xstrdup(rdi);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            r10_22 = v16;
            r13_17 = rax20;
            eax23 = 1;
            goto addr_60dc_19;
        }
    } else {
        r15_24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r10_7) - reinterpret_cast<unsigned char>(rdi));
        if (r15_24) {
            rsi = reinterpret_cast<void**>(&r15_24->f1);
            v16 = r10_7;
            rax25 = ximemdup();
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            r10_22 = v16;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax25) + reinterpret_cast<uint64_t>(r15_24)) = 0;
            r13_17 = rax25;
            if (!*reinterpret_cast<void***>(r10_22 + 1)) {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax25) == 43)) 
                    goto addr_6229_23;
                rdi = rax25;
                rax26 = fun_25e0(rdi, rsi);
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (!rax26) 
                    goto addr_6229_23;
                v27 = 1;
                *reinterpret_cast<int32_t*>(&r15_19) = 0;
                *reinterpret_cast<int32_t*>(&r15_19 + 4) = 0;
                goto addr_6104_26;
            } else {
                r15_19 = r10_22 + 1;
                eax23 = 0;
                goto addr_60dc_19;
            }
        } else {
            eax28 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r10_7 + 1));
            if (!*reinterpret_cast<signed char*>(&eax28)) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r10_7) = 0;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                goto addr_6040_9;
            } else {
                r15_19 = r10_7 + 1;
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                goto addr_6013_31;
            }
        }
    }
    return 0;
    addr_60dc_19:
    *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(!!r10_22);
    *reinterpret_cast<uint32_t*>(&rdx) = *reinterpret_cast<uint32_t*>(&rdx) & eax23;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx) + 4) = 0;
    zf29 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_17) == 43);
    v27 = *reinterpret_cast<unsigned char*>(&rdx);
    if (zf29) 
        goto addr_6247_33;
    rdi = r13_17;
    rax26 = fun_25e0(rdi, rsi);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
    if (!rax26) {
        if (v27) {
            addr_6229_23:
            fun_2670(rdi, rdi);
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            r12_9 = reinterpret_cast<uint32_t*>("invalid spec");
        } else {
            addr_6247_33:
            *reinterpret_cast<int32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rcx = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp21) + 40);
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx) + 4) = 0;
            r8 = reinterpret_cast<void***>(0x8d04);
            eax30 = xstrtoul(r13_17);
            if (eax30 || (rbx11 = v31, *reinterpret_cast<int32_t*>(&v16) = *reinterpret_cast<int32_t*>(&rbx11), reinterpret_cast<uint64_t>(rbx11) > 0xfffffffe)) {
                fun_2670(r13_17, r13_17);
                *reinterpret_cast<int32_t*>(&rbp10) = 0;
                *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
                r12_9 = reinterpret_cast<uint32_t*>("invalid user");
            } else {
                fun_2670(r13_17, r13_17);
                if (r15_19) {
                    eax28 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_19));
                    *reinterpret_cast<int32_t*>(&rbx11) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
                    goto addr_6013_31;
                } else {
                    *reinterpret_cast<int32_t*>(&r10_7) = 0;
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    *v13 = *reinterpret_cast<int32_t*>(&rbx11);
                    if (!r12_9) 
                        goto addr_605f_13;
                    *r12_9 = *reinterpret_cast<uint32_t*>(&r14_8);
                    goto addr_605f_13;
                }
            }
        }
    } else {
        addr_6104_26:
        *reinterpret_cast<int32_t*>(&rcx) = *reinterpret_cast<int32_t*>(rax26 + 16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx) + 4) = 0;
        *reinterpret_cast<int32_t*>(&v16) = *reinterpret_cast<int32_t*>(&rcx);
        if (v27) {
            *reinterpret_cast<uint32_t*>(&r14_8) = *reinterpret_cast<uint32_t*>(rax26 + 20);
            *reinterpret_cast<uint32_t*>(&rdi32) = *reinterpret_cast<uint32_t*>(&r14_8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
            rax33 = fun_2530(rdi32, rsi);
            if (!rax33) {
                rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 + 48);
                rax34 = umaxtostr();
                rdi35 = rax34;
            } else {
                rdi35 = *rax33;
            }
            rax36 = xstrdup(rdi35, rdi35);
            fun_23e0(rdi35, rdi35);
            fun_2670(rdi35, rdi35);
            r10_7 = rax36;
            goto addr_6040_9;
        } else {
            fun_2670(rdi, rdi);
            if (!r15_19) 
                goto addr_6317_17;
            eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_19));
            if (*reinterpret_cast<signed char*>(&eax37) != 43) 
                goto addr_601b_47; else 
                goto addr_6130_48;
        }
    }
    addr_616a_49:
    fun_2400(r13_17, rsi, r13_17, rsi);
    fun_2400(rbp10, rsi, rbp10, rsi);
    rax38 = v15 - g28;
    if (rax38) 
        goto addr_6312_16;
    addr_6013_31:
    if (*reinterpret_cast<signed char*>(&eax28) == 43) 
        goto addr_6130_48;
    addr_601b_47:
    rax39 = fun_2600(r15_19, rsi, rdx, rcx, r8);
    if (!rax39) {
        addr_6130_48:
        *reinterpret_cast<int32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax40 = xstrtoul(r15_19);
        if (eax40 || (r14_8 = v41, r14_8 > 0xfffffffe)) {
            fun_23e0(r15_19, r15_19);
            r12_9 = reinterpret_cast<uint32_t*>("invalid group");
            rax42 = xstrdup(r15_19, r15_19);
            rbp10 = rax42;
            goto addr_616a_49;
        } else {
            fun_23e0(r15_19, r15_19);
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            rax43 = xstrdup(r15_19, r15_19);
            r10_7 = rax43;
            goto addr_6040_9;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&r14_8) = rax39->f10;
        fun_23e0(r15_19, r15_19);
        rax44 = xstrdup(r15_19, r15_19);
        r10_7 = rax44;
        goto addr_6040_9;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8899);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x8894);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x889d);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8890);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbdf8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbdf8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23d3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t endgrent = 0x2030;

void fun_23e3() {
    __asm__("cli ");
    goto endgrent;
}

int64_t getenv = 0x2040;

void fun_23f3() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2050;

void fun_2403() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2413() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2423() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2433() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2443() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_2453() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20b0;

void fun_2463() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20c0;

void fun_2473() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2483() {
    __asm__("cli ");
    goto fclose;
}

int64_t getpwuid = 0x20e0;

void fun_2493() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x20f0;

void fun_24a3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_24b3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_24c3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_24d3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_24e3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getuid = 0x2140;

void fun_24f3() {
    __asm__("cli ");
    goto getuid;
}

int64_t getopt_long = 0x2150;

void fun_2503() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2160;

void fun_2513() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2170;

void fun_2523() {
    __asm__("cli ");
    goto strchr;
}

int64_t getgrgid = 0x2180;

void fun_2533() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t __overflow = 0x2190;

void fun_2543() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21a0;

void fun_2553() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21b0;

void fun_2563() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21c0;

void fun_2573() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21d0;

void fun_2583() {
    __asm__("cli ");
    goto memset;
}

int64_t geteuid = 0x21e0;

void fun_2593() {
    __asm__("cli ");
    goto geteuid;
}

int64_t memcmp = 0x21f0;

void fun_25a3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2200;

void fun_25b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2210;

void fun_25c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t fputc_unlocked = 0x2220;

void fun_25d3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t getpwnam = 0x2230;

void fun_25e3() {
    __asm__("cli ");
    goto getpwnam;
}

int64_t memcpy = 0x2240;

void fun_25f3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t getgrnam = 0x2250;

void fun_2603() {
    __asm__("cli ");
    goto getgrnam;
}

int64_t fileno = 0x2260;

void fun_2613() {
    __asm__("cli ");
    goto fileno;
}

int64_t getgid = 0x2270;

void fun_2623() {
    __asm__("cli ");
    goto getgid;
}

int64_t getgroups = 0x2280;

void fun_2633() {
    __asm__("cli ");
    goto getgroups;
}

int64_t malloc = 0x2290;

void fun_2643() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22a0;

void fun_2653() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22b0;

void fun_2663() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t endpwent = 0x22c0;

void fun_2673() {
    __asm__("cli ");
    goto endpwent;
}

int64_t getegid = 0x22d0;

void fun_2683() {
    __asm__("cli ");
    goto getegid;
}

int64_t __freading = 0x22e0;

void fun_2693() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22f0;

void fun_26a3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2300;

void fun_26b3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2310;

void fun_26c3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2320;

void fun_26d3() {
    __asm__("cli ");
    goto error;
}

int64_t getgrouplist = 0x2330;

void fun_26e3() {
    __asm__("cli ");
    goto getgrouplist;
}

int64_t fseeko = 0x2340;

void fun_26f3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoul = 0x2350;

void fun_2703() {
    __asm__("cli ");
    goto strtoul;
}

int64_t __cxa_atexit = 0x2360;

void fun_2713() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2370;

void fun_2723() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2380;

void fun_2733() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2390;

void fun_2743() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23a0;

void fun_2753() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23b0;

void fun_2763() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23c0;

void fun_2773() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_26b0(int64_t rdi, ...);

void fun_24a0(int64_t rdi, int64_t rsi);

void fun_2470(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2500(int64_t rdi, void*** rsi, int64_t rdx, void** rcx);

void usage();

void** Version = reinterpret_cast<void**>(42);

void version_etc(void** rdi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2720();

int32_t optind = 0;

int64_t parse_user_spec(void** rdi, void** rsi);

uint32_t fun_2680(int64_t rdi, void** rsi, void** rdx);

uint32_t fun_2620(int64_t rdi, void** rsi, void** rdx);

int32_t fun_2590(int64_t rdi, void** rsi, void** rdx);

int32_t fun_24f0(int64_t rdi, ...);

void fun_23f0(int64_t rdi, void** rsi, void** rdx);

int64_t fun_27c3(int32_t edi, void*** rsi) {
    void* rbp3;
    void*** rbx4;
    void** rdi5;
    int64_t rax6;
    int64_t v7;
    int64_t r8_8;
    void** rcx9;
    int64_t rdi10;
    int32_t eax11;
    void* rsp12;
    void** rax13;
    void** rsi14;
    void** rdx15;
    void** rdi16;
    int64_t rax17;
    uint32_t esi18;
    uint32_t edx19;
    void* rax20;
    uint32_t r12d21;
    uint32_t r13d22;
    void* rbp23;
    void** rdx24;
    void** r15_25;
    void** v26;
    void** r12_27;
    int64_t rax28;
    void** rax29;
    void** rax30;
    void*** rax31;
    int64_t rdi32;
    void** rax33;
    void** rax34;
    int32_t tmp32_35;
    void** rdi36;
    void** rax37;
    int32_t eax38;
    uint32_t eax39;
    uint32_t eax40;
    uint32_t eax41;
    int64_t rax42;
    int64_t rdx43;
    void*** rax44;
    uint32_t eax45;
    uint32_t eax46;
    void** rax47;
    void*** rax48;
    int32_t eax49;
    int1_t zf50;
    void*** rax51;
    int32_t eax52;
    void** rax53;
    int1_t zf54;
    int1_t zf55;
    int1_t zf56;
    void*** rax57;
    int32_t eax58;
    int1_t zf59;
    void*** rax60;
    int32_t eax61;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rbx4 = rsi;
    rdi5 = *rsi;
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_26b0(6, 6);
    fun_24a0("coreutils", "/usr/local/share/locale");
    fun_2470("coreutils", "/usr/local/share/locale");
    atexit(0x3af0, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r8_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_8) + 4) = 0;
    rcx9 = reinterpret_cast<void**>(0xba60);
    *reinterpret_cast<int32_t*>(&rdi10) = *reinterpret_cast<int32_t*>(&rbp3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    eax11 = fun_2500(rdi10, rbx4, "agnruzGZ", 0xba60);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax11 != -1) {
        if (eax11 > 0x7a) {
            addr_2bca_4:
            usage();
            rax13 = fun_24b0();
            rsi14 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
            rdx15 = rax13;
            fun_26d0();
            goto addr_2bf8_5;
        } else {
            if (eax11 <= 70) {
                if (eax11 == 0xffffff7d) {
                    rdi16 = stdout;
                    rcx9 = Version;
                    r8_8 = reinterpret_cast<int64_t>("Arnold Robbins");
                    version_etc(rdi16, "id", "GNU coreutils", rcx9, "Arnold Robbins", "David MacKenzie", 0, 0x2853);
                    eax11 = fun_2720();
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 - 8 - 8 + 8 - 8 + 8);
                }
                if (eax11 != 0xffffff7e) 
                    goto addr_2bca_4;
            } else {
                *reinterpret_cast<uint32_t*>(&rax17) = eax11 - 71;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax17) <= 51) {
                    goto *reinterpret_cast<int32_t*>(0x8734 + rax17 * 4) + 0x8734;
                }
            }
        }
        *reinterpret_cast<int32_t*>(&rdi10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        usage();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    }
    esi18 = just_group;
    edx19 = just_user;
    *reinterpret_cast<uint32_t*>(&rcx9) = just_group_list;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    rax20 = reinterpret_cast<void*>(static_cast<int64_t>(optind));
    r12d21 = edx19;
    r13d22 = esi18;
    rsi14 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rcx9)));
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    rbp23 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3) - *reinterpret_cast<int32_t*>(&rax20)));
    if (reinterpret_cast<int32_t>(edx19 + esi18 + reinterpret_cast<unsigned char>(rsi14)) > reinterpret_cast<int32_t>(1)) {
        fun_24b0();
        fun_26d0();
    } else {
        *reinterpret_cast<uint32_t*>(&rdx24) = r12d21;
        *reinterpret_cast<int32_t*>(&rdx24 + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx24) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx24) | *reinterpret_cast<unsigned char*>(&r13d22));
        if (*reinterpret_cast<unsigned char*>(&rdx24)) 
            goto addr_297a_16;
        if (!*reinterpret_cast<unsigned char*>(&rcx9)) 
            goto addr_29e8_18;
        addr_297a_16:
        if (rbp23) 
            goto addr_2a18_19; else 
            goto addr_2983_20;
    }
    addr_2d0d_21:
    fun_24b0();
    fun_26d0();
    addr_2d31_22:
    fun_24b0();
    fun_26d0();
    addr_2d55_23:
    fun_24b0();
    fun_26d0();
    fun_24e0();
    addr_2d7e_25:
    fun_24b0();
    fun_26d0();
    addr_2a18_19:
    rdx15 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax20)));
    r15_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 16);
    multiple_users = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rbp23) > 1);
    rbp3 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp23) + reinterpret_cast<unsigned char>(rdx15));
    while (reinterpret_cast<uint64_t>(rax20) < reinterpret_cast<uint64_t>(rbp3)) {
        v26 = reinterpret_cast<void**>(0);
        r12_27 = rbx4[reinterpret_cast<uint64_t>(rax20) * 8];
        if (!*reinterpret_cast<void***>(r12_27) || (*reinterpret_cast<int32_t*>(&r8_8) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_8) + 4) = 0, *reinterpret_cast<int32_t*>(&rdx15) = 0, *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0, rcx9 = r15_25, rsi14 = reinterpret_cast<void**>(0xc110), rax28 = parse_user_spec(r12_27, 0xc110), !!rax28)) {
            addr_2a37_28:
            rax29 = quote(r12_27, rsi14, rdx15, rcx9, r8_8, "David MacKenzie");
            rax30 = fun_24b0();
            rax31 = fun_2420();
            rcx9 = rax29;
            rdx15 = rax30;
            rsi14 = *rax31;
            *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
            fun_26d0();
            ok = 0;
        } else {
            if (1) {
                *reinterpret_cast<int32_t*>(&rdi32) = euid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
                rax33 = fun_2490(rdi32, 0xc110);
                rdx15 = rax33;
            } else {
                rax34 = fun_25e0(0, 0xc110);
                rdx15 = rax34;
            }
            if (!rdx15) 
                goto addr_2a37_28; else 
                goto addr_2ae5_33;
        }
        addr_2a71_34:
        fun_2400(v26, rsi14, v26, rsi14);
        tmp32_35 = optind + 1;
        optind = tmp32_35;
        rax20 = reinterpret_cast<void*>(static_cast<int64_t>(optind));
        continue;
        addr_2ae5_33:
        if (1) {
            addr_2bf8_5:
            rdi36 = *reinterpret_cast<void***>(rdx15);
            rax37 = xstrdup(rdi36, rdi36);
            rdx15 = rdx15;
            v26 = rax37;
        }
        eax38 = *reinterpret_cast<int32_t*>(rdx15 + 16);
        euid = eax38;
        ruid = eax38;
        eax39 = *reinterpret_cast<uint32_t*>(rdx15 + 20);
        egid = eax39;
        rgid = eax39;
        print_stuff(v26, rsi14, rdx15, rcx9, r8_8, "David MacKenzie", v26, rsi14, rdx15);
        goto addr_2a71_34;
    }
    addr_2b9a_36:
    eax40 = ok;
    eax41 = eax40 ^ 1;
    *reinterpret_cast<uint32_t*>(&rax42) = *reinterpret_cast<unsigned char*>(&eax41);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
    rdx43 = v7 - g28;
    if (!rdx43) {
        return rax42;
    }
    addr_2983_20:
    if (!*reinterpret_cast<signed char*>(&r12d21)) {
        if (!*reinterpret_cast<unsigned char*>(&r13d22)) {
            if (!*reinterpret_cast<unsigned char*>(&rcx9)) 
                goto addr_2c9d_40; else 
                goto addr_2b2b_41;
        }
        do {
            addr_2b52_42:
            rax44 = fun_2420();
            *rax44 = reinterpret_cast<void**>(0);
            eax45 = fun_2680(rdi10, rsi14, rdx24);
            egid = eax45;
            if (eax45 + 1) 
                goto addr_2b79_43;
            if (*rax44) 
                goto addr_2d0d_21;
            addr_2b79_43:
            *rax44 = reinterpret_cast<void**>(0);
            eax46 = fun_2620(rdi10, rsi14, rdx24);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8);
            rgid = eax46;
            if (eax46 + 1) 
                break;
            if (!*rax44) 
                break;
            rax47 = fun_24b0();
            rsi14 = *rax44;
            *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi10) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            rdx24 = rax47;
            fun_26d0();
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
            addr_2c54_47:
            rax48 = fun_2420();
            *rax48 = reinterpret_cast<void**>(0);
            eax49 = fun_2590(rdi10, rsi14, rdx24);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
            euid = eax49;
            if (eax49 + 1) 
                break;
            if (*rax48) 
                goto addr_2d7e_25;
            while (zf50 = use_real == 0, !zf50) {
                do {
                    addr_2b2b_41:
                    rax51 = fun_2420();
                    *rax51 = reinterpret_cast<void**>(0);
                    rbx4 = rax51;
                    eax52 = fun_24f0(rdi10, rdi10);
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
                    ruid = eax52;
                    if (eax52 + 1) 
                        goto addr_2b4d_51;
                    if (!*rbx4) 
                        goto addr_2b4d_51;
                    addr_29c4_54:
                    rax53 = fun_24b0();
                    rsi14 = *rbx4;
                    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                    rdx24 = rax53;
                    rax20 = fun_26d0();
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
                    addr_29e8_18:
                    zf54 = use_real == 0;
                    if (!zf54) 
                        goto addr_2d55_23;
                    zf55 = use_name == 0;
                    if (!zf55) 
                        goto addr_2d55_23;
                    zf56 = opt_zero == 0;
                    if (!zf56) 
                        goto addr_2d31_22;
                    if (rbp23) 
                        goto addr_2a18_19;
                    rdi10 = reinterpret_cast<int64_t>("POSIXLY_CORRECT");
                    fun_23f0("POSIXLY_CORRECT", rsi14, rdx24);
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                    addr_2c9d_40:
                    rax57 = fun_2420();
                    *rax57 = reinterpret_cast<void**>(0);
                    eax58 = fun_2590(rdi10, rsi14, rdx24);
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
                    euid = eax58;
                    if (eax58 + 1) 
                        goto addr_2b2b_41;
                    if (*rax57) 
                        goto addr_2d7e_25;
                    if (*reinterpret_cast<signed char*>(&r12d21)) 
                        break;
                } while (!*reinterpret_cast<unsigned char*>(&r13d22));
                goto addr_2cd6_62;
            }
            break;
            addr_2cd6_62:
            goto addr_2b52_42;
            addr_2b4d_51:
        } while (!*reinterpret_cast<signed char*>(&r12d21));
    } else {
        zf59 = use_real == 0;
        if (zf59) 
            goto addr_2c54_47;
        rax60 = fun_2420();
        *rax60 = reinterpret_cast<void**>(0);
        rbx4 = rax60;
        eax61 = fun_24f0(rdi10);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
        ruid = eax61;
        if (eax61 + 1) 
            goto addr_2b93_65;
        if (*rbx4) 
            goto addr_29c4_54;
    }
    addr_2b93_65:
    print_stuff(0, rsi14, rdx24, rcx9, r8_8, "David MacKenzie");
    goto addr_2b9a_36;
}

int64_t __libc_start_main = 0;

void fun_2db3() {
    __asm__("cli ");
    __libc_start_main(0x27c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_23d0(int64_t rdi);

int64_t fun_2e53() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23d0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2e93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

int32_t fun_2430(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2740(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3483(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** rcx4;
    void** r12_5;
    void** rax6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    uint32_t ecx15;
    uint32_t ecx16;
    int1_t zf17;
    void** rax18;
    void** rax19;
    int32_t eax20;
    void** rax21;
    void** r13_22;
    void** rax23;
    void** rax24;
    int32_t eax25;
    void** rax26;
    void** r14_27;
    void** rax28;
    void** rax29;
    void** rax30;
    void** rdi31;
    void** r8_32;
    void** r9_33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_24b0();
            fun_26c0(1, rax3, r12_2, rcx4);
            r12_5 = stdout;
            rax6 = fun_24b0();
            fun_25b0(rax6, r12_5, 5, rcx4);
            r12_7 = stdout;
            rax8 = fun_24b0();
            fun_25b0(rax8, r12_7, 5, rcx4);
            r12_9 = stdout;
            rax10 = fun_24b0();
            fun_25b0(rax10, r12_9, 5, rcx4);
            r12_11 = stdout;
            rax12 = fun_24b0();
            fun_25b0(rax12, r12_11, 5, rcx4);
            r12_13 = stdout;
            rax14 = fun_24b0();
            fun_25b0(rax14, r12_13, 5, rcx4);
            do {
                if (1) 
                    break;
                ecx15 = __cxa_finalize;
            } while (0x69 != ecx15 || ((ecx16 = g1, 100 != ecx16) || (zf17 = g2 == 0, !zf17)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax18 = fun_24b0();
                fun_26c0(1, rax18, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax19 = fun_26b0(5);
                if (!rax19 || (eax20 = fun_2430(rax19, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax20)) {
                    rax21 = fun_24b0();
                    r12_2 = reinterpret_cast<void**>("id");
                    r13_22 = reinterpret_cast<void**>(" invocation");
                    fun_26c0(1, rax21, "https://www.gnu.org/software/coreutils/", "id");
                } else {
                    r12_2 = reinterpret_cast<void**>("id");
                    goto addr_37eb_9;
                }
            } else {
                rax23 = fun_24b0();
                fun_26c0(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_26b0(5);
                if (!rax24 || (eax25 = fun_2430(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax25)) {
                    addr_36ee_11:
                    rax26 = fun_24b0();
                    r13_22 = reinterpret_cast<void**>(" invocation");
                    fun_26c0(1, rax26, "https://www.gnu.org/software/coreutils/", "id");
                    if (!reinterpret_cast<int1_t>(r12_2 == "id")) {
                        r13_22 = reinterpret_cast<void**>(0x8d04);
                    }
                } else {
                    addr_37eb_9:
                    r14_27 = stdout;
                    rax28 = fun_24b0();
                    fun_25b0(rax28, r14_27, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_36ee_11;
                }
            }
            rax29 = fun_24b0();
            rcx4 = r13_22;
            fun_26c0(1, rax29, r12_2, rcx4);
            addr_34d9_14:
            fun_2720();
        }
    } else {
        rax30 = fun_24b0();
        rdi31 = stderr;
        rcx4 = r12_2;
        fun_2740(rdi31, 1, rax30, rcx4, r8_32, r9_33, v34, v35, v36, v37, v38, v39);
        goto addr_34d9_14;
    }
}

int64_t fun_3823(int32_t edi, void** rsi) {
    int32_t r12d3;
    void** rbp4;
    void** rax5;
    void** rdi6;
    int64_t rdi7;
    void*** rax8;
    void** rax9;
    void** rcx10;
    void** rdx11;
    void** rsi12;
    int64_t rax13;

    __asm__("cli ");
    r12d3 = 1;
    *reinterpret_cast<int32_t*>(&rbp4) = edi;
    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&rsi)) {
        addr_3880_2:
        rax5 = umaxtostr();
        rdi6 = rax5;
    } else {
        *reinterpret_cast<int32_t*>(&rdi7) = *reinterpret_cast<int32_t*>(&rbp4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_2530(rdi7, rsi);
        if (!rax8) {
            r12d3 = 0;
            rax9 = fun_24b0();
            rcx10 = rbp4;
            rdx11 = rax9;
            fun_26d0();
            goto addr_3880_2;
        } else {
            rdi6 = *rax8;
            r12d3 = *reinterpret_cast<int32_t*>(&rsi);
        }
    }
    rsi12 = stdout;
    fun_25b0(rdi6, rsi12, rdx11, rcx10);
    *reinterpret_cast<int32_t*>(&rax13) = r12d3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    return rax13;
}

int64_t fun_38b3(void** rdi, void** rsi, uint32_t edx, void** rcx, int64_t r8, int64_t r9) {
    int32_t r12d7;
    uint32_t ebp8;
    uint32_t ebx9;
    void* rsp10;
    int32_t v11;
    unsigned char v12;
    int64_t rax13;
    int64_t v14;
    void** r15_15;
    int32_t r13d16;
    int64_t rdi17;
    void** rax18;
    uint32_t r12d19;
    int64_t rdi20;
    void** rsi21;
    unsigned char al22;
    void* rsp23;
    void** rdi24;
    void** rax25;
    uint32_t edx26;
    void** rsi27;
    int64_t rdi28;
    unsigned char al29;
    void** rsi30;
    int32_t eax31;
    void** rdi32;
    void** v33;
    void* r15_34;
    void* r14_35;
    void** rdi36;
    void** rax37;
    uint32_t ecx38;
    int64_t rdi39;
    void* v40;
    unsigned char al41;
    void** v42;
    int64_t rax43;
    int64_t rax44;

    __asm__("cli ");
    r12d7 = *reinterpret_cast<int32_t*>(&r8);
    ebp8 = *reinterpret_cast<uint32_t*>(&rcx);
    ebx9 = edx;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    v11 = *reinterpret_cast<int32_t*>(&r9);
    v12 = *reinterpret_cast<unsigned char*>(&r9);
    rax13 = g28;
    v14 = rax13;
    if (!rdi) {
        *reinterpret_cast<int32_t*>(&r15_15) = 0;
        *reinterpret_cast<int32_t*>(&r15_15 + 4) = 0;
        r13d16 = 1;
    } else {
        *reinterpret_cast<int32_t*>(&rdi17) = *reinterpret_cast<int32_t*>(&rsi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        rax18 = fun_2490(rdi17, rsi);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        r15_15 = rax18;
        *reinterpret_cast<unsigned char*>(&r13d16) = reinterpret_cast<uint1_t>(!!rax18);
    }
    r12d19 = *reinterpret_cast<unsigned char*>(&r12d7);
    *reinterpret_cast<uint32_t*>(&rdi20) = ebx9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rsi21) = r12d19;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    al22 = print_group(rdi20, rsi21);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    if (!al22) {
        r13d16 = 0;
    }
    if (ebx9 != ebp8) {
        rdi24 = stdout;
        rax25 = *reinterpret_cast<void***>(rdi24 + 40);
        if (reinterpret_cast<unsigned char>(rax25) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi24 + 48))) {
            fun_2540();
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        } else {
            *reinterpret_cast<void***>(rdi24 + 40) = rax25 + 1;
            edx26 = *reinterpret_cast<unsigned char*>(&v11);
            *reinterpret_cast<void***>(rax25) = *reinterpret_cast<void***>(&edx26);
        }
        *reinterpret_cast<uint32_t*>(&rsi27) = r12d19;
        *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi28) = ebp8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
        al29 = print_group(rdi28, rsi27);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        if (!al29) {
            r13d16 = 0;
        }
    }
    *reinterpret_cast<uint32_t*>(&rsi30) = ebp8;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    if (r15_15) {
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(r15_15 + 20);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    }
    eax31 = xgetgroups();
    if (eax31 < 0) {
        fun_2420();
        if (!rdi) {
            fun_24b0();
            fun_26d0();
        } else {
            quote(rdi, rsi30, reinterpret_cast<int64_t>(rsp23) + 16, rcx, r8, r9);
            fun_24b0();
            fun_26d0();
        }
        r13d16 = 0;
    } else {
        rdi32 = v33;
        *reinterpret_cast<int32_t*>(&r15_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_34) + 4) = 0;
        r14_35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(static_cast<int64_t>(eax31)) << 2);
        if (eax31) {
            do {
                if (*reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rdi32) + reinterpret_cast<uint64_t>(r15_34)) != ebx9 && *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rdi32) + reinterpret_cast<uint64_t>(r15_34)) != ebp8) {
                    rdi36 = stdout;
                    rax37 = *reinterpret_cast<void***>(rdi36 + 40);
                    if (reinterpret_cast<unsigned char>(rax37) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi36 + 48))) {
                        fun_2540();
                    } else {
                        *reinterpret_cast<void***>(rdi36 + 40) = rax37 + 1;
                        ecx38 = v12;
                        *reinterpret_cast<void***>(rax37) = *reinterpret_cast<void***>(&ecx38);
                    }
                    *reinterpret_cast<uint32_t*>(&rsi30) = r12d19;
                    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdi39) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(v40) + reinterpret_cast<uint64_t>(r15_34));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi39) + 4) = 0;
                    al41 = print_group(rdi39, rsi30);
                    rdi32 = v42;
                    if (!al41) {
                        r13d16 = 0;
                    }
                }
                r15_34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_34) + 4);
            } while (r14_35 != r15_34);
        }
        fun_2400(rdi32, rsi30);
    }
    rax43 = v14 - g28;
    if (rax43) {
        fun_24e0();
    } else {
        *reinterpret_cast<int32_t*>(&rax44) = r13d16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
        return rax44;
    }
}

int64_t file_name = 0;

void fun_3ad3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3ae3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2440(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3af3() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2420(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24b0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3b83_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26d0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2440(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3b83_5:
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26d0();
    }
}

struct s5 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_3ba3(uint64_t rdi, struct s5* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_2730(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s6 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s6* fun_2550();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_3c03(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s6* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2730("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2410("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2550();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2430(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_53a3(int64_t rdi) {
    int64_t rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2420();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xc260;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_53e3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5403(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc260);
    }
    *rdi = esi;
    return 0xc260;
}

int64_t fun_5423(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s7 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5463(struct s7* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xc260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s8 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s8* fun_5483(struct s8* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xc260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x278a;
    if (!rdx) 
        goto 0x278a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc260;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_54c3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s9* r8) {
    struct s9* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s9*>(0xc260);
    }
    rax7 = fun_2420();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x54f6);
    *rax7 = r15d8;
    return rax13;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5543(int64_t rdi, int64_t rsi, void*** rdx, struct s10* rcx) {
    struct s10* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s10*>(0xc260);
    }
    rax6 = fun_2420();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5571);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x55cc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5633() {
    __asm__("cli ");
}

void** gc098 = reinterpret_cast<void**>(96);

int64_t slotvec0 = 0x100;

void fun_5643() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2400(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0xc160) {
        fun_2400(rdi8, rsi9);
        gc098 = reinterpret_cast<void**>(0xc160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc090) {
        fun_2400(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0xc090);
    }
    nslots = 1;
    return;
}

void fun_56e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5703() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5713(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5733(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5753(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s0* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2790;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_57e3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s0* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2795;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_5873(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s0* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x279a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_24e0();
    } else {
        return rax5;
    }
}

void** fun_5903(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s0* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x279f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_5993(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x68c0]");
    __asm__("movdqa xmm1, [rip+0x68c8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x68b1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_24e0();
    } else {
        return rax10;
    }
}

void** fun_5a33(int64_t rdi, uint32_t esi) {
    struct s0* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6820]");
    __asm__("movdqa xmm1, [rip+0x6828]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6811]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_5ad3(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6780]");
    __asm__("movdqa xmm1, [rip+0x6788]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6769]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_24e0();
    } else {
        return rax3;
    }
}

void** fun_5b63(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x66f0]");
    __asm__("movdqa xmm1, [rip+0x66f8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x66e6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_24e0();
    } else {
        return rax4;
    }
}

void** fun_5bf3(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s0* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x27a4;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_5c93(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s0* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x65ba]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x65b2]");
    __asm__("movdqa xmm2, [rip+0x65ba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27a9;
    if (!rdx) 
        goto 0x27a9;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_5d33(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x651a]");
    __asm__("movdqa xmm1, [rip+0x6522]");
    __asm__("movdqa xmm2, [rip+0x652a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27ae;
    if (!rdx) 
        goto 0x27ae;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = rcx6 - g28;
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_5de3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s0* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x646a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6462]");
    __asm__("movdqa xmm2, [rip+0x646a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27b3;
    if (!rsi) 
        goto 0x27b3;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_5e83(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s0* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x63ca]");
    __asm__("movdqa xmm1, [rip+0x63d2]");
    __asm__("movdqa xmm2, [rip+0x63da]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27b8;
    if (!rsi) 
        goto 0x27b8;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void fun_5f23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f33(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5f73(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_2520(void** rdi, ...);

int64_t fun_6343(void** rdi, int32_t* rsi, uint32_t* rdx, void*** rcx, void** r8, signed char* r9) {
    int64_t rax7;
    int64_t r10_8;
    void** rax9;
    int64_t rax10;
    void** rax11;
    int32_t eax12;
    int64_t rax13;

    __asm__("cli ");
    if (!rdx) {
        rax7 = parse_with_separator(rdi, 0, rsi, 0, rcx, r8);
        r10_8 = rax7;
        goto addr_63e8_3;
    } else {
        rax9 = fun_2520(rdi, rdi);
        rax10 = parse_with_separator(rdi, rax9, rsi, rdx, rcx, r8);
        r10_8 = rax10;
        if (rax9 || (!rax10 || (rax11 = fun_2520(rdi, rdi), r10_8 = rax10, rax11 == 0))) {
            addr_63e8_3:
            if (!r9) {
                addr_63f1_5:
                return r10_8;
            } else {
                eax12 = 0;
            }
        } else {
            rax13 = parse_with_separator(rdi, rax11, rsi, rdx, rcx, r8);
            r10_8 = rax10;
            if (!rax13) {
                eax12 = 1;
                r10_8 = reinterpret_cast<int64_t>("warning: '.' should be ':'");
                if (!r9) {
                    *reinterpret_cast<int32_t*>(&r10_8) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0;
                    goto addr_63f1_5;
                }
            } else {
                goto addr_63e8_3;
            }
        }
        *r9 = *reinterpret_cast<signed char*>(&eax12);
        goto addr_63f1_5;
    }
}

int64_t fun_6443(void** rdi, int32_t* rsi, uint32_t* rdx, void*** rcx, void** r8) {
    void** rax6;
    int64_t rax7;
    int64_t r10_8;
    void** rax9;
    int64_t rax10;

    __asm__("cli ");
    if (!rdx) {
        goto parse_with_separator;
    } else {
        rax6 = fun_2520(rdi, rdi);
        rax7 = parse_with_separator(rdi, rax6, rsi, rdx, rcx, r8);
        r10_8 = rax7;
        if (!rax6 && (!rax7 || (rax9 = fun_2520(rdi, rdi), r10_8 = rax7, !!rax9) && (rax10 = parse_with_separator(rdi, rax9, rsi, rdx, rcx, r8), r10_8 = rax7, rax10 == 0))) {
            *reinterpret_cast<int32_t*>(&r10_8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0;
        }
        return r10_8;
    }
}

struct s11 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_6523(void** rdi, void** rsi, void** rdx, void** rcx, struct s11* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2740(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2740(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_24b0();
    fun_2740(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25d0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_24b0();
    fun_2740(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25d0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_24b0();
        fun_2740(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8fa8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8fa8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6993() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_69b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s12* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_24e0();
    } else {
        return;
    }
}

void fun_6a53(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6af6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6b00_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_24e0();
    } else {
        return;
    }
    addr_6af6_5:
    goto addr_6b00_7;
}

void fun_6b33() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25d0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24b0();
    fun_26c0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_24b0();
    fun_26c0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_24b0();
    goto fun_26c0;
}

int64_t fun_2460();

void xalloc_die();

void fun_6bd3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_2640(void** rdi);

void fun_6c13(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c33(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c53(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_26a0(void** rdi);

void fun_6c73(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26a0(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6ca3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26a0(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6cd3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6d13() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6d53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6d83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6dd3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2460();
            if (rax5) 
                break;
            addr_6e1d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_6e1d_5;
        rax8 = fun_2460();
        if (rax8) 
            goto addr_6e06_9;
        if (rbx4) 
            goto addr_6e1d_5;
        addr_6e06_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6e63(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2460();
            if (rax8) 
                break;
            addr_6eaa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_6eaa_5;
        rax11 = fun_2460();
        if (rax11) 
            goto addr_6e92_9;
        if (!rbx6) 
            goto addr_6e92_9;
        if (r12_4) 
            goto addr_6eaa_5;
        addr_6e92_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6ef3(void** rdi, void** rsi, void** rdx, void* rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_6f9d_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_6fb0_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_6f50_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_6f76_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6fc4_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_6f6d_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_6fc4_14;
            addr_6f6d_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6fc4_14;
            addr_6f76_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_26a0(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6fc4_14;
            if (!rbp13) 
                break;
            addr_6fc4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6f9d_9;
        } else {
            if (!r13_6) 
                goto addr_6fb0_10;
            goto addr_6f50_11;
        }
    }
}

int64_t fun_25c0();

void fun_6ff3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7023() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7053() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7073() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_25f0(signed char* rdi, void** rsi, void** rdx);

void fun_7093(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_70d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

struct s13 {
    signed char[1] pad1;
    void** f1;
};

void fun_7113(int64_t rdi, struct s13* rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_25f0;
    }
}

void** fun_24d0(void** rdi, ...);

void fun_7153(void** rdi) {
    void** rax2;
    void** rax3;

    __asm__("cli ");
    rax2 = fun_24d0(rdi);
    rax3 = fun_2640(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7193() {
    void** rdi1;

    __asm__("cli ");
    fun_24b0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26d0();
    fun_2410(rdi1);
}

int32_t mgetgroups();

int64_t fun_71d3() {
    int32_t eax1;
    void*** rax2;
    int64_t rax3;

    __asm__("cli ");
    eax1 = mgetgroups();
    if (eax1 != -1 || (rax2 = fun_2420(), !reinterpret_cast<int1_t>(*rax2 == 12))) {
        *reinterpret_cast<int32_t*>(&rax3) = eax1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        xalloc_die();
    }
}

void fun_2570(int64_t rdi);

void** fun_2770(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_2700(void** rdi);

int64_t fun_7203(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    int64_t rax7;
    int64_t v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    int64_t rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void*** rax19;
    void*** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    void** rax26;
    int64_t rax27;
    void** rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    void** rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoul");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2570("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_24e0();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_7574_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_7574_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_72bd_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_72c5_13:
            rax15 = v8 - g28;
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2420();
    *rax19 = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2770(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_72fb_21;
    rsi = r15_14;
    rax24 = fun_2700(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2520(r13_18), r8 = r8, rax26 == 0))) {
            addr_72fb_21:
            r12d11 = 4;
            goto addr_72c5_13;
        } else {
            addr_7339_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2520(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x9058 + rbp33 * 4) + 0x9058;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_72fb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_72bd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_72bd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2520(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_7339_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_72c5_13;
}

int64_t fun_2450();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7633(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void*** rax5;
    void*** rax6;

    __asm__("cli ");
    rax2 = fun_2450();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_768e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2420();
            *rax5 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_768e_3;
            rax6 = fun_2420();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax6 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2610(struct s14* rdi);

int32_t fun_2690(struct s14* rdi);

int64_t fun_2560(int64_t rdi, ...);

int32_t rpl_fflush(struct s14* rdi);

int64_t fun_2480(struct s14* rdi);

int64_t fun_76a3(struct s14* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2610(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2690(rdi);
        if (!(eax3 && (eax4 = fun_2610(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2560(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2420();
            r12d9 = *rax8;
            rax10 = fun_2480(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2480;
}

void rpl_fseeko(struct s14* rdi);

void fun_7733(struct s14* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2690(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7783(struct s14* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2610(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2560(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2660(int64_t rdi);

signed char* fun_7803() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2660(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2510(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7843(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2510(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_24e0();
    } else {
        return r12_7;
    }
}

int64_t fun_2630();

int32_t fun_26e0(int64_t rdi, int64_t rsi, void** rdx, void* rcx);

int64_t fun_78d3(int64_t rdi, void** esi, void*** rdx) {
    void** ebp4;
    void*** v5;
    int64_t rax6;
    int64_t v7;
    int64_t rax8;
    int64_t r12_9;
    void*** rax10;
    void** rax11;
    int32_t r12d12;
    void** rax13;
    void** r13_14;
    void** rax15;
    int64_t rbx16;
    int32_t v17;
    void** rax18;
    void** r14_19;
    void* r13_20;
    int64_t rsi21;
    int32_t eax22;
    uint64_t rsi23;
    void** rsi24;
    int64_t rax25;
    void** rax26;
    int64_t rax27;
    int64_t rax28;
    void** rsi29;
    int64_t rax30;
    void** esi31;
    void*** rcx32;
    void*** rax33;
    int64_t rax34;
    void*** rax35;

    __asm__("cli ");
    ebp4 = esi;
    v5 = rdx;
    rax6 = g28;
    v7 = rax6;
    if (!rdi) {
        rax8 = fun_2630();
        *reinterpret_cast<int32_t*>(&r12_9) = *reinterpret_cast<int32_t*>(&rax8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_9) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax8) < 0) {
            rax10 = fun_2420();
            if (!reinterpret_cast<int1_t>(*rax10 == 38) || (rax11 = fun_2640(4), rax11 == 0)) {
                addr_7b14_4:
                r12d12 = -1;
            } else {
                r12d12 = 0;
                *reinterpret_cast<void***>(rax11) = ebp4;
                *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(ebp4 == 0xffffffff));
                *v5 = rax11;
            }
        } else {
            if (!*reinterpret_cast<int32_t*>(&rax8) || ebp4 != 0xffffffff) {
                rax13 = fun_2640(reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(r12_9 + 1))) << 2);
                r13_14 = rax13;
                if (!rax13) 
                    goto addr_7b14_4;
                if (ebp4 == 0xffffffff) 
                    goto addr_7b1f_9; else 
                    goto addr_7a3d_10;
            } else {
                rax15 = fun_2640(reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax8))) << 2);
                r13_14 = rax15;
                if (!rax15) 
                    goto addr_7b14_4; else 
                    goto addr_7af6_12;
            }
        }
    } else {
        rbx16 = rdi;
        v17 = 10;
        rax18 = fun_2640(40);
        r14_19 = rax18;
        if (!rax18) 
            goto addr_7b14_4;
        r12d12 = 10;
        r13_20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 + 20);
        while (1) {
            *reinterpret_cast<void***>(&rsi21) = ebp4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            eax22 = fun_26e0(rbx16, rsi21, r14_19, r13_20);
            if (eax22 >= 0) {
                r12d12 = v17;
            } else {
                if (v17 == r12d12) {
                    r12d12 = r12d12 + r12d12;
                    v17 = r12d12;
                } else {
                    r12d12 = v17;
                }
            }
            rsi23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r12d12));
            rsi24 = reinterpret_cast<void**>(rsi23 << 2);
            *reinterpret_cast<uint32_t*>(&rax25) = reinterpret_cast<uint1_t>(!!(rsi23 >> 62));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
            if (__undefined()) 
                goto addr_7990_21;
            if (rax25) 
                goto addr_7990_21;
            rax26 = fun_26a0(r14_19);
            if (!rax26) 
                goto addr_799b_24;
            if (eax22 >= 0) 
                goto addr_79e0_26;
            r14_19 = rax26;
        }
    }
    addr_79a9_28:
    rax27 = v7 - g28;
    if (rax27) {
        fun_24e0();
    } else {
        *reinterpret_cast<int32_t*>(&rax28) = r12d12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        return rax28;
    }
    addr_7b1f_9:
    addr_7af6_12:
    rsi29 = r13_14;
    rax30 = fun_2630();
    r12d12 = *reinterpret_cast<int32_t*>(&rax30);
    if (*reinterpret_cast<int32_t*>(&rax30) >= 0) {
        addr_7a59_31:
        *v5 = r13_14;
        if (r12d12 > 1) {
            esi31 = *reinterpret_cast<void***>(r13_14);
            rcx32 = reinterpret_cast<void***>(r13_14 + r12d12 * 4);
            rax33 = reinterpret_cast<void***>(r13_14 + 4);
            if (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33)) {
                do {
                    if (*rax33 == esi31 || *rax33 == *reinterpret_cast<void***>(r13_14)) {
                        --r12d12;
                    } else {
                        *reinterpret_cast<void***>(r13_14 + 4) = *rax33;
                        r13_14 = r13_14 + 4;
                    }
                    rax33 = rax33 + 4;
                } while (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33));
                goto addr_79a9_28;
            } else {
                goto addr_79a9_28;
            }
        }
    } else {
        addr_7b0c_38:
        fun_2400(r13_14, rsi29);
        goto addr_7b14_4;
    }
    addr_7a3d_10:
    rsi29 = rax13 + 4;
    rax34 = fun_2630();
    if (*reinterpret_cast<int32_t*>(&rax34) < 0) 
        goto addr_7b0c_38;
    *reinterpret_cast<void***>(r13_14) = ebp4;
    r12d12 = static_cast<int32_t>(rax34 + 1);
    goto addr_7a59_31;
    addr_7990_21:
    rax35 = fun_2420();
    *rax35 = reinterpret_cast<void**>(12);
    addr_799b_24:
    r12d12 = -1;
    fun_2400(r14_19, rsi24, r14_19, rsi24);
    goto addr_79a9_28;
    addr_79e0_26:
    *v5 = rax26;
    goto addr_79a9_28;
}

int32_t setlocale_null_r();

int64_t fun_7b33() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_24e0();
    } else {
        return rax3;
    }
}

int64_t fun_7bb3(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_26b0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_24d0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25f0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25f0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7c63() {
    __asm__("cli ");
    goto fun_26b0;
}

void fun_7c73() {
    __asm__("cli ");
}

void fun_7c87() {
    __asm__("cli ");
    return;
}

void fun_2880() {
    opt_zero = 1;
    goto 0x2840;
}

uint32_t fun_25a0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2760(int64_t rdi, void** rsi);

uint32_t fun_2750(void** rdi, void** rsi);

void fun_3e35() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24b0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_24b0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24d0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4133_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4133_22; else 
                            goto addr_452d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_45ed_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4940_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4130_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4130_30; else 
                                goto addr_4959_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24d0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4940_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25a0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4940_28; else 
                            goto addr_3fdc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4aa0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4920_40:
                        if (r11_27 == 1) {
                            addr_44ad_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4a68_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_40e7_44;
                            }
                        } else {
                            goto addr_4930_46;
                        }
                    } else {
                        addr_4aaf_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_44ad_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4133_22:
                                if (v47 != 1) {
                                    addr_4689_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24d0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_46d4_54;
                                    }
                                } else {
                                    goto addr_4140_56;
                                }
                            } else {
                                addr_40e5_57:
                                ebp36 = 0;
                                goto addr_40e7_44;
                            }
                        } else {
                            addr_4914_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4aaf_47; else 
                                goto addr_491e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_44ad_41;
                        if (v47 == 1) 
                            goto addr_4140_56; else 
                            goto addr_4689_52;
                    }
                }
                addr_41a1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4038_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_405d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4360_65;
                    } else {
                        addr_41c9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4a18_67;
                    }
                } else {
                    goto addr_41c0_69;
                }
                addr_4071_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_40bc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4a18_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_40bc_81;
                }
                addr_41c0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_405d_64; else 
                    goto addr_41c9_66;
                addr_40e7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_419f_91;
                if (v22) 
                    goto addr_40ff_93;
                addr_419f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_41a1_62;
                addr_46d4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4e5b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4ecb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4ccf_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2760(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2750(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_47ce_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_418c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_47d8_112;
                    }
                } else {
                    addr_47d8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_48a9_114;
                }
                addr_4198_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_419f_91;
                while (1) {
                    addr_48a9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4db7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4816_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4dc5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4897_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4897_128;
                        }
                    }
                    addr_4845_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4897_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_4816_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4845_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_40bc_81;
                addr_4dc5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4a18_67;
                addr_4e5b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_47ce_109;
                addr_4ecb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_47ce_109;
                addr_4140_56:
                rax93 = fun_2770(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_418c_110;
                addr_491e_59:
                goto addr_4920_40;
                addr_45ed_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4133_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4198_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_40e5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4133_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4632_160;
                if (!v22) 
                    goto addr_4a07_162; else 
                    goto addr_4c13_163;
                addr_4632_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4a07_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4a18_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_44db_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4343_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4071_70; else 
                    goto addr_4357_169;
                addr_44db_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4038_63;
                goto addr_41c0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4914_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4a4f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4130_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4028_178; else 
                        goto addr_49d2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4914_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4133_22;
                }
                addr_4a4f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4130_30:
                    r8d42 = 0;
                    goto addr_4133_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_41a1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4a68_42;
                    }
                }
                addr_4028_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4038_63;
                addr_49d2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4930_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_41a1_62;
                } else {
                    addr_49e2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4133_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5192_188;
                if (v28) 
                    goto addr_4a07_162;
                addr_5192_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4343_168;
                addr_3fdc_37:
                if (v22) 
                    goto addr_4fd3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3ff3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4aa0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4b2b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4133_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4028_178; else 
                        goto addr_4b07_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4914_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4133_22;
                }
                addr_4b2b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4133_22;
                }
                addr_4b07_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4930_46;
                goto addr_49e2_186;
                addr_3ff3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4133_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4133_22; else 
                    goto addr_4004_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_50de_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4f64_210;
            if (1) 
                goto addr_4f62_212;
            if (!v29) 
                goto addr_4b9e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_50d1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4360_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_411b_219; else 
            goto addr_437a_220;
        addr_40ff_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4113_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_437a_220; else 
            goto addr_411b_219;
        addr_4ccf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_437a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4ced_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5160_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4bc6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_4db7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4113_221;
        addr_4c13_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4113_221;
        addr_4357_169:
        goto addr_4360_65;
        addr_50de_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_437a_220;
        goto addr_4ced_222;
        addr_4f64_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_4fbe_236;
        fun_24e0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5160_225;
        addr_4f62_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4f64_210;
        addr_4b9e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4f64_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_4bc6_226;
        }
        addr_50d1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_452d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8a2c + rax113 * 4) + 0x8a2c;
    addr_4959_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8b2c + rax114 * 4) + 0x8b2c;
    addr_4fd3_190:
    addr_411b_219:
    goto 0x3e00;
    addr_4004_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x892c + rax115 * 4) + 0x892c;
    addr_4fbe_236:
    goto v116;
}

void fun_4020() {
}

void fun_41d8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3ed2;
}

void fun_4231() {
    goto 0x3ed2;
}

void fun_431e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x41a1;
    }
    if (v2) 
        goto 0x4c13;
    if (!r10_3) 
        goto addr_4d7e_5;
    if (!v4) 
        goto addr_4c4e_7;
    addr_4d7e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4c4e_7:
    goto 0x4054;
}

void fun_433c() {
}

void fun_43e7() {
    signed char v1;

    if (v1) {
        goto 0x436f;
    } else {
        goto 0x40aa;
    }
}

void fun_4401() {
    signed char v1;

    if (!v1) 
        goto 0x43fa; else 
        goto "???";
}

void fun_4428() {
    goto 0x4343;
}

void fun_44a8() {
}

void fun_44c0() {
}

void fun_44ef() {
    goto 0x4343;
}

void fun_4541() {
    goto 0x44d0;
}

void fun_4570() {
    goto 0x44d0;
}

void fun_45a3() {
    goto 0x44d0;
}

void fun_4970() {
    goto 0x4028;
}

void fun_4c6e() {
    signed char v1;

    if (v1) 
        goto 0x4c13;
    goto 0x4054;
}

void fun_4d15() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4054;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4038;
        goto 0x4054;
    }
}

void fun_5132() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x43a0;
    } else {
        goto 0x3ed2;
    }
}

void fun_65f8() {
    fun_24b0();
}

void fun_73ac() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x73bb;
}

void fun_747c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x7489;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x73bb;
}

void fun_74a0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_74cc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73bb;
}

void fun_74ed() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73bb;
}

void fun_7511() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73bb;
}

void fun_7535() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74c4;
}

void fun_7559() {
}

void fun_7579() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74c4;
}

void fun_7595() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74c4;
}

void fun_2890() {
    just_user = 1;
    goto 0x2840;
}

void fun_425e() {
    goto 0x3ed2;
}

void fun_4434() {
    goto 0x43ec;
}

void fun_44fb() {
    goto 0x4028;
}

void fun_454d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x44d0;
    goto 0x40ff;
}

void fun_457f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x44db;
        goto 0x3f00;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x437a;
        goto 0x411b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4d18;
    if (r10_8 > r15_9) 
        goto addr_4465_9;
    addr_446a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4d23;
    goto 0x4054;
    addr_4465_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_446a_10;
}

void fun_45b2() {
    goto 0x40e7;
}

void fun_4980() {
    goto 0x40e7;
}

void fun_511f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x423c;
    } else {
        goto 0x43a0;
    }
}

void fun_66b0() {
}

void fun_744f() {
    if (__intrinsic()) 
        goto 0x7489; else 
        goto "???";
}

void fun_28a0() {
    use_real = 1;
    goto 0x2840;
}

void fun_45bc() {
    goto 0x4557;
}

void fun_498a() {
    goto 0x44ad;
}

void fun_6710() {
    fun_24b0();
    goto fun_2740;
}

void fun_28b0() {
    use_name = 1;
    goto 0x2840;
}

void fun_428d() {
    goto 0x3ed2;
}

void fun_45c8() {
    goto 0x4557;
}

void fun_4997() {
    goto 0x44fe;
}

void fun_6750() {
    fun_24b0();
    goto fun_2740;
}

void fun_28c0() {
    just_group = 1;
    goto 0x2840;
}

void fun_42ba() {
    goto 0x3ed2;
}

void fun_45d4() {
    goto 0x44d0;
}

void fun_6790() {
    fun_24b0();
    goto fun_2740;
}

void fun_28d0() {
    just_group_list = 1;
    goto 0x2840;
}

void fun_42dc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4c70;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x41a1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x41a1;
    }
    if (v11) 
        goto 0x4fd3;
    if (r10_12 > r15_13) 
        goto addr_5023_8;
    addr_5028_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4d61;
    addr_5023_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5028_9;
}

struct s15 {
    signed char[24] pad24;
    int64_t f18;
};

struct s16 {
    signed char[16] pad16;
    void** f10;
};

struct s17 {
    signed char[8] pad8;
    void** f8;
};

void fun_67e0() {
    int64_t r15_1;
    struct s15* rbx2;
    void** r14_3;
    struct s16* rbx4;
    void** r13_5;
    struct s17* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_24b0();
    fun_2740(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6802, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6838() {
    fun_24b0();
    goto 0x6809;
}

struct s18 {
    signed char[32] pad32;
    int64_t f20;
};

struct s19 {
    signed char[24] pad24;
    int64_t f18;
};

struct s20 {
    signed char[16] pad16;
    void** f10;
};

struct s21 {
    signed char[8] pad8;
    void** f8;
};

struct s22 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6870() {
    int64_t rcx1;
    struct s18* rbx2;
    int64_t r15_3;
    struct s19* rbx4;
    void** r14_5;
    struct s20* rbx6;
    void** r13_7;
    struct s21* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s22* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_24b0();
    fun_2740(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x68a4, __return_address(), rcx1);
    goto v15;
}

void fun_68e8() {
    fun_24b0();
    goto 0x68ab;
}
