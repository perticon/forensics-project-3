#include <stdint.h>

/* /tmp/tmp5zcoms8r @ 0x2db0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp5zcoms8r @ 0x3cb0 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000889d;
        rdx = 0x00008890;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00008897;
        rdx = 0x00008ca9;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00008899;
    rdx = 0x00008894;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x7800 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x2660 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp5zcoms8r @ 0x3d90 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2780)() ();
    }
    rdx = 0x00008900;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x8900 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x000088a1;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00008ca9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000892c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x892c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00008897;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00008ca9;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00008897;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00008ca9;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x00008a2c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x8a2c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x00008b2c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x8b2c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00008ca9;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00008897;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00008897;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00008ca9;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp5zcoms8r @ 0x2780 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 21762 named .text */
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x51b0 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2785)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x2785 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x278a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x2410 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp5zcoms8r @ 0x2790 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x2795 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x279a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x279f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x27a4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x27a9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x27ae */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x27b3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x27b8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x2ea0 */
 
int64_t dbg_print_stuff (void * arg1, int64_t arg3) {
    gid_t * groups;
    int64_t var_8h;
    rdi = arg1;
    rdx = arg3;
    /* void print_stuff(char const * pw_name); */
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (*(obj.just_user) == 0) {
        goto label_14;
    }
    ebp = euid;
    if (*(obj.use_real) != 0) {
        goto label_15;
    }
    r12 = obj_buf_1;
    if (*(obj.use_name) == 0) {
        goto label_7;
    }
    do {
        edi = ebp;
        rax = getpwuid ();
        if (rax == 0) {
            goto label_16;
        }
        rdi = *(rax);
label_0:
        rsi = stdout;
        fputs_unlocked ();
label_2:
        rdi = stdout;
        rax = *((rdi + 0x28));
        rdx = *((rdi + 0x30));
        if (*(obj.opt_zero) == 0) {
            goto label_17;
        }
        if (*(obj.just_group_list) != 0) {
            if (*(obj.multiple_users) != 0) {
                goto label_18;
            }
        }
        esi = 0;
label_1:
        if (rax >= rdx) {
            goto label_19;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = sil;
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_20;
        }
        return rax;
label_15:
        ebp = ruid;
        r12 = obj_buf_1;
    } while (*(obj.use_name) != 0);
label_7:
    rax = umaxtostr (rbp, r12, rdx);
    rdi = rax;
    goto label_0;
label_17:
    esi = 0xa;
    goto label_1;
label_14:
    if (*(obj.just_group) != 0) {
        esi = *(obj.use_name);
        edi = egid;
        if (*(obj.use_real) != 0) {
        }
        al = print_group (*(obj.rgid), rsi, rdx);
        *(obj.ok) &= al;
        goto label_2;
    }
    esi = ruid;
    if (*(obj.just_group_list) != 0) {
        r9d = *(obj.opt_zero);
        r8d = *(obj.use_name);
        r9d = (int32_t) r9b;
        r9d <<= 5;
        al = print_group_list (rdi, rsi, *(obj.rgid), *(obj.egid), r8, 1);
        *(obj.ok) &= al;
        goto label_2;
label_18:
        if (rax >= rdx) {
            goto label_21;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0;
label_12:
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_22;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0;
        goto label_3;
    }
    r12 = obj_buf_1;
    rax = umaxtostr (esi, r12, rdx);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, 0x0000858d);
    rdx = r13;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edi = ruid;
    rax = getpwuid ();
    r13 = rax;
    if (rax != 0) {
        rdx = *(rax);
        rsi = "(%s)";
        edi = 1;
        eax = 0;
        printf_chk ();
    }
    rbx = obj_buf_0;
    rax = umaxtostr (*(obj.rgid), rbx, rdx);
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, " gid=%s");
    edi = 1;
    rdx = r14;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edi = rgid;
    rax = getgrgid ();
    if (rax != 0) {
        rdx = *(rax);
        rsi = "(%s)";
        edi = 1;
        eax = 0;
        printf_chk ();
    }
    edi = euid;
    if (edi != *(obj.ruid)) {
        goto label_23;
    }
label_5:
    edi = egid;
    while (rax == 0) {
label_4:
        if (rbp == 0) {
            goto label_24;
        }
        esi = 0xffffffff;
        if (r13 != 0) {
        }
        eax = xgetgroups (rbp, *((r13 + 0x14)), rsp, rcx, r8, r9);
        r12d = eax;
        if (eax < 0) {
            goto label_25;
        }
label_6:
        if (r12d != 0) {
            goto label_26;
        }
label_8:
        free (*(rsp));
        goto label_2;
label_19:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_20;
        }
label_11:
        void (*0x2540)() ();
        rax = umaxtostr (rdi, rbx, rdx);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, " egid=%s");
        edi = 1;
        rdx = r12;
        rsi = rax;
        eax = 0;
        printf_chk ();
        edi = egid;
        rax = getgrgid ();
    }
    rdx = *(rax);
    rsi = "(%s)";
    edi = 1;
    eax = 0;
    printf_chk ();
    goto label_4;
label_23:
    rax = umaxtostr (rdi, r12, rdx);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, " euid=%s");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edi = euid;
    rax = getpwuid ();
    r13 = rax;
    if (rax == 0) {
        goto label_5;
    }
    rdx = *(rax);
    rsi = "(%s)";
    edi = 1;
    eax = 0;
    printf_chk ();
    goto label_5;
label_24:
    eax = xgetgroups (0, *(obj.egid), rsp, rcx, r8, r9);
    r12d = eax;
    if (eax >= 0) {
        goto label_6;
    }
    edx = 5;
    rax = dcgettext (0, "failed to get groups for the current process");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (0, *(rax), r12);
label_13:
    *(obj.ok) = 0;
    goto label_2;
label_16:
    r12 = obj_buf_1;
    rax = umaxtostr (rbp, r12, rdx);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot find name for user ID %s");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    *(obj.ok) = 0;
    goto label_7;
label_26:
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, " groups=");
    r12d--;
    r13 = "(%s)";
    rsi = rbp;
    rdi = rax;
    r12 <<= 2;
    ebp = 0;
    fputs_unlocked ();
    goto label_27;
label_9:
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x2c;
label_10:
    rbp += 4;
label_27:
    rax = *(rsp);
    r14 = stdout;
    rax = umaxtostr (*((rax + rbp)), rbx, rax + 1);
    rsi = r14;
    rdi = rax;
    fputs_unlocked ();
    rax = *(rsp);
    edi = *((rax + rbp));
    rax = getgrgid ();
    if (rax != 0) {
        rdx = *(rax);
        rsi = r13;
        edi = 1;
        eax = 0;
        printf_chk ();
    }
    if (rbp == r12) {
        goto label_8;
    }
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        goto label_9;
    }
    esi = 0x2c;
    overflow ();
    goto label_10;
label_22:
    rax = *((rsp + 8));
    rax -= *(fs:0x28);
    if (rax == 0) {
        esi = 0;
        goto label_11;
label_21:
        esi = 0;
        overflow ();
        goto label_12;
label_25:
        rax = quote (rbp, rsi, rdx, rcx, r8);
        edx = 5;
        r13 = rax;
        rax = dcgettext (0, "failed to get groups for user %s");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        error (0, *(rax), r12);
        goto label_13;
    }
label_20:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x2de0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x2e10 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x2e50 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000023d0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp5zcoms8r @ 0x23d0 */
 
void fcn_000023d0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp5zcoms8r @ 0x2e90 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp5zcoms8r @ 0x5f90 */
 
int64_t dbg_parse_with_separator (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    long unsigned int tmp;
    char[21] buf;
    void * ptr;
    int64_t var_10h;
    uint32_t var_18h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* char const * parse_with_separator(char const * spec,char const * separator,uid_t * uid,gid_t * gid,char ** username,char ** groupname); */
    r10 = rsi;
    r14d = 0xffffffff;
    r12 = rcx;
    rbx = r8;
    *((rsp + 0x10)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = *(rdx);
    *((rsp + 8)) = eax;
    if (rcx != 0) {
        r14d = *(rcx);
    }
    if (rbx != 0) {
        *(rbx) = 0;
    }
    if (rbp != 0) {
        *(rbp) = 0;
    }
    if (r10 == 0) {
        goto label_8;
    }
    r15 = r10;
    r15 -= rdi;
    if (r15 != 0) {
        goto label_9;
    }
    eax = *((r10 + 1));
    if (al == 0) {
        goto label_10;
    }
    r15 = r10 + 1;
    r13d = 0;
label_5:
    if (al == 0x2b) {
        goto label_11;
    }
    do {
        rdi = r15;
        rax = getgrnam ();
        if (rax == 0) {
            goto label_11;
        }
        r14d = *((rax + 0x10));
        endgrent ();
        rax = xstrdup (r15);
        r10 = rax;
label_0:
        rax = *((rsp + 0x10));
        ecx = *((rsp + 8));
        *(rax) = ecx;
        if (r12 != 0) {
            *(r12) = r14d;
        }
        if (rbx != 0) {
            *(rbx) = r13;
            r13d = 0;
        }
label_4:
        if (rbp != 0) {
            *(rbp) = r10;
            r10d = 0;
        }
        *((rsp + 8)) = r10;
        free (r13);
        free (*((rsp + 8)));
        rax = *((rsp + 0x48));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = 0;
        return rax;
label_9:
        *((rsp + 8)) = r10;
        rax = ximemdup (rdi, r15 + 1);
        r10 = *((rsp + 8));
        *((rax + r15)) = 0;
        r13 = rax;
        if (*((r10 + 1)) == 0) {
            goto label_13;
        }
        r15 = r10 + 1;
        eax = 0;
label_1:
        dl = (r10 != 0) ? 1 : 0;
        edx &= eax;
        *((rsp + 0x18)) = dl;
        if (*(r13) == 0x2b) {
            goto label_14;
        }
        rdi = r13;
        rax = getpwnam ();
        if (rax == 0) {
            goto label_15;
        }
label_7:
        ecx = *((rax + 0x10));
        *((rsp + 8)) = ecx;
        if (*((rsp + 0x18)) != 0) {
            goto label_16;
        }
        endpwent ();
        if (r15 == 0) {
            goto label_17;
        }
        eax = *(r15);
    } while (al != 0x2b);
label_11:
    eax = xstrtoul (r15, 0, 0xa, rsp + 0x28, 0x00008d04, r9);
    if (eax == 0) {
        goto label_18;
    }
label_2:
    endgrent ();
    r12 = "invalid group";
    rax = xstrdup (r15);
label_3:
    free (r13);
    free (rbp);
    rax = *((rsp + 0x48));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_12;
    }
    rsi = r12;
    edx = 5;
    edi = 0;
    void (*0x24b0)() ();
label_8:
    r13d = 0;
    if (*(rdi) == 0) {
        goto label_0;
    }
    *((rsp + 8)) = r10;
    r15d = 0;
    rax = xstrdup (rdi);
    r10 = *((rsp + 8));
    r13 = rax;
    eax = 1;
    goto label_1;
label_10:
    r13d = 0;
    r10d = 0;
    goto label_0;
label_18:
    r14 = *((rsp + 0x28));
    eax = 0xfffffffe;
    if (r14 > rax) {
        goto label_2;
    }
    endgrent ();
    ebp = 0;
    rax = xstrdup (r15);
    r10 = rax;
    goto label_0;
label_13:
    if (*(rax) != 0x2b) {
        goto label_19;
    }
    do {
label_6:
        endpwent ();
        ebp = 0;
        r12 = "invalid spec";
        goto label_3;
label_15:
    } while (*((rsp + 0x18)) != 0);
label_14:
    eax = xstrtoul (r13, 0, 0xa, rsp + 0x28, 0x00008d04, r9);
    if (eax != 0) {
        goto label_20;
    }
    rbx = *((rsp + 0x28));
    eax = 0xfffffffe;
    *((rsp + 8)) = ebx;
    if (rbx > rax) {
        goto label_20;
    }
    endpwent ();
    if (r15 != 0) {
        goto label_21;
    }
    rax = *((rsp + 0x10));
    r10d = 0;
    *(rax) = ebx;
    if (r12 == 0) {
        goto label_4;
    }
    *(r12) = r14d;
    goto label_4;
label_21:
    eax = *(r15);
    ebx = 0;
    goto label_5;
label_20:
    endpwent ();
    ebp = 0;
    r12 = 0x00008c68;
    goto label_3;
label_16:
    r14d = *((rax + 0x14));
    edi = *((rax + 0x14));
    rax = getgrgid ();
    if (rax == 0) {
        goto label_22;
    }
    do {
        rax = xstrdup (*(rax));
        *((rsp + 0x18)) = rax;
        endgrent ();
        endpwent ();
        r10 = *((rsp + 0x18));
        goto label_0;
label_22:
        rax = umaxtostr (r14d, rsp + 0x30, rdx);
        rdi = rax;
    } while (1);
label_12:
    rax = stack_chk_fail ();
label_17:
    r10d = 0;
    goto label_0;
label_19:
    rdi = rax;
    rax = getpwnam ();
    if (rax == 0) {
        goto label_6;
    }
    *((rsp + 0x18)) = 1;
    r15d = 0;
    goto label_7;
}

/* /tmp/tmp5zcoms8r @ 0x7c70 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp5zcoms8r @ 0x5700 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x78d0 */
 
int64_t dbg_mgetgroups (int64_t arg1, int64_t arg2, int64_t arg3) {
    int32_t max_n_groups;
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int mgetgroups(char const * username,gid_t gid,gid_t ** groups); */
    *((rsp + 8)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_6;
    }
    rbx = rdi;
    *((rsp + 0x14)) = 0xa;
    rax = malloc (0x28);
    r14 = rax;
    if (rax == 0) {
        goto label_7;
    }
    r12d = 0xa;
    r13 = rsp + 0x14;
    while (eax >= 0) {
        r12d = *((rsp + 0x14));
label_0:
        rsi = (int64_t) r12d;
        rax = rsi;
        rax >>= 0x3e;
        al = (rax != 0) ? 1 : 0;
        rsi <<= 2;
        eax = (int32_t) al;
        if (rsi < 0) {
            goto label_8;
        }
        if (rax != 0) {
            goto label_8;
        }
        rax = realloc (r14, rsi);
        if (rax == 0) {
            goto label_9;
        }
        if (r15d >= 0) {
            goto label_10;
        }
        r14 = rax;
        rcx = r13;
        rdx = r14;
        esi = ebp;
        rdi = rbx;
        eax = getgrouplist ();
        r15d = eax;
    }
    eax = *((rsp + 0x14));
    if (eax == r12d) {
        goto label_11;
    }
    r12d = eax;
    goto label_0;
label_8:
    errno_location ();
    *(rax) = 0xc;
label_9:
    r12d = 0xffffffff;
    free (r14);
    do {
label_1:
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_11:
        r12d += r12d;
        *((rsp + 0x14)) = r12d;
        goto label_0;
label_10:
        rcx = *((rsp + 8));
        *(rcx) = rax;
    } while (1);
label_6:
    esi = 0;
    edi = 0;
    eax = getgroups ();
    *((rsp + 0x14)) = eax;
    r12d = eax;
    if (eax < 0) {
        goto label_13;
    }
    if (eax != 0) {
        if (ebp == 0xffffffff) {
            goto label_14;
        }
    }
    ebx = r12 + 1;
    rdi = (int64_t) ebx;
    *((rsp + 0x14)) = ebx;
    rdi <<= 2;
    rax = malloc (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_7;
    }
    if (ebp == 0xffffffff) {
        goto label_15;
    }
    rsi = rax + 4;
    edi = r12d;
    eax = getgroups ();
    if (eax < 0) {
        goto label_16;
    }
    *(r13) = ebp;
    r12d = rax + 1;
label_4:
    rax = *((rsp + 8));
    *(rax) = r13;
    if (r12d <= 1) {
        goto label_1;
    }
    rax = (int64_t) r12d;
    esi = *(r13);
    rcx = r13 + rax*4;
    rax = r13 + 4;
    if (rcx > rax) {
        goto label_17;
    }
    goto label_1;
label_2:
    if (edx == *(r13)) {
        goto label_18;
    }
    *((r13 + 4)) = edx;
    r13 += 4;
label_3:
    rax += 4;
    if (rcx <= rax) {
        goto label_1;
    }
label_17:
    edx = *(rax);
    if (edx != esi) {
        goto label_2;
    }
label_18:
    r12d--;
    goto label_3;
label_13:
    rax = errno_location ();
    if (*(rax) != 0x26) {
        goto label_7;
    }
    rax = malloc (4);
    if (rax == 0) {
        goto label_7;
    }
    rcx = *((rsp + 8));
    r12d = 0;
    *(rax) = ebp;
    r12b = (ebp != 0xffffffff) ? 1 : 0;
    *(rcx) = rax;
    goto label_1;
label_14:
    rdi = (int64_t) eax;
    rdi <<= 2;
    rax = malloc (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_7;
    }
label_5:
    edi = r12d;
    rsi = r13;
    eax = getgroups ();
    r12d = eax;
    if (eax >= 0) {
        goto label_4;
    }
label_16:
    free (r13);
label_7:
    r12d = 0xffffffff;
    goto label_1;
label_15:
    r12d = ebx;
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5a30 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x24e0 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp5zcoms8r @ 0x6a50 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5540 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x2420 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp5zcoms8r @ 0x6c50 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x2640 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp5zcoms8r @ 0x7190 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00008847);
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x24b0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp5zcoms8r @ 0x26d0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp5zcoms8r @ 0x26a0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp5zcoms8r @ 0x2460 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp5zcoms8r @ 0x5460 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x7840 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5750 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2790)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x54c0 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x3c00 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp5zcoms8r @ 0x2550 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp5zcoms8r @ 0x2430 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp5zcoms8r @ 0x2730 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp5zcoms8r @ 0x5c90 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x27a9)() ();
    }
    if (rdx == 0) {
        void (*0x27a9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5d30 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x27ae)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x27ae)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5870 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x279a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5bf0 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x27a4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x7c84 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp5zcoms8r @ 0x6cd0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x6dd0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x71d0 */
 
uint64_t dbg_xgetgroups (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1ch, int64_t arg_20h, int64_t arg_28h) {
    int64_t var_45h;
    int64_t var_8h;
    int64_t var_28h;
    /* int xgetgroups(char const * username,gid_t gid,gid_t ** groups); */
    eax = mgetgroups (rdi, rsi, rdx);
    r12d = eax;
    while (*(rax) != 0xc) {
        eax = r12d;
        return eax;
        rax = errno_location ();
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x6c70 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7730 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2650)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp5zcoms8r @ 0x38b0 */
 
int64_t dbg_print_group_list (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    gid_t * groups;
    int64_t var_bh;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool print_group_list(char const * username,uid_t ruid,gid_t rgid,gid_t egid,_Bool use_names,char delim); */
    r14 = rdi;
    r12d = r8d;
    ebx = edx;
    *((rsp + 0xc)) = r9d;
    *((rsp + 0xb)) = r9b;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
    edi = esi;
    rax = getpwuid ();
    r15 = rax;
    r13b = (rax != 0) ? 1 : 0;
label_0:
    r12d = (int32_t) r12b;
    al = print_group (ebx, r12d, rdx);
    eax = 0;
    if (al == 0) {
        r13d = eax;
    }
    if (ebx != ebp) {
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_5;
        }
        *((rdi + 0x28)) = rdx;
        edx = *((rsp + 0xc));
        *(rax) = dl;
label_3:
        al = print_group (ebp, r12d, rax + 1);
        eax = 0;
        if (al != 0) {
            r13d = eax;
            goto label_6;
        }
    }
label_6:
    esi = ebp;
    if (r15 != 0) {
    }
    eax = xgetgroups (r14, *((r15 + 0x14)), rsp + 0x10, rcx, r8, r9);
    if (eax < 0) {
        goto label_7;
    }
    edx = *((rsp + 0xb));
    r14 = (int64_t) eax;
    rdi = *((rsp + 0x10));
    r15d = 0;
    r14 <<= 2;
    *((rsp + 0xc)) = edx;
    if (eax == 0) {
        goto label_8;
    }
    do {
        eax = *((rdi + r15));
        if (eax != ebx) {
            if (eax == ebp) {
                goto label_9;
            }
            rdi = stdout;
            rax = *((rdi + 0x28));
            if (rax >= *((rdi + 0x30))) {
                goto label_10;
            }
            rcx = rax + 1;
            *((rdi + 0x28)) = rcx;
            ecx = *((rsp + 0xb));
            *(rax) = cl;
label_1:
            rax = *((rsp + 0x10));
            al = print_group (*((rax + r15)), r12d, rdx);
            rdi = *((rsp + 0x10));
            if (al != 0) {
                goto label_9;
            }
            r13d = 0;
        }
label_9:
        r15 += 4;
    } while (r14 != r15);
label_8:
    free (rdi);
    do {
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_11;
        }
        eax = r13d;
        return rax;
label_4:
        r15d = 0;
        r13d = 1;
        goto label_0;
label_7:
        rax = errno_location ();
        rbx = rax;
        if (r14 == 0) {
            goto label_12;
        }
        rax = quote (r14, rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "failed to get groups for user %s");
        rcx = r12;
        eax = 0;
        error (0, *(rbx), rax);
label_2:
        r13d = 0;
    } while (1);
label_10:
    esi = *((rsp + 0xc));
    overflow ();
    goto label_1;
label_12:
    edx = 5;
    rax = dcgettext (0, "failed to get groups for the current process");
    eax = 0;
    error (0, *(rbx), rax);
    goto label_2;
label_5:
    esi = *((rsp + 0xc));
    overflow ();
    goto label_3;
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x6c10 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x5730 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x7150 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25f0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x24d0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp5zcoms8r @ 0x6c30 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x6990 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x6520)() ();
}

/* /tmp/tmp5zcoms8r @ 0x3af0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00008847);
    } while (1);
}

/* /tmp/tmp5zcoms8r @ 0x3ba0 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x7bb0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp5zcoms8r @ 0x6bd0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7b30 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5de0 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x27b3)() ();
    }
    if (rax == 0) {
        void (*0x27b3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5b60 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x7020 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x25c0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp5zcoms8r @ 0x5400 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x5f20 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x53a0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x7090 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25f0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x5640 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x3ae0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp5zcoms8r @ 0x6ca0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x6e60 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x6440 */
 
uint64_t dbg_parse_user_spec (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_48h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_28h;
    int64_t var_30h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* char const * parse_user_spec(char const * spec,uid_t * uid,gid_t * gid,char ** username,char ** groupname); */
    r14 = r8;
    r13 = rsi;
    rbx = rcx;
    if (rdx == 0) {
        goto label_0;
    }
    r12 = rdx;
    rax = strchr (rdi, 0x3a);
    rsi = rax;
    r15 = rax;
    rax = parse_with_separator (rbp, rsi, r13, r12, rbx, r14);
    r10 = rax;
    if (r15 != 0) {
        goto label_1;
    }
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    rax = strchr (rbp, 0x2e);
    r10 = *((rsp + 8));
    rsi = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = parse_with_separator (rbp, rsi, r13, r12, rbx, r14);
    r10 = *((rsp + 8));
    while (1) {
label_1:
        rax = r10;
        return rax;
label_0:
        r9 = r8;
        rdx = rsi;
        r8 = rcx;
        ecx = 0;
        esi = 0;
        void (*0x5f90)() ();
label_2:
        r10d = 0;
    }
}

/* /tmp/tmp5zcoms8r @ 0x76a0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2480)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp5zcoms8r @ 0x6d80 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x5f30 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x5e80 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x27b8)() ();
    }
    if (rax == 0) {
        void (*0x27b8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x70d0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25f0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x5630 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x5540)() ();
}

/* /tmp/tmp5zcoms8r @ 0x3820 */
 
uint64_t dbg_print_group (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool print_group(gid_t gid,_Bool use_name); */
    r12d = 1;
    if (sil != 0) {
        edi = ebp;
        ebx = esi;
        rax = getgrgid ();
        if (rax != 0) {
            rdi = *(rax);
            r12d = ebx;
            goto label_0;
        }
        edx = 5;
        r12d = 0;
        rax = dcgettext (0, "cannot find name for group ID %lu");
        rcx = rbp;
        eax = 0;
        error (0, 0, rax);
    }
    rax = umaxtostr (rbp, obj.buf.0_1, rdx);
    rdi = rax;
label_0:
    rsi = stdout;
    fputs_unlocked ();
    eax = r12d;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x5710 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x27c0 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    char * pw_name;
    int64_t var_8h;
    void * ptr;
    int64_t var_18h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r14 = obj_longopts;
    r13 = "agnruzGZ";
    r12 = 0x00008623;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00008d04);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x00008734;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_10;
        }
        if (eax > 0x7a) {
            goto label_11;
        }
        if (eax <= 0x46) {
            goto label_12;
        }
        eax -= 0x47;
        if (eax > 0x33) {
            goto label_11;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (52 cases) at 0x8734 */
        eax = void (*rax)() ();
        *(obj.opt_zero) = 1;
    } while (1);
    *(obj.just_user) = 1;
    goto label_0;
    *(obj.use_real) = 1;
    goto label_0;
    *(obj.use_name) = 1;
    goto label_0;
    *(obj.just_group) = 1;
    goto label_0;
    *(obj.just_group_list) = 1;
    goto label_0;
label_12:
    if (eax == 0xffffff7d) {
        eax = 0;
        version_etc (*(obj.stdout), 0x000085a6, "GNU coreutils", *(obj.Version), "Arnold Robbins", "David MacKenzie");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_11;
    }
    usage (0);
label_10:
    esi = *(obj.just_group);
    edx = *(obj.just_user);
    ecx = *(obj.just_group_list);
    rax = *(obj.optind);
    r12d = edx;
    r13d = esi;
    edx += esi;
    esi = (int32_t) cl;
    ebp -= eax;
    edx += esi;
    rbp = (int64_t) ebp;
    edx--;
    if (edx > 0) {
        goto label_13;
    }
    edx = r12d;
    dl |= r13b;
    if (dl == 0) {
        if (cl == 0) {
            goto label_14;
        }
    }
    if (rbp == 0) {
        if (r12b == 0) {
            goto label_15;
        }
        if (*(obj.use_real) == 0) {
            goto label_16;
        }
        errno_location ();
        *(rax) = 0;
        rbx = rax;
        eax = getuid ();
        *(obj.ruid) = eax;
        eax++;
        if (eax != 0) {
            goto label_4;
        }
        if (*(rbx) == 0) {
            goto label_4;
        }
label_9:
        edx = 5;
        rax = dcgettext (0, "cannot get real UID");
        eax = 0;
        eax = error (1, *(rbx), rax);
label_14:
        if (*(obj.use_real) != 0) {
            goto label_17;
        }
        if (*(obj.use_name) != 0) {
            goto label_17;
        }
        if (*(obj.opt_zero) != 0) {
            goto label_18;
        }
        if (rbp == 0) {
            goto label_19;
        }
    }
    rdx = (int64_t) eax;
    r15 = rsp + 0x10;
    obj.multiple_users = (rbp > 1) ? 1 : 0;
    r13 = "%s: no such user";
    rbp += rdx;
    while (*(r12) == 0) {
label_1:
        rax = quote (r12, rsi, rdx, rcx, r8);
        edx = 5;
        r14 = rax;
        rax = dcgettext (0, r13);
        r12 = rax;
        rax = errno_location ();
        rcx = r14;
        eax = 0;
        error (0, *(rax), r12);
        *(obj.ok) = 0;
label_2:
        free (*((rsp + 0x10)));
        *(obj.optind)++;
        rax = *(obj.optind);
        if (rax >= rbp) {
            goto label_20;
        }
        *((rsp + 0x10)) = 0;
        r12 = *((rbx + rax*8));
    }
    rax = parse_user_spec (r12, obj.euid, 0, r15, 0);
    if (rax != 0) {
        goto label_1;
    }
    rdi = *((rsp + 0x10));
    if (rdi == 0) {
        goto label_21;
    }
    rax = getpwnam ();
    rdx = rax;
label_3:
    if (rdx == 0) {
        goto label_1;
    }
    while (1) {
        eax = *((rdx + 0x10));
        *(obj.euid) = eax;
        *(obj.ruid) = eax;
        eax = *((rdx + 0x14));
        *(obj.egid) = eax;
        *(obj.rgid) = eax;
        print_stuff (*((rsp + 0x10)), rsi);
        goto label_2;
label_15:
        if (r13b == 0) {
            if (cl == 0) {
                goto label_22;
            }
label_5:
            errno_location ();
            *(rax) = 0;
            rbx = rax;
            eax = getuid ();
            *(obj.ruid) = eax;
            eax++;
            if (eax == 0) {
                goto label_23;
            }
label_8:
            if (r12b != 0) {
                goto label_4;
            }
        }
label_7:
        errno_location ();
        *(rax) = 0;
        rbx = rax;
        eax = getegid ();
        *(obj.egid) = eax;
        eax++;
        if (eax == 0) {
            if (*(rbx) != 0) {
                goto label_24;
            }
        }
        *(rbx) = 0;
        eax = getgid ();
        *(obj.rgid) = eax;
        eax++;
        if (eax == 0) {
            goto label_25;
        }
label_4:
        print_stuff (0, rsi);
label_20:
        eax = *(obj.ok);
        eax ^= 1;
        eax = (int32_t) al;
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_26;
        }
        return rax;
label_11:
        usage (1);
        edx = 5;
        rax = dcgettext (0, "--context (-Z) works only on an SELinux-enabled kernel");
        eax = 0;
        error (1, 0, rax);
        rdi = *(rdx);
        *((rsp + 8)) = rdx;
        rax = xstrdup (*(rdi));
        rdx = *((rsp + 8));
        *((rsp + 0x10)) = rax;
    }
label_21:
    edi = euid;
    rax = getpwuid ();
    rdx = rax;
    goto label_3;
label_25:
    if (*(rbx) == 0) {
        goto label_4;
    }
    edx = 5;
    rax = dcgettext (0, "cannot get real GID");
    eax = 0;
    error (1, *(rbx), rax);
label_16:
    errno_location ();
    *(rax) = 0;
    rbx = rax;
    eax = geteuid ();
    *(obj.euid) = eax;
    eax++;
    if (eax != 0) {
        goto label_4;
    }
    if (*(rbx) != 0) {
        goto label_27;
    }
label_6:
    if (*(obj.use_real) == 0) {
        goto label_4;
    }
    goto label_5;
label_19:
    getenv ("POSIXLY_CORRECT");
label_22:
    errno_location ();
    *(rax) = 0;
    rbx = rax;
    eax = geteuid ();
    *(obj.euid) = eax;
    eax++;
    if (eax != 0) {
        goto label_5;
    }
    if (*(rbx) != 0) {
        goto label_27;
    }
    if (r12b != 0) {
        goto label_6;
    }
    if (r13b == 0) {
        goto label_5;
    }
    goto label_7;
label_23:
    if (*(rbx) == 0) {
        goto label_8;
    }
    goto label_9;
label_13:
    edx = 5;
    rax = dcgettext (0, "cannot print \"only\" of more than one choice");
    eax = 0;
    error (1, 0, rax);
label_24:
    edx = 5;
    rax = dcgettext (0, "cannot get effective GID");
    eax = 0;
    error (1, *(rbx), rax);
label_18:
    edx = 5;
    rax = dcgettext (0, "option --zero not permitted in default format");
    eax = 0;
    error (1, 0, rax);
label_17:
    edx = 5;
    rax = dcgettext (0, "cannot print only names or real IDs in default format");
    eax = 0;
    error (1, 0, rax);
label_26:
    stack_chk_fail ();
label_27:
    edx = 5;
    rax = dcgettext (0, "cannot get effective UID");
    eax = 0;
    rax = error (1, *(rbx), rax);
}

/* /tmp/tmp5zcoms8r @ 0x3480 */
 
int64_t dbg_usage (int64_t arg1, char const * program) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    xmm4 = program;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [USER]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Print user and group information for each specified USER,\nor (when USER omitted) for the current process.\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\nWithout any OPTION, print some useful set of identified information.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x000085a9;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x69;
    *((rsp + 8)) = rax;
    rax = 0x00008623;
    edi = 0x64;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x000085a6;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000862d, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x000085c5;
    printf_chk ();
    rax = 0x00008d04;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000862d, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x000085a6;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x000085c5;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x000085a6;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmp5zcoms8r @ 0x2740 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp5zcoms8r @ 0x2720 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp5zcoms8r @ 0x26c0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp5zcoms8r @ 0x25b0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp5zcoms8r @ 0x26b0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp5zcoms8r @ 0x53e0 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x6340 */
 
uint64_t parse_user_spec_warn (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r15 = r8;
    r14 = rcx;
    r13 = rsi;
    rbx = r9;
    if (rdx == 0) {
        goto label_2;
    }
    r12 = rdx;
    rax = strchr (rdi, 0x3a);
    rsi = rax;
    *((rsp + 8)) = rax;
    rax = parse_with_separator (rbp, rsi, r13, r12, r14, r15);
    rsi = *((rsp + 8));
    r10 = rax;
    if (rsi != 0) {
        goto label_3;
    }
    if (rax == 0) {
        goto label_3;
    }
    *((rsp + 8)) = rax;
    rax = strchr (rbp, 0x2e);
    r10 = *((rsp + 8));
    rsi = rax;
    if (rax == 0) {
        goto label_3;
    }
    rax = parse_with_separator (rbp, rsi, r13, r12, r14, r15);
    r10 = *((rsp + 8));
    if (rax == 0) {
        goto label_4;
    }
    do {
label_3:
        if (rbx != 0) {
            eax = 0;
label_0:
            *(rbx) = al;
        }
label_1:
        rax = r10;
        return rax;
label_2:
        rax = parse_with_separator (rdi, 0, rsi, 0, rcx, r8);
        r10 = rax;
    } while (1);
label_4:
    eax = 1;
    r10 = "warning: '.' should be ':';
    if (rbx != 0) {
        goto label_0;
    }
    r10d = 0;
    goto label_1;
}

/* /tmp/tmp5zcoms8r @ 0x5ad0 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5900 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x279f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x57e0 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2795)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x5f70 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x5480 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x278a)() ();
    }
    if (rdx == 0) {
        void (*0x278a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x5990 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c270]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000c280]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x6ff0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7070 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7200 */
 
int64_t dbg_xstrtoul (long * arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1ch, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    char const[9] const __PRETTY_FUNCTION__;
    int64_t var_45h;
    char * t_ptr;
    int64_t var_8h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoul(char const * s,char ** ptr,int strtol_base,long unsigned int * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoul (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x00009058;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0x9058 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoul");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmp5zcoms8r @ 0x56e0 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x6d10 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x5f50 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp5zcoms8r @ 0x7780 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x26f0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp5zcoms8r @ 0x5420 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x6ef0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp5zcoms8r @ 0x6b30 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp5zcoms8r @ 0x25d0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp5zcoms8r @ 0x3ad0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp5zcoms8r @ 0x7630 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp5zcoms8r @ 0x2450 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp5zcoms8r @ 0x6520 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00008cbe);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00008fa8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x8fa8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2740)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2740)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2740)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp5zcoms8r @ 0x69b0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp5zcoms8r @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp5zcoms8r @ 0x7050 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7c60 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp5zcoms8r @ 0x6d50 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x7110 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x25f0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp5zcoms8r @ 0x23e0 */
 
void endgrent (void) {
    /* [15] -r-x section size 928 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp5zcoms8r @ 0x23f0 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmp5zcoms8r @ 0x2400 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp5zcoms8r @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rsi) += bh;
    *(rax) ^= al;
    *(rax) += al;
    *((rax + 0x2d)) += dh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dil;
    *(0x2800403d) += cl;
    *(rdi) += ah;
    *(rdi) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = 0x17;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = 0x17;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *((rcx + 0x5c)) += dl;
    *(rax) += al;
    *((rcx + 0x5c)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0x00000146) |= ah;
    *(rax) += cl;
    eax = 0;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmp5zcoms8r @ 0x2440 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp5zcoms8r @ 0x2470 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp5zcoms8r @ 0x2480 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp5zcoms8r @ 0x2490 */
 
void getpwuid (void) {
    __asm ("bnd jmp qword [reloc.getpwuid]");
}

/* /tmp/tmp5zcoms8r @ 0x24a0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp5zcoms8r @ 0x24c0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp5zcoms8r @ 0x24f0 */
 
void getuid (void) {
    __asm ("bnd jmp qword [reloc.getuid]");
}

/* /tmp/tmp5zcoms8r @ 0x2500 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp5zcoms8r @ 0x2510 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp5zcoms8r @ 0x2520 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmp5zcoms8r @ 0x2530 */
 
void getgrgid (void) {
    __asm ("bnd jmp qword [reloc.getgrgid]");
}

/* /tmp/tmp5zcoms8r @ 0x2540 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp5zcoms8r @ 0x2560 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp5zcoms8r @ 0x2570 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp5zcoms8r @ 0x2580 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp5zcoms8r @ 0x2590 */
 
void geteuid (void) {
    __asm ("bnd jmp qword [reloc.geteuid]");
}

/* /tmp/tmp5zcoms8r @ 0x25a0 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp5zcoms8r @ 0x25e0 */
 
void getpwnam (void) {
    __asm ("bnd jmp qword [reloc.getpwnam]");
}

/* /tmp/tmp5zcoms8r @ 0x25f0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp5zcoms8r @ 0x2600 */
 
void getgrnam (void) {
    __asm ("bnd jmp qword [reloc.getgrnam]");
}

/* /tmp/tmp5zcoms8r @ 0x2610 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp5zcoms8r @ 0x2620 */
 
void getgid (void) {
    __asm ("bnd jmp qword [reloc.getgid]");
}

/* /tmp/tmp5zcoms8r @ 0x2630 */
 
void getgroups (void) {
    __asm ("bnd jmp qword [reloc.getgroups]");
}

/* /tmp/tmp5zcoms8r @ 0x2650 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp5zcoms8r @ 0x2670 */
 
void endpwent (void) {
    __asm ("bnd jmp qword [reloc.endpwent]");
}

/* /tmp/tmp5zcoms8r @ 0x2680 */
 
void getegid (void) {
    __asm ("bnd jmp qword [reloc.getegid]");
}

/* /tmp/tmp5zcoms8r @ 0x2690 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp5zcoms8r @ 0x26e0 */
 
void getgrouplist (void) {
    __asm ("bnd jmp qword [reloc.getgrouplist]");
}

/* /tmp/tmp5zcoms8r @ 0x26f0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp5zcoms8r @ 0x2700 */
 
void strtoul (void) {
    __asm ("bnd jmp qword [reloc.strtoul]");
}

/* /tmp/tmp5zcoms8r @ 0x2710 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp5zcoms8r @ 0x2750 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp5zcoms8r @ 0x2760 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp5zcoms8r @ 0x2770 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp5zcoms8r @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 944 named .plt */
    __asm ("bnd jmp qword [0x0000bdf8]");
}

/* /tmp/tmp5zcoms8r @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp5zcoms8r @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}
