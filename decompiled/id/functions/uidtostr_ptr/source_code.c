uidtostr_ptr (uid_t const *uid)
{
  static char buf[INT_BUFSIZE_BOUND (uintmax_t)];
  return umaxtostr (*uid, buf);
}