print_user (uid_t uid)
{
  struct passwd *pwd = NULL;

  if (use_name)
    {
      pwd = getpwuid (uid);
      if (pwd == NULL)
        {
          error (0, 0, _("cannot find name for user ID %s"),
                 uidtostr (uid));
          ok &= false;
        }
    }

  char *s = pwd ? pwd->pw_name : uidtostr (uid);
  fputs (s, stdout);
}