void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002740(fn00000000000024B0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000034D9;
	}
	fn00000000000026C0(fn00000000000024B0(0x05, "Usage: %s [OPTION]... [USER]...\n", null), 0x01);
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "Print user and group information for each specified USER,\nor (when USER omitted) for the current process.\n\n", null));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "  -a             ignore, for compatibility with other versions\n  -Z, --context  print only the security context of the process\n  -g, --group    print only the effective group ID\n  -G, --groups   print all group IDs\n  -n, --name     print a name instead of a number, for -ugG\n  -r, --real     print the real ID instead of the effective ID, with -ugG\n  -u, --user     print only the effective user ID\n  -z, --zero     delimit entries with NUL characters, not whitespace;\n                   not permitted in default format\n", null));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "      --version     output version information and exit\n", null));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "\nWithout any OPTION, print some useful set of identified information.\n", null));
	struct Eq_1574 * rdx_143 = fp + ~0xA7;
	do
	{
		struct Eq_1594 * rax_167 = rdx_143[1];
		++rdx_143;
	} while (rax_167 != null && ((word32) rax_167->b0000 != 0x69 || ((word32) rax_167->b0001 != 100 || rax_167->b0002 != 0x00)));
	ptr64 r12_188 = rdx_143->qw0008;
	if (r12_188 != 0x00)
	{
		fn00000000000026C0(fn00000000000024B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_19 rax_275 = fn00000000000026B0(null, 0x05);
		if (rax_275 == 0x00 || fn0000000000002430(0x03, "en_", rax_275) == 0x00)
			goto l00000000000036EE;
	}
	else
	{
		fn00000000000026C0(fn00000000000024B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_19 rax_217 = fn00000000000026B0(null, 0x05);
		if (rax_217 == 0x00 || fn0000000000002430(0x03, "en_", rax_217) == 0x00)
		{
			fn00000000000026C0(fn00000000000024B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000372B:
			fn00000000000026C0(fn00000000000024B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000034D9:
			fn0000000000002720(edi);
		}
		r12_188 = 34214;
	}
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000036EE:
	fn00000000000026C0(fn00000000000024B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000372B;
}