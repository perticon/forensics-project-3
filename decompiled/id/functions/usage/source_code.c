usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("Usage: %s [OPTION]... [USER]...\n"), program_name);
      fputs (_("\
Print user and group information for each specified USER,\n\
or (when USER omitted) for the current process.\n\
\n"),
             stdout);
      fputs (_("\
  -a             ignore, for compatibility with other versions\n\
  -Z, --context  print only the security context of the process\n\
  -g, --group    print only the effective group ID\n\
  -G, --groups   print all group IDs\n\
  -n, --name     print a name instead of a number, for -ugG\n\
  -r, --real     print the real ID instead of the effective ID, with -ugG\n\
  -u, --user     print only the effective user ID\n\
  -z, --zero     delimit entries with NUL characters, not whitespace;\n\
                   not permitted in default format\n\
"), stdout);
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
      fputs (_("\
\n\
Without any OPTION, print some useful set of identified information.\n\
"), stdout);
      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}