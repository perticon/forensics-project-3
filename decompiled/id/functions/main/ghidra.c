byte main(ulong param_1,undefined8 *param_2)

{
  char *pcVar1;
  byte bVar2;
  byte bVar3;
  int iVar4;
  undefined8 uVar5;
  int *piVar6;
  ulong uVar7;
  long lVar8;
  passwd *ppVar9;
  passwd *extraout_RDX;
  char *pcVar10;
  char **unaff_R15;
  long in_FS_OFFSET;
  undefined auVar11 [16];
  undefined8 uVar12;
  undefined8 uStack88;
  passwd *local_50;
  char *local_48;
  long local_40;
  
  pcVar10 = "agnruzGZ";
  param_1 = param_1 & 0xffffffff;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  do {
    uVar12 = 0x102853;
    iVar4 = getopt_long(param_1,param_2,"agnruzGZ",longopts,0);
    if (iVar4 == -1) {
LAB_00102937:
      bVar3 = just_group;
      bVar2 = just_user;
      uVar7 = (ulong)optind;
      param_1 = (ulong)((int)param_1 - optind);
      if (1 < (uint)just_user + (uint)just_group + (uint)just_group_list) {
        uVar12 = dcgettext(0,"cannot print \"only\" of more than one choice",5);
        error(1,0,uVar12);
LAB_00102d0d:
        uVar12 = dcgettext(0,"cannot get effective GID",5);
        error(1,*(undefined4 *)param_2,uVar12);
        goto LAB_00102d31;
      }
      if (((just_user | just_group) == 0) && (just_group_list == 0)) goto LAB_001029e8;
      if (param_1 != 0) goto LAB_00102a18;
      if (just_user == 0) {
        if (just_group != 0) goto LAB_00102b52;
        if (just_group_list == 0) goto LAB_00102c9d;
        goto LAB_00102b2b;
      }
      if (use_real == '\0') goto LAB_00102c54;
      param_2 = (undefined8 *)__errno_location();
      *(undefined4 *)param_2 = 0;
      ruid = getuid();
      if ((ruid != 0xffffffff) || (*(int *)param_2 == 0)) goto LAB_00102b93;
      break;
    }
    if (0x7a < iVar4) goto switchD_0010287d_caseD_48;
    if (iVar4 < 0x47) {
      if (iVar4 == -0x83) {
        version_etc(stdout,&DAT_001085a6,"GNU coreutils",Version,"Arnold Robbins","David MacKenzie",
                    0,uVar12);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar4 == -0x82) {
        usage(0);
        goto LAB_00102937;
      }
      goto switchD_0010287d_caseD_48;
    }
    switch(iVar4) {
    case 0x47:
      just_group_list = 1;
      break;
    default:
switchD_0010287d_caseD_48:
      usage(1);
switchD_0010287d_caseD_5a:
      uVar12 = dcgettext(0,"--context (-Z) works only on an SELinux-enabled kernel",5);
      error(1,0,uVar12);
      ppVar9 = extraout_RDX;
LAB_00102bf8:
      local_50 = ppVar9;
      local_48 = (char *)xstrdup(ppVar9->pw_name);
      ppVar9 = local_50;
LAB_00102af1:
      euid = ppVar9->pw_uid;
      egid = ppVar9->pw_gid;
      rgid = egid;
      ruid = euid;
      print_stuff(local_48);
      do {
        free(local_48);
        optind = optind + 1;
        uVar7 = (ulong)optind;
LAB_00102a89:
        if (param_1 <= uVar7) goto LAB_00102b9a;
        local_48 = (char *)0x0;
        pcVar1 = (char *)param_2[uVar7];
        if ((*pcVar1 != '\0') && (lVar8 = parse_user_spec(pcVar1,&euid,0,unaff_R15,0), lVar8 == 0))
        {
          if (local_48 == (char *)0x0) {
            ppVar9 = getpwuid(euid);
          }
          else {
            ppVar9 = getpwnam(local_48);
          }
          if (ppVar9 != (passwd *)0x0) goto code_r0x00102ae5;
        }
        uVar12 = quote(pcVar1);
        uVar5 = dcgettext(0,pcVar10,5);
        piVar6 = __errno_location();
        error(0,*piVar6,uVar5,uVar12);
        ok = 0;
      } while( true );
    case 0x5a:
      goto switchD_0010287d_caseD_5a;
    case 0x61:
      break;
    case 0x67:
      just_group = 1;
      break;
    case 0x6e:
      use_name = '\x01';
      break;
    case 0x72:
      use_real = '\x01';
      break;
    case 0x75:
      just_user = 1;
      break;
    case 0x7a:
      opt_zero = '\x01';
    }
  } while( true );
LAB_001029c4:
  uVar12 = dcgettext(0,"cannot get real UID",5);
  uVar7 = error(1,*(undefined4 *)param_2,uVar12);
LAB_001029e8:
  if ((use_real != '\0') || (use_name != '\0')) goto LAB_00102d55;
  if (opt_zero != '\0') goto LAB_00102d31;
  if (param_1 != 0) {
LAB_00102a18:
    unaff_R15 = &local_48;
    multiple_users = 1 < param_1;
    pcVar10 = "%s: no such user";
    param_1 = param_1 + (long)(int)uVar7;
    goto LAB_00102a89;
  }
  getenv("POSIXLY_CORRECT");
LAB_00102c9d:
  piVar6 = __errno_location();
  *piVar6 = 0;
  euid = geteuid();
  if (euid != 0xffffffff) goto LAB_00102b2b;
  if (*piVar6 != 0) {
LAB_00102d7e:
    uVar12 = dcgettext(0,"cannot get effective UID",5);
    auVar11 = error(1,*piVar6,uVar12);
    uVar12 = uStack88;
    uStack88 = SUB168(auVar11,0);
    (*(code *)PTR___libc_start_main_0010bfd0)
              (main,uVar12,&local_50,0,0,SUB168(auVar11 >> 0x40,0),&uStack88);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  if (bVar2 != 0) goto LAB_00102c7f;
  if (bVar3 == 0) goto LAB_00102b2b;
LAB_00102b52:
  while( true ) {
    param_2 = (undefined8 *)__errno_location();
    *(undefined4 *)param_2 = 0;
    egid = getegid();
    if ((egid == 0xffffffff) && (*(int *)param_2 != 0)) goto LAB_00102d0d;
    *(undefined4 *)param_2 = 0;
    rgid = getgid();
    if ((rgid != 0xffffffff) || (*(int *)param_2 == 0)) goto LAB_00102b93;
    uVar12 = dcgettext(0,"cannot get real GID",5);
    error(1,*(undefined4 *)param_2,uVar12);
LAB_00102c54:
    piVar6 = __errno_location();
    *piVar6 = 0;
    euid = geteuid();
    if (euid != 0xffffffff) goto LAB_00102b93;
    if (*piVar6 != 0) goto LAB_00102d7e;
LAB_00102c7f:
    if (use_real == '\0') goto LAB_00102b93;
LAB_00102b2b:
    param_2 = (undefined8 *)__errno_location();
    *(undefined4 *)param_2 = 0;
    ruid = getuid();
    if ((ruid == 0xffffffff) && (*(int *)param_2 != 0)) break;
    if (bVar2 != 0) goto LAB_00102b93;
  }
  goto LAB_001029c4;
LAB_00102b93:
  print_stuff(0);
LAB_00102b9a:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return ok ^ 1;
  }
  goto LAB_00102d79;
code_r0x00102ae5:
  if (local_48 == (char *)0x0) goto LAB_00102bf8;
  goto LAB_00102af1;
LAB_00102d31:
  uVar12 = dcgettext(0,"option --zero not permitted in default format",5);
  error(1,0,uVar12);
LAB_00102d55:
  uVar12 = dcgettext(0,"cannot print only names or real IDs in default format",5);
  error(1,0,uVar12);
LAB_00102d79:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}