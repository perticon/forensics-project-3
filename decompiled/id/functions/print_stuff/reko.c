Eq_19 print_stuff(Eq_19 rdi, Eq_19 r15, struct Eq_445 * fs)
{
	ptr64 fp;
	Eq_19 qwLoc38;
	word64 rax_21 = fs->qw0028;
	if (g_bC11A != 0x00)
	{
		Eq_19 rdi_642;
		uint64 rbp_1091 = (uint64) g_dwC110;
		if (g_bC11B == 0x00)
		{
			if (g_bC118 == 0x00)
				goto l0000000000002F9A;
		}
		else
		{
			rbp_1091 = (uint64) g_dwC114;
			if (g_bC118 == 0x00)
				goto l0000000000002F9A;
		}
		Eq_19 rax_585 = fn0000000000002490((word32) rbp_1091);
		if (rax_585 != 0x00)
		{
			rdi_642 = *rax_585;
l0000000000002F03:
			fn00000000000025B0(stdout, rdi_642);
			goto l0000000000002F0F;
		}
		word64 r8_1093;
		umaxtostr(&g_tC0F0, rbp_1091, out r8_1093);
		fn00000000000026D0(fn00000000000024B0(0x05, "cannot find name for user ID %s", null), 0x00, 0x00);
		g_bC010 = 0x00;
l0000000000002F9A:
		word64 r8_1092;
		rdi_642 = umaxtostr(&g_tC0F0, rbp_1091, out r8_1092);
		goto l0000000000002F03;
	}
	if (g_bC11C != 0x00)
	{
		word32 esi_548 = (word32) g_bC118;
		word32 edi_549 = g_dwC108;
		if (g_bC11B != 0x00)
			edi_549 = g_dwC10C;
		word64 r8_1094;
		g_bC010 &= print_group(esi_548, edi_549, out r8_1094);
		goto l0000000000002F0F;
	}
	word32 esi_36 = g_dwC114;
	if (g_bC11D != 0x00)
	{
		g_bC010 &= print_group_list(g_dwC108, g_dwC10C, esi_36, rdi, (word32) g_bC118, (g_bC11E ^ 0x01) << 0x05, fs);
		goto l0000000000002F0F;
	}
	word64 r8_1095;
	umaxtostr(&g_tC0F0, (uint64) esi_36, out r8_1095);
	fn00000000000026C0(fn00000000000024B0(0x05, "uid=%s", null), 0x01);
	Eq_19 rax_72 = fn0000000000002490(g_dwC114);
	Eq_19 r13_180 = rax_72;
	if (rax_72 != 0x00)
		fn00000000000026C0(34174, 0x01);
	Eq_19 r8_147;
	umaxtostr(&g_tC0D0, (uint64) g_dwC10C, out r8_147);
	fn00000000000026C0(fn00000000000024B0(0x05, " gid=%s", null), 0x01);
	if (fn0000000000002530(g_dwC10C) != null)
		fn00000000000026C0(34174, 0x01);
	uint64 rdi_140 = (uint64) g_dwC110;
	if ((word32) rdi_140 != g_dwC114)
	{
		umaxtostr(&g_tC0F0, rdi_140, out r8_147);
		fn00000000000026C0(fn00000000000024B0(0x05, " euid=%s", null), 0x01);
		Eq_19 rax_179 = fn0000000000002490(g_dwC110);
		r13_180 = rax_179;
		if (rax_179 != 0x00)
			fn00000000000026C0(34174, 0x01);
	}
	uint64 rdi_196 = (uint64) g_dwC108;
	if ((word32) rdi_196 != g_dwC10C)
	{
		umaxtostr(&g_tC0D0, rdi_196, out r8_147);
		fn00000000000026C0(fn00000000000024B0(0x05, " egid=%s", null), 0x01);
		if (fn0000000000002530(g_dwC108) != null)
			fn00000000000026C0(34174, 0x01);
	}
	struct Eq_1051 * rbx_289;
	int32 r12d_406;
	if (rdi != 0x00)
	{
		uint64 rsi_326 = 0xFFFFFFFF;
		if (r13_180 != 0x00)
			rsi_326 = (uint64) *((word64) r13_180 + 20);
		word64 r15_347;
		Eq_19 rbp_351;
		word64 r14_1102;
		word32 r13d_1101;
		word32 r12d_1100;
		int32 eax_361 = xgetgroups(fp - 0x38, rsi_326, rdi, r8_147, r15, fs, out rbx_289, out rbp_351, out r12d_1100, out r13d_1101, out r14_1102, out r15_347);
		r12d_406 = eax_361;
		if (eax_361 >= 0x00)
		{
l000000000000319A:
			if (r12d_406 != 0x00)
			{
				fn00000000000025B0(stdout, fn00000000000024B0(0x05, " groups=", null));
				uint64 r12_424 = (uint64) (r12d_406 - 0x01);
				Eq_1441 rbp_430 = 0x00;
				while (true)
				{
					word64 r8_1103;
					fn00000000000025B0(stdout, umaxtostr(rbx_289, CONVERT(Mem436[qwLoc38 + rbp_430:word32], word32, uint64), out r8_1103));
					if (fn0000000000002530(Mem436[qwLoc38 + rbp_430:word32]) != 0x00)
						fn00000000000026C0(34174, 0x01);
					if (rbp_430 == r12_424 << 0x02)
						break;
					FILE * rdi_481 = stdout;
					byte * rax_482 = rdi_481->ptr0028;
					if (rax_482 >= rdi_481->ptr0030)
						fn0000000000002540(44, rdi_481);
					else
					{
						rdi_481->ptr0028 = rax_482 + 1;
						*rax_482 = 44;
					}
					rbp_430 = (word64) rbp_430 + 4;
				}
			}
			fn0000000000002400(qwLoc38);
l0000000000002F0F:
			uint64 rsi_657;
			FILE * rdi_651 = stdout;
			byte * rax_653 = rdi_651->ptr0028;
			byte * rdx_654 = rdi_651->ptr0030;
			if (g_bC11E != 0x00)
			{
				if (g_bC11D != 0x00 && g_bC119 != 0x00)
				{
					if (rax_653 < rdx_654)
					{
						rdi_651->ptr0028 = rax_653 + 1;
						*rax_653 = 0x00;
					}
					else
						fn0000000000002540(0x00, rdi_651);
					rdi_651 = stdout;
					byte * rax_675 = rdi_651->ptr0028;
					if (rax_675 < rdi_651->ptr0030)
					{
						rdi_651->ptr0028 = rax_675 + 1;
						*rax_675 = 0x00;
l0000000000002F57:
						if (rax_21 - fs->qw0028 == 0x00)
							return;
l000000000000346C:
						fn00000000000024E0();
					}
					if (rax_21 - fs->qw0028 != 0x00)
						goto l000000000000346C;
					rsi_657 = 0x00;
					goto l00000000000031CC;
				}
				rsi_657 = 0x00;
			}
			else
				rsi_657 = 0x0A;
			byte sil_752 = (byte) rsi_657;
			if (rax_653 < rdx_654)
			{
				rdi_651->ptr0028 = rax_653 + 1;
				*rax_653 = sil_752;
				goto l0000000000002F57;
			}
			if (rax_21 - fs->qw0028 != 0x00)
				goto l000000000000346C;
l00000000000031CC:
			fn0000000000002540((word32) rsi_657, rdi_651);
			return;
		}
		quote(rbp_351, fs);
		fn00000000000026D0(fn00000000000024B0(0x05, "failed to get groups for user %s", null), fn0000000000002420()[0], 0x00);
	}
	else
	{
		word64 r15_284;
		word64 rbp_1096;
		word32 r12d_1097;
		word32 r13d_1098;
		word64 r14_1099;
		int32 eax_298 = xgetgroups(fp - 0x38, (uint64) g_dwC108, 0x00, r8_147, r15, fs, out rbx_289, out rbp_1096, out r12d_1097, out r13d_1098, out r14_1099, out r15_284);
		r12d_406 = eax_298;
		if (eax_298 >= 0x00)
			goto l000000000000319A;
		fn00000000000026D0(fn00000000000024B0(0x05, "failed to get groups for the current process", null), fn0000000000002420()[0], 0x00);
	}
	g_bC010 = 0x00;
	goto l0000000000002F0F;
}