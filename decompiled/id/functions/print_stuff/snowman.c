void print_stuff(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9, ...) {
    int64_t rax7;
    int64_t v8;
    int1_t zf9;
    int1_t zf10;
    int1_t zf11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rax15;
    void* rsp16;
    void** r13_17;
    void** rdx18;
    void** rax19;
    void** rax20;
    int64_t rdi21;
    void*** rax22;
    void** rsp23;
    void** rdx24;
    int32_t edi25;
    int1_t zf26;
    void** rax27;
    void** rax28;
    int64_t rdi29;
    void** rax30;
    void** rdx31;
    uint32_t edi32;
    int1_t zf33;
    void** rax34;
    void** rax35;
    int64_t rdi36;
    void*** rax37;
    void** rdx38;
    unsigned char al39;
    int1_t zf40;
    void** rsi41;
    int64_t rdi42;
    unsigned char al43;
    int1_t zf44;
    int64_t rbp45;
    int1_t zf46;
    int64_t rdi47;
    void** rax48;
    void** rax49;
    void** rax50;
    void** rdi51;
    void** rax52;
    void** rsi53;
    int1_t zf54;
    void** rsi55;
    int32_t eax56;
    int32_t r12d57;
    void** rbp58;
    void** rdx59;
    void** rax60;
    int64_t r12_61;
    int64_t r12_62;
    int64_t rbp63;
    void** r14_64;
    void** rax65;
    int64_t rdi66;
    void* v67;
    void*** rax68;
    void** rdi69;
    void** rax70;
    void** v71;
    void** rdi72;
    int1_t zf73;
    void** rax74;
    int32_t esi75;
    int1_t zf76;
    int1_t zf77;
    void** rdi78;
    void** rax79;
    int64_t rax80;
    int64_t rax81;
    int64_t rax82;
    int32_t eax83;

    rax7 = g28;
    v8 = rax7;
    zf9 = just_user == 0;
    if (zf9) {
        zf10 = just_group == 0;
        if (zf10) {
            zf11 = just_group_list == 0;
            if (zf11) {
                rax12 = umaxtostr();
                rax13 = fun_24b0();
                fun_26c0(1, rax13, rax12, rcx);
                *reinterpret_cast<int32_t*>(&rdi14) = ruid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                rax15 = fun_2490(rdi14, rax13, rdi14, rax13);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                r13_17 = rax15;
                if (rax15) {
                    rdx18 = *reinterpret_cast<void***>(rax15);
                    fun_26c0(1, "(%s)", rdx18, rcx);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                }
                rax19 = umaxtostr();
                rax20 = fun_24b0();
                fun_26c0(1, rax20, rax19, rcx);
                *reinterpret_cast<uint32_t*>(&rdi21) = rgid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
                rax22 = fun_2530(rdi21, rax20, rdi21, rax20);
                rsp23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (rax22) {
                    rdx24 = *rax22;
                    fun_26c0(1, "(%s)", rdx24, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                edi25 = euid;
                zf26 = edi25 == ruid;
                if (!zf26 && (rax27 = umaxtostr(), rax28 = fun_24b0(), fun_26c0(1, rax28, rax27, rcx), *reinterpret_cast<int32_t*>(&rdi29) = euid, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0, rax30 = fun_2490(rdi29, rax28, rdi29, rax28), rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8), r13_17 = rax30, !!rax30)) {
                    rdx31 = *reinterpret_cast<void***>(rax30);
                    fun_26c0(1, "(%s)", rdx31, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                edi32 = egid;
                zf33 = edi32 == rgid;
                if (!zf33 && (rax34 = umaxtostr(), rax35 = fun_24b0(), fun_26c0(1, rax35, rax34, rcx), *reinterpret_cast<uint32_t*>(&rdi36) = egid, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi36) + 4) = 0, rax37 = fun_2530(rdi36, rax35, rdi36, rax35), rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8 - 8 + 8 - 8 + 8 - 8 + 8), !!rax37)) {
                    rdx38 = *rax37;
                    fun_26c0(1, "(%s)", rdx38, rcx);
                    rsp23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp23 - 8) + 8);
                }
                if (rdi) 
                    goto addr_3176_13;
            } else {
                al39 = print_group_list();
                ok = reinterpret_cast<unsigned char>(ok & al39);
                goto addr_2f0f_15;
            }
        } else {
            zf40 = use_real == 0;
            *reinterpret_cast<uint32_t*>(&rsi41) = use_name;
            *reinterpret_cast<int32_t*>(&rsi41 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi42) = egid;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
            if (!zf40) {
                *reinterpret_cast<uint32_t*>(&rdi42) = rgid;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
            }
            al43 = print_group(rdi42, rsi41);
            ok = reinterpret_cast<unsigned char>(ok & al43);
            goto addr_2f0f_15;
        }
    } else {
        zf44 = use_real == 0;
        *reinterpret_cast<int32_t*>(&rbp45) = euid;
        if (!zf44) {
            zf46 = use_name == 0;
            *reinterpret_cast<int32_t*>(&rbp45) = ruid;
            if (!zf46) {
                addr_2ef0_21:
                *reinterpret_cast<int32_t*>(&rdi47) = *reinterpret_cast<int32_t*>(&rbp45);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi47) + 4) = 0;
                rax48 = fun_2490(rdi47, rsi);
                if (!rax48) {
                    rax49 = umaxtostr();
                    rax50 = fun_24b0();
                    rcx = rax49;
                    rdx = rax50;
                    fun_26d0();
                    ok = 0;
                    goto addr_2f9a_23;
                } else {
                    rdi51 = *reinterpret_cast<void***>(rax48);
                }
            } else {
                addr_2f9a_23:
                rax52 = umaxtostr();
                rdi51 = rax52;
            }
            rsi53 = stdout;
            fun_25b0(rdi51, rsi53, rdx, rcx);
            goto addr_2f0f_15;
        } else {
            zf54 = use_name == 0;
            if (zf54) 
                goto addr_2f9a_23; else 
                goto addr_2ef0_21;
        }
    }
    *reinterpret_cast<uint32_t*>(&rsi55) = egid;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    eax56 = xgetgroups();
    r12d57 = eax56;
    if (eax56 >= 0) {
        addr_319a_28:
        if (r12d57) {
            rbp58 = stdout;
            *reinterpret_cast<int32_t*>(&rdx59) = 5;
            *reinterpret_cast<int32_t*>(&rdx59 + 4) = 0;
            rax60 = fun_24b0();
            *reinterpret_cast<int32_t*>(&r12_61) = r12d57 - 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_61) + 4) = 0;
            r12_62 = r12_61 << 2;
            *reinterpret_cast<int32_t*>(&rbp63) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp63) + 4) = 0;
            fun_25b0(rax60, rbp58, 5, rcx);
            while (1) {
                r14_64 = stdout;
                rax65 = umaxtostr();
                rsi55 = r14_64;
                fun_25b0(rax65, rsi55, rdx59, rcx);
                *reinterpret_cast<int32_t*>(&rdi66) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(v67) + rbp63);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi66) + 4) = 0;
                rax68 = fun_2530(rdi66, rsi55, rdi66, rsi55);
                if (rax68) {
                    rdx59 = *rax68;
                    rsi55 = reinterpret_cast<void**>("(%s)");
                    fun_26c0(1, "(%s)", rdx59, rcx);
                }
                if (rbp63 == r12_62) 
                    break;
                rdi69 = stdout;
                rax70 = *reinterpret_cast<void***>(rdi69 + 40);
                if (reinterpret_cast<unsigned char>(rax70) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi69 + 48))) {
                    rdx59 = rax70 + 1;
                    *reinterpret_cast<void***>(rdi69 + 40) = rdx59;
                    *reinterpret_cast<void***>(rax70) = reinterpret_cast<void**>(44);
                } else {
                    fun_2540();
                }
                rbp63 = rbp63 + 4;
            }
        }
    } else {
        fun_24b0();
        fun_2420();
        fun_26d0();
        goto addr_32ec_38;
    }
    fun_2400(v71, rsi55, v71, rsi55);
    addr_2f0f_15:
    rdi72 = stdout;
    zf73 = opt_zero == 0;
    rax74 = *reinterpret_cast<void***>(rdi72 + 40);
    if (zf73) {
        esi75 = 10;
    } else {
        zf76 = just_group_list == 0;
        if (zf76 || (zf77 = multiple_users == 0, zf77)) {
            esi75 = 0;
        } else {
            if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
                fun_2540();
            } else {
                *reinterpret_cast<void***>(rdi72 + 40) = rax74 + 1;
                *reinterpret_cast<void***>(rax74) = reinterpret_cast<void**>(0);
            }
            rdi78 = stdout;
            rax79 = *reinterpret_cast<void***>(rdi78 + 40);
            if (reinterpret_cast<unsigned char>(rax79) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi78 + 48))) 
                goto addr_3400_47; else 
                goto addr_3071_48;
        }
    }
    if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
        rax80 = v8 - g28;
        if (rax80) {
            addr_346c_51:
            fun_24e0();
        } else {
            addr_31cc_52:
            goto addr_2540_53;
        }
    } else {
        *reinterpret_cast<void***>(rdi72 + 40) = rax74 + 1;
        *reinterpret_cast<void***>(rax74) = *reinterpret_cast<void***>(&esi75);
        goto addr_2f57_55;
    }
    addr_2540_53:
    addr_2f57_55:
    rax81 = v8 - g28;
    if (!rax81) {
        return;
    }
    addr_3400_47:
    rax82 = v8 - g28;
    if (rax82) 
        goto addr_346c_51;
    goto addr_31cc_52;
    addr_3071_48:
    *reinterpret_cast<void***>(rdi78 + 40) = rax79 + 1;
    *reinterpret_cast<void***>(rax79) = reinterpret_cast<void**>(0);
    goto addr_2f57_55;
    addr_32ec_38:
    ok = 0;
    goto addr_2f0f_15;
    addr_3176_13:
    *reinterpret_cast<uint32_t*>(&rsi55) = 0xffffffff;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    if (r13_17) {
        *reinterpret_cast<uint32_t*>(&rsi55) = *reinterpret_cast<uint32_t*>(r13_17 + 20);
        *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    }
    eax83 = xgetgroups();
    r12d57 = eax83;
    if (eax83 >= 0) 
        goto addr_319a_28;
    quote(rdi, rsi55, rsp23, rcx, r8, r9);
    fun_24b0();
    fun_2420();
    fun_26d0();
    goto addr_32ec_38;
}