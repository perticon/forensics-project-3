void print_stuff(char * pw_name) {
    int64_t v1 = __readfsqword(40); // 0x2eac
    int32_t v2; // 0x2ea0
    int64_t v3; // 0x2ea0
    int64_t v4; // bp-56, 0x2ea0
    if (*(char *)&just_user == 0) {
        // 0x2fc0
        if (*(char *)&just_group == 0) {
            // 0x2ff8
            if (*(char *)&just_group_list == 0) {
                // 0x3088
                umaxtostr((int64_t)ruid, (char *)&g30);
                function_24b0();
                function_26c0();
                int64_t v5 = function_2490(); // 0x30c7
                if (v5 != 0) {
                    // 0x30d4
                    function_26c0();
                }
                // 0x30ea
                umaxtostr((int64_t)rgid, (char *)&g29);
                function_24b0();
                function_26c0();
                if (function_2530() != 0) {
                    // 0x3137
                    function_26c0();
                }
                int64_t v6 = v5; // 0x3159
                if (euid != ruid) {
                    // 0x3240
                    umaxtostr((int64_t)euid, (char *)&g30);
                    function_24b0();
                    function_26c0();
                    int64_t v7 = function_2490(); // 0x3276
                    v6 = 0;
                    if (v7 != 0) {
                        // 0x3287
                        function_26c0();
                        v6 = v7;
                    }
                }
                // 0x315f
                if (egid != rgid) {
                    // 0x31e0
                    umaxtostr((int64_t)egid, (char *)&g29);
                    function_24b0();
                    function_26c0();
                    if (function_2530() != 0) {
                        // 0x3224
                        function_26c0();
                    }
                }
                if (pw_name == NULL) {
                    int32_t v8 = xgetgroups(NULL, egid, (int32_t **)&v4); // 0x32b3
                    v2 = v8;
                    if (v8 >= 0) {
                        goto lab_0x319a;
                    } else {
                        // 0x32c3
                        function_24b0();
                        function_2420();
                        function_26d0();
                        // 0x32ec
                        *(char *)&ok = 0;
                        goto lab_0x2f0f;
                    }
                } else {
                    int32_t v9 = -1; // 0x317e
                    if (v6 != 0) {
                        // 0x3180
                        v9 = *(int32_t *)(v6 + 20);
                    }
                    int32_t v10 = xgetgroups(pw_name, v9, (int32_t **)&v4); // 0x318a
                    v2 = v10;
                    if (v10 < 0) {
                        // 0x3430
                        quote(pw_name);
                        function_24b0();
                        function_2420();
                        function_26d0();
                        // 0x32ec
                        *(char *)&ok = 0;
                        goto lab_0x2f0f;
                    } else {
                        goto lab_0x319a;
                    }
                }
            } else {
                unsigned char v11 = *(char *)&use_name; // 0x3012
                bool v12 = print_group_list(pw_name, ruid, rgid, egid, v11 % 2 != 0, 32 * *(char *)&opt_zero ^ 32); // 0x3032
                *(char *)&ok = v12 ? *(char *)&ok : 0;
                goto lab_0x2f0f;
            }
        } else {
            char v13 = *(char *)&use_real; // 0x2fc9
            bool v14 = print_group(v13 == 0 ? egid : rgid, *(char *)&use_name % 2 != 0); // 0x2fe5
            *(char *)&ok = v14 ? *(char *)&ok : 0;
            goto lab_0x2f0f;
        }
    } else {
        char v15 = *(char *)&use_name;
        if (*(char *)&use_real != 0) {
            int64_t v16 = ruid; // 0x2f87
            v3 = v16;
            if (v15 != 0) {
                goto lab_0x2ef0;
            } else {
                // 0x2f9a
                umaxtostr(v16, (char *)&g30);
                // 0x2f03
                function_25b0();
                goto lab_0x2f0f;
            }
        } else {
            int64_t v17 = euid; // 0x2ed0
            v3 = v17;
            if (v15 == 0) {
                // 0x2f9a
                umaxtostr(v17, (char *)&g30);
                // 0x2f03
                function_25b0();
                goto lab_0x2f0f;
            } else {
                goto lab_0x2ef0;
            }
        }
    }
  lab_0x2f0f:;
    int64_t v18 = (int64_t)g23; // 0x2f0f
    int64_t * v19 = (int64_t *)(v18 + 40); // 0x2f1d
    uint64_t v20 = *v19; // 0x2f1d
    uint64_t v21 = *(int64_t *)(v18 + 48); // 0x2f21
    char v22 = 10; // 0x2f25
    if (*(char *)&opt_zero == 0) {
        goto lab_0x2f43;
    } else {
        // 0x2f2b
        v22 = 0;
        if (*(char *)&just_group_list == 0) {
            goto lab_0x2f43;
        } else {
            // 0x2f34
            v22 = 0;
            if (*(char *)&multiple_users != 0) {
                if (v20 >= v21) {
                    // 0x3420
                    function_2540();
                } else {
                    // 0x3051
                    *v19 = v20 + 1;
                    *(char *)v20 = 0;
                }
                int64_t v23 = (int64_t)g23; // 0x305c
                int64_t * v24 = (int64_t *)(v23 + 40); // 0x3063
                uint64_t v25 = *v24; // 0x3063
                if (v25 >= *(int64_t *)(v23 + 48)) {
                    // 0x3400
                    if (v1 != __readfsqword(40)) {
                        // 0x346c
                        function_24e0();
                        return;
                    }
                    // 0x31cc
                    function_2540();
                    return;
                }
                // 0x3071
                *v24 = v25 + 1;
                *(char *)v25 = 0;
                goto lab_0x2f57;
            } else {
                goto lab_0x2f43;
            }
        }
    }
  lab_0x2ef0:
    // 0x2ef0
    if (function_2490() == 0) {
        // 0x3300
        umaxtostr(v3, (char *)&g30);
        function_24b0();
        function_26d0();
        *(char *)&ok = 0;
        // 0x2f9a
        umaxtostr(v3, (char *)&g30);
        // 0x2f03
        function_25b0();
        goto lab_0x2f0f;
    } else {
        // 0x2f03
        function_25b0();
        goto lab_0x2f0f;
    }
  lab_0x2f43:
    if (v20 >= v21) {
        // 0x31b8
        if (v1 != __readfsqword(40)) {
            // 0x346c
            function_24e0();
            return;
        }
        // 0x31cc
        function_2540();
        return;
    }
    // 0x2f4c
    *v19 = v20 + 1;
    *(char *)v20 = v22;
    goto lab_0x2f57;
  lab_0x319a:
    // 0x319a
    if (v2 != 0) {
        // 0x3348
        function_24b0();
        uint32_t v26; // 0x2ea0
        int64_t v27 = 4 * (int64_t)v26 + 0x3fffffffc & 0x3fffffffc; // 0x3373
        function_25b0();
        int64_t v28 = 0; // 0x337e
        int64_t v29 = v28;
        int64_t v30 = v4; // 0x338f
        uint32_t v31 = *(int32_t *)(v30 + v29); // 0x339d
        umaxtostr((int64_t)v31, (char *)&g29);
        function_25b0();
        if (function_2530() != 0) {
            // 0x33c1
            function_26c0();
        }
        while (v29 != v27) {
            struct _IO_FILE * v32 = g23; // 0x33dc
            int64_t v33 = (int64_t)v32; // 0x33dc
            int64_t * v34 = (int64_t *)(v33 + 40); // 0x33e3
            uint64_t v35 = *v34; // 0x33e3
            uint64_t v36 = *(int64_t *)(v33 + 48); // 0x33e7
            if (v35 < v36) {
                // 0x3380
                *v34 = v35 + 1;
                *(char *)v35 = 44;
            } else {
                // 0x33ed
                function_2540();
            }
            // 0x338b
            v28 = v29 + 4;
            v29 = v28;
            v30 = v4;
            v31 = *(int32_t *)(v30 + v29);
            umaxtostr((int64_t)v31, (char *)&g29);
            function_25b0();
            if (function_2530() != 0) {
                // 0x33c1
                function_26c0();
            }
        }
    }
    // 0x31a3
    function_2400();
    goto lab_0x2f0f;
  lab_0x2f57:
    // 0x2f57
    if (v1 == __readfsqword(40)) {
        // 0x2f6b
        return;
    }
    // 0x346c
    function_24e0();
}