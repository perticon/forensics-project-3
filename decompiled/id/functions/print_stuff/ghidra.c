void print_stuff(long param_1)

{
  FILE *pFVar1;
  byte bVar2;
  int iVar3;
  passwd *ppVar4;
  char *pcVar5;
  group *pgVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  int *piVar9;
  __uid_t __uid;
  long lVar10;
  __gid_t _Var11;
  long in_FS_OFFSET;
  void *local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (just_user == '\0') {
    if (just_group == '\0') {
      if (just_group_list == '\0') {
        uVar7 = umaxtostr(ruid,buf_1);
        uVar8 = dcgettext(0,"uid=%s",5);
        __printf_chk(1,uVar8,uVar7);
        ppVar4 = getpwuid(ruid);
        if (ppVar4 != (passwd *)0x0) {
          __printf_chk(1,&DAT_0010857e,ppVar4->pw_name);
        }
        uVar7 = umaxtostr(rgid,buf_0);
        uVar8 = dcgettext(0," gid=%s",5);
        __printf_chk(1,uVar8,uVar7);
        pgVar6 = getgrgid(rgid);
        if (pgVar6 != (group *)0x0) {
          __printf_chk(1,&DAT_0010857e,pgVar6->gr_name);
        }
        if (euid != ruid) {
          uVar7 = umaxtostr(euid,buf_1);
          uVar8 = dcgettext(0," euid=%s",5);
          __printf_chk(1,uVar8,uVar7);
          ppVar4 = getpwuid(euid);
          if (ppVar4 != (passwd *)0x0) {
            __printf_chk(1,&DAT_0010857e,ppVar4->pw_name);
          }
        }
        if (egid != rgid) {
          uVar7 = umaxtostr(egid,buf_0);
          uVar8 = dcgettext(0," egid=%s",5);
          __printf_chk(1,uVar8,uVar7);
          pgVar6 = getgrgid(egid);
          if (pgVar6 != (group *)0x0) {
            __printf_chk(1,&DAT_0010857e,pgVar6->gr_name);
          }
        }
        if (param_1 == 0) {
          iVar3 = xgetgroups(0,egid,&local_38);
          if (iVar3 < 0) {
            uVar7 = dcgettext(0,"failed to get groups for the current process",5);
            piVar9 = __errno_location();
            error(0,*piVar9,uVar7);
            goto LAB_001032ec;
          }
        }
        else {
          _Var11 = 0xffffffff;
          if (ppVar4 != (passwd *)0x0) {
            _Var11 = ppVar4->pw_gid;
          }
          iVar3 = xgetgroups(param_1,_Var11,&local_38);
          if (iVar3 < 0) {
            uVar7 = quote(param_1);
            uVar8 = dcgettext(0,"failed to get groups for user %s",5);
            piVar9 = __errno_location();
            error(0,*piVar9,uVar8,uVar7);
LAB_001032ec:
            ok = 0;
            goto LAB_00102f0f;
          }
        }
        pFVar1 = stdout;
        if (iVar3 != 0) {
          pcVar5 = (char *)dcgettext(0," groups=",5);
          lVar10 = 0;
          fputs_unlocked(pcVar5,pFVar1);
          while( true ) {
            pFVar1 = stdout;
            pcVar5 = (char *)umaxtostr(*(undefined4 *)((long)local_38 + lVar10),buf_0);
            fputs_unlocked(pcVar5,pFVar1);
            pgVar6 = getgrgid(*(__gid_t *)((long)local_38 + lVar10));
            if (pgVar6 != (group *)0x0) {
              __printf_chk(1,&DAT_0010857e,pgVar6->gr_name);
            }
            if (lVar10 == (ulong)(iVar3 - 1) << 2) break;
            pcVar5 = stdout->_IO_write_ptr;
            if (pcVar5 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar5 + 1;
              *pcVar5 = ',';
            }
            else {
              __overflow(stdout,0x2c);
            }
            lVar10 = lVar10 + 4;
          }
        }
        free(local_38);
      }
      else {
        bVar2 = print_group_list(param_1,ruid,rgid,egid,use_name,(opt_zero ^ 1) << 5);
        ok = ok & bVar2;
      }
    }
    else {
      _Var11 = egid;
      if (use_real != '\0') {
        _Var11 = rgid;
      }
      bVar2 = print_group(_Var11);
      ok = ok & bVar2;
    }
  }
  else {
    if (use_real == '\0') {
      __uid = euid;
      if (use_name == '\0') goto LAB_00102f9a;
LAB_00102ef0:
      ppVar4 = getpwuid(__uid);
      if (ppVar4 == (passwd *)0x0) {
        uVar7 = umaxtostr(__uid,buf_1);
        uVar8 = dcgettext(0,"cannot find name for user ID %s",5);
        error(0,0,uVar8,uVar7);
        ok = 0;
        goto LAB_00102f9a;
      }
      pcVar5 = ppVar4->pw_name;
    }
    else {
      __uid = ruid;
      if (use_name != '\0') goto LAB_00102ef0;
LAB_00102f9a:
      pcVar5 = (char *)umaxtostr(__uid,buf_1);
    }
    fputs_unlocked(pcVar5,stdout);
  }
LAB_00102f0f:
  pcVar5 = stdout->_IO_write_ptr;
  if (opt_zero == 0) {
    iVar3 = 10;
LAB_00102f43:
    if (stdout->_IO_write_end <= pcVar5) {
      if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
LAB_001031cc:
        __overflow(stdout,iVar3);
        return;
      }
      goto LAB_0010346c;
    }
    stdout->_IO_write_ptr = pcVar5 + 1;
    *pcVar5 = (char)iVar3;
  }
  else {
    if ((just_group_list == '\0') || (multiple_users == '\0')) {
      iVar3 = 0;
      goto LAB_00102f43;
    }
    if (pcVar5 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar5 + 1;
      *pcVar5 = '\0';
    }
    else {
      __overflow(stdout,0);
    }
    pcVar5 = stdout->_IO_write_ptr;
    if (stdout->_IO_write_end <= pcVar5) {
      if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
        iVar3 = 0;
        goto LAB_001031cc;
      }
      goto LAB_0010346c;
    }
    stdout->_IO_write_ptr = pcVar5 + 1;
    *pcVar5 = '\0';
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_0010346c:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}