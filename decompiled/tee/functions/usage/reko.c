void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000026A0(fn0000000000002450(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002E4E;
	}
	fn0000000000002620(fn0000000000002450(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002530(stdout, fn0000000000002450(0x05, "Copy standard input to each FILE, and also to standard output.\n\n  -a, --append              append to the given FILEs, do not overwrite\n  -i, --ignore-interrupts   ignore interrupt signals\n", null));
	fn0000000000002530(stdout, fn0000000000002450(0x05, "  -p                        diagnose errors writing to non pipes\n      --output-error[=MODE]   set behavior on write error.  See MODE below\n", null));
	fn0000000000002530(stdout, fn0000000000002450(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002450(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002450(0x05, "\nMODE determines behavior with write errors on the outputs:\n  warn           diagnose errors writing to any output\n  warn-nopipe    diagnose errors writing to any output not a pipe\n  exit           exit on error writing to any output\n  exit-nopipe    exit on error writing to any output not a pipe\nThe default MODE for the -p option is 'warn-nopipe'.\nThe default operation when --output-error is not specified, is to\nexit immediately on error writing to a pipe, and diagnose errors\nwriting to non pipe outputs.\n", null));
	struct Eq_1031 * rbx_165 = fp - 0xB8 + 16;
	do
	{
		Eq_25 rsi_167 = rbx_165->qw0000;
		++rbx_165;
	} while (rsi_167 != 0x00 && fn0000000000002550(rsi_167, 0x7023) != 0x00);
	ptr64 r13_180 = rbx_165->qw0008;
	if (r13_180 != 0x00)
	{
		fn0000000000002620(fn0000000000002450(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_25 rax_267 = fn0000000000002610(null, 0x05);
		if (rax_267 == 0x00 || fn00000000000023C0(0x03, 0x70AB, rax_267) == 0x00)
			goto l000000000000304E;
	}
	else
	{
		fn0000000000002620(fn0000000000002450(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_25 rax_209 = fn0000000000002610(null, 0x05);
		if (rax_209 == 0x00 || fn00000000000023C0(0x03, 0x70AB, rax_209) == 0x00)
		{
			fn0000000000002620(fn0000000000002450(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000308B:
			fn0000000000002620(fn0000000000002450(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002E4E:
			fn0000000000002680(edi);
		}
		r13_180 = 0x7023;
	}
	fn0000000000002530(stdout, fn0000000000002450(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000304E:
	fn0000000000002620(fn0000000000002450(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000308B;
}