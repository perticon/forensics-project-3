uint32_t tee_files(int32_t edi, int64_t* rsi) {
    int64_t r12_3;
    int64_t* v4;
    int64_t rbx5;
    int64_t rdi6;
    struct s0* rax7;
    struct s0* v8;
    int1_t zf9;
    struct s1* rax10;
    struct s1* rbp11;
    struct s0* rax12;
    struct s0* rax13;
    struct s0* rdi14;
    struct s0* rcx15;
    struct s0* rdx16;
    void* rsp17;
    int64_t v18;
    unsigned char v19;
    int64_t r14_20;
    int64_t rdi21;
    struct s0* rax22;
    void* rsp23;
    struct s0* rsi24;
    struct s0* rax25;
    int32_t* rax26;
    int32_t* r9_27;
    struct s0* r14_28;
    struct s0* rax29;
    struct s0* r12_30;
    int64_t r15_31;
    struct s0* r13_32;
    int64_t rax33;
    int32_t* rax34;
    int32_t r10d35;
    struct s0* rax36;
    uint32_t edx37;
    struct s0* rax38;
    int32_t* rax39;
    int64_t r8_40;
    void* rax41;
    int64_t r12_42;
    struct s0* rdi43;
    int64_t rax44;
    struct s0* rax45;
    int32_t* rax46;
    struct s0* rax47;
    int32_t* rax48;

    r12_3 = reinterpret_cast<int64_t>("a");
    v4 = rsi;
    *reinterpret_cast<int32_t*>(&rbx5) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    rdi6 = stdin;
    rax7 = g28;
    v8 = rax7;
    zf9 = append == 0;
    if (zf9) {
        r12_3 = reinterpret_cast<int64_t>("w");
    }
    fadvise(rdi6, 2);
    rax10 = xnmalloc(static_cast<int64_t>(static_cast<int32_t>(rbx5 + 1)), 8);
    rbp11 = rax10;
    rax12 = stdout;
    rbp11->f0 = rax12;
    rax13 = fun_2450();
    rdi14 = stdout;
    *reinterpret_cast<int32_t*>(&rcx15) = 0;
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    *reinterpret_cast<struct s0**>(rsi - 1) = rax13;
    *reinterpret_cast<uint32_t*>(&rdx16) = 2;
    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    fun_2630(rdi14);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("Mw") - reinterpret_cast<int64_t>("Mw") - 56 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) == 0)) {
        v18 = 1;
        v19 = 1;
    } else {
        v19 = 1;
        *reinterpret_cast<int32_t*>(&r14_20) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_20) + 4) = 0;
        v18 = 1;
        while (1) {
            rdi21 = *(v4 + r14_20 - 1);
            rax22 = fopen_safer(rdi21, r12_3, rdx16, rcx15);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r14_20 * 8) = rax22;
            if (rax22) {
                *reinterpret_cast<int32_t*>(&rcx15) = 0;
                *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx16) = 2;
                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi24) = 0;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                ++r14_20;
                fun_2630(rax22);
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                ++v18;
                if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r14_20)) 
                    break;
            } else {
                rax25 = quotearg_n_style_colon();
                rax26 = fun_23b0();
                rcx15 = rax25;
                rdx16 = reinterpret_cast<struct s0*>("%s");
                r9_27 = rax26;
                *reinterpret_cast<int32_t*>(&rsi24) = *r9_27;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                ++r14_20;
                fun_2640();
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8);
                v19 = 0;
                if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r14_20)) 
                    break;
            }
        }
        if (!v18) 
            goto addr_2bf3_10;
    }
    r14_28 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp17) + 32);
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdx16) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
        rsi24 = r14_28;
        rax29 = fun_2510();
        r12_30 = rax29;
        if (reinterpret_cast<signed char>(rax29) >= reinterpret_cast<signed char>(0)) {
            if (!rax29) 
                break;
            if (*reinterpret_cast<int32_t*>(&rbx5) < 0) 
                continue;
            *reinterpret_cast<int32_t*>(&r15_31) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_31) + 4) = 0;
            do {
                r13_32 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r15_31 * 8);
                if (r13_32 && (rcx15 = r13_32, *reinterpret_cast<uint32_t*>(&rdx16) = 1, *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0, rsi24 = r12_30, rax33 = fun_25e0(r14_28, rsi24, 1, rcx15), rax33 != 1)) {
                    rax34 = fun_23b0();
                    r10d35 = *rax34;
                    rax36 = stdout;
                    if (r10d35 != 32 || (edx37 = output_error, *reinterpret_cast<uint32_t*>(&rdx16) = edx37 & 0xfffffffd, *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx16) == 1)) {
                        if (r13_32 == rax36) {
                            fun_2410(r13_32, rsi24, r13_32, rsi24);
                            r10d35 = r10d35;
                        }
                        rax38 = quotearg_n_style_colon();
                        *reinterpret_cast<int32_t*>(&rsi24) = r10d35;
                        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                        rdx16 = reinterpret_cast<struct s0*>("%s");
                        rcx15 = rax38;
                        fun_2640();
                        v19 = 0;
                    } else {
                        if (r13_32 == rax36) {
                            fun_2410(r13_32, rsi24);
                        }
                    }
                    --v18;
                    *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r15_31 * 8) = reinterpret_cast<struct s0*>(0);
                }
                ++r15_31;
            } while (*reinterpret_cast<int32_t*>(&rbx5) >= *reinterpret_cast<int32_t*>(&r15_31));
            if (!v18) 
                goto addr_2d9c_26;
        } else {
            rax39 = fun_23b0();
            if (*rax39 != 4) 
                goto addr_2be5_28;
        }
    }
    addr_2bef_29:
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) == 0)) {
        addr_2c68_30:
        fun_2390(rbp11, rsi24, rdx16, rcx15, r8_40, r9_27);
        rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        if (rax41) {
            fun_2480();
        } else {
            return static_cast<uint32_t>(v19);
        }
    } else {
        addr_2bf3_10:
        *reinterpret_cast<int32_t*>(&r12_42) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_42) + 4) = 0;
    }
    while (1) {
        rdi43 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r12_42 * 8);
        if (!rdi43 || (rax44 = rpl_fclose(rdi43, rsi24, rdx16, rcx15), *reinterpret_cast<int32_t*>(&rax44) == 0)) {
            ++r12_42;
            if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r12_42)) 
                goto addr_2c68_30;
        } else {
            ++r12_42;
            rax45 = quotearg_n_style_colon();
            rax46 = fun_23b0();
            rcx15 = rax45;
            rdx16 = reinterpret_cast<struct s0*>("%s");
            *reinterpret_cast<int32_t*>(&rsi24) = *rax46;
            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
            fun_2640();
            v19 = 0;
            if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r12_42)) 
                break;
        }
    }
    goto addr_2c68_30;
    addr_2d9c_26:
    addr_2be5_28:
    if (r12_30 == 0xffffffffffffffff) {
        rax47 = fun_2450();
        rax48 = fun_23b0();
        rdx16 = rax47;
        *reinterpret_cast<int32_t*>(&rsi24) = *rax48;
        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        fun_2640();
        v19 = 0;
        goto addr_2bef_29;
    }
}