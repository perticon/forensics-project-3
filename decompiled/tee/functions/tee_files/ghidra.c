undefined tee_files(int param_1,long param_2)

{
  long lVar1;
  undefined uVar2;
  int iVar3;
  FILE **__ptr;
  undefined8 uVar4;
  FILE *pFVar5;
  int *piVar6;
  size_t __size;
  size_t sVar7;
  long lVar8;
  long in_FS_OFFSET;
  long local_2060;
  undefined local_2054;
  undefined local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  fadvise(stdin,2);
  __ptr = (FILE **)xnmalloc((long)(param_1 + 1),8);
  *__ptr = stdout;
  uVar4 = dcgettext(0,"standard output",5);
  pFVar5 = stdout;
  *(undefined8 *)(param_2 + -8) = uVar4;
  setvbuf(pFVar5,(char *)0x0,2,0);
  if (param_1 < 1) {
    local_2060 = 1;
    local_2054 = 1;
LAB_00102bb3:
    do {
      do {
        while( true ) {
          __size = read(0,local_2048,0x2000);
          if (-1 < (long)__size) break;
          piVar6 = __errno_location();
          if (*piVar6 != 4) goto LAB_00102be5;
        }
        if (__size == 0) goto LAB_00102bef;
      } while (param_1 < 0);
      lVar8 = 0;
      do {
        pFVar5 = __ptr[lVar8];
        if ((pFVar5 != (FILE *)0x0) &&
           (sVar7 = fwrite_unlocked(local_2048,__size,1,pFVar5), sVar7 != 1)) {
          piVar6 = __errno_location();
          iVar3 = *piVar6;
          if ((iVar3 == 0x20) && ((output_error & 0xfffffffd) != 1)) {
            if (pFVar5 == stdout) {
              clearerr_unlocked(pFVar5);
            }
          }
          else {
            if (pFVar5 == stdout) {
              clearerr_unlocked(pFVar5);
            }
            uVar4 = quotearg_n_style_colon(0,3,*(undefined8 *)(param_2 + -8 + lVar8 * 8));
            error(output_error - 3 < 2,iVar3,&DAT_001076b4,uVar4);
            local_2054 = 0;
          }
          local_2060 = local_2060 + -1;
          __ptr[lVar8] = (FILE *)0x0;
        }
        lVar8 = lVar8 + 1;
      } while ((int)lVar8 <= param_1);
    } while (local_2060 != 0);
LAB_00102be5:
    if (__size == 0xffffffffffffffff) {
      uVar4 = dcgettext(0,"read error",5);
      piVar6 = __errno_location();
      error(0,*piVar6,uVar4);
      local_2054 = 0;
    }
LAB_00102bef:
    if (param_1 < 1) goto LAB_00102c68;
  }
  else {
    local_2054 = 1;
    lVar8 = 1;
    local_2060 = 1;
    do {
      while( true ) {
        pFVar5 = (FILE *)fopen_safer(*(undefined8 *)(param_2 + -8 + lVar8 * 8));
        __ptr[lVar8] = pFVar5;
        if (pFVar5 != (FILE *)0x0) break;
        quotearg_n_style_colon(0,3,*(undefined8 *)(param_2 + -8 + lVar8 * 8));
        piVar6 = __errno_location();
        lVar8 = lVar8 + 1;
        error(output_error - 3 < 2,*piVar6,&DAT_001076b4);
        local_2054 = 0;
        if (param_1 < (int)lVar8) goto LAB_00102bab;
      }
      lVar8 = lVar8 + 1;
      setvbuf(pFVar5,(char *)0x0,2,0);
      local_2060 = local_2060 + 1;
    } while ((int)lVar8 <= param_1);
LAB_00102bab:
    if (local_2060 != 0) goto LAB_00102bb3;
  }
  lVar8 = 1;
  uVar2 = local_2054;
  do {
    while ((local_2054 = uVar2, __ptr[lVar8] != (FILE *)0x0 && (iVar3 = rpl_fclose(), iVar3 != 0)))
    {
      lVar1 = lVar8 * 8;
      lVar8 = lVar8 + 1;
      uVar4 = quotearg_n_style_colon(0,3,*(undefined8 *)(param_2 + -8 + lVar1));
      piVar6 = __errno_location();
      error(0,*piVar6,&DAT_001076b4,uVar4);
      local_2054 = 0;
      uVar2 = local_2054;
      local_2054 = 0;
      if (param_1 < (int)lVar8) goto LAB_00102c68;
    }
    lVar8 = lVar8 + 1;
    uVar2 = local_2054;
  } while ((int)lVar8 <= param_1);
LAB_00102c68:
  free(__ptr);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_2054;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}