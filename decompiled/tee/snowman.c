
int64_t stdin = 0;

struct s0 {
    unsigned char f0;
    signed char f1;
    signed char f2;
    signed char[5] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    signed char* f20;
    signed char* f28;
    signed char* f30;
    signed char[16] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

struct s0* g28;

signed char append = 0;

void fadvise(int64_t rdi, int64_t rsi);

struct s1 {
    struct s0* f0;
    signed char[7] pad8;
    struct s1* f8;
    signed char[8] pad24;
    struct s1* f18;
};

struct s1* xnmalloc(int64_t rdi, int64_t rsi);

struct s0* stdout = reinterpret_cast<struct s0*>(0);

struct s0* fun_2450();

void fun_2630(struct s0* rdi);

struct s0* fopen_safer(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

struct s0* quotearg_n_style_colon();

int32_t* fun_23b0();

void fun_2640();

struct s0* fun_2510();

int64_t fun_25e0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

uint32_t output_error = 0;

void fun_2410(struct s0* rdi, struct s0* rsi, ...);

void fun_2390(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int32_t* r9);

void fun_2480();

int64_t rpl_fclose(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

uint32_t tee_files(int32_t edi, int64_t* rsi) {
    int64_t r12_3;
    int64_t* v4;
    int64_t rbx5;
    int64_t rdi6;
    struct s0* rax7;
    struct s0* v8;
    int1_t zf9;
    struct s1* rax10;
    struct s1* rbp11;
    struct s0* rax12;
    struct s0* rax13;
    struct s0* rdi14;
    struct s0* rcx15;
    struct s0* rdx16;
    void* rsp17;
    int64_t v18;
    unsigned char v19;
    int64_t r14_20;
    int64_t rdi21;
    struct s0* rax22;
    void* rsp23;
    struct s0* rsi24;
    struct s0* rax25;
    int32_t* rax26;
    int32_t* r9_27;
    struct s0* r14_28;
    struct s0* rax29;
    struct s0* r12_30;
    int64_t r15_31;
    struct s0* r13_32;
    int64_t rax33;
    int32_t* rax34;
    int32_t r10d35;
    struct s0* rax36;
    uint32_t edx37;
    struct s0* rax38;
    int32_t* rax39;
    int64_t r8_40;
    void* rax41;
    int64_t r12_42;
    struct s0* rdi43;
    int64_t rax44;
    struct s0* rax45;
    int32_t* rax46;
    struct s0* rax47;
    int32_t* rax48;

    r12_3 = reinterpret_cast<int64_t>("a");
    v4 = rsi;
    *reinterpret_cast<int32_t*>(&rbx5) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    rdi6 = stdin;
    rax7 = g28;
    v8 = rax7;
    zf9 = append == 0;
    if (zf9) {
        r12_3 = reinterpret_cast<int64_t>("w");
    }
    fadvise(rdi6, 2);
    rax10 = xnmalloc(static_cast<int64_t>(static_cast<int32_t>(rbx5 + 1)), 8);
    rbp11 = rax10;
    rax12 = stdout;
    rbp11->f0 = rax12;
    rax13 = fun_2450();
    rdi14 = stdout;
    *reinterpret_cast<int32_t*>(&rcx15) = 0;
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    *reinterpret_cast<struct s0**>(rsi - 1) = rax13;
    *reinterpret_cast<uint32_t*>(&rdx16) = 2;
    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    fun_2630(rdi14);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("Mw") - reinterpret_cast<int64_t>("Mw") - 56 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) == 0)) {
        v18 = 1;
        v19 = 1;
    } else {
        v19 = 1;
        *reinterpret_cast<int32_t*>(&r14_20) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_20) + 4) = 0;
        v18 = 1;
        while (1) {
            rdi21 = *(v4 + r14_20 - 1);
            rax22 = fopen_safer(rdi21, r12_3, rdx16, rcx15);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r14_20 * 8) = rax22;
            if (rax22) {
                *reinterpret_cast<int32_t*>(&rcx15) = 0;
                *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx16) = 2;
                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi24) = 0;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                ++r14_20;
                fun_2630(rax22);
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                ++v18;
                if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r14_20)) 
                    break;
            } else {
                rax25 = quotearg_n_style_colon();
                rax26 = fun_23b0();
                rcx15 = rax25;
                rdx16 = reinterpret_cast<struct s0*>("%s");
                r9_27 = rax26;
                *reinterpret_cast<int32_t*>(&rsi24) = *r9_27;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                ++r14_20;
                fun_2640();
                rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8);
                v19 = 0;
                if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r14_20)) 
                    break;
            }
        }
        if (!v18) 
            goto addr_2bf3_10;
    }
    r14_28 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp17) + 32);
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdx16) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
        rsi24 = r14_28;
        rax29 = fun_2510();
        r12_30 = rax29;
        if (reinterpret_cast<signed char>(rax29) >= reinterpret_cast<signed char>(0)) {
            if (!rax29) 
                break;
            if (*reinterpret_cast<int32_t*>(&rbx5) < 0) 
                continue;
            *reinterpret_cast<int32_t*>(&r15_31) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_31) + 4) = 0;
            do {
                r13_32 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r15_31 * 8);
                if (r13_32 && (rcx15 = r13_32, *reinterpret_cast<uint32_t*>(&rdx16) = 1, *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0, rsi24 = r12_30, rax33 = fun_25e0(r14_28, rsi24, 1, rcx15), rax33 != 1)) {
                    rax34 = fun_23b0();
                    r10d35 = *rax34;
                    rax36 = stdout;
                    if (r10d35 != 32 || (edx37 = output_error, *reinterpret_cast<uint32_t*>(&rdx16) = edx37 & 0xfffffffd, *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx16) == 1)) {
                        if (r13_32 == rax36) {
                            fun_2410(r13_32, rsi24, r13_32, rsi24);
                            r10d35 = r10d35;
                        }
                        rax38 = quotearg_n_style_colon();
                        *reinterpret_cast<int32_t*>(&rsi24) = r10d35;
                        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                        rdx16 = reinterpret_cast<struct s0*>("%s");
                        rcx15 = rax38;
                        fun_2640();
                        v19 = 0;
                    } else {
                        if (r13_32 == rax36) {
                            fun_2410(r13_32, rsi24);
                        }
                    }
                    --v18;
                    *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r15_31 * 8) = reinterpret_cast<struct s0*>(0);
                }
                ++r15_31;
            } while (*reinterpret_cast<int32_t*>(&rbx5) >= *reinterpret_cast<int32_t*>(&r15_31));
            if (!v18) 
                goto addr_2d9c_26;
        } else {
            rax39 = fun_23b0();
            if (*rax39 != 4) 
                goto addr_2be5_28;
        }
    }
    addr_2bef_29:
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx5) == 0)) {
        addr_2c68_30:
        fun_2390(rbp11, rsi24, rdx16, rcx15, r8_40, r9_27);
        rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        if (rax41) {
            fun_2480();
        } else {
            return static_cast<uint32_t>(v19);
        }
    } else {
        addr_2bf3_10:
        *reinterpret_cast<int32_t*>(&r12_42) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_42) + 4) = 0;
    }
    while (1) {
        rdi43 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbp11) + r12_42 * 8);
        if (!rdi43 || (rax44 = rpl_fclose(rdi43, rsi24, rdx16, rcx15), *reinterpret_cast<int32_t*>(&rax44) == 0)) {
            ++r12_42;
            if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r12_42)) 
                goto addr_2c68_30;
        } else {
            ++r12_42;
            rax45 = quotearg_n_style_colon();
            rax46 = fun_23b0();
            rcx15 = rax45;
            rdx16 = reinterpret_cast<struct s0*>("%s");
            *reinterpret_cast<int32_t*>(&rsi24) = *rax46;
            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
            fun_2640();
            v19 = 0;
            if (*reinterpret_cast<int32_t*>(&rbx5) < *reinterpret_cast<int32_t*>(&r12_42)) 
                break;
        }
    }
    goto addr_2c68_30;
    addr_2d9c_26:
    addr_2be5_28:
    if (r12_30 == 0xffffffffffffffff) {
        rax47 = fun_2450();
        rax48 = fun_23b0();
        rdx16 = rax47;
        *reinterpret_cast<int32_t*>(&rsi24) = *rax48;
        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        fun_2640();
        v19 = 0;
        goto addr_2bef_29;
    }
}

int64_t fun_2460();

int64_t fun_23a0(struct s1* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2460();
    if (r8d > 10) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x7780 + rax11 * 4) + 0x7780;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* slotvec = reinterpret_cast<struct s1*>(0xb090);

uint32_t nslots = 1;

struct s1* xpalloc();

void fun_24e0();

struct s3 {
    struct s0* f0;
    signed char[7] pad8;
    struct s1* f8;
};

struct s1* xcharalloc(struct s0* rdi, ...);

struct s1* quotearg_n_options(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s1* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s1* rax12;
    struct s1* rax13;
    int64_t rax14;
    int64_t r8_15;
    struct s3* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s1* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    int32_t* r9_23;
    struct s0* rax24;
    struct s0* rsi25;
    struct s1* rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4edf;
    rax8 = fun_23b0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6011]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4f6b;
            fun_24e0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<int64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = reinterpret_cast<struct s0*>(&rax24->f1);
            rbx16->f0 = rsi25;
            if (r14_19 != 0xb120) {
                fun_2390(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x4ffa);
        }
        *rax8 = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2480();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x771b);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s0*>(0x7714);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x771f);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s0*>(0x7710);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2383() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2393() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_23a3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_23b3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_23f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20a0;

void fun_2403() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clearerr_unlocked = 0x20b0;

void fun_2413() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20c0;

void fun_2423() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2433() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2443() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2453() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2463() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2473() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2483() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2493() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_24a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2150;

void fun_24b3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2160;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2180;

void fun_24e3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2190;

void fun_24f3() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21a0;

void fun_2503() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x21b0;

void fun_2513() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x21c0;

void fun_2523() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_2533() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21e0;

void fun_2543() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21f0;

void fun_2553() {
    __asm__("cli ");
    goto strcmp;
}

int64_t signal = 0x2200;

void fun_2563() {
    __asm__("cli ");
    goto signal;
}

int64_t fputc_unlocked = 0x2210;

void fun_2573() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2583() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2593() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_25a3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25b3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25c3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25d3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2280;

void fun_25e3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2290;

void fun_25f3() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x22a0;

void fun_2603() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x22b0;

void fun_2613() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22c0;

void fun_2623() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x22d0;

void fun_2633() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t error = 0x22e0;

void fun_2643() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22f0;

void fun_2653() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2300;

void fun_2663() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x2310;

void fun_2673() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2320;

void fun_2683() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2330;

void fun_2693() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2340;

void fun_26a3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2350;

void fun_26b3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26c3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2370;

void fun_26d3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(int64_t rdi);

struct s0* fun_2610(int64_t rdi, ...);

void fun_2440(int64_t rdi, int64_t rsi);

void fun_2420(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

signed char ignore_interrupts = 0;

int32_t fun_2490(int64_t rdi, int64_t* rsi, int64_t rdx);

void usage();

void version_etc(struct s0* rdi, int64_t rsi, int64_t rdx);

int32_t fun_2680();

int64_t optarg = 0;

int64_t __xargmatch_internal(int64_t rdi);

void fun_2560(int64_t rdi, int64_t rsi, int64_t rdx);

int32_t optind = 0;

int32_t fun_24f0();

int64_t fun_2723(int32_t edi, int64_t* rsi) {
    int32_t ebp3;
    int64_t* rbx4;
    int64_t rdi5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rdi8;
    int64_t rsi9;
    int64_t rax10;
    int1_t zf11;
    int1_t zf12;
    int64_t rdx13;
    uint32_t eax14;
    int32_t eax15;
    uint32_t ebx16;
    int64_t rax17;

    __asm__("cli ");
    ebp3 = edi;
    rbx4 = rsi;
    rdi5 = *rsi;
    set_program_name(rdi5);
    fun_2610(6, 6);
    fun_2440("coreutils", "/usr/local/share/locale");
    fun_2420("coreutils", "/usr/local/share/locale");
    atexit(0x3610, "/usr/local/share/locale");
    append = 0;
    ignore_interrupts = 0;
    while (*reinterpret_cast<int32_t*>(&rdi6) = ebp3, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0, eax7 = fun_2490(rdi6, rbx4, "aip"), eax7 != -1) {
        if (eax7 == 97) {
            addr_2890_4:
            append = 1;
            continue;
        } else {
            if (eax7 <= 97) {
                if (eax7 != 0xffffff7d) {
                    if (eax7 != 0xffffff7e) 
                        goto addr_2912_8;
                    usage();
                    goto addr_2890_4;
                } else {
                    rdi8 = stdout;
                    version_etc(rdi8, "tee", "GNU coreutils");
                    eax7 = fun_2680();
                }
            } else {
                if (eax7 == 0x69) {
                    ignore_interrupts = 1;
                    continue;
                }
            }
        }
        if (eax7 != 0x70) 
            goto addr_2912_8;
        rsi9 = optarg;
        if (!rsi9) {
            output_error = 2;
        } else {
            rax10 = __xargmatch_internal("--output-error");
            output_error = *reinterpret_cast<uint32_t*>(0x7640 + rax10 * 4);
        }
    }
    zf11 = ignore_interrupts == 0;
    if (!zf11) {
        fun_2560(2, 1, "aip");
    }
    zf12 = output_error == 0;
    if (!zf12) {
        fun_2560(13, 1, "aip");
    }
    rdx13 = optind;
    eax14 = tee_files(ebp3 - *reinterpret_cast<int32_t*>(&rdx13), rbx4 + rdx13);
    eax15 = fun_24f0();
    if (!eax15) {
        ebx16 = eax14 ^ 1;
        *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<unsigned char*>(&ebx16);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        return rax17;
    }
    addr_291c_23:
    fun_2450();
    fun_23b0();
    fun_2640();
    addr_2912_8:
    usage();
    goto addr_291c_23;
}

int64_t __libc_start_main = 0;

void fun_2953() {
    __asm__("cli ");
    __libc_start_main(0x2720, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2380(int64_t rdi);

int64_t fun_29f3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2380(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2a33() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2620(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2530(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2550(unsigned char rdi, int64_t rsi, struct s0* rdx);

int32_t fun_23c0(struct s0* rdi, struct s0* rsi, struct s0* rdx, ...);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_26a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t* a8, int64_t a9, int64_t a10, int64_t a11, int64_t* a12);

void fun_2df3(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    struct s0* r12_7;
    struct s0* rax8;
    struct s0* r12_9;
    struct s0* rax10;
    struct s0* r12_11;
    struct s0* rax12;
    struct s0* r12_13;
    struct s0* rax14;
    struct s0* r12_15;
    struct s0* rax16;
    int32_t eax17;
    struct s0* r13_18;
    struct s0* rax19;
    struct s0* rax20;
    int32_t eax21;
    struct s0* rax22;
    struct s0* rax23;
    struct s0* rax24;
    int32_t eax25;
    struct s0* rax26;
    struct s0* r15_27;
    struct s0* rax28;
    struct s0* rax29;
    struct s0* rax30;
    struct s0* rdi31;
    struct s0* r8_32;
    struct s0* r9_33;
    int64_t v34;
    int64_t* v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t* v39;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2450();
            fun_2620(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2450();
            fun_2530(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2450();
            fun_2530(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2450();
            fun_2530(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2450();
            fun_2530(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2450();
            fun_2530(rax16, r12_15, 5, rcx6);
            do {
                if (1) 
                    break;
                eax17 = fun_2550("tee", 0, 5);
            } while (eax17);
            r13_18 = v4;
            if (!r13_18) {
                rax19 = fun_2450();
                fun_2620(1, rax19, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax20 = fun_2610(5);
                if (!rax20 || (eax21 = fun_23c0(rax20, "en_", 3, rax20, "en_", 3), !eax21)) {
                    rax22 = fun_2450();
                    r13_18 = reinterpret_cast<struct s0*>("tee");
                    fun_2620(1, rax22, "https://www.gnu.org/software/coreutils/", "tee");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_18 = reinterpret_cast<struct s0*>("tee");
                    goto addr_3148_9;
                }
            } else {
                rax23 = fun_2450();
                fun_2620(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_2610(5);
                if (!rax24 || (eax25 = fun_23c0(rax24, "en_", 3, rax24, "en_", 3), !eax25)) {
                    addr_304e_11:
                    rax26 = fun_2450();
                    fun_2620(1, rax26, "https://www.gnu.org/software/coreutils/", "tee");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_18 == "tee")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x7b41);
                    }
                } else {
                    addr_3148_9:
                    r15_27 = stdout;
                    rax28 = fun_2450();
                    fun_2530(rax28, r15_27, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_304e_11;
                }
            }
            rax29 = fun_2450();
            rcx6 = r12_2;
            fun_2620(1, rax29, r13_18, rcx6);
            addr_2e4e_14:
            fun_2680();
        }
    } else {
        rax30 = fun_2450();
        rdi31 = stderr;
        rcx6 = r12_2;
        fun_26a0(rdi31, 1, rax30, rcx6, r8_32, r9_33, v34, v35, v36, v37, v38, v39);
        goto addr_2e4e_14;
    }
}

void fun_3183() {
    __asm__("cli ");
    goto usage;
}

struct s0* fun_2470(struct s0* rdi, ...);

uint32_t fun_2520(struct s0* rdi, struct s0* rsi, struct s0* rdx, ...);

int64_t fun_3193(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* r14_5;
    struct s0* r13_6;
    struct s0* rbp7;
    struct s0** v8;
    struct s0* v9;
    struct s0* rax10;
    struct s0* r15_11;
    int64_t v12;
    unsigned char v13;
    struct s0* r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    struct s0* rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2470(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_23c0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_3213_5:
                ++rbx15;
                rbp7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_3260_6; else 
                    continue;
            } else {
                rax18 = fun_2470(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_3290_8;
                if (v16 == -1) 
                    goto addr_324e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_3213_5;
            } else {
                eax19 = fun_2520(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_3213_5;
            }
            addr_324e_10:
            v16 = rbx15;
            goto addr_3213_5;
        }
    }
    addr_3275_16:
    return v12;
    addr_3260_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_3275_16;
    addr_3290_8:
    v12 = rbx15;
    goto addr_3275_16;
}

int64_t fun_32a3(int64_t rdi, unsigned char* rsi, struct s0* rdx) {
    int64_t r12_4;
    unsigned char rdi5;
    unsigned char* rbp6;
    int64_t rbx7;
    int32_t eax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_32e8_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            eax8 = fun_2550(rdi5, r12_4, rdx);
            if (!eax8) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_32e8_2;
    }
    return rbx7;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_3303(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2450();
    } else {
        fun_2450();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_2640;
}

struct s0* quote(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3393(int64_t* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9) {
    struct s0* r13_7;
    int64_t* v8;
    int64_t* r12_9;
    struct s0* r12_10;
    struct s0* rdx11;
    int64_t v12;
    int64_t rbp13;
    struct s0* rbp14;
    int64_t v15;
    int64_t rbx16;
    struct s0* r14_17;
    int64_t* v18;
    struct s0* rax19;
    struct s0* rsi20;
    int64_t r15_21;
    int64_t rbx22;
    uint32_t eax23;
    struct s0* rax24;
    struct s0* rdi25;
    int64_t v26;
    int64_t v27;
    struct s0* rax28;
    struct s0* rdi29;
    int64_t v30;
    int64_t v31;
    struct s0* rdi32;
    signed char* rax33;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    *reinterpret_cast<int32_t*>(&rdx11) = 5;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    v12 = rbp13;
    rbp14 = rsi;
    v15 = rbx16;
    r14_17 = stderr;
    v18 = rdi;
    rax19 = fun_2450();
    rsi20 = r14_17;
    fun_2530(rax19, rsi20, 5, rcx);
    r15_21 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx22) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx22) + 4) = 0;
    if (r15_21) {
        do {
            if (!rbx22 || (rdx11 = r12_10, rsi20 = rbp14, eax23 = fun_2520(r13_7, rsi20, rdx11, r13_7, rsi20, rdx11), !!eax23)) {
                r13_7 = rbp14;
                rax24 = quote(r15_21, rsi20, rdx11, rcx);
                rdi25 = stderr;
                rdx11 = reinterpret_cast<struct s0*>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rcx = rax24;
                fun_26a0(rdi25, 1, "\n  - %s", rcx, r8, r9, v26, v18, v27, v15, v12, v8);
            } else {
                rax28 = quote(r15_21, rsi20, rdx11, rcx);
                rdi29 = stderr;
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rdx11 = reinterpret_cast<struct s0*>(", %s");
                rcx = rax28;
                fun_26a0(rdi29, 1, ", %s", rcx, r8, r9, v30, v18, v31, v15, v12, v8);
            }
            ++rbx22;
            rbp14 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp14) + reinterpret_cast<unsigned char>(r12_10));
            r15_21 = v18[rbx22];
        } while (r15_21);
    }
    rdi32 = stderr;
    rax33 = rdi32->f28;
    if (reinterpret_cast<uint64_t>(rax33) < reinterpret_cast<uint64_t>(rdi32->f30)) {
        rdi32->f28 = rax33 + 1;
        *rax33 = 10;
        return;
    }
}

int64_t argmatch(int64_t rdi, struct s0* rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_34c3(int64_t rdi, int64_t rsi, struct s0* rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    int64_t r14_9;
    int64_t r13_10;
    int64_t r12_11;
    struct s0* rbp12;
    int64_t v13;
    int64_t rax14;
    unsigned char rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_3507_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_357e_4;
        } else {
            addr_357e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<unsigned char*>(&rdx->f0);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_2550(rdi15, r14_9, rdx);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp12) + rbx16 * 8);
        } while (rdi15);
        goto addr_3500_8;
    } else {
        goto addr_3500_8;
    }
    return rbx16;
    addr_3500_8:
    rax14 = -1;
    goto addr_3507_3;
}

struct s5 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_3593(struct s0* rdi, struct s5* rsi, struct s0* rdx, struct s0* rcx) {
    int64_t r14_5;
    struct s0* r12_6;
    struct s0* r13_7;
    int64_t* rbx8;
    struct s0* rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2520(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_35f3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3603(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23d0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3613() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23b0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2450();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_36a3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2640();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_36a3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2640();
    }
}

void fun_36c3() {
    __asm__("cli ");
}

uint32_t fun_2590(struct s0* rdi);

void fun_36d3(struct s0* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2590(rdi);
        goto 0x2500;
    }
}

int32_t fun_25d0(struct s0* rdi);

int64_t fun_24d0(int64_t rdi, ...);

int32_t rpl_fflush(struct s0* rdi);

int64_t fun_2430(struct s0* rdi);

int64_t fun_3703(struct s0* rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2590(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_25d0(rdi);
        if (!(eax3 && (eax4 = fun_2590(rdi), *reinterpret_cast<uint32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23b0();
            r12d9 = *rax8;
            rax10 = fun_2430(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2430;
}

void rpl_fseeko(struct s0* rdi);

void fun_3793(struct s0* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25d0(rdi), !eax2) || !(*reinterpret_cast<unsigned char*>(&rdi->f0) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

struct s0* fun_2660();

int32_t dup_safer(int64_t rdi);

struct s0* fun_2600(int64_t rdi, int64_t rsi);

struct s0* fun_37e3() {
    struct s0* rax1;
    struct s0* r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    int32_t* rax6;
    struct s0* rdi7;
    int32_t r13d8;
    struct s0* rsi9;
    struct s0* rdx10;
    struct s0* rcx11;
    struct s0* rsi12;
    struct s0* rdx13;
    struct s0* rcx14;
    int64_t rax15;
    int64_t rdi16;
    int64_t rsi17;
    struct s0* rax18;
    int32_t* rax19;
    int32_t r12d20;

    __asm__("cli ");
    rax1 = fun_2660();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_3806_2;
    eax3 = fun_2590(rax1);
    if (eax3 > 2) 
        goto addr_3806_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_23b0();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        r13d8 = *rax6;
        rpl_fclose(rdi7, rsi9, rdx10, rcx11);
        *rax6 = r13d8;
        goto addr_3806_2;
    } else {
        rax15 = rpl_fclose(r12_2, rsi12, rdx13, rcx14);
        if (!*reinterpret_cast<int32_t*>(&rax15)) {
            *reinterpret_cast<int32_t*>(&rdi16) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
            rax18 = fun_2600(rdi16, rsi17);
            r12_2 = rax18;
            if (rax18) {
                addr_3806_2:
                return r12_2;
            }
        }
        rax19 = fun_23b0();
        r12d20 = *rax19;
        fun_24f0();
        *rax19 = r12d20;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        goto addr_3806_2;
    }
}

int64_t fun_3883(struct s0* rdi, int64_t rsi, int32_t edx) {
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2590(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<unsigned char*>(&rdi->f0) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdi->f0) & 0xffffffef);
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_2690(struct s1* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s6 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s6* fun_24c0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_3903(struct s0* rdi) {
    struct s0* rcx2;
    struct s0* rbx3;
    struct s6* rax4;
    struct s0* r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2690("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23a0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_23c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_50a3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23b0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xb220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_50e3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5103(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb220);
    }
    *rdi = esi;
    return 0xb220;
}

int64_t fun_5123(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s7 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5163(struct s7* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s8 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s8* fun_5183(struct s8* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xb220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ea;
    if (!rdx) 
        goto 0x26ea;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb220;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_51c3(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s9* r8) {
    struct s9* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s9*>(0xb220);
    }
    rax7 = fun_23b0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x51f6);
    *rax7 = r15d8;
    return rax13;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    unsigned char f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* fun_5243(struct s0* rdi, struct s0* rsi, struct s0** rdx, struct s10* rcx) {
    struct s10* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s1* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s10*>(0xb220);
    }
    rax6 = fun_23b0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5271);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x52cc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5333() {
    __asm__("cli ");
}

struct s1* gb098 = reinterpret_cast<struct s1*>(0xb120);

int64_t slotvec0 = 0x100;

void fun_5343() {
    uint32_t eax1;
    struct s1* r12_2;
    int64_t rax3;
    struct s1** rbx4;
    struct s1** rbp5;
    struct s1* rdi6;
    struct s0* rsi7;
    struct s0* rdx8;
    struct s0* rcx9;
    int64_t r8_10;
    int32_t* r9_11;
    struct s1* rdi12;
    struct s0* rsi13;
    struct s0* rdx14;
    struct s0* rcx15;
    int64_t r8_16;
    int32_t* r9_17;
    struct s0* rsi18;
    struct s0* rdx19;
    struct s0* rcx20;
    int64_t r8_21;
    int32_t* r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s1**>(reinterpret_cast<int64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2390(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = r12_2->f8;
    if (rdi12 != 0xb120) {
        fun_2390(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        gb098 = reinterpret_cast<struct s1*>(0xb120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb090) {
        fun_2390(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<struct s1*>(0xb090);
    }
    nslots = 1;
    return;
}

void fun_53e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5403() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5413(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5433(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s1* fun_5453(struct s1* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26f0;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s1* fun_54e3(struct s1* rdi, int32_t esi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26f5;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s1* fun_5573(int32_t edi, struct s0* rsi) {
    struct s0* rax3;
    struct s2* rcx4;
    struct s1* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26fa;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2480();
    } else {
        return rax5;
    }
}

struct s1* fun_5603(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26ff;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s1* fun_5693(struct s0* rdi, struct s0* rsi, uint32_t edx) {
    struct s2* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s1* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5b80]");
    __asm__("movdqa xmm1, [rip+0x5b88]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5b71]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2480();
    } else {
        return rax10;
    }
}

struct s1* fun_5733(struct s0* rdi, uint32_t esi) {
    struct s2* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5ae0]");
    __asm__("movdqa xmm1, [rip+0x5ae8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5ad1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s1* fun_57d3(struct s0* rdi) {
    struct s0* rax2;
    struct s1* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5a40]");
    __asm__("movdqa xmm1, [rip+0x5a48]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5a29]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2480();
    } else {
        return rax3;
    }
}

struct s1* fun_5863(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s1* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x59b0]");
    __asm__("movdqa xmm1, [rip+0x59b8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x59a6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2480();
    } else {
        return rax4;
    }
}

struct s1* fun_58f3(struct s1* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2704;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s1* fun_5993(struct s1* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x587a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5872]");
    __asm__("movdqa xmm2, [rip+0x587a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2709;
    if (!rdx) 
        goto 0x2709;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s1* fun_5a33(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rcx6;
    struct s2* rcx7;
    struct s1* rdi8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x57da]");
    __asm__("movdqa xmm1, [rip+0x57e2]");
    __asm__("movdqa xmm2, [rip+0x57ea]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x270e;
    if (!rdx) 
        goto 0x270e;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s1* fun_5ae3(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x572a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5722]");
    __asm__("movdqa xmm2, [rip+0x572a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2713;
    if (!rsi) 
        goto 0x2713;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s1* fun_5b83(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x568a]");
    __asm__("movdqa xmm1, [rip+0x5692]");
    __asm__("movdqa xmm2, [rip+0x569a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2718;
    if (!rsi) 
        goto 0x2718;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

void fun_5c23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c33(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c73(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c93() {
    __asm__("cli ");
}

struct s11 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t* f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t* f40;
};

void fun_2570(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_5cb3(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s11* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t* v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t* v13;
    int64_t v14;
    int64_t* v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t* v19;
    struct s0* rax20;
    int64_t v21;
    int64_t* v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t* v26;
    struct s0* rax27;
    int64_t v28;
    int64_t* v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t* v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t* rcx37;
    int64_t r15_38;
    int64_t* v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26a0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26a0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2450();
    fun_26a0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2570(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2450();
    fun_26a0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2570(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2450();
        fun_26a0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7de8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7de8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6123() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6143(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s12* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2480();
    } else {
        return;
    }
}

void fun_61e3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6286_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6290_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2480();
    } else {
        return;
    }
    addr_6286_5:
    goto addr_6290_7;
}

void fun_62c3() {
    struct s0* rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2570(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2450();
    fun_2620(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2450();
    fun_2620(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2450();
    goto fun_2620;
}

int64_t fun_23f0();

void xalloc_die();

void fun_6363(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_25a0(signed char* rdi);

void fun_63a3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_63c3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_63e3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25f0();

void fun_6403(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25f0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6433() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6463(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_64a3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6513(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6563(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23f0();
            if (rax5) 
                break;
            addr_65ad_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_65ad_5;
        rax8 = fun_23f0();
        if (rax8) 
            goto addr_6596_9;
        if (rbx4) 
            goto addr_65ad_5;
        addr_6596_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_65f3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23f0();
            if (rax8) 
                break;
            addr_663a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_663a_5;
        rax11 = fun_23f0();
        if (rax11) 
            goto addr_6622_9;
        if (!rbx6) 
            goto addr_6622_9;
        if (r12_4) 
            goto addr_663a_5;
        addr_6622_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6683(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_672d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6740_10:
                *r12_8 = 0;
            }
            addr_66e0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6706_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6754_14;
            if (rcx10 <= rsi9) 
                goto addr_66fd_16;
            if (rsi9 >= 0) 
                goto addr_6754_14;
            addr_66fd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6754_14;
            addr_6706_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25f0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6754_14;
            if (!rbp13) 
                break;
            addr_6754_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_672d_9;
        } else {
            if (!r13_6) 
                goto addr_6740_10;
            goto addr_66e0_11;
        }
    }
}

int64_t fun_2540();

void fun_6783() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6803() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2580(signed char* rdi, struct s0* rsi, signed char* rdx);

void fun_6823(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2580;
    }
}

void fun_6863(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2580;
    }
}

struct s13 {
    signed char[1] pad1;
    signed char f1;
};

void fun_68a3(int64_t rdi, struct s13* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25a0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2580;
    }
}

void fun_68e3(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2470(rdi);
    rax3 = fun_25a0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2580;
    }
}

void fun_6923() {
    struct s1* rdi1;

    __asm__("cli ");
    fun_2450();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2640();
    fun_23a0(rdi1);
}

int64_t fun_23e0();

int64_t fun_6963(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx) {
    int64_t rax5;
    uint32_t ebx6;
    int64_t rax7;
    int32_t* rax8;
    int32_t* rax9;

    __asm__("cli ");
    rax5 = fun_23e0();
    ebx6 = *reinterpret_cast<unsigned char*>(&rdi->f0) & 32;
    rax7 = rpl_fclose(rdi, rsi, rdx, rcx);
    if (ebx6) {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            addr_69be_3:
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rax8 = fun_23b0();
            *rax8 = 0;
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            if (rax5) 
                goto addr_69be_3;
            rax9 = fun_23b0();
            *reinterpret_cast<int32_t*>(&rax7) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax9 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    }
    return rax7;
}

uint32_t fun_2400(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_69d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    struct s0* rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    int32_t* rax20;
    int32_t r13d21;
    uint32_t eax22;
    int32_t* rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2400(rdi);
        r12d10 = eax9;
        goto addr_6ad4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2400(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_6ad4_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2480();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_6b89_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2400(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2400(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_23b0();
                    r12d10 = 0xffffffff;
                    r13d21 = *rax20;
                    fun_24f0();
                    *rax20 = r13d21;
                    goto addr_6ad4_3;
                }
            }
        } else {
            eax22 = fun_2400(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_23b0(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, *rax23 != 22)) {
                have_dupfd_cloexec_0 = 1;
                goto addr_6ad4_3;
            } else {
                eax25 = fun_2400(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_6ad4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_6b89_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_6a39_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_6a3d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_6a3d_18:
            if (0) {
            }
        } else {
            addr_6a85_23:
            eax28 = fun_2400(rdi);
            r12d10 = eax28;
            goto addr_6ad4_3;
        }
        eax29 = fun_2400(rdi);
        r12d10 = eax29;
        goto addr_6ad4_3;
    }
    if (0) {
    }
    eax30 = fun_2400(rdi);
    r12d10 = eax30;
    goto addr_6ad4_3;
    addr_6a39_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_6a3d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_6a85_23;
        goto addr_6a3d_18;
    }
}

signed char* fun_25c0(int64_t rdi);

signed char* fun_6c43() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25c0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6c83(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2480();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6d13() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax3;
    }
}

int64_t fun_6d93(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2610(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2470(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2580(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2580(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6e43() {
    __asm__("cli ");
    goto fun_2610;
}

void fun_6e53() {
    __asm__("cli ");
}

void fun_6e67() {
    __asm__("cli ");
    return;
}

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_26c0(int64_t rdi, struct s0* rsi);

uint32_t fun_26b0(struct s0* rdi, struct s0* rsi);

void** fun_26d0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3b35() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2450();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2450();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2470(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3e33_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3e33_22; else 
                            goto addr_422d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_42ed_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4640_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3e30_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3e30_30; else 
                                goto addr_4659_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2470(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4640_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2520(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4640_28; else 
                            goto addr_3cdc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_47a0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4620_40:
                        if (r11_27 == 1) {
                            addr_41ad_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4768_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3de7_44;
                            }
                        } else {
                            goto addr_4630_46;
                        }
                    } else {
                        addr_47af_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_41ad_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3e33_22:
                                if (v47 != 1) {
                                    addr_4389_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2470(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_43d4_54;
                                    }
                                } else {
                                    goto addr_3e40_56;
                                }
                            } else {
                                addr_3de5_57:
                                ebp36 = 0;
                                goto addr_3de7_44;
                            }
                        } else {
                            addr_4614_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_47af_47; else 
                                goto addr_461e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_41ad_41;
                        if (v47 == 1) 
                            goto addr_3e40_56; else 
                            goto addr_4389_52;
                    }
                }
                addr_3ea1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3d38_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3d5d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4060_65;
                    } else {
                        addr_3ec9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4718_67;
                    }
                } else {
                    goto addr_3ec0_69;
                }
                addr_3d71_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_3dbc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4718_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3dbc_81;
                }
                addr_3ec0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3d5d_64; else 
                    goto addr_3ec9_66;
                addr_3de7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3e9f_91;
                if (v22) 
                    goto addr_3dff_93;
                addr_3e9f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3ea1_62;
                addr_43d4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4b5b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4bcb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_49cf_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26c0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26b0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_44ce_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3e8c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_44d8_112;
                    }
                } else {
                    addr_44d8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_45a9_114;
                }
                addr_3e98_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3e9f_91;
                while (1) {
                    addr_45a9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4ab7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4516_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4ac5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4597_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4597_128;
                        }
                    }
                    addr_4545_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4597_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_4516_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4545_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3dbc_81;
                addr_4ac5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4718_67;
                addr_4b5b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_44ce_109;
                addr_4bcb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_44ce_109;
                addr_3e40_56:
                rax93 = fun_26d0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3e8c_110;
                addr_461e_59:
                goto addr_4620_40;
                addr_42ed_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3e33_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3e98_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3de5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3e33_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4332_160;
                if (!v22) 
                    goto addr_4707_162; else 
                    goto addr_4913_163;
                addr_4332_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4707_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4718_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_41db_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4043_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3d71_70; else 
                    goto addr_4057_169;
                addr_41db_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3d38_63;
                goto addr_3ec0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4614_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_474f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3e30_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3d28_178; else 
                        goto addr_46d2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4614_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3e33_22;
                }
                addr_474f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3e30_30:
                    r8d42 = 0;
                    goto addr_3e33_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3ea1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4768_42;
                    }
                }
                addr_3d28_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3d38_63;
                addr_46d2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4630_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3ea1_62;
                } else {
                    addr_46e2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3e33_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4e92_188;
                if (v28) 
                    goto addr_4707_162;
                addr_4e92_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4043_168;
                addr_3cdc_37:
                if (v22) 
                    goto addr_4cd3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3cf3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_47a0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_482b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3e33_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3d28_178; else 
                        goto addr_4807_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4614_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3e33_22;
                }
                addr_482b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3e33_22;
                }
                addr_4807_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4630_46;
                goto addr_46e2_186;
                addr_3cf3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3e33_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3e33_22; else 
                    goto addr_3d04_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_4dde_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4c64_210;
            if (1) 
                goto addr_4c62_212;
            if (!v29) 
                goto addr_489e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_4dd1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4060_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_3e1b_219; else 
            goto addr_407a_220;
        addr_3dff_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3e13_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_407a_220; else 
            goto addr_3e1b_219;
        addr_49cf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_407a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_49ed_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4e60_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_48c6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4ab7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3e13_221;
        addr_4913_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3e13_221;
        addr_4057_169:
        goto addr_4060_65;
        addr_4dde_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_407a_220;
        goto addr_49ed_222;
        addr_4c64_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4cbe_236;
        fun_2480();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4e60_225;
        addr_4c62_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4c64_210;
        addr_489e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4c64_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_48c6_226;
        }
        addr_4dd1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_422d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x78ac + rax113 * 4) + 0x78ac;
    addr_4659_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x79ac + rax114 * 4) + 0x79ac;
    addr_4cd3_190:
    addr_3e1b_219:
    goto 0x3b00;
    addr_3d04_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x77ac + rax115 * 4) + 0x77ac;
    addr_4cbe_236:
    goto v116;
}

void fun_3d20() {
}

void fun_3ed8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3bd2;
}

void fun_3f31() {
    goto 0x3bd2;
}

void fun_401e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3ea1;
    }
    if (v2) 
        goto 0x4913;
    if (!r10_3) 
        goto addr_4a7e_5;
    if (!v4) 
        goto addr_494e_7;
    addr_4a7e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_494e_7:
    goto 0x3d54;
}

void fun_403c() {
}

void fun_40e7() {
    signed char v1;

    if (v1) {
        goto 0x406f;
    } else {
        goto 0x3daa;
    }
}

void fun_4101() {
    signed char v1;

    if (!v1) 
        goto 0x40fa; else 
        goto "???";
}

void fun_4128() {
    goto 0x4043;
}

void fun_41a8() {
}

void fun_41c0() {
}

void fun_41ef() {
    goto 0x4043;
}

void fun_4241() {
    goto 0x41d0;
}

void fun_4270() {
    goto 0x41d0;
}

void fun_42a3() {
    goto 0x41d0;
}

void fun_4670() {
    goto 0x3d28;
}

void fun_496e() {
    signed char v1;

    if (v1) 
        goto 0x4913;
    goto 0x3d54;
}

void fun_4a15() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3d54;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3d38;
        goto 0x3d54;
    }
}

void fun_4e32() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x40a0;
    } else {
        goto 0x3bd2;
    }
}

void fun_5d88() {
    fun_2450();
}

void fun_3f5e() {
    goto 0x3bd2;
}

void fun_4134() {
    goto 0x40ec;
}

void fun_41fb() {
    goto 0x3d28;
}

void fun_424d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x41d0;
    goto 0x3dff;
}

void fun_427f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x41db;
        goto 0x3c00;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x407a;
        goto 0x3e1b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4a18;
    if (r10_8 > r15_9) 
        goto addr_4165_9;
    addr_416a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4a23;
    goto 0x3d54;
    addr_4165_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_416a_10;
}

void fun_42b2() {
    goto 0x3de7;
}

void fun_4680() {
    goto 0x3de7;
}

void fun_4e1f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3f3c;
    } else {
        goto 0x40a0;
    }
}

void fun_5e40() {
}

void fun_42bc() {
    goto 0x4257;
}

void fun_468a() {
    goto 0x41ad;
}

void fun_5ea0() {
    fun_2450();
    goto fun_26a0;
}

void fun_3f8d() {
    goto 0x3bd2;
}

void fun_42c8() {
    goto 0x4257;
}

void fun_4697() {
    goto 0x41fe;
}

void fun_5ee0() {
    fun_2450();
    goto fun_26a0;
}

void fun_3fba() {
    goto 0x3bd2;
}

void fun_42d4() {
    goto 0x41d0;
}

void fun_5f20() {
    fun_2450();
    goto fun_26a0;
}

void fun_3fdc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4970;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3ea1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3ea1;
    }
    if (v11) 
        goto 0x4cd3;
    if (r10_12 > r15_13) 
        goto addr_4d23_8;
    addr_4d28_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4a61;
    addr_4d23_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4d28_9;
}

struct s14 {
    signed char[24] pad24;
    int64_t f18;
};

struct s15 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s16 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_5f70() {
    int64_t r15_1;
    struct s14* rbx2;
    struct s0* r14_3;
    struct s15* rbx4;
    struct s0* r13_5;
    struct s16* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t* v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2450();
    fun_26a0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5f92, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5fc8() {
    fun_2450();
    goto 0x5f99;
}

struct s17 {
    signed char[32] pad32;
    int64_t* f20;
};

struct s18 {
    signed char[24] pad24;
    int64_t f18;
};

struct s19 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s20 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s21 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6000() {
    int64_t* rcx1;
    struct s17* rbx2;
    int64_t r15_3;
    struct s18* rbx4;
    struct s0* r14_5;
    struct s19* rbx6;
    struct s0* r13_7;
    struct s20* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s21* rbx12;
    struct s0* rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2450();
    fun_26a0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6034, __return_address(), rcx1);
    goto v15;
}

void fun_6078() {
    fun_2450();
    goto 0x603b;
}
