void main(word64 rbx, word32 ebp, char * rsi[], int32 edi, struct Eq_357 * fs)
{
	set_program_name(rsi[0]);
	fn0000000000002510("", 0x06);
	fn00000000000023A0("/usr/local/share/locale", "coreutils");
	fn0000000000002380("coreutils");
	atexit(&g_t2B70);
	parse_gnu_standard_options_only(0x00, 0x7004, rsi, (uint64) edi, 0x01, fs, &g_t2810);
	char * rax_79 = (int64) g_dwA090;
	if ((word32) rax_79 < edi)
	{
		quote(rsi[rax_79], fs);
		fn0000000000002530(fn00000000000023B0(0x05, "extra operand %s", null), 0x00, 0x00);
		usage(0x01);
	}
	else
	{
		fn00000000000024E0();
		if (rax_79 == null)
		{
			Eq_26 rax_129 = fn00000000000023B0(0x05, "no login name", rax_79);
			fn0000000000002530(rax_129, 0x00, 0x01);
			_start(rax_129, SLICE(rbx, word32, 32));
		}
		else
			fn0000000000002360(rax_79);
	}
}