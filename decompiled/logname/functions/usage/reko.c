void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002580(fn00000000000023B0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000286E;
	}
	fn0000000000002520(fn00000000000023B0(0x05, "Usage: %s [OPTION]\n", null), 0x01);
	fn0000000000002450(stdout, fn00000000000023B0(0x05, "Print the user's login name.\n\n", null));
	fn0000000000002450(stdout, fn00000000000023B0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002450(stdout, fn00000000000023B0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_564 * rbx_139 = fp - 0xB8 + 16;
	do
	{
		char * rsi_141 = rbx_139->qw0000;
		++rbx_139;
	} while (rsi_141 != null && fn0000000000002470(rsi_141, "logname") != 0x00);
	ptr64 r13_154 = rbx_139->qw0008;
	if (r13_154 != 0x00)
	{
		fn0000000000002520(fn00000000000023B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_241 = fn0000000000002510(null, 0x05);
		if (rax_241 == 0x00 || fn0000000000002330(0x03, "en_", rax_241) == 0x00)
			goto l0000000000002A26;
	}
	else
	{
		fn0000000000002520(fn00000000000023B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_183 = fn0000000000002510(null, 0x05);
		if (rax_183 == 0x00 || fn0000000000002330(0x03, "en_", rax_183) == 0x00)
		{
			fn0000000000002520(fn00000000000023B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002A63:
			fn0000000000002520(fn00000000000023B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000286E:
			fn0000000000002560(edi);
		}
		r13_154 = 0x7004;
	}
	fn0000000000002450(stdout, fn00000000000023B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002A26:
	fn0000000000002520(fn00000000000023B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002A63;
}