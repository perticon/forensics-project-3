show_date (char const *format, struct timespec when, timezone_t tz)
{
  struct tm tm;
  if (localtime_rz (tz, &when.tv_sec, &tm))
    fprintftime (stdout, format, &tm, tz, when.tv_nsec);
  else
    {
      char buf[INT_BUFSIZE_BOUND (intmax_t)];
      char *when_str = timetostr (when.tv_sec, buf);
      error (0, 0, _("time %s is out of range"), quote (when_str));
      fputs (when_str, stdout);
    }
}