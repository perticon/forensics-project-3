duinfo_set (struct duinfo *a, uintmax_t size, struct timespec tmax)
{
  a->size = size;
  a->inodes = 1;
  a->tmax = tmax;
}