void print_only_size(void** rdi) {
    void* rsp2;
    void** rbp3;
    void* rax4;
    void** rdx5;
    void** rax6;
    void* rsp7;
    void** rdi8;
    void** rax9;
    void* rax10;
    void** rdi11;
    void* rax12;
    int1_t zf13;
    void* rsp14;
    int1_t zf15;
    void** rdi16;
    void** rax17;
    void** r14_18;
    void** r13_19;
    void** rbp20;
    int64_t rax21;
    void** rsi22;
    int64_t v23;
    void** rax24;
    void** rax25;
    void** rsi26;
    void** rdi27;
    void* rax28;

    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0x2a0);
    rbp3 = stdout;
    rax4 = g28;
    if (rdi == 0xffffffffffffffff) {
        *reinterpret_cast<int32_t*>(&rdx5) = 5;
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
        rax6 = fun_3820();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rdi8 = rax6;
    } else {
        *reinterpret_cast<int32_t*>(&rdx5) = human_output_opts;
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
        rax9 = human_readable();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rdi8 = rax9;
    }
    fun_3980(rdi8, rbp3, rdx5);
    rax10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (!rax10) {
        return;
    }
    fun_3860();
    rdi11 = *reinterpret_cast<void***>(rdi8 + 8);
    rax12 = g28;
    zf13 = opt_inodes == 0;
    if (!zf13) 
        goto addr_54b1_8;
    rdi11 = *reinterpret_cast<void***>(rdi8);
    addr_54b1_8:
    print_only_size(rdi11);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x70 - 8 + 8);
    zf15 = opt_time == 0;
    if (!zf15) {
        rdi16 = stdout;
        rax17 = *reinterpret_cast<void***>(rdi16 + 40);
        if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
            fun_38c0();
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        } else {
            *reinterpret_cast<void***>(rdi16 + 40) = rax17 + 1;
            *reinterpret_cast<void***>(rax17) = reinterpret_cast<void**>(9);
        }
        r14_18 = localtz;
        __asm__("movdqu xmm0, [rbx+0x10]");
        r13_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp14) + 16);
        rbp20 = time_format;
        __asm__("movaps [rsp], xmm0");
        rax21 = localtime_rz(r14_18, rsp14, r13_19);
        if (!rax21) {
            rsi22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 + 80);
            rax24 = imaxtostr(v23, rsi22, r13_19);
            quote(rax24, rsi22, r13_19);
            rax25 = fun_3820();
            fun_3b70();
            rsi26 = stdout;
            fun_3980(rax24, rsi26, rax25);
        } else {
            rdi27 = stdout;
            fprintftime(rdi27, rbp20, r13_19);
        }
    }
    fun_3b30(1, "\t%s%c", rbp3);
    rax28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax12) - reinterpret_cast<uint64_t>(g28));
    if (rax28) {
        fun_3860();
    }
}