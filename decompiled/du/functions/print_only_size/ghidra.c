void print_only_size(long param_1)

{
  FILE *__stream;
  char *__s;
  long in_FS_OFFSET;
  undefined auStack680 [664];
  long local_10;
  
  __stream = stdout;
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == -1) {
    __s = (char *)dcgettext(0,"Infinity",5);
  }
  else {
    __s = (char *)human_readable(param_1,auStack680,human_output_opts,1,output_block_size);
  }
  fputs_unlocked(__s,__stream);
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}