void print_only_size(int64_t n_bytes) {
    int64_t v1 = __readfsqword(40); // 0x53ff
    if (n_bytes == -1) {
        // 0x5460
        function_3820();
    } else {
        // 0x5418
        int64_t v2; // bp-680, 0x53f0
        human_readable(n_bytes, (char *)&v2, human_output_opts, 1, output_block_size);
    }
    // 0x5435
    function_3980();
    if (v1 == __readfsqword(40)) {
        // 0x5450
        return;
    }
    // 0x5478
    function_3860();
}