void print_size(struct s0* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rdi7;
    void* rax8;
    int1_t zf9;
    void* rsp10;
    int1_t zf11;
    void** rdi12;
    void** rax13;
    void** r14_14;
    void** r13_15;
    void** rbp16;
    int64_t rax17;
    void** rsi18;
    int64_t v19;
    void** rax20;
    void** rax21;
    void** rsi22;
    void** rdi23;
    void* rax24;

    rdi7 = rdi->f8;
    rax8 = g28;
    zf9 = opt_inodes == 0;
    if (zf9) {
        rdi7 = rdi->f0;
    }
    print_only_size(rdi7);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x70 - 8 + 8);
    zf11 = opt_time == 0;
    if (!zf11) {
        rdi12 = stdout;
        rax13 = *reinterpret_cast<void***>(rdi12 + 40);
        if (reinterpret_cast<unsigned char>(rax13) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi12 + 48))) {
            fun_38c0();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        } else {
            *reinterpret_cast<void***>(rdi12 + 40) = rax13 + 1;
            *reinterpret_cast<void***>(rax13) = reinterpret_cast<void**>(9);
        }
        r14_14 = localtz;
        __asm__("movdqu xmm0, [rbx+0x10]");
        r13_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 16);
        rbp16 = time_format;
        __asm__("movaps [rsp], xmm0");
        rax17 = localtime_rz(r14_14, rsp10, r13_15);
        if (!rax17) {
            rsi18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 + 80);
            rax20 = imaxtostr(v19, rsi18, r13_15);
            quote(rax20, rsi18, r13_15);
            rax21 = fun_3820();
            fun_3b70();
            rsi22 = stdout;
            fun_3980(rax20, rsi22, rax21);
        } else {
            rdi23 = stdout;
            fprintftime(rdi23, rbp16, r13_15);
        }
    }
    fun_3b30(1, "\t%s%c", rsi);
    rax24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) - reinterpret_cast<uint64_t>(g28));
    if (rax24) {
        fun_3860();
    }
}