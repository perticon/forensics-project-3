print_size (const struct duinfo *pdui, char const *string)
{
  print_only_size (opt_inodes
                   ? pdui->inodes
                   : pdui->size);

  if (opt_time)
    {
      putchar ('\t');
      show_date (time_format, pdui->tmax, localtz);
    }
  printf ("\t%s%c", string, opt_nul_terminate_output ? '\0' : '\n');
  fflush (stdout);
}