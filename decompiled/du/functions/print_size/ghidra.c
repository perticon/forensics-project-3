void print_size(undefined8 *param_1,undefined8 param_2)

{
  long lVar1;
  char *pcVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  undefined8 local_98;
  ulong uStack144;
  undefined local_88 [64];
  undefined local_48 [24];
  long local_30;
  
  uVar3 = param_1[1];
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (opt_inodes == '\0') {
    uVar3 = *param_1;
  }
  print_only_size(uVar3);
  if (opt_time != '\0') {
    pcVar2 = stdout->_IO_write_ptr;
    if (pcVar2 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar2 + 1;
      *pcVar2 = '\t';
    }
    else {
      __overflow(stdout,9);
    }
    uVar4 = time_format;
    uVar3 = localtz;
    local_98 = param_1[2];
    uStack144 = param_1[3];
    lVar1 = localtime_rz(local_98,localtz,&local_98,local_88);
    if (lVar1 == 0) {
      pcVar2 = (char *)imaxtostr(local_98,local_48);
      uVar3 = quote(pcVar2);
      uVar4 = dcgettext(0,"time %s is out of range",5);
      error(0,0,uVar4,uVar3);
      fputs_unlocked(pcVar2,stdout);
    }
    else {
      fprintftime(stdout,uVar4,local_88,uVar3,uStack144 & 0xffffffff);
    }
  }
  __printf_chk(1,"\t%s%c",param_2,-(opt_nul_terminate_output == '\0') & 10);
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    (*(code *)PTR_fflush_unlocked_0012bf40)(stdout);
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}