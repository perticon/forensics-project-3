void print_size(int32_t * pdui, char * string) {
    int64_t v1 = (int64_t)pdui;
    int64_t v2 = __readfsqword(40); // 0x5496
    print_only_size(*(int64_t *)(v1 + 8));
    if (*(char *)&opt_time != 0) {
        int64_t v3 = (int64_t)g42; // 0x54bf
        int64_t * v4 = (int64_t *)(v3 + 40); // 0x54c6
        uint64_t v5 = *v4; // 0x54c6
        if (v5 >= *(int64_t *)(v3 + 48)) {
            // 0x5580
            function_38c0();
        } else {
            // 0x54d4
            *v4 = v5 + 1;
            *(char *)v5 = 9;
        }
        int64_t v6 = __asm_movaps(__asm_movdqu(*(int128_t *)(v1 + 16))); // bp-152, 0x5500
        int64_t v7; // bp-136, 0x5480
        if (localtime_rz(localtz, &v6, (struct tm *)&v7) == NULL) {
            // 0x5590
            int64_t v8; // bp-72, 0x5480
            char * v9 = imaxtostr(v6, (char *)&v8); // 0x5599
            quote(v9);
            function_3820();
            function_3b70();
            function_3980();
        } else {
            // 0x5512
            int32_t v10; // 0x5480
            fprintftime(g42, (char *)time_format, (struct tm *)&v7, localtz, v10);
        }
    }
    // 0x552c
    function_3b30();
    if (v2 != __readfsqword(40)) {
        // 0x55e4
        function_3860();
        return;
    }
    // 0x5562
    function_3c50();
}