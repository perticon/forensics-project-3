usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("\
Usage: %s [OPTION]... [FILE]...\n\
  or:  %s [OPTION]... --files0-from=F\n\
"), program_name, program_name);
      fputs (_("\
Summarize device usage of the set of FILEs, recursively for directories.\n\
"), stdout);

      emit_mandatory_arg_note ();

      fputs (_("\
  -0, --null            end each output line with NUL, not newline\n\
  -a, --all             write counts for all files, not just directories\n\
      --apparent-size   print apparent sizes rather than device usage; although\
\n\
                          the apparent size is usually smaller, it may be\n\
                          larger due to holes in ('sparse') files, internal\n\
                          fragmentation, indirect blocks, and the like\n\
"), stdout);
      fputs (_("\
  -B, --block-size=SIZE  scale sizes by SIZE before printing them; e.g.,\n\
                           '-BM' prints sizes in units of 1,048,576 bytes;\n\
                           see SIZE format below\n\
  -b, --bytes           equivalent to '--apparent-size --block-size=1'\n\
  -c, --total           produce a grand total\n\
  -D, --dereference-args  dereference only symlinks that are listed on the\n\
                          command line\n\
  -d, --max-depth=N     print the total for a directory (or file, with --all)\n\
                          only if it is N or fewer levels below the command\n\
                          line argument;  --max-depth=0 is the same as\n\
                          --summarize\n\
"), stdout);
      fputs (_("\
      --files0-from=F   summarize device usage of the\n\
                          NUL-terminated file names specified in file F;\n\
                          if F is -, then read names from standard input\n\
  -H                    equivalent to --dereference-args (-D)\n\
  -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)\
\n\
      --inodes          list inode usage information instead of block usage\n\
"), stdout);
      fputs (_("\
  -k                    like --block-size=1K\n\
  -L, --dereference     dereference all symbolic links\n\
  -l, --count-links     count sizes many times if hard linked\n\
  -m                    like --block-size=1M\n\
"), stdout);
      fputs (_("\
  -P, --no-dereference  don't follow any symbolic links (this is the default)\n\
  -S, --separate-dirs   for directories do not include size of subdirectories\n\
      --si              like -h, but use powers of 1000 not 1024\n\
  -s, --summarize       display only a total for each argument\n\
"), stdout);
      fputs (_("\
  -t, --threshold=SIZE  exclude entries smaller than SIZE if positive,\n\
                          or entries greater than SIZE if negative\n\
      --time            show time of the last modification of any file in the\n\
                          directory, or any of its subdirectories\n\
      --time=WORD       show time as WORD instead of modification time:\n\
                          atime, access, use, ctime or status\n\
      --time-style=STYLE  show times using STYLE, which can be:\n\
                            full-iso, long-iso, iso, or +FORMAT;\n\
                            FORMAT is interpreted like in 'date'\n\
"), stdout);
      fputs (_("\
  -X, --exclude-from=FILE  exclude files that match any pattern in FILE\n\
      --exclude=PATTERN    exclude files that match PATTERN\n\
  -x, --one-file-system    skip directories on different file systems\n\
"), stdout);
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
      emit_blocksize_note ("DU");
      emit_size_note ();
      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}