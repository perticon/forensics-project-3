fill_mount_table (void)
{
  struct mount_entry *mnt_ent = read_file_system_list (false);
  while (mnt_ent)
    {
      struct mount_entry *mnt_free;
      if (!mnt_ent->me_remote && !mnt_ent->me_dummy)
        {
          struct stat buf;
          if (!stat (mnt_ent->me_mountdir, &buf))
            hash_ins (di_mnt, buf.st_ino, buf.st_dev);
          else
            {
              /* Ignore stat failure.  False positives are too common.
                 E.g., "Permission denied" on /run/user/<name>/gvfs.  */
            }
        }

      mnt_free = mnt_ent;
      mnt_ent = mnt_ent->me_next;
      free_mount_entry (mnt_free);
    }
}