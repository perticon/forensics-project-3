void main(uint param_1,undefined8 *param_2)

{
  ulong uVar1;
  ulong uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  bool bVar6;
  uint uVar7;
  char cVar8;
  int iVar9;
  uint uVar10;
  char *pcVar11;
  long lVar12;
  undefined8 uVar13;
  long *plVar14;
  ulong *puVar15;
  int *piVar16;
  long *plVar17;
  long lVar18;
  undefined8 uVar19;
  ulong uVar20;
  long lVar21;
  ulong uVar22;
  ulong uVar23;
  long lVar24;
  uint *puVar25;
  ulong uVar26;
  ushort uVar27;
  undefined8 *puVar28;
  long lVar29;
  undefined **ppuVar30;
  undefined1 *puVar31;
  char *pcVar32;
  ulong uVar33;
  long in_FS_OFFSET;
  bool bVar34;
  bool local_158;
  char *local_150;
  uint local_140;
  uint local_134;
  stat *local_120;
  ulong local_108;
  ulong local_100;
  ulong local_f8;
  ulong uStack240;
  undefined *local_e8;
  undefined8 local_e0;
  stat local_d8;
  undefined8 local_40;
  
  pcVar32 = "0abd:chHklmst:xB:DLPSX:";
  puVar25 = &switchD_00103efc::switchdataD_00123560;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_e0 = 0;
  local_e8 = &DAT_001239ef;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar31 = long_options;
  textdomain("coreutils");
  atexit(close_stdout);
  exclude = new_exclude();
  pcVar11 = getenv("DU_BLOCK_SIZE");
  human_options(pcVar11,&human_output_opts,&output_block_size);
  bVar6 = false;
  uVar7 = 0x10;
  local_140 = 8;
  local_150 = (char *)0x0;
  local_158 = true;
  bVar34 = false;
LAB_00103eb8:
  puVar28 = param_2;
  uVar23 = (ulong)param_1;
  pcVar11 = (char *)getopt_long((ulong)param_1,param_2,"0abd:chHklmst:xB:DLPSX:");
  iVar9 = (int)pcVar11;
  if (iVar9 != -1) {
    if (0x87 < iVar9) {
switchD_00103efc_caseD_31:
      local_158 = false;
      goto LAB_00103eb8;
    }
    if (iVar9 < 0x30) {
      if (iVar9 == -0x83) {
        version_etc(stdout,&DAT_0012202b,"GNU coreutils",Version,"Torbjorn Granlund",
                    "David MacKenzie","Paul Eggert","Jim Meyering",0,puVar28);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar9 != -0x82) goto switchD_00103efc_caseD_31;
      usage(0);
      goto LAB_0010504d;
    }
    switch(iVar9) {
    default:
      goto switchD_00103efc_caseD_31;
    case 0x42:
      iVar9 = human_options(optarg,&human_output_opts,&output_block_size);
      if (iVar9 == 0) break;
      xstrtol_fatal(iVar9,0xffffffff,0x42,long_options,optarg);
    case 0x30:
      opt_nul_terminate_output = 1;
      break;
    case 0x44:
    case 0x48:
      uVar7 = 0x11;
      break;
    case 0x4c:
      uVar7 = 2;
      break;
    case 0x50:
      uVar7 = 0x10;
      break;
    case 0x53:
      opt_separate_dirs = '\x01';
      break;
    case 0x58:
      iVar9 = add_exclude_file(add_exclude,exclude,optarg,0x10000000,10);
      if (iVar9 != 0) {
        uVar13 = quotearg_n_style_colon(0,3,optarg);
        piVar16 = __errno_location();
        error(0,*piVar16,&DAT_00123768,uVar13);
        local_158 = false;
      }
      break;
    case 0x61:
      opt_all = '\x01';
      break;
    case 0x62:
      apparent_size = '\x01';
      human_output_opts = 0;
      output_block_size = 1;
      break;
    case 99:
      print_grand_total = 1;
      break;
    case 100:
      iVar9 = xstrtoumax(optarg,0,0,&local_108,"");
      if (iVar9 == 0) {
        max_depth = local_108;
        bVar34 = true;
      }
      else {
        uVar13 = quote();
        uVar19 = dcgettext(0,"invalid maximum depth %s",5);
        error(0,0,uVar19,uVar13);
        local_158 = false;
      }
      break;
    case 0x68:
      human_output_opts = 0xb0;
      output_block_size = 1;
      break;
    case 0x6b:
      human_output_opts = 0;
      output_block_size = 0x400;
      break;
    case 0x6c:
      opt_count_all = '\x01';
      break;
    case 0x6d:
      goto switchD_00103efc_caseD_6d;
    case 0x73:
      bVar6 = true;
      break;
    case 0x74:
      iVar9 = xstrtoimax(optarg,0,0,&opt_threshold,"kKmMGTPEZY0");
      if (iVar9 != 0) goto LAB_00105243;
      if ((opt_threshold == 0) && (*optarg == '-')) {
        uVar13 = dcgettext(0,"invalid --threshold argument \'-0\'",5);
        error(1,0,uVar13);
switchD_00103efc_caseD_6d:
        human_output_opts = 0;
        output_block_size = 0x100000;
      }
      break;
    case 0x78:
      local_140 = 0x48;
      break;
    case 0x80:
      apparent_size = '\x01';
      break;
    case 0x81:
      add_exclude(exclude,optarg,0x10000000);
      break;
    case 0x82:
      local_150 = optarg;
      break;
    case 0x83:
      human_output_opts = 0x90;
      output_block_size = 1;
      break;
    case 0x85:
      opt_time = '\x01';
      iVar9 = 0;
      if (optarg != (char *)0x0) {
        lVar12 = __xargmatch_internal("--time",optarg,time_args,time_types,4,argmatch_die,1,uVar23);
        iVar9 = *(int *)(time_types + lVar12 * 4);
      }
      time_type = iVar9;
      getenv("TZ");
      localtz = tzalloc();
      break;
    case 0x86:
      time_style = optarg;
      break;
    case 0x87:
      goto switchD_00103efc_caseD_87;
    }
    goto LAB_00103eb8;
  }
  if (local_158 == false) goto LAB_0010527e;
  if (opt_all == '\0') {
    if (bVar34) {
      if (bVar6) {
        if (max_depth != 0) {
LAB_0010450a:
          uVar13 = dcgettext(0,"warning: summarizing conflicts with --max-depth=%lu",5);
          error(0,0,uVar13);
          usage(1);
          goto LAB_00104540;
        }
        uVar13 = dcgettext(0,"warning: summarizing is the same as using --max-depth=0",5);
        pcVar11 = (char *)error(0,0,uVar13);
        if (max_depth != 0) goto LAB_0010450a;
LAB_00104ca7:
        max_depth = 0;
      }
    }
    else if (bVar6) goto LAB_00104ca7;
LAB_001042d4:
    if (opt_inodes != '\0') {
      if (apparent_size != '\0') {
        uVar13 = dcgettext(0,"warning: options --apparent-size and -b are ineffective with --inodes"
                           ,5);
        pcVar11 = (char *)error(0,0,uVar13);
      }
      output_block_size = 1;
    }
    if (opt_time != '\0') {
      pcVar32 = time_style;
      if (time_style != (char *)0x0) goto LAB_00104313;
LAB_0010504d:
      pcVar32 = getenv("TIME_STYLE");
      pcVar11 = pcVar32;
      if (pcVar32 == (char *)0x0) {
LAB_00104df2:
        pcVar32 = "long-iso";
        time_style = "long-iso";
      }
      else {
        time_style = pcVar32;
        uVar10 = strcmp(pcVar32,"locale");
        pcVar11 = (char *)(ulong)uVar10;
        if (uVar10 == 0) goto LAB_00104df2;
        if (*pcVar32 == '+') {
          pcVar11 = strchr(pcVar32,10);
          if (pcVar11 != (char *)0x0) {
            *pcVar11 = '\0';
            goto LAB_00104313;
          }
LAB_0010431e:
          time_format = pcVar32 + 1;
          goto LAB_00104329;
        }
        while( true ) {
          pcVar32 = time_style;
          uVar10 = strncmp(time_style,"posix-",6);
          pcVar11 = (char *)(ulong)uVar10;
          if (uVar10 != 0) break;
          time_style = pcVar32 + 6;
        }
LAB_00104313:
        if (*pcVar32 == '+') goto LAB_0010431e;
      }
      lVar12 = __xargmatch_internal
                         ("time style",pcVar32,time_style_args,time_style_types,4,argmatch_die,1,
                          pcVar11);
      iVar9 = *(int *)(time_style_types + lVar12 * 4);
      if (iVar9 == 1) {
        time_format = "%Y-%m-%d %H:%M";
      }
      else if (iVar9 == 2) {
        time_format = "%Y-%m-%d";
      }
      else if (iVar9 == 0) {
        time_format = "%Y-%m-%d %H:%M:%S.%N %z";
      }
    }
LAB_00104329:
    if (local_150 == (char *)0x0) {
      ppuVar30 = &local_e8;
      if (optind < (int)param_1) {
        ppuVar30 = (undefined **)(param_2 + optind);
      }
      pcVar32 = (char *)argv_iter_init_argv(ppuVar30);
      local_158 = optind + 1 < (int)param_1 || uVar7 == 2;
LAB_00104392:
      hash_all = local_158;
      if ((pcVar32 == (char *)0x0) || (di_files = di_set_alloc(), di_files == 0)) {
LAB_00105155:
                    /* WARNING: Subroutine does not return */
        xalloc_die();
      }
      if ((opt_count_all != '\0') || (hash_all == '\0')) {
        local_140 = local_140 | 0x100;
      }
      local_134 = local_140 | uVar7;
      local_120 = &local_d8;
LAB_001043f8:
      while( true ) {
        puVar25 = (uint *)argv_iter(pcVar32);
        if (puVar25 == (uint *)0x0) {
                    /* WARNING: Subroutine does not return */
          __assert_fail("!\"unexpected error code from argv_iter\"","src/du.c",0x439,
                        (char *)&__PRETTY_FUNCTION___4);
        }
        if (local_150 != (char *)0x0) break;
        if (*(char *)puVar25 != '\0') {
LAB_00104549:
          temp_argv_3._0_8_ = puVar25;
          lVar12 = xfts_open(temp_argv_3,local_134,0);
LAB_001045f1:
          plVar14 = (long *)rpl_fts_read();
          if (plVar14 == (long *)0x0) goto LAB_00104c1b;
          lVar24 = plVar14[7];
          uVar27 = *(ushort *)(plVar14 + 0xd);
          if (uVar27 == 4) {
            quotearg_style(4,lVar24);
            uVar13 = dcgettext(0,"cannot read directory %s",5);
            error(0,*(undefined4 *)(plVar14 + 8),uVar13);
          }
          else {
            if (uVar27 == 6) goto LAB_0010462d;
            cVar8 = excluded_file_name();
            if (cVar8 != '\0') {
LAB_001045de:
              if (uVar27 == 1) {
                rpl_fts_set(lVar12,plVar14,4);
                plVar17 = (long *)rpl_fts_read(lVar12);
                if (plVar14 != plVar17) {
                    /* WARNING: Subroutine does not return */
                  __assert_fail("e == ent","src/du.c",0x230,"process_file");
                }
              }
              goto LAB_001045f1;
            }
            if (uVar27 == 0xb) {
              rpl_fts_set(lVar12,plVar14,1);
              plVar17 = (long *)rpl_fts_read();
              if (plVar14 != plVar17) {
                    /* WARNING: Subroutine does not return */
                __assert_fail("e == ent","src/du.c",0x20f,"process_file");
              }
              uVar27 = *(ushort *)(plVar14 + 0xd);
            }
            if (uVar27 == 10 || uVar27 == 0xd) {
              quotearg_style(4,lVar24);
              uVar13 = dcgettext(0,"cannot access %s",5);
              error(0,*(undefined4 *)(plVar14 + 8),uVar13);
              goto LAB_001045f1;
            }
            if ((((*(byte *)(lVar12 + 0x48) & 0x40) != 0) && (0 < plVar14[0xb])) &&
               (*(long *)(lVar12 + 0x18) != plVar14[0xe])) goto LAB_001045de;
            if ((opt_count_all == '\0') &&
               ((hash_all != '\0' ||
                (((*(uint *)(plVar14 + 0x11) & 0xf000) != 0x4000 && (1 < (ulong)plVar14[0x10]))))))
            {
              iVar9 = di_set_insert(di_files,plVar14[0xe],plVar14[0xf]);
              if (iVar9 < 0) goto LAB_00105155;
              if (iVar9 == 0) goto LAB_001045de;
            }
            if (uVar27 == 2) {
              cVar8 = cycle_warning_required(lVar12);
              if (cVar8 != '\0') {
                plVar17 = (long *)*plVar14;
                if (di_mnt == 0) {
                  di_mnt = di_set_alloc();
                  if (di_mnt == 0) goto LAB_00105155;
                  lVar18 = read_file_system_list(0);
                  while (lVar18 != 0) {
                    if ((((*(byte *)(lVar18 + 0x28) & 3) == 0) &&
                        (iVar9 = stat(*(char **)(lVar18 + 8),local_120), iVar9 == 0)) &&
                       (iVar9 = di_set_insert(di_mnt,local_d8.st_dev,local_d8.st_ino), iVar9 < 0))
                    goto LAB_00105155;
                    lVar18 = *(long *)(lVar18 + 0x30);
                    free_mount_entry();
                  }
                }
                for (; plVar14 != (long *)0x0 && plVar17 != plVar14; plVar14 = (long *)plVar14[1]) {
                  iVar9 = di_set_lookup(di_mnt,plVar14[0xe],plVar14[0xf]);
                  if (0 < iVar9) goto LAB_001045f1;
                }
                quotearg_n_style_colon(0,3,lVar24);
                uVar13 = dcgettext(0,
                                   "WARNING: Circular directory structure.\nThis almost certainly means that you have a corrupted file system.\nNOTIFY YOUR SYSTEM MANAGER.\nThe following directory is part of the cycle:\n  %s\n"
                                   ,5);
                error(0,0,uVar13);
              }
              goto LAB_001045f1;
            }
            if (uVar27 == 7) {
              quotearg_n_style_colon(0,3,lVar24);
              error(0,*(undefined4 *)(plVar14 + 8),&DAT_00123768);
              goto LAB_0010462d;
            }
            if (uVar27 == 1) goto LAB_001045f1;
          }
LAB_0010462d:
          if (time_type == 0) {
            uVar23 = plVar14[0x19];
            uVar33 = plVar14[0x1a];
          }
          else if (time_type == 2) {
            uVar23 = plVar14[0x17];
            uVar33 = plVar14[0x18];
          }
          else {
            uVar23 = plVar14[0x1b];
            uVar33 = plVar14[0x1c];
          }
          if (apparent_size == '\0') {
            uVar26 = plVar14[0x16] << 9;
          }
          else {
            uVar26 = plVar14[0x14];
            if (plVar14[0x14] < 0) {
              uVar26 = 0;
            }
          }
          uVar1 = plVar14[0xb];
          local_100 = 1;
          local_108 = uVar26;
          local_f8 = uVar23;
          uStack240 = uVar33;
          if (n_alloc_1 == 0) {
            n_alloc_1 = uVar1 + 10;
            dulvl_0 = xcalloc();
          }
          else if (uVar1 != prev_level) {
            if (prev_level < uVar1) {
              if (n_alloc_1 <= uVar1) {
                dulvl_0 = xreallocarray(dulvl_0,uVar1,0x80);
                n_alloc_1 = uVar1 * 2;
              }
              uVar22 = prev_level + 1;
              if (uVar22 <= uVar1) {
                lVar24 = prev_level * 0x40 + dulvl_0;
                do {
                  uVar22 = uVar22 + 1;
                  *(undefined8 *)(lVar24 + 0x40) = 0;
                  *(undefined8 *)(lVar24 + 0x48) = 0;
                  *(undefined8 *)(lVar24 + 0x50) = 0x8000000000000000;
                  *(undefined8 *)(lVar24 + 0x58) = 0xffffffffffffffff;
                  *(undefined8 *)(lVar24 + 0x60) = 0;
                  *(undefined8 *)(lVar24 + 0x68) = 0;
                  *(undefined8 *)(lVar24 + 0x70) = 0x8000000000000000;
                  *(undefined8 *)(lVar24 + 0x78) = 0xffffffffffffffff;
                  lVar24 = lVar24 + 0x40;
                } while (uVar22 <= uVar1);
              }
            }
            else {
              if (uVar1 != prev_level - 1) goto LAB_00105288;
              puVar15 = (ulong *)(prev_level * 0x40 + dulvl_0);
              local_108 = uVar26 + *puVar15;
              if (CARRY8(uVar26,*puVar15)) {
                local_108 = 0xffffffffffffffff;
              }
              local_100 = puVar15[1] + 1;
              if ((int)(((uint)((long)puVar15[3] < (long)uVar33) -
                        (uint)((long)uVar33 < (long)puVar15[3])) +
                       ((uint)((long)puVar15[2] < (long)uVar23) -
                       (uint)((long)uVar23 < (long)puVar15[2])) * 2) < 0) {
                local_f8 = puVar15[2];
                uStack240 = puVar15[3];
              }
              if (opt_separate_dirs == '\0') {
                bVar34 = CARRY8(local_108,puVar15[4]);
                local_108 = local_108 + puVar15[4];
                if (bVar34) {
                  local_108 = 0xffffffffffffffff;
                }
                local_100 = local_100 + puVar15[5];
                if ((int)(((uint)((long)puVar15[7] < (long)uStack240) -
                          (uint)((long)uStack240 < (long)puVar15[7])) +
                         ((uint)((long)puVar15[6] < (long)local_f8) -
                         (uint)((long)local_f8 < (long)puVar15[6])) * 2) < 0) {
                  local_f8 = puVar15[6];
                  uStack240 = puVar15[7];
                }
              }
              lVar24 = dulvl_0 + uVar1 * 0x40;
              uVar20 = *puVar15 + *(ulong *)(lVar24 + 0x20);
              uVar22 = *(ulong *)(lVar24 + 0x30);
              if (CARRY8(*puVar15,*(ulong *)(lVar24 + 0x20))) {
                uVar20 = 0xffffffffffffffff;
              }
              lVar29 = puVar15[1] + *(long *)(lVar24 + 0x28);
              uVar2 = puVar15[2];
              lVar18 = *(long *)(lVar24 + 0x38);
              *(long *)(lVar24 + 0x28) = lVar29;
              *(ulong *)(lVar24 + 0x20) = uVar20;
              if ((int)(((uint)((long)puVar15[3] < lVar18) - (uint)(lVar18 < (long)puVar15[3])) +
                       ((uint)((long)uVar2 < (long)uVar22) - (uint)((long)uVar22 < (long)uVar2)) * 2
                       ) < 0) {
                uVar3 = *(undefined4 *)((long)puVar15 + 0x14);
                uVar4 = *(undefined4 *)(puVar15 + 3);
                uVar5 = *(undefined4 *)((long)puVar15 + 0x1c);
                uVar22 = puVar15[2];
                *(undefined4 *)(lVar24 + 0x30) = *(undefined4 *)(puVar15 + 2);
                *(undefined4 *)(lVar24 + 0x34) = uVar3;
                *(undefined4 *)(lVar24 + 0x38) = uVar4;
                *(undefined4 *)(lVar24 + 0x3c) = uVar5;
                lVar18 = *(long *)(lVar24 + 0x38);
              }
              lVar21 = uVar20 + puVar15[4];
              if (CARRY8(uVar20,puVar15[4])) {
                lVar21 = -1;
              }
              *(ulong *)(lVar24 + 0x28) = lVar29 + puVar15[5];
              uVar20 = puVar15[6];
              *(long *)(lVar24 + 0x20) = lVar21;
              if ((int)(((uint)((long)puVar15[7] < lVar18) - (uint)(lVar18 < (long)puVar15[7])) +
                       ((uint)((long)uVar20 < (long)uVar22) - (uint)((long)uVar22 < (long)uVar20)) *
                       2) < 0) {
                uVar3 = *(undefined4 *)((long)puVar15 + 0x34);
                uVar4 = *(undefined4 *)(puVar15 + 7);
                uVar5 = *(undefined4 *)((long)puVar15 + 0x3c);
                *(undefined4 *)(lVar24 + 0x30) = *(undefined4 *)(puVar15 + 6);
                *(undefined4 *)(lVar24 + 0x34) = uVar3;
                *(undefined4 *)(lVar24 + 0x38) = uVar4;
                *(undefined4 *)(lVar24 + 0x3c) = uVar5;
              }
            }
          }
          prev_level = uVar1;
          if ((opt_separate_dirs == '\0') || ((uVar27 & 0xfffd) != 4)) {
            puVar15 = (ulong *)(uVar1 * 0x40 + dulvl_0);
            uVar22 = uVar26 + *puVar15;
            if (CARRY8(uVar26,*puVar15)) {
              uVar22 = 0xffffffffffffffff;
            }
            puVar15[1] = puVar15[1] + 1;
            *puVar15 = uVar22;
            if ((int)(((uint)((long)uVar33 < (long)puVar15[3]) -
                      (uint)((long)puVar15[3] < (long)uVar33)) +
                     ((uint)((long)uVar23 < (long)puVar15[2]) -
                     (uint)((long)puVar15[2] < (long)uVar23)) * 2) < 0) {
              puVar15[2] = uVar23;
              puVar15[3] = uVar33;
            }
          }
          bVar34 = CARRY8(uVar26,tot_dui._0_8_);
          tot_dui._0_8_ = uVar26 + tot_dui._0_8_;
          if (bVar34) {
            tot_dui._0_8_ = 0xffffffffffffffff;
          }
          tot_dui._8_8_ = tot_dui._8_8_ + 1;
          if ((int)(((uint)((long)uVar33 < (long)tot_dui._24_8_) -
                    (uint)((long)tot_dui._24_8_ < (long)uVar33)) +
                   ((uint)((long)uVar23 < (long)tot_dui._16_8_) -
                   (uint)((long)tot_dui._16_8_ < (long)uVar23)) * 2) < 0) {
            tot_dui._16_8_ = uVar23;
            tot_dui._24_8_ = uVar33;
          }
          if (((((uVar27 & 0xfffd) == 4) || (opt_all != '\0')) && (uVar1 <= max_depth)) ||
             (uVar1 == 0)) {
            uVar23 = local_100;
            if (opt_inodes == '\0') {
              uVar23 = local_108;
            }
            bVar34 = opt_threshold <= uVar23;
            if ((long)opt_threshold < 0) {
              bVar34 = uVar23 <= -opt_threshold;
            }
            if (bVar34) {
              print_size(&local_108);
            }
          }
          goto LAB_001045f1;
        }
        dcgettext(0,"invalid zero-length file name",5);
        error(0,0,&DAT_00123768);
      }
      if ((((*local_150 == '-') && (local_150[1] == '\0')) && (*(char *)puVar25 == '-')) &&
         (*(char *)((long)puVar25 + 1) == '\0')) goto code_r0x00104446;
LAB_00104540:
      if (*(char *)puVar25 != '\0') goto LAB_00104549;
      goto LAB_00104480;
    }
    if (optind < (int)param_1) {
      uVar13 = quote(param_2[optind]);
      uVar19 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar19,uVar13);
      uVar13 = dcgettext(0,"file operands cannot be combined with --files0-from",5);
      __fprintf_chk(stderr,1,"%s\n",uVar13);
      usage(1);
    }
    else {
      iVar9 = strcmp(local_150,"-");
      if ((iVar9 == 0) || (lVar12 = freopen_safer(local_150,"r",stdin), lVar12 != 0)) {
        pcVar32 = (char *)argv_iter_init_stream(stdin);
        goto LAB_00104392;
      }
    }
    puVar31 = (undefined1 *)quotearg_style(4,local_150);
    uVar13 = dcgettext(0,"cannot open %s for reading",5);
    piVar16 = __errno_location();
    iVar9 = error(1,*piVar16,uVar13,puVar31);
LAB_00105243:
    xstrtol_fatal(iVar9,0xffffffff,0x74,puVar31,optarg);
  }
  else if (!bVar6) goto LAB_001042d4;
  uVar13 = dcgettext(0,"cannot both summarize and show all entries",5);
  error(0,0,uVar13);
LAB_0010527e:
  usage(1);
LAB_00105288:
                    /* WARNING: Subroutine does not return */
  __assert_fail("level == prev_level - 1","src/du.c",0x27e,"process_file");
switchD_00103efc_caseD_87:
  opt_inodes = '\x01';
  goto LAB_00103eb8;
code_r0x00104446:
  quotearg_style(4,puVar25);
  uVar13 = dcgettext(0,"when reading file names from stdin, no file name of %s allowed",5);
  error(0,0,uVar13);
  if (*(char *)puVar25 == '\0') {
LAB_00104480:
    argv_iter_n_args(pcVar32);
    dcgettext(0,"invalid zero-length file name",5);
    quotearg_n_style_colon(0,3,local_150);
    error(0,0,"%s:%lu: %s");
  }
  goto LAB_001043f8;
LAB_00104c1b:
  piVar16 = __errno_location();
  if (*piVar16 != 0) {
    quotearg_n_style_colon(0,3,*(undefined8 *)(lVar12 + 0x20));
    uVar13 = dcgettext(0,"fts_read failed: %s",5);
    error(0,*piVar16,uVar13);
  }
  prev_level = 0;
  iVar9 = rpl_fts_close(lVar12);
  if (iVar9 != 0) {
    uVar13 = dcgettext(0,"fts_close failed",5);
    error(0,*piVar16,uVar13);
  }
  goto LAB_001043f8;
}