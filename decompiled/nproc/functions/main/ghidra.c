undefined8 main(int param_1,undefined8 *param_2)

{
  int iVar1;
  undefined8 uVar2;
  ulong uVar3;
  long lVar4;
  undefined8 uVar5;
  ulong local_40;
  
  uVar5 = 2;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_40 = 0;
LAB_0010273a:
  do {
    iVar1 = getopt_long(param_1,param_2,"",longopts,0);
    if (iVar1 == -1) {
      if (optind == param_1) {
        uVar3 = num_processors(uVar5);
        lVar4 = 1;
        if (local_40 < uVar3) {
          lVar4 = uVar3 - local_40;
        }
        __printf_chk(1,&DAT_0010710e,lVar4);
        return 0;
      }
      uVar5 = quote(param_2[optind]);
      uVar2 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar2,uVar5);
LAB_0010287c:
                    /* WARNING: Subroutine does not return */
      usage(1);
    }
    if (iVar1 != 0x80) {
      if (iVar1 < 0x81) {
        if (iVar1 == -0x83) {
          version_etc(stdout,"nproc","GNU coreutils",Version,"Giuseppe Scrivano",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar1 == -0x82) {
                    /* WARNING: Subroutine does not return */
          usage(0);
        }
        goto LAB_0010287c;
      }
      if (iVar1 != 0x81) goto LAB_0010287c;
      uVar2 = dcgettext(0,"invalid number",5);
      local_40 = xdectoumax(optarg,0,0xffffffffffffffff,"",uVar2,0);
      goto LAB_0010273a;
    }
    uVar5 = 0;
  } while( true );
}