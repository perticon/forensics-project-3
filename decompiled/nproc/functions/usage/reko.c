void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002630(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000029DE;
	}
	fn00000000000025A0(fn0000000000002420(0x05, "Usage: %s [OPTION]...\n", null), 0x01);
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "Print the number of processing units available to the current process,\nwhich may be less than the number of online processors\n\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --all      print the number of installed processors\n      --ignore=N  if possible, exclude N processing units\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	struct Eq_643 * rbx_152 = fp - 0xB8 + 16;
	do
	{
		char * rsi_154 = rbx_152->qw0000;
		++rbx_152;
	} while (rsi_154 != null && fn0000000000002500(rsi_154, "nproc") != 0x00);
	ptr64 r13_167 = rbx_152->qw0008;
	if (r13_167 != 0x00)
	{
		fn00000000000025A0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_254 = fn0000000000002590(null, 0x05);
		if (rax_254 == 0x00 || fn00000000000023A0(0x03, "en_", rax_254) == 0x00)
			goto l0000000000002BB6;
	}
	else
	{
		fn00000000000025A0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_196 = fn0000000000002590(null, 0x05);
		if (rax_196 == 0x00 || fn00000000000023A0(0x03, "en_", rax_196) == 0x00)
		{
			fn00000000000025A0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002BF3:
			fn00000000000025A0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000029DE:
			fn0000000000002610(edi);
		}
		r13_167 = 0x7004;
	}
	fn00000000000024E0(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002BB6:
	fn00000000000025A0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002BF3;
}