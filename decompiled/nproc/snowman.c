
struct s0 {
    signed char f0;
    signed char f1;
};

struct s1 {
    struct s1* f0;
    unsigned char f1;
    signed char f2;
    signed char[5] pad8;
    struct s1* f8;
    signed char[7] pad16;
    struct s1* f10;
    signed char[7] pad24;
    struct s1* f18;
    signed char[7] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

struct s1* g28;

uint64_t fun_25f0();

unsigned char __cxa_finalize;

void fun_2450();

/* parse_omp_threads.part.0 */
uint64_t parse_omp_threads_part_0(struct s0* rdi) {
    struct s1* rax2;
    struct s1* v3;
    int32_t eax4;
    uint64_t rax5;
    uint32_t ecx6;
    unsigned char* rdx7;
    void* rdx8;

    rax2 = g28;
    v3 = rax2;
    eax4 = rdi->f0;
    if (!*reinterpret_cast<signed char*>(&eax4)) 
        goto addr_2dd3_2;
    if (*reinterpret_cast<signed char*>(&eax4) <= 13) 
        goto addr_2dcf_4;
    while (*reinterpret_cast<signed char*>(&eax4) == 32) {
        do {
            eax4 = rdi->f1;
            rdi = reinterpret_cast<struct s0*>(&rdi->f1);
            if (!*reinterpret_cast<signed char*>(&eax4)) 
                goto addr_2dd3_2;
            if (*reinterpret_cast<signed char*>(&eax4) > 13) 
                break;
            addr_2dcf_4:
        } while (*reinterpret_cast<signed char*>(&eax4) > 8);
        goto addr_2dd3_2;
    }
    if (eax4 - 48 > 9 || (rax5 = fun_25f0(), 1)) {
        addr_2dd3_2:
        *reinterpret_cast<int32_t*>(&rax5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        goto addr_2dd5_9;
    } else {
        ecx6 = __cxa_finalize;
        if (!*reinterpret_cast<signed char*>(&ecx6)) 
            goto addr_2dd5_9;
        rdx7 = reinterpret_cast<unsigned char*>(1);
        if (*reinterpret_cast<signed char*>(&ecx6) <= 13) 
            goto addr_2e3a_12;
        while (*reinterpret_cast<signed char*>(&ecx6) == 32) {
            do {
                ecx6 = *rdx7;
                ++rdx7;
                if (!*reinterpret_cast<signed char*>(&ecx6)) 
                    goto addr_2dd5_9;
                if (*reinterpret_cast<signed char*>(&ecx6) > 13) 
                    break;
                addr_2e3a_12:
            } while (*reinterpret_cast<signed char*>(&ecx6) > 8);
            goto addr_2dd3_2;
        }
    }
    if (*reinterpret_cast<signed char*>(&ecx6) == 44) {
        addr_2dd5_9:
        rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v3) - reinterpret_cast<unsigned char>(g28));
        if (rdx8) {
            fun_2450();
        } else {
            return rax5;
        }
    } else {
        goto addr_2dd3_2;
    }
}

int64_t fun_2430();

int64_t fun_2380(struct s1* rdi, ...);

struct s1* quotearg_buffer_restyled(struct s1* rdi, struct s1* rsi, struct s1* rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s1* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x7460 + rax11 * 4) + 0x7460;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    struct s1* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_2390(struct s1* rdi);

struct s1* slotvec = reinterpret_cast<struct s1*>(0x70);

uint32_t nslots = 1;

struct s1* xpalloc();

void fun_24c0();

struct s3 {
    struct s1* f0;
    signed char[7] pad8;
    struct s1* f8;
};

void fun_2370(struct s1* rdi);

struct s1* xcharalloc(struct s1* rdi, ...);

struct s1* quotearg_n_options(struct s1* rdi, struct s1* rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    struct s1* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s1* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s1* rax12;
    struct s1* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    struct s1* rsi18;
    struct s1* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s1* rax23;
    struct s1* rsi24;
    struct s1* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x463f;
    rax8 = fun_2390(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6891]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x46cb;
            fun_24c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s1*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb100) {
                fun_2370(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x475a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s1* gettext_quote_part_0(struct s1* rdi, int32_t esi, struct s1* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s1* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s1* rax10;
    struct s1* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s1*>(0x73f3);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s1**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s1*>(0x73ec);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s1*>(0x73f7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s1**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s1*>(0x73e8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s1*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s1*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2353() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2363() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2373() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_23e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_23f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t __sched_cpucount = 0x20d0;

void fun_2403() {
    __asm__("cli ");
    goto __sched_cpucount;
}

int64_t bindtextdomain = 0x20e0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2463() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2473() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2150;

void fun_2483() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2160;

void fun_2493() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_24a3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2180;

void fun_24b3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2190;

void fun_24c3() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x21a0;

void fun_24d3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24e3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24f3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_2503() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_2513() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21f0;

void fun_2523() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2200;

void fun_2533() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2210;

void fun_2543() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2220;

void fun_2553() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2230;

void fun_2563() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2240;

void fun_2573() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2250;

void fun_2583() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2260;

void fun_2593() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2270;

void fun_25a3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2280;

void fun_25b3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2290;

void fun_25c3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t sysconf = 0x22a0;

void fun_25d3() {
    __asm__("cli ");
    goto sysconf;
}

int64_t strtoumax = 0x22b0;

void fun_25e3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t strtoul = 0x22c0;

void fun_25f3() {
    __asm__("cli ");
    goto strtoul;
}

int64_t __cxa_atexit = 0x22d0;

void fun_2603() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22e0;

void fun_2613() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22f0;

void fun_2623() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2300;

void fun_2633() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t sched_getaffinity = 0x2310;

void fun_2643() {
    __asm__("cli ");
    goto sched_getaffinity;
}

int64_t mbsinit = 0x2320;

void fun_2653() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2330;

void fun_2663() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2340;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s1* rdi);

struct s1* fun_2590(int64_t rdi, ...);

void fun_2410(int64_t rdi, int64_t rsi);

void fun_23e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2460(int64_t rdi, struct s1** rsi, int64_t rdx, int64_t rcx);

void usage();

struct s1* stdout = reinterpret_cast<struct s1*>(0);

int64_t Version = 0x7363;

void version_etc(struct s1* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

int32_t fun_2610();

struct s1* fun_2420();

int64_t optarg = 0;

struct s1* xdectoumax(int64_t rdi);

int32_t optind = 0;

int64_t quote(struct s1* rdi, ...);

void fun_25b0();

struct s1* num_processors(int64_t rdi, struct s1** rsi, int64_t rdx, int64_t rcx);

void fun_25a0(int64_t rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

int64_t fun_26c3(int32_t edi, struct s1** rsi) {
    int32_t r14d3;
    int32_t ebp4;
    struct s1** rbx5;
    struct s1* rdi6;
    struct s1* v7;
    int64_t rdi8;
    int32_t eax9;
    struct s1* rdi10;
    int64_t rcx11;
    int64_t rdi12;
    struct s1* rax13;
    int64_t rax14;
    struct s1* rdi15;
    int64_t rdi16;
    struct s1* rax17;
    struct s1* rdx18;

    __asm__("cli ");
    r14d3 = 2;
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *rsi;
    set_program_name(rdi6);
    fun_2590(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    fun_23e0("coreutils", "/usr/local/share/locale");
    atexit(0x2d00, "/usr/local/share/locale");
    v7 = reinterpret_cast<struct s1*>(0);
    while (*reinterpret_cast<int32_t*>(&rdi8) = ebp4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0, eax9 = fun_2460(rdi8, rbx5, 0x7020, 0xab40), eax9 != -1) {
        if (eax9 == 0x80) {
            addr_27f8_4:
            r14d3 = 0;
            continue;
        } else {
            if (eax9 > 0x80) {
                addr_27a0_6:
                if (eax9 != 0x81) 
                    goto addr_287c_7;
            } else {
                if (eax9 != 0xffffff7d) {
                    if (eax9 != 0xffffff7e) 
                        goto addr_287c_7;
                    usage();
                    goto addr_27f8_4;
                } else {
                    rdi10 = stdout;
                    rcx11 = Version;
                    version_etc(rdi10, "nproc", "GNU coreutils", rcx11, "Giuseppe Scrivano");
                    eax9 = fun_2610();
                    goto addr_27a0_6;
                }
            }
        }
        fun_2420();
        rdi12 = optarg;
        rax13 = xdectoumax(rdi12);
        v7 = rax13;
    }
    rax14 = optind;
    if (*reinterpret_cast<int32_t*>(&rax14) != ebp4) {
        rdi15 = rbx5[rax14 * 8];
        quote(rdi15, rdi15);
        fun_2420();
        fun_25b0();
    } else {
        *reinterpret_cast<int32_t*>(&rdi16) = r14d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
        rax17 = num_processors(rdi16, rbx5, 0x7020, 0xab40);
        *reinterpret_cast<int32_t*>(&rdx18) = 1;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        if (reinterpret_cast<unsigned char>(v7) < reinterpret_cast<unsigned char>(rax17)) {
            rdx18 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rax17) - reinterpret_cast<unsigned char>(v7));
        }
        fun_25a0(1, "%lu\n", rdx18, v7);
        return 0;
    }
    addr_287c_7:
    usage();
}

int64_t __libc_start_main = 0;

void fun_2893() {
    __asm__("cli ");
    __libc_start_main(0x26c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2350(int64_t rdi);

int64_t fun_2933() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2350(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2973() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s1* program_name = reinterpret_cast<struct s1*>(0);

void fun_24e0(struct s1* rdi, struct s1* rsi, int64_t rdx, struct s1* rcx);

int32_t fun_2500(int64_t rdi);

int32_t fun_23a0(struct s1* rdi, int64_t rsi, int64_t rdx, struct s1* rcx);

struct s1* stderr = reinterpret_cast<struct s1*>(0);

void fun_2630(struct s1* rdi, int64_t rsi, struct s1* rdx, struct s1* rcx, struct s1* r8, struct s1* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2983(int32_t edi) {
    struct s1* r12_2;
    struct s1* rax3;
    struct s1* v4;
    struct s1* rax5;
    struct s1* rcx6;
    struct s1* r12_7;
    struct s1* rax8;
    struct s1* r12_9;
    struct s1* rax10;
    struct s1* r12_11;
    struct s1* rax12;
    struct s1* r12_13;
    struct s1* rax14;
    int32_t eax15;
    struct s1* r13_16;
    struct s1* rax17;
    struct s1* rax18;
    int32_t eax19;
    struct s1* rax20;
    struct s1* rax21;
    struct s1* rax22;
    int32_t eax23;
    struct s1* rax24;
    struct s1* r15_25;
    struct s1* rax26;
    struct s1* rax27;
    struct s1* rax28;
    struct s1* rdi29;
    struct s1* r8_30;
    struct s1* r9_31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2420();
            fun_25a0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2420();
            fun_24e0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2420();
            fun_24e0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2420();
            fun_24e0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2420();
            fun_24e0(rax14, r12_13, 5, rcx6);
            do {
                if (1) 
                    break;
                eax15 = fun_2500("nproc");
            } while (eax15);
            r13_16 = v4;
            if (!r13_16) {
                rax17 = fun_2420();
                fun_25a0(1, rax17, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax18 = fun_2590(5);
                if (!rax18 || (eax19 = fun_23a0(rax18, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax19)) {
                    rax20 = fun_2420();
                    r13_16 = reinterpret_cast<struct s1*>("nproc");
                    fun_25a0(1, rax20, "https://www.gnu.org/software/coreutils/", "nproc");
                    r12_2 = reinterpret_cast<struct s1*>(" invocation");
                } else {
                    r13_16 = reinterpret_cast<struct s1*>("nproc");
                    goto addr_2cb0_9;
                }
            } else {
                rax21 = fun_2420();
                fun_25a0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_2590(5);
                if (!rax22 || (eax23 = fun_23a0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    addr_2bb6_11:
                    rax24 = fun_2420();
                    fun_25a0(1, rax24, "https://www.gnu.org/software/coreutils/", "nproc");
                    r12_2 = reinterpret_cast<struct s1*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_16 == "nproc")) {
                        r12_2 = reinterpret_cast<struct s1*>(0x7020);
                    }
                } else {
                    addr_2cb0_9:
                    r15_25 = stdout;
                    rax26 = fun_2420();
                    fun_24e0(rax26, r15_25, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2bb6_11;
                }
            }
            rax27 = fun_2420();
            rcx6 = r12_2;
            fun_25a0(1, rax27, r13_16, rcx6);
            addr_29de_14:
            fun_2610();
        }
    } else {
        rax28 = fun_2420();
        rdi29 = stderr;
        rcx6 = r12_2;
        fun_2630(rdi29, 1, rax28, rcx6, r8_30, r9_31, v32, v33, v34, v35, v36, v37);
        goto addr_29de_14;
    }
}

int64_t file_name = 0;

void fun_2ce3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_2cf3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s1* rdi);

struct s1* quotearg_colon();

int32_t exit_failure = 1;

struct s1* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s1* rcx, struct s1* r8);

void fun_2d03() {
    struct s1* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s1* rdi6;
    int32_t eax7;
    struct s1* rax8;
    int64_t rdi9;
    struct s1* rax10;
    int64_t rsi11;
    struct s1* r8_12;
    struct s1* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_2d93_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25b0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_2d93_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25b0();
    }
}

struct s0* fun_2360(int64_t rdi);

int32_t fun_2640();

int32_t fun_2400(int64_t rdi, void* rsi, void* rdx);

uint64_t fun_25d0(int64_t rdi, void* rsi, void* rdx);

uint64_t fun_2e73(int32_t edi, void* rsi, void* rdx) {
    void* rsp4;
    struct s1* rax5;
    struct s0* rax6;
    void* rsp7;
    struct s0* rax8;
    uint64_t rax9;
    uint64_t r12_10;
    void* rbp11;
    void* rsi12;
    int32_t eax13;
    int32_t eax14;
    uint64_t rax15;
    uint64_t rax16;
    uint64_t rax17;
    struct s0* rax18;
    uint64_t rax19;
    uint64_t rax20;
    uint64_t rbx21;
    void* rbp22;
    int32_t eax23;
    int32_t eax24;
    uint64_t rax25;
    void* rax26;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x90);
    rax5 = g28;
    if (edi == 2) {
        rax6 = fun_2360("OMP_NUM_THREADS");
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        if (!rax6) {
            rax8 = fun_2360("OMP_THREAD_LIMIT");
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            if (!rax8 || (rax9 = parse_omp_threads_part_0(rax8), rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8), r12_10 = rax9, !rax9)) {
                addr_3035_4:
                r12_10 = 0xffffffffffffffff;
                goto addr_2f4f_5;
            } else {
                addr_2f4f_5:
                rbp11 = rsp4;
                *reinterpret_cast<int32_t*>(&rsi12) = 0x80;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi12) + 4) = 0;
                eax13 = fun_2640();
                if (eax13 || (rsi12 = rbp11, eax14 = fun_2400(0x80, rsi12, rbp11), rax15 = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax14)), rax15 == 0)) {
                    rax16 = fun_25d0(84, rsi12, rbp11);
                    if (rax16 <= r12_10) {
                        r12_10 = rax16;
                    }
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rax16) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rax16 == 0)) {
                        r12_10 = 1;
                    }
                    goto addr_2ecb_11;
                } else {
                    if (r12_10 > rax15) {
                        r12_10 = rax15;
                    }
                    goto addr_2ecb_11;
                }
            }
        } else {
            rax17 = parse_omp_threads_part_0(rax6);
            rax18 = fun_2360("OMP_THREAD_LIMIT");
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
            if (!rax18) {
                r12_10 = 0xffffffffffffffff;
            } else {
                rax19 = parse_omp_threads_part_0(rax18);
                rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
                r12_10 = rax19;
                if (!rax19) {
                    r12_10 = 0xffffffffffffffff;
                }
            }
            if (!rax17) 
                goto addr_2f4f_5;
            if (r12_10 > rax17) {
                r12_10 = rax17;
            }
            goto addr_2ecb_11;
        }
    }
    if (edi == 1) 
        goto addr_3035_4;
    rax20 = fun_25d0(83, rsi, rdx);
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
    rbx21 = rax20;
    r12_10 = rax20;
    if (rax20 + 0xffffffffffffffff > 1) 
        goto addr_2ebe_25;
    rbp22 = rsp4;
    eax23 = fun_2640();
    if (eax23 || ((eax24 = fun_2400(0x80, rbp22, rbp22), rax25 = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax24)), rax25 == 0) || rbx21 >= rax25)) {
        addr_2ecb_11:
        rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax26) {
            fun_2450();
        } else {
            return r12_10;
        }
    } else {
        rbx21 = rax25;
    }
    addr_2ebe_25:
    *reinterpret_cast<int32_t*>(&r12_10) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rbx21) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rbx21 == 0))) {
        r12_10 = rbx21;
        goto addr_2ecb_11;
    }
}

void fun_2620(struct s1* rdi, int64_t rsi, int64_t rdx, struct s1* rcx);

struct s5 {
    signed char[1] pad1;
    struct s1* f1;
    signed char[2] pad4;
    struct s1* f4;
};

struct s5* fun_2490();

struct s1* __progname = reinterpret_cast<struct s1*>(0);

struct s1* __progname_full = reinterpret_cast<struct s1*>(0);

void fun_3063(struct s1* rdi) {
    struct s1* rcx2;
    struct s1* rbx3;
    struct s5* rax4;
    struct s1* r12_5;
    struct s1* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2620("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2490();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s1*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s1**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s1*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(struct s1* rdi, int64_t rsi);

void fun_4803(struct s1* rdi) {
    struct s1* rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<struct s1*>(0xb200);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4843(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4863(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *rdi = esi;
    return 0xb200;
}

int64_t fun_4883(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_48c3(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_48e3(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x268a;
    if (!rdx) 
        goto 0x268a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb200;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    struct s1* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* fun_4923(struct s1* rdi, struct s1* rsi, struct s1* rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s1* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xb200);
    }
    rax7 = fun_2390(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x4956);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    struct s1* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s1* fun_49a3(struct s1* rdi, int64_t rsi, struct s1** rdx, struct s9* rcx) {
    struct s9* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s1* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s1* rax14;
    struct s1* rsi15;
    struct s1* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xb200);
    }
    rax6 = fun_2390(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s1*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x49d1);
    rsi15 = reinterpret_cast<struct s1*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4a2c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4a93() {
    __asm__("cli ");
}

struct s1* gb078 = reinterpret_cast<struct s1*>(0);

int64_t slotvec0 = 0x100;

void fun_4aa3() {
    uint32_t eax1;
    struct s1* r12_2;
    uint64_t rax3;
    struct s1** rbx4;
    struct s1** rbp5;
    struct s1* rdi6;
    struct s1* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s1**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2370(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xb100) {
        fun_2370(rdi7);
        gb078 = reinterpret_cast<struct s1*>(0xb100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_2370(r12_2);
        slotvec = reinterpret_cast<struct s1*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4b43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4b63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4b73(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4b93(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s1* fun_4bb3(struct s1* rdi, int32_t esi, struct s1* rdx) {
    struct s1* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2690;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s1* fun_4c43(struct s1* rdi, int32_t esi, struct s1* rdx, int64_t rcx) {
    struct s1* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2695;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s1* fun_4cd3(int32_t edi, struct s1* rsi) {
    struct s1* rax3;
    struct s2* rcx4;
    struct s1* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x269a;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

struct s1* fun_4d63(int32_t edi, struct s1* rsi, int64_t rdx) {
    struct s1* rax4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x269f;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s1* fun_4df3(struct s1* rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    struct s1* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s1* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6400]");
    __asm__("movdqa xmm1, [rip+0x6408]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x63f1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

struct s1* fun_4e93(struct s1* rdi, uint32_t esi) {
    struct s2* rsp3;
    struct s1* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6360]");
    __asm__("movdqa xmm1, [rip+0x6368]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6351]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s1* fun_4f33(struct s1* rdi) {
    struct s1* rax2;
    struct s1* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x62c0]");
    __asm__("movdqa xmm1, [rip+0x62c8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x62a9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s1* fun_4fc3(struct s1* rdi, int64_t rsi) {
    struct s1* rax3;
    struct s1* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6230]");
    __asm__("movdqa xmm1, [rip+0x6238]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6226]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

struct s1* fun_5053(struct s1* rdi, int32_t esi, struct s1* rdx) {
    struct s1* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26a4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s1* fun_50f3(struct s1* rdi, int64_t rsi, int64_t rdx, struct s1* rcx) {
    struct s1* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x60fa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x60f2]");
    __asm__("movdqa xmm2, [rip+0x60fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26a9;
    if (!rdx) 
        goto 0x26a9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s1* fun_5193(int32_t edi, int64_t rsi, int64_t rdx, struct s1* rcx, int64_t r8) {
    struct s1* rcx6;
    struct s2* rcx7;
    struct s1* rdi8;
    struct s1* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x605a]");
    __asm__("movdqa xmm1, [rip+0x6062]");
    __asm__("movdqa xmm2, [rip+0x606a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ae;
    if (!rdx) 
        goto 0x26ae;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s1* fun_5243(int64_t rdi, int64_t rsi, struct s1* rdx) {
    struct s1* rdx4;
    struct s2* rcx5;
    struct s1* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5faa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5fa2]");
    __asm__("movdqa xmm2, [rip+0x5faa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b3;
    if (!rsi) 
        goto 0x26b3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s1* fun_52e3(int64_t rdi, int64_t rsi, struct s1* rdx, int64_t rcx) {
    struct s1* rcx5;
    struct s2* rcx6;
    struct s1* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5f0a]");
    __asm__("movdqa xmm1, [rip+0x5f12]");
    __asm__("movdqa xmm2, [rip+0x5f1a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b8;
    if (!rsi) 
        goto 0x26b8;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_5383() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5393(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    struct s1* f0;
    signed char[7] pad8;
    struct s1* f8;
    signed char[7] pad16;
    struct s1* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2510(int64_t rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx, struct s1* r8, struct s1* r9);

void fun_53f3(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx, struct s10* r8, struct s1* r9) {
    struct s1* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s1* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s1* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s1* r14_40;
    struct s1* r13_41;
    struct s1* r12_42;
    struct s1* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2630(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2630(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2420();
    fun_2630(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2510(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2420();
    fun_2630(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2510(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2420();
        fun_2630(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7ac8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7ac8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5863() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5883(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s1* rax9;
    struct s1* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2450();
    } else {
        return;
    }
}

void fun_5923(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s1* rax15;
    struct s1* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_59c6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_59d0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        return;
    }
    addr_59c6_5:
    goto addr_59d0_7;
}

void fun_5a03() {
    struct s1* rsi1;
    struct s1* rdx2;
    struct s1* rcx3;
    struct s1* r8_4;
    struct s1* r9_5;
    struct s1* rax6;
    struct s1* rcx7;
    struct s1* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2510(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2420();
    fun_25a0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2420();
    fun_25a0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2420();
    goto fun_25a0;
}

int64_t fun_23d0();

void xalloc_die();

void fun_5aa3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2540(unsigned char* rdi);

void fun_5ae3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5b03(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5b23(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2580();

void fun_5b43(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2580();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5b73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2580();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ba3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5be3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5c23(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5c53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ca3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23d0();
            if (rax5) 
                break;
            addr_5ced_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_5ced_5;
        rax8 = fun_23d0();
        if (rax8) 
            goto addr_5cd6_9;
        if (rbx4) 
            goto addr_5ced_5;
        addr_5cd6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_5d33(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23d0();
            if (rax8) 
                break;
            addr_5d7a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_5d7a_5;
        rax11 = fun_23d0();
        if (rax11) 
            goto addr_5d62_9;
        if (!rbx6) 
            goto addr_5d62_9;
        if (r12_4) 
            goto addr_5d7a_5;
        addr_5d62_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_5dc3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_5e6d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_5e80_10:
                *r12_8 = 0;
            }
            addr_5e20_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_5e46_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_5e94_14;
            if (rcx10 <= rsi9) 
                goto addr_5e3d_16;
            if (rsi9 >= 0) 
                goto addr_5e94_14;
            addr_5e3d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_5e94_14;
            addr_5e46_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2580();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_5e94_14;
            if (!rbp13) 
                break;
            addr_5e94_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_5e6d_9;
        } else {
            if (!r13_6) 
                goto addr_5e80_10;
            goto addr_5e20_11;
        }
    }
}

int64_t fun_24f0();

void fun_5ec3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ef3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5f23() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5f43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2520(signed char* rdi, struct s1* rsi, unsigned char* rdx);

void fun_5f63(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_5fa3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

struct s12 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_5fe3(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2540(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2520;
    }
}

struct s1* fun_2440(struct s1* rdi, ...);

void fun_6023(struct s1* rdi) {
    struct s1* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2440(rdi);
    rax3 = fun_2540(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_6063() {
    struct s1* rdi1;

    __asm__("cli ");
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25b0();
    fun_2380(rdi1);
}

int32_t xstrtoumax();

uint64_t fun_60a3(struct s1* rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    struct s1* rax5;
    int32_t eax6;
    int32_t* rax7;
    int32_t* r12_8;
    uint64_t v9;
    void* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax();
    if (eax6) {
        rax7 = fun_2390(rdi);
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_6140_3:
            *r12_8 = 75;
        } else {
            if (eax6 == 3) {
                *rax7 = 0;
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2450();
            } else {
                return v9;
            }
        }
        rax11 = fun_2390(rdi);
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_6140_3;
        *rax11 = 34;
    }
    quote(rdi);
    if (*r12_8 != 22) 
        goto addr_615c_13;
    while (1) {
        addr_615c_13:
        if (!1) {
        }
        fun_25b0();
    }
}

void xnumtoumax();

void fun_61b3() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void fun_24b0(int64_t rdi);

void** fun_2670(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

struct s1* fun_25e0(struct s1* rdi);

int64_t fun_2480(struct s1* rdi);

int64_t fun_61e3(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx, struct s1* r8) {
    struct s1* v6;
    struct s1* rax7;
    struct s1* v8;
    void* rcx9;
    struct s1* rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s1* rax13;
    struct s1* r15_14;
    void* rax15;
    int64_t rax16;
    struct s1* rbp17;
    struct s1* r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void** rax21;
    struct s1* rax22;
    int64_t rdx23;
    struct s1* rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<struct s1*>("lib/xstrtol.c");
        fun_24b0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2450();
            while (1) {
                rbx10 = reinterpret_cast<struct s1*>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_6554_6;
                    rbx10 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_6554_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<struct s1**>(&r15_14->f0) = rax13;
            if (*reinterpret_cast<struct s1**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_629d_12:
            *reinterpret_cast<struct s1**>(&v6->f0) = rbx10;
            addr_62a5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2390(rdi);
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rbp17->f0));
    rax21 = fun_2670(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s1*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_62db_21;
    rsi = r15_14;
    rax24 = fun_25e0(rbp17);
    r8 = *reinterpret_cast<struct s1**>(&r15_14->f0);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2480(r13_18), r8 = r8, rax26 == 0))) {
            addr_62db_21:
            r12d11 = 4;
            goto addr_62a5_13;
        } else {
            addr_6319_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2480(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x7b78 + rbp33 * 4) + 0x7b78;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_62db_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_629d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_629d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2480(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_6319_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<struct s1**>(&v6->f0) = rbx10;
    goto addr_62a5_13;
}

int64_t fun_23c0();

int64_t rpl_fclose(struct s1* rdi);

int64_t fun_6613(struct s1* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23c0();
    ebx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rdi->f0)) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_666e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2390(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_666e_3;
            rax6 = fun_2390(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_2530(struct s1* rdi);

int32_t fun_2570(struct s1* rdi);

int64_t fun_24a0(int64_t rdi, ...);

int32_t rpl_fflush(struct s1* rdi);

int64_t fun_23f0(struct s1* rdi);

int64_t fun_6683(struct s1* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2530(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2570(rdi);
        if (!(eax3 && (eax4 = fun_2530(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24a0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390(rdi);
            r12d9 = *rax8;
            rax10 = fun_23f0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23f0;
}

void rpl_fseeko(struct s1* rdi);

void fun_6713(struct s1* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2570(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rdi->f0)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6763(struct s1* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2530(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24a0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<struct s1**>(&rdi->f0) = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rdi->f0)) & 0xffffffef);
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2560(int64_t rdi);

signed char* fun_67e3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2560(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2470(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6823(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s1* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2470(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_68b3() {
    struct s1* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

int64_t fun_6933(int64_t rdi, signed char* rsi, struct s1* rdx) {
    struct s1* rax4;
    int32_t r13d5;
    struct s1* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2590(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2440(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2520(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2520(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_69e3() {
    __asm__("cli ");
    goto fun_2590;
}

void fun_69f3() {
    __asm__("cli ");
}

void fun_6a07() {
    __asm__("cli ");
    return;
}

uint32_t fun_24d0(struct s1* rdi, struct s1* rsi, struct s1* rdx, struct s1* rcx);

struct s1* rpl_mbrtowc(void* rdi, struct s1* rsi);

int32_t fun_2660(int64_t rdi, struct s1* rsi);

uint32_t fun_2650(struct s1* rdi, struct s1* rsi);

void fun_3295() {
    struct s1** rsp1;
    int32_t ebp2;
    struct s1* rax3;
    struct s1** rsp4;
    struct s1* r11_5;
    struct s1* r11_6;
    struct s1* v7;
    int32_t ebp8;
    struct s1* rax9;
    struct s1* rdx10;
    struct s1* rax11;
    struct s1* r11_12;
    struct s1* v13;
    int32_t ebp14;
    struct s1* rax15;
    struct s1* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s1* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s1* v22;
    int32_t ebx23;
    struct s1* rax24;
    struct s1** rsp25;
    struct s1* v26;
    struct s1* r11_27;
    struct s1* v28;
    struct s1* v29;
    struct s1* rsi30;
    struct s1* v31;
    struct s1* v32;
    struct s1* r10_33;
    struct s1* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s1* r9_37;
    struct s1* v38;
    struct s1* rdi39;
    struct s1* v40;
    struct s1* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s1* rcx44;
    unsigned char al45;
    struct s1* v46;
    int64_t v47;
    struct s1* v48;
    struct s1* v49;
    struct s1* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s1* v58;
    unsigned char v59;
    struct s1* v60;
    struct s1* v61;
    struct s1* v62;
    signed char* v63;
    struct s1* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s1* r13_69;
    struct s1* rsi70;
    void* v71;
    struct s1* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s1* v107;
    struct s1* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s1**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s1*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s1*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s1**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3593_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3593_22; else 
                            goto addr_398d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3a4d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_3da0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3590_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3590_30; else 
                                goto addr_3db9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_3da0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24d0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_3da0_28; else 
                            goto addr_343c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_3f00_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_3d80_40:
                        if (r11_27 == 1) {
                            addr_390d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_3ec8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3547_44;
                            }
                        } else {
                            goto addr_3d90_46;
                        }
                    } else {
                        addr_3f0f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_390d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3593_22:
                                if (v47 != 1) {
                                    addr_3ae9_52:
                                    v48 = reinterpret_cast<struct s1*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2440(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3b34_54;
                                    }
                                } else {
                                    goto addr_35a0_56;
                                }
                            } else {
                                addr_3545_57:
                                ebp36 = 0;
                                goto addr_3547_44;
                            }
                        } else {
                            addr_3d74_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_3f0f_47; else 
                                goto addr_3d7e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_390d_41;
                        if (v47 == 1) 
                            goto addr_35a0_56; else 
                            goto addr_3ae9_52;
                    }
                }
                addr_3601_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s1*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3498_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_34bd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_37c0_65;
                    } else {
                        addr_3629_66:
                        r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_3e78_67;
                    }
                } else {
                    goto addr_3620_69;
                }
                addr_34d1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s1*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                addr_351c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_3e78_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_351c_81;
                }
                addr_3620_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_34bd_64; else 
                    goto addr_3629_66;
                addr_3547_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_35ff_91;
                if (v22) 
                    goto addr_355f_93;
                addr_35ff_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3601_62;
                addr_3b34_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_42bb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_432b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_412f_103;
                            rdx10 = reinterpret_cast<struct s1*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2660(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_3c2e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_35ec_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3c38_112;
                    }
                } else {
                    addr_3c38_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_3d09_114;
                }
                addr_35f8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_35ff_91;
                while (1) {
                    addr_3d09_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4217_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3c76_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4225_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_3cf7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s1*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_3cf7_128;
                        }
                    }
                    addr_3ca5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_3cf7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->f1);
                    continue;
                    addr_3c76_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s1*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3ca5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_351c_81;
                addr_4225_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3e78_67;
                addr_42bb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_3c2e_109;
                addr_432b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_3c2e_109;
                addr_35a0_56:
                rax93 = fun_2670(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_35ec_110;
                addr_3d7e_59:
                goto addr_3d80_40;
                addr_3a4d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3593_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_35f8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3545_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3593_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3a92_160;
                if (!v22) 
                    goto addr_3e67_162; else 
                    goto addr_4073_163;
                addr_3a92_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_3e67_162:
                    r9_37 = reinterpret_cast<struct s1*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_3e78_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_393b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_37a3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_34d1_70; else 
                    goto addr_37b7_169;
                addr_393b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3498_63;
                goto addr_3620_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_3d74_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_3eaf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3590_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3488_178; else 
                        goto addr_3e32_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3d74_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3593_22;
                }
                addr_3eaf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3590_30:
                    r8d42 = 0;
                    goto addr_3593_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3601_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_3ec8_42;
                    }
                }
                addr_3488_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3498_63;
                addr_3e32_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_3d90_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3601_62;
                } else {
                    addr_3e42_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3593_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_45f2_188;
                if (v28) 
                    goto addr_3e67_162;
                addr_45f2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_37a3_168;
                addr_343c_37:
                if (v22) 
                    goto addr_4433_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3453_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_3f00_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_3f8b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3593_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s1*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s1*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3488_178; else 
                        goto addr_3f67_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3d74_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3593_22;
                }
                addr_3f8b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3593_22;
                }
                addr_3f67_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3d90_46;
                goto addr_3e42_186;
                addr_3453_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3593_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3593_22; else 
                    goto addr_3464_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s1*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_453e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_43c4_210;
            if (1) 
                goto addr_43c2_212;
            if (!v29) 
                goto addr_3ffe_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s1*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s1*>("\"");
            if (!0) 
                goto addr_4531_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s1*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s1*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_37c0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_357b_219; else 
            goto addr_37da_220;
        addr_355f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3573_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_37da_220; else 
            goto addr_357b_219;
        addr_412f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_37da_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_414d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s1*>("'");
        v29 = reinterpret_cast<struct s1*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s1*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s1*>(1);
        v22 = reinterpret_cast<struct s1*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s1*>(0);
            continue;
        }
        addr_45c0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4026_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s1*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s1*>(0);
        v22 = reinterpret_cast<struct s1*>(0);
        v28 = reinterpret_cast<struct s1*>(1);
        v26 = reinterpret_cast<struct s1*>("'");
        continue;
        addr_4217_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3573_221;
        addr_4073_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3573_221;
        addr_37b7_169:
        goto addr_37c0_65;
        addr_453e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_37da_220;
        goto addr_414d_222;
        addr_43c4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s1**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s1*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_441e_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_45c0_225;
        addr_43c2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_43c4_210;
        addr_3ffe_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_43c4_210;
        } else {
            rdx10 = reinterpret_cast<struct s1*>(0);
            goto addr_4026_226;
        }
        addr_4531_216:
        r13_34 = reinterpret_cast<struct s1*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s1*>("\"");
        v29 = reinterpret_cast<struct s1*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s1*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s1*>(1);
        v22 = reinterpret_cast<struct s1*>(0);
        v31 = reinterpret_cast<struct s1*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_398d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x758c + rax113 * 4) + 0x758c;
    addr_3db9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x768c + rax114 * 4) + 0x768c;
    addr_4433_190:
    addr_357b_219:
    goto 0x3260;
    addr_3464_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x748c + rax115 * 4) + 0x748c;
    addr_441e_236:
    goto v116;
}

void fun_3480() {
}

void fun_3638() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3332;
}

void fun_3691() {
    goto 0x3332;
}

void fun_377e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3601;
    }
    if (v2) 
        goto 0x4073;
    if (!r10_3) 
        goto addr_41de_5;
    if (!v4) 
        goto addr_40ae_7;
    addr_41de_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_40ae_7:
    goto 0x34b4;
}

void fun_379c() {
}

void fun_3847() {
    signed char v1;

    if (v1) {
        goto 0x37cf;
    } else {
        goto 0x350a;
    }
}

void fun_3861() {
    signed char v1;

    if (!v1) 
        goto 0x385a; else 
        goto "???";
}

void fun_3888() {
    goto 0x37a3;
}

void fun_3908() {
}

void fun_3920() {
}

void fun_394f() {
    goto 0x37a3;
}

void fun_39a1() {
    goto 0x3930;
}

void fun_39d0() {
    goto 0x3930;
}

void fun_3a03() {
    goto 0x3930;
}

void fun_3dd0() {
    goto 0x3488;
}

void fun_40ce() {
    signed char v1;

    if (v1) 
        goto 0x4073;
    goto 0x34b4;
}

void fun_4175() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x34b4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3498;
        goto 0x34b4;
    }
}

void fun_4592() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3800;
    } else {
        goto 0x3332;
    }
}

void fun_54c8() {
    fun_2420();
}

void fun_638c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x639b;
}

void fun_645c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x6469;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x639b;
}

void fun_6480() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_64ac() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x639b;
}

void fun_64cd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x639b;
}

void fun_64f1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x639b;
}

void fun_6515() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x64a4;
}

void fun_6539() {
}

void fun_6559() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x64a4;
}

void fun_6575() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x64a4;
}

void fun_36be() {
    goto 0x3332;
}

void fun_3894() {
    goto 0x384c;
}

void fun_395b() {
    goto 0x3488;
}

void fun_39ad() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3930;
    goto 0x355f;
}

void fun_39df() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x393b;
        goto 0x3360;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x37da;
        goto 0x357b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4178;
    if (r10_8 > r15_9) 
        goto addr_38c5_9;
    addr_38ca_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4183;
    goto 0x34b4;
    addr_38c5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_38ca_10;
}

void fun_3a12() {
    goto 0x3547;
}

void fun_3de0() {
    goto 0x3547;
}

void fun_457f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x369c;
    } else {
        goto 0x3800;
    }
}

void fun_5580() {
}

void fun_642f() {
    if (__intrinsic()) 
        goto 0x6469; else 
        goto "???";
}

void fun_3a1c() {
    goto 0x39b7;
}

void fun_3dea() {
    goto 0x390d;
}

void fun_55e0() {
    fun_2420();
    goto fun_2630;
}

void fun_36ed() {
    goto 0x3332;
}

void fun_3a28() {
    goto 0x39b7;
}

void fun_3df7() {
    goto 0x395e;
}

void fun_5620() {
    fun_2420();
    goto fun_2630;
}

void fun_371a() {
    goto 0x3332;
}

void fun_3a34() {
    goto 0x3930;
}

void fun_5660() {
    fun_2420();
    goto fun_2630;
}

void fun_373c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x40d0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3601;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3601;
    }
    if (v11) 
        goto 0x4433;
    if (r10_12 > r15_13) 
        goto addr_4483_8;
    addr_4488_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x41c1;
    addr_4483_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4488_9;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

struct s14 {
    signed char[16] pad16;
    struct s1* f10;
};

struct s15 {
    signed char[8] pad8;
    struct s1* f8;
};

void fun_56b0() {
    int64_t r15_1;
    struct s13* rbx2;
    struct s1* r14_3;
    struct s14* rbx4;
    struct s1* r13_5;
    struct s15* rbx6;
    struct s1* r12_7;
    struct s1** rbx8;
    struct s1* rax9;
    struct s1* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2420();
    fun_2630(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x56d2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5708() {
    fun_2420();
    goto 0x56d9;
}

struct s16 {
    signed char[32] pad32;
    int64_t f20;
};

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s1* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s1* f8;
};

struct s20 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5740() {
    int64_t rcx1;
    struct s16* rbx2;
    int64_t r15_3;
    struct s17* rbx4;
    struct s1* r14_5;
    struct s18* rbx6;
    struct s1* r13_7;
    struct s19* rbx8;
    struct s1* r12_9;
    struct s1** rbx10;
    int64_t v11;
    struct s20* rbx12;
    struct s1* rax13;
    struct s1* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2420();
    fun_2630(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5774, __return_address(), rcx1);
    goto v15;
}

void fun_57b8() {
    fun_2420();
    goto 0x577b;
}
