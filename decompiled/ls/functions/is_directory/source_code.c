is_directory (const struct fileinfo *f)
{
  return f->filetype == directory || f->filetype == arg_directory;
}