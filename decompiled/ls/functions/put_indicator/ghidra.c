void put_indicator(size_t *param_1)

{
  __pid_t _Var1;
  
  if (used_color == '\0') {
    used_color = '\x01';
    _Var1 = tcgetpgrp(1);
    if (-1 < _Var1) {
      signal_setup(1);
    }
    if (color_indicator._40_8_ == 0) {
      put_indicator(color_indicator);
      put_indicator(0x125090);
      put_indicator(0x125070);
    }
    else {
      put_indicator(0x125080);
    }
  }
  fwrite_unlocked((void *)param_1[1],*param_1,1,stdout);
  return;
}