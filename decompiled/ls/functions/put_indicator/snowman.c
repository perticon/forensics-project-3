void put_indicator(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8) {
    int1_t zf9;
    int32_t eax10;
    int1_t zf11;
    void** rbx12;

    zf9 = used_color == 0;
    if (zf9) {
        used_color = 1;
        eax10 = fun_4a70(1, rsi, rdx);
        if (eax10 >= 0) {
            signal_setup(1, rsi, rdx, rcx, r8, r9);
        }
        zf11 = g25088 == 0;
        if (zf11) {
            put_indicator(0x25060, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
            put_indicator(0x25090, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
            put_indicator(0x25070, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
        } else {
            put_indicator(0x25080, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
        }
    }
}