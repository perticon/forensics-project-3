void put_indicator(int32_t * ind) {
    // 0x7810
    if (*(char *)&used_color != 0) {
        // 0x781d
        function_4ae0();
        return;
    }
    // 0x7840
    *(char *)&used_color = 1;
    if ((int32_t)function_4a70() >= 0) {
        // 0x7870
        signal_setup(1);
    }
    // 0x7855
    if (g47 == 0) {
        // 0x7880
        put_indicator((int32_t *)&color_indicator);
        put_indicator((int32_t *)&g48);
        put_indicator((int32_t *)&g44);
    } else {
        // 0x785f
        put_indicator((int32_t *)&g46);
    }
    // 0x781d
    function_4ae0();
}