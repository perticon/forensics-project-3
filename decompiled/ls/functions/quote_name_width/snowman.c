void* quote_name_width(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void** rbp8;
    void** rcx9;
    void* rax10;
    void** r9_11;
    void** r8_12;
    void* rsp13;
    void** rdi14;
    void* rax15;
    unsigned char v16;
    void* rdx17;
    void* v18;
    void* rsp19;
    void* rax20;
    int1_t zf21;
    void** rbx22;
    int1_t zf23;
    int1_t zf24;
    struct s5* rax25;
    void** rax26;
    signed char v27;
    int64_t rdx28;
    void** v29;
    void** rax30;
    void** rax31;
    int1_t zf32;
    int1_t zf33;
    void** v34;
    void** rax35;
    void** rax36;
    struct s6* rax37;
    void* rax38;
    void* v39;
    void** r8_40;
    uint32_t eax41;
    unsigned char v42;
    void** v43;
    uint32_t v44;
    void** al45;
    void** rsi46;
    void** v47;
    void** v48;
    void* rax49;
    int64_t v50;
    void** rsi51;
    void** rax52;
    void** r12_53;
    uint32_t ebp54;
    void* v55;
    void** rdi56;
    void** rbx57;
    void** rax58;
    void** r9_59;
    void** r8_60;
    void** rdi61;
    void** rbx62;
    void** rdx63;
    void** rcx64;
    void** rdx65;
    void** tmp64_66;
    void** rdx67;
    void** rax68;
    void** rdx69;
    void** rdi70;
    void** rdx71;
    int64_t* rax72;
    int64_t* rcx73;
    int64_t* rdx74;
    void** rdx75;
    signed char* rax76;
    int64_t v77;
    void** rax78;
    void** rdi79;
    void** rax80;
    void** r10_81;
    void** r9_82;
    void** r11_83;
    void** rcx84;
    void** rdi85;
    void** r8_86;
    int64_t rax87;
    void*** rdx88;
    void** rax89;
    void** rdi90;
    void** rax91;
    int1_t zf92;
    void** rax93;
    void** rax94;
    struct s7* rbx95;
    int1_t zf96;
    int1_t zf97;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x1000 - 0x1000 - 56);
    rbp8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
    rcx9 = edx;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    rax10 = g28;
    r9_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 15);
    r8_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 24);
    quote_name_buf_constprop_0(reinterpret_cast<int64_t>(rsp7) + 16, rdi, rsi, rcx9, r8_12, r9_11);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    rdi14 = rbp8;
    if (rdi14 != rbp8 && rdi14 != rdi) {
        fun_4630(rdi14, rdi14);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
    }
    *reinterpret_cast<uint32_t*>(&rax15) = v16;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    rdx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax10) - reinterpret_cast<uint64_t>(g28));
    if (!rdx17) {
        return reinterpret_cast<int64_t>(rax15) + reinterpret_cast<uint64_t>(v18);
    }
    fun_4870();
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 - 8 - 0x2a8);
    rax20 = g28;
    zf21 = print_inode == 0;
    if (!zf21) 
        goto addr_7dcc_7;
    *reinterpret_cast<int32_t*>(&rbx22) = 0;
    *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
    addr_7e72_9:
    zf23 = print_block_size == 0;
    if (!zf23) {
        zf24 = reinterpret_cast<int1_t>(format == 4);
        if (!zf24) {
            rax25 = reinterpret_cast<struct s5*>(static_cast<int64_t>(reinterpret_cast<int32_t>(block_size_width)));
            rax26 = reinterpret_cast<void**>(&rax25->f1);
        } else {
            *reinterpret_cast<int32_t*>(&rax26) = 2;
            *reinterpret_cast<int32_t*>(&rax26 + 4) = 0;
            if (v27) {
                r8_12 = output_block_size;
                rcx9 = reinterpret_cast<void**>(0x200);
                *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx28) = human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx28) + 4) = 0;
                rax30 = human_readable(v29, rsp19, rdx28, 0x200, r8_12, v29, rsp19, rdx28, 0x200, r8_12);
                rdi14 = rax30;
                rax31 = fun_4860(rdi14, rdi14);
                rax26 = rax31 + 1;
            }
        }
        rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<unsigned char>(rax26));
    }
    zf32 = print_scontext == 0;
    if (!zf32) {
        zf33 = reinterpret_cast<int1_t>(format == 4);
        if (zf33) {
            rdi14 = v34;
            rax35 = fun_4860(rdi14, rdi14);
            rax36 = rax35 + 1;
        } else {
            addr_7ed9_18:
            rax37 = reinterpret_cast<struct s6*>(static_cast<int64_t>(scontext_width));
            rax36 = reinterpret_cast<void**>(&rax37->f1);
        }
        rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<unsigned char>(rax36));
        rax38 = v39;
        if (rax38) {
            addr_7e1c_20:
            r8_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<uint64_t>(rax38));
            eax41 = indicator_style;
            if (eax41) {
                *reinterpret_cast<uint32_t*>(&rdi14) = v42;
                al45 = get_type_indicator(*reinterpret_cast<signed char*>(&rdi14), v43, v44, rcx9, r8_40, r9_11);
                r8_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_40) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_40) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(al45) < 1)))))));
            }
        } else {
            addr_7ef7_22:
            rsi46 = filename_quoting_options;
            rdi14 = v47;
            rax38 = quote_name_width(rdi14, rsi46, v48, rcx9, r8_12, r9_11);
            goto addr_7e1c_20;
        }
        rax49 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax20) - reinterpret_cast<uint64_t>(g28));
        if (!rax49) {
            goto v50;
        }
        fun_4870();
        rsi51 = max_idx;
        rax52 = column_info_alloc_3;
        r12_53 = cwd_n_used;
        ebp54 = *reinterpret_cast<uint32_t*>(&rdi14);
        if (rsi51) 
            goto addr_7f94_27;
    } else {
        addr_7e0c_28:
        rax38 = v55;
        if (!rax38) 
            goto addr_7ef7_22; else 
            goto addr_7e1c_20;
    }
    if (reinterpret_cast<unsigned char>(r12_53) > reinterpret_cast<unsigned char>(rax52)) {
        addr_7fb5_30:
        rdi56 = column_info;
        rbx57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_53) + reinterpret_cast<unsigned char>(r12_53));
        rax58 = xreallocarray(rdi56, r12_53, 48, rcx9);
        column_info = rax58;
    } else {
        addr_81fc_31:
        r9_59 = r12_53;
        if (r12_53) {
            addr_806c_32:
            r8_60 = column_info;
            *reinterpret_cast<int32_t*>(&rsi51) = 3;
            *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi61) = 0;
            *reinterpret_cast<int32_t*>(&rdi61 + 4) = 0;
            goto addr_8080_33;
        } else {
            addr_8208_34:
            if (r9_59) {
                addr_80c1_35:
                *reinterpret_cast<int32_t*>(&rbx62) = 0;
                *reinterpret_cast<int32_t*>(&rbx62 + 4) = 0;
                goto addr_80c8_36;
            } else {
                goto addr_81eb_38;
            }
        }
    }
    addr_7fd4_39:
    rdx63 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx64) = 0;
    *reinterpret_cast<int32_t*>(&rcx64 + 4) = 0;
    rdx65 = rdx63 + 1;
    tmp64_66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx65) + reinterpret_cast<unsigned char>(rbx57));
    *reinterpret_cast<unsigned char*>(&rcx64) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_66) < reinterpret_cast<unsigned char>(rdx65));
    *reinterpret_cast<uint32_t*>(&rdx67) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx67 + 4) = 0;
    if (rcx64 || rdx67) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi51) = 8;
    *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
    rax68 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx57) - reinterpret_cast<unsigned char>(rdx63)) * reinterpret_cast<unsigned char>(tmp64_66) >> 1, 8, rdx67, rcx64, r8_40, r9_11);
    rdx69 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx57) > reinterpret_cast<unsigned char>(rdx69)) {
        rdi70 = column_info;
        rdx71 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx69) * 8 + 8);
        rsi51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx57) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi70) + reinterpret_cast<uint64_t>(rdx71 + reinterpret_cast<unsigned char>(rdx71) * 2) - 8) = rax68;
            rax68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax68) + reinterpret_cast<unsigned char>(rdx71));
            rdx71 = rdx71 + 8;
        } while (rsi51 != rdx71);
    }
    column_info_alloc_3 = rbx57;
    r9_59 = cwd_n_used;
    if (!r12_53) 
        goto addr_8208_34; else 
        goto addr_806c_32;
    do {
        addr_8080_33:
        rax72 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 16) = rsi51;
        rcx73 = rax72 + reinterpret_cast<unsigned char>(rdi61);
        do {
            rdx74 = rax72;
            *rax72 = 3;
            ++rax72;
        } while (rcx73 != rdx74);
        ++rdi61;
        rsi51 = rsi51 + 3;
    } while (reinterpret_cast<unsigned char>(rdi61) < reinterpret_cast<unsigned char>(r12_53));
    if (r9_59) 
        goto addr_80c1_35;
    addr_81bd_48:
    if (reinterpret_cast<unsigned char>(r12_53) > reinterpret_cast<unsigned char>(1)) {
        rdx75 = column_info;
        rax76 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx75 + reinterpret_cast<uint64_t>(r12_53 + reinterpret_cast<unsigned char>(r12_53) * 2) * 8) - 24);
        do {
            if (*rax76) 
                break;
            --r12_53;
            rax76 = rax76 - 24;
        } while (r12_53 != 1);
    }
    addr_81eb_38:
    goto v77;
    do {
        addr_80c8_36:
        rax78 = sorted_file;
        rdi79 = *reinterpret_cast<void***>(rax78 + reinterpret_cast<unsigned char>(rbx62) * 8);
        rax80 = length_of_file_name_and_frills(rdi79, rsi51, rdi79, rsi51);
        r10_81 = cwd_n_used;
        r9_82 = rax80;
        if (r12_53) {
            r11_83 = line_length;
            rsi51 = column_info;
            *reinterpret_cast<int32_t*>(&rcx84) = 0;
            *reinterpret_cast<int32_t*>(&rcx84 + 4) = 0;
            do {
                rdi85 = rcx84;
                ++rcx84;
                if (*reinterpret_cast<void***>(rsi51)) {
                    if (*reinterpret_cast<signed char*>(&ebp54)) {
                        r8_86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx62) / ((reinterpret_cast<unsigned char>(r10_81) + reinterpret_cast<unsigned char>(rcx84) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx84)));
                    } else {
                        r8_86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx62) % reinterpret_cast<unsigned char>(rcx84));
                    }
                    *reinterpret_cast<int32_t*>(&rax87) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax87) = reinterpret_cast<uint1_t>(r8_86 != rdi85);
                    rdx88 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi51 + 16) + reinterpret_cast<unsigned char>(r8_86) * 8);
                    rax89 = r9_82 + rax87 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx88) < reinterpret_cast<unsigned char>(rax89)) {
                        *reinterpret_cast<void***>(rsi51 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi51 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax89) - reinterpret_cast<unsigned char>(*rdx88)));
                        *rdx88 = rax89;
                        *reinterpret_cast<void***>(rsi51) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi51 + 8)) < reinterpret_cast<unsigned char>(r11_83))));
                    }
                }
                rsi51 = rsi51 + 24;
            } while (r12_53 != rcx84);
        }
        ++rbx62;
    } while (reinterpret_cast<unsigned char>(rbx62) < reinterpret_cast<unsigned char>(r10_81));
    goto addr_81bd_48;
    addr_7f94_27:
    if (reinterpret_cast<unsigned char>(rsi51) < reinterpret_cast<unsigned char>(r12_53)) {
        if (reinterpret_cast<unsigned char>(rsi51) <= reinterpret_cast<unsigned char>(rax52)) {
            r9_59 = r12_53;
            r12_53 = rsi51;
            goto addr_806c_32;
        }
        r12_53 = rsi51;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi51) >> 1) > reinterpret_cast<unsigned char>(r12_53)) 
            goto addr_7fb5_30;
    } else {
        if (reinterpret_cast<unsigned char>(r12_53) <= reinterpret_cast<unsigned char>(rax52)) 
            goto addr_81fc_31;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi51) >> 1) > reinterpret_cast<unsigned char>(r12_53)) 
            goto addr_7fb5_30;
    }
    rdi90 = column_info;
    rax91 = xreallocarray(rdi90, rsi51, 24, rcx9);
    rbx57 = max_idx;
    column_info = rax91;
    goto addr_7fd4_39;
    addr_7dcc_7:
    zf92 = reinterpret_cast<int1_t>(format == 4);
    if (zf92) {
        rax93 = umaxtostr();
        rdi14 = rax93;
        rax94 = fun_4860(rdi14, rdi14);
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
        rbx22 = rax94 + 1;
        goto addr_7e72_9;
    } else {
        rbx95 = reinterpret_cast<struct s7*>(static_cast<int64_t>(inode_number_width));
        rbx22 = reinterpret_cast<void**>(&rbx95->f1);
        zf96 = print_block_size == 0;
        if (zf96) {
            zf97 = print_scontext == 0;
            if (!zf97) 
                goto addr_7ed9_18;
            goto addr_7e0c_28;
        }
    }
}