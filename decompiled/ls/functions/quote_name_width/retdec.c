int64_t quote_name_width(char * name, int32_t * options, uint32_t needs_general_quoting) {
    char smallbuf[8192]; // bp-8240, 0x7d00
    int64_t v1 = (int64_t)name;
    int64_t v2; // bp-8232, 0x7d00
    int64_t v3 = &v2; // 0x7d21
    int64_t v4 = __readfsqword(40); // 0x7d2d
    int64_t width = v3; // bp-8248, 0x7d50
    char v5; // bp-8249, 0x7d00
    quote_name_buf_constprop_0((int64_t)&width, v1, (int64_t)options, (int64_t)needs_general_quoting, smallbuf, (int64_t *)&v5);
    int64_t v6 = width; // 0x7d5a
    if (v6 != v3 && v6 != v1) {
        // 0x7d69
        function_4630();
    }
    // 0x7d6e
    if (v4 != __readfsqword(40)) {
        // 0x7d95
        return function_4870();
    }
    // 0x7d8b
    return *(int64_t *)&smallbuf + (int64_t)v5;
}