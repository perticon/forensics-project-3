abmon_init (char abmon[12][ABFORMAT_SIZE])
{
#ifndef HAVE_NL_LANGINFO
  return false;
#else
  size_t required_mon_width = MAX_MON_WIDTH;
  size_t curr_max_width;
  do
    {
      curr_max_width = required_mon_width;
      required_mon_width = 0;
      for (int i = 0; i < 12; i++)
        {
          size_t width = curr_max_width;
          char const *abbr = nl_langinfo (ABMON_1 + i);
          if (strchr (abbr, '%'))
            return false;
          mbs_align_t alignment = isdigit (to_uchar (*abbr))
                                  ? MBS_ALIGN_RIGHT : MBS_ALIGN_LEFT;
          size_t req = mbsalign (abbr, abmon[i], ABFORMAT_SIZE,
                                 &width, alignment, 0);
          if (! (req < ABFORMAT_SIZE))
            return false;
          required_mon_width = MAX (required_mon_width, width);
        }
    }
  while (curr_max_width > required_mon_width);

  return true;
#endif
}