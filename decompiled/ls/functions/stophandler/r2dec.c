int32_t stophandler (void) {
    eax = interrupt_signal;
    if (eax == 0) {
        eax = stop_signal_count;
        eax++;
        *(obj.stop_signal_count) = eax;
    }
    return eax;
}