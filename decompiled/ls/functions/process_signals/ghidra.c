void process_signals(void)

{
  int __sig;
  long in_FS_OFFSET;
  sigset_t sStack184;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  while ((interrupt_signal != 0 || (stop_signal_count != 0))) {
    if (used_color != '\0') {
      put_indicator(color_indicator);
      put_indicator(0x125070);
    }
    fflush_unlocked(stdout);
    sigprocmask(0,(sigset_t *)caught_signals,&sStack184);
    __sig = interrupt_signal;
    if (stop_signal_count == 0) {
      signal(interrupt_signal,(__sighandler_t)0x0);
    }
    else {
      stop_signal_count = stop_signal_count + -1;
      __sig = 0x13;
    }
    raise(__sig);
    sigprocmask(2,&sStack184,(sigset_t *)0x0);
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}