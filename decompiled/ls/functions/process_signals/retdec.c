void process_signals(void) {
    int64_t v1 = __readfsqword(40); // 0x8561
    if ((stop_signal_count || interrupt_signal) != 0) {
        if (*(char *)&used_color != 0) {
            // 0x85ed
            put_indicator((int32_t *)&color_indicator);
            put_indicator((int32_t *)&g44);
        }
        // 0x8580
        function_4c20();
        function_46b0();
        if (stop_signal_count == 0) {
            // 0x8600
            function_49c0();
        } else {
            // 0x85aa
            stop_signal_count--;
        }
        // 0x85b9
        function_46d0();
        function_46b0();
        while ((stop_signal_count || interrupt_signal) != 0) {
            // 0x85e4
            if (*(char *)&used_color != 0) {
                // 0x85ed
                put_indicator((int32_t *)&color_indicator);
                put_indicator((int32_t *)&g44);
            }
            // 0x8580
            function_4c20();
            function_46b0();
            if (stop_signal_count == 0) {
                // 0x8600
                function_49c0();
            } else {
                // 0x85aa
                stop_signal_count--;
            }
            // 0x85b9
            function_46d0();
            function_46b0();
        }
    }
    // 0x8610
    if (v1 == __readfsqword(40)) {
        // 0x8623
        return;
    }
    // 0x8633
    function_4870();
}