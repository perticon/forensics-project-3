void process_signals(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void* rax7;
    void* v8;
    void** rbx9;
    int32_t eax10;
    int32_t eax11;
    int1_t zf12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** rdi17;
    int32_t r14d18;
    int32_t eax19;
    int64_t rdi20;
    void* rax21;
    int1_t zf22;
    void** rax23;
    uint32_t eax24;
    int64_t v25;

    rax7 = g28;
    v8 = rax7;
    rbx9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x90);
    while ((eax10 = interrupt_signal, !!eax10) || (eax11 = stop_signal_count, !!eax11)) {
        zf12 = used_color == 0;
        if (!zf12) {
            put_indicator(0x25060, rsi, rdx, rcx, r8, r9, v13, v14);
            put_indicator(0x25070, rsi, rdx, rcx, r8, r9, v15, v16);
        }
        rdi17 = stdout;
        fun_4c20(rdi17, rsi);
        fun_46b0();
        r14d18 = interrupt_signal;
        eax19 = stop_signal_count;
        if (!eax19) {
            *reinterpret_cast<int32_t*>(&rdi20) = r14d18;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
            fun_49c0(rdi20);
        } else {
            stop_signal_count = eax19 - 1;
        }
        fun_46d0();
        *reinterpret_cast<int32_t*>(&rdx) = 0;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbx9;
        fun_46b0();
    }
    rax21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
    if (!rax21) {
        return;
    }
    fun_4870();
    zf22 = numeric_ids == 0;
    if (!zf22) 
        goto addr_864c_13;
    rax23 = getuser();
    if (rax23) 
        goto addr_867d_15;
    addr_864c_13:
    addr_867d_15:
    eax24 = gnu_mbswidth(rax23);
    if (reinterpret_cast<int32_t>(eax24) < reinterpret_cast<int32_t>(0)) {
    }
    goto v25;
}