uint stdout_isatty(void)

{
  uint uVar1;
  
  if (-1 < (char)out_tty_12) {
    return out_tty_12 & 1;
  }
  uVar1 = isatty(1);
  out_tty_12 = (char)uVar1;
  return uVar1 & 1;
}