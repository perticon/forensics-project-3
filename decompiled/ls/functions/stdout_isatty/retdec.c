bool stdout_isatty(void) {
    unsigned char v1 = *(char *)&g41; // 0x6ca0
    if (v1 >= 0) {
        // 0x6cab
        return v1 % 2 != 0;
    }
    uint64_t v2 = function_4740(); // 0x6cb9
    *(char *)&g41 = (char)v2;
    return v2 % 2 != 0;
}