unsigned char stdout_isatty(void** rdi, ...) {
    uint32_t eax2;
    uint32_t eax3;
    uint32_t eax4;
    uint32_t eax5;

    eax2 = out_tty_12;
    if (*reinterpret_cast<signed char*>(&eax2) < 0) {
        eax3 = fun_4740(1);
        out_tty_12 = *reinterpret_cast<unsigned char*>(&eax3);
        eax4 = eax3 & 1;
        return *reinterpret_cast<unsigned char*>(&eax4);
    } else {
        eax5 = eax2 & 1;
        return *reinterpret_cast<unsigned char*>(&eax5);
    }
}