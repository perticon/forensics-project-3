long length_of_file_name_and_frills(undefined8 *param_1)

{
  char cVar1;
  long lVar2;
  size_t sVar3;
  char *pcVar4;
  long lVar5;
  long in_FS_OFFSET;
  undefined auStack696 [664];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (print_inode == '\0') {
    lVar5 = 0;
LAB_00107e72:
    if (print_block_size != '\0') {
      if (format != 4) goto LAB_00107df1;
      lVar2 = 2;
      if (*(char *)(param_1 + 0x17) != '\0') {
        pcVar4 = (char *)human_readable(param_1[0xb],auStack696,human_output_opts,0x200,
                                        output_block_size);
        sVar3 = strlen(pcVar4);
        lVar2 = sVar3 + 1;
      }
LAB_00107dfc:
      lVar5 = lVar5 + lVar2;
    }
    if (print_scontext != '\0') {
      if (format == 4) {
        sVar3 = strlen((char *)param_1[0x16]);
      }
      else {
LAB_00107ed9:
        sVar3 = (size_t)scontext_width;
      }
      lVar5 = lVar5 + sVar3 + 1;
      lVar2 = param_1[0x19];
      goto joined_r0x00107ef1;
    }
  }
  else {
    if (format == 4) {
      pcVar4 = (char *)umaxtostr(param_1[4],auStack696);
      sVar3 = strlen(pcVar4);
      lVar5 = sVar3 + 1;
      goto LAB_00107e72;
    }
    lVar5 = (long)inode_number_width + 1;
    if (print_block_size != '\0') {
LAB_00107df1:
      lVar2 = (long)block_size_width + 1;
      goto LAB_00107dfc;
    }
    if (print_scontext != '\0') goto LAB_00107ed9;
  }
  lVar2 = param_1[0x19];
joined_r0x00107ef1:
  if (lVar2 == 0) {
    lVar2 = quote_name_width(*param_1,filename_quoting_options,*(undefined4 *)((long)param_1 + 0xc4)
                            );
  }
  lVar5 = lVar5 + lVar2;
  if (indicator_style != 0) {
    cVar1 = get_type_indicator(*(undefined *)(param_1 + 0x17),*(undefined4 *)(param_1 + 6),
                               *(undefined4 *)(param_1 + 0x15));
    lVar5 = (lVar5 + 1) - (ulong)(cVar1 == '\0');
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}