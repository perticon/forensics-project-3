int64_t length_of_file_name_and_frills(int32_t * f) {
    int64_t v1 = (int64_t)f;
    int64_t v2 = __readfsqword(40); // 0x7dac
    int64_t v3 = v1; // 0x7dc6
    int64_t v4 = 0; // 0x7dc6
    int64_t v5; // 0x7da0
    int64_t v6; // 0x7da0
    int64_t v7; // bp-696, 0x7da0
    if (*(char *)&print_inode == 0) {
        goto lab_0x7e72;
    } else {
        // 0x7dcc
        if (format == 4) {
            char * v8 = umaxtostr(*(int64_t *)(v1 + 32), (char *)&v7); // 0x7f37
            v3 = (int64_t)v8;
            v4 = function_4860() + 1;
            goto lab_0x7e72;
        } else {
            // 0x7dd9
            v5 = (int64_t)inode_number_width + 1;
            v6 = v1;
            if (*(char *)&print_block_size == 0) {
                // 0x7f18
            } else {
                goto lab_0x7df1;
            }
        }
    }
  lab_0x7e72:;
    int64_t v9 = v4; // 0x7e79
    int64_t v10 = v3; // 0x7e79
    int64_t v11; // 0x7da0
    int64_t v12; // 0x7da0
    int64_t v13; // 0x7da0
    if (*(char *)&print_block_size == 0) {
        goto lab_0x7dff;
    } else {
        // 0x7e7b
        v5 = v4;
        v6 = v3;
        if (format != 4) {
            goto lab_0x7df1;
        } else {
            // 0x7e88
            v11 = 2;
            v12 = v4;
            v13 = v3;
            if (*(char *)(v1 + 184) != 0) {
                char * v14 = human_readable(*(int64_t *)(v1 + 88), (char *)&v7, human_output_opts, 512, output_block_size); // 0x7eb3
                v11 = function_4860() + 1;
                v12 = v4;
                v13 = (int64_t)v14;
            }
            goto lab_0x7dfc;
        }
    }
  lab_0x7dff:;
    int64_t v15 = v10;
    int64_t v16 = v9;
    int64_t v17 = v16; // 0x7e06
    int64_t v18 = v15; // 0x7e06
    int64_t v19; // 0x7da0
    int64_t v20; // 0x7da0
    int64_t v21; // 0x7da0
    int64_t v22; // 0x7da0
    int64_t v23; // 0x7da0
    if (*(char *)&print_scontext != 0) {
        // 0x7ed0
        v19 = v16;
        v21 = v15;
        if (format == 4) {
            // 0x7f50
            v20 = v16;
            v22 = *(int64_t *)(v1 + 176);
            v23 = function_4860();
            goto lab_0x7ee4;
        } else {
            goto lab_0x7ed9;
        }
    } else {
        goto lab_0x7e0c;
    }
  lab_0x7e0c:;
    int64_t v24 = *(int64_t *)(v1 + 200); // 0x7e0c
    int64_t v25 = v24; // 0x7e16
    int64_t v26 = v17; // 0x7e16
    int64_t v27 = v17; // 0x7e16
    int64_t v28 = v18; // 0x7e16
    if (v24 == 0) {
        goto lab_0x7ef7;
    } else {
        goto lab_0x7e1c;
    }
  lab_0x7df1:
    // 0x7df1
    v11 = (int64_t)block_size_width + 1;
    v12 = v5;
    v13 = v6;
    goto lab_0x7dfc;
  lab_0x7ed9:
    // 0x7ed9
    v20 = v19;
    v22 = v21;
    v23 = scontext_width;
    goto lab_0x7ee4;
  lab_0x7ef7:
    // 0x7ef7
    v25 = quote_name_width((char *)v28, filename_quoting_options, *(int32_t *)(v1 + 196));
    v26 = v27;
    goto lab_0x7e1c;
  lab_0x7e1c:
    // 0x7e1c
    if (indicator_style != 0) {
        // 0x7e2a
        int64_t v31; // 0x7da0
        int64_t v32 = v31;
        int32_t v33 = *(int32_t *)(v1 + 168); // 0x7e2a
        int32_t v34 = *(int32_t *)(v1 + 48); // 0x7e30
        char v35 = *(char *)(v1 + 184); // 0x7e33
        int64_t v36; // 0x7e1c
        int64_t v37 = v36 + (int64_t)((char)get_type_indicator(v35, v34, v33, v32) != 0); // 0x7e41
    }
    // 0x7e45
    if (v2 != __readfsqword(40)) {
        // 0x7f62
        return function_4870();
    }
    // 0x7e5c
    return v26 + v25;
  lab_0x7dfc:
    // 0x7dfc
    v9 = v12 + v11;
    v10 = v13;
    goto lab_0x7dff;
  lab_0x7ee4:;
    int64_t v29 = v20 + 1 + v23; // 0x7ee4
    int64_t v30 = *(int64_t *)(v1 + 200); // 0x7ee7
    v25 = v30;
    v26 = v29;
    v27 = v29;
    v28 = v22;
    if (v30 != 0) {
        goto lab_0x7e1c;
    } else {
        goto lab_0x7ef7;
    }
}