void** length_of_file_name_and_frills(void** rdi, void** rsi, ...) {
    void** rbp3;
    void* rsp4;
    void* rax5;
    int1_t zf6;
    void** rbx7;
    int1_t zf8;
    void** rax9;
    void** rax10;
    struct s8* rbx11;
    int1_t zf12;
    int1_t zf13;
    int1_t zf14;
    int1_t zf15;
    struct s9* rax16;
    void** rax17;
    void** rdi18;
    void** r8_19;
    int64_t rdx20;
    void** rax21;
    void** rax22;
    int1_t zf23;
    int1_t zf24;
    void** rax25;
    void** rax26;
    struct s10* rax27;
    void* rax28;
    void** r8_29;
    uint32_t eax30;
    uint32_t edx31;
    void** esi32;
    void** r9_33;
    void** al34;
    void** edx35;
    void** rsi36;
    void** r9_37;
    void* rax38;
    void** rsi39;
    void** rax40;
    void** r12_41;
    uint32_t ebp42;
    void** rdi43;
    void** rbx44;
    void** rax45;
    void** r9_46;
    void** r8_47;
    void** rdi48;
    void** rbx49;
    void** rdx50;
    void** rcx51;
    void** rdx52;
    void** tmp64_53;
    void** rdx54;
    void** r9_55;
    void** rax56;
    void** rdx57;
    void** rdi58;
    void** rdx59;
    int64_t* rax60;
    int64_t* rcx61;
    int64_t* rdx62;
    void** rdx63;
    signed char* rax64;
    int64_t v65;
    void** rax66;
    void** rdi67;
    void** rax68;
    void** r10_69;
    void** r9_70;
    void** r11_71;
    void** rcx72;
    void** rdi73;
    void** r8_74;
    int64_t rax75;
    void*** rdx76;
    void** rax77;
    void** rdi78;
    void** rax79;

    rbp3 = rdi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8);
    rax5 = g28;
    zf6 = print_inode == 0;
    if (zf6) {
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
    } else {
        zf8 = reinterpret_cast<int1_t>(format == 4);
        if (zf8) {
            rax9 = umaxtostr();
            rdi = rax9;
            rax10 = fun_4860(rdi, rdi);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            rbx7 = rax10 + 1;
        } else {
            rbx11 = reinterpret_cast<struct s8*>(static_cast<int64_t>(inode_number_width));
            rbx7 = reinterpret_cast<void**>(&rbx11->f1);
            zf12 = print_block_size == 0;
            if (zf12) {
                zf13 = print_scontext == 0;
                if (!zf13) 
                    goto addr_7ed9_7;
                goto addr_7e0c_9;
            }
        }
    }
    zf14 = print_block_size == 0;
    if (!zf14) {
        zf15 = reinterpret_cast<int1_t>(format == 4);
        if (!zf15) {
            rax16 = reinterpret_cast<struct s9*>(static_cast<int64_t>(reinterpret_cast<int32_t>(block_size_width)));
            rax17 = reinterpret_cast<void**>(&rax16->f1);
        } else {
            *reinterpret_cast<int32_t*>(&rax17) = 2;
            *reinterpret_cast<int32_t*>(&rax17 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(rbp3 + 0xb8)) {
                rdi18 = *reinterpret_cast<void***>(rbp3 + 88);
                r8_19 = output_block_size;
                *reinterpret_cast<int32_t*>(&rdx20) = human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                rax21 = human_readable(rdi18, rsp4, rdx20, 0x200, r8_19);
                rdi = rax21;
                rax22 = fun_4860(rdi, rdi);
                rax17 = rax22 + 1;
            }
        }
        rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax17));
    }
    zf23 = print_scontext == 0;
    if (!zf23) {
        zf24 = reinterpret_cast<int1_t>(format == 4);
        if (zf24) {
            rdi = *reinterpret_cast<void***>(rbp3 + 0xb0);
            rax25 = fun_4860(rdi, rdi);
            rax26 = rax25 + 1;
        } else {
            addr_7ed9_7:
            rax27 = reinterpret_cast<struct s10*>(static_cast<int64_t>(scontext_width));
            rax26 = reinterpret_cast<void**>(&rax27->f1);
        }
        rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax26));
        rax28 = *reinterpret_cast<void**>(rbp3 + 0xc8);
        if (rax28) {
            addr_7e1c_20:
            r8_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<uint64_t>(rax28));
            eax30 = indicator_style;
            if (eax30) {
                edx31 = *reinterpret_cast<uint32_t*>(rbp3 + 0xa8);
                esi32 = *reinterpret_cast<void***>(rbp3 + 48);
                *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(rbp3 + 0xb8);
                al34 = get_type_indicator(*reinterpret_cast<signed char*>(&rdi), esi32, edx31, 0x200, r8_29, r9_33);
                r8_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_29) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_29) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(al34) < 1)))))));
            }
        } else {
            addr_7ef7_22:
            edx35 = *reinterpret_cast<void***>(rbp3 + 0xc4);
            rsi36 = filename_quoting_options;
            rdi = *reinterpret_cast<void***>(rbp3);
            rax28 = quote_name_width(rdi, rsi36, edx35, 0x200, r8_19, r9_37);
            goto addr_7e1c_20;
        }
        rax38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
        if (!rax38) {
            return r8_29;
        }
        fun_4870();
        rsi39 = max_idx;
        rax40 = column_info_alloc_3;
        r12_41 = cwd_n_used;
        ebp42 = *reinterpret_cast<uint32_t*>(&rdi);
        if (rsi39) 
            goto addr_7f94_27;
    } else {
        addr_7e0c_9:
        rax28 = *reinterpret_cast<void**>(rbp3 + 0xc8);
        if (!rax28) 
            goto addr_7ef7_22; else 
            goto addr_7e1c_20;
    }
    if (reinterpret_cast<unsigned char>(r12_41) > reinterpret_cast<unsigned char>(rax40)) {
        addr_7fb5_29:
        rdi43 = column_info;
        rbx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_41) + reinterpret_cast<unsigned char>(r12_41));
        rax45 = xreallocarray(rdi43, r12_41, 48, 0x200);
        column_info = rax45;
    } else {
        addr_81fc_30:
        r9_46 = r12_41;
        if (r12_41) {
            addr_806c_31:
            r8_47 = column_info;
            *reinterpret_cast<int32_t*>(&rsi39) = 3;
            *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi48) = 0;
            *reinterpret_cast<int32_t*>(&rdi48 + 4) = 0;
            goto addr_8080_32;
        } else {
            addr_8208_33:
            if (r9_46) {
                addr_80c1_34:
                *reinterpret_cast<int32_t*>(&rbx49) = 0;
                *reinterpret_cast<int32_t*>(&rbx49 + 4) = 0;
                goto addr_80c8_35;
            } else {
                goto addr_81eb_37;
            }
        }
    }
    addr_7fd4_38:
    rdx50 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx51) = 0;
    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
    rdx52 = rdx50 + 1;
    tmp64_53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx52) + reinterpret_cast<unsigned char>(rbx44));
    *reinterpret_cast<unsigned char*>(&rcx51) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_53) < reinterpret_cast<unsigned char>(rdx52));
    *reinterpret_cast<uint32_t*>(&rdx54) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx54 + 4) = 0;
    if (rcx51 || rdx54) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi39) = 8;
    *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
    rax56 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx44) - reinterpret_cast<unsigned char>(rdx50)) * reinterpret_cast<unsigned char>(tmp64_53) >> 1, 8, rdx54, rcx51, r8_29, r9_55);
    rdx57 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx44) > reinterpret_cast<unsigned char>(rdx57)) {
        rdi58 = column_info;
        rdx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx57) * 8 + 8);
        rsi39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx44) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi58) + reinterpret_cast<uint64_t>(rdx59 + reinterpret_cast<unsigned char>(rdx59) * 2) - 8) = rax56;
            rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax56) + reinterpret_cast<unsigned char>(rdx59));
            rdx59 = rdx59 + 8;
        } while (rsi39 != rdx59);
    }
    column_info_alloc_3 = rbx44;
    r9_46 = cwd_n_used;
    if (!r12_41) 
        goto addr_8208_33; else 
        goto addr_806c_31;
    do {
        addr_8080_32:
        rax60 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 16) = rsi39;
        rcx61 = rax60 + reinterpret_cast<unsigned char>(rdi48);
        do {
            rdx62 = rax60;
            *rax60 = 3;
            ++rax60;
        } while (rcx61 != rdx62);
        ++rdi48;
        rsi39 = rsi39 + 3;
    } while (reinterpret_cast<unsigned char>(rdi48) < reinterpret_cast<unsigned char>(r12_41));
    if (r9_46) 
        goto addr_80c1_34;
    addr_81bd_47:
    if (reinterpret_cast<unsigned char>(r12_41) > reinterpret_cast<unsigned char>(1)) {
        rdx63 = column_info;
        rax64 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx63 + reinterpret_cast<uint64_t>(r12_41 + reinterpret_cast<unsigned char>(r12_41) * 2) * 8) - 24);
        do {
            if (*rax64) 
                break;
            --r12_41;
            rax64 = rax64 - 24;
        } while (r12_41 != 1);
    }
    addr_81eb_37:
    goto v65;
    do {
        addr_80c8_35:
        rax66 = sorted_file;
        rdi67 = *reinterpret_cast<void***>(rax66 + reinterpret_cast<unsigned char>(rbx49) * 8);
        rax68 = length_of_file_name_and_frills(rdi67, rsi39, rdi67, rsi39);
        r10_69 = cwd_n_used;
        r9_70 = rax68;
        if (r12_41) {
            r11_71 = line_length;
            rsi39 = column_info;
            *reinterpret_cast<int32_t*>(&rcx72) = 0;
            *reinterpret_cast<int32_t*>(&rcx72 + 4) = 0;
            do {
                rdi73 = rcx72;
                ++rcx72;
                if (*reinterpret_cast<void***>(rsi39)) {
                    if (*reinterpret_cast<signed char*>(&ebp42)) {
                        r8_74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx49) / ((reinterpret_cast<unsigned char>(r10_69) + reinterpret_cast<unsigned char>(rcx72) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx72)));
                    } else {
                        r8_74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx49) % reinterpret_cast<unsigned char>(rcx72));
                    }
                    *reinterpret_cast<int32_t*>(&rax75) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax75) = reinterpret_cast<uint1_t>(r8_74 != rdi73);
                    rdx76 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi39 + 16) + reinterpret_cast<unsigned char>(r8_74) * 8);
                    rax77 = r9_70 + rax75 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx76) < reinterpret_cast<unsigned char>(rax77)) {
                        *reinterpret_cast<void***>(rsi39 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi39 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax77) - reinterpret_cast<unsigned char>(*rdx76)));
                        *rdx76 = rax77;
                        *reinterpret_cast<void***>(rsi39) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi39 + 8)) < reinterpret_cast<unsigned char>(r11_71))));
                    }
                }
                rsi39 = rsi39 + 24;
            } while (r12_41 != rcx72);
        }
        ++rbx49;
    } while (reinterpret_cast<unsigned char>(rbx49) < reinterpret_cast<unsigned char>(r10_69));
    goto addr_81bd_47;
    addr_7f94_27:
    if (reinterpret_cast<unsigned char>(rsi39) < reinterpret_cast<unsigned char>(r12_41)) {
        if (reinterpret_cast<unsigned char>(rsi39) <= reinterpret_cast<unsigned char>(rax40)) {
            r9_46 = r12_41;
            r12_41 = rsi39;
            goto addr_806c_31;
        }
        r12_41 = rsi39;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi39) >> 1) > reinterpret_cast<unsigned char>(r12_41)) 
            goto addr_7fb5_29;
    } else {
        if (reinterpret_cast<unsigned char>(r12_41) <= reinterpret_cast<unsigned char>(rax40)) 
            goto addr_81fc_30;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi39) >> 1) > reinterpret_cast<unsigned char>(r12_41)) 
            goto addr_7fb5_29;
    }
    rdi78 = column_info;
    rax79 = xreallocarray(rdi78, rsi39, 24, 0x200);
    rbx44 = max_idx;
    column_info = rax79;
    goto addr_7fd4_38;
}