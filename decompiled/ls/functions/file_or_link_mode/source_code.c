file_or_link_mode (struct fileinfo const *file)
{
  return (color_symlink_as_referent && file->linkok
          ? file->linkmode : file->stat.st_mode);
}