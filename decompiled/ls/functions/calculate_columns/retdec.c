int64_t calculate_columns(bool by_columns) {
    uint64_t v1 = max_idx; // 0x7f70
    uint64_t v2 = g64; // 0x7f77
    int64_t v3; // 0x7f70
    int64_t v4; // 0x7f70
    if (v1 == 0) {
        if (cwd_n_used > v2) {
            goto lab_0x7fb5;
        } else {
            goto lab_0x81fc;
        }
    } else {
        if (v1 < cwd_n_used) {
            // 0x816f
            v3 = v1;
            v4 = v1;
            if (v1 > v2) {
                goto lab_0x818a;
            } else {
                goto lab_0x806c;
            }
        } else {
            if (cwd_n_used > v2) {
                // 0x7fa6
                v4 = cwd_n_used;
                if (v1 / 2 > cwd_n_used) {
                    goto lab_0x7fb5;
                } else {
                    goto lab_0x818a;
                }
            } else {
                goto lab_0x81fc;
            }
        }
    }
  lab_0x7fb5:;
    int64_t v5 = 2 * cwd_n_used; // 0x7fcd
    int64_t v6 = cwd_n_used; // 0x7fcd
    int64_t v7 = xreallocarray(); // 0x7fcd
    goto lab_0x7fd4;
  lab_0x81fc:
    // 0x81fc
    v3 = cwd_n_used;
    int64_t result = cwd_n_used; // 0x8202
    if (cwd_n_used != 0) {
        goto lab_0x806c;
    } else {
        goto lab_0x8208;
    }
  lab_0x7fd4:
    // 0x7fd4
    *(int64_t *)&column_info = v7;
    uint64_t v8 = g64 + 1; // 0x7fe3
    uint64_t v9 = v8 + v5; // 0x7fe7
    if (v9 < v8) {
        // 0x8223
        xalloc_die();
        return &g93;
    }
    uint128_t v10 = (int128_t)v9 * (int128_t)(v5 - g64); // 0x7fed
    if (v10 > 0xffffffffffffffff) {
        // 0x8223
        xalloc_die();
        return &g93;
    }
    char * v11 = xnmalloc((int64_t)v10 / 2, 8); // 0x8013
    uint64_t v12 = g64; // 0x8018
    if (v5 > v12) {
        int64_t v13 = (int64_t)v11; // 0x803b
        int64_t v14 = 8 * v12; // 0x803b
        v14 += 8;
        *(int64_t *)((int64_t)column_info - 8 + 3 * v14) = v13;
        v13 += v14;
        while (8 * v5 != v14) {
            // 0x8040
            v14 += 8;
            *(int64_t *)((int64_t)column_info - 8 + 3 * v14) = v13;
            v13 += v14;
        }
        // 0x8055
        g64 = v5;
        v3 = v6;
        result = 0;
        if (v6 == 0) {
            goto lab_0x8208;
        } else {
            goto lab_0x806c;
        }
    } else {
        // 0x8055
        g64 = v5;
        v3 = v6;
        goto lab_0x806c;
    }
  lab_0x806c:;
    int64_t v15 = 3; // 0x807a
    int64_t v16 = 0; // 0x807a
    int64_t v17 = 8 * v15 + (int64_t)column_info;
    int64_t v18 = *(int64_t *)(v17 - 8); // 0x8080
    *(char *)(v17 - 24) = 1;
    *(int64_t *)(v17 - 16) = v15;
    int64_t v19 = v18;
    *(int64_t *)v19 = 3;
    int64_t v20 = v19 + 8; // 0x80a9
    while (v18 + 8 * v16 != v19) {
        // 0x8098
        v19 = v20;
        *(int64_t *)v19 = 3;
        v20 = v19 + 8;
    }
    int64_t v21 = v16 + 1; // 0x80ab
    v15 += 3;
    v16 = v21;
    while (v21 < v3) {
        // 0x8080
        v17 = 8 * v15 + (int64_t)column_info;
        v18 = *(int64_t *)(v17 - 8);
        *(char *)(v17 - 24) = 1;
        *(int64_t *)(v17 - 16) = v15;
        v19 = v18;
        *(int64_t *)v19 = 3;
        v20 = v19 + 8;
        while (v18 + 8 * v16 != v19) {
            // 0x8098
            v19 = v20;
            *(int64_t *)v19 = 3;
            v20 = v19 + 8;
        }
        // 0x80ab
        v21 = v16 + 1;
        v15 += 3;
        v16 = v21;
    }
    int64_t result2 = v3; // 0x80bb
    if (cwd_n_used == 0) {
        goto lab_0x81bd;
    } else {
        goto lab_0x80c1;
    }
  lab_0x8208:
    // 0x8208
    if (cwd_n_used == 0) {
        // 0x81eb
        return result;
    }
    goto lab_0x80c1;
  lab_0x818a:;
    int64_t v48 = xreallocarray(); // 0x8196
    v5 = max_idx;
    v6 = v4;
    v7 = v48;
    goto lab_0x7fd4;
  lab_0x81bd:
    // 0x81bd
    if (result2 < 2) {
        // 0x81eb
        return result2;
    }
    int64_t v49 = 24 * result2 - 24 + (int64_t)column_info; // 0x81d3
    int64_t v50 = result2;
    int64_t result3 = v50; // 0x81e9
    while (*(char *)v49 == 0) {
        // 0x81d8
        v49 -= 24;
        result3 = 1;
        if (v50 == 2) {
            // break -> 0x81eb
            break;
        }
        v50--;
        result3 = v50;
    }
    // 0x81eb
    return result3;
  lab_0x80c1:
    // 0x80c1
    while (true) {
        // 0x80c8
        int64_t v22; // 0x7f70
        uint64_t v23 = v22;
        char ** v24 = sorted_file; // 0x80c8
        int64_t v25 = *(int64_t *)(8 * v23 + (int64_t)v24); // 0x80cf
        int64_t v26 = length_of_file_name_and_frills((int32_t *)v25); // 0x80d3
        uint64_t v27 = cwd_n_used; // 0x80d8
        int64_t v28; // 0x7f70
        if (v28 != 0) {
            uint64_t v29 = line_length; // 0x80eb
            int32_t * v30 = column_info; // 0x80f2
            int64_t v31 = 0; // 0x80fb
            int64_t v32 = (int64_t)v30; // 0x80fb
            int64_t v33 = v32;
            int64_t v34 = v31;
            uint64_t v35 = v34 + 1; // 0x8154
            char * v36 = (char *)v33; // 0x8158
            char v37 = *v36; // 0x8158
            int64_t v38; // 0x7f70
            int64_t v39; // 0x7f70
            int64_t v40; // 0x8118
            uint64_t v41; // 0x8128
            int64_t * v42; // 0x812c
            uint64_t v43; // 0x812c
            int64_t * v44; // 0x813a
            int64_t v45; // 0x813a
            uint64_t v46; // 0x8141
            if (v37 != 0) {
                if (by_columns) {
                    // 0x8100
                    v38 = v23 / ((v34 + v27) / v35);
                } else {
                    // 0x8162
                    v38 = v23 % v35;
                }
                // 0x8118
                v39 = v38;
                v40 = *(int64_t *)(v33 + 16);
                v41 = 2 * (int64_t)(v39 != v34) + v26;
                v42 = (int64_t *)(v40 + 8 * v39);
                v43 = *v42;
                if (v41 > v43) {
                    // 0x8134
                    v44 = (int64_t *)(v33 + 8);
                    v45 = *v44;
                    *v44 = v41 - v43 + v45;
                    *v42 = v41;
                    v46 = *v44;
                    *v36 = (char)(v46 < v29);
                }
            }
            // 0x8148
            v31 = v35;
            v32 = v33 + 24;
            while (v28 != v35) {
                // 0x8151
                v33 = v32;
                v34 = v31;
                v35 = v34 + 1;
                v36 = (char *)v33;
                v37 = *v36;
                if (v37 != 0) {
                    if (by_columns) {
                        // 0x8100
                        v38 = v23 / ((v34 + v27) / v35);
                    } else {
                        // 0x8162
                        v38 = v23 % v35;
                    }
                    // 0x8118
                    v39 = v38;
                    v40 = *(int64_t *)(v33 + 16);
                    v41 = 2 * (int64_t)(v39 != v34) + v26;
                    v42 = (int64_t *)(v40 + 8 * v39);
                    v43 = *v42;
                    if (v41 > v43) {
                        // 0x8134
                        v44 = (int64_t *)(v33 + 8);
                        v45 = *v44;
                        *v44 = v41 - v43 + v45;
                        *v42 = v41;
                        v46 = *v44;
                        *v36 = (char)(v46 < v29);
                    }
                }
                // 0x8148
                v31 = v35;
                v32 = v33 + 24;
            }
        }
        int64_t v47 = v23 + 1; // 0x81b0
        v22 = v47;
        result2 = v28;
        if (v47 >= v27) {
            // break -> 0x81bd
            break;
        }
    }
    goto lab_0x81bd;
}