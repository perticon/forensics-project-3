void** calculate_columns(int32_t edi) {
    void** rsi2;
    void** rax3;
    void** r12_4;
    int32_t ebp5;
    void** rdi6;
    void** rbx7;
    void** rcx8;
    void** rax9;
    void** r9_10;
    void** r8_11;
    void** rdi12;
    void** rbx13;
    void** rdi14;
    void** rcx15;
    void** rax16;
    void** rdx17;
    void** rcx18;
    void** rdx19;
    void** tmp64_20;
    void** rdx21;
    void** r8_22;
    void** r9_23;
    void** rax24;
    void** rdx25;
    void** rdi26;
    void** rdx27;
    int64_t* rax28;
    int64_t* rcx29;
    int64_t* rdx30;
    void** rdx31;
    signed char* rax32;
    void** rax33;
    void** rdi34;
    void** rax35;
    void** r10_36;
    void** r9_37;
    void** r11_38;
    void** rcx39;
    void** rdi40;
    void** r8_41;
    int64_t rax42;
    void*** rdx43;
    void** rax44;

    rsi2 = max_idx;
    rax3 = column_info_alloc_3;
    r12_4 = cwd_n_used;
    ebp5 = edi;
    if (!rsi2) {
        if (reinterpret_cast<unsigned char>(r12_4) > reinterpret_cast<unsigned char>(rax3)) {
            addr_7fb5_3:
            rdi6 = column_info;
            rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r12_4));
            rax9 = xreallocarray(rdi6, r12_4, 48, rcx8);
            column_info = rax9;
        } else {
            addr_81fc_4:
            r9_10 = r12_4;
            if (r12_4) {
                addr_806c_5:
                r8_11 = column_info;
                *reinterpret_cast<int32_t*>(&rsi2) = 3;
                *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi12) = 0;
                *reinterpret_cast<int32_t*>(&rdi12 + 4) = 0;
                goto addr_8080_6;
            } else {
                addr_8208_7:
                if (r9_10) {
                    addr_80c1_8:
                    *reinterpret_cast<int32_t*>(&rbx13) = 0;
                    *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                    goto addr_80c8_9;
                } else {
                    goto addr_81eb_11;
                }
            }
        }
    } else {
        if (reinterpret_cast<unsigned char>(rsi2) < reinterpret_cast<unsigned char>(r12_4)) {
            if (reinterpret_cast<unsigned char>(rsi2) <= reinterpret_cast<unsigned char>(rax3)) {
                r9_10 = r12_4;
                r12_4 = rsi2;
                goto addr_806c_5;
            }
            r12_4 = rsi2;
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi2) >> 1) > reinterpret_cast<unsigned char>(r12_4)) 
                goto addr_7fb5_3;
        } else {
            if (reinterpret_cast<unsigned char>(r12_4) <= reinterpret_cast<unsigned char>(rax3)) 
                goto addr_81fc_4;
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi2) >> 1) > reinterpret_cast<unsigned char>(r12_4)) 
                goto addr_7fb5_3;
        }
        rdi14 = column_info;
        rax16 = xreallocarray(rdi14, rsi2, 24, rcx15);
        rbx7 = max_idx;
        column_info = rax16;
    }
    rdx17 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx18) = 0;
    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
    rdx19 = rdx17 + 1;
    tmp64_20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx19) + reinterpret_cast<unsigned char>(rbx7));
    *reinterpret_cast<unsigned char*>(&rcx18) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_20) < reinterpret_cast<unsigned char>(rdx19));
    *reinterpret_cast<uint32_t*>(&rdx21) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
    if (rcx18 || rdx21) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi2) = 8;
    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
    rax24 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx7) - reinterpret_cast<unsigned char>(rdx17)) * reinterpret_cast<unsigned char>(tmp64_20) >> 1, 8, rdx21, rcx18, r8_22, r9_23);
    rdx25 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx7) > reinterpret_cast<unsigned char>(rdx25)) {
        rdi26 = column_info;
        rdx27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx25) * 8 + 8);
        rsi2 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi26) + reinterpret_cast<uint64_t>(rdx27 + reinterpret_cast<unsigned char>(rdx27) * 2) - 8) = rax24;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rdx27));
            rdx27 = rdx27 + 8;
        } while (rsi2 != rdx27);
    }
    column_info_alloc_3 = rbx7;
    r9_10 = cwd_n_used;
    if (!r12_4) 
        goto addr_8208_7; else 
        goto addr_806c_5;
    do {
        addr_8080_6:
        rax28 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 16) = rsi2;
        rcx29 = rax28 + reinterpret_cast<unsigned char>(rdi12);
        do {
            rdx30 = rax28;
            *rax28 = 3;
            ++rax28;
        } while (rcx29 != rdx30);
        ++rdi12;
        rsi2 = rsi2 + 3;
    } while (reinterpret_cast<unsigned char>(rdi12) < reinterpret_cast<unsigned char>(r12_4));
    if (r9_10) 
        goto addr_80c1_8;
    addr_81bd_28:
    if (reinterpret_cast<unsigned char>(r12_4) > reinterpret_cast<unsigned char>(1)) {
        rdx31 = column_info;
        rax32 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx31 + reinterpret_cast<uint64_t>(r12_4 + reinterpret_cast<unsigned char>(r12_4) * 2) * 8) - 24);
        do {
            if (*rax32) 
                break;
            --r12_4;
            rax32 = rax32 - 24;
        } while (r12_4 != 1);
    }
    addr_81eb_11:
    return r12_4;
    do {
        addr_80c8_9:
        rax33 = sorted_file;
        rdi34 = *reinterpret_cast<void***>(rax33 + reinterpret_cast<unsigned char>(rbx13) * 8);
        rax35 = length_of_file_name_and_frills(rdi34, rsi2);
        r10_36 = cwd_n_used;
        r9_37 = rax35;
        if (r12_4) {
            r11_38 = line_length;
            rsi2 = column_info;
            *reinterpret_cast<int32_t*>(&rcx39) = 0;
            *reinterpret_cast<int32_t*>(&rcx39 + 4) = 0;
            do {
                rdi40 = rcx39;
                ++rcx39;
                if (*reinterpret_cast<void***>(rsi2)) {
                    if (*reinterpret_cast<signed char*>(&ebp5)) {
                        r8_41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) / ((reinterpret_cast<unsigned char>(r10_36) + reinterpret_cast<unsigned char>(rcx39) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx39)));
                    } else {
                        r8_41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) % reinterpret_cast<unsigned char>(rcx39));
                    }
                    *reinterpret_cast<int32_t*>(&rax42) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax42) = reinterpret_cast<uint1_t>(r8_41 != rdi40);
                    rdx43 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi2 + 16) + reinterpret_cast<unsigned char>(r8_41) * 8);
                    rax44 = r9_37 + rax42 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx43) < reinterpret_cast<unsigned char>(rax44)) {
                        *reinterpret_cast<void***>(rsi2 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi2 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax44) - reinterpret_cast<unsigned char>(*rdx43)));
                        *rdx43 = rax44;
                        *reinterpret_cast<void***>(rsi2) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi2 + 8)) < reinterpret_cast<unsigned char>(r11_38))));
                    }
                }
                rsi2 = rsi2 + 24;
            } while (r12_4 != rcx39);
        }
        ++rbx13;
    } while (reinterpret_cast<unsigned char>(rbx13) < reinterpret_cast<unsigned char>(r10_36));
    goto addr_81bd_28;
}