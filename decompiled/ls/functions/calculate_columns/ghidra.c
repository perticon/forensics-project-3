ulong calculate_columns(char param_1)

{
  ulong *puVar1;
  undefined8 *puVar2;
  undefined auVar3 [16];
  ulong uVar4;
  long lVar5;
  undefined8 *puVar6;
  ulong uVar7;
  ulong uVar8;
  long lVar9;
  ulong uVar10;
  ulong uVar11;
  char *pcVar12;
  ulong uVar13;
  ulong uVar14;
  bool bVar15;
  
  uVar14 = cwd_n_used;
  if (max_idx == 0) {
    if (column_info_alloc_3 < cwd_n_used) {
LAB_00107fb5:
      column_info = (char *)xreallocarray(column_info,uVar14,0x30);
      uVar11 = uVar14 * 2;
      goto LAB_00107fd4;
    }
LAB_001081fc:
    if (cwd_n_used == 0) goto LAB_00108208;
LAB_0010806c:
    uVar11 = cwd_n_used;
    pcVar12 = column_info;
    lVar5 = 3;
    uVar13 = 0;
    do {
      puVar2 = *(undefined8 **)(pcVar12 + lVar5 * 8 + -8);
      pcVar12[lVar5 * 8 + -0x18] = '\x01';
      *(long *)(pcVar12 + lVar5 * 8 + -0x10) = lVar5;
      puVar6 = puVar2;
      do {
        *puVar6 = 3;
        bVar15 = puVar2 + uVar13 != puVar6;
        puVar6 = puVar6 + 1;
      } while (bVar15);
      uVar13 = uVar13 + 1;
      lVar5 = lVar5 + 3;
    } while (uVar13 < uVar14);
    if (uVar11 == 0) goto LAB_001081bd;
  }
  else {
    if (max_idx < cwd_n_used) {
      uVar14 = max_idx;
      if (max_idx <= column_info_alloc_3) goto LAB_0010806c;
      if (max_idx < max_idx >> 1) goto LAB_00107fb5;
    }
    else {
      if (cwd_n_used <= column_info_alloc_3) goto LAB_001081fc;
      if (cwd_n_used < max_idx >> 1) goto LAB_00107fb5;
    }
    column_info = (char *)xreallocarray(column_info,max_idx,0x18);
    uVar11 = max_idx;
LAB_00107fd4:
    auVar3 = ZEXT816(uVar11 - column_info_alloc_3) * ZEXT816(column_info_alloc_3 + 1 + uVar11);
    if ((CARRY8(column_info_alloc_3 + 1,uVar11) != false) || (SUB168(auVar3 >> 0x40,0) != 0)) {
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
    lVar5 = xnmalloc(SUB168(auVar3,0) >> 1,8);
    pcVar12 = column_info;
    if (column_info_alloc_3 < uVar11) {
      lVar9 = column_info_alloc_3 * 8 + 8;
      do {
        *(long *)(pcVar12 + lVar9 * 3 + -8) = lVar5;
        lVar5 = lVar5 + lVar9;
        lVar9 = lVar9 + 8;
      } while (uVar11 * 8 + 8 != lVar9);
    }
    column_info_alloc_3 = uVar11;
    if (uVar14 != 0) goto LAB_0010806c;
LAB_00108208:
    if (cwd_n_used == 0) {
      return uVar14;
    }
  }
  uVar11 = 0;
  do {
    lVar5 = length_of_file_name_and_frills(*(undefined8 *)(sorted_file + uVar11 * 8));
    uVar4 = cwd_n_used;
    uVar13 = line_length;
    if (uVar14 != 0) {
      uVar7 = 0;
      pcVar12 = column_info;
      do {
        uVar8 = uVar7 + 1;
        if (*pcVar12 != '\0') {
          if (param_1 == '\0') {
            uVar10 = uVar11 % uVar8;
          }
          else {
            uVar10 = SUB168(ZEXT816(uVar11) /
                            (ZEXT816((uVar4 - 1) + uVar8) / ZEXT816(uVar8) &
                            (undefined  [16])0xffffffffffffffff),0);
          }
          puVar1 = (ulong *)(*(long *)(pcVar12 + 0x10) + uVar10 * 8);
          uVar7 = lVar5 + (ulong)(uVar10 != uVar7) * 2;
          uVar10 = *puVar1;
          if (uVar10 < uVar7) {
            *(ulong *)(pcVar12 + 8) = *(long *)(pcVar12 + 8) + (uVar7 - uVar10);
            *puVar1 = uVar7;
            *pcVar12 = *(ulong *)(pcVar12 + 8) < uVar13;
          }
        }
        pcVar12 = pcVar12 + 0x18;
        uVar7 = uVar8;
      } while (uVar14 != uVar8);
    }
    uVar11 = uVar11 + 1;
  } while (uVar11 < uVar4);
LAB_001081bd:
  if (1 < uVar14) {
    pcVar12 = column_info + uVar14 * 0x18 + -0x18;
    do {
      if (*pcVar12 != '\0') {
        return uVar14;
      }
      uVar14 = uVar14 - 1;
      pcVar12 = pcVar12 + -0x18;
    } while (uVar14 != 1);
  }
  return uVar14;
}