undefined4 main(uint param_1,char *param_2)

{
  undefined *puVar1;
  FILE *pFVar2;
  char cVar3;
  byte bVar4;
  int iVar5;
  uint uVar6;
  void *pvVar7;
  char **ppcVar8;
  long lVar9;
  undefined8 uVar10;
  ulong uVar11;
  size_t sVar12;
  void *pvVar13;
  char *pcVar14;
  undefined8 uVar15;
  undefined1 *puVar16;
  undefined8 extraout_RDX;
  char *pcVar17;
  long lVar18;
  void **__ptr;
  char *pcVar19;
  undefined8 *puVar20;
  undefined *puVar21;
  undefined8 *puVar22;
  undefined *in_R9;
  undefined8 *in_R10;
  byte *pbVar23;
  long in_FS_OFFSET;
  bool bVar24;
  undefined auVar25 [16];
  undefined8 local_98;
  int local_90;
  int local_8c;
  ulong local_88;
  char *local_80;
  undefined *local_78;
  int local_70;
  char local_69;
  byte *local_60;
  undefined8 local_58;
  undefined8 local_50;
  ushort local_43;
  undefined local_41;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*(undefined8 *)param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  exit_failure = 2;
  atexit(close_stdout);
  print_dir_name = 1;
  exit_status = 0;
  pending_dirs = (void **)0x0;
  current_time._0_8_ = 0x8000000000000000;
  current_time._8_8_ = 0xffffffffffffffff;
  local_88 = 0xffffffffffffffff;
  local_78 = (undefined *)0xffffffffffffffff;
  local_8c = -1;
  local_90 = -1;
  local_70 = -1;
  local_98 = (undefined8 *)CONCAT44(local_98._4_4_,0xffffffff);
  local_69 = '\0';
  local_80 = (char *)0x0;
LAB_00104e20:
  do {
    puVar16 = long_options;
    local_58 = (undefined *)CONCAT44(local_58._4_4_,0xffffffff);
    puVar20 = (undefined8 *)param_2;
    puVar21 = (undefined *)(ulong)param_1;
    puVar22 = &local_58;
    iVar5 = getopt_long((undefined *)(ulong)param_1,param_2,
                        "abcdfghiklmnopqrstuvw:xABCDFGHI:LNQRST:UXZ1");
    if (iVar5 == -1) {
      if (output_block_size == 0) {
        pcVar17 = getenv("LS_BLOCK_SIZE");
        human_options(pcVar17,&human_output_opts,&output_block_size);
        if ((pcVar17 != (char *)0x0) || (pcVar17 = getenv("BLOCK_SIZE"), pcVar17 != (char *)0x0)) {
          file_human_output_opts = human_output_opts;
          file_output_block_size = output_block_size;
        }
        if (local_69 != '\0') {
          human_output_opts = 0;
          output_block_size = 0x400;
        }
      }
      if ((int)local_98 < 0) {
        if (ls_mode == 1) goto LAB_00106441;
        if (ls_mode == 2) goto LAB_00106372;
        format = 0;
        goto LAB_001052b1;
      }
      format = (int)local_98;
      if (2 < (int)local_98 - 2U) goto LAB_001052b1;
      goto LAB_00104eb5;
    }
    switch(iVar5) {
    case 0x31:
      local_98 = (undefined8 *)((ulong)local_98 & 0xffffffff00000000 | (ulong)((int)local_98 != 0));
      break;
    case 0x41:
      ignore_mode = 1;
      break;
    case 0x42:
      add_ignore_pattern(&DAT_0011bc5a);
      add_ignore_pattern();
      break;
    case 0x43:
      local_98 = (undefined8 *)CONCAT44(local_98._4_4_,2);
      break;
    case 0x44:
      dired = 1;
      break;
    case 0x46:
      if (optarg != (char *)0x0) {
        in_R9 = (undefined *)0x1;
        in_R10 = puVar22;
        lVar18 = __xargmatch_internal
                           ("--classify",optarg,when_args,when_types,4,argmatch_die,1,puVar22);
        if ((*(int *)(when_types + lVar18 * 4) != 1) &&
           ((*(int *)(when_types + lVar18 * 4) != 2 || (cVar3 = stdout_isatty(), cVar3 == '\0'))))
        break;
      }
      indicator_style = 3;
      break;
    case 0x47:
      print_group = 0;
      break;
    case 0x48:
      dereference = 2;
      break;
    case 0x49:
      add_ignore_pattern();
      break;
    case 0x4c:
      dereference = 4;
      break;
    case 0x4e:
      local_90 = 0;
      break;
    case 0x51:
      local_90 = 5;
      break;
    case 0x52:
      recursive = '\x01';
      break;
    case 0x53:
      local_8c = 3;
      break;
    case 0x54:
      in_R9 = (undefined *)dcgettext(0,"invalid tab size",5);
      local_78 = (undefined *)
                 xnumtoumax(optarg,0,0,0x7fffffffffffffff,&DAT_0011bb0a,in_R9,2,puVar16);
      break;
    case 0x55:
      local_8c = 6;
      break;
    case 0x58:
      local_8c = 1;
      break;
    case 0x5a:
      print_scontext = '\x01';
      break;
    case 0x61:
      ignore_mode = 2;
      break;
    case 0x62:
      local_90 = 7;
      break;
    case 99:
      time_type = 1;
      break;
    case 100:
      immediate_dirs = '\x01';
      break;
    case 0x66:
      ignore_mode = 2;
      if ((int)local_98 == 0) {
        local_98 = (undefined8 *)CONCAT44(local_98._4_4_,0xffffffff);
      }
      print_with_color = 0;
      print_hyperlink = 0;
      print_block_size = '\0';
      local_8c = 6;
      break;
    case 0x67:
      print_owner = 0;
      local_98 = (undefined8 *)((ulong)local_98._4_4_ << 0x20);
      break;
    case 0x68:
      human_output_opts = 0xb0;
      file_human_output_opts = 0xb0;
      output_block_size = 1;
      file_output_block_size = 1;
      break;
    case 0x69:
      print_inode = 1;
      break;
    case 0x6b:
      local_69 = '\x01';
      break;
    case 0x6c:
      local_98 = (undefined8 *)((ulong)local_98._4_4_ << 0x20);
      break;
    case 0x6d:
      local_98 = (undefined8 *)CONCAT44(local_98._4_4_,4);
      break;
    case 0x6e:
      numeric_ids = 1;
      local_98 = (undefined8 *)((ulong)local_98._4_4_ << 0x20);
      break;
    case 0x6f:
      print_group = 0;
      local_98 = (undefined8 *)((ulong)local_98._4_4_ << 0x20);
      break;
    case 0x70:
      indicator_style = 1;
      break;
    case 0x71:
      local_70 = 1;
      break;
    case 0x72:
      sort_reverse = 1;
      break;
    case 0x73:
      print_block_size = '\x01';
      break;
    case 0x74:
      local_8c = 5;
      break;
    case 0x75:
      time_type = 2;
      break;
    case 0x76:
      local_8c = 4;
      break;
    case 0x77:
      local_88 = decode_line_length();
      if (-1 < (long)local_88) break;
      param_2 = (char *)quote(optarg);
      uVar10 = dcgettext(0,"invalid line width",5);
      error(2,0,"%s: %s",uVar10,param_2);
    case 0x80:
      print_author = 1;
      break;
    case 0x78:
      local_98 = (undefined8 *)CONCAT44(local_98._4_4_,3);
      break;
    case 0x81:
      iVar5 = human_options(optarg,&human_output_opts,&output_block_size);
      if (iVar5 != 0) {
        xstrtol_fatal(iVar5,(ulong)local_58 & 0xffffffff,0,long_options,optarg);
        goto LAB_0010677e;
      }
      file_human_output_opts = human_output_opts;
      file_output_block_size = output_block_size;
      break;
    case 0x82:
      if (optarg == (char *)0x0) {
LAB_001059ca:
        bVar4 = 1;
      }
      else {
        in_R9 = argmatch_die;
        lVar18 = __xargmatch_internal("--color",optarg,when_args,when_types,4,argmatch_die,1);
        if (*(int *)(when_types + lVar18 * 4) == 1) goto LAB_001059ca;
        bVar4 = 0;
        if (*(int *)(when_types + lVar18 * 4) == 2) {
          bVar4 = stdout_isatty();
        }
      }
      print_with_color = bVar4 & 1;
      break;
    case 0x83:
      dereference = 3;
      break;
    case 0x84:
      indicator_style = 2;
      break;
    case 0x85:
      in_R9 = argmatch_die;
      lVar18 = __xargmatch_internal
                         ("--format",optarg,format_args,format_types,4,argmatch_die,1,puVar20);
      local_98 = (undefined8 *)
                 ((ulong)local_98 & 0xffffffff00000000 | (ulong)*(uint *)(format_types + lVar18 * 4)
                 );
      break;
    case 0x86:
      local_98 = (undefined8 *)((ulong)local_98 & 0xffffffff00000000);
      local_80 = "full-iso";
      break;
    case 0x87:
      directories_first = 1;
      break;
    case 0x88:
      ppcVar8 = (char **)xmalloc();
      *ppcVar8 = optarg;
      ppcVar8[1] = (char *)hide_patterns;
      hide_patterns = ppcVar8;
      break;
    case 0x89:
      if (optarg == (char *)0x0) {
LAB_001059b3:
        bVar4 = 1;
      }
      else {
        in_R9 = argmatch_die;
        lVar18 = __xargmatch_internal
                           ("--hyperlink",optarg,when_args,when_types,4,argmatch_die,1,in_R10);
        if (*(int *)(when_types + lVar18 * 4) == 1) goto LAB_001059b3;
        bVar4 = 0;
        if (*(int *)(when_types + lVar18 * 4) == 2) {
          bVar4 = stdout_isatty();
        }
      }
      print_hyperlink = bVar4 & 1;
      break;
    case 0x8a:
      in_R9 = puVar21;
      lVar18 = __xargmatch_internal
                         ("--indicator-style",optarg,indicator_style_args,indicator_style_types,4,
                          argmatch_die,1,puVar21);
      indicator_style = *(uint *)(indicator_style_types + lVar18 * 4);
      break;
    case 0x8b:
      in_R9 = argmatch_die;
      lVar18 = __xargmatch_internal
                         ("--quoting-style",optarg,quoting_style_args,quoting_style_vals,4,
                          argmatch_die,1,extraout_RDX);
      local_90 = *(int *)(quoting_style_vals + lVar18 * 4);
      break;
    case 0x8c:
      goto switchD_00104e59_caseD_8c;
    case 0x8d:
      human_output_opts = 0x90;
      file_human_output_opts = 0x90;
      output_block_size = 1;
      file_output_block_size = 1;
      break;
    case 0x8e:
      in_R9 = argmatch_die;
      lVar18 = __xargmatch_internal
                         ("--sort",optarg,sort_args,sort_types,4,argmatch_die,1,
                          (long)&switchD_00104e59::switchdataD_0011a190 +
                          (long)(int)(&switchD_00104e59::switchdataD_0011a190)[iVar5 + 0x83]);
      local_8c = *(int *)(sort_types + lVar18 * 4);
      break;
    case 0x8f:
      in_R10 = (undefined8 *)0x1;
      puVar21 = argmatch_die;
      lVar18 = __xargmatch_internal("--time",optarg,time_args,time_types,4,argmatch_die,1,in_R9);
      time_type = *(int *)(time_types + lVar18 * 4);
      in_R9 = puVar21;
      break;
    case 0x90:
      goto switchD_00104e59_caseD_90;
    case 0x91:
      eolbyte = '\0';
      print_with_color = 0;
      local_98 = (undefined8 *)((ulong)local_98 & 0xffffffff00000000 | (ulong)((int)local_98 != 0));
      local_90 = 0;
switchD_00104e59_caseD_8c:
      local_70 = 0;
      break;
    case -0x83:
      pcVar17 = "ls";
      iVar5 = ls_mode;
      if (ls_mode != 1) {
        pcVar17 = "dir";
        iVar5 = 0x11bb70;
        if (ls_mode != 2) {
          pcVar17 = "vdir";
        }
      }
      version_etc(stdout,pcVar17,"GNU coreutils",Version,"Richard M. Stallman","David MacKenzie",0,
                  iVar5);
                    /* WARNING: Subroutine does not return */
      exit(0);
    case -0x82:
      goto switchD_00104e59_caseD_ffffff7e;
    default:
      goto switchD_00104e59_caseD_ffffff7f;
    }
  } while( true );
switchD_00104e59_caseD_90:
  local_80 = optarg;
  goto LAB_00104e20;
switchD_00104e59_caseD_ffffff7e:
  usage(0);
LAB_00106441:
  cVar3 = stdout_isatty();
  if (cVar3 == '\0') {
    format = 1;
LAB_001052b1:
    if (print_with_color != 0) goto LAB_00104eb5;
    uVar11 = local_88;
    if (local_88 != 0xffffffffffffffff) goto LAB_00104ec1;
LAB_001052ca:
    local_88 = 0x50;
    uVar11 = local_88;
  }
  else {
LAB_00106372:
    format = 2;
LAB_00104eb5:
    uVar11 = local_88;
    if (local_88 == 0xffffffffffffffff) {
      cVar3 = stdout_isatty();
      if ((cVar3 != '\0') && (iVar5 = ioctl(1,0x5413,&local_58), -1 < iVar5)) {
        uVar11 = (ulong)local_58._2_2_;
        if (local_58._2_2_ != 0) goto LAB_00104ec1;
      }
      pcVar17 = getenv("COLUMNS");
      if ((pcVar17 != (char *)0x0) && (*pcVar17 != '\0')) {
        local_88 = decode_line_length(pcVar17);
        uVar11 = local_88;
        if (-1 < (long)local_88) goto LAB_00104ec1;
        uVar10 = quote(pcVar17);
        uVar15 = dcgettext(0,"ignoring invalid width in environment variable COLUMNS: %s",5);
        error(0,0,uVar15,uVar10);
      }
      goto LAB_001052ca;
    }
  }
LAB_00104ec1:
  local_88 = uVar11;
  line_length = local_88;
  max_idx = (local_88 / 3 + 1) - (ulong)(local_88 % 3 == 0);
  puVar21 = tabsize;
  if ((format - 2U < 3) && (puVar21 = local_78, (long)local_78 < 0)) {
    tabsize = &DAT_00000008;
    pcVar17 = getenv("TABSIZE");
    puVar21 = tabsize;
    if ((pcVar17 != (char *)0x0) &&
       (iVar5 = xstrtoumax(pcVar17,0,0,&local_58,&DAT_0011bb0a), puVar21 = local_58, iVar5 != 0)) {
      quote(pcVar17);
      uVar10 = dcgettext(0,"ignoring invalid tab size in environment variable TABSIZE: %s",5);
      error(0,0,uVar10);
      puVar21 = tabsize;
    }
  }
  tabsize = puVar21;
  bVar4 = (byte)local_70;
  if ((local_70 == -1) && (bVar4 = 0, ls_mode == 1)) {
    bVar4 = stdout_isatty();
  }
  qmark_funny_chars = bVar4 & 1;
  if (local_90 < 0) {
    pcVar17 = getenv("QUOTING_STYLE");
    if (pcVar17 == (char *)0x0) goto LAB_001061c3;
    iVar5 = argmatch(pcVar17,quoting_style_args,quoting_style_vals);
    if (iVar5 < 0) goto LAB_0010670f;
    local_90 = *(int *)(quoting_style_vals + (long)iVar5 * 4);
    if (local_90 < 0) goto LAB_001061c3;
  }
LAB_00104f2c:
  set_quoting_style(0,local_90);
LAB_00104f37:
  iVar5 = get_quoting_style(0);
  if (((format == 0) || ((format - 2U < 2 && (line_length != 0)))) &&
     ((iVar5 == 3 || ((iVar5 == 6 || (iVar5 == 1)))))) {
    align_variable_outer_quotes = 1;
    filename_quoting_options = clone_quoting_options(0);
  }
  else {
    align_variable_outer_quotes = 0;
    filename_quoting_options = clone_quoting_options(0);
    if (iVar5 == 7) {
      set_char_quoting(filename_quoting_options,0x20,1);
    }
  }
  if (1 < indicator_style) {
    pcVar17 = &DAT_0011bd16 + (indicator_style - 2);
    cVar3 = (&DAT_0011bd16)[indicator_style - 2];
    while (cVar3 != '\0') {
      pcVar17 = pcVar17 + 1;
      set_char_quoting(filename_quoting_options,(int)cVar3,1);
      cVar3 = *pcVar17;
    }
  }
  dirname_quoting_options = clone_quoting_options(0);
  set_char_quoting(dirname_quoting_options,0x3a,1);
  dired = (print_hyperlink ^ 1) & format == 0 & dired;
  if ((int)eolbyte < (int)(uint)dired) {
    uVar10 = dcgettext(0,"--dired and --zero are incompatible",5);
    auVar25 = error(2,0,uVar10);
    puVar20 = local_98;
    local_98 = SUB168(auVar25,0);
    (*(code *)PTR___libc_start_main_00124fa8)
              (main,puVar20,&local_90,0,0,SUB168(auVar25 >> 0x40,0),&local_98);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  sort_type = local_8c;
  if (local_8c < 0) {
    if (format == 0) {
      sort_type = 0;
      goto LAB_00105a9f;
    }
    if (2 < time_type - 1U) {
      sort_type = 0;
      goto LAB_00105021;
    }
    sort_type = 5;
LAB_0010502f:
    uVar6 = optind;
    lVar18 = (long)(int)optind;
    if (print_with_color == 0) goto LAB_00105043;
    local_60 = (byte *)getenv("LS_COLORS");
    if ((local_60 != (byte *)0x0) && (*local_60 != 0)) {
      local_43 = 0x3f3f;
      local_41 = 0;
      color_buf = (undefined *)xstrdup(local_60);
      local_98 = (undefined8 *)param_2;
      local_58 = color_buf;
      goto LAB_00105ec1;
    }
    pcVar17 = getenv("COLORTERM");
    if ((pcVar17 != (char *)0x0) && (*pcVar17 != '\0')) goto LAB_00106000;
    pcVar17 = getenv("TERM");
    if ((pcVar17 == (char *)0x0) || (*pcVar17 == '\0')) goto LAB_00106311;
    local_98 = (undefined8 *)((ulong)local_98 & 0xffffffff00000000 | (ulong)uVar6);
    pcVar19 = "# Configuration file for dircolors, a utility to help you set the";
    goto LAB_001062d8;
  }
LAB_00105021:
  if (format != 0) goto LAB_0010502f;
LAB_00105a9f:
  pcVar17 = local_80;
  if ((local_80 != (char *)0x0) ||
     (pcVar17 = getenv("TIME_STYLE"), local_80 = pcVar17, pcVar17 != (char *)0x0)) {
    while (iVar5 = strncmp(pcVar17,"posix-",6), iVar5 == 0) {
      cVar3 = hard_locale(2);
      if (cVar3 == '\0') goto LAB_0010502f;
      pcVar17 = pcVar17 + 6;
    }
    local_80 = pcVar17;
    if (*pcVar17 != '+') goto LAB_00105afa;
    pcVar17 = pcVar17 + 1;
    pcVar14 = strchr(pcVar17,10);
    pcVar19 = pcVar17;
    if (pcVar14 == (char *)0x0) goto LAB_00105b57;
    pcVar19 = strchr(pcVar14 + 1,10);
    if (pcVar19 == (char *)0x0) {
      *pcVar14 = '\0';
      pcVar19 = pcVar14 + 1;
      goto LAB_00105b57;
    }
    uVar10 = quote(pcVar17);
    uVar15 = dcgettext(0,"invalid time style format %s",5);
    error(2,0,uVar15,uVar10);
LAB_0010670f:
    quote();
    uVar10 = dcgettext(0,"ignoring invalid value of environment variable QUOTING_STYLE: %s",5);
    error(0,0,uVar10);
LAB_001061c3:
    local_90 = 7;
    if (ls_mode != 1) goto LAB_00104f2c;
    cVar3 = stdout_isatty();
    if (cVar3 != '\0') goto code_r0x001061e5;
    goto LAB_00104f37;
  }
  local_80 = "locale";
LAB_00105afa:
  puVar16 = time_style_args;
  lVar18 = argmatch(local_80,time_style_args,time_style_types);
  if (-1 < lVar18) {
    if (lVar18 == 2) {
      long_time_format._0_8_ = "%Y-%m-%d ";
      long_time_format._8_8_ = "%m-%d %H:%M";
      pcVar17 = long_time_format._0_8_;
      pcVar19 = long_time_format._8_8_;
    }
    else {
      pcVar17 = long_time_format._0_8_;
      pcVar19 = long_time_format._8_8_;
      if (lVar18 < 3) {
        if (lVar18 == 0) {
          long_time_format._8_8_ = "%Y-%m-%d %H:%M:%S.%N %z";
          long_time_format._0_8_ = "%Y-%m-%d %H:%M:%S.%N %z";
          pcVar17 = long_time_format._0_8_;
          pcVar19 = long_time_format._8_8_;
        }
        else if (lVar18 == 1) {
          long_time_format._8_8_ = "%Y-%m-%d %H:%M";
          long_time_format._0_8_ = "%Y-%m-%d %H:%M";
          pcVar17 = long_time_format._0_8_;
          pcVar19 = long_time_format._8_8_;
        }
      }
      else if ((lVar18 == 3) &&
              (cVar3 = hard_locale(2), pcVar17 = long_time_format._0_8_,
              pcVar19 = long_time_format._8_8_, cVar3 != '\0')) {
        long_time_format._0_8_ = (char *)dcgettext(0,long_time_format._0_8_,2);
        pcVar19 = (char *)dcgettext(0,long_time_format._8_8_,2);
        pcVar17 = long_time_format._0_8_;
      }
    }
LAB_00105b57:
    long_time_format._8_8_ = pcVar19;
    long_time_format._0_8_ = pcVar17;
    abformat_init();
    goto LAB_0010502f;
  }
  param_2 = "  - [posix-]%s\n";
  argmatch_invalid("time style",local_80,lVar18);
  pFVar2 = stderr;
  pcVar17 = (char *)dcgettext(0,"Valid arguments are:\n",5);
  fputs_unlocked(pcVar17,pFVar2);
  pcVar17 = "full-iso";
  do {
    puVar16 = (undefined1 *)((long)puVar16 + 8);
    __fprintf_chk(stderr,1,"  - [posix-]%s\n",pcVar17);
    pFVar2 = stderr;
    pcVar17 = *(char **)puVar16;
  } while (pcVar17 != (char *)0x0);
  pcVar17 = (char *)dcgettext(0,"  - +FORMAT (e.g., +%H:%M) for a \'date\'-style format\n",5);
  fputs_unlocked(pcVar17,pFVar2);
switchD_00104e59_caseD_ffffff7f:
  usage();
  goto switchD_00104e59_caseD_ffffff7e;
code_r0x001061e5:
  local_90 = 3;
  goto LAB_00104f2c;
LAB_00105ec1:
  bVar4 = *local_60;
  if (bVar4 == 0x2a) {
    pbVar23 = local_60 + 1;
    pvVar13 = (void *)xmalloc(0x28);
    pvVar7 = pvVar13;
    *(void **)((long)pvVar13 + 0x20) = color_ext_list;
    color_ext_list = pvVar7;
    *(undefined **)((long)pvVar13 + 8) = local_58;
    local_60 = pbVar23;
    cVar3 = get_funky_string(&local_58,&local_60,1);
    param_2 = (char *)local_98;
    if ((cVar3 == '\0') ||
       (pbVar23 = local_60 + 1, bVar4 = *local_60, local_60 = pbVar23, bVar4 != 0x3d))
    goto LAB_00105fa4;
    *(undefined **)((long)pvVar13 + 0x18) = local_58;
    cVar3 = get_funky_string(&local_58,&local_60,0);
    param_2 = (char *)local_98;
    if (cVar3 == '\0') goto LAB_00105fa4;
    goto LAB_00105ec1;
  }
  if (bVar4 == 0x3a) {
    local_60 = local_60 + 1;
    goto LAB_00105ec1;
  }
  param_2 = (char *)local_98;
  if (bVar4 == 0) goto LAB_00105ff2;
  local_43 = local_43 & 0xff00 | (ushort)bVar4;
  if (local_60[1] == 0) goto LAB_00105fa4;
  local_43 = CONCAT11(local_60[1],bVar4);
  pbVar23 = local_60 + 2;
  local_60 = local_60 + 3;
  if (*pbVar23 != 0x3d) goto LAB_00105fa4;
  lVar9 = 0;
  pcVar17 = "lc";
  while (iVar5 = strcmp((char *)&local_43,pcVar17), iVar5 != 0) {
    lVar9 = lVar9 + 1;
    pcVar17 = *(char **)(indicator_name + lVar9 * 8);
    if (pcVar17 == (char *)0x0) goto LAB_00105f71;
  }
  *(undefined **)(color_indicator + (long)(int)lVar9 * 0x10 + 8) = local_58;
  cVar3 = get_funky_string(&local_58,&local_60,0);
  if (cVar3 != '\0') goto LAB_00105ec1;
LAB_00105f71:
  param_2 = (char *)local_98;
  quote(&local_43);
  uVar10 = dcgettext(0,"unrecognized prefix: %s",5);
  error(0,0,uVar10);
LAB_00105fa4:
  uVar10 = dcgettext(0,"unparsable value for LS_COLORS environment variable",5);
  error(0,0,uVar10);
  free(color_buf);
  pvVar7 = color_ext_list;
  while (pvVar7 != (void *)0x0) {
    pvVar13 = *(void **)((long)pvVar7 + 0x20);
    free(pvVar7);
    pvVar7 = pvVar13;
  }
  print_with_color = 0;
LAB_00105ff2:
  if ((color_indicator._112_8_ == 6) &&
     (iVar5 = strncmp(color_indicator._120_8_,"target",6), iVar5 == 0)) {
    color_symlink_as_referent = '\x01';
  }
  goto LAB_00106000;
  while( true ) {
    sVar12 = strlen(pcVar19);
    pcVar19 = pcVar19 + sVar12 + 1;
    if ((char *)0x12c7 < pcVar19 + -0x11a7a0) break;
LAB_001062d8:
    iVar5 = strncmp(pcVar19,"TERM ",5);
    if ((iVar5 == 0) && (iVar5 = fnmatch(pcVar19 + 5,pcVar17,0), iVar5 == 0)) {
      lVar18 = (long)(int)local_98;
      goto LAB_00106000;
    }
  }
  lVar18 = (long)(int)local_98;
LAB_00106311:
  print_with_color = 0;
LAB_00106000:
  if (print_with_color == 0) {
LAB_00105043:
    if (directories_first != 0) goto LAB_0010504c;
  }
  else {
    tabsize = (undefined *)0x0;
    if ((((directories_first != 0) || (cVar3 = is_colored(0xd), cVar3 != '\0')) ||
        ((cVar3 = is_colored(0xe), cVar3 != '\0' && (color_symlink_as_referent != '\0')))) ||
       ((cVar3 = is_colored(0xc), cVar3 != '\0' && (format == 0)))) {
LAB_0010504c:
      check_symlink_mode = 1;
    }
  }
  if (((dereference == 0) && (dereference = 1, immediate_dirs == '\0')) && (indicator_style != 3)) {
    dereference = (-(uint)(format == 0) & 0xfffffffe) + 3;
  }
  if (recursive != '\0') {
    active_dir_set = hash_initialize(0x1e,0,dev_ino_hash,dev_ino_compare,dev_ino_free);
    if (active_dir_set == 0) {
LAB_0010677e:
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
    _obstack_begin(dev_ino_obstack,0,0,PTR_malloc_00124fd8,PTR_free_00124fa0);
  }
  getenv("TZ");
  localtz = tzalloc();
  puVar1 = PTR_malloc_00124fd8;
  puVar21 = PTR_free_00124fa0;
  if ((((sort_type - 3U & 0xfffffffd) == 0) || (format == 0)) ||
     ((print_scontext != '\0' || (print_block_size != '\0')))) {
    format_needs_stat = 1;
    bVar4 = 0;
  }
  else {
    format_needs_stat = 0;
    bVar4 = 1;
    if (((recursive == '\0') && (print_with_color == 0)) && (indicator_style == 0)) {
      bVar4 = directories_first;
    }
  }
  format_needs_type = bVar4 & 1;
  if (dired != 0) {
    _obstack_begin(dired_obstack,0,0,PTR_malloc_00124fd8,PTR_free_00124fa0);
    _obstack_begin(subdired_obstack,0,0,puVar1,puVar21);
  }
  if (print_hyperlink != 0) {
    uVar11 = 0;
LAB_00106127:
    do {
      iVar5 = (int)uVar11;
      if (uVar11 < 0x5b) {
        bVar24 = true;
        if ((iVar5 < 0x41) && (9 < iVar5 - 0x30U)) goto joined_r0x00106100;
      }
      else {
        bVar24 = true;
        if (0x19 < iVar5 - 0x61U) {
joined_r0x00106100:
          if ((iVar5 - 0x2dU < 2) || (iVar5 == 0x7e)) {
            RFC3986[uVar11] = RFC3986[uVar11] | 1;
            uVar11 = uVar11 + 1;
            if (uVar11 == 0x100) break;
            goto LAB_00106127;
          }
          bVar24 = iVar5 == 0x5f;
        }
      }
      RFC3986[uVar11] = RFC3986[uVar11] | bVar24;
      uVar11 = uVar11 + 1;
    } while (uVar11 != 0x100);
    hostname = (undefined *)xgethostname();
    if (hostname == (undefined *)0x0) {
      hostname = &DAT_0011bb0a;
    }
  }
  cwd_n_alloc = 100;
  cwd_file = xnmalloc(100,0xd0);
  iVar5 = param_1 - (int)lVar18;
  cwd_n_used = 0;
  clear_files();
  if (iVar5 < 1) {
    if (immediate_dirs == '\0') {
      queue_directory(&DAT_0011bde2,0,1);
    }
    else {
      gobble_file_constprop_0(&DAT_0011bde2,3,1,&DAT_0011bb0a);
    }
    if (cwd_n_used != 0) goto LAB_0010606f;
LAB_00105b96:
    if (pending_dirs == (void **)0x0) goto LAB_001059e1;
    __ptr = pending_dirs;
    if (pending_dirs[3] == (void *)0x0) {
      print_dir_name = 0;
    }
  }
  else {
    do {
      puVar20 = (undefined8 *)((long)param_2 + lVar18 * 8);
      lVar18 = lVar18 + 1;
      gobble_file_constprop_0(*puVar20,0,1,&DAT_0011bb0a);
    } while ((int)lVar18 < (int)param_1);
    if (cwd_n_used == 0) {
LAB_001051a6:
      if (1 < iVar5) goto LAB_001051f5;
      goto LAB_00105b96;
    }
LAB_0010606f:
    sort_files();
    if (immediate_dirs == '\0') {
      extract_dirs_from_files(0,1);
    }
    if (cwd_n_used == 0) goto LAB_001051a6;
    print_current_files();
    if (pending_dirs == (void **)0x0) goto LAB_001059e1;
    dired_pos = dired_pos + 1;
    pcVar17 = stdout->_IO_write_ptr;
    if (stdout->_IO_write_end <= pcVar17) {
      __overflow(stdout,10);
      goto LAB_001051f5;
    }
    stdout->_IO_write_ptr = pcVar17 + 1;
    *pcVar17 = '\n';
    __ptr = pending_dirs;
  }
  do {
    pending_dirs = (void **)__ptr[3];
    if ((active_dir_set == 0) || (*__ptr != (void *)0x0)) {
      print_dir(*__ptr,__ptr[1],*(undefined *)(__ptr + 2));
      free(*__ptr);
      free(__ptr[1]);
      free(__ptr);
      print_dir_name = 1;
    }
    else {
      if ((ulong)(dev_ino_obstack._24_8_ - dev_ino_obstack._16_8_) < 0x10) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("dev_ino_size <= obstack_object_size (&dev_ino_obstack)","src/ls.c",0x41d,
                      "dev_ino_pop");
      }
      local_58 = *(undefined **)(dev_ino_obstack._24_8_ + -0x10);
      local_50 = *(undefined8 *)(dev_ino_obstack._24_8_ + -8);
      dev_ino_obstack._24_8_ = dev_ino_obstack._24_8_ + -0x10;
      pvVar7 = (void *)hash_remove(active_dir_set,&local_58);
      if (pvVar7 == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("found","src/ls.c",0x70d,(char *)&__PRETTY_FUNCTION___14);
      }
      free(pvVar7);
      free(*__ptr);
      free(__ptr[1]);
      free(__ptr);
    }
LAB_001051f5:
    __ptr = pending_dirs;
  } while (pending_dirs != (void **)0x0);
LAB_001059e1:
  if ((print_with_color != 0) && (used_color != '\0')) {
    if ((color_indicator._0_8_ != 2) ||
       (((*color_indicator._8_8_ != 0x5b1b || (color_indicator._16_8_ != 1)) ||
        (*color_indicator._24_8_ != 'm')))) {
      put_indicator(color_indicator);
      put_indicator(0x125070);
    }
    fflush_unlocked(stdout);
    signal_setup();
    for (iVar5 = stop_signal_count; iVar5 != 0; iVar5 = iVar5 + -1) {
      raise(0x13);
    }
    if (interrupt_signal != 0) {
      raise(interrupt_signal);
    }
  }
  if (dired != 0) {
    dired_dump_obstack("//DIRED//",dired_obstack);
    dired_dump_obstack("//SUBDIRED//",subdired_obstack);
    uVar6 = get_quoting_style(filename_quoting_options);
    __printf_chk(1,"//DIRED-OPTIONS// --quoting-style=%s\n",
                 *(undefined8 *)(quoting_style_args + (ulong)uVar6 * 8));
  }
  lVar18 = active_dir_set;
  if (active_dir_set != 0) {
    lVar9 = hash_get_n_entries(active_dir_set);
    if (lVar9 != 0) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("hash_get_n_entries (active_dir_set) == 0","src/ls.c",0x741,
                    (char *)&__PRETTY_FUNCTION___14);
    }
    hash_free(lVar18);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return exit_status;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}