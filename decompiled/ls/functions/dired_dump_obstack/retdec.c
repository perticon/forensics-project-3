void dired_dump_obstack(char * prefix, int32_t * os) {
    int64_t v1 = (int64_t)os;
    int64_t * v2 = (int64_t *)(v1 + 24); // 0x73e4
    int64_t v3 = *v2; // 0x73e4
    int64_t * v4 = (int64_t *)(v1 + 16); // 0x73e8
    int64_t v5 = *v4; // 0x73e8
    uint64_t v6 = v3 - v5; // 0x73ef
    if (v6 < 8) {
        // 0x7487
        return;
    }
    if (v3 == v5) {
        char * v7 = (char *)(v1 + 80); // 0x748c
        *v7 = *v7 | 2;
    }
    int64_t v8 = *(int64_t *)(v1 + 48); // 0x7405
    int64_t v9 = v8 + v3 & -1 - v8; // 0x7416
    int64_t v10 = *(int64_t *)(v1 + 32); // 0x7419
    int64_t v11 = *(int64_t *)(v1 + 8); // 0x7420
    int64_t v12 = v9 - v11 > v10 - v11 ? v10 : v9; // 0x742e
    *v2 = v12;
    *v4 = v12;
    function_4990();
    int64_t v13 = v5; // 0x744d
    v13 += 8;
    function_4b20();
    while (v13 != (v6 & -8) + v5) {
        // 0x7450
        v13 += 8;
        function_4b20();
    }
    int64_t v14 = (int64_t)g59; // 0x746b
    int64_t * v15 = (int64_t *)(v14 + 40); // 0x7472
    uint64_t v16 = *v15; // 0x7472
    if (v16 >= *(int64_t *)(v14 + 48)) {
        // 0x7495
        function_48d0();
        return;
    }
    // 0x747c
    *v15 = v16 + 1;
    *(char *)v16 = 10;
}