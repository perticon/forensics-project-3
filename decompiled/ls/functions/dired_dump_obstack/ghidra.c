void dired_dump_obstack(char *param_1,long param_2)

{
  undefined8 uVar1;
  char *pcVar2;
  ulong uVar3;
  ulong uVar4;
  undefined8 *puVar5;
  undefined8 *puVar6;
  
  puVar6 = *(undefined8 **)(param_2 + 0x18);
  puVar5 = *(undefined8 **)(param_2 + 0x10);
  if (7 < (ulong)((long)puVar6 - (long)puVar5)) {
    if (puVar6 == puVar5) {
      *(byte *)(param_2 + 0x50) = *(byte *)(param_2 + 0x50) | 2;
    }
    uVar3 = (long)puVar6 + *(ulong *)(param_2 + 0x30) & ~*(ulong *)(param_2 + 0x30);
    uVar4 = *(ulong *)(param_2 + 0x20);
    if (uVar3 - *(long *)(param_2 + 8) <= *(ulong *)(param_2 + 0x20) - *(long *)(param_2 + 8)) {
      uVar4 = uVar3;
    }
    puVar6 = (undefined8 *)(((long)puVar6 - (long)puVar5 & 0xfffffffffffffff8U) + (long)puVar5);
    *(ulong *)(param_2 + 0x18) = uVar4;
    *(ulong *)(param_2 + 0x10) = uVar4;
    fputs_unlocked(param_1,stdout);
    do {
      uVar1 = *puVar5;
      puVar5 = puVar5 + 1;
      __printf_chk(1,&DAT_0011ba78,uVar1);
    } while (puVar5 != puVar6);
    pcVar2 = stdout->_IO_write_ptr;
    if (stdout->_IO_write_end <= pcVar2) {
      __overflow(stdout,10);
      return;
    }
    stdout->_IO_write_ptr = pcVar2 + 1;
    *pcVar2 = '\n';
  }
  return;
}