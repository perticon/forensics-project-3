void dired_dump_obstack(void** rdi, struct s23* rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void** rbx7;
    void* rbp8;
    void** rax9;
    void** rdx10;
    void** r8_11;
    void** rcx12;
    void** rbp13;
    void** rsi14;
    void** rdx15;
    void** rdi16;
    void** rax17;

    rax6 = rsi->f18;
    rbx7 = rsi->f10;
    rbp8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(rbx7));
    if (reinterpret_cast<uint64_t>(rbp8) > 7) {
        if (rax6 == rbx7) {
            rsi->f50 = reinterpret_cast<unsigned char>(rsi->f50 | 2);
        }
        rax9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) + reinterpret_cast<uint64_t>(rsi->f30) & ~reinterpret_cast<uint64_t>(rsi->f30));
        rdx10 = rsi->f20;
        r8_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax9) - rsi->f8);
        rcx12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx10) - rsi->f8);
        if (reinterpret_cast<unsigned char>(r8_11) <= reinterpret_cast<unsigned char>(rcx12)) {
            rdx10 = rax9;
        }
        rbp13 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rbp8) & 0xfffffffffffffff8) + reinterpret_cast<unsigned char>(rbx7));
        rsi->f18 = rdx10;
        rsi->f10 = rdx10;
        rsi14 = stdout;
        fun_4990(rdi, rsi14, rdx10, rcx12, r8_11);
        do {
            rdx15 = *reinterpret_cast<void***>(rbx7);
            rbx7 = rbx7 + 8;
            fun_4b20(1, " %ld", rdx15, rcx12, r8_11);
        } while (rbx7 != rbp13);
        rdi16 = stdout;
        rax17 = *reinterpret_cast<void***>(rdi16 + 40);
        if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
            *reinterpret_cast<void***>(rdi16 + 40) = rax17 + 1;
            *reinterpret_cast<void***>(rax17) = reinterpret_cast<void**>(10);
        }
    }
    return;
}