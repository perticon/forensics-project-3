void queue_directory(void** rdi, void** rsi, void** edx, void** rcx, void** r8) {
    void** r13d6;
    void** r12_7;
    void** rbp8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;

    r13d6 = edx;
    r12_7 = rsi;
    rbp8 = rdi;
    rax9 = xmalloc(32, rsi);
    if (r12_7) {
        rax10 = xstrdup(r12_7, rsi);
        r12_7 = rax10;
    }
    *reinterpret_cast<void***>(rax9 + 8) = r12_7;
    if (rbp8) {
        rax11 = xstrdup(rbp8, rsi);
        rbp8 = rax11;
    }
    rax12 = pending_dirs;
    *reinterpret_cast<void***>(rax9) = rbp8;
    *reinterpret_cast<void***>(rax9 + 16) = r13d6;
    *reinterpret_cast<void***>(rax9 + 24) = rax12;
    pending_dirs = rax9;
    return;
}