int64_t queue_directory(int64_t * a1, int32_t a2, int64_t a3) {
    int64_t v1 = xmalloc(); // 0x6f58
    int64_t v2 = 0; // 0x6f63
    if (a2 != 0) {
        // 0x6f65
        v2 = (int64_t)xstrdup((char *)(int64_t)a2);
    }
    // 0x6f70
    *(int64_t *)(v1 + 8) = v2;
    int64_t v3 = (int64_t)a1; // 0x6f77
    if (a1 != NULL) {
        // 0x6f79
        v3 = (int64_t)xstrdup((char *)a1);
    }
    int64_t result = (int64_t)pending_dirs; // 0x6f84
    *(int64_t *)v1 = v3;
    *(char *)(v1 + 16) = (char)a3;
    *(int64_t *)(v1 + 24) = result;
    *(int64_t *)&pending_dirs = v1;
    return result;
}