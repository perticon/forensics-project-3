void queue_directory(long param_1,long param_2,undefined param_3)

{
  long *plVar1;
  
  plVar1 = (long *)xmalloc(0x20);
  if (param_2 != 0) {
    param_2 = xstrdup(param_2);
  }
  plVar1[1] = param_2;
  if (param_1 != 0) {
    param_1 = xstrdup(param_1);
  }
  *plVar1 = param_1;
  *(undefined *)(plVar1 + 2) = param_3;
  plVar1[3] = (long)pending_dirs;
  pending_dirs = plVar1;
  return;
}