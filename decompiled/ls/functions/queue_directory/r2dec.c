uint64_t queue_directory (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r13d = edx;
    r12 = rsi;
    rax = xmalloc (0x20);
    rbx = rax;
    if (r12 != 0) {
        rax = xstrdup (r12);
        r12 = rax;
    }
    *((rbx + 8)) = r12;
    if (rbp != 0) {
        rax = xstrdup (rbp);
    }
    rax = pending_dirs;
    *(rbx) = rbp;
    *((rbx + 0x10)) = r13b;
    *((rbx + 0x18)) = rax;
    *(obj.pending_dirs) = rbx;
    return rax;
}