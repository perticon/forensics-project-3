void print_with_separator(char param_1)

{
  ulong uVar1;
  undefined8 uVar2;
  char *pcVar3;
  byte *pbVar4;
  ulong uVar5;
  byte bVar6;
  ulong uVar7;
  ulong uVar8;
  
  if (cwd_n_used != 0) {
    uVar8 = 0;
    uVar7 = 0;
    do {
      uVar2 = *(undefined8 *)(sorted_file + uVar7 * 8);
      if (line_length == 0) {
        if (uVar7 != 0) {
          uVar1 = uVar8 + 2;
LAB_0010c5e0:
          uVar5 = uVar1;
          bVar6 = 0x20;
          goto LAB_0010c5ea;
        }
      }
      else {
        uVar5 = length_of_file_name_and_frills(uVar2);
        if (uVar7 == 0) {
          uVar8 = uVar8 + uVar5;
        }
        else {
          uVar1 = uVar5 + uVar8 + 2;
          if ((line_length == 0) ||
             ((bVar6 = eolbyte, uVar1 < line_length && (uVar8 <= -uVar5 - 3)))) goto LAB_0010c5e0;
LAB_0010c5ea:
          pcVar3 = stdout->_IO_write_ptr;
          if (pcVar3 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar3 + 1;
            *pcVar3 = param_1;
          }
          else {
            __overflow(stdout,(int)param_1);
          }
          pbVar4 = (byte *)stdout->_IO_write_ptr;
          uVar8 = uVar5;
          if (pbVar4 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
            *pbVar4 = bVar6;
          }
          else {
            __overflow(stdout,(uint)bVar6);
          }
        }
      }
      uVar7 = uVar7 + 1;
      print_file_name_and_frills_isra_0(uVar2);
    } while (uVar7 < cwd_n_used);
  }
  bVar6 = eolbyte;
  pbVar4 = (byte *)stdout->_IO_write_ptr;
  if (stdout->_IO_write_end <= pbVar4) {
    __overflow(stdout,(uint)eolbyte);
    return;
  }
  stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
  *pbVar4 = bVar6;
  return;
}