int64_t quote_name(char * name, int32_t * options, uint32_t needs_general_quoting, int32_t * color, bool allow_pad, int32_t * stack, char * absolute_name) {
    char smallbuf[8192]; // bp-8272, 0xba50
    char v1[8192]; // 0xbabb
    int64_t v2 = (int64_t)name;
    int64_t v3 = __readfsqword(40); // 0xba89
    int64_t v4; // bp-8264, 0xba50
    int64_t v5 = &v4; // 0xbab7
    v1[0] = v5;
    smallbuf = v1;
    char v6; // bp-8273, 0xba50
    int64_t v7 = quote_name_buf_constprop_0((int64_t)&smallbuf, v2, (int64_t)options, (int64_t)needs_general_quoting, NULL, (int64_t *)&v6); // 0xbac0
    if (allow_pad == (v6 != 0)) {
        int64_t v8 = (int64_t)g59; // 0xbd60
        dired_pos++;
        int64_t * v9 = (int64_t *)(v8 + 40); // 0xbd6f
        uint64_t v10 = *v9; // 0xbd6f
        if (v10 >= *(int64_t *)(v8 + 48)) {
            // 0xbe1d
            function_48d0();
        } else {
            // 0xbd7d
            *v9 = v10 + 1;
            *(char *)v10 = 32;
        }
    }
    if (color != NULL) {
        // 0xbadd
        if (is_colored(4)) {
            // 0xbd90
            put_indicator((int32_t *)&color_indicator);
            put_indicator((int32_t *)&g44);
        }
        // 0xbafa
        put_indicator((int32_t *)&color_indicator);
        put_indicator(color);
        put_indicator((int32_t *)&g44);
    }
    int64_t v11; // 0xba50
    int64_t v12; // 0xba50
    if (absolute_name == NULL) {
        // 0xbd20
        v11 = 0;
        v12 = 0;
        if (stack != NULL) {
            goto lab_0xbbb2;
        } else {
            goto lab_0xbd32;
        }
    } else {
        unsigned char v13 = *(char *)&align_variable_outer_quotes; // 0xbb1b
        int64_t v14 = v13; // 0xbb26
        if (v13 != 0) {
            unsigned char v15 = *(char *)&cwd_some_quoted; // 0xbcc8
            v14 = 0;
            if (v15 != 0) {
                // 0xbcd9
                v14 = 0;
                if (v6 == 0) {
                    int64_t v16 = v15; // 0xbcc8
                    int64_t v17 = (int64_t)g59; // 0xbce9
                    int64_t * v18 = (int64_t *)(v17 + 40); // 0xbcf7
                    uint64_t v19 = *v18; // 0xbcf7
                    if (v19 >= *(int64_t *)(v17 + 48)) {
                        // 0xbe2c
                        function_48d0();
                        v14 = v16;
                    } else {
                        // 0xbd05
                        *v18 = v19 + 1;
                        *(char *)v19 = *(char *)*(int64_t *)&smallbuf;
                        v14 = v16;
                    }
                }
            }
        }
        // 0xbb32
        file_escape((char *)hostname, false);
        file_escape(absolute_name, true);
        function_4b20();
        function_4630();
        function_4630();
        v11 = v14;
        v12 = v14;
        if (stack == NULL) {
            goto lab_0xbd32;
        } else {
            goto lab_0xbbb2;
        }
    }
  lab_0xbbb2:;
    int64_t v20 = (int64_t)stack;
    if (*(char *)&dired != 0) {
        int64_t * v21 = (int64_t *)(v20 + 24); // 0xbbbb
        int64_t v22 = *v21; // 0xbbbb
        int64_t v23 = v22; // 0xbbca
        if (*(int64_t *)(v20 + 32) - v22 < 8) {
            // 0xbdb0
            _obstack_newchunk(stack, 8);
            v23 = *v21;
        }
        // 0xbbd0
        *(int64_t *)v23 = dired_pos;
        *v21 = *v21 + 8;
    }
    // 0xbbdf
    function_4ae0();
    int64_t v24 = dired_pos + v7; // 0xbbfb
    dired_pos = v24;
    int64_t v25 = v11; // 0xbc09
    if (*(char *)&dired != 0) {
        int64_t * v26 = (int64_t *)(v20 + 24); // 0xbc0b
        int64_t v27 = *v26; // 0xbc0b
        int64_t v28 = v24; // 0xbc1a
        int64_t v29 = v27; // 0xbc1a
        if (*(int64_t *)(v20 + 32) - v27 < 8) {
            // 0xbde0
            _obstack_newchunk(stack, 8);
            v29 = *v26;
            v28 = dired_pos;
        }
        // 0xbc20
        *(int64_t *)v29 = v28;
        *v26 = *v26 + 8;
        v25 = v11;
    }
    goto lab_0xbc2f;
  lab_0xbd32:
    // 0xbd32
    function_4ae0();
    dired_pos += v7;
    v25 = v12;
    goto lab_0xbc2f;
  lab_0xbc2f:
    if (absolute_name != NULL) {
        // 0xbc34
        function_4ae0();
        if ((char)v25 != 0) {
            int64_t v30 = (int64_t)g59; // 0xbc5b
            int64_t * v31 = (int64_t *)(v30 + 40); // 0xbc67
            uint64_t v32 = *v31; // 0xbc67
            if (v32 >= *(int64_t *)(v30 + 48)) {
                // 0xbe10
                function_48d0();
            } else {
                // 0xbc75
                *v31 = v32 + 1;
                *(char *)v32 = *(char *)(v7 - 1 + *(int64_t *)&smallbuf);
            }
        }
    }
    int64_t v33 = *(int64_t *)&smallbuf; // 0xbc7f
    if (v33 != v2 && v33 != v5) {
        // 0xbc8f
        function_4630();
    }
    // 0xbc94
    if (v3 != __readfsqword(40)) {
        // 0xbe49
        return function_4870();
    }
    // 0xbcb3
    return v7 + (int64_t)v6;
}