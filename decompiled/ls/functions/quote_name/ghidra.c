long quote_name(byte *param_1,undefined8 param_2,undefined4 param_3,long param_4,char param_5,
               long param_6,long param_7)

{
  byte bVar1;
  byte *pbVar2;
  char cVar3;
  size_t sVar4;
  void *__ptr;
  char *pcVar5;
  char *pcVar6;
  long *plVar7;
  long lVar8;
  size_t sVar9;
  char cVar10;
  long in_FS_OFFSET;
  byte local_2051;
  byte *local_2050;
  byte local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_2050 = local_2048;
  sVar4 = quote_name_buf_constprop_0(&local_2050,param_1,param_2,param_3,0);
  if ((local_2051 != 0) && (param_5 != '\0')) {
    dired_pos = dired_pos + 1;
    pcVar5 = stdout->_IO_write_ptr;
    if (pcVar5 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar5 + 1;
      *pcVar5 = ' ';
    }
    else {
      __overflow(stdout,0x20);
    }
  }
  if (param_4 != 0) {
    cVar3 = is_colored(4);
    if (cVar3 != '\0') {
      put_indicator(color_indicator);
      put_indicator(0x125070);
    }
    put_indicator(color_indicator);
    put_indicator(param_4);
    put_indicator(0x125070);
  }
  cVar3 = cwd_some_quoted;
  sVar9 = sVar4;
  if (param_7 == 0) {
    lVar8 = 0;
    cVar3 = '\0';
  }
  else {
    cVar10 = align_variable_outer_quotes;
    if ((align_variable_outer_quotes == '\0') || (cVar10 = cwd_some_quoted, cwd_some_quoted == '\0')
       ) {
      lVar8 = 0;
      cVar3 = cVar10;
    }
    else if (local_2051 == 0) {
      bVar1 = *local_2050;
      pbVar2 = (byte *)stdout->_IO_write_ptr;
      sVar9 = sVar4 - 2;
      if (pbVar2 < stdout->_IO_write_end) {
        lVar8 = 1;
        stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
        *pbVar2 = bVar1;
      }
      else {
        __overflow(stdout,(uint)bVar1);
        lVar8 = 1;
      }
    }
    else {
      lVar8 = 0;
      cVar3 = '\0';
    }
    __ptr = (void *)file_escape(hostname,0);
    pcVar5 = (char *)file_escape(param_7,1);
    pcVar6 = "";
    if (*pcVar5 != '/') {
      pcVar6 = "/";
    }
    __printf_chk(1,&DAT_0011baf0,__ptr,pcVar6);
    free(__ptr);
    free(pcVar5);
  }
  if (param_6 == 0) {
    fwrite_unlocked(local_2050 + lVar8,1,sVar9,stdout);
    dired_pos = dired_pos + sVar4;
  }
  else {
    if (dired != '\0') {
      plVar7 = *(long **)(param_6 + 0x18);
      if ((ulong)(*(long *)(param_6 + 0x20) - (long)plVar7) < 8) {
        _obstack_newchunk(param_6,8);
        plVar7 = *(long **)(param_6 + 0x18);
      }
      *plVar7 = dired_pos;
      *(long *)(param_6 + 0x18) = *(long *)(param_6 + 0x18) + 8;
    }
    fwrite_unlocked(local_2050 + lVar8,1,sVar9,stdout);
    dired_pos = dired_pos + sVar4;
    if (dired != '\0') {
      plVar7 = *(long **)(param_6 + 0x18);
      if ((ulong)(*(long *)(param_6 + 0x20) - (long)plVar7) < 8) {
        _obstack_newchunk(param_6,8);
        plVar7 = *(long **)(param_6 + 0x18);
      }
      *plVar7 = dired_pos;
      *(long *)(param_6 + 0x18) = *(long *)(param_6 + 0x18) + 8;
    }
  }
  if ((param_7 != 0) && (fwrite_unlocked(&DAT_0011bb04,1,6,stdout), cVar3 != '\0')) {
    bVar1 = local_2050[sVar4 - 1];
    pbVar2 = (byte *)stdout->_IO_write_ptr;
    if (pbVar2 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
      *pbVar2 = bVar1;
    }
    else {
      __overflow(stdout,(uint)bVar1);
    }
  }
  if ((local_2050 != param_1) && (local_2050 != local_2048)) {
    free(local_2050);
  }
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_2051 + sVar4;
}