void* quote_name(void** rdi, void** rsi, void** edx, void** rcx, uint32_t r8d, void** r9, void** a7) {
    void*** rsp8;
    void** r15_9;
    void** rcx10;
    void* rax11;
    void** r8_12;
    void** rax13;
    void** rdx14;
    void** r9_15;
    void** rsi16;
    void** v17;
    void** rax18;
    signed char v19;
    void** rdi20;
    void** tmp64_21;
    void** rax22;
    unsigned char al23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    void** v28;
    int64_t r13_29;
    void** r9_30;
    signed char v31;
    void** rdi32;
    unsigned char v33;
    void** rax34;
    void** rdi35;
    void** rax36;
    void** rax37;
    void** rcx38;
    void** r9_39;
    int1_t zf40;
    void** rdx41;
    void** rax42;
    void** rcx43;
    uint32_t esi44;
    void** tmp64_45;
    void** tmp64_46;
    int1_t zf47;
    void** rdx48;
    void** rax49;
    void** rdi50;
    uint32_t edx51;
    void** rax52;
    void* rax53;
    unsigned char v54;
    void** rdx55;
    void** r15_56;
    uint32_t r14d57;
    void** rbp58;
    uint32_t eax59;
    void** r13_60;
    void** v61;
    int1_t zf62;
    uint32_t eax63;
    unsigned char v64;
    uint32_t esi65;
    uint32_t v66;
    uint32_t v67;
    uint32_t ecx68;
    unsigned char v69;
    uint32_t edx70;
    void** v71;
    unsigned char al72;
    unsigned char al73;
    unsigned char al74;
    signed char v75;
    unsigned char al76;
    uint64_t v77;
    unsigned char al78;
    void** rax79;
    void** rbx80;
    void** rcx81;
    struct s12* rcx82;
    void** v83;
    int32_t eax84;
    void** rcx85;
    unsigned char al86;
    unsigned char al87;
    unsigned char al88;
    void* rcx89;
    uint64_t rdx90;
    int64_t rcx91;
    int32_t v92;
    uint32_t r14d93;
    void** rdx94;
    void** v95;
    void** r8_96;
    void** rsi97;
    void** v98;
    void* rax99;
    int1_t zf100;
    void** v101;
    void** v102;
    void** v103;
    void** v104;
    unsigned char al105;
    void** rsi106;
    void** rdx107;
    void** v108;
    uint32_t r14d109;
    void** r8_110;
    void** v111;
    void** rcx112;
    void** rsi113;
    void* rax114;
    void** v115;
    int1_t zf116;
    unsigned char al117;
    void** v118;
    uint32_t v119;
    signed char v120;
    unsigned char v121;
    unsigned char al122;

    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 72);
    *reinterpret_cast<uint32_t*>(&r15_9) = r8d;
    rcx10 = edx;
    *reinterpret_cast<int32_t*>(&rcx10 + 4) = 0;
    rax11 = g28;
    *reinterpret_cast<int32_t*>(&r8_12) = 0;
    *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    rax13 = reinterpret_cast<void**>(rsp8 + 48);
    rdx14 = rsi;
    r9_15 = reinterpret_cast<void**>(rsp8 + 39);
    rsi16 = rdi;
    v17 = rax13;
    rax18 = quote_name_buf_constprop_0(rsp8 + 40, rsi16, rdx14, rcx10, 0, r9_15);
    if (v19 && *reinterpret_cast<signed char*>(&r15_9)) {
        rdi20 = stdout;
        tmp64_21 = dired_pos + 1;
        dired_pos = tmp64_21;
        rax22 = *reinterpret_cast<void***>(rdi20 + 40);
        if (reinterpret_cast<unsigned char>(rax22) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi20 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi16) = 32;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            fun_48d0();
        } else {
            rdx14 = rax22 + 1;
            *reinterpret_cast<void***>(rdi20 + 40) = rdx14;
            *reinterpret_cast<void***>(rax22) = reinterpret_cast<void**>(32);
        }
    }
    if (rcx) {
        al23 = is_colored(4, 4);
        r8_12 = reinterpret_cast<void**>(0x25060);
        if (al23) {
            put_indicator(0x25060, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v24);
            put_indicator(0x25070, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v25);
            r8_12 = reinterpret_cast<void**>(0x25060);
        }
        put_indicator(0x25060, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v26);
        put_indicator(rcx, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v27);
        put_indicator(0x25070, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v28);
    }
    if (a7) {
        *reinterpret_cast<uint32_t*>(&r13_29) = align_variable_outer_quotes;
        if (!*reinterpret_cast<signed char*>(&r13_29) || (*reinterpret_cast<uint32_t*>(&r13_29) = cwd_some_quoted, *reinterpret_cast<signed char*>(&r13_29) == 0)) {
            *reinterpret_cast<int32_t*>(&r9_30) = 0;
            *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
        } else {
            if (v31) {
                *reinterpret_cast<int32_t*>(&r9_30) = 0;
                *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_29) = 0;
            } else {
                rdi32 = stdout;
                *reinterpret_cast<uint32_t*>(&rdx14) = v33;
                *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                rax34 = *reinterpret_cast<void***>(rdi32 + 40);
                if (reinterpret_cast<unsigned char>(rax34) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi32 + 48))) {
                    fun_48d0();
                    *reinterpret_cast<int32_t*>(&r9_30) = 1;
                    *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                } else {
                    rcx10 = rax34 + 1;
                    *reinterpret_cast<int32_t*>(&r9_30) = 1;
                    *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                    *reinterpret_cast<void***>(rdi32 + 40) = rcx10;
                    *reinterpret_cast<void***>(rax34) = rdx14;
                }
            }
        }
        rdi35 = hostname;
        rax36 = file_escape(rdi35, 0, rdx14, rcx10, r8_12, r9_30);
        rax37 = file_escape(a7, 1, rdx14, rcx10, r8_12, r9_30);
        rcx38 = reinterpret_cast<void**>(0x1bb0a);
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax37) == 47)) {
            rcx38 = reinterpret_cast<void**>("/");
        }
        fun_4b20(1, 0x1baf0, rax36, rcx38, rax37);
        fun_4630(rax36, rax36);
        fun_4630(rax37, rax37);
        r9_39 = r9_30;
        if (!r9) 
            goto addr_bd32_20; else 
            goto addr_bbb2_21;
    }
    *reinterpret_cast<int32_t*>(&r9_39) = 0;
    *reinterpret_cast<int32_t*>(&r9_39 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r13_29) = 0;
    if (r9) {
        addr_bbb2_21:
        zf40 = dired == 0;
        if (!zf40) {
            rdx41 = *reinterpret_cast<void***>(r9 + 24);
            if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 32)) - reinterpret_cast<unsigned char>(rdx41)) <= 7) {
                _obstack_newchunk(r9, r9);
                rdx41 = *reinterpret_cast<void***>(r9 + 24);
                r9_39 = r9_39;
            }
            rax42 = dired_pos;
            *reinterpret_cast<void***>(rdx41) = rax42;
            *reinterpret_cast<void***>(r9 + 24) = *reinterpret_cast<void***>(r9 + 24) + 8;
        }
    } else {
        addr_bd32_20:
        rcx43 = stdout;
        esi44 = 1;
        fun_4ae0(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(r9_39));
        tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax18));
        dired_pos = tmp64_45;
        goto addr_bc2f_26;
    }
    rcx43 = stdout;
    esi44 = 1;
    fun_4ae0(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(r9_39));
    tmp64_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax18));
    dired_pos = tmp64_46;
    zf47 = dired == 0;
    if (!zf47) {
        rdx48 = *reinterpret_cast<void***>(r9 + 24);
        if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 32)) - reinterpret_cast<unsigned char>(rdx48)) <= 7) {
            esi44 = 8;
            _obstack_newchunk(r9);
            rdx48 = *reinterpret_cast<void***>(r9 + 24);
        }
        rax49 = dired_pos;
        *reinterpret_cast<void***>(rdx48) = rax49;
        *reinterpret_cast<void***>(r9 + 24) = *reinterpret_cast<void***>(r9 + 24) + 8;
    }
    addr_bc2f_26:
    if (a7 && (rcx43 = stdout, esi44 = 1, fun_4ae0(0x1bb04), !!*reinterpret_cast<signed char*>(&r13_29))) {
        rdi50 = stdout;
        edx51 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff);
        rax52 = *reinterpret_cast<void***>(rdi50 + 40);
        if (reinterpret_cast<unsigned char>(rax52) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi50 + 48))) {
            esi44 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&edx51));
            fun_48d0();
        } else {
            rcx43 = rax52 + 1;
            *reinterpret_cast<void***>(rdi50 + 40) = rcx43;
            *reinterpret_cast<void***>(rax52) = *reinterpret_cast<void***>(&edx51);
        }
    }
    if (rax13 != rdi && rax13 != v17) {
        fun_4630(rax13, rax13);
    }
    *reinterpret_cast<uint32_t*>(&rax53) = v54;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
    rdx55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax11) - reinterpret_cast<uint64_t>(g28));
    if (!rdx55) {
        return reinterpret_cast<int64_t>(rax53) + reinterpret_cast<unsigned char>(rax18);
    }
    fun_4870();
    r15_56 = rdx55;
    r14d57 = esi44;
    rbp58 = rcx43;
    eax59 = print_with_color;
    if (*reinterpret_cast<signed char*>(&esi44)) 
        goto addr_be7a_40;
    r13_60 = v61;
    if (!*reinterpret_cast<signed char*>(&eax59)) 
        goto addr_be82_42;
    zf62 = color_symlink_as_referent == 0;
    eax63 = v64;
    if (zf62 || !*reinterpret_cast<signed char*>(&eax63)) {
        esi65 = v66;
    } else {
        esi65 = v67;
    }
    addr_bf24_46:
    ecx68 = v69;
    if (*reinterpret_cast<unsigned char*>(&ecx68)) {
        addr_c073_47:
        edx70 = esi65 & 0xf000;
        if (edx70 == 0x8000) {
            if (!(esi65 & 0x800) || (v71 = reinterpret_cast<void**>(0xc1b2), al72 = is_colored(16), al72 == 0)) {
                if (!(esi65 & reinterpret_cast<uint32_t>("v")) || (v71 = reinterpret_cast<void**>(0xc202), al73 = is_colored(17), al73 == 0)) {
                    v71 = reinterpret_cast<void**>(0xc1d2);
                    al74 = is_colored(21);
                    if (!al74 || !v75) {
                        if (!(esi65 & 73) || (v71 = reinterpret_cast<void**>(0xc28f), al76 = is_colored(14), al76 == 0)) {
                            if (v77 <= 1 || (v71 = reinterpret_cast<void**>(0xc2b6), al78 = is_colored(22), al78 == 0)) {
                                addr_c120_53:
                                v71 = reinterpret_cast<void**>(0xc128);
                                rax79 = fun_4860(r13_60);
                                rbx80 = color_ext_list;
                                rcx81 = rax79;
                                if (!rbx80) {
                                    addr_c171_54:
                                    *reinterpret_cast<int32_t*>(&rcx82) = 80;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                                    goto addr_bf8e_55;
                                } else {
                                    do {
                                        if (reinterpret_cast<unsigned char>(rcx81) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx80))) 
                                            continue;
                                        v83 = rcx81;
                                        v71 = reinterpret_cast<void**>(0xc15f);
                                        eax84 = c_strncasecmp();
                                        rcx81 = v83;
                                        if (!eax84) 
                                            break;
                                        rbx80 = *reinterpret_cast<void***>(rbx80 + 32);
                                    } while (rbx80);
                                    goto addr_c171_54;
                                }
                            } else {
                                *reinterpret_cast<int32_t*>(&rcx82) = 0x160;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                                goto addr_bf8e_55;
                            }
                        } else {
                            *reinterpret_cast<int32_t*>(&rcx82) = 0xe0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                            goto addr_bf8e_55;
                        }
                    } else {
                        *reinterpret_cast<int32_t*>(&rcx82) = 0x150;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                        goto addr_bf8e_55;
                    }
                } else {
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x110;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    goto addr_bf8e_55;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx82) = 0x100;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                goto addr_bf8e_55;
            }
            rcx85 = rbx80 + 16;
        } else {
            if (edx70 == 0x4000) {
                if ((esi65 & 0x202) == 0x202) {
                    v71 = reinterpret_cast<void**>(0xc2da);
                    al86 = is_colored(20);
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x140;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    if (al86) {
                        addr_bf8e_55:
                        rcx85 = reinterpret_cast<void**>(&rcx82->f25060);
                    } else {
                        goto addr_c222_70;
                    }
                } else {
                    addr_c222_70:
                    if ((!(*reinterpret_cast<unsigned char*>(&esi65) & 2) || (v71 = reinterpret_cast<void**>(0xc232), al87 = is_colored(19), *reinterpret_cast<int32_t*>(&rcx82) = 0x130, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, !al87)) && (*reinterpret_cast<int32_t*>(&rcx82) = 96, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, !!(esi65 & 0x200))) {
                        v71 = reinterpret_cast<void**>(0xc25a);
                        al88 = is_colored(18);
                        rcx89 = reinterpret_cast<void*>(96 - (96 + reinterpret_cast<uint1_t>(96 < 96 + reinterpret_cast<uint1_t>(al88 < 1))));
                        *reinterpret_cast<unsigned char*>(&rcx89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx89) & 64);
                        rcx82 = reinterpret_cast<struct s12*>(reinterpret_cast<uint64_t>(rcx89) + 0x120);
                        goto addr_bf8e_55;
                    }
                }
            } else {
                if (edx70 == 0xa000) {
                    *reinterpret_cast<int32_t*>(&rdx90) = 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx90) + 4) = 0;
                    goto addr_bf56_74;
                } else {
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    if (edx70 != 0x1000 && ((*reinterpret_cast<int32_t*>(&rcx82) = 0x90, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, edx70 != 0xc000) && (*reinterpret_cast<int32_t*>(&rcx82) = 0xa0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, edx70 != 0x6000))) {
                        *reinterpret_cast<int32_t*>(&rcx82) = 0xb0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                        if (edx70 != 0x2000) {
                            rcx82 = reinterpret_cast<struct s12*>(0xd0);
                        }
                        goto addr_bf8e_55;
                    }
                }
            }
        }
    } else {
        addr_bf35_79:
        *reinterpret_cast<int32_t*>(&rcx91) = v92;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx91) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx90) = *reinterpret_cast<int32_t*>("\r" + rcx91 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx90) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&ecx68) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx90) == 7);
        if (*reinterpret_cast<int32_t*>(&rdx90) == 5) 
            goto addr_c120_53; else 
            goto addr_bf56_74;
    }
    if (*reinterpret_cast<void***>(rcx85 + 8)) {
        addr_bfb3_81:
        r14d93 = r14d57 ^ 1;
        rdx94 = v95;
        *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r8_96) = *reinterpret_cast<unsigned char*>(&r14d93);
        *reinterpret_cast<int32_t*>(&r8_96 + 4) = 0;
        rsi97 = filename_quoting_options;
        rax99 = quote_name(r13_60, rsi97, rdx94, rcx85, *reinterpret_cast<uint32_t*>(&r8_96), r15_56, v98);
        process_signals(r13_60, rsi97, rdx94, rcx85, r8_96, r15_56);
        zf100 = g25088 == 0;
        if (zf100) {
            put_indicator(0x25060, rsi97, v71, rcx85, r8_96, r15_56, v101, v83);
            put_indicator(0x25090, rsi97, v71, rcx85, r8_96, r15_56, v102, v83);
            put_indicator(0x25070, rsi97, v71, rcx85, r8_96, r15_56, v103, v83);
        } else {
            put_indicator(0x25080, rsi97, v71, rcx85, r8_96, r15_56, v104, v83);
        }
    } else {
        v71 = reinterpret_cast<void**>(0xbfa9);
        al105 = is_colored(4, 4);
        if (!al105) {
            addr_be82_42:
            rsi106 = filename_quoting_options;
            rdx107 = v108;
            *reinterpret_cast<int32_t*>(&rdx107 + 4) = 0;
            r14d109 = r14d57 ^ 1;
            *reinterpret_cast<uint32_t*>(&r8_110) = *reinterpret_cast<unsigned char*>(&r14d109);
            *reinterpret_cast<int32_t*>(&r8_110 + 4) = 0;
            quote_name(r13_60, rsi106, rdx107, 0, *reinterpret_cast<uint32_t*>(&r8_110), r15_56, v111);
            process_signals(r13_60, rsi106, rdx107, 0, r8_110, r15_56);
            goto addr_beb9_85;
        } else {
            *reinterpret_cast<int32_t*>(&rcx85) = 0;
            *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
            goto addr_bfb3_81;
        }
    }
    rcx112 = line_length;
    if (rcx112 && (rsi113 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) / reinterpret_cast<unsigned char>(rcx112)), rax114 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax99) + reinterpret_cast<unsigned char>(rbp58) + 0xffffffffffffffff), rsi113 != reinterpret_cast<uint64_t>(rax114) / reinterpret_cast<unsigned char>(rcx112))) {
        put_indicator(0x251d0, rsi113, reinterpret_cast<uint64_t>(rax114) % reinterpret_cast<unsigned char>(rcx112), rcx112, r8_96, r15_56, v115, v83);
    }
    addr_beb9_85:
    goto v17;
    addr_bf56_74:
    if (eax63 || !*reinterpret_cast<unsigned char*>(&ecx68)) {
        rcx82 = reinterpret_cast<struct s12*>(rdx90 << 4);
        goto addr_bf8e_55;
    } else {
        zf116 = color_symlink_as_referent == 0;
        *reinterpret_cast<int32_t*>(&rcx82) = 0xd0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
        if (zf116) {
            v71 = reinterpret_cast<void**>(0xbf7e);
            al117 = is_colored(13);
            rcx82 = reinterpret_cast<struct s12*>((0xd0 - (0xd0 + reinterpret_cast<uint1_t>(0xd0 < 0xd0 + reinterpret_cast<uint1_t>(al117 < 1))) & 0xffffffffffffffa0) + 0xd0);
            goto addr_bf8e_55;
        }
    }
    addr_be7a_40:
    r13_60 = v118;
    if (!*reinterpret_cast<signed char*>(&eax59)) 
        goto addr_be82_42;
    esi65 = v119;
    if (v120) {
        ecx68 = v121;
        eax63 = 0;
        if (!*reinterpret_cast<unsigned char*>(&ecx68)) 
            goto addr_bf35_79; else 
            goto addr_c073_47;
    } else {
        v71 = reinterpret_cast<void**>(0xbeed);
        al122 = is_colored(12);
        if (al122) {
            *reinterpret_cast<int32_t*>(&rcx82) = 0xc0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
            goto addr_bf8e_55;
        } else {
            eax63 = 0xffffffff;
            goto addr_bf24_46;
        }
    }
}