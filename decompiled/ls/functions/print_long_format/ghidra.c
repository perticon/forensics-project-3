void print_long_format(long param_1)

{
  char cVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  byte *pbVar4;
  byte bVar5;
  int iVar6;
  int iVar7;
  uint uVar8;
  undefined1 *puVar9;
  long lVar10;
  long lVar11;
  undefined8 uVar12;
  undefined8 uVar13;
  undefined1 *puVar14;
  char *pcVar15;
  char *pcVar16;
  int iVar17;
  char *pcVar18;
  long in_FS_OFFSET;
  bool bVar19;
  undefined8 local_1340;
  ulong local_1338;
  ulong local_1330;
  undefined local_1328 [16];
  int local_1318;
  undefined local_12e8 [16];
  int local_12d8;
  undefined local_12a4;
  undefined8 local_12a3;
  ushort local_129b;
  undefined local_1299;
  undefined local_1298 [32];
  undefined local_1278 [1008];
  char local_e88 [3656];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*(char *)(param_1 + 0xb8) == '\0') {
    local_1299 = 0;
    local_129b = 0x3f3f;
    local_12a4 = filetype_letter[*(uint *)(param_1 + 0xa8)];
    local_12a3 = 0x3f3f3f3f3f3f3f3f;
  }
  else {
    filemodestring(param_1 + 0x18,&local_12a4);
  }
  if (any_has_acl == '\0') {
    local_129b = local_129b & 0xff;
LAB_0010c756:
    if (time_type == 2) goto LAB_0010c980;
LAB_0010c765:
    if (time_type < 3) {
      if (time_type == 0) {
        local_1330 = *(ulong *)(param_1 + 0x78);
        local_1338 = *(ulong *)(param_1 + 0x70);
        bVar19 = true;
      }
      else {
        local_1330 = *(ulong *)(param_1 + 0x88);
        local_1338 = *(ulong *)(param_1 + 0x80);
        bVar19 = true;
      }
    }
    else {
      if (time_type != 3) {
        print_long_format_cold();
        return;
      }
      local_1338 = *(ulong *)(param_1 + 0x70);
      local_1330 = *(ulong *)(param_1 + 0x78);
      bVar19 = (local_1338 & local_1330) != 0xffffffffffffffff;
    }
  }
  else {
    if (*(int *)(param_1 + 0xbc) == 1) {
      local_129b = CONCAT11(0x2e,(undefined)local_129b);
      goto LAB_0010c756;
    }
    if (*(int *)(param_1 + 0xbc) != 2) goto LAB_0010c756;
    local_129b = CONCAT11(0x2b,(undefined)local_129b);
    if (time_type != 2) goto LAB_0010c765;
LAB_0010c980:
    local_1330 = *(ulong *)(param_1 + 0x68);
    local_1338 = *(ulong *)(param_1 + 0x60);
    bVar19 = true;
  }
  pcVar15 = local_e88;
  if (print_inode != '\0') {
    puVar9 = &DAT_0011bb0b;
    if ((*(char *)(param_1 + 0xb8) != '\0') && (*(long *)(param_1 + 0x20) != 0)) {
      puVar9 = (undefined1 *)umaxtostr(*(long *)(param_1 + 0x20),local_1278);
    }
    iVar7 = __sprintf_chk(local_e88,1,0xe3b,&DAT_0011bb1d,inode_number_width,puVar9);
    pcVar15 = local_e88 + iVar7;
  }
  if (print_block_size != '\0') {
    pcVar18 = "?";
    if (*(char *)(param_1 + 0xb8) != '\0') {
      pcVar18 = (char *)human_readable(*(undefined8 *)(param_1 + 0x58),local_1278,human_output_opts,
                                       0x200);
    }
    iVar7 = block_size_width;
    iVar6 = gnu_mbswidth(pcVar18,0);
    iVar7 = iVar7 - iVar6;
    if (0 < iVar7) {
      memset(pcVar15,0x20,(long)iVar7);
      pcVar15 = pcVar15 + iVar7;
    }
    do {
      pcVar16 = pcVar15;
      cVar1 = *pcVar18;
      pcVar18 = pcVar18 + 1;
      pcVar15 = pcVar16 + 1;
      *pcVar16 = cVar1;
    } while (cVar1 != '\0');
    *pcVar16 = ' ';
  }
  puVar9 = &DAT_0011bb0b;
  if (*(char *)(param_1 + 0xb8) != '\0') {
    puVar9 = (undefined1 *)umaxtostr(*(undefined8 *)(param_1 + 0x28),local_1278);
  }
  iVar7 = __sprintf_chk(pcVar15,1,0xffffffffffffffff,"%s %*s ",&local_12a4,nlink_width,puVar9);
  pcVar15 = pcVar15 + iVar7;
  if (dired != '\0') {
    dired_outbuf(&DAT_0011bb15,2);
  }
  if ((((print_owner != '\0') || (print_group != '\0')) || (print_author != '\0')) ||
     (print_scontext != '\0')) {
    dired_outbuf(local_e88);
    uVar2 = owner_width;
    uVar3 = group_width;
    if (print_owner != '\0') {
      puVar9 = &DAT_0011bb0b;
      uVar3 = *(undefined4 *)(param_1 + 0x34);
      if ((*(char *)(param_1 + 0xb8) != '\0') && (puVar9 = (undefined1 *)0x0, numeric_ids == '\0'))
      {
        puVar9 = (undefined1 *)getuser(uVar3);
      }
      format_user_or_group(puVar9,uVar3,uVar2);
      uVar3 = group_width;
    }
    uVar2 = author_width;
    group_width = uVar3;
    if (print_group != '\0') {
      puVar9 = &DAT_0011bb0b;
      uVar2 = *(undefined4 *)(param_1 + 0x38);
      if ((*(char *)(param_1 + 0xb8) != '\0') && (puVar9 = (undefined1 *)0x0, numeric_ids == '\0'))
      {
        puVar9 = (undefined1 *)getgroup(uVar2);
      }
      format_user_or_group(puVar9,uVar2,uVar3);
      uVar2 = author_width;
    }
    author_width = uVar2;
    if (print_author != '\0') {
      puVar9 = &DAT_0011bb0b;
      uVar3 = *(undefined4 *)(param_1 + 0x34);
      if ((*(char *)(param_1 + 0xb8) != '\0') && (puVar9 = (undefined1 *)0x0, numeric_ids == '\0'))
      {
        puVar9 = (undefined1 *)getuser(uVar3);
      }
      format_user_or_group(puVar9,uVar3,uVar2);
    }
    pcVar15 = local_e88;
    if (print_scontext != '\0') {
      format_user_or_group(*(undefined8 *)(param_1 + 0xb0),0,scontext_width);
    }
  }
  if (*(char *)(param_1 + 0xb8) == '\0') {
    pcVar18 = "?";
LAB_0010c9f7:
    iVar7 = file_size_width;
    iVar6 = gnu_mbswidth(pcVar18,0);
    iVar7 = iVar7 - iVar6;
    if (0 < iVar7) {
      memset(pcVar15,0x20,(long)iVar7);
      pcVar15 = pcVar15 + iVar7;
    }
    do {
      pcVar16 = pcVar15;
      cVar1 = *pcVar18;
      pcVar18 = pcVar18 + 1;
      pcVar15 = pcVar16 + 1;
      *pcVar16 = cVar1;
    } while (cVar1 != '\0');
    *pcVar16 = ' ';
  }
  else {
    if ((*(uint *)(param_1 + 0x30) & 0xb000) != 0x2000) {
      pcVar18 = (char *)human_readable(*(undefined8 *)(param_1 + 0x48),local_1278,
                                       file_human_output_opts,1);
      goto LAB_0010c9f7;
    }
    iVar17 = file_size_width - (major_device_number_width + 2 + minor_device_number_width);
    uVar12 = umaxtostr((uint)((*(ulong *)(param_1 + 0x40) >> 0x14) << 8) |
                       (uint)*(ulong *)(param_1 + 0x40) & 0xff,local_1278);
    iVar7 = minor_device_number_width;
    uVar13 = umaxtostr((uint)((ulong)*(undefined8 *)(param_1 + 0x40) >> 0x20) & 0xfffff000 |
                       (uint)((ulong)*(undefined8 *)(param_1 + 0x40) >> 8) & 0xfff,local_1298);
    iVar6 = 0;
    if (-1 < iVar17) {
      iVar6 = iVar17;
    }
    iVar7 = __sprintf_chk(pcVar15,1,0xffffffffffffffff,&DAT_0011bb18,
                          iVar6 + major_device_number_width,uVar13,iVar7,uVar12);
    pcVar15 = pcVar15 + iVar7;
  }
  *pcVar15 = '\x01';
  if ((*(char *)(param_1 + 0xb8) == '\0') || (!bVar19)) {
LAB_0010ca57:
    puVar9 = &DAT_0011bb0b;
LAB_0010ca5e:
    if (width_2 < 0) {
      local_1340 = 0;
      lVar10 = localtime_rz(localtz,&local_1340,local_12e8);
      if (lVar10 != 0) {
        puVar14 = long_time_format._0_8_;
        if (use_abformat != '\0') {
          puVar14 = abformat + (long)local_12d8 * 0x80;
        }
        lVar10 = nstrftime(local_1278,0x3e9,puVar14,local_12e8,localtz,0);
        if (lVar10 != 0) {
          width_2 = mbsnwidth(local_1278,lVar10,0);
        }
      }
      if (width_2 < 0) {
        width_2 = 0;
      }
    }
    iVar7 = __sprintf_chk(pcVar15,1,0xffffffffffffffff,&DAT_0011bb1d,width_2,puVar9);
    pcVar15 = pcVar15 + iVar7;
  }
  else {
    lVar10 = localtime_rz(localtz,&local_1338,local_1328);
    if (lVar10 == 0) {
LAB_0010d070:
      if (*pcVar15 != '\0') {
        if (*(char *)(param_1 + 0xb8) == '\0') goto LAB_0010ca57;
        puVar9 = (undefined1 *)imaxtostr(local_1338,local_1298);
        goto LAB_0010ca5e;
      }
    }
    else {
      bVar19 = (long)local_1330 < current_time._8_8_;
      uVar8 = (uint)(current_time._8_8_ < (long)local_1330);
      iVar7 = (uint)bVar19 - (uint)(current_time._8_8_ < (long)local_1330);
      if ((int)(iVar7 + ((uint)((long)local_1338 < current_time._0_8_) -
                        (uint)(current_time._0_8_ < (long)local_1338)) * 2) < 0) {
        gettime(current_time);
        bVar19 = (long)local_1330 < current_time._8_8_;
        uVar8 = (uint)(current_time._8_8_ < (long)local_1330);
        iVar7 = bVar19 - uVar8;
      }
      uVar8 = (uVar8 - bVar19) +
              ((uint)(current_time._0_8_ < (long)local_1338) -
              (uint)((long)local_1338 < current_time._0_8_)) * 2 &
              iVar7 + ((uint)((long)local_1338 < current_time._0_8_ + -0xf0c2ac) -
                      (uint)(current_time._0_8_ + -0xf0c2ac < (long)local_1338)) * 2;
      if (use_abformat == '\0') {
        puVar9 = *(undefined1 **)(long_time_format + (ulong)(uVar8 >> 0x1f) * 8);
      }
      else {
        puVar9 = abformat + ((ulong)((int)uVar8 >> 0x1f & 0xc) + (long)local_1318) * 0x80;
      }
      lVar10 = nstrftime(pcVar15,0x3e9,puVar9,local_1328);
      if (lVar10 == 0) goto LAB_0010d070;
      pcVar15 = pcVar15 + lVar10;
    }
    *pcVar15 = ' ';
    pcVar15 = pcVar15 + 1;
  }
  lVar10 = (long)pcVar15 - (long)local_e88;
  dired_outbuf(local_e88);
  lVar11 = print_name_with_quoting(param_1,0,dired_obstack,lVar10);
  if (*(int *)(param_1 + 0xa8) == 6) {
    if (*(long *)(param_1 + 8) == 0) goto LAB_0010cad4;
    dired_outbuf(&DAT_0011bb22,4);
    print_name_with_quoting(param_1,1,0,lVar10 + 4 + lVar11);
    if (indicator_style == 0) goto LAB_0010cad4;
    bVar5 = get_type_indicator(1,*(undefined4 *)(param_1 + 0xac),0);
  }
  else {
    if (indicator_style == 0) goto LAB_0010cad4;
    bVar5 = get_type_indicator(*(undefined *)(param_1 + 0xb8));
  }
  if (bVar5 != 0) {
    dired_pos = dired_pos + 1;
    pbVar4 = (byte *)stdout->_IO_write_ptr;
    if (pbVar4 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
      *pbVar4 = bVar5;
    }
    else {
      __overflow(stdout,(uint)bVar5);
    }
  }
LAB_0010cad4:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}