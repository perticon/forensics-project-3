void print_long_format(void** rdi) {
    void* rsp2;
    void* rax3;
    void* v4;
    void** rbp5;
    void** v6;
    int1_t zf7;
    uint32_t eax8;
    int1_t below_or_equal9;
    void** rax10;
    int32_t r13d11;
    uint64_t v12;
    void** v13;
    uint64_t rax14;
    void** rdx15;
    void** rax16;
    void** rax17;
    void** r12_18;
    int1_t zf19;
    void** rbx20;
    int32_t eax21;
    int1_t zf22;
    void** r15_23;
    void** rdi24;
    void** r8_25;
    void* rsi26;
    int64_t rdx27;
    void** rax28;
    uint32_t r14d29;
    uint32_t eax30;
    uint32_t eax31;
    void** rdi32;
    uint32_t eax33;
    void** r9_34;
    void** rcx35;
    int32_t eax36;
    void* rsp37;
    void** rbx38;
    int1_t zf39;
    int1_t zf40;
    int1_t zf41;
    int1_t zf42;
    int1_t zf43;
    void** rsi44;
    int1_t zf45;
    void** rdx46;
    void** rdi47;
    void** rsi48;
    int1_t zf49;
    void** rax50;
    int1_t zf51;
    int1_t zf52;
    void** rdx53;
    void** rdi54;
    void** rsi55;
    int1_t zf56;
    int64_t rdi57;
    void** rax58;
    int1_t zf59;
    void** rdx60;
    void** rdi61;
    void** rsi62;
    int1_t zf63;
    void** rax64;
    int1_t zf65;
    int1_t zf66;
    int1_t zf67;
    void** r15_68;
    int64_t rdx69;
    int64_t rax70;
    uint32_t r14d71;
    uint32_t r14d72;
    int32_t edx73;
    void** rax74;
    void** rdx75;
    uint32_t eax76;
    uint32_t tmp32_77;
    void** r8_78;
    int32_t eax79;
    void* rsp80;
    void** rdi81;
    void** r8_82;
    void* rsi83;
    int64_t rdx84;
    void** rax85;
    void** rdi86;
    void** rdx87;
    uint32_t r14d88;
    uint32_t eax89;
    uint32_t eax90;
    void** rdi91;
    uint32_t eax92;
    void** r13_93;
    void** rdi94;
    void** rdx95;
    int64_t rax96;
    void** rax97;
    void** r13_98;
    void** rax99;
    void** r14_100;
    void** rdi101;
    int64_t rax102;
    int1_t zf103;
    void** r8_104;
    void** rdx105;
    int32_t v106;
    void** r15_107;
    void** rax108;
    int32_t eax109;
    int32_t r8d110;
    uint64_t rdx111;
    uint1_t less112;
    int1_t less_or_equal113;
    uint64_t rcx114;
    int32_t edi115;
    uint32_t eax116;
    int64_t rsi117;
    int64_t rdi118;
    uint1_t less119;
    int1_t less_or_equal120;
    uint64_t rdi121;
    int64_t rax122;
    int64_t rcx123;
    int64_t rdx124;
    uint32_t eax125;
    int64_t rdx126;
    int1_t zf127;
    int64_t rax128;
    int64_t rax129;
    int32_t v130;
    void** rax131;
    void** rax132;
    void* rax133;
    uint32_t edx134;
    void** rcx135;
    uint32_t edx136;
    void** esi137;
    void** al138;
    void* rax139;
    void** rdi140;
    void** tmp64_141;
    void** rdx142;
    uint32_t eax143;
    void** esi144;
    uint32_t edi145;
    uint32_t eax146;

    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - reinterpret_cast<int64_t>("x86-64.so.2"));
    rax3 = g28;
    v4 = rax3;
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(rdi + 0xb8)) {
        v6 = reinterpret_cast<void**>(0xc741);
        filemodestring(rdi + 24, reinterpret_cast<int64_t>(rsp2) + 0xb4);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
    }
    zf7 = any_has_acl == 0;
    if (!zf7) {
        if (*reinterpret_cast<int32_t*>(rbp5 + 0xbc) != 1) {
            if (*reinterpret_cast<int32_t*>(rbp5 + 0xbc) == 2) {
                eax8 = time_type;
                below_or_equal9 = eax8 <= 2;
                if (eax8 != 2) 
                    goto addr_c765_9;
                goto addr_c980_11;
            }
        }
    }
    eax8 = time_type;
    below_or_equal9 = eax8 <= 2;
    if (eax8 == 2) {
        addr_c980_11:
        rax10 = *reinterpret_cast<void***>(rbp5 + 0x68);
        r13d11 = 1;
        v12 = *reinterpret_cast<uint64_t*>(rbp5 + 96);
        v13 = rax10;
    } else {
        addr_c765_9:
        if (!below_or_equal9) {
            if (eax8 != 3) {
                addr_d115_15:
                goto addr_4c96_16;
            } else {
                rax14 = *reinterpret_cast<uint64_t*>(rbp5 + 0x70);
                rdx15 = *reinterpret_cast<void***>(rbp5 + 0x78);
                v12 = rax14;
                v13 = rdx15;
                *reinterpret_cast<unsigned char*>(&r13d11) = reinterpret_cast<uint1_t>((rax14 & reinterpret_cast<unsigned char>(rdx15)) != 0xffffffffffffffff);
            }
        } else {
            if (!eax8) {
                rax16 = *reinterpret_cast<void***>(rbp5 + 0x78);
                r13d11 = 1;
                v12 = *reinterpret_cast<uint64_t*>(rbp5 + 0x70);
                v13 = rax16;
            } else {
                rax17 = *reinterpret_cast<void***>(rbp5 + 0x88);
                r13d11 = 1;
                v12 = *reinterpret_cast<uint64_t*>(rbp5 + 0x80);
                v13 = rax17;
            }
        }
    }
    r12_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp2) + 0x4d0);
    zf19 = print_inode == 0;
    rbx20 = r12_18;
    if (!zf19) {
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && *reinterpret_cast<void***>(rbp5 + 32)) {
            umaxtostr();
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        }
        r12_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp2) + 0x4d0);
        v6 = reinterpret_cast<void**>(0xcc3a);
        eax21 = fun_4c80(r12_18, 1, 0xe3b, "%*s ", r12_18, 1, 0xe3b, "%*s ");
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rbx20 = reinterpret_cast<void**>(static_cast<int64_t>(eax21) + reinterpret_cast<unsigned char>(r12_18));
    }
    zf22 = print_block_size == 0;
    if (!zf22) {
        r15_23 = reinterpret_cast<void**>("?");
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
            rdi24 = *reinterpret_cast<void***>(rbp5 + 88);
            r8_25 = output_block_size;
            rsi26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xe0);
            *reinterpret_cast<int32_t*>(&rdx27) = human_output_opts;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            rax28 = human_readable(rdi24, rsi26, rdx27, 0x200, r8_25, rdi24, rsi26, rdx27, 0x200, r8_25);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            r15_23 = rax28;
        }
        r14d29 = block_size_width;
        v6 = reinterpret_cast<void**>(0xc7d7);
        eax30 = gnu_mbswidth(r15_23, r15_23);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        eax31 = r14d29 - eax30;
        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax31) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax31 == 0))) {
            rdi32 = rbx20;
            rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax31))));
            v6 = reinterpret_cast<void**>(0xc7fa);
            fun_4920(rdi32, rdi32);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        }
        do {
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_23));
            ++r15_23;
            ++rbx20;
            *reinterpret_cast<void***>(rbx20 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax33);
        } while (*reinterpret_cast<void***>(&eax33));
        *reinterpret_cast<void***>(rbx20 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
    }
    if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
        v6 = reinterpret_cast<void**>(0xcbd1);
        umaxtostr();
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
    }
    *reinterpret_cast<int32_t*>(&r9_34) = nlink_width;
    *reinterpret_cast<int32_t*>(&r9_34 + 4) = 0;
    rcx35 = reinterpret_cast<void**>("%s %*s ");
    eax36 = fun_4c80(rbx20, 1, -1, "%s %*s ", rbx20, 1, -1, "%s %*s ");
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 - 8 - 8 + 8 + 8 + 8);
    rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax36)));
    zf39 = dired == 0;
    if (!zf39) {
        dired_outbuf("  ", 2, "  ", 2);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    }
    zf40 = print_owner == 0;
    if (zf40 && ((zf41 = print_group == 0, zf41) && (zf42 = print_author == 0, zf42))) {
        zf43 = print_scontext == 0;
        if (zf43) 
            goto addr_c8c6_38;
    }
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) - reinterpret_cast<unsigned char>(r12_18));
    dired_outbuf(r12_18, rsi44, r12_18, rsi44);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf45 = print_owner == 0;
    if (!zf45) {
        *reinterpret_cast<uint32_t*>(&rdx46) = owner_width;
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        rdi47 = reinterpret_cast<void**>("?");
        *reinterpret_cast<int32_t*>(&rsi48) = *reinterpret_cast<int32_t*>(rbp5 + 52);
        *reinterpret_cast<int32_t*>(&rsi48 + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi47) = 0, *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0, zf49 = numeric_ids == 0, zf49)) {
            rax50 = getuser();
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
            rsi48 = rsi48;
            *reinterpret_cast<uint32_t*>(&rdx46) = *reinterpret_cast<uint32_t*>(&rdx46);
            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
            rdi47 = rax50;
        }
        format_user_or_group(rdi47, rsi48, rdx46, "%s %*s ", v6, r9_34);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        zf51 = print_group == 0;
        if (zf51) 
            goto addr_c8a9_44; else 
            goto addr_cc77_45;
    }
    zf52 = print_group == 0;
    if (!zf52) {
        addr_cc77_45:
        *reinterpret_cast<uint32_t*>(&rdx53) = group_width;
        *reinterpret_cast<int32_t*>(&rdx53 + 4) = 0;
        rdi54 = reinterpret_cast<void**>("?");
        *reinterpret_cast<int32_t*>(&rsi55) = *reinterpret_cast<int32_t*>(rbp5 + 56);
        *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi54) = 0, *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0, zf56 = numeric_ids == 0, zf56)) {
            *reinterpret_cast<int32_t*>(&rdi57) = *reinterpret_cast<int32_t*>(&rsi55);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
            rax58 = getgroup(rdi57, rdi57);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
            rsi55 = rsi55;
            *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(&rdx53);
            *reinterpret_cast<int32_t*>(&rdx53 + 4) = 0;
            rdi54 = rax58;
        }
    } else {
        addr_c8a9_44:
        zf59 = print_author == 0;
        if (!zf59) {
            addr_cca6_48:
            *reinterpret_cast<uint32_t*>(&rdx60) = author_width;
            *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
            rdi61 = reinterpret_cast<void**>("?");
            *reinterpret_cast<int32_t*>(&rsi62) = *reinterpret_cast<int32_t*>(rbp5 + 52);
            *reinterpret_cast<int32_t*>(&rsi62 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi61) = 0, *reinterpret_cast<int32_t*>(&rdi61 + 4) = 0, zf63 = numeric_ids == 0, zf63)) {
                rax64 = getuser();
                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                rsi62 = rsi62;
                *reinterpret_cast<uint32_t*>(&rdx60) = *reinterpret_cast<uint32_t*>(&rdx60);
                *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
                rdi61 = rax64;
                goto addr_ccc3_50;
            }
        } else {
            addr_c8b6_51:
            zf65 = print_scontext == 0;
            rbx38 = r12_18;
            if (!zf65) 
                goto addr_ccd8_52; else 
                goto addr_c8c6_38;
        }
    }
    format_user_or_group(rdi54, rsi55, rdx53, "%s %*s ", v6, r9_34);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf66 = print_author == 0;
    if (zf66) 
        goto addr_c8b6_51; else 
        goto addr_cca6_48;
    addr_ccc3_50:
    format_user_or_group(rdi61, rsi62, rdx60, "%s %*s ", v6, r9_34);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf67 = print_scontext == 0;
    rbx38 = r12_18;
    if (zf67) {
        addr_c8c6_38:
        if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
            r15_68 = reinterpret_cast<void**>("?");
        } else {
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 48)) & 0xb000) == 0x2000) {
                *reinterpret_cast<uint32_t*>(&rdx69) = major_device_number_width;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx69) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax70) = minor_device_number_width;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax70) + 4) = 0;
                r14d71 = file_size_width;
                r14d72 = r14d71 - static_cast<uint32_t>(rdx69 + rax70 + 2);
                umaxtostr();
                edx73 = minor_device_number_width;
                rax74 = umaxtostr();
                *reinterpret_cast<int32_t*>(&rdx75) = edx73;
                *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
                r9_34 = rax74;
                eax76 = 0;
                if (reinterpret_cast<int32_t>(r14d72) >= reinterpret_cast<int32_t>(0)) {
                    eax76 = r14d72;
                }
                tmp32_77 = eax76 + major_device_number_width;
                *reinterpret_cast<uint32_t*>(&r8_78) = tmp32_77;
                *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
                eax79 = fun_4c80(rbx38, 1, -1, "%*s, %*s ");
                rcx35 = rdx75;
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
                rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax79)));
                goto addr_ca42_59;
            } else {
                rdi81 = *reinterpret_cast<void***>(rbp5 + 72);
                r8_82 = file_output_block_size;
                rsi83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) + 0xe0);
                *reinterpret_cast<int32_t*>(&rcx35) = 1;
                *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx84) = file_human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx84) + 4) = 0;
                rax85 = human_readable(rdi81, rsi83, rdx84, 1, r8_82, rdi81, rsi83, rdx84, 1, r8_82);
                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                r15_68 = rax85;
            }
        }
    } else {
        addr_ccd8_52:
        rdi86 = *reinterpret_cast<void***>(rbp5 + 0xb0);
        *reinterpret_cast<int32_t*>(&rdx87) = scontext_width;
        *reinterpret_cast<int32_t*>(&rdx87 + 4) = 0;
        format_user_or_group(rdi86, 0, rdx87, "%s %*s ", v6, r9_34);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        goto addr_c8c6_38;
    }
    r14d88 = file_size_width;
    eax89 = gnu_mbswidth(r15_68, r15_68);
    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&r8_78) = eax89;
    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
    eax90 = r14d88 - *reinterpret_cast<uint32_t*>(&r8_78);
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax90) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax90 == 0))) {
        rdi91 = rbx38;
        rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax90))));
        fun_4920(rdi91, rdi91);
        rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
    }
    do {
        eax92 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_68));
        ++r15_68;
        ++rbx38;
        *reinterpret_cast<void***>(rbx38 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax92);
    } while (*reinterpret_cast<void***>(&eax92));
    *reinterpret_cast<void***>(rbx38 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
    addr_ca42_59:
    *reinterpret_cast<void***>(rbx38) = reinterpret_cast<void**>(1);
    if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) 
        goto addr_ca57_65;
    if (!*reinterpret_cast<unsigned char*>(&r13d11)) 
        goto addr_ca57_65;
    r13_93 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 48);
    rdi94 = localtz;
    rdx95 = r13_93;
    rax96 = localtime_rz(rdi94, reinterpret_cast<int64_t>(rsp80) + 32, rdx95, rcx35);
    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
    if (!rax96) {
        addr_d070_68:
        if (0) {
            addr_cf00_69:
            *reinterpret_cast<void***>(rbx38) = reinterpret_cast<void**>(32);
            rax97 = rbx38 + 1;
        } else {
            if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
                addr_ca57_65:
                r13_98 = reinterpret_cast<void**>("?");
            } else {
                rax99 = imaxtostr(v12, reinterpret_cast<int64_t>(rsp80) + 0xc0, rdx95, rcx35, r8_78, r9_34);
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
                r13_98 = rax99;
            }
            *reinterpret_cast<uint32_t*>(&r8_78) = width_2;
            *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r8_78) < reinterpret_cast<int32_t>(0)) {
                r14_100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 0x70);
                rdi101 = localtz;
                rax102 = localtime_rz(rdi101, reinterpret_cast<int64_t>(rsp80) + 24, r14_100, rcx35);
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
                if (!rax102) {
                    addr_d044_74:
                    *reinterpret_cast<uint32_t*>(&r8_78) = width_2;
                    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
                    goto addr_d04b_75;
                } else {
                    zf103 = use_abformat == 0;
                    r8_104 = localtz;
                    rdx105 = long_time_format;
                    if (!zf103) {
                        rdx105 = reinterpret_cast<void**>((static_cast<int64_t>(v106) << 7) + 0x253e0);
                    }
                    r15_107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 0xe0);
                    rax108 = nstrftime(r15_107, 0x3e9, rdx105, r14_100, r8_104);
                    if (rax108) 
                        goto addr_d0b0_79; else 
                        goto addr_d044_74;
                }
            } else {
                addr_ca6e_80:
                r9_34 = r13_98;
                eax109 = fun_4c80(rbx38, 1, -1, "%*s ");
                rax97 = reinterpret_cast<void**>(static_cast<int64_t>(eax109) + reinterpret_cast<unsigned char>(rbx38));
            }
        }
    } else {
        r8d110 = 0;
        r9_34 = v13;
        rdx111 = v12;
        less112 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(g26378) < reinterpret_cast<signed char>(r9_34));
        less_or_equal113 = reinterpret_cast<signed char>(g26378) <= reinterpret_cast<signed char>(r9_34);
        rcx114 = current_time;
        *reinterpret_cast<unsigned char*>(&r8d110) = reinterpret_cast<uint1_t>(!less_or_equal113);
        edi115 = 0;
        eax116 = less112;
        *reinterpret_cast<uint32_t*>(&rsi117) = r8d110 - eax116;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi117) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&edi115) = reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) > reinterpret_cast<int64_t>(rdx111));
        *reinterpret_cast<uint32_t*>(&rdi118) = edi115 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) < reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi118) + 4) = 0;
        if (static_cast<int32_t>(rsi117 + rdi118 * 2) < 0) {
            gettime(0x26370);
            rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
            r8d110 = 0;
            r9_34 = v13;
            less119 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r9_34) < reinterpret_cast<signed char>(g26378));
            less_or_equal120 = reinterpret_cast<signed char>(r9_34) <= reinterpret_cast<signed char>(g26378);
            *reinterpret_cast<unsigned char*>(&r8d110) = less119;
            rdx111 = v12;
            rcx114 = current_time;
            eax116 = reinterpret_cast<uint1_t>(!less_or_equal120);
            *reinterpret_cast<uint32_t*>(&rsi117) = r8d110 - eax116;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi117) + 4) = 0;
        }
        rdi121 = rcx114 + 0xffffffffff0f3d54;
        *reinterpret_cast<uint32_t*>(&rax122) = eax116 - r8d110;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
        r8_78 = localtz;
        *reinterpret_cast<uint32_t*>(&rcx123) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) < reinterpret_cast<int64_t>(rdx111))) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) > reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx123) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx124) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rdi121) > reinterpret_cast<int64_t>(rdx111))) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rdi121) < reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx124) + 4) = 0;
        eax125 = static_cast<uint32_t>(rax122 + rcx123 * 2) & static_cast<uint32_t>(rsi117 + rdx124 * 2);
        *reinterpret_cast<uint32_t*>(&rdx126) = eax125 >> 31;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx126) + 4) = 0;
        zf127 = use_abformat == 0;
        if (zf127) {
            rdx95 = *reinterpret_cast<void***>(0x25040 + rdx126 * 8);
        } else {
            rax128 = static_cast<int64_t>(reinterpret_cast<int32_t>(eax125)) >> 63;
            *reinterpret_cast<uint32_t*>(&rax129) = *reinterpret_cast<uint32_t*>(&rax128) & 12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax129) + 4) = 0;
            rdx95 = reinterpret_cast<void**>(0x253e0 + (rax129 + reinterpret_cast<uint64_t>(static_cast<int64_t>(v130)) << 7));
        }
        rcx35 = r13_93;
        rax131 = nstrftime(rbx38, 0x3e9, rdx95, rcx35, r8_78);
        rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
        if (!rax131) 
            goto addr_d070_68; else 
            goto addr_cefb_87;
    }
    rax132 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax97) - reinterpret_cast<unsigned char>(r12_18));
    dired_outbuf(r12_18, rax132, r12_18, rax132);
    rax133 = print_name_with_quoting(rbp5, 0, 0x261a0, rax132, r8_78, r9_34);
    edx134 = *reinterpret_cast<uint32_t*>(rbp5 + 0xa8);
    if (edx134 == 6) {
        if (!*reinterpret_cast<void***>(rbp5 + 8) || ((dired_outbuf(" -> ", 4, " -> ", 4), rcx135 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax132) + reinterpret_cast<uint64_t>(rax133) + 4), print_name_with_quoting(rbp5, 1, 0, rcx135, r8_78, r9_34), edx136 = indicator_style, edx136 == 0) || (esi137 = *reinterpret_cast<void***>(rbp5 + 0xac), al138 = get_type_indicator(1, esi137, 0, rcx135, r8_78, r9_34), al138 == 0))) {
            addr_cad4_90:
            rax139 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v4) - reinterpret_cast<uint64_t>(g28));
            if (rax139) {
                fun_4870();
                goto addr_d115_15;
            } else {
                return;
            }
        } else {
            addr_cb74_93:
            rdi140 = stdout;
            tmp64_141 = dired_pos + 1;
            dired_pos = tmp64_141;
            rdx142 = *reinterpret_cast<void***>(rdi140 + 40);
            if (reinterpret_cast<unsigned char>(rdx142) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi140 + 48))) {
                fun_48d0();
                goto addr_cad4_90;
            } else {
                *reinterpret_cast<void***>(rdi140 + 40) = rdx142 + 1;
                *reinterpret_cast<void***>(rdx142) = al138;
                goto addr_cad4_90;
            }
        }
    } else {
        eax143 = indicator_style;
        if (!eax143) 
            goto addr_cad4_90;
        esi144 = *reinterpret_cast<void***>(rbp5 + 48);
        edi145 = *reinterpret_cast<unsigned char*>(rbp5 + 0xb8);
        al138 = get_type_indicator(*reinterpret_cast<signed char*>(&edi145), esi144, edx134, rax132, r8_78, r9_34);
        if (!al138) 
            goto addr_cad4_90;
        goto addr_cb74_93;
    }
    addr_d04b_75:
    if (*reinterpret_cast<int32_t*>(&r8_78) < reinterpret_cast<int32_t>(0)) {
        width_2 = 0;
        *reinterpret_cast<uint32_t*>(&r8_78) = 0;
        *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
        goto addr_ca6e_80;
    }
    addr_d0b0_79:
    eax146 = mbsnwidth(r15_107, rax108);
    width_2 = eax146;
    *reinterpret_cast<uint32_t*>(&r8_78) = eax146;
    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
    goto addr_d04b_75;
    addr_cefb_87:
    rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<unsigned char>(rax131));
    goto addr_cf00_69;
    addr_4c96_16:
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
}