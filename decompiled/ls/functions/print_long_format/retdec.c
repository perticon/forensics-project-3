int64_t print_long_format(int64_t a1) {
    int64_t v1 = __readfsqword(40); // 0xc70d
    char * v2 = (char *)(a1 + 184); // 0xc720
    int64_t v3; // bp-4772, 0xc6f0
    if (*v2 == 0) {
        uint32_t v4 = *(int32_t *)(a1 + 168); // 0xc9a0
        char v5 = *(char *)((int64_t)v4 + (int64_t)"?pcdb-lswd"); // 0xc9c4
        v3 = v5;
    } else {
        // 0xc730
        filemodestring((struct stat *)(a1 + 24), (char *)&v3);
    }
    // 0xc741
    int32_t v6; // 0xc6f0
    if (*(char *)&any_has_acl != 0) {
        int32_t v7 = *(int32_t *)(a1 + 188); // 0xc948
        if (v7 == 1 || v7 != 2) {
            goto lab_0xc756;
        } else {
            // 0xc960
            v6 = time_type;
            if (time_type != 2) {
                goto lab_0xc765;
            } else {
                goto lab_0xc980;
            }
        }
    } else {
        goto lab_0xc756;
    }
  lab_0xc756:
    // 0xc756
    v6 = time_type;
    if (time_type == 2) {
        goto lab_0xc980;
    } else {
        goto lab_0xc765;
    }
  lab_0xc980:;
    int64_t v8 = *(int64_t *)(a1 + 96); // bp-4920, 0xc98e
    int64_t v9 = *(int64_t *)(a1 + 104); // 0xc998
    char v10 = 1; // 0xc998
    goto lab_0xc791;
  lab_0xc765:
    // 0xc765
    if (v6 < 2) {
        if (v6 == 0) {
            // 0xccf8
            v8 = *(int64_t *)(a1 + 112);
            v9 = *(int64_t *)(a1 + 120);
            v10 = 1;
        } else {
            // 0xc773
            v8 = *(int64_t *)(a1 + 128);
            v9 = *(int64_t *)(a1 + 136);
            v10 = 1;
        }
    } else {
        if (v6 != 3) {
            // 0xd115
            return print_long_format_cold();
        }
        int64_t v92 = *(int64_t *)(a1 + 112); // 0xc921
        int64_t v93 = *(int64_t *)(a1 + 120); // 0xc925
        v8 = v92;
        v9 = v93;
        v10 = (v93 & v92) != -1;
    }
    goto lab_0xc791;
  lab_0xc791:;
    // 0xc791
    int64_t v11; // bp-3720, 0xc6f0
    int64_t v12 = &v11; // 0xc791
    int64_t v13; // bp-4728, 0xc6f0
    if (*(char *)&print_inode != 0) {
        char v14 = *v2; // 0xcbe0
        if (v14 != 0) {
            int64_t v15 = *(int64_t *)(a1 + 32); // 0xcbf0
            if (v15 != 0) {
                // 0xcbf9
                umaxtostr(v15, (char *)&v13);
            }
        }
        int64_t v16 = (0x100000000 * function_4c80() >> 32) + v12; // 0xcc40
    }
    int64_t v17 = v12; // 0xc7b0
    if (*(char *)&print_block_size != 0) {
        int64_t v18 = (int64_t)&g9; // 0xc7c0
        if (*v2 != 0) {
            int64_t v19 = *(int64_t *)(a1 + 88); // 0xcf10
            char * v20 = human_readable(v19, (char *)&v13, human_output_opts, 512, output_block_size); // 0xcf2e
            v18 = (int64_t)v20;
        }
        int32_t v21 = block_size_width - gnu_mbswidth((char *)v18, 0); // 0xc7dd
        int64_t v22 = v12; // 0xc7e2
        if (v21 >= 1) {
            // 0xc7e4
            function_4920();
            v22 = v12 + (int64_t)v21;
        }
        int64_t v23 = v18; // 0xc6f0
        char v24 = *(char *)v23; // 0xc7fa
        int64_t v25 = v22 + 1; // 0xc802
        char * v26 = (char *)v22;
        *v26 = v24;
        int64_t v27 = v25; // 0xc80b
        v23++;
        while (v24 != 0) {
            // 0xc7fa
            v24 = *(char *)v23;
            v25 = v27 + 1;
            v26 = (char *)v27;
            *v26 = v24;
            v27 = v25;
            v23++;
        }
        // 0xc80d
        *v26 = 32;
        v17 = v25;
    }
    // 0xc811
    if (*v2 != 0) {
        // 0xcbc0
        umaxtostr(*(int64_t *)(a1 + 40), (char *)&v13);
    }
    int64_t v28 = function_4c80(); // 0xc851
    if (*(char *)&dired != 0) {
        // 0xcba0
        dired_outbuf("  ", 2);
    }
    int64_t v29 = (0x100000000 * v28 >> 32) + v17; // 0xc85b
    int64_t v30; // 0xc6f0
    if ((*(char *)&print_group | *(char *)&print_owner) == 0) {
        // 0xcb00
        v30 = v29;
        if ((*(char *)&print_scontext | *(char *)&print_author) == 0) {
            goto lab_0xc8c6;
        } else {
            goto lab_0xc881;
        }
    } else {
        goto lab_0xc881;
    }
  lab_0xc881:
    // 0xc881
    dired_outbuf((char *)&v11, v29 - v12);
    if (*(char *)&print_owner != 0) {
        uint32_t v31 = *(int32_t *)(a1 + 52); // 0xcc5c
        int64_t v32 = (int64_t)&g9; // 0xcc5f
        if (*v2 != 0) {
            // 0xcf40
            v32 = 0;
            if (*(char *)&numeric_ids == 0) {
                // 0xcf4f
                v32 = (int64_t)getuser(v31);
            }
        }
        // 0xcc65
        format_user_or_group(v32, (int64_t)v31, owner_width);
        if (*(char *)&print_group == 0) {
            goto lab_0xc8a9;
        } else {
            goto lab_0xcc77;
        }
    } else {
        // 0xc89c
        if (*(char *)&print_group != 0) {
            goto lab_0xcc77;
        } else {
            goto lab_0xc8a9;
        }
    }
  lab_0xc8c6:;
    int64_t v33 = (int64_t)&g9; // 0xc8cd
    int64_t v34; // 0xc6f0
    int64_t v35; // bp-4760, 0xc6f0
    if (*v2 == 0) {
        goto lab_0xc9f7;
    } else {
        // 0xc8d3
        if ((*(int32_t *)(a1 + 48) & 0xb000) == 0x2000) {
            int64_t * v36 = (int64_t *)(a1 + 64); // 0xcd6a
            uint64_t v37 = *v36; // 0xcd6a
            umaxtostr(v37 / 0x1000 & 0xffffff00 | v37 % 256, (char *)&v13);
            uint64_t v38 = *v36; // 0xcd93
            umaxtostr(v38 / 0x100000000 & 0xfffff000 | v38 / 256 & (int64_t)(int32_t)&g92, (char *)&v35);
            v34 = (0x100000000 * function_4c80() >> 32) + v30;
            goto lab_0xca42;
        } else {
            char * v39 = human_readable(*(int64_t *)(a1 + 72), (char *)&v13, file_human_output_opts, 1, file_output_block_size); // 0xc904
            v33 = (int64_t)v39;
            goto lab_0xc9f7;
        }
    }
  lab_0xc9f7:;
    int32_t v40 = file_size_width - gnu_mbswidth((char *)v33, 0); // 0xca0e
    int64_t v41 = v30; // 0xca13
    if (v40 >= 1) {
        // 0xca15
        function_4920();
        v41 = v30 + (int64_t)v40;
    }
    int64_t v42 = v33; // 0xc6f0
    char v43 = *(char *)v42; // 0xca2b
    int64_t v44 = v41 + 1; // 0xca33
    char * v45 = (char *)v41;
    *v45 = v43;
    int64_t v46 = v44; // 0xca3c
    v42++;
    while (v43 != 0) {
        // 0xca2b
        v43 = *(char *)v42;
        v44 = v46 + 1;
        v45 = (char *)v46;
        *v45 = v43;
        v46 = v44;
        v42++;
    }
    // 0xca3e
    *v45 = 32;
    v34 = v44;
    goto lab_0xca42;
  lab_0xc8a9:
    // 0xc8a9
    if (*(char *)&print_author != 0) {
        goto lab_0xcca6;
    } else {
        goto lab_0xc8b6;
    }
  lab_0xcc77:;
    uint32_t v90 = *(int32_t *)(a1 + 56); // 0xcc8b
    int64_t v91 = (int64_t)&g9; // 0xcc8e
    if (*v2 != 0) {
        // 0xcf70
        v91 = 0;
        if (*(char *)&numeric_ids == 0) {
            // 0xcf7f
            v91 = (int64_t)getgroup(v90);
        }
    }
    // 0xcc94
    format_user_or_group(v91, (int64_t)v90, group_width);
    if (*(char *)&print_author == 0) {
        goto lab_0xc8b6;
    } else {
        goto lab_0xcca6;
    }
  lab_0xca42:;
    char * v47 = (char *)v34;
    *v47 = 1;
    char * v48; // 0xc6f0
    int64_t v49; // 0xc6f0
    int32_t v50; // 0xc6f0
    if (v10 != 0 == (*v2 != 0)) {
        // 0xce08
        int64_t v51; // bp-4904, 0xc6f0
        if (localtime_rz(localtz, &v8, (struct tm *)&v51) == NULL) {
            goto lab_0xd070;
        } else {
            int64_t v52 = g78; // 0xce37
            int64_t v53 = v52 - v9; // 0xce37
            int64_t v54 = current_time; // 0xce3e
            int64_t v55 = v53 < 0 == ((v53 ^ v52) & (v9 ^ v52)) < 0 == (v53 != 0); // 0xce45
            int64_t v56 = v53 < 0 != ((v53 ^ v52) & (v9 ^ v52)) < 0; // 0xce4e
            int64_t v57 = v55 - v56; // 0xce54
            int64_t v58 = v54 - v8;
            int64_t v59 = (v58 ^ v54) & (v54 ^ v8);
            if ((int32_t)(2 * ((int64_t)(v58 < 0 == v59 < 0 == (v58 != 0)) - (int64_t)(v58 < 0 != v59 < 0)) + v57) < 0) {
                // 0xd0c8
                gettime();
            }
            int64_t v60 = v8;
            int64_t v61 = v58;
            int64_t v62 = v59;
            int64_t v63 = v54 - 0xf0c2ac; // 0xce76
            int64_t v64 = v63 - v60; // 0xce98
            uint64_t v65 = 2 * ((int64_t)(v64 < 0 == ((v64 ^ v63) & (v63 ^ v60)) < 0 == (v64 != 0)) - (int64_t)(v64 < 0 != ((v64 ^ v63) & (v63 ^ v60)) < 0)) + (v57 & 0xffffffff) & v56 - v55 + (2 * ((int64_t)(v61 < 0 != v62 < 0) - (int64_t)(v61 < 0 == v62 < 0 == (v61 != 0))) & 0xfffffffe);
            int64_t v66; // 0xc6f0
            if (*(char *)&use_abformat == 0) {
                int64_t v67 = *(int64_t *)((v65 / 0x10000000 & 8) + (int64_t)&long_time_format); // 0xd0a7
                v66 = v67;
            } else {
                // 0xcec3
                v66 = 128 * ((0x100000000 * v65 / 0x8000000000000000 & 12) + (int64_t)v50) + (int64_t)&abformat;
            }
            int64_t v68 = nstrftime(v47, (int64_t)&g84, (char *)v66, (struct tm *)&v51, localtz, (int32_t)v9); // 0xceed
            if (v68 == 0) {
                goto lab_0xd070;
            } else {
                int64_t v69 = v68 + v34; // 0xcefb
                v48 = (char *)v69;
                v49 = v69;
                goto lab_0xcf00;
            }
        }
    } else {
        goto lab_0xca5e;
    }
  lab_0xcca6:;
    uint32_t v70 = *(int32_t *)(a1 + 52); // 0xccba
    int64_t v71 = (int64_t)&g9; // 0xccbd
    if (*v2 != 0) {
        // 0xcfa0
        v71 = 0;
        if (*(char *)&numeric_ids == 0) {
            // 0xcfaf
            v71 = (int64_t)getuser(v70);
        }
    }
    // 0xccc3
    format_user_or_group(v71, (int64_t)v70, author_width);
    v30 = v12;
    if (*(char *)&print_scontext == 0) {
        goto lab_0xc8c6;
    } else {
        goto lab_0xccd8;
    }
  lab_0xc8b6:
    // 0xc8b6
    v30 = v12;
    if (*(char *)&print_scontext != 0) {
        goto lab_0xccd8;
    } else {
        goto lab_0xc8c6;
    }
  lab_0xca5e:;
    // 0xca5e
    int64_t v72; // 0xc6f0
    int32_t v73; // 0xc6f0
    if (g40 < 0) {
        int64_t v74 = 0; // bp-4928, 0xcfe1
        int64_t v75; // bp-4840, 0xc6f0
        if (localtime_rz(localtz, &v74, (struct tm *)&v75) == NULL) {
            // 0xd044
            v73 = g40;
            goto lab_0xd04b;
        } else {
            int64_t v76 = *(int64_t *)&long_time_format; // 0xd00c
            if (*(char *)&use_abformat != 0) {
                // 0xd00e
                v76 = 128 * (int64_t)v50 + (int64_t)&abformat;
            }
            int64_t v77 = nstrftime((char *)&v13, (int64_t)&g84, (char *)v76, (struct tm *)&v75, localtz, 0); // 0xd03a
            if (v77 != 0) {
                int32_t v78 = mbsnwidth((char *)&v13, v77, 0); // 0xd0b8
                g40 = v78;
                v73 = v78;
                goto lab_0xd04b;
            } else {
                // 0xd044
                v73 = g40;
                goto lab_0xd04b;
            }
        }
    } else {
        // 0xca6e
        v72 = (0x100000000 * function_4c80() >> 32) + v34;
        goto lab_0xca93;
    }
  lab_0xccd8:
    // 0xccd8
    format_user_or_group(*(int64_t *)(a1 + 176), 0, scontext_width);
    v30 = v12;
    goto lab_0xc8c6;
  lab_0xd070:
    // 0xd070
    v48 = v47;
    v49 = v34;
    if (*v47 == 0) {
        goto lab_0xcf00;
    } else {
        // 0xd079
        if (*v2 != 0) {
            // 0xd086
            imaxtostr(v8, (char *)&v35);
        }
        goto lab_0xca5e;
    }
  lab_0xcf00:
    // 0xcf00
    *v48 = 32;
    v72 = v49 + 1;
    goto lab_0xca93;
  lab_0xca93:;
    int64_t v79 = v72 - v12; // 0xca93
    dired_outbuf((char *)&v11, v79);
    int32_t * v80 = (int32_t *)a1; // 0xcab3
    int64_t v81 = print_name_with_quoting(v80, false, (int32_t *)&dired_obstack, v79); // 0xcab3
    int32_t v82 = *(int32_t *)(a1 + 168); // 0xcab8
    int64_t v83; // 0xc6f0
    if (v82 == 6) {
        // 0xcb20
        if (*(int64_t *)(a1 + 8) == 0) {
            goto lab_0xcad4;
        } else {
            // 0xcb27
            dired_outbuf(" -> ", 4);
            int64_t v84 = v79 + 4 + v81; // 0xcb3a
            print_name_with_quoting(v80, true, NULL, v84);
            if (indicator_style == 0) {
                goto lab_0xcad4;
            } else {
                int64_t v85 = get_type_indicator(1, *(int32_t *)(a1 + 172), 0, v84); // 0xcb67
                v83 = v85;
                if ((char)v85 == 0) {
                    goto lab_0xcad4;
                } else {
                    goto lab_0xcb74;
                }
            }
        }
    } else {
        // 0xcac6
        if (indicator_style != 0) {
            int64_t v86 = get_type_indicator(*v2, *(int32_t *)(a1 + 48), v82, v79); // 0xcd22
            v83 = v86;
            if ((char)v86 == 0) {
                goto lab_0xcad4;
            } else {
                goto lab_0xcb74;
            }
        } else {
            goto lab_0xcad4;
        }
    }
  lab_0xd04b:
    // 0xd04b
    if (v73 < 0) {
        // 0xd054
        g40 = 0;
    }
    // 0xca6e
    v72 = (0x100000000 * function_4c80() >> 32) + v34;
    goto lab_0xca93;
  lab_0xcad4:;
    int64_t result = v1 - __readfsqword(40); // 0xcadc
    if (result == 0) {
        // 0xcaeb
        return result;
    }
    // 0xd110
    function_4870();
    // 0xd115
    return print_long_format_cold();
  lab_0xcb74:;
    int64_t v87 = (int64_t)g59; // 0xcb74
    dired_pos++;
    int64_t * v88 = (int64_t *)(v87 + 40); // 0xcb83
    uint64_t v89 = *v88; // 0xcb83
    if (v89 >= *(int64_t *)(v87 + 48)) {
        // 0xd103
        function_48d0();
    } else {
        // 0xcb91
        *v88 = v89 + 1;
        *(char *)v89 = (char)v83;
    }
    goto lab_0xcad4;
}