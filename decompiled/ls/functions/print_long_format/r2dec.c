int64_t print_long_format (uint32_t arg_8h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_34h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_a8h, int64_t arg_ach, int64_t arg_b0h, uint32_t arg_b8h, int64_t arg_bch, uint32_t arg1) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_b4h;
    int64_t var_b5h;
    int64_t var_bdh;
    int64_t var_beh;
    int64_t var_bfh;
    int64_t var_c0h;
    int64_t var_e0h;
    int64_t var_4d0h;
    int64_t var_1318h;
    rdi = arg1;
    rax = *(fs:0x28);
    *((rsp + 0x1318)) = rax;
    eax = 0;
    if (*((rdi + 0xb8)) == 0) {
        goto label_26;
    }
    filemodestring (rdi + 0x18, rsp + 0xb4);
label_3:
    if (*(obj.any_has_acl) != 0) {
        goto label_27;
    }
    *((rsp + 0xbe)) = 0;
label_1:
    eax = time_type;
    if (eax == 2) {
        goto label_28;
    }
    if (eax > 2) {
label_2:
        goto label_29;
    }
    if (eax == 0) {
        goto label_30;
    }
    rax = *((rbp + 0x88));
    rdx = *((rbp + 0x80));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
label_0:
    r12 = rsp + 0x4d0;
    rbx = r12;
    if (*(obj.print_inode) != 0) {
        goto label_31;
    }
label_9:
    if (*(obj.print_block_size) == 0) {
        goto label_32;
    }
    r15 = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        goto label_33;
    }
label_15:
    r14d = block_size_width;
    eax = gnu_mbswidth (r15, 0);
    r8d = eax;
    eax = r14d;
    eax -= r8d;
    if (eax <= 0) {
        goto label_34;
    }
    r14 = (int64_t) eax;
    rbx += r14;
    memset (rbx, 0x20, r14);
    do {
label_34:
        eax = *(r15);
        r15++;
        rbx++;
        *((rbx - 1)) = al;
    } while (al != 0);
    *((rbx - 1)) = 0x20;
label_32:
    rax = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        goto label_35;
    }
label_8:
    rdi = rbx;
    r9d = nlink_width;
    rcx = "%s %*s ";
    rdx = 0xffffffffffffffff;
    eax = 0;
    esi = 1;
    r8 = rsp + 0xc4;
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rbx += rax;
    if (*(obj.dired) != 0) {
        goto label_36;
    }
label_7:
    if (*(obj.print_owner) == 0) {
        if (*(obj.print_group) == 0) {
            goto label_37;
        }
    }
label_4:
    rsi = rbx;
    rsi -= r12;
    dired_outbuf (r12);
    if (*(obj.print_owner) != 0) {
        goto label_38;
    }
    if (*(obj.print_group) != 0) {
        goto label_39;
    }
label_10:
    if (*(obj.print_author) != 0) {
        goto label_40;
    }
label_11:
    rbx = r12;
    if (*(obj.print_scontext) != 0) {
        goto label_41;
    }
label_5:
    if (*((rbp + 0xb8)) == 0) {
        goto label_42;
    }
    eax = *((rbp + 0x30));
    eax &= 0xb000;
    if (eax == 0x2000) {
        goto label_43;
    }
    rax = human_readable (*((rbp + 0x48)), rsp + 0xe0, *(obj.file_human_output_opts), 1, *(obj.file_output_block_size), r9);
    r15 = rax;
    goto label_44;
label_29:
    if (eax != 3) {
        goto label_45;
    }
    rax = *((rbp + 0x70));
    rdx = *((rbp + 0x78));
    *((rsp + 0x20)) = rax;
    rax &= rdx;
    *((rsp + 0x28)) = rdx;
    r13b = (rax != -1) ? 1 : 0;
    goto label_0;
label_27:
    eax = *((rbp + 0xbc));
    if (eax == 1) {
        goto label_46;
    }
    if (eax != 2) {
        goto label_1;
    }
    eax = time_type;
    *((rsp + 0xbe)) = 0x2b;
    if (eax != 2) {
        goto label_2;
    }
label_28:
    rax = *((rbp + 0x68));
    rdx = *((rbp + 0x60));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
    goto label_0;
label_26:
    eax = *((rdi + 0xa8));
    rdx = "?pcdb-lswd";
    r9d = 0x3f3f;
    *((rsp + 0xbf)) = 0;
    *((rsp + 0xbd)) = r9w;
    eax = *((rdx + rax));
    *((rsp + 0xb4)) = al;
    rax = 0x3f3f3f3f3f3f3f3f;
    *((rsp + 0xb5)) = rax;
    goto label_3;
label_42:
    r15 = 0x0001bb0b;
label_44:
    r14d = file_size_width;
    eax = gnu_mbswidth (r15, 0);
    r8d = eax;
    eax = r14d;
    eax -= r8d;
    if (eax <= 0) {
        goto label_47;
    }
    r14 = (int64_t) eax;
    rbx += r14;
    memset (rbx, 0x20, r14);
    do {
label_47:
        eax = *(r15);
        r15++;
        rbx++;
        *((rbx - 1)) = al;
    } while (al != 0);
    *((rbx - 1)) = 0x20;
label_13:
    *(rbx) = 1;
    if (*((rbp + 0xb8)) != 0) {
        if (r13b != 0) {
            goto label_48;
        }
    }
label_21:
    r13 = 0x0001bb0b;
label_22:
    r8d = width.2;
    if (r8d < 0) {
        goto label_49;
    }
label_19:
    r9 = r13;
    rcx = 0x0001bb1d;
    rdi = rbx;
    eax = 0;
    rdx = 0xffffffffffffffff;
    esi = 1;
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rax += rbx;
label_14:
    rax -= r12;
    rbx = rax;
    rsi = rax;
    dired_outbuf (r12);
    rax = print_name_with_quoting (rbp, 0, obj.dired_obstack, rbx);
    edx = *((rbp + 0xa8));
    r12 = rax;
    if (edx == 6) {
        goto label_50;
    }
    eax = indicator_style;
    if (eax != 0) {
        goto label_51;
    }
label_6:
    rax = *((rsp + 0x1318));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_52;
    }
    return rax;
label_37:
    if (*(obj.print_author) != 0) {
        goto label_4;
    }
    if (*(obj.print_scontext) == 0) {
        goto label_5;
    }
    goto label_4;
label_50:
    if (*((rbp + 8)) == 0) {
        goto label_6;
    }
    esi = 4;
    dired_outbuf (" -> ");
    print_name_with_quoting (rbp, 1, 0, rbx + r12 + 4);
    edx = indicator_style;
    if (edx == 0) {
        goto label_6;
    }
    esi = *((rbp + 0xac));
    edx = 0;
    edi = 1;
    al = get_type_indicator ();
    if (al == 0) {
        goto label_6;
    }
label_12:
    rdi = stdout;
    *(obj.dired_pos)++;
    rdx = *((rdi + 0x28));
    if (rdx >= *((rdi + 0x30))) {
        goto label_53;
    }
    rcx = rdx + 1;
    *((rdi + 0x28)) = rcx;
    *(rdx) = al;
    goto label_6;
label_36:
    esi = 2;
    dired_outbuf (0x0001bb15);
    goto label_7;
label_35:
    umaxtostr (*((rbp + 0x28)), rsp + 0xe0, rdx);
    goto label_8;
label_31:
    r9 = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        rdi = *((rbp + 0x20));
        if (rdi == 0) {
            goto label_54;
        }
        rax = umaxtostr (rdi, rsp + 0xe0, rdx);
        r9 = rax;
    }
label_54:
    r12 = rsp + 0x4d0;
    r8d = inode_number_width;
    edx = 0xe3b;
    eax = 0;
    rcx = 0x0001bb1d;
    esi = 1;
    rdi = r12;
    eax = sprintf_chk ();
    rbx = (int64_t) eax;
    rbx += r12;
    goto label_9;
label_38:
    edx = owner_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x34));
    if (*((rbp + 0xb8)) != 0) {
        goto label_55;
    }
label_16:
    format_user_or_group ();
    if (*(obj.print_group) == 0) {
        goto label_10;
    }
label_39:
    edx = group_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x38));
    if (*((rbp + 0xb8)) != 0) {
        goto label_56;
    }
label_17:
    format_user_or_group ();
    if (*(obj.print_author) == 0) {
        goto label_11;
    }
label_40:
    edx = author_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x34));
    if (*((rbp + 0xb8)) != 0) {
        goto label_57;
    }
label_18:
    format_user_or_group ();
    rbx = r12;
    if (*(obj.print_scontext) == 0) {
        goto label_5;
    }
label_41:
    rdi = *((rbp + 0xb0));
    edx = scontext_width;
    esi = 0;
    format_user_or_group ();
    goto label_5;
label_30:
    rax = *((rbp + 0x78));
    rdx = *((rbp + 0x70));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
    goto label_0;
label_51:
    esi = *((rbp + 0x30));
    edi = *((rbp + 0xb8));
    al = get_type_indicator ();
    if (al == 0) {
        goto label_6;
    }
    goto label_12;
label_46:
    *((rsp + 0xbe)) = 0x2e;
    goto label_1;
label_43:
    eax = minor_device_number_width;
    r14d = file_size_width;
    eax = rdx + rax + 2;
    r14d -= eax;
    rax = *((rbp + 0x40));
    edx = (int32_t) al;
    rax >>= 0xc;
    dil = 0;
    edi |= edx;
    rax = umaxtostr (rax, rsp + 0xe0, *(obj.major_device_number_width));
    r15 = rax;
    rax = *((rbp + 0x40));
    *((rsp + 4)) = edx;
    rcx = rax;
    rax >>= 0x20;
    rcx >>= 8;
    ecx &= 0xfff;
    edi &= 0xfffff000;
    edi |= ecx;
    rax = umaxtostr (rax, rsp + 0xc0, *(obj.minor_device_number_width));
    edx = *((rsp + 0xc));
    esi = 1;
    r9 = rax;
    eax = 0;
    rdi = rbx;
    __asm ("cmovns eax, r14d");
    rcx = "%*s, %*s ";
    rdx = 0xffffffffffffffff;
    r8d = eax;
    eax = 0;
    r8d += *(obj.major_device_number_width);
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rbx += rax;
    goto label_13;
label_48:
    r13 = rsp + 0x30;
    rax = localtime_rz (*(obj.localtz), rsp + 0x20, r13);
    if (rax == 0) {
        goto label_58;
    }
    r8d = 0;
    r9 = *((rsp + 0x28));
    rdx = *((rsp + 0x20));
    rcx = current_time;
    r8b = (*(0x00026378) > r9) ? 1 : 0;
    al = (*(0x00026378) < r9) ? 1 : 0;
    edi = 0;
    eax = (int32_t) al;
    esi = r8d;
    esi -= eax;
    r10b = (rcx < rdx) ? 1 : 0;
    dil = (rcx > rdx) ? 1 : 0;
    r10d = (int32_t) r10b;
    edi -= r10d;
    edi = rsi + rdi*2;
    if (edi < 0) {
        goto label_59;
    }
label_25:
    rdi = rcx - 0xf0c2ac;
    r10b = (rcx > rdx) ? 1 : 0;
    cl = (rcx < rdx) ? 1 : 0;
    eax -= r8d;
    r8 = localtz;
    ecx = (int32_t) cl;
    r10d = (int32_t) r10b;
    ecx -= r10d;
    dl = (rdi > rdx) ? 1 : 0;
    eax = rax + rcx*2;
    cl = (rdi < rdx) ? 1 : 0;
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    edx -= ecx;
    edx = rsi + rdx*2;
    eax &= edx;
    edx = eax;
    edx >>= 0x1f;
    if (*(obj.use_abformat) == 0) {
        goto label_60;
    }
    rax = (int64_t) eax;
    rdx = *((rsp + 0x40));
    rax >>= 0x3f;
    eax &= 0xc;
    rax += rdx;
    rdx = obj_abformat;
    rax <<= 7;
    rdx += rax;
label_23:
    rcx = r13;
    rax = nstrftime (rbx, 0x3e9);
    if (rax == 0) {
        goto label_58;
    }
    rbx += rax;
label_20:
    *(rbx) = 0x20;
    rax = rbx + 1;
    goto label_14;
label_33:
    rax = human_readable (*((rbp + 0x58)), rsp + 0xe0, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    r15 = rax;
    goto label_15;
label_55:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_16;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getuser (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_16;
label_56:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_17;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getgroup (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_17;
label_57:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_18;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getuser (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_18;
label_49:
    r14 = rsp + 0x70;
    *((rsp + 0x18)) = 0;
    rax = localtime_rz (*(obj.localtz), rsp + 0x18, r14);
    if (rax != 0) {
        r8 = localtz;
        rdx = long_time_format;
        if (*(obj.use_abformat) != 0) {
            rdx = *((rsp + 0x80));
            rax = obj_abformat;
            rdx <<= 7;
            rdx += rax;
        }
        r15 = rsp + 0xe0;
        r9d = 0;
        rcx = r14;
        rax = nstrftime (r15, 0x3e9);
        if (rax != 0) {
            goto label_61;
        }
    }
    r8d = width.2;
label_24:
    if (r8d >= 0) {
        goto label_19;
    }
    *(obj.width.2) = 0;
    r8d = 0;
    goto label_19;
label_58:
    if (*(rbx) == 0) {
        goto label_20;
    }
    if (*((rbp + 0xb8)) == 0) {
        goto label_21;
    }
    rax = imaxtostr (*((rsp + 0x20)), rsp + 0xc0, rdx);
    r13 = rax;
    goto label_22;
label_60:
    rax = obj_long_time_format;
    rdx = *((rax + rdx*8));
    goto label_23;
label_61:
    edx = 0;
    eax = mbsnwidth (r15, rax);
    *(obj.width.2) = eax;
    r8d = eax;
    goto label_24;
label_59:
    rdi = obj_current_time;
    gettime ();
    r8d = 0;
    r9 = *((rsp + 0x28));
    r8b = (r9 < *(0x00026378)) ? 1 : 0;
    al = (r9 > *(0x00026378)) ? 1 : 0;
    rdx = *((rsp + 0x20));
    rcx = current_time;
    eax = (int32_t) al;
    esi = r8d;
    esi -= eax;
    goto label_25;
label_53:
    esi = (int32_t) al;
    overflow ();
    goto label_6;
label_52:
    stack_chk_fail ();
label_45:
    return print_long_format_cold ();
}