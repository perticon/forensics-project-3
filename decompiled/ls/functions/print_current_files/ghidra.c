void print_current_files(void)

{
  long lVar1;
  byte *pbVar2;
  byte bVar3;
  char cVar4;
  ulong uVar5;
  long lVar6;
  ulong uVar7;
  ulong uVar8;
  long lVar9;
  undefined8 uVar10;
  long lVar11;
  long lVar12;
  ulong local_40;
  
  switch(format) {
  case 0:
    uVar5 = 0;
    if (cwd_n_used == 0) {
      return;
    }
    do {
      if ((print_with_color != '\0') && (cVar4 = is_colored(4), cVar4 != '\0')) {
        put_indicator(color_indicator);
        put_indicator(0x1250a0);
        put_indicator(0x125070);
      }
      print_long_format(sorted_file[uVar5]);
      bVar3 = eolbyte;
      dired_pos = dired_pos + 1;
      pbVar2 = (byte *)stdout->_IO_write_ptr;
      if (pbVar2 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
        *pbVar2 = bVar3;
      }
      else {
        __overflow(stdout,(uint)eolbyte);
      }
      uVar5 = uVar5 + 1;
    } while (uVar5 < cwd_n_used);
    return;
  case 1:
    uVar5 = 0;
    if (cwd_n_used != 0) {
      do {
        print_file_name_and_frills_isra_0(sorted_file[uVar5]);
        bVar3 = eolbyte;
        pbVar2 = (byte *)stdout->_IO_write_ptr;
        if (pbVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
          *pbVar2 = bVar3;
        }
        else {
          __overflow(stdout,(uint)eolbyte);
        }
        uVar5 = uVar5 + 1;
      } while (uVar5 < cwd_n_used);
    }
    return;
  case 2:
    if (line_length != 0) {
      uVar5 = calculate_columns(1);
      local_40 = 0;
      lVar1 = column_info + uVar5 * 0x18;
      uVar5 = (cwd_n_used / uVar5 + 1) - (ulong)(cwd_n_used % uVar5 == 0);
      if (uVar5 == 0) {
        return;
      }
      do {
        lVar6 = 0;
        uVar8 = local_40;
        lVar11 = 0;
        while( true ) {
          uVar10 = sorted_file[uVar8];
          lVar12 = length_of_file_name_and_frills();
          lVar9 = *(long *)(*(long *)(lVar1 + -8) + lVar6);
          lVar6 = lVar6 + 8;
          print_file_name_and_frills_isra_0(uVar10);
          bVar3 = eolbyte;
          uVar8 = uVar8 + uVar5;
          if (cwd_n_used <= uVar8) break;
          lVar9 = lVar9 + lVar11;
          indent(lVar12 + lVar11,lVar9);
          lVar11 = lVar9;
        }
        pbVar2 = (byte *)stdout->_IO_write_ptr;
        if (pbVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
          *pbVar2 = bVar3;
        }
        else {
          __overflow(stdout,(uint)eolbyte);
        }
        local_40 = local_40 + 1;
      } while (uVar5 != local_40);
      return;
    }
    break;
  case 3:
    if (line_length != 0) {
      uVar5 = calculate_columns(0);
      lVar1 = column_info + -0x18 + uVar5 * 0x18;
      uVar10 = *sorted_file;
      lVar6 = length_of_file_name_and_frills(uVar10);
      lVar11 = **(long **)(lVar1 + 0x10);
      print_file_name_and_frills_isra_0(uVar10);
      if (1 < cwd_n_used) {
        uVar8 = 1;
        lVar9 = 0;
        do {
          bVar3 = eolbyte;
          uVar7 = uVar8 % uVar5;
          if (uVar7 == 0) {
            pbVar2 = (byte *)stdout->_IO_write_ptr;
            if (pbVar2 < stdout->_IO_write_end) {
              lVar12 = 0;
              stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
              *pbVar2 = bVar3;
            }
            else {
              lVar12 = 0;
              __overflow(stdout,(uint)eolbyte);
            }
          }
          else {
            lVar12 = lVar11 + lVar9;
            indent(lVar9 + lVar6,lVar12);
          }
          uVar10 = sorted_file[uVar8];
          uVar8 = uVar8 + 1;
          print_file_name_and_frills_isra_0(uVar10);
          lVar6 = length_of_file_name_and_frills(uVar10);
          lVar11 = *(long *)(*(long *)(lVar1 + 0x10) + uVar7 * 8);
          lVar9 = lVar12;
        } while (uVar8 < cwd_n_used);
      }
      bVar3 = eolbyte;
      pbVar2 = (byte *)stdout->_IO_write_ptr;
      if (pbVar2 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
        *pbVar2 = bVar3;
        return;
      }
      __overflow(stdout,(uint)eolbyte);
      return;
    }
    break;
  case 4:
    uVar10 = 0x2c;
    goto LAB_0010d255;
  default:
    return;
  }
  uVar10 = 0x20;
LAB_0010d255:
  print_with_separator(uVar10);
  return;
}