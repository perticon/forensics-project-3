void print_current_files(void** rdi, void** rsi, ...) {
    int1_t below_or_equal3;
    int64_t rax4;

    below_or_equal3 = reinterpret_cast<unsigned char>(format) <= reinterpret_cast<unsigned char>(4);
    if (!below_or_equal3) {
        return;
    } else {
        *reinterpret_cast<void***>(&rax4) = format;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1a17c + rax4 * 4) + 0x1a17c;
    }
}