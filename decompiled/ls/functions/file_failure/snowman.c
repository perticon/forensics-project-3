void file_failure(uint32_t edi, void** rsi, void** rdx, ...) {
    uint32_t ebx4;
    int32_t eax5;

    ebx4 = edi;
    quotearg_style(4, rdx);
    fun_46f0();
    fun_4b70();
    if (!*reinterpret_cast<signed char*>(&ebx4)) {
        eax5 = exit_status;
        if (!eax5) {
            exit_status = 1;
            return;
        }
    } else {
        exit_status = 2;
    }
    return;
}