void do_statx(undefined8 param_1,undefined8 param_2,ulong *param_3,uint param_4,uint param_5)

{
  int iVar1;
  long in_FS_OFFSET;
  undefined uStack296;
  byte local_127;
  uint local_124;
  uint local_118;
  undefined8 local_114;
  ushort local_10c;
  ulong local_108;
  ulong local_100;
  ulong local_f8;
  ulong local_e8;
  uint local_e0;
  ulong local_d8;
  uint local_d0;
  ulong local_c8;
  uint local_c0;
  ulong local_b8;
  uint local_b0;
  uint local_a8;
  uint local_a4;
  uint local_a0;
  uint local_9c;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = statx(param_1,param_2,param_4 | 0x800,param_5,&uStack296);
  if (-1 < iVar1) {
    *param_3 = ((ulong)local_9c & 0xffffff00) << 0xc |
               ((ulong)local_a0 & 0xfffff000) << 0x20 | (ulong)((local_a0 & 0xfff) << 8) |
               (ulong)(byte)local_9c;
    param_3[2] = (ulong)local_118;
    param_3[1] = local_108;
    *(uint *)(param_3 + 3) = (uint)local_10c;
    *(undefined8 *)((long)param_3 + 0x1c) = local_114;
    param_3[5] = ((ulong)local_a4 & 0xffffff00) << 0xc |
                 ((ulong)local_a8 & 0xfffff000) << 0x20 | (ulong)((local_a8 & 0xfff) << 8) |
                 (ulong)(byte)local_a4;
    param_3[7] = (ulong)local_124;
    param_3[6] = local_100;
    param_3[10] = (ulong)local_e0;
    param_3[8] = local_f8;
    param_3[0xc] = (ulong)local_b0;
    param_3[9] = local_e8;
    param_3[0xe] = (ulong)local_c0;
    param_3[0xb] = local_b8;
    param_3[0xd] = local_c8;
    if ((param_5 & 0x800) != 0) {
      if ((local_127 & 8) == 0) {
        param_3[0xc] = 0xffffffffffffffff;
        param_3[0xb] = 0xffffffffffffffff;
      }
      else {
        param_3[0xb] = local_d8;
        param_3[0xc] = (ulong)local_d0;
      }
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}