int32_t do_statx(int32_t fd, char * name, struct stat * st, int32_t flags, int32_t mask) {
    // 0x6fe0
    int32_t v1; // 0x6fe0
    uint32_t v2 = v1;
    uint32_t v3 = v1;
    int64_t v4 = __readfsqword(40); // 0x6ff6
    int32_t result = function_4b30(); // 0x7011
    if (result >= 0) {
        int64_t v5 = (int64_t)st;
        int64_t v6 = 0x100000000 * (int64_t)v1; // 0x705b
        int64_t v7 = v1 < 0xfffff001 ? v6 : v6 + 0xfffffffffff; // 0x7063
        *(int64_t *)st = 0x1000 * (int64_t)v3 & 0xffffff00000 | (int64_t)(v3 % 256) | (int64_t)(256 * v1 & 0xfff00) | v7;
        *(int64_t *)(v5 + 16) = (int64_t)v1;
        int64_t v8 = 0x100000000 * (int64_t)v1; // 0x70ac
        *(int32_t *)(v5 + 24) = v1 % 0x10000;
        int64_t v9 = v1 < 0xfffff001 ? v8 : v8 + 0xfffffffffff; // 0x70bc
        *(int64_t *)(v5 + 40) = 0x1000 * (int64_t)v2 & 0xffffff00000 | (int64_t)(v2 % 256) | (int64_t)(256 * v1 & 0xfff00) | v9;
        *(int64_t *)(v5 + 56) = (int64_t)v1;
        *(int64_t *)(v5 + 80) = (int64_t)v1;
        int64_t * v10 = (int64_t *)(v5 + 96); // 0x7119
        *v10 = (int64_t)v1;
        *(int64_t *)(v5 + 112) = (int64_t)v1;
        if ((mask & (int32_t)&g89) != 0) {
            char v11; // 0x6fe0
            if ((v11 & 8) == 0) {
                // 0x7160
                *v10 = -1;
                *(int64_t *)(v5 + 88) = -1;
            } else {
                // 0x7148
                *v10 = (int64_t)(uint32_t)v1;
            }
        }
    }
    // 0x7015
    if (v4 != __readfsqword(40)) {
        // 0x7175
        return function_4870();
    }
    // 0x702c
    return result;
}