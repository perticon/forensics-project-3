void extract_dirs_from_files(long param_1,undefined param_2)

{
  long *plVar1;
  int iVar2;
  char **ppcVar3;
  char *pcVar4;
  long *plVar5;
  undefined8 *puVar6;
  undefined8 uVar7;
  char *pcVar8;
  void *__ptr;
  long *plVar9;
  long lVar10;
  bool bVar11;
  
  if ((param_1 != 0) && (active_dir_set != 0)) {
    puVar6 = (undefined8 *)xmalloc(0x20);
    uVar7 = xstrdup(param_1);
    *puVar6 = 0;
    puVar6[1] = uVar7;
    *(undefined *)(puVar6 + 2) = 0;
    puVar6[3] = pending_dirs;
    pending_dirs = puVar6;
  }
  lVar10 = cwd_n_used + -1;
  if (cwd_n_used != 0) {
    do {
      while( true ) {
        ppcVar3 = (char **)sorted_file[lVar10];
        if ((*(int *)(ppcVar3 + 0x15) == 3) || (*(int *)(ppcVar3 + 0x15) == 9)) break;
LAB_00109460:
        bVar11 = lVar10 == 0;
        lVar10 = lVar10 + -1;
        if (bVar11) goto LAB_00109508;
      }
      pcVar4 = *ppcVar3;
      if (param_1 == 0) {
LAB_00109560:
        queue_directory(pcVar4,ppcVar3[1],param_2);
      }
      else {
        pcVar8 = (char *)last_component(pcVar4);
        if ((*pcVar8 == '.') &&
           ((pcVar8[(ulong)(pcVar8[1] == '.') + 1] == '\0' ||
            (pcVar8[(ulong)(pcVar8[1] == '.') + 1] == '/')))) goto LAB_00109460;
        if (*pcVar4 == '/') goto LAB_00109560;
        __ptr = (void *)file_name_concat(param_1,pcVar4,0);
        queue_directory(__ptr,ppcVar3[1],param_2);
        free(__ptr);
      }
      if (*(int *)(ppcVar3 + 0x15) != 9) goto LAB_00109460;
      free(*ppcVar3);
      free(ppcVar3[1]);
      free(ppcVar3[2]);
      bVar11 = lVar10 != 0;
      lVar10 = lVar10 + -1;
    } while (bVar11);
LAB_00109508:
    plVar5 = sorted_file;
    if (cwd_n_used != 0) {
      plVar1 = sorted_file + cwd_n_used;
      cwd_n_used = 0;
      plVar9 = sorted_file;
      do {
        iVar2 = *(int *)(*plVar9 + 0xa8);
        plVar5[cwd_n_used] = *plVar9;
        plVar9 = plVar9 + 1;
        cwd_n_used = cwd_n_used + (ulong)(iVar2 != 9);
      } while (plVar1 != plVar9);
      return;
    }
  }
  cwd_n_used = 0;
  return;
}