void extract_dirs_from_files(char * dirname, bool command_line_arg) {
    // 0x93f0
    if (dirname != NULL && active_dir_set != NULL) {
        int64_t v1 = xmalloc(); // 0x9413
        char * v2 = xstrdup(dirname); // 0x941e
        *(int64_t *)v1 = 0;
        *(int64_t *)(v1 + 8) = (int64_t)v2;
        *(char *)(v1 + 16) = 0;
        *(int64_t *)(v1 + 24) = (int64_t)pending_dirs;
        *(int64_t *)&pending_dirs = v1;
    }
    int64_t v3 = command_line_arg ? 255 : 0; // 0x93f4
    int64_t v4 = cwd_n_used; // 0x9452
    if (cwd_n_used == 0) {
        // 0x95a0
        cwd_n_used = 0;
        return;
    }
    int64_t v5; // 0x93f0
    int64_t v6; // 0x9485
    char * v7; // 0x9495
    int64_t v8; // 0x9471
    int32_t * v9; // 0x9475
    while (true) {
      lab_0x946a_2:
        // 0x946a
        v5 = v4 - 1;
        v8 = *(int64_t *)(8 * v5 + (int64_t)sorted_file);
        v9 = (int32_t *)(v8 + 168);
        int32_t v10 = *v9; // 0x9475
        if (v10 != 3 == (v10 != 9)) {
            goto lab_0x9460;
        } else {
            // 0x9485
            v6 = *(int64_t *)v8;
            if (dirname == NULL) {
                // 0x9560
                queue_directory((int64_t *)v6, (int32_t)*(int64_t *)(v8 + 8), v3);
                goto lab_0x94d4;
            } else {
                // 0x9492
                v7 = (char *)v6;
                char * v11 = last_component(v7); // 0x9495
                if (*v11 == 46) {
                    int64_t v12 = (int64_t)v11 + 1; // 0x957a
                    char v13 = *(char *)(v12 + (int64_t)(*(char *)v12 == 46)); // 0x9581
                    switch (v13) {
                        case 0: {
                            goto lab_0x9460;
                        }
                        case 47: {
                            goto lab_0x9460;
                        }
                        default: {
                            goto lab_0x94a3;
                        }
                    }
                } else {
                    goto lab_0x94a3;
                }
            }
        }
    }
  lab_0x9508_2:;
    int64_t v14 = cwd_n_used; // 0x9508
    if (v14 == 0) {
        // 0x95a0
        cwd_n_used = 0;
        return;
    }
    int64_t v15 = (int64_t)sorted_file; // 0x9518
    int64_t v16 = 0; // 0x9528
    int64_t v17 = *(int64_t *)v15; // 0x9530
    *(int64_t *)(8 * v16 + v15) = v17;
    int64_t v18 = v15 + 8; // 0x9541
    int64_t v19 = v16 + (int64_t)(*(int32_t *)(v17 + 168) != 9); // 0x9548
    int64_t v20 = v18; // 0x954e
    v16 = v19;
    while (8 * v14 + v15 != v18) {
        // 0x9530
        v17 = *(int64_t *)v20;
        *(int64_t *)(8 * v16 + v15) = v17;
        v18 = v20 + 8;
        v19 = v16 + (int64_t)(*(int32_t *)(v17 + 168) != 9);
        v20 = v18;
        v16 = v19;
    }
    // 0x9550
    cwd_n_used = v19;
  lab_0x9460:
    if (v5 == 0) {
        // break -> 0x9508
        goto lab_0x9508_2;
    }
    // 0x946a
    v4 = v5;
    goto lab_0x946a_2;
  lab_0x94d4:
    // 0x94d4
    if (*v9 != 9) {
        goto lab_0x9460;
    } else {
        // 0x94dd
        function_4630();
        function_4630();
        function_4630();
        if (v5 == 0) {
            // break -> 0x9508
            goto lab_0x9508_2;
        }
        // 0x946a
        v4 = v5;
        goto lab_0x946a_2;
    }
  lab_0x94a3:
    // 0x94a3
    if (*v7 == 47) {
        // 0x9560
        queue_directory((int64_t *)v6, (int32_t)*(int64_t *)(v8 + 8), v3);
        goto lab_0x94d4;
    } else {
        char * v21 = file_name_concat(dirname, v7, NULL); // 0x94b5
        queue_directory((int64_t *)v21, (int32_t)*(int64_t *)(v8 + 8), v3);
        function_4630();
        goto lab_0x94d4;
    }
}