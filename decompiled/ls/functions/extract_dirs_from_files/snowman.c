void extract_dirs_from_files(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13d7;
    void** r12_8;
    int1_t zf9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rax13;
    void** rbx14;
    void** rax15;
    void** rbp16;
    void** r14_17;
    struct s25* rax18;
    uint32_t eax19;
    int1_t cf20;
    void** rax21;
    void** rdi22;
    void** rdi23;
    void** rdi24;
    int1_t cf25;
    void** rdx26;
    void** rsi27;
    void** rdi28;
    void** rax29;
    void** rdx30;
    int1_t zf31;
    void* rcx32;

    r13d7 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rsi)));
    r12_8 = rdi;
    if (rdi && (zf9 = active_dir_set == 0, !zf9)) {
        rax10 = xmalloc(32, rsi);
        rax11 = xstrdup(r12_8, rsi);
        *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax10 + 8) = rax11;
        rax12 = pending_dirs;
        *reinterpret_cast<void***>(rax10 + 16) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax10 + 24) = rax12;
        pending_dirs = rax10;
    }
    rax13 = cwd_n_used;
    rbx14 = rax13 + 0xffffffffffffffff;
    if (rax13) {
        while (1) {
            rax15 = sorted_file;
            rbp16 = *reinterpret_cast<void***>(rax15 + reinterpret_cast<unsigned char>(rbx14) * 8);
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) == 3) 
                goto addr_9485_5;
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) != 9) 
                goto addr_9460_7;
            addr_9485_5:
            r14_17 = *reinterpret_cast<void***>(rbp16);
            if (!r12_8) 
                goto addr_9560_8;
            rax18 = last_component(r14_17, rsi, rdx);
            if (rax18->f0 != 46) 
                goto addr_94a3_10;
            rdx = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(rax18->f1 == 46);
            eax19 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax18) + reinterpret_cast<unsigned char>(rdx) + 1);
            if (!*reinterpret_cast<signed char*>(&eax19) || *reinterpret_cast<signed char*>(&eax19) == 47) {
                addr_9460_7:
                cf20 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(1);
                --rbx14;
                if (cf20) 
                    break; else 
                    continue;
            }
            addr_94a3_10:
            if (*reinterpret_cast<void***>(r14_17) == 47) {
                addr_9560_8:
                rsi = *reinterpret_cast<void***>(rbp16 + 8);
                rdx = r13d7;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                queue_directory(r14_17, rsi, rdx, rcx, r8);
            } else {
                rax21 = file_name_concat(r12_8, r14_17);
                rsi = *reinterpret_cast<void***>(rbp16 + 8);
                rdx = r13d7;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                queue_directory(rax21, rsi, rdx, rcx, r8);
                fun_4630(rax21, rax21);
            }
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) == 9) {
                rdi22 = *reinterpret_cast<void***>(rbp16);
                fun_4630(rdi22, rdi22);
                rdi23 = *reinterpret_cast<void***>(rbp16 + 8);
                fun_4630(rdi23, rdi23);
                rdi24 = *reinterpret_cast<void***>(rbp16 + 16);
                fun_4630(rdi24, rdi24);
                cf25 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(1);
                --rbx14;
                if (cf25) 
                    goto addr_9502_16;
            }
        }
    } else {
        goto addr_95a0_18;
    }
    addr_9508_19:
    rdx26 = cwd_n_used;
    if (rdx26) {
        rsi27 = sorted_file;
        rdi28 = rsi27 + reinterpret_cast<unsigned char>(rdx26) * 8;
        rax29 = rsi27;
        *reinterpret_cast<int32_t*>(&rdx30) = 0;
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        do {
            zf31 = *reinterpret_cast<uint32_t*>(*reinterpret_cast<void***>(rax29) + 0xa8) == 9;
            *reinterpret_cast<void***>(rsi27 + reinterpret_cast<unsigned char>(rdx30) * 8) = *reinterpret_cast<void***>(rax29);
            rax29 = rax29 + 8;
            *reinterpret_cast<uint32_t*>(&rcx32) = reinterpret_cast<uint1_t>(!zf31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx32) + 4) = 0;
            rdx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx30) + reinterpret_cast<uint64_t>(rcx32));
        } while (rdi28 != rax29);
        cwd_n_used = rdx30;
        return;
    }
    addr_95a0_18:
    cwd_n_used = reinterpret_cast<void**>(0);
    return;
    addr_9502_16:
    goto addr_9508_19;
}