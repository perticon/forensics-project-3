void abformat_init(void) {
    char abmon[12][128]; // bp-1616, 0x6cd0
    char * pb[2]; // bp-1632, 0x6cd0
    char * v1[2]; // 0x6d93
    // 0x6cd0
    __readfsqword(40);
    int64_t v2; // bp-1624, 0x6cd0
    for (int64_t i = 0; i < 9; i += 8) {
        int64_t v3 = *(int64_t *)(i + (int64_t)&long_time_format); // 0x6d07
        char v4 = *(char *)v3; // 0x6d0b
        int64_t v5 = v3; // 0x6d10
        char v6 = v4; // 0x6d10
        int64_t v7 = 0; // 0x6d10
        if (v4 != 0) {
            int64_t v8 = v5;
            int64_t v9 = v8 + 1;
            char v10 = *(char *)v9; // 0x6d18
            int64_t v11 = v9; // 0x6d1f
            char v12 = v10; // 0x6d1f
            int64_t v13; // 0x6e10
            if (v6 == 37) {
                if (v10 == 37) {
                    // 0x6e10
                    v13 = v8 + 2;
                    v11 = v13;
                    v12 = *(char *)v13;
                } else {
                    // 0x6d39
                    v11 = v9;
                    v12 = v10;
                    if (v10 == 98) {
                        // break -> 0x6d3e
                        break;
                    }
                }
            }
            // 0x6d23
            v6 = v12;
            v7 = 0;
            while (v12 != 0) {
                // 0x6d18
                v8 = v11;
                v9 = v8 + 1;
                v10 = *(char *)v9;
                v11 = v9;
                v12 = v10;
                if (v6 == 37) {
                    if (v10 == 37) {
                        // 0x6e10
                        v13 = v8 + 2;
                        v11 = v13;
                        v12 = *(char *)v13;
                    } else {
                        // 0x6d39
                        v11 = v9;
                        v12 = v10;
                        v7 = v8;
                        if (v10 == 98) {
                            // break -> 0x6d3e
                            break;
                        }
                    }
                }
                // 0x6d23
                v6 = v12;
                v7 = 0;
            }
        }
        // 0x6d3e
        *(int64_t *)(i + (int64_t)&v2) = v7;
    }
    // 0x6d4c
    int64_t v14; // bp-1672, 0x6cd0
    int64_t v15 = &v14; // 0x6cdc
    if (v2 == 0) {
        // 0x6e4d
        if (*(int64_t *)&abmon != 0) {
            goto lab_0x6d5e;
        } else {
            goto lab_0x6e24_2;
        }
    } else {
        goto lab_0x6d5e;
    }
  lab_0x6e24_2:
    // 0x6e24
    if (*(int64_t *)(v15 + (int64_t)&g88) != __readfsqword(40)) {
        // 0x6f3a
        function_4870();
        return;
    }
  lab_0x6d5e:;
    // 0x6d5e
    int64_t v16; // bp-72, 0x6cd0
    v14 = &v16;
    int64_t v17 = 12; // 0x6d75
    int64_t v18; // 0x6cd0
    int64_t * v19; // 0x6e6d
    while (true) {
        uint64_t v20 = v17;
        v1[0] = (char *)v20;
        int64_t v21; // bp-1608, 0x6cd0
        int64_t v22 = &v21; // 0x6d86
        pb = v1;
        int64_t v23 = function_4ab0(); // 0x6d98
        if (function_48a0() != 0) {
            // break (via goto) -> 0x6e24
            goto lab_0x6e24_2;
        }
        int64_t v24 = function_4c70(); // 0x6db2
        char * v25 = (char *)v23; // 0x6db7
        uint16_t v26 = *(int16_t *)(2 * (int64_t)*v25 + *(int64_t *)v24); // 0x6dcb
        uint64_t v27 = mbsalign(v25, (char *)v22, 128, (int64_t *)&pb, (int32_t)(v26 / 2048 % 2), 0); // 0x6dde
        if (v27 >= 128) {
            // break (via goto) -> 0x6e24
            goto lab_0x6e24_2;
        }
        uint64_t v28 = 0;
        uint64_t v29 = *(int64_t *)&pb; // 0x6de9
        int64_t v30 = v28 < v29 ? v29 : v28; // 0x6df1
        v22 += 128;
        int64_t v31 = v30; // 0x6e01
        while (v22 != v14) {
            // 0x6d90
            pb = v1;
            v23 = function_4ab0();
            if (function_48a0() != 0) {
                // break (via goto) -> 0x6e24
                goto lab_0x6e24_2;
            }
            // 0x6db2
            v24 = function_4c70();
            v25 = (char *)v23;
            v26 = *(int16_t *)(2 * (int64_t)*v25 + *(int64_t *)v24);
            v27 = mbsalign(v25, (char *)v22, 128, (int64_t *)&pb, (int32_t)(v26 / 2048 % 2), 0);
            if (v27 >= 128) {
                // break (via goto) -> 0x6e24
                goto lab_0x6e24_2;
            }
            // 0x6de9
            v28 = v31;
            v29 = *(int64_t *)&pb;
            v30 = v28 < v29 ? v29 : v28;
            v22 += 128;
            v31 = v30;
        }
        // 0x6e03
        v17 = v30;
        if (v20 <= v30) {
            // 0x6e6d
            v19 = (int64_t *)(v15 + 16);
            v18 = 8 * *v19;
            goto lab_0x6e6d_2;
        }
    }
    goto lab_0x6e24_2;
  lab_0x6e6d_2:;
    int64_t v32 = *(int64_t *)(v18 + (int64_t)&long_time_format); // 0x6e85
    int64_t v33 = *(int64_t *)(*(int64_t *)(v15 + 24) + v18); // 0x6e8e
    int64_t v34 = v33 + 2;
    int64_t v35 = *(int64_t *)(v15 + 8); // 0x6e9d
    goto lab_0x6efa;
  lab_0x6efa:;
    int64_t v36 = v35;
    int64_t v37; // 0x6cd0
    if (v33 != 0) {
        if (v33 - v32 > 128) {
            goto lab_0x6e24_2;
        } else {
            // 0x6eb3
            *(int64_t *)(v15 - 16) = v34;
            *(int64_t *)(v15 - 24) = v36;
            *(int64_t *)(v15 - 32) = v32;
            v37 = function_46c0();
            goto lab_0x6ee4;
        }
    } else {
        // 0x6eff
        v37 = function_48c0();
        goto lab_0x6ee4;
    }
  lab_0x6ee4:
    // 0x6ee4
    if ((int32_t)v37 < 128) {
        int64_t v38 = v36 + 128; // 0x6eed
        v35 = v38;
        if (v38 == v14) {
            // 0x6f20
            if (*v19 == 1) {
                // 0x6e1d
                *(char *)&use_abformat = 1;
                goto lab_0x6e24_2;
            } else {
                // 0x6f2c
                *v19 = 1;
                v18 = 8;
                goto lab_0x6e6d_2;
            }
        } else {
            goto lab_0x6efa;
        }
    } else {
        goto lab_0x6e24_2;
    }
}