bool get_funky_string(char ** dest, char ** src, bool equals_end, int64_t * output_count) {
    int64_t v1 = (int64_t)src;
    int64_t v2 = (int64_t)dest;
    int64_t v3 = equals_end ? 0xffffffff : 0; // 0x6912
    int64_t v4; // 0x6910
    char v5 = v4;
    int64_t v6 = v2 + 1; // 0x6936
    int64_t v7 = v6; // 0x6948
    int64_t v8 = v1; // 0x6948
    char v9 = v5; // 0x6948
    int64_t v10 = 1; // 0x6948
    int64_t v11 = 0; // 0x6948
    int64_t v12 = v2; // 0x6948
    int64_t v13 = v6; // 0x6948
    int64_t v14 = v1; // 0x6948
    int64_t v15 = 1; // 0x6948
    int64_t v16 = 0; // 0x6948
    int64_t v17 = v2; // 0x6948
    if (v5 == 92) {
        goto lab_0x6976;
    } else {
        goto lab_0x694a;
    }
  lab_0x6976:;
    char v18 = *(char *)(v14 + 1); // 0x6976
    int64_t v19 = v14 + 2; // 0x697a
    int64_t v20 = 0; // 0x6980
    int64_t v21 = v19; // 0x6980
    int64_t v22 = v16; // 0x6980
    int64_t v23 = v17; // 0x6980
    int64_t v24; // 0x6910
    int64_t v25; // 0x6910
    int64_t v26; // 0x6910
    if (v18 == 0) {
        goto lab_0x69ae;
    } else {
        if (v18 < 121) {
            // 0x6990
            return v3 % 2 != 0;
        }
        // 0x69f0
        *(char *)(v13 - 1) = v18;
        v26 = v13;
        v24 = v19;
        v25 = v15;
        goto lab_0x6966;
    }
  lab_0x694a:;
    int64_t v27 = v12;
    int64_t v28 = v11;
    int64_t v29 = v10;
    char v30 = v9;
    int64_t v31 = v8;
    int64_t v32 = v7;
    char v33; // 0x6910
    char v34; // 0x6910
    if (v33 > v34) {
        if (v30 != 94) {
            goto lab_0x6957;
        } else {
            int64_t v35 = v31 + 1; // 0x69c5
            unsigned char v36 = *(char *)v35; // 0x69c5
            if (v36 < 127) {
                // 0x69d3
                *(char *)(v32 - 1) = v36 % 32;
                v26 = v32;
                v24 = v31 + 2;
                v25 = v29;
                goto lab_0x6966;
            } else {
                // 0x6a08
                v20 = 0;
                v21 = v35;
                v22 = v28;
                v23 = v27;
                if (v36 == 63) {
                    // 0x6a28
                    *(char *)(v32 - 1) = 127;
                    v26 = v32;
                    v24 = v35;
                    v25 = v29;
                    goto lab_0x6966;
                } else {
                    goto lab_0x69ae;
                }
            }
        }
    } else {
        if (v30 == 61) {
            // 0x6a18
            v20 = v3;
            v21 = v31;
            v22 = v28;
            v23 = v27;
            if ((char)v3 != 0) {
                goto lab_0x69ae;
            } else {
                goto lab_0x6957;
            }
        } else {
            if (v30 > 61) {
                goto lab_0x6957;
            } else {
                // 0x69a0
                v20 = 1;
                v21 = v31;
                v22 = v28;
                v23 = v27;
                if (v30 != 0 == (v30 != 58)) {
                    goto lab_0x6957;
                } else {
                    goto lab_0x69ae;
                }
            }
        }
    }
  lab_0x69ae:
    // 0x69ae
    *(int64_t *)dest = v23;
    *(int64_t *)src = v21;
    *output_count = v22;
    return v20 % 2 != 0;
  lab_0x6966:;
    int64_t v37 = v26 + 1;
    int64_t v38 = v25 + 1;
    char v39 = *(char *)v24; // 0x6966
    v7 = v37;
    v33 = v39;
    v34 = 92;
    v8 = v24;
    v9 = v39;
    v10 = v38;
    v11 = v25;
    v12 = v26;
    v13 = v37;
    v14 = v24;
    v15 = v38;
    v16 = v25;
    v17 = v26;
    if (v39 != 92) {
        goto lab_0x694a;
    } else {
        goto lab_0x6976;
    }
  lab_0x6957:
    // 0x6957
    *(char *)(v32 - 1) = v30;
    v26 = v32;
    v24 = v31 + 1;
    v25 = v29;
    goto lab_0x6966;
}