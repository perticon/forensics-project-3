undefined4 get_funky_string(long *param_1,char **param_2,undefined4 param_3,long *param_4)

{
  byte bVar1;
  char *pcVar2;
  char *pcVar3;
  char cVar4;
  char cVar5;
  long lVar6;
  long lVar7;
  long lVar8;
  long lVar9;
  long lVar10;
  char *pcVar11;
  bool bVar12;
  
  pcVar2 = *param_2;
  lVar10 = *param_1;
  lVar8 = 1;
  cVar4 = *pcVar2;
  lVar9 = 0;
  lVar7 = lVar10 + 1;
  cVar5 = cVar4 + -0x5c;
  bVar12 = cVar4 == '\\';
  pcVar3 = pcVar2;
  lVar6 = lVar7;
  if (bVar12) goto LAB_00106976;
LAB_0010694a:
  if (bVar12 || SBORROW1(cVar4,'\\') != cVar5 < '\0') {
    if (cVar4 == '=') {
      if ((char)param_3 != '\0') goto LAB_001069ae;
    }
    else if ((cVar4 < '>') && ((cVar4 == '\0' || (cVar4 == ':')))) {
      param_3 = 1;
LAB_001069ae:
      *param_1 = lVar10;
      *param_2 = pcVar2;
      *param_4 = lVar9;
      return param_3;
    }
  }
  else if (cVar4 == '^') {
    bVar1 = pcVar2[1];
    if ((byte)(bVar1 - 0x40) < 0x3f) {
      pcVar2 = pcVar2 + 2;
      lVar8 = lVar8 + 1;
      lVar7 = lVar6 + 1;
      *(byte *)(lVar6 + -1) = bVar1 & 0x1f;
      goto LAB_00106966;
    }
    pcVar2 = pcVar2 + 1;
    if (bVar1 == 0x3f) {
      lVar8 = lVar8 + 1;
      lVar7 = lVar6 + 1;
      *(undefined *)(lVar6 + -1) = 0x7f;
      goto LAB_00106966;
    }
    param_3 = 0;
    goto LAB_001069ae;
  }
  pcVar2 = pcVar2 + 1;
  lVar7 = lVar6;
LAB_0010695b:
  *(char *)(lVar7 + -1) = cVar4;
  lVar8 = lVar8 + 1;
  lVar7 = lVar7 + 1;
LAB_00106966:
  do {
    cVar4 = *pcVar2;
    lVar10 = lVar7 + -1;
    lVar9 = lVar8 + -1;
    cVar5 = cVar4 + -0x5c;
    bVar12 = cVar5 == '\0';
    pcVar3 = pcVar2;
    lVar6 = lVar7;
    if (!bVar12) goto LAB_0010694a;
LAB_00106976:
    cVar5 = pcVar3[1];
    pcVar2 = pcVar3 + 2;
    if (cVar5 == '\0') {
      param_3 = 0;
      goto LAB_001069ae;
    }
    cVar4 = cVar5 + -0x30;
    switch(cVar4) {
    case '\0':
    case '\x01':
    case '\x02':
    case '\x03':
    case '\x04':
    case '\x05':
    case '\x06':
    case '\a':
      cVar5 = *pcVar2;
      if (7 < (byte)(cVar5 - 0x30U)) goto LAB_0010695b;
      do {
        pcVar2 = pcVar2 + 1;
        cVar4 = cVar5 + -0x30 + cVar4 * '\b';
        cVar5 = *pcVar2;
      } while ((byte)(cVar5 - 0x30U) < 8);
      *(char *)(lVar7 + -1) = cVar4;
      lVar8 = lVar8 + 1;
      lVar7 = lVar7 + 1;
      goto LAB_00106966;
    case '\x0f':
      cVar5 = '\x7f';
      break;
    case '(':
    case 'H':
      goto switchD_0010699b_caseD_28;
    case '/':
      cVar5 = ' ';
      break;
    case '1':
      cVar5 = '\a';
      break;
    case '2':
      cVar5 = '\b';
      break;
    case '5':
      cVar5 = '\x1b';
      break;
    case '6':
      cVar5 = '\f';
      break;
    case '>':
      cVar5 = '\n';
      break;
    case 'B':
      cVar5 = '\r';
      break;
    case 'D':
      cVar5 = '\t';
      break;
    case 'F':
      cVar5 = '\v';
    }
    *(char *)(lVar7 + -1) = cVar5;
    lVar8 = lVar8 + 1;
    lVar7 = lVar7 + 1;
  } while( true );
switchD_0010699b_caseD_28:
  cVar5 = pcVar3[2];
  pcVar11 = pcVar3 + 3;
  cVar4 = '\0';
  pcVar2 = pcVar3 + 2;
  if ('F' < cVar5) goto LAB_00106ad2;
  do {
    if (cVar5 < 'A') {
      if (9 < (byte)(cVar5 - 0x30U)) goto LAB_0010695b;
      cVar4 = cVar5 + -0x30 + cVar4 * '\x10';
      pcVar2 = pcVar11;
    }
    else {
      cVar4 = cVar5 + -0x37 + cVar4 * '\x10';
      pcVar2 = pcVar11;
    }
    while( true ) {
      cVar5 = *pcVar2;
      pcVar11 = pcVar2 + 1;
      if (cVar5 < 'G') break;
LAB_00106ad2:
      if (5 < (byte)(cVar5 + 0x9fU)) goto LAB_0010695b;
      cVar4 = cVar5 + -0x57 + cVar4 * '\x10';
      pcVar2 = pcVar11;
    }
  } while( true );
}