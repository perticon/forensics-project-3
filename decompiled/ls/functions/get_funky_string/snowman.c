signed char get_funky_string(void** rdi, void** rsi, void** edx, void** rcx, void** r8) {
    void** eax6;
    void** r11_7;
    void** r10_8;
    void** r12d9;
    void** rbp10;
    void** rcx11;
    void* rdi12;
    uint32_t edx13;
    void** r8_14;
    void** rsi15;
    void** r9_16;
    int1_t less_or_equal17;
    int64_t rdx18;
    void** r13_19;
    int32_t r9d20;
    int64_t rdx21;
    int32_t r13d22;
    uint32_t edx23;
    int64_t r8_24;

    eax6 = edx;
    r11_7 = rdi;
    r10_8 = rsi;
    r12d9 = edx;
    rbp10 = rcx;
    rcx11 = *reinterpret_cast<void***>(rsi);
    *reinterpret_cast<int32_t*>(&rdi12) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11));
    r8_14 = reinterpret_cast<void**>(0);
    rsi15 = *reinterpret_cast<void***>(rdi) + 1;
    r9_16 = rsi15 + 0xffffffffffffffff;
    less_or_equal17 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) <= reinterpret_cast<signed char>(92);
    if (*reinterpret_cast<void***>(&edx13) != 92) 
        goto addr_694a_2;
    while (*reinterpret_cast<uint32_t*>(&rdx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11 + 1)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx18) + 4) = 0, r13_19 = rcx11 + 2, !!*reinterpret_cast<void***>(&rdx18)) {
        r9d20 = static_cast<int32_t>(rdx18 - 48);
        if (*reinterpret_cast<unsigned char*>(&r9d20) <= 72) 
            goto addr_6990_5;
        rcx11 = r13_19;
        *reinterpret_cast<void***>(rsi15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&rdx18);
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
        ++rsi15;
        while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11)), r9_16 = rsi15 + 0xffffffffffffffff, r8_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdi12) + 0xffffffffffffffff), less_or_equal17 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) <= reinterpret_cast<signed char>(92), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 92)) {
            addr_694a_2:
            if (!less_or_equal17) {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 94)) {
                    *reinterpret_cast<uint32_t*>(&rdx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11 + 1));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx21) + 4) = 0;
                    r13d22 = static_cast<int32_t>(rdx21 - 64);
                    if (*reinterpret_cast<unsigned char*>(&r13d22) > 62) {
                        ++rcx11;
                        if (*reinterpret_cast<signed char*>(&rdx21) != 63) 
                            goto addr_6a11_12;
                        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
                        ++rsi15;
                        *reinterpret_cast<signed char*>(rsi15 + 0xfffffffffffffffe) = 0x7f;
                        continue;
                    } else {
                        edx23 = *reinterpret_cast<uint32_t*>(&rdx21) & 31;
                        rcx11 = rcx11 + 2;
                        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
                        ++rsi15;
                        *reinterpret_cast<signed char*>(rsi15 + 0xfffffffffffffffe) = *reinterpret_cast<signed char*>(&edx23);
                        continue;
                    }
                }
            } else {
                if (*reinterpret_cast<void***>(&edx13) == 61) {
                    if (*reinterpret_cast<signed char*>(&r12d9)) 
                        goto addr_69ae_17;
                } else {
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) > reinterpret_cast<signed char>(61)) 
                        goto addr_6957_20;
                    if (!*reinterpret_cast<void***>(&edx13)) 
                        goto addr_69a9_22;
                    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 58)) 
                        goto addr_69a9_22;
                }
            }
            addr_6957_20:
            ++rcx11;
            *reinterpret_cast<void***>(rsi15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&edx13);
            rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
            ++rsi15;
        }
    }
    rcx11 = r13_19;
    eax6 = reinterpret_cast<void**>(0);
    addr_69ae_17:
    *reinterpret_cast<void***>(r11_7) = r9_16;
    *reinterpret_cast<void***>(r10_8) = rcx11;
    *reinterpret_cast<void***>(rbp10) = r8_14;
    return *reinterpret_cast<signed char*>(&eax6);
    addr_6990_5:
    *reinterpret_cast<uint32_t*>(&r8_24) = *reinterpret_cast<unsigned char*>(&r9d20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_24) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1a020 + r8_24 * 4) + 0x1a020;
    addr_6a11_12:
    eax6 = reinterpret_cast<void**>(0);
    goto addr_69ae_17;
    addr_69a9_22:
    eax6 = reinterpret_cast<void**>(1);
    goto addr_69ae_17;
}