bool is_colored(uint32_t type) {
    int64_t v1 = 16 * (int64_t)type; // 0x6c3c
    int64_t v2 = *(int64_t *)(v1 + (int64_t)&color_indicator); // 0x6c43
    if (v2 == 0) {
        // 0x6c61
        return false;
    }
    int64_t v3 = *(int64_t *)(v1 + (int64_t)&color_indicator + 8); // 0x6c4b
    switch (v2) {
        case 1: {
            // 0x6c68
            return *(char *)v3 != 48;
        }
        case 2: {
            int32_t v4 = (int32_t)*(char *)v3 - 48; // 0x6c7b
            int32_t v5 = v4; // 0x6c7e
            if (v4 == 0) {
                // 0x6c80
                v5 = (int32_t)*(char *)(v3 + 1) - 48;
            }
            // 0x6c87
            return v5 != 0;
        }
    }
    // 0x6c61
    return true;
}