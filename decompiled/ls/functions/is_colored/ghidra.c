bool is_colored(uint param_1)

{
  long lVar1;
  byte *pbVar2;
  int iVar3;
  bool bVar4;
  
  bVar4 = false;
  lVar1 = *(long *)(color_indicator + (ulong)param_1 * 0x10);
  if (lVar1 != 0) {
    pbVar2 = *(byte **)(color_indicator + (ulong)param_1 * 0x10 + 8);
    if (lVar1 == 1) {
      return *pbVar2 != 0x30;
    }
    bVar4 = true;
    if (lVar1 == 2) {
      iVar3 = *pbVar2 - 0x30;
      if (iVar3 == 0) {
        iVar3 = pbVar2[1] - 0x30;
      }
      return iVar3 != 0;
    }
  }
  return bVar4;
}