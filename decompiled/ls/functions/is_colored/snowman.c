unsigned char is_colored(uint32_t edi, ...) {
    int64_t rdi2;
    int32_t r8d3;
    struct s2* rax4;
    int32_t eax5;
    uint32_t eax6;
    int32_t eax7;
    int32_t eax8;

    *reinterpret_cast<uint32_t*>(&rdi2) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi2) + 4) = 0;
    r8d3 = 0;
    rax4 = reinterpret_cast<struct s2*>(0x25060 + (rdi2 << 4));
    if (rax4->f0) {
        if (rax4->f0 == 1) {
            *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(rax4->f8->f0 != 48);
            eax5 = r8d3;
            return *reinterpret_cast<unsigned char*>(&eax5);
        } else {
            r8d3 = 1;
            if (rax4->f0 == 2) {
                eax6 = rax4->f8->f0 - 48;
                if (!eax6) {
                    eax6 = rax4->f8->f1 - 48;
                }
                *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!eax6);
                eax7 = r8d3;
                return *reinterpret_cast<unsigned char*>(&eax7);
            }
        }
    }
    eax8 = r8d3;
    return *reinterpret_cast<unsigned char*>(&eax8);
}