void cmp_extension(char **param_1,char **param_2,code *UNRECOVERED_JUMPTABLE)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  
  pcVar2 = strrchr(*param_1,0x2e);
  pcVar3 = strrchr(*param_2,0x2e);
  if (pcVar3 == (char *)0x0) {
    pcVar3 = "";
  }
  if (pcVar2 == (char *)0x0) {
    pcVar2 = "";
  }
  iVar1 = (*UNRECOVERED_JUMPTABLE)(pcVar2,pcVar3);
  if (iVar1 == 0) {
                    /* WARNING: Could not recover jumptable at 0x001071e7. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    (*UNRECOVERED_JUMPTABLE)(*param_1,*param_2);
    return;
  }
  return;
}