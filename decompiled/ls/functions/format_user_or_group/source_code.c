format_user_or_group (char const *name, uintmax_t id, int width)
{
  if (name)
    {
      int width_gap = width - mbswidth (name, 0);
      int pad = MAX (0, width_gap);
      dired_outstring (name);

      do
        dired_outbyte (' ');
      while (pad--);
    }
  else
    dired_pos += printf ("%*"PRIuMAX" ", width, id);
}