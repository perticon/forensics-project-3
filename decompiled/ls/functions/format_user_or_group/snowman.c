void** format_user_or_group(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    void** rax8;
    void** tmp64_9;
    uint32_t eax10;
    uint32_t ebx11;
    void** rax12;
    void** rdi13;
    void** tmp64_14;
    void** rax15;
    int1_t cf16;

    if (!rdi) {
        eax7 = fun_4b20(1, "%*lu ", rdx, rsi, r8);
        rax8 = reinterpret_cast<void**>(static_cast<int64_t>(eax7));
        tmp64_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax8));
        dired_pos = tmp64_9;
        return rax8;
    } else {
        eax10 = gnu_mbswidth(rdi);
        ebx11 = *reinterpret_cast<int32_t*>(&rdx) - eax10;
        if (reinterpret_cast<int32_t>(ebx11) < reinterpret_cast<int32_t>(0)) {
            ebx11 = 0;
        }
        rax12 = fun_4860(rdi);
        dired_outbuf(rdi, rax12);
        do {
            rdi13 = stdout;
            tmp64_14 = dired_pos + 1;
            dired_pos = tmp64_14;
            rax15 = *reinterpret_cast<void***>(rdi13 + 40);
            if (reinterpret_cast<unsigned char>(rax15) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 48))) {
                rax15 = fun_48d0();
            } else {
                *reinterpret_cast<void***>(rdi13 + 40) = rax15 + 1;
                *reinterpret_cast<void***>(rax15) = reinterpret_cast<void**>(32);
            }
            cf16 = ebx11 < 1;
            --ebx11;
        } while (!cf16);
        return rax15;
    }
}