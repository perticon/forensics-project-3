uint64_t format_user_or_group (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    ebx = edx;
    if (rdi == 0) {
        goto label_1;
    }
    eax = gnu_mbswidth (rdi, 0);
    ebx -= eax;
    eax = 0;
    __asm ("cmovs ebx, eax");
    rax = strlen (rbp);
    rsi = rax;
    rax = dired_outbuf (rbp);
    do {
        rdi = stdout;
        *(obj.dired_pos)++;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_2;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0x20;
label_0:
        ebx--;
    } while (ebx >= 0);
    return rax;
label_2:
    esi = 0x20;
    eax = overflow ();
    goto label_0;
label_1:
    rcx = rsi;
    edi = 1;
    eax = 0;
    rsi = "%*lu ";
    rax = printf_chk ();
    rax = (int64_t) eax;
    *(obj.dired_pos) += rax;
    return rax;
}