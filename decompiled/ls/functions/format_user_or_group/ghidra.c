void format_user_or_group(char *param_1,undefined8 param_2,undefined8 param_3)

{
  char *pcVar1;
  int iVar2;
  size_t sVar3;
  bool bVar4;
  
  if (param_1 != (char *)0x0) {
    iVar2 = gnu_mbswidth(param_1,0);
    iVar2 = (int)param_3 - iVar2;
    if (iVar2 < 0) {
      iVar2 = 0;
    }
    sVar3 = strlen(param_1);
    dired_outbuf(param_1,sVar3);
    do {
      dired_pos = dired_pos + 1;
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = ' ';
      }
      else {
        __overflow(stdout,0x20);
      }
      bVar4 = iVar2 != 0;
      iVar2 = iVar2 + -1;
    } while (bVar4);
    return;
  }
  iVar2 = __printf_chk(1,"%*lu ",param_3,param_2);
  dired_pos = dired_pos + iVar2;
  return;
}