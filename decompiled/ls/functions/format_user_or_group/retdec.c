int64_t format_user_or_group(int64_t a1, int64_t a2, int32_t a3) {
    if (a1 == 0) {
        int64_t result = 0x100000000 * function_4b20() >> 32; // 0x9236
        dired_pos += result;
        return result;
    }
    char * v1 = (char *)a1; // 0x91b2
    int32_t v2 = a3 - gnu_mbswidth(v1, 0); // 0x91ba
    dired_outbuf(v1, function_4860());
    int64_t v3 = v2 > 0 ? v2 : 0; // 0x91d4
    int64_t v4 = (int64_t)g59; // 0x91d8
    dired_pos++;
    int64_t * v5 = (int64_t *)(v4 + 40); // 0x91e7
    uint64_t v6 = *v5; // 0x91e7
    int64_t result2; // 0x91a0
    if (v6 >= *(int64_t *)(v4 + 48)) {
        // 0x9210
        result2 = function_48d0();
    } else {
        // 0x91f1
        *v5 = v6 + 1;
        *(char *)v6 = 32;
        result2 = v6;
    }
    while (v3 != 0) {
        // 0x91fc
        v3 = v3 + 0xffffffff & 0xffffffff;
        v4 = (int64_t)g59;
        dired_pos++;
        v5 = (int64_t *)(v4 + 40);
        v6 = *v5;
        if (v6 >= *(int64_t *)(v4 + 48)) {
            // 0x9210
            result2 = function_48d0();
        } else {
            // 0x91f1
            *v5 = v6 + 1;
            *(char *)v6 = 32;
            result2 = v6;
        }
    }
    // 0x9201
    return result2;
}