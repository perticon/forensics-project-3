assert_matching_dev_ino (char const *name, struct dev_ino di)
{
  struct stat sb;
  assert (name);
  assert (0 <= stat (name, &sb));
  assert (sb.st_dev == di.st_dev);
  assert (sb.st_ino == di.st_ino);
}