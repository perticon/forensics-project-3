uint calc_req_mask(void)

{
  uint uVar1;
  uint uVar2;
  
  uVar1 = (-(uint)(print_inode == '\0') & 0xffffff00) + 0x102;
  if (print_block_size != '\0') {
    uVar1 = uVar1 | 0x400;
  }
  if (format == 0) {
    if (time_type < 4) {
      uVar2 = *(uint *)(CSWTCH_875 + (ulong)time_type * 4);
      if ((print_owner == '\0') && (print_author == '\0')) {
        uVar1 = uVar1 | uVar2 | 0x204;
      }
      else {
        uVar1 = uVar1 | uVar2 | 0x20c;
      }
      if (print_group != '\0') {
        uVar1 = uVar1 | 0x10;
      }
      switch(sort_type) {
      case 0:
      case 1:
      case 2:
      case 4:
      case 6:
        goto switchD_00108741_caseD_0;
      case 3:
        goto switchD_00108741_caseD_3;
      case 5:
switchD_00108719_caseD_5:
        return uVar1 | uVar2;
      }
    }
  }
  else {
    switch(sort_type) {
    case 0:
    case 1:
    case 2:
    case 4:
    case 6:
switchD_00108741_caseD_0:
      return uVar1;
    case 3:
switchD_00108741_caseD_3:
      return uVar1 | 0x200;
    case 5:
      if (time_type < 4) {
        uVar2 = *(uint *)(CSWTCH_875 + (ulong)time_type * 4);
        goto switchD_00108719_caseD_5;
      }
    }
  }
                    /* WARNING: Subroutine does not return */
  abort();
}