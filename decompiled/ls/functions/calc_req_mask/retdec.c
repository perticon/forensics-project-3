int64_t calc_req_mask(int64_t a1, int64_t a2, int64_t a3) {
    int64_t v1 = *(char *)&print_inode == 0 ? 2 : 258; // 0x869b
    int64_t v2 = *(char *)&print_block_size == 0 ? v1 : v1 | 1026;
    int64_t result; // 0x8690
    if (format != 0) {
        // 0x8720
        result = v2;
        if (sort_type >= 7) {
            result = calc_req_mask_cold();
        }
        // 0x8705
        return result;
    }
    int64_t v3 = v2; // 0x86bf
    if (time_type >= 4) {
        v3 = calc_req_mask_cold();
    }
    int32_t v4 = *(int32_t *)(4 * (int64_t)time_type + (int64_t)&g4); // 0x86cc
    int64_t v5 = (*(char *)&print_author | *(char *)&print_owner) == 0 ? 516 : 524;
    int64_t v6 = v5 | (int64_t)(v4 | (int32_t)v3);
    result = *(char *)&print_group == 0 ? v6 : v6 | 16;
    if (sort_type >= 7) {
        result = calc_req_mask_cold();
    }
    // 0x8705
    return result;
}