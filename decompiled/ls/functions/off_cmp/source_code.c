off_cmp (off_t a, off_t b)
{
  return (a > b) - (a < b);
}