char * indent(ulong param_1,ulong param_2)

{
  ulong uVar1;
  uint uVar2;
  char *in_RAX;
  char *pcVar3;
  ulong uVar5;
  char *pcVar4;
  
  if (param_2 <= param_1) {
    return in_RAX;
  }
  do {
    while( true ) {
      uVar5 = tabsize;
      uVar1 = param_1 + 1;
      pcVar3 = stdout->_IO_write_ptr;
      if ((tabsize == 0) || (param_2 / tabsize <= uVar1 / tabsize)) break;
      if (pcVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar3 + 1;
        *pcVar3 = '\t';
      }
      else {
        __overflow(stdout,9);
        uVar5 = tabsize;
      }
      pcVar3 = (char *)(param_1 / uVar5);
      param_1 = (uVar5 + param_1) - param_1 % uVar5;
      if (param_2 <= param_1) {
        return pcVar3;
      }
    }
    if (pcVar3 < stdout->_IO_write_end) {
      pcVar4 = pcVar3 + 1;
      stdout->_IO_write_ptr = pcVar4;
      *pcVar3 = ' ';
    }
    else {
      uVar2 = __overflow(stdout,0x20);
      pcVar4 = (char *)(ulong)uVar2;
    }
    param_1 = uVar1;
  } while (uVar1 < param_2);
  return pcVar4;
}