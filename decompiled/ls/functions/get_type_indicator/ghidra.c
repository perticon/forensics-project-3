byte get_type_indicator(char param_1,uint param_2,int param_3)

{
  uint uVar1;
  bool bVar2;
  
  if (param_1 == '\0') {
    if (param_3 == 5) {
      return 0;
    }
    if ((param_3 == 3) || (param_3 == 9)) {
      return 0x2f;
    }
    bVar2 = param_3 == 6;
    if (indicator_style == 1) {
      return 0;
    }
  }
  else {
    uVar1 = param_2 & 0xf000;
    if (uVar1 == 0x8000) {
      if (indicator_style != 3) {
        return 0;
      }
      return -((param_2 & 0x49) != 0) & 0x2a;
    }
    if (uVar1 == 0x4000) {
      return 0x2f;
    }
    bVar2 = uVar1 == 0xa000;
    if (indicator_style == 1) {
      return 0;
    }
  }
  if (bVar2) {
    return 0x40;
  }
  if (param_1 == '\0') {
    if (param_3 == 1) {
      return 0x7c;
    }
    bVar2 = param_3 == 7;
  }
  else {
    if ((param_2 & 0xf000) == 0x1000) {
      return 0x7c;
    }
    bVar2 = (param_2 & 0xf000) == 0xc000;
  }
  return -bVar2 & 0x3d;
}