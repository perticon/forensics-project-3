int64_t get_type_indicator(char a1, int32_t a2, int32_t a3, int64_t a4) {
    int64_t v1 = a2;
    int64_t v2; // 0x7540
    if (a1 == 0) {
        // 0x7590
        switch (a3) {
            case 5: {
                // 0x7564
                return 0;
            }
            case 3: {
            }
            case 9: {
                // 0x7564
                return 47;
            }
        }
        // 0x75a1
        v2 = a3 == 6;
        if (indicator_style == 1) {
            // 0x7564
            return 0;
        }
    } else {
        int64_t v3 = v1 & 0xf000; // 0x7547
        if (v3 == 0x8000) {
            // 0x7555
            if (indicator_style == 3) {
                // 0x75e8
                return (v1 & 73) == 0 ? 0 : 42;
            }
            // 0x7564
            return 0;
        }
        // 0x7568
        if (v3 == 0x4000) {
            // 0x7564
            return 47;
        }
        // 0x7575
        v2 = v3 | (int64_t)(v3 == 0xa000);
        if (indicator_style == 1) {
            // 0x7564
            return 0;
        }
    }
    // 0x75b0
    if ((char)v2 != 0) {
        // 0x7564
        return 64;
    }
    if (a1 == 0) {
        // 0x7600
        if (a3 == 1) {
            // 0x7564
            return 124;
        }
        // 0x7610
        return a3 == 7 ? 61 : 0;
    }
    int64_t v4 = v1 & 0xf000; // 0x75be
    if (v4 == 0x1000) {
        // 0x7564
        return 124;
    }
    // 0x7610
    return v4 == 0xc000 ? 61 : 0;
}