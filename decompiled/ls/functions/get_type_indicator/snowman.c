void** get_type_indicator(signed char dil, void** esi, uint32_t edx, void** rcx, void** r8, void** r9) {
    uint32_t eax7;
    unsigned char cl8;
    int1_t zf9;
    uint32_t ecx10;
    int1_t zf11;
    int1_t zf12;
    uint32_t eax13;
    int32_t eax14;
    uint32_t esi15;
    uint32_t eax16;

    if (!dil) {
        eax7 = 0;
        if (edx == 5) {
            return *reinterpret_cast<void***>(&eax7);
        } else {
            if (edx == 3 || edx == 9) {
                return 47;
            } else {
                cl8 = reinterpret_cast<uint1_t>(edx == 6);
                zf9 = indicator_style == 1;
                if (zf9) {
                    return 0;
                }
            }
        }
    } else {
        ecx10 = reinterpret_cast<unsigned char>(esi) & 0xf000;
        if (ecx10 != 0x8000) {
            if (ecx10 == 0x4000) {
                return 47;
            } else {
                cl8 = reinterpret_cast<uint1_t>(ecx10 == 0xa000);
                zf11 = indicator_style == 1;
                if (zf11) {
                    return 0;
                }
            }
        } else {
            eax7 = 0;
            zf12 = indicator_style == 3;
            if (zf12) {
                *reinterpret_cast<void***>(&eax7) = reinterpret_cast<void**>(-static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(esi) & 73)))))))));
                eax13 = eax7 & 42;
                return *reinterpret_cast<void***>(&eax13);
            }
        }
    }
    eax7 = 64;
    if (!cl8) {
        if (!dil) {
            eax14 = 0x7c;
            if (edx == 1) {
                return 0x7c;
            } else {
                *reinterpret_cast<void***>(&eax14) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(edx == 7)));
            }
        } else {
            esi15 = reinterpret_cast<unsigned char>(esi) & 0xf000;
            eax14 = 0x7c;
            if (esi15 == 0x1000) {
                return 0x7c;
            } else {
                *reinterpret_cast<void***>(&eax14) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(esi15 == 0xc000)));
            }
        }
        eax16 = reinterpret_cast<uint32_t>(-eax14) & 61;
        return *reinterpret_cast<void***>(&eax16);
    }
}