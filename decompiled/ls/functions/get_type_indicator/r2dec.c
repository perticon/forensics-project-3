int32_t get_type_indicator (uint32_t arg2, uint32_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (dil == 0) {
        goto label_1;
    }
    ecx = esi;
    ecx &= 0xf000;
    if (ecx != 0x8000) {
        goto label_2;
    }
    eax = 0;
    if (*(obj.indicator_style) == 3) {
        goto label_3;
    }
    do {
label_0:
        return eax;
label_2:
        eax = 0x2f;
        if (ecx == sym._init) {
            goto label_4;
        }
        eax = 0;
        cl = (ecx == 0xa000) ? 1 : 0;
        if (*(obj.indicator_style) != 1) {
            goto label_5;
        }
        return eax;
label_1:
        eax = 0;
    } while (edx == 5);
    if (edx == 3) {
        goto label_6;
    }
    if (edx == 9) {
        goto label_6;
    }
    cl = (edx == 6) ? 1 : 0;
    if (*(obj.indicator_style) == 1) {
        goto label_7;
    }
label_5:
    eax = 0x40;
    if (cl != 0) {
        goto label_0;
    }
    if (dil != 0) {
        esi &= 0xf000;
        eax = 0x7c;
        if (esi == 0x1000) {
            goto label_8;
        }
        al = (esi == 0xc000) ? 1 : 0;
        goto label_9;
label_4:
        return eax;
label_3:
        esi &= 0x49;
        esi = -esi;
        al -= al;
        eax &= 0x2a;
        return eax;
label_6:
        eax = 0x2f;
        return eax;
    }
    eax = 0x7c;
    if (edx != 1) {
        al = (edx == 7) ? 1 : 0;
label_9:
        eax = -eax;
        eax &= 0x3d;
        return eax;
label_7:
        return eax;
    }
    return eax;
label_8:
    return eax;
}