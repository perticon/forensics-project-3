int64_t signal_setup(int32_t a1) {
    int64_t v1 = __readfsqword(40); // 0x7651
    int64_t v2 = (int64_t)&g5; // 0x7667
    if ((char)a1 != 0) {
        // 0x76e0
        function_4a10();
        int64_t v3 = (int64_t)&g5;
        function_4750();
        int64_t v4; // 0x7640
        if (v4 != 1) {
            // 0x772c
            function_4c50();
        }
        int64_t v5 = v3 + 4; // 0x773a
        while (v3 != (int64_t)&g6) {
            // 0x7718
            v3 = v5;
            function_4750();
            if (v4 != 1) {
                // 0x772c
                function_4c50();
            }
            // 0x7737
            v5 = v3 + 4;
        }
        int128_t v6 = __asm_movdqa(*(int128_t *)&caught_signals); // 0x773c
        int128_t v7 = __asm_movdqa(g71); // 0x7744
        int128_t v8 = __asm_movdqa(g72); // 0x775c
        int128_t v9 = __asm_movdqa(g73); // 0x7764
        int128_t v10 = __asm_movdqa(g74); // 0x7773
        int128_t v11 = __asm_movdqa(g75); // 0x777b
        int128_t v12; // 0x7640
        __asm_movups(v12, v6);
        int128_t v13 = __asm_movdqa(g76); // 0x7788
        int128_t v14 = __asm_movdqa(g77); // 0x7790
        __asm_movups(v12, v7);
        __asm_movups(v12, v8);
        __asm_movups(v12, v9);
        __asm_movups(v12, v10);
        __asm_movups(v12, v11);
        __asm_movups(v12, v13);
        __asm_movups(v12, v14);
        int64_t v15 = (int64_t)&g5;
        if ((int32_t)function_4be0() != 0) {
            // 0x77d4
            function_4750();
        }
        int64_t v16 = v15 + 4; // 0x77f5
        while (v15 != (int64_t)&g6) {
            // 0x77c6
            v15 = v16;
            if ((int32_t)function_4be0() != 0) {
                // 0x77d4
                function_4750();
            }
            // 0x77f2
            v16 = v15 + 4;
        }
    } else {
        while (true) {
            int64_t v17 = v2;
            if ((int32_t)function_4be0() == 0) {
                if (v17 == (int64_t)&g6) {
                    // break -> 0x76af
                    break;
                }
            } else {
                // 0x76a1
                function_49c0();
                if (v17 == (int64_t)&g6) {
                    // break -> 0x76af
                    break;
                }
            }
            // 0x768d
            v2 = v17 + 4;
        }
    }
    int64_t result = v1 - __readfsqword(40); // 0x76b7
    if (result != 0) {
        // 0x77fc
        return function_4870();
    }
    // 0x76c6
    return result;
}