void signal_setup(signed char dil, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rax7;
    void* v8;
    void** rbx9;
    void** r15d10;
    void** r14_11;
    void** rbp12;
    int64_t rdi13;
    void** v14;
    int64_t rsi15;
    void** ebp16;
    void** rsi17;
    int32_t eax18;
    void** rax19;
    int64_t rdi20;
    void** v21;
    void** ebp22;
    int32_t eax23;
    void* rax24;
    int1_t zf25;
    int32_t eax26;
    int1_t zf27;

    rax7 = g28;
    v8 = rax7;
    if (dil) {
        rbx9 = reinterpret_cast<void**>(0x1a664);
        r15d10 = reinterpret_cast<void**>(20);
        r14_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8);
        rbp12 = reinterpret_cast<void**>(0x1a664);
        fun_4a10(0x26220);
        while (1) {
            rdx = r14_11;
            *reinterpret_cast<void***>(&rdi13) = r15d10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
            fun_4750(rdi13, rdi13);
            if (v14 != 1) {
                *reinterpret_cast<void***>(&rsi15) = r15d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi15) + 4) = 0;
                fun_4c50(0x26220, rsi15, rdx);
            }
            if (reinterpret_cast<int1_t>(rbp12 == 0x1a690)) 
                break;
            r15d10 = *reinterpret_cast<void***>(rbp12);
            rbp12 = rbp12 + 4;
        }
        __asm__("movdqa xmm0, [rip+0x1eadc]");
        __asm__("movdqa xmm1, [rip+0x1eae4]");
        ebp16 = reinterpret_cast<void**>(20);
        __asm__("movdqa xmm2, [rip+0x1eadc]");
        __asm__("movdqa xmm3, [rip+0x1eae4]");
        __asm__("movdqa xmm4, [rip+0x1eae5]");
        __asm__("movdqa xmm5, [rip+0x1eaed]");
        __asm__("movups [rsp+0x8], xmm0");
        __asm__("movdqa xmm6, [rip+0x1eaf0]");
        __asm__("movdqa xmm7, [rip+0x1eaf8]");
        __asm__("movups [rsp+0x18], xmm1");
        __asm__("movups [rsp+0x28], xmm2");
        __asm__("movups [rsp+0x38], xmm3");
        __asm__("movups [rsp+0x48], xmm4");
        __asm__("movups [rsp+0x58], xmm5");
        __asm__("movups [rsp+0x68], xmm6");
        __asm__("movups [rsp+0x78], xmm7");
        while (1) {
            rsi17 = ebp16;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            eax18 = fun_4be0();
            if (eax18) {
                rax19 = reinterpret_cast<void**>(0x68f0);
                rsi17 = r14_11;
                *reinterpret_cast<void***>(&rdi20) = ebp16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                if (ebp16 == 20) {
                    rax19 = reinterpret_cast<void**>(0x74b0);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                v21 = rax19;
                fun_4750(rdi20, rdi20);
            }
            if (reinterpret_cast<int1_t>(rbx9 == 0x1a690)) 
                break;
            ebp16 = *reinterpret_cast<void***>(rbx9);
            rbx9 = rbx9 + 4;
        }
    } else {
        rbx9 = reinterpret_cast<void**>(0x1a664);
        ebp22 = reinterpret_cast<void**>(20);
        while (1) {
            rsi17 = ebp22;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            eax23 = fun_4be0();
            if (!eax23) {
                if (rbx9 == 0x1a690) 
                    break;
            } else {
                rsi17 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
                *reinterpret_cast<void***>(&rdi20) = ebp22;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                fun_49c0(rdi20);
                if (reinterpret_cast<int1_t>(rbx9 == 0x1a690)) 
                    break;
            }
            ebp22 = *reinterpret_cast<void***>(rbx9);
            rbx9 = rbx9 + 4;
        }
    }
    rax24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
    if (!rax24) {
        return;
    }
    fun_4870();
    zf25 = used_color == 0;
    if (!zf25) 
        goto addr_781d_24;
    used_color = 1;
    eax26 = fun_4a70(1, rsi17, rdx);
    if (eax26 >= 0) {
        signal_setup(1, rsi17, rdx, rcx, r8, r9);
    }
    zf27 = g25088 == 0;
    if (zf27) {
        put_indicator(0x25060, rsi17, rdx, rcx, r8, r9, rbx9, v21);
        put_indicator(0x25090, rsi17, rdx, rcx, r8, r9, rbx9, v21);
        put_indicator(0x25070, rsi17, rdx, rcx, r8, r9, rbx9, v21);
    } else {
        put_indicator(0x25080, rsi17, rdx, rcx, r8, r9, rbx9, v21);
    }
    addr_781d_24:
}