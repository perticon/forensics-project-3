int64_t signal_setup (void) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_28h;
    int64_t var_38h;
    int64_t var_48h;
    int64_t var_58h;
    int64_t var_68h;
    int64_t var_78h;
    int64_t var_88h;
    int64_t var_98h;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    if (dil != 0) {
        goto label_2;
    }
    rbx = 0x0001a664;
    r12 = obj_caught_signals;
    r13 = rbx + 0x2c;
    while (eax == 0) {
        if (rbx == r13) {
            goto label_1;
        }
label_0:
        ebp = *(rbx);
        rbx += 4;
        esi = ebp;
        rdi = r12;
        eax = sigismember ();
    }
    esi = 0;
    signal (ebp);
    if (rbx != r13) {
        goto label_0;
    }
label_1:
    rax = *((rsp + 0x98));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_3;
    }
    return rax;
label_2:
    r12 = obj_caught_signals;
    rbx = 0x0001a664;
    r15d = 0x14;
    r14 = rsp;
    rdi = r12;
    r13 = rbx + 0x2c;
    sigemptyset ();
    while (rbp != r13) {
        r15d = *(rbp);
        rbp += 4;
        sigaction (r15d, 0, r14);
        if (*(rsp) != 1) {
            esi = r15d;
            rdi = r12;
            sigaddset ();
        }
    }
    __asm ("movdqa xmm0, xmmword [obj.caught_signals]");
    __asm ("movdqa xmm1, xmmword [0x00026230]");
    *((rsp + 0x88)) = 0x10000000;
    __asm ("movdqa xmm2, xmmword [0x00026240]");
    __asm ("movdqa xmm3, xmmword [0x00026250]");
    r15 = sym_stophandler;
    __asm ("movdqa xmm4, xmmword [0x00026260]");
    __asm ("movdqa xmm5, xmmword [0x00026270]");
    __asm ("movups xmmword [rsp + 8], xmm0");
    __asm ("movdqa xmm6, xmmword [0x00026280]");
    __asm ("movdqa xmm7, xmmword [0x00026290]");
    __asm ("movups xmmword [rsp + 0x18], xmm1");
    __asm ("movups xmmword [rsp + 0x28], xmm2");
    __asm ("movups xmmword [rsp + 0x38], xmm3");
    __asm ("movups xmmword [rsp + 0x48], xmm4");
    __asm ("movups xmmword [rsp + 0x58], xmm5");
    __asm ("movups xmmword [rsp + 0x68], xmm6");
    __asm ("movups xmmword [rsp + 0x78], xmm7");
    while (rbx != r13) {
        ebp = *(rbx);
        rbx += 4;
        esi = ebp;
        rdi = r12;
        eax = sigismember ();
        if (eax != 0) {
            rax = dbg_sighandler;
            rsi = r14;
            edi = ebp;
            if (ebp == 0x14) {
                rax = r15;
            }
            *(rsp) = rax;
            sigaction (rdi, rsi, 0);
        }
    }
    goto label_1;
label_3:
    return stack_chk_fail ();
}