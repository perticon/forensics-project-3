void clear_files(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rbx4;
    void** r12_5;
    void** rbp6;
    void** rdi7;
    void** rdi8;
    void** rdi9;

    rax3 = cwd_n_used;
    if (rax3) {
        rbx4 = sorted_file;
        r12_5 = rbx4 + reinterpret_cast<unsigned char>(rax3) * 8;
        do {
            rbp6 = *reinterpret_cast<void***>(rbx4);
            rbx4 = rbx4 + 8;
            rdi7 = *reinterpret_cast<void***>(rbp6);
            fun_4630(rdi7);
            rdi8 = *reinterpret_cast<void***>(rbp6 + 8);
            fun_4630(rdi8);
            rdi9 = *reinterpret_cast<void***>(rbp6 + 16);
            fun_4630(rdi9);
        } while (r12_5 != rbx4);
    }
    cwd_some_quoted = 0;
    cwd_n_used = reinterpret_cast<void**>(0);
    any_has_acl = 0;
    inode_number_width = 0;
    block_size_width = 0;
    nlink_width = 0;
    owner_width = 0;
    group_width = 0;
    author_width = 0;
    scontext_width = 0;
    major_device_number_width = 0;
    minor_device_number_width = 0;
    file_size_width = 0;
    return;
}