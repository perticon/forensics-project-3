void clear_files(void) {
    int64_t v1 = cwd_n_used; // 0x8270
    if (v1 != 0) {
        int64_t v2 = (int64_t)sorted_file; // 0x8280
        int64_t v3 = v2; // 0x828b
        v3 += 8;
        function_4630();
        function_4630();
        function_4630();
        while (8 * v1 + v2 != v3) {
            // 0x8290
            v3 += 8;
            function_4630();
            function_4630();
            function_4630();
        }
    }
    // 0x82b7
    *(char *)&cwd_some_quoted = 0;
    cwd_n_used = 0;
    *(char *)&any_has_acl = 0;
    inode_number_width = 0;
    block_size_width = 0;
    nlink_width = 0;
    owner_width = 0;
    group_width = 0;
    author_width = 0;
    scontext_width = 0;
    major_device_number_width = 0;
    minor_device_number_width = 0;
    file_size_width = 0;
}