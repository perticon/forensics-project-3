getfilecon_cache (char const *file, struct fileinfo *f, bool deref)
{
  /* st_dev of the most recently processed device for which we've
     found that [l]getfilecon fails indicating lack of support.  */
  static dev_t unsupported_device;

  if (f->stat.st_dev == unsupported_device)
    {
      errno = ENOTSUP;
      return -1;
    }
  int r = 0;
#ifdef HAVE_SMACK
  if (is_smack_enabled ())
    r = smack_new_label_from_path (file, "security.SMACK64", deref,
                                   &f->scontext);
  else
#endif
    r = (deref
         ? getfilecon (file, &f->scontext)
         : lgetfilecon (file, &f->scontext));
  if (r < 0 && errno_unsupported (errno))
    unsupported_device = f->stat.st_dev;
  return r;
}