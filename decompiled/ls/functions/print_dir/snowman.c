void print_dir(void** rdi, void** rsi, void** edx, void** rcx) {
    void** rdx3;
    void** r14_5;
    void** r13_6;
    void** ebx7;
    void** v8;
    void* rax9;
    void* v10;
    void*** rax11;
    void** rdi12;
    void*** rbp13;
    void** rax14;
    void* rsp15;
    void* rax16;
    uint32_t ebx17;
    int1_t zf18;
    void** r12_19;
    int32_t eax20;
    void* rsp21;
    struct s1* rdx22;
    void** rdi23;
    int64_t rcx24;
    void** rsi25;
    int32_t eax26;
    void* rsp27;
    void** rax28;
    void** v29;
    void** v30;
    void** rax31;
    void** r8_32;
    void** rax33;
    uint32_t edi34;
    void* rax35;
    void** rax36;
    int64_t rdx37;
    int32_t eax38;
    void** v39;
    void* rsp40;
    int1_t zf41;
    int1_t zf42;
    int1_t zf43;
    int1_t zf44;
    int1_t zf45;
    void** r15_46;
    int1_t zf47;
    void** rsi48;
    void** rax49;
    void** rax50;
    int1_t zf51;
    void** rdi52;
    void** tmp64_53;
    void** rax54;
    int1_t zf55;
    void** v56;
    uint32_t v57;
    void** rdi58;
    struct s22* rax59;
    void* rsp60;
    struct s22* rbx61;
    void** rax62;
    void** r14_63;
    int32_t eax64;
    void* rax65;
    void** r15_66;
    int32_t eax67;
    void** r15_68;
    int32_t eax69;
    uint32_t eax70;
    int64_t rax71;
    void** rax72;
    int1_t zf73;
    int1_t zf74;
    int1_t zf75;
    int1_t zf76;
    void** rdi77;
    int32_t eax78;
    void* rsp79;
    void** rax80;
    void* rsp81;
    int1_t zf82;
    void** eax83;
    int1_t zf84;
    int64_t rdx85;
    void** r8_86;
    void* rsi87;
    void** rax88;
    void** rax89;
    uint32_t edx90;
    void** r12_91;
    signed char* rax92;
    int1_t zf93;
    void** rax94;
    void** rax95;
    void** rsi96;
    int1_t zf97;
    void* rax98;
    void* rax99;
    int1_t below_or_equal100;
    int32_t eax101;
    int64_t rax102;

    rdx3 = edx;
    r14_5 = rsi;
    r13_6 = rdi;
    ebx7 = rdx3;
    v8 = rdx3;
    rax9 = g28;
    v10 = rax9;
    rax11 = fun_46f0();
    rdi12 = r13_6;
    *rax11 = reinterpret_cast<void**>(0);
    rbp13 = rax11;
    rax14 = fun_4810(rdi12);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x358 - 8 + 8 - 8 + 8);
    if (!rax14) {
        fun_4840();
        rax16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
        if (rax16) {
            addr_dbd1_3:
            fun_4870();
        } else {
            ebx17 = *reinterpret_cast<unsigned char*>(&v8);
            quotearg_style(4, r13_6);
            fun_46f0();
            fun_4b70();
            if (!*reinterpret_cast<signed char*>(&ebx17)) 
                goto addr_7518_6; else 
                goto addr_7504_7;
        }
    } else {
        zf18 = active_dir_set == 0;
        r12_19 = rax14;
        if (!zf18) {
            eax20 = fun_49d0(rax14);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            rdx22 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rsp21) + 32);
            *reinterpret_cast<int32_t*>(&rdi23) = eax20;
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            if (eax20 < 0) {
                *reinterpret_cast<int32_t*>(&rcx24) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
                rsi25 = r13_6;
                eax26 = do_statx(0xffffff9c, rsi25, rdx22);
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (eax26 >= 0) {
                    addr_d5b6_11:
                    rax28 = xmalloc(16, rsi25, 16, rsi25);
                    rdi12 = active_dir_set;
                    *reinterpret_cast<void***>(rax28 + 8) = v29;
                    rsi = rax28;
                    *reinterpret_cast<void***>(rax28) = v30;
                    rax31 = hash_insert(rdi12, rsi, rdx22, rcx24);
                    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8);
                    r8_32 = rax28;
                    if (!rax31) {
                        xalloc_die();
                        goto addr_dbd1_3;
                    }
                } else {
                    addr_d8af_13:
                    rax33 = fun_4840();
                    edi34 = *reinterpret_cast<unsigned char*>(&v8);
                    file_failure(edi34, rax33, r13_6, edi34, rax33, r13_6);
                    rax35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
                    if (!rax35) {
                    }
                }
                if (r8_32 == rax31) {
                    rax36 = g260f8;
                    rdx37 = g26100;
                    if (reinterpret_cast<uint64_t>(rdx37 - reinterpret_cast<unsigned char>(rax36)) <= 15) {
                        *reinterpret_cast<uint32_t*>(&rsi) = 16;
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        rdi12 = reinterpret_cast<void**>(0x260e0);
                        _obstack_newchunk(0x260e0, 0x260e0);
                        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                        rax36 = g260f8;
                    }
                    rcx = v30;
                    rdx3 = rax36 + 16;
                    g260f8 = rdx3;
                    *reinterpret_cast<void***>(rax36 + 8) = v29;
                    *reinterpret_cast<void***>(rax36) = rcx;
                } else {
                    fun_4630(r8_32, r8_32);
                    quotearg_n_style_colon();
                    fun_4840();
                    fun_4b70();
                    fun_4960(r12_19);
                    exit_status = 2;
                    goto addr_d655_21;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx24) = 0x1000;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
                rsi25 = reinterpret_cast<void**>(0x1bb0a);
                eax38 = do_statx(rdi23, 0x1bb0a, rdx22);
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (eax38 < 0) 
                    goto addr_d8af_13; else 
                    goto addr_d5b6_11;
            }
        }
        v39 = reinterpret_cast<void**>(0xd6b5);
        clear_files(rdi12, rsi);
        rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        zf41 = recursive == 0;
        if (!zf41) {
            zf42 = first_0 == 0;
            if (zf42) 
                goto addr_d7f4_25; else 
                goto addr_d6cf_26;
        }
        zf43 = print_dir_name == 0;
        if (!zf43) {
            zf44 = first_0 == 0;
            if (!zf44) {
                addr_d6cf_26:
                zf45 = dired == 0;
                first_0 = 0;
                if (!zf45) {
                    addr_d830_29:
                    *reinterpret_cast<int32_t*>(&r15_46) = 0;
                    *reinterpret_cast<int32_t*>(&r15_46 + 4) = 0;
                    v39 = reinterpret_cast<void**>(0xd844);
                    dired_outbuf("  ", 2);
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                    zf47 = print_hyperlink == 0;
                    if (zf47) {
                        addr_d6f3_30:
                        rsi48 = dirname_quoting_options;
                        rdx3 = reinterpret_cast<void**>(0xffffffff);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        if (!r14_5) {
                            r14_5 = r13_6;
                        }
                    } else {
                        addr_d851_32:
                        v39 = reinterpret_cast<void**>(0xd85e);
                        rax49 = canonicalize_filename_mode(r13_6, 2);
                        rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                        r15_46 = rax49;
                        if (!rax49) {
                            rax50 = fun_4840();
                            v39 = reinterpret_cast<void**>(0xd88f);
                            file_failure(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v8)), rax50, r13_6);
                            rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8 - 8 + 8);
                            goto addr_d6f3_30;
                        }
                    }
                } else {
                    addr_d6e3_34:
                    *reinterpret_cast<int32_t*>(&r15_46) = 0;
                    *reinterpret_cast<int32_t*>(&r15_46 + 4) = 0;
                    zf51 = print_hyperlink == 0;
                    if (!zf51) 
                        goto addr_d851_32; else 
                        goto addr_d6f3_30;
                }
            } else {
                addr_d7f4_25:
                rdi52 = stdout;
                tmp64_53 = dired_pos + 1;
                dired_pos = tmp64_53;
                rax54 = *reinterpret_cast<void***>(rdi52 + 40);
                if (reinterpret_cast<unsigned char>(rax54) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi52 + 48))) {
                    v39 = reinterpret_cast<void**>(0xdb80);
                    fun_48d0();
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                    goto addr_d6cf_26;
                } else {
                    zf55 = dired == 0;
                    first_0 = 0;
                    *reinterpret_cast<void***>(rdi52 + 40) = rax54 + 1;
                    *reinterpret_cast<void***>(rax54) = reinterpret_cast<void**>(10);
                    if (zf55) 
                        goto addr_d6e3_34; else 
                        goto addr_d830_29;
                }
            }
            *reinterpret_cast<uint32_t*>(&r8_32) = 1;
            *reinterpret_cast<int32_t*>(&r8_32 + 4) = 0;
            quote_name(r14_5, rsi48, 0xffffffff, 0, 1, 0x26140, r15_46);
            fun_4630(r15_46, r15_46);
            dired_outbuf(":\n", 2, ":\n", 2);
            rcx = r15_46;
            rsi = v39;
            rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 + 8 + 8);
        }
        v56 = reinterpret_cast<void**>(0);
        v57 = *reinterpret_cast<unsigned char*>(&ebx7);
        while (1) {
            *rbp13 = reinterpret_cast<void**>(0);
            rdi58 = r12_19;
            rax59 = fun_4a80(rdi58, rsi, rdx3, rcx, r8_32, 0x26140);
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
            rbx61 = rax59;
            if (!rax59) {
                rdx3 = *rbp13;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                if (!rdx3) 
                    break;
                rax62 = fun_4840();
                *reinterpret_cast<uint32_t*>(&rdi58) = v57;
                *reinterpret_cast<int32_t*>(&rdi58 + 4) = 0;
                rdx3 = r13_6;
                rsi = rax62;
                file_failure(*reinterpret_cast<uint32_t*>(&rdi58), rsi, rdx3, *reinterpret_cast<uint32_t*>(&rdi58), rsi, rdx3);
                rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8);
                if (*rbp13 == 75) 
                    goto addr_d7d0_42; else 
                    break;
            }
            r14_63 = reinterpret_cast<void**>(&rax59->f13);
            eax64 = ignore_mode;
            if (eax64 == 2) 
                goto addr_da60_44;
            if (*reinterpret_cast<void***>(&rbx61->f13) == 46) {
                if (!eax64 || (*reinterpret_cast<int32_t*>(&rax65) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax65) + 4) = 0, *reinterpret_cast<unsigned char*>(&rax65) = reinterpret_cast<uint1_t>(rbx61->f14 == 46), *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rbx61) + reinterpret_cast<int64_t>(rax65) + 20) == 0)) {
                    addr_d7d0_42:
                    process_signals(rdi58, rsi, rdx3, rcx, r8_32, 0x26140, rdi58, rsi, rdx3);
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                    continue;
                } else {
                    goto addr_da60_44;
                }
            }
            if (eax64) {
                addr_da60_44:
                r15_66 = ignore_patterns;
                if (r15_66) {
                    do {
                        rdi58 = *reinterpret_cast<void***>(r15_66);
                        rdx3 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsi = r14_63;
                        eax67 = fun_4910(rdi58, rsi, rdi58, rsi);
                        rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                        if (!eax67) 
                            break;
                        r15_66 = *reinterpret_cast<void***>(r15_66 + 8);
                    } while (r15_66);
                    goto addr_da98_51;
                } else {
                    goto addr_da98_51;
                }
            } else {
                r15_68 = hide_patterns;
                if (r15_68) {
                    do {
                        rdi58 = *reinterpret_cast<void***>(r15_68);
                        rdx3 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsi = r14_63;
                        eax69 = fun_4910(rdi58, rsi, rdi58, rsi);
                        rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                        if (!eax69) 
                            goto addr_d7c9_55;
                        r15_68 = *reinterpret_cast<void***>(r15_68 + 8);
                    } while (r15_68);
                    goto addr_da60_44;
                } else {
                    goto addr_da60_44;
                }
            }
            goto addr_d7d0_42;
            addr_da98_51:
            *reinterpret_cast<uint32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            eax70 = rbx61->f12 - 1;
            if (*reinterpret_cast<unsigned char*>(&eax70) <= 13) {
                *reinterpret_cast<uint32_t*>(&rax71) = *reinterpret_cast<unsigned char*>(&eax70);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<uint32_t*>(0x1a600 + rax71 * 4);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            }
            rdx3 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
            rcx = r13_6;
            rdi58 = r14_63;
            rax72 = gobble_file_constprop_0(rdi58, *reinterpret_cast<uint32_t*>(&rsi), 0, rcx, r8_32, 0x26140);
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
            v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v56) + reinterpret_cast<unsigned char>(rax72));
            zf73 = reinterpret_cast<int1_t>(format == 1);
            if (zf73 && ((zf74 = sort_type == 6, zf74) && ((zf75 = print_block_size == 0, zf75) && (zf76 = recursive == 0, zf76)))) {
                sort_files(rdi58, rsi, 0, rcx, r8_32, 0x26140);
                print_current_files(rdi58, rsi);
                clear_files(rdi58, rsi);
                rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_d7d0_42;
            }
            addr_d7c9_55:
            goto addr_d7d0_42;
        }
        rdi77 = r12_19;
        eax78 = fun_4960(rdi77, rdi77);
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
        if (eax78) {
            rax80 = fun_4840();
            *reinterpret_cast<uint32_t*>(&rdi77) = *reinterpret_cast<unsigned char*>(&v8);
            *reinterpret_cast<int32_t*>(&rdi77 + 4) = 0;
            rdx3 = r13_6;
            rsi = rax80;
            file_failure(*reinterpret_cast<uint32_t*>(&rdi77), rsi, rdx3, *reinterpret_cast<uint32_t*>(&rdi77), rsi, rdx3);
            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        }
        sort_files(rdi77, rsi, rdx3, rcx, r8_32, 0x26140, rdi77, rsi);
        rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
        zf82 = recursive == 0;
        if (!zf82) {
            extract_dirs_from_files(r13_6, 0, rdx3, rcx, r8_32, 0x26140);
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
        }
        eax83 = format;
        if (!eax83 || (zf84 = print_block_size == 0, !zf84)) {
            *reinterpret_cast<int32_t*>(&rdx85) = human_output_opts;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx85) + 4) = 0;
            r8_86 = output_block_size;
            rsi87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) + 0xb1);
            rax88 = human_readable(v56, rsi87, rdx85, 0x200, r8_86, v56, rsi87, rdx85, 0x200, r8_86);
            rax89 = fun_4860(rax88, rax88);
            edx90 = eolbyte;
            *reinterpret_cast<void***>(rax88 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
            r12_91 = rax88 + 0xffffffffffffffff;
            rax92 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<unsigned char>(rax88));
            zf93 = dired == 0;
            *rax92 = *reinterpret_cast<signed char*>(&edx90);
            if (!zf93) {
                dired_outbuf("  ", 2, "  ", 2);
            }
            rax94 = fun_4840();
            rax95 = fun_4860(rax94, rax94);
            dired_outbuf(rax94, rax95, rax94, rax95);
            rsi96 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax92 + 1) - reinterpret_cast<unsigned char>(r12_91));
            dired_outbuf(r12_91, rsi96, r12_91, rsi96);
        }
        zf97 = cwd_n_used == 0;
        if (zf97) {
            addr_d655_21:
            rax98 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
            if (!rax98) {
                return;
            }
        } else {
            rax99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
            if (rax99) 
                goto addr_dbd1_3;
            below_or_equal100 = reinterpret_cast<unsigned char>(format) <= reinterpret_cast<unsigned char>(4);
            if (!below_or_equal100) 
                goto addr_d51b_75; else 
                goto addr_d12d_76;
        }
    }
    addr_7518_6:
    eax101 = exit_status;
    if (!eax101) {
        exit_status = 1;
        return;
    }
    addr_750e_79:
    return;
    addr_7504_7:
    exit_status = 2;
    goto addr_750e_79;
    addr_d51b_75:
    return;
    addr_d12d_76:
    *reinterpret_cast<void***>(&rax102) = format;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1a17c + rax102 * 4) + 0x1a17c;
}