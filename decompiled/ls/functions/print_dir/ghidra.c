void print_dir(char *param_1,char *param_2,undefined param_3)

{
  char **ppcVar1;
  char **ppcVar2;
  char cVar3;
  byte bVar4;
  int iVar5;
  int *piVar6;
  DIR *__dirp;
  undefined8 *puVar7;
  undefined8 *puVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  dirent *pdVar11;
  void *__ptr;
  char *pcVar12;
  size_t sVar13;
  char *__s;
  size_t sVar14;
  long lVar15;
  undefined4 uVar16;
  long in_FS_OFFSET;
  bool bVar17;
  long local_378;
  undefined8 local_368;
  undefined8 local_360;
  undefined local_2d7 [663];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  piVar6 = __errno_location();
  *piVar6 = 0;
  __dirp = opendir(param_1);
  if (__dirp == (DIR *)0x0) {
    uVar9 = dcgettext(0,"cannot open directory %s",5);
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      file_failure(param_3,uVar9,param_1);
      return;
    }
    goto LAB_0010dbd1;
  }
  if (active_dir_set != 0) {
    iVar5 = dirfd(__dirp);
    if (iVar5 < 0) {
      iVar5 = do_statx(0xffffff9c,param_1,&local_368,0,0x100);
    }
    else {
      iVar5 = do_statx(iVar5,&DAT_0011bb0a,&local_368,0x1000,0x100);
    }
    if (iVar5 < 0) {
      uVar9 = dcgettext(0,"cannot determine device and inode of %s",5);
      file_failure(param_3,uVar9,param_1);
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        closedir(__dirp);
        return;
      }
      goto LAB_0010dbd1;
    }
    puVar7 = (undefined8 *)xmalloc(0x10);
    lVar15 = active_dir_set;
    puVar7[1] = local_368;
    *puVar7 = local_360;
    puVar8 = (undefined8 *)hash_insert(lVar15,puVar7);
    if (puVar8 == (undefined8 *)0x0) {
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
    if (puVar7 != puVar8) {
      free(puVar7);
      uVar9 = quotearg_n_style_colon(0,3,param_1);
      uVar10 = dcgettext(0,"%s: not listing already-listed directory",5);
      error(0,0,uVar10,uVar9);
      closedir(__dirp);
      exit_status = 2;
      goto LAB_0010d655;
    }
    puVar7 = dev_ino_obstack._24_8_;
    if ((ulong)(dev_ino_obstack._32_8_ - (long)dev_ino_obstack._24_8_) < 0x10) {
      _obstack_newchunk(dev_ino_obstack,0x10);
      puVar7 = dev_ino_obstack._24_8_;
    }
    dev_ino_obstack._24_8_ = puVar7 + 2;
    puVar7[1] = local_368;
    *puVar7 = local_360;
  }
  uVar9 = 0x10d6b5;
  clear_files();
  cVar3 = dired;
  if (recursive == '\0') {
    if (print_dir_name == '\0') goto LAB_0010d73e;
    if (first_0 == '\0') goto LAB_0010d7f4;
LAB_0010d6cf:
    first_0 = '\0';
    cVar3 = dired;
  }
  else {
    if (first_0 != '\0') goto LAB_0010d6cf;
LAB_0010d7f4:
    dired_pos = dired_pos + 1;
    pcVar12 = stdout->_IO_write_ptr;
    if (stdout->_IO_write_end <= pcVar12) {
      uVar9 = 0x10db80;
      __overflow(stdout,10);
      goto LAB_0010d6cf;
    }
    first_0 = '\0';
    stdout->_IO_write_ptr = pcVar12 + 1;
    *pcVar12 = '\n';
  }
  if (cVar3 != '\0') {
    uVar9 = 0x10d844;
    dired_outbuf(&DAT_0011bb15,2);
  }
  __ptr = (void *)0x0;
  if (print_hyperlink != '\0') {
    uVar9 = 0x10d85e;
    __ptr = (void *)canonicalize_filename_mode(param_1,2);
    if (__ptr == (void *)0x0) {
      uVar10 = dcgettext(0,"error canonicalizing %s",5);
      uVar9 = 0x10d88f;
      file_failure(param_3,uVar10,param_1);
    }
  }
  if (param_2 == (char *)0x0) {
    param_2 = param_1;
  }
  quote_name(param_2,dirname_quoting_options,0xffffffff,0,1,subdired_obstack,__ptr,uVar9);
  free(__ptr);
  dired_outbuf(":\n",2);
LAB_0010d73e:
  local_378 = 0;
  do {
    *piVar6 = 0;
    pdVar11 = readdir(__dirp);
    if (pdVar11 == (dirent *)0x0) {
      if (*piVar6 == 0) break;
      uVar9 = dcgettext(0,"reading directory %s",5);
      file_failure(param_3,uVar9,param_1);
      if (*piVar6 != 0x4b) break;
    }
    else {
      pcVar12 = pdVar11->d_name;
      ppcVar2 = ignore_patterns;
      if (ignore_mode != 2) {
        if (pdVar11->d_name[0] == '.') {
          if ((ignore_mode == 0) ||
             (pdVar11->d_name[(ulong)(pdVar11->d_name[1] == '.') + 1] == '\0')) goto LAB_0010d7d0;
        }
        else {
          ppcVar1 = hide_patterns;
          if (ignore_mode == 0) {
            for (; ppcVar2 = ignore_patterns, ppcVar1 != (char **)0x0; ppcVar1 = (char **)ppcVar1[1]
                ) {
              iVar5 = fnmatch(*ppcVar1,pcVar12,4);
              if (iVar5 == 0) goto LAB_0010d7d0;
            }
          }
        }
      }
      for (; ppcVar2 != (char **)0x0; ppcVar2 = (char **)ppcVar2[1]) {
        iVar5 = fnmatch(*ppcVar2,pcVar12,4);
        if (iVar5 == 0) goto LAB_0010d7d0;
      }
      uVar16 = 0;
      bVar4 = pdVar11->d_type - 1;
      if (bVar4 < 0xe) {
        uVar16 = *(undefined4 *)(CSWTCH_995 + (ulong)bVar4 * 4);
      }
      lVar15 = gobble_file_constprop_0(pcVar12,uVar16,0,param_1);
      local_378 = local_378 + lVar15;
      if ((((format == 1) && (sort_type == 6)) && (print_block_size == '\0')) && (recursive == '\0')
         ) {
        sort_files();
        print_current_files();
        clear_files();
      }
    }
LAB_0010d7d0:
    process_signals();
  } while( true );
  iVar5 = closedir(__dirp);
  if (iVar5 != 0) {
    uVar9 = dcgettext(0,"closing directory %s",5);
    file_failure(param_3,uVar9,param_1);
  }
  sort_files();
  if (recursive != '\0') {
    extract_dirs_from_files(param_1,0);
  }
  if ((format == 0) || (print_block_size != '\0')) {
    pcVar12 = (char *)human_readable(local_378,local_2d7,human_output_opts,0x200,output_block_size);
    sVar13 = strlen(pcVar12);
    cVar3 = eolbyte;
    pcVar12[-1] = ' ';
    bVar17 = dired != '\0';
    pcVar12[sVar13] = cVar3;
    if (bVar17) {
      dired_outbuf(&DAT_0011bb15,2);
    }
    __s = (char *)dcgettext(0,"total",5);
    sVar14 = strlen(__s);
    dired_outbuf(__s,sVar14);
    dired_outbuf(pcVar12 + -1,pcVar12 + sVar13 + (1 - (long)(pcVar12 + -1)));
  }
  if (cwd_n_used != 0) {
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      print_current_files();
      return;
    }
    goto LAB_0010dbd1;
  }
LAB_0010d655:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_0010dbd1:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}