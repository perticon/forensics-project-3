void print_dir(char * name, char * realname, bool command_line_arg) {
    int64_t v1 = __readfsqword(40); // 0xd53d
    int32_t * v2 = (int32_t *)function_46f0(); // 0xd558
    *v2 = 0;
    if (function_4810() == 0) {
        int64_t v3 = function_4840(); // 0xdb93
        if (v1 == __readfsqword(40)) {
            // 0xdbae
            file_failure(command_line_arg, (char *)v3, name);
            return;
        }
        // 0xdbd1
        function_4870();
        return;
    }
    // 0xd56f
    int32_t v4; // bp-872, 0xd520
    if (active_dir_set == NULL) {
        goto lab_0xd6b0;
    } else {
        int32_t v5 = function_49d0(); // 0xd595
        if (v5 < 0) {
            // 0xd898
            if (do_statx(-100, name, (struct stat *)&v4, 0, 256) >= 0) {
                goto lab_0xd5b6;
            } else {
                goto lab_0xd8af;
            }
        } else {
            int32_t v6 = do_statx(v5, (char *)&g8, (struct stat *)&v4, (int32_t)&g1, 256); // 0xd5a9
            if (v6 < 0) {
                goto lab_0xd8af;
            } else {
                goto lab_0xd5b6;
            }
        }
    }
  lab_0xda60:;
    // 0xda60
    int64_t v7; // 0xd520
    int64_t v8; // 0xd520
    int64_t v9; // 0xd75a
    int64_t v10; // 0xd76b
    if (ignore_patterns != NULL) {
        int64_t v11 = (int64_t)ignore_patterns; // 0xda70
        v8 = v7;
        while ((int32_t)function_4910() != 0) {
            // 0xda70
            v11 += 8;
            if (v11 == 0) {
                goto lab_0xda98;
            }
            v8 = v7;
        }
    } else {
      lab_0xda98:;
        int64_t v12 = (int64_t)*(char *)(v9 + 18) + 0xffffffff; // 0xda9e
        int32_t v13 = 0; // 0xdaa3
        if ((char)v12 < 14) {
            // 0xdaa5
            v13 = *(int32_t *)((4 * v12 & 1020) + (int64_t)&g3);
        }
        int64_t v14 = gobble_file_constprop_0(v10, v13, 0, (int64_t *)name) + v7; // 0xdabf
        v8 = v14;
        if (format == 1) {
            // 0xdad1
            v8 = v14;
            if (sort_type == 6) {
                // 0xdade
                v8 = v14;
                if (*(char *)&print_block_size == 0) {
                    // 0xdaeb
                    v8 = v14;
                    if (*(char *)&recursive == 0) {
                        // 0xdaf8
                        sort_files();
                        print_current_files();
                        clear_files();
                        v8 = v14;
                    }
                }
            }
        }
    }
    goto lab_0xd7d0;
  lab_0xd7d0:
    // 0xd7d0
    process_signals();
    int64_t v15 = v8; // 0xd7d5
    goto lab_0xd750;
  lab_0xd6b0:
    // 0xd6b0
    clear_files();
    if (*(char *)&recursive == 0) {
        // 0xd7da
        if (*(char *)&print_dir_name == 0) {
            goto lab_0xd73e;
        } else {
            // 0xd7e7
            if (g39 != 0) {
                goto lab_0xd6cf;
            } else {
                goto lab_0xd7f4;
            }
        }
    } else {
        // 0xd6c2
        if (g39 == 0) {
            goto lab_0xd7f4;
        } else {
            goto lab_0xd6cf;
        }
    }
  lab_0xd73e:
    // 0xd73e
    v15 = 0;
    while (true) {
      lab_0xd750:
        // 0xd750
        v7 = v15;
        *v2 = 0;
        v9 = function_4a80();
        if (v9 == 0) {
            // 0xd908
            if (*v2 == 0) {
                // break -> 0xd93b
                break;
            }
            // 0xd90f
            file_failure(command_line_arg, (char *)function_4840(), name);
            v8 = v7;
            if (*v2 != 75) {
                // break -> 0xd93b
                break;
            }
            goto lab_0xd7d0;
        } else {
            // 0xd76b
            v10 = v9 + 19;
            if (ignore_mode == 2) {
                goto lab_0xda60;
            } else {
                // 0xd77e
                if (*(char *)v10 == 46) {
                    // 0xda40
                    v8 = v7;
                    if (ignore_mode == 0) {
                        goto lab_0xd7d0;
                    } else {
                        int64_t v16 = v9 + 20; // 0xda4a
                        v8 = v7;
                        if (*(char *)(v16 + (int64_t)(*(char *)v16 == 46)) == 0) {
                            goto lab_0xd7d0;
                        } else {
                            goto lab_0xda60;
                        }
                    }
                } else {
                    if (ignore_mode != 0) {
                        goto lab_0xda60;
                    } else {
                        // 0xd790
                        if (hide_patterns != NULL) {
                            int64_t v17 = (int64_t)hide_patterns; // 0xd7a8
                            v8 = v7;
                            while ((int32_t)function_4910() != 0) {
                                // 0xd7a8
                                v17 += 8;
                                if (v17 == 0) {
                                    goto lab_0xda60;
                                }
                                v8 = v7;
                            }
                            goto lab_0xd7d0;
                        } else {
                            goto lab_0xda60;
                        }
                    }
                }
            }
        }
    }
    // 0xd93b
    if ((int32_t)function_4960() != 0) {
        // 0xdb1b
        file_failure(command_line_arg, (char *)function_4840(), name);
    }
    // 0xd94b
    sort_files();
    if (*(char *)&recursive != 0) {
        // 0xdb0c
        extract_dirs_from_files(name, false);
    }
    // 0xd95d
    if (format != 0 != (*(char *)&print_block_size == 0)) {
        // 0xd974
        int64_t v18; // bp-727, 0xd520
        char * v19 = human_readable(v7, (char *)&v18, human_output_opts, 512, output_block_size); // 0xd993
        int64_t v20 = (int64_t)v19; // 0xd993
        int64_t v21 = function_4860(); // 0xd99e
        char * v22 = (char *)(v20 - 1); // 0xd9aa
        *v22 = 32;
        *(char *)(v21 + v20) = eolbyte;
        if (*(char *)&dired != 0) {
            // 0xdb43
            dired_outbuf("  ", 2);
        }
        int64_t v23 = function_4840(); // 0xd9d9
        dired_outbuf((char *)v23, function_4860());
        dired_outbuf(v22, v21 + 2);
    }
    // 0xd9ff
    if (cwd_n_used != 0) {
        // 0xda0d
        if (v1 == __readfsqword(40)) {
            // 0xda24
            print_current_files();
            return;
        }
        // 0xdbd1
        function_4870();
        return;
    }
    goto lab_0xd655;
  lab_0xd7f4:;
    int64_t v28 = (int64_t)g59; // 0xd7f4
    dired_pos++;
    int64_t * v29 = (int64_t *)(v28 + 40); // 0xd803
    uint64_t v30 = *v29; // 0xd803
    if (v30 >= *(int64_t *)(v28 + 48)) {
        // 0xdb76
        function_48d0();
        goto lab_0xd6cf;
    } else {
        // 0xd811
        g39 = 0;
        *v29 = v30 + 1;
        *(char *)v30 = 10;
        if (*(char *)&dired == 0) {
            goto lab_0xd6e3;
        } else {
            goto lab_0xd830;
        }
    }
  lab_0xd6cf:
    // 0xd6cf
    g39 = 0;
    if (*(char *)&dired != 0) {
        goto lab_0xd830;
    } else {
        goto lab_0xd6e3;
    }
  lab_0xd5b6:;
    int64_t v31 = v4; // 0xd5c0
    int64_t v32 = xmalloc(); // 0xd5ca
    *(int64_t *)(v32 + 8) = v31;
    char * v33 = hash_insert(active_dir_set, (int32_t *)v32); // 0xd5ed
    if (v33 == NULL) {
        // 0xdbcc
        xalloc_die();
        // 0xdbd1
        function_4870();
        return;
    }
    if (v32 == (int64_t)v33) {
        int64_t v34 = g69; // 0xd693
        if (g70 - g69 < 16) {
            // 0xdb59
            _obstack_newchunk((int32_t *)&dev_ino_obstack, 16);
            v34 = g69;
        }
        // 0xd699
        g69 = v34 + 16;
        *(int64_t *)(v34 + 8) = v31;
        int64_t v35; // 0xd520
        *(int64_t *)v34 = (int64_t)((uint64_t)v35 % 2 != 0);
        goto lab_0xd6b0;
    } else {
        // 0xd605
        function_4630();
        quotearg_n_style_colon();
        function_4840();
        function_4b70();
        function_4960();
        exit_status = 2;
        goto lab_0xd655;
    }
  lab_0xd8af:
    // 0xd8af
    file_failure(command_line_arg, (char *)function_4840(), name);
    if (v1 == __readfsqword(40)) {
        // 0xd8e9
        function_4960();
        return;
    }
    // 0xdbd1
    function_4870();
  lab_0xd655:
    // 0xd655
    if (v1 == __readfsqword(40)) {
        // 0xd66c
        return;
    }
    // 0xdbd1
    function_4870();
  lab_0xd830:
    // 0xd830
    dired_outbuf("  ", 2);
    int64_t v24 = 0; // 0xd84b
    if (*(char *)&print_hyperlink == 0) {
        goto lab_0xd6f3;
    } else {
        goto lab_0xd851;
    }
  lab_0xd6e3:
    // 0xd6e3
    v24 = 0;
    if (*(char *)&print_hyperlink != 0) {
        goto lab_0xd851;
    } else {
        goto lab_0xd6f3;
    }
  lab_0xd6f3:;
    char * v25 = realname == NULL ? name : realname;
    quote_name(v25, dirname_quoting_options, -1, NULL, true, (int32_t *)&subdired_obstack, (char *)v24);
    function_4630();
    dired_outbuf(":\n", 2);
    goto lab_0xd73e;
  lab_0xd851:;
    char * v26 = canonicalize_filename_mode(name, 2); // 0xd859
    int64_t v27 = (int64_t)v26; // 0xd859
    v24 = v27;
    if (v26 == NULL) {
        // 0xd86a
        file_failure(command_line_arg, (char *)function_4840(), name);
        v24 = v27;
    }
    goto lab_0xd6f3;
}