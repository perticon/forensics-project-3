void dired_outbuf(void** rdi, void** rsi, ...) {
    void** tmp64_3;

    tmp64_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rsi));
    dired_pos = tmp64_3;
}