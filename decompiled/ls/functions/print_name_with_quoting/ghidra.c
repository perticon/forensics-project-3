long print_name_with_quoting(char **param_1,byte param_2,undefined8 param_3,ulong param_4)

{
  char cVar1;
  uint uVar2;
  int iVar3;
  long lVar4;
  size_t sVar5;
  ulong *puVar6;
  uint uVar7;
  ulong uVar8;
  char *__s;
  
  if (param_2 == 0) {
    __s = *param_1;
    if (print_with_color == '\0') goto LAB_0010be82;
    uVar2 = (uint)*(byte *)((long)param_1 + 0xb9);
    if ((color_symlink_as_referent == '\0') || (*(byte *)((long)param_1 + 0xb9) == 0)) {
      uVar8 = (ulong)*(uint *)(param_1 + 6);
    }
    else {
      uVar8 = (ulong)*(uint *)((long)param_1 + 0xac);
    }
LAB_0010bf24:
    cVar1 = *(char *)(param_1 + 0x17);
    if (cVar1 == '\0') goto LAB_0010bf35;
LAB_0010c073:
    uVar7 = (uint)uVar8 & 0xf000;
    if (uVar7 == 0x8000) {
      if (((uVar8 & 0x800) != 0) && (cVar1 = is_colored(0x10), cVar1 != '\0')) {
        lVar4 = 0x100;
        goto LAB_0010bf8e;
      }
      uVar2 = (uint)uVar8;
      if (((uVar8 & 0x400) != 0) && (cVar1 = is_colored(0x11), cVar1 != '\0')) {
        lVar4 = 0x110;
        goto LAB_0010bf8e;
      }
      cVar1 = is_colored(0x15);
      if ((cVar1 != '\0') && (*(char *)(param_1 + 0x18) != '\0')) {
        lVar4 = 0x150;
        goto LAB_0010bf8e;
      }
      if (((uVar2 & 0x49) != 0) && (cVar1 = is_colored(0xe), cVar1 != '\0')) {
        lVar4 = 0xe0;
        goto LAB_0010bf8e;
      }
      if (((char *)0x1 < param_1[5]) && (cVar1 = is_colored(0x16), cVar1 != '\0')) {
        lVar4 = 0x160;
        goto LAB_0010bf8e;
      }
      goto LAB_0010c120;
    }
    if (uVar7 != 0x4000) {
      if (uVar7 != 0xa000) {
        lVar4 = 0x80;
        if ((((uVar7 != 0x1000) && (lVar4 = 0x90, uVar7 != 0xc000)) &&
            (lVar4 = 0xa0, uVar7 != 0x6000)) && (lVar4 = 0xb0, uVar7 != 0x2000)) {
          lVar4 = 0xd0;
        }
        goto LAB_0010bf8e;
      }
      uVar8 = 7;
      goto LAB_0010bf56;
    }
    if (((uint)uVar8 & 0x202) == 0x202) {
      cVar1 = is_colored(0x14);
      lVar4 = 0x140;
      if (cVar1 != '\0') goto LAB_0010bf8e;
    }
    uVar2 = (uint)uVar8;
    if ((uVar8 & 2) != 0) {
      cVar1 = is_colored(0x13);
      lVar4 = 0x130;
      if (cVar1 != '\0') goto LAB_0010bf8e;
    }
    lVar4 = 0x60;
    if ((uVar2 & 0x200) != 0) {
      cVar1 = is_colored(0x12);
      lVar4 = (-(ulong)(cVar1 == '\0') & 0xffffffffffffff40) + 0x120;
    }
  }
  else {
    __s = param_1[1];
    if (print_with_color == '\0') goto LAB_0010be82;
    uVar8 = (ulong)*(uint *)((long)param_1 + 0xac);
    if (*(char *)((long)param_1 + 0xb9) == '\0') {
      cVar1 = is_colored(0xc);
      if (cVar1 == '\0') {
        uVar2 = 0xffffffff;
        goto LAB_0010bf24;
      }
      lVar4 = 0xc0;
      goto LAB_0010bf8e;
    }
    cVar1 = *(char *)(param_1 + 0x17);
    uVar2 = 0;
    if (cVar1 != '\0') goto LAB_0010c073;
LAB_0010bf35:
    uVar7 = *(uint *)(filetype_indicator_5 + (ulong)*(uint *)(param_1 + 0x15) * 4);
    uVar8 = (ulong)uVar7;
    cVar1 = uVar7 == 7;
    if (uVar7 == 5) {
LAB_0010c120:
      sVar5 = strlen(__s);
      for (puVar6 = color_ext_list; puVar6 != (ulong *)0x0; puVar6 = (ulong *)puVar6[4]) {
        if ((*puVar6 <= sVar5) &&
           (iVar3 = c_strncasecmp(__s + (sVar5 - *puVar6),puVar6[1]), iVar3 == 0)) {
          puVar6 = puVar6 + 2;
          goto LAB_0010bf98;
        }
      }
      lVar4 = 0x50;
    }
    else {
LAB_0010bf56:
      if ((uVar2 == 0) && (cVar1 != '\0')) {
        lVar4 = 0xd0;
        if (color_symlink_as_referent == '\0') {
          cVar1 = is_colored(0xd);
          lVar4 = (-(ulong)(cVar1 == '\0') & 0xffffffffffffffa0) + 0xd0;
        }
      }
      else {
        lVar4 = uVar8 << 4;
      }
    }
  }
LAB_0010bf8e:
  puVar6 = (ulong *)(color_indicator + lVar4);
LAB_0010bf98:
  if (puVar6[1] == 0) {
    cVar1 = is_colored(4);
    if (cVar1 == '\0') {
LAB_0010be82:
      lVar4 = quote_name(__s,filename_quoting_options,*(undefined4 *)((long)param_1 + 0xc4),0,
                         param_2 ^ 1,param_3,param_1[2]);
      process_signals();
      return lVar4;
    }
    puVar6 = (ulong *)0x0;
  }
  lVar4 = quote_name(__s,filename_quoting_options,*(undefined4 *)((long)param_1 + 0xc4),puVar6,
                     param_2 ^ 1,param_3,param_1[2]);
  process_signals();
  if (color_indicator._40_8_ == 0) {
    put_indicator(color_indicator);
    put_indicator(0x125090);
    put_indicator(0x125070);
  }
  else {
    put_indicator(0x125080);
  }
  if (line_length == 0) {
    return lVar4;
  }
  uVar8 = lVar4 + -1 + param_4;
  if (param_4 / line_length == uVar8 / line_length) {
    return lVar4;
  }
  put_indicator(0x1251d0,param_4 / line_length,uVar8 % line_length);
  return lVar4;
}