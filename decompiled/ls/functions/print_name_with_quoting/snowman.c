void* print_name_with_quoting(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r14d8;
    void** r12_9;
    void** rbp10;
    uint32_t eax11;
    void** r13_12;
    int1_t zf13;
    uint32_t eax14;
    void** esi15;
    uint32_t ecx16;
    uint32_t edx17;
    void** v18;
    unsigned char al19;
    unsigned char al20;
    unsigned char al21;
    unsigned char al22;
    unsigned char al23;
    void** rax24;
    void** rbx25;
    void** rcx26;
    struct s13* rcx27;
    void** v28;
    int32_t eax29;
    void** rcx30;
    unsigned char al31;
    unsigned char al32;
    unsigned char al33;
    void* rcx34;
    uint64_t rdx35;
    int64_t rcx36;
    uint32_t r14d37;
    void** rdx38;
    void** v39;
    void** r8_40;
    void** rsi41;
    void* rax42;
    void* r12_43;
    int1_t zf44;
    void** v45;
    void** v46;
    void** v47;
    void** v48;
    unsigned char al49;
    void** rsi50;
    void** rdx51;
    void** v52;
    uint32_t r14d53;
    void** r8_54;
    void* rax55;
    void** rcx56;
    void** rsi57;
    void* rax58;
    void** v59;
    int1_t zf60;
    unsigned char al61;
    unsigned char al62;

    r15_7 = rdx;
    r14d8 = esi;
    r12_9 = rdi;
    rbp10 = rcx;
    eax11 = print_with_color;
    if (!*reinterpret_cast<signed char*>(&esi)) {
        r13_12 = *reinterpret_cast<void***>(rdi);
        if (!*reinterpret_cast<signed char*>(&eax11)) 
            goto addr_be82_3;
        zf13 = color_symlink_as_referent == 0;
        eax14 = *reinterpret_cast<unsigned char*>(rdi + 0xb9);
        if (zf13 || !*reinterpret_cast<signed char*>(&eax14)) {
            esi15 = *reinterpret_cast<void***>(r12_9 + 48);
        } else {
            esi15 = *reinterpret_cast<void***>(r12_9 + 0xac);
        }
    } else {
        r13_12 = *reinterpret_cast<void***>(rdi + 8);
        if (!*reinterpret_cast<signed char*>(&eax11)) 
            goto addr_be82_3;
        esi15 = *reinterpret_cast<void***>(rdi + 0xac);
        if (*reinterpret_cast<unsigned char*>(rdi + 0xb9)) 
            goto addr_c060_9; else 
            goto addr_bee3_10;
    }
    addr_bf24_11:
    ecx16 = *reinterpret_cast<unsigned char*>(r12_9 + 0xb8);
    if (*reinterpret_cast<unsigned char*>(&ecx16)) {
        addr_c073_12:
        edx17 = reinterpret_cast<unsigned char>(esi15) & 0xf000;
        if (edx17 == 0x8000) {
            if (!(reinterpret_cast<unsigned char>(esi15) & 0x800) || (v18 = reinterpret_cast<void**>(0xc1b2), al19 = is_colored(16), al19 == 0)) {
                if (!(reinterpret_cast<unsigned char>(esi15) & reinterpret_cast<uint32_t>("v")) || (v18 = reinterpret_cast<void**>(0xc202), al20 = is_colored(17), al20 == 0)) {
                    v18 = reinterpret_cast<void**>(0xc1d2);
                    al21 = is_colored(21);
                    if (!al21 || !*reinterpret_cast<signed char*>(r12_9 + 0xc0)) {
                        if (!(reinterpret_cast<unsigned char>(esi15) & 73) || (v18 = reinterpret_cast<void**>(0xc28f), al22 = is_colored(14), al22 == 0)) {
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 40)) <= reinterpret_cast<unsigned char>(1) || (v18 = reinterpret_cast<void**>(0xc2b6), al23 = is_colored(22), al23 == 0)) {
                                addr_c120_18:
                                v18 = reinterpret_cast<void**>(0xc128);
                                rax24 = fun_4860(r13_12);
                                rbx25 = color_ext_list;
                                rcx26 = rax24;
                                if (!rbx25) {
                                    addr_c171_19:
                                    *reinterpret_cast<int32_t*>(&rcx27) = 80;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                                    goto addr_bf8e_20;
                                } else {
                                    do {
                                        if (reinterpret_cast<unsigned char>(rcx26) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx25))) 
                                            continue;
                                        v28 = rcx26;
                                        v18 = reinterpret_cast<void**>(0xc15f);
                                        eax29 = c_strncasecmp();
                                        rcx26 = v28;
                                        if (!eax29) 
                                            break;
                                        rbx25 = *reinterpret_cast<void***>(rbx25 + 32);
                                    } while (rbx25);
                                    goto addr_c171_19;
                                }
                            } else {
                                *reinterpret_cast<int32_t*>(&rcx27) = 0x160;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                                goto addr_bf8e_20;
                            }
                        } else {
                            *reinterpret_cast<int32_t*>(&rcx27) = 0xe0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                            goto addr_bf8e_20;
                        }
                    } else {
                        *reinterpret_cast<int32_t*>(&rcx27) = 0x150;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                        goto addr_bf8e_20;
                    }
                } else {
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x110;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    goto addr_bf8e_20;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx27) = 0x100;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                goto addr_bf8e_20;
            }
            rcx30 = rbx25 + 16;
        } else {
            if (edx17 == 0x4000) {
                if ((reinterpret_cast<unsigned char>(esi15) & 0x202) == 0x202) {
                    v18 = reinterpret_cast<void**>(0xc2da);
                    al31 = is_colored(20);
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x140;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    if (al31) {
                        addr_bf8e_20:
                        rcx30 = reinterpret_cast<void**>(&rcx27->f25060);
                    } else {
                        goto addr_c222_35;
                    }
                } else {
                    addr_c222_35:
                    if ((!(*reinterpret_cast<unsigned char*>(&esi15) & 2) || (v18 = reinterpret_cast<void**>(0xc232), al32 = is_colored(19), *reinterpret_cast<int32_t*>(&rcx27) = 0x130, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, !al32)) && (*reinterpret_cast<int32_t*>(&rcx27) = 96, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, !!(reinterpret_cast<unsigned char>(esi15) & 0x200))) {
                        v18 = reinterpret_cast<void**>(0xc25a);
                        al33 = is_colored(18);
                        rcx34 = reinterpret_cast<void*>(96 - (96 + reinterpret_cast<uint1_t>(96 < 96 + reinterpret_cast<uint1_t>(al33 < 1))));
                        *reinterpret_cast<unsigned char*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx34) & 64);
                        rcx27 = reinterpret_cast<struct s13*>(reinterpret_cast<uint64_t>(rcx34) + 0x120);
                        goto addr_bf8e_20;
                    }
                }
            } else {
                if (edx17 == 0xa000) {
                    *reinterpret_cast<int32_t*>(&rdx35) = 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
                    goto addr_bf56_39;
                } else {
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    if (edx17 != 0x1000 && ((*reinterpret_cast<int32_t*>(&rcx27) = 0x90, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, edx17 != 0xc000) && (*reinterpret_cast<int32_t*>(&rcx27) = 0xa0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, edx17 != 0x6000))) {
                        *reinterpret_cast<int32_t*>(&rcx27) = 0xb0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                        if (edx17 != 0x2000) {
                            rcx27 = reinterpret_cast<struct s13*>(0xd0);
                        }
                        goto addr_bf8e_20;
                    }
                }
            }
        }
    } else {
        addr_bf35_44:
        *reinterpret_cast<uint32_t*>(&rcx36) = *reinterpret_cast<uint32_t*>(r12_9 + 0xa8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx35) = *reinterpret_cast<int32_t*>("\r" + rcx36 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&ecx16) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx35) == 7);
        if (*reinterpret_cast<int32_t*>(&rdx35) == 5) 
            goto addr_c120_18; else 
            goto addr_bf56_39;
    }
    if (*reinterpret_cast<void***>(rcx30 + 8)) {
        addr_bfb3_46:
        r14d37 = reinterpret_cast<unsigned char>(r14d8) ^ 1;
        rdx38 = *reinterpret_cast<void***>(r12_9 + 0xc4);
        *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
        v39 = *reinterpret_cast<void***>(r12_9 + 16);
        *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<unsigned char*>(&r14d37);
        *reinterpret_cast<int32_t*>(&r8_40 + 4) = 0;
        rsi41 = filename_quoting_options;
        rax42 = quote_name(r13_12, rsi41, rdx38, rcx30, *reinterpret_cast<uint32_t*>(&r8_40), r15_7, v39);
        r12_43 = rax42;
        process_signals(r13_12, rsi41, rdx38, rcx30, r8_40, r15_7);
        zf44 = g25088 == 0;
        if (zf44) {
            put_indicator(0x25060, rsi41, v18, rcx30, r8_40, r15_7, v45, v28);
            put_indicator(0x25090, rsi41, v18, rcx30, r8_40, r15_7, v46, v28);
            put_indicator(0x25070, rsi41, v18, rcx30, r8_40, r15_7, v47, v28);
        } else {
            put_indicator(0x25080, rsi41, v18, rcx30, r8_40, r15_7, v48, v28);
        }
    } else {
        v18 = reinterpret_cast<void**>(0xbfa9);
        al49 = is_colored(4, 4);
        if (!al49) {
            addr_be82_3:
            rsi50 = filename_quoting_options;
            rdx51 = *reinterpret_cast<void***>(r12_9 + 0xc4);
            *reinterpret_cast<int32_t*>(&rdx51 + 4) = 0;
            v52 = *reinterpret_cast<void***>(r12_9 + 16);
            r14d53 = reinterpret_cast<unsigned char>(r14d8) ^ 1;
            *reinterpret_cast<uint32_t*>(&r8_54) = *reinterpret_cast<unsigned char*>(&r14d53);
            *reinterpret_cast<int32_t*>(&r8_54 + 4) = 0;
            rax55 = quote_name(r13_12, rsi50, rdx51, 0, *reinterpret_cast<uint32_t*>(&r8_54), r15_7, v52);
            r12_43 = rax55;
            process_signals(r13_12, rsi50, rdx51, 0, r8_54, r15_7);
            goto addr_beb9_50;
        } else {
            *reinterpret_cast<int32_t*>(&rcx30) = 0;
            *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
            goto addr_bfb3_46;
        }
    }
    rcx56 = line_length;
    if (rcx56 && (rsi57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) / reinterpret_cast<unsigned char>(rcx56)), rax58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_43) + reinterpret_cast<unsigned char>(rbp10) + 0xffffffffffffffff), rsi57 != reinterpret_cast<uint64_t>(rax58) / reinterpret_cast<unsigned char>(rcx56))) {
        put_indicator(0x251d0, rsi57, reinterpret_cast<uint64_t>(rax58) % reinterpret_cast<unsigned char>(rcx56), rcx56, r8_40, r15_7, v59, v28);
    }
    addr_beb9_50:
    return r12_43;
    addr_bf56_39:
    if (eax14 || !*reinterpret_cast<unsigned char*>(&ecx16)) {
        rcx27 = reinterpret_cast<struct s13*>(rdx35 << 4);
        goto addr_bf8e_20;
    } else {
        zf60 = color_symlink_as_referent == 0;
        *reinterpret_cast<int32_t*>(&rcx27) = 0xd0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
        if (zf60) {
            v18 = reinterpret_cast<void**>(0xbf7e);
            al61 = is_colored(13);
            rcx27 = reinterpret_cast<struct s13*>((0xd0 - (0xd0 + reinterpret_cast<uint1_t>(0xd0 < 0xd0 + reinterpret_cast<uint1_t>(al61 < 1))) & 0xffffffffffffffa0) + 0xd0);
            goto addr_bf8e_20;
        }
    }
    addr_c060_9:
    ecx16 = *reinterpret_cast<unsigned char*>(r12_9 + 0xb8);
    eax14 = 0;
    if (!*reinterpret_cast<unsigned char*>(&ecx16)) 
        goto addr_bf35_44; else 
        goto addr_c073_12;
    addr_bee3_10:
    v18 = reinterpret_cast<void**>(0xbeed);
    al62 = is_colored(12);
    if (al62) {
        *reinterpret_cast<int32_t*>(&rcx27) = 0xc0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
        goto addr_bf8e_20;
    } else {
        eax14 = 0xffffffff;
        goto addr_bf24_11;
    }
}