int64_t print_name_with_quoting(int32_t * f, bool symlink_target, int32_t * stack, uint64_t start_col) {
    int64_t v1 = (int64_t)f;
    uint64_t v2 = symlink_target ? 0xffffffff : 0; // 0xbe57
    char v3 = *(char *)&print_with_color; // 0xbe6a
    int64_t v4; // 0xbe50
    int64_t v5; // 0xbe50
    int64_t v6; // 0xbe50
    int64_t v7; // 0xbe50
    int64_t v8; // 0xbe50
    int64_t v9; // 0xbe50
    int64_t v10; // 0xbe50
    int64_t v11; // 0xbe50
    int64_t v12; // 0xbe50
    char v13; // 0xbe50
    int64_t v14; // 0xbe50
    int64_t v15; // 0xbe50
    if (!symlink_target) {
        // 0xbf00
        v4 = v1;
        if (v3 == 0) {
            goto lab_0xbe82;
        } else {
            unsigned char v16 = *(char *)(v1 + 185); // 0xbf12
            int64_t v17 = v16; // 0xbf12
            if (*(char *)&color_symlink_as_referent == 0 || v16 == 0) {
                // 0xbf1f
                v9 = v17;
                v14 = (int64_t)*(int32_t *)(v1 + 48);
                v5 = v1;
            } else {
                // 0xc050
                v9 = v17;
                v14 = (int64_t)*(int32_t *)(v1 + 172);
                v5 = v1;
            }
            goto lab_0xbf24;
        }
    } else {
        int64_t v18 = *(int64_t *)(v1 + 8); // 0xbe7a
        v4 = v18;
        if (v3 != 0) {
            int64_t v19 = (int64_t)*(int32_t *)(v1 + 172); // 0xbed7
            if (*(char *)(v1 + 185) != 0) {
                char v20 = *(char *)(v1 + 184); // 0xc060
                v10 = 0;
                v6 = v18;
                v11 = 0;
                v13 = v20;
                v15 = v19;
                v8 = v18;
                if (v20 == 0) {
                    goto lab_0xbf35;
                } else {
                    goto lab_0xc073;
                }
            } else {
                // 0xbee3
                v9 = 0xffffffff;
                v14 = v19;
                v5 = v18;
                v12 = 192;
                v7 = v18;
                if (is_colored(12)) {
                    goto lab_0xbf8e;
                } else {
                    goto lab_0xbf24;
                }
            }
        } else {
            goto lab_0xbe82;
        }
    }
  lab_0xbe82:;
    int32_t v21 = *(int32_t *)(v1 + 196); // 0xbe92
    int64_t v22 = *(int64_t *)(v1 + 16); // 0xbe9a
    int64_t result = quote_name((char *)v4, filename_quoting_options, v21, NULL, v2 % 2 == 0, stack, (char *)v22); // 0xbeaa
    process_signals();
    // 0xbeb9
    return result;
  lab_0xbf24:;
    char v23 = *(char *)(v1 + 184); // 0xbf24
    v10 = v9;
    v6 = v5;
    v11 = v9;
    v13 = v23;
    v15 = v14;
    v8 = v5;
    if (v23 != 0) {
        goto lab_0xc073;
    } else {
        goto lab_0xbf35;
    }
  lab_0xc073:;
    int64_t v24 = v15 & 0xf000; // 0xc075
    int64_t v25; // 0xbe50
    int64_t v26; // 0xbe50
    char v27; // 0xbe50
    int64_t v28; // 0xbe50
    int32_t v29; // 0xc1a0
    if (v24 == 0x8000) {
        // 0xc1a0
        v29 = v15;
        if ((v29 & (int32_t)&g89) == 0) {
            goto lab_0xc1c0;
        } else {
            // 0xc1a8
            v12 = 256;
            v7 = v8;
            if (!is_colored(16)) {
                goto lab_0xc1c0;
            } else {
                goto lab_0xbf8e;
            }
        }
    } else {
        // 0xc087
        v26 = v11;
        v27 = v13;
        v28 = 7;
        v25 = v8;
        v12 = 128;
        v7 = v8;
        switch ((int16_t)v24) {
            case 0x4000: {
                if ((v15 & 514) == 514) {
                    // 0xc2d0
                    v12 = 320;
                    v7 = v8;
                    if (is_colored(20)) {
                        goto lab_0xbf8e;
                    } else {
                        goto lab_0xc222;
                    }
                } else {
                    goto lab_0xc222;
                }
            }
            case -0x6000: {
                goto lab_0xbf56;
            }
            case 0x1000: {
                goto lab_0xbf8e;
            }
            case -0x4000: {
                // 0xbf8e
                v12 = 144;
                v7 = v8;
                goto lab_0xbf8e;
            }
            default: {
                int32_t v30 = v24; // 0xc087
                v12 = 160;
                v7 = v8;
                if (v30 != 0x6000) {
                    // 0xc0d2
                    v12 = v30 != 0x2000 ? 208 : 176;
                    v7 = v8;
                }
                goto lab_0xbf8e;
            }
        }
    }
  lab_0xbf35:;
    uint32_t v31 = *(int32_t *)(v1 + 168); // 0xbf35
    uint32_t v32 = *(int32_t *)(4 * (int64_t)v31 + (int64_t)&g7); // 0xbf44
    v26 = v10;
    v27 = v32 == 7;
    v28 = v32;
    v25 = v6;
    int64_t v33 = v6; // 0xbf50
    if (v32 == 5) {
        goto lab_0xc120;
    } else {
        goto lab_0xbf56;
    }
  lab_0xbf8e:;
    int64_t v34 = v7; // 0xbf95
    int64_t v35 = v12 + (int64_t)&color_indicator; // 0xbf95
    goto lab_0xbf98;
  lab_0xc120:;
    uint64_t v39 = function_4860(); // 0xc123
    v12 = 80;
    v7 = v33;
    if (color_ext_list != NULL) {
        int64_t v40 = (int64_t)color_ext_list;
        uint64_t v41 = *(int64_t *)v40; // 0xc140
        int64_t v42; // 0xc14b
        if (v39 >= v41) {
            // 0xc148
            v42 = *(int64_t *)(v40 + 8);
            if (c_strncasecmp((char *)(v39 + v33 - v41), (char *)v42, v41) == 0) {
                // 0xc180
                v34 = v33;
                v35 = v40 + 16;
                goto lab_0xbf98;
            }
        }
        int64_t v43 = *(int64_t *)(v40 + 32); // 0xc168
        v12 = 80;
        v7 = v33;
        while (v43 != 0) {
            // 0xc140
            v40 = v43;
            v41 = *(int64_t *)v40;
            if (v39 >= v41) {
                // 0xc148
                v42 = *(int64_t *)(v40 + 8);
                if (c_strncasecmp((char *)(v39 + v33 - v41), (char *)v42, v41) == 0) {
                    // 0xc180
                    v34 = v33;
                    v35 = v40 + 16;
                    goto lab_0xbf98;
                }
            }
            // 0xc168
            v43 = *(int64_t *)(v40 + 32);
            v12 = 80;
            v7 = v33;
        }
    }
    goto lab_0xbf8e;
  lab_0xbf56:
    // 0xbf56
    if (v27 == 0 || (int32_t)v26 != 0) {
        // 0xc2ec
        v12 = 16 * v28;
        v7 = v25;
    } else {
        // 0xbf66
        v12 = 208;
        v7 = v25;
        if (*(char *)&color_symlink_as_referent == 0) {
            // 0xbf74
            v12 = !is_colored(13) ? 112 : 208;
            v7 = v25;
        }
    }
    goto lab_0xbf8e;
  lab_0xbf98:
    // 0xbf98
    if (*(int64_t *)(v35 + 8) != 0) {
        goto lab_0xbfb3;
    } else {
        // 0xbf9f
        v4 = v34;
        if (!is_colored(4)) {
            goto lab_0xbe82;
        } else {
            goto lab_0xbfb3;
        }
    }
  lab_0xc1c0:
    if ((v29 & (int32_t)&g86) != 0) {
        // 0xc1f8
        v12 = 272;
        v7 = v8;
        if (!is_colored(17)) {
            goto lab_0xc1c8;
        } else {
            goto lab_0xbf8e;
        }
    } else {
        goto lab_0xc1c8;
    }
  lab_0xbfb3:;
    int32_t v36 = *(int32_t *)(v1 + 196); // 0xbfc1
    int64_t v37 = *(int64_t *)(v1 + 16); // 0xbfc9
    int64_t result2 = quote_name((char *)v34, filename_quoting_options, v36, (int32_t *)0, v2 % 2 == 0, stack, (char *)v37); // 0xbfd9
    process_signals();
    if (g47 == 0) {
        // 0xc0f0
        put_indicator((int32_t *)&color_indicator);
        put_indicator((int32_t *)&g48);
        put_indicator((int32_t *)&g44);
    } else {
        // 0xbff6
        put_indicator((int32_t *)&g46);
    }
    uint64_t v38 = line_length; // 0xc002
    if (v38 != 0) {
        // 0xc012
        if (start_col / v38 != (start_col - 1 + result2) / v38) {
            // 0xc030
            put_indicator((int32_t *)&g51);
        }
    }
    // 0xbeb9
    return result2;
  lab_0xc1c8:
    // 0xc1c8
    if (!is_colored(21)) {
        goto lab_0xc280;
    } else {
        // 0xc1da
        v12 = 336;
        v7 = v8;
        if (*(char *)(v1 + 192) == 0) {
            goto lab_0xc280;
        } else {
            goto lab_0xbf8e;
        }
    }
  lab_0xc222:
    if ((v15 & 2) == 0) {
        goto lab_0xc23f;
    } else {
        // 0xc228
        v12 = 304;
        v7 = v8;
        if (is_colored(19)) {
            goto lab_0xbf8e;
        } else {
            goto lab_0xc23f;
        }
    }
  lab_0xc280:
    if ((v15 & 73) == 0) {
        goto lab_0xc2a0;
    } else {
        // 0xc285
        v12 = 224;
        v7 = v8;
        if (!is_colored(14)) {
            goto lab_0xc2a0;
        } else {
            goto lab_0xbf8e;
        }
    }
  lab_0xc23f:
    // 0xc23f
    v12 = 96;
    v7 = v8;
    if ((v15 & 512) != 0) {
        // 0xc250
        v12 = !is_colored(18) ? 96 : 288;
        v7 = v8;
    }
    goto lab_0xbf8e;
  lab_0xc2a0:
    // 0xc2a0
    v33 = v8;
    if (*(int64_t *)(v1 + 40) < 2) {
        goto lab_0xc120;
    } else {
        // 0xc2ac
        v12 = 352;
        v7 = v8;
        v33 = v8;
        if (!is_colored(22)) {
            goto lab_0xc120;
        } else {
            goto lab_0xbf8e;
        }
    }
}