errno_unsupported (int err)
{
  return (err == EINVAL || err == ENOSYS || is_ENOTSUP (err));
}