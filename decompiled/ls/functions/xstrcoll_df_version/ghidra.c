int xstrcoll_df_version(char **param_1,char **param_2)

{
  char *__s1;
  char *__s2;
  int iVar1;
  
  iVar1 = *(int *)(param_1 + 0x15);
  if (((*(int *)(param_2 + 0x15) == 3) || (*(int *)(param_2 + 0x15) == 9)) ||
     ((*(uint *)((long)param_2 + 0xac) & 0xf000) == 0x4000)) {
    if (iVar1 == 9 || iVar1 == 3) goto LAB_0010b780;
    iVar1 = 1;
  }
  else {
    if (iVar1 == 9 || iVar1 == 3) {
      return -1;
    }
    iVar1 = 0;
  }
  iVar1 = iVar1 - (uint)((*(uint *)((long)param_1 + 0xac) & 0xf000) == 0x4000);
  if (iVar1 != 0) {
    return iVar1;
  }
LAB_0010b780:
  __s1 = *param_1;
  __s2 = *param_2;
  iVar1 = filevercmp(__s1,__s2);
  if (iVar1 == 0) {
    iVar1 = strcmp(__s1,__s2);
    return iVar1;
  }
  return iVar1;
}