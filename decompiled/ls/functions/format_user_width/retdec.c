int32_t format_user_width(int32_t u) {
    // 0x8640
    if (*(char *)&numeric_ids != 0) {
        // 0x864c
        return function_46c0();
    }
    char * v1 = getuser(u); // 0x8670
    if (v1 == NULL) {
        // 0x864c
        return function_46c0();
    }
    int32_t v2 = gnu_mbswidth(v1, 0); // 0x867f
    return v2 > 0 ? v2 : 0;
}