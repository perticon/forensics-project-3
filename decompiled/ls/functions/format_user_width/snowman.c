uint32_t format_user_width(int32_t edi, ...) {
    int1_t zf2;
    void** rax3;
    uint32_t eax4;

    zf2 = numeric_ids == 0;
    if (!(!zf2 || (rax3 = getuser(), rax3 == 0))) {
        eax4 = gnu_mbswidth(rax3);
        if (reinterpret_cast<int32_t>(eax4) < reinterpret_cast<int32_t>(0)) {
            eax4 = 0;
        }
        return eax4;
    }
}