ulong format_user_width(undefined4 param_1)

{
  uint uVar1;
  ulong uVar2;
  long lVar3;
  
  if (numeric_ids == '\0') {
    lVar3 = getuser();
    if (lVar3 != 0) {
      uVar1 = gnu_mbswidth(lVar3,0);
      uVar2 = (ulong)uVar1;
      if ((int)uVar1 < 0) {
        uVar2 = 0;
      }
      return uVar2;
    }
  }
  uVar2 = __snprintf_chk(0,0,1,0xffffffffffffffff,&DAT_0011ba7d,param_1);
  return uVar2;
}