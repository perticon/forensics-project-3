get_stat_btime (struct stat const *st)
{
  struct timespec btimespec;

#if HAVE_STATX && defined STATX_INO
  btimespec = get_stat_mtime (st);
#else
  btimespec = get_stat_birthtime (st);
#endif

  return btimespec;
}