format_user_or_group_width (char const *name, uintmax_t id)
{
  if (name)
    {
      int len = mbswidth (name, 0);
      return MAX (0, len);
    }
  else
    return snprintf (NULL, 0, "%"PRIuMAX, id);
}