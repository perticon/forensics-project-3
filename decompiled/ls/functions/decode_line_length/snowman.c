void** decode_line_length(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void* rax6;
    int64_t rax7;
    void** rax8;
    void** v9;
    int64_t rax10;
    void* rdx11;
    int64_t rdi12;
    struct s4* rax13;
    int64_t v14;
    int64_t v15;
    int64_t v16;

    rax6 = g28;
    rax7 = xstrtoumax(rdi);
    if (!*reinterpret_cast<int32_t*>(&rax7)) {
        rax8 = v9;
        if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) {
            rax8 = reinterpret_cast<void**>(0);
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax10) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) != 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        rax8 = reinterpret_cast<void**>(-rax10);
    }
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
    if (!rdx11) {
        return rax8;
    }
    fun_4870();
    *reinterpret_cast<int32_t*>(&rdi12) = *reinterpret_cast<int32_t*>(&rdi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    rax13 = reinterpret_cast<struct s4*>(0x25060 + (rdi12 << 4));
    if (rax13->f0) 
        goto addr_6c4b_10;
    goto v14;
    addr_6c4b_10:
    if (rax13->f0 == 1) {
        goto v15;
    } else {
        if (rax13->f0 == 2) {
            if (!(*rax13->f8 - 48)) {
            }
            goto v16;
        }
    }
}