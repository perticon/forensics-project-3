int64_t decode_line_length(char * spec) {
    int64_t v1 = __readfsqword(40); // 0x6bcf
    int64_t v2; // bp-24, 0x6bc0
    int32_t v3 = xstrtoumax(spec, NULL, 0, &v2, (char *)&g8); // 0x6be2
    int64_t result; // 0x6bc0
    if (v3 == 0) {
        int64_t v4 = v2; // 0x6c10
        result = v4 > 0 ? v4 : 0;
    } else {
        // 0x6beb
        result = v3 != 1;
    }
    // 0x6bf7
    if (v1 != __readfsqword(40)) {
        // 0x6c1f
        return function_4870();
    }
    // 0x6c07
    return result;
}