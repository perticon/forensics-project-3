long decode_line_length(undefined8 param_1)

{
  int iVar1;
  long in_FS_OFFSET;
  long local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = xstrtoumax(param_1,0,0,&local_18,&DAT_0011bb0a);
  if (iVar1 == 0) {
    if (local_18 < 0) {
      local_18 = 0;
    }
  }
  else {
    local_18 = -(ulong)(iVar1 != 1);
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_18;
}