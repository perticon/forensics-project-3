void add_ignore_pattern(void** rdi, void** rsi) {
    void** rax3;
    void** rdx4;

    rax3 = xmalloc(16, rsi);
    rdx4 = ignore_patterns;
    *reinterpret_cast<void***>(rax3) = rdi;
    *reinterpret_cast<void***>(rax3 + 8) = rdx4;
    ignore_patterns = rax3;
    return;
}