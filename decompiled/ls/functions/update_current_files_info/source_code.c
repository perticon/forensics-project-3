update_current_files_info (void)
{
  /* Cache screen width of name, if needed multiple times.  */
  if (sort_type == sort_width
      || (line_length && (format == many_per_line || format == horizontal)))
    {
      size_t i;
      for (i = 0; i < cwd_n_used; i++)
        {
          struct fileinfo *f = sorted_file[i];
          f->width = fileinfo_name_width (f);
        }
    }
}