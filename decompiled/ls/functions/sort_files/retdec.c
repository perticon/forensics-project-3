void sort_files(void) {
    int64_t v1 = cwd_n_used; // 0x8d16
    int64_t v2 = v1; // 0x8d2d
    if (v1 / 2 + v1 > sorted_file_alloc) {
        // 0x8d2f
        function_4630();
        char * v3 = xnmalloc(v1, 24); // 0x8d43
        v2 = cwd_n_used;
        *(int64_t *)&sorted_file = (int64_t)v3;
        sorted_file_alloc = 3 * v2;
    }
    // 0x8d62
    int32_t v4; // 0x8d10
    if (v2 == 0) {
        // 0x8ef8
        v4 = sort_type;
        if (sort_type == 2) {
            // 0x8db9
            function_4980();
            return;
        }
        goto lab_0x8da2;
    } else {
        int64_t v5 = (int64_t)sorted_file; // 0x8d6b
        int64_t v6 = v5; // 0x8d7d
        int64_t v7 = (int64_t)cwd_file; // 0x8d7d
        *(int64_t *)v6 = v7;
        v6 += 8;
        v7 += 208;
        while (v6 != 8 * v2 + v5) {
            // 0x8d80
            *(int64_t *)v6 = v7;
            v6 += 8;
            v7 += 208;
        }
        // 0x8d93
        v4 = sort_type;
        if (sort_type == 2) {
            goto lab_0x8e9b;
        } else {
            goto lab_0x8da2;
        }
    }
  lab_0x8da2:;
    int32_t v8 = v4; // 0x8daa
    if (line_length != 0) {
        // 0x8e80
        v8 = v4;
        if (v2 == 0 || (format || 1) != 3) {
            goto lab_0x8db0;
        } else {
            goto lab_0x8e9b;
        }
    } else {
        goto lab_0x8db0;
    }
  lab_0x8e9b:;
    int64_t v9 = 0; // 0x8e9d
    int64_t v10 = *(int64_t *)(8 * v9 + (int64_t)sorted_file); // 0x8ebb
    int64_t * v11 = (int64_t *)(v10 + 200); // 0x8ebf
    int64_t v12 = *v11; // 0x8ebf
    int64_t v13 = v12; // 0x8ec9
    int64_t v14; // 0x8ed8
    if (v12 == 0) {
        // 0x8ecb
        v14 = *(int64_t *)v10;
        v13 = quote_name_width((char *)v14, filename_quoting_options, *(int32_t *)(v10 + 196));
    }
    int64_t v15 = v9 + 1; // 0x8ea0
    *v11 = v13;
    v9 = v15;
    while (v15 < cwd_n_used) {
        // 0x8eb4
        v10 = *(int64_t *)(8 * v9 + (int64_t)sorted_file);
        v11 = (int64_t *)(v10 + 200);
        v12 = *v11;
        v13 = v12;
        if (v12 == 0) {
            // 0x8ecb
            v14 = *(int64_t *)v10;
            v13 = quote_name_width((char *)v14, filename_quoting_options, *(int32_t *)(v10 + 196));
        }
        // 0x8ea0
        v15 = v9 + 1;
        *v11 = v13;
        v9 = v15;
    }
    // 0x8ee8
    v8 = sort_type;
    goto lab_0x8db0;
  lab_0x8db0:
    // 0x8db0
    if (v8 == 6) {
        // 0x8e5d
        return;
    }
    // 0x8db9
    function_4980();
}