void sort_files(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void** rbp7;
    int1_t below_or_equal8;
    void** rdi9;
    void** rax10;
    void** rax11;
    uint32_t eax12;
    int1_t zf13;
    void** rbx14;
    void** rax15;
    void** rbp16;
    void* rax17;
    void** rdi18;
    int1_t cf19;

    rbp7 = cwd_n_used;
    below_or_equal8 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp7) >> 1) + reinterpret_cast<unsigned char>(rbp7) <= reinterpret_cast<uint64_t>(sorted_file_alloc);
    if (!below_or_equal8) {
        rdi9 = sorted_file;
        fun_4630(rdi9);
        rsi = reinterpret_cast<void**>(24);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax10 = xnmalloc(rbp7, 24, rdx, rcx, r8, r9);
        rbp7 = cwd_n_used;
        sorted_file = rax10;
        sorted_file_alloc = reinterpret_cast<void***>(rbp7 + reinterpret_cast<unsigned char>(rbp7) * 2);
    }
    if (rbp7) {
        rax11 = sorted_file;
        rdx = cwd_file;
        rcx = rax11 + reinterpret_cast<unsigned char>(rbp7) * 8;
        do {
            *reinterpret_cast<void***>(rax11) = rdx;
            rax11 = rax11 + 8;
            rdx = rdx + 0xd0;
        } while (rax11 != rcx);
        eax12 = sort_type;
        if (eax12 == 2) 
            goto addr_8e9b_7; else 
            goto addr_8da2_8;
    }
    eax12 = sort_type;
    if (eax12 != 2) {
        addr_8da2_8:
        zf13 = line_length == 0;
        if (zf13 || ((rsi = format, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rdx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(rsi + 0xfffffffffffffffe))), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(1)) || !rbp7)) {
            addr_8db0_10:
            if (eax12 == 6) {
                return;
            }
        } else {
            addr_8e9b_7:
            *reinterpret_cast<int32_t*>(&rbx14) = 0;
            *reinterpret_cast<int32_t*>(&rbx14 + 4) = 0;
            goto addr_8eb4_12;
        }
    }
    fun_4980(0x25300, rsi, rdx);
    do {
        addr_8eb4_12:
        rax15 = sorted_file;
        rbp16 = *reinterpret_cast<void***>(rax15 + reinterpret_cast<unsigned char>(rbx14) * 8);
        rax17 = *reinterpret_cast<void**>(rbp16 + 0xc8);
        if (!rax17) {
            rdx = *reinterpret_cast<void***>(rbp16 + 0xc4);
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rsi = filename_quoting_options;
            rdi18 = *reinterpret_cast<void***>(rbp16);
            rax17 = quote_name_width(rdi18, rsi, rdx, rcx, r8, r9);
        }
        ++rbx14;
        cf19 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(cwd_n_used);
        *reinterpret_cast<void**>(rbp16 + 0xc8) = rax17;
    } while (cf19);
    eax12 = sort_type;
    goto addr_8db0_10;
}