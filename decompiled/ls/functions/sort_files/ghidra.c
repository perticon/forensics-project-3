void sort_files(void)

{
  uint uVar1;
  long *plVar2;
  undefined8 *puVar3;
  int iVar4;
  long *plVar5;
  long *plVar6;
  long lVar7;
  ulong uVar8;
  bool bVar9;
  
  uVar8 = cwd_n_used;
  if (sorted_file_alloc < (cwd_n_used >> 1) + cwd_n_used) {
    free(sorted_file);
    sorted_file = (long *)xnmalloc(uVar8);
    sorted_file_alloc = cwd_n_used * 3;
  }
  uVar8 = cwd_n_used;
  if (cwd_n_used == 0) {
    if (sort_type == 2) goto LAB_00108db9;
LAB_00108da2:
    if (((line_length != 0) && (format - 2U < 2)) && (uVar8 != 0)) goto LAB_00108e9b;
  }
  else {
    plVar2 = sorted_file + cwd_n_used;
    plVar5 = sorted_file;
    lVar7 = cwd_file;
    do {
      *plVar5 = lVar7;
      plVar5 = plVar5 + 1;
      lVar7 = lVar7 + 0xd0;
    } while (plVar5 != plVar2);
    if (sort_type != 2) goto LAB_00108da2;
LAB_00108e9b:
    uVar8 = 0;
    do {
      puVar3 = (undefined8 *)sorted_file[uVar8];
      lVar7 = puVar3[0x19];
      if (lVar7 == 0) {
        lVar7 = quote_name_width(*puVar3,filename_quoting_options,
                                 *(undefined4 *)((long)puVar3 + 0xc4));
      }
      uVar8 = uVar8 + 1;
      bVar9 = uVar8 < cwd_n_used;
      puVar3[0x19] = lVar7;
    } while (bVar9);
  }
  if (sort_type == 6) {
    return;
  }
LAB_00108db9:
  iVar4 = _setjmp((__jmp_buf_tag *)failed_strcoll);
  uVar8 = cwd_n_used;
  plVar2 = sorted_file;
  uVar1 = sort_type;
  if (iVar4 != 0) {
    if (sort_type == 4) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("sort_type != sort_version","src/ls.c",0x1008,"sort_files");
    }
    if (cwd_n_used != 0) {
      plVar5 = sorted_file + cwd_n_used;
      plVar6 = sorted_file;
      lVar7 = cwd_file;
      do {
        *plVar6 = lVar7;
        plVar6 = plVar6 + 1;
        lVar7 = lVar7 + 0xd0;
      } while (plVar5 != plVar6);
    }
    iVar4 = 1;
  }
  if (uVar1 == 5) {
    uVar1 = time_type + 5;
  }
  mpsort(plVar2,uVar8,
         *(undefined8 *)
          (sort_functions +
          ((ulong)directories_first +
          ((ulong)sort_reverse + ((long)iVar4 + (ulong)uVar1 * 2) * 2) * 2) * 8));
  return;
}