#include <stdint.h>

/* /tmp/tmprboi4m6a @ 0x67d0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmprboi4m6a @ 0x14e80 */
 
int64_t dbg_obstack_memory_used (obstack * h) {
    rdi = h;
    /* size_t _obstack_memory_used(obstack * h); */
    rax = *((rdi + 8));
    r8d = 0;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rdx = *(rax);
        rdx -= rax;
        rax = *((rax + 8));
        r8 += rdx;
    } while (rax != 0);
label_0:
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x14c40 */
 
void dbg_obstack_begin (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int _obstack_begin(obstack * h,size_t size,size_t alignment,void * (*)() chunkfun,void (*)() freefun); */
    *((rdi + 0x50)) &= 0xfe;
    *((rdi + 0x38)) = rcx;
    *((rdi + 0x40)) = r8;
    return void (*0x14ba0)() ();
}

/* /tmp/tmprboi4m6a @ 0x14df0 */
 
int64_t obstack_free (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 8));
    rbx = rdi;
    if (rsi != 0) {
        goto label_1;
    }
    goto label_2;
    do {
        rdi = *((rbx + 0x48));
        void (*rax)() ();
        *((rbx + 0x50)) |= 2;
        if (rbp == 0) {
            goto label_2;
        }
label_0:
        rsi = rbp;
label_1:
        if (rsi < r12) {
            rax = *(rsi);
            if (rax >= r12) {
                goto label_3;
            }
        }
        rbp = *((rsi + 8));
        rax = *((rbx + 0x40));
    } while ((*((rbx + 0x50)) & 1) != 0);
    rdi = rsi;
    rax = void (*rax)() ();
    *((rbx + 0x50)) |= 2;
    if (rbp != 0) {
        goto label_0;
    }
label_2:
    if (r12 != 0) {
        void (*0x4cbf)() ();
    }
    return rax;
label_3:
    *((rbx + 0x18)) = r12;
    *((rbx + 0x10)) = r12;
    *((rbx + 0x20)) = rax;
    *((rbx + 8)) = rsi;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x14db0 */
 
int64_t dbg_obstack_allocated_p (obstack * h, void * obj) {
    rdi = h;
    rsi = obj;
    /* int _obstack_allocated_p(obstack * h,void * obj); */
    rax = *((rdi + 8));
    if (rax == 0) {
        goto label_0;
    }
    do {
        if (rsi > rax) {
            if (*(rax) >= rsi) {
                goto label_1;
            }
        }
        rax = *((rax + 8));
    } while (rax != 0);
    eax = 0;
    return rax;
label_1:
    eax = 1;
    return rax;
label_0:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x14c60 */
 
void dbg_obstack_begin_1 (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int _obstack_begin_1(obstack * h,size_t size,size_t alignment,void * (*)() chunkfun,void (*)() freefun,void * arg); */
    *((rdi + 0x50)) |= 1;
    *((rdi + 0x38)) = rcx;
    *((rdi + 0x40)) = r8;
    *((rdi + 0x48)) = r9;
    return void (*0x14ba0)() ();
}

/* /tmp/tmprboi4m6a @ 0x14c80 */
 
int64_t dbg_obstack_newchunk (int64_t arg_8h, int64_t arg_10h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void _obstack_newchunk(obstack * h,size_t length); */
    ecx = 0;
    rbx = rdi;
    r13 = *((rdi + 0x18));
    r13 -= *((rdi + 0x10));
    rdx = *(rbx);
    rsi += r13;
    rax = r13;
    rbp = *((rdi + 8));
    cl = (rsi < 0) ? 1 : 0;
    rsi += *((rdi + 0x30));
    dil = (rsi < 0) ? 1 : 0;
    rax >>= 3;
    rax = rsi + rax + 0x64;
    if (rsi < rdx) {
        rsi = rdx;
    }
    if (rax >= rsi) {
        rsi = rax;
    }
    if (rcx != 0) {
        goto label_1;
    }
    edi = (int32_t) dil;
    if (rdi != 0) {
        goto label_1;
    }
    rax = *((rbx + 0x38));
    r14 = rsi;
    if ((*((rbx + 0x50)) & 1) != 0) {
        goto label_2;
    }
    rdi = rsi;
    rax = void (*rax)() ();
    r12 = rax;
    do {
        if (r12 == 0) {
            goto label_1;
        }
        rax = r12 + r14;
        *((rbx + 8)) = r12;
        *((r12 + 8)) = rbp;
        *((rbx + 0x20)) = rax;
        *(r12) = rax;
        rax = *((rbx + 0x30));
        r14 = r12 + rax + 0x10;
        rax = ~rax;
        r14 &= rax;
        memcpy (r14, *((rbx + 0x10)), r13);
        edx = *((rbx + 0x50));
        if ((dl & 2) == 0) {
            rax = *((rbx + 0x30));
            rcx = rbp + rax + 0x10;
            rax = ~rax;
            rax &= rcx;
            if (*((rbx + 0x10)) == rax) {
                goto label_3;
            }
        }
label_0:
        *((rbx + 0x10)) = r14;
        r14 += r13;
        *((rbx + 0x18)) = r14;
        *((rbx + 0x50)) &= 0xfd;
        return rax;
label_2:
        rdi = *((rbx + 0x48));
        rax = void (*rax)() ();
        r12 = rax;
    } while (1);
label_3:
    rax = *((rbp + 8));
    edx &= 1;
    *((r12 + 8)) = rax;
    rax = *((rbx + 0x40));
    if (edx != 0) {
        rdi = *((rbx + 0x48));
        rsi = rbp;
        rax = void (*rax)() ();
        goto label_0;
    }
    rdi = rbp;
    void (*rax)() ();
    goto label_0;
label_1:
    uint64_t (*obstack_alloc_failed_handler)() ();
}

/* /tmp/tmprboi4m6a @ 0x68c0 */
 
int64_t dbg_dev_ino_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t dev_ino_hash( const * x,size_t table_size); */
    rax = *(rdi);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x68d0 */
 
int64_t dbg_dev_ino_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool dev_ino_compare( const * x, const * y); */
    rdx = *(rsi);
    eax = 0;
    if (*(rdi) != rdx) {
        return eax;
    }
    rax = *((rsi + 8));
    al = (*((rdi + 8)) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x68f0 */
 
int32_t dbg_sighandler (int32_t sig) {
    rdi = sig;
    /* void sighandler(int sig); */
    eax = interrupt_signal;
    if (eax == 0) {
        *(obj.interrupt_signal) = edi;
    }
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x6910 */
 
int32_t dbg_get_funky_string (int64_t arg1, char * arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool get_funky_string(char ** dest,char const ** src,_Bool equals_end,size_t * output_count); */
    eax = edx;
    r11 = rdi;
    r10 = rsi;
    r12d = edx;
    rcx = *(rsi);
    rbx = *(rdi);
    edi = 1;
    edx = *(rcx);
    r8 = rdi - 1;
    rsi = rbx + 1;
    rbx = 0x0001a020;
    r9 = rsi - 1;
    if (dl == 0x5c) {
        goto label_8;
    }
    do {
        if (dl > 0x5c) {
            goto label_9;
        }
        if (dl == 0x3d) {
            goto label_10;
        }
        if (dl <= 0x3d) {
            goto label_11;
        }
label_0:
        rcx++;
label_3:
        *((rsi - 1)) = dl;
        rdi++;
        rsi++;
label_1:
        edx = *(rcx);
        r9 = rsi - 1;
        r8 = rdi - 1;
    } while (dl != 0x5c);
label_8:
    edx = *((rcx + 1));
    r13 = rcx + 2;
    if (dl == 0) {
        goto label_12;
    }
    r9d = rdx - 0x30;
    if (r9b > 0x48) {
        goto label_7;
    }
    r8d = (int32_t) r9b;
    r8 = *((rbx + r8*4));
    r8 += rbx;
    /* switch table (73 cases) at 0x1a020 */
    void (*r8)() ();
label_11:
    if (dl == 0) {
        goto label_13;
    }
    if (dl != 0x3a) {
        goto label_0;
    }
label_13:
    eax = 1;
label_2:
    *(r11) = r9;
    *(r10) = rcx;
    *(rbp) = r8;
    r12 = rbx;
    r13 = rbx;
    return eax;
label_9:
    if (dl != 0x5e) {
        goto label_0;
    }
    edx = *((rcx + 1));
    r13d = rdx - 0x40;
    if (r13b <= 0x3e) {
        edx &= 0x1f;
        rcx += 2;
        rdi++;
        rsi++;
        *((rsi - 2)) = dl;
        goto label_1;
        edx = 0xb;
label_7:
        rcx = r13;
label_6:
        *((rsi - 1)) = dl;
        rdi++;
        rsi++;
        goto label_1;
    }
    rcx++;
    if (dl == 0x3f) {
        goto label_14;
    }
    eax = 0;
    goto label_2;
label_10:
    if (r12b != 0) {
        goto label_2;
    }
    goto label_0;
label_14:
    edx = 0x7f;
    rdi++;
    rsi++;
    *((rsi - 2)) = dl;
    goto label_1;
    rcx = r13;
    edx = r9d;
    r8d = *(rcx);
    r9d = r8 - 0x30;
    if (r9b > 7) {
        goto label_3;
    }
    do {
        rcx++;
        edx = r8 + rdx*8 - 0x30;
        r8d = *(rcx);
        r9d = r8 - 0x30;
    } while (r9b <= 7);
    *((rsi - 1)) = dl;
    rdi++;
    rsi++;
    goto label_1;
    r8d = *((rcx + 2));
    r9 = rcx + 3;
    edx = 0;
    rcx = r9 - 1;
    if (r8b > 0x46) {
        goto label_15;
    }
label_4:
    if (r8b > 0x40) {
        goto label_16;
    }
    r13d = r8 - 0x30;
    if (r13b > 9) {
        goto label_3;
    }
    edx <<= 4;
    edx = r8 + rdx - 0x30;
label_5:
    r8d = *(r9);
    r9++;
    rcx = r9 - 1;
    if (r8b <= 0x46) {
        goto label_4;
    }
label_15:
    r13d = r8 - 0x61;
    if (r13b > 5) {
        goto label_3;
    }
    edx <<= 4;
    edx = r8 + rdx - 0x57;
    goto label_5;
    edx = 9;
    rcx = r13;
    goto label_6;
    edx = 0x7f;
    rcx = r13;
    goto label_6;
    edx = 0x1b;
    rcx = r13;
    goto label_6;
    edx = 0xc;
    rcx = r13;
    goto label_6;
    edx = 0xa;
    rcx = r13;
    goto label_6;
    edx = 0x20;
    rcx = r13;
    goto label_6;
    edx = 0xd;
    rcx = r13;
    goto label_6;
label_16:
    edx <<= 4;
    edx = r8 + rdx - 0x37;
    goto label_5;
label_12:
    rcx = r13;
    eax = 0;
    goto label_2;
    edx = 7;
    goto label_7;
    edx = 8;
    goto label_7;
}

/* /tmp/tmprboi4m6a @ 0x6b80 */
 
void dev_ino_free (void) {
    return void (*0x4630)() ();
}

/* /tmp/tmprboi4m6a @ 0x6b90 */
 
int64_t dbg_add_ignore_pattern (int64_t arg1) {
    rdi = arg1;
    /* void add_ignore_pattern(char const * pattern); */
    rbx = rdi;
    xmalloc (0x10);
    rdx = ignore_patterns;
    *(rax) = rbx;
    *((rax + 8)) = rdx;
    *(obj.ignore_patterns) = rax;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18150 */
 
uint64_t xmalloc (void) {
    rax = fcn_00004670 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x4670 */
 
void fcn_00004670 (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmprboi4m6a @ 0x186d0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0001e27c);
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4840 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmprboi4m6a @ 0x4b70 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmprboi4m6a @ 0x46e0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmprboi4m6a @ 0x4af0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmprboi4m6a @ 0x4770 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmprboi4m6a @ 0x6bc0 */
 
int64_t dbg_decode_line_length (void) {
    uintmax_t val;
    int64_t var_8h;
    /* ptrdiff_t decode_line_length(char const * spec); */
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, 0, rsp, 0x0001bb0a);
    if (eax == 0) {
        goto label_0;
    }
    al = (eax != 1) ? 1 : 0;
    eax = (int32_t) al;
    rax = -rax;
    do {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        rax = *(rsp);
        edx = 0;
        __asm ("cmovs rax, rdx");
    } while (1);
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x6c30 */
 
uint64_t dbg_is_colored (int64_t arg1) {
    rdi = arg1;
    /* _Bool is_colored(indicator_no type); */
    rax = obj_color_indicator;
    r8d = 0;
    rdi <<= 4;
    rax += rdi;
    rdx = *(rax);
    if (rdx != 0) {
        rcx = *((rax + 8));
        if (rdx == 1) {
            goto label_0;
        }
        r8d = 1;
        if (rdx == 2) {
            goto label_1;
        }
    }
    eax = r8d;
    return rax;
label_0:
    r8b = (*(rcx) != 0x30) ? 1 : 0;
    eax = r8d;
    return rax;
label_1:
    eax = *(rcx);
    eax -= 0x30;
    if (eax == 0) {
        eax = *((rcx + 1));
        eax -= 0x30;
    }
    r8b = (eax != 0) ? 1 : 0;
    eax = r8d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x6ca0 */
 
uint32_t dbg_stdout_isatty (signed char out_tty) {
    rax = out_tty;
    /* _Bool stdout_isatty(); */
    eax = *(obj.out_tty.12);
    if (al >= 0) {
        eax &= 1;
        return eax;
    }
    al = isatty (1);
    *(obj.out_tty.12) = al;
    eax &= 1;
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x4740 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmprboi4m6a @ 0x6cd0 */
 
int64_t dbg_abformat_init (void) {
    size_t width;
    char const *[2] pb;
    char[12][128] abmon;
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_18h;
    int64_t var_28h;
    uint32_t var_30h;
    uint32_t var_38h;
    int64_t var_40h;
    int64_t var_640h;
    int64_t var_648h;
    /* void abformat_init(); */
    esi = 0;
    rax = *(fs:0x28);
    *((rsp + 0x648)) = rax;
    eax = 0;
    rdi = rsp + 0x30;
    *((rsp + 0x18)) = rdi;
label_1:
    rax = obj_long_time_format;
    rax = *((rax + rsi));
    edx = *(rax);
    if (dl == 0) {
        goto label_10;
    }
    do {
        ecx = *((rax + 1));
        if (dl == 0x25) {
            goto label_11;
        }
label_0:
        edx = ecx;
label_3:
        rax++;
    } while (dl != 0);
label_10:
    eax = 0;
    goto label_12;
label_11:
    if (cl == 0x25) {
        goto label_13;
    }
    if (cl != 0x62) {
        goto label_0;
    }
label_12:
    *((rdi + rsi)) = rax;
    rsi += 8;
    if (rsi != 0x10) {
        goto label_1;
    }
    r14d = 0xc;
    if (*((rsp + 0x30)) == 0) {
        goto label_14;
    }
label_4:
    rax = rsp + 0x40;
    r15 = rsp + 0x28;
    *((rsp + 8)) = rax;
    rax = rsp + 0x640;
    *(rsp) = rax;
label_2:
    rbx = *((rsp + 8));
    r12d = 0x2000e;
    ebp = 0;
    do {
        *((rsp + 0x28)) = r14;
        rax = nl_langinfo (r12d);
        rdi = rax;
        r13 = rax;
        rax = strchr (rdi, 0x25);
        if (rax != 0) {
            goto label_5;
        }
        rax = ctype_b_loc ();
        edx = *(r13);
        rax = *(rax);
        r8d = *((rax + rdx*2));
        r8w >>= 0xb;
        r8d &= 1;
        rax = mbsalign (r13, rbx, 0x80, r15, r8, 0);
        if (rax > 0x7f) {
            goto label_5;
        }
        rax = *((rsp + 0x28));
        if (rbp < rax) {
        }
        r12d++;
        rbx -= 0xffffffffffffff80;
    } while (rbx != *(rsp));
    if (r14 <= rbp) {
        goto label_15;
    }
    r14 = rbp;
    goto label_2;
label_13:
    edx = *((rax + 2));
    rax++;
    goto label_3;
label_8:
    *(obj.use_abformat) = 1;
label_5:
    rax = *((rsp + 0x648));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_16;
    }
    return rax;
label_14:
    if (*((rsp + 0x38)) != 0) {
        goto label_4;
    }
    goto label_5;
label_15:
    ebp = 0;
    r12 = *(rsp);
    r13 = 0x0001e27c;
    *((rsp + 0x10)) = rbp;
label_9:
    rax = *((rsp + 0x10));
    rsi = obj_long_time_format;
    rbx = *((rsp + 8));
    rdi = rax * 0x600;
    rbp = *((rsi + rax*8));
    rsi = *((rsp + 0x18));
    r14 = *((rsi + rax*8));
    rax = obj_abformat;
    r15 = rax + rdi;
    goto label_17;
label_6:
    r9 = r14;
    r9 -= rbp;
    if (r9 > 0x80) {
        goto label_5;
    }
    rax = r14 + 2;
    edx = 1;
    rdi = r15;
    r8 = "%.*s%s%s";
    rcx = 0xffffffffffffffff;
    eax = 0;
    esi = 0x80;
    eax = snprintf_chk ();
label_7:
    if (eax > 0x7f) {
        goto label_5;
    }
    rbx -= 0xffffffffffffff80;
    r15 -= 0xffffffffffffff80;
    if (rbx == r12) {
        goto label_18;
    }
label_17:
    if (r14 != 0) {
        goto label_6;
    }
    eax = 0;
    snprintf (r15, 0x80, r13, rbp);
    goto label_7;
label_18:
    if (*((rsp + 0x10)) == 1) {
        goto label_8;
    }
    *((rsp + 0x10)) = 1;
    goto label_9;
label_16:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x6f40 */
 
uint64_t queue_directory (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r13d = edx;
    r12 = rsi;
    rax = xmalloc (0x20);
    rbx = rax;
    if (r12 != 0) {
        rax = xstrdup (r12);
        r12 = rax;
    }
    *((rbx + 8)) = r12;
    if (rbp != 0) {
        rax = xstrdup (rbp);
    }
    rax = pending_dirs;
    *(rbx) = rbp;
    *((rbx + 0x10)) = r13b;
    *((rbx + 0x18)) = rax;
    *(obj.pending_dirs) = rbx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x6fb0 */
 
void strcmp_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rsi = *(rsi);
    rdi = *(rdi);
    return void (*0x4660)() ();
}

/* /tmp/tmprboi4m6a @ 0x6fc0 */
 
void rev_strcmp_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r8 = rsi;
    rsi = *(rdi);
    rdi = *(r8);
    return void (*0x4660)() ();
}

/* /tmp/tmprboi4m6a @ 0x6fe0 */
 
int64_t dbg_do_statx (int64_t arg3, int64_t arg4, int64_t arg5) {
    statx stx;
    int64_t var_1h;
    int64_t var_4h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_84h;
    int64_t var_88h;
    int64_t var_8ch;
    int64_t var_108h;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int do_statx(int fd,char const * name,stat * st,int flags,unsigned int mask); */
    ch |= 8;
    rbx = rdx;
    edx = ecx;
    ecx = ebp;
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    r8 = rsp;
    eax = statx ();
    while (ebp == 0) {
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        ecx = *((rsp + 0x88));
        edx = *((rsp + 0x8c));
        rsi = 0xfffff00000000000;
        rdi = rcx;
        rcx <<= 0x20;
        rdi <<= 8;
        rcx &= rsi;
        edi &= 0xfff00;
        rcx |= rdi;
        edi = (int32_t) dl;
        rdx <<= 0xc;
        rcx |= rdi;
        rdi = 0xffffff00000;
        rdx &= rdi;
        rdx |= rcx;
        ecx = *((rsp + 0x10));
        *(rbx) = rdx;
        rdx = *((rsp + 0x20));
        *((rbx + 0x10)) = rcx;
        ecx = *((rsp + 0x80));
        *((rbx + 8)) = rdx;
        edx = *((rsp + 0x1c));
        r8 = rcx;
        rcx <<= 0x20;
        *((rbx + 0x18)) = edx;
        rdx = *((rsp + 0x14));
        r8 <<= 8;
        rcx &= rsi;
        r8d &= 0xfff00;
        *((rbx + 0x1c)) = rdx;
        edx = *((rsp + 0x84));
        rcx |= r8;
        esi = (int32_t) dl;
        rdx <<= 0xc;
        rcx |= rsi;
        rdx &= rdi;
        esi = *((rsp + 4));
        rdx |= rcx;
        ebp &= 0x800;
        *((rbx + 0x28)) = rdx;
        rdx = *((rsp + 0x28));
        *((rbx + 0x38)) = rsi;
        esi = *((rsp + 0x48));
        *((rbx + 0x30)) = rdx;
        rdx = *((rsp + 0x30));
        *((rbx + 0x50)) = rsi;
        esi = *((rsp + 0x78));
        *((rbx + 0x40)) = rdx;
        rdx = *((rsp + 0x40));
        *((rbx + 0x60)) = rsi;
        esi = *((rsp + 0x68));
        *((rbx + 0x48)) = rdx;
        rdx = *((rsp + 0x70));
        *((rbx + 0x70)) = rsi;
        *((rbx + 0x58)) = rdx;
        rdx = *((rsp + 0x60));
        *((rbx + 0x68)) = rdx;
    }
    if ((*((rsp + 1)) & 8) != 0) {
        rdx = *((rsp + 0x50));
        edi = *((rsp + 0x58));
        *((rbx + 0x58)) = rdx;
        *((rbx + 0x60)) = rdi;
        goto label_0;
    }
    *((rbx + 0x60)) = 0xffffffffffffffff;
    *((rbx + 0x58)) = 0xffffffffffffffff;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x7180 */
 
uint64_t cmp_extension (int64_t arg2, int64_t arg3, char ** s) {
    rsi = arg2;
    rdx = arg3;
    rdi = s;
    r13 = rdi;
    r12 = rsi;
    rbx = rdx;
    rax = strrchr (*(rdi), 0x2e);
    rax = strrchr (*(r12), 0x2e);
    rsi = rax;
    rax = 0x0001bb0a;
    if (rsi == 0) {
        rsi = rax;
    }
    if (rbp == 0) {
    }
    rdi = rbp;
    eax = void (*rbx)() ();
    if (eax == 0) {
        rsi = *(r12);
        rdi = *(r13);
        rax = rbx;
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x7200 */
 
void rev_strcmp_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r8 = rdi;
    rdx = *(reloc.strcmp);
    rdi = rsi;
    rsi = r8;
    return cmp_extension ();
}

/* /tmp/tmprboi4m6a @ 0x7220 */
 
void strcmp_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rdx = *(reloc.strcmp);
    return cmp_extension ();
}

/* /tmp/tmprboi4m6a @ 0x7230 */
 
uint64_t dbg_file_escape (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * file_escape(char const * str,_Bool path); */
    r12d = esi;
    rbx = rdi;
    strlen (rdi);
    rax = xnmalloc (3, rax + 1);
    r13 = rax;
    eax = *(rbx);
    if (al == 0) {
        goto label_2;
    }
    r14 = obj_RFC3986;
    r15 = "%%%02x";
    while (*((r14 + rdx)) != 0) {
        *(rbp) = al;
        rbp++;
label_1:
        eax = *(rbx);
        if (al == 0) {
            goto label_2;
        }
label_0:
        rbx++;
        if (al == 0x2f) {
            if (r12b != 0) {
                goto label_3;
            }
        }
        edx = (int32_t) al;
        r8d = (int32_t) al;
    }
    rdi = rbp;
    rcx = r15;
    rdx = 0xffffffffffffffff;
    eax = 0;
    esi = 1;
    rbp += 3;
    sprintf_chk ();
    eax = *(rbx);
    if (al != 0) {
        goto label_0;
    }
label_2:
    *(rbp) = 0;
    rax = r13;
    return rax;
label_3:
    *(rbp) = 0x2f;
    rbp++;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x72f0 */
 
int64_t dbg_indent (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void indent(size_t from,size_t to); */
    if (rdi >= rsi) {
        goto label_3;
    }
    r12 = rsi;
    rbx = rdi;
    while (rcx != 0) {
        rax = r12;
        edx = 0;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        edx = 0;
        r9 = rax;
        rax = rbp;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        if (r9 <= rax) {
            goto label_4;
        }
        if (rsi >= r8) {
            goto label_5;
        }
        rax = rsi + 1;
        *((rdi + 0x28)) = rax;
        *(rsi) = 9;
label_2:
        rax = rbx;
        edx = 0;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        rcx += rbx;
        rbx = rcx;
        rbx -= rdx;
        if (rbx >= r12) {
            goto label_6;
        }
label_0:
        rdi = stdout;
        rcx = tabsize;
        rbp = rbx + 1;
        rsi = *((rdi + 0x28));
        r8 = *((rdi + 0x30));
    }
label_4:
    if (rsi >= r8) {
        goto label_7;
    }
    rax = rsi + 1;
    *((rdi + 0x28)) = rax;
    *(rsi) = 0x20;
label_1:
    rbx = rbp;
    if (rbx < r12) {
        goto label_0;
    }
label_6:
    r12 = rbx;
    return rax;
label_7:
    esi = 0x20;
    overflow ();
    goto label_1;
label_5:
    esi = 9;
    overflow ();
    rcx = tabsize;
    goto label_2;
label_3:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x73c0 */
 
void dbg_dired_outbuf (int64_t arg2) {
    rsi = arg2;
    /* void dired_outbuf(char const * s,size_t s_len); */
    *(obj.dired_pos) += rsi;
    rcx = stdout;
    rdx = rsi;
    esi = 1;
    return fwrite_unlocked ();
}

/* /tmp/tmprboi4m6a @ 0x73e0 */
 
int64_t dbg_dired_dump_obstack (int64_t arg2) {
    rsi = arg2;
    /* void dired_dump_obstack(char const * prefix,obstack * os); */
    rax = *((rsi + 0x18));
    rbx = *((rsi + 0x10));
    rbp -= rbx;
    if (rbp <= 7) {
        goto label_1;
    }
    if (rax == rbx) {
        goto label_2;
    }
label_0:
    rdx = *((rsi + 0x30));
    r12 = " %ld";
    rax += rdx;
    rdx = ~rdx;
    rax &= rdx;
    rdx = *((rsi + 0x20));
    r8 = rax;
    r8 -= *((rsi + 8));
    rcx = rdx;
    rcx -= *((rsi + 8));
    if (r8 <= rcx) {
        rdx = rax;
    }
    rbp &= 0xfffffffffffffff8;
    rbp += rbx;
    *((rsi + 0x18)) = rdx;
    *((rsi + 0x10)) = rdx;
    rsi = stdout;
    rax = fputs_unlocked ();
    do {
        rdx = *(rbx);
        rsi = r12;
        edi = 1;
        eax = 0;
        rbx += 8;
        printf_chk ();
    } while (rbx != rbp);
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
label_1:
        return rax;
label_2:
        *((rsi + 0x50)) |= 2;
        goto label_0;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmprboi4m6a @ 0x74b0 */
 
int32_t stophandler (void) {
    eax = interrupt_signal;
    if (eax == 0) {
        eax = stop_signal_count;
        eax++;
        *(obj.stop_signal_count) = eax;
    }
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x74d0 */
 
uint64_t dbg_file_failure (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void file_failure(_Bool serious,char const * message,char const * file); */
    r12 = rsi;
    rsi = rdx;
    ebx = edi;
    edi = 4;
    rax = quotearg_style ();
    r13 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    if (bl == 0) {
        goto label_0;
    }
    *(obj.exit_status) = 2;
    do {
        return rax;
label_0:
        eax = exit_status;
    } while (eax != 0);
    *(obj.exit_status) = 1;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x16b20 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x4cde)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x16460 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x4cc9)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                rdi = r14;
                *((rsp + 0x10)) = rsi;
                fcn_00004630 ();
                rsi = *((rsp + 0x10));
            }
            rdi = *((rsp + 0x10));
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc ();
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x4870 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmprboi4m6a @ 0x46f0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmprboi4m6a @ 0x7540 */
 
int32_t get_type_indicator (uint32_t arg2, uint32_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (dil == 0) {
        goto label_1;
    }
    ecx = esi;
    ecx &= 0xf000;
    if (ecx != 0x8000) {
        goto label_2;
    }
    eax = 0;
    if (*(obj.indicator_style) == 3) {
        goto label_3;
    }
    do {
label_0:
        return eax;
label_2:
        eax = 0x2f;
        if (ecx == sym._init) {
            goto label_4;
        }
        eax = 0;
        cl = (ecx == 0xa000) ? 1 : 0;
        if (*(obj.indicator_style) != 1) {
            goto label_5;
        }
        return eax;
label_1:
        eax = 0;
    } while (edx == 5);
    if (edx == 3) {
        goto label_6;
    }
    if (edx == 9) {
        goto label_6;
    }
    cl = (edx == 6) ? 1 : 0;
    if (*(obj.indicator_style) == 1) {
        goto label_7;
    }
label_5:
    eax = 0x40;
    if (cl != 0) {
        goto label_0;
    }
    if (dil != 0) {
        esi &= 0xf000;
        eax = 0x7c;
        if (esi == 0x1000) {
            goto label_8;
        }
        al = (esi == 0xc000) ? 1 : 0;
        goto label_9;
label_4:
        return eax;
label_3:
        esi &= 0x49;
        esi = -esi;
        al -= al;
        eax &= 0x2a;
        return eax;
label_6:
        eax = 0x2f;
        return eax;
    }
    eax = 0x7c;
    if (edx != 1) {
        al = (edx == 7) ? 1 : 0;
label_9:
        eax = -eax;
        eax &= 0x3d;
        return eax;
label_7:
        return eax;
    }
    return eax;
label_8:
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x7640 */
 
int64_t signal_setup (void) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_28h;
    int64_t var_38h;
    int64_t var_48h;
    int64_t var_58h;
    int64_t var_68h;
    int64_t var_78h;
    int64_t var_88h;
    int64_t var_98h;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    if (dil != 0) {
        goto label_2;
    }
    rbx = 0x0001a664;
    r12 = obj_caught_signals;
    r13 = rbx + 0x2c;
    while (eax == 0) {
        if (rbx == r13) {
            goto label_1;
        }
label_0:
        ebp = *(rbx);
        rbx += 4;
        esi = ebp;
        rdi = r12;
        eax = sigismember ();
    }
    esi = 0;
    signal (ebp);
    if (rbx != r13) {
        goto label_0;
    }
label_1:
    rax = *((rsp + 0x98));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_3;
    }
    return rax;
label_2:
    r12 = obj_caught_signals;
    rbx = 0x0001a664;
    r15d = 0x14;
    r14 = rsp;
    rdi = r12;
    r13 = rbx + 0x2c;
    sigemptyset ();
    while (rbp != r13) {
        r15d = *(rbp);
        rbp += 4;
        sigaction (r15d, 0, r14);
        if (*(rsp) != 1) {
            esi = r15d;
            rdi = r12;
            sigaddset ();
        }
    }
    __asm ("movdqa xmm0, xmmword [obj.caught_signals]");
    __asm ("movdqa xmm1, xmmword [0x00026230]");
    *((rsp + 0x88)) = 0x10000000;
    __asm ("movdqa xmm2, xmmword [0x00026240]");
    __asm ("movdqa xmm3, xmmword [0x00026250]");
    r15 = sym_stophandler;
    __asm ("movdqa xmm4, xmmword [0x00026260]");
    __asm ("movdqa xmm5, xmmword [0x00026270]");
    __asm ("movups xmmword [rsp + 8], xmm0");
    __asm ("movdqa xmm6, xmmword [0x00026280]");
    __asm ("movdqa xmm7, xmmword [0x00026290]");
    __asm ("movups xmmword [rsp + 0x18], xmm1");
    __asm ("movups xmmword [rsp + 0x28], xmm2");
    __asm ("movups xmmword [rsp + 0x38], xmm3");
    __asm ("movups xmmword [rsp + 0x48], xmm4");
    __asm ("movups xmmword [rsp + 0x58], xmm5");
    __asm ("movups xmmword [rsp + 0x68], xmm6");
    __asm ("movups xmmword [rsp + 0x78], xmm7");
    while (rbx != r13) {
        ebp = *(rbx);
        rbx += 4;
        esi = ebp;
        rdi = r12;
        eax = sigismember ();
        if (eax != 0) {
            rax = dbg_sighandler;
            rsi = r14;
            edi = ebp;
            if (ebp == 0x14) {
                rax = r15;
            }
            *(rsp) = rax;
            sigaction (rdi, rsi, 0);
        }
    }
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x7810 */
 
uint32_t dbg_put_indicator (int64_t arg1) {
    rdi = arg1;
    do {
        /* void put_indicator(bin_str const * ind); */
label_2:
        rbx = rdi;
        if (*(obj.used_color) != 0) {
label_0:
            rdi = *((rbx + 8));
            rsi = *(rbx);
            edx = 1;
            rcx = stdout;
            void (*0x4ae0)() ();
        }
        edi = 1;
        *(obj.used_color) = 1;
        eax = tcgetpgrp ();
        if (eax >= 0) {
            goto label_3;
        }
label_1:
        if (*(0x00025088) == 0) {
            goto label_4;
        }
        put_indicator (0x00025080);
    } while (1);
    goto label_0;
label_3:
    edi = 1;
    signal_setup ();
    goto label_1;
label_4:
    put_indicator (obj.color_indicator);
    goto label_2;
    put_indicator (0x00025090);
    goto label_2;
    put_indicator (0x00025070);
    goto label_2;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x78b0 */
 
int64_t quote_name_buf_constprop_0 (uint32_t arg_20h, int64_t arg_38h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5, int64_t arg6) {
    void * var_8h;
    int64_t var_10h;
    void * s1;
    uint32_t var_20h;
    int64_t var_2fh;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r12d = ecx;
    rbx = rdx;
    *((rsp + 0x30)) = rdi;
    *((rsp + 0x20)) = r8;
    *((rsp + 0x38)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    rax = *(rdi);
    *((rsp + 0x18)) = rax;
    eax = get_quoting_style (rdx);
    al = (eax <= 2) ? 1 : 0;
    al &= *(obj.qmark_funny_chars);
    *((rsp + 0x2f)) = al;
    if (al != 0) {
        goto label_14;
    }
    r13d = 0;
    if (r12d != 0) {
label_13:
        rax = quotearg_buffer (*((rsp + 0x18)), 0x2000, rbp, 0xffffffffffffffff, rbx);
        r12 = rax;
        if (rax > 0x1fff) {
            goto label_15;
        }
label_12:
        rax = *((rsp + 0x18));
        *((rsp + 0x2f)) = 1;
        eax = *(rax);
        if (*(rbp) == al) {
            rax = strlen (rbp);
            rsp + 0x2f = (r12 != rax) ? 1 : 0;
        }
        if (r13d == 0) {
            goto label_16;
        }
        rbx = r12;
        goto label_17;
    }
    rax = strlen (rbp);
    *((rsp + 0x18)) = rbp;
    r12 = rax;
label_16:
    if (*((rsp + 0x20)) == 0) {
        goto label_18;
    }
    rax = ctype_get_mb_cur_max ();
    if (rax > 1) {
        goto label_19;
    }
    rbx = *((rsp + 0x18));
    rbp = rbx + r12;
    if (rbp <= rbx) {
        goto label_20;
    }
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbx;
    ebx = 0;
    do {
        edx = *(rax);
        edx = *((rcx + rdx*2));
        dx &= sym._init;
        rbx -= 0xffffffffffffffff;
        rax++;
    } while (rbp != rax);
label_9:
    if (*(obj.align_variable_outer_quotes) != 0) {
        goto label_4;
    }
    rax = *((rsp + 0x38));
    *(rax) = 0;
label_1:
    rax = *((rsp + 0x20));
    *(rax) = rbx;
    do {
label_2:
        rax = *((rsp + 0x30));
        rbx = *((rsp + 0x18));
        *(rax) = rbx;
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_21;
        }
        rax = r12;
        return rax;
label_18:
        if (*(obj.align_variable_outer_quotes) != 0) {
            goto label_22;
        }
        rax = *((rsp + 0x38));
        *(rax) = 0;
    } while (1);
label_14:
    if (r12d != 0) {
        goto label_23;
    }
    rax = strlen (rbp);
    rbx = rax;
    r12 = rax + 1;
    if (rax > 0x1fff) {
        goto label_24;
    }
label_11:
    memcpy (*((rsp + 0x18)), rbp, r12);
    *((rsp + 0x2f)) = 0;
label_17:
    rax = ctype_get_mb_cur_max ();
    rcx = *((rsp + 0x18));
    rsi = rcx + rbx;
    *((rsp + 8)) = rsi;
    if (rax <= 1) {
        goto label_25;
    }
    if (rcx >= rsi) {
        goto label_26;
    }
    r14 = rcx;
    r12 = rcx;
    rbp = rsp + 0x50;
    ebx = 0;
label_6:
    eax = *(r12);
    if (al > 0x5f) {
        goto label_27;
    }
    if (al > 0x40) {
        goto label_8;
    }
    if (al > 0x23) {
        goto label_28;
    }
    if (al > 0x1f) {
        goto label_8;
    }
label_5:
    *((rsp + 0x50)) = 0;
    r15 = rsp + 0x4c;
    while (eax < 0) {
        *(r14) = 0x3f;
        r14 = *((rsp + 0x10));
        r12 += r13;
        rbx++;
label_0:
        eax = mbsinit (rbp);
        if (eax != 0) {
            goto label_10;
        }
        rdx -= r12;
        rax = rpl_mbrtowc (r15, r12, *((rsp + 8)), rbp);
        rsi = r14 + 1;
        *((rsp + 0x10)) = rsi;
        if (rax == -1) {
            goto label_29;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_30;
        }
        edx = 1;
        edi = *((rsp + 0x4c));
        if (rax != 0) {
            rdx = rax;
        }
        r13 = rdx;
        eax = wcwidth ();
    }
    edx = 0;
    do {
        edi = *((r12 + rdx));
        *((r14 + rdx)) = dil;
        rdx++;
    } while (r13 != rdx);
    rax = (int64_t) eax;
    r12 += r13;
    r14 += r13;
    rbx += rax;
    goto label_0;
label_22:
    ebx = 0;
label_4:
    eax = *((rsp + 0x2f));
    eax ^= 1;
    al &= *(obj.cwd_some_quoted);
label_3:
    rcx = *((rsp + 0x38));
    *(rcx) = al;
    if (*((rsp + 0x20)) != 0) {
        goto label_1;
    }
    goto label_2;
label_25:
    r15 = *((rsp + 0x18));
    rax = *((rsp + 8));
    if (r15 >= rax) {
        goto label_31;
    }
    rax = ctype_b_loc ();
    rdx = r15;
    do {
        esi = *(rdx);
        rcx = *(rax);
        if ((*((rcx + rsi*2 + 1)) & 0x40) == 0) {
            *(rdx) = 0x3f;
        }
        rdx++;
    } while (rdx != *((rsp + 8)));
label_31:
    r12 = rbx;
label_7:
    eax = *(obj.align_variable_outer_quotes);
    if (al == 0) {
        goto label_3;
    }
    goto label_4;
label_28:
    edx = rax - 0x25;
    if (dl > 0x1a) {
        goto label_5;
    }
label_8:
    *(r14) = al;
    r12++;
    rbx++;
    r14++;
label_10:
    if (r12 < *((rsp + 8))) {
        goto label_6;
    }
    r14 -= *((rsp + 0x18));
    r12 = r14;
    goto label_7;
label_27:
    edx = rax - 0x61;
    if (dl <= 0x1d) {
        goto label_8;
    }
    goto label_5;
label_19:
    edx = 0;
    eax = mbsnwidth (*((rsp + 0x18)), r12);
    rbx = (int64_t) eax;
    goto label_9;
label_29:
    r12++;
    rbx++;
    do {
        *(r14) = 0x3f;
        r14 = *((rsp + 0x10));
        goto label_10;
label_30:
        r12 = *((rsp + 8));
        rbx++;
    } while (1);
label_24:
    rax = xmalloc (r12);
    *((rsp + 0x18)) = rax;
    goto label_11;
label_15:
    r15 = rax + 1;
    rax = xmalloc (r15);
    rdi = rax;
    *((rsp + 0x18)) = rax;
    quotearg_buffer (rdi, r15, rbp, 0xffffffffffffffff, rbx);
    goto label_12;
label_20:
    ebx = 0;
    goto label_9;
label_26:
    r12d = 0;
    ebx = 0;
    goto label_7;
label_21:
    stack_chk_fail ();
label_23:
    r13d = 1;
    goto label_13;
}

/* /tmp/tmprboi4m6a @ 0x7d00 */
 
int64_t dbg_quote_name_width (int64_t arg1, int64_t arg2, int64_t arg3) {
    _Bool pad;
    char * buf;
    size_t width;
    char[8192] smallbuf;
    int64_t var_2028h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t quote_name_width(char const * name,quoting_options const * options,int needs_general_quoting); */
    rbx = rdi;
    rbp = rsp + 0x20;
    rdi = rsp + 0x10;
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x2028)) = rax;
    eax = 0;
    rdx = rsi;
    r9 = rsp + 0xf;
    r8 = rsp + 0x18;
    rsi = rbx;
    *((rsp + 0x10)) = rbp;
    quote_name_buf_constprop_0 ();
    rdi = *((rsp + 0x10));
    if (rdi != rbp) {
        if (rdi == rbx) {
            goto label_0;
        }
        fcn_00004630 ();
    }
label_0:
    eax = *((rsp + 0xf));
    rax += *((rsp + 0x18));
    rdx = *((rsp + 0x2028));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x7da0 */
 
int64_t dbg_length_of_file_name_and_frills (int64_t arg_30h, int64_t arg_58h, int64_t arg_a8h, char * canary, uint32_t arg_b8h, int64_t arg_c4h, int64_t arg_c8h, int64_t arg1, int64_t arg3) {
    char[652] buf;
    int64_t var_298h;
    rdi = arg1;
    rdx = arg3;
    /* size_t length_of_file_name_and_frills(fileinfo const * f); */
    rax = *(fs:0x28);
    *((rsp + 0x298)) = rax;
    eax = 0;
    if (*(obj.print_inode) == 0) {
        goto label_7;
    }
    if (*(obj.format) == 4) {
        goto label_8;
    }
    rbx = *(obj.inode_number_width);
    rbx++;
    if (*(obj.print_block_size) == 0) {
        goto label_9;
    }
label_0:
    rax = *(obj.block_size_width);
    rax++;
label_1:
    rbx += rax;
    do {
        if (*(obj.print_scontext) != 0) {
            goto label_10;
        }
label_4:
        rax = *((rbp + 0xc8));
        if (rax == 0) {
            goto label_11;
        }
label_2:
        r8 = rbx + rax;
        eax = indicator_style;
        if (eax != 0) {
            edx = *((rbp + 0xa8));
            esi = *((rbp + 0x30));
            edi = *((rbp + 0xb8));
            al = get_type_indicator ();
            r8 -= 0xffffffffffffffff;
        }
        rax = *((rsp + 0x298));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        rax = r8;
        return rax;
label_7:
        ebx = 0;
label_5:
    } while (*(obj.print_block_size) == 0);
    if (*(obj.format) != 4) {
        goto label_0;
    }
    eax = 2;
    if (*((rbp + 0xb8)) == 0) {
        goto label_1;
    }
    rax = human_readable (*((rbp + 0x58)), rsp, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    rax = strlen (rax);
    rax++;
    goto label_1;
label_10:
    if (*(obj.format) == 4) {
        goto label_13;
    }
label_3:
    rax = *(obj.scontext_width);
    rax++;
label_6:
    rbx += rax;
    rax = *((rbp + 0xc8));
    if (rax != 0) {
        goto label_2;
    }
label_11:
    quote_name_width (*(rbp), *(obj.filename_quoting_options), *((rbp + 0xc4)));
    goto label_2;
label_9:
    if (*(obj.print_scontext) != 0) {
        goto label_3;
    }
    goto label_4;
label_8:
    rax = umaxtostr (*((rdi + 0x20)), rsp, rdx);
    strlen (rax);
    rbx = rax + 1;
    goto label_5;
label_13:
    rax = strlen (*((rbp + 0xb0)));
    rax++;
    goto label_6;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x7f70 */
 
int64_t dbg_calculate_columns (int64_t arg1) {
    rdi = arg1;
    /* size_t calculate_columns(_Bool by_columns); */
    rsi = max_idx;
    rax = column_info_alloc.3;
    r12 = cwd_n_used;
    if (rsi == 0) {
        goto label_9;
    }
    if (rsi < r12) {
        goto label_10;
    }
    if (r12 <= rax) {
        goto label_11;
    }
    rax = rsi;
    rax >>= 1;
    if (rax <= r12) {
        goto label_12;
    }
label_3:
    rdi = column_info;
    edx = 0x30;
    rsi = r12;
    rbx = r12 + r12;
    rax = xreallocarray ();
    *(obj.column_info) = rax;
label_4:
    rdx = column_info_alloc.3;
    rax = rbx;
    ecx = 0;
    rax -= rdx;
    rdx++;
    rdx += rbx;
    cl = (rdx < 0) ? 1 : 0;
    rdx:rax = rax * rdx;
    __asm ("seto dl");
    edx = (int32_t) dl;
    if (rcx != 0) {
        goto label_13;
    }
    if (rdx != 0) {
        goto label_13;
    }
    rax >>= 1;
    rax = xnmalloc (rax, 8);
    rdx = column_info_alloc.3;
    if (rbx <= rdx) {
        goto label_14;
    }
    rdi = column_info;
    rdx = rdx*8 + 8;
    rsi = rbx*8 + 8;
    do {
        rcx = rdx * 3;
        *((rdi + rcx - 8)) = rax;
        rax += rdx;
        rdx += 8;
    } while (rsi != rdx);
label_14:
    *(obj.column_info_alloc.3) = rbx;
    r9 = cwd_n_used;
    if (r12 == 0) {
        goto label_15;
    }
label_6:
    r8 = column_info;
    esi = 3;
    edi = 0;
label_0:
    rax = *((r8 + rsi*8 - 8));
    *((r8 + rsi*8 - 0x18)) = 1;
    *((r8 + rsi*8 - 0x10)) = rsi;
    rcx = rax + rdi*8;
    do {
        rdx = rax;
        *(rax) = 3;
        rax += 8;
    } while (rcx != rdx);
    rdi++;
    rsi += 3;
    if (rdi < r12) {
        goto label_0;
    }
    if (r9 == 0) {
        goto label_16;
    }
label_7:
    ebx = 0;
label_5:
    rax = sorted_file;
    rax = length_of_file_name_and_frills (*((rax + rbx*8)), rsi, rdx, rcx, r8, r9);
    r10 = cwd_n_used;
    r9 = rax;
    if (r12 == 0) {
        goto label_17;
    }
    r11 = line_length;
    rsi = column_info;
    ecx = 0;
    goto label_18;
label_1:
    rax = r10 + rcx - 1;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    edx = 0;
    r8 = rax;
    rax = rbx;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    r8 = rax;
label_2:
    rdx = *((rsi + 0x10));
    eax = 0;
    al = (r8 != rdi) ? 1 : 0;
    rdx = rdx + r8*8;
    rax = r9 + rax*2;
    rdi = *(rdx);
    if (rdi >= rax) {
        goto label_19;
    }
    r8 = rax;
    r8 -= rdi;
    *((rsi + 8)) += r8;
    *(rdx) = rax;
    rsi = (*((rsi + 8)) < r11) ? 1 : 0;
    do {
label_19:
        rsi += 0x18;
        if (r12 == rcx) {
            goto label_17;
        }
label_18:
        rdi = rcx;
        rcx++;
    } while (*(rsi) == 0);
    if (bpl != 0) {
        goto label_1;
    }
    rax = rbx;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    r8 = rdx;
    goto label_2;
label_10:
    if (rsi <= rax) {
        goto label_20;
    }
    rax = rsi;
    r12 = rsi;
    rax >>= 1;
    if (rax > r12) {
        goto label_3;
    }
label_12:
    rdi = column_info;
    edx = 0x18;
    rax = xreallocarray ();
    rbx = max_idx;
    *(obj.column_info) = rax;
    goto label_4;
label_17:
    rbx++;
    if (rbx < r10) {
        goto label_5;
    }
label_16:
    if (r12 <= 1) {
        goto label_8;
    }
    rdx = column_info;
    rax = r12 * 3;
    rax = rdx + rax*8 - 0x18;
    while (*(rax) == 0) {
        r12--;
        rax -= 0x18;
        if (r12 == 1) {
            goto label_8;
        }
    }
label_8:
    rax = r12;
    return rax;
label_9:
    if (r12 > rax) {
        goto label_3;
    }
label_11:
    r9 = r12;
    if (r12 != 0) {
        goto label_6;
    }
label_15:
    if (r9 != 0) {
        goto label_7;
    }
    goto label_8;
label_20:
    r9 = r12;
    r12 = rsi;
    goto label_6;
label_13:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x8230 */
 
uint32_t rev_xstrcoll_version (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbp = *(rsi);
    r12 = *(rdi);
    eax = filevercmp (rbp, r12, rdx, rcx);
    if (eax == 0) {
        rsi = r12;
        rdi = rbp;
        void (*0x4660)() ();
    }
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x8270 */
 
int64_t dbg_clear_files (int64_t arg_8h, int64_t arg_10h) {
    /* void clear_files(); */
    rax = cwd_n_used;
    if (rax == 0) {
        goto label_0;
    }
    rbx = sorted_file;
    r12 = rbx + rax*8;
    do {
        rbp = *(rbx);
        rbx += 8;
        rdi = *(rbp);
        fcn_00004630 ();
        rdi = *((rbp + 8));
        fcn_00004630 ();
        rdi = *((rbp + 0x10));
        fcn_00004630 ();
    } while (r12 != rbx);
label_0:
    *(obj.cwd_some_quoted) = 0;
    *(obj.cwd_n_used) = 0;
    *(obj.any_has_acl) = 0;
    *(obj.inode_number_width) = 0;
    *(obj.block_size_width) = 0;
    *(obj.nlink_width) = 0;
    *(obj.owner_width) = 0;
    *(obj.group_width) = 0;
    *(obj.author_width) = 0;
    *(obj.scontext_width) = 0;
    *(obj.major_device_number_width) = 0;
    *(obj.minor_device_number_width) = 0;
    *(obj.file_size_width) = 0;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8340 */
 
uint32_t xstrcoll_version (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbp = *(rdi);
    r12 = *(rsi);
    eax = filevercmp (rbp, r12, rdx, rcx);
    if (eax == 0) {
        rsi = r12;
        rdi = rbp;
        void (*0x4660)() ();
    }
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x8380 */
 
int64_t rev_strcmp_size (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rdi + 0x48));
    rdx = rsi;
    al = (*((rsi + 0x48)) < rax) ? 1 : 0;
    cl = (*((rsi + 0x48)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    if (eax == 0) {
        rsi = *(rdi);
        rdi = *(rdx);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x83c0 */
 
int64_t rev_strcmp_btime (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rcx = *((rdi + 0x70));
    rax = rsi;
    cl = (*((rsi + 0x70)) < rcx) ? 1 : 0;
    dl = (*((rsi + 0x70)) > rcx) ? 1 : 0;
    rsi = *((rdi + 0x78));
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    ecx -= edx;
    edx = 0;
    sil = (*((rax + 0x78)) > rsi) ? 1 : 0;
    dl = (*((rax + 0x78)) < rsi) ? 1 : 0;
    esi = (int32_t) sil;
    edx -= esi;
    r8d = rdx + rcx*2;
    if (r8d == 0) {
        rsi = *(rdi);
        rdi = *(rax);
        void (*0x4660)() ();
    }
    eax = r8d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8420 */
 
int64_t rev_strcmp_mtime (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rcx = *((rdi + 0x70));
    rax = rsi;
    cl = (*((rsi + 0x70)) < rcx) ? 1 : 0;
    dl = (*((rsi + 0x70)) > rcx) ? 1 : 0;
    rsi = *((rdi + 0x78));
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    ecx -= edx;
    edx = 0;
    sil = (*((rax + 0x78)) > rsi) ? 1 : 0;
    dl = (*((rax + 0x78)) < rsi) ? 1 : 0;
    esi = (int32_t) sil;
    edx -= esi;
    r8d = rdx + rcx*2;
    if (r8d == 0) {
        rsi = *(rdi);
        rdi = *(rax);
        void (*0x4660)() ();
    }
    eax = r8d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8480 */
 
int64_t rev_strcmp_atime (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rcx = *((rdi + 0x60));
    rax = rsi;
    cl = (*((rsi + 0x60)) < rcx) ? 1 : 0;
    dl = (*((rsi + 0x60)) > rcx) ? 1 : 0;
    rsi = *((rdi + 0x68));
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    ecx -= edx;
    edx = 0;
    sil = (*((rax + 0x68)) > rsi) ? 1 : 0;
    dl = (*((rax + 0x68)) < rsi) ? 1 : 0;
    esi = (int32_t) sil;
    edx -= esi;
    r8d = rdx + rcx*2;
    if (r8d == 0) {
        rsi = *(rdi);
        rdi = *(rax);
        void (*0x4660)() ();
    }
    eax = r8d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x84e0 */
 
int64_t rev_strcmp_ctime (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rcx = *((rdi + 0x80));
    rax = rsi;
    cl = (*((rsi + 0x80)) < rcx) ? 1 : 0;
    dl = (*((rsi + 0x80)) > rcx) ? 1 : 0;
    rsi = *((rdi + 0x88));
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    ecx -= edx;
    edx = 0;
    sil = (*((rax + 0x88)) > rsi) ? 1 : 0;
    dl = (*((rax + 0x88)) < rsi) ? 1 : 0;
    esi = (int32_t) sil;
    edx -= esi;
    r8d = rdx + rcx*2;
    if (r8d == 0) {
        rsi = *(rdi);
        rdi = *(rax);
        void (*0x4660)() ();
    }
    eax = r8d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8540 */
 
int64_t dbg_process_signals (void) {
    sigset_t oldset;
    int64_t var_88h;
    /* void process_signals(); */
    r13 = obj_color_indicator;
    r12 = r13 + 0x10;
    rbp = obj_caught_signals;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rbx = rsp;
    while (*(obj.used_color) == 0) {
label_0:
        rdi = stdout;
        fflush_unlocked ();
        rdx = rbx;
        rsi = rbp;
        edi = 0;
        sigprocmask ();
        r14d = interrupt_signal;
        eax = stop_signal_count;
        if (eax == 0) {
            goto label_2;
        }
        eax--;
        r14d = 0x13;
        *(obj.stop_signal_count) = eax;
label_1:
        raise (r14d);
        edx = 0;
        rsi = rbx;
        edi = 2;
        sigprocmask ();
        eax = interrupt_signal;
        if (eax == 0) {
            eax = stop_signal_count;
            if (eax == 0) {
                goto label_3;
            }
        }
    }
    put_indicator (r13);
    put_indicator (r12);
    goto label_0;
label_2:
    esi = 0;
    signal (r14d);
    goto label_1;
label_3:
    rax = *((rsp + 0x88));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x8640 */
 
uint64_t dbg_format_user_width (int64_t arg1) {
    rdi = arg1;
    /* int format_user_width(uid_t u); */
    ebx = edi;
    while (rax == 0) {
        r9d = ebx;
        r8 = 0x0001ba7d;
        edx = 1;
        rcx = 0xffffffffffffffff;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x46c0)() ();
        rax = getuser (rdi);
        rdi = rax;
    }
    eax = gnu_mbswidth (rdi, 0);
    edx = 0;
    __asm ("cmovs eax, edx");
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8690 */
 
int32_t calc_req_mask (void) {
    eax -= eax;
    al = 0;
    eax += 0x102;
    if (*(obj.print_block_size) != 0) {
        ah |= 4;
    }
    edx = format;
    if (edx != 0) {
        goto label_1;
    }
    edx = time_type;
    if (edx > 3) {
        void (*0x4c90)() ();
    }
    rcx = obj_CSWTCH_875;
    esi = *((rcx + rdx*4));
    eax |= esi;
    if (*(obj.print_owner) == 0) {
        if (*(obj.print_author) == 0) {
            goto label_2;
        }
    }
    eax |= 0x20c;
label_0:
    if (*(obj.print_group) != 0) {
        eax |= 0x10;
    }
    if (*(obj.sort_type) > 6) {
        void (*0x4c90)() ();
    }
    edx = sort_type;
    rcx = 0x0001a144;
    rdx = *((rcx + rdx*4));
    rdx += rcx;
    /* switch table (7 cases) at 0x1a144 */
    eax = void (*rdx)() ();
label_1:
    if (*(obj.sort_type) > 6) {
        void (*0x4c90)() ();
    }
    edx = sort_type;
    rcx = 0x0001a160;
    rdx = *((rcx + rdx*4));
    rdx += rcx;
    /* switch table (7 cases) at 0x1a160 */
    eax = void (*rdx)() ();
    return eax;
    edx = time_type;
    if (edx > 3) {
        void (*0x4c90)() ();
    }
    rcx = obj_CSWTCH_875;
    esi = *((rcx + rdx*4));
    eax |= esi;
    return eax;
    ah |= 2;
    return eax;
label_2:
    eax |= 0x204;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x4c90 */
 
void calc_req_mask_cold (void) {
    /* [16] -r-x section size 85266 named .text */
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x8790 */
 
int64_t xstrcoll_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = *(rsi);
    rbp = *(rdi);
    errno_location ();
    *(rax) = 0;
    rsi = r12;
    rdi = rbp;
    return strcoll ();
}

/* /tmp/tmprboi4m6a @ 0x87c0 */
 
int64_t rev_xstrcoll_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = *(rdi);
    rbp = *(rsi);
    errno_location ();
    *(rax) = 0;
    rsi = r12;
    rdi = rbp;
    return strcoll ();
}

/* /tmp/tmprboi4m6a @ 0x87f0 */
 
int64_t xstrcoll_size (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x48));
    al = (*((rdi + 0x48)) < rax) ? 1 : 0;
    dl = (*((rdi + 0x48)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    edx = (int32_t) dl;
    eax -= edx;
    if (eax == 0) {
        rbp = *(rdi);
        r12 = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8850 */
 
int64_t rev_xstrcoll_size (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rdi + 0x48));
    al = (*((rsi + 0x48)) < rax) ? 1 : 0;
    dl = (*((rsi + 0x48)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    edx = (int32_t) dl;
    eax -= edx;
    if (eax == 0) {
        rbp = *(rsi);
        r12 = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x88b0 */
 
int64_t strcmp_size (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x48));
    al = (*((rdi + 0x48)) < rax) ? 1 : 0;
    dl = (*((rdi + 0x48)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    edx = (int32_t) dl;
    eax -= edx;
    if (eax == 0) {
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x88f0 */
 
int64_t dbg_rev_strcmp_width (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_width(V a,V b); */
    r12 = rdi;
    rax = *((rsi + 0xc8));
    if (rax == 0) {
        goto label_1;
    }
label_0:
    rdx = *((r12 + 0xc8));
    ebx = eax;
    while (1) {
        eax = ebx;
        eax -= edx;
        if (eax == 0) {
            rsi = *(r12);
            rdi = *(rbp);
            r12 = rbx;
            void (*0x4660)() ();
        }
        r12 = rbx;
        return rax;
        rax = quote_name_width (*(r12), *(obj.filename_quoting_options), *((r12 + 0xc4)));
        rdx = rax;
    }
label_1:
    quote_name_width (*(rbp), *(obj.filename_quoting_options), *((rsi + 0xc4)));
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x8970 */
 
int64_t dbg_xstrcoll_ctime (uint32_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_ctime(V a,V b); */
    rax = *((rsi + 0x80));
    edx = 0;
    al = (*((rdi + 0x80)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x80)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x88));
    al = (*((rdi + 0x88)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x88)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rdi);
        r12 = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x89f0 */
 
int64_t dbg_rev_xstrcoll_btime (int64_t arg1, uint32_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_btime(V a,V b); */
    rax = *((rdi + 0x70));
    edx = 0;
    al = (*((rsi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rsi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rdi + 0x78));
    al = (*((rsi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rsi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rsi);
        r12 = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8a60 */
 
int64_t dbg_rev_xstrcoll_mtime (int64_t arg1, uint32_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_mtime(V a,V b); */
    rax = *((rdi + 0x70));
    edx = 0;
    al = (*((rsi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rsi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rdi + 0x78));
    al = (*((rsi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rsi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rsi);
        r12 = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8ad0 */
 
int64_t dbg_rev_xstrcoll_ctime (int64_t arg1, uint32_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_ctime(V a,V b); */
    rax = *((rdi + 0x80));
    edx = 0;
    al = (*((rsi + 0x80)) > rax) ? 1 : 0;
    dl = (*((rsi + 0x80)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rdi + 0x88));
    al = (*((rsi + 0x88)) < rax) ? 1 : 0;
    cl = (*((rsi + 0x88)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rsi);
        r12 = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8b50 */
 
int64_t dbg_xstrcoll_atime (uint32_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_atime(V a,V b); */
    rax = *((rsi + 0x60));
    edx = 0;
    al = (*((rdi + 0x60)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x60)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x68));
    al = (*((rdi + 0x68)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x68)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rdi);
        r12 = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8bc0 */
 
int64_t dbg_xstrcoll_mtime (uint32_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_mtime(V a,V b); */
    rax = *((rsi + 0x70));
    edx = 0;
    al = (*((rdi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x78));
    al = (*((rdi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rdi);
        r12 = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8c30 */
 
int64_t dbg_xstrcoll_btime (uint32_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_btime(V a,V b); */
    rax = *((rsi + 0x70));
    edx = 0;
    al = (*((rdi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x78));
    al = (*((rdi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rdi);
        r12 = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8ca0 */
 
int64_t dbg_rev_xstrcoll_atime (int64_t arg1, uint32_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_atime(V a,V b); */
    rax = *((rdi + 0x60));
    edx = 0;
    al = (*((rsi + 0x60)) > rax) ? 1 : 0;
    dl = (*((rsi + 0x60)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rdi + 0x68));
    al = (*((rsi + 0x68)) < rax) ? 1 : 0;
    cl = (*((rsi + 0x68)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rbp = *(rsi);
        r12 = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8d10 */
 
int64_t dbg_sort_files (int64_t arg_c4h, int64_t arg_c8h) {
    /* void sort_files(); */
    rbp = cwd_n_used;
    rax = cwd_n_used;
    rax >>= 1;
    rax += rbp;
    if (rax > *(obj.sorted_file_alloc)) {
        rdi = sorted_file;
        fcn_00004630 ();
        rax = xnmalloc (rbp, 0x18);
        rbp = cwd_n_used;
        *(obj.sorted_file) = rax;
        rax = rbp + rbp*2;
        *(obj.sorted_file_alloc) = rax;
    }
    if (rbp == 0) {
        goto label_4;
    }
    rax = sorted_file;
    rdx = cwd_file;
    rcx = rax + rbp*8;
    do {
        *(rax) = rdx;
        rax += 8;
        rdx += 0xd0;
    } while (rax != rcx);
    eax = sort_type;
    if (eax == 2) {
        goto label_5;
    }
label_2:
    if (*(obj.line_length) != 0) {
        goto label_6;
    }
label_0:
    if (eax == 6) {
        goto label_7;
    }
label_3:
    eax = setjmp (obj.failed_strcoll);
    ecx = sort_type;
    if (eax == 0) {
        goto label_8;
    }
    if (ecx == 4) {
        goto label_9;
    }
    r8 = cwd_n_used;
    rdi = sorted_file;
    if (r8 == 0) {
        goto label_10;
    }
    rdx = cwd_file;
    rax = rdi;
    rsi = rdi + r8*8;
    do {
        *(rax) = rdx;
        rax += 8;
        rdx += 0xd0;
    } while (rsi != rax);
label_10:
    eax = 1;
    do {
        if (ecx == 5) {
            ebx = time_type;
            ecx = rbx + 5;
        }
        rax = (int64_t) eax;
        edx = *(obj.directories_first);
        rcx = rax + rcx*2;
        eax = *(obj.sort_reverse);
        rax = rax + rcx*2;
        rdx = rdx + rax*2;
        rax = obj_sort_functions;
        rax = mpsort (rdi, r8, *((rax + rdx*8)), rcx);
label_7:
        return rax;
label_8:
        rdi = sorted_file;
        r8 = cwd_n_used;
    } while (1);
label_6:
    esi = format;
    edx = rsi - 2;
    if (edx > 1) {
        goto label_0;
    }
    if (rbp == 0) {
        goto label_0;
    }
label_5:
    ebx = 0;
    while (rax != 0) {
label_1:
        rbx++;
        *((rbp + 0xc8)) = rax;
        if (rbx >= *(obj.cwd_n_used)) {
            goto label_11;
        }
        rax = sorted_file;
        rbp = *((rax + rbx*8));
        rax = *((rbp + 0xc8));
    }
    quote_name_width (*(rbp), *(obj.filename_quoting_options), *((rbp + 0xc4)));
    goto label_1;
label_11:
    eax = sort_type;
    goto label_0;
label_4:
    eax = sort_type;
    if (eax != 2) {
        goto label_2;
    }
    goto label_3;
label_9:
    return assert_fail ("sort_type != sort_version", "src/ls.c", 0x1008, "sort_files");
}

/* /tmp/tmprboi4m6a @ 0x8f30 */
 
int64_t strcmp_atime (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x60));
    edx = 0;
    al = (*((rdi + 0x60)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x60)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x68));
    al = (*((rdi + 0x68)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x68)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8f80 */
 
int64_t strcmp_ctime (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x80));
    edx = 0;
    al = (*((rdi + 0x80)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x80)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x88));
    al = (*((rdi + 0x88)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x88)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x8fe0 */
 
int64_t strcmp_btime (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x70));
    edx = 0;
    al = (*((rdi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x78));
    al = (*((rdi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x9030 */
 
int64_t strcmp_mtime (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = *((rsi + 0x70));
    edx = 0;
    al = (*((rdi + 0x70)) > rax) ? 1 : 0;
    dl = (*((rdi + 0x70)) < rax) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    rax = *((rsi + 0x78));
    al = (*((rdi + 0x78)) < rax) ? 1 : 0;
    cl = (*((rdi + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    ecx = (int32_t) cl;
    eax -= ecx;
    eax = rax + rdx*2;
    if (eax == 0) {
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x9080 */
 
int64_t dbg_rev_xstrcoll_width (int64_t arg_c4h, int64_t arg_c8h, int64_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_width(V a,V b); */
    r12 = rsi;
    rax = *((rsi + 0xc8));
    if (rax == 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 0xc8));
    ebx = eax;
    while (1) {
        eax = ebx;
        eax -= edx;
        if (eax == 0) {
            errno_location ();
            r12 = *(r12);
            rbp = *(rbp);
            *(rax) = 0;
            rsi = rbp;
            rdi = r12;
            void (*0x4ac0)() ();
        }
        return rax;
        rax = quote_name_width (*(rbp), *(obj.filename_quoting_options), *((rbp + 0xc4)));
        rdx = rax;
    }
label_1:
    quote_name_width (*(r12), *(obj.filename_quoting_options), *((rsi + 0xc4)));
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x9110 */
 
int64_t dbg_xstrcoll_width (int64_t arg_c4h, int64_t arg_c8h, int64_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_width(V a,V b); */
    r12 = rdi;
    rax = *((rdi + 0xc8));
    if (rax == 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 0xc8));
    ebx = eax;
    while (1) {
        eax = ebx;
        eax -= edx;
        if (eax == 0) {
            errno_location ();
            r12 = *(r12);
            rbp = *(rbp);
            *(rax) = 0;
            rsi = rbp;
            rdi = r12;
            void (*0x4ac0)() ();
        }
        return rax;
        rax = quote_name_width (*(rbp), *(obj.filename_quoting_options), *((rbp + 0xc4)));
        rdx = rax;
    }
label_1:
    quote_name_width (*(rdi), *(obj.filename_quoting_options), *((rdi + 0xc4)));
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x91a0 */
 
uint64_t format_user_or_group (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    ebx = edx;
    if (rdi == 0) {
        goto label_1;
    }
    eax = gnu_mbswidth (rdi, 0);
    ebx -= eax;
    eax = 0;
    __asm ("cmovs ebx, eax");
    rax = strlen (rbp);
    rsi = rax;
    rax = dired_outbuf (rbp);
    do {
        rdi = stdout;
        *(obj.dired_pos)++;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_2;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0x20;
label_0:
        ebx--;
    } while (ebx >= 0);
    return rax;
label_2:
    esi = 0x20;
    eax = overflow ();
    goto label_0;
label_1:
    rcx = rsi;
    edi = 1;
    eax = 0;
    rsi = "%*lu ";
    rax = printf_chk ();
    rax = (int64_t) eax;
    *(obj.dired_pos) += rax;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x9250 */
 
uint64_t dbg_xstrcoll_extension (int64_t arg2, int32_t diff, char ** s) {
    rsi = arg2;
    rax = diff;
    rdi = s;
    /* int xstrcoll_extension(V a,V b); */
    r12 = rsi;
    r13 = *(rdi);
    rax = strrchr (*(rdi), 0x2e);
    r14 = *(r12);
    rax = strrchr (r14, 0x2e);
    r12 = rax;
    rax = 0x0001bb0a;
    if (r12 == 0) {
        r12 = rax;
    }
    if (rbp == 0) {
    }
    errno_location ();
    *(rax) = 0;
    eax = strcoll (rbp, r12);
    if (eax == 0) {
        rsi = r14;
        rdi = r13;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x48e0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmprboi4m6a @ 0x4ac0 */
 
void strcoll (void) {
    __asm ("bnd jmp qword [reloc.strcoll]");
}

/* /tmp/tmprboi4m6a @ 0x92e0 */
 
uint64_t dbg_rev_xstrcoll_extension (int64_t arg1, int32_t diff, char ** s) {
    rdi = arg1;
    rax = diff;
    rsi = s;
    /* int rev_xstrcoll_extension(V a,V b); */
    r12 = rdi;
    r13 = *(rsi);
    rax = strrchr (r13, 0x2e);
    r14 = *(r12);
    rax = strrchr (r14, 0x2e);
    r12 = rax;
    rax = 0x0001bb0a;
    if (r12 == 0) {
        r12 = rax;
    }
    if (rbp == 0) {
    }
    errno_location ();
    *(rax) = 0;
    eax = strcoll (rbp, r12);
    if (eax == 0) {
        rsi = r14;
        rdi = r13;
        void (*0x4ac0)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x9370 */
 
int64_t dbg_strcmp_width (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_width(V a,V b); */
    r12 = rsi;
    rax = *((rdi + 0xc8));
    if (rax == 0) {
        goto label_1;
    }
label_0:
    rdx = *((r12 + 0xc8));
    ebx = eax;
    while (1) {
        eax = ebx;
        eax -= edx;
        if (eax == 0) {
            rsi = *(r12);
            rdi = *(rbp);
            r12 = rbx;
            void (*0x4660)() ();
        }
        r12 = rbx;
        return rax;
        rax = quote_name_width (*(r12), *(obj.filename_quoting_options), *((r12 + 0xc4)));
        rdx = rax;
    }
label_1:
    quote_name_width (*(rdi), *(obj.filename_quoting_options), *((rdi + 0xc4)));
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x93f0 */
 
uint64_t dbg_extract_dirs_from_files (int64_t arg_8h, int64_t arg_10h, uint32_t arg_a8h, int64_t arg1) {
    rdi = arg1;
    /* void extract_dirs_from_files(char const * dirname,_Bool command_line_arg); */
    r13d = (int32_t) sil;
    r12 = rdi;
    if (rdi != 0) {
        if (*(obj.active_dir_set) == 0) {
            goto label_4;
        }
        rax = xmalloc (0x20);
        rbx = rax;
        rax = xstrdup (r12);
        *(rbx) = 0;
        *((rbx + 8)) = rax;
        rax = pending_dirs;
        *((rbx + 0x10)) = 0;
        *((rbx + 0x18)) = rax;
        *(obj.pending_dirs) = rbx;
    }
label_4:
    rax = cwd_n_used;
    rbx = rax - 1;
    if (rax != 0) {
        goto label_1;
    }
    goto label_5;
    do {
label_0:
        rbx--;
        if (rbx < 0) {
            goto label_6;
        }
label_1:
        rax = sorted_file;
        rbp = *((rax + rbx*8));
        eax = *((rbp + 0xa8));
        if (eax == 3) {
            goto label_7;
        }
    } while (eax != 9);
label_7:
    r14 = *(rbp);
    if (r12 == 0) {
        goto label_8;
    }
    rdi = r14;
    rax = last_component ();
    if (*(rax) == 0x2e) {
        goto label_9;
    }
label_3:
    if (*(r14) == 0x2f) {
        goto label_8;
    }
    rsi = r14;
    edx = 0;
    rdi = r12;
    rax = file_name_concat ();
    rsi = *((rbp + 8));
    edx = r13d;
    r14 = rax;
    rdi = rax;
    queue_directory ();
    rdi = r14;
    fcn_00004630 ();
label_2:
    if (*((rbp + 0xa8)) != 9) {
        goto label_0;
    }
    rdi = *(rbp);
    fcn_00004630 ();
    rdi = *((rbp + 8));
    fcn_00004630 ();
    rdi = *((rbp + 0x10));
    fcn_00004630 ();
    rbx--;
    if (rbx >= 0) {
        goto label_1;
    }
label_6:
    rdx = cwd_n_used;
    if (rdx == 0) {
        goto label_5;
    }
    rsi = sorted_file;
    rdi = rsi + rdx*8;
    rax = rsi;
    edx = 0;
    do {
        rcx = *(rax);
        *((rsi + rdx*8)) = rcx;
        cl = (*((rcx + 0xa8)) != 9) ? 1 : 0;
        rax += 8;
        ecx = (int32_t) cl;
        rdx += rcx;
    } while (rdi != rax);
    *(obj.cwd_n_used) = rdx;
    return rax;
label_8:
    rsi = *((rbp + 8));
    edx = r13d;
    rdi = r14;
    queue_directory ();
    goto label_2;
label_9:
    edx = 0;
    dl = (*((rax + 1)) == 0x2e) ? 1 : 0;
    eax = *((rax + rdx + 1));
    if (al == 0) {
        goto label_0;
    }
    if (al == 0x2f) {
        goto label_0;
    }
    goto label_3;
label_5:
    edx = 0;
    *(obj.cwd_n_used) = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x95c0 */
 
int64_t gobble_file_constprop_0 (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4) {
    int64_t var_391h;
    int64_t canary;
    int64_t var_388h;
    uint32_t var_384h;
    int64_t var_380h;
    int64_t var_368h;
    int64_t var_2f0h;
    int64_t var_2d0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r15 = rcx;
    r13d = esi;
    r12 = rdi;
    *((rbp - 0x384)) = edx;
    rsi = cwd_n_used;
    rdi = cwd_file;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    if (rsi == *(obj.cwd_n_alloc)) {
        goto label_37;
    }
label_9:
    rax = rsi * 3;
    rdx = rsi + rax*4;
    eax = 0;
    rdx <<= 4;
    rbx = rdi + rdx;
    rdi = rbx + 8;
    rcx = rbx;
    *(rbx) = 0;
    *((rbx + 0xc8)) = 0;
    rdi &= 0xfffffffffffffff8;
    rcx -= rdi;
    ecx += 0xd0;
    ecx >>= 3;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    *((rbx + 0xa8)) = r13d;
    *((rbx + 0xc4)) = 0xffffffff;
    if (*(obj.cwd_some_quoted) == 0) {
        if (*(obj.align_variable_outer_quotes) != 0) {
            goto label_38;
        }
    }
label_10:
    esi = *(obj.print_hyperlink);
    if (*((rbp - 0x384)) == 0) {
        goto label_39;
    }
    edx = *(r12);
    r14 = r12;
    if (dl != 0x2f) {
        r8d = *(r15);
        if (r8b != 0) {
            goto label_8;
        }
    }
label_13:
    if (sil != 0) {
        goto label_12;
    }
label_1:
    r9d = dereference;
    r15 = rbx + 0x18;
    *((rbp - 0x390)) = r9d;
    calc_req_mask ();
    r9d = *((rbp - 0x390));
    if (r9d > 3) {
        goto label_40;
    }
    if (r9d > 1) {
        goto label_41;
    }
label_4:
    eax = calc_req_mask ();
    ecx = 0x100;
    r8d = eax;
    eax = do_statx (0xffffff9c, r14, r15);
    if (eax != 0) {
        goto label_42;
    }
label_5:
    *((rbx + 0xb8)) = 1;
    if (r13d == 5) {
        goto label_43;
    }
    eax = *((rbx + 0x30));
    eax &= 0xf000;
    if (eax == 0x8000) {
        goto label_43;
    }
label_7:
    edx = format;
    if (edx != 0) {
        if (*(obj.print_scontext) == 0) {
            goto label_44;
        }
    }
    rcx = *((rbx + 0x18));
    *((rbp - 0x388)) = edx;
    *((rbp - 0x390)) = rcx;
    errno_location ();
    rcx = *((rbp - 0x390));
    *(rax) = 0x5f;
    edx = *((rbp - 0x388));
    r13 = rax;
    if (rcx != *(obj.unsupported_device.9)) {
        *(obj.unsupported_device.9) = rcx;
    }
    rax = obj_UNKNOWN_SECURITY_CONTEXT;
    *((rbx + 0xb0)) = rax;
    if (edx == 0) {
        goto label_45;
    }
    *((rbx + 0xbc)) = 0;
label_44:
    eax = *((rbx + 0x30));
    eax &= 0xf000;
    if (eax == 0xa000) {
        goto label_46;
    }
label_16:
    edx = 5;
    if (eax == sym._init) {
        goto label_47;
    }
label_17:
    esi = format;
    r14 = *((rbx + 0x58));
    *((rbx + 0xa8)) = edx;
    if (esi == 0) {
        goto label_48;
    }
    if (*(obj.print_block_size) != 0) {
        goto label_48;
    }
label_2:
    if (*(obj.print_scontext) != 0) {
        goto label_49;
    }
label_3:
    eax = *(obj.print_inode);
    if (al != 0) {
        goto label_50;
    }
    do {
label_0:
        rax = xstrdup (r12);
        *(obj.cwd_n_used)++;
        *(rbx) = rax;
label_6:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_51;
        }
        rsp = rbp - 0x28;
        rax = r14;
        return rax;
label_39:
        if (sil != 0) {
            goto label_52;
        }
        if (*(obj.format_needs_stat) != 0) {
            goto label_23;
        }
        if (r13d == 3) {
            goto label_53;
        }
label_27:
        eax = *(obj.print_inode);
        if (al != 0) {
            goto label_54;
        }
        if (*(obj.format_needs_type) == 0) {
            goto label_55;
        }
        edx = 0x41;
        if (((rdx >> r13) & 1) < 0) {
label_24:
            if (*(obj.dereference) == 4) {
                goto label_56;
            }
            if (*(obj.color_symlink_as_referent) != 0) {
                goto label_23;
            }
            if (*(obj.check_symlink_mode) != 0) {
                goto label_23;
            }
            if (al != 0) {
                goto label_23;
            }
        }
        if (*(obj.format_needs_type) == 0) {
            goto label_55;
        }
        if (r13d == 0) {
            goto label_23;
        }
        r14d = 0;
    } while (r13d != 5);
    if (*(obj.indicator_style) == 3) {
        goto label_23;
    }
    if (*(obj.print_with_color) == 0) {
        goto label_0;
    }
    al = is_colored (0xe);
    if (al != 0) {
        goto label_23;
    }
    al = is_colored (0x10);
    if (al != 0) {
        goto label_23;
    }
    al = is_colored (0x11);
    if (al != 0) {
        goto label_23;
    }
    al = is_colored (0x15);
    if (al == 0) {
        goto label_0;
    }
label_23:
    edx = *(r12);
    r14 = r12;
    if (dl == 0x2f) {
        goto label_1;
    }
label_34:
    r8d = *(r15);
    r14 = r12;
    if (r8b == 0) {
        goto label_1;
    }
label_8:
    *((rbp - 0x391)) = sil;
    *((rbp - 0x388)) = dl;
    *((rbp - 0x390)) = r8b;
    rax = strlen (r12);
    r14 = rax;
    strlen (r15);
    rdi = rsp;
    esi = *((rbp - 0x391));
    edx = *((rbp - 0x388));
    rax = r14 + rax + 0x19;
    r8d = *((rbp - 0x390));
    rcx = rax;
    rax &= 0xfffffffffffff000;
    rcx &= 0xfffffffffffffff0;
    rdi -= rax;
    do {
        if (rsp == rdi) {
            goto label_57;
        }
    } while (1);
label_48:
    rax = human_readable (r14, rbp - 0x2d0, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    eax = gnu_mbswidth (rax, 0);
    if (eax > *(obj.block_size_width)) {
        *(obj.block_size_width) = eax;
    }
    ecx = format;
    if (ecx != 0) {
        goto label_2;
    }
    if (*(obj.print_owner) != 0) {
        goto label_58;
    }
label_21:
    if (*(obj.print_group) != 0) {
        goto label_59;
    }
label_20:
    if (*(obj.print_author) != 0) {
        goto label_60;
    }
label_19:
    if (*(obj.print_scontext) != 0) {
label_49:
        eax = strlen (*((rbx + 0xb0)));
        if (eax > *(obj.scontext_width)) {
            goto label_61;
        }
    }
label_11:
    edx = format;
    if (edx != 0) {
        goto label_3;
    }
    rax = umaxtostr (*((rbx + 0x28)), rbp - 0x2f0, rdx);
    eax = strlen (rax);
    if (eax > *(obj.nlink_width)) {
        *(obj.nlink_width) = eax;
    }
    eax = *((rbx + 0x30));
    eax &= 0xb000;
    if (eax != 0x2000) {
        goto label_62;
    }
    r13 = rbp - 0x2d0;
    rax = rdi;
    rdi >>= 0x20;
    rax >>= 8;
    edi &= 0xfffff000;
    eax &= 0xfff;
    edi |= eax;
    rax = umaxtostr (*((rbx + 0x40)), r13, rdx);
    eax = strlen (rax);
    if (eax > *(obj.major_device_number_width)) {
        *(obj.major_device_number_width) = eax;
    }
    eax = (int32_t) dil;
    rdi >>= 0xc;
    dil = 0;
    edi |= eax;
    rax = umaxtostr (*((rbx + 0x40)), r13, rdx);
    eax = strlen (rax);
    edx = minor_device_number_width;
    if (eax > edx) {
        *(obj.minor_device_number_width) = eax;
        edx = eax;
    }
    eax = major_device_number_width;
    eax = rdx + rax + 2;
    if (eax <= *(obj.file_size_width)) {
        goto label_3;
    }
    goto label_63;
label_40:
    if (r9d != 4) {
        goto label_4;
    }
label_35:
    ecx = 0;
    r8d = eax;
    eax = do_statx (0xffffff9c, r14, r15);
    if (eax == 0) {
        goto label_5;
    }
label_42:
    edx = 5;
    rax = dcgettext (0, "cannot access %s");
    r15d = *((rbp - 0x384));
    r14d = 0;
    edi = (int32_t) r15b;
    file_failure (rdi, rax, r14);
    rax = obj_UNKNOWN_SECURITY_CONTEXT;
    *((rbx + 0xb0)) = rax;
    if (r15b != 0) {
        goto label_6;
    }
    goto label_0;
label_43:
    if (*(obj.print_with_color) == 0) {
        goto label_7;
    }
    al = is_colored (0x15);
    if (al == 0) {
        goto label_7;
    }
    errno_location ();
    r13 = *((rbx + 0x18));
    *(rax) = 0x5f;
    if (r13 != *(obj.unsupported_device.10)) {
        *(obj.unsupported_device.10) = r13;
    }
    *((rbx + 0xc0)) = 0;
    goto label_7;
label_41:
    if (*((rbp - 0x384)) == 0) {
        goto label_4;
    }
    r8d = eax;
    ecx = 0;
    *((rbp - 0x390)) = r9d;
    eax = do_statx (0xffffff9c, r14, r15);
    r9d = *((rbp - 0x390));
    if (r9d == 2) {
        goto label_64;
    }
    if (eax < 0) {
        goto label_65;
    }
    edx = *((rbx + 0x30));
    edx &= 0xf000;
    if (edx != sym._init) {
        goto label_4;
    }
label_64:
    if (eax == 0) {
        goto label_5;
    }
    goto label_66;
label_52:
    edx = *(r12);
    if (dl == 0x2f) {
        goto label_67;
    }
    r8d = *(r15);
    r14 = r12;
    if (r8b != 0) {
        goto label_8;
    }
label_12:
    esi = 2;
    rdi = r14;
    rax = canonicalize_filename_mode ();
    *((rbx + 0x10)) = rax;
    if (rax != 0) {
        goto label_1;
    }
    edx = 5;
    rax = dcgettext (0, "error canonicalizing %s");
    eax = *((rbp - 0x384));
    file_failure (eax, rax, r14);
    goto label_1;
label_50:
    rax = umaxtostr (*((rbx + 0x20)), rbp - 0x2d0, rdx);
    eax = strlen (rax);
    if (eax <= *(obj.inode_number_width)) {
        goto label_0;
    }
    *(obj.inode_number_width) = eax;
    goto label_0;
label_37:
    edx = 0x1a0;
    rax = xreallocarray ();
    rsi = cwd_n_used;
    *(obj.cwd_n_alloc) <<= 1;
    *(obj.cwd_file) = rax;
    rdi = rax;
    goto label_9;
label_38:
    rax = quotearg_buffer (rbp - 0x2d0, 2, r12, 0xffffffffffffffff, *(obj.filename_quoting_options));
    r14 = rax;
    eax = *((rbp - 0x2d0));
    if (*(r12) == al) {
        goto label_68;
    }
label_22:
    *((rbx + 0xc4)) = 1;
    *(obj.cwd_some_quoted) = 1;
    goto label_10;
label_61:
    *(obj.scontext_width) = eax;
    goto label_11;
label_62:
    rax = human_readable (*((rbx + 0x48)), rbp - 0x2d0, *(obj.file_human_output_opts), 1, *(obj.file_output_block_size), r9);
    eax = gnu_mbswidth (rax, 0);
    if (eax <= *(obj.file_size_width)) {
        goto label_3;
    }
label_63:
    *(obj.file_size_width) = eax;
    goto label_3;
label_67:
    r14 = r12;
    goto label_12;
label_57:
    ecx &= 0xfff;
    if (rcx != 0) {
        goto label_69;
    }
label_25:
    r9 = rsp + 0xf;
    edi = *((r15 + 1));
    r9 &= 0xfffffffffffffff0;
    r14 = r9;
    if (r8b == 0x2e) {
        goto label_70;
    }
label_15:
    rcx = r15;
    while (dil != 0) {
        r8d = edi;
        edi = *((rcx + 1));
        r9 = rax;
        rax = r9 + 1;
        rcx++;
        *((rax - 1)) = r8b;
    }
    if (r15 < rcx) {
        if (*((rcx - 1)) == 0x2f) {
            goto label_14;
        }
        *(rax) = 0x2f;
        rax = r9 + 2;
    }
label_14:
    if (dl == 0) {
        goto label_71;
    }
    rcx = r12;
    do {
        rcx++;
        *(rax) = dl;
        rax++;
        edx = *(rcx);
    } while (dl != 0);
label_71:
    *(rax) = 0;
    goto label_13;
label_70:
    rax = r9;
    if (dil == 0) {
        goto label_14;
    }
    goto label_15;
label_45:
    if (rcx == *(obj.unsupported_device.8)) {
        goto label_72;
    }
    *(r13) = 0;
    rsi = r15;
    rdi = r14;
    eax = file_has_acl ();
    if (eax <= 0) {
        goto label_73;
    }
    *((rbx + 0xbc)) = 2;
    *(obj.any_has_acl) = 1;
label_26:
    eax = *((rbx + 0x30));
    eax &= 0xf000;
    if (eax != 0xa000) {
        goto label_16;
    }
    edi = format;
    if (edi != 0) {
label_46:
        if (*(obj.check_symlink_mode) == 0) {
            goto label_74;
        }
    }
label_32:
    rax = areadlink_with_size (r14, *((rbx + 0x48)));
    *((rbx + 8)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_75;
    }
label_33:
    if (*(r13) == 0x2f) {
        goto label_76;
    }
    rdi = r14;
    rax = dir_len ();
    rdi = r13;
    r15 = rax;
    if (rax == 0) {
        goto label_77;
    }
    strlen (rdi);
    rax = xmalloc (r15 + rax + 2);
    r9 = rax;
    if (*((r14 + r15 - 1)) != 0x2f) {
        r15++;
    }
    rdi = r9;
    rdx = r15;
    rsi = r14;
    *((rbp - 0x390)) = r9;
    rax = stpncpy ();
    strcpy (rax, r13);
    r9 = *((rbp - 0x390));
label_28:
    eax = *((rbx + 0xc4));
    if (eax == 0) {
        goto label_78;
    }
label_31:
    if (*(obj.indicator_style) > 1) {
        goto label_79;
    }
    if (*(obj.check_symlink_mode) != 0) {
        goto label_79;
    }
label_18:
    rdi = r9;
    fcn_00004630 ();
    eax = *((rbx + 0x30));
    eax &= 0xf000;
    if (eax != 0xa000) {
        goto label_16;
    }
label_74:
    edx = 6;
    goto label_17;
label_79:
    ecx = 0;
    rsi = r9;
    r8d = 2;
    *((rbp - 0x390)) = r9;
    eax = do_statx (0xffffff9c, rsi, rbp - 0x380);
    r9 = *((rbp - 0x390));
    if (eax != 0) {
        goto label_18;
    }
    eax = *((rbp - segment.NOTE_1));
    *((rbx + 0xb9)) = 1;
    *((rbx + 0xac)) = eax;
    goto label_18;
label_47:
    edx = 3;
    if (*((rbp - 0x384)) == 0) {
        goto label_17;
    }
    edx -= edx;
    edx &= 6;
    edx += 3;
    goto label_17;
label_60:
    eax = format_user_width (*((rbx + 0x34)));
    if (eax <= *(obj.author_width)) {
        goto label_19;
    }
    *(obj.author_width) = eax;
    goto label_19;
label_59:
    r13d = *((rbx + 0x38));
    if (*(obj.numeric_ids) == 0) {
        goto label_80;
    }
label_29:
    r9d = r13d;
    r8 = 0x0001ba7d;
    esi = 0;
    edi = 0;
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    eax = snprintf_chk ();
label_30:
    if (*(obj.group_width) >= eax) {
        goto label_20;
    }
    *(obj.group_width) = eax;
    goto label_20;
label_58:
    eax = format_user_width (*((rbx + 0x34)));
    if (eax <= *(obj.owner_width)) {
        goto label_21;
    }
    *(obj.owner_width) = eax;
    goto label_21;
label_68:
    rax = strlen (r12);
    if (r14 != rax) {
        goto label_22;
    }
    *((rbx + 0xc4)) = 0;
    goto label_10;
label_54:
    edx = 0x41;
    if (((rdx >> r13) & 1) >= 0) {
        goto label_23;
    }
    goto label_24;
label_69:
    goto label_25;
label_65:
    rax = errno_location ();
    if (*(rax) == 2) {
        goto label_4;
    }
label_66:
    edx = 5;
    rax = dcgettext (0, "cannot access %s");
    r14d = 0;
    file_failure (1, rax, r14);
    rax = obj_UNKNOWN_SECURITY_CONTEXT;
    *((rbx + 0xb0)) = rax;
    goto label_6;
label_55:
    r14d = 0;
    goto label_0;
label_73:
    edx = *(r13);
    ecx = rdx - 0x16;
    ecx &= 0xffffffef;
    if (ecx != 0) {
        if (edx != 0x5f) {
            goto label_81;
        }
    }
    rdx = *((rbx + 0x18));
    *(obj.unsupported_device.8) = rdx;
label_81:
    *((rbx + 0xbc)) = 0;
    if (eax == 0) {
        goto label_26;
    }
    rdx = r14;
    edi = 0;
    esi = 3;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    error (0, *(r13), 0x0001e27c);
    goto label_26;
label_53:
    if (*(obj.print_with_color) == 0) {
        goto label_27;
    }
    al = is_colored (0x13);
    if (al != 0) {
        goto label_23;
    }
    al = is_colored (0x12);
    if (al != 0) {
        goto label_23;
    }
    al = is_colored (0x14);
    if (al != 0) {
        goto label_23;
    }
    goto label_27;
label_77:
    rax = xstrdup (rdi);
    r9 = rax;
    goto label_28;
label_80:
    rax = getgroup (r13d);
    rdi = rax;
    if (rax == 0) {
        goto label_29;
    }
    eax = gnu_mbswidth (rdi, 0);
    edx = 0;
    __asm ("cmovs eax, edx");
    goto label_30;
label_78:
    r14 = *((rbx + 8));
    *((rbp - 0x390)) = r9;
    rax = quotearg_buffer (rbp - 0x2d0, 2, r14, 0xffffffffffffffff, *(obj.filename_quoting_options));
    r9 = *((rbp - 0x390));
    r13 = rax;
    eax = *((rbp - 0x2d0));
    if (*(r14) == al) {
        goto label_82;
    }
label_36:
    *((rbx + 0xc4)) = 0xffffffff;
    goto label_31;
label_72:
    *(r13) = 0x5f;
    eax = *((rbx + 0x30));
    *((rbx + 0xbc)) = 0;
    eax &= 0xf000;
    if (eax != 0xa000) {
        goto label_16;
    }
    goto label_32;
label_75:
    edx = 5;
    rax = dcgettext (0, "cannot read symbolic link %s");
    edi = *((rbp - 0x384));
    file_failure (rdi, rax, r14);
    r13 = *((rbx + 8));
    r9d = 0;
    if (r13 != 0) {
        goto label_33;
    }
    goto label_18;
label_76:
    rax = xstrdup (r13);
    r9 = rax;
    goto label_28;
label_56:
    edx = *(r12);
    if (dl != 0x2f) {
        goto label_34;
    }
    calc_req_mask ();
    r15 = rbx + 0x18;
    r14 = r12;
    goto label_35;
label_82:
    rax = strlen (r14);
    r9 = *((rbp - 0x390));
    if (r13 != rax) {
        goto label_36;
    }
    goto label_31;
label_51:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xa330 */
 
int64_t dbg_strcmp_df_ctime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_ctime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rcx + 0x80));
        esi = 0;
        al = (*((rdx + 0x80)) > rax) ? 1 : 0;
        sil = (*((rdx + 0x80)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rcx + 0x88));
        al = (*((rdx + 0x88)) < rax) ? 1 : 0;
        dil = (*((rdx + 0x88)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rcx);
    rdi = *(rdx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x4660 */
 
void fcn_00004660 (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmprboi4m6a @ 0xa410 */
 
int32_t dbg_rev_xstrcoll_df_ctime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_ctime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8ad0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xa4a0 */
 
int64_t dbg_strcmp_df_btime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_btime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rcx + 0x70));
        esi = 0;
        al = (*((rdx + 0x70)) > rax) ? 1 : 0;
        sil = (*((rdx + 0x70)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rcx + 0x78));
        al = (*((rdx + 0x78)) < rax) ? 1 : 0;
        dil = (*((rdx + 0x78)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rcx);
    rdi = *(rdx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xa570 */
 
int32_t dbg_rev_xstrcoll_df_btime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_btime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x89f0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xa600 */
 
int64_t dbg_rev_strcmp_df_size (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_size(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x48));
        al = (*((rcx + 0x48)) < rax) ? 1 : 0;
        sil = (*((rcx + 0x48)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        esi = (int32_t) sil;
        eax -= esi;
    }
    rsi = *(rdx);
    rdi = *(rcx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xa6b0 */
 
int64_t dbg_xstrcoll_df_name (int64_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_df_name(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 3) ? 1 : 0;
    dl = (edx == 9) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        r12 = *(rsi);
        rbp = *(rdi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xa770 */
 
int32_t dbg_strcmp_df_name (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_name(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rsi = *(rsi);
        rdi = *(rdi);
        void (*0x4660)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xa810 */
 
int64_t dbg_rev_xstrcoll_df_name (int64_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_df_name(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 3) ? 1 : 0;
    dl = (edx == 9) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        r12 = *(rdi);
        rbp = *(rsi);
        errno_location ();
        *(rax) = 0;
        rsi = r12;
        rdi = rbp;
        void (*0x4ac0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xa8d0 */
 
int64_t dbg_rev_strcmp_df_btime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_btime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x70));
        esi = 0;
        al = (*((rcx + 0x70)) > rax) ? 1 : 0;
        sil = (*((rcx + 0x70)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rdx + 0x78));
        al = (*((rcx + 0x78)) < rax) ? 1 : 0;
        dil = (*((rcx + 0x78)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rdx);
    rdi = *(rcx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xa9a0 */
 
int64_t dbg_rev_strcmp_df_mtime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_mtime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x70));
        esi = 0;
        al = (*((rcx + 0x70)) > rax) ? 1 : 0;
        sil = (*((rcx + 0x70)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rdx + 0x78));
        al = (*((rcx + 0x78)) < rax) ? 1 : 0;
        dil = (*((rcx + 0x78)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rdx);
    rdi = *(rcx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xaa70 */
 
int64_t dbg_strcmp_df_size (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_size(V a,V b); */
    rdx = rdi;
    edi = *((rsi + 0xa8));
    ecx = *((rdx + 0xa8));
    al = (ecx == 9) ? 1 : 0;
    cl = (ecx == 3) ? 1 : 0;
    eax |= ecx;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        ecx = *((rsi + 0xac));
        ecx &= 0xf000;
        if (ecx == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    ecx = *((rdx + 0xac));
    ecx &= 0xf000;
    cl = (ecx == sym._init) ? 1 : 0;
    ecx = (int32_t) cl;
    eax -= ecx;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rsi + 0x48));
        al = (*((rdx + 0x48)) < rax) ? 1 : 0;
        cl = (*((rdx + 0x48)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        ecx = (int32_t) cl;
        eax -= ecx;
    }
    rsi = *(rsi);
    rdi = *(rdx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xab20 */
 
int64_t dbg_rev_xstrcoll_df_size (int64_t arg1, uint32_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int rev_xstrcoll_df_size(V a,V b); */
    rdx = rdi;
    edi = *((rsi + 0xa8));
    ecx = *((rdx + 0xa8));
    al = (ecx == 9) ? 1 : 0;
    cl = (ecx == 3) ? 1 : 0;
    eax |= ecx;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        ecx = *((rsi + 0xac));
        ecx &= 0xf000;
        if (ecx == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    ecx = *((rdx + 0xac));
    ecx &= 0xf000;
    cl = (ecx == sym._init) ? 1 : 0;
    ecx = (int32_t) cl;
    eax -= ecx;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x48));
        al = (*((rsi + 0x48)) < rax) ? 1 : 0;
        cl = (*((rsi + 0x48)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        ecx = (int32_t) cl;
        eax -= ecx;
    }
    rbp = *(rsi);
    r12 = *(rdx);
    errno_location ();
    *(rax) = 0;
    rsi = r12;
    rdi = rbp;
    void (*0x4ac0)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xabf0 */
 
int32_t dbg_xstrcoll_df_mtime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_mtime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8bc0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xac80 */
 
int32_t dbg_rev_strcmp_df_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_extension(V a,V b); */
    r8 = rdi;
    ecx = *((rsi + 0xa8));
    rdi = rsi;
    edx = *((r8 + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((r8 + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rdx = *(reloc.strcmp);
        rsi = r8;
        void (*0x7180)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xad20 */
 
int32_t dbg_strcmp_df_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_extension(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rdx = *(reloc.strcmp);
        void (*0x7180)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xadc0 */
 
int32_t dbg_rev_xstrcoll_df_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_extension(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x92e0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xae50 */
 
int32_t dbg_rev_strcmp_df_name (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_name(V a,V b); */
    ecx = *((rdi + 0xa8));
    rdx = rsi;
    esi = *((rsi + 0xa8));
    al = (ecx == 9) ? 1 : 0;
    cl = (ecx == 3) ? 1 : 0;
    eax |= ecx;
    if (esi != 3) {
        if (esi == 9) {
            goto label_0;
        }
        ecx = *((rdx + 0xac));
        ecx &= 0xf000;
        if (ecx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        ecx = *((rdi + 0xac));
        ecx &= 0xf000;
        cl = (ecx == sym._init) ? 1 : 0;
        ecx = (int32_t) cl;
        eax -= ecx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rsi = *(rdi);
        rdi = *(rdx);
        void (*0x4660)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xaef0 */
 
int32_t dbg_xstrcoll_df_width (int64_t arg_c4h, int64_t arg_c8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_width(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x9110)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xaf80 */
 
int32_t dbg_strcmp_df_width (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_width(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x9370)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb010 */
 
int32_t dbg_rev_xstrcoll_df_width (int64_t arg_c4h, int64_t arg_c8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_width(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x9080)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb0a0 */
 
int32_t dbg_xstrcoll_df_ctime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_ctime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8970)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb130 */
 
int32_t dbg_xstrcoll_df_btime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_btime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8c30)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb1c0 */
 
int64_t dbg_strcmp_df_atime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_atime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rcx + 0x60));
        esi = 0;
        al = (*((rdx + 0x60)) > rax) ? 1 : 0;
        sil = (*((rdx + 0x60)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rcx + 0x68));
        al = (*((rdx + 0x68)) < rax) ? 1 : 0;
        dil = (*((rdx + 0x68)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rcx);
    rdi = *(rdx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xb290 */
 
int32_t dbg_rev_xstrcoll_df_atime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_atime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8ca0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb320 */
 
int32_t dbg_xstrcoll_df_extension (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_extension(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x9250)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb3b0 */
 
int32_t dbg_rev_xstrcoll_df_version (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_version(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rbp = *(rsi);
        r12 = *(rdi);
        eax = filevercmp (rbp, r12, rdx, rcx);
        if (eax == 0) {
            rsi = r12;
            rdi = rbp;
            void (*0x4660)() ();
        }
        return eax;
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xff50 */
 
void dbg_filevercmp (int64_t arg_1h, int64_t arg_2h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    /* int filevercmp(char const * s1,char const * s2); */
    rdx = rsi;
    rcx = 0xffffffffffffffff;
    rsi = 0xffffffffffffffff;
    return void (*0xfdb0)() ();
}

/* /tmp/tmprboi4m6a @ 0xb480 */
 
int32_t dbg_xstrcoll_df_atime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_atime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8b50)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb510 */
 
int64_t dbg_strcmp_df_mtime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int strcmp_df_mtime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rcx + 0x70));
        esi = 0;
        al = (*((rdx + 0x70)) > rax) ? 1 : 0;
        sil = (*((rdx + 0x70)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rcx + 0x78));
        al = (*((rdx + 0x78)) < rax) ? 1 : 0;
        dil = (*((rdx + 0x78)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rcx);
    rdi = *(rdx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xb5e0 */
 
int32_t dbg_rev_xstrcoll_df_mtime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_xstrcoll_df_mtime(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x8a60)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb670 */
 
int32_t dbg_rev_strcmp_df_width (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_width(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        void (*0x88f0)() ();
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb700 */
 
int32_t dbg_xstrcoll_df_version (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int xstrcoll_df_version(V a,V b); */
    edx = *((rdi + 0xa8));
    ecx = *((rsi + 0xa8));
    al = (edx == 9) ? 1 : 0;
    dl = (edx == 3) ? 1 : 0;
    eax |= edx;
    if (ecx != 3) {
        if (ecx == 9) {
            goto label_0;
        }
        edx = *((rsi + 0xac));
        edx &= 0xf000;
        if (edx == sym._init) {
            goto label_0;
        }
        if (al == 0) {
            goto label_1;
        }
        eax = 0xffffffff;
        return eax;
    }
label_0:
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    do {
        edx = *((rdi + 0xac));
        edx &= 0xf000;
        dl = (edx == sym._init) ? 1 : 0;
        edx = (int32_t) dl;
        eax -= edx;
        if (eax != 0) {
            return eax;
        }
label_2:
        rbp = *(rdi);
        r12 = *(rsi);
        eax = filevercmp (rbp, r12, rdx, rcx);
        if (eax == 0) {
            rsi = r12;
            rdi = rbp;
            void (*0x4660)() ();
        }
        return eax;
label_1:
        eax = 0;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0xb7d0 */
 
int64_t dbg_xstrcoll_df_size (int64_t arg1, int64_t arg2, int32_t diff) {
    rdi = arg1;
    rsi = arg2;
    rax = diff;
    /* int xstrcoll_df_size(V a,V b); */
    rdx = rdi;
    edi = *((rsi + 0xa8));
    ecx = *((rdx + 0xa8));
    al = (ecx == 9) ? 1 : 0;
    cl = (ecx == 3) ? 1 : 0;
    eax |= ecx;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        ecx = *((rsi + 0xac));
        ecx &= 0xf000;
        if (ecx == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    ecx = *((rdx + 0xac));
    ecx &= 0xf000;
    cl = (ecx == sym._init) ? 1 : 0;
    ecx = (int32_t) cl;
    eax -= ecx;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rsi + 0x48));
        al = (*((rdx + 0x48)) < rax) ? 1 : 0;
        cl = (*((rdx + 0x48)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        ecx = (int32_t) cl;
        eax -= ecx;
    }
    rbp = *(rdx);
    r12 = *(rsi);
    errno_location ();
    *(rax) = 0;
    rsi = r12;
    rdi = rbp;
    void (*0x4ac0)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xb8a0 */
 
int64_t dbg_rev_strcmp_df_ctime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_ctime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x80));
        esi = 0;
        al = (*((rcx + 0x80)) > rax) ? 1 : 0;
        sil = (*((rcx + 0x80)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rdx + 0x88));
        al = (*((rcx + 0x88)) < rax) ? 1 : 0;
        dil = (*((rcx + 0x88)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rdx);
    rdi = *(rcx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xb980 */
 
int64_t dbg_rev_strcmp_df_atime (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int rev_strcmp_df_atime(V a,V b); */
    rdx = rdi;
    rcx = rsi;
    edi = *((rsi + 0xa8));
    esi = *((rdx + 0xa8));
    al = (esi == 9) ? 1 : 0;
    sil = (esi == 3) ? 1 : 0;
    eax |= esi;
    if (edi != 3) {
        if (edi == 9) {
            goto label_1;
        }
        esi = *((rcx + 0xac));
        esi &= 0xf000;
        if (esi == sym._init) {
            goto label_1;
        }
        if (al == 0) {
            goto label_2;
        }
        eax = 0xffffffff;
        return eax;
    }
label_1:
    if (al != 0) {
        goto label_3;
    }
    eax = 1;
label_0:
    esi = *((rdx + 0xac));
    esi &= 0xf000;
    sil = (esi == sym._init) ? 1 : 0;
    esi = (int32_t) sil;
    eax -= esi;
    while (eax != 0) {
        return eax;
label_3:
        rax = *((rdx + 0x60));
        esi = 0;
        al = (*((rcx + 0x60)) > rax) ? 1 : 0;
        sil = (*((rcx + 0x60)) < rax) ? 1 : 0;
        eax = (int32_t) al;
        esi -= eax;
        rax = *((rdx + 0x68));
        al = (*((rcx + 0x68)) < rax) ? 1 : 0;
        dil = (*((rcx + 0x68)) > rax) ? 1 : 0;
        eax = (int32_t) al;
        edi = (int32_t) dil;
        eax -= edi;
        eax = rax + rsi*2;
    }
    rsi = *(rdx);
    rdi = *(rcx);
    void (*0x4660)() ();
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0xba50 */
 
int64_t dbg_quote_name (int64_t arg_2080h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    char const * absolute_name;
    _Bool pad;
    char * buf;
    char[8192] smallbuf;
    int64_t var_2h;
    int64_t var_2038h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* size_t quote_name(char const * name,quoting_options const * options,int needs_general_quoting,bin_str const * color,_Bool allow_pad,obstack * stack,char const * absolute_name); */
    r14 = *((rsp + 0x2080));
    r12 = rdi;
    r13 = rcx;
    r15d = r8d;
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x2038)) = rax;
    eax = 0;
    r8d = 0;
    rax = rsp + 0x30;
    rdx = rsi;
    rbx = r9;
    rdi = rsp + 0x28;
    r9 = rsp + 0x27;
    rsi = r12;
    *(rsp) = rax;
    *((rsp + 0x28)) = rax;
    rax = quote_name_buf_constprop_0 ();
    if (*((rsp + 0x27)) != 0) {
        if (r15b != 0) {
            goto label_8;
        }
    }
label_3:
    if (r13 != 0) {
        al = is_colored (4);
        r8 = obj_color_indicator;
        r15 = r8 + 0x10;
        if (al != 0) {
            goto label_9;
        }
label_4:
        put_indicator (r8);
        put_indicator (r13);
        put_indicator (r15);
    }
    if (r14 == 0) {
        goto label_10;
    }
    r13d = *(obj.align_variable_outer_quotes);
    while (r13b == 0) {
        r10 = rbp;
        r9d = 0;
label_0:
        *((rsp + 0x18)) = r10;
        *((rsp + 0x10)) = r9;
        rax = file_escape (*(obj.hostname), 0);
        r15 = rax;
        rax = file_escape (r14, 1);
        rcx = 0x0001bb0a;
        rdx = r15;
        rsi = "\e]8;;file://%s%s%s\a";
        r8 = rax;
        rax = 0x0001bdf2;
        edi = 1;
        if (*(rax) != 0x2f) {
            rcx = rax;
        }
        eax = 0;
        *((rsp + 8)) = r8;
        printf_chk ();
        rdi = r15;
        fcn_00004630 ();
        rdi = *((rsp + 8));
        fcn_00004630 ();
        r9 = *((rsp + 0x10));
        r10 = *((rsp + 0x18));
        if (rbx == 0) {
            goto label_11;
        }
label_1:
        if (*(obj.dired) != 0) {
            rdx = *((rbx + 0x18));
            rax = *((rbx + 0x20));
            rax -= rdx;
            if (rax <= 7) {
                goto label_12;
            }
label_5:
            rax = dired_pos;
            *(rdx) = rax;
            *((rbx + 0x18)) += 8;
        }
        rdi = *((rsp + 0x28));
        rcx = stdout;
        rdx = r10;
        esi = 1;
        rdi += r9;
        fwrite_unlocked ();
        *(obj.dired_pos) += rbp;
        if (*(obj.dired) != 0) {
            rdx = *((rbx + 0x18));
            rax = *((rbx + 0x20));
            rax -= rdx;
            if (rax <= 7) {
                goto label_13;
            }
label_6:
            rax = dired_pos;
            *(rdx) = rax;
            *((rbx + 0x18)) += 8;
        }
label_2:
        if (r14 != 0) {
            rcx = stdout;
            edx = 6;
            esi = 1;
            rdi = "\e]8;;\a";
            fwrite_unlocked ();
            if (r13b == 0) {
                goto label_7;
            }
            rax = *((rsp + 0x28));
            rdi = stdout;
            edx = *((rax + rbp - 1));
            rax = *((rdi + 0x28));
            if (rax >= *((rdi + 0x30))) {
                goto label_14;
            }
            rcx = rax + 1;
            *((rdi + 0x28)) = rcx;
            *(rax) = dl;
        }
label_7:
        rdi = *((rsp + 0x28));
        if (rdi != r12) {
            if (rdi == *(rsp)) {
                goto label_15;
            }
            fcn_00004630 ();
        }
label_15:
        eax = *((rsp + 0x27));
        rax += rbp;
        rdx = *((rsp + 0x2038));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_16;
        }
        return rax;
        r13d = *(obj.cwd_some_quoted);
    }
    if (*((rsp + 0x27)) != 0) {
        goto label_17;
    }
    rax = *((rsp + 0x28));
    rdi = stdout;
    r10 = rbp - 2;
    edx = *(rax);
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_18;
    }
    rcx = rax + 1;
    r9d = 1;
    *((rdi + 0x28)) = rcx;
    *(rax) = dl;
    goto label_0;
label_10:
    r10 = rbp;
    r9d = 0;
    r13d = 0;
    if (rbx != 0) {
        goto label_1;
    }
label_11:
    rdi = *((rsp + 0x28));
    rcx = stdout;
    rdx = r10;
    esi = 1;
    rdi += r9;
    fwrite_unlocked ();
    *(obj.dired_pos) += rbp;
    goto label_2;
label_8:
    rdi = stdout;
    *(obj.dired_pos)++;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0x20;
        goto label_3;
label_9:
        put_indicator (r8);
        put_indicator (r15);
        r8 = r15 - 0x10;
        goto label_4;
label_12:
        *((rsp + 0x10)) = r10;
        *((rsp + 8)) = r9;
        _obstack_newchunk (rbx, 8, rdx, rcx);
        rdx = *((rbx + 0x18));
        r10 = *((rsp + 0x10));
        r9 = *((rsp + 8));
        goto label_5;
label_13:
        _obstack_newchunk (rbx, 8, rdx, rcx);
        rdx = *((rbx + 0x18));
        goto label_6;
label_17:
        r10 = rbp;
        r9d = 0;
        r13d = 0;
        goto label_0;
label_14:
        esi = (int32_t) dl;
        overflow ();
        goto label_7;
    }
    esi = 0x20;
    overflow ();
    goto label_3;
label_18:
    esi = (int32_t) dl;
    *((rsp + 8)) = r10;
    overflow ();
    r10 = *((rsp + 8));
    r9d = 1;
    goto label_0;
label_16:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xbe50 */
 
void dbg_print_name_with_quoting (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* size_t print_name_with_quoting(fileinfo const * f,_Bool symlink_target,obstack * stack,size_t start_col); */
    r15 = rdx;
    r14d = esi;
    r12 = rdi;
    eax = *(obj.print_with_color);
    if (sil == 0) {
        goto label_11;
    }
    r13 = *((rdi + 8));
    while (al == 0) {
label_0:
        r14d ^= 1;
        r8d = (int32_t) r14b;
        rax = quote_name (r13, *(obj.filename_quoting_options), *((r12 + 0xc4)), 0, r8, r15);
        r12 = rax;
        process_signals ();
label_1:
        rax = r12;
        return rax;
        esi = *((rdi + 0xac));
        if (*((rdi + 0xb9)) != 0) {
            goto label_12;
        }
        al = is_colored (0xc);
        if (al != 0) {
            goto label_13;
        }
        eax = 0xffffffff;
        goto label_3;
label_11:
        r13 = *(rdi);
    }
    eax = *((rdi + 0xb9));
    if (*(obj.color_symlink_as_referent) != 0) {
        goto label_14;
    }
label_2:
    esi = *((r12 + 0x30));
label_3:
    ecx = *((r12 + 0xb8));
    if (cl != 0) {
        goto label_15;
    }
label_4:
    ecx = *((r12 + 0xa8));
    rdx = obj_filetype_indicator_5;
    edx = *((rdx + rcx*4));
    cl = (edx == 7) ? 1 : 0;
    if (edx == 5) {
        goto label_9;
    }
label_8:
    if (eax != 0) {
        goto label_16;
    }
    if (cl == 0) {
        goto label_16;
    }
    ecx = 0xd0;
    if (*(obj.color_symlink_as_referent) == 0) {
        al = is_colored (0xd);
        rcx -= rcx;
        rcx &= 0xffffffffffffffa0;
        rcx += 0xd0;
    }
label_5:
    rax = obj_color_indicator;
    rcx += rax;
label_7:
    if (*((rcx + 8)) != 0) {
        goto label_17;
    }
    al = is_colored (4);
    if (al == 0) {
        goto label_0;
    }
label_17:
    r14d ^= 1;
    r8d = (int32_t) r14b;
    rax = quote_name (r13, *(obj.filename_quoting_options), *((r12 + 0xc4)), 0, r8, r15);
    r12 = rax;
    process_signals ();
    if (*(0x00025088) == 0) {
        goto label_18;
    }
    put_indicator (0x00025080);
label_6:
    rcx = line_length;
    if (rcx == 0) {
        goto label_1;
    }
    rax = rbp;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    edx = 0;
    rsi = rax;
    rax = r12 + rbp - 1;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rsi == rax) {
        goto label_1;
    }
    al = put_indicator (0x000251d0);
    goto label_1;
label_14:
    if (al == 0) {
        goto label_2;
    }
    esi = *((r12 + 0xac));
    goto label_3;
label_12:
    ecx = *((r12 + 0xb8));
    eax = 0;
    if (cl == 0) {
        goto label_4;
    }
label_15:
    edx = esi;
    edx &= 0xf000;
    if (edx == 0x8000) {
        goto label_19;
    }
    if (edx == sym._init) {
        goto label_20;
    }
    if (edx == 0xa000) {
        goto label_21;
    }
    ecx = 0x80;
    if (edx == 0x1000) {
        goto label_5;
    }
    ecx = 0x90;
    if (edx == 0xc000) {
        goto label_5;
    }
    ecx = 0xa0;
    if (edx == 0x6000) {
        goto label_5;
    }
    ecx = 0xb0;
    eax = 0xd0;
    if (edx != 0x2000) {
        rcx = rax;
    }
    goto label_5;
label_18:
    put_indicator (obj.color_indicator);
    put_indicator (0x00025090);
    put_indicator (0x00025070);
    goto label_6;
label_9:
    rax = strlen (r13);
    rbx = color_ext_list;
    rcx = rax;
    if (rbx == 0) {
        goto label_22;
    }
    do {
        rdx = *(rbx);
        if (rcx >= rdx) {
            rdi = rcx;
            rsi = *((rbx + 8));
            *((rsp + 8)) = rcx;
            rdi -= rdx;
            rdi += r13;
            eax = c_strncasecmp (rdi);
            rcx = *((rsp + 8));
            if (eax == 0) {
                goto label_23;
            }
        }
        rbx = *((rbx + 0x20));
    } while (rbx != 0);
label_22:
    ecx = 0x50;
    goto label_5;
label_23:
    rcx = rbx + 0x10;
    goto label_7;
label_13:
    ecx = 0xc0;
    goto label_5;
label_19:
    if ((esi & 0x800) != 0) {
        al = is_colored (0x10);
        if (al == 0) {
            goto label_24;
        }
        ecx = 0x100;
        goto label_5;
    }
label_24:
    while (al == 0) {
        al = is_colored (0x15);
        if (al == 0) {
            goto label_25;
        }
        if (*((r12 + 0xc0)) == 0) {
            goto label_25;
        }
        ecx = 0x150;
        goto label_5;
        al = is_colored (0x11);
    }
    ecx = 0x110;
    goto label_5;
label_20:
    eax = esi;
    eax &= 0x202;
    if (eax == 0x202) {
        goto label_26;
    }
label_10:
    if ((sil & 2) == 0) {
        goto label_27;
    }
    al = is_colored (0x13);
    ecx = 0x130;
    if (al != 0) {
        goto label_5;
    }
label_27:
    esi &= 0x200;
    ecx = 0x60;
    if (esi == 0) {
        goto label_5;
    }
    al = is_colored (0x12);
    rcx -= rcx;
    cl &= 0x40;
    rcx += 0x120;
    goto label_5;
label_21:
    edx = 7;
    goto label_8;
label_25:
    esi &= 0x49;
    if (esi != 0) {
        al = is_colored (0xe);
        if (al == 0) {
            goto label_28;
        }
        ecx = 0xe0;
        goto label_5;
    }
label_28:
    if (*((r12 + 0x28)) <= 1) {
        goto label_9;
    }
    al = is_colored (0x16);
    if (al == 0) {
        goto label_9;
    }
    ecx = 0x160;
    goto label_5;
label_26:
    al = is_colored (0x14);
    ecx = 0x140;
    if (al != 0) {
        goto label_5;
    }
    goto label_10;
label_16:
    rcx = rdx;
    rcx <<= 4;
    goto label_5;
}

/* /tmp/tmprboi4m6a @ 0xc300 */
 
int64_t print_file_name_and_frills_isra_0 (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_298h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x298)) = rax;
    eax = 0;
    if (*(obj.print_with_color) != 0) {
        al = is_colored (4);
        if (al != 0) {
            goto label_4;
        }
    }
label_3:
    while (1) {
        if (*(obj.print_block_size) != 0) {
            rcx = 0x0001bb0b;
            if (*((rbx + 0xb8)) != 0) {
                goto label_5;
            }
label_2:
            edx = 0;
            if (*(obj.format) != 4) {
                edx = block_size_width;
            }
            rsi = 0x0001bb1d;
            edi = 1;
            eax = 0;
            eax = printf_chk ();
        }
        if (*(obj.print_scontext) != 0) {
            edx = 0;
            rcx = *((rbx + 0xb0));
            if (*(obj.format) != 4) {
                edx = scontext_width;
            }
            rsi = 0x0001bb1d;
            edi = 1;
            eax = 0;
            printf_chk ();
        }
        print_name_with_quoting (rbx, 0, 0, r12);
        eax = indicator_style;
        if (eax != 0) {
            goto label_6;
        }
label_0:
        rax = *((rsp + 0x298));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        return rax;
        rcx = 0x0001bb0b;
        if (*((rbx + 0xb8)) != 0) {
            goto label_8;
        }
label_1:
        edx = 0;
        if (*(obj.format) != 4) {
            edx = inode_number_width;
        }
        rsi = 0x0001bb1d;
        edi = 1;
        eax = 0;
        printf_chk ();
    }
label_6:
    edx = *((rbx + 0xa8));
    esi = *((rbx + 0x30));
    edi = *((rbx + 0xb8));
    al = get_type_indicator ();
    if (al == 0) {
        goto label_0;
    }
    rdi = stdout;
    *(obj.dired_pos)++;
    rdx = *((rdi + 0x28));
    if (rdx >= *((rdi + 0x30))) {
        goto label_9;
    }
    rcx = rdx + 1;
    *((rdi + 0x28)) = rcx;
    *(rdx) = al;
    goto label_0;
label_8:
    rdi = *((rbx + 0x20));
    if (rdi == 0) {
        goto label_1;
    }
    rax = umaxtostr (rdi, rsp, rdx);
    rcx = rax;
    goto label_1;
label_5:
    rax = human_readable (*((rbx + 0x58)), rsp, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    rcx = rax;
    goto label_2;
label_4:
    put_indicator (obj.color_indicator);
    put_indicator (0x000250a0);
    al = put_indicator (0x00025070);
    goto label_3;
label_9:
    esi = (int32_t) al;
    overflow ();
    goto label_0;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xc530 */
 
int64_t dbg_print_with_separator (int64_t arg2) {
    int64_t var_bh;
    int64_t var_ch;
    rsi = arg2;
    /* void print_with_separator(char sep); */
    if (*(obj.cwd_n_used) == 0) {
        goto label_3;
    }
    r15d = (int32_t) dil;
    r13d = 0;
    ebx = 0;
    r14d = r15d;
    while (*(obj.line_length) == 0) {
        if (rbx != 0) {
            goto label_4;
        }
label_0:
        rsi = rbp;
        rdi = r12;
        rbx++;
        print_file_name_and_frills_isra_0 ();
        if (*(obj.cwd_n_used) <= rbx) {
            goto label_3;
        }
        rax = sorted_file;
        r12 = *((rax + rbx*8));
    }
    rax = length_of_file_name_and_frills (r12, rsi, rdx, rcx, r8, r9);
    if (rbx == 0) {
        goto label_5;
    }
    rdx = line_length;
    rbp = r13 + 2;
    rcx = rax + rbp;
    if (rdx != 0) {
        if (rdx <= rcx) {
            goto label_6;
        }
        rdx = 0xfffffffffffffffd;
        rdx -= rax;
        if (rdx < r13) {
            goto label_6;
        }
    }
    r13 = rcx;
    do {
        ecx = 0x20;
        edx = 0x20;
label_1:
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_7;
        }
        rsi = rax + 1;
        *((rdi + 0x28)) = rsi;
        *(rax) = r14b;
label_2:
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_8;
        }
        rcx = rax + 1;
        *((rdi + 0x28)) = rcx;
        *(rax) = dl;
        goto label_0;
label_4:
        rbp = r13 + 2;
        r13 = rbp;
    } while (1);
label_6:
    ecx = *(obj.eolbyte);
    r13 = rax;
    ebp = 0;
    edx = ecx;
    goto label_1;
label_3:
    rdi = stdout;
    edx = *(obj.eolbyte);
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rcx = rax + 1;
        *((rdi + 0x28)) = rcx;
        *(rax) = dl;
        return rax;
label_5:
        r13 += rax;
        goto label_0;
label_7:
        esi = r15d;
        *((rsp + 0xc)) = ecx;
        *((rsp + 0xb)) = dl;
        overflow ();
        edx = *((rsp + 0xb));
        ecx = *((rsp + 0xc));
        goto label_2;
label_8:
        esi = (int32_t) cl;
        overflow ();
        goto label_0;
    }
    esi = (int32_t) dl;
    return overflow ();
}

/* /tmp/tmprboi4m6a @ 0xc6f0 */
 
int64_t print_long_format (uint32_t arg_8h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_34h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_a8h, int64_t arg_ach, int64_t arg_b0h, uint32_t arg_b8h, int64_t arg_bch, uint32_t arg1) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_b4h;
    int64_t var_b5h;
    int64_t var_bdh;
    int64_t var_beh;
    int64_t var_bfh;
    int64_t var_c0h;
    int64_t var_e0h;
    int64_t var_4d0h;
    int64_t var_1318h;
    rdi = arg1;
    rax = *(fs:0x28);
    *((rsp + 0x1318)) = rax;
    eax = 0;
    if (*((rdi + 0xb8)) == 0) {
        goto label_26;
    }
    filemodestring (rdi + 0x18, rsp + 0xb4);
label_3:
    if (*(obj.any_has_acl) != 0) {
        goto label_27;
    }
    *((rsp + 0xbe)) = 0;
label_1:
    eax = time_type;
    if (eax == 2) {
        goto label_28;
    }
    if (eax > 2) {
label_2:
        goto label_29;
    }
    if (eax == 0) {
        goto label_30;
    }
    rax = *((rbp + 0x88));
    rdx = *((rbp + 0x80));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
label_0:
    r12 = rsp + 0x4d0;
    rbx = r12;
    if (*(obj.print_inode) != 0) {
        goto label_31;
    }
label_9:
    if (*(obj.print_block_size) == 0) {
        goto label_32;
    }
    r15 = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        goto label_33;
    }
label_15:
    r14d = block_size_width;
    eax = gnu_mbswidth (r15, 0);
    r8d = eax;
    eax = r14d;
    eax -= r8d;
    if (eax <= 0) {
        goto label_34;
    }
    r14 = (int64_t) eax;
    rbx += r14;
    memset (rbx, 0x20, r14);
    do {
label_34:
        eax = *(r15);
        r15++;
        rbx++;
        *((rbx - 1)) = al;
    } while (al != 0);
    *((rbx - 1)) = 0x20;
label_32:
    rax = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        goto label_35;
    }
label_8:
    rdi = rbx;
    r9d = nlink_width;
    rcx = "%s %*s ";
    rdx = 0xffffffffffffffff;
    eax = 0;
    esi = 1;
    r8 = rsp + 0xc4;
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rbx += rax;
    if (*(obj.dired) != 0) {
        goto label_36;
    }
label_7:
    if (*(obj.print_owner) == 0) {
        if (*(obj.print_group) == 0) {
            goto label_37;
        }
    }
label_4:
    rsi = rbx;
    rsi -= r12;
    dired_outbuf (r12);
    if (*(obj.print_owner) != 0) {
        goto label_38;
    }
    if (*(obj.print_group) != 0) {
        goto label_39;
    }
label_10:
    if (*(obj.print_author) != 0) {
        goto label_40;
    }
label_11:
    rbx = r12;
    if (*(obj.print_scontext) != 0) {
        goto label_41;
    }
label_5:
    if (*((rbp + 0xb8)) == 0) {
        goto label_42;
    }
    eax = *((rbp + 0x30));
    eax &= 0xb000;
    if (eax == 0x2000) {
        goto label_43;
    }
    rax = human_readable (*((rbp + 0x48)), rsp + 0xe0, *(obj.file_human_output_opts), 1, *(obj.file_output_block_size), r9);
    r15 = rax;
    goto label_44;
label_29:
    if (eax != 3) {
        goto label_45;
    }
    rax = *((rbp + 0x70));
    rdx = *((rbp + 0x78));
    *((rsp + 0x20)) = rax;
    rax &= rdx;
    *((rsp + 0x28)) = rdx;
    r13b = (rax != -1) ? 1 : 0;
    goto label_0;
label_27:
    eax = *((rbp + 0xbc));
    if (eax == 1) {
        goto label_46;
    }
    if (eax != 2) {
        goto label_1;
    }
    eax = time_type;
    *((rsp + 0xbe)) = 0x2b;
    if (eax != 2) {
        goto label_2;
    }
label_28:
    rax = *((rbp + 0x68));
    rdx = *((rbp + 0x60));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
    goto label_0;
label_26:
    eax = *((rdi + 0xa8));
    rdx = "?pcdb-lswd";
    r9d = 0x3f3f;
    *((rsp + 0xbf)) = 0;
    *((rsp + 0xbd)) = r9w;
    eax = *((rdx + rax));
    *((rsp + 0xb4)) = al;
    rax = 0x3f3f3f3f3f3f3f3f;
    *((rsp + 0xb5)) = rax;
    goto label_3;
label_42:
    r15 = 0x0001bb0b;
label_44:
    r14d = file_size_width;
    eax = gnu_mbswidth (r15, 0);
    r8d = eax;
    eax = r14d;
    eax -= r8d;
    if (eax <= 0) {
        goto label_47;
    }
    r14 = (int64_t) eax;
    rbx += r14;
    memset (rbx, 0x20, r14);
    do {
label_47:
        eax = *(r15);
        r15++;
        rbx++;
        *((rbx - 1)) = al;
    } while (al != 0);
    *((rbx - 1)) = 0x20;
label_13:
    *(rbx) = 1;
    if (*((rbp + 0xb8)) != 0) {
        if (r13b != 0) {
            goto label_48;
        }
    }
label_21:
    r13 = 0x0001bb0b;
label_22:
    r8d = width.2;
    if (r8d < 0) {
        goto label_49;
    }
label_19:
    r9 = r13;
    rcx = 0x0001bb1d;
    rdi = rbx;
    eax = 0;
    rdx = 0xffffffffffffffff;
    esi = 1;
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rax += rbx;
label_14:
    rax -= r12;
    rbx = rax;
    rsi = rax;
    dired_outbuf (r12);
    rax = print_name_with_quoting (rbp, 0, obj.dired_obstack, rbx);
    edx = *((rbp + 0xa8));
    r12 = rax;
    if (edx == 6) {
        goto label_50;
    }
    eax = indicator_style;
    if (eax != 0) {
        goto label_51;
    }
label_6:
    rax = *((rsp + 0x1318));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_52;
    }
    return rax;
label_37:
    if (*(obj.print_author) != 0) {
        goto label_4;
    }
    if (*(obj.print_scontext) == 0) {
        goto label_5;
    }
    goto label_4;
label_50:
    if (*((rbp + 8)) == 0) {
        goto label_6;
    }
    esi = 4;
    dired_outbuf (" -> ");
    print_name_with_quoting (rbp, 1, 0, rbx + r12 + 4);
    edx = indicator_style;
    if (edx == 0) {
        goto label_6;
    }
    esi = *((rbp + 0xac));
    edx = 0;
    edi = 1;
    al = get_type_indicator ();
    if (al == 0) {
        goto label_6;
    }
label_12:
    rdi = stdout;
    *(obj.dired_pos)++;
    rdx = *((rdi + 0x28));
    if (rdx >= *((rdi + 0x30))) {
        goto label_53;
    }
    rcx = rdx + 1;
    *((rdi + 0x28)) = rcx;
    *(rdx) = al;
    goto label_6;
label_36:
    esi = 2;
    dired_outbuf (0x0001bb15);
    goto label_7;
label_35:
    umaxtostr (*((rbp + 0x28)), rsp + 0xe0, rdx);
    goto label_8;
label_31:
    r9 = 0x0001bb0b;
    if (*((rbp + 0xb8)) != 0) {
        rdi = *((rbp + 0x20));
        if (rdi == 0) {
            goto label_54;
        }
        rax = umaxtostr (rdi, rsp + 0xe0, rdx);
        r9 = rax;
    }
label_54:
    r12 = rsp + 0x4d0;
    r8d = inode_number_width;
    edx = 0xe3b;
    eax = 0;
    rcx = 0x0001bb1d;
    esi = 1;
    rdi = r12;
    eax = sprintf_chk ();
    rbx = (int64_t) eax;
    rbx += r12;
    goto label_9;
label_38:
    edx = owner_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x34));
    if (*((rbp + 0xb8)) != 0) {
        goto label_55;
    }
label_16:
    format_user_or_group ();
    if (*(obj.print_group) == 0) {
        goto label_10;
    }
label_39:
    edx = group_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x38));
    if (*((rbp + 0xb8)) != 0) {
        goto label_56;
    }
label_17:
    format_user_or_group ();
    if (*(obj.print_author) == 0) {
        goto label_11;
    }
label_40:
    edx = author_width;
    rdi = 0x0001bb0b;
    esi = *((rbp + 0x34));
    if (*((rbp + 0xb8)) != 0) {
        goto label_57;
    }
label_18:
    format_user_or_group ();
    rbx = r12;
    if (*(obj.print_scontext) == 0) {
        goto label_5;
    }
label_41:
    rdi = *((rbp + 0xb0));
    edx = scontext_width;
    esi = 0;
    format_user_or_group ();
    goto label_5;
label_30:
    rax = *((rbp + 0x78));
    rdx = *((rbp + 0x70));
    r13d = 1;
    *((rsp + 0x20)) = rdx;
    *((rsp + 0x28)) = rax;
    goto label_0;
label_51:
    esi = *((rbp + 0x30));
    edi = *((rbp + 0xb8));
    al = get_type_indicator ();
    if (al == 0) {
        goto label_6;
    }
    goto label_12;
label_46:
    *((rsp + 0xbe)) = 0x2e;
    goto label_1;
label_43:
    eax = minor_device_number_width;
    r14d = file_size_width;
    eax = rdx + rax + 2;
    r14d -= eax;
    rax = *((rbp + 0x40));
    edx = (int32_t) al;
    rax >>= 0xc;
    dil = 0;
    edi |= edx;
    rax = umaxtostr (rax, rsp + 0xe0, *(obj.major_device_number_width));
    r15 = rax;
    rax = *((rbp + 0x40));
    *((rsp + 4)) = edx;
    rcx = rax;
    rax >>= 0x20;
    rcx >>= 8;
    ecx &= 0xfff;
    edi &= 0xfffff000;
    edi |= ecx;
    rax = umaxtostr (rax, rsp + 0xc0, *(obj.minor_device_number_width));
    edx = *((rsp + 0xc));
    esi = 1;
    r9 = rax;
    eax = 0;
    rdi = rbx;
    __asm ("cmovns eax, r14d");
    rcx = "%*s, %*s ";
    rdx = 0xffffffffffffffff;
    r8d = eax;
    eax = 0;
    r8d += *(obj.major_device_number_width);
    rax = sprintf_chk ();
    rax = (int64_t) eax;
    rbx += rax;
    goto label_13;
label_48:
    r13 = rsp + 0x30;
    rax = localtime_rz (*(obj.localtz), rsp + 0x20, r13);
    if (rax == 0) {
        goto label_58;
    }
    r8d = 0;
    r9 = *((rsp + 0x28));
    rdx = *((rsp + 0x20));
    rcx = current_time;
    r8b = (*(0x00026378) > r9) ? 1 : 0;
    al = (*(0x00026378) < r9) ? 1 : 0;
    edi = 0;
    eax = (int32_t) al;
    esi = r8d;
    esi -= eax;
    r10b = (rcx < rdx) ? 1 : 0;
    dil = (rcx > rdx) ? 1 : 0;
    r10d = (int32_t) r10b;
    edi -= r10d;
    edi = rsi + rdi*2;
    if (edi < 0) {
        goto label_59;
    }
label_25:
    rdi = rcx - 0xf0c2ac;
    r10b = (rcx > rdx) ? 1 : 0;
    cl = (rcx < rdx) ? 1 : 0;
    eax -= r8d;
    r8 = localtz;
    ecx = (int32_t) cl;
    r10d = (int32_t) r10b;
    ecx -= r10d;
    dl = (rdi > rdx) ? 1 : 0;
    eax = rax + rcx*2;
    cl = (rdi < rdx) ? 1 : 0;
    edx = (int32_t) dl;
    ecx = (int32_t) cl;
    edx -= ecx;
    edx = rsi + rdx*2;
    eax &= edx;
    edx = eax;
    edx >>= 0x1f;
    if (*(obj.use_abformat) == 0) {
        goto label_60;
    }
    rax = (int64_t) eax;
    rdx = *((rsp + 0x40));
    rax >>= 0x3f;
    eax &= 0xc;
    rax += rdx;
    rdx = obj_abformat;
    rax <<= 7;
    rdx += rax;
label_23:
    rcx = r13;
    rax = nstrftime (rbx, 0x3e9);
    if (rax == 0) {
        goto label_58;
    }
    rbx += rax;
label_20:
    *(rbx) = 0x20;
    rax = rbx + 1;
    goto label_14;
label_33:
    rax = human_readable (*((rbp + 0x58)), rsp + 0xe0, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    r15 = rax;
    goto label_15;
label_55:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_16;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getuser (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_16;
label_56:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_17;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getgroup (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_17;
label_57:
    edi = 0;
    if (*(obj.numeric_ids) != 0) {
        goto label_18;
    }
    *((rsp + 8)) = rsi;
    *((rsp + 4)) = edx;
    rax = getuser (esi);
    rsi = *((rsp + 8));
    edx = *((rsp + 4));
    rdi = rax;
    goto label_18;
label_49:
    r14 = rsp + 0x70;
    *((rsp + 0x18)) = 0;
    rax = localtime_rz (*(obj.localtz), rsp + 0x18, r14);
    if (rax != 0) {
        r8 = localtz;
        rdx = long_time_format;
        if (*(obj.use_abformat) != 0) {
            rdx = *((rsp + 0x80));
            rax = obj_abformat;
            rdx <<= 7;
            rdx += rax;
        }
        r15 = rsp + 0xe0;
        r9d = 0;
        rcx = r14;
        rax = nstrftime (r15, 0x3e9);
        if (rax != 0) {
            goto label_61;
        }
    }
    r8d = width.2;
label_24:
    if (r8d >= 0) {
        goto label_19;
    }
    *(obj.width.2) = 0;
    r8d = 0;
    goto label_19;
label_58:
    if (*(rbx) == 0) {
        goto label_20;
    }
    if (*((rbp + 0xb8)) == 0) {
        goto label_21;
    }
    rax = imaxtostr (*((rsp + 0x20)), rsp + 0xc0, rdx);
    r13 = rax;
    goto label_22;
label_60:
    rax = obj_long_time_format;
    rdx = *((rax + rdx*8));
    goto label_23;
label_61:
    edx = 0;
    eax = mbsnwidth (r15, rax);
    *(obj.width.2) = eax;
    r8d = eax;
    goto label_24;
label_59:
    rdi = obj_current_time;
    gettime ();
    r8d = 0;
    r9 = *((rsp + 0x28));
    r8b = (r9 < *(0x00026378)) ? 1 : 0;
    al = (r9 > *(0x00026378)) ? 1 : 0;
    rdx = *((rsp + 0x20));
    rcx = current_time;
    eax = (int32_t) al;
    esi = r8d;
    esi -= eax;
    goto label_25;
label_53:
    esi = (int32_t) al;
    overflow ();
    goto label_6;
label_52:
    stack_chk_fail ();
label_45:
    return print_long_format_cold ();
}

/* /tmp/tmprboi4m6a @ 0x4c96 */
 
void print_long_format_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0xd120 */
 
int64_t dbg_print_current_files (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg2) {
    int64_t var_bh;
    int64_t var_ch;
    rsi = arg2;
    /* void print_current_files(); */
    if (*(obj.format) > 4) {
        goto label_7;
    }
    rdx = 0x0001a17c;
    eax = format;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0x1a17c */
    void (*rax)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    if (*(obj.line_length) == 0) {
        goto label_8;
    }
    rax = calculate_columns (0);
    *((rsp + 8)) = rax;
    rax = column_info;
    r14 = rax + rdx*8 - 0x18;
    rax = sorted_file;
    rbp = *(rax);
    rax = length_of_file_name_and_frills (*(rax), rsi, rax + rax*2, rcx, r8, r9);
    esi = 0;
    rdi = rbp;
    r15 = rax;
    rax = *((r14 + 0x10));
    r12 = *(rax);
    print_file_name_and_frills_isra_0 ();
    if (*(obj.cwd_n_used) <= 1) {
        goto label_9;
    }
    r13d = 0;
    ebx = 1;
    while (rdx == 0) {
        rdi = stdout;
        edx = *(obj.eolbyte);
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_10;
        }
        rcx = rax + 1;
        r13d = 0;
        *((rdi + 0x28)) = rcx;
        *(rax) = dl;
label_0:
        rax = sorted_file;
        rsi = r13;
        r12 = *((rax + rbx*8));
        rbx++;
        rdi = r12;
        print_file_name_and_frills_isra_0 ();
        rax = length_of_file_name_and_frills (r12, rsi, rdx, rcx, r8, r9);
        r15 = rax;
        rax = *((r14 + 0x10));
        r12 = *((rax + rbp*8));
        if (rbx >= *(obj.cwd_n_used)) {
            goto label_9;
        }
        rax = rbx;
        edx = 0;
        rax = *(rdx:rax) / rsp + 8;
        rdx = *(rdx:rax) % rsp + 8;
    }
    r12 += r13;
    rsi = r12;
    r13 = r12;
    indent (r13 + r15, rsi);
    goto label_0;
    edi = 0x2c;
label_3:
    void (*0xc530)() ();
    r13 = obj_color_indicator;
    ebx = 0;
    r12 = r13 + 0x40;
    rbp = r12 - 0x30;
    if (*(obj.cwd_n_used) != 0) {
        goto label_11;
    }
    goto label_12;
    do {
label_1:
        rax = sorted_file;
        rdi = *((rax + rbx*8));
        print_long_format ();
        rdi = stdout;
        *(obj.dired_pos)++;
        esi = *(obj.eolbyte);
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_13;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = sil;
label_4:
        rbx++;
        if (*(obj.cwd_n_used) <= rbx) {
            goto label_12;
        }
label_11:
    } while (*(obj.print_with_color) == 0);
    al = is_colored (4);
    if (al == 0) {
        goto label_1;
    }
    put_indicator (r13);
    put_indicator (r12);
    put_indicator (rbp);
    goto label_1;
    ebx = 0;
    if (*(obj.cwd_n_used) == 0) {
        goto label_12;
    }
    do {
        rax = sorted_file;
        esi = 0;
        rdi = *((rax + rbx*8));
        print_file_name_and_frills_isra_0 ();
        rdi = stdout;
        esi = *(obj.eolbyte);
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_14;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = sil;
label_5:
        rbx++;
    } while (*(obj.cwd_n_used) > rbx);
    goto label_12;
    if (*(obj.line_length) == 0) {
        goto label_8;
    }
    rax = calculate_columns (1);
    *((rsp + 0x18)) = 0;
    rdx = rax * 3;
    rcx = rax;
    rax = column_info;
    r15 = rax + rdx*8 - 0x18;
    rax = cwd_n_used;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rax -= 0xffffffffffffffff;
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_12;
    }
label_2:
    r12 = *((rsp + 0x18));
    r13d = 0;
    r14d = 0;
    while (r12 < *(obj.cwd_n_used)) {
        rbp += r14;
        rsi = rbp;
        r14 = rbp;
        indent (rbx + r14, rsi);
        rax = sorted_file;
        *((rsp + 8)) = rdi;
        rax = length_of_file_name_and_frills (*((rax + r12*8)), rsi, rdx, rcx, r8, r9);
        rdi = *((rsp + 8));
        rsi = r14;
        rbx = rax;
        rax = *((r15 + 0x10));
        rbp = *((rax + r13));
        r13 += 8;
        print_file_name_and_frills_isra_0 ();
        r12 += *((rsp + 0x10));
    }
    rdi = stdout;
    edx = *(obj.eolbyte);
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_15;
    }
    rcx = rax + 1;
    *((rdi + 0x28)) = rcx;
    *(rax) = dl;
label_6:
    rax = *((rsp + 0x18));
    if (*((rsp + 0x10)) != rax) {
        goto label_2;
    }
    do {
label_12:
        return rax;
label_8:
        edi = 0x20;
        goto label_3;
label_13:
        overflow ();
        goto label_4;
label_14:
        overflow ();
        goto label_5;
label_9:
        rdi = stdout;
        edx = *(obj.eolbyte);
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_16;
        }
        rcx = rax + 1;
        *((rdi + 0x28)) = rcx;
        *(rax) = dl;
    } while (1);
label_15:
    esi = (int32_t) dl;
    overflow ();
    goto label_6;
label_10:
    esi = (int32_t) dl;
    r13d = 0;
    overflow ();
    goto label_0;
label_16:
    esi = (int32_t) dl;
    void (*0x48d0)() ();
label_7:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xd520 */
 
int64_t dbg_print_dir (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg1, int64_t arg2, int64_t arg3, _Bool first) {
    stat dir_stat;
    char[654] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_b1h;
    int64_t var_348h;
    int64_t var_bh;
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = first;
    /* void print_dir(char const * name,char const * realname,_Bool command_line_arg); */
    r14 = rsi;
    r13 = rdi;
    ebx = edx;
    *((rsp + 0x1c)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x348)) = rax;
    eax = 0;
    errno_location ();
    rdi = r13;
    *(rax) = 0;
    rax = opendir ();
    if (rax == 0) {
        goto label_12;
    }
    r12 = rax;
    if (*(obj.active_dir_set) != 0) {
        rdi = rax;
        eax = dirfd ();
        rdx = rsp + 0x20;
        r8d = 0x100;
        edi = eax;
        if (eax < 0) {
            goto label_13;
        }
        ecx = 0x1000;
        eax = do_statx (rdi, 0x0001bb0a, rdx);
        if (eax < 0) {
            goto label_14;
        }
label_5:
        rax = *((rsp + 0x28));
        r15 = *((rsp + 0x20));
        *((rsp + 8)) = rax;
        rax = xmalloc (0x10);
        r8 = rax;
        rax = *((rsp + 8));
        *((r8 + 8)) = r15;
        rsi = r8;
        *(r8) = rax;
        *((rsp + 0x10)) = r8;
        rax = hash_insert (*(obj.active_dir_set));
        r8 = *((rsp + 0x10));
        if (rax == 0) {
            goto label_15;
        }
        if (r8 != rax) {
            rdi = r8;
            fcn_00004630 ();
            rdx = r13;
            esi = 3;
            edi = 0;
            rax = quotearg_n_style_colon ();
            edx = 5;
            r13 = rax;
            rax = dcgettext (0, "%s: not listing already-listed directory");
            rcx = r13;
            eax = 0;
            error (0, 0, rax);
            rdi = r12;
            closedir ();
            *(obj.exit_status) = 2;
label_7:
            rax = *((rsp + 0x348));
            rax -= *(fs:0x28);
            if (rax != 0) {
                goto label_16;
            }
            return rax;
        }
        rax = Scrt1.o;
        rdx = Scrt1.o;
        rdx -= rax;
        if (rdx <= 0xf) {
            goto label_17;
        }
label_11:
        rcx = *((rsp + 8));
        rdx = rax + 0x10;
        *(0x000260f8) = rdx;
        *((rax + 8)) = r15;
        *(rax) = rcx;
    }
    clear_files (rdi, rsi);
    if (*(obj.recursive) == 0) {
        goto label_18;
    }
    if (*(obj.first.0) == 0) {
        goto label_19;
    }
label_2:
    *(obj.first.0) = 0;
    if (*(obj.dired) != 0) {
        goto label_20;
    }
label_3:
    r15d = 0;
    if (*(obj.print_hyperlink) != 0) {
        goto label_21;
    }
label_4:
    rsi = dirname_quoting_options;
    edx = 0xffffffff;
    r9 = obj_subdired_obstack;
    if (r14 == 0) {
        r14 = r13;
    }
    quote_name (r14, rsi, rdx, 0, 1, r9);
    rdi = r15;
    fcn_00004630 ();
    esi = 2;
    dired_outbuf (0x0001bd62);
label_1:
    *((rsp + 0x10)) = 0;
    eax = (int32_t) bl;
    *((rsp + 8)) = eax;
label_0:
    *(rbp) = 0;
    rdi = r12;
    rax = readdir ();
    rbx = rax;
    if (rax == 0) {
        goto label_22;
    }
    r14 = rax + 0x13;
    eax = ignore_mode;
    if (eax == 2) {
        goto label_23;
    }
    if (*((rbx + 0x13)) == 0x2e) {
        goto label_24;
    }
    if (eax != 0) {
        goto label_23;
    }
    r15 = hide_patterns;
    if (r15 != 0) {
        goto label_25;
    }
    goto label_23;
    do {
        r15 = *((r15 + 8));
        if (r15 == 0) {
            goto label_23;
        }
label_25:
        rdi = *(r15);
        edx = 4;
        rsi = r14;
        eax = fnmatch ();
    } while (eax != 0);
label_6:
    process_signals ();
    goto label_0;
label_18:
    if (*(obj.print_dir_name) == 0) {
        goto label_1;
    }
    if (*(obj.first.0) != 0) {
        goto label_2;
    }
label_19:
    rdi = stdout;
    *(obj.dired_pos)++;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_26;
    }
    rdx = rax + 1;
    *(obj.first.0) = 0;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
    if (*(obj.dired) == 0) {
        goto label_3;
    }
label_20:
    esi = 2;
    r15d = 0;
    dired_outbuf (0x0001bb15);
    if (*(obj.print_hyperlink) == 0) {
        goto label_4;
    }
label_21:
    esi = 2;
    rdi = r13;
    rax = canonicalize_filename_mode ();
    r15 = rax;
    if (rax != 0) {
        goto label_4;
    }
    edx = 5;
    rax = dcgettext (0, "error canonicalizing %s");
    eax = *((rsp + 0x1c));
    file_failure (eax, rax, r13);
    goto label_4;
label_13:
    ecx = 0;
    eax = do_statx (0xffffff9c, r13, rdx);
    if (eax >= 0) {
        goto label_5;
    }
label_14:
    edx = 5;
    rax = dcgettext (0, "cannot determine device and inode of %s");
    edi = *((rsp + 0x1c));
    file_failure (rdi, rax, r13);
    rax = *((rsp + 0x348));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_16;
    }
    rdi = r12;
    void (*0x4960)() ();
label_22:
    edx = *(rbp);
    if (edx == 0) {
        goto label_27;
    }
    edx = 5;
    rax = dcgettext (0, "reading directory %s");
    file_failure (*((rsp + 8)), rax, r13);
    if (*(rbp) == 0x4b) {
        goto label_6;
    }
label_27:
    rdi = r12;
    eax = closedir ();
    if (eax != 0) {
        goto label_28;
    }
label_9:
    sort_files (rdi, rsi);
    if (*(obj.recursive) != 0) {
        goto label_29;
    }
label_8:
    eax = format;
    if (eax != 0) {
        if (*(obj.print_block_size) == 0) {
            goto label_30;
        }
    }
    rax = human_readable (*((rsp + 0x10)), rsp + 0xb1, *(obj.human_output_opts), 0x200, *(obj.output_block_size), r9);
    rdi = rax;
    rbx = rax;
    rax = strlen (rdi);
    edx = *(obj.eolbyte);
    *((rbx - 1)) = 0x20;
    r12 = rbx - 1;
    rax += rbx;
    *(rax) = dl;
    rbx = rax + 1;
    if (*(obj.dired) != 0) {
        goto label_31;
    }
label_10:
    edx = 5;
    rbx -= r12;
    rax = dcgettext (0, "total");
    rdi = rax;
    rax = strlen (rdi);
    rsi = rax;
    dired_outbuf (rbp);
    rsi = rbx;
    dired_outbuf (r12);
label_30:
    if (*(obj.cwd_n_used) == 0) {
        goto label_7;
    }
    rax = *((rsp + 0x348));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_16;
    }
    void (*0xd120)() ();
label_24:
    if (eax == 0) {
        goto label_6;
    }
    eax = 0;
    al = (*((rbx + 0x14)) == 0x2e) ? 1 : 0;
    if (*((rbx + rax + 0x14)) == 0) {
        goto label_6;
    }
label_23:
    r15 = ignore_patterns;
    if (r15 != 0) {
        goto label_32;
    }
    goto label_33;
    do {
        r15 = *((r15 + 8));
        if (r15 == 0) {
            goto label_33;
        }
label_32:
        rdi = *(r15);
        edx = 4;
        rsi = r14;
        eax = fnmatch ();
    } while (eax != 0);
    goto label_6;
label_33:
    eax = *((rbx + 0x12));
    esi = 0;
    eax--;
    if (al <= 0xd) {
        eax = (int32_t) al;
        rcx = obj_CSWTCH_995;
        esi = *((rcx + rax*4));
    }
    edx = 0;
    rcx = r13;
    rdi = r14;
    rax = gobble_file_constprop_0 ();
    if (*(obj.format) != 1) {
        goto label_6;
    }
    if (*(obj.sort_type) != 6) {
        goto label_6;
    }
    if (*(obj.print_block_size) != 0) {
        goto label_6;
    }
    if (*(obj.recursive) != 0) {
        goto label_6;
    }
    sort_files (rdi, rsi);
    print_current_files (rdi, rsi, rdx, rcx);
    clear_files (rdi, rsi);
    goto label_6;
label_29:
    extract_dirs_from_files (r13, 0, rdx, rcx);
    goto label_8;
label_28:
    edx = 5;
    rax = dcgettext (0, "closing directory %s");
    edi = *((rsp + 0x1c));
    file_failure (rdi, rax, r13);
    goto label_9;
label_31:
    esi = 2;
    dired_outbuf (0x0001bb15);
    goto label_10;
label_17:
    _obstack_newchunk (obj.dev_ino_obstack, 0x10, rdx, rcx);
    rax = Scrt1.o;
    goto label_11;
label_26:
    esi = 0xa;
    overflow ();
    goto label_2;
label_12:
    edx = 5;
    rax = dcgettext (0, "cannot open directory %s");
    rsi = rax;
    rax = *((rsp + 0x348));
    rax -= *(fs:0x28);
    if (rax == 0) {
        edi = *((rsp + 0x1c));
        rdx = r13;
        void (*0x74d0)() ();
label_15:
        xalloc_die ();
    }
label_16:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x10050 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x10070 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* _Bool raw_comparator( const * a, const * b); */
    bl += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *((rax + riz)) += dl;
}

/* /tmp/tmprboi4m6a @ 0x10080 */
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    /* _Bool check_tuning(Hash_table * table); */
    bl += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    bl += ah;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rdx + riz*8)) += bh;
    *((rdx + 0x29)) += dh;
    __asm ("addss xmm1, dword [0x0001e324]");
    xmm2 = *((rax + 4));
    __asm ("comiss xmm2, xmm1");
    if (*((rdx + 0x29)) > 0) {
        xmm3 = *(0x0001e330);
        __asm ("comiss xmm3, xmm2");
        if (*((rdx + 0x29)) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*((rdx + 0x29)) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x10110 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x4c9b)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x4c9b */
 
void hash_find_entry_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x10220 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x0001e334]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x0001e338]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x0001e338]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmprboi4m6a @ 0x10360 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x4ca0)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x4ca0)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    edi = 0x10;
    rax = fcn_00004670 ();
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x4ca0 */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4ca5 */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4caa */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cb0 */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cb5 */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cba */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x14b50 */
 
uint64_t dbg_print_and_abort (void) {
    /* void print_and_abort(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rdi = stderr;
    esi = 1;
    rdx = 0x0001e9f0;
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    return exit (*(obj.exit_failure));
}

/* /tmp/tmprboi4m6a @ 0x4c10 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmprboi4m6a @ 0x4bf0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmprboi4m6a @ 0x14ba0 */
 
int64_t dbg_obstack_begin_worker (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int _obstack_begin_worker(obstack * h,size_t size,size_t alignment); */
    rbx = rdi;
    if (rdx == 0) {
        goto label_1;
    }
    r12 = rdx - 1;
    do {
        eax = 0xfe0;
        *((rbx + 0x30)) = r12;
        if (rsi == 0) {
            rsi = rax;
        }
        rax = *((rbx + 0x38));
        *(rbx) = rsi;
        if ((*((rbx + 0x50)) & 1) == 0) {
            goto label_2;
        }
        rdi = *((rbx + 0x48));
        rax = void (*rax)() ();
label_0:
        *((rbx + 8)) = rax;
        if (rax == 0) {
            goto label_3;
        }
        rdx = rax + r12 + 0x10;
        rbp = -rbp;
        rdx &= rbp;
        *((rbx + 0x10)) = rdx;
        *((rbx + 0x18)) = rdx;
        rdx = *(rbx);
        rdx += rax;
        *(rax) = rdx;
        *((rbx + 0x20)) = rdx;
        *((rax + 8)) = 0;
        eax = 1;
        *((rbx + 0x50)) &= 0xf9;
        return rax;
label_1:
        r12d = 0xf;
    } while (1);
label_2:
    rdi = rsi;
    void (*rax)() ();
    goto label_0;
label_3:
    uint64_t (*obstack_alloc_failed_handler)() ();
}

/* /tmp/tmprboi4m6a @ 0x4cbf */
 
void obstack_free_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x14f60 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0001e61d;
        rdx = 0x0001e610;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0001e617;
        rdx = 0x0001ed68;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0001e619;
    rdx = 0x0001e614;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x19950 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x4ab0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmprboi4m6a @ 0x15040 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x4cc4)() ();
    }
    rdx = 0x0001e680;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x1e680 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0001e621;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0001ed68;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0001e6ac;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x1e6ac */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0001e617;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0001ed68;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0001e617;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0001ed68;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0001e7ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1e7ac */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0001e8ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1e8ac */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0001ed68;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0001e617;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0001e617;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0001ed68;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmprboi4m6a @ 0x4cc4 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cc9 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cce */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cd4 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cd9 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cde */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4ce3 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4ce8 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4ced */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cf2 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cf7 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4cfc */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4d01 */
 
void xstrtol_fatal_cold (void) {
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x6800 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x6830 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x6870 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00004680 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmprboi4m6a @ 0x4680 */
 
void fcn_00004680 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmprboi4m6a @ 0x68b0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmprboi4m6a @ 0xe690 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0xdbe0)() ();
}

/* /tmp/tmprboi4m6a @ 0x4b20 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmprboi4m6a @ 0x4990 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmprboi4m6a @ 0x4b10 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmprboi4m6a @ 0x4700 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmprboi4m6a @ 0xec10 */
 
int64_t dbg_canonicalize_filename_mode_stk (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_1h;
    stat st;
    scratch_buffer extra_buffer;
    scratch_buffer link_buffer;
    uint32_t var_8h;
    uint32_t var_10h;
    size_t n;
    size_t * s1;
    uint32_t var_28h;
    size_t * s2;
    int64_t var_3ch;
    int64_t var_40h;
    uint32_t var_4bh;
    signed int64_t var_4ch;
    size_t * var_50h;
    size_t * var_58h;
    int64_t var_60h;
    int64_t var_f0h;
    uint32_t var_f8h;
    int64_t var_100h;
    int64_t var_500h;
    int64_t var_508h;
    int64_t var_510h;
    int64_t var_918h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * canonicalize_filename_mode_stk(char const * name,canonicalize_mode_t can_mode,scratch_buffer * rname_buf); */
    r14d = esi;
    r14d &= 3;
    rax = *(fs:0x28);
    *((rsp + 0x918)) = rax;
    eax = 0;
    eax = r14 - 1;
    if ((eax & r14d) != 0) {
        goto label_21;
    }
    if (rdi == 0) {
        goto label_21;
    }
    if (*(rdi) == 0) {
        goto label_22;
    }
    rax = rsp + 0x100;
    ebx = esi;
    *((rdx + 8)) = 0x400;
    r12 = rdx;
    *((rsp + 8)) = rax;
    esi = 0x400;
    *((rsp + 0xf0)) = rax;
    rax = rsp + 0x510;
    *(rsp) = rax;
    *((rsp + 0x500)) = rax;
    rax = rdx + 0x10;
    *(rdx) = rax;
    r13 = rax;
    *((rsp + 0xf8)) = 0x400;
    *((rsp + 0x508)) = 0x400;
    *((rsp + 0x10)) = rax;
    if (*(rdi) == 0x2f) {
        goto label_23;
    }
    do {
        rdi = r13;
        rax = getcwd ();
        if (rax != 0) {
            goto label_24;
        }
        rax = errno_location ();
        eax = *(rax);
        if (eax == 0xc) {
            goto label_12;
        }
        if (eax != 0x22) {
            r15 = r13;
            ebx = 1;
label_4:
            rdi = *((rsp + 0xf0));
            if (rdi != *((rsp + 8))) {
                fcn_00004630 ();
            }
            rdi = *((rsp + 0x500));
            if (rdi != *(rsp)) {
                fcn_00004630 ();
            }
            if (bl != 0) {
                goto label_25;
            }
            *(r13) = 0;
            rsi -= r15;
            rax = gl_scratch_buffer_dupfree (r12, r13 + 1);
            if (rax == 0) {
                goto label_12;
            }
label_0:
            rdx = *((rsp + 0x918));
            rdx -= *(fs:0x28);
            if (rdx != 0) {
                goto label_26;
            }
            return rax;
        }
        al = gl_scratch_buffer_grow (r12);
        if (al == 0) {
            goto label_12;
        }
        r13 = *(r12);
        rsi = *((r12 + 8));
    } while (1);
label_21:
    errno_location ();
    *(rax) = 0x16;
    eax = 0;
    goto label_0;
label_1:
    if (dl == 0x2e) {
        goto label_17;
    }
label_2:
    if (*((r13 - 1)) != 0x2f) {
        *(r13) = 0x2f;
        r13++;
    }
    rax = *((rsp + 0x18));
    rsi = rax + 2;
    rax = *((r12 + 8));
    rax += r15;
    rax -= r13;
    if (rax >= rsi) {
        goto label_27;
    }
    *((rsp + 0x30)) = rbx;
    rcx = r13;
    rbx = rsi;
    r13 = r12;
    r12 = rbp;
    while (al != 0) {
        rax = *((r13 + 8));
        r15 = *(r13);
        rax -= rbp;
        rcx = r15 + rbp;
        if (rax >= rbx) {
            goto label_28;
        }
        rcx -= r15;
        al = gl_scratch_buffer_grow_preserve (r13);
    }
label_12:
    xalloc_die ();
label_23:
    *((rdx + 0x10)) = 0x2f;
    r15 = *((rsp + 0x10));
    r13 = rdx + 0x11;
label_11:
    eax = *(rbp);
    if (al == 0) {
        goto label_29;
    }
    ebx &= 4;
    rcx = rsp + 0x500;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x3c)) = ebx;
    *((rsp + 0x4b)) = 0;
    *((rsp + 0x4c)) = 0;
    *((rsp + 0x40)) = rcx;
label_3:
    if (al != 0x2f) {
        goto label_30;
    }
    do {
        edx = *((rbp + 1));
        rbp++;
    } while (dl == 0x2f);
    if (dl == 0) {
        goto label_6;
    }
    *((rsp + 0x20)) = rbp;
    do {
label_7:
        rbx = rbp;
        eax = *((rbp + 1));
        rbp++;
        if (al == 0) {
            goto label_31;
        }
    } while (al != 0x2f);
label_31:
    rcx = rbp;
    rcx -= *((rsp + 0x20));
    *((rsp + 0x18)) = rcx;
    if (rcx == 0) {
        goto label_6;
    }
    if (rcx == 1) {
        goto label_1;
    }
    if (*((rsp + 0x18)) != 2) {
        goto label_2;
    }
    if (dl != 0x2e) {
        goto label_2;
    }
    rcx = *((rsp + 0x20));
    if (*((rcx + 1)) != 0x2e) {
        goto label_2;
    }
    rdx = r15 + 1;
    if (r13 <= rdx) {
        goto label_17;
    }
    r13--;
    if (r13 <= r15) {
        goto label_17;
    }
    do {
        if (*((r13 - 1)) == 0x2f) {
            goto label_17;
        }
        r13--;
    } while (r13 != r15);
label_17:
    if (al != 0) {
        goto label_3;
    }
label_6:
    rax = r15 + 1;
    ebx = 0;
    if (r13 > rax) {
        eax = 0;
        al = (*((r13 - 1)) == 0x2f) ? 1 : 0;
        r13 -= rax;
    }
label_10:
    rdi = *((rsp + 0x28));
    if (rdi == 0) {
        goto label_4;
    }
    eax = hash_free (rdi, rsi);
    goto label_4;
label_25:
    rdi = *(r12);
    eax = 0;
    if (*((rsp + 0x10)) == rdi) {
        goto label_0;
    }
    *(rsp) = rax;
    fcn_00004630 ();
    rax = *(rsp);
    goto label_0;
label_22:
    errno_location ();
    *(rax) = 2;
    eax = 0;
    goto label_0;
label_28:
    rbx = *((rsp + 0x30));
    r12 = r13;
    r13 = rcx;
label_27:
    rdx = *((rsp + 0x18));
    rsi = *((rsp + 0x20));
    rdi = r13;
    mempcpy ();
    ecx = *((rsp + 0x3c));
    *(rax) = 0;
    r13 = rax;
    if (ecx == 0) {
        goto label_32;
    }
label_14:
    if (r14d == 2) {
        goto label_9;
    }
    eax = *(rbp);
    if (al != 0x2f) {
        goto label_33;
    }
    rdx = rbp;
    do {
label_5:
        rsi = rdx;
        ecx = *((rdx + 1));
        rdx++;
    } while (cl == 0x2f);
    rsi += 2;
    if (cl == 0) {
        goto label_34;
    }
    if (cl != 0x2e) {
        goto label_33;
    }
    ecx = *((rdx + 1));
    if (cl == 0) {
        goto label_34;
    }
    if (cl == 0x2e) {
        goto label_35;
    }
    if (cl != 0x2f) {
        goto label_33;
    }
    rdx = rsi;
    goto label_5;
label_8:
    if (al != 0) {
        goto label_3;
    }
label_13:
    edx = 0;
    ecx = 0x200;
    rsi = r15;
    edi = 0xffffff9c;
    eax = faccessat ();
    if (eax != 0) {
        goto label_36;
    }
label_9:
    eax = *((rbx + 1));
    if (al != 0) {
        goto label_3;
    }
    goto label_6;
label_30:
    *((rsp + 0x20)) = rbp;
    edx = eax;
    goto label_7;
label_35:
    edx = *((rdx + 2));
    if (dl == 0) {
        goto label_34;
    }
    if (dl == 0x2f) {
        goto label_34;
    }
label_33:
    edx = *((rsp + 0x3c));
    if (edx != 0) {
        goto label_8;
    }
    rax = errno_location ();
    if (*(rax) == 0x16) {
        goto label_9;
    }
label_36:
    if (r14d != 1) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) != 2) {
        goto label_15;
    }
    strspn (rbp, 0x0001bdf2);
    if (*((rbp + rax)) == 0) {
        goto label_9;
    }
label_15:
    ebx = 1;
    goto label_10;
label_24:
    rdi = r13;
    esi = 0;
    r15 = r13;
    rax = rawmemchr ();
    r13 = rax;
    goto label_11;
label_32:
    *((rsp + 0x18)) = rax;
    r13 = *((rsp + 0x40));
    *((rsp + 0x30)) = rbx;
    *((rsp + 0x50)) = rbp;
    do {
        rax = *((rsp + 0x508));
        rbx = *((rsp + 0x500));
        rdi = r15;
        rbp = rax - 1;
        rsi = rbx;
        rdx = rbp;
        rax = readlink ();
        if (rbp > rax) {
            goto label_37;
        }
        al = gl_scratch_buffer_grow (r13);
    } while (al != 0);
    goto label_12;
label_34:
    eax = *(obj.dir_suffix);
    *(r13) = ax;
    goto label_13;
label_37:
    r10 = rbx;
    r13 = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    r9 = rax;
    rbp = *((rsp + 0x50));
    if (rax < 0) {
        goto label_14;
    }
    if (*((rsp + 0x4c)) <= 0x13) {
        goto label_38;
    }
    rax = *((rsp + 0x20));
    if (*(rax) == 0) {
        goto label_18;
    }
    rdx = rax;
    r11 = rsp + 0x60;
    *((rsp + 0x58)) = r9;
    rdi = 0x0001bde2;
    rdx -= rbp;
    rsi = r11;
    *((rsp + 0x50)) = r10;
    rdx += r13;
    *((rsp + 0x18)) = r11;
    *(rdx) = 0;
    if (*(r15) != 0) {
        rdi = r15;
    }
    *((rsp + 0x30)) = rdx;
    eax = stat ();
    if (eax != 0) {
        goto label_15;
    }
    rax = *((rsp + 0x20));
    rdx = *((rsp + 0x30));
    r11 = *((rsp + 0x18));
    eax = *(rax);
    r10 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    *(rdx) = al;
    if (*((rsp + 0x28)) == 0) {
        goto label_39;
    }
label_20:
    rdx = r11;
    *((rsp + 0x50)) = r9;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r11;
    al = seen_file (*((rsp + 0x28)), *((rsp + 0x20)), rdx, rcx, r8, r9);
    r11 = *((rsp + 0x18));
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    if (al != 0) {
        goto label_40;
    }
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    record_file (*((rsp + 0x28)), *((rsp + 0x20)), r11, rcx, r8, r9);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
label_18:
    *((r10 + r9)) = 0;
    r8 = *((rsp + 0xf0));
    if (*((rsp + 0x4b)) == 0) {
        goto label_41;
    }
    rax = rbp;
    *((rsp + 0x58)) = r9;
    rax -= r8;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    *((rsp + 0x20)) = rax;
    rax = strlen (rbp);
    r9 = *((rsp + 0x58));
    r8 = *((rsp + 0x30));
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (*((rsp + 0xf8)) > rax) {
        goto label_42;
    }
label_16:
    rbx = rsp + 0xf0;
    *((rsp + 0x58)) = rbp;
    *((rsp + 0x30)) = r10;
    rbx = rax;
    *((rsp + 0x50)) = r9;
    while (al != 0) {
        r8 = *((rsp + 0xf0));
        if (*((rsp + 0xf8)) > rbx) {
            goto label_43;
        }
        al = gl_scratch_buffer_grow_preserve (rbp);
    }
    goto label_12;
label_29:
    rax = r15 + 1;
    if (r13 <= rax) {
        goto label_44;
    }
    if (*((r13 - 1)) != 0x2f) {
        goto label_44;
    }
    r13--;
    ebx = 0;
    goto label_4;
label_40:
    if (r14d == 2) {
        goto label_9;
    }
    errno_location ();
    *(rax) = 0x28;
    goto label_15;
label_44:
    ebx = 0;
    goto label_4;
label_41:
    *((rsp + 0x20)) = r9;
    *((rsp + 0x50)) = r10;
    *((rsp + 0x30)) = r8;
    rax = strlen (rbp);
    r9 = *((rsp + 0x20));
    r8 = *((rsp + 0x30));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x18)) = rax;
    r10 = *((rsp + 0x50));
    rax += r9;
    if (rax >= *((rsp + 0xf8))) {
        goto label_16;
    }
label_19:
    *((rsp + 0x30)) = r10;
    *((rsp + 0x18)) = r9;
    rdx++;
    *((rsp + 0x20)) = r8;
    memmove (r8 + r9, rbp, *((rsp + 0x18)));
    r10 = *((rsp + 0x30));
    rsi = r10;
    *((rsp + 0x18)) = r10;
    rax = memcpy (*((rsp + 0x20)), rsi, *((rsp + 0x18)));
    r10 = *((rsp + 0x18));
    rdx = r15 + 1;
    if (*(r10) == 0x2f) {
        goto label_45;
    }
    *((rsp + 0x4b)) = 1;
    eax = *(rax);
    if (r13 <= rdx) {
        goto label_17;
    }
    do {
        r13--;
        if (r13 == r15) {
            goto label_46;
        }
    } while (*((r13 - 1)) != 0x2f);
label_46:
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_38:
    goto label_18;
label_43:
    r10 = *((rsp + 0x30));
    r9 = *((rsp + 0x50));
    rbp = *((rsp + 0x58));
    if (*((rsp + 0x4b)) == 0) {
        goto label_19;
    }
label_42:
    rbp = *((rsp + 0x20));
    rbp += r8;
    goto label_19;
label_39:
    *((rsp + 0x50)) = r11;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x18)) = r10;
    rax = hash_initialize (7, 0, dbg.triple_hash, dbg.triple_compare_ino_str, dbg.triple_free);
    r10 = *((rsp + 0x18));
    r9 = *((rsp + 0x30));
    *((rsp + 0x28)) = rax;
    r11 = *((rsp + 0x50));
    if (rax != 0) {
        goto label_20;
    }
    goto label_12;
label_45:
    *(r15) = 0x2f;
    r13 = rdx;
    eax = *(rax);
    *((rsp + 0x4b)) = 1;
    goto label_17;
label_26:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xfa40 */
 
uint64_t dbg_file_prefixlen (ptrdiff_t * len, char const * s) {
    rsi = len;
    rdi = s;
    /* idx_t file_prefixlen(char const * s,ptrdiff_t * len); */
    r8 = *(rsi);
    r10d = 0;
    while (al <= 0x5a) {
        if (al > 0x40) {
            goto label_6;
        }
label_0:
        if (r8 >= 0) {
label_2:
            if (r8 == rdx) {
                goto label_7;
            }
        }
label_3:
        r10 = rcx;
        rcx = r10 + 1;
        rdx = r10;
        if (rcx >= r8) {
            goto label_8;
        }
label_1:
        if (*((rdi + rdx)) != 0x2e) {
            goto label_8;
        }
        eax = *((rdi + rdx + 1));
    }
    r9d = rax - 0x61;
    if (r9b <= 0x19) {
        goto label_6;
    }
    if (al != 0x7e) {
        goto label_0;
    }
label_6:
    rdx += 2;
    if (r8 > rdx) {
label_5:
        eax = *((rdi + rdx));
        if (al > 0x5a) {
            goto label_9;
        }
        if (al > 0x40) {
            goto label_10;
        }
        eax -= 0x30;
        if (al <= 9) {
            goto label_10;
        }
    }
label_4:
    rcx = rdx + 1;
    if (rcx < r8) {
        goto label_1;
    }
label_8:
    if (r8 >= 0) {
        goto label_2;
    }
    if (*((rdi + rdx)) != 0) {
        goto label_3;
    }
label_7:
    *(rsi) = rdx;
    rax = r10;
    return rax;
label_9:
    ecx = rax - 0x61;
    if (cl <= 0x19) {
        goto label_10;
    }
    if (al != 0x7e) {
        goto label_4;
    }
label_10:
    rdx++;
    if (r8 > rdx) {
        goto label_5;
    }
    goto label_4;
}

/* /tmp/tmprboi4m6a @ 0xfaf0 */
 
int64_t dbg_verrevcmp (int64_t arg1, int64_t arg3, idx_t s1_len, idx_t s2_len) {
    rdi = arg1;
    rdx = arg3;
    rsi = s1_len;
    rcx = s2_len;
    /* int verrevcmp(char const * s1,idx_t s1_len,char const * s2,idx_t s2_len); */
    r8 = rdi;
    eax = 0;
    rdi = rdx;
    edx = 0;
label_11:
    if (rdx >= rsi) {
        goto label_12;
    }
    do {
label_0:
        r9d = *((r8 + rdx));
        r10d = (int32_t) r9b;
        r10d -= 0x30;
        if (r10d <= 9) {
            goto label_16;
        }
        r10d = (int32_t) r9b;
        ebx = 0;
        r11d = r10 - 0x30;
        if (r11d > 9) {
label_2:
            ebx = r10d;
            if (r9b > 0x5a) {
                goto label_17;
            }
            if (r9b > 0x40) {
                goto label_4;
            }
label_5:
            if (r9b == 0x7e) {
                goto label_18;
            }
            ebx = r10 + 0x100;
        }
label_4:
        if (rax == rcx) {
            goto label_19;
        }
label_3:
        r9d = *((rdi + rax));
        r10d = 0;
        r11d = (int32_t) r9b;
        ebp = r11 - 0x30;
        if (ebp > 9) {
label_13:
            r10d = r11d;
            if (r9b > 0x5a) {
                goto label_20;
            }
            if (r9b > 0x40) {
                goto label_6;
            }
label_7:
            if (r9b == 0x7e) {
                goto label_21;
            }
            r10d = r11 + 0x100;
        }
label_6:
        if (r10d != ebx) {
            goto label_14;
        }
        rdx++;
        rax++;
label_1:
    } while (rdx < rsi);
label_16:
    if (rax < rcx) {
        r10d = *((rdi + rax));
        r9d = r10d;
        r10d -= 0x30;
        if (r10d > 9) {
            goto label_22;
        }
    }
    if (rdx < rsi) {
        goto label_23;
    }
    goto label_24;
    do {
        rdx++;
        if (rsi == rdx) {
            goto label_24;
        }
label_23:
    } while (*((r8 + rdx)) == 0x30);
    if (rax < rcx) {
        goto label_10;
    }
    goto label_25;
    do {
        rax++;
        if (rcx == rax) {
            goto label_25;
        }
label_10:
    } while (*((rdi + rax)) == 0x30);
    r9d = 0;
    if (rdx >= rsi) {
        goto label_26;
    }
label_8:
    if (rcx <= rax) {
        goto label_26;
    }
    r10d = *((r8 + rdx));
    r11d = r10 - 0x30;
    if (r11d <= 9) {
        goto label_27;
    }
label_9:
    if (rcx > rax) {
        r10d = *((rdi + rax));
        r10d -= 0x30;
        if (r10d <= 9) {
            goto label_28;
        }
    }
    if (r9d != 0) {
        goto label_29;
    }
    if (rdx < rsi) {
        goto label_0;
    }
label_12:
    if (rax < rcx) {
        goto label_1;
    }
    r9d = 0;
    goto label_29;
label_22:
    if (rdx == rsi) {
        goto label_30;
    }
    r10d = *((r8 + rdx));
    r11d = r10 - 0x30;
    r9d = r10d;
    if (r11d > 9) {
        goto label_2;
    }
    ebx = 0;
    goto label_3;
label_17:
    r11d = r9 - 0x61;
    if (r11b <= 0x19) {
        goto label_4;
    }
    goto label_5;
label_20:
    ebp = r9 - 0x61;
    if (bpl <= 0x19) {
        goto label_6;
    }
    goto label_7;
label_27:
    r11d = *((rdi + rax));
    ebx = r11 - 0x30;
    if (ebx > 9) {
        goto label_31;
    }
    r10d -= r11d;
    if (r9d == 0) {
        r9d = r10d;
    }
    rdx++;
    rax++;
    if (rsi > rdx) {
        goto label_8;
    }
    goto label_9;
label_24:
    if (rax < rcx) {
        goto label_10;
    }
    goto label_11;
label_25:
    if (rdx >= rsi) {
        goto label_12;
    }
    r9d = 0;
label_15:
    r10d = *((r8 + rdx));
    r10d -= 0x30;
    if (r10d > 9) {
        goto label_9;
    }
label_31:
    r9d = 1;
label_29:
    eax = 1;
    return rax;
label_28:
    r9d = 0xffffffff;
    eax = r9d;
    return rax;
label_19:
    r10d = 0xffffffff;
label_14:
    ebx -= r10d;
    r9d = ebx;
    eax = r9d;
    return rax;
label_30:
    r11d = (int32_t) r9b;
    ebx = 0xffffffff;
    r10d = r11 - 0x30;
    if (r10d > 9) {
        goto label_13;
    }
    r10d = 0;
    goto label_14;
label_18:
    ebx = 0xfffffffe;
    goto label_4;
label_21:
    r10d = 0xfffffffe;
    goto label_6;
label_26:
    if (rsi <= rdx) {
        goto label_9;
    }
    goto label_15;
}

/* /tmp/tmprboi4m6a @ 0x12ca0 */
 
int64_t mpsort_with_tmp_part_0 (uint32_t arg1, size_t arg2, uint32_t arg3, int64_t arg4) {
    uint32_t var_8h;
    size_t var_10h;
    uint32_t var_18h;
    size_t n;
    uint32_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_5:
    rax = rsi;
    rax >>= 1;
    rbx = rdi;
    *((rsp + 0x10)) = rsi;
    rsi -= rax;
    *((rsp + 8)) = rax;
    rax = rdi + rax*8;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x28)) = rax;
    if (rsi > 2) {
        goto label_8;
    }
    if (rsi == 2) {
        goto label_9;
    }
label_1:
    if (*((rsp + 0x10)) != 3) {
        goto label_10;
    }
label_2:
    r14 = *(rbx);
    rax = *((rsp + 0x18));
    *(rax) = r14;
label_4:
    rax = *((rsp + 0x28));
    r15d = 0;
    r13 = *((rsp + 8));
    r12 = *(rax);
label_0:
    rsi = r12;
    rdi = r14;
    r15++;
    eax = void (*rbp)(uint64_t) (0);
    if (eax <= 0) {
        goto label_11;
    }
    do {
        *((rbx + r15*8 - 8)) = r12;
        r13++;
        if (*((rsp + 0x10)) == r13) {
            goto label_12;
        }
        r12 = *((rbx + r13*8));
        rdi = r14;
        r15++;
        rsi = r12;
        eax = void (*rbp)() ();
    } while (eax > 0);
label_11:
    rax = *(rsp);
    *((rbx + r15*8 - 8)) = r14;
    if (*((rsp + 8)) == rax) {
        goto label_13;
    }
    rdx = *((rsp + 0x18));
    r14 = *((rdx + rax*8));
    goto label_0;
label_12:
    rax = *(rsp);
    r12 = *((rsp + 8));
    rdi = rbx + r15*8;
    rcx = *((rsp + 0x18));
    r12 -= rax;
    rdx = r12*8;
    rsi = rcx + rax*8;
    void (*0x4a30)() ();
label_9:
    r14 = *(rax);
    r12 = *((rax + 8));
    r15 = rax;
    rsi = r12;
    rdi = r14;
    eax = void (*rcx)() ();
    if (eax <= 0) {
        goto label_1;
    }
    *(r15) = r12;
    *((r15 + 8)) = r14;
    if (*((rsp + 0x10)) == 3) {
        goto label_2;
    }
label_10:
    rax = *((rsp + 0x10));
    rsi = *((rsp + 8));
    rax >>= 2;
    rsi -= rax;
    *((rsp + 0x20)) = rax;
    r15 = rbx + rax*8;
    if (rsi > 2) {
        goto label_14;
    }
    if (rsi == 2) {
        goto label_15;
    }
label_7:
    if (*((rsp + 0x20)) > 2) {
        goto label_16;
    }
    r14 = *(rbx);
    if (*((rsp + 0x20)) == 2) {
        goto label_17;
    }
label_6:
    r15 = *(r15);
    r12 = *((rsp + 0x20));
    r13 = *((rsp + 0x18));
label_3:
    rsi = r15;
    rdi = r14;
    r13 += 8;
    eax = void (*rbp)(uint64_t) (0);
    if (eax <= 0) {
        goto label_18;
    }
    do {
        *((r13 - 8)) = r15;
        r12++;
        if (*((rsp + 8)) == r12) {
            goto label_19;
        }
        r15 = *((rbx + r12*8));
        rdi = r14;
        r13 += 8;
        rsi = r15;
        eax = void (*rbp)() ();
    } while (eax > 0);
label_18:
    rax = *(rsp);
    *((r13 - 8)) = r14;
    if (*((rsp + 0x20)) != rax) {
        r14 = *((rbx + rax*8));
        goto label_3;
    }
    rax = *((rsp + 8));
    *(rsp) = r12;
    *((rsp + 0x20)) = rax;
label_19:
    rax = *(rsp);
    rdx -= rax;
    rdx <<= 3;
    memcpy (r13, rbx + rax*8, *((rsp + 0x20)));
    rax = *((rsp + 0x18));
    r14 = *(rax);
    goto label_4;
label_13:
    return rax;
label_8:
    rdx = *((rsp + 0x18));
    rdi = *((rsp + 0x28));
    mpsort_with_tmp_part_0 ();
    goto label_5;
    goto label_1;
label_16:
    rdx = *((rsp + 0x18));
    rsi = *((rsp + 0x20));
    rcx = rbp;
    rdi = rbx;
    mpsort_with_tmp_part_0 ();
    goto label_5;
    do {
        r14 = *(rbx);
        goto label_6;
label_14:
        rdx = *((rsp + 0x18));
        rcx = rbp;
        rdi = r15;
        mpsort_with_tmp_part_0 ();
        goto label_5;
        goto label_7;
label_17:
        r12 = *((rbx + 8));
        rdi = r14;
        rsi = r12;
        eax = void (*rbp)() ();
    } while (eax <= 0);
    *((rbx + 8)) = r14;
    r14 = r12;
    *(rbx) = r12;
    goto label_6;
label_15:
    r12 = *(r15);
    r13 = *((r15 + 8));
    rdi = r12;
    rsi = r13;
    eax = void (*rbp)() ();
    if (eax <= 0) {
        goto label_7;
    }
    *(r15) = r13;
    *((r15 + 8)) = r12;
    goto label_7;
}

/* /tmp/tmprboi4m6a @ 0x12fc0 */
 
uint32_t rotate_right32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t strftime_internal_isra_0 (int64_t arg_500h, int64_t arg_508h, int64_t arg_518h, int64_t arg1, uint32_t arg2, int64_t arg3, tm * arg4, uint32_t arg6) {
    int64_t var_1h;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_1fh;
    tm * timeptr;
    tm * var_28h;
    int64_t var_30h;
    uint32_t var_34h;
    size_t var_38h;
    size_t var_40h;
    size_t var_50h;
    tm * var_58h;
    int64_t var_60h;
    tm * var_64h;
    tm * var_68h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_8ch;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_a8h;
    char * format;
    int64_t var_ach;
    int64_t var_adh;
    int64_t var_aeh;
    int64_t var_afh;
    char * s;
    void * s2;
    int64_t var_c7h;
    int64_t var_4b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
label_19:
    r14 = rdi;
    rbx = rcx;
    rax = *((rsp + 0x508));
    *((rsp + 8)) = rsi;
    *((rsp + 0x20)) = rcx;
    r15 = *((rsp + 0x500));
    *((rsp + 0x34)) = r9d;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x1f)) = r8b;
    rax = *(fs:0x28);
    *((rsp + 0x4b8)) = rax;
    eax = 0;
    rax = errno_location ();
    r11 = *((rbx + 0x30));
    r10d = *((rbx + 8));
    *((rsp + 0x10)) = rax;
    eax = *(rax);
    *((rsp + 0x30)) = eax;
    rax = 0x0001bb0a;
    if (r11 == 0) {
        r11 = rax;
    }
    if (r10d <= 0xc) {
        goto label_63;
    }
    r10d -= 0xc;
label_1:
    eax = *(rbp);
    r13d = 0;
    if (al == 0) {
        goto label_64;
    }
    *((rsp + 0x60)) = r10d;
    r12 = r11;
    do {
        if (al == 0x25) {
            goto label_65;
        }
        eax = 0;
        rdx = *((rsp + 8));
        ecx = 1;
        __asm ("cmovns rax, r15");
        if (rax != 0) {
            rcx = rax;
        }
        rdx -= r13;
        if (rcx >= rdx) {
            goto label_9;
        }
        if (r14 != 0) {
            if (r15d > 1) {
                goto label_66;
            }
label_2:
            eax = *(rbp);
            r14++;
            *((r14 - 1)) = al;
        }
        r13 += rcx;
        rbx = rbp;
label_0:
        eax = *((rbx + 1));
        rbp = rbx + 1;
        r15 = 0xffffffffffffffff;
    } while (al != 0);
label_64:
    if (r14 != 0) {
        if (*((rsp + 8)) == 0) {
            goto label_67;
        }
        *(r14) = 0;
    }
label_67:
    rax = *((rsp + 0x10));
    edi = *((rsp + 0x30));
    *(rax) = edi;
    goto label_68;
label_12:
    if (*((rsp + 8)) != r13) {
        goto label_0;
    }
    do {
label_9:
        rax = *((rsp + 0x10));
        *(rax) = 0x22;
label_62:
        r13d = 0;
label_68:
        rax = *((rsp + 0x4b8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_69;
        }
        rax = r13;
        return rax;
label_63:
        eax = 0xc;
        if (r10d == 0) {
            r10d = eax;
        }
        goto label_1;
label_66:
        rbx = rax - 1;
        *((rsp + 0x38)) = rcx;
        r14 += rbx;
        memset (r14, 0x20, rbx);
        rcx = *((rsp + 0x38));
        goto label_2;
label_65:
        eax = *((rsp + 0x1f));
        rbx = rbp;
        r8d = 0;
        r10d = 0;
        *((rsp + 0x38)) = al;
label_7:
        edx = *((rbx + 1));
        rbx++;
        ecx = rdx - 0x23;
        esi = edx;
        edi = edx;
        if (cl <= 0x3c) {
            r9 = 0x1000000000002500;
            eax = 1;
            rax <<= cl;
            if ((rax & r9) != 0) {
                goto label_70;
            }
            if (cl == 0x3b) {
                goto label_71;
            }
            eax &= 1;
            if (eax != 0) {
                goto label_72;
            }
        }
        edx -= 0x30;
        if (edx <= 9) {
            goto label_73;
        }
label_4:
        if (sil == 0x45) {
            goto label_74;
        }
        if (sil == 0x4f) {
            goto label_74;
        }
        edi = 0;
label_3:
        if (sil <= 0x7a) {
            rcx = 0x0001e3cc;
            eax = (int32_t) sil;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (123 cases) at 0x1e3cc */
            rax = void (*rax)() ();
            if (edi != 0x4f) {
                goto label_75;
            }
        }
label_8:
        rcx = rbx;
        rcx -= rbp;
        r8 = rcx + 1;
        if (r15d >= 0) {
            if (r10d != 0x2d) {
                rdx = (int64_t) r15d;
                r9 = rdx;
                if (r8 >= rdx) {
                    r9 = r8;
                }
            }
        } else {
            r9 = r8;
            edx = 0;
        }
        rax = *((rsp + 8));
        rax -= r13;
    } while (rax <= r9);
    if (r14 == 0) {
        goto label_76;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r9;
        *((rsp + 0x50)) = r8;
        r15 = r14 + rdx;
        *((rsp + 0x40)) = rcx;
        if (r10d == 0x30) {
            goto label_77;
        }
        if (r10d == 0x2b) {
            goto label_77;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r9 = *((rsp + 0x58));
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_37:
    if (*((rsp + 0x38)) == 0) {
        goto label_78;
    }
    r15 = rcx;
    if (r8 == 0) {
        goto label_79;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    do {
        ecx = *((rbp + r15));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + r15)) = dl;
        r15--;
    } while (r15 >= 0);
    goto label_79;
label_74:
    esi = *((rbx + 1));
    rbx++;
    goto label_3;
label_78:
    rdx = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rbp, rdx);
    r9 = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
label_79:
    r14 += r8;
label_76:
    r13 += r9;
    goto label_0;
label_73:
    r15d = 0;
    goto label_80;
label_5:
    eax = *(rbx);
    eax -= 0x30;
    r15d += eax;
    if (r15d overflow 0) {
        goto label_81;
    }
label_6:
    edi = *((rbx + 1));
    rbx++;
    eax = rdi - 0x30;
    esi = edi;
    if (eax > 9) {
        goto label_4;
    }
label_80:
    r15d *= 0xa;
    if (eax !overflow 9) {
        goto label_5;
    }
label_81:
    r15d = 0x7fffffff;
    goto label_6;
label_70:
    r10d = edx;
    goto label_7;
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    *((rsp + 0x38)) = al;
    if (edi == 0x45) {
        goto label_8;
    }
label_75:
    ebp = 0;
label_16:
    r11d = 0x2520;
    *((rsp + 0xab)) = r11w;
    if (edi != 0) {
        goto label_82;
    }
    rax = rsp + 0xad;
label_10:
    *(rax) = sil;
    *((rax + 1)) = 0;
    *((rsp + 0x40)) = r10d;
    rax = strftime (rsp + 0xb0, 0x400, rsp + 0xab, *((rsp + 0x20)));
    rcx = rax;
    if (rax == 0) {
        goto label_0;
    }
    r10d = *((rsp + 0x40));
    r8 = rax - 1;
    if (r10d == 0x2d) {
        goto label_83;
    }
    if (r15d < 0) {
        goto label_83;
    }
    rdx = (int64_t) r15d;
    r15 = rdx;
    if (r8 >= rdx) {
        r15 = r8;
    }
label_31:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r8;
        rax = r14 + rdx;
        *((rsp + 0x50)) = rcx;
        *((rsp + 0x40)) = rax;
        if (r10d == 0x30) {
            goto label_84;
        }
        if (r10d == 0x2b) {
            goto label_84;
        }
        memset (r14, 0x20, rdx);
        r14 = *((rsp + 0x40));
        r8 = *((rsp + 0x58));
        rcx = *((rsp + 0x50));
    }
label_41:
    if (bpl != 0) {
        goto label_85;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_86;
    }
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_38:
    r14 += r8;
label_15:
    r13 += r15;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rdx = *((rsp + 0x20));
    ecx = *((rdx + 0x14));
    r11d = *((rdx + 0x1c));
    ebp = *((rdx + 0x18));
    eax = ecx;
    edx = r11d;
    eax >>= 0x1f;
    edx -= ebp;
    edx += 0x17e;
    eax &= 0x190;
    r9d = rcx + rax - 0x64;
    rax = (int64_t) edx;
    r8d = edx;
    rax *= 0xffffffff92492493;
    r8d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r8d;
    r8d = rax*8;
    r8d -= eax;
    eax = r11d;
    eax -= edx;
    r8d = rax + r8 + 3;
    if (r8d < 0) {
        goto label_87;
    }
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_88;
        }
        eax = r9d;
        r9d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r9d;
        edx = edx:eax % r9d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_88:
    r11d -= eax;
    edx = r11d;
    edx -= ebp;
    edx += 0x17e;
    rax = (int64_t) edx;
    r9d = edx;
    r11d -= edx;
    rax *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r9d;
    r9d = rax*8;
    r9d -= eax;
    eax = r11 + r9 + 3;
    __asm ("cmovns r8d, eax");
    eax >>= 0x1f;
    eax++;
label_52:
    if (sil == 0x47) {
        goto label_89;
    }
    if (sil != 0x67) {
        goto label_90;
    }
    rdx = (int64_t) ecx;
    r8d = ecx;
    rdx *= 0x51eb851f;
    r8d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r8d;
    r8d = ecx;
    edx *= 0x64;
    r8d -= edx;
    r8d += eax;
    rdx = (int64_t) r8d;
    r9d = r8d;
    rdx *= 0x51eb851f;
    r9d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r9d;
    r9d = edx * 0x64;
    edx = r8d;
    edx -= r9d;
    if (edx < 0) {
        goto label_91;
    }
    if (r10d != 0) {
        goto label_61;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_92;
    }
label_26:
    *((rsp + 0x58)) = 0;
    eax = 1;
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
label_18:
    r9d = 0;
label_13:
    if (edi != 0x4f) {
        goto label_93;
    }
    if (al == 0) {
        goto label_93;
    }
label_17:
    ecx = 0x2520;
    ebp = 0;
    *((rsp + 0xab)) = cx;
label_82:
    *((rsp + 0xad)) = dil;
    rax = rsp + 0xae;
    goto label_10;
    if (edi != 0) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    edx = 0x2520;
    *((rsp + 0xab)) = dx;
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    rax = rsp + 0xad;
    goto label_10;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = 9;
    edx = *((rsp + 0x510));
    if (r15d <= 0) {
        r15d = eax;
    }
    while (ebp > r15d) {
label_11:
        rax *= 0x66666667;
        ecx = edx;
        ebp--;
        ecx >>= 0x1f;
        rax >>= 0x22;
        eax -= ecx;
        edx = eax;
        rax = (int64_t) edx;
    }
    rcx = rax * 0x66666667;
    esi = edx;
    esi >>= 0x1f;
    rcx >>= 0x22;
    ecx -= esi;
    esi = rcx * 5;
    ecx = edx;
    esi += esi;
    ecx -= esi;
    if (ebp == 1) {
        goto label_94;
    }
    if (ecx == 0) {
        goto label_11;
    }
    rsi = (int64_t) ebp;
    rdi = rsi;
    if (ebp == 0) {
        goto label_95;
    }
label_51:
    rcx = rsp + rdi + 0xb0;
    r8d = rbp - 1;
    rdi = rsp + rdi + 0xaf;
    rdi -= r8;
    while (rcx != rdi) {
        rax = (int64_t) eax;
        rax *= 0x66666667;
        r8d = edx;
        rcx--;
        r8d >>= 0x1f;
        rax >>= 0x22;
        eax -= r8d;
        r8d = rax * 5;
        r8d += r8d;
        edx -= r8d;
        edx += 0x30;
        *(rcx) = dl;
        edx = eax;
    }
label_60:
    eax = 0x30;
    if (r10d == 0) {
        r10d = eax;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= rsi) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_96;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_97;
    }
    rdx = rsi - 1;
    *((rsp + 0x50)) = rdx;
    if (rsi == 0) {
        goto label_58;
    }
    *((rsp + 0x40)) = rsi;
    *((rsp + 0x38)) = r10d;
    rax = ctype_toupper_loc ();
    r10d = *((rsp + 0x38));
    rdx = *((rsp + 0x50));
    rdi = rsp + 0xb0;
    rsi = *((rsp + 0x40));
    rcx = rax;
    do {
        r8d = *((rdi + rdx));
        rax = *(rcx);
        eax = *((rax + r8*4));
        *((r14 + rdx)) = al;
        rdx--;
    } while (rdx >= 0);
label_58:
    r14 += rsi;
label_96:
    r13 += rsi;
    if (r10d == 0x2d) {
        goto label_12;
    }
    edx = r15d;
    edx -= ebp;
    if (edx < 0) {
        goto label_12;
    }
    rax = *((rsp + 8));
    rdx = (int64_t) edx;
    rax -= r13;
    if (rdx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_98;
    }
    if (rdx == 0) {
        goto label_0;
    }
    rbp = r14 + rdx;
    r13 += rdx;
    if (r10d == 0x30) {
        goto label_99;
    }
    if (r10d == 0x2b) {
        goto label_99;
    }
    r14 = rbp;
    memset (r14, 0x20, rdx);
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *(rax);
label_14:
    eax = edx;
    *((rsp + 0x58)) = 0;
    r9d = 0;
    eax >>= 0x1f;
    *((rsp + 0x40)) = eax;
    eax = edx;
    eax = ~eax;
    eax >>= 0x1f;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    *((rsp + 0x50)) = 2;
    edx = *((rsp + 0x60));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 4));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 8));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    r11 = *((rsp + 0x20));
    eax = *((r11 + 0x18));
    r8d = *((r11 + 0x1c));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    eax = ecx;
    eax -= edx;
    eax = rax + r8 + 7;
label_24:
    rdx = (int64_t) eax;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += eax;
    eax >>= 0x1f;
    edx >>= 2;
    edx -= eax;
    goto label_14;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = r10d;
    __asm ("movdqu xmm4, xmmword [rax + 0x20]");
    __asm ("movdqu xmm0, xmmword [rax]");
    __asm ("movdqu xmm2, xmmword [rax + 0x10]");
    rax = *((rax + 0x30));
    *((rsp + 0x70)) = xmm0;
    *((rsp + 0x80)) = xmm2;
    *((rsp + 0xa0)) = rax;
    *((rsp + 0x8c)) = 0xffffffff;
    *((rsp + 0x40)) = xmm4;
    *((rsp + 0x90)) = xmm4;
    rax = mktime_z (*((rsp + 0x28)), rsp + 0x70, rdx, rcx, r8, r9);
    r9d = *((rsp + 0x8c));
    r10d = *((rsp + 0x50));
    rsi = rax;
    if (r9d < 0) {
        goto label_100;
    }
    rax >>= 0x3f;
    rcx = rsi;
    rdi = rsp + 0xc7;
    r11 = 0x6666666666666667;
    *((rsp + 0x40)) = rax;
    r8 = rdi;
    r9d = 0x30;
    do {
        rax = rcx;
        rdx:rax = rax * r11;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rbp = rdx * 5;
        rax = rdx;
        rbp += rbp;
        rcx -= rbp;
        rdx = rcx;
        rcx = rax;
        eax = r9d;
        eax -= edx;
        edx += 0x30;
        __asm ("cmovs edx, eax");
        r8--;
        *(r8) = dl;
    } while (rcx != 0);
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 1;
label_21:
    if (r10d == 0) {
        goto label_101;
    }
    al = (r10d != 0x2d) ? 1 : 0;
label_34:
    __asm ("cmovs r15d, dword [rsp + 0x50]");
    rdi -= r8;
    if (*((rsp + 0x40)) != 0) {
        goto label_102;
    }
    if (*((rsp + 0x58)) != 0) {
        goto label_103;
    }
    rcx = (int64_t) edi;
    if (r15d <= edi) {
        goto label_104;
    }
    if (al == 0) {
        goto label_104;
    }
label_33:
    rdx = (int64_t) r15d;
    r15 = rcx;
    if (rdx >= rcx) {
        r15 = rdx;
    }
label_32:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (rdx > rcx) {
        rdx -= rcx;
        *((rsp + 0x50)) = r8;
        *((rsp + 0x40)) = rcx;
        rbp = r14 + rdx;
        if (r10d == 0x30) {
            goto label_105;
        }
        if (r10d == 0x2b) {
            goto label_105;
        }
        r14 = rbp;
        memset (r14, 0x20, rdx);
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_43:
    if (*((rsp + 0x38)) == 0) {
        goto label_106;
    }
    *((rsp + 0x38)) = r8;
    rbp = rcx - 1;
    if (rcx == 0) {
        goto label_36;
    }
    *((rsp + 0x40)) = rcx;
    rax = ctype_toupper_loc ();
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
    do {
        esi = *((r8 + rbp));
        rdx = *(rax);
        edx = *((rdx + rsi*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_36:
    r14 += rcx;
    goto label_15;
    rbx--;
    goto label_8;
    rax = rbx - 1;
    if (rax == rbp) {
        goto label_107;
    }
    rbx = rax;
    goto label_8;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_108;
    }
    if (r15d < 0) {
        goto label_108;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 != 0) {
        if (r15d > 1) {
            rdx--;
            r15 = r14 + rdx;
            if (r10d == 0x30) {
                goto label_109;
            }
            if (r10d == 0x2b) {
                goto label_109;
            }
            r14 = r15;
            memset (r14, 0x20, rdx);
        }
label_45:
        *(r14) = 9;
        r14++;
    }
label_23:
    r13 += rbp;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    goto label_16;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    ecx = *((rax + 0x14));
    eax = rcx + 0x76c;
    rsp + 0x40 = (ecx < 0xfffff894) ? 1 : 0;
    eax -= eax;
    eax &= 0xffffff9d;
    eax += ecx;
    rdx = (int64_t) eax;
    eax >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x25;
    edx -= eax;
    edx += 0x13;
    al = (ecx >= 0xfffff894) ? 1 : 0;
    if (r10d != 0) {
        goto label_110;
    }
    r10d = *((rsp + 0x34));
    if (*((rsp + 0x34)) == 0x2b) {
        goto label_111;
    }
label_53:
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    goto label_18;
    if (edi != 0) {
        goto label_8;
    }
    *((rsp + 0x40)) = 0xffffffff;
    r11 = "%m/%d/%y";
label_20:
    r8d = *((rsp + 0x38));
    r9d = r10d;
    rdx = r11;
    eax = *((rsp + 0x518));
    edi = 0;
    rsi = 0xffffffffffffffff;
    eax = *((rsp + 0x58));
    rcx = *((rsp + 0x40));
    *((rsp + 0x78)) = r10d;
    *((rsp + 0x70)) = r8d;
    *((rsp + 0x58)) = r11;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r10d = *((rsp + 0x78));
    r11 = *((rsp + 0x38));
    r8d = *((rsp + 0x50));
    if (r10d == 0x2d) {
        goto label_112;
    }
    if (r15d < 0) {
        goto label_112;
    }
    rdx = (int64_t) r15d;
    rax = rdx;
    if (rax >= rdx) {
        rax = rbp;
    }
    *((rsp + 0x38)) = rax;
label_39:
    r15 = *((rsp + 8));
    r15 -= r13;
    if (r15 <= *((rsp + 0x38))) {
        goto label_9;
    }
    if (r14 != 0) {
        if (rdx > rbp) {
            rdx -= rbp;
            *((rsp + 0x64)) = r10d;
            rcx = r14 + rdx;
            *((rsp + 0x58)) = r11;
            *((rsp + 0x68)) = rcx;
            *((rsp + 0x50)) = r8d;
            if (r10d == 0x30) {
                goto label_113;
            }
            if (r10d == 0x2b) {
                goto label_113;
            }
            memset (r14, 0x20, rdx);
            r14 = *((rsp + 0x68));
            r10d = *((rsp + 0x64));
            r11 = *((rsp + 0x58));
            r8d = *((rsp + 0x50));
        }
label_49:
        rdi = r14;
        r9d = r10d;
        rdx = r11;
        eax = *((rsp + 0x518));
        rsi = r15;
        r14 += rbp;
        eax = *((rsp + 0x58));
        rcx = *((rsp + 0x40));
        eax = _strftime_internal_isra_0 ();
        goto label_19;
    }
    r13 += *((rsp + 0x38));
    goto label_0;
    if (edi != 0) {
        goto label_8;
    }
    if (r15d < 0) {
        if (r10d == 0) {
            goto label_114;
        }
    }
    edi = r15 - 6;
    eax = 0;
    r11 = "%Y-%m-%d";
    __asm ("cmovns eax, edi");
    *((rsp + 0x40)) = eax;
    goto label_20;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    edx = *((rax + 0x18));
    goto label_14;
    if (edi == 0x4f) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x10));
    *((rsp + 0x40)) = 0;
    edx = rax * 5;
    edx = rax + rdx*2;
    edx >>= 5;
    edx++;
label_25:
    rdi = rsp + 0xc7;
    r8 = rdi;
label_22:
    rsi = rdi;
    if ((r9b & 1) != 0) {
        *((r8 - 1)) = 0x3a;
        rsi--;
    }
    eax = edx;
    ecx = edx;
    r9d >>= 1;
    r8 = rsi - 1;
    rax *= rbp;
    rax >>= 0x23;
    r11d = rax * 5;
    r11d += r11d;
    ecx -= r11d;
    ecx += 0x30;
    *((rsi - 1)) = cl;
    if (edx > 9) {
        goto label_115;
    }
    if (r9d == 0) {
        goto label_21;
    }
label_115:
    edx = eax;
    goto label_22;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x18));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    edx -= ecx;
    edx++;
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    edx = *((rsp + 0x60));
label_27:
    eax = 0x5f;
    *((rsp + 0x50)) = 2;
    if (r10d == 0) {
        r10d = eax;
    }
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    eax = *((rax + 0x10));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_116;
    }
    if (r15d < 0) {
        goto label_116;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    if (r15d > 1) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_117;
        }
        if (r10d == 0x2b) {
            goto label_117;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_46:
    *(r14) = 0xa;
    r14++;
    goto label_23;
    if (edi == 0x45) {
        goto label_8;
    }
    rcx = *((rsp + 0x20));
    eax = *((rcx + 0x1c));
    eax -= *((rcx + 0x18));
    eax += 7;
    goto label_24;
    if (edi == 0x45) {
        goto label_17;
    }
    if (edi == 0x4f) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0x14));
    rsp + 0x40 = (edx < 0xfffff894) ? 1 : 0;
    edx += 0x76c;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 4;
label_93:
    eax = edx;
    eax = -eax;
    if (*((rsp + 0x40)) != 0) {
        edx = eax;
    }
    goto label_25;
    edi = *((rsp + 0x38));
    eax = 0;
    *((rsp + 0x50)) = r10d;
    *((rsp + 0x40)) = r8b;
    if (r8b != 0) {
        edi = eax;
    }
    *((rsp + 0x38)) = dil;
    rax = strlen (r12);
    r10d = *((rsp + 0x50));
    r8d = *((rsp + 0x40));
    if (r10d == 0x2d) {
        goto label_119;
    }
    if (r15d < 0) {
        goto label_119;
    }
    r15 = (int64_t) r15d;
    rax = r15;
    if (rax >= r15) {
        rax = rbp;
    }
    *((rsp + 0x40)) = rax;
label_47:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= *((rsp + 0x40))) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_120;
    }
    if (rbp < r15) {
        rdx = r15;
        *((rsp + 0x50)) = r8b;
        rdx -= rbp;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_121;
        }
        if (r10d == 0x2b) {
            goto label_121;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r8d = *((rsp + 0x50));
    }
label_59:
    if (r8b != 0) {
        goto label_122;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_123;
    }
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_toupper_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
label_57:
    r14 += rbp;
label_120:
    r13 += *((rsp + 0x40));
    goto label_0;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    rdx = *((rax + 0x14));
    rax = rdx;
    rdx *= 0x51eb851f;
    ecx = eax;
    ecx >>= 0x1f;
    rdx >>= 0x25;
    edx -= ecx;
    ecx = edx * 0x64;
    edx = eax;
    edx -= ecx;
    if (edx < 0) {
        ecx = edx;
        edx += 0x64;
        ecx = -ecx;
        if (eax > 0xfffff893) {
            edx = ecx;
            goto label_124;
        }
    }
label_124:
    if (r10d != 0) {
        goto label_61;
    }
label_50:
    eax = *((rsp + 0x34));
    if (eax == 0x2b) {
        goto label_92;
    }
    r10d = eax;
    goto label_26;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 0xc));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0xc));
    goto label_27;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 3;
    eax = *((rax + 0x1c));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 8));
    goto label_27;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    r11d = 1;
    if (al == 0x3a) {
        goto label_30;
    }
label_29:
    if (al != 0x7a) {
        goto label_8;
    }
    rbx = rdx;
label_28:
    rax = *((rsp + 0x20));
    r8d = *((rax + 0x20));
    if (r8d < 0) {
        goto label_0;
    }
    rdx = *((rax + 0x28));
    *((rsp + 0x40)) = 1;
    if (edx >= 0) {
        *((rsp + 0x40)) = 0;
        if (edx != 0) {
            goto label_125;
        }
        rsp + 0x40 = (*(r12) == 0x2d) ? 1 : 0;
    }
label_125:
    rax = (int64_t) edx;
    ecx = edx;
    r8 = rax * 0xffffffff91a2b3c5;
    ecx >>= 0x1f;
    rax *= 0xffffffff88888889;
    r8 >>= 0x20;
    rax >>= 0x20;
    r8d += edx;
    eax += edx;
    r8d >>= 0xb;
    eax >>= 5;
    r8d -= ecx;
    eax -= ecx;
    r9 = (int64_t) eax;
    ecx = eax;
    r9 *= 0xffffffff88888889;
    ecx >>= 0x1f;
    r9 >>= 0x20;
    r9d += eax;
    r9d >>= 5;
    r9d -= ecx;
    ecx = r9d * 0x3c;
    r9d = eax;
    eax *= 0x3c;
    r9d -= ecx;
    ecx = edx;
    ecx -= eax;
    if (r11 == 2) {
        goto label_126;
    }
    if (r11 > 2) {
        goto label_127;
    }
    if (r11 == 0) {
        goto label_128;
    }
label_44:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 6;
    eax ^= 1;
    edx += r9d;
    r9d = 4;
    goto label_13;
    r11d = 0;
    goto label_28;
    ebp = 0;
    do {
        ecx = *((rsp + 0x38));
        eax = 0;
        esi = 0x70;
        if (r8b != 0) {
        }
        if (r8b != 0) {
            ecx = eax;
        }
        *((rsp + 0x38)) = cl;
        goto label_16;
        *((rsp + 0x40)) = 0xffffffff;
        r11 = 0x0001bd96;
        goto label_20;
    } while (1);
    *((rsp + 0x40)) = 0xffffffff;
    r11 = "%H:%M:%S";
    goto label_20;
label_30:
    r11++;
    eax = *((rbx + r11));
    rdx = rbx + r11;
    if (al != 0x3a) {
        goto label_29;
    }
    goto label_30;
label_83:
    r15 = r8;
    edx = 0;
    goto label_31;
label_102:
    ecx = 0x2d;
label_40:
    esi = r15 - 1;
    edx = esi;
    edx -= ebp;
    if (edx <= 0) {
        goto label_129;
    }
    if (al == 0) {
        goto label_129;
    }
label_35:
    if (r10d == 0x5f) {
        goto label_130;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
label_42:
        *(r14) = cl;
        r14++;
    }
    r13++;
    rcx = (int64_t) ebp;
    if (r10d == 0x2d) {
        goto label_131;
    }
label_48:
    r15 = rcx;
    edx = 0;
    if (esi < 0) {
        goto label_32;
    }
    r15d = esi;
    goto label_33;
label_101:
    eax = 1;
    r10d = 0x30;
    goto label_34;
label_129:
    edx = 0;
    goto label_35;
label_104:
    if (r10d != 0x2d) {
        goto label_33;
    }
label_131:
    r15 = rcx;
    edx = 0;
    goto label_32;
label_106:
    rdx = rcx;
    *((rsp + 0x38)) = rcx;
    eax = memcpy (r14, r8, rdx);
    rcx = *((rsp + 0x38));
    goto label_36;
label_71:
    *((rsp + 0x38)) = 1;
    goto label_7;
label_72:
    r8d = eax;
    goto label_7;
label_77:
    r14 = r15;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    goto label_37;
label_86:
    rdx = r8;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rsp + 0xb1, rdx);
    r8 = *((rsp + 0x38));
    goto label_38;
label_85:
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_tolower_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
    goto label_38;
label_112:
    *((rsp + 0x38)) = rbp;
    edx = 0;
    goto label_39;
label_103:
    ecx = 0x2b;
    goto label_40;
label_84:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x40));
    rcx = *((rsp + 0x50));
    r8 = *((rsp + 0x58));
    goto label_41;
label_130:
    r9d = r15d;
    r15 = *((rsp + 8));
    r9d -= edx;
    rdx = (int64_t) edx;
    r13 += rdx;
    r15 -= r13;
    if (r14 == 0) {
        goto label_132;
    }
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x68)) = cl;
    *((rsp + 0x64)) = r9d;
    *((rsp + 0x58)) = r10d;
    *((rsp + 0x50)) = r8;
    memset (r14, 0x20, rdx);
    rdx = *((rsp + 0x40));
    r14 += rdx;
    if (r15 <= 1) {
        goto label_9;
    }
    r9d = *((rsp + 0x64));
    r8 = *((rsp + 0x50));
    r10d = *((rsp + 0x58));
    ecx = *((rsp + 0x68));
    esi = r9 - 1;
    goto label_42;
label_105:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    goto label_43;
label_127:
    if (r11 != 3) {
        goto label_8;
    }
    if (ecx != 0) {
        goto label_126;
    }
    if (r9d != 0) {
        goto label_44;
    }
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d;
    *((rsp + 0x50)) = 3;
    eax ^= 1;
    goto label_13;
label_108:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_45;
    }
    goto label_23;
label_116:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_46;
    }
    goto label_23;
label_119:
    *((rsp + 0x40)) = rbp;
    r15d = 0;
    goto label_47;
label_132:
    if (r15 <= 1) {
        goto label_9;
    }
    r13++;
    esi = r9 - 1;
    rcx = (int64_t) ebp;
    goto label_48;
label_113:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x68));
    r8d = *((rsp + 0x50));
    r11 = *((rsp + 0x58));
    r10d = *((rsp + 0x64));
    goto label_49;
label_107:
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_133;
    }
    if (r15d < 0) {
        goto label_133;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    r15d--;
    if (r15d > 0) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_134;
        }
        if (r10d == 0x2b) {
            goto label_134;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_56:
    eax = *(rbx);
    r14++;
    *((r14 - 1)) = al;
    goto label_23;
label_91:
    r8d = 0xfffff894;
    r8d -= eax;
    if (ecx >= r8d) {
        goto label_135;
    }
    edx = -edx;
    if (r10d == 0) {
        goto label_50;
    }
label_61:
    if (r10d != 0x2b) {
        goto label_26;
    }
label_92:
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
    do {
        eax = *((rsp + 0x40));
        r10d = 0x2b;
        rsp + 0x58 = (r15d > *((rsp + 0x50))) ? 1 : 0;
        eax ^= 1;
        goto label_18;
label_94:
        esi = 1;
        edi = 1;
        goto label_51;
label_55:
        *((rsp + 0x50)) = 4;
        eax = 0x270f;
        if (r10d != 0x2b) {
            goto label_136;
        }
label_54:
    } while (eax >= edx);
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    r10d = 0x2b;
    eax ^= 1;
    goto label_18;
label_87:
    r9d--;
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_137;
        }
        eax = r9d;
        r8d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r8d;
        edx = edx:eax % r8d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_137:
    eax += r11d;
    r8d = eax;
    r8d -= ebp;
    r8d += 0x17e;
    rdx = (int64_t) r8d;
    r9d = r8d;
    eax -= r8d;
    rdx *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rdx >>= 0x20;
    edx += r8d;
    edx >>= 2;
    edx -= r9d;
    r9d = rdx*8;
    r9d -= edx;
    r8d = rax + r9 + 3;
    eax = 0xffffffff;
    goto label_52;
label_110:
    if (r10d != 0x2b) {
        goto label_53;
    }
label_111:
    *((rsp + 0x50)) = 2;
    eax = 0x63;
    goto label_54;
label_90:
    rdx = (int64_t) r8d;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += r8d;
    r8d >>= 0x1f;
    edx >>= 2;
    edx -= r8d;
    edx++;
    goto label_14;
label_89:
    r8d = 0xfffff894;
    edx = rcx + rax + 0x76c;
    r8d -= eax;
    rsp + 0x40 = (ecx < r8d) ? 1 : 0;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 4;
    al = (ecx >= r8d) ? 1 : 0;
    goto label_18;
label_133:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_56;
    }
    goto label_23;
label_123:
    memcpy (r14, r12, rbp);
    goto label_57;
label_122:
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_tolower_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
    goto label_57;
label_126:
    r9d *= 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d * 0x2710;
    *((rsp + 0x50)) = 9;
    eax ^= 1;
    edx += r9d;
    r9d = 0x14;
    edx += ecx;
    goto label_13;
label_117:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_46;
label_97:
    rdi = rsp + 0xb0;
    if (rsi >= 8) {
        goto label_138;
    }
    if ((sil & 4) != 0) {
        goto label_139;
    }
    if (rsi == 0) {
        goto label_58;
    }
    eax = *((rsp + 0xb0));
    *(r14) = al;
    if ((sil & 2) == 0) {
        goto label_58;
    }
    eax = *((rsp + rsi + 0xae));
    *((r14 + rsi - 2)) = ax;
    goto label_58;
label_109:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_45;
label_128:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 5;
    eax ^= 1;
    edx += r9d;
    r9d = 0;
    goto label_13;
label_121:
    r14 = r15;
    memset (r14, 0x30, rdx);
    r8d = *((rsp + 0x50));
    goto label_59;
label_118:
    *((rsp + 0x50)) = 4;
    eax = 0x270f;
    goto label_54;
label_114:
    r8d = *((rsp + 0x38));
    r15 = "%Y-%m-%d";
    rsi |= 0xffffffffffffffff;
    eax = *((rsp + 0x518));
    rdx = r15;
    r9d = 0x2b;
    edi = 0;
    rcx = *((rsp + 0x40));
    *((rsp + 0x70)) = r8d;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r11 = r15;
    edx = 0;
    r10d = 0x2b;
    r8d = *((rsp + 0x50));
    *((rsp + 0x38)) = rax;
    *((rsp + 0x40)) = 4;
    goto label_39;
label_99:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    goto label_0;
label_138:
    rax = *((rsp + 0xb0));
    rdx = r14 + 8;
    rdx &= 0xfffffffffffffff8;
    *(r14) = rax;
    rax = *((rsp + rsi + 0xa8));
    *((r14 + rsi - 8)) = rax;
    rax = r14;
    rax -= rdx;
    rdi -= rax;
    rax += rsi;
    rax &= 0xfffffffffffffff8;
    if (rax < 8) {
        goto label_58;
    }
    rax &= 0xfffffffffffffff8;
    ecx = 0;
    do {
        r8 = *((rdi + rcx));
        *((rdx + rcx)) = r8;
        rcx += 8;
    } while (rcx < rax);
    goto label_58;
label_134:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_56;
label_95:
    esi = 0;
    goto label_60;
label_98:
    r13 += rdx;
    goto label_0;
label_69:
    stack_chk_fail ();
label_135:
    edx += 0x64;
    if (r10d == 0) {
        goto label_50;
    }
    goto label_61;
label_136:
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 0;
    eax ^= 1;
    goto label_18;
label_100:
    rax = *((rsp + 0x10));
    *(rax) = 0x4b;
    goto label_62;
label_139:
    eax = *((rsp + 0xb0));
    *(r14) = eax;
    eax = *((rsp + rsi + 0xac));
    *((r14 + rsi - 4)) = eax;
    goto label_58;
}

/* /tmp/tmprboi4m6a @ 0x174a0 */
 
uint64_t revert_tz_part_0 (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    rax = errno_location ();
    r12d = *(rax);
    if (*((rbx + 8)) != 0) {
        goto label_2;
    }
    rdi = 0x0001bddf;
    eax = unsetenv ();
    if (eax == 0) {
        goto label_3;
    }
label_0:
    r12d = *(rbp);
    r13d = 0;
    do {
label_1:
        rdi = rbx;
        rbx = *(rbx);
        fcn_00004630 ();
    } while (rbx != 0);
    *(rbp) = r12d;
    eax = r13d;
    return rax;
label_2:
    eax = setenv (0x0001bddf, rbx + 9, 1);
    if (eax != 0) {
        goto label_0;
    }
label_3:
    tzset ();
    r13d = 1;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x175d0 */
 
uint64_t dbg_save_abbr (uint32_t arg_8h, int64_t arg_9h, int64_t arg_80h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool save_abbr(timezone_t tz,tm * tm); */
    r12 = *((rsi + 0x30));
    if (r12 == 0) {
        goto label_4;
    }
    r13 = rsi;
    if (rsi <= r12) {
        rdx = rsi + 0x38;
        eax = 1;
        if (r12 < rdx) {
            goto label_3;
        }
    }
    rbx = rbp + 9;
    if (*(r12) == 0) {
        goto label_5;
    }
    do {
label_0:
        rsi = r12;
        rdi = rbx;
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
        if (eax == 0) {
            goto label_2;
        }
label_1:
        if (*(rbx) == 0) {
            rax = rbp + 9;
            if (rbx != rax) {
                goto label_6;
            }
            if (*((rbp + 8)) == 0) {
                goto label_6;
            }
        }
        strlen (rbx);
        rbx = rbx + rax + 1;
    } while (*(rbx) != 0);
    rax = *(rbp);
    if (rax == 0) {
        goto label_0;
    }
    rbx = rax + 9;
    rsi = r12;
    rdi = rbx;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_1;
    }
    do {
label_2:
        *((r13 + 0x30)) = rbx;
        eax = 1;
label_3:
        return rax;
label_5:
        rbx = 0x0001bb0a;
    } while (1);
label_4:
    eax = 1;
    return rax;
label_6:
    rax = strlen (r12);
    r14 = rax;
    rdx = rax + 1;
    rax = rbp + 0x80;
    rax -= rbx;
    if (rax > rdx) {
        memcpy (rbx, r12, rdx);
        *((rbx + r14 + 1)) = 0;
        goto label_2;
    }
    rax = tzalloc (r12);
    *(rbp) = rax;
    if (rax != 0) {
        *((rax + 8)) = 0;
        rbx = rax + 9;
        goto label_2;
    }
    eax = 0;
    goto label_3;
}

/* /tmp/tmprboi4m6a @ 0x17700 */
 
uint64_t dbg_set_tz (int64_t arg1) {
    rdi = arg1;
    /* timezone_t set_tz(timezone_t tz); */
    r13 = 0x0001bddf;
    rbx = rdi;
    rax = getenv (r13);
    if (rax == 0) {
        goto label_2;
    }
    while (eax != 0) {
label_0:
        rax = tzalloc (rbp);
        r12 = rax;
        if (rax != 0) {
            if (*((rbx + 8)) != 0) {
                goto label_3;
            }
            rdi = r13;
            eax = unsetenv ();
            if (eax != 0) {
                goto label_4;
            }
label_1:
            tzset ();
        }
        rax = r12;
        return rax;
        rdi = rbx + 9;
        rsi = rax;
        r12d = 1;
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    }
    rax = r12;
    return rax;
label_2:
    r12d = 1;
    if (*((rbx + 8)) != 0) {
        goto label_0;
    }
    rax = r12;
    return rax;
label_3:
    eax = setenv (r13, rbx + 9, 1);
    if (eax == 0) {
        goto label_1;
    }
label_4:
    rax = errno_location ();
    ebx = *(rax);
    if (r12 == 1) {
        goto label_5;
    }
    do {
        rdi = r12;
        r12 = *(r12);
        fcn_00004630 ();
    } while (r12 != 0);
label_5:
    *(rbp) = ebx;
    r12d = 0;
    rax = r12;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18e60 */
 
int64_t dbg_ydhms_diff (int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg3, int64_t arg4, int64_t arg6, int32_t sec1, long_int yday1) {
    int32_t yday0;
    int32_t hour0;
    int32_t min0;
    int32_t sec0;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    r8 = sec1;
    rsi = yday1;
    /* long_int ydhms_diff(long_int year1,long_int yday1,int hour1,int min1,int sec1,int year0,int yday0,int hour0,int min0,int sec0); */
    r11 = (int64_t) ecx;
    rcx = rdi;
    rax = rdi;
    ecx &= 3;
    r10 = (int64_t) edx;
    rax >>= 2;
    rbx = (int64_t) r9d;
    r8 = (int64_t) r8d;
    rdx = rbx;
    eax -= 0xfffffe25;
    r9 = rbx;
    edx &= 3;
    r9 >>= 2;
    r9d -= 0xfffffe25;
    ebp >>= 0x1f;
    rdi -= rbx;
    ecx = rbp + rax;
    eax -= r9d;
    rdx = (int64_t) ecx;
    ecx >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x23;
    edx -= ecx;
    edx -= ebp;
    ebp >>= 0x1f;
    r12d = rbp + r9;
    r9d = edx;
    rdx = (int64_t) edx;
    rcx = (int64_t) r12d;
    r12d >>= 0x1f;
    rcx *= 0x51eb851f;
    rdx >>= 2;
    rcx >>= 0x23;
    ecx -= r12d;
    ecx -= ebp;
    r9d -= ecx;
    rcx = (int64_t) ecx;
    rcx >>= 2;
    eax -= r9d;
    edx -= ecx;
    rcx = *((rsp + 0x20));
    eax += edx;
    rdx = rdi * 9;
    rdx = rdi + rdx*8;
    rax = (int64_t) eax;
    rdx *= 5;
    rdx += rsi;
    rdx -= rcx;
    rax += rdx;
    rdx = *((rsp + 0x28));
    rax *= 3;
    rax = r10 + rax*8;
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r11 + rdx*4;
    rdx = *((rsp + 0x30));
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r8 + rdx*4;
    rdx = *((rsp + 0x38));
    r12 = rbx;
    rax -= rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18f60 */
 
int64_t dbg_ranged_convert (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    time_t x;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t canary;
    int64_t var_38h;
    int64_t var_40h;
    signed int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* tm * ranged_convert(tm * (*)() convert,long_int * t,tm * tp); */
    r15 = rsi;
    r14 = rdx;
    rbx = rdi;
    r12 = *(rsi);
    rbp = rsp + 0x50;
    rsi = rdx;
    rdi = rbp;
    rax = *(fs:0x28);
    eax = 0;
    rax = void (*rbx)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    *((rsp + 0x38)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    *(r15) = r12;
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = *((rsp + 0x38));
        return rax;
label_2:
        rax = errno_location ();
        *((rsp + 0x40)) = rax;
    } while (*(rax) != 0x4b);
    rcx = r12;
    rax = r12;
    ecx &= 1;
    rax >>= 1;
    r13 = rcx + rax;
    if (r12 == r13) {
        goto label_0;
    }
    if (r13 == 0) {
        goto label_0;
    }
    r15d = 0;
    rax = r14;
    *((rsp + 4)) = 0xffffffff;
    r14 = r12;
    r12 = r13;
    r13 = r15;
    r15 = rax;
    while (rax != 0) {
        eax = *(r15);
        r13 = r12;
        *((rsp + 4)) = eax;
        eax = *((r15 + 4));
        *((rsp + 0x2c)) = eax;
        eax = *((r15 + 8));
        *((rsp + 0x28)) = eax;
        eax = *((r15 + 0xc));
        *((rsp + 0x24)) = eax;
        eax = *((r15 + 0x10));
        *((rsp + 0x20)) = eax;
        eax = *((r15 + 0x14));
        *((rsp + 0xc)) = eax;
        eax = *((r15 + 0x18));
        *((rsp + 0x18)) = eax;
        eax = *((r15 + 0x1c));
        *((rsp + 0x1c)) = eax;
        eax = *((r15 + 0x20));
        *((rsp + 8)) = eax;
        rax = *((r15 + 0x28));
        *((rsp + 0x10)) = rax;
        rax = *((r15 + 0x30));
        *((rsp + 0x30)) = rax;
label_1:
        rdx = r13;
        rax = r14;
        rax >>= 1;
        rdx >>= 1;
        rdx += rax;
        rax = r13;
        rax |= r14;
        eax &= 1;
        r12 = rdx + rax;
        if (r12 == r13) {
            goto label_4;
        }
        if (r12 == r14) {
            goto label_4;
        }
        rsi = r15;
        rdi = rbp;
        rax = void (*rbx)(uint64_t) (r12);
    }
    rax = *((rsp + 0x40));
    if (*(rax) != 0x4b) {
        goto label_0;
    }
    r14 = r12;
    goto label_1;
label_4:
    eax = *((rsp + 4));
    r14 = r15;
    if (eax < 0) {
        goto label_0;
    }
    rcx = *((rsp + 0x48));
    *((rsp + 0x38)) = r14;
    *(rcx) = r13;
    *(r14) = eax;
    eax = *((rsp + 0x2c));
    *((r14 + 4)) = eax;
    eax = *((rsp + 0x28));
    *((r14 + 8)) = eax;
    eax = *((rsp + 0x24));
    *((r14 + 0xc)) = eax;
    eax = *((rsp + 0x20));
    *((r14 + 0x10)) = eax;
    eax = *((rsp + 0xc));
    *((r14 + 0x14)) = eax;
    eax = *((rsp + 0x18));
    *((r14 + 0x18)) = eax;
    eax = *((rsp + 0x1c));
    *((r14 + 0x1c)) = eax;
    eax = *((rsp + 8));
    *((r14 + 0x20)) = eax;
    rax = *((rsp + 0x10));
    *((r14 + 0x28)) = rax;
    rax = *((rsp + 0x30));
    *((r14 + 0x30)) = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x19990 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmprboi4m6a @ 0x169b0 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0xeb60 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x16ce0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x17f90 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf910 */
 
void dbg_filemodestring (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void filemodestring(stat const * statp,char * str); */
    edi = *((rdi + 0x18));
    return void (*0xf790)() ();
}

/* /tmp/tmprboi4m6a @ 0x167f0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc ();
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18190 */
 
uint64_t dbg_xcharalloc (void) {
    /* char * xcharalloc(size_t n); */
    rax = fcn_00004670 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x12400 */
 
int64_t dbg_imaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * imaxtostr(intmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    r8 = rsi + 0x14;
    rcx = rdi;
    rsi = 0xcccccccccccccccd;
    if (rdi < 0) {
        goto label_0;
    }
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rsi;
        rdx >>= 3;
        rax = rdx * 5;
        rax += rax;
        rcx -= rax;
        ecx += 0x30;
        *(r8) = cl;
        rcx = rdx;
    } while (rdx != 0);
    rax = r8;
    return rax;
label_0:
    r9 = 0x6666666666666667;
    edi = 0x30;
    do {
        rax = rcx;
        rsi = r8;
        r8--;
        rdx:rax = rax * r9;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rax = rdx * 5;
        eax = rdi + rax*2;
        eax -= ecx;
        rcx = rdx;
        *(r8) = al;
    } while (rdx != 0);
    *((r8 - 1)) = 0x2d;
    r8 = rsi - 2;
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x17800 */
 
void tzfree (uint32_t arg1) {
    rdi = arg1;
    if (rdi == 1) {
        goto label_0;
    }
    rbx = rdi;
    if (rdi == 0) {
        goto label_1;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        fcn_00004630 ();
    } while (rbx != 0);
label_1:
    return;
label_0:
}

/* /tmp/tmprboi4m6a @ 0x16710 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x12500 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x16a00 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x4cd4)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x129a0 */
 
int64_t dbg_ambsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * ambsalign(char const * src,size_t * width,mbs_align_t align,int flags); */
    r14 = rdi;
    r13d = edx;
    r12d = 0;
    rbx = rsi;
    rax = *(rsi);
    *((rsp + 0xc)) = ecx;
    *(rsp) = rax;
    do {
        rbp = rax + 1;
        rdi = r12;
        r15 = r12;
        rax = realloc (rdi, rbp);
        r12 = rax;
        if (rax == 0) {
            goto label_1;
        }
        rax = *(rsp);
        *(rbx) = rax;
        rax = mbsalign (r14, r12, rbp, rbx, r13d, *((rsp + 0xc)));
        if (rax == -1) {
            goto label_2;
        }
    } while (rbp <= rax);
    do {
label_0:
        rax = r12;
        return rax;
label_2:
        rdi = r12;
        r12d = 0;
        fcn_00004630 ();
    } while (1);
label_1:
    rdi = r15;
    fcn_00004630 ();
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x16770 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x105d0 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x0001e340]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x4c10)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x0001e340]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x14eb0 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmprboi4m6a @ 0x4c00 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmprboi4m6a @ 0xe810 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, uint32_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x4b70)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x17200 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x16f40 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x4ced)() ();
    }
    if (rdx == 0) {
        void (*0x4ced)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x11020 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xfdb0 */
 
uint64_t dbg_filenvercmp (uint32_t arg_1h, uint32_t arg_2h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int filenvercmp(char const * a,ptrdiff_t alen,char const * b,ptrdiff_t blen); */
    r12 = rdx;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    *(rsp) = rcx;
    if (rsi < 0) {
        goto label_10;
    }
    al = (rsi == 0) ? 1 : 0;
    do {
        rdx = *(rsp);
        cl = (rdx == 0) ? 1 : 0;
        if (rdx < 0) {
            cl = (*(r12) == 0) ? 1 : 0;
        }
        if (al != 0) {
            goto label_2;
        }
        eax = 1;
        if (cl == 0) {
            ecx = *(r12);
            if (*(rbp) == 0x2e) {
                goto label_11;
            }
            if (cl == 0x2e) {
                goto label_3;
            }
label_4:
            rsi = rsp + 8;
            rdi = rbp;
            rax = file_prefixlen ();
            rsi = rsp;
            rdi = r12;
            r11 = rax;
            rax = file_prefixlen ();
            r13 = *((rsp + 8));
            rdx = r12;
            rcx = rax;
            if (r13 == r11) {
                goto label_12;
            }
label_0:
            eax = verrevcmp (rbp, r11);
            if (eax != 0) {
                goto label_3;
            }
            rcx = *(rsp);
            rdx = r12;
label_1:
            verrevcmp (rbp, r13);
        }
label_3:
        return rax;
label_10:
        al = (*(rbp) == 0) ? 1 : 0;
    } while (1);
label_2:
    ecx = (int32_t) cl;
    eax = rcx - 1;
    return rax;
label_12:
    if (*(rsp) != rax) {
        goto label_0;
    }
    goto label_1;
label_11:
    if (cl != 0x2e) {
        goto label_6;
    }
    al = (rdi == 1) ? 1 : 0;
    if (rdi < 0) {
        al = (*((rbp + 1)) == 0) ? 1 : 0;
    }
    cl = (rdx == 1) ? 1 : 0;
    if (rdx < 0) {
        cl = (*((r12 + 1)) == 0) ? 1 : 0;
    }
    if (al != 0) {
        goto label_2;
    }
    eax = 1;
    if (cl != 0) {
        goto label_3;
    }
    if (*((rbp + 1)) == 0x2e) {
        goto label_13;
    }
label_5:
    if (*((r12 + 1)) != 0x2e) {
        goto label_4;
    }
    eax = 0;
label_7:
    if (rdx < 0) {
        goto label_14;
    }
    if (rdx == 2) {
        goto label_15;
    }
label_8:
    if (eax == 0) {
        goto label_4;
    }
label_6:
    eax = 0xffffffff;
    goto label_3;
label_13:
    if (rdi < 0) {
        goto label_16;
    }
    if (rdi != 2) {
        goto label_5;
    }
label_9:
    if (*((r12 + 1)) != 0x2e) {
        goto label_6;
    }
    eax = 1;
    goto label_7;
label_14:
    if (*((r12 + 2)) != 0) {
        goto label_8;
    }
label_15:
    eax ^= 1;
    goto label_3;
label_16:
    if (*((rbp + 2)) == 0) {
        goto label_9;
    }
    goto label_5;
}

/* /tmp/tmprboi4m6a @ 0x16fe0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x4cf2)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x4cf2)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf5d0 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    rdi = r12 + rax + 1;
    ebx &= 1;
    rax = fcn_00004670 ();
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rdi = r12 + 1;
        rax = fcn_00004670 ();
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x11290 */
 
void dbg_triple_free (int64_t arg1) {
    rdi = arg1;
    /* void triple_free(void * x); */
    rdi = *(rdi);
    fcn_00004630 ();
    rdi = rbp;
    return void (*0x4630)() ();
}

/* /tmp/tmprboi4m6a @ 0x16ea0 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x4ce8)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x199a4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmprboi4m6a @ 0x10b80 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00004630 ();
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00004630 ();
    } while (rbx != 0);
label_6:
    rdi = *(r12);
    fcn_00004630 ();
    rdi = r12;
    return void (*0x4630)() ();
}

/* /tmp/tmprboi4m6a @ 0x18210 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x18960 */
 
int64_t xstrtol_fatal (uint32_t arg1, char * arg2, int64_t arg4, int64_t arg5) {
    int64_t var_6h;
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r8 = arg5;
    rbx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    ebp = exit_failure;
    if (edi <= 3) {
        if (edi > 1) {
            goto label_3;
        }
        if (edi != 1) {
            goto label_4;
        }
        r9 = "%s%s argument '%s' too large";
label_2:
        rax = (int64_t) esi;
        if (esi < 0) {
            goto label_5;
        }
label_0:
        rax <<= 5;
        r12 = 0x0001ed87;
        r13 = *((rcx + rax));
label_1:
        edx = 5;
        rax = dcgettext (0, r9);
        r9 = rbx;
        r8 = r13;
        rcx = r12;
        eax = 0;
        error (ebp, 0, rax);
        abort ();
    }
    if (edi != 4) {
        void (*0x4d01)() ();
    }
    r9 = "invalid %s%s argument '%s';
    rax = (int64_t) esi;
    if (esi >= 0) {
        goto label_0;
    }
label_5:
    r12 = 0x0001ed87;
    *((rsp + 6)) = dl;
    r13 = rsp + 6;
    *((rsp + 7)) = 0;
    r12 -= rax;
    goto label_1;
label_3:
    r9 = "invalid suffix in %s%s argument '%s';
    goto label_2;
label_4:
    return xstrtol_fatal_cold ();
}

/* /tmp/tmprboi4m6a @ 0x18310 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xf6a0 */
 
uint64_t dbg_record_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void record_file(Hash_table * ht,char const * file,stat const * stats); */
    if (rdi != 0) {
        r13 = rsi;
        r12 = rdi;
        rbx = rdx;
        rax = xmalloc (0x18);
        rax = xstrdup (r13);
        rsi = rbp;
        *(rbp) = rax;
        rax = *((rbx + 8));
        *((rbp + 8)) = rax;
        rax = *(rbx);
        *((rbp + 0x10)) = rax;
        rax = hash_insert (r12);
        if (rax == 0) {
            goto label_0;
        }
        if (rbp != rax) {
            rdi = rbp;
            void (*0x11290)() ();
        }
        return rax;
    }
    return rax;
label_0:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x12140 */
 
uint64_t dbg_getuidbyname (int64_t arg1) {
    rdi = arg1;
    /* uid_t * getuidbyname(char const * user); */
    rbx = user_alist;
    if (rbx == 0) {
        goto label_3;
    }
    r12d = *(rdi);
    while (*((rbx + 0x10)) != r12b) {
label_0:
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_3;
        }
    }
    rdi = rbx + 0x10;
    rsi = rbp;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_0;
    }
label_2:
    r12 = rbx;
    rax = r12;
    return rax;
label_3:
    rbx = nouser_alist;
    if (rbx == 0) {
        goto label_4;
    }
    r12d = *(rbp);
    while (*((rbx + 0x10)) != r12b) {
label_1:
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_4;
        }
    }
    rdi = rbx + 0x10;
    rsi = rbp;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_1;
    }
    r12d = 0;
    rax = r12;
    return rax;
label_4:
    rdi = rbp;
    rax = getpwnam ();
    r12 = rax;
    strlen (rbp);
    rdi &= 0xfffffffffffffff8;
    rax = xmalloc (rax + 0x18);
    rbx = rax;
    strcpy (rax + 0x10, rbp);
    if (r12 != 0) {
        eax = *((r12 + 0x10));
        *(rbx) = eax;
        rax = user_alist;
        *(obj.user_alist) = rbx;
        *((rbx + 8)) = rax;
        goto label_2;
    }
    rax = nouser_alist;
    *(obj.nouser_alist) = rbx;
    *((rbx + 8)) = rax;
    rax = r12;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x181b0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x18850 */
 
int64_t dbg_xgethostname (void) {
    idx_t size;
    char[100] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_78h;
    /* char * xgethostname(); */
    r14d = 0;
    r13 = 0xffffffefffbfeffe;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rbp = rsp + 0x10;
    *((rsp + 8)) = 0x64;
    rax = errno_location ();
    rbx = rax;
    eax = 0x64;
    while (eax <= 0x24) {
        if (((r13 >> rax) & 1) < 0) {
            goto label_1;
        }
        r8d = 1;
        rax = xpalloc (0, rsp + 8, 1, 0xffffffffffffffff);
        rax = *((rsp + 8));
        r14 = rbp;
        *((rbp + rax - 1)) = 0;
        r12 = rax - 1;
        rdi = rbp;
        *(rbx) = 0;
        rsi = r12;
        eax = gethostname ();
        if (eax == 0) {
            rax = strlen (rbp);
            rax++;
            if (r12 > rax) {
                goto label_2;
            }
            *(rbx) = 0;
        }
        rdi = r14;
        fcn_00004630 ();
        eax = *(rbx);
    }
label_1:
    r14d = 0;
    do {
label_0:
        rax = *((rsp + 0x78));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r14;
        return rax;
label_2:
    } while (r14 != 0);
    rax = ximemdup (rbp, rax);
    r14 = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf940 */
 
uint64_t dbg_mfile_name_concat (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * mfile_name_concat(char const * dir,char const * base,char ** base_in_result); */
    r12 = rsi;
    *((rsp + 8)) = rdx;
    rax = last_component ();
    r13 = rax;
    rax = base_len (rax);
    r13 -= rbp;
    r14 = r13 + rax;
    rbx = rax;
    rax = strlen (r12);
    r13 = rax;
    if (rbx == 0) {
        goto label_1;
    }
    if (*((rbp + r14 - 1)) == 0x2f) {
        goto label_2;
    }
    ebx = 0;
    r15d = 0;
    eax = 0x2f;
    if (*(r12) == 0x2f) {
        eax = r15d;
    }
    bl = (*(r12) != 0x2f) ? 1 : 0;
    *((rsp + 7)) = al;
    do {
label_0:
        rdi = r14 + r13 + 1;
        rdi += rbx;
        rax = fcn_00004670 ();
        r15 = rax;
        if (rax != 0) {
            rdi = rax;
            rdx = r14;
            rsi = rbp;
            mempcpy ();
            ecx = *((rsp + 7));
            rdi = rax + rbx;
            *(rax) = cl;
            rax = *((rsp + 8));
            if (rax != 0) {
                *(rax) = rdi;
            }
            rdx = r13;
            rsi = r12;
            mempcpy ();
            *(rax) = 0;
        }
        rax = r15;
        return rax;
label_1:
        ebx = 0;
        r15d = 0;
        eax = 0x2e;
        if (*(r12) != 0x2f) {
            eax = r15d;
        }
        bl = (*(r12) == 0x2f) ? 1 : 0;
        *((rsp + 7)) = al;
    } while (1);
label_2:
    *((rsp + 7)) = 0;
    ebx = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x19840 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x4aa0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmprboi4m6a @ 0x10960 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x169e0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x18690 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rdi = r12;
    rax = fcn_00004670 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x4a30)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x4860 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmprboi4m6a @ 0x18170 */
 
uint64_t ximalloc (void) {
    rax = fcn_00004670 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0xe6a0 */
 
uint64_t dbg_argmatch (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmprboi4m6a @ 0x17ed0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x17a60)() ();
}

/* /tmp/tmprboi4m6a @ 0xf4e0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0001e27c);
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x124a0 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xeb00 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x173e0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x18110 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x10560 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xffd0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xe9d0 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    rsi = r14;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x17090 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x4cf7)() ();
    }
    if (rax == 0) {
        void (*0x4cf7)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x10e00 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x4cba)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x0001e334]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        edi = 0x10;
        rax = fcn_00004670 ();
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x0001e338]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x4cba)() ();
label_17:
    __asm ("subss xmm4, dword [0x0001e338]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x104e0 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x16e10 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x18560 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x49b0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmprboi4m6a @ 0x166b0 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x112b0 */
 
void dbg_human_readable (int64_t arg_48h, int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_1h;
    char[41] buf;
    int64_t var_10h_2;
    void ** s1;
    uint32_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    void ** var_30h;
    int64_t var_34h;
    int64_t var_38h;
    int64_t var_40h;
    uint32_t var_48h;
    int64_t var_4ch;
    int64_t var_4eh;
    int64_t var_50h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* char * human_readable(uintmax_t n,char * buf,int opts,uintmax_t from_block_size,uintmax_t to_block_size); */
    r13 = r8;
    r12 = rcx;
    rbx = rdi;
    *(rsp) = rsi;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x48)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = edx;
    edx &= 0x20;
    *((rsp + 0x30)) = edx;
    eax &= 3;
    *((rsp + 0x10)) = eax;
    eax -= eax;
    eax &= 0xffffffe8;
    eax += 0x400;
    *((rsp + 0x34)) = eax;
    rax = localeconv ();
    r15 = *(rax);
    r14 = rax;
    rax = strlen (r15);
    rcx = *((r14 + 0x10));
    r14 = *((r14 + 8));
    rax--;
    eax = 1;
    rdi = r14;
    *((rsp + 0x38)) = rcx;
    if (rax >= 0x10) {
    }
    rax = 0x0001bde2;
    if (rax >= 0x10) {
        r15 = rax;
    }
    rax = strlen (rdi);
    rax = 0x0001bb0a;
    if (rax > 0x10) {
        r14 = rax;
    }
    rax = *(rsp);
    rax += 0x287;
    *((rsp + 8)) = rax;
    if (r13 > r12) {
        goto label_36;
    }
    rax = r12;
    edx = 0;
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    if (rdx == 0) {
        rdx:rax = rax * rbx;
        rcx = rax;
        if (rdx !overflow 0) {
            goto label_37;
        }
    }
label_0:
    *((rsp + 0x20)) = r12;
    *(fp_stack--) = *((rsp + 0x20));
    if (r12 < 0) {
        goto label_38;
    }
label_5:
    rax = *((rsp + 0x18));
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        goto label_39;
    }
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx < 0) {
        goto label_4;
    }
label_3:
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) == 0) {
        goto label_40;
    }
label_2:
    *(fp_stack--) = *((rsp + 0x34));
    ebx = 0;
    *(fp_stack--) = fp_stack[0];
    while (ebx != 8) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_0 = fp_stack[2];
        fp_stack[2] = fp_stack[0];
        fp_stack[0] = fp_tmp_0;
        *(fp_stack--) = fp_stack[0];
        ebx++;
        fp_stack[0] *= fp_stack[2];
        fp_tmp_1 = fp_stack[3];
        fp_stack[3] = fp_stack[0];
        fp_stack[0] = fp_tmp_1;
        if (fp_stack[0] < fp_stack[3]) {
            goto label_41;
        }
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    goto label_42;
label_41:
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
label_42:
    r15 = rbp + 1;
    fp_stack[1] /= fp_stack[0];
    fp_stack++;
    rbp += 2;
    if (*((rsp + 0x10)) == 1) {
        goto label_43;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (*((rsp + 0x10)) <= 1) {
        goto label_44;
    }
    *(fp_stack--) = *(0x0001e338);
    fp_tmp_2 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_2;
    if (fp_stack[0] >= fp_stack[1]) {
        goto label_45;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_21:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x0001e334);
    }
    ecx = *((rsp + 0x10));
    if (ecx != 0) {
        goto label_46;
    }
    fp_tmp_3 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_3;
    if (fp_stack[0] != fp_stack[1]) {
        if (fp_stack[0] == fp_stack[1]) {
            goto label_47;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
    rax++;
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax >= 0) {
        goto label_48;
    }
    fp_stack[0] += *(0x0001e334);
    fp_tmp_4 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_4;
    goto label_47;
label_36:
    if (r12 == 0) {
        goto label_0;
    }
    rax = *((rsp + 0x18));
    edx = 0;
    rax = rdx:rax / r12;
    rdx = rdx:rax % r12;
    r8 = rax;
    if (rdx != 0) {
        goto label_0;
    }
    rax = rbx;
    edx = 0;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rcx = rax;
    rax = rdx * 5;
    edx = 0;
    rax += rax;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rdx += rdx;
    edi = eax;
    if (r8 <= rdx) {
        goto label_49;
    }
    esi = 0;
    sil = (rdx != 0) ? 1 : 0;
label_20:
    r10d = *((rsp + 0x48));
    r10d &= 0x10;
    if (r10d == 0) {
        goto label_50;
    }
label_12:
    r8d = *((rsp + 0x34));
    ebx = 0;
    r11 = r8;
    if (r8 <= rcx) {
        goto label_51;
    }
label_8:
    r8 = *((rsp + 8));
    if (*((rsp + 0x10)) == 1) {
        goto label_52;
    }
label_28:
    r11d = *((rsp + 0x10));
    if (r11d == 0) {
        esi += edi;
        if (esi <= 0) {
            goto label_19;
        }
label_18:
        rcx++;
        if (r10d == 0) {
            goto label_19;
        }
        eax = *((rsp + 0x34));
        if (rax == rcx) {
            goto label_53;
        }
    }
label_19:
    rsi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        rbp--;
        rdx:rax = rax * rsi;
        rax = rcx;
        rdx >>= 3;
        rdi = rdx * 5;
        rdi += rdi;
        rax -= rdi;
        eax += 0x30;
        *(rbp) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    if ((*((rsp + 0x48)) & 4) == 0) {
        goto label_54;
    }
label_6:
    r12 = r8;
    *((rsp + 0x40)) = r8;
    r13 = 0xffffffffffffffff;
    rax = strlen (r14);
    r12 -= rbp;
    rsi = rbp;
    ecx = 0x29;
    r15 = rax;
    rax = rsp + 0x50;
    rdx = r12;
    rdi = rax;
    *((rsp + 0x10)) = rax;
    memcpy_chk ();
    *((rsp + 0x20)) = ebx;
    rbp = *((rsp + 0x40));
    rbx = r12;
    r12 = *((rsp + 0x38));
    while (al == 0) {
        rax = *((rsp + 0x10));
        if (r13 > rbx) {
            r13 = rbx;
        }
        rbx -= r13;
label_1:
        rbp -= r13;
        memcpy (rbp, rax + rbx, r13);
        if (rbx == 0) {
            goto label_55;
        }
        rbp -= r15;
        memcpy (rbp, r14, r15);
        eax = *(r12);
    }
    if (al > 0x7e) {
        goto label_56;
    }
    if (rax > rbx) {
        rax = rbx;
    }
    rbx -= rax;
    r13 = rax;
    rax = *((rsp + 0x10));
    rsi = rax + rbx;
label_7:
    r12++;
    goto label_1;
label_4:
    fp_stack[0] += *(0x0001e334);
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) != 0) {
        goto label_2;
    }
    goto label_40;
label_39:
    fp_stack[0] += *(0x0001e334);
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx >= 0) {
        goto label_3;
    }
    goto label_4;
label_38:
    fp_stack[0] += *(0x0001e334);
    goto label_5;
label_40:
    if (*((rsp + 0x10)) != 1) {
        *(fp_stack--) = fp_stack[?];
        __asm ("fcompi st(1)");
        if (*((rsp + 0x10)) <= 1) {
            goto label_27;
        }
        *(fp_stack--) = *(0x0001e338);
        fp_tmp_5 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_5;
        if (fp_stack[0] >= fp_stack[1]) {
            goto label_57;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        eax = *((rsp + 0x4e));
        ah |= 0xc;
        *((rsp + 0x4c)) = ax;
        *(fp_stack--) = fp_stack[0];
        *((rsp + 0x20)) = fp_stack[0];
        fp_stack--;
        rax = *((rsp + 0x20));
label_24:
        *((rsp + 0x20)) = rax;
        *(fp_stack--) = *((rsp + 0x20));
        if (rax < 0) {
            fp_stack[0] += *(0x0001e334);
        }
        r8d = *((rsp + 0x10));
        if (r8d == 0) {
            fp_tmp_6 = fp_stack[1];
            fp_stack[1] = fp_stack[0];
            fp_stack[0] = fp_tmp_6;
            __asm ("fucompi st(1)");
            if (r8d != 0) {
                if (r8d == 0) {
                    goto label_27;
                }
                fp_stack++;
            } else {
                fp_stack++;
            }
            rax++;
            *((rsp + 0x10)) = rax;
            *(fp_stack--) = *((rsp + 0x10));
            if (rax < 0) {
                goto label_58;
            }
        } else {
            fp_stack[1] = fp_stack[0];
            fp_stack--;
        }
    }
label_27:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    rbx = *((rsp + 0x10));
    rcx = 0x0001e348;
    ? = fp_stack[0];
    fp_stack--;
    rdi = rbx;
    sprintf_chk ();
    ebx = 0xffffffff;
    rax = strlen (rbx);
    rdx = rax;
    r12 = rax;
label_11:
    rbp = *((rsp + 8));
    rbp -= rdx;
    memmove (rbp, *(rsp), rdx);
    r8 = rbp + r12;
label_26:
    if ((*((rsp + 0x48)) & 4) != 0) {
        goto label_6;
    }
    do {
label_54:
        if ((*((rsp + 0x48)) & 0x80) != 0) {
            if (ebx == 0xffffffff) {
                goto label_59;
            }
label_33:
            eax = *((rsp + 0x48));
            eax &= 0x100;
            esi = eax;
            esi |= ebx;
            if (esi != 0) {
                goto label_60;
            }
        }
label_13:
        rax = *((rsp + 8));
        *(rax) = 0;
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_61;
        }
        rax = rbp;
        return rax;
label_56:
        r13 = rbx;
        rsi = *((rsp + 0x10));
        ebx = 0;
        goto label_7;
label_55:
        ebx = *((rsp + 0x20));
    } while (1);
label_9:
    sil = (esi != 0) ? 1 : 0;
    esi = (int32_t) sil;
label_10:
    ebx++;
    if (r8 > r9) {
        goto label_62;
    }
    if (ebx == 8) {
        goto label_8;
    }
label_51:
    rax = rcx;
    edx = 0;
    ecx = esi;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    ecx >>= 1;
    r9 = rax;
    eax = rdx * 5;
    edx = 0;
    eax = rdi + rax*2;
    eax = edx:eax / r11d;
    edx = edx:eax % r11d;
    edx = rcx + rdx*2;
    edi = eax;
    rcx = r9;
    esi += edx;
    if (r11d > edx) {
        goto label_9;
    }
    sil = (r11d < esi) ? 1 : 0;
    esi = (int32_t) sil;
    esi += 2;
    goto label_10;
label_44:
    *(fp_stack--) = fp_stack[0];
    goto label_47;
label_46:
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    goto label_47;
label_48:
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
label_47:
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    eax = 0;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdx = 0xffffffffffffffff;
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    rdx = rax;
    *(fp_stack--) = fp_stack[?];
    if (rdx > rbp) {
        goto label_63;
    }
label_14:
    if ((*((rsp + 0x48)) & 8) != 0) {
        rax = *(rsp);
        if (*((rax + rdx - 1)) == 0x30) {
            goto label_64;
        }
        fp_stack++;
    } else {
        fp_stack++;
    }
    r12 = rdx;
    r12 -= r15;
    goto label_11;
label_37:
    r10d = *((rsp + 0x48));
    esi = 0;
    edi = 0;
    r10d &= 0x10;
    if (r10d != 0) {
        goto label_12;
    }
label_50:
    ebx = 0xffffffff;
    goto label_8;
label_59:
    rcx = *((rsp + 0x18));
    if (rcx <= 1) {
        goto label_65;
    }
    edx = *((rsp + 0x34));
    ebx = 1;
    eax = 1;
    do {
        rax *= rdx;
        if (rcx <= rax) {
            goto label_66;
        }
        ebx++;
    } while (ebx != 8);
label_66:
    esi = *((rsp + 0x48));
    eax = *((rsp + 0x48));
    eax &= 0x100;
    esi &= 0x40;
    if (esi != 0) {
label_16:
        rsi = *(rsp);
        rdi = rsi + 0x288;
        *((rsi + 0x287)) = 0x20;
        *((rsp + 8)) = rdi;
label_15:
        if (ebx == 0) {
            goto label_67;
        }
    }
    rsi = *((rsp + 8));
    r9d = *((rsp + 0x30));
    rdx = rsi + 1;
    if (r9d == 0) {
        if (ebx == 1) {
            goto label_68;
        }
    }
    rbx = (int64_t) ebx;
    rcx = obj_power_letter;
    ecx = *((rcx + rbx));
    *(rsi) = cl;
    if (eax == 0) {
        goto label_69;
    }
    r8d = *((rsp + 0x30));
    if (r8d != 0) {
        goto label_70;
    }
label_22:
    rax = rdx + 1;
    *(rdx) = 0x42;
    *((rsp + 8)) = rax;
    goto label_13;
label_43:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    *(fp_stack--) = fp_stack[?];
    rdx = rax;
    if (rax <= rbp) {
        goto label_14;
    }
    *(fp_stack--) = *(0x0001e394);
    fp_stack[1] *= fp_stack[0];
label_17:
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    rdx = 0xffffffffffffffff;
    eax = 0;
    r15 = *((rsp + 0x10));
    rcx = 0x0001e348;
    esi = 1;
    rdi = r15;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r15);
    rdx = rax;
    r12 = rax;
    goto label_11;
label_60:
    if ((*((rsp + 0x48)) & 0x40) == 0) {
        goto label_15;
    }
    goto label_16;
label_63:
    *(fp_stack--) = *(0x0001e394);
    fp_stack[1] *= fp_stack[0];
label_23:
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(2)");
    if ((*((rsp + 0x48)) & 0x40) <= 0) {
        goto label_17;
    }
    *(fp_stack--) = *(0x0001e338);
    fp_tmp_9 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    if (fp_stack[0] >= fp_stack[2]) {
        goto label_71;
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_25:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x0001e334);
    }
    edx = *((rsp + 0x10));
    if (edx == 0) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        if (fp_stack[0] != fp_stack[1]) {
            if (fp_stack[0] == fp_stack[1]) {
                goto label_72;
            }
            fp_stack++;
        } else {
            fp_stack++;
        }
        rax++;
        *((rsp + 0x10)) = rax;
        *(fp_stack--) = *((rsp + 0x10));
        if (rax < 0) {
            goto label_73;
        }
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
label_72:
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    goto label_17;
label_52:
    rax = rcx;
    rsi = (int64_t) esi;
    eax &= 1;
    rax += rsi;
    al = (rax != 0) ? 1 : 0;
    eax = (int32_t) al;
    eax += edi;
    if (eax > 5) {
        goto label_18;
    }
    goto label_19;
label_49:
    esi = 2;
    eax = 3;
    if (eax < 5) {
        esi = eax;
    }
    goto label_20;
label_45:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_12 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_21;
label_70:
    *((rsi + 1)) = 0x69;
    rdx = rsi + 2;
    goto label_22;
label_64:
    *(fp_stack--) = *(0x0001e394);
    fp_stack[1] *= fp_stack[0];
    if (*((rsp + 0x10)) != 1) {
        goto label_23;
    }
    goto label_17;
label_57:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_24;
label_71:
    fp_stack[0] -= fp_stack[2];
    fp_tmp_14 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    fp_tmp_15 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_15;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_25;
label_53:
    if (ebx == 8) {
        goto label_19;
    }
    ebx++;
    if ((*((rsp + 0x48)) & 8) == 0) {
        goto label_74;
    }
label_35:
    *((r8 - 1)) = 0x31;
    rbp = r8 - 1;
    goto label_26;
label_58:
    fp_stack[0] += *(0x0001e334);
    goto label_27;
label_73:
    fp_stack[0] += *(0x0001e334);
    fp_tmp_16 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_16;
    goto label_17;
label_68:
    *(rsi) = 0x6b;
    if (eax != 0) {
        goto label_22;
    }
label_69:
    *((rsp + 8)) = rdx;
    goto label_13;
label_62:
    if (r9 > 9) {
        goto label_8;
    }
    if (*((rsp + 0x10)) == 1) {
        goto label_75;
    }
    r12d = *((rsp + 0x10));
    if (r12d != 0) {
        goto label_76;
    }
    if (esi == 0) {
        goto label_76;
    }
label_30:
    edx = rax + 1;
    if (eax == 9) {
        goto label_77;
    }
    eax = rdx + 0x30;
label_32:
    rdi = *(rsp);
    r8 = rdi + 0x286;
    *((rdi + 0x286)) = al;
    eax = ebp;
    r8 -= rbp;
    if (ebp >= 8) {
        goto label_78;
    }
    if ((bpl & 4) != 0) {
        goto label_79;
    }
    if (eax != 0) {
        edx = *(r15);
        *(r8) = dl;
        if ((al & 2) != 0) {
            goto label_80;
        }
    }
label_29:
    esi = 0;
label_31:
    edi = 0;
    if (*((rsp + 0x10)) != 1) {
        goto label_28;
    }
    goto label_19;
label_80:
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_29;
label_75:
    edx = eax;
    edx &= 1;
    edx += esi;
    if (edx > 2) {
        goto label_30;
    }
label_76:
    if (eax != 0) {
        goto label_81;
    }
label_34:
    r8 = *((rsp + 8));
    if ((*((rsp + 0x48)) & 8) != 0) {
        goto label_31;
    }
    eax = 0x30;
    goto label_32;
label_65:
    ebx = 0;
    goto label_33;
label_78:
    rax = *(r15);
    rsi = r8 + 8;
    rdi = r15;
    rsi &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rsi;
    rdi -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_29;
    }
    eax &= 0xfffffff8;
    r9d = eax;
    eax = 0;
    do {
        edx = eax;
        eax += 8;
        r11 = *((rdi + rdx));
        *((rsi + rdx)) = r11;
    } while (eax < r9d);
    goto label_29;
label_77:
    rcx = r9 + 1;
    if (r9 == 9) {
        goto label_82;
    }
    esi = 0;
    goto label_34;
label_74:
    rax = rbp;
    *((r8 - 1)) = 0x30;
    rax = ~rax;
    r8 += rax;
    eax = ebp;
    if (ebp >= 8) {
        goto label_83;
    }
    ebp &= 4;
    if (ebp != 0) {
        goto label_84;
    }
    if (eax == 0) {
        goto label_35;
    }
    edx = *(r15);
    *(r8) = dl;
    if ((al & 2) == 0) {
        goto label_35;
    }
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_35;
label_83:
    rax = *(r15);
    rcx = r8 + 8;
    rcx &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rcx;
    r15 -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_35;
    }
    eax &= 0xfffffff8;
    edx = 0;
    do {
        esi = edx;
        edx += 8;
        rdi = *((r15 + rsi));
        *((rcx + rsi)) = rdi;
    } while (edx < eax);
    goto label_35;
label_82:
    r8 = *((rsp + 8));
    goto label_29;
label_79:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_29;
label_61:
    eax = stack_chk_fail ();
label_84:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_35;
label_67:
    rdx = *((rsp + 8));
    if (eax != 0) {
        goto label_22;
    }
    goto label_13;
label_81:
    eax += 0x30;
    goto label_32;
}

/* /tmp/tmprboi4m6a @ 0x109d0 */
 
uint64_t dbg_hash_initialize (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = fcn_00004670 ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rdi = r12;
        r12d = 0;
        fcn_00004630 ();
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x4630 */
 
void fcn_00004630 (void) {
    /* [14] -r-x section size 96 named .plt.got */
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmprboi4m6a @ 0x171d0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x16650 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x185d0 */
 
uint64_t xmemdup (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rdi = rsi;
    rax = fcn_00004670 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x4a30)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x168f0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rdi = *(rbx);
        rbx += 0x10;
        fcn_00004630 ();
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        fcn_00004630 ();
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        rdi = r12;
        fcn_00004630 ();
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xff80 */
 
int64_t dbg_current_timespec (void) {
    timespec ts;
    int64_t var_8h;
    int64_t var_18h;
    /* timespec current_timespec(); */
    edi = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rsi = rsp;
    clock_gettime ();
    rax = *(rsp);
    rdx = *((rsp + 8));
    rcx = *((rsp + 0x18));
    rcx -= *(fs:0x28);
    if (rcx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x47d0 */
 
void clock_gettime (void) {
    __asm ("bnd jmp qword [reloc.clock_gettime]");
}

/* /tmp/tmprboi4m6a @ 0x10730 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x4ca5)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x14b30 */
 
void dbg_nstrftime (int64_t arg5, int64_t arg6) {
    r8 = arg5;
    r9 = arg6;
    /* size_t nstrftime(char * s,size_t maxsize,char const * format,tm const * tp,timezone_t tz,int ns); */
    r9d = 0;
    r8d = 0;
    _strftime_internal_isra_0 ();
}

/* /tmp/tmprboi4m6a @ 0xf690 */
 
int32_t dbg_file_has_acl (char const * name, stat const * sb) {
    rdi = name;
    rsi = sb;
    /* int file_has_acl(char const * name,stat const * sb); */
    eax = 0;
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x11220 */
 
uint64_t dbg_triple_hash (int64_t arg_8h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* size_t triple_hash( const * x,size_t table_size); */
    rbx = rsi;
    rax = hash_pjw (*(rdi));
    edx = 0;
    rax ^= *((rbp + 8));
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    rax = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x19910 */
 
uint64_t rotate_left64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t dbg_hash_pjw (int64_t arg1, size_t tablesize) {
    rdi = arg1;
    rsi = tablesize;
    /* size_t hash_pjw( const * x,size_t tablesize); */
    rdx = *(rdi);
    if (dl == 0) {
        goto label_0;
    }
    eax = 0;
    do {
        rax = rotate_left64 (rax, 9);
        rdi++;
        rax += rdx;
        rdx = *(rdi);
    } while (dl != 0);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    r8 = rdx;
    rax = rdx;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18820 */
 
void dbg_xdectoumax (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* uintmax_t xdectoumax(char const * n_str,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoumax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmprboi4m6a @ 0x18710 */
 
int64_t dbg_xnumtoumax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    uintmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* uintmax_t xnumtoumax(char const * n_str,int base,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x18a30 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0001edf0;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0x1edf0 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmprboi4m6a @ 0x17220 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x10ad0 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x109a0 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x11250 */
 
int32_t dbg_triple_compare_ino_str (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool triple_compare_ino_str( const * x, const * y); */
    rdx = *((rsi + 8));
    eax = 0;
    while (*((rdi + 0x10)) != rcx) {
        return eax;
        rcx = *((rsi + 0x10));
    }
    rsi = *(rsi);
    rdi = *(rdi);
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    al = (eax == 0) ? 1 : 0;
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x10890 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xf4d0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmprboi4m6a @ 0xeb90 */
 
uint32_t dbg_c_strncasecmp (uint32_t arg3, char const * s1, char const * s2) {
    rdx = arg3;
    rdi = s1;
    rsi = s2;
    /* int c_strncasecmp(char const * s1,char const * s2,size_t n); */
    if (rdi == rsi) {
        goto label_0;
    }
    if (rdx == 0) {
        goto label_0;
    }
    rdx--;
    ecx = 0;
    while (rcx != rdx) {
        if (eax == 0) {
            goto label_1;
        }
        rcx++;
        if (r9b != r8b) {
            goto label_1;
        }
        eax = *((rdi + rcx));
        r8d = rax - 0x41;
        r9d = eax;
        if (r8d <= 0x19) {
            eax += 0x20;
            r9d += 0x20;
        }
        r10d = *((rsi + rcx));
        r11d = r10 - 0x41;
        r8d = r10d;
        if (r11d <= 0x19) {
            r10d += 0x20;
            r8d += 0x20;
        }
    }
label_1:
    eax -= r10d;
    return eax;
label_0:
    eax = 0;
    return eax;
}

/* /tmp/tmprboi4m6a @ 0x181e0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x183a0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xe7b0 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        rsi = r12;
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x10c70 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x4cb5)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x4cb5)() ();
    }
    rdi = *(rsp);
    fcn_00004630 ();
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    rdi = *(rbp);
    fcn_00004630 ();
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x197b0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x4800)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x182c0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x17240 */
 
uint64_t dbg_gl_scratch_buffer_dupfree (uint32_t arg1, size_t size) {
    size_t var_8h;
    rdi = arg1;
    rsi = size;
    /* void * gl_scratch_buffer_dupfree(scratch_buffer * buffer,size_t size); */
    rdi += 0x10;
    r12 = *((rdi - 0x10));
    if (r12 == rdi) {
        goto label_1;
    }
    rax = realloc (r12, rsi);
    if (rax == 0) {
        goto label_2;
    }
    do {
label_0:
        return rax;
label_1:
        rdi = rsi;
        *((rsp + 8)) = rsi;
        rax = fcn_00004670 ();
        rdi = rax;
    } while (rax == 0);
    rdx = *((rsp + 8));
    rsi = r12;
    void (*0x4a30)() ();
label_2:
    rax = r12;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x171e0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x17130 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x4cfc)() ();
    }
    if (rax == 0) {
        void (*0x4cfc)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf730 */
 
int64_t dbg_seen_file (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg1, uint32_t arg2, int64_t arg3) {
    F_triple new_ent;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool seen_file(Hash_table const * ht,char const * file,stat const * stats); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi != 0) {
        rax = *((rdx + 8));
        *(rsp) = rsi;
        rsi = rsp;
        *((rsp + 8)) = rax;
        rax = *(rdx);
        *((rsp + 0x10)) = rax;
        rax = hash_lookup ();
        al = (rax != 0) ? 1 : 0;
    }
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x10500 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xe8a0 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmprboi4m6a @ 0x18610 */
 
uint64_t dbg_ximemdup (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rdi = rsi;
    rax = fcn_00004670 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x4a30)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x122f0 */
 
uint64_t dbg_getgidbyname (int64_t arg1) {
    rdi = arg1;
    /* gid_t * getgidbyname(char const * group); */
    rbx = group_alist;
    if (rbx == 0) {
        goto label_3;
    }
    r12d = *(rdi);
    while (*((rbx + 0x10)) != r12b) {
label_0:
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_3;
        }
    }
    rdi = rbx + 0x10;
    rsi = rbp;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_0;
    }
label_2:
    r12 = rbx;
    rax = r12;
    return rax;
label_3:
    rbx = nogroup_alist;
    if (rbx == 0) {
        goto label_4;
    }
    r12d = *(rbp);
    while (*((rbx + 0x10)) != r12b) {
label_1:
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_4;
        }
    }
    rdi = rbx + 0x10;
    rsi = rbp;
    eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_1;
    }
    r12d = 0;
    rax = r12;
    return rax;
label_4:
    rdi = rbp;
    rax = getgrnam ();
    r12 = rax;
    strlen (rbp);
    rdi &= 0xfffffffffffffff8;
    rax = xmalloc (rax + 0x18);
    rbx = rax;
    strcpy (rax + 0x10, rbp);
    if (r12 != 0) {
        eax = *((r12 + 0x10));
        *(rbx) = eax;
        rax = group_alist;
        *(obj.group_alist) = rbx;
        *((rbx + 8)) = rax;
        goto label_2;
    }
    rax = nogroup_alist;
    *(obj.nogroup_alist) = rbx;
    *((rbx + 8)) = rax;
    rax = r12;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x17530 */
 
int64_t dbg_tzalloc (int64_t arg1) {
    rdi = arg1;
    /* timezone_t tzalloc(char const * name); */
    if (rdi == 0) {
        goto label_0;
    }
    strlen (rdi);
    rbx = rax + 1;
    eax = 0x76;
    if (rbx >= rax) {
        rax = rbx;
    }
    rdi = rax;
    rdi += 0x11;
    rdi &= 0xfffffffffffffff8;
    rax = fcn_00004670 ();
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rax) = 0;
    eax = 1;
    *((r12 + 8)) = ax;
    memcpy (r12 + 9, rbp, rbx);
    *((r12 + rbx + 9)) = 0;
    do {
label_1:
        rax = r12;
        return rax;
label_0:
        edi = 0x80;
        rax = fcn_00004670 ();
        r12 = rax;
    } while (rax == 0);
    edx = 0;
    *(r12) = 0;
    rax = r12;
    *((r12 + 8)) = dx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x4a30 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmprboi4m6a @ 0xf590 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x11080 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x0001e338]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rdi = rbp;
        rbp = *((rbp + 8));
        rax = fcn_00004630 ();
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x0001e338]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x168e0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x167f0)() ();
}

/* /tmp/tmprboi4m6a @ 0x17840 */
 
uint64_t dbg_localtime_rz (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
label_1:
    __asm ("bnd jmp qword [reloc.gmtime_r]");
    /* tm * localtime_rz(timezone_t tz,time_t const * t,tm * tm); */
    r14 = rsi;
    if (rdi == 0) {
        goto label_2;
    }
    r12 = rdi;
    rax = set_tz (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rsi = rbp;
    rdi = r14;
    rax = fcn_00004640 ();
    if (rax == 0) {
        goto label_3;
    }
    al = save_abbr (r12, rbp, rdx, rcx, r8);
    if (al == 0) {
        goto label_3;
    }
    while (al != 0) {
        rax = rbp;
        return rax;
label_3:
        if (r13 != 1) {
            rdi = r13;
            eax = revert_tz_part_0 ();
        }
label_0:
        eax = 0;
        return rax;
        rdi = r13;
        al = revert_tz_part_0 ();
    }
    goto label_0;
label_2:
    rdi = r14;
    rsi = rdx;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x12590 */
 
uint64_t dbg_mbsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_24h;
    size_t n;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* size_t mbsalign(char const * src,char * dest,size_t dest_size,size_t * width,mbs_align_t align,int flags); */
    r13 = rdi;
    rbx = rsi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rcx;
    *((rsp + 0x24)) = r8d;
    rax = strlen (rdi);
    *((rsp + 8)) = rax;
    r9 = rax;
    if ((bpl & 2) == 0) {
        goto label_11;
    }
label_3:
    r12 = r9;
    r15d = 0;
    r14d = 0;
label_5:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_12;
    }
    r9 = rax;
    edx = 0;
label_9:
    rsi = *((rsp + 0x10));
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax == 0) {
        goto label_13;
    }
label_0:
    r12d = 0;
    if (eax != 1) {
        r12 = rdx;
        edx &= 1;
        r12 >>= 1;
        rdx += r12;
    }
label_1:
    r8 = rdx + r9;
    if ((bpl & 4) != 0) {
        r8 = r9;
        edx = 0;
    }
    ebp &= 8;
    if (ebp != 0) {
        goto label_14;
    }
    r8 += r12;
label_2:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_4;
    }
    rbp = rbx + rax - 1;
    rdi = rbx;
    if (rbx >= rbp) {
        goto label_15;
    }
    if (rdx != 0) {
        goto label_16;
    }
    goto label_15;
    do {
        if (rbp <= rdi) {
            goto label_15;
        }
label_16:
        rdi++;
        rax = rbx;
        *((rdi - 1)) = 0x20;
        rax -= rdi;
        rax += rdx;
    } while (rax != 0);
label_15:
    rdx = rbp;
    rsi = r13;
    *((rsp + 8)) = r8;
    rdx -= rdi;
    if (rdx > r9) {
        rdx = r9;
    }
    rax = mempcpy ();
    r8 = *((rsp + 8));
    rdx = rax;
    if (rbp <= rax) {
        goto label_17;
    }
    if (r12 != 0) {
        goto label_18;
    }
    goto label_17;
    do {
        if (rbp <= rdx) {
            goto label_17;
        }
label_18:
        rdx++;
        rcx = r12;
        *((rdx - 1)) = 0x20;
        rcx -= rdx;
        rcx += rax;
    } while (rcx != 0);
label_17:
    *(rdx) = 0;
label_4:
    rdi = r15;
    *((rsp + 8)) = r8;
    fcn_00004630 ();
    rdi = r14;
    fcn_00004630 ();
    rax = *((rsp + 8));
    return rax;
label_7:
    r14d = 0;
label_12:
    if (r12 >= rax) {
        goto label_19;
    }
    rax -= r12;
    rsi = *((rsp + 0x10));
    rdx = rax;
    rax = r12;
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax != 0) {
        goto label_0;
    }
label_13:
    r12 = rdx;
    edx = 0;
    goto label_1;
label_14:
    r12d = 0;
    goto label_2;
label_11:
    *((rsp + 0x28)) = rax;
    rax = ctype_get_mb_cur_max ();
    r9 = *((rsp + 0x28));
    if (rax <= 1) {
        goto label_3;
    }
    rax = mbstowcs (0, r13, 0);
    r9 = *((rsp + 0x28));
    if (rax != -1) {
        goto label_20;
    }
    if ((bpl & 1) != 0) {
        goto label_3;
    }
label_10:
    r15d = 0;
    r14d = 0;
    r8 = 0xffffffffffffffff;
    goto label_4;
label_20:
    r8 = rax + 1;
    *((rsp + 0x30)) = r9;
    rax = r8*4;
    *((rsp + 0x28)) = r8;
    rdi = rax;
    *((rsp + 0x38)) = rax;
    rax = fcn_00004670 ();
    r8 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    r15 = rax;
    if (rax == 0) {
        goto label_21;
    }
    rdx = r8;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r8;
    r14d = 0;
    rax = mbstowcs (rax, r13, rdx);
    r9 = *((rsp + 0x30));
    r8 = *((rsp + 0x28));
    r12 = r9;
    if (rax == 0) {
        goto label_5;
    }
    rax = *((rsp + 0x38));
    *((r15 + rax - 4)) = 0;
    edi = *(r15);
    if (edi == 0) {
        goto label_22;
    }
    r12 = r15;
    do {
        *((rsp + 0x30)) = r8;
        *((rsp + 0x28)) = r9;
        eax = iswprint (rdi);
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x30));
        if (eax == 0) {
            *(r12) = 0xfffd;
            r14d = 1;
        }
        edi = *((r12 + 4));
        r12 += 4;
    } while (edi != 0);
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
    if (r14b == 0) {
        goto label_23;
    }
    *((rsp + 8)) = r9;
    rax = wcstombs (0, r15, 0);
    r9 = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
label_8:
    rdi = *((rsp + 0x28));
    *((rsp + 8)) = r9;
    rax = fcn_00004670 ();
    r9 = *((rsp + 8));
    r14 = rax;
    if (rax == 0) {
        goto label_24;
    }
    rax = *((rsp + 0x10));
    edi = *(r15);
    r13 = r15;
    r12d = 0;
    rax = *(rax);
    *((rsp + 8)) = rax;
    if (edi != 0) {
        goto label_25;
    }
    goto label_26;
    do {
        rax = (int64_t) eax;
        rax += r12;
        if (*((rsp + 8)) < rax) {
            goto label_26;
        }
label_6:
        edi = *((r13 + 4));
        r13 += 4;
        r12 = rax;
        if (edi == 0) {
            goto label_26;
        }
label_25:
        eax = wcwidth ();
    } while (eax != 0xffffffff);
    eax = 1;
    *(r13) = 0xfffd;
    rax += r12;
    if (*((rsp + 8)) >= rax) {
        goto label_6;
    }
label_26:
    *(r13) = 0;
    rdi = r14;
    r13 = r14;
    rax = wcstombs (rdi, r15, *((rsp + 0x28)));
    r9 = rax;
    goto label_5;
label_22:
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
label_23:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_7;
    }
    rax = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
    goto label_8;
label_24:
    r8 |= 0xffffffffffffffff;
    if ((bpl & 1) != 0) {
        goto label_5;
    }
    goto label_4;
label_19:
    rax = r12;
    edx = 0;
    goto label_9;
label_21:
    if ((bpl & 1) == 0) {
        goto label_10;
    }
    r12 = r9;
    r14d = 0;
    goto label_5;
}

/* /tmp/tmprboi4m6a @ 0x104d0 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x12f60 */
 
uint32_t dbg_mpsort (int64_t arg_8h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void mpsort( const ** base,size_t n,comparison_function cmp); */
    if (rsi > 2) {
        goto label_0;
    }
    while (eax <= 0) {
        r12 = rbx;
        return;
        r12 = *((rdi + 8));
        rbx = *(rdi);
        rsi = r12;
        rdi = rbx;
        eax = void (*rdx)() ();
    }
    *(rbp) = r12;
    *((rbp + 8)) = rbx;
    return eax;
label_0:
    r8 = rdi + rsi*8;
    rcx = rdx;
    rdx = r8;
    return mpsort_with_tmp_part_0 ();
}

/* /tmp/tmprboi4m6a @ 0x12250 */
 
uint64_t dbg_getgroup (int64_t arg1) {
    rdi = arg1;
    /* char * getgroup(gid_t gid); */
    rbx = group_alist;
    if (rbx != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_1;
        }
label_0:
    } while (*(rbx) != ebp);
    do {
        eax = 0;
        if (*((rbx + 0x10)) != 0) {
            rax = rbx + 0x10;
        }
        r12 = rbx;
        return rax;
label_1:
        edi = ebp;
        r12 = 0x0001bb0a;
        rax = getgrgid ();
        edi = 0x18;
        if (rax != 0) {
            r12 = *(rax);
            strlen (*(rax));
            rdi &= 0xfffffffffffffff8;
        }
        xmalloc (rax + 0x18);
        *(rax) = ebp;
        rbx = rax;
        strcpy (rax + 0x10, r12);
        rax = group_alist;
        *(obj.group_alist) = rbx;
        *((rbx + 8)) = rax;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x19710 */
 
void dbg_rpl_mktime (int64_t arg1, mktime_offset_t localtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    xmm4 = localtime_offset;
    /* time_t rpl_mktime(tm * tp); */
    tzset ();
    rsi = *(reloc.localtime_r);
    rdi = rbp;
    rdx = obj_localtime_offset_0;
    return void (*0x19160)() ();
}

/* /tmp/tmprboi4m6a @ 0x169c0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x10800 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x4cb0)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0xe500 */
 
int64_t dbg_areadlink_with_size (int64_t arg1, uint32_t arg2) {
    char[128] stackbuf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    /* char * areadlink_with_size(char const * file,size_t size); */
    ebx = 0x80;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    r14b = (rsi != 0) ? 1 : 0;
    if (rsi != 0) {
        rbx = rsi + 1;
        eax = 0x401;
        if (rsi < 0x401) {
            rbx = rax;
            goto label_3;
        }
    }
label_3:
    r12 = 0x3fffffffffffffff;
    rax = rsp + 0x10;
    *((rsp + 8)) = rax;
label_1:
    if (rbx != 0x80) {
        goto label_4;
    }
    do {
        r13 = *((rsp + 8));
        r15d = 0;
        if (r14b != 0) {
            goto label_4;
        }
label_0:
        rdx = rbx;
        rsi = r13;
        rdi = rbp;
        rax = readlink ();
        if (rax < 0) {
            goto label_5;
        }
        if (rbx > rax) {
            goto label_6;
        }
        rdi = r15;
        fcn_00004630 ();
        if (rbx > r12) {
            goto label_7;
        }
        rbx += rbx;
    } while (rbx == 0x80);
label_4:
    rdi = rbx;
    rax = fcn_00004670 ();
    r13 = rax;
    if (rax == 0) {
        goto label_8;
    }
    r15 = rax;
    goto label_0;
label_7:
    rax = 0x7fffffffffffffff;
    if (rbx == rax) {
        goto label_8;
    }
    rbx = 0x7fffffffffffffff;
    goto label_1;
label_5:
    rdi = r15;
    r15d = 0;
    fcn_00004630 ();
    do {
label_2:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        rax = r15;
        return rax;
label_6:
        *((r13 + rax)) = 0;
        r12 = rax + 1;
        if (r15 == 0) {
            goto label_10;
        }
    } while (rbx <= r12);
    rax = realloc (r15, r12);
    if (rax != 0) {
        r15 = rax;
    }
    goto label_2;
label_8:
    errno_location ();
    r15d = 0;
    *(rax) = 0xc;
    goto label_2;
label_10:
    rdi = r12;
    rax = fcn_00004670 ();
    r15 = rax;
    if (rax == 0) {
        goto label_2;
    }
    memcpy (rax, r13, r12);
    goto label_2;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x4d10 */
 
void dbg_main (int32_t argc, char ** argv) {
    char const * p;
    char * buf;
    char[3] label;
    int64_t var_10h_2;
    uint32_t var_8h;
    int64_t var_ch;
    uint32_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    uint32_t var_2fh;
    char * var_38h;
    int64_t var_40h;
    int64_t var_42h;
    int64_t var_48h;
    int64_t var_55h;
    int64_t var_56h;
    int64_t var_57h;
    int64_t var_58h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r14d = edi;
    r13 = "abcdfghiklmnopqrstuvw:xABCDFGHI:LNQRST:UXZ1";
    r12 = obj_long_options;
    rbp = 0x0001bbf2;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r15 = rsp + 0x40;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0001bb0a);
    bindtextdomain (rbp, "/usr/local/share/locale");
    rbp = 0x0001a190;
    textdomain (rbp, rsi);
    rdi = dbg_close_stdout;
    *(obj.exit_failure) = 2;
    atexit ();
    *(obj.print_dir_name) = 1;
    rax = 0x8000000000000000;
    *(obj.exit_status) = 0;
    *(obj.pending_dirs) = 0;
    *(obj.current_time) = rax;
    *(0x00026378) = 0xffffffffffffffff;
    *((rsp + 0x10)) = 0xffffffffffffffff;
    *((rsp + 0x20)) = 0xffffffffffffffff;
    *((rsp + 0xc)) = 0xffffffff;
    *((rsp + 8)) = 0xffffffff;
    *((rsp + 0x28)) = 0xffffffff;
    *(rsp) = 0xffffffff;
    *((rsp + 0x2f)) = 0;
    *((rsp + 0x18)) = 0;
    do {
label_4:
        r8 = r15;
        rcx = r12;
        rdx = r13;
        rsi = rbx;
        edi = r14d;
        *((rsp + 0x40)) = 0xffffffff;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_55;
        }
        eax += 0x83;
        if (eax > 0x114) {
            goto label_56;
        }
        rax = *((rbp + rax*4));
        rax += rbp;
        /* switch table (277 cases) at 0x1a190 */
        eax = void (*rax)() ();
        eax = 0;
        *(obj.eolbyte) = 0;
        al = (*(rsp) != 0) ? 1 : 0;
        *(obj.print_with_color) = 0;
        *(rsp) = eax;
        *((rsp + 8)) = 0;
        *((rsp + 0x28)) = 0;
    } while (1);
label_55:
    if (*(obj.output_block_size) == 0) {
        goto label_57;
    }
label_12:
    if (*(rsp) < 0) {
        goto label_58;
    }
    eax = *(rsp);
    *(obj.format) = eax;
    eax -= 2;
    if (eax > 2) {
        goto label_59;
    }
label_2:
    if (*((rsp + 0x10)) == -1) {
        goto label_60;
    }
label_3:
    rax = *((rsp + 0x10));
    edx = 0;
    ecx = 3;
    *(obj.line_length) = rax;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rax -= 0xffffffffffffffff;
    *(obj.max_idx) = rax;
    eax = format;
    eax -= 2;
    if (eax <= 2) {
        rax = *((rsp + 0x20));
        if (rax < 0) {
            goto label_61;
        }
label_20:
        *(obj.tabsize) = rax;
    }
label_19:
    esi = *((rsp + 0x28));
    eax = *((rsp + 0x28));
    eax &= 1;
    esi++;
    if (esi == 0) {
        goto label_62;
    }
label_11:
    *(obj.qmark_funny_chars) = al;
    if (*((rsp + 8)) < 0) {
        goto label_63;
    }
label_32:
    esi = *((rsp + 8));
    set_quoting_style (0);
label_33:
    eax = get_quoting_style (0);
    eax = format;
    if (eax == 0) {
        goto label_64;
    }
    eax -= 2;
    if (eax <= 1) {
        goto label_65;
    }
label_5:
    *(obj.align_variable_outer_quotes) = 0;
    rax = clone_quoting_options (0);
    *(obj.filename_quoting_options) = rax;
    if (ebp == 7) {
        goto label_66;
    }
label_6:
    eax = indicator_style;
    if (eax <= 1) {
        goto label_67;
    }
    eax -= 2;
    rdx = "*=>@|";
    rbp = rax + rdx;
    eax = *((rdx + rax));
    if (al == 0) {
        goto label_67;
    }
    do {
        rdi = filename_quoting_options;
        esi = (int32_t) al;
        edx = 1;
        rbp++;
        set_char_quoting ();
        eax = *(rbp);
    } while (al != 0);
label_67:
    rax = clone_quoting_options (0);
    edx = 1;
    esi = 0x3a;
    rdi = rax;
    *(obj.dirname_quoting_options) = rax;
    set_char_quoting ();
    edx = format;
    eax = *(obj.print_hyperlink);
    eax ^= 1;
    cl = (edx == 0) ? 1 : 0;
    eax &= ecx;
    ecx = *(obj.eolbyte);
    al &= *(obj.dired);
    *(obj.dired) = al;
    eax = (int32_t) al;
    if (eax > ecx) {
        goto label_68;
    }
    eax = *((rsp + 0xc));
    if (eax < 0) {
        goto label_69;
    }
label_17:
    *(obj.sort_type) = eax;
    if (edx == 0) {
        goto label_70;
    }
label_7:
    rbp = *(obj.optind);
    if (*(obj.print_with_color) != 0) {
        goto label_71;
    }
label_24:
    if (*(obj.directories_first) != 0) {
label_25:
        *(obj.check_symlink_mode) = 1;
    }
label_26:
    if (*(obj.dereference) == 0) {
        eax = 1;
        if (*(obj.immediate_dirs) == 0) {
            goto label_72;
        }
label_16:
        *(obj.dereference) = eax;
    }
    if (*(obj.recursive) != 0) {
        rax = hash_initialize (0x1e, 0, dbg.dev_ino_hash, dbg.dev_ino_compare, sym.dev_ino_free);
        *(obj.active_dir_set) = rax;
        if (rax == 0) {
            goto label_73;
        }
        _obstack_begin (obj.dev_ino_obstack, 0, 0, *(reloc.malloc), *(reloc.free));
    }
    rax = getenv (0x0001bddf);
    rax = tzalloc (rax);
    *(obj.localtz) = rax;
    eax = sort_type;
    eax -= 3;
    eax &= 0xfffffffd;
    if (eax != 0) {
        if (*(obj.format) != 0) {
            goto label_74;
        }
    }
label_14:
    *(obj.format_needs_stat) = 1;
    eax = 0;
label_15:
    *(obj.format_needs_type) = al;
    *(obj.format_needs_type) &= 1;
    if (*(obj.dired) != 0) {
        goto label_75;
    }
label_22:
    if (*(obj.print_hyperlink) != 0) {
        goto label_76;
    }
label_31:
    edi = 0x64;
    r12d = r14d;
    *(obj.cwd_n_alloc) = 0x64;
    rax = xnmalloc (edi, 0xd0);
    r12d -= ebp;
    *(obj.cwd_n_used) = 0;
    *(obj.cwd_file) = rax;
    clear_files (rdi, rsi);
    if (r12d <= 0) {
        goto label_77;
    }
    do {
        rdi = *((rbx + rbp*8));
        rcx = 0x0001bb0a;
        edx = 1;
        esi = 0;
        rbp++;
        gobble_file_constprop_0 ();
    } while (r14d > ebp);
    if (*(obj.cwd_n_used) != 0) {
        goto label_78;
    }
label_27:
    r12d--;
    rbp = pending_dirs;
    if (r12d > 0) {
        goto label_53;
    }
    goto label_79;
    do {
label_0:
        edx = *((rbp + 0x10));
        print_dir (rdi, *((rbp + 8)), rdx, rcx, r8, r9);
        rdi = *(rbp);
        fcn_00004630 ();
        rdi = *((rbp + 8));
        fcn_00004630 ();
        rdi = rbp;
        fcn_00004630 ();
        *(obj.print_dir_name) = 1;
label_1:
        rbp = pending_dirs;
label_53:
        if (rbp == 0) {
            goto label_9;
        }
label_10:
        rax = *((rbp + 0x18));
        r8 = active_dir_set;
        rdi = *(rbp);
        *(obj.pending_dirs) = rax;
    } while (r8 == 0);
    if (rdi != 0) {
        goto label_0;
    }
    rax = Scrt1.o;
    rdx = Scrt1.o;
    rdx -= *(0x000260f0);
    if (rdx <= 0xf) {
        goto label_80;
    }
    rdx = rax - 0x10;
    *(0x000260f8) = rdx;
    rdx = *((rax - 0x10));
    rax = *((rax - 8));
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rax;
    rax = hash_remove (r8, r15);
    rdi = rax;
    if (rax == 0) {
        goto label_81;
    }
    fcn_00004630 ();
    rdi = *(rbp);
    fcn_00004630 ();
    rdi = *((rbp + 8));
    fcn_00004630 ();
    rdi = rbp;
    fcn_00004630 ();
    goto label_1;
label_58:
    eax = ls_mode;
    if (eax == 1) {
        goto label_82;
    }
    if (eax == 2) {
        goto label_39;
    }
    eax = 0;
label_40:
    *(obj.format) = eax;
label_59:
    if (*(obj.print_with_color) != 0) {
        goto label_2;
    }
    if (*((rsp + 0x10)) != -1) {
        goto label_3;
    }
label_34:
    *((rsp + 0x10)) = 0x50;
    goto label_3;
    rax = optarg;
    *((rsp + 0x18)) = rax;
    goto label_4;
    _xargmatch_internal ("--time", *(obj.optarg), obj.time_args, obj.time_types, 4, *(obj.argmatch_die));
    rcx = obj_time_types;
    eax = *((rcx + rax*4));
    *(obj.time_type) = eax;
    goto label_4;
    _xargmatch_internal ("--sort", *(obj.optarg), obj.sort_args, obj.sort_types, 4, *(obj.argmatch_die));
    rcx = obj_sort_types;
    eax = *((rcx + rax*4));
    *((rsp + 0x1c)) = eax;
    goto label_4;
    *(obj.human_output_opts) = 0x90;
    *(obj.file_human_output_opts) = 0x90;
    *(obj.output_block_size) = 1;
    *(obj.file_output_block_size) = 1;
    goto label_4;
    _xargmatch_internal ("--quoting-style", *(obj.optarg), obj.quoting_style_args, obj.quoting_style_vals, 4, *(obj.argmatch_die));
    rcx = obj_quoting_style_vals;
    eax = *((rcx + rax*4));
    *((rsp + 0x18)) = eax;
    goto label_4;
    _xargmatch_internal ("--indicator-style", *(obj.optarg), obj.indicator_style_args, obj.indicator_style_types, 4, *(obj.argmatch_die));
    rcx = obj_indicator_style_types;
    eax = *((rcx + rax*4));
    *(obj.indicator_style) = eax;
    goto label_4;
    rsi = optarg;
    if (rsi == 0) {
        goto label_83;
    }
    _xargmatch_internal ("--hyperlink", rsi, obj.when_args, obj.when_types, 4, *(obj.argmatch_die));
    rcx = obj_when_types;
    edx = *((rcx + rax*4));
    if (edx == 1) {
        goto label_83;
    }
    eax = 0;
    if (edx != 2) {
        goto label_84;
    }
    al = stdout_isatty ();
    eax = (int32_t) al;
    goto label_84;
    xmalloc (0x10);
    rdx = optarg;
    *(rax) = rdx;
    rdx = hide_patterns;
    *(obj.hide_patterns) = rax;
    *((rax + 8)) = rdx;
    goto label_4;
    *(obj.directories_first) = 1;
    goto label_4;
    rax = "full-iso";
    *(rsp) = 0;
    *((rsp + 0x18)) = rax;
    goto label_4;
    _xargmatch_internal ("--format", *(obj.optarg), obj.format_args, obj.format_types, 4, *(obj.argmatch_die));
    rcx = obj_format_types;
    eax = *((rcx + rax*4));
    *((rsp + 0x10)) = eax;
    goto label_4;
    *(obj.indicator_style) = 2;
    goto label_4;
    *(obj.dereference) = 3;
    goto label_4;
    rsi = optarg;
    if (rsi == 0) {
        goto label_85;
    }
    _xargmatch_internal ("--color", rsi, obj.when_args, obj.when_types, 4, *(obj.argmatch_die));
    rcx = obj_when_types;
    edx = *((rcx + rax*4));
    if (edx == 1) {
        goto label_85;
    }
    eax = 0;
    if (edx != 2) {
        goto label_86;
    }
    al = stdout_isatty ();
    eax = (int32_t) al;
    goto label_86;
    eax = human_options (*(obj.optarg), obj.human_output_opts, obj.output_block_size);
    if (eax != 0) {
        goto label_87;
    }
    eax = human_output_opts;
    *(obj.file_human_output_opts) = eax;
    rax = output_block_size;
    *(obj.file_output_block_size) = rax;
    goto label_4;
    *((rsp + 0xc)) = 4;
    goto label_4;
    *(obj.time_type) = 2;
    goto label_4;
    *((rsp + 0xc)) = 5;
    goto label_4;
    *(obj.print_block_size) = 1;
    goto label_4;
    *(obj.sort_reverse) = 1;
    goto label_4;
    *((rsp + 0x28)) = 1;
    goto label_4;
    *(obj.indicator_style) = 1;
    goto label_4;
    *(obj.print_group) = 0;
    *(rsp) = 0;
    goto label_4;
    *(rsp) = 3;
    goto label_4;
    rdi = optarg;
    rax = decode_line_length ();
    *((rsp + 0x10)) = rax;
    if (rax >= 0) {
        goto label_4;
    }
    rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "invalid line width");
    r8 = rbx;
    rcx = rax;
    eax = 0;
    error (2, 0, "%s: %s");
    *(obj.print_author) = 1;
    goto label_4;
    *(obj.numeric_ids) = 1;
    *(rsp) = 0;
    goto label_4;
    *(rsp) = 4;
    goto label_4;
    *(obj.print_inode) = 1;
    goto label_4;
    *(obj.human_output_opts) = 0xb0;
    *(obj.file_human_output_opts) = 0xb0;
    *(obj.output_block_size) = 1;
    *(obj.file_output_block_size) = 1;
    goto label_4;
    *(obj.print_owner) = 0;
    *(rsp) = 0;
    goto label_4;
    *(obj.ignore_mode) = 2;
    if (*(rsp) == 0) {
        *(rsp) = 0xffffffff;
    }
    *(obj.print_with_color) = 0;
    *(obj.print_hyperlink) = 0;
    *(obj.print_block_size) = 0;
    *((rsp + 0xc)) = 6;
    goto label_4;
    *(obj.immediate_dirs) = 1;
    goto label_4;
    *(obj.time_type) = 1;
    goto label_4;
    *((rsp + 8)) = 7;
    goto label_4;
    *(obj.ignore_mode) = 2;
    goto label_4;
    *(obj.print_scontext) = 1;
    goto label_4;
    *((rsp + 0xc)) = 1;
    goto label_4;
    *((rsp + 0xc)) = 6;
    goto label_4;
    edx = 5;
    rax = dcgettext (0, "invalid tab size");
    rcx = 0x7fffffffffffffff;
    rax = xnumtoumax (*(obj.optarg), 0, 0, rcx, 0x0001bb0a, rax);
    *((rsp + 0x30)) = rax;
    goto label_4;
    *((rsp + 0xc)) = 3;
    goto label_4;
    *(obj.recursive) = 1;
    goto label_4;
    *((rsp + 8)) = 5;
    goto label_4;
    *((rsp + 8)) = 0;
    goto label_4;
    *(obj.dereference) = 4;
    goto label_4;
    add_ignore_pattern (*(obj.optarg));
    goto label_4;
    *(obj.dereference) = 2;
    goto label_4;
    *(obj.print_group) = 0;
    goto label_4;
    rsi = optarg;
    if (rsi == 0) {
        goto label_88;
    }
    _xargmatch_internal ("--classify", rsi, obj.when_args, obj.when_types, 4, *(obj.argmatch_die));
    rcx = obj_when_types;
    eax = *((rcx + rax*4));
    if (eax == 1) {
        goto label_88;
    }
    if (eax != 2) {
        goto label_4;
    }
    al = stdout_isatty ();
    if (al == 0) {
        goto label_4;
    }
label_88:
    *(obj.indicator_style) = 3;
    goto label_4;
    *(obj.dired) = 1;
    goto label_4;
    *(rsp) = 2;
    goto label_4;
    add_ignore_pattern (0x0001bc5a);
    eax = add_ignore_pattern (0x0001bc59);
    goto label_4;
    *(obj.ignore_mode) = 1;
    goto label_4;
    eax = 0;
    al = (*(rsp) != 0) ? 1 : 0;
    *(rsp) = eax;
    goto label_4;
    eax = ls_mode;
    rcx = Version;
    rsi = 0x0001bbf9;
    if (eax != 1) {
        rsi = 0x0001bb71;
        rax = "vdir";
        if (eax == 2) {
            rsi = rax;
            goto label_89;
        }
    }
label_89:
    eax = 0;
    version_etc (*(obj.stdout), rsi, "GNU coreutils", rcx, "Richard M. Stallman", "David MacKenzie");
    exit (0);
label_83:
    eax = 1;
label_84:
    *(obj.print_hyperlink) = al;
    *(obj.print_hyperlink) &= 1;
    goto label_4;
label_85:
    eax = 1;
label_86:
    *(obj.print_with_color) = al;
    *(obj.print_with_color) &= 1;
    goto label_4;
label_9:
    if (*(obj.print_with_color) != 0) {
        if (*(obj.used_color) != 0) {
            goto label_90;
        }
    }
label_13:
    if (*(obj.dired) != 0) {
        goto label_91;
    }
label_21:
    rbp = active_dir_set;
    if (rbp == 0) {
        goto label_92;
    }
    rdi = rbp;
    rax = hash_get_n_entries ();
    if (rax != 0) {
        void (*0x6743)() ();
    }
label_92:
    eax = exit_status;
    rdx = *((rsp + 0x58));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        void (*0x6762)() ();
    }
label_65:
    if (*(obj.line_length) == 0) {
        goto label_5;
    }
label_64:
    if (ebp == 3) {
        goto label_93;
    }
    if (ebp == 6) {
        goto label_93;
    }
    if (ebp != 1) {
        goto label_5;
    }
label_93:
    *(obj.align_variable_outer_quotes) = 1;
    rax = clone_quoting_options (0);
    *(obj.filename_quoting_options) = rax;
    goto label_6;
label_69:
    if (edx != 0) {
        goto label_94;
    }
    *(obj.sort_type) = 0;
label_70:
    if (*((rsp + 0x18)) == 0) {
        goto label_95;
    }
label_47:
    r12 = *((rsp + 0x18));
    rbp = "posix-";
    goto label_96;
label_8:
    edi = 2;
    al = hard_locale ();
    if (al == 0) {
        goto label_7;
    }
    r12 += 6;
label_96:
    eax = strncmp (r12, rbp, 6);
    if (eax == 0) {
        goto label_8;
    }
    *((rsp + 0x18)) = r12;
    if (*(r12) == 0x2b) {
        goto label_97;
    }
label_48:
    rbp = obj_time_style_args;
    rax = argmatch (*((rsp + 0x18)), rbp, obj.time_style_types, 4);
    if (rax < 0) {
        goto label_98;
    }
    if (rax == 2) {
        goto label_99;
    }
    if (rax > 2) {
        goto label_100;
    }
    if (rax == 0) {
        goto label_101;
    }
    rax--;
    if (rax == 0) {
        rax = "%Y-%m-%d %H:%M";
        *(0x00025048) = rax;
        *(obj.long_time_format) = rax;
    }
label_37:
    abformat_init ();
    goto label_7;
label_77:
    if (*(obj.immediate_dirs) != 0) {
        goto label_102;
    }
    edx = 1;
    esi = 0;
    rdi = 0x0001bde2;
    eax = queue_directory ();
label_18:
    rbp = pending_dirs;
    if (*(obj.cwd_n_used) != 0) {
        goto label_78;
    }
label_79:
    if (rbp == 0) {
        goto label_9;
    }
    if (*((rbp + 0x18)) != 0) {
        goto label_10;
    }
    *(obj.print_dir_name) = 0;
    goto label_10;
label_62:
    eax = 0;
    if (*(obj.ls_mode) == 1) {
        goto label_103;
    }
label_41:
    eax &= 1;
    goto label_11;
label_57:
    rax = getenv ("LS_BLOCK_SIZE");
    human_options (rax, obj.human_output_opts, obj.output_block_size);
    if (rbp == 0) {
        goto label_104;
    }
label_50:
    eax = human_output_opts;
    *(obj.file_human_output_opts) = eax;
    rax = output_block_size;
    *(obj.file_output_block_size) = rax;
label_51:
    if (*((rsp + 0x2f)) == 0) {
        goto label_12;
    }
    *(obj.human_output_opts) = 0;
    *(obj.output_block_size) = 0x400;
    goto label_12;
label_90:
    if (*(obj.color_indicator) == 2) {
        rax = *(0x00025068);
        if (*(rax) == 0x5b1b) {
            goto label_105;
        }
    }
label_42:
    put_indicator (obj.color_indicator);
    put_indicator (0x00025070);
    rdi = stdout;
    fflush_unlocked ();
    edi = 0;
    signal_setup ();
    ebx = stop_signal_count;
    if (ebx == 0) {
        goto label_106;
    }
    do {
        raise (0x13);
        ebx--;
    } while (ebx != 0);
label_106:
    edi = interrupt_signal;
    if (edi == 0) {
        goto label_13;
    }
label_74:
    if (*(obj.print_scontext) != 0) {
        goto label_14;
    }
    if (*(obj.print_block_size) != 0) {
        goto label_14;
    }
    *(obj.format_needs_stat) = 0;
    eax = 1;
    if (*(obj.recursive) != 0) {
        goto label_15;
    }
    if (*(obj.print_with_color) != 0) {
        goto label_15;
    }
    if (*(obj.indicator_style) != 0) {
        goto label_15;
    }
    eax = *(obj.directories_first);
    goto label_15;
label_72:
    if (*(obj.indicator_style) == 3) {
        goto label_16;
    }
    eax -= eax;
    eax &= 0xfffffffe;
    eax += 3;
    goto label_16;
label_94:
    eax = time_type;
    eax--;
    if (eax <= 2) {
        goto label_107;
    }
    eax = 0;
    goto label_17;
label_102:
    rcx = 0x0001bb0a;
    edx = 1;
    esi = 3;
    rdi = 0x0001bde2;
    rax = gobble_file_constprop_0 ();
    goto label_18;
label_66:
    edx = 1;
    esi = 0x20;
    rdi = rax;
    set_char_quoting ();
    goto label_6;
    *(rsp) = 0;
    goto label_4;
    *((rsp + 0x2f)) = 1;
    goto label_4;
label_61:
    *(obj.tabsize) = 8;
    rax = getenv ("TABSIZE");
    if (rax == 0) {
        goto label_19;
    }
    eax = xstrtoumax (rax, 0, 0, r15, 0x0001bb0a);
    if (eax != 0) {
        goto label_108;
    }
    rax = *((rsp + 0x40));
    goto label_20;
label_91:
    rsi = obj_dired_obstack;
    dired_dump_obstack ("//DIRED//");
    rsi = obj_subdired_obstack;
    dired_dump_obstack ("//SUBDIRED//");
    eax = get_quoting_style (*(obj.filename_quoting_options));
    rdx = obj_quoting_style_args;
    edi = 1;
    rsi = "//DIRED-OPTIONS// --quoting-style=%s\n";
    rdx = *((rdx + rax*8));
    eax = 0;
    printf_chk ();
    goto label_21;
label_75:
    r13 = *(reloc.free);
    r12 = *(reloc.malloc);
    _obstack_begin (obj.dired_obstack, 0, 0, r12, r13);
    _obstack_begin (obj.subdired_obstack, 0, 0, r12, r13);
    goto label_22;
label_71:
    rax = getenv ("LS_COLORS");
    *((rsp + 0x38)) = rax;
    if (rax == 0) {
        goto label_109;
    }
    if (*(rax) == 0) {
        goto label_109;
    }
    *((rsp + 0x55)) = 0x3f3f;
    *((rsp + 0x57)) = 0;
    rax = xstrdup (rax);
    *(rsp) = rbx;
    *(obj.color_buf) = rax;
    *((rsp + 0x40)) = rax;
label_23:
    r12 = *((rsp + 0x38));
    eax = *(r12);
    if (al == 0x2a) {
        goto label_110;
    }
    if (al == 0x3a) {
        goto label_111;
    }
    if (al == 0) {
        goto label_112;
    }
    *((rsp + 0x55)) = al;
    eax = *((r12 + 1));
    if (al == 0) {
        goto label_46;
    }
    *((rsp + 0x56)) = al;
    rax = r12 + 3;
    *((rsp + 0x38)) = rax;
    if (*((r12 + 2)) != 0x3d) {
        goto label_46;
    }
    ebx = 0;
    rsi = 0x0001bc24;
    r13 = rsp + 0x55;
    while (eax != 0) {
        rbx++;
        rax = obj_indicator_name;
        rsi = *((rax + rbx*8));
        if (rsi == 0) {
            goto label_113;
        }
        rdi = r13;
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    }
    rcx = (int64_t) ebx;
    rax = obj_color_indicator;
    rcx <<= 4;
    rcx += rax;
    rax = *((rsp + 0x40));
    *((rcx + 8)) = rax;
    al = get_funky_string (r15, rsp + 0x38, 0, rcx);
    if (al != 0) {
        goto label_23;
    }
label_113:
    rbx = *(rsp);
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "unrecognized prefix: %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
label_45:
    edx = 5;
    rax = dcgettext (0, "unparsable value for LS_COLORS environment variable");
    eax = 0;
    error (0, 0, rax);
    rdi = color_buf;
    fcn_00004630 ();
    rdi = color_ext_list;
    while (rdi != 0) {
        r12 = *((rdi + 0x20));
        fcn_00004630 ();
        rdi = r12;
    }
    *(obj.print_with_color) = 0;
label_44:
    if (*(0x000250d0) == 6) {
        goto label_114;
    }
label_35:
    eax = *(obj.directories_first);
    if (*(obj.print_with_color) == 0) {
        goto label_24;
    }
    *(obj.tabsize) = 0;
    if (al != 0) {
        goto label_25;
    }
    al = is_colored (0xd);
    if (al != 0) {
        goto label_25;
    }
    al = is_colored (0xe);
    if (al != 0) {
        goto label_115;
    }
label_43:
    al = is_colored (0xc);
    if (al == 0) {
        goto label_26;
    }
    if (*(obj.format) == 0) {
        goto label_25;
    }
    goto label_26;
label_78:
    sort_files (rdi, rsi);
    if (*(obj.immediate_dirs) == 0) {
        goto label_116;
    }
label_38:
    if (*(obj.cwd_n_used) == 0) {
        goto label_27;
    }
    print_current_files (rdi, rsi, rdx, rcx);
    if (*(obj.pending_dirs) == 0) {
        goto label_9;
    }
    rdi = stdout;
    *(obj.dired_pos)++;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_117;
    }
    rdx = rax + 1;
    rbp = pending_dirs;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
    goto label_10;
label_76:
    eax = 0;
    rsi = obj_RFC3986;
    while (rax <= 0x5a) {
        edx = 1;
        if (eax <= 0x40) {
            edi = rax - 0x30;
            if (edi <= 9) {
                goto label_28;
            }
            ecx -= 0x2d;
            if (ecx <= 1) {
                goto label_118;
            }
label_29:
            if (eax == 0x7e) {
                goto label_118;
            }
            dl = (eax == 0x5f) ? 1 : 0;
        }
label_28:
        *((rsi + rax)) |= dl;
        rax++;
        if (rax == 0x100) {
            goto label_119;
        }
label_30:
        ecx = eax;
    }
    edi = rax - 0x61;
    edx = 1;
    if (edi <= 0x19) {
        goto label_28;
    }
    ecx -= 0x2d;
    if (ecx > 1) {
        goto label_29;
    }
label_118:
    edx = 1;
    *((rsi + rax)) |= dl;
    rax++;
    if (rax != 0x100) {
        goto label_30;
    }
label_119:
    rax = xgethostname ();
    rcx = 0x0001bb0a;
    if (rax == 0) {
        rax = rcx;
    }
    *(obj.hostname) = rax;
    goto label_31;
label_63:
    rax = getenv ("QUOTING_STYLE");
    if (rax == 0) {
        goto label_54;
    }
    r12 = obj_quoting_style_vals;
    eax = argmatch (rax, obj.quoting_style_args, r12, 4);
    if (eax < 0) {
        goto label_120;
    }
    rax = (int64_t) eax;
    eax = *((r12 + rax*4));
    *((rsp + 8)) = eax;
    if (eax >= 0) {
        goto label_32;
    }
label_54:
    *((rsp + 8)) = 7;
    if (*(obj.ls_mode) != 1) {
        goto label_32;
    }
    al = stdout_isatty ();
    if (al == 0) {
        goto label_33;
    }
    *((rsp + 8)) = 3;
    goto label_32;
label_60:
    al = stdout_isatty ();
    if (al != 0) {
        goto label_121;
    }
label_49:
    rax = getenv ("COLUMNS");
    if (rax == 0) {
        goto label_34;
    }
    if (*(rax) == 0) {
        goto label_34;
    }
    rdi = rax;
    rax = decode_line_length ();
    *((rsp + 0x10)) = rax;
    if (rax >= 0) {
        goto label_3;
    }
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "ignoring invalid width in environment variable COLUMNS: %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_34;
label_109:
    rax = getenv ("COLORTERM");
    if (rax == 0) {
        goto label_122;
    }
    if (*(rax) != 0) {
        goto label_35;
    }
label_122:
    rax = getenv (0x0001bdb5);
    if (rax == 0) {
        goto label_123;
    }
    if (*(rax) == 0) {
        goto label_123;
    }
    r12 = "# Configuration file for dircolors, a utility to help you set the";
    *(rsp) = ebp;
    r13 = "TERM ";
    r12 = rbx;
    rbx = rax;
    while (eax != 0) {
label_36:
        strlen (rbp);
        rsi = "# Configuration file for dircolors, a utility to help you set the";
        rbp = rbp + rax + 1;
        rax = rbp;
        rax -= rsi;
        if (rax > 0x12c7) {
            goto label_124;
        }
        eax = strncmp (rbp, r13, 5);
    }
    edx = 0;
    rdi = rbp + 5;
    rsi = rbx;
    eax = fnmatch ();
    if (eax != 0) {
        goto label_36;
    }
    rbp = *(rsp);
    rbx = r12;
    goto label_35;
label_124:
    rbp = *(rsp);
    rbx = r12;
label_123:
    *(obj.print_with_color) = 0;
    goto label_35;
label_100:
    if (rax != 3) {
        goto label_37;
    }
    edi = 2;
    al = hard_locale ();
    if (al == 0) {
        goto label_37;
    }
    edx = 2;
    rax = dcgettext (0, *(obj.long_time_format));
    edx = 2;
    *(obj.long_time_format) = rax;
    rax = dcgettext (0, *(0x00025048));
    *(0x00025048) = rax;
    goto label_37;
label_39:
    *(obj.format) = 2;
    goto label_2;
label_116:
    rax = extract_dirs_from_files (0, 1, rdx, rcx);
    goto label_38;
label_107:
    *(obj.sort_type) = 5;
    goto label_7;
label_98:
    rbx = "  - [posix-]%s\n";
    argmatch_invalid ("time style", *((rsp + 0x18)), rax);
    r12 = stderr;
    edx = 5;
    rax = dcgettext (0, "Valid arguments are:\n");
    rsi = r12;
    rdi = rax;
    eax = fputs_unlocked ();
    rcx = "full-iso";
    do {
        rdi = stderr;
        rbp += 8;
        rdx = rbx;
        eax = 0;
        esi = 1;
        fprintf_chk ();
        rcx = *(rbp);
    } while (rcx != 0);
    rbp = stderr;
    edx = 5;
    rax = dcgettext (0, "  - +FORMAT (e.g., +%H:%M) for a 'date'-style format\n");
    rdi = rax;
    rsi = rbp;
    fputs_unlocked ();
label_56:
    usage (2);
    usage (0);
label_82:
    al = stdout_isatty ();
    if (al != 0) {
        goto label_39;
    }
    eax = 1;
    goto label_40;
label_103:
    al = stdout_isatty ();
    eax = (int32_t) al;
    goto label_41;
label_105:
    if (*(0x00025070) != 1) {
        goto label_42;
    }
label_115:
    if (*(obj.color_symlink_as_referent) != 0) {
        goto label_25;
    }
    goto label_43;
label_112:
    rbx = *(rsp);
    goto label_44;
label_111:
    r12++;
    *((rsp + 0x38)) = r12;
    goto label_23;
label_110:
    r12++;
    rax = xmalloc (0x28);
    *((rsp + 0x38)) = r12;
    r12 = rsp + 0x38;
    r13 = rax;
    rax = color_ext_list;
    rcx = r13;
    *(obj.color_ext_list) = r13;
    *((r13 + 0x20)) = rax;
    rax = *((rsp + 0x40));
    *((r13 + 8)) = rax;
    al = get_funky_string (r15, r12, 1, rcx);
    while (*(rax) != 0x3d) {
label_46:
        rbx = *(rsp);
        goto label_45;
        rax = *((rsp + 0x38));
        rdx = rax + 1;
        *((rsp + 0x38)) = rdx;
    }
    rax = *((rsp + 0x40));
    *((r13 + 0x18)) = rax;
    al = get_funky_string (r15, r12, 0, r13 + 0x10);
    if (al != 0) {
        goto label_23;
    }
    goto label_46;
label_95:
    rax = getenv ("TIME_STYLE");
    *((rsp + 0x18)) = rax;
    if (rax != 0) {
        goto label_47;
    }
    rax = 0x0001bc38;
    *((rsp + 0x18)) = rax;
    goto label_48;
label_97:
    r12++;
    rax = strchr (r12, 0xa);
    if (rax == 0) {
        goto label_125;
    }
    r13 = rax + 1;
    rax = strchr (r13, 0xa);
    if (rax != 0) {
        goto label_126;
    }
    *(rbp) = 0;
label_52:
    *(obj.long_time_format) = r12;
    *(0x00025048) = r13;
    goto label_37;
label_121:
    eax = 0;
    rdx = r15;
    eax = ioctl (1, 0x5413);
    if (eax < 0) {
        goto label_49;
    }
    eax = *((rsp + 0x42));
    if (ax == 0) {
        goto label_49;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_104:
    rax = getenv (0x0001bced);
    if (rax != 0) {
        goto label_50;
    }
    goto label_51;
label_114:
    eax = strncmp (*(str.01_36), "target", 6);
    if (eax != 0) {
        goto label_35;
    }
    *(obj.color_symlink_as_referent) = 1;
    goto label_35;
label_101:
    rax = "%Y-%m-%d %H:%M:%S.%N %z";
    *(0x00025048) = rax;
    *(obj.long_time_format) = rax;
    goto label_37;
label_99:
    rax = "%Y-%m-%d ";
    *(obj.long_time_format) = rax;
    rax = 0x0001bd90;
    *(0x00025048) = rax;
    goto label_37;
label_125:
    r13 = r12;
    goto label_52;
label_117:
    esi = 0xa;
    overflow ();
    rbp = pending_dirs;
    goto label_53;
label_108:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "ignoring invalid tab size in environment variable TABSIZE: %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_19;
label_81:
    assert_fail ("found", "src/ls.c", 0x70d, "main");
label_126:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid time style format %s");
    rcx = r12;
    eax = 0;
    error (2, 0, rax);
label_120:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "ignoring invalid value of environment variable QUOTING_STYLE: %s");
    rcx = r12;
    eax = 0;
    eax = error (0, 0, rax);
    goto label_54;
label_87:
    r8 = optarg;
    esi = *((rsp + 0x40));
    rcx = r12;
    edx = 0;
    edi = eax;
    xstrtol_fatal ();
label_73:
    xalloc_die ();
label_80:
    assert_fail ("dev_ino_size <= obstack_object_size (&dev_ino_obstack)", "src/ls.c", 0x41d, "dev_ino_pop");
label_68:
    edx = 5;
    rax = dcgettext (0, "--dired and --zero are incompatible");
    eax = 0;
    error (2, 0, rax);
}

/* /tmp/tmprboi4m6a @ 0xdbe0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12d = edi;
    r13 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r13;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (r12d);
    }
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n");
    rdx = r13;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "List information about the FILEs (the current directory by default).\nSort entries alphabetically if none of -cftuvSUX nor --sort is specified.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -a, --all                  do not ignore entries starting with .\n  -A, --almost-all           do not list implied . and ..\n      --author               with -l, print the author of each file\n  -b, --escape               print C-style escapes for nongraphic characters\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --block-size=SIZE      with -l, scale sizes by SIZE when printing them;\n                             e.g., '--block-size=M'; see SIZE format below\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -B, --ignore-backups       do not list implied entries ending with ~\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -c                         with -lt: sort by, and show, ctime (time of last\n                             modification of file status information);\n                             with -l: show ctime and sort by name;\n                             otherwise: sort by ctime, newest first\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -C                         list entries by columns\n      --color[=WHEN]         color the output WHEN; more info below\n  -d, --directory            list directories themselves, not their contents\n  -D, --dired                generate output designed for Emacs' dired mode\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -f                         list all entries in directory order\n  -F, --classify[=WHEN]      append indicator (one of */=>@|) to entries WHEN\n      --file-type            likewise, except do not append '*'\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --format=WORD          across -x, commas -m, horizontal -x, long -l,\n                             single-column -1, verbose -l, vertical -C\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --full-time            like -l --time-style=full-iso\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -g                         like -l, but do not list owner\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --group-directories-first\n                             group directories before files;\n                             can be augmented with a --sort option, but any\n                             use of --sort=none (-U) disables grouping\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -G, --no-group             in a long listing, don't print group names\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -h, --human-readable       with -l and -s, print sizes like 1K 234M 2G etc.\n      --si                   likewise, but use powers of 1000 not 1024\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -H, --dereference-command-line\n                             follow symbolic links listed on the command line\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --dereference-command-line-symlink-to-dir\n                             follow each command line symbolic link\n                             that points to a directory\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --hide=PATTERN         do not list implied entries matching shell PATTERN\n                             (overridden by -a or -A)\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --hyperlink[=WHEN]     hyperlink file names WHEN\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --indicator-style=WORD\n                             append indicator with style WORD to entry names:\n                             none (default), slash (-p),\n                             file-type (--file-type), classify (-F)\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -i, --inode                print the index number of each file\n  -I, --ignore=PATTERN       do not list implied entries matching shell PATTERN\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -k, --kibibytes            default to 1024-byte blocks for file system usage;\n                             used only with -s and per directory totals\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -l                         use a long listing format\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -L, --dereference          when showing file information for a symbolic\n                             link, show information for the file the link\n                             references rather than for the link itself\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -m                         fill width with a comma separated list of entries\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -n, --numeric-uid-gid      like -l, but list numeric user and group IDs\n  -N, --literal              print entry names without quoting\n  -o                         like -l, but do not list group information\n  -p, --indicator-style=slash\n                             append / indicator to directories\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -q, --hide-control-chars   print ? instead of nongraphic characters\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --show-control-chars   show nongraphic characters as-is (the default,\n                             unless program is 'ls' and output is a terminal)\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -Q, --quote-name           enclose entry names in double quotes\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --quoting-style=WORD   use quoting style WORD for entry names:\n                             literal, locale, shell, shell-always,\n                             shell-escape, shell-escape-always, c, escape\n                             (overrides QUOTING_STYLE environment variable)\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -r, --reverse              reverse order while sorting\n  -R, --recursive            list subdirectories recursively\n  -s, --size                 print the allocated size of each file, in blocks\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -S                         sort by file size, largest first\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --sort=WORD            sort by WORD instead of name: none (-U), size (-S),\n                             time (-t), version (-v), extension (-X), width\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --time=WORD            change the default of using modification times;\n                               access time (-u): atime, access, use;\n                               change time (-c): ctime, status;\n                               birth time: birth, creation;\n                             with -l, WORD determines which time to show;\n                             with --sort=time, sort by WORD (newest first)\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --time-style=TIME_STYLE\n                             time/date format with -l; see TIME_STYLE below\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -t                         sort by time, newest first; see --time\n  -T, --tabsize=COLS         assume tab stops at each COLS instead of 8\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -u                         with -lt: sort by, and show, access time;\n                             with -l: show access time and sort by name;\n                             otherwise: sort by access time, newest first\n\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -U                         do not sort; list entries in directory order\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -v                         natural sort of (version) numbers within text\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "  -w, --width=COLS           set output width to COLS.  0 means no limit\n  -x                         list entries by lines instead of by columns\n  -X                         sort alphabetically by entry extension\n  -Z, --context              print any security context of each file\n      --zero                 end each output line with NUL, not newline\n  -1                         list one file per line\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe TIME_STYLE argument can be full-iso, long-iso, iso, locale, or +FORMAT.\nFORMAT is interpreted like in date(1).  If FORMAT is FORMAT1<newline>FORMAT2,\nthen FORMAT1 applies to non-recent files and FORMAT2 to recent files.\nTIME_STYLE prefixed with 'posix-' takes effect only outside the POSIX locale.\nAlso the TIME_STYLE environment variable sets the default style to use.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe WHEN argument defaults to 'always' and can also be 'auto' or 'never'.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nUsing color to distinguish file types is disabled both by default and\nwith --color=never.  With --color=auto, ls emits color codes only when\nstandard output is connected to a terminal.  The LS_COLORS environment\nvariable can change the settings.  Use the dircolors(1) command to set it.\n");
    rsi = rbp;
    rdi = rax;
    fputs_unlocked ();
    rbp = stdout;
    edx = 5;
    rax = dcgettext (0, "\nExit status:\n 0  if OK,\n 1  if minor problems (e.g., cannot access subdirectory),\n 2  if serious trouble (e.g., cannot access command-line argument).\n");
    rsi = rbp;
    rbp = 0x0001bbf9;
    rdi = rax;
    fputs_unlocked ();
    eax = ls_mode;
    if (eax != 1) {
        rbp = 0x0001bb71;
        rax = "vdir";
        if (eax == 2) {
            goto label_1;
        }
    }
label_1:
    rax = "test invocation";
    rcx = "sha256sum";
    rbx = rsp;
    *((rsp + 0x60)) = 0;
    *((rsp + 8)) = rax;
    rax = 0x0001bbf2;
    r13 = 0x0001bff4;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    rsi = r13;
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *(rsp) = r13;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    while (eax != 0) {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        rdi = rbp;
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
    }
label_2:
    r14 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    r15 = "https://www.gnu.org/software/coreutils/";
    if (r14 == 0) {
        r14 = rbp;
    }
    rax = dcgettext (0, rsi);
    rcx = r15;
    edi = 1;
    rsi = rax;
    rdx = "GNU coreutils";
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax == 0) {
        goto label_3;
    }
    eax = strncmp (rdi, 0x0001bbfc, 3);
    while (1) {
label_3:
        rsi = r13;
        rdi = rbp;
        r13 = "test";
        eax = fcn_00004660 (rdi, rsi, rdx, rcx, r8, r9);
        edx = 5;
        rsi = "Full documentation <%s%s>\n";
        if (eax != 0) {
            r13 = rbp;
        }
        rax = dcgettext (0, rsi);
        rdx = r15;
        edi = 1;
        rcx = r13;
        rsi = rax;
        r13 = 0x0001bb94;
        eax = 0;
        printf_chk ();
        rax = 0x0001bb0a;
        edx = 5;
        if (rbp != r14) {
            r13 = rax;
        }
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rdx = r14;
        edi = 1;
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        goto label_0;
        rbx = stdout;
        edx = 5;
        rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
        rdi = rax;
        rsi = rbx;
        fputs_unlocked ();
    }
}

/* /tmp/tmprboi4m6a @ 0x19160 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_mktime_internal (int64_t arg2, int64_t arg3, int32_t mon) {
    long_int t;
    long_int ot;
    tm tm;
    time_t x;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    signed int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    uint32_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rsi = arg2;
    rdx = arg3;
    rdi = mon;
    /* time_t mktime_internal(tm * tp,tm * (*)() convert,mktime_offset_t * offset); */
    rbx = rdi;
    rcx = *((rdi + 0x10));
    *((rsp + 0x58)) = rdi;
    *((rsp + 0x20)) = rsi;
    rsi = *((rdi + 0xc));
    *((rsp + 0x40)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = *(rdi);
    *((rsp + 0x48)) = eax;
    eax = *((rdi + 4));
    *((rsp + 0x10)) = eax;
    eax = *((rdi + 8));
    rdi = rcx;
    rcx *= 0x2aaaaaab;
    *((rsp + 0x14)) = eax;
    eax = *((rbx + 0x20));
    rcx >>= 0x21;
    *((rsp + 0x38)) = eax;
    eax = edi;
    eax >>= 0x1f;
    ecx -= eax;
    edx = rcx * 3;
    eax = ecx;
    edx <<= 2;
    edi -= edx;
    edx = edi;
    ecx = edi;
    edx >>= 0x1f;
    eax -= edx;
    rdx = *((rbx + 0x14));
    rax = (int64_t) eax;
    r15 = rax + rdx;
    edx = 0;
    if ((r15b & 3) == 0) {
        rax = 0x8f5c28f5c28f5c29;
        rdx = 0x51eb851eb851eb8;
        rdi = 0x28f5c28f5c28f5c;
        rax *= r15;
        rax += rdx;
        edx = 1;
        rax = rotate_right64 (rax, 2);
        if (rax <= rdi) {
            goto label_6;
        }
    }
label_1:
    eax = ecx;
    r12d = 0x3b;
    r9d = 0x46;
    rdi = r15;
    eax >>= 0x1f;
    eax &= 0xc;
    eax += ecx;
    rcx = rdx * 3;
    rdx = rdx + rcx*4;
    rax = (int64_t) eax;
    rax += rdx;
    rdx = obj___mon_yday;
    eax = *((rdx + rax*2));
    eax--;
    rax = (int64_t) eax;
    rsi += rax;
    rax = *((rsp + 0x48));
    *((rsp + 0x18)) = rsi;
    if (eax <= r12d) {
        r12 = rax;
    }
    eax = 0;
    __asm ("cmovs r12, rax");
    rax = *((rsp + 0x40));
    rax = *(rax);
    *((rsp + 8)) = rax;
    eax = *((rsp + 8));
    eax = -eax;
    *((rsp + 0x4c)) = eax;
    rax = ydhms_diff (rdi, rsi, *((rsp + 0x34)), *((rsp + 0x30)), r12d, r9);
    *((rsp + 0x50)) = rax;
    r13 = rsp + 0xa0;
    r14 = rsp + 0x88;
    *((rsp + 0x88)) = rax;
    *((rsp + 8)) = rax;
    *((rsp + 0x28)) = 6;
    *((rsp + 0x30)) = 0;
    do {
        rax = ranged_convert (*((rsp + 0x20)), r14, r13);
        if (rax == 0) {
            goto label_4;
        }
        ebx = *((rsp + 0xa0));
        eax = *((rsp + 0xac));
        eax = *((rsp + 0xb8));
        eax = *((rsp + 0xd4));
        rax = ydhms_diff (r15, *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), r12d, *((rsp + 0xd4)));
        r10 = *((rsp + 0x88));
        if (rax == 0) {
            goto label_7;
        }
        if (rbp != r10) {
            if (*((rsp + 8)) == r10) {
                goto label_8;
            }
        }
label_0:
        if (*((rsp + 8)) == r10) {
            goto label_5;
        }
        edx = *((rsp + 0xc0));
        rax += r10;
        *((rsp + 8)) = rbp;
        *((rsp + 0x88)) = rax;
        eax = 0;
        al = (edx != 0) ? 1 : 0;
        *((rsp + 0x30)) = eax;
    } while (1);
label_8:
    esi = *((rsp + 0xc0));
    if (esi < 0) {
        goto label_2;
    }
    ecx = *((rsp + 0x38));
    dl = (esi != 0) ? 1 : 0;
    if (ecx < 0) {
        goto label_9;
    }
    cl = (ecx != 0) ? 1 : 0;
    if (cl == dl) {
        goto label_0;
    }
label_2:
    rax = *((rsp + 0x4c));
    rdx = r10;
    rax += *((rsp + 0x50));
    rdx -= rax;
    edi = *((rsp + 0x48));
    rax = *((rsp + 0x40));
    *(rax) = rdx;
    if (edi != ebx) {
        al = (ebx == 0x3c) ? 1 : 0;
        edx = 0;
        dl = (edi <= 0) ? 1 : 0;
        rdx &= rax;
        rax = (int64_t) edi;
        rdx -= r12;
        rax += rdx;
        rax += r10;
        *((rsp + 0x88)) = rax;
        if (rax overflow 0) {
            goto label_5;
        }
        rdi = rsp + 0xe0;
        rsi = r13;
        rax = *((rsp + 0x20));
        rax = void (*rax)(uint64_t, uint64_t) (rax, rax);
        r10 = *((rsp + 8));
        if (rax == 0) {
            goto label_4;
        }
    }
    rcx = *((rsp + 0x58));
    __asm ("movdqa xmm0, xmmword [rsp + 0xa0]");
    __asm ("movdqa xmm1, xmmword [rsp + 0xb0]");
    rax = *((rsp + 0xd0));
    __asm ("movdqa xmm2, xmmword [rsp + 0xc0]");
    __asm ("movups xmmword [rcx], xmm0");
    *((rcx + 0x30)) = rax;
    __asm ("movups xmmword [rcx + 0x10], xmm1");
    __asm ("movups xmmword [rcx + 0x20], xmm2");
    goto label_10;
label_5:
    errno_location ();
    *(rax) = 0x4b;
label_4:
    r10 = 0xffffffffffffffff;
label_10:
    rax = *((rsp + 0x118));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_11;
    }
    rax = r10;
    return rax;
label_6:
    rdx = 0xa3d70a3d70a3d70b;
    rax = r15;
    rdx:rax = rax * rdx;
    rax = r15;
    rax >>= 0x3f;
    rdx += r15;
    rdx >>= 6;
    rdx -= rax;
    edx &= 3;
    dl = (rdx == 1) ? 1 : 0;
    edx = (int32_t) dl;
    goto label_1;
label_9:
    edx = (int32_t) dl;
    if (edx < *((rsp + 0x30))) {
        goto label_0;
    }
    goto label_2;
label_7:
    esi = *((rsp + 0x38));
    eax = *((rsp + 0xc0));
    rsp + 8 = (esi == 0) ? 1 : 0;
    edi = *((rsp + 8));
    rsp + 0x7f = (eax == 0) ? 1 : 0;
    ecx = *((rsp + 0x7f));
    if (cl == dil) {
        goto label_2;
    }
    eax |= esi;
    if (eax < 0) {
        goto label_2;
    }
    rax = rsp + 0xe0;
    *((rsp + 0x78)) = r12d;
    r12 = *((rsp + 0x20));
    *((rsp + 0x28)) = rax;
    rax = rsp + 0x90;
    r14d = 0x92c70;
    *((rsp + 0x30)) = rax;
    rax = rsp + 0x98;
    *((rsp + 0x68)) = rax;
    *((rsp + 0x70)) = r15;
    *((rsp + 0x60)) = r13;
    while (r13d == 1) {
        r14d += 0x92c70;
        if (r14d == 0xdb04f20) {
            goto label_12;
        }
        ebx = r14d;
        r15d = r14 + r14;
        r13d = 2;
        ebx = -ebx;
        rax = (int64_t) ebx;
        rax += rbp;
        *((rsp + 0x90)) = rax;
        if (rax !overflow 0) {
            goto label_13;
        }
label_3:
        ebx += r15d;
    }
    rax = (int64_t) ebx;
    r13d = 1;
    rax += rbp;
    *((rsp + 0x90)) = rax;
    if (rax overflow 0) {
        goto label_3;
    }
label_13:
    rax = ranged_convert (r12, *((rsp + 0x30)), *((rsp + 0x28)));
    if (rax == 0) {
        goto label_4;
    }
    eax = *((rsp + 0x100));
    dl = (eax == 0) ? 1 : 0;
    if (*((rsp + 8)) == dl) {
        goto label_14;
    }
    if (eax >= 0) {
        goto label_3;
    }
label_14:
    eax = *((rsp + 0xe0));
    eax = *((rsp + 0xec));
    eax = *((rsp + 0xf8));
    eax = *((rsp + 0x114));
    rax = ydhms_diff (*((rsp + 0x90)), *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), *((rsp + 0x98)), *((rsp + 0x114)));
    rax += *((rsp + 0xb0));
    rsi = *((rsp + 0x60));
    rdi = *((rsp + 0x68));
    rax = void (*r12)(uint64_t) (rax);
    rdx = *((rsp + 0x38));
    if (rax != 0) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) == 0x4b) {
        goto label_3;
    }
    goto label_4;
label_12:
    eax = *((rsp + 8));
    edx = *((rsp + 0x7f));
    r10 = rbp;
    r13 = *((rsp + 0x60));
    r12 = *((rsp + 0x78));
    eax -= edx;
    rdi = *((rsp + 0x28));
    eax *= 0xe10;
    rsi = r13;
    rax = (int64_t) eax;
    r10 += rax;
    rax = *((rsp + 0x20));
    rax = void (*rax)(uint64_t, uint64_t) (r10, r10);
    if (rax == 0) {
        goto label_5;
    }
    ebx = *((rsp + 0xa0));
    r10 = *((rsp + 8));
    goto label_2;
label_15:
    r12 = *((rsp + 0x78));
    r13 = *((rsp + 0x60));
    r10 = rdx;
    ebx = *((rsp + 0xa0));
    goto label_2;
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x120a0 */
 
uint64_t dbg_getuser (int64_t arg1) {
    rdi = arg1;
    /* char * getuser(uid_t uid); */
    rbx = user_alist;
    if (rbx != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rbx = *((rbx + 8));
        if (rbx == 0) {
            goto label_1;
        }
label_0:
    } while (*(rbx) != ebp);
    do {
        eax = 0;
        if (*((rbx + 0x10)) != 0) {
            rax = rbx + 0x10;
        }
        r12 = rbx;
        return rax;
label_1:
        edi = ebp;
        r12 = 0x0001bb0a;
        rax = getpwuid ();
        edi = 0x18;
        if (rax != 0) {
            r12 = *(rax);
            strlen (*(rax));
            rdi &= 0xfffffffffffffff8;
        }
        xmalloc (rax + 0x18);
        *(rax) = ebp;
        rbx = rax;
        strcpy (rax + 0x10, r12);
        rax = user_alist;
        *(obj.user_alist) = rbx;
        *((rbx + 8)) = rax;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x16690 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x16d80 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf790 */
 
int32_t dbg_strmode (char * bp, int32_t mode) {
    rsi = bp;
    rdi = mode;
    /* void strmode(mode_t mode,char * str); */
    rdx = rsi;
    esi = edi;
    eax = edi;
    ecx = 0x2d;
    esi &= 0xf000;
    if (esi != 0x8000) {
        ecx = 0x64;
        if (esi == sym._init) {
            goto label_1;
        }
        ecx = 0x62;
        if (esi == 0x6000) {
            goto label_1;
        }
        ecx = 0x63;
        if (esi == 0x2000) {
            goto label_1;
        }
        ecx = 0x6c;
        if (esi == 0xa000) {
            goto label_1;
        }
        ecx = 0x70;
        if (esi == 0x1000) {
            goto label_1;
        }
        ecx = 0x73;
        esi = 0x3f;
        if (esi == 0xc000) {
            ecx = esi;
            goto label_1;
        }
    }
label_1:
    *(rdx) = cl;
    ecx = eax;
    ecx &= 0x100;
    ecx -= ecx;
    ecx &= 0xffffffbb;
    ecx += 0x72;
    *((rdx + 1)) = cl;
    ecx = eax;
    ecx &= 0x80;
    ecx -= ecx;
    ecx &= 0xffffffb6;
    ecx += 0x77;
    *((rdx + 2)) = cl;
    ecx = eax;
    ecx &= 0x40;
    ecx -= ecx;
    if ((ah & 8) == 0) {
        goto label_2;
    }
    ecx &= 0xffffffe0;
    ecx += 0x73;
    do {
        *((rdx + 3)) = cl;
        ecx = eax;
        ecx &= 0x20;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 4)) = cl;
        ecx = eax;
        ecx &= 0x10;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 5)) = cl;
        ecx = eax;
        ecx &= 8;
        ecx -= ecx;
        if ((ah & 4) == 0) {
            goto label_3;
        }
        ecx &= 0xffffffe0;
        ecx += 0x73;
label_0:
        *((rdx + 6)) = cl;
        ecx = eax;
        ecx &= 4;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 7)) = cl;
        ecx = eax;
        ecx &= 2;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 8)) = cl;
        ecx = eax;
        ecx &= 1;
        if ((ah & 2) == 0) {
            goto label_4;
        }
        eax -= eax;
        eax &= 0xffffffe0;
        eax += 0x74;
        *((rdx + 9)) = al;
        eax = 0x20;
        *((rdx + 0xa)) = ax;
        return eax;
label_2:
        ecx &= 0xffffffb5;
        ecx += 0x78;
    } while (1);
label_4:
    eax -= eax;
    eax &= 0xffffffb5;
    eax += 0x78;
    *((rdx + 9)) = al;
    eax = 0x20;
    *((rdx + 0xa)) = ax;
    return eax;
label_3:
    ecx &= 0xffffffb5;
    ecx += 0x78;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x16bb0 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x4ce3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x107b0 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x4caa)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmprboi4m6a @ 0x16a90 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x4cd9)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf920 */
 
uint64_t dbg_file_name_concat (void) {
    int64_t var_7h;
    int64_t var_8h;
    /* char * file_name_concat(char const * dir,char const * base,char ** base_in_result); */
    rax = mfile_name_concat (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x11210 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0x11080)() ();
}

/* /tmp/tmprboi4m6a @ 0x16730 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x4cce)() ();
    }
    if (rdx == 0) {
        void (*0x4cce)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x16c40 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00026530]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00026540]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x18530 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x185b0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x12a50 */
 
int64_t dbg_mbsnwidth (void * arg1, int64_t arg3) {
    mbstate_t mbstate;
    int64_t var_8h;
    int64_t var_ch;
    wint_t wc;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rdx = arg3;
    /* int mbsnwidth(char const * string,size_t nbytes,int flags); */
    r15 = rdi;
    rbp = rdi + rsi;
    *((rsp + 0xc)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = ctype_get_mb_cur_max ();
    if (rax <= 1) {
        goto label_7;
    }
    r12d = 0;
    if (r15 >= rbp) {
        goto label_4;
    }
    eax = *((rsp + 0xc));
    r13 = rsp + 0x20;
    r14 = rsp + 0x1c;
    eax &= 2;
    *((rsp + 8)) = eax;
label_2:
    eax = *(r15);
    if (al > 0x5f) {
        goto label_8;
    }
    if (al > 0x40) {
        goto label_3;
    }
    if (al > 0x23) {
        goto label_9;
    }
    if (al > 0x1f) {
        goto label_3;
    }
label_1:
    *(r13) = 0;
    while (eax >= 0) {
        edx = 0x7fffffff;
        edx -= r12d;
        if (edx < eax) {
            goto label_10;
        }
        r12d += eax;
label_0:
        r15 += rbx;
        eax = mbsinit (r13);
        if (eax != 0) {
            goto label_6;
        }
        rdx -= r15;
        rax = rpl_mbrtowc (r14, r15, rbp, r13);
        if (rax == -1) {
            goto label_11;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_12;
        }
        edi = *((rsp + 0x1c));
        ebx = 1;
        if (rax != 0) {
            rbx = rax;
        }
        eax = wcwidth ();
    }
    eax = *((rsp + 8));
    if (eax != 0) {
        goto label_13;
    }
    eax = iswcntrl (*((rsp + 0x1c)));
    if (eax != 0) {
        goto label_0;
    }
    if (r12d == 0x7fffffff) {
        goto label_10;
    }
    r12d++;
    goto label_0;
label_9:
    eax -= 0x25;
    if (al > 0x1a) {
        goto label_1;
    }
label_3:
    r15++;
    r12d++;
label_6:
    if (r15 < rbp) {
        goto label_2;
    }
label_4:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_14;
    }
    eax = r12d;
    return rax;
label_8:
    eax -= 0x61;
    if (al <= 0x1d) {
        goto label_3;
    }
    goto label_1;
label_7:
    r12d = 0;
    if (r15 >= rbp) {
        goto label_4;
    }
    rax = ctype_b_loc ();
    ecx = *((rsp + 0xc));
    r12d = 0;
    rdx = *(rax);
    ecx &= 2;
label_5:
    eax = *(r15);
    r15++;
    eax = *((rdx + rax*2));
    if ((ah & 0x40) == 0) {
        if (ecx != 0) {
            goto label_13;
        }
        if ((al & 2) != 0) {
            goto label_15;
        }
    }
    if (r12d == 0x7fffffff) {
        goto label_4;
    }
    r12d++;
label_15:
    if (rbp != r15) {
        goto label_5;
    }
    goto label_4;
label_11:
    if ((*((rsp + 0xc)) & 1) == 0) {
        goto label_3;
    }
    do {
label_13:
        r12d = 0xffffffff;
        goto label_4;
label_12:
    } while ((*((rsp + 0xc)) & 1) != 0);
    r12d++;
    r15 = rbp;
    goto label_6;
label_10:
    r12d = 0x7fffffff;
    goto label_4;
label_14:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x16990 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprboi4m6a @ 0x18250 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x108e0 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmprboi4m6a @ 0x12c70 */
 
uint64_t dbg_gnu_mbswidth (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* int gnu_mbswidth(char const * string,int flags); */
    r12d = esi;
    rax = strlen (rdi);
    edx = r12d;
    rdi = rbp;
    rsi = rax;
    return void (*0x12a50)() ();
}

/* /tmp/tmprboi4m6a @ 0x19890 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x4b80)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmprboi4m6a @ 0x166d0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18430 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmprboi4m6a @ 0x178f0 */
 
int64_t dbg_mktime_z (int64_t arg_8h, int64_t arg_10h, int64_t arg_20h, int64_t arg_30h, tm * arg1, int64_t arg2) {
    tm tm_1;
    int64_t var_114h_2;
    int64_t var_8h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_118h;
    int64_t var_30h_2;
    int64_t var_38h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h_2;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    rdi = arg1;
    rsi = arg2;
    /* time_t mktime_z(timezone_t tz,tm * tm); */
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_2;
    }
    r14 = rdi;
    rax = set_tz (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = *(rbp);
    r15 = rsp;
    *((rsp + 0x1c)) = 0xffffffff;
    *(rsp) = rax;
    rax = *((rbp + 8));
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x10));
    *((rsp + 0x10)) = rax;
    eax = *((rbp + 0x20));
    *((rsp + 0x20)) = eax;
    rax = rpl_mktime (r15);
    r13 = rax;
    eax = *((rsp + 0x1c));
    while (al == 0) {
        if (r12 != 1) {
            rdi = r12;
            revert_tz_part_0 ();
        }
label_1:
        r13 = 0xffffffffffffffff;
label_0:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r13;
        return rax;
        al = save_abbr (r14, r15, rdx, rcx, r8);
    }
    while (al != 0) {
        __asm ("movdqa xmm0, xmmword [rsp]");
        __asm ("movdqa xmm1, xmmword [rsp + 0x10]");
        __asm ("movdqa xmm2, xmmword [rsp + 0x20]");
        rax = *((rsp + 0x30));
        __asm ("movups xmmword [rbp], xmm0");
        *((rbp + 0x30)) = rax;
        __asm ("movups xmmword [rbp + 0x10], xmm1");
        __asm ("movups xmmword [rbp + 0x20], xmm2");
        goto label_0;
        rdi = r12;
        al = revert_tz_part_0 ();
    }
    goto label_1;
label_2:
    rax = *((rsp + 0x38));
    rax -= *(fs:0x28);
    if (rax == 0) {
        rdi = rsi;
        void (*0x17a40)() ();
    }
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0xf470 */
 
int64_t dbg_canonicalize_filename_mode (void) {
    scratch_buffer rname_buffer;
    int64_t var_418h;
    /* char * canonicalize_filename_mode(char const * name,canonicalize_mode_t can_mode); */
    rax = *(fs:0x28);
    *((rsp + 0x418)) = rax;
    eax = 0;
    canonicalize_filename_mode_stk (rdi, rsi, rsp);
    rdx = *((rsp + 0x418));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x11eb0 */
 
int64_t dbg_human_options (int64_t arg1, int64_t arg2, int64_t arg3) {
    char * ptr;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* strtol_error human_options(char const * spec,int * opts,uintmax_t * block_size); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
label_1:
    r12d = 0;
    if (*(rbx) == 0x27) {
        rbx++;
        r12d = 4;
    }
    r14 = obj_block_size_opts;
    eax = argmatch (rbx, obj.block_size_args, r14, 4);
    if (eax >= 0) {
        rax = (int64_t) eax;
        *(rbp) = 1;
        r12d |= *((r14 + rax*4));
        eax = 0;
        *(r13) = r12d;
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
    }
    eax = xstrtoumax (rbx, rsp, 0, rbp, "eEgGkKmMpPtTyYzZ0");
    if (eax != 0) {
        goto label_6;
    }
    ecx = *(rbx);
    edx = rcx - 0x30;
    rcx = *(rsp);
    if (dl > 9) {
        goto label_7;
    }
    goto label_2;
    do {
        edi = *((rbx + 1));
        rbx++;
        edx = rdi - 0x30;
        if (dl <= 9) {
            goto label_2;
        }
label_7:
    } while (rcx != rbx);
    if (*((rcx - 1)) == 0x42) {
        goto label_8;
    }
    r12b |= 0x80;
label_3:
    r12d |= 0x20;
label_2:
    rdx = *(rbp);
    *(r13) = r12d;
    goto label_9;
label_6:
    *(r13) = 0;
    rdx = *(rbp);
label_9:
    if (rdx != 0) {
        goto label_0;
    }
    rax = getenv ("POSIXLY_CORRECT");
    rax -= rax;
    eax &= 0x200;
    rax += 0x200;
    *(rbp) = rax;
    eax = 4;
    goto label_0;
label_4:
    rax = getenv (0x0001bced);
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("BLOCKSIZE");
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("POSIXLY_CORRECT");
    if (rax == 0) {
        goto label_10;
    }
    *(rbp) = 0x200;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_8:
    r12d |= 0x180;
    if (*((rcx - 2)) != 0x69) {
        goto label_2;
    }
    goto label_3;
label_10:
    *(rbp) = 0x400;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x18070 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmprboi4m6a @ 0x49e0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmprboi4m6a @ 0xf4c0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmprboi4m6a @ 0x17320 */
 
uint64_t dbg_gl_scratch_buffer_grow_preserve (int64_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow_preserve(scratch_buffer * buffer); */
    r14 = rdi + 0x10;
    r13 = *((rdi + 8));
    rbx = rdi;
    r12 = *(rdi);
    rbp = r13 + r13;
    if (r12 == r14) {
        goto label_1;
    }
    if (r13 > rbp) {
        goto label_2;
    }
    rax = realloc (r12, rbp);
    rcx = rax;
    if (rax == 0) {
        goto label_3;
    }
    do {
        *(rbx) = rcx;
        eax = 1;
        *((rbx + 8)) = rbp;
label_0:
        return rax;
label_1:
        rdi = rbp;
        rax = fcn_00004670 ();
        if (rax == 0) {
            goto label_4;
        }
        rax = memcpy (rax, r12, r13);
        rcx = rax;
    } while (1);
label_2:
    errno_location ();
    *(rax) = 0xc;
    do {
        rdi = r12;
        eax = fcn_00004630 ();
        *(rbx) = r14;
        eax = 0;
        *((rbx + 8)) = 0x400;
        goto label_0;
label_3:
        r12 = *(rbx);
    } while (1);
label_4:
    eax = 0;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x19740 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprboi4m6a @ 0x4730 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmprboi4m6a @ 0x172a0 */
 
int64_t dbg_gl_scratch_buffer_grow (uint32_t arg1) {
    rdi = arg1;
    /* _Bool gl_scratch_buffer_grow(scratch_buffer * buffer); */
    rbx = rdi;
    rax = *((rdi + 8));
    rdi = *(rdi);
    r12 = rbx + 0x10;
    rbp = rax + rax;
    if (rdi != r12) {
        fcn_00004630 ();
        rax = *((rbx + 8));
    }
    if (rax <= rbp) {
        rdi = rbp;
        rax = fcn_00004670 ();
        if (rax == 0) {
            goto label_0;
        }
        r8d = 1;
        *(rbx) = rax;
        *((rbx + 8)) = rbp;
        eax = r8d;
        return rax;
    }
    errno_location ();
    *(rax) = 0xc;
label_0:
    rax = r12;
    r8d = 0;
    *(rbx) = rax;
    eax = r8d;
    *((rbx + 8)) = rbp;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x17a60 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0001e9e8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0001e9fb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0001ece8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x1ece8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x4c10)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x4c10)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x4c10)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmprboi4m6a @ 0x17ef0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprboi4m6a @ 0x4000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x18590 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0xff70 */
 
void gettime (int64_t arg1) {
    rdi = arg1;
    rsi = rdi;
    edi = 0;
    return clock_gettime ();
}

/* /tmp/tmprboi4m6a @ 0x104f0 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x17490 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmprboi4m6a @ 0x18290 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0x17a40 */
 
void dbg_rpl_timegm (int64_t arg1, mktime_offset_t gmtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    xmm4 = gmtime_offset;
    /* time_t rpl_timegm(tm * tmp); */
    *((rdi + 0x20)) = 0;
    rsi = *(reloc.gmtime_r);
    rdx = obj_gmtime_offset_0;
    return void (*0x19160)() ();
}

/* /tmp/tmprboi4m6a @ 0x18650 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rdi = rsi + 1;
    rax = fcn_00004670 ();
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x4a30)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprboi4m6a @ 0xeaa0 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmprboi4m6a @ 0x4690 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 1536 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmprboi4m6a @ 0x46a0 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmprboi4m6a @ 0x46b0 */
 
void sigprocmask (void) {
    __asm ("bnd jmp qword [reloc.sigprocmask]");
}

/* /tmp/tmprboi4m6a @ 0x46c0 */
 
void snprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__snprintf_chk]");
}

/* /tmp/tmprboi4m6a @ 0x46d0 */
 
void raise (void) {
    __asm ("bnd jmp qword [reloc.raise]");
}

/* /tmp/tmprboi4m6a @ 0x0 */
 
uint64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    rax = 6 (rdi, rsi, rdx, rcx, r8, r9);
    *(rax)++;
    *(rdi) += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    bh += bh;
    *((rax + 0x12)) = fp_stack[0];
    fp_stack--;
    *(rax) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += 0;
    *(rax) += al;
    *(rdx) += ch;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rcx) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmprboi4m6a @ 0x4710 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmprboi4m6a @ 0x4720 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmprboi4m6a @ 0x4750 */
 
void sigaction (void) {
    __asm ("bnd jmp qword [reloc.sigaction]");
}

/* /tmp/tmprboi4m6a @ 0x4760 */
 
void iswcntrl (void) {
    __asm ("bnd jmp qword [reloc.iswcntrl]");
}

/* /tmp/tmprboi4m6a @ 0x4780 */
 
void wcswidth (void) {
    __asm ("bnd jmp qword [reloc.wcswidth]");
}

/* /tmp/tmprboi4m6a @ 0x4790 */
 
void localeconv (void) {
    __asm ("bnd jmp qword [reloc.localeconv]");
}

/* /tmp/tmprboi4m6a @ 0x47a0 */
 
void faccessat (void) {
    __asm ("bnd jmp qword [reloc.faccessat]");
}

/* /tmp/tmprboi4m6a @ 0x47b0 */
 
void mbstowcs (void) {
    __asm ("bnd jmp qword [reloc.mbstowcs]");
}

/* /tmp/tmprboi4m6a @ 0x47c0 */
 
void readlink (void) {
    __asm ("bnd jmp qword [reloc.readlink]");
}

/* /tmp/tmprboi4m6a @ 0x47e0 */
 
void setenv (void) {
    __asm ("bnd jmp qword [reloc.setenv]");
}

/* /tmp/tmprboi4m6a @ 0x47f0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmprboi4m6a @ 0x4800 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmprboi4m6a @ 0x4810 */
 
void opendir (void) {
    __asm ("bnd jmp qword [reloc.opendir]");
}

/* /tmp/tmprboi4m6a @ 0x4820 */
 
void getpwuid (void) {
    __asm ("bnd jmp qword [reloc.getpwuid]");
}

/* /tmp/tmprboi4m6a @ 0x4830 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmprboi4m6a @ 0x4850 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmprboi4m6a @ 0x4880 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmprboi4m6a @ 0x4890 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmprboi4m6a @ 0x48a0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmprboi4m6a @ 0x48b0 */
 
void getgrgid (void) {
    __asm ("bnd jmp qword [reloc.getgrgid]");
}

/* /tmp/tmprboi4m6a @ 0x48c0 */
 
void snprintf (void) {
    __asm ("bnd jmp qword [reloc.snprintf]");
}

/* /tmp/tmprboi4m6a @ 0x48d0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmprboi4m6a @ 0x48f0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmprboi4m6a @ 0x4900 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmprboi4m6a @ 0x4910 */
 
void fnmatch (void) {
    __asm ("bnd jmp qword [reloc.fnmatch]");
}

/* /tmp/tmprboi4m6a @ 0x4920 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmprboi4m6a @ 0x4930 */
 
void ioctl (void) {
    __asm ("bnd jmp qword [reloc.ioctl]");
}

/* /tmp/tmprboi4m6a @ 0x4940 */
 
void getcwd (void) {
    __asm ("bnd jmp qword [reloc.getcwd]");
}

/* /tmp/tmprboi4m6a @ 0x4950 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmprboi4m6a @ 0x4960 */
 
void closedir (void) {
    __asm ("bnd jmp qword [reloc.closedir]");
}

/* /tmp/tmprboi4m6a @ 0x4970 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmprboi4m6a @ 0x4980 */
 
void setjmp (void) {
    __asm ("bnd jmp qword [reloc._setjmp]");
}

/* /tmp/tmprboi4m6a @ 0x49a0 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmprboi4m6a @ 0x49c0 */
 
void signal (void) {
    __asm ("bnd jmp qword [reloc.signal]");
}

/* /tmp/tmprboi4m6a @ 0x49d0 */
 
void dirfd (void) {
    __asm ("bnd jmp qword [reloc.dirfd]");
}

/* /tmp/tmprboi4m6a @ 0x49f0 */
 
void getpwnam (void) {
    __asm ("bnd jmp qword [reloc.getpwnam]");
}

/* /tmp/tmprboi4m6a @ 0x4a00 */
 
void memcpy_chk (void) {
    __asm ("bnd jmp qword [reloc.__memcpy_chk]");
}

/* /tmp/tmprboi4m6a @ 0x4a10 */
 
void sigemptyset (void) {
    __asm ("bnd jmp qword [reloc.sigemptyset]");
}

/* /tmp/tmprboi4m6a @ 0x4a20 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmprboi4m6a @ 0x4a40 */
 
void getgrnam (void) {
    __asm ("bnd jmp qword [reloc.getgrnam]");
}

/* /tmp/tmprboi4m6a @ 0x4a50 */
 
void tzset (void) {
    __asm ("bnd jmp qword [reloc.tzset]");
}

/* /tmp/tmprboi4m6a @ 0x4a60 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmprboi4m6a @ 0x4a70 */
 
void tcgetpgrp (void) {
    __asm ("bnd jmp qword [reloc.tcgetpgrp]");
}

/* /tmp/tmprboi4m6a @ 0x4a80 */
 
void readdir (void) {
    __asm ("bnd jmp qword [reloc.readdir]");
}

/* /tmp/tmprboi4m6a @ 0x4a90 */
 
void wcwidth (void) {
    __asm ("bnd jmp qword [reloc.wcwidth]");
}

/* /tmp/tmprboi4m6a @ 0x4aa0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmprboi4m6a @ 0x4ad0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmprboi4m6a @ 0x4ae0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmprboi4m6a @ 0x4b00 */
 
void stpncpy (void) {
    __asm ("bnd jmp qword [reloc.stpncpy]");
}

/* /tmp/tmprboi4m6a @ 0x4b30 */
 
void statx (void) {
    __asm ("bnd jmp qword [reloc.statx]");
}

/* /tmp/tmprboi4m6a @ 0x4b40 */
 
void strftime (void) {
    __asm ("bnd jmp qword [reloc.strftime]");
}

/* /tmp/tmprboi4m6a @ 0x4b50 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmprboi4m6a @ 0x4b60 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmprboi4m6a @ 0x4b80 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmprboi4m6a @ 0x4b90 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmprboi4m6a @ 0x4ba0 */
 
void unsetenv (void) {
    __asm ("bnd jmp qword [reloc.unsetenv]");
}

/* /tmp/tmprboi4m6a @ 0x4bb0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmprboi4m6a @ 0x4bc0 */
 
void wcstombs (void) {
    __asm ("bnd jmp qword [reloc.wcstombs]");
}

/* /tmp/tmprboi4m6a @ 0x4bd0 */
 
void gethostname (void) {
    __asm ("bnd jmp qword [reloc.gethostname]");
}

/* /tmp/tmprboi4m6a @ 0x4be0 */
 
void sigismember (void) {
    __asm ("bnd jmp qword [reloc.sigismember]");
}

/* /tmp/tmprboi4m6a @ 0x4c20 */
 
void fflush_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fflush_unlocked]");
}

/* /tmp/tmprboi4m6a @ 0x4c30 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmprboi4m6a @ 0x4c40 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmprboi4m6a @ 0x4c50 */
 
void sigaddset (void) {
    __asm ("bnd jmp qword [reloc.sigaddset]");
}

/* /tmp/tmprboi4m6a @ 0x4c60 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmprboi4m6a @ 0x4c70 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmprboi4m6a @ 0x4c80 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmprboi4m6a @ 0x4640 */
 
void fcn_00004640 (void) {
    __asm ("bnd jmp qword [reloc.localtime_r]");
}

/* /tmp/tmprboi4m6a @ 0x4030 */
 
void fcn_00004030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1552 named .plt */
    __asm ("bnd jmp qword [0x00024c98]");
}

/* /tmp/tmprboi4m6a @ 0x4040 */
 
void fcn_00004040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4050 */
 
void fcn_00004050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4060 */
 
void fcn_00004060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4070 */
 
void fcn_00004070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4080 */
 
void fcn_00004080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4090 */
 
void fcn_00004090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40a0 */
 
void fcn_000040a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40b0 */
 
void fcn_000040b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40c0 */
 
void fcn_000040c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40d0 */
 
void fcn_000040d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40e0 */
 
void fcn_000040e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x40f0 */
 
void fcn_000040f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4100 */
 
void fcn_00004100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4110 */
 
void fcn_00004110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4120 */
 
void fcn_00004120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4130 */
 
void fcn_00004130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4140 */
 
void fcn_00004140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4150 */
 
void fcn_00004150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4160 */
 
void fcn_00004160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4170 */
 
void fcn_00004170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4180 */
 
void fcn_00004180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4190 */
 
void fcn_00004190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41a0 */
 
void fcn_000041a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41b0 */
 
void fcn_000041b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41c0 */
 
void fcn_000041c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41d0 */
 
void fcn_000041d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41e0 */
 
void fcn_000041e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x41f0 */
 
void fcn_000041f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4200 */
 
void fcn_00004200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4210 */
 
void fcn_00004210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4220 */
 
void fcn_00004220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4230 */
 
void fcn_00004230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4240 */
 
void fcn_00004240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4250 */
 
void fcn_00004250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4260 */
 
void fcn_00004260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4270 */
 
void fcn_00004270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4280 */
 
void fcn_00004280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4290 */
 
void fcn_00004290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42a0 */
 
void fcn_000042a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42b0 */
 
void fcn_000042b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42c0 */
 
void fcn_000042c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42d0 */
 
void fcn_000042d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42e0 */
 
void fcn_000042e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x42f0 */
 
void fcn_000042f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4300 */
 
void fcn_00004300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4310 */
 
void fcn_00004310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4320 */
 
void fcn_00004320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4330 */
 
void fcn_00004330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4340 */
 
void fcn_00004340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4350 */
 
void fcn_00004350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4360 */
 
void fcn_00004360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4370 */
 
void fcn_00004370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4380 */
 
void fcn_00004380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4390 */
 
void fcn_00004390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43a0 */
 
void fcn_000043a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43b0 */
 
void fcn_000043b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43c0 */
 
void fcn_000043c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43d0 */
 
void fcn_000043d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43e0 */
 
void fcn_000043e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x43f0 */
 
void fcn_000043f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4400 */
 
void fcn_00004400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4410 */
 
void fcn_00004410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4420 */
 
void fcn_00004420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4430 */
 
void fcn_00004430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4440 */
 
void fcn_00004440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4450 */
 
void fcn_00004450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4460 */
 
void fcn_00004460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4470 */
 
void fcn_00004470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4480 */
 
void fcn_00004480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4490 */
 
void fcn_00004490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44a0 */
 
void fcn_000044a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44b0 */
 
void fcn_000044b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44c0 */
 
void fcn_000044c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44d0 */
 
void fcn_000044d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44e0 */
 
void fcn_000044e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x44f0 */
 
void fcn_000044f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4500 */
 
void fcn_00004500 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4510 */
 
void fcn_00004510 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4520 */
 
void fcn_00004520 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4530 */
 
void fcn_00004530 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4540 */
 
void fcn_00004540 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4550 */
 
void fcn_00004550 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4560 */
 
void fcn_00004560 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4570 */
 
void fcn_00004570 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4580 */
 
void fcn_00004580 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4590 */
 
void fcn_00004590 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45a0 */
 
void fcn_000045a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45b0 */
 
void fcn_000045b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45c0 */
 
void fcn_000045c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45d0 */
 
void fcn_000045d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45e0 */
 
void fcn_000045e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x45f0 */
 
void fcn_000045f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4600 */
 
void fcn_00004600 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4610 */
 
void fcn_00004610 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprboi4m6a @ 0x4620 */
 
void fcn_00004620 (void) {
    return __asm ("bnd jmp section..plt");
}
