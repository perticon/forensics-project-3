
void** cwd_n_used = reinterpret_cast<void**>(0);

void** sorted_file = reinterpret_cast<void**>(0);

void fun_4630(void** rdi, ...);

unsigned char cwd_some_quoted = 0;

signed char any_has_acl = 0;

int32_t inode_number_width = 0;

uint32_t block_size_width = 0;

int32_t nlink_width = 0;

uint32_t owner_width = 0;

uint32_t group_width = 0;

uint32_t author_width = 0;

int32_t scontext_width = 0;

uint32_t major_device_number_width = 0;

int32_t minor_device_number_width = 0;

uint32_t file_size_width = 0;

void clear_files(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rbx4;
    void** r12_5;
    void** rbp6;
    void** rdi7;
    void** rdi8;
    void** rdi9;

    rax3 = cwd_n_used;
    if (rax3) {
        rbx4 = sorted_file;
        r12_5 = rbx4 + reinterpret_cast<unsigned char>(rax3) * 8;
        do {
            rbp6 = *reinterpret_cast<void***>(rbx4);
            rbx4 = rbx4 + 8;
            rdi7 = *reinterpret_cast<void***>(rbp6);
            fun_4630(rdi7);
            rdi8 = *reinterpret_cast<void***>(rbp6 + 8);
            fun_4630(rdi8);
            rdi9 = *reinterpret_cast<void***>(rbp6 + 16);
            fun_4630(rdi9);
        } while (r12_5 != rbx4);
    }
    cwd_some_quoted = 0;
    cwd_n_used = reinterpret_cast<void**>(0);
    any_has_acl = 0;
    inode_number_width = 0;
    block_size_width = 0;
    nlink_width = 0;
    owner_width = 0;
    group_width = 0;
    author_width = 0;
    scontext_width = 0;
    major_device_number_width = 0;
    minor_device_number_width = 0;
    file_size_width = 0;
    return;
}

void** cwd_file = reinterpret_cast<void**>(0);

void* g28;

void** cwd_n_alloc = reinterpret_cast<void**>(0);

void** xreallocarray(void** rdi, void** rsi, int64_t rdx, void** rcx);

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[23] pad48;
    uint32_t f30;
    int32_t f34;
    int32_t f38;
    signed char[12] pad72;
    void** f48;
    signed char[15] pad88;
    void** f58;
    signed char[79] pad168;
    uint32_t fa8;
    int32_t fac;
    void** fb0;
    signed char[7] pad184;
    signed char fb8;
    signed char fb9;
    signed char[2] pad188;
    int32_t fbc;
    signed char fc0;
    signed char[3] pad196;
    int32_t fc4;
    int64_t fc8;
};

unsigned char align_variable_outer_quotes = 0;

void** filename_quoting_options = reinterpret_cast<void**>(0);

void** quotearg_buffer(void** rdi, void** rsi, void** rdx, int64_t rcx, void** r8);

void** fun_4860(void** rdi, ...);

unsigned char print_hyperlink = 0;

signed char format_needs_stat = 0;

unsigned char print_with_color = 0;

void** canonicalize_filename_mode(void** rdi, int64_t rsi);

void** fun_4840();

void file_failure(uint32_t edi, void** rsi, void** rdx, ...);

uint32_t dereference = 0;

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    uint32_t f18;
    int64_t f1c;
    signed char[4] pad40;
    uint64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    int64_t f50;
    int64_t f58;
    void** f60;
    signed char[7] pad104;
    int64_t f68;
    void** f70;
};

int32_t calc_req_mask(void** rdi, ...);

int32_t do_statx(void** rdi, void** rsi, struct s1* rdx);

void* fun_4870();

unsigned char is_colored(uint32_t edi, ...);

void*** fun_46f0();

/* unsupported_device.10 */
void** unsupported_device_10 = reinterpret_cast<void**>(0);

unsigned char print_inode = 0;

unsigned char format_needs_type = 0;

signed char color_symlink_as_referent = 0;

signed char check_symlink_mode = 0;

uint32_t indicator_style = 0;

void** xstrdup(void** rdi, void** rsi, ...);

void** format = reinterpret_cast<void**>(0);

signed char print_scontext = 0;

/* unsupported_device.9 */
void** unsupported_device_9 = reinterpret_cast<void**>(0);

/* unsupported_device.8 */
void** unsupported_device_8 = reinterpret_cast<void**>(0);

int32_t file_has_acl(void** rdi, struct s1* rsi);

void** quotearg_n_style_colon();

void fun_4b70();

void** areadlink_with_size(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void* dir_len(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void** xmalloc(void** rdi, void** rsi, ...);

void*** fun_4b00(void** rdi, void** rsi, void* rdx, void** rcx, int64_t r8);

void fun_4720(void*** rdi, void** rsi, void* rdx, void** rcx, int64_t r8);

unsigned char immediate_dirs = 0;

signed char print_block_size = 0;

void** output_block_size = reinterpret_cast<void**>(0);

int32_t human_output_opts = 0;

void** human_readable(void** rdi, void* rsi, int64_t rdx, void** rcx, void** r8, ...);

uint32_t gnu_mbswidth(void** rdi, ...);

signed char print_owner = 1;

uint32_t format_user_width(int32_t edi, ...);

signed char print_group = 1;

signed char numeric_ids = 0;

void** getgroup(int64_t rdi, ...);

uint32_t fun_46c0();

signed char print_author = 0;

void** umaxtostr();

void** file_output_block_size = reinterpret_cast<void**>(1);

int32_t file_human_output_opts = 0;

/* gobble_file.constprop.0 */
void** gobble_file_constprop_0(void** rdi, uint32_t esi, void** edx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void* rbp8;
    void** r15_9;
    int64_t r13_10;
    void** r12_11;
    void* rsp12;
    void** v13;
    void** rsi14;
    void** rdi15;
    void* rax16;
    void* v17;
    int1_t zf18;
    void** rax19;
    struct s0* rbx20;
    void** rdi21;
    void* rcx22;
    int64_t rcx23;
    int1_t zf24;
    int1_t zf25;
    void** r8_26;
    void** rax27;
    uint32_t eax28;
    unsigned char v29;
    void** rax30;
    void** rsi31;
    uint32_t edx32;
    void** r14_33;
    uint32_t r8d34;
    int1_t zf35;
    int1_t zf36;
    int64_t* rsp37;
    void** rax38;
    int64_t* rsp39;
    void** rax40;
    int64_t* rsp41;
    uint32_t r9d42;
    struct s1* r15_43;
    int64_t* rsp44;
    int32_t eax45;
    int64_t* rsp46;
    int32_t eax47;
    void** rcx48;
    int64_t r8_49;
    int64_t* rsp50;
    int32_t eax51;
    int64_t* rsp52;
    void** rax53;
    void** r15d54;
    void** r14_55;
    uint32_t edi56;
    int64_t* rsp57;
    void* rax58;
    int1_t zf59;
    int64_t* rsp60;
    unsigned char al61;
    int64_t* rsp62;
    void*** rax63;
    void** r13_64;
    int1_t zf65;
    int64_t* rsp66;
    int32_t eax67;
    int64_t* rsp68;
    int32_t eax69;
    void** rax70;
    void** rax71;
    void* rsp72;
    uint32_t edx73;
    void* rax74;
    uint32_t r8d75;
    uint64_t rcx76;
    void* rdi77;
    uint32_t eax78;
    int1_t zf79;
    int1_t zf80;
    int1_t zf81;
    int1_t zf82;
    int1_t zf83;
    int1_t zf84;
    int1_t zf85;
    unsigned char al86;
    unsigned char al87;
    unsigned char al88;
    unsigned char al89;
    int64_t* rsp90;
    void** rax91;
    void** tmp64_92;
    unsigned char al93;
    unsigned char al94;
    unsigned char al95;
    void** rdx96;
    int1_t zf97;
    void** rcx98;
    int64_t* rsp99;
    void*** rax100;
    int1_t zf101;
    uint32_t eax102;
    int1_t zf103;
    uint32_t eax104;
    int64_t* rsp105;
    int32_t eax106;
    int64_t* rsp107;
    void** rax108;
    int64_t* rsp109;
    void** edi110;
    void** rsi111;
    int64_t* rsp112;
    void** rax113;
    void* rsp114;
    void** r13_115;
    int64_t* rsp116;
    void** rax117;
    uint32_t edi118;
    int64_t* rsp119;
    void** r9_120;
    int64_t* rsp121;
    void** rax122;
    int64_t* rsp123;
    void* rax124;
    void* rsp125;
    void* r15_126;
    int64_t* rsp127;
    void** rax128;
    int64_t* rsp129;
    void** rax130;
    void** rdi131;
    int64_t* rsp132;
    void** rax133;
    int64_t* rsp134;
    void*** rax135;
    int64_t* rsp136;
    void** r14_137;
    void** r8_138;
    void** v139;
    int64_t* rsp140;
    void** rax141;
    uint32_t eax142;
    unsigned char v143;
    int64_t* rsp144;
    void** rax145;
    int1_t below_or_equal146;
    int1_t zf147;
    int64_t* rsp148;
    int32_t eax149;
    int32_t v150;
    int64_t* rsp151;
    uint32_t edx152;
    uint1_t cf153;
    int1_t zf154;
    void** r8_155;
    int64_t rdx156;
    int64_t* rsp157;
    void** rax158;
    int64_t* rsp159;
    uint32_t eax160;
    int1_t less_or_equal161;
    void** ecx162;
    int1_t zf163;
    int1_t zf164;
    int32_t edi165;
    int64_t* rsp166;
    uint32_t eax167;
    int1_t less_or_equal168;
    int1_t zf169;
    int1_t zf170;
    int64_t rdi171;
    int64_t* rsp172;
    void** rax173;
    int64_t* rsp174;
    uint32_t eax175;
    int64_t* rsp176;
    int1_t less177;
    int1_t zf178;
    int32_t edi179;
    int64_t* rsp180;
    uint32_t eax181;
    int1_t less_or_equal182;
    int1_t zf183;
    void** edx184;
    int64_t* rsp185;
    void** rax186;
    int64_t* rsp187;
    void** rax188;
    void* rsp189;
    int1_t less_or_equal190;
    void** rdi191;
    void** r8_192;
    void* rsi193;
    int64_t rdx194;
    int64_t* rsp195;
    void** rax196;
    int64_t* rsp197;
    uint32_t eax198;
    int1_t less_or_equal199;
    uint32_t eax200;
    int64_t* rsp201;
    void** rax202;
    int64_t* rsp203;
    void** rax204;
    int1_t less_or_equal205;
    int64_t* rsp206;
    void** rax207;
    int64_t* rsp208;
    void** rax209;
    int1_t less_or_equal210;
    int64_t* rsp211;
    void** rax212;
    int64_t* rsp213;
    void** rax214;
    int64_t rdx215;
    int64_t rax216;
    int1_t less_or_equal217;
    void** rdi218;
    int64_t* rsp219;
    void** rax220;
    int1_t less_or_equal221;
    int1_t zf222;
    int64_t* rsp223;
    void** rax224;
    int64_t* rsp225;
    int64_t* rsp226;
    void*** rax227;
    void* rcx228;
    void** r9_229;
    void** rax230;
    void** rcx231;
    void** rcx232;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    r15_9 = rcx;
    *reinterpret_cast<uint32_t*>(&r13_10) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_10) + 4) = 0;
    r12_11 = rdi;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 8 - 0x378);
    v13 = edx;
    rsi14 = cwd_n_used;
    rdi15 = cwd_file;
    rax16 = g28;
    v17 = rax16;
    zf18 = rsi14 == cwd_n_alloc;
    if (zf18) {
        rax19 = xreallocarray(rdi15, rsi14, 0x1a0, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
        rsi14 = cwd_n_used;
        cwd_n_alloc = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(cwd_n_alloc) << 1);
        cwd_file = rax19;
        rdi15 = rax19;
    }
    rbx20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi15) + (reinterpret_cast<uint64_t>(rsi14 + reinterpret_cast<uint64_t>(rsi14 + reinterpret_cast<unsigned char>(rsi14) * 2) * 4) << 4));
    rbx20->f0 = reinterpret_cast<void**>(0);
    rbx20->fc8 = 0;
    rdi21 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(&rbx20->f8) & 0xfffffffffffffff8);
    rcx22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx20) - reinterpret_cast<unsigned char>(rdi21));
    *reinterpret_cast<uint32_t*>(&rcx23) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx22) + 0xd0) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx23) + 4) = 0;
    while (rcx23) {
        --rcx23;
        *reinterpret_cast<void***>(rdi21) = reinterpret_cast<void**>(0);
        rdi21 = rdi21 + 8;
    }
    rbx20->fa8 = *reinterpret_cast<uint32_t*>(&r13_10);
    rbx20->fc4 = -1;
    zf24 = cwd_some_quoted == 0;
    if (zf24 && (zf25 = align_variable_outer_quotes == 0, !zf25)) {
        r8_26 = filename_quoting_options;
        rdi21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xfffffffffffffd30);
        rax27 = quotearg_buffer(rdi21, 2, r12_11, -1, r8_26);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
        eax28 = v29;
        if (*reinterpret_cast<void***>(r12_11) != *reinterpret_cast<void***>(&eax28) || (rdi21 = r12_11, rax30 = fun_4860(rdi21, rdi21), rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8), rax27 != rax30)) {
            rbx20->fc4 = 1;
            cwd_some_quoted = 1;
        } else {
            rbx20->fc4 = 0;
        }
    }
    rsi31 = reinterpret_cast<void**>(static_cast<uint32_t>(print_hyperlink));
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    if (!*reinterpret_cast<unsigned char*>(&v13)) {
        if (*reinterpret_cast<unsigned char*>(&rsi31)) {
            edx32 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_11));
            if (*reinterpret_cast<unsigned char*>(&edx32) == 47) {
                r14_33 = r12_11;
                goto addr_9c4e_14;
            } else {
                r8d34 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_9));
                r14_33 = r12_11;
                if (*reinterpret_cast<unsigned char*>(&r8d34)) 
                    goto addr_9931_16; else 
                    goto addr_9c4e_14;
            }
        } else {
            zf35 = format_needs_stat == 0;
            if (!zf35) 
                goto addr_9910_18;
            if (*reinterpret_cast<uint32_t*>(&r13_10) != 3) 
                goto addr_9850_20;
            zf36 = print_with_color == 0;
            if (!zf36) 
                goto addr_a18d_22;
        }
    } else {
        edx32 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_11));
        r14_33 = r12_11;
        if (*reinterpret_cast<unsigned char*>(&edx32) == 47 || (r8d34 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_9)), !*reinterpret_cast<unsigned char*>(&r8d34))) {
            addr_96a0_24:
            if (*reinterpret_cast<unsigned char*>(&rsi31)) {
                addr_9c4e_14:
                rdi21 = r14_33;
                rsp37 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                *rsp37 = 0x9c5b;
                rax38 = canonicalize_filename_mode(rdi21, 2);
                rsp12 = reinterpret_cast<void*>(rsp37 + 1);
                rbx20->f10 = rax38;
                if (!rax38) {
                    rsp39 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                    *rsp39 = 0x9c7b;
                    rax40 = fun_4840();
                    *reinterpret_cast<uint32_t*>(&rdi21) = *reinterpret_cast<unsigned char*>(&v13);
                    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
                    rsp41 = rsp39 + 1 - 1;
                    *rsp41 = 0x9c8f;
                    file_failure(*reinterpret_cast<uint32_t*>(&rdi21), rax40, r14_33, *reinterpret_cast<uint32_t*>(&rdi21), rax40, r14_33);
                    rsp12 = reinterpret_cast<void*>(rsp41 + 1);
                    goto addr_96a9_26;
                }
            } else {
                addr_96a9_26:
                r9d42 = dereference;
                r15_43 = reinterpret_cast<struct s1*>(&rbx20->f18);
                rsp44 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                *rsp44 = 0x96c0;
                eax45 = calc_req_mask(rdi21, rdi21);
                rsp12 = reinterpret_cast<void*>(rsp44 + 1);
                if (r9d42 > 3) {
                    if (r9d42 != 4) {
                        addr_96db_28:
                        rsp46 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                        *rsp46 = 0x96e0;
                        eax47 = calc_req_mask(rdi21, rdi21);
                        *reinterpret_cast<uint32_t*>(&rcx48) = 0x100;
                        *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r8_49) = eax47;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_49) + 4) = 0;
                        rsp50 = rsp46 + 1 - 1;
                        *rsp50 = 0x96f8;
                        eax51 = do_statx(0xffffff9c, r14_33, r15_43);
                        rsp12 = reinterpret_cast<void*>(rsp50 + 1);
                        if (eax51) {
                            addr_9b37_29:
                            rsp52 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                            *rsp52 = 0x9b4a;
                            rax53 = fun_4840();
                            r15d54 = v13;
                            *reinterpret_cast<int32_t*>(&r14_55) = 0;
                            *reinterpret_cast<int32_t*>(&r14_55 + 4) = 0;
                            rsi31 = rax53;
                            edi56 = *reinterpret_cast<unsigned char*>(&r15d54);
                            rsp57 = rsp52 + 1 - 1;
                            *rsp57 = 0x9b63;
                            file_failure(edi56, rsi31, r14_33, edi56, rsi31, r14_33);
                            rsp12 = reinterpret_cast<void*>(rsp57 + 1);
                            rbx20->fb0 = reinterpret_cast<void**>("?");
                            if (*reinterpret_cast<unsigned char*>(&r15d54)) {
                                addr_980a_30:
                                rax58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v17) - reinterpret_cast<uint64_t>(g28));
                                if (rax58) {
                                    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8) = 0xa32b;
                                    fun_4870();
                                } else {
                                    return r14_55;
                                }
                            } else {
                                goto addr_97f7_34;
                            }
                        } else {
                            addr_9700_35:
                            rbx20->fb8 = 1;
                            if ((*reinterpret_cast<uint32_t*>(&r13_10) == 5 || (rbx20->f30 & 0xf000) == 0x8000) && ((zf59 = print_with_color == 0, !zf59) && (rsp60 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp60 = 0x9b97, al61 = is_colored(21, 21), rsp12 = reinterpret_cast<void*>(rsp60 + 1), !!al61))) {
                                rsp62 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                                *rsp62 = 0x9ba4;
                                rax63 = fun_46f0();
                                rsp12 = reinterpret_cast<void*>(rsp62 + 1);
                                r13_64 = rbx20->f18;
                                zf65 = r13_64 == unsupported_device_10;
                                *rax63 = reinterpret_cast<void**>(95);
                                if (!zf65) {
                                    unsupported_device_10 = r13_64;
                                }
                                rbx20->fc0 = 0;
                                goto addr_9724_39;
                            }
                        }
                    } else {
                        addr_9b1a_40:
                        *reinterpret_cast<uint32_t*>(&rcx48) = 0;
                        *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r8_49) = eax45;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_49) + 4) = 0;
                        rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                        *rsp66 = 0x9b2f;
                        eax67 = do_statx(0xffffff9c, r14_33, r15_43);
                        rsp12 = reinterpret_cast<void*>(rsp66 + 1);
                        if (!eax67) 
                            goto addr_9700_35; else 
                            goto addr_9b37_29;
                    }
                } else {
                    if (r9d42 <= 1) 
                        goto addr_96db_28;
                    if (!*reinterpret_cast<unsigned char*>(&v13)) 
                        goto addr_96db_28;
                    *reinterpret_cast<int32_t*>(&r8_49) = eax45;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_49) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx48) = 0;
                    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rdi21) = 0xffffff9c;
                    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
                    rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                    *rsp68 = 0x9bf9;
                    eax69 = do_statx(0xffffff9c, r14_33, r15_43);
                    rsp12 = reinterpret_cast<void*>(rsp68 + 1);
                    if (r9d42 == 2) 
                        goto addr_9c23_44; else 
                        goto addr_9c06_45;
                }
            }
        } else {
            addr_9931_16:
            rax70 = fun_4860(r12_11, r12_11);
            rax71 = fun_4860(r15_9, r15_9);
            rsp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8 - 8 + 8);
            rsi31 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rsi31)));
            edx73 = *reinterpret_cast<unsigned char*>(&edx32);
            rax74 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax70) + reinterpret_cast<unsigned char>(rax71) + 25);
            r8d75 = *reinterpret_cast<unsigned char*>(&r8d34);
            rcx76 = reinterpret_cast<uint64_t>(rax74) & 0xfffffffffffffff0;
            rdi77 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - (reinterpret_cast<uint64_t>(rax74) & 0xfffffffffffff000));
            goto addr_9986_46;
        }
    }
    addr_9850_20:
    eax78 = print_inode;
    if (*reinterpret_cast<signed char*>(&eax78)) {
        if (!static_cast<int1_t>(65 >> r13_10)) 
            goto addr_9910_18;
    } else {
        zf79 = format_needs_type == 0;
        if (zf79) 
            goto addr_a110_50;
        if (!static_cast<int1_t>(65 >> r13_10)) 
            goto addr_989e_52;
    }
    zf80 = dereference == 4;
    if (zf80) {
        edx32 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_11));
        if (*reinterpret_cast<unsigned char*>(&edx32) == 47) {
            eax45 = calc_req_mask(rdi21);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
            r15_43 = reinterpret_cast<struct s1*>(&rbx20->f18);
            r14_33 = r12_11;
            goto addr_9b1a_40;
        }
    } else {
        zf81 = color_symlink_as_referent == 0;
        if (!zf81) 
            goto addr_9910_18;
        zf82 = check_symlink_mode == 0;
        if (!zf82) 
            goto addr_9910_18;
        if (*reinterpret_cast<signed char*>(&eax78)) 
            goto addr_9910_18; else 
            goto addr_989e_52;
    }
    addr_9921_59:
    r8d34 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_9));
    r14_33 = r12_11;
    if (!*reinterpret_cast<unsigned char*>(&r8d34)) 
        goto addr_96a9_26; else 
        goto addr_9931_16;
    addr_989e_52:
    zf83 = format_needs_type == 0;
    if (zf83) {
        addr_a110_50:
        *reinterpret_cast<int32_t*>(&r14_55) = 0;
        *reinterpret_cast<int32_t*>(&r14_55 + 4) = 0;
        goto addr_97f7_34;
    } else {
        if (!*reinterpret_cast<uint32_t*>(&r13_10)) {
            addr_9910_18:
            edx32 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_11));
            r14_33 = r12_11;
            if (*reinterpret_cast<unsigned char*>(&edx32) == 47) 
                goto addr_96a9_26; else 
                goto addr_9921_59;
        } else {
            *reinterpret_cast<int32_t*>(&r14_55) = 0;
            *reinterpret_cast<int32_t*>(&r14_55 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&r13_10) != 5) 
                goto addr_97f7_34;
            zf84 = indicator_style == 3;
            if (zf84) 
                goto addr_9910_18;
        }
    }
    zf85 = print_with_color == 0;
    if (zf85) 
        goto addr_97f7_34;
    *reinterpret_cast<uint32_t*>(&rdi21) = 14;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al86 = is_colored(14, 14);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al86) 
        goto addr_9910_18;
    *reinterpret_cast<uint32_t*>(&rdi21) = 16;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al87 = is_colored(16, 16);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al87) 
        goto addr_9910_18;
    *reinterpret_cast<uint32_t*>(&rdi21) = 17;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al88 = is_colored(17, 17);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al88) 
        goto addr_9910_18;
    *reinterpret_cast<uint32_t*>(&rdi21) = 21;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al89 = is_colored(21, 21);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (!al89) {
        addr_97f7_34:
        rsp90 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp90 = 0x97ff;
        rax91 = xstrdup(r12_11, rsi31, r12_11, rsi31);
        rsp12 = reinterpret_cast<void*>(rsp90 + 1);
        tmp64_92 = cwd_n_used + 1;
        cwd_n_used = tmp64_92;
        rbx20->f0 = rax91;
        goto addr_980a_30;
    } else {
        goto addr_9910_18;
    }
    addr_a18d_22:
    *reinterpret_cast<uint32_t*>(&rdi21) = 19;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al93 = is_colored(19, 19);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al93) 
        goto addr_9910_18;
    *reinterpret_cast<uint32_t*>(&rdi21) = 18;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al94 = is_colored(18, 18);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al94) 
        goto addr_9910_18;
    *reinterpret_cast<uint32_t*>(&rdi21) = 20;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    al95 = is_colored(20, 20);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    if (al95) 
        goto addr_9910_18;
    goto addr_9850_20;
    addr_9724_39:
    rdx96 = format;
    *reinterpret_cast<int32_t*>(&rdx96 + 4) = 0;
    if (!rdx96 || (zf97 = print_scontext == 0, !zf97)) {
        rcx98 = rbx20->f18;
        rsp99 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp99 = 0x974d;
        rax100 = fun_46f0();
        rsp12 = reinterpret_cast<void*>(rsp99 + 1);
        rcx48 = rcx98;
        zf101 = rcx48 == unsupported_device_9;
        *rax100 = reinterpret_cast<void**>(95);
        rdx96 = rdx96;
        *reinterpret_cast<int32_t*>(&rdx96 + 4) = 0;
        if (!zf101) {
            unsupported_device_9 = rcx48;
        }
        rbx20->fb0 = reinterpret_cast<void**>("?");
        if (rdx96) 
            goto addr_9789_75;
    } else {
        addr_9793_76:
        eax102 = rbx20->f30 & 0xf000;
        if (eax102 == 0xa000) 
            goto addr_9ea8_77; else 
            goto addr_97a6_78;
    }
    zf103 = rcx48 == unsupported_device_8;
    if (zf103) {
        *rax100 = reinterpret_cast<void**>(95);
        eax104 = rbx20->f30;
        rbx20->fbc = 0;
        eax102 = eax104 & 0xf000;
        if (eax102 != 0xa000) 
            goto addr_97a6_78;
    } else {
        *rax100 = reinterpret_cast<void**>(0);
        rsp105 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp105 = 0x9e68;
        eax106 = file_has_acl(r14_33, r15_43);
        rsp12 = reinterpret_cast<void*>(rsp105 + 1);
        if (reinterpret_cast<uint1_t>(eax106 < 0) | reinterpret_cast<uint1_t>(eax106 == 0)) {
            rdx96 = *rax100;
            *reinterpret_cast<int32_t*>(&rdx96 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx48) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdx96 + 0xffffffffffffffea)) & 0xffffffef;
            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
            if (!*reinterpret_cast<uint32_t*>(&rcx48) || reinterpret_cast<int1_t>(rdx96 == 95)) {
                rdx96 = rbx20->f18;
                unsupported_device_8 = rdx96;
            }
            rbx20->fbc = 0;
            if (eax106) {
                rsp107 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
                *rsp107 = 0xa15d;
                rax108 = quotearg_n_style_colon();
                rdx96 = reinterpret_cast<void**>("%s");
                rcx48 = rax108;
                rsp109 = rsp107 + 1 - 1;
                *rsp109 = 0xa174;
                fun_4b70();
                rsp12 = reinterpret_cast<void*>(rsp109 + 1);
            }
        } else {
            rbx20->fbc = 2;
            any_has_acl = 1;
        }
        eax102 = rbx20->f30 & 0xf000;
        if (eax102 != 0xa000) 
            goto addr_97a6_78;
        edi110 = format;
        if (edi110) 
            goto addr_9ea5_90;
    }
    addr_9eb5_91:
    rsi111 = rbx20->f48;
    rsp112 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp112 = 0x9ec1;
    rax113 = areadlink_with_size(r14_33, rsi111, rdx96, rcx48, r8_49);
    rsp114 = reinterpret_cast<void*>(rsp112 + 1);
    rbx20->f8 = rax113;
    r13_115 = rax113;
    if (rax113 || (rsp116 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8), *rsp116 = 0xa2b3, rax117 = fun_4840(), edi118 = *reinterpret_cast<unsigned char*>(&v13), rdx96 = r14_33, rsi111 = rax117, rsp119 = rsp116 + 1 - 1, *rsp119 = 0xa2c5, file_failure(edi118, rsi111, rdx96, edi118, rsi111, rdx96), rsp114 = reinterpret_cast<void*>(rsp119 + 1), r13_115 = rbx20->f8, *reinterpret_cast<int32_t*>(&r9_120) = 0, *reinterpret_cast<int32_t*>(&r9_120 + 4) = 0, !!r13_115)) {
        if (*reinterpret_cast<void***>(r13_115) == 47) {
            rsp121 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8);
            *rsp121 = 0xa2e2;
            rax122 = xstrdup(r13_115, rsi111, r13_115, rsi111);
            rsp114 = reinterpret_cast<void*>(rsp121 + 1);
            r9_120 = rax122;
        } else {
            rsp123 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8);
            *rsp123 = 0x9ee4;
            rax124 = dir_len(r14_33, rsi111, rdx96, rcx48, r8_49);
            rsp125 = reinterpret_cast<void*>(rsp123 + 1);
            r15_126 = rax124;
            if (!rax124) {
                rsp127 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp125) - 8);
                *rsp127 = 0xa1d5;
                rax128 = xstrdup(r13_115, rsi111, r13_115, rsi111);
                rsp114 = reinterpret_cast<void*>(rsp127 + 1);
                r9_120 = rax128;
            } else {
                rsp129 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp125) - 8);
                *rsp129 = 0x9ef8;
                rax130 = fun_4860(r13_115, r13_115);
                rdi131 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_126) + reinterpret_cast<unsigned char>(rax130) + 2);
                rsp132 = rsp129 + 1 - 1;
                *rsp132 = 0x9f02;
                rax133 = xmalloc(rdi131, rsi111, rdi131, rsi111);
                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_33) + reinterpret_cast<uint64_t>(r15_126) + 0xffffffffffffffff) != 47) {
                    r15_126 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_126) + 1);
                }
                rsp134 = rsp132 + 1 - 1;
                *rsp134 = 0x9f26;
                rax135 = fun_4b00(rax133, r14_33, r15_126, rcx48, r8_49);
                rsp136 = rsp134 + 1 - 1;
                *rsp136 = 0x9f31;
                fun_4720(rax135, r13_115, r15_126, rcx48, r8_49);
                rsp114 = reinterpret_cast<void*>(rsp136 + 1);
                r9_120 = rax133;
            }
        }
        if (!rbx20->fc4) {
            r14_137 = rbx20->f8;
            r8_138 = filename_quoting_options;
            v139 = r9_120;
            rsp140 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8);
            *rsp140 = 0xa23d;
            rax141 = quotearg_buffer(reinterpret_cast<int64_t>(rbp8) + 0xfffffffffffffd30, 2, r14_137, -1, r8_138);
            rsp114 = reinterpret_cast<void*>(rsp140 + 1);
            r9_120 = v139;
            eax142 = v143;
            if (*reinterpret_cast<void***>(r14_137) != *reinterpret_cast<void***>(&eax142) || (rsp144 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8), *rsp144 = 0xa311, rax145 = fun_4860(r14_137, r14_137), rsp114 = reinterpret_cast<void*>(rsp144 + 1), r9_120 = v139, rax141 != rax145)) {
                rbx20->fc4 = -1;
            }
        }
        below_or_equal146 = indicator_style <= 1;
        if ((!below_or_equal146 || (zf147 = check_symlink_mode == 0, !zf147)) && (rsp148 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8), *rsp148 = 0x9fa3, eax149 = do_statx(0xffffff9c, r9_120, reinterpret_cast<int64_t>(rbp8) + 0xfffffffffffffc80), rsp114 = reinterpret_cast<void*>(rsp148 + 1), r9_120 = r9_120, !eax149)) {
            rbx20->fb9 = 1;
            rbx20->fac = v150;
        }
    }
    rsp151 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp114) - 8);
    *rsp151 = 0x9f60;
    fun_4630(r9_120, r9_120);
    rsp12 = reinterpret_cast<void*>(rsp151 + 1);
    eax102 = rbx20->f30 & 0xf000;
    if (eax102 != 0xa000) {
        addr_97a6_78:
        edx152 = 5;
        if (eax102 == 0x4000 && (edx152 = 3, !!*reinterpret_cast<unsigned char*>(&v13))) {
            cf153 = reinterpret_cast<uint1_t>(immediate_dirs < 1);
            edx152 = (3 - (3 + reinterpret_cast<uint1_t>(3 < 3 + cf153)) & 6) + 3;
        }
    } else {
        addr_9f73_108:
        edx152 = 6;
    }
    rsi31 = format;
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    r14_55 = rbx20->f58;
    rbx20->fa8 = edx152;
    if (!rsi31 || (zf154 = print_block_size == 0, !zf154)) {
        r8_155 = output_block_size;
        *reinterpret_cast<int32_t*>(&rdx156) = human_output_opts;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx156) + 4) = 0;
        rsp157 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp157 = 0x99c9;
        rax158 = human_readable(r14_55, reinterpret_cast<int64_t>(rbp8) - 0x2d0, rdx156, 0x200, r8_155);
        rsi31 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
        rsp159 = rsp157 + 1 - 1;
        *rsp159 = 0x99d3;
        eax160 = gnu_mbswidth(rax158, rax158);
        rsp12 = reinterpret_cast<void*>(rsp159 + 1);
        less_or_equal161 = reinterpret_cast<int32_t>(eax160) <= reinterpret_cast<int32_t>(block_size_width);
        if (!less_or_equal161) {
            block_size_width = eax160;
        }
        ecx162 = format;
        if (ecx162) 
            goto addr_97db_113;
    } else {
        addr_97db_113:
        zf163 = print_scontext == 0;
        if (!zf163) 
            goto addr_9a1f_114; else 
            goto addr_97e8_115;
    }
    zf164 = print_owner == 0;
    if (!zf164 && (edi165 = rbx20->f34, rsp166 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp166 = 0xa068, eax167 = format_user_width(edi165), rsp12 = reinterpret_cast<void*>(rsp166 + 1), less_or_equal168 = reinterpret_cast<int32_t>(eax167) <= reinterpret_cast<int32_t>(owner_width), !less_or_equal168)) {
        owner_width = eax167;
    }
    zf169 = print_group == 0;
    if (!zf169) {
        zf170 = numeric_ids == 0;
        if (!zf170 || (*reinterpret_cast<int32_t*>(&rdi171) = rbx20->f38, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi171) + 4) = 0, rsp172 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp172 = 0xa1e8, rax173 = getgroup(rdi171, rdi171), rsp12 = reinterpret_cast<void*>(rsp172 + 1), rax173 == 0)) {
            rsi31 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
            rsp174 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
            *rsp174 = 0xa042;
            eax175 = fun_46c0();
            rsp12 = reinterpret_cast<void*>(rsp174 + 1);
        } else {
            rsi31 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
            rsp176 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
            *rsp176 = 0xa1fb;
            eax175 = gnu_mbswidth(rax173, rax173);
            rsp12 = reinterpret_cast<void*>(rsp176 + 1);
            if (reinterpret_cast<int32_t>(eax175) < reinterpret_cast<int32_t>(0)) {
                eax175 = 0;
            }
        }
        less177 = reinterpret_cast<int32_t>(group_width) < reinterpret_cast<int32_t>(eax175);
        if (less177) {
            group_width = eax175;
        }
    }
    zf178 = print_author == 0;
    if (!zf178 && (edi179 = rbx20->f34, rsp180 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp180 = 0x9ff8, eax181 = format_user_width(edi179, edi179), rsp12 = reinterpret_cast<void*>(rsp180 + 1), less_or_equal182 = reinterpret_cast<int32_t>(eax181) <= reinterpret_cast<int32_t>(author_width), !less_or_equal182)) {
        author_width = eax181;
    }
    zf183 = print_scontext == 0;
    if (!zf183) 
        goto addr_9a1f_114;
    addr_9a37_129:
    edx184 = format;
    if (edx184) 
        goto addr_97e8_115;
    rsp185 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp185 = 0x9a55;
    rax186 = umaxtostr();
    rsp187 = rsp185 + 1 - 1;
    *rsp187 = 0x9a5d;
    rax188 = fun_4860(rax186, rax186);
    rsp189 = reinterpret_cast<void*>(rsp187 + 1);
    less_or_equal190 = *reinterpret_cast<int32_t*>(&rax188) <= nlink_width;
    if (!less_or_equal190) {
        nlink_width = *reinterpret_cast<int32_t*>(&rax188);
    }
    if ((rbx20->f30 & 0xb000) == 0x2000) 
        goto addr_9a7e_133;
    rdi191 = rbx20->f48;
    r8_192 = file_output_block_size;
    rsi193 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp8) - 0x2d0);
    *reinterpret_cast<int32_t*>(&rdx194) = file_human_output_opts;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx194) + 4) = 0;
    rsp195 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp189) - 8);
    *rsp195 = 0x9d82;
    rax196 = human_readable(rdi191, rsi193, rdx194, 1, r8_192, rdi191, rsi193, rdx194, 1, r8_192);
    rsi31 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    rsp197 = rsp195 + 1 - 1;
    *rsp197 = 0x9d8c;
    eax198 = gnu_mbswidth(rax196, rax196);
    rsp12 = reinterpret_cast<void*>(rsp197 + 1);
    less_or_equal199 = reinterpret_cast<int32_t>(eax198) <= reinterpret_cast<int32_t>(file_size_width);
    if (less_or_equal199) {
        addr_97e8_115:
        eax200 = print_inode;
        if (*reinterpret_cast<signed char*>(&eax200) && (rsi31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xfffffffffffffd30), rsp201 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp201 = 0x9ca8, rax202 = umaxtostr(), rsp203 = rsp201 + 1 - 1, *rsp203 = 0x9cb0, rax204 = fun_4860(rax202, rax202), rsp12 = reinterpret_cast<void*>(rsp203 + 1), less_or_equal205 = *reinterpret_cast<int32_t*>(&rax204) <= inode_number_width, !less_or_equal205)) {
            inode_number_width = *reinterpret_cast<int32_t*>(&rax204);
            goto addr_97f7_34;
        }
    } else {
        addr_9d98_136:
        file_size_width = eax198;
        goto addr_97e8_115;
    }
    addr_9a7e_133:
    rsp206 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp189) - 8);
    *rsp206 = 0x9aa9;
    rax207 = umaxtostr();
    rsp208 = rsp206 + 1 - 1;
    *rsp208 = 0x9ab1;
    rax209 = fun_4860(rax207, rax207);
    less_or_equal210 = *reinterpret_cast<int32_t*>(&rax209) <= reinterpret_cast<int32_t>(major_device_number_width);
    if (!less_or_equal210) {
        major_device_number_width = *reinterpret_cast<uint32_t*>(&rax209);
    }
    rsi31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp8) + 0xfffffffffffffd30);
    rsp211 = rsp208 + 1 - 1;
    *rsp211 = 0x9ad8;
    rax212 = umaxtostr();
    rsp213 = rsp211 + 1 - 1;
    *rsp213 = 0x9ae0;
    rax214 = fun_4860(rax212, rax212);
    rsp12 = reinterpret_cast<void*>(rsp213 + 1);
    *reinterpret_cast<int32_t*>(&rdx215) = minor_device_number_width;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx215) + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&rax214) > *reinterpret_cast<int32_t*>(&rdx215)) {
        minor_device_number_width = *reinterpret_cast<int32_t*>(&rax214);
        *reinterpret_cast<int32_t*>(&rdx215) = *reinterpret_cast<int32_t*>(&rax214);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx215) + 4) = 0;
    }
    *reinterpret_cast<uint32_t*>(&rax216) = major_device_number_width;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax216) + 4) = 0;
    eax198 = static_cast<uint32_t>(rdx215 + rax216 + 2);
    less_or_equal217 = reinterpret_cast<int32_t>(eax198) <= reinterpret_cast<int32_t>(file_size_width);
    if (less_or_equal217) 
        goto addr_97e8_115;
    goto addr_9d98_136;
    addr_9a1f_114:
    rdi218 = rbx20->fb0;
    rsp219 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp219 = 0x9a2b;
    rax220 = fun_4860(rdi218, rdi218);
    rsp12 = reinterpret_cast<void*>(rsp219 + 1);
    less_or_equal221 = *reinterpret_cast<int32_t*>(&rax220) <= scontext_width;
    if (!less_or_equal221) {
        scontext_width = *reinterpret_cast<int32_t*>(&rax220);
        goto addr_9a37_129;
    }
    addr_9ea5_90:
    addr_9ea8_77:
    zf222 = check_symlink_mode == 0;
    if (zf222) 
        goto addr_9f73_108; else 
        goto addr_9eb5_91;
    addr_9789_75:
    rbx20->fbc = 0;
    goto addr_9793_76;
    addr_9c23_44:
    if (!eax69) 
        goto addr_9700_35;
    addr_a0d6_144:
    rsp223 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp223 = 0xa0e9;
    rax224 = fun_4840();
    *reinterpret_cast<int32_t*>(&r14_55) = 0;
    *reinterpret_cast<int32_t*>(&r14_55 + 4) = 0;
    rsp225 = rsp223 + 1 - 1;
    *rsp225 = 0xa0fc;
    file_failure(1, rax224, r14_33);
    rsp12 = reinterpret_cast<void*>(rsp225 + 1);
    rbx20->fb0 = reinterpret_cast<void**>("?");
    goto addr_980a_30;
    addr_9c06_45:
    if (eax69 < 0) {
        rsp226 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp226 = 0xa0cd;
        rax227 = fun_46f0();
        rsp12 = reinterpret_cast<void*>(rsp226 + 1);
        if (*rax227 == 2) 
            goto addr_96db_28; else 
            goto addr_a0d6_144;
    } else {
        if ((rbx20->f30 & 0xf000) != 0x4000) 
            goto addr_96db_28; else 
            goto addr_9c23_44;
    }
    addr_9986_46:
    while (rsp72 != rdi77) {
        rsp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - 0x1000);
    }
    *reinterpret_cast<uint32_t*>(&rcx228) = *reinterpret_cast<uint32_t*>(&rcx76) & reinterpret_cast<uint32_t>("c");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx228) + 4) = 0;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - reinterpret_cast<int64_t>(rcx228));
    if (rcx228) {
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp12) + reinterpret_cast<int64_t>(rcx228) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp12) + reinterpret_cast<int64_t>(rcx228) - 8);
    }
    *reinterpret_cast<uint32_t*>(&rdi21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_9 + 1));
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    r9_229 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp12) + 15 & 0xfffffffffffffff0);
    r14_33 = r9_229;
    if (*reinterpret_cast<void***>(&r8d75) == 46) {
        rax230 = r9_229;
        if (!*reinterpret_cast<signed char*>(&rdi21)) {
            addr_9e0d_152:
            if (*reinterpret_cast<void***>(&edx73)) {
                rcx231 = r12_11;
                do {
                    ++rcx231;
                    *reinterpret_cast<void***>(rax230) = *reinterpret_cast<void***>(&edx73);
                    ++rax230;
                    edx73 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx231));
                } while (*reinterpret_cast<void***>(&edx73));
            }
        } else {
            goto addr_9dd9_156;
        }
    } else {
        addr_9dd9_156:
        rcx232 = r15_9;
        goto addr_9dea_157;
    }
    *reinterpret_cast<void***>(rax230) = reinterpret_cast<void**>(0);
    goto addr_96a0_24;
    addr_9dea_157:
    while (rax230 = r9_229 + 1, ++rcx232, *reinterpret_cast<void***>(rax230 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&r8d75), !!*reinterpret_cast<signed char*>(&rdi21)) {
        r8d75 = *reinterpret_cast<uint32_t*>(&rdi21);
        *reinterpret_cast<uint32_t*>(&rdi21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx232 + 1));
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        r9_229 = rax230;
    }
    if (reinterpret_cast<unsigned char>(r15_9) < reinterpret_cast<unsigned char>(rcx232) && *reinterpret_cast<void***>(rcx232 + 0xffffffffffffffff) != 47) {
        *reinterpret_cast<void***>(rax230) = reinterpret_cast<void**>(47);
        rax230 = r9_229 + 2;
        goto addr_9e0d_152;
    }
}

/* out_tty.12 */
unsigned char out_tty_12 = 0xff;

uint32_t fun_4740(int64_t rdi);

unsigned char stdout_isatty(void** rdi, ...) {
    uint32_t eax2;
    uint32_t eax3;
    uint32_t eax4;
    uint32_t eax5;

    eax2 = out_tty_12;
    if (*reinterpret_cast<signed char*>(&eax2) < 0) {
        eax3 = fun_4740(1);
        out_tty_12 = *reinterpret_cast<unsigned char*>(&eax3);
        eax4 = eax3 & 1;
        return *reinterpret_cast<unsigned char*>(&eax4);
    } else {
        eax5 = eax2 & 1;
        return *reinterpret_cast<unsigned char*>(&eax5);
    }
}

void** pending_dirs = reinterpret_cast<void**>(0);

void queue_directory(void** rdi, void** rsi, void** edx, void** rcx, void** r8) {
    void** r13d6;
    void** r12_7;
    void** rbp8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;

    r13d6 = edx;
    r12_7 = rsi;
    rbp8 = rdi;
    rax9 = xmalloc(32, rsi);
    if (r12_7) {
        rax10 = xstrdup(r12_7, rsi);
        r12_7 = rax10;
    }
    *reinterpret_cast<void***>(rax9 + 8) = r12_7;
    if (rbp8) {
        rax11 = xstrdup(rbp8, rsi);
        rbp8 = rax11;
    }
    rax12 = pending_dirs;
    *reinterpret_cast<void***>(rax9) = rbp8;
    *reinterpret_cast<void***>(rax9 + 16) = r13d6;
    *reinterpret_cast<void***>(rax9 + 24) = rax12;
    pending_dirs = rax9;
    return;
}

signed char used_color = 0;

int32_t fun_4a70(int64_t rdi, void** rsi, void** rdx);

void signal_setup(signed char dil, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t g25088 = 0;

void put_indicator(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8) {
    int1_t zf9;
    int32_t eax10;
    int1_t zf11;
    void** rbx12;

    zf9 = used_color == 0;
    if (zf9) {
        used_color = 1;
        eax10 = fun_4a70(1, rsi, rdx);
        if (eax10 >= 0) {
            signal_setup(1, rsi, rdx, rcx, r8, r9);
        }
        zf11 = g25088 == 0;
        if (zf11) {
            put_indicator(0x25060, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
            put_indicator(0x25090, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
            put_indicator(0x25070, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
        } else {
            put_indicator(0x25080, rsi, rdx, rcx, r8, r9, rbx12, __return_address());
        }
    }
}

void fun_4a10(int64_t rdi);

void fun_4750(int64_t rdi, ...);

void fun_4c50(int64_t rdi, int64_t rsi, void** rdx);

int32_t fun_4be0();

void fun_49c0(int64_t rdi);

void signal_setup(signed char dil, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rax7;
    void* v8;
    void** rbx9;
    void** r15d10;
    void** r14_11;
    void** rbp12;
    int64_t rdi13;
    void** v14;
    int64_t rsi15;
    void** ebp16;
    void** rsi17;
    int32_t eax18;
    void** rax19;
    int64_t rdi20;
    void** v21;
    void** ebp22;
    int32_t eax23;
    void* rax24;
    int1_t zf25;
    int32_t eax26;
    int1_t zf27;

    rax7 = g28;
    v8 = rax7;
    if (dil) {
        rbx9 = reinterpret_cast<void**>(0x1a664);
        r15d10 = reinterpret_cast<void**>(20);
        r14_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8);
        rbp12 = reinterpret_cast<void**>(0x1a664);
        fun_4a10(0x26220);
        while (1) {
            rdx = r14_11;
            *reinterpret_cast<void***>(&rdi13) = r15d10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
            fun_4750(rdi13, rdi13);
            if (v14 != 1) {
                *reinterpret_cast<void***>(&rsi15) = r15d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi15) + 4) = 0;
                fun_4c50(0x26220, rsi15, rdx);
            }
            if (reinterpret_cast<int1_t>(rbp12 == 0x1a690)) 
                break;
            r15d10 = *reinterpret_cast<void***>(rbp12);
            rbp12 = rbp12 + 4;
        }
        __asm__("movdqa xmm0, [rip+0x1eadc]");
        __asm__("movdqa xmm1, [rip+0x1eae4]");
        ebp16 = reinterpret_cast<void**>(20);
        __asm__("movdqa xmm2, [rip+0x1eadc]");
        __asm__("movdqa xmm3, [rip+0x1eae4]");
        __asm__("movdqa xmm4, [rip+0x1eae5]");
        __asm__("movdqa xmm5, [rip+0x1eaed]");
        __asm__("movups [rsp+0x8], xmm0");
        __asm__("movdqa xmm6, [rip+0x1eaf0]");
        __asm__("movdqa xmm7, [rip+0x1eaf8]");
        __asm__("movups [rsp+0x18], xmm1");
        __asm__("movups [rsp+0x28], xmm2");
        __asm__("movups [rsp+0x38], xmm3");
        __asm__("movups [rsp+0x48], xmm4");
        __asm__("movups [rsp+0x58], xmm5");
        __asm__("movups [rsp+0x68], xmm6");
        __asm__("movups [rsp+0x78], xmm7");
        while (1) {
            rsi17 = ebp16;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            eax18 = fun_4be0();
            if (eax18) {
                rax19 = reinterpret_cast<void**>(0x68f0);
                rsi17 = r14_11;
                *reinterpret_cast<void***>(&rdi20) = ebp16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                if (ebp16 == 20) {
                    rax19 = reinterpret_cast<void**>(0x74b0);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                v21 = rax19;
                fun_4750(rdi20, rdi20);
            }
            if (reinterpret_cast<int1_t>(rbx9 == 0x1a690)) 
                break;
            ebp16 = *reinterpret_cast<void***>(rbx9);
            rbx9 = rbx9 + 4;
        }
    } else {
        rbx9 = reinterpret_cast<void**>(0x1a664);
        ebp22 = reinterpret_cast<void**>(20);
        while (1) {
            rsi17 = ebp22;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            eax23 = fun_4be0();
            if (!eax23) {
                if (rbx9 == 0x1a690) 
                    break;
            } else {
                rsi17 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
                *reinterpret_cast<void***>(&rdi20) = ebp22;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                fun_49c0(rdi20);
                if (reinterpret_cast<int1_t>(rbx9 == 0x1a690)) 
                    break;
            }
            ebp22 = *reinterpret_cast<void***>(rbx9);
            rbx9 = rbx9 + 4;
        }
    }
    rax24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
    if (!rax24) {
        return;
    }
    fun_4870();
    zf25 = used_color == 0;
    if (!zf25) 
        goto addr_781d_24;
    used_color = 1;
    eax26 = fun_4a70(1, rsi17, rdx);
    if (eax26 >= 0) {
        signal_setup(1, rsi17, rdx, rcx, r8, r9);
    }
    zf27 = g25088 == 0;
    if (zf27) {
        put_indicator(0x25060, rsi17, rdx, rcx, r8, r9, rbx9, v21);
        put_indicator(0x25090, rsi17, rdx, rcx, r8, r9, rbx9, v21);
        put_indicator(0x25070, rsi17, rdx, rcx, r8, r9, rbx9, v21);
    } else {
        put_indicator(0x25080, rsi17, rdx, rcx, r8, r9, rbx9, v21);
    }
    addr_781d_24:
}

signed char get_funky_string(void** rdi, void** rsi, void** edx, void** rcx, void** r8) {
    void** eax6;
    void** r11_7;
    void** r10_8;
    void** r12d9;
    void** rbp10;
    void** rcx11;
    void* rdi12;
    uint32_t edx13;
    void** r8_14;
    void** rsi15;
    void** r9_16;
    int1_t less_or_equal17;
    int64_t rdx18;
    void** r13_19;
    int32_t r9d20;
    int64_t rdx21;
    int32_t r13d22;
    uint32_t edx23;
    int64_t r8_24;

    eax6 = edx;
    r11_7 = rdi;
    r10_8 = rsi;
    r12d9 = edx;
    rbp10 = rcx;
    rcx11 = *reinterpret_cast<void***>(rsi);
    *reinterpret_cast<int32_t*>(&rdi12) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11));
    r8_14 = reinterpret_cast<void**>(0);
    rsi15 = *reinterpret_cast<void***>(rdi) + 1;
    r9_16 = rsi15 + 0xffffffffffffffff;
    less_or_equal17 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) <= reinterpret_cast<signed char>(92);
    if (*reinterpret_cast<void***>(&edx13) != 92) 
        goto addr_694a_2;
    while (*reinterpret_cast<uint32_t*>(&rdx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11 + 1)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx18) + 4) = 0, r13_19 = rcx11 + 2, !!*reinterpret_cast<void***>(&rdx18)) {
        r9d20 = static_cast<int32_t>(rdx18 - 48);
        if (*reinterpret_cast<unsigned char*>(&r9d20) <= 72) 
            goto addr_6990_5;
        rcx11 = r13_19;
        *reinterpret_cast<void***>(rsi15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&rdx18);
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
        ++rsi15;
        while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11)), r9_16 = rsi15 + 0xffffffffffffffff, r8_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdi12) + 0xffffffffffffffff), less_or_equal17 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) <= reinterpret_cast<signed char>(92), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 92)) {
            addr_694a_2:
            if (!less_or_equal17) {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 94)) {
                    *reinterpret_cast<uint32_t*>(&rdx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx11 + 1));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx21) + 4) = 0;
                    r13d22 = static_cast<int32_t>(rdx21 - 64);
                    if (*reinterpret_cast<unsigned char*>(&r13d22) > 62) {
                        ++rcx11;
                        if (*reinterpret_cast<signed char*>(&rdx21) != 63) 
                            goto addr_6a11_12;
                        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
                        ++rsi15;
                        *reinterpret_cast<signed char*>(rsi15 + 0xfffffffffffffffe) = 0x7f;
                        continue;
                    } else {
                        edx23 = *reinterpret_cast<uint32_t*>(&rdx21) & 31;
                        rcx11 = rcx11 + 2;
                        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
                        ++rsi15;
                        *reinterpret_cast<signed char*>(rsi15 + 0xfffffffffffffffe) = *reinterpret_cast<signed char*>(&edx23);
                        continue;
                    }
                }
            } else {
                if (*reinterpret_cast<void***>(&edx13) == 61) {
                    if (*reinterpret_cast<signed char*>(&r12d9)) 
                        goto addr_69ae_17;
                } else {
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&edx13)) > reinterpret_cast<signed char>(61)) 
                        goto addr_6957_20;
                    if (!*reinterpret_cast<void***>(&edx13)) 
                        goto addr_69a9_22;
                    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&edx13) == 58)) 
                        goto addr_69a9_22;
                }
            }
            addr_6957_20:
            ++rcx11;
            *reinterpret_cast<void***>(rsi15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&edx13);
            rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi12) + 1);
            ++rsi15;
        }
    }
    rcx11 = r13_19;
    eax6 = reinterpret_cast<void**>(0);
    addr_69ae_17:
    *reinterpret_cast<void***>(r11_7) = r9_16;
    *reinterpret_cast<void***>(r10_8) = rcx11;
    *reinterpret_cast<void***>(rbp10) = r8_14;
    return *reinterpret_cast<signed char*>(&eax6);
    addr_6990_5:
    *reinterpret_cast<uint32_t*>(&r8_24) = *reinterpret_cast<unsigned char*>(&r9d20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_24) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1a020 + r8_24 * 4) + 0x1a020;
    addr_6a11_12:
    eax6 = reinterpret_cast<void**>(0);
    goto addr_69ae_17;
    addr_69a9_22:
    eax6 = reinterpret_cast<void**>(1);
    goto addr_69ae_17;
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
};

struct s2 {
    int64_t f0;
    struct s3* f8;
};

unsigned char is_colored(uint32_t edi, ...) {
    int64_t rdi2;
    int32_t r8d3;
    struct s2* rax4;
    int32_t eax5;
    uint32_t eax6;
    int32_t eax7;
    int32_t eax8;

    *reinterpret_cast<uint32_t*>(&rdi2) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi2) + 4) = 0;
    r8d3 = 0;
    rax4 = reinterpret_cast<struct s2*>(0x25060 + (rdi2 << 4));
    if (rax4->f0) {
        if (rax4->f0 == 1) {
            *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(rax4->f8->f0 != 48);
            eax5 = r8d3;
            return *reinterpret_cast<unsigned char*>(&eax5);
        } else {
            r8d3 = 1;
            if (rax4->f0 == 2) {
                eax6 = rax4->f8->f0 - 48;
                if (!eax6) {
                    eax6 = rax4->f8->f1 - 48;
                }
                *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!eax6);
                eax7 = r8d3;
                return *reinterpret_cast<unsigned char*>(&eax7);
            }
        }
    }
    eax8 = r8d3;
    return *reinterpret_cast<unsigned char*>(&eax8);
}

void print_current_files(void** rdi, void** rsi, ...) {
    int1_t below_or_equal3;
    int64_t rax4;

    below_or_equal3 = reinterpret_cast<unsigned char>(format) <= reinterpret_cast<unsigned char>(4);
    if (!below_or_equal3) {
        return;
    } else {
        *reinterpret_cast<void***>(&rax4) = format;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1a17c + rax4 * 4) + 0x1a17c;
    }
}

int64_t xstrtoumax(void** rdi, ...);

struct s4 {
    int64_t f0;
    unsigned char* f8;
};

void** decode_line_length(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void* rax6;
    int64_t rax7;
    void** rax8;
    void** v9;
    int64_t rax10;
    void* rdx11;
    int64_t rdi12;
    struct s4* rax13;
    int64_t v14;
    int64_t v15;
    int64_t v16;

    rax6 = g28;
    rax7 = xstrtoumax(rdi);
    if (!*reinterpret_cast<int32_t*>(&rax7)) {
        rax8 = v9;
        if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) {
            rax8 = reinterpret_cast<void**>(0);
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax10) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) != 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        rax8 = reinterpret_cast<void**>(-rax10);
    }
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
    if (!rdx11) {
        return rax8;
    }
    fun_4870();
    *reinterpret_cast<int32_t*>(&rdi12) = *reinterpret_cast<int32_t*>(&rdi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    rax13 = reinterpret_cast<struct s4*>(0x25060 + (rdi12 << 4));
    if (rax13->f0) 
        goto addr_6c4b_10;
    goto v14;
    addr_6c4b_10:
    if (rax13->f0 == 1) {
        goto v15;
    } else {
        if (rax13->f0 == 2) {
            if (!(*rax13->f8 - 48)) {
            }
            goto v16;
        }
    }
}

void** xnmalloc(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_4c80(void** rdi, int64_t rsi, int64_t rdx, void** rcx, ...);

void** file_escape(void** rdi, int32_t esi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t r12d7;
    void** rbx8;
    void** rax9;
    void** rax10;
    void** r13_11;
    uint32_t eax12;
    void** rbp13;
    int64_t rdx14;
    void** rdi15;

    r12d7 = esi;
    rbx8 = rdi;
    rax9 = fun_4860(rdi);
    rax10 = xnmalloc(3, rax9 + 1, rdx, rcx, r8, r9);
    r13_11 = rax10;
    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    rbp13 = r13_11;
    if (*reinterpret_cast<void***>(&eax12)) {
        do {
            addr_7286_3:
            ++rbx8;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax12) == 47) || !*reinterpret_cast<signed char*>(&r12d7)) {
                *reinterpret_cast<uint32_t*>(&rdx14) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax12));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                if (*reinterpret_cast<signed char*>(0x25fe0 + rdx14)) {
                    *reinterpret_cast<void***>(rbp13) = *reinterpret_cast<void***>(&eax12);
                    ++rbp13;
                } else {
                    rdi15 = rbp13;
                    rbp13 = rbp13 + 3;
                    fun_4c80(rdi15, 1, -1, "%%%02x", rdi15, 1, -1, "%%%02x");
                    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
                    if (*reinterpret_cast<void***>(&eax12)) 
                        goto addr_7286_3; else 
                        break;
                }
            } else {
                *reinterpret_cast<void***>(rbp13) = reinterpret_cast<void**>(47);
                ++rbp13;
            }
            eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
        } while (*reinterpret_cast<void***>(&eax12));
    }
    *reinterpret_cast<void***>(rbp13) = reinterpret_cast<void**>(0);
    return r13_11;
}

void** get_type_indicator(signed char dil, void** esi, uint32_t edx, void** rcx, void** r8, void** r9) {
    uint32_t eax7;
    unsigned char cl8;
    int1_t zf9;
    uint32_t ecx10;
    int1_t zf11;
    int1_t zf12;
    uint32_t eax13;
    int32_t eax14;
    uint32_t esi15;
    uint32_t eax16;

    if (!dil) {
        eax7 = 0;
        if (edx == 5) {
            return *reinterpret_cast<void***>(&eax7);
        } else {
            if (edx == 3 || edx == 9) {
                return 47;
            } else {
                cl8 = reinterpret_cast<uint1_t>(edx == 6);
                zf9 = indicator_style == 1;
                if (zf9) {
                    return 0;
                }
            }
        }
    } else {
        ecx10 = reinterpret_cast<unsigned char>(esi) & 0xf000;
        if (ecx10 != 0x8000) {
            if (ecx10 == 0x4000) {
                return 47;
            } else {
                cl8 = reinterpret_cast<uint1_t>(ecx10 == 0xa000);
                zf11 = indicator_style == 1;
                if (zf11) {
                    return 0;
                }
            }
        } else {
            eax7 = 0;
            zf12 = indicator_style == 3;
            if (zf12) {
                *reinterpret_cast<void***>(&eax7) = reinterpret_cast<void**>(-static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < reinterpret_cast<unsigned char>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(esi) & 73)))))))));
                eax13 = eax7 & 42;
                return *reinterpret_cast<void***>(&eax13);
            }
        }
    }
    eax7 = 64;
    if (!cl8) {
        if (!dil) {
            eax14 = 0x7c;
            if (edx == 1) {
                return 0x7c;
            } else {
                *reinterpret_cast<void***>(&eax14) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(edx == 7)));
            }
        } else {
            esi15 = reinterpret_cast<unsigned char>(esi) & 0xf000;
            eax14 = 0x7c;
            if (esi15 == 0x1000) {
                return 0x7c;
            } else {
                *reinterpret_cast<void***>(&eax14) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(esi15 == 0xc000)));
            }
        }
        eax16 = reinterpret_cast<uint32_t>(-eax14) & 61;
        return *reinterpret_cast<void***>(&eax16);
    }
}

/* quote_name_buf.constprop.0 */
void** quote_name_buf_constprop_0(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s5 {
    signed char[1] pad1;
    void** f1;
};

struct s6 {
    signed char[1] pad1;
    void** f1;
};

void** max_idx = reinterpret_cast<void**>(0);

/* column_info_alloc.3 */
void** column_info_alloc_3 = reinterpret_cast<void**>(0);

void** column_info = reinterpret_cast<void**>(0);

void xalloc_die();

void** length_of_file_name_and_frills(void** rdi, void** rsi, ...);

void** line_length = reinterpret_cast<void**>(0);

struct s7 {
    signed char[1] pad1;
    void** f1;
};

void* quote_name_width(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void** rbp8;
    void** rcx9;
    void* rax10;
    void** r9_11;
    void** r8_12;
    void* rsp13;
    void** rdi14;
    void* rax15;
    unsigned char v16;
    void* rdx17;
    void* v18;
    void* rsp19;
    void* rax20;
    int1_t zf21;
    void** rbx22;
    int1_t zf23;
    int1_t zf24;
    struct s5* rax25;
    void** rax26;
    signed char v27;
    int64_t rdx28;
    void** v29;
    void** rax30;
    void** rax31;
    int1_t zf32;
    int1_t zf33;
    void** v34;
    void** rax35;
    void** rax36;
    struct s6* rax37;
    void* rax38;
    void* v39;
    void** r8_40;
    uint32_t eax41;
    unsigned char v42;
    void** v43;
    uint32_t v44;
    void** al45;
    void** rsi46;
    void** v47;
    void** v48;
    void* rax49;
    int64_t v50;
    void** rsi51;
    void** rax52;
    void** r12_53;
    uint32_t ebp54;
    void* v55;
    void** rdi56;
    void** rbx57;
    void** rax58;
    void** r9_59;
    void** r8_60;
    void** rdi61;
    void** rbx62;
    void** rdx63;
    void** rcx64;
    void** rdx65;
    void** tmp64_66;
    void** rdx67;
    void** rax68;
    void** rdx69;
    void** rdi70;
    void** rdx71;
    int64_t* rax72;
    int64_t* rcx73;
    int64_t* rdx74;
    void** rdx75;
    signed char* rax76;
    int64_t v77;
    void** rax78;
    void** rdi79;
    void** rax80;
    void** r10_81;
    void** r9_82;
    void** r11_83;
    void** rcx84;
    void** rdi85;
    void** r8_86;
    int64_t rax87;
    void*** rdx88;
    void** rax89;
    void** rdi90;
    void** rax91;
    int1_t zf92;
    void** rax93;
    void** rax94;
    struct s7* rbx95;
    int1_t zf96;
    int1_t zf97;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x1000 - 0x1000 - 56);
    rbp8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
    rcx9 = edx;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    rax10 = g28;
    r9_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 15);
    r8_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 24);
    quote_name_buf_constprop_0(reinterpret_cast<int64_t>(rsp7) + 16, rdi, rsi, rcx9, r8_12, r9_11);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    rdi14 = rbp8;
    if (rdi14 != rbp8 && rdi14 != rdi) {
        fun_4630(rdi14, rdi14);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
    }
    *reinterpret_cast<uint32_t*>(&rax15) = v16;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    rdx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax10) - reinterpret_cast<uint64_t>(g28));
    if (!rdx17) {
        return reinterpret_cast<int64_t>(rax15) + reinterpret_cast<uint64_t>(v18);
    }
    fun_4870();
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 - 8 - 0x2a8);
    rax20 = g28;
    zf21 = print_inode == 0;
    if (!zf21) 
        goto addr_7dcc_7;
    *reinterpret_cast<int32_t*>(&rbx22) = 0;
    *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
    addr_7e72_9:
    zf23 = print_block_size == 0;
    if (!zf23) {
        zf24 = reinterpret_cast<int1_t>(format == 4);
        if (!zf24) {
            rax25 = reinterpret_cast<struct s5*>(static_cast<int64_t>(reinterpret_cast<int32_t>(block_size_width)));
            rax26 = reinterpret_cast<void**>(&rax25->f1);
        } else {
            *reinterpret_cast<int32_t*>(&rax26) = 2;
            *reinterpret_cast<int32_t*>(&rax26 + 4) = 0;
            if (v27) {
                r8_12 = output_block_size;
                rcx9 = reinterpret_cast<void**>(0x200);
                *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx28) = human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx28) + 4) = 0;
                rax30 = human_readable(v29, rsp19, rdx28, 0x200, r8_12, v29, rsp19, rdx28, 0x200, r8_12);
                rdi14 = rax30;
                rax31 = fun_4860(rdi14, rdi14);
                rax26 = rax31 + 1;
            }
        }
        rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<unsigned char>(rax26));
    }
    zf32 = print_scontext == 0;
    if (!zf32) {
        zf33 = reinterpret_cast<int1_t>(format == 4);
        if (zf33) {
            rdi14 = v34;
            rax35 = fun_4860(rdi14, rdi14);
            rax36 = rax35 + 1;
        } else {
            addr_7ed9_18:
            rax37 = reinterpret_cast<struct s6*>(static_cast<int64_t>(scontext_width));
            rax36 = reinterpret_cast<void**>(&rax37->f1);
        }
        rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<unsigned char>(rax36));
        rax38 = v39;
        if (rax38) {
            addr_7e1c_20:
            r8_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) + reinterpret_cast<uint64_t>(rax38));
            eax41 = indicator_style;
            if (eax41) {
                *reinterpret_cast<uint32_t*>(&rdi14) = v42;
                al45 = get_type_indicator(*reinterpret_cast<signed char*>(&rdi14), v43, v44, rcx9, r8_40, r9_11);
                r8_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_40) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_40) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(al45) < 1)))))));
            }
        } else {
            addr_7ef7_22:
            rsi46 = filename_quoting_options;
            rdi14 = v47;
            rax38 = quote_name_width(rdi14, rsi46, v48, rcx9, r8_12, r9_11);
            goto addr_7e1c_20;
        }
        rax49 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax20) - reinterpret_cast<uint64_t>(g28));
        if (!rax49) {
            goto v50;
        }
        fun_4870();
        rsi51 = max_idx;
        rax52 = column_info_alloc_3;
        r12_53 = cwd_n_used;
        ebp54 = *reinterpret_cast<uint32_t*>(&rdi14);
        if (rsi51) 
            goto addr_7f94_27;
    } else {
        addr_7e0c_28:
        rax38 = v55;
        if (!rax38) 
            goto addr_7ef7_22; else 
            goto addr_7e1c_20;
    }
    if (reinterpret_cast<unsigned char>(r12_53) > reinterpret_cast<unsigned char>(rax52)) {
        addr_7fb5_30:
        rdi56 = column_info;
        rbx57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_53) + reinterpret_cast<unsigned char>(r12_53));
        rax58 = xreallocarray(rdi56, r12_53, 48, rcx9);
        column_info = rax58;
    } else {
        addr_81fc_31:
        r9_59 = r12_53;
        if (r12_53) {
            addr_806c_32:
            r8_60 = column_info;
            *reinterpret_cast<int32_t*>(&rsi51) = 3;
            *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi61) = 0;
            *reinterpret_cast<int32_t*>(&rdi61 + 4) = 0;
            goto addr_8080_33;
        } else {
            addr_8208_34:
            if (r9_59) {
                addr_80c1_35:
                *reinterpret_cast<int32_t*>(&rbx62) = 0;
                *reinterpret_cast<int32_t*>(&rbx62 + 4) = 0;
                goto addr_80c8_36;
            } else {
                goto addr_81eb_38;
            }
        }
    }
    addr_7fd4_39:
    rdx63 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx64) = 0;
    *reinterpret_cast<int32_t*>(&rcx64 + 4) = 0;
    rdx65 = rdx63 + 1;
    tmp64_66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx65) + reinterpret_cast<unsigned char>(rbx57));
    *reinterpret_cast<unsigned char*>(&rcx64) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_66) < reinterpret_cast<unsigned char>(rdx65));
    *reinterpret_cast<uint32_t*>(&rdx67) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx67 + 4) = 0;
    if (rcx64 || rdx67) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi51) = 8;
    *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
    rax68 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx57) - reinterpret_cast<unsigned char>(rdx63)) * reinterpret_cast<unsigned char>(tmp64_66) >> 1, 8, rdx67, rcx64, r8_40, r9_11);
    rdx69 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx57) > reinterpret_cast<unsigned char>(rdx69)) {
        rdi70 = column_info;
        rdx71 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx69) * 8 + 8);
        rsi51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx57) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi70) + reinterpret_cast<uint64_t>(rdx71 + reinterpret_cast<unsigned char>(rdx71) * 2) - 8) = rax68;
            rax68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax68) + reinterpret_cast<unsigned char>(rdx71));
            rdx71 = rdx71 + 8;
        } while (rsi51 != rdx71);
    }
    column_info_alloc_3 = rbx57;
    r9_59 = cwd_n_used;
    if (!r12_53) 
        goto addr_8208_34; else 
        goto addr_806c_32;
    do {
        addr_8080_33:
        rax72 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_60 + reinterpret_cast<unsigned char>(rsi51) * 8) - 16) = rsi51;
        rcx73 = rax72 + reinterpret_cast<unsigned char>(rdi61);
        do {
            rdx74 = rax72;
            *rax72 = 3;
            ++rax72;
        } while (rcx73 != rdx74);
        ++rdi61;
        rsi51 = rsi51 + 3;
    } while (reinterpret_cast<unsigned char>(rdi61) < reinterpret_cast<unsigned char>(r12_53));
    if (r9_59) 
        goto addr_80c1_35;
    addr_81bd_48:
    if (reinterpret_cast<unsigned char>(r12_53) > reinterpret_cast<unsigned char>(1)) {
        rdx75 = column_info;
        rax76 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx75 + reinterpret_cast<uint64_t>(r12_53 + reinterpret_cast<unsigned char>(r12_53) * 2) * 8) - 24);
        do {
            if (*rax76) 
                break;
            --r12_53;
            rax76 = rax76 - 24;
        } while (r12_53 != 1);
    }
    addr_81eb_38:
    goto v77;
    do {
        addr_80c8_36:
        rax78 = sorted_file;
        rdi79 = *reinterpret_cast<void***>(rax78 + reinterpret_cast<unsigned char>(rbx62) * 8);
        rax80 = length_of_file_name_and_frills(rdi79, rsi51, rdi79, rsi51);
        r10_81 = cwd_n_used;
        r9_82 = rax80;
        if (r12_53) {
            r11_83 = line_length;
            rsi51 = column_info;
            *reinterpret_cast<int32_t*>(&rcx84) = 0;
            *reinterpret_cast<int32_t*>(&rcx84 + 4) = 0;
            do {
                rdi85 = rcx84;
                ++rcx84;
                if (*reinterpret_cast<void***>(rsi51)) {
                    if (*reinterpret_cast<signed char*>(&ebp54)) {
                        r8_86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx62) / ((reinterpret_cast<unsigned char>(r10_81) + reinterpret_cast<unsigned char>(rcx84) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx84)));
                    } else {
                        r8_86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx62) % reinterpret_cast<unsigned char>(rcx84));
                    }
                    *reinterpret_cast<int32_t*>(&rax87) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax87) = reinterpret_cast<uint1_t>(r8_86 != rdi85);
                    rdx88 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi51 + 16) + reinterpret_cast<unsigned char>(r8_86) * 8);
                    rax89 = r9_82 + rax87 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx88) < reinterpret_cast<unsigned char>(rax89)) {
                        *reinterpret_cast<void***>(rsi51 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi51 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax89) - reinterpret_cast<unsigned char>(*rdx88)));
                        *rdx88 = rax89;
                        *reinterpret_cast<void***>(rsi51) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi51 + 8)) < reinterpret_cast<unsigned char>(r11_83))));
                    }
                }
                rsi51 = rsi51 + 24;
            } while (r12_53 != rcx84);
        }
        ++rbx62;
    } while (reinterpret_cast<unsigned char>(rbx62) < reinterpret_cast<unsigned char>(r10_81));
    goto addr_81bd_48;
    addr_7f94_27:
    if (reinterpret_cast<unsigned char>(rsi51) < reinterpret_cast<unsigned char>(r12_53)) {
        if (reinterpret_cast<unsigned char>(rsi51) <= reinterpret_cast<unsigned char>(rax52)) {
            r9_59 = r12_53;
            r12_53 = rsi51;
            goto addr_806c_32;
        }
        r12_53 = rsi51;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi51) >> 1) > reinterpret_cast<unsigned char>(r12_53)) 
            goto addr_7fb5_30;
    } else {
        if (reinterpret_cast<unsigned char>(r12_53) <= reinterpret_cast<unsigned char>(rax52)) 
            goto addr_81fc_31;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi51) >> 1) > reinterpret_cast<unsigned char>(r12_53)) 
            goto addr_7fb5_30;
    }
    rdi90 = column_info;
    rax91 = xreallocarray(rdi90, rsi51, 24, rcx9);
    rbx57 = max_idx;
    column_info = rax91;
    goto addr_7fd4_39;
    addr_7dcc_7:
    zf92 = reinterpret_cast<int1_t>(format == 4);
    if (zf92) {
        rax93 = umaxtostr();
        rdi14 = rax93;
        rax94 = fun_4860(rdi14, rdi14);
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
        rbx22 = rax94 + 1;
        goto addr_7e72_9;
    } else {
        rbx95 = reinterpret_cast<struct s7*>(static_cast<int64_t>(inode_number_width));
        rbx22 = reinterpret_cast<void**>(&rbx95->f1);
        zf96 = print_block_size == 0;
        if (zf96) {
            zf97 = print_scontext == 0;
            if (!zf97) 
                goto addr_7ed9_18;
            goto addr_7e0c_28;
        }
    }
}

struct s8 {
    signed char[1] pad1;
    void** f1;
};

struct s9 {
    signed char[1] pad1;
    void** f1;
};

struct s10 {
    signed char[1] pad1;
    void** f1;
};

void** length_of_file_name_and_frills(void** rdi, void** rsi, ...) {
    void** rbp3;
    void* rsp4;
    void* rax5;
    int1_t zf6;
    void** rbx7;
    int1_t zf8;
    void** rax9;
    void** rax10;
    struct s8* rbx11;
    int1_t zf12;
    int1_t zf13;
    int1_t zf14;
    int1_t zf15;
    struct s9* rax16;
    void** rax17;
    void** rdi18;
    void** r8_19;
    int64_t rdx20;
    void** rax21;
    void** rax22;
    int1_t zf23;
    int1_t zf24;
    void** rax25;
    void** rax26;
    struct s10* rax27;
    void* rax28;
    void** r8_29;
    uint32_t eax30;
    uint32_t edx31;
    void** esi32;
    void** r9_33;
    void** al34;
    void** edx35;
    void** rsi36;
    void** r9_37;
    void* rax38;
    void** rsi39;
    void** rax40;
    void** r12_41;
    uint32_t ebp42;
    void** rdi43;
    void** rbx44;
    void** rax45;
    void** r9_46;
    void** r8_47;
    void** rdi48;
    void** rbx49;
    void** rdx50;
    void** rcx51;
    void** rdx52;
    void** tmp64_53;
    void** rdx54;
    void** r9_55;
    void** rax56;
    void** rdx57;
    void** rdi58;
    void** rdx59;
    int64_t* rax60;
    int64_t* rcx61;
    int64_t* rdx62;
    void** rdx63;
    signed char* rax64;
    int64_t v65;
    void** rax66;
    void** rdi67;
    void** rax68;
    void** r10_69;
    void** r9_70;
    void** r11_71;
    void** rcx72;
    void** rdi73;
    void** r8_74;
    int64_t rax75;
    void*** rdx76;
    void** rax77;
    void** rdi78;
    void** rax79;

    rbp3 = rdi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8);
    rax5 = g28;
    zf6 = print_inode == 0;
    if (zf6) {
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
    } else {
        zf8 = reinterpret_cast<int1_t>(format == 4);
        if (zf8) {
            rax9 = umaxtostr();
            rdi = rax9;
            rax10 = fun_4860(rdi, rdi);
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
            rbx7 = rax10 + 1;
        } else {
            rbx11 = reinterpret_cast<struct s8*>(static_cast<int64_t>(inode_number_width));
            rbx7 = reinterpret_cast<void**>(&rbx11->f1);
            zf12 = print_block_size == 0;
            if (zf12) {
                zf13 = print_scontext == 0;
                if (!zf13) 
                    goto addr_7ed9_7;
                goto addr_7e0c_9;
            }
        }
    }
    zf14 = print_block_size == 0;
    if (!zf14) {
        zf15 = reinterpret_cast<int1_t>(format == 4);
        if (!zf15) {
            rax16 = reinterpret_cast<struct s9*>(static_cast<int64_t>(reinterpret_cast<int32_t>(block_size_width)));
            rax17 = reinterpret_cast<void**>(&rax16->f1);
        } else {
            *reinterpret_cast<int32_t*>(&rax17) = 2;
            *reinterpret_cast<int32_t*>(&rax17 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(rbp3 + 0xb8)) {
                rdi18 = *reinterpret_cast<void***>(rbp3 + 88);
                r8_19 = output_block_size;
                *reinterpret_cast<int32_t*>(&rdx20) = human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                rax21 = human_readable(rdi18, rsp4, rdx20, 0x200, r8_19);
                rdi = rax21;
                rax22 = fun_4860(rdi, rdi);
                rax17 = rax22 + 1;
            }
        }
        rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax17));
    }
    zf23 = print_scontext == 0;
    if (!zf23) {
        zf24 = reinterpret_cast<int1_t>(format == 4);
        if (zf24) {
            rdi = *reinterpret_cast<void***>(rbp3 + 0xb0);
            rax25 = fun_4860(rdi, rdi);
            rax26 = rax25 + 1;
        } else {
            addr_7ed9_7:
            rax27 = reinterpret_cast<struct s10*>(static_cast<int64_t>(scontext_width));
            rax26 = reinterpret_cast<void**>(&rax27->f1);
        }
        rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax26));
        rax28 = *reinterpret_cast<void**>(rbp3 + 0xc8);
        if (rax28) {
            addr_7e1c_20:
            r8_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<uint64_t>(rax28));
            eax30 = indicator_style;
            if (eax30) {
                edx31 = *reinterpret_cast<uint32_t*>(rbp3 + 0xa8);
                esi32 = *reinterpret_cast<void***>(rbp3 + 48);
                *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(rbp3 + 0xb8);
                al34 = get_type_indicator(*reinterpret_cast<signed char*>(&rdi), esi32, edx31, 0x200, r8_29, r9_33);
                r8_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_29) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_29) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(al34) < 1)))))));
            }
        } else {
            addr_7ef7_22:
            edx35 = *reinterpret_cast<void***>(rbp3 + 0xc4);
            rsi36 = filename_quoting_options;
            rdi = *reinterpret_cast<void***>(rbp3);
            rax28 = quote_name_width(rdi, rsi36, edx35, 0x200, r8_19, r9_37);
            goto addr_7e1c_20;
        }
        rax38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
        if (!rax38) {
            return r8_29;
        }
        fun_4870();
        rsi39 = max_idx;
        rax40 = column_info_alloc_3;
        r12_41 = cwd_n_used;
        ebp42 = *reinterpret_cast<uint32_t*>(&rdi);
        if (rsi39) 
            goto addr_7f94_27;
    } else {
        addr_7e0c_9:
        rax28 = *reinterpret_cast<void**>(rbp3 + 0xc8);
        if (!rax28) 
            goto addr_7ef7_22; else 
            goto addr_7e1c_20;
    }
    if (reinterpret_cast<unsigned char>(r12_41) > reinterpret_cast<unsigned char>(rax40)) {
        addr_7fb5_29:
        rdi43 = column_info;
        rbx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_41) + reinterpret_cast<unsigned char>(r12_41));
        rax45 = xreallocarray(rdi43, r12_41, 48, 0x200);
        column_info = rax45;
    } else {
        addr_81fc_30:
        r9_46 = r12_41;
        if (r12_41) {
            addr_806c_31:
            r8_47 = column_info;
            *reinterpret_cast<int32_t*>(&rsi39) = 3;
            *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi48) = 0;
            *reinterpret_cast<int32_t*>(&rdi48 + 4) = 0;
            goto addr_8080_32;
        } else {
            addr_8208_33:
            if (r9_46) {
                addr_80c1_34:
                *reinterpret_cast<int32_t*>(&rbx49) = 0;
                *reinterpret_cast<int32_t*>(&rbx49 + 4) = 0;
                goto addr_80c8_35;
            } else {
                goto addr_81eb_37;
            }
        }
    }
    addr_7fd4_38:
    rdx50 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx51) = 0;
    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
    rdx52 = rdx50 + 1;
    tmp64_53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx52) + reinterpret_cast<unsigned char>(rbx44));
    *reinterpret_cast<unsigned char*>(&rcx51) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_53) < reinterpret_cast<unsigned char>(rdx52));
    *reinterpret_cast<uint32_t*>(&rdx54) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx54 + 4) = 0;
    if (rcx51 || rdx54) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi39) = 8;
    *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
    rax56 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx44) - reinterpret_cast<unsigned char>(rdx50)) * reinterpret_cast<unsigned char>(tmp64_53) >> 1, 8, rdx54, rcx51, r8_29, r9_55);
    rdx57 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx44) > reinterpret_cast<unsigned char>(rdx57)) {
        rdi58 = column_info;
        rdx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx57) * 8 + 8);
        rsi39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx44) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi58) + reinterpret_cast<uint64_t>(rdx59 + reinterpret_cast<unsigned char>(rdx59) * 2) - 8) = rax56;
            rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax56) + reinterpret_cast<unsigned char>(rdx59));
            rdx59 = rdx59 + 8;
        } while (rsi39 != rdx59);
    }
    column_info_alloc_3 = rbx44;
    r9_46 = cwd_n_used;
    if (!r12_41) 
        goto addr_8208_33; else 
        goto addr_806c_31;
    do {
        addr_8080_32:
        rax60 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_47 + reinterpret_cast<unsigned char>(rsi39) * 8) - 16) = rsi39;
        rcx61 = rax60 + reinterpret_cast<unsigned char>(rdi48);
        do {
            rdx62 = rax60;
            *rax60 = 3;
            ++rax60;
        } while (rcx61 != rdx62);
        ++rdi48;
        rsi39 = rsi39 + 3;
    } while (reinterpret_cast<unsigned char>(rdi48) < reinterpret_cast<unsigned char>(r12_41));
    if (r9_46) 
        goto addr_80c1_34;
    addr_81bd_47:
    if (reinterpret_cast<unsigned char>(r12_41) > reinterpret_cast<unsigned char>(1)) {
        rdx63 = column_info;
        rax64 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx63 + reinterpret_cast<uint64_t>(r12_41 + reinterpret_cast<unsigned char>(r12_41) * 2) * 8) - 24);
        do {
            if (*rax64) 
                break;
            --r12_41;
            rax64 = rax64 - 24;
        } while (r12_41 != 1);
    }
    addr_81eb_37:
    goto v65;
    do {
        addr_80c8_35:
        rax66 = sorted_file;
        rdi67 = *reinterpret_cast<void***>(rax66 + reinterpret_cast<unsigned char>(rbx49) * 8);
        rax68 = length_of_file_name_and_frills(rdi67, rsi39, rdi67, rsi39);
        r10_69 = cwd_n_used;
        r9_70 = rax68;
        if (r12_41) {
            r11_71 = line_length;
            rsi39 = column_info;
            *reinterpret_cast<int32_t*>(&rcx72) = 0;
            *reinterpret_cast<int32_t*>(&rcx72 + 4) = 0;
            do {
                rdi73 = rcx72;
                ++rcx72;
                if (*reinterpret_cast<void***>(rsi39)) {
                    if (*reinterpret_cast<signed char*>(&ebp42)) {
                        r8_74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx49) / ((reinterpret_cast<unsigned char>(r10_69) + reinterpret_cast<unsigned char>(rcx72) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx72)));
                    } else {
                        r8_74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx49) % reinterpret_cast<unsigned char>(rcx72));
                    }
                    *reinterpret_cast<int32_t*>(&rax75) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax75) = reinterpret_cast<uint1_t>(r8_74 != rdi73);
                    rdx76 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi39 + 16) + reinterpret_cast<unsigned char>(r8_74) * 8);
                    rax77 = r9_70 + rax75 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx76) < reinterpret_cast<unsigned char>(rax77)) {
                        *reinterpret_cast<void***>(rsi39 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi39 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax77) - reinterpret_cast<unsigned char>(*rdx76)));
                        *rdx76 = rax77;
                        *reinterpret_cast<void***>(rsi39) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi39 + 8)) < reinterpret_cast<unsigned char>(r11_71))));
                    }
                }
                rsi39 = rsi39 + 24;
            } while (r12_41 != rcx72);
        }
        ++rbx49;
    } while (reinterpret_cast<unsigned char>(rbx49) < reinterpret_cast<unsigned char>(r10_69));
    goto addr_81bd_47;
    addr_7f94_27:
    if (reinterpret_cast<unsigned char>(rsi39) < reinterpret_cast<unsigned char>(r12_41)) {
        if (reinterpret_cast<unsigned char>(rsi39) <= reinterpret_cast<unsigned char>(rax40)) {
            r9_46 = r12_41;
            r12_41 = rsi39;
            goto addr_806c_31;
        }
        r12_41 = rsi39;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi39) >> 1) > reinterpret_cast<unsigned char>(r12_41)) 
            goto addr_7fb5_29;
    } else {
        if (reinterpret_cast<unsigned char>(r12_41) <= reinterpret_cast<unsigned char>(rax40)) 
            goto addr_81fc_30;
        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi39) >> 1) > reinterpret_cast<unsigned char>(r12_41)) 
            goto addr_7fb5_29;
    }
    rdi78 = column_info;
    rax79 = xreallocarray(rdi78, rsi39, 24, 0x200);
    rbx44 = max_idx;
    column_info = rax79;
    goto addr_7fd4_38;
}

void** dired_pos = reinterpret_cast<void**>(0);

void dired_outbuf(void** rdi, void** rsi, ...) {
    void** tmp64_3;

    tmp64_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rsi));
    dired_pos = tmp64_3;
}

uint32_t sort_type = 0;

uint32_t time_type = 0;

int64_t fun_46e0();

int32_t calc_req_mask(void** rdi, ...) {
    int1_t zf2;
    void** edx3;
    int1_t below_or_equal4;
    int64_t rdx5;
    int64_t rdx6;
    int1_t zf7;
    int1_t zf8;
    int1_t zf9;
    int1_t below_or_equal10;
    int64_t rdx11;

    zf2 = print_block_size == 0;
    if (!zf2) {
    }
    edx3 = format;
    if (edx3) {
        below_or_equal4 = sort_type <= 6;
        if (below_or_equal4) {
            *reinterpret_cast<uint32_t*>(&rdx5) = sort_type;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x1a160 + rdx5 * 4) + 0x1a160;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rdx6) = time_type;
        if (*reinterpret_cast<uint32_t*>(&rdx6) <= 3) {
            zf7 = print_owner == 0;
            if (!zf7 || (zf8 = print_author == 0, !zf8)) {
            }
            zf9 = print_group == 0;
            if (!zf9) {
            }
            below_or_equal10 = sort_type <= 6;
            if (below_or_equal10) {
                *reinterpret_cast<uint32_t*>(&rdx11) = sort_type;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0x1a144 + rdx11 * 4) + 0x1a144;
            }
        }
    }
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
}

int32_t fun_4b30();

struct s11 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s11* fun_48e0(void** rdi, int64_t rsi);

int32_t do_statx(void** rdi, void** rsi, struct s1* rdx) {
    void* rax4;
    int32_t eax5;
    uint64_t rcx6;
    int32_t v7;
    uint64_t rdx8;
    int32_t v9;
    uint64_t rdi10;
    uint64_t rdi11;
    uint64_t rdi12;
    int64_t rcx13;
    int32_t v14;
    uint64_t rcx15;
    int32_t v16;
    void** v17;
    uint16_t v18;
    uint64_t r8_19;
    uint64_t r8_20;
    int64_t v21;
    uint64_t rdx22;
    int32_t v23;
    uint64_t rsi24;
    int64_t rsi25;
    int32_t v26;
    int64_t rsi27;
    int32_t v28;
    int64_t v29;
    void** rsi30;
    int32_t v31;
    int64_t v32;
    int32_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    uint32_t r8d37;
    unsigned char v38;
    int32_t v39;
    int64_t v40;
    void* rdx41;
    void** rdi42;
    struct s11* rax43;
    void** rdi44;
    struct s11* rbp45;
    struct s11* rax46;
    struct s11* rsi47;
    int32_t eax48;
    int64_t v49;

    rax4 = g28;
    eax5 = fun_4b30();
    if (eax5 >= 0 && (*reinterpret_cast<int32_t*>(&rcx6) = v7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0, *reinterpret_cast<int32_t*>(&rdx8) = v9, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0, rdi10 = rcx6 << 8, *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdi10) & 0xfff00, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi12) = *reinterpret_cast<unsigned char*>(&rdx8), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0, rdi = reinterpret_cast<void**>(0xffffff00000), *reinterpret_cast<int32_t*>(&rcx13) = v14, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx13) + 4) = 0, rdx->f0 = reinterpret_cast<void**>(rdx8 << 12 & reinterpret_cast<unsigned char>(0xffffff00000) | (rcx6 << 32 & 0xfffff00000000000 | rdi11 | rdi12)), rdx->f10 = rcx13, *reinterpret_cast<int32_t*>(&rcx15) = v16, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx15) + 4) = 0, rdx->f8 = v17, rdx->f18 = v18, r8_19 = rcx15 << 8, *reinterpret_cast<uint32_t*>(&r8_20) = *reinterpret_cast<uint32_t*>(&r8_19) & 0xfff00, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0, rdx->f1c = v21, *reinterpret_cast<int32_t*>(&rdx22) = v23, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0, *reinterpret_cast<uint32_t*>(&rsi24) = *reinterpret_cast<unsigned char*>(&rdx22), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi24) + 4) = 0, *reinterpret_cast<int32_t*>(&rsi25) = v26, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0, rdx->f28 = rdx22 << 12 & reinterpret_cast<unsigned char>(0xffffff00000) | (rcx15 << 32 & 0xfffff00000000000 | r8_20 | rsi24), rdx->f38 = rsi25, *reinterpret_cast<int32_t*>(&rsi27) = v28, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi27) + 4) = 0, rdx->f30 = v29, rdx->f50 = rsi27, *reinterpret_cast<int32_t*>(&rsi30) = v31, *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0, rdx->f40 = v32, rdx->f60 = rsi30, *reinterpret_cast<int32_t*>(&rsi) = v33, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rdx->f48 = v34, rdx->f70 = rsi, rdx->f58 = v35, rdx->f68 = v36, !!(r8d37 & 0x800))) {
        if (!(v38 & 8)) {
            rdx->f60 = reinterpret_cast<void**>(0xffffffffffffffff);
            rdx->f58 = -1;
        } else {
            *reinterpret_cast<int32_t*>(&rdi) = v39;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rdx->f58 = v40;
            rdx->f60 = rdi;
        }
    }
    rdx41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (!rdx41) {
        return eax5;
    }
    fun_4870();
    rdi42 = *reinterpret_cast<void***>(rdi);
    rax43 = fun_48e0(rdi42, 46);
    rdi44 = *reinterpret_cast<void***>(rsi);
    rbp45 = rax43;
    rax46 = fun_48e0(rdi44, 46);
    rsi47 = rax46;
    if (rsi47) 
        goto addr_71c2_9;
    rsi47 = reinterpret_cast<struct s11*>(0x1bb0a);
    addr_71c2_9:
    if (!rbp45) {
        rbp45 = reinterpret_cast<struct s11*>(0x1bb0a);
    }
    eax48 = reinterpret_cast<int32_t>(rdx41(rbp45, rsi47));
    if (eax48) {
        goto v49;
    } else {
        goto rdx41;
    }
}

int64_t quotearg_style(int64_t rdi, void** rsi);

int32_t exit_status = 0;

void file_failure(uint32_t edi, void** rsi, void** rdx, ...) {
    uint32_t ebx4;
    int32_t eax5;

    ebx4 = edi;
    quotearg_style(4, rdx);
    fun_46f0();
    fun_4b70();
    if (!*reinterpret_cast<signed char*>(&ebx4)) {
        eax5 = exit_status;
        if (!eax5) {
            exit_status = 1;
            return;
        }
    } else {
        exit_status = 2;
    }
    return;
}

void** stdout = reinterpret_cast<void**>(0);

void** fun_48d0();

void** hostname = reinterpret_cast<void**>(0);

int32_t fun_4b20(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

unsigned char dired = 0;

void _obstack_newchunk(void** rdi, ...);

void fun_4ae0(void* rdi);

void** color_ext_list = reinterpret_cast<void**>(0);

struct s12 {
    signed char[151648] pad151648;
    void** f25060;
};

int32_t c_strncasecmp();

void process_signals(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void* quote_name(void** rdi, void** rsi, void** edx, void** rcx, uint32_t r8d, void** r9, void** a7) {
    void*** rsp8;
    void** r15_9;
    void** rcx10;
    void* rax11;
    void** r8_12;
    void** rax13;
    void** rdx14;
    void** r9_15;
    void** rsi16;
    void** v17;
    void** rax18;
    signed char v19;
    void** rdi20;
    void** tmp64_21;
    void** rax22;
    unsigned char al23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    void** v28;
    int64_t r13_29;
    void** r9_30;
    signed char v31;
    void** rdi32;
    unsigned char v33;
    void** rax34;
    void** rdi35;
    void** rax36;
    void** rax37;
    void** rcx38;
    void** r9_39;
    int1_t zf40;
    void** rdx41;
    void** rax42;
    void** rcx43;
    uint32_t esi44;
    void** tmp64_45;
    void** tmp64_46;
    int1_t zf47;
    void** rdx48;
    void** rax49;
    void** rdi50;
    uint32_t edx51;
    void** rax52;
    void* rax53;
    unsigned char v54;
    void** rdx55;
    void** r15_56;
    uint32_t r14d57;
    void** rbp58;
    uint32_t eax59;
    void** r13_60;
    void** v61;
    int1_t zf62;
    uint32_t eax63;
    unsigned char v64;
    uint32_t esi65;
    uint32_t v66;
    uint32_t v67;
    uint32_t ecx68;
    unsigned char v69;
    uint32_t edx70;
    void** v71;
    unsigned char al72;
    unsigned char al73;
    unsigned char al74;
    signed char v75;
    unsigned char al76;
    uint64_t v77;
    unsigned char al78;
    void** rax79;
    void** rbx80;
    void** rcx81;
    struct s12* rcx82;
    void** v83;
    int32_t eax84;
    void** rcx85;
    unsigned char al86;
    unsigned char al87;
    unsigned char al88;
    void* rcx89;
    uint64_t rdx90;
    int64_t rcx91;
    int32_t v92;
    uint32_t r14d93;
    void** rdx94;
    void** v95;
    void** r8_96;
    void** rsi97;
    void** v98;
    void* rax99;
    int1_t zf100;
    void** v101;
    void** v102;
    void** v103;
    void** v104;
    unsigned char al105;
    void** rsi106;
    void** rdx107;
    void** v108;
    uint32_t r14d109;
    void** r8_110;
    void** v111;
    void** rcx112;
    void** rsi113;
    void* rax114;
    void** v115;
    int1_t zf116;
    unsigned char al117;
    void** v118;
    uint32_t v119;
    signed char v120;
    unsigned char v121;
    unsigned char al122;

    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 72);
    *reinterpret_cast<uint32_t*>(&r15_9) = r8d;
    rcx10 = edx;
    *reinterpret_cast<int32_t*>(&rcx10 + 4) = 0;
    rax11 = g28;
    *reinterpret_cast<int32_t*>(&r8_12) = 0;
    *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    rax13 = reinterpret_cast<void**>(rsp8 + 48);
    rdx14 = rsi;
    r9_15 = reinterpret_cast<void**>(rsp8 + 39);
    rsi16 = rdi;
    v17 = rax13;
    rax18 = quote_name_buf_constprop_0(rsp8 + 40, rsi16, rdx14, rcx10, 0, r9_15);
    if (v19 && *reinterpret_cast<signed char*>(&r15_9)) {
        rdi20 = stdout;
        tmp64_21 = dired_pos + 1;
        dired_pos = tmp64_21;
        rax22 = *reinterpret_cast<void***>(rdi20 + 40);
        if (reinterpret_cast<unsigned char>(rax22) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi20 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi16) = 32;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            fun_48d0();
        } else {
            rdx14 = rax22 + 1;
            *reinterpret_cast<void***>(rdi20 + 40) = rdx14;
            *reinterpret_cast<void***>(rax22) = reinterpret_cast<void**>(32);
        }
    }
    if (rcx) {
        al23 = is_colored(4, 4);
        r8_12 = reinterpret_cast<void**>(0x25060);
        if (al23) {
            put_indicator(0x25060, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v24);
            put_indicator(0x25070, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v25);
            r8_12 = reinterpret_cast<void**>(0x25060);
        }
        put_indicator(0x25060, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v26);
        put_indicator(rcx, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v27);
        put_indicator(0x25070, rsi16, rdx14, rcx10, 0x25060, r9_15, v17, v28);
    }
    if (a7) {
        *reinterpret_cast<uint32_t*>(&r13_29) = align_variable_outer_quotes;
        if (!*reinterpret_cast<signed char*>(&r13_29) || (*reinterpret_cast<uint32_t*>(&r13_29) = cwd_some_quoted, *reinterpret_cast<signed char*>(&r13_29) == 0)) {
            *reinterpret_cast<int32_t*>(&r9_30) = 0;
            *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
        } else {
            if (v31) {
                *reinterpret_cast<int32_t*>(&r9_30) = 0;
                *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_29) = 0;
            } else {
                rdi32 = stdout;
                *reinterpret_cast<uint32_t*>(&rdx14) = v33;
                *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                rax34 = *reinterpret_cast<void***>(rdi32 + 40);
                if (reinterpret_cast<unsigned char>(rax34) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi32 + 48))) {
                    fun_48d0();
                    *reinterpret_cast<int32_t*>(&r9_30) = 1;
                    *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                } else {
                    rcx10 = rax34 + 1;
                    *reinterpret_cast<int32_t*>(&r9_30) = 1;
                    *reinterpret_cast<int32_t*>(&r9_30 + 4) = 0;
                    *reinterpret_cast<void***>(rdi32 + 40) = rcx10;
                    *reinterpret_cast<void***>(rax34) = rdx14;
                }
            }
        }
        rdi35 = hostname;
        rax36 = file_escape(rdi35, 0, rdx14, rcx10, r8_12, r9_30);
        rax37 = file_escape(a7, 1, rdx14, rcx10, r8_12, r9_30);
        rcx38 = reinterpret_cast<void**>(0x1bb0a);
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax37) == 47)) {
            rcx38 = reinterpret_cast<void**>("/");
        }
        fun_4b20(1, 0x1baf0, rax36, rcx38, rax37);
        fun_4630(rax36, rax36);
        fun_4630(rax37, rax37);
        r9_39 = r9_30;
        if (!r9) 
            goto addr_bd32_20; else 
            goto addr_bbb2_21;
    }
    *reinterpret_cast<int32_t*>(&r9_39) = 0;
    *reinterpret_cast<int32_t*>(&r9_39 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r13_29) = 0;
    if (r9) {
        addr_bbb2_21:
        zf40 = dired == 0;
        if (!zf40) {
            rdx41 = *reinterpret_cast<void***>(r9 + 24);
            if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 32)) - reinterpret_cast<unsigned char>(rdx41)) <= 7) {
                _obstack_newchunk(r9, r9);
                rdx41 = *reinterpret_cast<void***>(r9 + 24);
                r9_39 = r9_39;
            }
            rax42 = dired_pos;
            *reinterpret_cast<void***>(rdx41) = rax42;
            *reinterpret_cast<void***>(r9 + 24) = *reinterpret_cast<void***>(r9 + 24) + 8;
        }
    } else {
        addr_bd32_20:
        rcx43 = stdout;
        esi44 = 1;
        fun_4ae0(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(r9_39));
        tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax18));
        dired_pos = tmp64_45;
        goto addr_bc2f_26;
    }
    rcx43 = stdout;
    esi44 = 1;
    fun_4ae0(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(r9_39));
    tmp64_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax18));
    dired_pos = tmp64_46;
    zf47 = dired == 0;
    if (!zf47) {
        rdx48 = *reinterpret_cast<void***>(r9 + 24);
        if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 32)) - reinterpret_cast<unsigned char>(rdx48)) <= 7) {
            esi44 = 8;
            _obstack_newchunk(r9);
            rdx48 = *reinterpret_cast<void***>(r9 + 24);
        }
        rax49 = dired_pos;
        *reinterpret_cast<void***>(rdx48) = rax49;
        *reinterpret_cast<void***>(r9 + 24) = *reinterpret_cast<void***>(r9 + 24) + 8;
    }
    addr_bc2f_26:
    if (a7 && (rcx43 = stdout, esi44 = 1, fun_4ae0(0x1bb04), !!*reinterpret_cast<signed char*>(&r13_29))) {
        rdi50 = stdout;
        edx51 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff);
        rax52 = *reinterpret_cast<void***>(rdi50 + 40);
        if (reinterpret_cast<unsigned char>(rax52) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi50 + 48))) {
            esi44 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&edx51));
            fun_48d0();
        } else {
            rcx43 = rax52 + 1;
            *reinterpret_cast<void***>(rdi50 + 40) = rcx43;
            *reinterpret_cast<void***>(rax52) = *reinterpret_cast<void***>(&edx51);
        }
    }
    if (rax13 != rdi && rax13 != v17) {
        fun_4630(rax13, rax13);
    }
    *reinterpret_cast<uint32_t*>(&rax53) = v54;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax53) + 4) = 0;
    rdx55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax11) - reinterpret_cast<uint64_t>(g28));
    if (!rdx55) {
        return reinterpret_cast<int64_t>(rax53) + reinterpret_cast<unsigned char>(rax18);
    }
    fun_4870();
    r15_56 = rdx55;
    r14d57 = esi44;
    rbp58 = rcx43;
    eax59 = print_with_color;
    if (*reinterpret_cast<signed char*>(&esi44)) 
        goto addr_be7a_40;
    r13_60 = v61;
    if (!*reinterpret_cast<signed char*>(&eax59)) 
        goto addr_be82_42;
    zf62 = color_symlink_as_referent == 0;
    eax63 = v64;
    if (zf62 || !*reinterpret_cast<signed char*>(&eax63)) {
        esi65 = v66;
    } else {
        esi65 = v67;
    }
    addr_bf24_46:
    ecx68 = v69;
    if (*reinterpret_cast<unsigned char*>(&ecx68)) {
        addr_c073_47:
        edx70 = esi65 & 0xf000;
        if (edx70 == 0x8000) {
            if (!(esi65 & 0x800) || (v71 = reinterpret_cast<void**>(0xc1b2), al72 = is_colored(16), al72 == 0)) {
                if (!(esi65 & reinterpret_cast<uint32_t>("v")) || (v71 = reinterpret_cast<void**>(0xc202), al73 = is_colored(17), al73 == 0)) {
                    v71 = reinterpret_cast<void**>(0xc1d2);
                    al74 = is_colored(21);
                    if (!al74 || !v75) {
                        if (!(esi65 & 73) || (v71 = reinterpret_cast<void**>(0xc28f), al76 = is_colored(14), al76 == 0)) {
                            if (v77 <= 1 || (v71 = reinterpret_cast<void**>(0xc2b6), al78 = is_colored(22), al78 == 0)) {
                                addr_c120_53:
                                v71 = reinterpret_cast<void**>(0xc128);
                                rax79 = fun_4860(r13_60);
                                rbx80 = color_ext_list;
                                rcx81 = rax79;
                                if (!rbx80) {
                                    addr_c171_54:
                                    *reinterpret_cast<int32_t*>(&rcx82) = 80;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                                    goto addr_bf8e_55;
                                } else {
                                    do {
                                        if (reinterpret_cast<unsigned char>(rcx81) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx80))) 
                                            continue;
                                        v83 = rcx81;
                                        v71 = reinterpret_cast<void**>(0xc15f);
                                        eax84 = c_strncasecmp();
                                        rcx81 = v83;
                                        if (!eax84) 
                                            break;
                                        rbx80 = *reinterpret_cast<void***>(rbx80 + 32);
                                    } while (rbx80);
                                    goto addr_c171_54;
                                }
                            } else {
                                *reinterpret_cast<int32_t*>(&rcx82) = 0x160;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                                goto addr_bf8e_55;
                            }
                        } else {
                            *reinterpret_cast<int32_t*>(&rcx82) = 0xe0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                            goto addr_bf8e_55;
                        }
                    } else {
                        *reinterpret_cast<int32_t*>(&rcx82) = 0x150;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                        goto addr_bf8e_55;
                    }
                } else {
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x110;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    goto addr_bf8e_55;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx82) = 0x100;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                goto addr_bf8e_55;
            }
            rcx85 = rbx80 + 16;
        } else {
            if (edx70 == 0x4000) {
                if ((esi65 & 0x202) == 0x202) {
                    v71 = reinterpret_cast<void**>(0xc2da);
                    al86 = is_colored(20);
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x140;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    if (al86) {
                        addr_bf8e_55:
                        rcx85 = reinterpret_cast<void**>(&rcx82->f25060);
                    } else {
                        goto addr_c222_70;
                    }
                } else {
                    addr_c222_70:
                    if ((!(*reinterpret_cast<unsigned char*>(&esi65) & 2) || (v71 = reinterpret_cast<void**>(0xc232), al87 = is_colored(19), *reinterpret_cast<int32_t*>(&rcx82) = 0x130, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, !al87)) && (*reinterpret_cast<int32_t*>(&rcx82) = 96, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, !!(esi65 & 0x200))) {
                        v71 = reinterpret_cast<void**>(0xc25a);
                        al88 = is_colored(18);
                        rcx89 = reinterpret_cast<void*>(96 - (96 + reinterpret_cast<uint1_t>(96 < 96 + reinterpret_cast<uint1_t>(al88 < 1))));
                        *reinterpret_cast<unsigned char*>(&rcx89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx89) & 64);
                        rcx82 = reinterpret_cast<struct s12*>(reinterpret_cast<uint64_t>(rcx89) + 0x120);
                        goto addr_bf8e_55;
                    }
                }
            } else {
                if (edx70 == 0xa000) {
                    *reinterpret_cast<int32_t*>(&rdx90) = 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx90) + 4) = 0;
                    goto addr_bf56_74;
                } else {
                    *reinterpret_cast<int32_t*>(&rcx82) = 0x80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                    if (edx70 != 0x1000 && ((*reinterpret_cast<int32_t*>(&rcx82) = 0x90, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, edx70 != 0xc000) && (*reinterpret_cast<int32_t*>(&rcx82) = 0xa0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0, edx70 != 0x6000))) {
                        *reinterpret_cast<int32_t*>(&rcx82) = 0xb0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
                        if (edx70 != 0x2000) {
                            rcx82 = reinterpret_cast<struct s12*>(0xd0);
                        }
                        goto addr_bf8e_55;
                    }
                }
            }
        }
    } else {
        addr_bf35_79:
        *reinterpret_cast<int32_t*>(&rcx91) = v92;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx91) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx90) = *reinterpret_cast<int32_t*>("\r" + rcx91 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx90) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&ecx68) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx90) == 7);
        if (*reinterpret_cast<int32_t*>(&rdx90) == 5) 
            goto addr_c120_53; else 
            goto addr_bf56_74;
    }
    if (*reinterpret_cast<void***>(rcx85 + 8)) {
        addr_bfb3_81:
        r14d93 = r14d57 ^ 1;
        rdx94 = v95;
        *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r8_96) = *reinterpret_cast<unsigned char*>(&r14d93);
        *reinterpret_cast<int32_t*>(&r8_96 + 4) = 0;
        rsi97 = filename_quoting_options;
        rax99 = quote_name(r13_60, rsi97, rdx94, rcx85, *reinterpret_cast<uint32_t*>(&r8_96), r15_56, v98);
        process_signals(r13_60, rsi97, rdx94, rcx85, r8_96, r15_56);
        zf100 = g25088 == 0;
        if (zf100) {
            put_indicator(0x25060, rsi97, v71, rcx85, r8_96, r15_56, v101, v83);
            put_indicator(0x25090, rsi97, v71, rcx85, r8_96, r15_56, v102, v83);
            put_indicator(0x25070, rsi97, v71, rcx85, r8_96, r15_56, v103, v83);
        } else {
            put_indicator(0x25080, rsi97, v71, rcx85, r8_96, r15_56, v104, v83);
        }
    } else {
        v71 = reinterpret_cast<void**>(0xbfa9);
        al105 = is_colored(4, 4);
        if (!al105) {
            addr_be82_42:
            rsi106 = filename_quoting_options;
            rdx107 = v108;
            *reinterpret_cast<int32_t*>(&rdx107 + 4) = 0;
            r14d109 = r14d57 ^ 1;
            *reinterpret_cast<uint32_t*>(&r8_110) = *reinterpret_cast<unsigned char*>(&r14d109);
            *reinterpret_cast<int32_t*>(&r8_110 + 4) = 0;
            quote_name(r13_60, rsi106, rdx107, 0, *reinterpret_cast<uint32_t*>(&r8_110), r15_56, v111);
            process_signals(r13_60, rsi106, rdx107, 0, r8_110, r15_56);
            goto addr_beb9_85;
        } else {
            *reinterpret_cast<int32_t*>(&rcx85) = 0;
            *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
            goto addr_bfb3_81;
        }
    }
    rcx112 = line_length;
    if (rcx112 && (rsi113 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) / reinterpret_cast<unsigned char>(rcx112)), rax114 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax99) + reinterpret_cast<unsigned char>(rbp58) + 0xffffffffffffffff), rsi113 != reinterpret_cast<uint64_t>(rax114) / reinterpret_cast<unsigned char>(rcx112))) {
        put_indicator(0x251d0, rsi113, reinterpret_cast<uint64_t>(rax114) % reinterpret_cast<unsigned char>(rcx112), rcx112, r8_96, r15_56, v115, v83);
    }
    addr_beb9_85:
    goto v17;
    addr_bf56_74:
    if (eax63 || !*reinterpret_cast<unsigned char*>(&ecx68)) {
        rcx82 = reinterpret_cast<struct s12*>(rdx90 << 4);
        goto addr_bf8e_55;
    } else {
        zf116 = color_symlink_as_referent == 0;
        *reinterpret_cast<int32_t*>(&rcx82) = 0xd0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
        if (zf116) {
            v71 = reinterpret_cast<void**>(0xbf7e);
            al117 = is_colored(13);
            rcx82 = reinterpret_cast<struct s12*>((0xd0 - (0xd0 + reinterpret_cast<uint1_t>(0xd0 < 0xd0 + reinterpret_cast<uint1_t>(al117 < 1))) & 0xffffffffffffffa0) + 0xd0);
            goto addr_bf8e_55;
        }
    }
    addr_be7a_40:
    r13_60 = v118;
    if (!*reinterpret_cast<signed char*>(&eax59)) 
        goto addr_be82_42;
    esi65 = v119;
    if (v120) {
        ecx68 = v121;
        eax63 = 0;
        if (!*reinterpret_cast<unsigned char*>(&ecx68)) 
            goto addr_bf35_79; else 
            goto addr_c073_47;
    } else {
        v71 = reinterpret_cast<void**>(0xbeed);
        al122 = is_colored(12);
        if (al122) {
            *reinterpret_cast<int32_t*>(&rcx82) = 0xc0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx82) + 4) = 0;
            goto addr_bf8e_55;
        } else {
            eax63 = 0xffffffff;
            goto addr_bf24_46;
        }
    }
}

int32_t interrupt_signal = 0;

int32_t stop_signal_count = 0;

void fun_4c20(void** rdi, void** rsi, ...);

void fun_46b0();

void fun_46d0();

void** getuser();

void process_signals(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void* rax7;
    void* v8;
    void** rbx9;
    int32_t eax10;
    int32_t eax11;
    int1_t zf12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** rdi17;
    int32_t r14d18;
    int32_t eax19;
    int64_t rdi20;
    void* rax21;
    int1_t zf22;
    void** rax23;
    uint32_t eax24;
    int64_t v25;

    rax7 = g28;
    v8 = rax7;
    rbx9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x90);
    while ((eax10 = interrupt_signal, !!eax10) || (eax11 = stop_signal_count, !!eax11)) {
        zf12 = used_color == 0;
        if (!zf12) {
            put_indicator(0x25060, rsi, rdx, rcx, r8, r9, v13, v14);
            put_indicator(0x25070, rsi, rdx, rcx, r8, r9, v15, v16);
        }
        rdi17 = stdout;
        fun_4c20(rdi17, rsi);
        fun_46b0();
        r14d18 = interrupt_signal;
        eax19 = stop_signal_count;
        if (!eax19) {
            *reinterpret_cast<int32_t*>(&rdi20) = r14d18;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
            fun_49c0(rdi20);
        } else {
            stop_signal_count = eax19 - 1;
        }
        fun_46d0();
        *reinterpret_cast<int32_t*>(&rdx) = 0;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbx9;
        fun_46b0();
    }
    rax21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
    if (!rax21) {
        return;
    }
    fun_4870();
    zf22 = numeric_ids == 0;
    if (!zf22) 
        goto addr_864c_13;
    rax23 = getuser();
    if (rax23) 
        goto addr_867d_15;
    addr_864c_13:
    addr_867d_15:
    eax24 = gnu_mbswidth(rax23);
    if (reinterpret_cast<int32_t>(eax24) < reinterpret_cast<int32_t>(0)) {
    }
    goto v25;
}

void* print_name_with_quoting(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9);

unsigned char eolbyte = 10;

/* print_file_name_and_frills.isra.0 */
void print_file_name_and_frills_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void* rsp8;
    void* rax9;
    int1_t zf10;
    unsigned char al11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** v17;
    int1_t zf18;
    void** rcx19;
    void** rax20;
    void** rdx21;
    int1_t zf22;
    int1_t zf23;
    void** rcx24;
    void** rdi25;
    int64_t rdx26;
    void** rax27;
    void** rdx28;
    int1_t zf29;
    int1_t zf30;
    void** rdx31;
    int1_t zf32;
    void** rcx33;
    void** rcx34;
    void** rdx35;
    void** rsi36;
    void** rdi37;
    uint32_t eax38;
    void** al39;
    void** tmp64_40;
    void* rax41;
    int1_t zf42;
    void** rdi43;
    uint32_t edx44;
    void** rax45;
    int64_t v46;
    void** r13_47;
    void** rbx48;
    int32_t r14d49;
    void** rax50;
    int1_t zf51;
    void** r12_52;
    void** rbp53;
    int64_t v54;
    int1_t below_or_equal55;
    void** rax56;
    void** rdx57;
    void** rcx58;
    void** rdi59;
    void** rax60;
    void** rdi61;
    void** rax62;

    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8);
    rax9 = g28;
    zf10 = print_with_color == 0;
    if (!zf10 && (al11 = is_colored(4), rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8), !!al11)) {
        put_indicator(0x25060, rsi, rdx, rcx, r8, r9, v12, v13);
        put_indicator(0x250a0, rsi, rdx, rcx, r8, r9, v14, v15);
        put_indicator(0x25070, rsi, rdx, rcx, r8, r9, v16, v17);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    zf18 = print_inode == 0;
    if (!zf18) {
        rcx19 = reinterpret_cast<void**>("?");
        if (*reinterpret_cast<unsigned char*>(rdi + 0xb8) && *reinterpret_cast<void***>(rdi + 32)) {
            rax20 = umaxtostr();
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            rcx19 = rax20;
        }
        *reinterpret_cast<int32_t*>(&rdx21) = 0;
        *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
        zf22 = reinterpret_cast<int1_t>(format == 4);
        if (!zf22) {
            *reinterpret_cast<int32_t*>(&rdx21) = inode_number_width;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
        }
        fun_4b20(1, "%*s ", rdx21, rcx19, r8);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    }
    zf23 = print_block_size == 0;
    if (!zf23) {
        rcx24 = reinterpret_cast<void**>("?");
        if (*reinterpret_cast<unsigned char*>(rdi + 0xb8)) {
            rdi25 = *reinterpret_cast<void***>(rdi + 88);
            r8 = output_block_size;
            *reinterpret_cast<int32_t*>(&rdx26) = human_output_opts;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
            rax27 = human_readable(rdi25, rsp8, rdx26, 0x200, r8);
            rcx24 = rax27;
        }
        *reinterpret_cast<uint32_t*>(&rdx28) = 0;
        *reinterpret_cast<int32_t*>(&rdx28 + 4) = 0;
        zf29 = reinterpret_cast<int1_t>(format == 4);
        if (!zf29) {
            *reinterpret_cast<uint32_t*>(&rdx28) = block_size_width;
            *reinterpret_cast<int32_t*>(&rdx28 + 4) = 0;
        }
        fun_4b20(1, "%*s ", rdx28, rcx24, r8);
    }
    zf30 = print_scontext == 0;
    if (!zf30) {
        *reinterpret_cast<int32_t*>(&rdx31) = 0;
        *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
        zf32 = reinterpret_cast<int1_t>(format == 4);
        rcx33 = *reinterpret_cast<void***>(rdi + 0xb0);
        if (!zf32) {
            *reinterpret_cast<int32_t*>(&rdx31) = scontext_width;
            *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
        }
        fun_4b20(1, "%*s ", rdx31, rcx33, r8);
    }
    rcx34 = rsi;
    *reinterpret_cast<uint32_t*>(&rdx35) = 0;
    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
    rsi36 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0;
    rdi37 = rdi;
    print_name_with_quoting(rdi37, 0, 0, rcx34, r8, r9);
    eax38 = indicator_style;
    if (eax38 && (*reinterpret_cast<uint32_t*>(&rdx35) = *reinterpret_cast<uint32_t*>(rdi + 0xa8), *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0, rsi36 = *reinterpret_cast<void***>(rdi + 48), *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi37) = *reinterpret_cast<unsigned char*>(rdi + 0xb8), al39 = get_type_indicator(*reinterpret_cast<signed char*>(&rdi37), rsi36, *reinterpret_cast<uint32_t*>(&rdx35), rcx34, r8, r9), !!al39)) {
        rdi37 = stdout;
        tmp64_40 = dired_pos + 1;
        dired_pos = tmp64_40;
        rdx35 = *reinterpret_cast<void***>(rdi37 + 40);
        if (reinterpret_cast<unsigned char>(rdx35) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi37 + 48))) {
            rsi36 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(al39)));
            *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0;
            fun_48d0();
        } else {
            rcx34 = rdx35 + 1;
            *reinterpret_cast<void***>(rdi37 + 40) = rcx34;
            *reinterpret_cast<void***>(rdx35) = al39;
        }
    }
    rax41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax9) - reinterpret_cast<uint64_t>(g28));
    if (!rax41) {
        return;
    }
    fun_4870();
    zf42 = cwd_n_used == 0;
    if (!zf42) 
        goto addr_c54c_27;
    addr_c650_28:
    rdi43 = stdout;
    edx44 = eolbyte;
    rax45 = *reinterpret_cast<void***>(rdi43 + 40);
    if (reinterpret_cast<unsigned char>(rax45) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi43 + 48))) {
        *reinterpret_cast<void***>(rdi43 + 40) = rax45 + 1;
        *reinterpret_cast<void***>(rax45) = *reinterpret_cast<void***>(&edx44);
        goto v46;
    }
    addr_c54c_27:
    *reinterpret_cast<int32_t*>(&r13_47) = 0;
    *reinterpret_cast<int32_t*>(&r13_47 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx48) = 0;
    *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
    r14d49 = *reinterpret_cast<signed char*>(&rdi37);
    while (1) {
        rax50 = sorted_file;
        zf51 = line_length == 0;
        r12_52 = *reinterpret_cast<void***>(rax50 + reinterpret_cast<unsigned char>(rbx48) * 8);
        if (zf51) {
            rbp53 = r13_47;
            if (rbx48) {
                rbp53 = r13_47 + 2;
                r13_47 = rbp53;
            } else {
                addr_c56c_35:
                rsi36 = rbp53;
                ++rbx48;
                print_file_name_and_frills_isra_0(r12_52, rsi36, rdx35, rcx34, r8, r9, v54);
                below_or_equal55 = reinterpret_cast<unsigned char>(cwd_n_used) <= reinterpret_cast<unsigned char>(rbx48);
                if (below_or_equal55) 
                    goto addr_c650_28; else 
                    continue;
            }
        } else {
            rax56 = length_of_file_name_and_frills(r12_52, rsi36, r12_52, rsi36);
            if (!rbx48) {
                rbp53 = r13_47;
                r13_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_47) + reinterpret_cast<unsigned char>(rax56));
                goto addr_c56c_35;
            }
            rdx57 = line_length;
            rbp53 = r13_47 + 2;
            rcx58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax56) + reinterpret_cast<unsigned char>(rbp53));
            if (!rdx57) 
                goto addr_c5d6_39;
            if (reinterpret_cast<unsigned char>(rdx57) <= reinterpret_cast<unsigned char>(rcx58)) 
                goto addr_c640_41;
            if (reinterpret_cast<unsigned char>(-3 - reinterpret_cast<unsigned char>(rax56)) < reinterpret_cast<unsigned char>(r13_47)) 
                goto addr_c640_41; else 
                goto addr_c5d6_39;
        }
        addr_c5e0_43:
        *reinterpret_cast<uint32_t*>(&rcx34) = 32;
        *reinterpret_cast<int32_t*>(&rcx34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx35) = 32;
        *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
        addr_c5ea_44:
        rdi59 = stdout;
        rax60 = *reinterpret_cast<void***>(rdi59 + 40);
        if (reinterpret_cast<unsigned char>(rax60) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi59 + 48))) {
            fun_48d0();
            *reinterpret_cast<uint32_t*>(&rdx35) = reinterpret_cast<unsigned char>(rdx35);
            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx34) = *reinterpret_cast<uint32_t*>(&rcx34);
            *reinterpret_cast<int32_t*>(&rcx34 + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi59 + 40) = rax60 + 1;
            *reinterpret_cast<void***>(rax60) = *reinterpret_cast<void***>(&r14d49);
        }
        rdi61 = stdout;
        rax62 = *reinterpret_cast<void***>(rdi61 + 40);
        if (reinterpret_cast<unsigned char>(rax62) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi61 + 48))) {
            fun_48d0();
            goto addr_c56c_35;
        } else {
            rcx34 = rax62 + 1;
            *reinterpret_cast<void***>(rdi61 + 40) = rcx34;
            *reinterpret_cast<void***>(rax62) = rdx35;
            goto addr_c56c_35;
        }
        addr_c5d6_39:
        r13_47 = rcx58;
        goto addr_c5e0_43;
        addr_c640_41:
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(eolbyte)));
        *reinterpret_cast<int32_t*>(&rcx34 + 4) = 0;
        r13_47 = rax56;
        *reinterpret_cast<int32_t*>(&rbp53) = 0;
        *reinterpret_cast<int32_t*>(&rbp53 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx35) = *reinterpret_cast<uint32_t*>(&rcx34);
        *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
        goto addr_c5ea_44;
    }
}

struct s13 {
    signed char[151648] pad151648;
    void** f25060;
};

void* print_name_with_quoting(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r14d8;
    void** r12_9;
    void** rbp10;
    uint32_t eax11;
    void** r13_12;
    int1_t zf13;
    uint32_t eax14;
    void** esi15;
    uint32_t ecx16;
    uint32_t edx17;
    void** v18;
    unsigned char al19;
    unsigned char al20;
    unsigned char al21;
    unsigned char al22;
    unsigned char al23;
    void** rax24;
    void** rbx25;
    void** rcx26;
    struct s13* rcx27;
    void** v28;
    int32_t eax29;
    void** rcx30;
    unsigned char al31;
    unsigned char al32;
    unsigned char al33;
    void* rcx34;
    uint64_t rdx35;
    int64_t rcx36;
    uint32_t r14d37;
    void** rdx38;
    void** v39;
    void** r8_40;
    void** rsi41;
    void* rax42;
    void* r12_43;
    int1_t zf44;
    void** v45;
    void** v46;
    void** v47;
    void** v48;
    unsigned char al49;
    void** rsi50;
    void** rdx51;
    void** v52;
    uint32_t r14d53;
    void** r8_54;
    void* rax55;
    void** rcx56;
    void** rsi57;
    void* rax58;
    void** v59;
    int1_t zf60;
    unsigned char al61;
    unsigned char al62;

    r15_7 = rdx;
    r14d8 = esi;
    r12_9 = rdi;
    rbp10 = rcx;
    eax11 = print_with_color;
    if (!*reinterpret_cast<signed char*>(&esi)) {
        r13_12 = *reinterpret_cast<void***>(rdi);
        if (!*reinterpret_cast<signed char*>(&eax11)) 
            goto addr_be82_3;
        zf13 = color_symlink_as_referent == 0;
        eax14 = *reinterpret_cast<unsigned char*>(rdi + 0xb9);
        if (zf13 || !*reinterpret_cast<signed char*>(&eax14)) {
            esi15 = *reinterpret_cast<void***>(r12_9 + 48);
        } else {
            esi15 = *reinterpret_cast<void***>(r12_9 + 0xac);
        }
    } else {
        r13_12 = *reinterpret_cast<void***>(rdi + 8);
        if (!*reinterpret_cast<signed char*>(&eax11)) 
            goto addr_be82_3;
        esi15 = *reinterpret_cast<void***>(rdi + 0xac);
        if (*reinterpret_cast<unsigned char*>(rdi + 0xb9)) 
            goto addr_c060_9; else 
            goto addr_bee3_10;
    }
    addr_bf24_11:
    ecx16 = *reinterpret_cast<unsigned char*>(r12_9 + 0xb8);
    if (*reinterpret_cast<unsigned char*>(&ecx16)) {
        addr_c073_12:
        edx17 = reinterpret_cast<unsigned char>(esi15) & 0xf000;
        if (edx17 == 0x8000) {
            if (!(reinterpret_cast<unsigned char>(esi15) & 0x800) || (v18 = reinterpret_cast<void**>(0xc1b2), al19 = is_colored(16), al19 == 0)) {
                if (!(reinterpret_cast<unsigned char>(esi15) & reinterpret_cast<uint32_t>("v")) || (v18 = reinterpret_cast<void**>(0xc202), al20 = is_colored(17), al20 == 0)) {
                    v18 = reinterpret_cast<void**>(0xc1d2);
                    al21 = is_colored(21);
                    if (!al21 || !*reinterpret_cast<signed char*>(r12_9 + 0xc0)) {
                        if (!(reinterpret_cast<unsigned char>(esi15) & 73) || (v18 = reinterpret_cast<void**>(0xc28f), al22 = is_colored(14), al22 == 0)) {
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 40)) <= reinterpret_cast<unsigned char>(1) || (v18 = reinterpret_cast<void**>(0xc2b6), al23 = is_colored(22), al23 == 0)) {
                                addr_c120_18:
                                v18 = reinterpret_cast<void**>(0xc128);
                                rax24 = fun_4860(r13_12);
                                rbx25 = color_ext_list;
                                rcx26 = rax24;
                                if (!rbx25) {
                                    addr_c171_19:
                                    *reinterpret_cast<int32_t*>(&rcx27) = 80;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                                    goto addr_bf8e_20;
                                } else {
                                    do {
                                        if (reinterpret_cast<unsigned char>(rcx26) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx25))) 
                                            continue;
                                        v28 = rcx26;
                                        v18 = reinterpret_cast<void**>(0xc15f);
                                        eax29 = c_strncasecmp();
                                        rcx26 = v28;
                                        if (!eax29) 
                                            break;
                                        rbx25 = *reinterpret_cast<void***>(rbx25 + 32);
                                    } while (rbx25);
                                    goto addr_c171_19;
                                }
                            } else {
                                *reinterpret_cast<int32_t*>(&rcx27) = 0x160;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                                goto addr_bf8e_20;
                            }
                        } else {
                            *reinterpret_cast<int32_t*>(&rcx27) = 0xe0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                            goto addr_bf8e_20;
                        }
                    } else {
                        *reinterpret_cast<int32_t*>(&rcx27) = 0x150;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                        goto addr_bf8e_20;
                    }
                } else {
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x110;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    goto addr_bf8e_20;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx27) = 0x100;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                goto addr_bf8e_20;
            }
            rcx30 = rbx25 + 16;
        } else {
            if (edx17 == 0x4000) {
                if ((reinterpret_cast<unsigned char>(esi15) & 0x202) == 0x202) {
                    v18 = reinterpret_cast<void**>(0xc2da);
                    al31 = is_colored(20);
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x140;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    if (al31) {
                        addr_bf8e_20:
                        rcx30 = reinterpret_cast<void**>(&rcx27->f25060);
                    } else {
                        goto addr_c222_35;
                    }
                } else {
                    addr_c222_35:
                    if ((!(*reinterpret_cast<unsigned char*>(&esi15) & 2) || (v18 = reinterpret_cast<void**>(0xc232), al32 = is_colored(19), *reinterpret_cast<int32_t*>(&rcx27) = 0x130, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, !al32)) && (*reinterpret_cast<int32_t*>(&rcx27) = 96, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, !!(reinterpret_cast<unsigned char>(esi15) & 0x200))) {
                        v18 = reinterpret_cast<void**>(0xc25a);
                        al33 = is_colored(18);
                        rcx34 = reinterpret_cast<void*>(96 - (96 + reinterpret_cast<uint1_t>(96 < 96 + reinterpret_cast<uint1_t>(al33 < 1))));
                        *reinterpret_cast<unsigned char*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx34) & 64);
                        rcx27 = reinterpret_cast<struct s13*>(reinterpret_cast<uint64_t>(rcx34) + 0x120);
                        goto addr_bf8e_20;
                    }
                }
            } else {
                if (edx17 == 0xa000) {
                    *reinterpret_cast<int32_t*>(&rdx35) = 7;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
                    goto addr_bf56_39;
                } else {
                    *reinterpret_cast<int32_t*>(&rcx27) = 0x80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                    if (edx17 != 0x1000 && ((*reinterpret_cast<int32_t*>(&rcx27) = 0x90, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, edx17 != 0xc000) && (*reinterpret_cast<int32_t*>(&rcx27) = 0xa0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0, edx17 != 0x6000))) {
                        *reinterpret_cast<int32_t*>(&rcx27) = 0xb0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
                        if (edx17 != 0x2000) {
                            rcx27 = reinterpret_cast<struct s13*>(0xd0);
                        }
                        goto addr_bf8e_20;
                    }
                }
            }
        }
    } else {
        addr_bf35_44:
        *reinterpret_cast<uint32_t*>(&rcx36) = *reinterpret_cast<uint32_t*>(r12_9 + 0xa8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx35) = *reinterpret_cast<int32_t*>("\r" + rcx36 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&ecx16) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx35) == 7);
        if (*reinterpret_cast<int32_t*>(&rdx35) == 5) 
            goto addr_c120_18; else 
            goto addr_bf56_39;
    }
    if (*reinterpret_cast<void***>(rcx30 + 8)) {
        addr_bfb3_46:
        r14d37 = reinterpret_cast<unsigned char>(r14d8) ^ 1;
        rdx38 = *reinterpret_cast<void***>(r12_9 + 0xc4);
        *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
        v39 = *reinterpret_cast<void***>(r12_9 + 16);
        *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<unsigned char*>(&r14d37);
        *reinterpret_cast<int32_t*>(&r8_40 + 4) = 0;
        rsi41 = filename_quoting_options;
        rax42 = quote_name(r13_12, rsi41, rdx38, rcx30, *reinterpret_cast<uint32_t*>(&r8_40), r15_7, v39);
        r12_43 = rax42;
        process_signals(r13_12, rsi41, rdx38, rcx30, r8_40, r15_7);
        zf44 = g25088 == 0;
        if (zf44) {
            put_indicator(0x25060, rsi41, v18, rcx30, r8_40, r15_7, v45, v28);
            put_indicator(0x25090, rsi41, v18, rcx30, r8_40, r15_7, v46, v28);
            put_indicator(0x25070, rsi41, v18, rcx30, r8_40, r15_7, v47, v28);
        } else {
            put_indicator(0x25080, rsi41, v18, rcx30, r8_40, r15_7, v48, v28);
        }
    } else {
        v18 = reinterpret_cast<void**>(0xbfa9);
        al49 = is_colored(4, 4);
        if (!al49) {
            addr_be82_3:
            rsi50 = filename_quoting_options;
            rdx51 = *reinterpret_cast<void***>(r12_9 + 0xc4);
            *reinterpret_cast<int32_t*>(&rdx51 + 4) = 0;
            v52 = *reinterpret_cast<void***>(r12_9 + 16);
            r14d53 = reinterpret_cast<unsigned char>(r14d8) ^ 1;
            *reinterpret_cast<uint32_t*>(&r8_54) = *reinterpret_cast<unsigned char*>(&r14d53);
            *reinterpret_cast<int32_t*>(&r8_54 + 4) = 0;
            rax55 = quote_name(r13_12, rsi50, rdx51, 0, *reinterpret_cast<uint32_t*>(&r8_54), r15_7, v52);
            r12_43 = rax55;
            process_signals(r13_12, rsi50, rdx51, 0, r8_54, r15_7);
            goto addr_beb9_50;
        } else {
            *reinterpret_cast<int32_t*>(&rcx30) = 0;
            *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
            goto addr_bfb3_46;
        }
    }
    rcx56 = line_length;
    if (rcx56 && (rsi57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) / reinterpret_cast<unsigned char>(rcx56)), rax58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_43) + reinterpret_cast<unsigned char>(rbp10) + 0xffffffffffffffff), rsi57 != reinterpret_cast<uint64_t>(rax58) / reinterpret_cast<unsigned char>(rcx56))) {
        put_indicator(0x251d0, rsi57, reinterpret_cast<uint64_t>(rax58) % reinterpret_cast<unsigned char>(rcx56), rcx56, r8_40, r15_7, v59, v28);
    }
    addr_beb9_50:
    return r12_43;
    addr_bf56_39:
    if (eax14 || !*reinterpret_cast<unsigned char*>(&ecx16)) {
        rcx27 = reinterpret_cast<struct s13*>(rdx35 << 4);
        goto addr_bf8e_20;
    } else {
        zf60 = color_symlink_as_referent == 0;
        *reinterpret_cast<int32_t*>(&rcx27) = 0xd0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
        if (zf60) {
            v18 = reinterpret_cast<void**>(0xbf7e);
            al61 = is_colored(13);
            rcx27 = reinterpret_cast<struct s13*>((0xd0 - (0xd0 + reinterpret_cast<uint1_t>(0xd0 < 0xd0 + reinterpret_cast<uint1_t>(al61 < 1))) & 0xffffffffffffffa0) + 0xd0);
            goto addr_bf8e_20;
        }
    }
    addr_c060_9:
    ecx16 = *reinterpret_cast<unsigned char*>(r12_9 + 0xb8);
    eax14 = 0;
    if (!*reinterpret_cast<unsigned char*>(&ecx16)) 
        goto addr_bf35_44; else 
        goto addr_c073_12;
    addr_bee3_10:
    v18 = reinterpret_cast<void**>(0xbeed);
    al62 = is_colored(12);
    if (al62) {
        *reinterpret_cast<int32_t*>(&rcx27) = 0xc0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
        goto addr_bf8e_20;
    } else {
        eax14 = 0xffffffff;
        goto addr_bf24_11;
    }
}

void** format_user_or_group(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    void** rax8;
    void** tmp64_9;
    uint32_t eax10;
    uint32_t ebx11;
    void** rax12;
    void** rdi13;
    void** tmp64_14;
    void** rax15;
    int1_t cf16;

    if (!rdi) {
        eax7 = fun_4b20(1, "%*lu ", rdx, rsi, r8);
        rax8 = reinterpret_cast<void**>(static_cast<int64_t>(eax7));
        tmp64_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(dired_pos) + reinterpret_cast<unsigned char>(rax8));
        dired_pos = tmp64_9;
        return rax8;
    } else {
        eax10 = gnu_mbswidth(rdi);
        ebx11 = *reinterpret_cast<int32_t*>(&rdx) - eax10;
        if (reinterpret_cast<int32_t>(ebx11) < reinterpret_cast<int32_t>(0)) {
            ebx11 = 0;
        }
        rax12 = fun_4860(rdi);
        dired_outbuf(rdi, rax12);
        do {
            rdi13 = stdout;
            tmp64_14 = dired_pos + 1;
            dired_pos = tmp64_14;
            rax15 = *reinterpret_cast<void***>(rdi13 + 40);
            if (reinterpret_cast<unsigned char>(rax15) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 48))) {
                rax15 = fun_48d0();
            } else {
                *reinterpret_cast<void***>(rdi13 + 40) = rax15 + 1;
                *reinterpret_cast<void***>(rax15) = reinterpret_cast<void**>(32);
            }
            cf16 = ebx11 < 1;
            --ebx11;
        } while (!cf16);
        return rax15;
    }
}

void** calculate_columns(int32_t edi) {
    void** rsi2;
    void** rax3;
    void** r12_4;
    int32_t ebp5;
    void** rdi6;
    void** rbx7;
    void** rcx8;
    void** rax9;
    void** r9_10;
    void** r8_11;
    void** rdi12;
    void** rbx13;
    void** rdi14;
    void** rcx15;
    void** rax16;
    void** rdx17;
    void** rcx18;
    void** rdx19;
    void** tmp64_20;
    void** rdx21;
    void** r8_22;
    void** r9_23;
    void** rax24;
    void** rdx25;
    void** rdi26;
    void** rdx27;
    int64_t* rax28;
    int64_t* rcx29;
    int64_t* rdx30;
    void** rdx31;
    signed char* rax32;
    void** rax33;
    void** rdi34;
    void** rax35;
    void** r10_36;
    void** r9_37;
    void** r11_38;
    void** rcx39;
    void** rdi40;
    void** r8_41;
    int64_t rax42;
    void*** rdx43;
    void** rax44;

    rsi2 = max_idx;
    rax3 = column_info_alloc_3;
    r12_4 = cwd_n_used;
    ebp5 = edi;
    if (!rsi2) {
        if (reinterpret_cast<unsigned char>(r12_4) > reinterpret_cast<unsigned char>(rax3)) {
            addr_7fb5_3:
            rdi6 = column_info;
            rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r12_4));
            rax9 = xreallocarray(rdi6, r12_4, 48, rcx8);
            column_info = rax9;
        } else {
            addr_81fc_4:
            r9_10 = r12_4;
            if (r12_4) {
                addr_806c_5:
                r8_11 = column_info;
                *reinterpret_cast<int32_t*>(&rsi2) = 3;
                *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi12) = 0;
                *reinterpret_cast<int32_t*>(&rdi12 + 4) = 0;
                goto addr_8080_6;
            } else {
                addr_8208_7:
                if (r9_10) {
                    addr_80c1_8:
                    *reinterpret_cast<int32_t*>(&rbx13) = 0;
                    *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                    goto addr_80c8_9;
                } else {
                    goto addr_81eb_11;
                }
            }
        }
    } else {
        if (reinterpret_cast<unsigned char>(rsi2) < reinterpret_cast<unsigned char>(r12_4)) {
            if (reinterpret_cast<unsigned char>(rsi2) <= reinterpret_cast<unsigned char>(rax3)) {
                r9_10 = r12_4;
                r12_4 = rsi2;
                goto addr_806c_5;
            }
            r12_4 = rsi2;
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi2) >> 1) > reinterpret_cast<unsigned char>(r12_4)) 
                goto addr_7fb5_3;
        } else {
            if (reinterpret_cast<unsigned char>(r12_4) <= reinterpret_cast<unsigned char>(rax3)) 
                goto addr_81fc_4;
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi2) >> 1) > reinterpret_cast<unsigned char>(r12_4)) 
                goto addr_7fb5_3;
        }
        rdi14 = column_info;
        rax16 = xreallocarray(rdi14, rsi2, 24, rcx15);
        rbx7 = max_idx;
        column_info = rax16;
    }
    rdx17 = column_info_alloc_3;
    *reinterpret_cast<int32_t*>(&rcx18) = 0;
    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
    rdx19 = rdx17 + 1;
    tmp64_20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx19) + reinterpret_cast<unsigned char>(rbx7));
    *reinterpret_cast<unsigned char*>(&rcx18) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_20) < reinterpret_cast<unsigned char>(rdx19));
    *reinterpret_cast<uint32_t*>(&rdx21) = __intrinsic();
    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
    if (rcx18 || rdx21) {
        xalloc_die();
    }
    *reinterpret_cast<int32_t*>(&rsi2) = 8;
    *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
    rax24 = xnmalloc(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx7) - reinterpret_cast<unsigned char>(rdx17)) * reinterpret_cast<unsigned char>(tmp64_20) >> 1, 8, rdx21, rcx18, r8_22, r9_23);
    rdx25 = column_info_alloc_3;
    if (reinterpret_cast<unsigned char>(rbx7) > reinterpret_cast<unsigned char>(rdx25)) {
        rdi26 = column_info;
        rdx27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx25) * 8 + 8);
        rsi2 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) * 8 + 8);
        do {
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi26) + reinterpret_cast<uint64_t>(rdx27 + reinterpret_cast<unsigned char>(rdx27) * 2) - 8) = rax24;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rdx27));
            rdx27 = rdx27 + 8;
        } while (rsi2 != rdx27);
    }
    column_info_alloc_3 = rbx7;
    r9_10 = cwd_n_used;
    if (!r12_4) 
        goto addr_8208_7; else 
        goto addr_806c_5;
    do {
        addr_8080_6:
        rax28 = *reinterpret_cast<int64_t**>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 8);
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 24) = 1;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_11 + reinterpret_cast<unsigned char>(rsi2) * 8) - 16) = rsi2;
        rcx29 = rax28 + reinterpret_cast<unsigned char>(rdi12);
        do {
            rdx30 = rax28;
            *rax28 = 3;
            ++rax28;
        } while (rcx29 != rdx30);
        ++rdi12;
        rsi2 = rsi2 + 3;
    } while (reinterpret_cast<unsigned char>(rdi12) < reinterpret_cast<unsigned char>(r12_4));
    if (r9_10) 
        goto addr_80c1_8;
    addr_81bd_28:
    if (reinterpret_cast<unsigned char>(r12_4) > reinterpret_cast<unsigned char>(1)) {
        rdx31 = column_info;
        rax32 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rdx31 + reinterpret_cast<uint64_t>(r12_4 + reinterpret_cast<unsigned char>(r12_4) * 2) * 8) - 24);
        do {
            if (*rax32) 
                break;
            --r12_4;
            rax32 = rax32 - 24;
        } while (r12_4 != 1);
    }
    addr_81eb_11:
    return r12_4;
    do {
        addr_80c8_9:
        rax33 = sorted_file;
        rdi34 = *reinterpret_cast<void***>(rax33 + reinterpret_cast<unsigned char>(rbx13) * 8);
        rax35 = length_of_file_name_and_frills(rdi34, rsi2);
        r10_36 = cwd_n_used;
        r9_37 = rax35;
        if (r12_4) {
            r11_38 = line_length;
            rsi2 = column_info;
            *reinterpret_cast<int32_t*>(&rcx39) = 0;
            *reinterpret_cast<int32_t*>(&rcx39 + 4) = 0;
            do {
                rdi40 = rcx39;
                ++rcx39;
                if (*reinterpret_cast<void***>(rsi2)) {
                    if (*reinterpret_cast<signed char*>(&ebp5)) {
                        r8_41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) / ((reinterpret_cast<unsigned char>(r10_36) + reinterpret_cast<unsigned char>(rcx39) + 0xffffffffffffffff) / reinterpret_cast<unsigned char>(rcx39)));
                    } else {
                        r8_41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) % reinterpret_cast<unsigned char>(rcx39));
                    }
                    *reinterpret_cast<int32_t*>(&rax42) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rax42) = reinterpret_cast<uint1_t>(r8_41 != rdi40);
                    rdx43 = reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi2 + 16) + reinterpret_cast<unsigned char>(r8_41) * 8);
                    rax44 = r9_37 + rax42 * 2;
                    if (reinterpret_cast<unsigned char>(*rdx43) < reinterpret_cast<unsigned char>(rax44)) {
                        *reinterpret_cast<void***>(rsi2 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi2 + 8)) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax44) - reinterpret_cast<unsigned char>(*rdx43)));
                        *rdx43 = rax44;
                        *reinterpret_cast<void***>(rsi2) = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi2 + 8)) < reinterpret_cast<unsigned char>(r11_38))));
                    }
                }
                rsi2 = rsi2 + 24;
            } while (r12_4 != rcx39);
        }
        ++rbx13;
    } while (reinterpret_cast<unsigned char>(rbx13) < reinterpret_cast<unsigned char>(r10_36));
    goto addr_81bd_28;
}

void** tabsize = reinterpret_cast<void**>(0);

void** indent(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rbx5;
    void** rdi6;
    void** rcx7;
    void** rbp8;
    void** rsi9;
    void** rax10;

    if (reinterpret_cast<unsigned char>(rdi) >= reinterpret_cast<unsigned char>(rsi)) {
        return rax3;
    } else {
        r12_4 = rsi;
        rbx5 = rdi;
        while (1) {
            rdi6 = stdout;
            rcx7 = tabsize;
            rbp8 = rbx5 + 1;
            rsi9 = *reinterpret_cast<void***>(rdi6 + 40);
            if (!rcx7 || reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_4) / reinterpret_cast<unsigned char>(rcx7)) <= reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp8) / reinterpret_cast<unsigned char>(rcx7))) {
                if (reinterpret_cast<unsigned char>(rsi9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 48))) {
                    rax10 = fun_48d0();
                } else {
                    rax10 = rsi9 + 1;
                    *reinterpret_cast<void***>(rdi6 + 40) = rax10;
                    *reinterpret_cast<void***>(rsi9) = reinterpret_cast<void**>(32);
                }
                rbx5 = rbp8;
                if (reinterpret_cast<unsigned char>(rbx5) >= reinterpret_cast<unsigned char>(r12_4)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rsi9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 48))) {
                    fun_48d0();
                    rcx7 = tabsize;
                } else {
                    *reinterpret_cast<void***>(rdi6 + 40) = rsi9 + 1;
                    *reinterpret_cast<void***>(rsi9) = reinterpret_cast<void**>(9);
                }
                rax10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx5) / reinterpret_cast<unsigned char>(rcx7));
                rbx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx7) + reinterpret_cast<unsigned char>(rbx5) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx5) % reinterpret_cast<unsigned char>(rcx7)));
                if (reinterpret_cast<unsigned char>(rbx5) >= reinterpret_cast<unsigned char>(r12_4)) 
                    break;
            }
        }
        return rax10;
    }
}

void*** sorted_file_alloc = reinterpret_cast<void***>(0);

void fun_4980(int64_t rdi, void** rsi, void** rdx);

void sort_files(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void** rbp7;
    int1_t below_or_equal8;
    void** rdi9;
    void** rax10;
    void** rax11;
    uint32_t eax12;
    int1_t zf13;
    void** rbx14;
    void** rax15;
    void** rbp16;
    void* rax17;
    void** rdi18;
    int1_t cf19;

    rbp7 = cwd_n_used;
    below_or_equal8 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp7) >> 1) + reinterpret_cast<unsigned char>(rbp7) <= reinterpret_cast<uint64_t>(sorted_file_alloc);
    if (!below_or_equal8) {
        rdi9 = sorted_file;
        fun_4630(rdi9);
        rsi = reinterpret_cast<void**>(24);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax10 = xnmalloc(rbp7, 24, rdx, rcx, r8, r9);
        rbp7 = cwd_n_used;
        sorted_file = rax10;
        sorted_file_alloc = reinterpret_cast<void***>(rbp7 + reinterpret_cast<unsigned char>(rbp7) * 2);
    }
    if (rbp7) {
        rax11 = sorted_file;
        rdx = cwd_file;
        rcx = rax11 + reinterpret_cast<unsigned char>(rbp7) * 8;
        do {
            *reinterpret_cast<void***>(rax11) = rdx;
            rax11 = rax11 + 8;
            rdx = rdx + 0xd0;
        } while (rax11 != rcx);
        eax12 = sort_type;
        if (eax12 == 2) 
            goto addr_8e9b_7; else 
            goto addr_8da2_8;
    }
    eax12 = sort_type;
    if (eax12 != 2) {
        addr_8da2_8:
        zf13 = line_length == 0;
        if (zf13 || ((rsi = format, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rdx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(rsi + 0xfffffffffffffffe))), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(1)) || !rbp7)) {
            addr_8db0_10:
            if (eax12 == 6) {
                return;
            }
        } else {
            addr_8e9b_7:
            *reinterpret_cast<int32_t*>(&rbx14) = 0;
            *reinterpret_cast<int32_t*>(&rbx14 + 4) = 0;
            goto addr_8eb4_12;
        }
    }
    fun_4980(0x25300, rsi, rdx);
    do {
        addr_8eb4_12:
        rax15 = sorted_file;
        rbp16 = *reinterpret_cast<void***>(rax15 + reinterpret_cast<unsigned char>(rbx14) * 8);
        rax17 = *reinterpret_cast<void**>(rbp16 + 0xc8);
        if (!rax17) {
            rdx = *reinterpret_cast<void***>(rbp16 + 0xc4);
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            rsi = filename_quoting_options;
            rdi18 = *reinterpret_cast<void***>(rbp16);
            rax17 = quote_name_width(rdi18, rsi, rdx, rcx, r8, r9);
        }
        ++rbx14;
        cf19 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(cwd_n_used);
        *reinterpret_cast<void**>(rbp16 + 0xc8) = rax17;
    } while (cf19);
    eax12 = sort_type;
    goto addr_8db0_10;
}

signed char gl_scratch_buffer_grow_preserve(void** rdi, void** rsi);

void** fun_4b50(void** rdi, void** rsi, void** rdx);

void** fun_47c0(void** rdi, void** rsi, void** rdx);

signed char gl_scratch_buffer_grow(void** rdi, void** rsi);

int32_t fun_4a20(void** rdi, void** rsi);

void** hash_initialize(int64_t rdi);

signed char seen_file(void** rdi, void** rsi);

void fun_4b60(void** rdi, void** rsi);

void** fun_4a30(void** rdi, void** rsi, void** rdx, void** rcx);

void record_file(void** rdi, void** rsi);

int32_t fun_47a0(int64_t rdi, void** rsi);

void* fun_4950(void** rdi, void** rsi);

uint16_t dir_suffix = 47;

void** gl_scratch_buffer_dupfree(void** rdi);

void hash_free(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_4940(void** rdi, void** rsi);

void** fun_49a0(void** rdi);

void** canonicalize_filename_mode_stk(void** rdi, uint32_t esi, void** rdx) {
    int64_t r14_4;
    void*** rsp5;
    void* rax6;
    void* v7;
    void** rbp8;
    void*** rax9;
    void** rax10;
    void*** rax11;
    void** rax12;
    void** rbx13;
    void** r12_14;
    void** v15;
    void** rsi16;
    void** v17;
    void** rax18;
    void** v19;
    void** v20;
    void** rax21;
    int1_t zf22;
    void** r13_23;
    void** v24;
    void** r15_25;
    uint32_t eax26;
    void** rcx27;
    void** v28;
    uint32_t v29;
    signed char v30;
    int32_t v31;
    void** v32;
    void** v33;
    void** v34;
    void** v35;
    void** rcx36;
    void** r13_37;
    signed char al38;
    void** rax39;
    void** v40;
    void** r13_41;
    void** v42;
    void** v43;
    void** rax44;
    signed char al45;
    void** r10_46;
    void** r9_47;
    void*** rax48;
    void** rdx49;
    void** rsi50;
    void** rax51;
    void** r9_52;
    void** r8_53;
    void* v54;
    void** r10_55;
    void** rax56;
    void** rax57;
    void** r11_58;
    void** rdi59;
    int32_t eax60;
    void** r11_61;
    uint32_t eax62;
    void** r10_63;
    void** r9_64;
    void** rax65;
    signed char al66;
    void** v67;
    void** v68;
    void** v69;
    signed char al70;
    void** rax71;
    signed char v72;
    int32_t eax73;
    void*** rax74;
    void* rax75;
    uint32_t edx76;
    uint32_t eax77;
    uint64_t rax78;
    void*** rax79;
    int64_t rax80;
    void* rsp81;
    void*** rax82;
    signed char al83;
    void* rdx84;
    void** rdi85;
    void** rax86;

    *reinterpret_cast<uint32_t*>(&r14_4) = esi & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_4) + 4) = 0;
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x928);
    rax6 = g28;
    v7 = rax6;
    if (static_cast<uint32_t>(r14_4 - 1) & *reinterpret_cast<uint32_t*>(&r14_4) || (rbp8 = rdi, rdi == 0)) {
        rax9 = fun_46f0();
        *rax9 = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi)) {
            rax11 = fun_46f0();
            *rax11 = reinterpret_cast<void**>(2);
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
        } else {
            rax12 = reinterpret_cast<void**>(rsp5 + 0x100);
            *reinterpret_cast<uint32_t*>(&rbx13) = esi;
            *reinterpret_cast<void***>(rdx + 8) = reinterpret_cast<void**>("v");
            r12_14 = rdx;
            v15 = rax12;
            *reinterpret_cast<int32_t*>(&rsi16) = reinterpret_cast<int32_t>("v");
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            v17 = rax12;
            rax18 = reinterpret_cast<void**>(rsp5 + 0x510);
            v19 = rax18;
            v20 = rax18;
            rax21 = rdx + 16;
            *reinterpret_cast<void***>(rdx) = rax21;
            zf22 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 47);
            r13_23 = rax21;
            v24 = rax21;
            if (zf22) {
                while (1) {
                    *reinterpret_cast<void***>(rdx + 16) = reinterpret_cast<void**>(47);
                    r15_25 = v24;
                    r13_23 = rdx + 17;
                    addr_ee2d_7:
                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                    if (!*reinterpret_cast<signed char*>(&eax26)) {
                        if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25 + 1) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47)) {
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        } else {
                            --r13_23;
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        }
                    } else {
                        rcx27 = reinterpret_cast<void**>(rsp5 + 0x500);
                        v28 = reinterpret_cast<void**>(0);
                        v29 = *reinterpret_cast<uint32_t*>(&rbx13) & 4;
                        v30 = 0;
                        v31 = 0;
                        v32 = rcx27;
                        while (1) {
                            if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                v33 = rbp8;
                                *reinterpret_cast<uint32_t*>(&rdx) = eax26;
                                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            } else {
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                    ++rbp8;
                                } while (*reinterpret_cast<signed char*>(&rdx) == 47);
                                if (!*reinterpret_cast<signed char*>(&rdx)) 
                                    goto addr_ef10_17;
                                v33 = rbp8;
                            }
                            do {
                                rbx13 = rbp8;
                                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                ++rbp8;
                                if (!*reinterpret_cast<signed char*>(&eax26)) 
                                    break;
                            } while (*reinterpret_cast<signed char*>(&eax26) != 47);
                            rcx27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v33));
                            v34 = rcx27;
                            if (!rcx27) 
                                goto addr_ef10_17;
                            if (rcx27 != 1) {
                                if (!reinterpret_cast<int1_t>(v34 == 2)) 
                                    goto addr_eda9_24;
                                if (*reinterpret_cast<signed char*>(&rdx) != 46) 
                                    goto addr_eda9_24;
                                rcx27 = v33;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx27 + 1) == 46)) 
                                    goto addr_eda9_24;
                                rdx = r15_25 + 1;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(rdx)) 
                                    goto addr_ef08_28;
                                --r13_23;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25)) 
                                    goto addr_ef08_28;
                                do {
                                    if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47) 
                                        goto addr_ef08_28;
                                    --r13_23;
                                } while (r13_23 != r15_25);
                                goto addr_ef08_28;
                            }
                            if (*reinterpret_cast<signed char*>(&rdx) == 46) {
                                addr_ef08_28:
                                if (*reinterpret_cast<signed char*>(&eax26)) 
                                    continue; else 
                                    goto addr_ef10_17;
                            } else {
                                addr_eda9_24:
                                if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) != 47) {
                                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(47);
                                    ++r13_23;
                                }
                            }
                            rsi16 = v34 + 2;
                            if (reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_14 + 8)) + reinterpret_cast<unsigned char>(r15_25)) - reinterpret_cast<unsigned char>(r13_23)) < reinterpret_cast<unsigned char>(rsi16)) {
                                v35 = rbx13;
                                rcx36 = r13_23;
                                rbx13 = rsi16;
                                r13_37 = r12_14;
                                r12_14 = rbp8;
                                do {
                                    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx36) - reinterpret_cast<unsigned char>(r15_25));
                                    al38 = gl_scratch_buffer_grow_preserve(r13_37, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (!al38) 
                                        goto addr_ee1a_38;
                                    r15_25 = *reinterpret_cast<void***>(r13_37);
                                    rcx36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) + reinterpret_cast<unsigned char>(rbp8));
                                } while (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37 + 8)) - reinterpret_cast<unsigned char>(rbp8)) < reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = v35;
                                rbp8 = r12_14;
                                r12_14 = r13_37;
                                r13_23 = rcx36;
                            }
                            rdx = v34;
                            rsi16 = v33;
                            rax39 = fun_4b50(r13_23, rsi16, rdx);
                            rsp5 = rsp5 - 8 + 8;
                            *reinterpret_cast<uint32_t*>(&rcx27) = v29;
                            *reinterpret_cast<int32_t*>(&rcx27 + 4) = 0;
                            *reinterpret_cast<void***>(rax39) = reinterpret_cast<void**>(0);
                            r13_23 = rax39;
                            if (!*reinterpret_cast<uint32_t*>(&rcx27)) {
                                v40 = rax39;
                                r13_41 = v32;
                                v42 = rbx13;
                                v43 = rbp8;
                                do {
                                    rbx13 = v20;
                                    rbp8 = reinterpret_cast<void**>(0x3ff);
                                    rsi16 = rbx13;
                                    rdx = reinterpret_cast<void**>(0x3ff);
                                    rax44 = fun_47c0(r15_25, rsi16, 0x3ff);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (reinterpret_cast<signed char>(0x3ff) > reinterpret_cast<signed char>(rax44)) 
                                        break;
                                    al45 = gl_scratch_buffer_grow(r13_41, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                } while (al45);
                                goto addr_f113_45;
                                r10_46 = rbx13;
                                r13_23 = v40;
                                rbx13 = v42;
                                r9_47 = rax44;
                                rbp8 = v43;
                                if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) 
                                    goto addr_efb2_47;
                            } else {
                                addr_efb2_47:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) {
                                    addr_f030_48:
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1));
                                    if (*reinterpret_cast<signed char*>(&eax26)) 
                                        continue; else 
                                        goto addr_f03c_49;
                                } else {
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                                    if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                        addr_f070_51:
                                        *reinterpret_cast<uint32_t*>(&rdx) = v29;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx)) {
                                            if (*reinterpret_cast<signed char*>(&eax26)) 
                                                continue; else 
                                                goto addr_f014_53;
                                        } else {
                                            rax48 = fun_46f0();
                                            rsp5 = rsp5 - 8 + 8;
                                            if (*rax48 == 22) 
                                                goto addr_f030_48; else 
                                                goto addr_f082_55;
                                        }
                                    } else {
                                        rdx49 = rbp8;
                                        while (1) {
                                            rsi50 = rdx49;
                                            *reinterpret_cast<uint32_t*>(&rcx27) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx49 + 1));
                                            *reinterpret_cast<int32_t*>(&rcx27 + 4) = 0;
                                            ++rdx49;
                                            if (*reinterpret_cast<signed char*>(&rcx27) == 47) 
                                                continue;
                                            rsi16 = rsi50 + 2;
                                            if (!*reinterpret_cast<signed char*>(&rcx27)) 
                                                goto addr_f118_59;
                                            if (*reinterpret_cast<signed char*>(&rcx27) != 46) 
                                                goto addr_f070_51;
                                            *reinterpret_cast<uint32_t*>(&rcx27) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx49 + 1));
                                            *reinterpret_cast<int32_t*>(&rcx27 + 4) = 0;
                                            if (!*reinterpret_cast<signed char*>(&rcx27)) 
                                                goto addr_f118_59;
                                            if (*reinterpret_cast<signed char*>(&rcx27) == 46) 
                                                goto addr_f054_63;
                                            if (*reinterpret_cast<signed char*>(&rcx27) != 47) 
                                                goto addr_f070_51;
                                            rdx49 = rsi16;
                                        }
                                    }
                                }
                            }
                            if (v31 <= 19) {
                                ++v31;
                                goto addr_f22c_68;
                            }
                            if (!*reinterpret_cast<void***>(v33)) {
                                addr_f22c_68:
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_46) + reinterpret_cast<unsigned char>(r9_47)) = 0;
                                if (!v30) {
                                    rax51 = fun_4860(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_52 = r9_47;
                                    r8_53 = v17;
                                    v54 = reinterpret_cast<void*>(0);
                                    r10_55 = r10_46;
                                    rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax51) + reinterpret_cast<unsigned char>(r9_52));
                                    if (reinterpret_cast<unsigned char>(rax56) < reinterpret_cast<unsigned char>("v")) 
                                        goto addr_f352_71;
                                } else {
                                    v54 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v17));
                                    rax57 = fun_4860(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_52 = r9_47;
                                    r8_53 = v17;
                                    r10_55 = r10_46;
                                    rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<unsigned char>(r9_52));
                                    if (reinterpret_cast<unsigned char>("v") > reinterpret_cast<unsigned char>(rax56)) 
                                        goto addr_f3f7_73;
                                }
                            } else {
                                r11_58 = reinterpret_cast<void**>(rsp5 + 96);
                                rdi59 = reinterpret_cast<void**>(".");
                                rsi16 = r11_58;
                                rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v33) - reinterpret_cast<unsigned char>(rbp8)) + reinterpret_cast<unsigned char>(r13_23));
                                *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
                                if (*reinterpret_cast<void***>(r15_25)) {
                                    rdi59 = r15_25;
                                }
                                eax60 = fun_4a20(rdi59, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (eax60) 
                                    goto addr_f0a8_77;
                                r11_61 = r11_58;
                                eax62 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v33));
                                r10_63 = r10_46;
                                r9_64 = r9_47;
                                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax62);
                                if (v28) 
                                    goto addr_f1ce_79;
                                r8_53 = reinterpret_cast<void**>(0x11290);
                                rcx27 = reinterpret_cast<void**>(0x11250);
                                *reinterpret_cast<int32_t*>(&rsi16) = 0;
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                rdx = reinterpret_cast<void**>(0x11220);
                                rax65 = hash_initialize(7);
                                rsp5 = rsp5 - 8 + 8;
                                r10_63 = r10_63;
                                r9_64 = r9_64;
                                v28 = rax65;
                                r11_61 = r11_61;
                                if (!rax65) 
                                    goto addr_f451_81;
                                addr_f1ce_79:
                                rsi16 = v33;
                                rdx = r11_61;
                                al66 = seen_file(v28, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (al66) 
                                    goto addr_f2ec_82; else 
                                    goto addr_f206_83;
                            }
                            v67 = rbp8;
                            v68 = r10_55;
                            rbp8 = reinterpret_cast<void**>(rsp5 + 0xf0);
                            rbx13 = rax56;
                            v69 = r9_52;
                            do {
                                al70 = gl_scratch_buffer_grow_preserve(rbp8, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (!al70) 
                                    goto addr_f2cc_86;
                                r8_53 = v17;
                            } while (reinterpret_cast<unsigned char>("v") <= reinterpret_cast<unsigned char>(rbx13));
                            r10_55 = v68;
                            r9_52 = v69;
                            rbp8 = v67;
                            if (!v30) {
                                addr_f352_71:
                                fun_4b60(reinterpret_cast<unsigned char>(r8_53) + reinterpret_cast<unsigned char>(r9_52), rbp8);
                                rsi16 = r10_55;
                                rax71 = fun_4a30(r8_53, rsi16, r9_52, rcx27);
                                rsp5 = rsp5 - 8 + 8 - 8 + 8;
                                rdx = r15_25 + 1;
                                rbp8 = rax71;
                                if (v72 == 47) {
                                    *reinterpret_cast<void***>(r15_25) = reinterpret_cast<void**>(47);
                                    r13_23 = rdx;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax71));
                                    v30 = 1;
                                    goto addr_ef08_28;
                                } else {
                                    v30 = 1;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax71));
                                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(rdx)) {
                                        do {
                                            --r13_23;
                                            if (r13_23 == r15_25) 
                                                break;
                                        } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47));
                                        v30 = 1;
                                        goto addr_ef08_28;
                                    }
                                }
                            } else {
                                addr_f3f7_73:
                                rbp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v54) + reinterpret_cast<unsigned char>(r8_53));
                                goto addr_f352_71;
                            }
                            addr_f2ec_82:
                            if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) 
                                goto addr_f030_48; else 
                                goto addr_f2f6_94;
                            addr_f206_83:
                            rsi16 = v33;
                            rdx = r11_61;
                            record_file(v28, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            r10_46 = r10_63;
                            r9_47 = r9_64;
                            goto addr_f22c_68;
                            addr_f014_53:
                            *reinterpret_cast<uint32_t*>(&rdx) = 0;
                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rcx27) = 0x200;
                            *reinterpret_cast<int32_t*>(&rcx27 + 4) = 0;
                            rsi16 = r15_25;
                            eax73 = fun_47a0(0xffffff9c, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            if (eax73) {
                                addr_f082_55:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) != 1) 
                                    goto addr_f0a8_77;
                                rax74 = fun_46f0();
                                rsp5 = rsp5 - 8 + 8;
                                if (!reinterpret_cast<int1_t>(*rax74 == 2)) 
                                    goto addr_f0a8_77;
                                rsi16 = reinterpret_cast<void**>("/");
                                rax75 = fun_4950(rbp8, "/");
                                rsp5 = rsp5 - 8 + 8;
                                if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(rax75))) 
                                    goto addr_f030_48; else 
                                    goto addr_f0a8_77;
                            } else {
                                goto addr_f030_48;
                            }
                            addr_f054_63:
                            edx76 = *reinterpret_cast<unsigned char*>(rdx49 + 2);
                            if (!*reinterpret_cast<signed char*>(&edx76) || *reinterpret_cast<signed char*>(&edx76) == 47) {
                                addr_f118_59:
                                eax77 = dir_suffix;
                                *reinterpret_cast<void***>(r13_23) = *reinterpret_cast<void***>(&eax77);
                                goto addr_f014_53;
                            } else {
                                goto addr_f070_51;
                            }
                        }
                    }
                    addr_ecfa_99:
                    if (v17 != v15) {
                        fun_4630(v17, v17);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (v20 != v19) {
                        fun_4630(v20, v20);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx13)) 
                        goto addr_ef40_104;
                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(0);
                    rsi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23 + 1) - reinterpret_cast<unsigned char>(r15_25));
                    rax10 = gl_scratch_buffer_dupfree(r12_14);
                    rsp5 = rsp5 - 8 + 8;
                    if (rax10) 
                        break;
                    addr_ee1a_38:
                    xalloc_die();
                    rsp5 = rsp5 - 8 + 8;
                    continue;
                    addr_ef10_17:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(r15_25 + 1)) {
                        *reinterpret_cast<int32_t*>(&rax78) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47);
                        r13_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23) - rax78);
                    }
                    addr_ef28_107:
                    if (v28) {
                        hash_free(v28, rsi16, rdx, rcx27, r8_53);
                        rsp5 = rsp5 - 8 + 8;
                        goto addr_ecfa_99;
                    }
                    addr_f113_45:
                    goto addr_ee1a_38;
                    addr_f03c_49:
                    goto addr_ef10_17;
                    addr_f0a8_77:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
                    goto addr_ef28_107;
                    addr_f2cc_86:
                    goto addr_ee1a_38;
                    addr_f451_81:
                    goto addr_ee1a_38;
                    addr_f2f6_94:
                    rax79 = fun_46f0();
                    rsp5 = rsp5 - 8 + 8;
                    *rax79 = reinterpret_cast<void**>(40);
                    goto addr_f0a8_77;
                }
            } else {
                while (rax80 = fun_4940(r13_23, rsi16), rsp81 = reinterpret_cast<void*>(rsp5 - 8 + 8), !rax80) {
                    rax82 = fun_46f0();
                    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
                    if (*rax82 == 12) 
                        goto addr_ee1a_38;
                    if (*rax82 != 34) 
                        goto addr_ecf2_112;
                    al83 = gl_scratch_buffer_grow(r12_14, rsi16);
                    rsp5 = rsp5 - 8 + 8;
                    if (!al83) 
                        goto addr_ee1a_38;
                    r13_23 = *reinterpret_cast<void***>(r12_14);
                    rsi16 = *reinterpret_cast<void***>(r12_14 + 8);
                }
                goto addr_f0b2_115;
            }
        }
    }
    addr_ed46_116:
    rdx84 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v7) - reinterpret_cast<uint64_t>(g28));
    if (rdx84) {
        fun_4870();
    } else {
        return rax10;
    }
    addr_ef40_104:
    rdi85 = *reinterpret_cast<void***>(r12_14);
    *reinterpret_cast<int32_t*>(&rax10) = 0;
    *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    if (v24 != rdi85) {
        fun_4630(rdi85, rdi85);
        rax10 = reinterpret_cast<void**>(0);
        goto addr_ed46_116;
    }
    addr_f0b2_115:
    *reinterpret_cast<int32_t*>(&rsi16) = 0;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    r15_25 = r13_23;
    rax86 = fun_49a0(r13_23);
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
    r13_23 = rax86;
    goto addr_ee2d_7;
    addr_ecf2_112:
    r15_25 = r13_23;
    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
    goto addr_ecfa_99;
}

struct s14 {
    unsigned char f0;
    signed char f1;
    signed char f2;
};

uint64_t file_prefixlen(struct s14* rdi, uint64_t* rsi) {
    uint64_t r8_3;
    uint64_t r10_4;
    uint64_t rcx5;
    uint64_t rdx6;
    int64_t rax7;
    int32_t r9d8;
    int64_t rax9;
    int32_t ecx10;
    uint32_t eax11;

    r8_3 = *rsi;
    *reinterpret_cast<int32_t*>(&r10_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_4) + 4) = 0;
    while (1) {
        rcx5 = r10_4 + 1;
        rdx6 = r10_4;
        if (rcx5 >= r8_3) {
            addr_faba_3:
            if (reinterpret_cast<int64_t>(r8_3) >= reinterpret_cast<int64_t>(0)) {
                addr_fa60_4:
                if (r8_3 == rdx6) 
                    break;
            } else {
                if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi) + rdx6)) 
                    break;
            }
        } else {
            while (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi) + rdx6) == 46) {
                *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) + rdx6 + 1);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rax7) <= 90) {
                    if (*reinterpret_cast<signed char*>(&rax7) <= 64) 
                        goto addr_fa54_10;
                } else {
                    r9d8 = static_cast<int32_t>(rax7 - 97);
                    if (*reinterpret_cast<unsigned char*>(&r9d8) <= 25) 
                        goto addr_fa95_12;
                    if (*reinterpret_cast<signed char*>(&rax7) != 0x7e) 
                        goto addr_fa54_10;
                }
                addr_fa95_12:
                rdx6 = rdx6 + 2;
                if (r8_3 <= rdx6) {
                    addr_fab1_14:
                    rcx5 = rdx6 + 1;
                    if (rcx5 < r8_3) 
                        continue; else 
                        break;
                } else {
                    do {
                        *reinterpret_cast<uint32_t*>(&rax9) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) + rdx6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rax9) > 90) {
                            ecx10 = static_cast<int32_t>(rax9 - 97);
                            if (*reinterpret_cast<unsigned char*>(&ecx10) > 25) {
                                if (*reinterpret_cast<signed char*>(&rax9) != 0x7e) 
                                    goto addr_fab1_14;
                            }
                        } else {
                            if (*reinterpret_cast<signed char*>(&rax9) > 64) 
                                continue;
                            eax11 = *reinterpret_cast<uint32_t*>(&rax9) - 48;
                            if (*reinterpret_cast<unsigned char*>(&eax11) > 9) 
                                goto addr_fab1_14;
                        }
                        ++rdx6;
                    } while (r8_3 > rdx6);
                }
                goto addr_fab1_14;
            }
            goto addr_faba_3;
        }
        r10_4 = rcx5;
        continue;
        addr_fa54_10:
        if (reinterpret_cast<int64_t>(r8_3) >= reinterpret_cast<int64_t>(0)) {
            goto addr_fa60_4;
        }
    }
    *rsi = rdx6;
    return r10_4;
}

int64_t verrevcmp(struct s14* rdi, uint64_t rsi, struct s14* rdx, uint64_t rcx) {
    struct s14* r8_5;
    uint64_t rax6;
    struct s14* rdi7;
    uint64_t rdx8;
    int64_t rax9;
    int32_t r9d10;
    int64_t rax11;
    uint32_t ebx12;
    uint32_t r10d13;
    int64_t r9_14;
    uint32_t r10d15;
    int64_t r9_16;
    int64_t r11_17;
    int64_t r10_18;
    int32_t r11d19;
    int64_t r10_20;
    int64_t r11_21;
    int32_t ebp22;

    r8_5 = rdi;
    *reinterpret_cast<int32_t*>(&rax6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rdi7 = rdx;
    *reinterpret_cast<int32_t*>(&rdx8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
    goto addr_fafc_2;
    addr_fd46_3:
    *reinterpret_cast<int32_t*>(&rax9) = r9d10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
    addr_fd50_4:
    return 0xffffffff;
    addr_fd3f_5:
    addr_fd40_6:
    r9d10 = 1;
    goto addr_fd46_3;
    addr_fd62_7:
    *reinterpret_cast<uint32_t*>(&rax11) = ebx12 - r10d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    return rax11;
    addr_fd85_8:
    r10d13 = 0;
    goto addr_fd62_7;
    addr_fd5c_9:
    r10d13 = 0xffffffff;
    goto addr_fd62_7;
    addr_fc71_10:
    r9d10 = 0;
    goto addr_fd46_3;
    while (1) {
        *reinterpret_cast<uint32_t*>(&r9_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&r9_14) - 48 <= 9) {
            while (1) {
                if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx) || (r10d15 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6))), *reinterpret_cast<uint32_t*>(&r9_16) = r10d15, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0, r10d15 - 48 <= 9)) {
                    if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) {
                        do {
                            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8) != 48) 
                                break;
                            ++rdx8;
                        } while (rsi != rdx8);
                        goto addr_fd10_16;
                    } else {
                        goto addr_fd10_16;
                    }
                } else {
                    if (rdx8 == rsi) {
                        *reinterpret_cast<uint32_t*>(&r11_17) = *reinterpret_cast<unsigned char*>(&r9_16);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_17) + 4) = 0;
                        ebx12 = 0xffffffff;
                        if (static_cast<uint32_t>(r11_17 - 48) > 9) 
                            goto addr_fb6e_20; else 
                            goto addr_fd85_8;
                    } else {
                        *reinterpret_cast<uint32_t*>(&r10_18) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_18) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r9_14) = *reinterpret_cast<uint32_t*>(&r10_18);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
                        if (static_cast<uint32_t>(r10_18 - 48) > 9) {
                            addr_fb2c_22:
                            ebx12 = *reinterpret_cast<uint32_t*>(&r10_18);
                            if (*reinterpret_cast<signed char*>(&r9_14) > reinterpret_cast<signed char>(90)) {
                                r11d19 = static_cast<int32_t>(r9_14 - 97);
                                if (*reinterpret_cast<unsigned char*>(&r11d19) <= 25) {
                                    addr_fb50_24:
                                    if (rax6 == rcx) 
                                        goto addr_fd5c_9; else 
                                        goto addr_fb59_25;
                                } else {
                                    goto addr_fb3f_27;
                                }
                            } else {
                                if (*reinterpret_cast<signed char*>(&r9_14) > reinterpret_cast<signed char>(64)) 
                                    goto addr_fb50_24; else 
                                    goto addr_fb3f_27;
                            }
                        } else {
                            ebx12 = 0;
                            goto addr_fb59_25;
                        }
                    }
                }
                if (reinterpret_cast<int64_t>(rax6) < reinterpret_cast<int64_t>(rcx)) {
                    do {
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6) != 48) 
                            break;
                        ++rax6;
                    } while (rcx != rax6);
                    goto addr_fd20_33;
                } else {
                    goto addr_fd20_33;
                }
                r9d10 = 0;
                if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) {
                    addr_fd9f_36:
                    if (reinterpret_cast<int64_t>(rsi) <= reinterpret_cast<int64_t>(rdx8)) {
                        addr_fc3b_37:
                        if (reinterpret_cast<int64_t>(rcx) <= reinterpret_cast<int64_t>(rax6)) 
                            goto addr_fc53_38;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6) - 48 <= 9) 
                            goto addr_fd50_4;
                    } else {
                        goto addr_fd2c_41;
                    }
                } else {
                    do {
                        if (reinterpret_cast<int64_t>(rcx) <= reinterpret_cast<int64_t>(rax6)) 
                            goto addr_fd9f_36;
                        *reinterpret_cast<int32_t*>(&r10_20) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_20) + 4) = 0;
                        if (static_cast<uint32_t>(r10_20 - 48) > 9) 
                            goto addr_fc3b_37;
                        *reinterpret_cast<int32_t*>(&r11_21) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_21) + 4) = 0;
                        if (static_cast<uint32_t>(r11_21 - 48) > 9) 
                            goto addr_fd40_6;
                        if (!r9d10) {
                            r9d10 = *reinterpret_cast<int32_t*>(&r10_20) - *reinterpret_cast<int32_t*>(&r11_21);
                        }
                        ++rdx8;
                        ++rax6;
                    } while (reinterpret_cast<int64_t>(rsi) > reinterpret_cast<int64_t>(rdx8));
                    goto addr_fd09_48;
                }
                addr_fc53_38:
                if (r9d10) 
                    goto addr_fd46_3;
                if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) 
                    break;
                goto addr_fc68_51;
                addr_fd2c_41:
                if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8) - 48 > 9) 
                    goto addr_fc3b_37; else 
                    goto addr_fd3f_5;
                addr_fd09_48:
                goto addr_fc3b_37;
                addr_fd20_33:
                if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) {
                    addr_fc68_51:
                    if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx)) 
                        goto addr_fc71_10;
                } else {
                    r9d10 = 0;
                    goto addr_fd2c_41;
                }
                addr_fba3_53:
                if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) 
                    break; else 
                    continue;
                addr_fd10_16:
                if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx)) {
                    addr_fafc_2:
                    if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) 
                        goto addr_fc68_51; else 
                        break;
                }
                addr_fb6e_20:
                r10d13 = *reinterpret_cast<uint32_t*>(&r11_17);
                if (*reinterpret_cast<signed char*>(&r9_16) > reinterpret_cast<signed char>(90)) {
                    ebp22 = static_cast<int32_t>(r9_16 - 97);
                    if (*reinterpret_cast<unsigned char*>(&ebp22) <= 25) {
                        addr_fb92_56:
                        if (r10d13 != ebx12) 
                            goto addr_fd62_7;
                    } else {
                        goto addr_fb81_58;
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&r9_16) > reinterpret_cast<signed char>(64)) 
                        goto addr_fb92_56; else 
                        goto addr_fb81_58;
                }
                ++rdx8;
                ++rax6;
                goto addr_fba3_53;
                addr_fb81_58:
                if (*reinterpret_cast<unsigned char*>(&r9_16) == 0x7e) {
                    r10d13 = 0xfffffffe;
                    goto addr_fb92_56;
                } else {
                    r10d13 = static_cast<uint32_t>(r11_17 + 0x100);
                    goto addr_fb92_56;
                }
                addr_fb59_25:
                *reinterpret_cast<uint32_t*>(&r9_16) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi7) + rax6);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0;
                r10d13 = 0;
                *reinterpret_cast<uint32_t*>(&r11_17) = *reinterpret_cast<unsigned char*>(&r9_16);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_17) + 4) = 0;
                if (static_cast<uint32_t>(r11_17 - 48) <= 9) 
                    goto addr_fb92_56; else 
                    goto addr_fb6e_20;
                addr_fb3f_27:
                if (*reinterpret_cast<unsigned char*>(&r9_14) == 0x7e) {
                    ebx12 = 0xfffffffe;
                    goto addr_fb50_24;
                } else {
                    ebx12 = static_cast<uint32_t>(r10_18 + 0x100);
                    goto addr_fb50_24;
                }
            }
        } else {
            *reinterpret_cast<uint32_t*>(&r10_18) = *reinterpret_cast<unsigned char*>(&r9_14);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_18) + 4) = 0;
            ebx12 = 0;
            if (static_cast<uint32_t>(r10_18 - 48) <= 9) 
                goto addr_fb50_24; else 
                goto addr_fb2c_22;
        }
    }
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x1e310);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x1e310);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x1e310) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0xe288]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x1e310);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0xe0ef]");
        if (1) 
            goto addr_1032c_6;
        __asm__("comiss xmm1, [rip+0xe0e6]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0xe0d8]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_1030c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_10302_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_1030c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_10302_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_10302_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_1030c_13:
        return 0;
    } else {
        addr_1032c_6:
        return r8_3;
    }
}

struct s16 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s15 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s16* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s17 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s18 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_4670(void** rdi, void** rsi, ...);

int32_t transfer_entries(struct s15* rdi, struct s15* rsi, int32_t edx) {
    struct s15* r14_4;
    int32_t r12d5;
    struct s15* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rax12;
    struct s17* rax13;
    void** rdx14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s18* r13_18;
    void** rax19;
    void** rdx20;

    r14_4 = rdi;
    r12d5 = edx;
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_10396_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_10388_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_10396_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rax12 = reinterpret_cast<void**>(r14_4->f30(r15_11, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax13 = reinterpret_cast<struct s17*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx14 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax13->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax13->f8;
                            rax13->f8 = r13_9;
                            if (!rdx14) 
                                goto addr_1040e_9;
                        } else {
                            rax13->f0 = r15_11;
                            rax15 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            r14_4->f48 = r13_9;
                            if (!rdx14) 
                                goto addr_1040e_9;
                        }
                        r13_9 = rdx14;
                    }
                    goto addr_4ca0_12;
                    addr_1040e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_10388_3;
            }
            rsi16 = r14_4->f10;
            rax17 = reinterpret_cast<void**>(r14_4->f30(r15_8, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_4ca0_12;
            r13_18 = reinterpret_cast<struct s18*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_18->f0) 
                goto addr_10448_16;
            r13_18->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_10448_16:
            rax19 = r14_4->f48;
            if (!rax19) {
                rax19 = fun_4670(16, rsi16);
                if (!rax19) 
                    goto addr_104ba_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx20 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx20;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_4ca0_12:
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    addr_104ba_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void*** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *rdx = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_101bf_24:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_10164_27;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<int32_t*>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_101d0_31;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<int32_t*>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_101d0_31;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_101bf_24;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_10164_27;
            }
        }
    }
    addr_101c1_35:
    return rax11;
    addr_10164_27:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_101c1_35;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_101d0_31:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

void** fun_4b40();

void fun_4920(void** rdi, ...);

uint32_t** fun_4690();

uint32_t** fun_4c60(void** rdi, ...);

/* __strftime_internal.isra.0 */
void** __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, unsigned char r8b, void** r9d, int32_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11) {
    void** r14_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r15_16;
    void** v17;
    unsigned char v18;
    void* rax19;
    void* v20;
    void*** rax21;
    void* rsp22;
    void** r11_23;
    void** r10d24;
    void*** v25;
    void** v26;
    uint32_t eax27;
    void** r13_28;
    void* rax29;
    uint32_t edx30;
    unsigned char v31;
    void** rdi32;
    void** r8_33;
    int64_t rax34;
    uint64_t rax35;
    int32_t eax36;
    void** rbp37;
    void** rax38;
    void** rcx39;
    uint32_t ecx40;
    int64_t r9_41;
    void** rcx42;
    int64_t r9_43;
    void** rbp44;
    void** v45;
    void** rbp46;
    uint32_t** rax47;
    void** r8_48;
    int1_t cf49;
    void** rax50;
    void** rcx51;
    void** rbx52;
    uint32_t eax53;
    uint32_t eax54;
    int64_t rdx55;
    int32_t ecx56;
    uint64_t rax57;
    void** rcx58;
    void** r8_59;
    void** r9_60;
    void** r15_61;
    void** r15_62;
    uint32_t** rax63;
    int64_t rcx64;
    int1_t cf65;
    void** r8_66;
    void** v67;
    signed char* rbp68;
    uint32_t** rax69;
    int64_t rcx70;
    int1_t cf71;
    signed char* rbp72;
    uint32_t** rax73;
    int64_t rcx74;
    int1_t cf75;
    int64_t rax76;

    r14_12 = rdi;
    rbp13 = rdx;
    rbx14 = rcx;
    v15 = rsi;
    r15_16 = reinterpret_cast<void**>(static_cast<int64_t>(a7));
    v17 = r9d;
    v18 = r8b;
    rax19 = g28;
    v20 = rax19;
    rax21 = fun_46f0();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    r11_23 = *reinterpret_cast<void***>(rbx14 + 48);
    r10d24 = *reinterpret_cast<void***>(rbx14 + 8);
    v25 = rax21;
    v26 = *rax21;
    if (!r11_23) {
    }
    if (reinterpret_cast<signed char>(r10d24) <= reinterpret_cast<signed char>(12)) {
        if (!r10d24) {
            r10d24 = reinterpret_cast<void**>(12);
        }
    } else {
        r10d24 = r10d24 - 12;
    }
    eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
    *reinterpret_cast<int32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax27)) {
        goto addr_1305d_10;
    }
    while (1) {
        addr_130bb_11:
        if (r14_12 && v15) {
            *reinterpret_cast<void***>(r14_12) = reinterpret_cast<void**>(0);
        }
        rdi = v26;
        *v25 = rdi;
        while (rax29 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v20) - reinterpret_cast<uint64_t>(g28)), !!rax29) {
            fun_4870();
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            edx30 = *reinterpret_cast<uint32_t*>(&rdx) + 100;
            if (!r10d24) {
                if (v17 == 43) {
                    addr_146ff_18:
                } else {
                    r10d24 = v17;
                    goto addr_1362c_20;
                }
            } else {
                if (!reinterpret_cast<int1_t>(r10d24 == 43)) 
                    goto addr_1362c_20; else 
                    goto addr_146ff_18;
            }
            r10d24 = reinterpret_cast<void**>(43);
            v31 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(2));
            addr_13643_24:
            if (static_cast<int1_t>(!reinterpret_cast<int1_t>(rdi == 79))) {
                if (0) {
                    edx30 = -edx30;
                }
                rdi32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xc7);
                r8_33 = rdi32;
                while (1) {
                    rsi = r8_33;
                    if (!1) {
                        --rsi;
                    }
                    *reinterpret_cast<uint32_t*>(&rax34) = edx30;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
                    r8_33 = rsi + 0xffffffffffffffff;
                    rax35 = reinterpret_cast<uint64_t>(rax34 * 0xcccccccd) >> 35;
                    if (edx30 > 9) 
                        goto addr_13ea3_33;
                    if (1) 
                        break;
                    addr_13ea3_33:
                    edx30 = *reinterpret_cast<uint32_t*>(&rax35);
                }
                if (!r10d24) {
                    eax36 = 1;
                    r10d24 = reinterpret_cast<void**>(48);
                } else {
                    *reinterpret_cast<unsigned char*>(&eax36) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(r10d24 == 45));
                }
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                    r15_16 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                }
                rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi32) - reinterpret_cast<unsigned char>(r8_33));
                rbp37 = rdi;
                if (!0) 
                    goto addr_13a7a_41;
            } else {
                rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xab);
                rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb0);
                rax38 = fun_4b40();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                rcx39 = rax38;
                if (!rax38) 
                    goto addr_130a8_45; else 
                    goto addr_133d9_46;
            }
            ecx40 = 45;
            addr_14395_48:
            rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(r15_16 + 0xffffffffffffffff)));
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(rsi) - *reinterpret_cast<uint32_t*>(&rbp37);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx) == 0) || !*reinterpret_cast<unsigned char*>(&eax36)) {
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
            }
            if (r10d24 == 95) {
                *reinterpret_cast<void**>(&r9_41) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_16) - *reinterpret_cast<uint32_t*>(&rdx));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_41) + 4) = 0;
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx)));
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rdx));
                r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                if (!r14_12) {
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_130e0_53;
                    ++r13_28;
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_41 - 1));
                    rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
                    goto addr_143da_55;
                } else {
                    rdi = r14_12;
                    fun_4920(rdi);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rdx = rdx;
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_130e0_53;
                    *reinterpret_cast<void**>(&r9_43) = *reinterpret_cast<void**>(&r9_41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_43) + 4) = 0;
                    r8_33 = r8_33;
                    r10d24 = r10d24;
                    ecx40 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ecx40));
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_43 - 1));
                }
            } else {
                if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= 1) 
                    goto addr_130e0_53;
                if (!r14_12) 
                    goto addr_143cd_60;
            }
            *reinterpret_cast<void***>(r14_12) = *reinterpret_cast<void***>(&ecx40);
            ++r14_12;
            addr_143cd_60:
            ++r13_28;
            rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
            if (r10d24 == 45) {
                addr_1440e_62:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_13aa6_63;
            } else {
                addr_143da_55:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
                    addr_13aa6_63:
                    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) {
                        addr_130e0_53:
                        *v25 = reinterpret_cast<void**>(34);
                    } else {
                        if (r14_12) {
                            if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rcx42)) {
                                rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(rcx42));
                                rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                                if (r10d24 == 48 || r10d24 == 43) {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_4920(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_33 = r8_33;
                                } else {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_4920(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    r8_33 = r8_33;
                                    rcx42 = rcx42;
                                }
                            }
                            if (!*reinterpret_cast<signed char*>(&v45)) {
                                rdx = rcx42;
                                rdi = r14_12;
                                v45 = rcx42;
                                fun_4a30(rdi, r8_33, rdx, rcx42);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                rcx42 = v45;
                            } else {
                                v45 = r8_33;
                                rbp46 = rcx42 + 0xffffffffffffffff;
                                if (rcx42) {
                                    rax47 = fun_4690();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_48 = v45;
                                    do {
                                        rsi = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r8_48) + reinterpret_cast<unsigned char>(rbp46))));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rdx) = (*rax47)[reinterpret_cast<unsigned char>(rsi)];
                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rbp46)) = *reinterpret_cast<signed char*>(&rdx);
                                        cf49 = reinterpret_cast<unsigned char>(rbp46) < reinterpret_cast<unsigned char>(1);
                                        --rbp46;
                                    } while (!cf49);
                                }
                            }
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rcx42));
                            goto addr_134b7_75;
                        }
                    }
                } else {
                    r15_16 = rsi;
                    goto addr_13a99_77;
                }
            }
            *reinterpret_cast<int32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            continue;
            addr_134b7_75:
            r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r15_16));
            addr_130a8_45:
            while (eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1)), rbp13 = rbx14 + 1, r15_16 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax27)) {
                addr_1305d_10:
                if (*reinterpret_cast<signed char*>(&eax27) != 37) {
                    *reinterpret_cast<int32_t*>(&rax50) = 0;
                    *reinterpret_cast<int32_t*>(&rax50 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rcx51) = 1;
                    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                    if (reinterpret_cast<signed char>(r15_16) >= reinterpret_cast<signed char>(0)) {
                        rax50 = r15_16;
                    }
                    if (rax50) {
                        rcx51 = rax50;
                    }
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                    if (reinterpret_cast<unsigned char>(rcx51) >= reinterpret_cast<unsigned char>(rdx)) 
                        goto addr_130e0_53;
                    if (r14_12) {
                        if (reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(1)) {
                            rbx52 = rax50 + 0xffffffffffffffff;
                            rdi = r14_12;
                            v45 = rcx51;
                            rdx = rbx52;
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rbx52));
                            fun_4920(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx51 = v45;
                        }
                        eax53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
                        ++r14_12;
                        *reinterpret_cast<void***>(r14_12 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax53);
                    }
                    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rcx51));
                    rbx14 = rbp13;
                    continue;
                }
                eax54 = v18;
                rbx14 = rbp13;
                r10d24 = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(&v45) = *reinterpret_cast<signed char*>(&eax54);
                while (*reinterpret_cast<void***>(&rdx55) = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, ++rbx14, ecx56 = static_cast<int32_t>(rdx55 - 35), rsi = *reinterpret_cast<void***>(&rdx55), rdi = *reinterpret_cast<void***>(&rdx55), *reinterpret_cast<unsigned char*>(&ecx56) <= 60) {
                    rax57 = 1 << *reinterpret_cast<unsigned char*>(&ecx56);
                    if (rax57 & 0x1000000000002500) {
                        r10d24 = *reinterpret_cast<void***>(&rdx55);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&ecx56) == 59) {
                            *reinterpret_cast<signed char*>(&v45) = 1;
                        } else {
                            if (!(*reinterpret_cast<uint32_t*>(&rax57) & 1)) 
                                break;
                        }
                    }
                }
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rdx55) - 48) > 9) 
                    goto addr_131bf_98;
                r15_16 = reinterpret_cast<void**>(0);
                do {
                    if (__intrinsic() || (r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_16) * 10 + reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48)), *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0, __intrinsic())) {
                        r15_16 = reinterpret_cast<void**>(0x7fffffff);
                        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                    }
                    rdi = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    ++rbx14;
                    rsi = rdi;
                } while (static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffd0)) <= 9);
                addr_131bf_98:
                if (*reinterpret_cast<unsigned char*>(&rsi) == 69 || *reinterpret_cast<unsigned char*>(&rsi) == 79) {
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    ++rbx14;
                } else {
                    rdi = reinterpret_cast<void**>(0);
                }
                if (*reinterpret_cast<unsigned char*>(&rsi) <= 0x7a) 
                    goto addr_131db_106;
                rcx58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rbp13));
                r8_59 = rcx58 + 1;
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0) || r10d24 == 45) {
                    r9_60 = r8_59;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                } else {
                    rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                    r9_60 = rdx;
                    if (reinterpret_cast<unsigned char>(r8_59) >= reinterpret_cast<unsigned char>(rdx)) {
                        r9_60 = r8_59;
                    }
                }
                if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r9_60)) 
                    goto addr_130e0_53;
                if (r14_12) {
                    if (reinterpret_cast<unsigned char>(r8_59) < reinterpret_cast<unsigned char>(rdx)) {
                        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_59));
                        r15_61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                        if (r10d24 == 48 || r10d24 == 43) {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_4920(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx58 = rcx58;
                            r8_59 = r8_59;
                            r9_60 = r9_60;
                        } else {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_4920(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r9_60 = r9_60;
                            r8_59 = r8_59;
                            rcx58 = rcx58;
                        }
                    }
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_59;
                        rdi = r14_12;
                        v45 = r8_59;
                        fun_4a30(rdi, rbp13, rdx, rcx58);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r9_60 = r9_60;
                        r8_59 = v45;
                    } else {
                        r15_62 = rcx58;
                        if (r8_59) {
                            v45 = r8_59;
                            rax63 = fun_4690();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_59 = v45;
                            r9_60 = r9_60;
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx64) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r15_62));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx64) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax63)[rcx64];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_62)) = *reinterpret_cast<signed char*>(&rdx);
                                cf65 = reinterpret_cast<unsigned char>(r15_62) < reinterpret_cast<unsigned char>(1);
                                --r15_62;
                            } while (!cf65);
                        }
                    }
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_59));
                }
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r9_60));
            }
            goto addr_130bb_11;
            addr_13a99_77:
            rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
            r15_16 = rcx42;
            if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(rcx42)) {
                r15_16 = rdx;
                goto addr_13aa6_63;
            }
            addr_13a7a_41:
            if (v31) {
                ecx40 = 43;
                goto addr_14395_48;
            } else {
                rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(rdi)));
                if (reinterpret_cast<signed char>(r15_16) <= reinterpret_cast<signed char>(rdi)) 
                    goto addr_14404_129;
                if (*reinterpret_cast<unsigned char*>(&eax36)) 
                    goto addr_13a99_77;
                addr_14404_129:
                if (!reinterpret_cast<int1_t>(r10d24 == 45)) 
                    goto addr_13a99_77; else 
                    goto addr_1440e_62;
            }
            addr_133d9_46:
            r10d24 = r10d24;
            r8_66 = rax38 + 0xffffffffffffffff;
            if (r10d24 == 45 || reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                r15_16 = r8_66;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                r15_16 = rdx;
                if (reinterpret_cast<unsigned char>(r8_66) >= reinterpret_cast<unsigned char>(rdx)) {
                    r15_16 = r8_66;
                }
            }
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) 
                goto addr_130e0_53;
            if (r14_12) {
                if (reinterpret_cast<unsigned char>(r8_66) < reinterpret_cast<unsigned char>(rdx)) {
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_66));
                    v67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (r10d24 == 48 || r10d24 == 43) {
                        rdi = r14_12;
                        fun_4920(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        rcx39 = rcx39;
                        r8_66 = r8_66;
                    } else {
                        rdi = r14_12;
                        fun_4920(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        r8_66 = r8_66;
                        rcx39 = rcx39;
                    }
                }
                if (0) {
                    rbp68 = reinterpret_cast<signed char*>(rcx39 + 0xfffffffffffffffe);
                    if (r8_66) {
                        v45 = r8_66;
                        rax69 = fun_4c60(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx70) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint32_t>(rbp68));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx70) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rdx) = (*rax69)[rcx70];
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint32_t>(rbp68)) = *reinterpret_cast<signed char*>(&rdx);
                            cf71 = reinterpret_cast<uint32_t>(rbp68) < reinterpret_cast<uint32_t>(1);
                            --rbp68;
                        } while (!cf71);
                    }
                } else {
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_66;
                        rdi = r14_12;
                        v45 = r8_66;
                        fun_4a30(rdi, reinterpret_cast<int64_t>(rsp22) + 0xb1, rdx, rcx39);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                    } else {
                        rbp72 = reinterpret_cast<signed char*>(rcx39 + 0xfffffffffffffffe);
                        if (r8_66) {
                            v45 = r8_66;
                            rax73 = fun_4690();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_66 = v45;
                            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx74) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint32_t>(rbp72));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx74) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax73)[rcx74];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint32_t>(rbp72)) = *reinterpret_cast<signed char*>(&rdx);
                                cf75 = reinterpret_cast<uint32_t>(rbp72) < reinterpret_cast<uint32_t>(1);
                                --rbp72;
                            } while (!cf75);
                        }
                    }
                }
                r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_66));
                goto addr_134b7_75;
            }
            addr_1362c_20:
            v31 = 0;
            goto addr_13643_24;
        }
        break;
    }
    return r13_28;
    addr_131db_106:
    *reinterpret_cast<uint32_t*>(&rax76) = *reinterpret_cast<unsigned char*>(&rsi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax76) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1e3cc + rax76 * 4) + 0x1e3cc;
}

uint64_t fun_4850();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_4850();
    if (r8d > 10) {
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1e680 + rax11 * 4) + 0x1e680;
    }
}

struct s19 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

struct s20 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s19* rcx, ...) {
    int64_t rbx5;
    void* rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rdi15;
    int64_t rax16;
    uint32_t r8d17;
    struct s20* rbx18;
    uint32_t r15d19;
    void** rsi20;
    void** r14_21;
    int64_t v22;
    int64_t v23;
    uint32_t r15d24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x1648f;
    rax8 = fun_46f0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
        fun_46e0();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x25270) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xec41]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            rdi15 = reinterpret_cast<void**>((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            v7 = 0x1651b;
            fun_4920(rdi15, rdi15);
            rax16 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax16);
        }
        r8d17 = rcx->f0;
        rbx18 = reinterpret_cast<struct s20*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d19 = rcx->f4;
        rsi20 = rbx18->f0;
        r14_21 = rbx18->f8;
        v22 = rcx->f30;
        v23 = rcx->f28;
        r15d24 = r15d19 | 1;
        rax25 = quotearg_buffer_restyled(r14_21, rsi20, rsi, rdx, r8d17, r15d24, &rcx->f8, v23, v22, v7);
        if (reinterpret_cast<unsigned char>(rsi20) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx18->f0 = rsi26;
            if (r14_21 != 0x26420) {
                fun_4630(r14_21, r14_21);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx18->f8 = rax27;
            v29 = rcx->f30;
            r14_21 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d24, rsi26, v30, v29, 0x165aa);
        }
        *rax8 = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
        if (rax31) {
            fun_4870();
        } else {
            return r14_21;
        }
    }
}

void** fun_46a0(void** rdi, ...);

int64_t fun_4660(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** tzalloc(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_4ba0(int64_t rdi, void** rsi);

int32_t fun_47e0(int64_t rdi, void** rsi, int64_t rdx);

void fun_4a50(int64_t rdi, void** rsi, int64_t rdx);

void** set_tz(void** rdi) {
    void** rax2;
    void** rsi3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    int64_t rax7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** rax11;
    void** r12_12;
    int32_t eax13;
    int32_t eax14;
    void*** rax15;
    void** ebx16;
    void*** rbp17;
    void** rdi18;

    rax2 = fun_46a0("TZ");
    if (!rax2) {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            return 1;
        }
    } else {
        if (*reinterpret_cast<void***>(rdi + 8) && (rsi3 = rax2, rax7 = fun_4660(rdi + 9, rsi3, rdx4, rcx5, r8_6), !*reinterpret_cast<int32_t*>(&rax7))) {
            return 1;
        }
    }
    rax11 = tzalloc(rax2, rsi3, rdx8, rcx9, r8_10);
    r12_12 = rax11;
    if (!rax11) {
        addr_17751_7:
        return r12_12;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            eax13 = fun_4ba0("TZ", rsi3);
            if (eax13) 
                goto addr_177bd_10; else 
                goto addr_1774c_11;
        }
        rsi3 = rdi + 9;
        eax14 = fun_47e0("TZ", rsi3, 1);
        if (!eax14) {
            addr_1774c_11:
            fun_4a50("TZ", rsi3, 1);
            goto addr_17751_7;
        } else {
            addr_177bd_10:
            rax15 = fun_46f0();
            ebx16 = *rax15;
            rbp17 = rax15;
            if (r12_12 != 1) {
                do {
                    rdi18 = r12_12;
                    r12_12 = *reinterpret_cast<void***>(r12_12);
                    fun_4630(rdi18, rdi18);
                } while (r12_12);
            }
        }
    }
    *rbp17 = ebx16;
    return 0;
}

signed char save_abbr(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** r13_5;
    void** rdx6;
    int32_t eax7;
    void** rbx8;
    void** rsi9;
    void** rcx10;
    void** r8_11;
    int64_t rax12;
    void** rax13;
    void** rcx14;
    void** r8_15;
    int64_t rax16;
    void** rax17;
    void** rdx18;
    void** rcx19;
    void** r8_20;
    void** rax21;
    void** rcx22;

    r12_3 = *reinterpret_cast<void***>(rsi + 48);
    if (!r12_3) {
        return 1;
    }
    rbp4 = rdi;
    r13_5 = rsi;
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(r12_3) || (rdx6 = rsi + 56, eax7 = 1, reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rdx6))) {
        rbx8 = rbp4 + 9;
        if (!*reinterpret_cast<void***>(r12_3)) {
            rbx8 = reinterpret_cast<void**>(0x1bb0a);
        } else {
            while (rsi9 = r12_3, rax12 = fun_4660(rbx8, rsi9, rdx6, rcx10, r8_11), !!*reinterpret_cast<int32_t*>(&rax12)) {
                do {
                    if (*reinterpret_cast<void***>(rbx8)) 
                        goto addr_17633_9;
                    if (rbx8 != rbp4 + 9) 
                        goto addr_176a0_11;
                    if (!*reinterpret_cast<void***>(rbp4 + 8)) 
                        goto addr_176a0_11;
                    addr_17633_9:
                    rax13 = fun_4860(rbx8, rbx8);
                    rbx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax13) + 1);
                    if (*reinterpret_cast<void***>(rbx8)) 
                        break;
                    if (!*reinterpret_cast<void***>(rbp4)) 
                        break;
                    rbx8 = *reinterpret_cast<void***>(rbp4) + 9;
                    rsi9 = r12_3;
                    rbp4 = *reinterpret_cast<void***>(rbp4);
                    rax16 = fun_4660(rbx8, rsi9, rdx6, rcx14, r8_15);
                } while (*reinterpret_cast<int32_t*>(&rax16));
                goto addr_17664_15;
            }
        }
    } else {
        addr_17671_16:
        return *reinterpret_cast<signed char*>(&eax7);
    }
    addr_17668_17:
    *reinterpret_cast<void***>(r13_5 + 48) = rbx8;
    eax7 = 1;
    goto addr_17671_16;
    addr_176a0_11:
    rax17 = fun_4860(r12_3, r12_3);
    rdx18 = rax17 + 1;
    if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbp4 + 0x80) - reinterpret_cast<unsigned char>(rbx8)) <= reinterpret_cast<signed char>(rdx18)) {
        rax21 = tzalloc(r12_3, rsi9, rdx18, rcx19, r8_20);
        *reinterpret_cast<void***>(rbp4) = rax21;
        if (!rax21) {
            eax7 = 0;
            goto addr_17671_16;
        } else {
            *reinterpret_cast<void***>(rax21 + 8) = reinterpret_cast<void**>(0);
            rbx8 = rax21 + 9;
            goto addr_17668_17;
        }
    } else {
        fun_4a30(rbx8, r12_3, rdx18, rcx22);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax17) + 1) = 0;
        goto addr_17668_17;
    }
    addr_17664_15:
    goto addr_17668_17;
}

/* revert_tz.part.0 */
signed char revert_tz_part_0(void** rdi, void** rsi) {
    void** rbx3;
    void*** rax4;
    void** r12d5;
    void*** rbp6;
    int32_t eax7;
    int32_t r13d8;
    void** rdi9;
    int32_t eax10;
    int32_t eax11;

    rbx3 = rdi;
    rax4 = fun_46f0();
    r12d5 = *rax4;
    rbp6 = rax4;
    if (*reinterpret_cast<void***>(rbx3 + 8)) {
        rsi = rbx3 + 9;
        eax7 = fun_47e0("TZ", rsi, 1);
        if (eax7) {
            addr_174ce_3:
            r12d5 = *rbp6;
            r13d8 = 0;
        } else {
            addr_17519_4:
            fun_4a50("TZ", rsi, 1);
            r13d8 = 1;
        }
        do {
            rdi9 = rbx3;
            rbx3 = *reinterpret_cast<void***>(rbx3);
            fun_4630(rdi9, rdi9);
        } while (rbx3);
        *rbp6 = r12d5;
        eax10 = r13d8;
        return *reinterpret_cast<signed char*>(&eax10);
    } else {
        eax11 = fun_4ba0("TZ", rsi);
        if (!eax11) 
            goto addr_17519_4; else 
            goto addr_174ce_3;
    }
}

uint64_t ydhms_diff(int64_t rdi, int64_t rsi, int32_t edx, uint32_t ecx, int32_t r8d, int32_t r9d, int32_t a7, int32_t a8, int32_t a9, int32_t a10) {
    int64_t rcx11;
    uint64_t rcx12;
    int64_t rax13;
    int64_t rbx14;
    int64_t rdx15;
    int64_t rax16;
    int64_t r9_17;
    int64_t r9_18;
    int64_t rbp19;
    int64_t rdi20;
    int32_t ecx21;
    int64_t rdx22;
    uint32_t edx23;
    int64_t rbp24;
    int32_t r12d25;
    int64_t rdx26;
    int64_t rcx27;
    uint32_t ecx28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t rax31;
    int64_t rax32;
    int64_t rax33;

    rcx11 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx11) & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
    rax13 = rdi >> 2;
    rbx14 = r9d;
    rdx15 = rbx14;
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax13) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax13) < 0x1db - reinterpret_cast<uint1_t>(rcx12 < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    r9_17 = rbx14 >> 2;
    *reinterpret_cast<uint32_t*>(&r9_18) = *reinterpret_cast<uint32_t*>(&r9_17) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_17) < 0x1db - reinterpret_cast<uint1_t>((*reinterpret_cast<uint32_t*>(&rdx15) & 3) < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_18) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbp19) = *reinterpret_cast<uint32_t*>(&rax16) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    rdi20 = rdi - rbx14;
    ecx21 = static_cast<int32_t>(rbp19 + rax16);
    rdx22 = ecx21 * 0x51eb851f >> 35;
    edx23 = *reinterpret_cast<int32_t*>(&rdx22) - (ecx21 >> 31) - *reinterpret_cast<uint32_t*>(&rbp19);
    *reinterpret_cast<uint32_t*>(&rbp24) = *reinterpret_cast<uint32_t*>(&r9_18) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    r12d25 = static_cast<int32_t>(rbp24 + r9_18);
    rdx26 = static_cast<int64_t>(reinterpret_cast<int32_t>(edx23)) >> 2;
    rcx27 = r12d25 * 0x51eb851f >> 35;
    ecx28 = *reinterpret_cast<int32_t*>(&rcx27) - (r12d25 >> 31) - *reinterpret_cast<uint32_t*>(&rbp24);
    rcx29 = static_cast<int64_t>(reinterpret_cast<int32_t>(ecx28)) >> 2;
    rdx30 = rdi20 + (rdi20 + rdi20 * 8) * 8;
    rax31 = reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax16) - *reinterpret_cast<uint32_t*>(&r9_18) - (edx23 - ecx28) + (*reinterpret_cast<int32_t*>(&rdx26) - *reinterpret_cast<uint32_t*>(&rcx29))) + (rdx30 + rdx30 * 4 + rsi - a7);
    rax32 = edx + (rax31 + rax31 * 2) * 8 - a8;
    rax33 = reinterpret_cast<int32_t>(ecx) + ((rax32 << 4) - rax32) * 4 - a9;
    return r8d + ((rax33 << 4) - rax33) * 4 - reinterpret_cast<uint64_t>(static_cast<int64_t>(a10));
}

struct s21 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
    int64_t f30;
};

struct s21* ranged_convert(int64_t rdi, uint64_t* rsi, struct s21* rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rbx7;
    uint64_t r12_8;
    uint64_t* v9;
    void* rbp10;
    void* rax11;
    void* v12;
    struct s21* rax13;
    struct s21* v14;
    void*** rax15;
    int1_t zf16;
    void*** v17;
    uint64_t rcx18;
    int64_t rcx19;
    uint64_t r13_20;
    void* rax21;
    int32_t v22;
    uint64_t r14_23;
    uint64_t r12_24;
    uint64_t r13_25;
    struct s21* r15_26;
    int64_t rax27;
    int32_t v28;
    int32_t v29;
    int32_t v30;
    int32_t v31;
    int32_t v32;
    int32_t v33;
    int32_t v34;
    int32_t v35;
    int64_t v36;
    int64_t v37;
    uint64_t rax38;
    uint64_t rax39;

    rbx7 = rdi;
    r12_8 = *rsi;
    v9 = rsi;
    rbp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 + 80);
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<struct s21*>(rbx7(rbp10, rdx));
    v14 = rax13;
    if (!rax13) {
        rax15 = fun_46f0();
        zf16 = reinterpret_cast<int1_t>(*rax15 == 75);
        v17 = rax15;
        if (!zf16 || ((rcx18 = r12_8, *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<uint32_t*>(&rcx18) & 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0, r13_20 = rcx19 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_8) >> 1), r12_8 == r13_20) || !r13_20)) {
            addr_18fae_3:
            rax21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v12) - reinterpret_cast<uint64_t>(g28));
            if (rax21) {
                fun_4870();
            } else {
                return v14;
            }
        } else {
            v22 = -1;
            r14_23 = r12_8;
            r12_24 = r13_20;
            r13_25 = 0;
            r15_26 = rdx;
            do {
                rax27 = reinterpret_cast<int64_t>(rbx7(rbp10, r15_26));
                if (rax27) {
                    r13_25 = r12_24;
                    v22 = r15_26->f0;
                    v28 = r15_26->f4;
                    v29 = r15_26->f8;
                    v30 = r15_26->fc;
                    v31 = r15_26->f10;
                    v32 = r15_26->f14;
                    v33 = r15_26->f18;
                    v34 = r15_26->f1c;
                    v35 = r15_26->f20;
                    v36 = r15_26->f28;
                    v37 = r15_26->f30;
                } else {
                    if (!reinterpret_cast<int1_t>(*v17 == 75)) 
                        goto addr_18fae_3;
                    r14_23 = r12_24;
                }
                rax38 = r13_25 | r14_23;
                *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
                r12_24 = (reinterpret_cast<int64_t>(r13_25) >> 1) + (reinterpret_cast<int64_t>(r14_23) >> 1) + rax39;
            } while (r12_24 != r13_25 && r12_24 != r14_23);
        }
        if (v22 >= 0) {
            v14 = r15_26;
            *v9 = r13_25;
            r15_26->f0 = v22;
            r15_26->f4 = v28;
            r15_26->f8 = v29;
            r15_26->fc = v30;
            r15_26->f10 = v31;
            r15_26->f14 = v32;
            r15_26->f18 = v33;
            r15_26->f1c = v34;
            r15_26->f20 = v35;
            r15_26->f28 = v36;
            r15_26->f30 = v37;
            goto addr_18fae_3;
        }
    } else {
        *rsi = r12_8;
        goto addr_18fae_3;
    }
}

void** fun_4810(void** rdi);

void** active_dir_set = reinterpret_cast<void**>(0);

int32_t fun_49d0(void** rdi);

void** hash_insert(void** rdi, void** rsi, struct s1* rdx, int64_t rcx);

void** g260f8 = reinterpret_cast<void**>(0);

int64_t g26100 = 0;

int32_t fun_4960(void** rdi, ...);

signed char recursive = 0;

/* first.0 */
signed char first_0 = 1;

signed char print_dir_name = 0;

void** dirname_quoting_options = reinterpret_cast<void**>(0);

struct s22 {
    signed char[18] pad18;
    unsigned char f12;
    void** f13;
    signed char f14;
};

struct s22* fun_4a80(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t ignore_mode = 0;

void** ignore_patterns = reinterpret_cast<void**>(0);

int32_t fun_4910(void** rdi, void** rsi, ...);

void** hide_patterns = reinterpret_cast<void**>(0);

void extract_dirs_from_files(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void print_dir(void** rdi, void** rsi, void** edx, void** rcx) {
    void** rdx3;
    void** r14_5;
    void** r13_6;
    void** ebx7;
    void** v8;
    void* rax9;
    void* v10;
    void*** rax11;
    void** rdi12;
    void*** rbp13;
    void** rax14;
    void* rsp15;
    void* rax16;
    uint32_t ebx17;
    int1_t zf18;
    void** r12_19;
    int32_t eax20;
    void* rsp21;
    struct s1* rdx22;
    void** rdi23;
    int64_t rcx24;
    void** rsi25;
    int32_t eax26;
    void* rsp27;
    void** rax28;
    void** v29;
    void** v30;
    void** rax31;
    void** r8_32;
    void** rax33;
    uint32_t edi34;
    void* rax35;
    void** rax36;
    int64_t rdx37;
    int32_t eax38;
    void** v39;
    void* rsp40;
    int1_t zf41;
    int1_t zf42;
    int1_t zf43;
    int1_t zf44;
    int1_t zf45;
    void** r15_46;
    int1_t zf47;
    void** rsi48;
    void** rax49;
    void** rax50;
    int1_t zf51;
    void** rdi52;
    void** tmp64_53;
    void** rax54;
    int1_t zf55;
    void** v56;
    uint32_t v57;
    void** rdi58;
    struct s22* rax59;
    void* rsp60;
    struct s22* rbx61;
    void** rax62;
    void** r14_63;
    int32_t eax64;
    void* rax65;
    void** r15_66;
    int32_t eax67;
    void** r15_68;
    int32_t eax69;
    uint32_t eax70;
    int64_t rax71;
    void** rax72;
    int1_t zf73;
    int1_t zf74;
    int1_t zf75;
    int1_t zf76;
    void** rdi77;
    int32_t eax78;
    void* rsp79;
    void** rax80;
    void* rsp81;
    int1_t zf82;
    void** eax83;
    int1_t zf84;
    int64_t rdx85;
    void** r8_86;
    void* rsi87;
    void** rax88;
    void** rax89;
    uint32_t edx90;
    void** r12_91;
    signed char* rax92;
    int1_t zf93;
    void** rax94;
    void** rax95;
    void** rsi96;
    int1_t zf97;
    void* rax98;
    void* rax99;
    int1_t below_or_equal100;
    int32_t eax101;
    int64_t rax102;

    rdx3 = edx;
    r14_5 = rsi;
    r13_6 = rdi;
    ebx7 = rdx3;
    v8 = rdx3;
    rax9 = g28;
    v10 = rax9;
    rax11 = fun_46f0();
    rdi12 = r13_6;
    *rax11 = reinterpret_cast<void**>(0);
    rbp13 = rax11;
    rax14 = fun_4810(rdi12);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x358 - 8 + 8 - 8 + 8);
    if (!rax14) {
        fun_4840();
        rax16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
        if (rax16) {
            addr_dbd1_3:
            fun_4870();
        } else {
            ebx17 = *reinterpret_cast<unsigned char*>(&v8);
            quotearg_style(4, r13_6);
            fun_46f0();
            fun_4b70();
            if (!*reinterpret_cast<signed char*>(&ebx17)) 
                goto addr_7518_6; else 
                goto addr_7504_7;
        }
    } else {
        zf18 = active_dir_set == 0;
        r12_19 = rax14;
        if (!zf18) {
            eax20 = fun_49d0(rax14);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            rdx22 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rsp21) + 32);
            *reinterpret_cast<int32_t*>(&rdi23) = eax20;
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            if (eax20 < 0) {
                *reinterpret_cast<int32_t*>(&rcx24) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
                rsi25 = r13_6;
                eax26 = do_statx(0xffffff9c, rsi25, rdx22);
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (eax26 >= 0) {
                    addr_d5b6_11:
                    rax28 = xmalloc(16, rsi25, 16, rsi25);
                    rdi12 = active_dir_set;
                    *reinterpret_cast<void***>(rax28 + 8) = v29;
                    rsi = rax28;
                    *reinterpret_cast<void***>(rax28) = v30;
                    rax31 = hash_insert(rdi12, rsi, rdx22, rcx24);
                    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8);
                    r8_32 = rax28;
                    if (!rax31) {
                        xalloc_die();
                        goto addr_dbd1_3;
                    }
                } else {
                    addr_d8af_13:
                    rax33 = fun_4840();
                    edi34 = *reinterpret_cast<unsigned char*>(&v8);
                    file_failure(edi34, rax33, r13_6, edi34, rax33, r13_6);
                    rax35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
                    if (!rax35) {
                    }
                }
                if (r8_32 == rax31) {
                    rax36 = g260f8;
                    rdx37 = g26100;
                    if (reinterpret_cast<uint64_t>(rdx37 - reinterpret_cast<unsigned char>(rax36)) <= 15) {
                        *reinterpret_cast<uint32_t*>(&rsi) = 16;
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        rdi12 = reinterpret_cast<void**>(0x260e0);
                        _obstack_newchunk(0x260e0, 0x260e0);
                        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                        rax36 = g260f8;
                    }
                    rcx = v30;
                    rdx3 = rax36 + 16;
                    g260f8 = rdx3;
                    *reinterpret_cast<void***>(rax36 + 8) = v29;
                    *reinterpret_cast<void***>(rax36) = rcx;
                } else {
                    fun_4630(r8_32, r8_32);
                    quotearg_n_style_colon();
                    fun_4840();
                    fun_4b70();
                    fun_4960(r12_19);
                    exit_status = 2;
                    goto addr_d655_21;
                }
            } else {
                *reinterpret_cast<int32_t*>(&rcx24) = 0x1000;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
                rsi25 = reinterpret_cast<void**>(0x1bb0a);
                eax38 = do_statx(rdi23, 0x1bb0a, rdx22);
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (eax38 < 0) 
                    goto addr_d8af_13; else 
                    goto addr_d5b6_11;
            }
        }
        v39 = reinterpret_cast<void**>(0xd6b5);
        clear_files(rdi12, rsi);
        rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        zf41 = recursive == 0;
        if (!zf41) {
            zf42 = first_0 == 0;
            if (zf42) 
                goto addr_d7f4_25; else 
                goto addr_d6cf_26;
        }
        zf43 = print_dir_name == 0;
        if (!zf43) {
            zf44 = first_0 == 0;
            if (!zf44) {
                addr_d6cf_26:
                zf45 = dired == 0;
                first_0 = 0;
                if (!zf45) {
                    addr_d830_29:
                    *reinterpret_cast<int32_t*>(&r15_46) = 0;
                    *reinterpret_cast<int32_t*>(&r15_46 + 4) = 0;
                    v39 = reinterpret_cast<void**>(0xd844);
                    dired_outbuf("  ", 2);
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                    zf47 = print_hyperlink == 0;
                    if (zf47) {
                        addr_d6f3_30:
                        rsi48 = dirname_quoting_options;
                        rdx3 = reinterpret_cast<void**>(0xffffffff);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        if (!r14_5) {
                            r14_5 = r13_6;
                        }
                    } else {
                        addr_d851_32:
                        v39 = reinterpret_cast<void**>(0xd85e);
                        rax49 = canonicalize_filename_mode(r13_6, 2);
                        rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                        r15_46 = rax49;
                        if (!rax49) {
                            rax50 = fun_4840();
                            v39 = reinterpret_cast<void**>(0xd88f);
                            file_failure(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v8)), rax50, r13_6);
                            rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8 - 8 + 8);
                            goto addr_d6f3_30;
                        }
                    }
                } else {
                    addr_d6e3_34:
                    *reinterpret_cast<int32_t*>(&r15_46) = 0;
                    *reinterpret_cast<int32_t*>(&r15_46 + 4) = 0;
                    zf51 = print_hyperlink == 0;
                    if (!zf51) 
                        goto addr_d851_32; else 
                        goto addr_d6f3_30;
                }
            } else {
                addr_d7f4_25:
                rdi52 = stdout;
                tmp64_53 = dired_pos + 1;
                dired_pos = tmp64_53;
                rax54 = *reinterpret_cast<void***>(rdi52 + 40);
                if (reinterpret_cast<unsigned char>(rax54) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi52 + 48))) {
                    v39 = reinterpret_cast<void**>(0xdb80);
                    fun_48d0();
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                    goto addr_d6cf_26;
                } else {
                    zf55 = dired == 0;
                    first_0 = 0;
                    *reinterpret_cast<void***>(rdi52 + 40) = rax54 + 1;
                    *reinterpret_cast<void***>(rax54) = reinterpret_cast<void**>(10);
                    if (zf55) 
                        goto addr_d6e3_34; else 
                        goto addr_d830_29;
                }
            }
            *reinterpret_cast<uint32_t*>(&r8_32) = 1;
            *reinterpret_cast<int32_t*>(&r8_32 + 4) = 0;
            quote_name(r14_5, rsi48, 0xffffffff, 0, 1, 0x26140, r15_46);
            fun_4630(r15_46, r15_46);
            dired_outbuf(":\n", 2, ":\n", 2);
            rcx = r15_46;
            rsi = v39;
            rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 + 8 + 8);
        }
        v56 = reinterpret_cast<void**>(0);
        v57 = *reinterpret_cast<unsigned char*>(&ebx7);
        while (1) {
            *rbp13 = reinterpret_cast<void**>(0);
            rdi58 = r12_19;
            rax59 = fun_4a80(rdi58, rsi, rdx3, rcx, r8_32, 0x26140);
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
            rbx61 = rax59;
            if (!rax59) {
                rdx3 = *rbp13;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                if (!rdx3) 
                    break;
                rax62 = fun_4840();
                *reinterpret_cast<uint32_t*>(&rdi58) = v57;
                *reinterpret_cast<int32_t*>(&rdi58 + 4) = 0;
                rdx3 = r13_6;
                rsi = rax62;
                file_failure(*reinterpret_cast<uint32_t*>(&rdi58), rsi, rdx3, *reinterpret_cast<uint32_t*>(&rdi58), rsi, rdx3);
                rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8);
                if (*rbp13 == 75) 
                    goto addr_d7d0_42; else 
                    break;
            }
            r14_63 = reinterpret_cast<void**>(&rax59->f13);
            eax64 = ignore_mode;
            if (eax64 == 2) 
                goto addr_da60_44;
            if (*reinterpret_cast<void***>(&rbx61->f13) == 46) {
                if (!eax64 || (*reinterpret_cast<int32_t*>(&rax65) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax65) + 4) = 0, *reinterpret_cast<unsigned char*>(&rax65) = reinterpret_cast<uint1_t>(rbx61->f14 == 46), *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rbx61) + reinterpret_cast<int64_t>(rax65) + 20) == 0)) {
                    addr_d7d0_42:
                    process_signals(rdi58, rsi, rdx3, rcx, r8_32, 0x26140, rdi58, rsi, rdx3);
                    rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                    continue;
                } else {
                    goto addr_da60_44;
                }
            }
            if (eax64) {
                addr_da60_44:
                r15_66 = ignore_patterns;
                if (r15_66) {
                    do {
                        rdi58 = *reinterpret_cast<void***>(r15_66);
                        rdx3 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsi = r14_63;
                        eax67 = fun_4910(rdi58, rsi, rdi58, rsi);
                        rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                        if (!eax67) 
                            break;
                        r15_66 = *reinterpret_cast<void***>(r15_66 + 8);
                    } while (r15_66);
                    goto addr_da98_51;
                } else {
                    goto addr_da98_51;
                }
            } else {
                r15_68 = hide_patterns;
                if (r15_68) {
                    do {
                        rdi58 = *reinterpret_cast<void***>(r15_68);
                        rdx3 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsi = r14_63;
                        eax69 = fun_4910(rdi58, rsi, rdi58, rsi);
                        rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
                        if (!eax69) 
                            goto addr_d7c9_55;
                        r15_68 = *reinterpret_cast<void***>(r15_68 + 8);
                    } while (r15_68);
                    goto addr_da60_44;
                } else {
                    goto addr_da60_44;
                }
            }
            goto addr_d7d0_42;
            addr_da98_51:
            *reinterpret_cast<uint32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            eax70 = rbx61->f12 - 1;
            if (*reinterpret_cast<unsigned char*>(&eax70) <= 13) {
                *reinterpret_cast<uint32_t*>(&rax71) = *reinterpret_cast<unsigned char*>(&eax70);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<uint32_t*>(0x1a600 + rax71 * 4);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            }
            rdx3 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
            rcx = r13_6;
            rdi58 = r14_63;
            rax72 = gobble_file_constprop_0(rdi58, *reinterpret_cast<uint32_t*>(&rsi), 0, rcx, r8_32, 0x26140);
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
            v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v56) + reinterpret_cast<unsigned char>(rax72));
            zf73 = reinterpret_cast<int1_t>(format == 1);
            if (zf73 && ((zf74 = sort_type == 6, zf74) && ((zf75 = print_block_size == 0, zf75) && (zf76 = recursive == 0, zf76)))) {
                sort_files(rdi58, rsi, 0, rcx, r8_32, 0x26140);
                print_current_files(rdi58, rsi);
                clear_files(rdi58, rsi);
                rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_d7d0_42;
            }
            addr_d7c9_55:
            goto addr_d7d0_42;
        }
        rdi77 = r12_19;
        eax78 = fun_4960(rdi77, rdi77);
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8);
        if (eax78) {
            rax80 = fun_4840();
            *reinterpret_cast<uint32_t*>(&rdi77) = *reinterpret_cast<unsigned char*>(&v8);
            *reinterpret_cast<int32_t*>(&rdi77 + 4) = 0;
            rdx3 = r13_6;
            rsi = rax80;
            file_failure(*reinterpret_cast<uint32_t*>(&rdi77), rsi, rdx3, *reinterpret_cast<uint32_t*>(&rdi77), rsi, rdx3);
            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        }
        sort_files(rdi77, rsi, rdx3, rcx, r8_32, 0x26140, rdi77, rsi);
        rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
        zf82 = recursive == 0;
        if (!zf82) {
            extract_dirs_from_files(r13_6, 0, rdx3, rcx, r8_32, 0x26140);
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
        }
        eax83 = format;
        if (!eax83 || (zf84 = print_block_size == 0, !zf84)) {
            *reinterpret_cast<int32_t*>(&rdx85) = human_output_opts;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx85) + 4) = 0;
            r8_86 = output_block_size;
            rsi87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) + 0xb1);
            rax88 = human_readable(v56, rsi87, rdx85, 0x200, r8_86, v56, rsi87, rdx85, 0x200, r8_86);
            rax89 = fun_4860(rax88, rax88);
            edx90 = eolbyte;
            *reinterpret_cast<void***>(rax88 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
            r12_91 = rax88 + 0xffffffffffffffff;
            rax92 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<unsigned char>(rax88));
            zf93 = dired == 0;
            *rax92 = *reinterpret_cast<signed char*>(&edx90);
            if (!zf93) {
                dired_outbuf("  ", 2, "  ", 2);
            }
            rax94 = fun_4840();
            rax95 = fun_4860(rax94, rax94);
            dired_outbuf(rax94, rax95, rax94, rax95);
            rsi96 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax92 + 1) - reinterpret_cast<unsigned char>(r12_91));
            dired_outbuf(r12_91, rsi96, r12_91, rsi96);
        }
        zf97 = cwd_n_used == 0;
        if (zf97) {
            addr_d655_21:
            rax98 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
            if (!rax98) {
                return;
            }
        } else {
            rax99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
            if (rax99) 
                goto addr_dbd1_3;
            below_or_equal100 = reinterpret_cast<unsigned char>(format) <= reinterpret_cast<unsigned char>(4);
            if (!below_or_equal100) 
                goto addr_d51b_75; else 
                goto addr_d12d_76;
        }
    }
    addr_7518_6:
    eax101 = exit_status;
    if (!eax101) {
        exit_status = 1;
        return;
    }
    addr_750e_79:
    return;
    addr_7504_7:
    exit_status = 2;
    goto addr_750e_79;
    addr_d51b_75:
    return;
    addr_d12d_76:
    *reinterpret_cast<void***>(&rax102) = format;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1a17c + rax102 * 4) + 0x1a17c;
}

struct s23 {
    signed char[8] pad8;
    uint64_t f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[15] pad48;
    void* f30;
    signed char[24] pad80;
    unsigned char f50;
};

void fun_4990(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void dired_dump_obstack(void** rdi, struct s23* rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void** rbx7;
    void* rbp8;
    void** rax9;
    void** rdx10;
    void** r8_11;
    void** rcx12;
    void** rbp13;
    void** rsi14;
    void** rdx15;
    void** rdi16;
    void** rax17;

    rax6 = rsi->f18;
    rbx7 = rsi->f10;
    rbp8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(rbx7));
    if (reinterpret_cast<uint64_t>(rbp8) > 7) {
        if (rax6 == rbx7) {
            rsi->f50 = reinterpret_cast<unsigned char>(rsi->f50 | 2);
        }
        rax9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) + reinterpret_cast<uint64_t>(rsi->f30) & ~reinterpret_cast<uint64_t>(rsi->f30));
        rdx10 = rsi->f20;
        r8_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax9) - rsi->f8);
        rcx12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx10) - rsi->f8);
        if (reinterpret_cast<unsigned char>(r8_11) <= reinterpret_cast<unsigned char>(rcx12)) {
            rdx10 = rax9;
        }
        rbp13 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rbp8) & 0xfffffffffffffff8) + reinterpret_cast<unsigned char>(rbx7));
        rsi->f18 = rdx10;
        rsi->f10 = rdx10;
        rsi14 = stdout;
        fun_4990(rdi, rsi14, rdx10, rcx12, r8_11);
        do {
            rdx15 = *reinterpret_cast<void***>(rbx7);
            rbx7 = rbx7 + 8;
            fun_4b20(1, " %ld", rdx15, rcx12, r8_11);
        } while (rbx7 != rbp13);
        rdi16 = stdout;
        rax17 = *reinterpret_cast<void***>(rdi16 + 40);
        if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi16 + 48))) {
            *reinterpret_cast<void***>(rdi16 + 40) = rax17 + 1;
            *reinterpret_cast<void***>(rax17) = reinterpret_cast<void**>(10);
        }
    }
    return;
}

struct s24 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
};

void** fun_4ab0(int64_t rdi);

void** fun_48a0(void** rdi, ...);

uint16_t** fun_4c70(void** rdi, void** rsi, ...);

void** mbsalign(void** rdi, void** rsi, ...);

uint32_t fun_48c0();

signed char use_abformat = 0;

void abformat_init(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rsi6;
    void*** rsp7;
    void* rax8;
    void* v9;
    void** rdi10;
    void** v11;
    struct s24* rax12;
    void** rdx13;
    uint32_t ecx14;
    uint64_t r14_15;
    int64_t v16;
    void** v17;
    void** v18;
    void** rbx19;
    void** r12_20;
    uint64_t rbp21;
    int64_t rdi22;
    void** rax23;
    void** rax24;
    void** rax25;
    void* rax26;
    uint32_t r13d27;
    void** r12_28;
    void** rbp29;
    void** rax30;
    void** rax31;
    void** rax32;
    void** rax33;
    int64_t v34;
    void** r15_35;
    uint32_t eax36;

    *reinterpret_cast<int32_t*>(&rsi6) = 0;
    *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
    rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x658);
    rax8 = g28;
    v9 = rax8;
    rdi10 = reinterpret_cast<void**>(rsp7 + 48);
    v11 = rdi10;
    do {
        rax12 = *reinterpret_cast<struct s24**>(rsi6 + 0x25040);
        *reinterpret_cast<uint32_t*>(&rdx13) = rax12->f0;
        if (!*reinterpret_cast<signed char*>(&rdx13)) {
            addr_6d2b_3:
            *reinterpret_cast<int32_t*>(&rax12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        } else {
            do {
                ecx14 = rax12->f1;
                if (*reinterpret_cast<signed char*>(&rdx13) == 37) {
                    if (*reinterpret_cast<signed char*>(&ecx14) == 37) {
                        *reinterpret_cast<uint32_t*>(&rdx13) = rax12->f2;
                        rax12 = reinterpret_cast<struct s24*>(&rax12->f1);
                    } else {
                        if (*reinterpret_cast<signed char*>(&ecx14) != 98) 
                            goto addr_6d21_9; else 
                            break;
                    }
                } else {
                    addr_6d21_9:
                    *reinterpret_cast<uint32_t*>(&rdx13) = ecx14;
                }
                rax12 = reinterpret_cast<struct s24*>(&rax12->f1);
            } while (*reinterpret_cast<signed char*>(&rdx13));
            goto addr_6d2b_3;
        }
        *reinterpret_cast<struct s24**>(reinterpret_cast<unsigned char>(rdi10) + reinterpret_cast<unsigned char>(rsi6)) = rax12;
        rsi6 = rsi6 + 8;
    } while (!reinterpret_cast<int1_t>(rsi6 == 16));
    *reinterpret_cast<int32_t*>(&r14_15) = 12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_15) + 4) = 0;
    if (!1 || v16) {
        v17 = reinterpret_cast<void**>(rsp7 + 64);
        v18 = reinterpret_cast<void**>(rsp7 + 0x640);
        while (1) {
            rbx19 = v17;
            *reinterpret_cast<int32_t*>(&r12_20) = 0x2000e;
            *reinterpret_cast<int32_t*>(&rbp21) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp21) + 4) = 0;
            do {
                *reinterpret_cast<int32_t*>(&rdi22) = *reinterpret_cast<int32_t*>(&r12_20);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                rax23 = fun_4ab0(rdi22);
                *reinterpret_cast<int32_t*>(&rsi6) = 37;
                *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
                rdi10 = rax23;
                rax24 = fun_48a0(rdi10, rdi10);
                if (rax24) 
                    goto addr_6e24_16;
                fun_4c70(rdi10, 37);
                rsi6 = rbx19;
                rdi10 = rax23;
                *reinterpret_cast<uint32_t*>(&rdx13) = 0x80;
                rax25 = mbsalign(rdi10, rsi6);
                if (reinterpret_cast<unsigned char>(rax25) > reinterpret_cast<unsigned char>(0x7f)) 
                    goto addr_6e24_16;
                if (rbp21 < r14_15) {
                    rbp21 = r14_15;
                }
                *reinterpret_cast<int32_t*>(&r12_20) = *reinterpret_cast<int32_t*>(&r12_20) + 1;
                rbx19 = rbx19 - 0xffffffffffffff80;
            } while (rbx19 != v18);
            if (r14_15 <= rbp21) 
                goto addr_6e5b_22;
            r14_15 = rbp21;
        }
    }
    addr_6e24_16:
    rax26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v9) - reinterpret_cast<uint64_t>(g28));
    if (!rax26) {
        return;
    }
    fun_4870();
    r13d27 = *reinterpret_cast<uint32_t*>(&rdx13);
    r12_28 = rsi6;
    rbp29 = rdi10;
    rax30 = xmalloc(32, rsi6);
    if (r12_28) 
        goto addr_6f65_28;
    addr_6f70_29:
    *reinterpret_cast<void***>(rax30 + 8) = r12_28;
    if (rbp29) {
        rax31 = xstrdup(rbp29, rsi6);
        rbp29 = rax31;
    }
    rax32 = pending_dirs;
    *reinterpret_cast<void***>(rax30) = rbp29;
    *reinterpret_cast<void***>(rax30 + 16) = *reinterpret_cast<void***>(&r13d27);
    *reinterpret_cast<void***>(rax30 + 24) = rax32;
    pending_dirs = rax30;
    goto v18;
    addr_6f65_28:
    rax33 = xstrdup(r12_28, rsi6);
    r12_28 = rax33;
    goto addr_6f70_29;
    addr_6e5b_22:
    r12_20 = v18;
    v34 = 0;
    while (1) {
        rbx19 = v17;
        rdi10 = reinterpret_cast<void**>(v34 * 0x600);
        rbp21 = *reinterpret_cast<uint64_t*>(0x25040 + v34 * 8);
        rsi6 = v11;
        r15_35 = rdi10 + 0x253e0;
        do {
            if (0) {
                if (reinterpret_cast<int64_t>(-rbp21) > reinterpret_cast<int64_t>(0x80)) 
                    goto addr_6e24_16;
                *reinterpret_cast<uint32_t*>(&rdx13) = 1;
                rdi10 = r15_35;
                *reinterpret_cast<int32_t*>(&rsi6) = 0x80;
                *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
                eax36 = fun_46c0();
            } else {
                rdx13 = reinterpret_cast<void**>("%s");
                *reinterpret_cast<int32_t*>(&rsi6) = 0x80;
                *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
                rdi10 = r15_35;
                eax36 = fun_48c0();
            }
            if (eax36 > 0x7f) 
                goto addr_6e24_16;
            rbx19 = rbx19 - 0xffffffffffffff80;
            r15_35 = r15_35 - 0xffffffffffffff80;
        } while (rbx19 != r12_20);
        if (v34 == 1) 
            break;
        v34 = 1;
    }
    use_abformat = 1;
    goto addr_6e24_16;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x25280;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

uint32_t get_quoting_style();

unsigned char qmark_funny_chars = 0;

uint32_t mbsnwidth(void** rdi, void** rsi);

void** rpl_mbrtowc(void* rdi, void** rsi, ...);

int32_t fun_4a90();

uint32_t fun_4c30(void** rdi, void** rsi, ...);

/* quote_name_buf.constprop.0 */
void** quote_name_buf_constprop_0(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rbx8;
    void*** v9;
    void** v10;
    void** v11;
    void* rax12;
    void* v13;
    void** v14;
    uint32_t eax15;
    void* rsp16;
    unsigned char al17;
    unsigned char v18;
    int32_t r13d19;
    void** rdi20;
    void** rax21;
    void* rsp22;
    void** r12_23;
    void** r15_24;
    void** rax25;
    uint32_t eax26;
    void** rax27;
    int1_t zf28;
    void** rbx29;
    uint64_t rax30;
    uint32_t eax31;
    uint16_t** rax32;
    uint16_t* rcx33;
    void** rax34;
    int64_t rdx35;
    uint32_t edx36;
    int1_t zf37;
    uint64_t rax38;
    void** rsi39;
    void** v40;
    uint32_t eax41;
    uint32_t eax42;
    void* rax43;
    uint16_t** rax44;
    void** rdx45;
    int64_t rsi46;
    void** r14_47;
    void** r12_48;
    int64_t rax49;
    int32_t edx50;
    void* r15_51;
    void** rax52;
    void** rsi53;
    void** rdx54;
    void** r13_55;
    int32_t eax56;
    void* rsp57;
    void** rdx58;
    uint32_t edi59;
    uint32_t eax60;
    int32_t edx61;
    void** rax62;
    void* rsp63;
    void** r12_64;
    void** rax65;
    void** rax66;

    rbp7 = rsi;
    rbx8 = rdx;
    v9 = rdi;
    v10 = r8;
    v11 = r9;
    rax12 = g28;
    v13 = rax12;
    v14 = *rdi;
    eax15 = get_quoting_style();
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 - 8 + 8);
    al17 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax15 <= 2)) & qmark_funny_chars);
    v18 = al17;
    if (al17) {
        if (*reinterpret_cast<int32_t*>(&rcx)) {
            while (1) {
                r13d19 = 1;
                addr_7912_4:
                rdi20 = v14;
                *reinterpret_cast<int32_t*>(&rsi) = 0x2000;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                rax21 = quotearg_buffer(rdi20, 0x2000, rbp7, -1, rbx8);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                r12_23 = rax21;
                if (reinterpret_cast<unsigned char>(rax21) > reinterpret_cast<unsigned char>(0x1fff)) {
                    r15_24 = rax21 + 1;
                    rax25 = xmalloc(r15_24, 0x2000, r15_24, 0x2000);
                    rsi = r15_24;
                    rdi20 = rax25;
                    v14 = rax25;
                    quotearg_buffer(rdi20, rsi, rbp7, -1, rbx8);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8);
                }
                v18 = 1;
                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v14));
                if (*reinterpret_cast<void***>(rbp7) == *reinterpret_cast<void***>(&eax26)) {
                    rdi20 = rbp7;
                    rax27 = fun_4860(rdi20, rdi20);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    v18 = reinterpret_cast<uint1_t>(r12_23 != rax27);
                }
                if (!r13d19) {
                    addr_7980_9:
                    if (!v10) {
                        zf28 = align_variable_outer_quotes == 0;
                        if (!zf28) {
                            *reinterpret_cast<int32_t*>(&rbx29) = 0;
                            *reinterpret_cast<int32_t*>(&rbx29 + 4) = 0;
                        } else {
                            *reinterpret_cast<void***>(v11) = reinterpret_cast<void**>(0);
                            goto addr_79fa_13;
                        }
                    } else {
                        rax30 = fun_4850();
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        if (rax30 > 1) {
                            eax31 = mbsnwidth(v14, r12_23);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rbx29 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax31)));
                        } else {
                            rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<unsigned char>(r12_23));
                            if (reinterpret_cast<unsigned char>(rbp7) <= reinterpret_cast<unsigned char>(v14)) {
                                *reinterpret_cast<int32_t*>(&rbx29) = 0;
                                *reinterpret_cast<int32_t*>(&rbx29 + 4) = 0;
                            } else {
                                rax32 = fun_4c70(rdi20, rsi, rdi20, rsi);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                rcx33 = *rax32;
                                rax34 = v14;
                                *reinterpret_cast<int32_t*>(&rbx29) = 0;
                                *reinterpret_cast<int32_t*>(&rbx29 + 4) = 0;
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx35) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax34));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
                                    edx36 = rcx33[rdx35];
                                    rbx29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx29) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbx29) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&edx36) & 0x4000) < 1)))))));
                                    ++rax34;
                                } while (rbp7 != rax34);
                            }
                        }
                        zf37 = align_variable_outer_quotes == 0;
                        if (zf37) 
                            goto addr_79ea_21;
                    }
                } else {
                    rbx29 = r12_23;
                    addr_7a89_23:
                    rax38 = fun_4850();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rsi39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<unsigned char>(rbx29));
                    v40 = rsi39;
                    if (rax38 <= 1) 
                        goto addr_7bc0_24; else 
                        goto addr_7aa6_25;
                }
                addr_7b9a_26:
                eax41 = static_cast<uint32_t>(v18) ^ 1;
                *reinterpret_cast<void***>(&eax42) = reinterpret_cast<void**>(*reinterpret_cast<unsigned char*>(&eax41) & cwd_some_quoted);
                goto addr_7ba8_27;
                addr_79fa_13:
                rbx8 = v14;
                *v9 = rbx8;
                rax43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v13) - reinterpret_cast<uint64_t>(g28));
                if (!rax43) 
                    break;
                fun_4870();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                continue;
                addr_79ea_21:
                *reinterpret_cast<void***>(v11) = reinterpret_cast<void**>(0);
                goto addr_79f2_29;
                addr_7bc0_24:
                if (reinterpret_cast<unsigned char>(v14) < reinterpret_cast<unsigned char>(v40)) {
                    rax44 = fun_4c70(rdi20, rsi39, rdi20, rsi39);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rdx45 = v14;
                    do {
                        *reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx45));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                        if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax44 + rsi46) + 1) & 64)) {
                            *reinterpret_cast<void***>(rdx45) = reinterpret_cast<void**>(63);
                        }
                        ++rdx45;
                    } while (rdx45 != v40);
                }
                r12_23 = rbx29;
                addr_7bfe_35:
                eax42 = align_variable_outer_quotes;
                if (!*reinterpret_cast<void***>(&eax42)) {
                    addr_7ba8_27:
                    *reinterpret_cast<void***>(v11) = *reinterpret_cast<void***>(&eax42);
                    if (v10) {
                        addr_79f2_29:
                        *reinterpret_cast<void***>(v10) = rbx29;
                        goto addr_79fa_13;
                    } else {
                        goto addr_79fa_13;
                    }
                } else {
                    goto addr_7b9a_26;
                }
                addr_7aa6_25:
                if (reinterpret_cast<unsigned char>(v14) >= reinterpret_cast<unsigned char>(rsi39)) {
                    *reinterpret_cast<int32_t*>(&r12_23) = 0;
                    *reinterpret_cast<int32_t*>(&r12_23 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rbx29) = 0;
                    *reinterpret_cast<int32_t*>(&rbx29 + 4) = 0;
                    goto addr_7bfe_35;
                } else {
                    r14_47 = v14;
                    r12_48 = v14;
                    rbp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 80);
                    *reinterpret_cast<int32_t*>(&rbx29) = 0;
                    *reinterpret_cast<int32_t*>(&rbx29 + 4) = 0;
                    do {
                        *reinterpret_cast<uint32_t*>(&rax49) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
                        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax49)) > reinterpret_cast<signed char>(95)) {
                            edx50 = static_cast<int32_t>(rax49 - 97);
                            if (*reinterpret_cast<unsigned char*>(&edx50) <= 29) {
                                addr_7c1c_42:
                                *reinterpret_cast<void***>(r14_47) = *reinterpret_cast<void***>(&rax49);
                                ++r12_48;
                                ++rbx29;
                                ++r14_47;
                                continue;
                            }
                        } else {
                            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax49)) > reinterpret_cast<signed char>(64)) 
                                goto addr_7c1c_42;
                            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax49)) > reinterpret_cast<signed char>(35)) 
                                goto addr_7c10_46; else 
                                goto addr_7add_47;
                        }
                        addr_7ae5_48:
                        r15_51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) + 76);
                        do {
                            rax52 = rpl_mbrtowc(r15_51, r12_48, r15_51, r12_48);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rsi53 = r14_47 + 1;
                            if (rax52 == 0xffffffffffffffff) 
                                break;
                            if (rax52 == 0xfffffffffffffffe) 
                                goto addr_7c88_51;
                            *reinterpret_cast<int32_t*>(&rdx54) = 1;
                            *reinterpret_cast<int32_t*>(&rdx54 + 4) = 0;
                            if (rax52) {
                                rdx54 = rax52;
                            }
                            r13_55 = rdx54;
                            eax56 = fun_4a90();
                            rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            if (eax56 < 0) {
                                *reinterpret_cast<void***>(r14_47) = reinterpret_cast<void**>(63);
                                r14_47 = rsi53;
                                r12_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_48) + reinterpret_cast<unsigned char>(r13_55));
                                ++rbx29;
                            } else {
                                *reinterpret_cast<int32_t*>(&rdx58) = 0;
                                *reinterpret_cast<int32_t*>(&rdx58 + 4) = 0;
                                do {
                                    edi59 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r12_48) + reinterpret_cast<unsigned char>(rdx58));
                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_47) + reinterpret_cast<unsigned char>(rdx58)) = *reinterpret_cast<signed char*>(&edi59);
                                    ++rdx58;
                                } while (r13_55 != rdx58);
                                r12_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_48) + reinterpret_cast<unsigned char>(r13_55));
                                r14_47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_47) + reinterpret_cast<unsigned char>(r13_55));
                                rbx29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx29) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax56)));
                            }
                            eax60 = fun_4c30(rbp7, rsi53, rbp7, rsi53);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
                        } while (!eax60);
                        continue;
                        ++r12_48;
                        ++rbx29;
                        addr_7c78_61:
                        *reinterpret_cast<void***>(r14_47) = reinterpret_cast<void**>(63);
                        r14_47 = rsi53;
                        continue;
                        addr_7c88_51:
                        r12_48 = v40;
                        ++rbx29;
                        goto addr_7c78_61;
                        addr_7c10_46:
                        edx61 = static_cast<int32_t>(rax49 - 37);
                        if (*reinterpret_cast<unsigned char*>(&edx61) > 26) 
                            goto addr_7ae5_48; else 
                            goto addr_7c1c_42;
                        addr_7add_47:
                        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rax49)) > reinterpret_cast<signed char>(31)) 
                            goto addr_7c1c_42; else 
                            goto addr_7ae5_48;
                    } while (reinterpret_cast<unsigned char>(r12_48) < reinterpret_cast<unsigned char>(v40));
                    r12_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_47) - reinterpret_cast<unsigned char>(v14));
                    goto addr_7bfe_35;
                }
            }
            return r12_23;
        } else {
            rax62 = fun_4860(rbp7);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
            rbx29 = rax62;
            r12_64 = rax62 + 1;
            if (reinterpret_cast<unsigned char>(rax62) > reinterpret_cast<unsigned char>(0x1fff)) {
                rax65 = xmalloc(r12_64, rsi);
                rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                v14 = rax65;
            }
            rdi20 = v14;
            fun_4a30(rdi20, rbp7, r12_64, rcx);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            v18 = 0;
            goto addr_7a89_23;
        }
    } else {
        r13d19 = 0;
        if (*reinterpret_cast<int32_t*>(&rcx)) 
            goto addr_7912_4;
        rdi20 = rbp7;
        rax66 = fun_4860(rdi20);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        v14 = rbp7;
        r12_23 = rax66;
        goto addr_7980_9;
    }
}

uint32_t format_user_width(int32_t edi, ...) {
    int1_t zf2;
    void** rax3;
    uint32_t eax4;

    zf2 = numeric_ids == 0;
    if (!(!zf2 || (rax3 = getuser(), rax3 == 0))) {
        eax4 = gnu_mbswidth(rax3);
        if (reinterpret_cast<int32_t>(eax4) < reinterpret_cast<int32_t>(0)) {
            eax4 = 0;
        }
        return eax4;
    }
}

void filemodestring(void*** rdi, void* rsi);

void** localtz = reinterpret_cast<void**>(0);

int64_t localtime_rz(void** rdi, void* rsi, void** rdx, void** rcx);

void** imaxtostr(uint64_t rdi, void* rsi, void** rdx, void** rcx, void** r8, void** r9);

/* width.2 */
uint32_t width_2 = 0xffffffff;

void** long_time_format = reinterpret_cast<void**>(0xdd);

void** nstrftime(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

void** g26378 = reinterpret_cast<void**>(0);

uint64_t current_time = 0;

void gettime(int64_t rdi);

void print_long_format(void** rdi) {
    void* rsp2;
    void* rax3;
    void* v4;
    void** rbp5;
    void** v6;
    int1_t zf7;
    uint32_t eax8;
    int1_t below_or_equal9;
    void** rax10;
    int32_t r13d11;
    uint64_t v12;
    void** v13;
    uint64_t rax14;
    void** rdx15;
    void** rax16;
    void** rax17;
    void** r12_18;
    int1_t zf19;
    void** rbx20;
    int32_t eax21;
    int1_t zf22;
    void** r15_23;
    void** rdi24;
    void** r8_25;
    void* rsi26;
    int64_t rdx27;
    void** rax28;
    uint32_t r14d29;
    uint32_t eax30;
    uint32_t eax31;
    void** rdi32;
    uint32_t eax33;
    void** r9_34;
    void** rcx35;
    int32_t eax36;
    void* rsp37;
    void** rbx38;
    int1_t zf39;
    int1_t zf40;
    int1_t zf41;
    int1_t zf42;
    int1_t zf43;
    void** rsi44;
    int1_t zf45;
    void** rdx46;
    void** rdi47;
    void** rsi48;
    int1_t zf49;
    void** rax50;
    int1_t zf51;
    int1_t zf52;
    void** rdx53;
    void** rdi54;
    void** rsi55;
    int1_t zf56;
    int64_t rdi57;
    void** rax58;
    int1_t zf59;
    void** rdx60;
    void** rdi61;
    void** rsi62;
    int1_t zf63;
    void** rax64;
    int1_t zf65;
    int1_t zf66;
    int1_t zf67;
    void** r15_68;
    int64_t rdx69;
    int64_t rax70;
    uint32_t r14d71;
    uint32_t r14d72;
    int32_t edx73;
    void** rax74;
    void** rdx75;
    uint32_t eax76;
    uint32_t tmp32_77;
    void** r8_78;
    int32_t eax79;
    void* rsp80;
    void** rdi81;
    void** r8_82;
    void* rsi83;
    int64_t rdx84;
    void** rax85;
    void** rdi86;
    void** rdx87;
    uint32_t r14d88;
    uint32_t eax89;
    uint32_t eax90;
    void** rdi91;
    uint32_t eax92;
    void** r13_93;
    void** rdi94;
    void** rdx95;
    int64_t rax96;
    void** rax97;
    void** r13_98;
    void** rax99;
    void** r14_100;
    void** rdi101;
    int64_t rax102;
    int1_t zf103;
    void** r8_104;
    void** rdx105;
    int32_t v106;
    void** r15_107;
    void** rax108;
    int32_t eax109;
    int32_t r8d110;
    uint64_t rdx111;
    uint1_t less112;
    int1_t less_or_equal113;
    uint64_t rcx114;
    int32_t edi115;
    uint32_t eax116;
    int64_t rsi117;
    int64_t rdi118;
    uint1_t less119;
    int1_t less_or_equal120;
    uint64_t rdi121;
    int64_t rax122;
    int64_t rcx123;
    int64_t rdx124;
    uint32_t eax125;
    int64_t rdx126;
    int1_t zf127;
    int64_t rax128;
    int64_t rax129;
    int32_t v130;
    void** rax131;
    void** rax132;
    void* rax133;
    uint32_t edx134;
    void** rcx135;
    uint32_t edx136;
    void** esi137;
    void** al138;
    void* rax139;
    void** rdi140;
    void** tmp64_141;
    void** rdx142;
    uint32_t eax143;
    void** esi144;
    uint32_t edi145;
    uint32_t eax146;

    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - reinterpret_cast<int64_t>("x86-64.so.2"));
    rax3 = g28;
    v4 = rax3;
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(rdi + 0xb8)) {
        v6 = reinterpret_cast<void**>(0xc741);
        filemodestring(rdi + 24, reinterpret_cast<int64_t>(rsp2) + 0xb4);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
    }
    zf7 = any_has_acl == 0;
    if (!zf7) {
        if (*reinterpret_cast<int32_t*>(rbp5 + 0xbc) != 1) {
            if (*reinterpret_cast<int32_t*>(rbp5 + 0xbc) == 2) {
                eax8 = time_type;
                below_or_equal9 = eax8 <= 2;
                if (eax8 != 2) 
                    goto addr_c765_9;
                goto addr_c980_11;
            }
        }
    }
    eax8 = time_type;
    below_or_equal9 = eax8 <= 2;
    if (eax8 == 2) {
        addr_c980_11:
        rax10 = *reinterpret_cast<void***>(rbp5 + 0x68);
        r13d11 = 1;
        v12 = *reinterpret_cast<uint64_t*>(rbp5 + 96);
        v13 = rax10;
    } else {
        addr_c765_9:
        if (!below_or_equal9) {
            if (eax8 != 3) {
                addr_d115_15:
                goto addr_4c96_16;
            } else {
                rax14 = *reinterpret_cast<uint64_t*>(rbp5 + 0x70);
                rdx15 = *reinterpret_cast<void***>(rbp5 + 0x78);
                v12 = rax14;
                v13 = rdx15;
                *reinterpret_cast<unsigned char*>(&r13d11) = reinterpret_cast<uint1_t>((rax14 & reinterpret_cast<unsigned char>(rdx15)) != 0xffffffffffffffff);
            }
        } else {
            if (!eax8) {
                rax16 = *reinterpret_cast<void***>(rbp5 + 0x78);
                r13d11 = 1;
                v12 = *reinterpret_cast<uint64_t*>(rbp5 + 0x70);
                v13 = rax16;
            } else {
                rax17 = *reinterpret_cast<void***>(rbp5 + 0x88);
                r13d11 = 1;
                v12 = *reinterpret_cast<uint64_t*>(rbp5 + 0x80);
                v13 = rax17;
            }
        }
    }
    r12_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp2) + 0x4d0);
    zf19 = print_inode == 0;
    rbx20 = r12_18;
    if (!zf19) {
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && *reinterpret_cast<void***>(rbp5 + 32)) {
            umaxtostr();
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        }
        r12_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp2) + 0x4d0);
        v6 = reinterpret_cast<void**>(0xcc3a);
        eax21 = fun_4c80(r12_18, 1, 0xe3b, "%*s ", r12_18, 1, 0xe3b, "%*s ");
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rbx20 = reinterpret_cast<void**>(static_cast<int64_t>(eax21) + reinterpret_cast<unsigned char>(r12_18));
    }
    zf22 = print_block_size == 0;
    if (!zf22) {
        r15_23 = reinterpret_cast<void**>("?");
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
            rdi24 = *reinterpret_cast<void***>(rbp5 + 88);
            r8_25 = output_block_size;
            rsi26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xe0);
            *reinterpret_cast<int32_t*>(&rdx27) = human_output_opts;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            rax28 = human_readable(rdi24, rsi26, rdx27, 0x200, r8_25, rdi24, rsi26, rdx27, 0x200, r8_25);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            r15_23 = rax28;
        }
        r14d29 = block_size_width;
        v6 = reinterpret_cast<void**>(0xc7d7);
        eax30 = gnu_mbswidth(r15_23, r15_23);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        eax31 = r14d29 - eax30;
        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax31) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax31 == 0))) {
            rdi32 = rbx20;
            rbx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax31))));
            v6 = reinterpret_cast<void**>(0xc7fa);
            fun_4920(rdi32, rdi32);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        }
        do {
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_23));
            ++r15_23;
            ++rbx20;
            *reinterpret_cast<void***>(rbx20 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax33);
        } while (*reinterpret_cast<void***>(&eax33));
        *reinterpret_cast<void***>(rbx20 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
    }
    if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
        v6 = reinterpret_cast<void**>(0xcbd1);
        umaxtostr();
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
    }
    *reinterpret_cast<int32_t*>(&r9_34) = nlink_width;
    *reinterpret_cast<int32_t*>(&r9_34 + 4) = 0;
    rcx35 = reinterpret_cast<void**>("%s %*s ");
    eax36 = fun_4c80(rbx20, 1, -1, "%s %*s ", rbx20, 1, -1, "%s %*s ");
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 - 8 - 8 + 8 + 8 + 8);
    rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax36)));
    zf39 = dired == 0;
    if (!zf39) {
        dired_outbuf("  ", 2, "  ", 2);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    }
    zf40 = print_owner == 0;
    if (zf40 && ((zf41 = print_group == 0, zf41) && (zf42 = print_author == 0, zf42))) {
        zf43 = print_scontext == 0;
        if (zf43) 
            goto addr_c8c6_38;
    }
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) - reinterpret_cast<unsigned char>(r12_18));
    dired_outbuf(r12_18, rsi44, r12_18, rsi44);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf45 = print_owner == 0;
    if (!zf45) {
        *reinterpret_cast<uint32_t*>(&rdx46) = owner_width;
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        rdi47 = reinterpret_cast<void**>("?");
        *reinterpret_cast<int32_t*>(&rsi48) = *reinterpret_cast<int32_t*>(rbp5 + 52);
        *reinterpret_cast<int32_t*>(&rsi48 + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi47) = 0, *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0, zf49 = numeric_ids == 0, zf49)) {
            rax50 = getuser();
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
            rsi48 = rsi48;
            *reinterpret_cast<uint32_t*>(&rdx46) = *reinterpret_cast<uint32_t*>(&rdx46);
            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
            rdi47 = rax50;
        }
        format_user_or_group(rdi47, rsi48, rdx46, "%s %*s ", v6, r9_34);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        zf51 = print_group == 0;
        if (zf51) 
            goto addr_c8a9_44; else 
            goto addr_cc77_45;
    }
    zf52 = print_group == 0;
    if (!zf52) {
        addr_cc77_45:
        *reinterpret_cast<uint32_t*>(&rdx53) = group_width;
        *reinterpret_cast<int32_t*>(&rdx53 + 4) = 0;
        rdi54 = reinterpret_cast<void**>("?");
        *reinterpret_cast<int32_t*>(&rsi55) = *reinterpret_cast<int32_t*>(rbp5 + 56);
        *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi54) = 0, *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0, zf56 = numeric_ids == 0, zf56)) {
            *reinterpret_cast<int32_t*>(&rdi57) = *reinterpret_cast<int32_t*>(&rsi55);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
            rax58 = getgroup(rdi57, rdi57);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
            rsi55 = rsi55;
            *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(&rdx53);
            *reinterpret_cast<int32_t*>(&rdx53 + 4) = 0;
            rdi54 = rax58;
        }
    } else {
        addr_c8a9_44:
        zf59 = print_author == 0;
        if (!zf59) {
            addr_cca6_48:
            *reinterpret_cast<uint32_t*>(&rdx60) = author_width;
            *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
            rdi61 = reinterpret_cast<void**>("?");
            *reinterpret_cast<int32_t*>(&rsi62) = *reinterpret_cast<int32_t*>(rbp5 + 52);
            *reinterpret_cast<int32_t*>(&rsi62 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(rbp5 + 0xb8) && (*reinterpret_cast<int32_t*>(&rdi61) = 0, *reinterpret_cast<int32_t*>(&rdi61 + 4) = 0, zf63 = numeric_ids == 0, zf63)) {
                rax64 = getuser();
                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                rsi62 = rsi62;
                *reinterpret_cast<uint32_t*>(&rdx60) = *reinterpret_cast<uint32_t*>(&rdx60);
                *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
                rdi61 = rax64;
                goto addr_ccc3_50;
            }
        } else {
            addr_c8b6_51:
            zf65 = print_scontext == 0;
            rbx38 = r12_18;
            if (!zf65) 
                goto addr_ccd8_52; else 
                goto addr_c8c6_38;
        }
    }
    format_user_or_group(rdi54, rsi55, rdx53, "%s %*s ", v6, r9_34);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf66 = print_author == 0;
    if (zf66) 
        goto addr_c8b6_51; else 
        goto addr_cca6_48;
    addr_ccc3_50:
    format_user_or_group(rdi61, rsi62, rdx60, "%s %*s ", v6, r9_34);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    zf67 = print_scontext == 0;
    rbx38 = r12_18;
    if (zf67) {
        addr_c8c6_38:
        if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
            r15_68 = reinterpret_cast<void**>("?");
        } else {
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 48)) & 0xb000) == 0x2000) {
                *reinterpret_cast<uint32_t*>(&rdx69) = major_device_number_width;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx69) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax70) = minor_device_number_width;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax70) + 4) = 0;
                r14d71 = file_size_width;
                r14d72 = r14d71 - static_cast<uint32_t>(rdx69 + rax70 + 2);
                umaxtostr();
                edx73 = minor_device_number_width;
                rax74 = umaxtostr();
                *reinterpret_cast<int32_t*>(&rdx75) = edx73;
                *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
                r9_34 = rax74;
                eax76 = 0;
                if (reinterpret_cast<int32_t>(r14d72) >= reinterpret_cast<int32_t>(0)) {
                    eax76 = r14d72;
                }
                tmp32_77 = eax76 + major_device_number_width;
                *reinterpret_cast<uint32_t*>(&r8_78) = tmp32_77;
                *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
                eax79 = fun_4c80(rbx38, 1, -1, "%*s, %*s ");
                rcx35 = rdx75;
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
                rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax79)));
                goto addr_ca42_59;
            } else {
                rdi81 = *reinterpret_cast<void***>(rbp5 + 72);
                r8_82 = file_output_block_size;
                rsi83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) + 0xe0);
                *reinterpret_cast<int32_t*>(&rcx35) = 1;
                *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx84) = file_human_output_opts;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx84) + 4) = 0;
                rax85 = human_readable(rdi81, rsi83, rdx84, 1, r8_82, rdi81, rsi83, rdx84, 1, r8_82);
                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                r15_68 = rax85;
            }
        }
    } else {
        addr_ccd8_52:
        rdi86 = *reinterpret_cast<void***>(rbp5 + 0xb0);
        *reinterpret_cast<int32_t*>(&rdx87) = scontext_width;
        *reinterpret_cast<int32_t*>(&rdx87 + 4) = 0;
        format_user_or_group(rdi86, 0, rdx87, "%s %*s ", v6, r9_34);
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        goto addr_c8c6_38;
    }
    r14d88 = file_size_width;
    eax89 = gnu_mbswidth(r15_68, r15_68);
    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&r8_78) = eax89;
    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
    eax90 = r14d88 - *reinterpret_cast<uint32_t*>(&r8_78);
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax90) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax90 == 0))) {
        rdi91 = rbx38;
        rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax90))));
        fun_4920(rdi91, rdi91);
        rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
    }
    do {
        eax92 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_68));
        ++r15_68;
        ++rbx38;
        *reinterpret_cast<void***>(rbx38 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax92);
    } while (*reinterpret_cast<void***>(&eax92));
    *reinterpret_cast<void***>(rbx38 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
    addr_ca42_59:
    *reinterpret_cast<void***>(rbx38) = reinterpret_cast<void**>(1);
    if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) 
        goto addr_ca57_65;
    if (!*reinterpret_cast<unsigned char*>(&r13d11)) 
        goto addr_ca57_65;
    r13_93 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 48);
    rdi94 = localtz;
    rdx95 = r13_93;
    rax96 = localtime_rz(rdi94, reinterpret_cast<int64_t>(rsp80) + 32, rdx95, rcx35);
    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
    if (!rax96) {
        addr_d070_68:
        if (0) {
            addr_cf00_69:
            *reinterpret_cast<void***>(rbx38) = reinterpret_cast<void**>(32);
            rax97 = rbx38 + 1;
        } else {
            if (!*reinterpret_cast<unsigned char*>(rbp5 + 0xb8)) {
                addr_ca57_65:
                r13_98 = reinterpret_cast<void**>("?");
            } else {
                rax99 = imaxtostr(v12, reinterpret_cast<int64_t>(rsp80) + 0xc0, rdx95, rcx35, r8_78, r9_34);
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
                r13_98 = rax99;
            }
            *reinterpret_cast<uint32_t*>(&r8_78) = width_2;
            *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r8_78) < reinterpret_cast<int32_t>(0)) {
                r14_100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 0x70);
                rdi101 = localtz;
                rax102 = localtime_rz(rdi101, reinterpret_cast<int64_t>(rsp80) + 24, r14_100, rcx35);
                rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
                if (!rax102) {
                    addr_d044_74:
                    *reinterpret_cast<uint32_t*>(&r8_78) = width_2;
                    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
                    goto addr_d04b_75;
                } else {
                    zf103 = use_abformat == 0;
                    r8_104 = localtz;
                    rdx105 = long_time_format;
                    if (!zf103) {
                        rdx105 = reinterpret_cast<void**>((static_cast<int64_t>(v106) << 7) + 0x253e0);
                    }
                    r15_107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp80) + 0xe0);
                    rax108 = nstrftime(r15_107, 0x3e9, rdx105, r14_100, r8_104);
                    if (rax108) 
                        goto addr_d0b0_79; else 
                        goto addr_d044_74;
                }
            } else {
                addr_ca6e_80:
                r9_34 = r13_98;
                eax109 = fun_4c80(rbx38, 1, -1, "%*s ");
                rax97 = reinterpret_cast<void**>(static_cast<int64_t>(eax109) + reinterpret_cast<unsigned char>(rbx38));
            }
        }
    } else {
        r8d110 = 0;
        r9_34 = v13;
        rdx111 = v12;
        less112 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(g26378) < reinterpret_cast<signed char>(r9_34));
        less_or_equal113 = reinterpret_cast<signed char>(g26378) <= reinterpret_cast<signed char>(r9_34);
        rcx114 = current_time;
        *reinterpret_cast<unsigned char*>(&r8d110) = reinterpret_cast<uint1_t>(!less_or_equal113);
        edi115 = 0;
        eax116 = less112;
        *reinterpret_cast<uint32_t*>(&rsi117) = r8d110 - eax116;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi117) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&edi115) = reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) > reinterpret_cast<int64_t>(rdx111));
        *reinterpret_cast<uint32_t*>(&rdi118) = edi115 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) < reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi118) + 4) = 0;
        if (static_cast<int32_t>(rsi117 + rdi118 * 2) < 0) {
            gettime(0x26370);
            rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
            r8d110 = 0;
            r9_34 = v13;
            less119 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r9_34) < reinterpret_cast<signed char>(g26378));
            less_or_equal120 = reinterpret_cast<signed char>(r9_34) <= reinterpret_cast<signed char>(g26378);
            *reinterpret_cast<unsigned char*>(&r8d110) = less119;
            rdx111 = v12;
            rcx114 = current_time;
            eax116 = reinterpret_cast<uint1_t>(!less_or_equal120);
            *reinterpret_cast<uint32_t*>(&rsi117) = r8d110 - eax116;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi117) + 4) = 0;
        }
        rdi121 = rcx114 + 0xffffffffff0f3d54;
        *reinterpret_cast<uint32_t*>(&rax122) = eax116 - r8d110;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
        r8_78 = localtz;
        *reinterpret_cast<uint32_t*>(&rcx123) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) < reinterpret_cast<int64_t>(rdx111))) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rcx114) > reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx123) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx124) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rdi121) > reinterpret_cast<int64_t>(rdx111))) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rdi121) < reinterpret_cast<int64_t>(rdx111)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx124) + 4) = 0;
        eax125 = static_cast<uint32_t>(rax122 + rcx123 * 2) & static_cast<uint32_t>(rsi117 + rdx124 * 2);
        *reinterpret_cast<uint32_t*>(&rdx126) = eax125 >> 31;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx126) + 4) = 0;
        zf127 = use_abformat == 0;
        if (zf127) {
            rdx95 = *reinterpret_cast<void***>(0x25040 + rdx126 * 8);
        } else {
            rax128 = static_cast<int64_t>(reinterpret_cast<int32_t>(eax125)) >> 63;
            *reinterpret_cast<uint32_t*>(&rax129) = *reinterpret_cast<uint32_t*>(&rax128) & 12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax129) + 4) = 0;
            rdx95 = reinterpret_cast<void**>(0x253e0 + (rax129 + reinterpret_cast<uint64_t>(static_cast<int64_t>(v130)) << 7));
        }
        rcx35 = r13_93;
        rax131 = nstrftime(rbx38, 0x3e9, rdx95, rcx35, r8_78);
        rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8);
        if (!rax131) 
            goto addr_d070_68; else 
            goto addr_cefb_87;
    }
    rax132 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax97) - reinterpret_cast<unsigned char>(r12_18));
    dired_outbuf(r12_18, rax132, r12_18, rax132);
    rax133 = print_name_with_quoting(rbp5, 0, 0x261a0, rax132, r8_78, r9_34);
    edx134 = *reinterpret_cast<uint32_t*>(rbp5 + 0xa8);
    if (edx134 == 6) {
        if (!*reinterpret_cast<void***>(rbp5 + 8) || ((dired_outbuf(" -> ", 4, " -> ", 4), rcx135 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax132) + reinterpret_cast<uint64_t>(rax133) + 4), print_name_with_quoting(rbp5, 1, 0, rcx135, r8_78, r9_34), edx136 = indicator_style, edx136 == 0) || (esi137 = *reinterpret_cast<void***>(rbp5 + 0xac), al138 = get_type_indicator(1, esi137, 0, rcx135, r8_78, r9_34), al138 == 0))) {
            addr_cad4_90:
            rax139 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v4) - reinterpret_cast<uint64_t>(g28));
            if (rax139) {
                fun_4870();
                goto addr_d115_15;
            } else {
                return;
            }
        } else {
            addr_cb74_93:
            rdi140 = stdout;
            tmp64_141 = dired_pos + 1;
            dired_pos = tmp64_141;
            rdx142 = *reinterpret_cast<void***>(rdi140 + 40);
            if (reinterpret_cast<unsigned char>(rdx142) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi140 + 48))) {
                fun_48d0();
                goto addr_cad4_90;
            } else {
                *reinterpret_cast<void***>(rdi140 + 40) = rdx142 + 1;
                *reinterpret_cast<void***>(rdx142) = al138;
                goto addr_cad4_90;
            }
        }
    } else {
        eax143 = indicator_style;
        if (!eax143) 
            goto addr_cad4_90;
        esi144 = *reinterpret_cast<void***>(rbp5 + 48);
        edi145 = *reinterpret_cast<unsigned char*>(rbp5 + 0xb8);
        al138 = get_type_indicator(*reinterpret_cast<signed char*>(&edi145), esi144, edx134, rax132, r8_78, r9_34);
        if (!al138) 
            goto addr_cad4_90;
        goto addr_cb74_93;
    }
    addr_d04b_75:
    if (*reinterpret_cast<int32_t*>(&r8_78) < reinterpret_cast<int32_t>(0)) {
        width_2 = 0;
        *reinterpret_cast<uint32_t*>(&r8_78) = 0;
        *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
        goto addr_ca6e_80;
    }
    addr_d0b0_79:
    eax146 = mbsnwidth(r15_107, rax108);
    width_2 = eax146;
    *reinterpret_cast<uint32_t*>(&r8_78) = eax146;
    *reinterpret_cast<int32_t*>(&r8_78 + 4) = 0;
    goto addr_d04b_75;
    addr_cefb_87:
    rbx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx38) + reinterpret_cast<unsigned char>(rax131));
    goto addr_cf00_69;
    addr_4c96_16:
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
    fun_46e0();
}

struct s25 {
    signed char f0;
    signed char f1;
};

struct s25* last_component(void** rdi, void** rsi, void** rdx);

void** file_name_concat(void** rdi, void** rsi);

void extract_dirs_from_files(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13d7;
    void** r12_8;
    int1_t zf9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rax13;
    void** rbx14;
    void** rax15;
    void** rbp16;
    void** r14_17;
    struct s25* rax18;
    uint32_t eax19;
    int1_t cf20;
    void** rax21;
    void** rdi22;
    void** rdi23;
    void** rdi24;
    int1_t cf25;
    void** rdx26;
    void** rsi27;
    void** rdi28;
    void** rax29;
    void** rdx30;
    int1_t zf31;
    void* rcx32;

    r13d7 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rsi)));
    r12_8 = rdi;
    if (rdi && (zf9 = active_dir_set == 0, !zf9)) {
        rax10 = xmalloc(32, rsi);
        rax11 = xstrdup(r12_8, rsi);
        *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax10 + 8) = rax11;
        rax12 = pending_dirs;
        *reinterpret_cast<void***>(rax10 + 16) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax10 + 24) = rax12;
        pending_dirs = rax10;
    }
    rax13 = cwd_n_used;
    rbx14 = rax13 + 0xffffffffffffffff;
    if (rax13) {
        while (1) {
            rax15 = sorted_file;
            rbp16 = *reinterpret_cast<void***>(rax15 + reinterpret_cast<unsigned char>(rbx14) * 8);
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) == 3) 
                goto addr_9485_5;
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) != 9) 
                goto addr_9460_7;
            addr_9485_5:
            r14_17 = *reinterpret_cast<void***>(rbp16);
            if (!r12_8) 
                goto addr_9560_8;
            rax18 = last_component(r14_17, rsi, rdx);
            if (rax18->f0 != 46) 
                goto addr_94a3_10;
            rdx = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(rax18->f1 == 46);
            eax19 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax18) + reinterpret_cast<unsigned char>(rdx) + 1);
            if (!*reinterpret_cast<signed char*>(&eax19) || *reinterpret_cast<signed char*>(&eax19) == 47) {
                addr_9460_7:
                cf20 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(1);
                --rbx14;
                if (cf20) 
                    break; else 
                    continue;
            }
            addr_94a3_10:
            if (*reinterpret_cast<void***>(r14_17) == 47) {
                addr_9560_8:
                rsi = *reinterpret_cast<void***>(rbp16 + 8);
                rdx = r13d7;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                queue_directory(r14_17, rsi, rdx, rcx, r8);
            } else {
                rax21 = file_name_concat(r12_8, r14_17);
                rsi = *reinterpret_cast<void***>(rbp16 + 8);
                rdx = r13d7;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                queue_directory(rax21, rsi, rdx, rcx, r8);
                fun_4630(rax21, rax21);
            }
            if (*reinterpret_cast<uint32_t*>(rbp16 + 0xa8) == 9) {
                rdi22 = *reinterpret_cast<void***>(rbp16);
                fun_4630(rdi22, rdi22);
                rdi23 = *reinterpret_cast<void***>(rbp16 + 8);
                fun_4630(rdi23, rdi23);
                rdi24 = *reinterpret_cast<void***>(rbp16 + 16);
                fun_4630(rdi24, rdi24);
                cf25 = reinterpret_cast<unsigned char>(rbx14) < reinterpret_cast<unsigned char>(1);
                --rbx14;
                if (cf25) 
                    goto addr_9502_16;
            }
        }
    } else {
        goto addr_95a0_18;
    }
    addr_9508_19:
    rdx26 = cwd_n_used;
    if (rdx26) {
        rsi27 = sorted_file;
        rdi28 = rsi27 + reinterpret_cast<unsigned char>(rdx26) * 8;
        rax29 = rsi27;
        *reinterpret_cast<int32_t*>(&rdx30) = 0;
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        do {
            zf31 = *reinterpret_cast<uint32_t*>(*reinterpret_cast<void***>(rax29) + 0xa8) == 9;
            *reinterpret_cast<void***>(rsi27 + reinterpret_cast<unsigned char>(rdx30) * 8) = *reinterpret_cast<void***>(rax29);
            rax29 = rax29 + 8;
            *reinterpret_cast<uint32_t*>(&rcx32) = reinterpret_cast<uint1_t>(!zf31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx32) + 4) = 0;
            rdx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx30) + reinterpret_cast<uint64_t>(rcx32));
        } while (rdi28 != rax29);
        cwd_n_used = rdx30;
        return;
    }
    addr_95a0_18:
    cwd_n_used = reinterpret_cast<void**>(0);
    return;
    addr_9502_16:
    goto addr_9508_19;
}

struct s26 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

/* mpsort_with_tmp.part.0 */
void mpsort_with_tmp_part_0(struct s26* rdi, uint64_t rsi, void** rdx, void** rcx) {
    uint64_t rax5;
    void** rbp6;
    struct s26* rbx7;
    uint64_t v8;
    uint64_t rsi9;
    uint64_t v10;
    struct s26* rax11;
    void** v12;
    struct s26* v13;
    void** r14_14;
    void** r12_15;
    int32_t eax16;
    uint64_t rax17;
    uint64_t rsi18;
    uint64_t v19;
    struct s26* r15_20;
    void** r12_21;
    void** r13_22;
    int32_t eax23;
    void** r14_24;
    void** r14_25;
    void** r12_26;
    int32_t eax27;
    void** r15_28;
    uint64_t v29;
    uint64_t r12_30;
    void** r13_31;
    int32_t eax32;
    int32_t eax33;
    uint64_t v34;
    int64_t r15_35;
    uint64_t r13_36;
    void** r12_37;
    int32_t eax38;
    int32_t eax39;

    rax5 = rsi >> 1;
    rbp6 = rcx;
    rbx7 = rdi;
    v8 = rsi;
    rsi9 = rsi - rax5;
    v10 = rax5;
    rax11 = reinterpret_cast<struct s26*>(reinterpret_cast<uint64_t>(rdi) + rax5 * 8);
    v12 = rdx;
    v13 = rax11;
    if (rsi9 > 2) {
        rdx = v12;
        mpsort_with_tmp_part_0(v13, rsi9, rdx, rcx);
    } else {
        if (rsi9 == 2 && (r14_14 = rax11->f0, r12_15 = rax11->f8, eax16 = reinterpret_cast<int32_t>(rcx(r14_14, r12_15)), !(reinterpret_cast<uint1_t>(eax16 < 0) | reinterpret_cast<uint1_t>(eax16 == 0)))) {
            rax11->f0 = r12_15;
            rax11->f8 = r14_14;
            if (v8 == 3) 
                goto addr_12cf1_5;
            goto addr_12dd8_7;
        }
    }
    if (v8 != 3) {
        addr_12dd8_7:
        rax17 = v8 >> 2;
        rsi18 = v10 - rax17;
        v19 = rax17;
        r15_20 = reinterpret_cast<struct s26*>(reinterpret_cast<uint64_t>(rbx7) + rax17 * 8);
        if (rsi18 > 2) {
            rdx = v12;
            rcx = rbp6;
            mpsort_with_tmp_part_0(r15_20, rsi18, rdx, rcx);
        } else {
            if (rsi18 == 2 && (r12_21 = r15_20->f0, r13_22 = r15_20->f8, eax23 = reinterpret_cast<int32_t>(rbp6(r12_21, r13_22, rdx)), !(reinterpret_cast<uint1_t>(eax23 < 0) | reinterpret_cast<uint1_t>(eax23 == 0)))) {
                r15_20->f0 = r13_22;
                r15_20->f8 = r12_21;
            }
        }
    } else {
        addr_12cf1_5:
        r14_24 = rbx7->f0;
        *reinterpret_cast<void***>(v12) = r14_24;
        goto addr_12cfc_12;
    }
    if (v19 > 2) {
        rdx = v12;
        rcx = rbp6;
        mpsort_with_tmp_part_0(rbx7, v19, rdx, rcx);
        goto addr_12ef9_15;
    }
    r14_25 = rbx7->f0;
    if (v19 == 2) {
        r12_26 = rbx7->f8;
        eax27 = reinterpret_cast<int32_t>(rbp6(r14_25, r12_26, rdx, rcx));
        if (reinterpret_cast<uint1_t>(eax27 < 0) | reinterpret_cast<uint1_t>(eax27 == 0)) {
            addr_12ef9_15:
            r14_25 = rbx7->f0;
        } else {
            rbx7->f8 = r14_25;
            r14_25 = r12_26;
            rbx7->f0 = r12_26;
        }
    }
    r15_28 = r15_20->f0;
    v29 = 0;
    r12_30 = v19;
    r13_31 = v12;
    while (1) {
        r13_31 = r13_31 + 8;
        eax32 = reinterpret_cast<int32_t>(rbp6(r14_25, r15_28, rdx, rcx));
        if (!(reinterpret_cast<uint1_t>(eax32 < 0) | reinterpret_cast<uint1_t>(eax32 == 0))) {
            do {
                *reinterpret_cast<void***>(r13_31 + 0xfffffffffffffff8) = r15_28;
                ++r12_30;
                if (v10 == r12_30) 
                    goto addr_12e8e_22;
                r15_28 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) + r12_30 * 8);
                r13_31 = r13_31 + 8;
                eax33 = reinterpret_cast<int32_t>(rbp6(r14_25, r15_28, rdx, rcx));
            } while (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax33 < 0) | reinterpret_cast<uint1_t>(eax33 == 0)));
        }
        ++v29;
        *reinterpret_cast<void***>(r13_31 + 0xfffffffffffffff8) = r14_25;
        if (v19 == v29) 
            break;
        r14_25 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) + v29 * 8);
    }
    v29 = r12_30;
    v19 = v10;
    addr_12e8e_22:
    rdx = reinterpret_cast<void**>(v19 - v29 << 3);
    fun_4a30(r13_31, reinterpret_cast<uint64_t>(rbx7) + v29 * 8, rdx, rcx);
    r14_24 = *reinterpret_cast<void***>(v12);
    addr_12cfc_12:
    v34 = 0;
    *reinterpret_cast<int32_t*>(&r15_35) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_35) + 4) = 0;
    r13_36 = v10;
    r12_37 = v13->f0;
    while (1) {
        ++r15_35;
        eax38 = reinterpret_cast<int32_t>(rbp6(r14_24, r12_37, rdx, rcx));
        if (!(reinterpret_cast<uint1_t>(eax38 < 0) | reinterpret_cast<uint1_t>(eax38 == 0))) {
            do {
                *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) + r15_35 * 8 - 8) = r12_37;
                ++r13_36;
                if (v8 == r13_36) 
                    goto addr_12d70_29;
                r12_37 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) + r13_36 * 8);
                ++r15_35;
                eax39 = reinterpret_cast<int32_t>(rbp6(r14_24, r12_37, rdx, rcx));
            } while (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax39 < 0) | reinterpret_cast<uint1_t>(eax39 == 0)));
        }
        ++v34;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) + r15_35 * 8 - 8) = r14_24;
        if (v10 == v34) 
            break;
        rdx = v12;
        r14_24 = *reinterpret_cast<void***>(rdx + v34 * 8);
    }
    return;
    addr_12d70_29:
}

struct s27 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s27* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s27* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x1e619);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x1e614);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x1e61d);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x1e610);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

void add_ignore_pattern(void** rdi, void** rsi) {
    void** rax3;
    void** rdx4;

    rax3 = xmalloc(16, rsi);
    rdx4 = ignore_patterns;
    *reinterpret_cast<void***>(rax3) = rdi;
    *reinterpret_cast<void***>(rax3 + 8) = rdx4;
    ignore_patterns = rax3;
    return;
}

int64_t __gmon_start__ = 0;

void fun_4003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g24c98 = 0;

void fun_4033() {
    __asm__("cli ");
    goto g24c98;
}

void fun_4043() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4053() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4063() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4073() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4083() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4093() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_40f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4103() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4113() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4123() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4133() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4143() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4153() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4163() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4173() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4183() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4193() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_41f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4203() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4213() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4223() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4233() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4243() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4253() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4263() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4273() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4283() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4293() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_42f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4303() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4313() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4323() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4333() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4343() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4353() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4363() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4373() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4383() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4393() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_43f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4403() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4413() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4423() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4433() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4443() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4453() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4463() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4473() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4483() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4493() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_44f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4503() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4513() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4523() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4533() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4543() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4553() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4563() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4573() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4583() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4593() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45a3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45b3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45c3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45d3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45e3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_45f3() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4603() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4613() {
    __asm__("cli ");
    goto 0x4020;
}

void fun_4623() {
    __asm__("cli ");
    goto 0x4020;
}

void** free = reinterpret_cast<void**>(0);

void fun_4633() {
    __asm__("cli ");
    goto free;
}

int64_t localtime_r = 0;

void fun_4643() {
    __asm__("cli ");
    goto localtime_r;
}

int64_t gmtime_r = 0;

void fun_4653() {
    __asm__("cli ");
    goto gmtime_r;
}

int64_t strcmp = 0;

void fun_4663() {
    __asm__("cli ");
    goto strcmp;
}

void** malloc = reinterpret_cast<void**>(0);

void fun_4673() {
    __asm__("cli ");
    goto malloc;
}

int64_t __cxa_finalize = 0;

void fun_4683() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x4030;

void fun_4693() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x4040;

void fun_46a3() {
    __asm__("cli ");
    goto getenv;
}

int64_t sigprocmask = 0x4050;

void fun_46b3() {
    __asm__("cli ");
    goto sigprocmask;
}

int64_t __snprintf_chk = 0x4060;

void fun_46c3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t raise = 0x4070;

void fun_46d3() {
    __asm__("cli ");
    goto raise;
}

int64_t abort = 0x4080;

void fun_46e3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x4090;

void fun_46f3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x40a0;

void fun_4703() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x40b0;

void fun_4713() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x40c0;

void fun_4723() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x40d0;

void fun_4733() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x40e0;

void fun_4743() {
    __asm__("cli ");
    goto isatty;
}

int64_t sigaction = 0x40f0;

void fun_4753() {
    __asm__("cli ");
    goto sigaction;
}

int64_t iswcntrl = 0x4100;

void fun_4763() {
    __asm__("cli ");
    goto iswcntrl;
}

int64_t reallocarray = 0x4110;

void fun_4773() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t wcswidth = 0x4120;

void fun_4783() {
    __asm__("cli ");
    goto wcswidth;
}

int64_t localeconv = 0x4130;

void fun_4793() {
    __asm__("cli ");
    goto localeconv;
}

int64_t faccessat = 0x4140;

void fun_47a3() {
    __asm__("cli ");
    goto faccessat;
}

int64_t mbstowcs = 0x4150;

void fun_47b3() {
    __asm__("cli ");
    goto mbstowcs;
}

int64_t readlink = 0x4160;

void fun_47c3() {
    __asm__("cli ");
    goto readlink;
}

int64_t clock_gettime = 0x4170;

void fun_47d3() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t setenv = 0x4180;

void fun_47e3() {
    __asm__("cli ");
    goto setenv;
}

int64_t textdomain = 0x4190;

void fun_47f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x41a0;

void fun_4803() {
    __asm__("cli ");
    goto fclose;
}

int64_t opendir = 0x41b0;

void fun_4813() {
    __asm__("cli ");
    goto opendir;
}

int64_t getpwuid = 0x41c0;

void fun_4823() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x41d0;

void fun_4833() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x41e0;

void fun_4843() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x41f0;

void fun_4853() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x4200;

void fun_4863() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x4210;

void fun_4873() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x4220;

void fun_4883() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x4230;

void fun_4893() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x4240;

void fun_48a3() {
    __asm__("cli ");
    goto strchr;
}

int64_t getgrgid = 0x4250;

void fun_48b3() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t snprintf = 0x4260;

void fun_48c3() {
    __asm__("cli ");
    goto snprintf;
}

int64_t __overflow = 0x4270;

void fun_48d3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x4280;

void fun_48e3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x4290;

void fun_48f3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x42a0;

void fun_4903() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t fnmatch = 0x42b0;

void fun_4913() {
    __asm__("cli ");
    goto fnmatch;
}

int64_t memset = 0x42c0;

void fun_4923() {
    __asm__("cli ");
    goto memset;
}

int64_t ioctl = 0x42d0;

void fun_4933() {
    __asm__("cli ");
    goto ioctl;
}

int64_t getcwd = 0x42e0;

void fun_4943() {
    __asm__("cli ");
    goto getcwd;
}

int64_t strspn = 0x42f0;

void fun_4953() {
    __asm__("cli ");
    goto strspn;
}

int64_t closedir = 0x4300;

void fun_4963() {
    __asm__("cli ");
    goto closedir;
}

int64_t memcmp = 0x4310;

void fun_4973() {
    __asm__("cli ");
    goto memcmp;
}

int64_t _setjmp = 0x4320;

void fun_4983() {
    __asm__("cli ");
    goto _setjmp;
}

int64_t fputs_unlocked = 0x4330;

void fun_4993() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x4340;

void fun_49a3() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x4350;

void fun_49b3() {
    __asm__("cli ");
    goto calloc;
}

int64_t signal = 0x4360;

void fun_49c3() {
    __asm__("cli ");
    goto signal;
}

int64_t dirfd = 0x4370;

void fun_49d3() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x4380;

void fun_49e3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t getpwnam = 0x4390;

void fun_49f3() {
    __asm__("cli ");
    goto getpwnam;
}

int64_t __memcpy_chk = 0x43a0;

void fun_4a03() {
    __asm__("cli ");
    goto __memcpy_chk;
}

int64_t sigemptyset = 0x43b0;

void fun_4a13() {
    __asm__("cli ");
    goto sigemptyset;
}

int64_t stat = 0x43c0;

void fun_4a23() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x43d0;

void fun_4a33() {
    __asm__("cli ");
    goto memcpy;
}

int64_t getgrnam = 0x43e0;

void fun_4a43() {
    __asm__("cli ");
    goto getgrnam;
}

int64_t tzset = 0x43f0;

void fun_4a53() {
    __asm__("cli ");
    goto tzset;
}

int64_t fileno = 0x4400;

void fun_4a63() {
    __asm__("cli ");
    goto fileno;
}

int64_t tcgetpgrp = 0x4410;

void fun_4a73() {
    __asm__("cli ");
    goto tcgetpgrp;
}

int64_t readdir = 0x4420;

void fun_4a83() {
    __asm__("cli ");
    goto readdir;
}

int64_t wcwidth = 0x4430;

void fun_4a93() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t fflush = 0x4440;

void fun_4aa3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x4450;

void fun_4ab3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t strcoll = 0x4460;

void fun_4ac3() {
    __asm__("cli ");
    goto strcoll;
}

int64_t __freading = 0x4470;

void fun_4ad3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x4480;

void fun_4ae3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x4490;

void fun_4af3() {
    __asm__("cli ");
    goto realloc;
}

int64_t stpncpy = 0x44a0;

void fun_4b03() {
    __asm__("cli ");
    goto stpncpy;
}

int64_t setlocale = 0x44b0;

void fun_4b13() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x44c0;

void fun_4b23() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t statx = 0x44d0;

void fun_4b33() {
    __asm__("cli ");
    goto statx;
}

int64_t strftime = 0x44e0;

void fun_4b43() {
    __asm__("cli ");
    goto strftime;
}

int64_t mempcpy = 0x44f0;

void fun_4b53() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x4500;

void fun_4b63() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x4510;

void fun_4b73() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x4520;

void fun_4b83() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x4530;

void fun_4b93() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t unsetenv = 0x4540;

void fun_4ba3() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t __cxa_atexit = 0x4550;

void fun_4bb3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t wcstombs = 0x4560;

void fun_4bc3() {
    __asm__("cli ");
    goto wcstombs;
}

int64_t gethostname = 0x4570;

void fun_4bd3() {
    __asm__("cli ");
    goto gethostname;
}

int64_t sigismember = 0x4580;

void fun_4be3() {
    __asm__("cli ");
    goto sigismember;
}

int64_t exit = 0x4590;

void fun_4bf3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x45a0;

void fun_4c03() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x45b0;

void fun_4c13() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t fflush_unlocked = 0x45c0;

void fun_4c23() {
    __asm__("cli ");
    goto fflush_unlocked;
}

int64_t mbsinit = 0x45d0;

void fun_4c33() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x45e0;

void fun_4c43() {
    __asm__("cli ");
    goto iswprint;
}

int64_t sigaddset = 0x45f0;

void fun_4c53() {
    __asm__("cli ");
    goto sigaddset;
}

int64_t __ctype_tolower_loc = 0x4600;

void fun_4c63() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x4610;

void fun_4c73() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x4620;

void fun_4c83() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_4b10(int64_t rdi, ...);

void fun_4830(int64_t rdi, int64_t rsi);

void fun_47f0(int64_t rdi, int64_t rsi);

uint32_t exit_failure = 1;

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_4880(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t human_options(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t ls_mode = 1;

void** optarg = reinterpret_cast<void**>(0);

void xstrtol_fatal(int64_t rdi, int64_t rsi);

void fun_4900(int64_t rdi, void** rsi, int64_t rdx, uint16_t* rcx, void** r8);

void set_quoting_style();

void** clone_quoting_options();

void set_char_quoting(void** rdi, void** rsi, int64_t rdx);

int32_t optind = 0;

int32_t fun_4700(void** rdi, void** rsi, void** rdx, ...);

signed char hard_locale();

void** quote(void** rdi, ...);

void** g25048 = reinterpret_cast<void**>(0xe7);

int64_t argmatch(void** rdi, void** rsi, void** rdx, ...);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** stderr = reinterpret_cast<void**>(0);

void fun_4c10(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12);

void usage();

int32_t fun_4930(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

unsigned char directories_first = 0;

void** color_buf = reinterpret_cast<void**>(0);

void _obstack_begin(void** rdi);

void** xgethostname(void** rdi);

uint64_t g260f0 = 0;

void** hash_remove(void** rdi, void** rsi);

int64_t g250d0 = 5;

void** g250d8 = reinterpret_cast<void**>(0xfc);

int64_t color_indicator = 2;

int16_t* g25068 = reinterpret_cast<int16_t*>(0x1bff3);

int64_t g25070 = 1;

signed char* g25078 = reinterpret_cast<signed char*>(0x1bbd5);

int64_t hash_get_n_entries(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_4d13(void** edi, void** rsi) {
    void** r14d3;
    void** rbx4;
    void* rsp5;
    void** rdi6;
    void* rax7;
    void* v8;
    void** r15_9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** r8_15;
    void** rcx16;
    void** rdx17;
    void** rsi18;
    void** rdi19;
    void** v20;
    int32_t eax21;
    void* rsp22;
    int1_t zf23;
    void** rax24;
    void** rax25;
    int32_t eax26;
    void** rax27;
    int32_t eax28;
    void** eax29;
    int64_t rax30;
    void* rax31;
    int64_t rsi32;
    int64_t rdi33;
    void** rsi34;
    void* rsp35;
    void** rbp36;
    void** rdx37;
    void** rdi38;
    void** rcx39;
    void** rdi40;
    void** rdi41;
    void** rax42;
    void** rdi43;
    void** rdi44;
    void* rsp45;
    uint32_t eax46;
    void* rsp47;
    void** eax48;
    int1_t zf49;
    void** rax50;
    void* rsp51;
    void** rax52;
    uint32_t eax53;
    int64_t rax54;
    unsigned char* rbp55;
    uint32_t eax56;
    void** rdi57;
    void** rsi58;
    void** rax59;
    void** rsi60;
    void*** rsp61;
    void** rdx62;
    uint32_t eax63;
    void** rcx64;
    uint32_t eax65;
    unsigned char al66;
    uint32_t eax67;
    uint32_t eax68;
    void** rax69;
    int1_t zf70;
    int64_t rbp71;
    void** r12_72;
    int32_t eax73;
    signed char al74;
    int1_t zf75;
    void** r12_76;
    void** rdi77;
    void** rax78;
    void* rsp79;
    void** r13_80;
    void** rax81;
    void** rbp82;
    int64_t rax83;
    void** r12_84;
    void** rax85;
    void* rsp86;
    void** rdi87;
    void** r9_88;
    void** rbp89;
    void** rax90;
    unsigned char al91;
    signed char al92;
    void** rax93;
    void** rax94;
    unsigned char al95;
    int32_t eax96;
    void** rax97;
    void** rax98;
    void** rax99;
    void* rax100;
    void** eax101;
    void** rax102;
    void** rax103;
    uint32_t eax104;
    uint32_t eax105;
    int1_t zf106;
    unsigned char al107;
    int64_t rax108;
    void** rax109;
    void** rdi110;
    void** rax111;
    int64_t rax112;
    void** rax113;
    int32_t eax114;
    int1_t zf115;
    unsigned char al116;
    int1_t zf117;
    void** rax118;
    void* rsp119;
    void** v120;
    void** rax121;
    void** rax122;
    void** rbp123;
    void** r12_124;
    void** rbx125;
    int32_t eax126;
    int32_t eax127;
    void** rax128;
    int1_t zf129;
    uint32_t eax130;
    int1_t zf131;
    int1_t zf132;
    int1_t zf133;
    uint32_t eax134;
    int1_t zf135;
    uint1_t cf136;
    unsigned char al137;
    unsigned char al138;
    void** rax139;
    uint32_t eax140;
    void** rax141;
    void* rsp142;
    void** r12_143;
    void** rax144;
    signed char al145;
    int1_t zf146;
    signed char al147;
    uint32_t eax148;
    int1_t zf149;
    int64_t rbx150;
    void** rsi151;
    void** r13_152;
    int64_t rax153;
    signed char al154;
    int1_t zf155;
    void** rax156;
    void** rax157;
    void** rdi158;
    void** rax159;
    void* rsp160;
    uint32_t eax161;
    int1_t zf162;
    int1_t zf163;
    int1_t zf164;
    uint32_t eax165;
    int1_t zf166;
    int1_t zf167;
    int1_t zf168;
    int1_t zf169;
    void** r13_170;
    void** r12_171;
    int1_t zf172;
    uint64_t rax173;
    void** rax174;
    void** r9_175;
    void** rax176;
    int1_t zf177;
    void** rdi178;
    void** r9_179;
    int1_t zf180;
    void** r9_181;
    int1_t zf182;
    void** r9_183;
    int1_t zf184;
    void** r9_185;
    int1_t zf186;
    int1_t less_or_equal187;
    int1_t zf188;
    void** rdi189;
    void** tmp64_190;
    void** rax191;
    void** rax192;
    uint64_t rdx193;
    int1_t zf194;
    unsigned char al195;
    int1_t zf196;
    void** rax197;
    void** rdi198;
    void** rdi199;
    void** r12_200;
    int1_t zf201;
    void** rdi202;
    int32_t eax203;
    void** rax204;
    int1_t zf205;
    int1_t zf206;
    int1_t zf207;
    int16_t* rax208;
    int1_t zf209;
    signed char* rax210;
    void** r9_211;
    void** r9_212;
    void** rdi213;
    void** r9_214;
    int32_t ebx215;
    int32_t edi216;
    int1_t zf217;
    uint32_t eax218;
    int64_t rax219;
    void** rbp220;
    int64_t rax221;
    void* rdx222;
    int64_t rax223;

    __asm__("cli ");
    r14d3 = edi;
    rbx4 = rsi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68);
    rdi6 = *reinterpret_cast<void***>(rsi);
    rax7 = g28;
    v8 = rax7;
    r15_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 64);
    set_program_name(rdi6);
    fun_4b10(6, 6);
    fun_4830("coreutils", "/usr/local/share/locale");
    fun_47f0("coreutils", "/usr/local/share/locale");
    exit_failure = 2;
    atexit(0xf4e0, "/usr/local/share/locale");
    print_dir_name = 1;
    exit_status = 0;
    pending_dirs = reinterpret_cast<void**>(0);
    current_time = 0x8000000000000000;
    g26378 = reinterpret_cast<void**>(0xffffffffffffffff);
    v10 = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<uint32_t*>(&v11 + 4) = 0xffffffff;
    *reinterpret_cast<int32_t*>(&v11) = -1;
    *reinterpret_cast<int32_t*>(&v12) = -1;
    v13 = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<signed char*>(&v12 + 7) = 0;
    v14 = reinterpret_cast<void**>(0);
    r8_15 = r15_9;
    rcx16 = reinterpret_cast<void**>(0x24320);
    rdx17 = reinterpret_cast<void**>("abcdfghiklmnopqrstuvw:xABCDFGHI:LNQRST:UXZ1");
    rsi18 = rbx4;
    rdi19 = r14d3;
    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
    *reinterpret_cast<int32_t*>(&v20) = -1;
    eax21 = fun_4880(rdi19, rsi18, "abcdfghiklmnopqrstuvw:xABCDFGHI:LNQRST:UXZ1", 0x24320, r8_15);
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax21 == -1) {
        zf23 = output_block_size == 0;
        if (zf23) {
            rax24 = fun_46a0("LS_BLOCK_SIZE", "LS_BLOCK_SIZE");
            rdx17 = reinterpret_cast<void**>(0x26320);
            rsi18 = reinterpret_cast<void**>(0x26328);
            rdi19 = rax24;
            human_options(rdi19, 0x26328, 0x26320, 0x24320, r8_15);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8);
            if (rax24 || (rdi19 = reinterpret_cast<void**>("BLOCK_SIZE"), rax25 = fun_46a0("BLOCK_SIZE", "BLOCK_SIZE"), rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8), !!rax25)) {
                eax26 = human_output_opts;
                file_human_output_opts = eax26;
                rax27 = output_block_size;
                file_output_block_size = rax27;
            }
            if (!1) {
                human_output_opts = 0;
                output_block_size = reinterpret_cast<void**>("v");
            }
        }
        if (1) {
            eax28 = ls_mode;
            if (eax28 == 1) 
                goto addr_6441_11;
            if (eax28 == 2) 
                goto addr_6372_13;
            eax29 = reinterpret_cast<void**>(0);
            goto addr_52ab_15;
        } else {
            format = reinterpret_cast<void**>(0xffffffff);
            if (1) 
                goto addr_52b1_17; else 
                goto addr_4eb5_18;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax30) = reinterpret_cast<uint32_t>(eax21 + 0x83);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rax30) > 0x114) 
            goto addr_6430_20;
        goto *reinterpret_cast<int32_t*>(0x1a190 + rax30 * 4) + 0x1a190;
    }
    addr_67a2_22:
    fun_4840();
    fun_4b70();
    addr_6762_23:
    rax31 = fun_4870();
    r8_15 = optarg;
    *reinterpret_cast<int32_t*>(&rsi32) = *reinterpret_cast<int32_t*>(&v20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi32) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi33) = *reinterpret_cast<int32_t*>(&rax31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
    xstrtol_fatal(rdi33, rsi32);
    addr_677e_25:
    xalloc_die();
    addr_6783_26:
    fun_4900("dev_ino_size <= obstack_object_size (&dev_ino_obstack)", "src/ls.c", 0x41d, "dev_ino_pop", r8_15);
    goto addr_67a2_22;
    while (1) {
        addr_6674_27:
        *reinterpret_cast<uint32_t*>(&rsi34) = 10;
        *reinterpret_cast<int32_t*>(&rsi34 + 4) = 0;
        fun_48d0();
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
        rbp36 = pending_dirs;
        while (1) {
            addr_51f5_28:
            if (!rbp36) 
                goto addr_59e1_29; else 
                goto addr_51fe_30;
            addr_51c0_31:
            rdx37 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp36 + 16))));
            *reinterpret_cast<int32_t*>(&rdx37 + 4) = 0;
            rsi34 = *reinterpret_cast<void***>(rbp36 + 8);
            print_dir(rdi38, rsi34, rdx37, rcx39);
            rdi40 = *reinterpret_cast<void***>(rbp36);
            fun_4630(rdi40, rdi40);
            rdi41 = *reinterpret_cast<void***>(rbp36 + 8);
            fun_4630(rdi41, rdi41);
            fun_4630(rbp36, rbp36);
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            print_dir_name = 1;
            addr_51ee_32:
            rbp36 = pending_dirs;
            continue;
            addr_526d_33:
            fun_4630(rax42, rax42);
            rdi43 = *reinterpret_cast<void***>(rbp36);
            fun_4630(rdi43, rdi43);
            rdi44 = *reinterpret_cast<void***>(rbp36 + 8);
            fun_4630(rdi44, rdi44);
            fun_4630(rbp36, rbp36);
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_51ee_32;
            while (1) {
                addr_61e5_34:
                *reinterpret_cast<int32_t*>(&v11) = 3;
                while (1) {
                    set_quoting_style();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    while (1) {
                        eax46 = get_quoting_style();
                        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        eax48 = format;
                        if (eax48 && (reinterpret_cast<uint32_t>(eax48 - 2) > 1 || (zf49 = line_length == 0, zf49)) || eax46 != 3 && (eax46 != 6 && eax46 != 1)) {
                            align_variable_outer_quotes = 0;
                            rax50 = clone_quoting_options();
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                            filename_quoting_options = rax50;
                            if (eax46 == 7) {
                                set_char_quoting(rax50, 32, 1);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                            }
                        } else {
                            align_variable_outer_quotes = 1;
                            rax52 = clone_quoting_options();
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                            filename_quoting_options = rax52;
                        }
                        eax53 = indicator_style;
                        if (eax53 > 1 && (*reinterpret_cast<uint32_t*>(&rax54) = eax53 - 2, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0, rbp55 = reinterpret_cast<unsigned char*>(rax54 + reinterpret_cast<int64_t>("*=>@|")), eax56 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("*=>@|") + rax54), !!*reinterpret_cast<signed char*>(&eax56))) {
                            do {
                                rdi57 = filename_quoting_options;
                                *reinterpret_cast<int32_t*>(&rsi58) = *reinterpret_cast<signed char*>(&eax56);
                                *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
                                ++rbp55;
                                set_char_quoting(rdi57, rsi58, 1);
                                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                                eax56 = *rbp55;
                            } while (*reinterpret_cast<signed char*>(&eax56));
                        }
                        rax59 = clone_quoting_options();
                        *reinterpret_cast<int32_t*>(&rsi60) = 58;
                        *reinterpret_cast<int32_t*>(&rsi60 + 4) = 0;
                        dirname_quoting_options = rax59;
                        set_char_quoting(rax59, 58, 1);
                        rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                        rdx62 = format;
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        eax63 = print_hyperlink;
                        *reinterpret_cast<unsigned char*>(&rcx64) = reinterpret_cast<uint1_t>(rdx62 == 0);
                        eax65 = (eax63 ^ 1) & *reinterpret_cast<uint32_t*>(&rcx64);
                        *reinterpret_cast<uint32_t*>(&rcx39) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(eolbyte)));
                        *reinterpret_cast<int32_t*>(&rcx39 + 4) = 0;
                        al66 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax65) & dired);
                        dired = al66;
                        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(al66)) > *reinterpret_cast<int32_t*>(&rcx39)) 
                            goto addr_67a2_22;
                        eax67 = 0xffffffff;
                        if (1) {
                            if (rdx62) {
                                eax68 = time_type;
                                if (eax68 - 1 <= 2) {
                                    sort_type = 5;
                                    goto addr_502f_48;
                                } else {
                                    eax67 = 0;
                                }
                            } else {
                                sort_type = 0;
                                goto addr_5a9f_51;
                            }
                        }
                        sort_type = eax67;
                        if (!rdx62) {
                            addr_5a9f_51:
                            if (!v14 && (rax69 = fun_46a0("TIME_STYLE", "TIME_STYLE"), rsp61 = rsp61 - 8 + 8, v14 = rax69, !rax69)) {
                                v14 = reinterpret_cast<void**>("locale");
                                goto addr_5afa_54;
                            }
                        } else {
                            addr_502f_48:
                            zf70 = print_with_color == 0;
                            rbp71 = optind;
                            if (!zf70) 
                                goto addr_5e7a_55; else 
                                goto addr_5043_56;
                        }
                        r12_72 = v14;
                        while (rdx62 = reinterpret_cast<void**>(6), *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0, rsi60 = reinterpret_cast<void**>("posix-"), eax73 = fun_4700(r12_72, "posix-", 6), rsp61 = rsp61 - 8 + 8, eax73 == 0) {
                            al74 = hard_locale();
                            rsp61 = rsp61 - 8 + 8;
                            if (!al74) 
                                goto addr_502f_48;
                            r12_72 = r12_72 + 6;
                        }
                        zf75 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_72) == 43);
                        v14 = r12_72;
                        if (zf75) {
                            r12_76 = r12_72 + 1;
                            *reinterpret_cast<int32_t*>(&rsi60) = 10;
                            *reinterpret_cast<int32_t*>(&rsi60 + 4) = 0;
                            rdi77 = r12_76;
                            rax78 = fun_48a0(rdi77, rdi77);
                            rsp79 = reinterpret_cast<void*>(rsp61 - 8 + 8);
                            rbp36 = rax78;
                            if (!rax78) {
                                r13_80 = r12_76;
                            } else {
                                r13_80 = rax78 + 1;
                                *reinterpret_cast<int32_t*>(&rsi60) = 10;
                                *reinterpret_cast<int32_t*>(&rsi60 + 4) = 0;
                                rdi77 = r13_80;
                                rax81 = fun_48a0(rdi77, rdi77);
                                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                                if (rax81) {
                                    addr_66dd_65:
                                    quote(r12_76, r12_76);
                                    fun_4840();
                                    fun_4b70();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_670f_66;
                                } else {
                                    *reinterpret_cast<void***>(rbp36) = reinterpret_cast<void**>(0);
                                }
                            }
                            long_time_format = r12_76;
                            g25048 = r13_80;
                        } else {
                            addr_5afa_54:
                            rbp82 = reinterpret_cast<void**>(0x249e0);
                            rdi77 = v14;
                            *reinterpret_cast<uint32_t*>(&rcx39) = 4;
                            *reinterpret_cast<int32_t*>(&rcx39 + 4) = 0;
                            rdx62 = reinterpret_cast<void**>(0x1a780);
                            rsi60 = reinterpret_cast<void**>(0x249e0);
                            rax83 = argmatch(rdi77, 0x249e0, 0x1a780, rdi77, 0x249e0, 0x1a780);
                            rsp79 = reinterpret_cast<void*>(rsp61 - 8 + 8);
                            if (rax83 < 0) {
                                rbx4 = reinterpret_cast<void**>("  - [posix-]%s\n");
                                argmatch_invalid("time style", v14, rax83, 4, r8_15);
                                r12_84 = stderr;
                                rax85 = fun_4840();
                                fun_4990(rax85, r12_84, 5, 4, r8_15);
                                rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8 - 8 + 8);
                                rcx16 = reinterpret_cast<void**>("full-iso");
                                do {
                                    rdi87 = stderr;
                                    rbp82 = rbp82 + 8;
                                    fun_4c10(rdi87, 1, "  - [posix-]%s\n", rcx16, r8_15, r9_88, v13, v11, v10, v14, 0xffffffffffffffff, v12);
                                    rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8);
                                    rcx16 = *reinterpret_cast<void***>(rbp82);
                                } while (rcx16);
                                rbp89 = stderr;
                                *reinterpret_cast<int32_t*>(&rdx17) = 5;
                                *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
                                rax90 = fun_4840();
                                rsi18 = rbp89;
                                fun_4990(rax90, rsi18, 5, rcx16, r8_15);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8);
                                addr_6430_20:
                                usage();
                                rdi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                usage();
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8);
                                addr_6441_11:
                                al91 = stdout_isatty(rdi19, rdi19);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                if (al91) 
                                    goto addr_6372_13; else 
                                    goto addr_644e_72;
                            } else {
                                if (rax83 == 2) {
                                    long_time_format = reinterpret_cast<void**>("%Y-%m-%d ");
                                    g25048 = reinterpret_cast<void**>("%m-%d %H:%M");
                                } else {
                                    if (rax83 > 2) {
                                        if (rax83 == 3 && (*reinterpret_cast<int32_t*>(&rdi77) = 2, *reinterpret_cast<int32_t*>(&rdi77 + 4) = 0, al92 = hard_locale(), rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8), !!al92)) {
                                            rax93 = fun_4840();
                                            rsi60 = g25048;
                                            rdx62 = reinterpret_cast<void**>(2);
                                            *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                            *reinterpret_cast<int32_t*>(&rdi77) = 0;
                                            *reinterpret_cast<int32_t*>(&rdi77 + 4) = 0;
                                            long_time_format = rax93;
                                            rax94 = fun_4840();
                                            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
                                            g25048 = rax94;
                                        }
                                    } else {
                                        if (!rax83) {
                                            g25048 = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
                                            long_time_format = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
                                        } else {
                                            if (!(rax83 - 1)) {
                                                g25048 = reinterpret_cast<void**>("%Y-%m-%d %H:%M");
                                                long_time_format = reinterpret_cast<void**>("%Y-%m-%d %H:%M");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        abformat_init(rdi77, rsi60, rdx62, rcx39, r8_15);
                        rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                        goto addr_502f_48;
                        addr_6372_13:
                        format = reinterpret_cast<void**>(2);
                        addr_4eb5_18:
                        if (v10 != 0xffffffffffffffff) 
                            goto addr_4ec1_83;
                        al95 = stdout_isatty(rdi19, rdi19);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        if (al95 && ((rdx17 = r15_9, *reinterpret_cast<int32_t*>(&rsi18) = 0x5413, *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0, rdi19 = reinterpret_cast<void**>(1), *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0, eax96 = fun_4930(1, 0x5413, rdx17, rcx16, r8_15), rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8), eax96 >= 0) && (*reinterpret_cast<uint32_t*>(&rax97) = *reinterpret_cast<uint16_t*>(&v20 + 2), *reinterpret_cast<int32_t*>(&rax97 + 4) = 0, !!*reinterpret_cast<int16_t*>(&rax97)))) {
                            v10 = rax97;
                            goto addr_4ec1_83;
                        }
                        rdi19 = reinterpret_cast<void**>("COLUMNS");
                        rax98 = fun_46a0("COLUMNS", "COLUMNS");
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        if (!rax98 || !*reinterpret_cast<void***>(rax98)) {
                            addr_52ca_87:
                            v10 = reinterpret_cast<void**>(80);
                            goto addr_4ec1_83;
                        } else {
                            rdi19 = rax98;
                            rax99 = decode_line_length(rdi19, rsi18, rdx17, rcx16, r8_15);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            v10 = rax99;
                            if (reinterpret_cast<signed char>(rax99) >= reinterpret_cast<signed char>(0)) {
                                addr_4ec1_83:
                                *reinterpret_cast<uint32_t*>(&rcx64) = 3;
                                line_length = v10;
                                rax100 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) / reinterpret_cast<unsigned char>(3));
                                max_idx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax100) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax100) < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v10) % reinterpret_cast<unsigned char>(3)) < 1))))));
                                eax101 = format;
                                if (reinterpret_cast<uint32_t>(eax101 - 2) > 2) 
                                    goto addr_4f09_89;
                            } else {
                                quote(rax98, rax98);
                                fun_4840();
                                rdi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                fun_4b70();
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_52ca_87;
                            }
                        }
                        rax102 = reinterpret_cast<void**>(0xffffffffffffffff);
                        if (1) {
                            rdi19 = reinterpret_cast<void**>("TABSIZE");
                            tabsize = reinterpret_cast<void**>(8);
                            rax103 = fun_46a0("TABSIZE", "TABSIZE");
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            if (!rax103) {
                                addr_4f09_89:
                                eax104 = 1;
                                if (1) {
                                    eax105 = 0;
                                    zf106 = ls_mode == 1;
                                    if (zf106) {
                                        al107 = stdout_isatty(rdi19, rdi19);
                                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                        eax105 = al107;
                                    }
                                    eax104 = eax105 & 1;
                                }
                            } else {
                                r8_15 = reinterpret_cast<void**>(0x1bb0a);
                                rcx64 = r15_9;
                                rdi19 = rax103;
                                rax108 = xstrtoumax(rdi19);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                if (*reinterpret_cast<int32_t*>(&rax108)) {
                                    rax109 = quote(rax103);
                                    fun_4840();
                                    rcx64 = rax109;
                                    rdi19 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                    fun_4b70();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_4f09_89;
                                } else {
                                    rax102 = v20;
                                    goto addr_4f02_99;
                                }
                            }
                        } else {
                            addr_4f02_99:
                            tabsize = rax102;
                            goto addr_4f09_89;
                        }
                        qmark_funny_chars = *reinterpret_cast<unsigned char*>(&eax104);
                        if (*reinterpret_cast<int32_t*>(&v11) >= 0) 
                            break;
                        rdi110 = reinterpret_cast<void**>("QUOTING_STYLE");
                        rax111 = fun_46a0("QUOTING_STYLE", "QUOTING_STYLE");
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        rbp36 = rax111;
                        if (rax111) {
                            *reinterpret_cast<uint32_t*>(&rcx64) = 4;
                            rdi110 = rax111;
                            rax112 = argmatch(rdi110, 0x24a40, 0x1e9c0);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            if (*reinterpret_cast<int32_t*>(&rax112) < 0) {
                                addr_670f_66:
                                rax113 = quote(rbp36, rbp36);
                                fun_4840();
                                rcx64 = rax113;
                                *reinterpret_cast<int32_t*>(&rdi110) = 0;
                                *reinterpret_cast<int32_t*>(&rdi110 + 4) = 0;
                                fun_4b70();
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8);
                            } else {
                                eax114 = *reinterpret_cast<int32_t*>(0x1e9c0 + *reinterpret_cast<int32_t*>(&rax112) * 4);
                                *reinterpret_cast<int32_t*>(&v11) = eax114;
                                if (eax114 >= 0) 
                                    break;
                            }
                        }
                        zf115 = ls_mode == 1;
                        *reinterpret_cast<int32_t*>(&v11) = 7;
                        if (!zf115) 
                            break;
                        al116 = stdout_isatty(rdi110, rdi110);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        if (!al116) 
                            continue; else 
                            goto addr_61e5_34;
                        addr_644e_72:
                        eax29 = reinterpret_cast<void**>(1);
                        addr_52ab_15:
                        format = eax29;
                        addr_52b1_17:
                        zf117 = print_with_color == 0;
                        if (!zf117) 
                            goto addr_4eb5_18;
                        if (!reinterpret_cast<int1_t>(v10 == 0xffffffffffffffff)) 
                            goto addr_4ec1_83; else 
                            goto addr_52ca_87;
                        addr_5e7a_55:
                        rax118 = fun_46a0("LS_COLORS", "LS_COLORS");
                        rsp119 = reinterpret_cast<void*>(rsp61 - 8 + 8);
                        v120 = rax118;
                        if (!rax118 || !*reinterpret_cast<void***>(rax118)) {
                            rax121 = fun_46a0("COLORTERM", "COLORTERM");
                            rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp119) - 8 + 8);
                            if (!rax121 || !*reinterpret_cast<void***>(rax121)) {
                                rax122 = fun_46a0("TERM", "TERM");
                                rsp61 = rsp61 - 8 + 8;
                                if (!rax122 || !*reinterpret_cast<void***>(rax122)) {
                                    addr_6311_109:
                                    print_with_color = 0;
                                    goto addr_6000_110;
                                } else {
                                    v13 = *reinterpret_cast<void***>(&rbp71);
                                    rbp123 = reinterpret_cast<void**>("# Configuration file for dircolors, a utility to help you set the");
                                    r12_124 = rbx4;
                                    rbx125 = rax122;
                                    do {
                                        rdx62 = reinterpret_cast<void**>(5);
                                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                        eax126 = fun_4700(rbp123, "TERM ", 5, rbp123, "TERM ", 5);
                                        rsp61 = rsp61 - 8 + 8;
                                        if (eax126) 
                                            continue;
                                        rdx62 = reinterpret_cast<void**>(0);
                                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                        rsi60 = rbx125;
                                        eax127 = fun_4910(rbp123 + 5, rsi60);
                                        rsp61 = rsp61 - 8 + 8;
                                        if (!eax127) 
                                            break;
                                        rax128 = fun_4860(rbp123, rbp123);
                                        rsp61 = rsp61 - 8 + 8;
                                        rsi60 = reinterpret_cast<void**>("# Configuration file for dircolors, a utility to help you set the");
                                        rbp123 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp123) + reinterpret_cast<unsigned char>(rax128) + 1);
                                    } while (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp123) - reinterpret_cast<unsigned char>("# Configuration file for dircolors, a utility to help you set the")) <= reinterpret_cast<uint64_t>("env"));
                                    goto addr_630a_115;
                                }
                            } else {
                                addr_6000_110:
                                zf129 = print_with_color == 0;
                                eax130 = directories_first;
                                if (zf129) {
                                    addr_5043_56:
                                    zf131 = directories_first == 0;
                                    if (zf131) {
                                        addr_5053_116:
                                        zf132 = dereference == 0;
                                        if (zf132) {
                                            zf133 = immediate_dirs == 0;
                                            eax134 = 1;
                                            if (zf133 && (zf135 = indicator_style == 3, !zf135)) {
                                                cf136 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(format) < reinterpret_cast<unsigned char>(1));
                                                eax134 = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + cf136)) & 0xfffffffe) + 3;
                                            }
                                            dereference = eax134;
                                            goto addr_5074_120;
                                        }
                                    } else {
                                        addr_504c_121:
                                        check_symlink_mode = 1;
                                        goto addr_5053_116;
                                    }
                                } else {
                                    tabsize = reinterpret_cast<void**>(0);
                                    if (*reinterpret_cast<signed char*>(&eax130)) 
                                        goto addr_504c_121;
                                    al137 = is_colored(13, 13);
                                    rsp61 = rsp61 - 8 + 8;
                                    if (al137) 
                                        goto addr_504c_121;
                                    al138 = is_colored(14, 14);
                                    rsp61 = rsp61 - 8 + 8;
                                    if (al138) 
                                        goto addr_6488_125; else 
                                        goto addr_604b_126;
                                }
                            }
                        } else {
                            rax139 = xstrdup(rax118, rsi60, rax118, rsi60);
                            rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp119) - 8 + 8);
                            v13 = rbx4;
                            color_buf = rax139;
                            v20 = rax139;
                            while (1) {
                                eax140 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v120));
                                if (*reinterpret_cast<signed char*>(&eax140) == 42) {
                                    rax141 = xmalloc(40, rsi60, 40, rsi60);
                                    rsp142 = reinterpret_cast<void*>(rsp61 - 8 + 8);
                                    r12_143 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp142) + 56);
                                    rax144 = color_ext_list;
                                    rcx39 = rax141;
                                    color_ext_list = rax141;
                                    *reinterpret_cast<void***>(rax141 + 32) = rax144;
                                    *reinterpret_cast<void***>(rax141 + 8) = v20;
                                    al145 = get_funky_string(r15_9, r12_143, 1, rcx39, r8_15);
                                    rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp142) - 8 + 8);
                                    if (!al145) 
                                        goto addr_64fe_130;
                                    zf146 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v120 + 1) == 61);
                                    v120 = v120 + 1 + 1;
                                    if (!zf146) 
                                        goto addr_64fe_130;
                                    rdx62 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                    rcx39 = rax141 + 16;
                                    rsi60 = r12_143;
                                    *reinterpret_cast<void***>(rax141 + 24) = v20;
                                    al147 = get_funky_string(r15_9, rsi60, 0, rcx39, r8_15);
                                    rsp61 = rsp61 - 8 + 8;
                                    if (!al147) 
                                        goto addr_653c_133;
                                } else {
                                    if (*reinterpret_cast<signed char*>(&eax140) == 58) {
                                        ++v120;
                                    } else {
                                        if (!*reinterpret_cast<signed char*>(&eax140)) 
                                            goto addr_649a_137;
                                        eax148 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v120 + 1));
                                        if (!*reinterpret_cast<signed char*>(&eax148)) 
                                            goto addr_64fe_130;
                                        zf149 = *reinterpret_cast<unsigned char*>(v120 + 2) == 61;
                                        v120 = v120 + 3;
                                        if (!zf149) 
                                            goto addr_64fe_130;
                                        *reinterpret_cast<int32_t*>(&rbx150) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx150) + 4) = 0;
                                        rsi151 = reinterpret_cast<void**>("lc");
                                        r13_152 = reinterpret_cast<void**>(rsp61 + 85);
                                        do {
                                            rax153 = fun_4660(r13_152, rsi151, rdx62, rcx39, r8_15);
                                            rsp61 = rsp61 - 8 + 8;
                                            if (!*reinterpret_cast<int32_t*>(&rax153)) 
                                                break;
                                            ++rbx150;
                                            rsi151 = *reinterpret_cast<void***>(0x248c0 + rbx150 * 8);
                                        } while (rsi151);
                                        goto addr_5f71_143;
                                        rdx62 = reinterpret_cast<void**>(0);
                                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                        rsi60 = reinterpret_cast<void**>(rsp61 + 56);
                                        rcx39 = reinterpret_cast<void**>((static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx150)) << 4) + 0x25060);
                                        *reinterpret_cast<void***>(rcx39 + 8) = v20;
                                        al154 = get_funky_string(r15_9, rsi60, 0, rcx39, r8_15);
                                        rsp61 = rsp61 - 8 + 8;
                                        if (!al154) 
                                            goto addr_5f71_143;
                                    }
                                }
                            }
                        }
                        rbp71 = reinterpret_cast<int32_t>(v13);
                        rbx4 = r12_124;
                        goto addr_6000_110;
                        addr_630a_115:
                        rbp71 = reinterpret_cast<int32_t>(v13);
                        rbx4 = r12_124;
                        goto addr_6311_109;
                        addr_5074_120:
                        zf155 = recursive == 0;
                        if (!zf155) {
                            r8_15 = reinterpret_cast<void**>(0x6b80);
                            rax156 = hash_initialize(30);
                            active_dir_set = rax156;
                            if (!rax156) 
                                goto addr_677e_25;
                            r8_15 = free;
                            rcx39 = malloc;
                            rdx62 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rsi60) = 0;
                            *reinterpret_cast<int32_t*>(&rsi60 + 4) = 0;
                            _obstack_begin(0x260e0);
                            rsp61 = rsp61 - 8 + 8 - 8 + 8;
                        }
                        rax157 = fun_46a0("TZ", "TZ");
                        rdi158 = rax157;
                        rax159 = tzalloc(rdi158, rsi60, rdx62, rcx39, r8_15);
                        rsp160 = reinterpret_cast<void*>(rsp61 - 8 + 8 - 8 + 8);
                        localtz = rax159;
                        eax161 = sort_type;
                        if (!(eax161 - 3 & 0xfffffffd) || ((zf162 = format == 0, zf162) || ((zf163 = print_scontext == 0, !zf163) || (zf164 = print_block_size == 0, !zf164)))) {
                            format_needs_stat = 1;
                            eax165 = 0;
                        } else {
                            zf166 = recursive == 0;
                            format_needs_stat = 0;
                            eax165 = 1;
                            if (zf166 && ((zf167 = print_with_color == 0, zf167) && (zf168 = indicator_style == 0, zf168))) {
                                eax165 = directories_first;
                            }
                        }
                        format_needs_type = *reinterpret_cast<unsigned char*>(&eax165);
                        format_needs_type = reinterpret_cast<unsigned char>(format_needs_type & 1);
                        zf169 = dired == 0;
                        if (!zf169) {
                            r13_170 = free;
                            r12_171 = malloc;
                            _obstack_begin(0x261a0);
                            r8_15 = r13_170;
                            rcx39 = r12_171;
                            rdx62 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                            rdi158 = reinterpret_cast<void**>(0x26140);
                            _obstack_begin(0x26140);
                            rsp160 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp160) - 8 + 8 - 8 + 8);
                        }
                        zf172 = print_hyperlink == 0;
                        if (!zf172) {
                            *reinterpret_cast<int32_t*>(&rax173) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax173) + 4) = 0;
                            while (1) {
                                if (rax173 <= 90) {
                                    rdx62 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                    if (*reinterpret_cast<int32_t*>(&rax173) > 64 || (*reinterpret_cast<uint32_t*>(&rdi158) = static_cast<uint32_t>(rax173 - 48), *reinterpret_cast<int32_t*>(&rdi158 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi158) <= 9)) {
                                        addr_6118_158:
                                        *reinterpret_cast<unsigned char*>(0x25fe0 + rax173) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(0x25fe0 + rax173) | *reinterpret_cast<unsigned char*>(&rdx62));
                                        ++rax173;
                                        if (rax173 == 0x100) 
                                            break; else 
                                            continue;
                                    } else {
                                        if (*reinterpret_cast<int32_t*>(&rax173) - 45 <= 1) 
                                            goto addr_6144_160;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&rdi158) = static_cast<uint32_t>(rax173 - 97);
                                    *reinterpret_cast<int32_t*>(&rdi158 + 4) = 0;
                                    rdx62 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&rdi158) <= 25) 
                                        goto addr_6118_158;
                                    if (*reinterpret_cast<int32_t*>(&rax173) - 45 <= 1) 
                                        goto addr_6144_160;
                                }
                                if (*reinterpret_cast<int32_t*>(&rax173) == 0x7e) {
                                    addr_6144_160:
                                    rdx62 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                                    *reinterpret_cast<unsigned char*>(0x25fe0 + rax173) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(0x25fe0 + rax173) | 1);
                                    ++rax173;
                                    if (rax173 == 0x100) 
                                        break;
                                } else {
                                    *reinterpret_cast<unsigned char*>(&rdx62) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax173) == 95);
                                    goto addr_6118_158;
                                }
                            }
                            rax174 = xgethostname(rdi158);
                            rsp160 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp160) - 8 + 8);
                            rcx39 = reinterpret_cast<void**>(0x1bb0a);
                            if (rax174) 
                                goto addr_616b_167;
                        } else {
                            addr_5132_168:
                            cwd_n_alloc = reinterpret_cast<void**>(100);
                            rax176 = xnmalloc(100, 0xd0, rdx62, rcx39, r8_15, r9_175);
                            *reinterpret_cast<void**>(&r12_76) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14d3) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp71)));
                            *reinterpret_cast<int32_t*>(&r12_76 + 4) = 0;
                            cwd_n_used = reinterpret_cast<void**>(0);
                            cwd_file = rax176;
                            clear_files(100, 0xd0, 100, 0xd0);
                            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp160) - 8 + 8 - 8 + 8);
                            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_76)) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<void**>(&r12_76) == 0)) {
                                zf177 = immediate_dirs == 0;
                                if (!zf177) {
                                    rcx39 = reinterpret_cast<void**>(0x1bb0a);
                                    rdx37 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx37 + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&rsi34) = 3;
                                    *reinterpret_cast<int32_t*>(&rsi34 + 4) = 0;
                                    rdi178 = reinterpret_cast<void**>(".");
                                    gobble_file_constprop_0(".", 3, 1, 0x1bb0a, r8_15, r9_179);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                } else {
                                    rdx37 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx37 + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&rsi34) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi34 + 4) = 0;
                                    rdi178 = reinterpret_cast<void**>(".");
                                    queue_directory(".", 0, 1, rcx39, r8_15);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                }
                                zf180 = cwd_n_used == 0;
                                rbp36 = pending_dirs;
                                if (!zf180) 
                                    goto addr_606f_173; else 
                                    goto addr_5b96_174;
                            } else {
                                do {
                                    rdi178 = *reinterpret_cast<void***>(rbx4 + rbp71 * 8);
                                    rcx39 = reinterpret_cast<void**>(0x1bb0a);
                                    rdx37 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&rdx37 + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&rsi34) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi34 + 4) = 0;
                                    ++rbp71;
                                    gobble_file_constprop_0(rdi178, 0, 1, 0x1bb0a, r8_15, r9_181);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                } while (reinterpret_cast<signed char>(r14d3) > reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rbp71)));
                                zf182 = cwd_n_used == 0;
                                if (!zf182) 
                                    goto addr_606f_173; else 
                                    goto addr_51a6_178;
                            }
                        }
                        rax174 = reinterpret_cast<void**>(0x1bb0a);
                        addr_616b_167:
                        hostname = rax174;
                        goto addr_5132_168;
                        addr_606f_173:
                        sort_files(rdi178, rsi34, 1, rcx39, r8_15, r9_183, rdi178, rsi34);
                        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                        zf184 = immediate_dirs == 0;
                        if (zf184) {
                            *reinterpret_cast<uint32_t*>(&rsi34) = 1;
                            *reinterpret_cast<int32_t*>(&rsi34 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdi178) = 0;
                            *reinterpret_cast<int32_t*>(&rdi178 + 4) = 0;
                            extract_dirs_from_files(0, 1, 1, rcx39, r8_15, r9_185);
                            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                        }
                        zf186 = cwd_n_used == 0;
                        if (zf186) {
                            addr_51a6_178:
                            less_or_equal187 = reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_76)) <= reinterpret_cast<int32_t>(1);
                            *reinterpret_cast<void**>(&r12_76) = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(*reinterpret_cast<void**>(&r12_76)) - 1);
                            *reinterpret_cast<int32_t*>(&r12_76 + 4) = 0;
                            rbp36 = pending_dirs;
                            if (!less_or_equal187) 
                                goto addr_51f5_28;
                        } else {
                            print_current_files(rdi178, rsi34, rdi178, rsi34);
                            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                            zf188 = pending_dirs == 0;
                            if (zf188) 
                                goto addr_59e1_29;
                            rdi189 = stdout;
                            tmp64_190 = dired_pos + 1;
                            dired_pos = tmp64_190;
                            rax191 = *reinterpret_cast<void***>(rdi189 + 40);
                            if (reinterpret_cast<unsigned char>(rax191) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi189 + 48))) 
                                goto addr_6674_27;
                            rbp36 = pending_dirs;
                            *reinterpret_cast<void***>(rdi189 + 40) = rax191 + 1;
                            *reinterpret_cast<void***>(rax191) = reinterpret_cast<void**>(10);
                            goto addr_51fe_30;
                        }
                        addr_5b96_174:
                        if (!rbp36) 
                            goto addr_59e1_29;
                        if (!*reinterpret_cast<void***>(rbp36 + 24)) {
                            print_dir_name = 0;
                        }
                        addr_51fe_30:
                        r8_15 = active_dir_set;
                        rdi38 = *reinterpret_cast<void***>(rbp36);
                        pending_dirs = *reinterpret_cast<void***>(rbp36 + 24);
                        if (!r8_15) 
                            goto addr_51c0_31;
                        if (rdi38) 
                            goto addr_51c0_31;
                        rax192 = g260f8;
                        rdx193 = reinterpret_cast<unsigned char>(rax192) - g260f0;
                        if (rdx193 <= 15) 
                            goto addr_6783_26;
                        rsi34 = r15_9;
                        g260f8 = rax192 + 0xfffffffffffffff0;
                        rdx37 = *reinterpret_cast<void***>(rax192 + 0xfffffffffffffff0);
                        v20 = rdx37;
                        rax42 = hash_remove(r8_15, rsi34);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                        if (rax42) 
                            goto addr_526d_33;
                        fun_4900("found", "src/ls.c", 0x70d, "main", r8_15);
                        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        goto addr_66dd_65;
                        addr_6488_125:
                        zf194 = color_symlink_as_referent == 0;
                        if (!zf194) 
                            goto addr_504c_121;
                        addr_604b_126:
                        al195 = is_colored(12, 12);
                        rsp61 = rsp61 - 8 + 8;
                        if (!al195) 
                            goto addr_5053_116;
                        zf196 = format == 0;
                        if (zf196) 
                            goto addr_504c_121;
                        goto addr_5053_116;
                        addr_64fe_130:
                        rbx4 = v13;
                        addr_5fa4_195:
                        rax197 = fun_4840();
                        *reinterpret_cast<int32_t*>(&rsi60) = 0;
                        *reinterpret_cast<int32_t*>(&rsi60 + 4) = 0;
                        rdx62 = rax197;
                        fun_4b70();
                        rdi198 = color_buf;
                        fun_4630(rdi198, rdi198);
                        rsp61 = rsp61 - 8 + 8 - 8 + 8 - 8 + 8;
                        rdi199 = color_ext_list;
                        while (rdi199) {
                            r12_200 = *reinterpret_cast<void***>(rdi199 + 32);
                            fun_4630(rdi199, rdi199);
                            rsp61 = rsp61 - 8 + 8;
                            rdi199 = r12_200;
                        }
                        print_with_color = 0;
                        addr_5ff2_199:
                        zf201 = g250d0 == 6;
                        if (zf201 && (rdi202 = g250d8, rdx62 = reinterpret_cast<void**>(6), *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0, rsi60 = reinterpret_cast<void**>("target"), eax203 = fun_4700(rdi202, "target", 6, rdi202, "target", 6), rsp61 = rsp61 - 8 + 8, !eax203)) {
                            color_symlink_as_referent = 1;
                            goto addr_6000_110;
                        }
                        addr_653c_133:
                        goto addr_64fe_130;
                        addr_649a_137:
                        rbx4 = v13;
                        goto addr_5ff2_199;
                        addr_5f71_143:
                        rbx4 = v13;
                        rax204 = quote(r13_152, r13_152);
                        fun_4840();
                        rcx39 = rax204;
                        fun_4b70();
                        rsp61 = rsp61 - 8 + 8 - 8 + 8 - 8 + 8;
                        goto addr_5fa4_195;
                    }
                }
            }
        }
    }
    addr_59e1_29:
    zf205 = print_with_color == 0;
    if (!zf205 && (zf206 = used_color == 0, !zf206)) {
        zf207 = color_indicator == 2;
        if (!zf207 || ((rax208 = g25068, *rax208 != 0x5b1b) || ((zf209 = g25070 == 1, !zf209) || (rax210 = g25078, *rax210 != 0x6d)))) {
            put_indicator(0x25060, rsi34, rdx37, rcx39, r8_15, r9_211, v13, v11);
            put_indicator(0x25070, rsi34, rdx37, rcx39, r8_15, r9_212, v13, v11);
        }
        rdi213 = stdout;
        fun_4c20(rdi213, rsi34, rdi213, rsi34);
        signal_setup(0, rsi34, rdx37, rcx39, r8_15, r9_214);
        ebx215 = stop_signal_count;
        if (ebx215) {
            do {
                fun_46d0();
                --ebx215;
            } while (ebx215);
        }
        edi216 = interrupt_signal;
        if (edi216) {
            fun_46d0();
        }
    }
    zf217 = dired == 0;
    if (!zf217) {
        dired_dump_obstack("//DIRED//", 0x261a0, rdx37, rcx39, r8_15);
        dired_dump_obstack("//SUBDIRED//", 0x26140, rdx37, rcx39, r8_15);
        eax218 = get_quoting_style();
        rsi34 = reinterpret_cast<void**>("//DIRED-OPTIONS// --quoting-style=%s\n");
        *reinterpret_cast<uint32_t*>(&rax219) = eax218;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax219) + 4) = 0;
        rdx37 = *reinterpret_cast<void***>(0x24a40 + rax219 * 8);
        fun_4b20(1, "//DIRED-OPTIONS// --quoting-style=%s\n", rdx37, rcx39, r8_15);
    }
    rbp220 = active_dir_set;
    if (!rbp220) {
        addr_5a29_212:
        *reinterpret_cast<int32_t*>(&rax221) = exit_status;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax221) + 4) = 0;
        rdx222 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
        if (!rdx222) {
            return rax221;
        }
    } else {
        rax223 = hash_get_n_entries(rbp220, rsi34, rdx37, rcx39, r8_15);
        if (rax223) {
            fun_4900("hash_get_n_entries (active_dir_set) == 0", "src/ls.c", 0x741, "main", r8_15);
            goto addr_6762_23;
        } else {
            hash_free(rbp220, rsi34, rdx37, rcx39, r8_15);
            goto addr_5a29_212;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_67d3() {
    __asm__("cli ");
    __libc_start_main(0x4d10, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x25008;

void fun_4680(int64_t rdi);

int64_t fun_6873() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_4680(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_68b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

uint64_t fun_68c3(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

struct s28 {
    int64_t f0;
    int64_t f8;
};

struct s29 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_68d3(struct s28* rdi, struct s29* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f0 == rsi->f0) {
        rax3 = rsi->f8;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f8 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

void fun_68f3(int32_t edi) {
    int32_t eax2;

    __asm__("cli ");
    eax2 = interrupt_signal;
    if (!eax2) {
        interrupt_signal = edi;
    }
    return;
}

void fun_6b83() {
    __asm__("cli ");
    goto fun_4630;
}

void fun_6fb3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_4660;
}

void fun_6fc3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_4660;
}

void fun_7203(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto 0x7180;
}

void fun_7223() {
    __asm__("cli ");
    goto 0x7180;
}

void fun_74b3() {
    int32_t eax1;
    int32_t eax2;

    __asm__("cli ");
    eax1 = interrupt_signal;
    if (!eax1) {
        eax2 = stop_signal_count;
        stop_signal_count = eax2 + 1;
    }
    return;
}

int64_t filevercmp(int64_t rdi, int64_t rsi);

void fun_8233(int64_t* rdi, int64_t* rsi) {
    int64_t rdi3;
    int64_t rsi4;
    int64_t rax5;

    __asm__("cli ");
    rdi3 = *rsi;
    rsi4 = *rdi;
    rax5 = filevercmp(rdi3, rsi4);
    if (*reinterpret_cast<int32_t*>(&rax5)) {
        return;
    } else {
        goto fun_4660;
    }
}

void fun_8343(int64_t* rdi, int64_t* rsi) {
    int64_t rdi3;
    int64_t rsi4;
    int64_t rax5;

    __asm__("cli ");
    rdi3 = *rdi;
    rsi4 = *rsi;
    rax5 = filevercmp(rdi3, rsi4);
    if (*reinterpret_cast<int32_t*>(&rax5)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s30 {
    signed char[72] pad72;
    int64_t f48;
};

struct s31 {
    signed char[72] pad72;
    int64_t f48;
};

void fun_8383(struct s30* rdi, struct s31* rsi) {
    __asm__("cli ");
    if (reinterpret_cast<uint1_t>(rsi->f48 < rdi->f48) - reinterpret_cast<uint1_t>(rsi->f48 > rdi->f48)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s32 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s33 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

int64_t fun_83c3(struct s32* rdi, struct s33* rsi) {
    int64_t rcx3;
    int32_t edx4;
    int64_t rdx5;
    int32_t r8d6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx4 = 0;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78);
    *reinterpret_cast<uint32_t*>(&rdx5) = edx4 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    r8d6 = static_cast<int32_t>(rdx5 + rcx3 * 2);
    if (r8d6) {
        *reinterpret_cast<int32_t*>(&rax7) = r8d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        goto fun_4660;
    }
}

struct s34 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s35 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

int64_t fun_8423(struct s34* rdi, struct s35* rsi) {
    int64_t rcx3;
    int32_t edx4;
    int64_t rdx5;
    int32_t r8d6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx4 = 0;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78);
    *reinterpret_cast<uint32_t*>(&rdx5) = edx4 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    r8d6 = static_cast<int32_t>(rdx5 + rcx3 * 2);
    if (r8d6) {
        *reinterpret_cast<int32_t*>(&rax7) = r8d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        goto fun_4660;
    }
}

struct s36 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

struct s37 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

int64_t fun_8483(struct s36* rdi, struct s37* rsi) {
    int64_t rcx3;
    int32_t edx4;
    int64_t rdx5;
    int32_t r8d6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f60 < rdi->f60)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f60 > rdi->f60));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx4 = 0;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(rsi->f68 < rdi->f68);
    *reinterpret_cast<uint32_t*>(&rdx5) = edx4 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f68 > rdi->f68));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    r8d6 = static_cast<int32_t>(rdx5 + rcx3 * 2);
    if (r8d6) {
        *reinterpret_cast<int32_t*>(&rax7) = r8d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        goto fun_4660;
    }
}

struct s38 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

struct s39 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

int64_t fun_84e3(struct s38* rdi, struct s39* rsi) {
    int64_t rcx3;
    int32_t edx4;
    int64_t rdx5;
    int32_t r8d6;
    int64_t rax7;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f80 < rdi->f80)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f80 > rdi->f80));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx4 = 0;
    *reinterpret_cast<unsigned char*>(&edx4) = reinterpret_cast<uint1_t>(rsi->f88 < rdi->f88);
    *reinterpret_cast<uint32_t*>(&rdx5) = edx4 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f88 > rdi->f88));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    r8d6 = static_cast<int32_t>(rdx5 + rcx3 * 2);
    if (r8d6) {
        *reinterpret_cast<int32_t*>(&rax7) = r8d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        goto fun_4660;
    }
}

int32_t fun_4ac0(struct s11* rdi, struct s11* rsi);

void fun_8793(int64_t rdi, int64_t rsi) {
    void*** rax3;

    __asm__("cli ");
    rax3 = fun_46f0();
    *rax3 = reinterpret_cast<void**>(0);
    goto fun_4ac0;
}

void fun_87c3(int64_t rdi, int64_t rsi) {
    void*** rax3;

    __asm__("cli ");
    rax3 = fun_46f0();
    *rax3 = reinterpret_cast<void**>(0);
    goto fun_4ac0;
}

struct s40 {
    signed char[72] pad72;
    int64_t f48;
};

struct s41 {
    signed char[72] pad72;
    int64_t f48;
};

void fun_87f3(struct s40* rdi, struct s41* rsi) {
    void*** rax3;

    __asm__("cli ");
    if (reinterpret_cast<uint1_t>(rdi->f48 < rsi->f48) - reinterpret_cast<uint1_t>(rdi->f48 > rsi->f48)) {
        return;
    } else {
        rax3 = fun_46f0();
        *rax3 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s42 {
    signed char[72] pad72;
    int64_t f48;
};

struct s43 {
    signed char[72] pad72;
    int64_t f48;
};

void fun_8853(struct s42* rdi, struct s43* rsi) {
    void*** rax3;

    __asm__("cli ");
    if (reinterpret_cast<uint1_t>(rsi->f48 < rdi->f48) - reinterpret_cast<uint1_t>(rsi->f48 > rdi->f48)) {
        return;
    } else {
        rax3 = fun_46f0();
        *rax3 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s44 {
    signed char[72] pad72;
    int64_t f48;
};

struct s45 {
    signed char[72] pad72;
    int64_t f48;
};

void fun_88b3(struct s44* rdi, struct s45* rsi) {
    __asm__("cli ");
    if (reinterpret_cast<uint1_t>(rdi->f48 < rsi->f48) - reinterpret_cast<uint1_t>(rdi->f48 > rsi->f48)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s46 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

struct s47 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

void fun_88f3(struct s46* rdi, struct s47* rsi) {
    void* rax3;
    void** edx4;
    void** rdi5;
    void** rsi6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void* rdx10;
    void** edx11;
    void** rsi12;
    void** rdi13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    void* rax17;

    __asm__("cli ");
    rax3 = rsi->fc8;
    if (!rax3) {
        edx4 = rsi->fc4;
        rdi5 = rsi->f0;
        rsi6 = filename_quoting_options;
        rax3 = quote_name_width(rdi5, rsi6, edx4, rcx7, r8_8, r9_9);
    }
    rdx10 = rdi->fc8;
    if (!rdx10) {
        edx11 = rdi->fc4;
        rsi12 = filename_quoting_options;
        rdi13 = rdi->f0;
        rax17 = quote_name_width(rdi13, rsi12, edx11, rcx14, r8_15, r9_16);
        rdx10 = rax17;
    }
    if (*reinterpret_cast<int32_t*>(&rax3) - *reinterpret_cast<int32_t*>(&rdx10)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s48 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

struct s49 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

void fun_8973(struct s48* rdi, struct s49* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f80 < rsi->f80);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f80 > rsi->f80));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 < rsi->f88)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 > rsi->f88));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s50 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s51 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_89f3(struct s50* rdi, struct s51* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s52 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s53 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_8a63(struct s52* rdi, struct s53* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s54 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

struct s55 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

void fun_8ad3(struct s54* rdi, struct s55* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rsi->f80 < rdi->f80);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f80 > rdi->f80));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f88 < rdi->f88)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f88 > rdi->f88));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s56 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

struct s57 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

void fun_8b53(struct s56* rdi, struct s57* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f60 < rsi->f60);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f60 > rsi->f60));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 < rsi->f68)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 > rsi->f68));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s58 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s59 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_8bc3(struct s58* rdi, struct s59* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s60 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s61 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_8c33(struct s60* rdi, struct s61* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s62 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

struct s63 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

void fun_8ca3(struct s62* rdi, struct s63* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;
    void*** rax6;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rsi->f60 < rdi->f60);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f60 > rdi->f60));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f68 < rdi->f68)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f68 > rdi->f68));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        rax6 = fun_46f0();
        *rax6 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

unsigned char sort_reverse = 0;

void mpsort(void** rdi, void** rsi, int64_t rdx);

void fun_8dc8() {
    int64_t rcx1;
    int32_t eax2;
    void** rdi3;
    void** r8_4;
    void** r8_5;
    void** rdx6;
    void** rax7;
    void** rsi8;
    int64_t rbx9;
    int64_t rdx10;
    int64_t rax11;
    int64_t rdx12;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx1) = sort_type;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx1) + 4) = 0;
    if (!eax2) {
        rdi3 = sorted_file;
        r8_4 = cwd_n_used;
    } else {
        if (*reinterpret_cast<uint32_t*>(&rcx1) == 4) {
            fun_4900("sort_type != sort_version", "src/ls.c", 0x1008, "sort_files", r8_5);
        } else {
            r8_4 = cwd_n_used;
            rdi3 = sorted_file;
            if (r8_4) {
                rdx6 = cwd_file;
                rax7 = rdi3;
                rsi8 = rdi3 + reinterpret_cast<unsigned char>(r8_4) * 8;
                do {
                    *reinterpret_cast<void***>(rax7) = rdx6;
                    rax7 = rax7 + 8;
                    rdx6 = rdx6 + 0xd0;
                } while (rsi8 != rax7);
            }
        }
    }
    if (*reinterpret_cast<uint32_t*>(&rcx1) == 5) {
        *reinterpret_cast<uint32_t*>(&rbx9) = time_type;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx1) = static_cast<uint32_t>(rbx9 + 5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx1) + 4) = 0;
    }
    *reinterpret_cast<uint32_t*>(&rdx10) = directories_first;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = sort_reverse;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    rdx12 = *reinterpret_cast<int64_t*>(0x23fc0 + (rdx10 + (rax11 + (1 + rcx1 * 2) * 2) * 2) * 8);
    mpsort(rdi3, r8_4, rdx12);
}

struct s64 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

struct s65 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
};

void fun_8f33(struct s64* rdi, struct s65* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f60 < rsi->f60);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f60 > rsi->f60));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 < rsi->f68)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 > rsi->f68));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s66 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

struct s67 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
};

void fun_8f83(struct s66* rdi, struct s67* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f80 < rsi->f80);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f80 > rsi->f80));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 < rsi->f88)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 > rsi->f88));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s68 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s69 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_8fe3(struct s68* rdi, struct s69* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s70 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

struct s71 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
};

void fun_9033(struct s70* rdi, struct s71* rsi) {
    int32_t edx3;
    int64_t rdx4;
    int64_t rax5;

    __asm__("cli ");
    edx3 = 0;
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
    *reinterpret_cast<uint32_t*>(&rdx4) = edx3 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (static_cast<int32_t>(rax5 + rdx4 * 2)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s72 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

struct s73 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

void fun_9083(struct s72* rdi, struct s73* rsi) {
    void* rax3;
    void** edx4;
    void** rdi5;
    void** rsi6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void* rdx10;
    void** edx11;
    void** rsi12;
    void** rdi13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    void* rax17;
    void*** rax18;

    __asm__("cli ");
    rax3 = rsi->fc8;
    if (!rax3) {
        edx4 = rsi->fc4;
        rdi5 = rsi->f0;
        rsi6 = filename_quoting_options;
        rax3 = quote_name_width(rdi5, rsi6, edx4, rcx7, r8_8, r9_9);
    }
    rdx10 = rdi->fc8;
    if (!rdx10) {
        edx11 = rdi->fc4;
        rsi12 = filename_quoting_options;
        rdi13 = rdi->f0;
        rax17 = quote_name_width(rdi13, rsi12, edx11, rcx14, r8_15, r9_16);
        rdx10 = rax17;
    }
    if (*reinterpret_cast<int32_t*>(&rax3) - *reinterpret_cast<int32_t*>(&rdx10)) {
        return;
    } else {
        rax18 = fun_46f0();
        *rax18 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

struct s74 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

struct s75 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

void fun_9113(struct s74* rdi, struct s75* rsi) {
    void* rax3;
    void** edx4;
    void** rsi5;
    void** rdi6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void* rdx10;
    void** edx11;
    void** rsi12;
    void** rdi13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    void* rax17;
    void*** rax18;

    __asm__("cli ");
    rax3 = rdi->fc8;
    if (!rax3) {
        edx4 = rdi->fc4;
        rsi5 = filename_quoting_options;
        rdi6 = rdi->f0;
        rax3 = quote_name_width(rdi6, rsi5, edx4, rcx7, r8_8, r9_9);
    }
    rdx10 = rsi->fc8;
    if (!rdx10) {
        edx11 = rsi->fc4;
        rsi12 = filename_quoting_options;
        rdi13 = rsi->f0;
        rax17 = quote_name_width(rdi13, rsi12, edx11, rcx14, r8_15, r9_16);
        rdx10 = rax17;
    }
    if (*reinterpret_cast<int32_t*>(&rax3) - *reinterpret_cast<int32_t*>(&rdx10)) {
        return;
    } else {
        rax18 = fun_46f0();
        *rax18 = reinterpret_cast<void**>(0);
        goto fun_4ac0;
    }
}

void fun_9253(void*** rdi, void*** rsi) {
    void** rdi3;
    struct s11* rax4;
    struct s11* rbp5;
    void** rdi6;
    struct s11* rax7;
    struct s11* r12_8;
    void*** rax9;
    int32_t eax10;

    __asm__("cli ");
    rdi3 = *rdi;
    rax4 = fun_48e0(rdi3, 46);
    rbp5 = rax4;
    rdi6 = *rsi;
    rax7 = fun_48e0(rdi6, 46);
    r12_8 = rax7;
    if (!r12_8) {
        r12_8 = reinterpret_cast<struct s11*>(0x1bb0a);
    }
    if (!rbp5) {
        rbp5 = reinterpret_cast<struct s11*>(0x1bb0a);
    }
    rax9 = fun_46f0();
    *rax9 = reinterpret_cast<void**>(0);
    eax10 = fun_4ac0(rbp5, r12_8);
    if (eax10) {
        return;
    } else {
        goto fun_4ac0;
    }
}

void fun_92e3(void*** rdi, void*** rsi) {
    void** rdi3;
    struct s11* rax4;
    struct s11* rbp5;
    void** rdi6;
    struct s11* rax7;
    struct s11* r12_8;
    void*** rax9;
    int32_t eax10;

    __asm__("cli ");
    rdi3 = *rsi;
    rax4 = fun_48e0(rdi3, 46);
    rbp5 = rax4;
    rdi6 = *rdi;
    rax7 = fun_48e0(rdi6, 46);
    r12_8 = rax7;
    if (!r12_8) {
        r12_8 = reinterpret_cast<struct s11*>(0x1bb0a);
    }
    if (!rbp5) {
        rbp5 = reinterpret_cast<struct s11*>(0x1bb0a);
    }
    rax9 = fun_46f0();
    *rax9 = reinterpret_cast<void**>(0);
    eax10 = fun_4ac0(rbp5, r12_8);
    if (eax10) {
        return;
    } else {
        goto fun_4ac0;
    }
}

struct s76 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

struct s77 {
    void** f0;
    signed char[195] pad196;
    void** fc4;
    signed char[3] pad200;
    void* fc8;
};

void fun_9373(struct s76* rdi, struct s77* rsi) {
    void* rax3;
    void** edx4;
    void** rsi5;
    void** rdi6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void* rdx10;
    void** edx11;
    void** rsi12;
    void** rdi13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    void* rax17;

    __asm__("cli ");
    rax3 = rdi->fc8;
    if (!rax3) {
        edx4 = rdi->fc4;
        rsi5 = filename_quoting_options;
        rdi6 = rdi->f0;
        rax3 = quote_name_width(rdi6, rsi5, edx4, rcx7, r8_8, r9_9);
    }
    rdx10 = rsi->fc8;
    if (!rdx10) {
        edx11 = rsi->fc4;
        rsi12 = filename_quoting_options;
        rdi13 = rsi->f0;
        rax17 = quote_name_width(rdi13, rsi12, edx11, rcx14, r8_15, r9_16);
        rdx10 = rax17;
    }
    if (*reinterpret_cast<int32_t*>(&rax3) - *reinterpret_cast<int32_t*>(&rdx10)) {
        return;
    } else {
        goto fun_4660;
    }
}

struct s78 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
    signed char[24] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s79 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
    signed char[24] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a333(struct s78* rdi, struct s79* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_a3b0_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_a3b0_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rdi->f80 < rsi->f80);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f80 > rsi->f80));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 < rsi->f88)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f88 > rsi->f88));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s80 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s81 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a413(struct s80* rdi, struct s81* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_a469_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_a469_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_a469_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s82 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s83 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a4a3(struct s82* rdi, struct s83* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_a520_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_a520_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s84 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s85 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a573(struct s84* rdi, struct s85* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_a5c9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_a5c9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_a5c9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s86 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s87 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a603(struct s86* rdi, struct s87* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_a680_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_a680_3:
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f48 < rdi->f48)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f48 > rdi->f48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s88 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s89 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a6b3(struct s88* rdi, struct s89* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    void*** rax6;
    int32_t eax7;
    int64_t rax8;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 3);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 9);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            rax6 = fun_46f0();
            *rax6 = reinterpret_cast<void**>(0);
            goto fun_4ac0;
        } else {
            eax7 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax7 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax8) = eax7 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax8)) {
        return rax8;
    }
}

struct s90 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s91 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a773(struct s90* rdi, struct s91* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            goto fun_4660;
        } else {
            eax6 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s92 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s93 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a813(struct s92* rdi, struct s93* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    void*** rax6;
    int32_t eax7;
    int64_t rax8;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 3);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 9);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            rax6 = fun_46f0();
            *rax6 = reinterpret_cast<void**>(0);
            goto fun_4ac0;
        } else {
            eax7 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax7 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax8) = eax7 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax8)) {
        return rax8;
    }
}

struct s94 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s95 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a8d3(struct s94* rdi, struct s95* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_a950_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_a950_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s96 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s97 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_a9a3(struct s96* rdi, struct s97* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_aa20_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_aa20_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rsi->f70 < rdi->f70);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f70 > rdi->f70));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 < rdi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f78 > rdi->f78));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s98 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s99 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_aa73(struct s98* rdi, struct s99* rsi) {
    uint32_t ecx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    ecx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(ecx3 == 9);
    *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(ecx3 == 3);
    eax5 = eax4 | ecx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_aaf0_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_aaf0_3:
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f48 < rsi->f48)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f48 > rsi->f48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s100 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s101 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_ab23(struct s100* rdi, struct s101* rsi) {
    uint32_t ecx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    void*** rax8;

    __asm__("cli ");
    ecx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(ecx3 == 9);
    *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(ecx3 == 3);
    eax5 = eax4 | ecx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_aba0_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_aba0_3:
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f48 < rdi->f48)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f48 > rdi->f48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            rax8 = fun_46f0();
            *rax8 = reinterpret_cast<void**>(0);
            goto fun_4ac0;
        }
    } else {
        return rax7;
    }
}

struct s102 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s103 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_abf3(struct s102* rdi, struct s103* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_ac49_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_ac49_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_ac49_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s104 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s105 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_ac83(struct s104* rdi, struct s105* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            goto 0x7180;
        } else {
            eax6 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s106 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s107 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_ad23(struct s106* rdi, struct s107* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            goto 0x7180;
        } else {
            eax6 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s108 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s109 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_adc3(struct s108* rdi, struct s109* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_ae19_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_ae19_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_ae19_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s110 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s111 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_ae53(struct s110* rdi, struct s111* rsi) {
    uint32_t ecx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    ecx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(ecx3 == 9);
    *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(ecx3 == 3);
    eax5 = eax4 | ecx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            goto fun_4660;
        } else {
            eax6 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s112 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s113 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_aef3(struct s112* rdi, struct s113* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_af49_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_af49_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_af49_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s114 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s115 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_af83(struct s114* rdi, struct s115* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_afd9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_afd9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_afd9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s116 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s117 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b013(struct s116* rdi, struct s117* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b069_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b069_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b069_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s118 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s119 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b0a3(struct s118* rdi, struct s119* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b0f9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b0f9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b0f9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s120 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s121 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b133(struct s120* rdi, struct s121* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b189_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b189_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b189_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s122 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
    signed char[56] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s123 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
    signed char[56] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b1c3(struct s122* rdi, struct s123* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_b240_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_b240_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rdi->f60 < rsi->f60);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f60 > rsi->f60));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 < rsi->f68)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f68 > rsi->f68));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s124 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s125 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b293(struct s124* rdi, struct s125* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b2e9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b2e9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b2e9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s126 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s127 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b323(struct s126* rdi, struct s127* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b379_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b379_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b379_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s128 {
    int64_t f0;
    signed char[160] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s129 {
    int64_t f0;
    signed char[160] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b3b3(struct s128* rdi, struct s129* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdi6;
    int64_t rsi7;
    int64_t rax8;
    int32_t eax9;
    int64_t rax10;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            rdi6 = rsi->f0;
            rsi7 = rdi->f0;
            rax8 = filevercmp(rdi6, rsi7);
            if (*reinterpret_cast<int32_t*>(&rax8)) {
                return rax8;
            } else {
                goto fun_4660;
            }
        } else {
            eax9 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax9 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax10) = eax9 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax10)) {
        return rax10;
    }
}

struct s130 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s131 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b483(struct s130* rdi, struct s131* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b4d9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b4d9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b4d9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s132 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s133 {
    signed char[112] pad112;
    int64_t f70;
    int64_t f78;
    signed char[40] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b513(struct s132* rdi, struct s133* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_b590_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_b590_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rdi->f70 < rsi->f70);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f70 > rsi->f70));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 < rsi->f78)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f78 > rsi->f78));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s134 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s135 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b5e3(struct s134* rdi, struct s135* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b639_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b639_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b639_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s136 {
    signed char[168] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s137 {
    signed char[168] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b673(struct s136* rdi, struct s137* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 1;
            goto addr_b6c9_5;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
            goto addr_b6c9_5;
        } else {
            return 0xffffffff;
        }
    }
    addr_b6c9_5:
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        return rax7;
    }
}

struct s138 {
    int64_t f0;
    signed char[160] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s139 {
    int64_t f0;
    signed char[160] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b703(struct s138* rdi, struct s139* rsi) {
    uint32_t edx3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdi6;
    int64_t rsi7;
    int64_t rax8;
    int32_t eax9;
    int64_t rax10;

    __asm__("cli ");
    edx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(edx3 == 9);
    *reinterpret_cast<unsigned char*>(&edx3) = reinterpret_cast<uint1_t>(edx3 == 3);
    eax5 = eax4 | edx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) {
            rdi6 = rdi->f0;
            rsi7 = rsi->f0;
            rax8 = filevercmp(rdi6, rsi7);
            if (*reinterpret_cast<int32_t*>(&rax8)) {
                return rax8;
            } else {
                goto fun_4660;
            }
        } else {
            eax9 = 1;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax9 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax10) = eax9 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax10)) {
        return rax10;
    }
}

struct s140 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s141 {
    signed char[72] pad72;
    int64_t f48;
    signed char[88] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b7d3(struct s140* rdi, struct s141* rsi) {
    uint32_t ecx3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    void*** rax8;

    __asm__("cli ");
    ecx3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(ecx3 == 9);
    *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(ecx3 == 3);
    eax5 = eax4 | ecx3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_b850_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_b850_3:
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f48 < rsi->f48)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi->f48 > rsi->f48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            rax8 = fun_46f0();
            *rax8 = reinterpret_cast<void**>(0);
            goto fun_4ac0;
        }
    } else {
        return rax7;
    }
}

struct s142 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
    signed char[24] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s143 {
    signed char[128] pad128;
    int64_t f80;
    int64_t f88;
    signed char[24] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b8a3(struct s142* rdi, struct s143* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_b920_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_b920_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rsi->f80 < rdi->f80);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f80 > rdi->f80));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f88 < rdi->f88)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f88 > rdi->f88));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

struct s144 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
    signed char[56] pad168;
    uint32_t fa8;
    uint32_t fac;
};

struct s145 {
    signed char[96] pad96;
    int64_t f60;
    int64_t f68;
    signed char[56] pad168;
    int32_t fa8;
    uint32_t fac;
};

int64_t fun_b983(struct s144* rdi, struct s145* rsi) {
    uint32_t esi3;
    uint32_t eax4;
    uint32_t eax5;
    int32_t eax6;
    int64_t rax7;
    int32_t esi8;
    int64_t rsi9;
    int64_t rax10;

    __asm__("cli ");
    esi3 = rdi->fa8;
    *reinterpret_cast<unsigned char*>(&eax4) = reinterpret_cast<uint1_t>(esi3 == 9);
    *reinterpret_cast<unsigned char*>(&esi3) = reinterpret_cast<uint1_t>(esi3 == 3);
    eax5 = eax4 | esi3;
    if (rsi->fa8 == 3 || (rsi->fa8 == 9 || (rsi->fac & 0xf000) == 0x4000)) {
        if (*reinterpret_cast<signed char*>(&eax5)) 
            goto addr_ba00_3;
        eax6 = 1;
    } else {
        if (!*reinterpret_cast<signed char*>(&eax5)) {
            eax6 = 0;
        } else {
            return 0xffffffff;
        }
    }
    *reinterpret_cast<uint32_t*>(&rax7) = eax6 - static_cast<unsigned char>(reinterpret_cast<uint1_t>((rdi->fac & 0xf000) == 0x4000));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rax7)) {
        addr_ba00_3:
        esi8 = 0;
        *reinterpret_cast<unsigned char*>(&esi8) = reinterpret_cast<uint1_t>(rsi->f60 < rdi->f60);
        *reinterpret_cast<uint32_t*>(&rsi9) = esi8 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f60 > rdi->f60));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax10) = static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f68 < rdi->f68)) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi->f68 > rdi->f68));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(rax10 + rsi9 * 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax7)) {
            goto fun_4660;
        }
    } else {
        return rax7;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_4bf0();

void fun_dbe3(int32_t edi) {
    void** r13_2;
    void** rax3;
    void** rcx4;
    void** r8_5;
    int64_t r8_6;
    void** rbp7;
    void** rax8;
    void** r8_9;
    int64_t r8_10;
    void** rbp11;
    void** rax12;
    void** r8_13;
    int64_t r8_14;
    void** rbp15;
    void** rax16;
    void** r8_17;
    int64_t r8_18;
    void** rbp19;
    void** rax20;
    void** r8_21;
    int64_t r8_22;
    void** rbp23;
    void** rax24;
    void** r8_25;
    int64_t r8_26;
    void** rbp27;
    void** rax28;
    void** r8_29;
    int64_t r8_30;
    void** rbp31;
    void** rax32;
    void** r8_33;
    int64_t r8_34;
    void** rbp35;
    void** rax36;
    void** r8_37;
    int64_t r8_38;
    void** rbp39;
    void** rax40;
    void** r8_41;
    int64_t r8_42;
    void** rbp43;
    void** rax44;
    void** r8_45;
    int64_t r8_46;
    void** rbp47;
    void** rax48;
    void** r8_49;
    int64_t r8_50;
    void** rbp51;
    void** rax52;
    void** r8_53;
    int64_t r8_54;
    void** rbp55;
    void** rax56;
    void** r8_57;
    int64_t r8_58;
    void** rbp59;
    void** rax60;
    void** r8_61;
    int64_t r8_62;
    void** rbp63;
    void** rax64;
    void** r8_65;
    int64_t r8_66;
    void** rbp67;
    void** rax68;
    void** r8_69;
    int64_t r8_70;
    void** rbp71;
    void** rax72;
    void** r8_73;
    int64_t r8_74;
    void** rbp75;
    void** rax76;
    void** r8_77;
    int64_t r8_78;
    void** rbp79;
    void** rax80;
    void** r8_81;
    int64_t r8_82;
    void** rbp83;
    void** rax84;
    void** r8_85;
    int64_t r8_86;
    void** rbp87;
    void** rax88;
    void** r8_89;
    int64_t r8_90;
    void** rbp91;
    void** rax92;
    void** r8_93;
    int64_t r8_94;
    void** rbp95;
    void** rax96;
    void** r8_97;
    int64_t r8_98;
    void** rbp99;
    void** rax100;
    void** r8_101;
    int64_t r8_102;
    void** rbp103;
    void** rax104;
    void** r8_105;
    int64_t r8_106;
    void** rbp107;
    void** rax108;
    void** r8_109;
    int64_t r8_110;
    void** rbp111;
    void** rax112;
    void** r8_113;
    int64_t r8_114;
    void** rbp115;
    void** rax116;
    void** r8_117;
    int64_t r8_118;
    void** rbp119;
    void** rax120;
    void** r8_121;
    int64_t r8_122;
    void** rbp123;
    void** rax124;
    void** r8_125;
    int64_t r8_126;
    void** rbp127;
    void** rax128;
    void** r8_129;
    int64_t r8_130;
    void** rbp131;
    void** rax132;
    void** r8_133;
    int64_t r8_134;
    void** rbp135;
    void** rax136;
    void** r8_137;
    int64_t r8_138;
    void** rbp139;
    void** rax140;
    void** r8_141;
    int64_t r8_142;
    void** rbp143;
    void** rax144;
    void** r8_145;
    int64_t r8_146;
    void** rbp147;
    void** rax148;
    void** r8_149;
    int64_t r8_150;
    void** rbp151;
    void** rax152;
    void** r8_153;
    int64_t r8_154;
    void** rbp155;
    void** rax156;
    void** r8_157;
    int64_t r8_158;
    void** rbp159;
    void** rax160;
    void** r8_161;
    int64_t r8_162;
    void** rbp163;
    void** rax164;
    void** r8_165;
    int64_t r8_166;
    void** rbp167;
    void** rax168;
    void** r8_169;
    int64_t r8_170;
    void** rbp171;
    void** rax172;
    void** r8_173;
    int64_t r8_174;
    void** rbp175;
    void** rax176;
    void** r8_177;
    int64_t r8_178;
    void** rbp179;
    void** rax180;
    void** r8_181;
    int64_t r8_182;
    void** rbp183;
    void** rax184;
    void** r8_185;
    int64_t r8_186;
    void** rbp187;
    void** rax188;
    void** rbp189;
    void** r8_190;
    int64_t r8_191;
    int32_t eax192;
    void** rsi193;
    void** r8_194;
    int64_t r8_195;
    int64_t rax196;
    void** r14_197;
    void** rax198;
    void** rdx199;
    void** r8_200;
    int64_t r8_201;
    void** rax202;
    int32_t eax203;
    void** rbx204;
    void** rax205;
    void** r8_206;
    int64_t r8_207;
    void** r13_208;
    void** r8_209;
    int64_t r8_210;
    int64_t rax211;
    void** rax212;
    void** r8_213;
    int64_t r8_214;
    void** rax215;
    void** r8_216;
    int64_t r8_217;
    void** rax218;
    void** rdi219;
    void** r8_220;
    void** r9_221;
    void** v222;
    void** v223;
    void** v224;
    void** v225;
    void** v226;
    void** v227;

    __asm__("cli ");
    r13_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_4840();
            fun_4b20(1, rax3, r13_2, rcx4, r8_5, 1, rax3, r13_2, rcx4, r8_6);
            rbp7 = stdout;
            rax8 = fun_4840();
            fun_4990(rax8, rbp7, 5, rcx4, r8_9, rax8, rbp7, 5, rcx4, r8_10);
            rbp11 = stdout;
            rax12 = fun_4840();
            fun_4990(rax12, rbp11, 5, rcx4, r8_13, rax12, rbp11, 5, rcx4, r8_14);
            rbp15 = stdout;
            rax16 = fun_4840();
            fun_4990(rax16, rbp15, 5, rcx4, r8_17, rax16, rbp15, 5, rcx4, r8_18);
            rbp19 = stdout;
            rax20 = fun_4840();
            fun_4990(rax20, rbp19, 5, rcx4, r8_21, rax20, rbp19, 5, rcx4, r8_22);
            rbp23 = stdout;
            rax24 = fun_4840();
            fun_4990(rax24, rbp23, 5, rcx4, r8_25, rax24, rbp23, 5, rcx4, r8_26);
            rbp27 = stdout;
            rax28 = fun_4840();
            fun_4990(rax28, rbp27, 5, rcx4, r8_29, rax28, rbp27, 5, rcx4, r8_30);
            rbp31 = stdout;
            rax32 = fun_4840();
            fun_4990(rax32, rbp31, 5, rcx4, r8_33, rax32, rbp31, 5, rcx4, r8_34);
            rbp35 = stdout;
            rax36 = fun_4840();
            fun_4990(rax36, rbp35, 5, rcx4, r8_37, rax36, rbp35, 5, rcx4, r8_38);
            rbp39 = stdout;
            rax40 = fun_4840();
            fun_4990(rax40, rbp39, 5, rcx4, r8_41, rax40, rbp39, 5, rcx4, r8_42);
            rbp43 = stdout;
            rax44 = fun_4840();
            fun_4990(rax44, rbp43, 5, rcx4, r8_45, rax44, rbp43, 5, rcx4, r8_46);
            rbp47 = stdout;
            rax48 = fun_4840();
            fun_4990(rax48, rbp47, 5, rcx4, r8_49, rax48, rbp47, 5, rcx4, r8_50);
            rbp51 = stdout;
            rax52 = fun_4840();
            fun_4990(rax52, rbp51, 5, rcx4, r8_53, rax52, rbp51, 5, rcx4, r8_54);
            rbp55 = stdout;
            rax56 = fun_4840();
            fun_4990(rax56, rbp55, 5, rcx4, r8_57, rax56, rbp55, 5, rcx4, r8_58);
            rbp59 = stdout;
            rax60 = fun_4840();
            fun_4990(rax60, rbp59, 5, rcx4, r8_61, rax60, rbp59, 5, rcx4, r8_62);
            rbp63 = stdout;
            rax64 = fun_4840();
            fun_4990(rax64, rbp63, 5, rcx4, r8_65, rax64, rbp63, 5, rcx4, r8_66);
            rbp67 = stdout;
            rax68 = fun_4840();
            fun_4990(rax68, rbp67, 5, rcx4, r8_69, rax68, rbp67, 5, rcx4, r8_70);
            rbp71 = stdout;
            rax72 = fun_4840();
            fun_4990(rax72, rbp71, 5, rcx4, r8_73, rax72, rbp71, 5, rcx4, r8_74);
            rbp75 = stdout;
            rax76 = fun_4840();
            fun_4990(rax76, rbp75, 5, rcx4, r8_77, rax76, rbp75, 5, rcx4, r8_78);
            rbp79 = stdout;
            rax80 = fun_4840();
            fun_4990(rax80, rbp79, 5, rcx4, r8_81, rax80, rbp79, 5, rcx4, r8_82);
            rbp83 = stdout;
            rax84 = fun_4840();
            fun_4990(rax84, rbp83, 5, rcx4, r8_85, rax84, rbp83, 5, rcx4, r8_86);
            rbp87 = stdout;
            rax88 = fun_4840();
            fun_4990(rax88, rbp87, 5, rcx4, r8_89, rax88, rbp87, 5, rcx4, r8_90);
            rbp91 = stdout;
            rax92 = fun_4840();
            fun_4990(rax92, rbp91, 5, rcx4, r8_93, rax92, rbp91, 5, rcx4, r8_94);
            rbp95 = stdout;
            rax96 = fun_4840();
            fun_4990(rax96, rbp95, 5, rcx4, r8_97, rax96, rbp95, 5, rcx4, r8_98);
            rbp99 = stdout;
            rax100 = fun_4840();
            fun_4990(rax100, rbp99, 5, rcx4, r8_101, rax100, rbp99, 5, rcx4, r8_102);
            rbp103 = stdout;
            rax104 = fun_4840();
            fun_4990(rax104, rbp103, 5, rcx4, r8_105, rax104, rbp103, 5, rcx4, r8_106);
            rbp107 = stdout;
            rax108 = fun_4840();
            fun_4990(rax108, rbp107, 5, rcx4, r8_109, rax108, rbp107, 5, rcx4, r8_110);
            rbp111 = stdout;
            rax112 = fun_4840();
            fun_4990(rax112, rbp111, 5, rcx4, r8_113, rax112, rbp111, 5, rcx4, r8_114);
            rbp115 = stdout;
            rax116 = fun_4840();
            fun_4990(rax116, rbp115, 5, rcx4, r8_117, rax116, rbp115, 5, rcx4, r8_118);
            rbp119 = stdout;
            rax120 = fun_4840();
            fun_4990(rax120, rbp119, 5, rcx4, r8_121, rax120, rbp119, 5, rcx4, r8_122);
            rbp123 = stdout;
            rax124 = fun_4840();
            fun_4990(rax124, rbp123, 5, rcx4, r8_125, rax124, rbp123, 5, rcx4, r8_126);
            rbp127 = stdout;
            rax128 = fun_4840();
            fun_4990(rax128, rbp127, 5, rcx4, r8_129, rax128, rbp127, 5, rcx4, r8_130);
            rbp131 = stdout;
            rax132 = fun_4840();
            fun_4990(rax132, rbp131, 5, rcx4, r8_133, rax132, rbp131, 5, rcx4, r8_134);
            rbp135 = stdout;
            rax136 = fun_4840();
            fun_4990(rax136, rbp135, 5, rcx4, r8_137, rax136, rbp135, 5, rcx4, r8_138);
            rbp139 = stdout;
            rax140 = fun_4840();
            fun_4990(rax140, rbp139, 5, rcx4, r8_141, rax140, rbp139, 5, rcx4, r8_142);
            rbp143 = stdout;
            rax144 = fun_4840();
            fun_4990(rax144, rbp143, 5, rcx4, r8_145, rax144, rbp143, 5, rcx4, r8_146);
            rbp147 = stdout;
            rax148 = fun_4840();
            fun_4990(rax148, rbp147, 5, rcx4, r8_149, rax148, rbp147, 5, rcx4, r8_150);
            rbp151 = stdout;
            rax152 = fun_4840();
            fun_4990(rax152, rbp151, 5, rcx4, r8_153, rax152, rbp151, 5, rcx4, r8_154);
            rbp155 = stdout;
            rax156 = fun_4840();
            fun_4990(rax156, rbp155, 5, rcx4, r8_157, rax156, rbp155, 5, rcx4, r8_158);
            rbp159 = stdout;
            rax160 = fun_4840();
            fun_4990(rax160, rbp159, 5, rcx4, r8_161, rax160, rbp159, 5, rcx4, r8_162);
            rbp163 = stdout;
            rax164 = fun_4840();
            fun_4990(rax164, rbp163, 5, rcx4, r8_165, rax164, rbp163, 5, rcx4, r8_166);
            rbp167 = stdout;
            rax168 = fun_4840();
            fun_4990(rax168, rbp167, 5, rcx4, r8_169, rax168, rbp167, 5, rcx4, r8_170);
            rbp171 = stdout;
            rax172 = fun_4840();
            fun_4990(rax172, rbp171, 5, rcx4, r8_173, rax172, rbp171, 5, rcx4, r8_174);
            rbp175 = stdout;
            rax176 = fun_4840();
            fun_4990(rax176, rbp175, 5, rcx4, r8_177, rax176, rbp175, 5, rcx4, r8_178);
            rbp179 = stdout;
            rax180 = fun_4840();
            fun_4990(rax180, rbp179, 5, rcx4, r8_181, rax180, rbp179, 5, rcx4, r8_182);
            rbp183 = stdout;
            rax184 = fun_4840();
            fun_4990(rax184, rbp183, 5, rcx4, r8_185, rax184, rbp183, 5, rcx4, r8_186);
            rbp187 = stdout;
            rax188 = fun_4840();
            rbp189 = reinterpret_cast<void**>("ls");
            fun_4990(rax188, rbp187, 5, rcx4, r8_190, rax188, rbp187, 5, rcx4, r8_191);
            eax192 = ls_mode;
            if (eax192 != 1 && (rbp189 = reinterpret_cast<void**>("dir"), eax192 != 2)) {
                rbp189 = reinterpret_cast<void**>("vdir");
            }
            rsi193 = reinterpret_cast<void**>("[");
            do {
                rax196 = fun_4660(rbp189, rsi193, 5, "sha512sum", r8_194, rbp189, rsi193, 5, "sha512sum", r8_195);
                if (!*reinterpret_cast<int32_t*>(&rax196)) 
                    break;
                rsi193 = reinterpret_cast<void**>(0);
            } while (!1);
            r14_197 = reinterpret_cast<void**>(0);
            if (1) {
                r14_197 = rbp189;
            }
            rax198 = fun_4840();
            rdx199 = reinterpret_cast<void**>("GNU coreutils");
            fun_4b20(1, rax198, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_200, 1, rax198, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_201);
            rax202 = fun_4b10(5);
            if (rax202 && (*reinterpret_cast<int32_t*>(&rdx199) = 3, *reinterpret_cast<int32_t*>(&rdx199 + 4) = 0, eax203 = fun_4700(rax202, "en_", 3, rax202, "en_", 3), !!eax203)) {
                rbx204 = stdout;
                *reinterpret_cast<int32_t*>(&rdx199) = 5;
                *reinterpret_cast<int32_t*>(&rdx199 + 4) = 0;
                rax205 = fun_4840();
                fun_4990(rax205, rbx204, 5, "https://www.gnu.org/software/coreutils/", r8_206, rax205, rbx204, 5, "https://www.gnu.org/software/coreutils/", r8_207);
            }
            r13_208 = reinterpret_cast<void**>("test");
            rax211 = fun_4660(rbp189, "[", rdx199, "https://www.gnu.org/software/coreutils/", r8_209, rbp189, "[", rdx199, "https://www.gnu.org/software/coreutils/", r8_210);
            if (*reinterpret_cast<int32_t*>(&rax211)) {
                r13_208 = rbp189;
            }
            rax212 = fun_4840();
            r13_2 = reinterpret_cast<void**>(" invocation");
            fun_4b20(1, rax212, "https://www.gnu.org/software/coreutils/", r13_208, r8_213, 1, rax212, "https://www.gnu.org/software/coreutils/", r13_208, r8_214);
            if (rbp189 != r14_197) {
                r13_2 = reinterpret_cast<void**>(0x1bb0a);
            }
            rax215 = fun_4840();
            rcx4 = r13_2;
            fun_4b20(1, rax215, r14_197, rcx4, r8_216, 1, rax215, r14_197, rcx4, r8_217);
            addr_dc3f_16:
            fun_4bf0();
        }
    } else {
        rax218 = fun_4840();
        rdi219 = stderr;
        rcx4 = r13_2;
        fun_4c10(rdi219, 1, rax218, rcx4, r8_220, r9_221, v222, v223, v224, v225, v226, v227);
        goto addr_dc3f_16;
    }
}

void** fun_4af0(void** rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9);

void** fun_e503(void** rdi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void* rax5;
    void* v6;
    int1_t zf7;
    unsigned char r14b8;
    void** v9;
    void*** rax10;
    void** r15_11;
    void* rax12;
    void** rdi13;
    void** rax14;
    void** r13_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** rcx19;
    void** rcx20;
    uint64_t r8_21;
    int64_t r9_22;
    void** rax23;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0x80;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    rax5 = g28;
    v6 = rax5;
    zf7 = rsi == 0;
    r14b8 = reinterpret_cast<uint1_t>(!zf7);
    if (!zf7 && (rbx4 = rsi + 1, reinterpret_cast<unsigned char>(rsi) >= reinterpret_cast<unsigned char>(0x401))) {
        rbx4 = reinterpret_cast<void**>(0x401);
    }
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_e568_4;
    addr_e650_5:
    rax10 = fun_46f0();
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    *rax10 = reinterpret_cast<void**>(12);
    addr_e5fb_6:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v6) - reinterpret_cast<uint64_t>(g28));
    if (rax12) {
        fun_4870();
    } else {
        return r15_11;
    }
    addr_e5f0_9:
    rdi13 = r15_11;
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    fun_4630(rdi13, rdi13);
    goto addr_e5fb_6;
    while (rax14 = fun_4670(rbx4, rsi, rbx4, rsi), r13_15 = rax14, !!rax14) {
        r15_11 = rax14;
        do {
            rsi = r13_15;
            rax16 = fun_47c0(rbp3, rsi, rbx4);
            if (reinterpret_cast<signed char>(rax16) < reinterpret_cast<signed char>(0)) 
                goto addr_e5f0_9;
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(rax16)) 
                goto addr_e628_14;
            fun_4630(r15_11, r15_11);
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (rbx4 == 0x7fffffffffffffff) 
                    goto addr_e650_5;
                rbx4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_e568_4:
                if (!reinterpret_cast<int1_t>(rbx4 == 0x80)) 
                    break;
            } else {
                rbx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(rbx4));
                if (rbx4 != 0x80) 
                    break;
            }
            r13_15 = v9;
            *reinterpret_cast<int32_t*>(&r15_11) = 0;
            *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        } while (!r14b8);
    }
    goto addr_e650_5;
    addr_e628_14:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_15) + reinterpret_cast<unsigned char>(rax16)) = 0;
    r12_17 = rax16 + 1;
    if (!r15_11) {
        rax18 = fun_4670(r12_17, rsi, r12_17, rsi);
        r15_11 = rax18;
        if (rax18) {
            fun_4a30(rax18, r13_15, r12_17, rcx19);
            goto addr_e5fb_6;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(r12_17)) {
            rax23 = fun_4af0(r15_11, r12_17, rbx4, rcx20, r8_21, r9_22);
            if (rax23) {
                r15_11 = rax23;
            }
            goto addr_e5fb_6;
        }
    }
}

void fun_e693() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_4970(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_e6a3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_4860(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_4700(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_e723_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_e770_6; else 
                    continue;
            } else {
                rax18 = fun_4860(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_e7a0_8;
                if (v16 == -1) 
                    goto addr_e75e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_e723_5;
            } else {
                eax19 = fun_4970(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_e723_5;
            }
            addr_e75e_10:
            v16 = rbx15;
            goto addr_e723_5;
        }
    }
    addr_e785_16:
    return v12;
    addr_e770_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_e785_16;
    addr_e7a0_8:
    v12 = rbx15;
    goto addr_e785_16;
}

int64_t fun_e7b3(void** rdi, void*** rsi, void** rdx, void** rcx, void** r8) {
    void** r12_6;
    void** rdi7;
    void*** rbp8;
    int64_t rbx9;
    int64_t rax10;

    __asm__("cli ");
    r12_6 = rdi;
    rdi7 = *rsi;
    if (!rdi7) {
        addr_e7f8_2:
        return -1;
    } else {
        rbp8 = rsi;
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        do {
            rax10 = fun_4660(rdi7, r12_6, rdx, rcx, r8);
            if (!*reinterpret_cast<int32_t*>(&rax10)) 
                break;
            ++rbx9;
            rdi7 = rbp8[rbx9 * 8];
        } while (rdi7);
        goto addr_e7f8_2;
    }
    return rbx9;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_e813(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_4840();
    } else {
        fun_4840();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_4b70;
}

void fun_e8a3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** v8;
    void** r12_9;
    void** r12_10;
    void** v11;
    void** rbp12;
    void** rbp13;
    void** v14;
    void** rbx15;
    void** r14_16;
    void** v17;
    void** rax18;
    void** r15_19;
    int64_t rbx20;
    uint32_t eax21;
    void** rax22;
    void** rdi23;
    void** v24;
    void** v25;
    void** rax26;
    void** rdi27;
    void** v28;
    void** v29;
    void** rdi30;
    void** rax31;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    v11 = rbp12;
    rbp13 = rsi;
    v14 = rbx15;
    r14_16 = stderr;
    v17 = rdi;
    rax18 = fun_4840();
    fun_4990(rax18, r14_16, 5, rcx, r8);
    r15_19 = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
    if (r15_19) {
        do {
            if (!rbx20 || (eax21 = fun_4970(r13_7, rbp13, r12_10, r13_7, rbp13, r12_10), !!eax21)) {
                r13_7 = rbp13;
                rax22 = quote(r15_19, r15_19);
                rdi23 = stderr;
                fun_4c10(rdi23, 1, "\n  - %s", rax22, r8, r9, v24, v17, v25, v14, v11, v8);
            } else {
                rax26 = quote(r15_19, r15_19);
                rdi27 = stderr;
                fun_4c10(rdi27, 1, ", %s", rax26, r8, r9, v28, v17, v29, v14, v11, v8);
            }
            ++rbx20;
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r12_10));
            r15_19 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v17) + reinterpret_cast<uint64_t>(rbx20 * 8));
        } while (r15_19);
    }
    rdi30 = stderr;
    rax31 = *reinterpret_cast<void***>(rdi30 + 40);
    if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi30 + 48))) {
        goto fun_48d0;
    } else {
        *reinterpret_cast<void***>(rdi30 + 40) = rax31 + 1;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(10);
        return;
    }
}

void argmatch_valid(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_e9d3(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    void** r13_10;
    void** r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int64_t rax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, r14_9, rbp12, r12_11);
        if (rax14 < 0) {
            addr_ea17_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx, r8);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_ea8e_4;
        } else {
            addr_ea8e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            rax17 = fun_4660(rdi15, r14_9, rdx, rcx, r8);
            if (!*reinterpret_cast<int32_t*>(&rax17)) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_ea10_8;
    } else {
        goto addr_ea10_8;
    }
    return rbx16;
    addr_ea10_8:
    rax14 = -1;
    goto addr_ea17_3;
}

struct s146 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_eaa3(void** rdi, struct s146* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_4970(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

struct s147 {
    unsigned char f0;
    unsigned char f1;
};

struct s147* fun_eb03(struct s147* rdi) {
    uint32_t edx2;
    struct s147* rax3;
    struct s147* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s147*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s147*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s147*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_eb63(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_4860(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int64_t fun_eb93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    int64_t rcx5;
    int64_t rax6;
    uint32_t r9d7;
    int64_t r10_8;
    uint32_t r8d9;
    int64_t rax10;

    __asm__("cli ");
    if (rdi == rsi || !rdx) {
        return 0;
    } else {
        rdx4 = rdx - 1;
        *reinterpret_cast<int32_t*>(&rcx5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        do {
            *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(rdi + rcx5);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
            r9d7 = *reinterpret_cast<uint32_t*>(&rax6);
            if (static_cast<uint32_t>(rax6 - 65) <= 25) {
                *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<uint32_t*>(&rax6) + 32;
                r9d7 = r9d7 + 32;
            }
            *reinterpret_cast<uint32_t*>(&r10_8) = *reinterpret_cast<unsigned char*>(rsi + rcx5);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0;
            r8d9 = *reinterpret_cast<uint32_t*>(&r10_8);
            if (static_cast<uint32_t>(r10_8 - 65) <= 25) {
                *reinterpret_cast<uint32_t*>(&r10_8) = *reinterpret_cast<uint32_t*>(&r10_8) + 32;
                r8d9 = r8d9 + 32;
            }
        } while (rcx5 != rdx4 && (*reinterpret_cast<uint32_t*>(&rax6) && (++rcx5, *reinterpret_cast<signed char*>(&r9d7) == *reinterpret_cast<signed char*>(&r8d9))));
        *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rax6) - *reinterpret_cast<uint32_t*>(&r10_8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void** fun_f473(void** rdi, uint32_t esi) {
    void* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    rax3 = g28;
    rax4 = canonicalize_filename_mode_stk(rdi, esi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x428);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
    if (rdx5) {
        fun_4870();
    } else {
        return rax4;
    }
}

int64_t file_name = 0;

void fun_f4c3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_f4d3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

void** fun_4710(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_f4e3() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_46f0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_4840();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_f573_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_4b70();
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_4710(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_f573_5:
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_4b70();
    }
}

void fun_f593(void** rdi, void** rsi, void** rdx) {
    void* rbp4;
    void** rbx5;
    struct s25* rax6;
    void* rax7;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp4) + 4) = 0;
    rbx5 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp4) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax6 = last_component(rdi, rsi, rdx);
    rax7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<unsigned char>(rbx5));
    while (reinterpret_cast<uint64_t>(rax7) > reinterpret_cast<uint64_t>(rbp4) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<uint64_t>(rax7) + 0xffffffffffffffff) == 47) {
        rax7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax7) + 0xffffffffffffffff);
    }
    return;
}

void** fun_f5d3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    void** rbx6;
    struct s25* rax7;
    void** r12_8;
    void* rax9;
    uint32_t ebx10;
    void** rax11;
    void** r8_12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    rbp5 = rdi;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx6) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx);
    r12_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax7) - reinterpret_cast<unsigned char>(rbp5));
    while (reinterpret_cast<unsigned char>(rbx6) < reinterpret_cast<unsigned char>(r12_8)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_8) + 0xffffffffffffffff) != 47) 
            goto addr_f650_4;
        --r12_8;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_8) ^ 1);
    ebx10 = *reinterpret_cast<uint32_t*>(&rax9) & 1;
    rax11 = fun_4670(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rax9) + 1, rsi);
    if (!rax11) {
        addr_f67d_7:
        *reinterpret_cast<int32_t*>(&r8_12) = 0;
        *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    } else {
        rax13 = fun_4a30(rax11, rbp5, r12_8, rcx);
        r8_12 = rax13;
        if (!*reinterpret_cast<signed char*>(&ebx10)) {
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_f63e_10;
        } else {
            *reinterpret_cast<void***>(rax13) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_f63e_10;
        }
    }
    addr_f643_12:
    return r8_12;
    addr_f63e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_12) + reinterpret_cast<unsigned char>(r12_8)) = 0;
    goto addr_f643_12;
    addr_f650_4:
    rax14 = fun_4670(r12_8 + 1, rsi);
    if (!rax14) 
        goto addr_f67d_7;
    rax15 = fun_4a30(rax14, rbp5, r12_8, rcx);
    r8_12 = rax15;
    goto addr_f63e_10;
}

int64_t fun_f693() {
    __asm__("cli ");
    return 0;
}

void fun_f6a3(void** rdi, void** rsi, struct s1* rdx, int64_t rcx) {
    void** rax5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        rax5 = xmalloc(24, rsi);
        rax6 = xstrdup(rsi, rsi);
        *reinterpret_cast<void***>(rax5) = rax6;
        *reinterpret_cast<void***>(rax5 + 8) = rdx->f8;
        *reinterpret_cast<void***>(rax5 + 16) = rdx->f0;
        rax7 = hash_insert(rdi, rax5, rdx, rcx);
        if (!rax7) {
            xalloc_die();
        } else {
            if (rax5 == rax7) {
                return;
            }
        }
    }
}

int64_t hash_lookup();

int64_t fun_f733(int64_t rdi, int64_t rsi, int64_t rdx) {
    void* rax4;
    int64_t rax5;
    void* rdx6;

    __asm__("cli ");
    rax4 = g28;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (rdi) {
        rax5 = hash_lookup();
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(!!rax5);
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx6) {
        fun_4870();
    } else {
        return rax5;
    }
}

struct s148 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
    signed char f8;
    signed char f9;
    int16_t fa;
};

void fun_f793(uint32_t edi, struct s148* rsi) {
    uint32_t eax3;
    int32_t ecx4;
    uint32_t esi5;
    uint32_t ecx6;
    uint32_t ecx7;
    uint32_t ecx8;
    uint32_t ecx9;
    uint32_t ecx10;
    uint32_t ecx11;
    uint32_t ecx12;
    uint32_t ecx13;
    uint32_t ecx14;
    uint32_t ecx15;
    uint32_t ecx16;
    uint32_t ecx17;
    uint32_t ecx18;
    uint32_t ecx19;
    uint32_t ecx20;
    uint32_t ecx21;
    uint32_t ecx22;
    uint32_t ecx23;
    uint32_t ecx24;
    uint32_t eax25;
    uint32_t eax26;

    __asm__("cli ");
    eax3 = edi;
    ecx4 = 45;
    esi5 = edi & 0xf000;
    if (esi5 != 0x8000 && ((ecx4 = 100, esi5 != 0x4000) && ((ecx4 = 98, esi5 != 0x6000) && ((ecx4 = 99, esi5 != 0x2000) && ((ecx4 = 0x6c, esi5 != 0xa000) && ((ecx4 = 0x70, esi5 != 0x1000) && (ecx4 = 0x73, esi5 != 0xc000))))))) {
        ecx4 = 63;
    }
    rsi->f0 = *reinterpret_cast<signed char*>(&ecx4);
    ecx6 = eax3 & 0x100;
    ecx7 = (ecx6 - (ecx6 + reinterpret_cast<uint1_t>(ecx6 < ecx6 + reinterpret_cast<uint1_t>(ecx6 < 1))) & 0xffffffbb) + 0x72;
    rsi->f1 = *reinterpret_cast<signed char*>(&ecx7);
    ecx8 = eax3 & 0x80;
    ecx9 = (ecx8 - (ecx8 + reinterpret_cast<uint1_t>(ecx8 < ecx8 + reinterpret_cast<uint1_t>(ecx8 < 1))) & 0xffffffb6) + 0x77;
    rsi->f2 = *reinterpret_cast<signed char*>(&ecx9);
    ecx10 = eax3 & 64;
    ecx11 = ecx10 - (ecx10 + reinterpret_cast<uint1_t>(ecx10 < ecx10 + reinterpret_cast<uint1_t>(ecx10 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 8)) {
        ecx12 = (ecx11 & 0xffffffb5) + 0x78;
    } else {
        ecx12 = (ecx11 & 0xffffffe0) + 0x73;
    }
    rsi->f3 = *reinterpret_cast<signed char*>(&ecx12);
    ecx13 = eax3 & 32;
    ecx14 = (ecx13 - (ecx13 + reinterpret_cast<uint1_t>(ecx13 < ecx13 + reinterpret_cast<uint1_t>(ecx13 < 1))) & 0xffffffbb) + 0x72;
    rsi->f4 = *reinterpret_cast<signed char*>(&ecx14);
    ecx15 = eax3 & 16;
    ecx16 = (ecx15 - (ecx15 + reinterpret_cast<uint1_t>(ecx15 < ecx15 + reinterpret_cast<uint1_t>(ecx15 < 1))) & 0xffffffb6) + 0x77;
    rsi->f5 = *reinterpret_cast<signed char*>(&ecx16);
    ecx17 = eax3 & 8;
    ecx18 = ecx17 - (ecx17 + reinterpret_cast<uint1_t>(ecx17 < ecx17 + reinterpret_cast<uint1_t>(ecx17 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 4)) {
        ecx19 = (ecx18 & 0xffffffb5) + 0x78;
    } else {
        ecx19 = (ecx18 & 0xffffffe0) + 0x73;
    }
    rsi->f6 = *reinterpret_cast<signed char*>(&ecx19);
    ecx20 = eax3 & 4;
    ecx21 = (ecx20 - (ecx20 + reinterpret_cast<uint1_t>(ecx20 < ecx20 + reinterpret_cast<uint1_t>(ecx20 < 1))) & 0xffffffbb) + 0x72;
    rsi->f7 = *reinterpret_cast<signed char*>(&ecx21);
    ecx22 = eax3 & 2;
    ecx23 = (ecx22 - (ecx22 + reinterpret_cast<uint1_t>(ecx22 < ecx22 + reinterpret_cast<uint1_t>(ecx22 < 1))) & 0xffffffb6) + 0x77;
    rsi->f8 = *reinterpret_cast<signed char*>(&ecx23);
    ecx24 = eax3 & 1;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 2)) {
        eax25 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffb5) + 0x78;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax25);
        rsi->fa = 32;
        return;
    } else {
        eax26 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffe0) + 0x74;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax26);
        rsi->fa = 32;
        return;
    }
}

void fun_f913(int64_t rdi) {
    __asm__("cli ");
    goto 0xf790;
}

int64_t mfile_name_concat();

void fun_f923() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* base_len(struct s25* rdi);

void** fun_f943(void** rdi, void** rsi, void** rdx) {
    struct s25* rax4;
    void* rax5;
    void** r14_6;
    void** rax7;
    void* rbx8;
    uint1_t zf9;
    int32_t eax10;
    unsigned char v11;
    int1_t zf12;
    int32_t eax13;
    void** rax14;
    void** rax15;
    uint32_t ecx16;
    void** rdi17;
    void** rax18;

    __asm__("cli ");
    rax4 = last_component(rdi, rsi, rdx);
    rax5 = base_len(rax4);
    r14_6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax5));
    rax7 = fun_4860(rsi);
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rbx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        zf9 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi) == 47);
        eax10 = 46;
        if (!zf9) {
            eax10 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx8) = zf9;
        v11 = *reinterpret_cast<unsigned char*>(&eax10);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_6) + 0xffffffffffffffff) == 47) {
            v11 = 0;
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
            zf12 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47);
            eax13 = 47;
            if (zf12) {
                eax13 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx8) = reinterpret_cast<uint1_t>(!zf12);
            v11 = *reinterpret_cast<unsigned char*>(&eax13);
        }
    }
    rax14 = fun_4670(reinterpret_cast<unsigned char>(r14_6) + reinterpret_cast<unsigned char>(rax7) + 1 + reinterpret_cast<uint64_t>(rbx8), rsi);
    if (rax14) {
        rax15 = fun_4b50(rax14, rdi, r14_6);
        ecx16 = v11;
        rdi17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax15) + reinterpret_cast<uint64_t>(rbx8));
        *reinterpret_cast<void***>(rax15) = *reinterpret_cast<void***>(&ecx16);
        if (rdx) {
            *reinterpret_cast<void***>(rdx) = rdi17;
        }
        rax18 = fun_4b50(rdi17, rsi, rax7);
        *reinterpret_cast<void***>(rax18) = reinterpret_cast<void**>(0);
    }
    return rax14;
}

int64_t fun_fdb3(struct s14* rdi, uint64_t rsi, struct s14* rdx, uint64_t rcx) {
    uint64_t* rsp5;
    unsigned char al6;
    unsigned char cl7;
    int64_t rcx8;
    int64_t rax9;
    int64_t rax10;
    uint32_t ecx11;
    unsigned char al12;
    uint32_t eax13;
    uint64_t rax14;
    uint64_t rax15;
    struct s14* rdx16;
    uint64_t rcx17;

    __asm__("cli ");
    rsp5 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16);
    if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
        al6 = reinterpret_cast<uint1_t>(rdi->f0 == 0);
    } else {
        al6 = reinterpret_cast<uint1_t>(rsi == 0);
    }
    cl7 = reinterpret_cast<uint1_t>(rcx == 0);
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        cl7 = reinterpret_cast<uint1_t>(rdx->f0 == 0);
    }
    if (al6) {
        addr_fe80_7:
        *reinterpret_cast<uint32_t*>(&rcx8) = cl7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax9) = static_cast<int32_t>(rcx8 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    } else {
        *reinterpret_cast<uint32_t*>(&rax10) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (cl7) 
            goto addr_fe60_9;
        ecx11 = rdx->f0;
        if (rdi->f0 != 46) 
            goto addr_fe0f_11;
    }
    if (*reinterpret_cast<signed char*>(&ecx11) != 46) 
        goto addr_ff01_13;
    al12 = reinterpret_cast<uint1_t>(rsi == 1);
    if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
        al12 = reinterpret_cast<uint1_t>(rdi->f1 == 0);
    }
    cl7 = reinterpret_cast<uint1_t>(rcx == 1);
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        cl7 = reinterpret_cast<uint1_t>(rdx->f1 == 0);
    }
    if (al12) 
        goto addr_fe80_7;
    *reinterpret_cast<uint32_t*>(&rax10) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    if (cl7) 
        goto addr_fe60_9;
    if (rdi->f1 == 46) {
        if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
            if (!rdi->f2) {
                addr_ff1b_23:
                if (rdx->f1 != 46) 
                    goto addr_ff01_13;
            } else {
                goto addr_fee0_25;
            }
        } else {
            if (rsi != 2) 
                goto addr_fee0_25; else 
                goto addr_ff1b_23;
        }
    } else {
        addr_fee0_25:
        if (rdx->f1 != 46) 
            goto addr_fe14_27; else 
            goto addr_feec_28;
    }
    eax13 = 1;
    addr_feee_30:
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        if (rdx->f2) {
            addr_fef9_32:
            if (!eax13) {
                addr_fe14_27:
                rax14 = file_prefixlen(rdi, rsp5 + 1);
                rax15 = file_prefixlen(rdx, rsp5 - 1 + 1);
                rdx16 = rdx;
                rcx17 = rax15;
                if (rsi != rax14 || rcx != rax15) {
                    rax10 = verrevcmp(rdi, rax14, rdx16, rcx17);
                    if (*reinterpret_cast<uint32_t*>(&rax10)) {
                        addr_fe60_9:
                        return rax10;
                    } else {
                        rcx17 = rcx;
                        rdx16 = rdx;
                    }
                }
            } else {
                addr_ff01_13:
                *reinterpret_cast<uint32_t*>(&rax10) = 0xffffffff;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
                goto addr_fe60_9;
            }
        } else {
            addr_ff38_36:
            *reinterpret_cast<uint32_t*>(&rax10) = eax13 ^ 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            goto addr_fe60_9;
        }
        rax10 = verrevcmp(rdi, rsi, rdx16, rcx17);
        goto addr_fe60_9;
    } else {
        if (rcx == 2) 
            goto addr_ff38_36; else 
            goto addr_fef9_32;
    }
    addr_feec_28:
    eax13 = 0;
    goto addr_feee_30;
    addr_fe0f_11:
    if (*reinterpret_cast<signed char*>(&ecx11) == 46) 
        goto addr_fe60_9; else 
        goto addr_fe14_27;
}

void fun_ff53() {
    __asm__("cli ");
}

void fun_47d0();

void fun_ff73(int64_t rdi) {
    __asm__("cli ");
    goto fun_47d0;
}

int64_t fun_ff83() {
    void* rax1;
    void* rcx2;
    int64_t v3;

    __asm__("cli ");
    rax1 = g28;
    fun_47d0();
    rcx2 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rcx2) {
        fun_4870();
    } else {
        return v3;
    }
}

int32_t setlocale_null_r();

int64_t fun_ffd3() {
    void* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_4870();
    } else {
        return rax3;
    }
}

uint64_t fun_10053(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_10073(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s149 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_104d3(struct s149* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s150 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_104e3(struct s150* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s151 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_104f3(struct s151* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s154 {
    signed char[8] pad8;
    struct s154* f8;
};

struct s153 {
    int64_t f0;
    struct s154* f8;
};

struct s152 {
    struct s153* f0;
    struct s153* f8;
};

uint64_t fun_10503(struct s152* rdi) {
    struct s153* rcx2;
    struct s153* rsi3;
    uint64_t r8_4;
    struct s154* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s157 {
    signed char[8] pad8;
    struct s157* f8;
};

struct s156 {
    int64_t f0;
    struct s157* f8;
};

struct s155 {
    struct s156* f0;
    struct s156* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_10563(struct s155* rdi) {
    struct s156* rcx2;
    struct s156* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s157* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s160 {
    signed char[8] pad8;
    struct s160* f8;
};

struct s159 {
    int64_t f0;
    struct s160* f8;
};

struct s158 {
    struct s159* f0;
    struct s159* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_105d3(struct s158* rdi, void** rsi) {
    void** v3;
    void** v4;
    void** r13_5;
    void** v6;
    void** r12_7;
    uint64_t r12_8;
    void** v9;
    void** rbp10;
    void** rbp11;
    void** v12;
    void** rbx13;
    struct s159* rcx14;
    struct s159* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s160* rax19;
    uint64_t rdx20;
    void** r9_21;
    void** v22;
    void** r9_23;
    void** v24;
    void** r9_25;
    void** v26;

    v3 = reinterpret_cast<void**>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_4c10(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_4c10(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0xdc3c]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_1068a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_10709_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_4c10(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_4c10;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0xdcbb]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_10709_14; else 
            goto addr_1068a_13;
    }
}

struct s161 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s162 {
    int64_t f0;
    struct s162* f8;
};

int64_t fun_10733(struct s161* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s161* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s162* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x4ca5;
    rbx7 = reinterpret_cast<struct s162*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_10798_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_1078b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_1078b_7;
    }
    addr_1079b_10:
    return r12_3;
    addr_10798_5:
    r12_3 = rbx7->f0;
    goto addr_1079b_10;
    addr_1078b_7:
    return 0;
}

struct s163 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_107b3(struct s163* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x4caa;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_107ef_7;
    return *rax2;
    addr_107ef_7:
    goto 0x4caa;
}

struct s165 {
    int64_t f0;
    struct s165* f8;
};

struct s164 {
    int64_t* f0;
    struct s165* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_10803(struct s164* rdi, int64_t rsi) {
    struct s164* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s165* rax7;
    struct s165* rdx8;
    struct s165* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x4cb0;
    rax7 = reinterpret_cast<struct s165*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_1084e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_1084e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_1086c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_1086c_10:
    return r8_10;
}

struct s167 {
    int64_t f0;
    struct s167* f8;
};

struct s166 {
    struct s167* f0;
    struct s167* f8;
};

void fun_10893(struct s166* rdi, int64_t rsi, uint64_t rdx) {
    struct s167* r9_4;
    uint64_t rax5;
    struct s167* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_108d2_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_108d2_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s169 {
    int64_t f0;
    struct s169* f8;
};

struct s168 {
    struct s169* f0;
    struct s169* f8;
};

int64_t fun_108e3(struct s168* rdi, int64_t rsi, int64_t rdx) {
    struct s169* r14_4;
    int64_t r12_5;
    struct s168* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s169* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_1090f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_10951_10;
            }
            addr_1090f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_10919_11:
    return r12_5;
    addr_10951_10:
    goto addr_10919_11;
}

uint64_t fun_10963(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s170 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_109a3(struct s170* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_49b0(void** rdi, int64_t rsi);

void** fun_109d3(uint64_t rdi, void** rsi, void** rdx, int32_t rcx, int64_t r8) {
    void** r15_6;
    void** rbp7;
    int32_t rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x10050);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<int32_t>(0x10070);
    }
    rax9 = fun_4670(80, rsi);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x1e310);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_49b0(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_4630(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<int32_t*>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int64_t*>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s173 {
    int64_t f0;
    struct s173* f8;
};

struct s172 {
    int64_t f0;
    struct s173* f8;
};

struct s171 {
    struct s172* f0;
    struct s172* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s173* f48;
};

void fun_10ad3(struct s171* rdi) {
    struct s171* rbp2;
    struct s172* r12_3;
    struct s173* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s173* rax7;
    struct s173* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s174 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_10b83(struct s174* rdi) {
    struct s174* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_10bf3_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_4630(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_10bf3_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_4630(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_4630(rdi12);
    goto fun_4630;
}

int64_t fun_10c73(struct s15* rdi, uint64_t rsi) {
    struct s16* r12_3;
    void* rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s15* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_10db0_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_49b0(rax6, 16);
        if (!rax8) {
            addr_10db0_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_4630(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x4cb5;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x4cb5;
                fun_4630(rax8, rax8);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rax15) {
        fun_4870();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s175 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_10e03(void** rdi, void** rsi, void*** rdx) {
    void* rax4;
    void*** r12_5;
    void** rbp6;
    void** rax7;
    void** rax8;
    uint1_t below_or_equal9;
    uint64_t rax10;
    int1_t cf11;
    signed char al12;
    void** rax13;
    struct s175* v14;
    int32_t r8d15;
    void** rax16;
    void* rax17;
    int64_t rax18;
    void** rdx19;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x4cba;
    r12_5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rbp6 = rsi;
    rax7 = hash_find_entry(rdi, rsi, r12_5, 0);
    if (!rax7) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 24)) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax8 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax8 == 0)));
            if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) 
                goto addr_10f1e_5; else 
                goto addr_10e8f_6;
        }
        __asm__("pxor xmm5, xmm5");
        rax8 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax8 == 0)));
        if (reinterpret_cast<signed char>(rax8) >= reinterpret_cast<signed char>(0)) {
            addr_10e8f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_10f1e_5:
            *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rax8) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            below_or_equal9 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax8) >> 1) | rax10) == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal9 && (check_tuning(rdi), !below_or_equal9)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0xd3c1]");
            if (!cf11) 
                goto addr_10f75_12;
            __asm__("comiss xmm4, [rip+0xd371]");
            if (!cf11) {
                __asm__("subss xmm4, [rip+0xd330]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al12 = hash_rehash(rdi);
            if (!al12) 
                goto addr_10f75_12;
            rsi = rbp6;
            rax13 = hash_find_entry(rdi, rsi, r12_5, 0);
            if (rax13) {
                goto 0x4cba;
            }
        }
        if (!v14->f0) {
            v14->f0 = rbp6;
            r8d15 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax16 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax16) {
                rax16 = fun_4670(16, rsi, 16, rsi);
                if (!rax16) {
                    addr_10f75_12:
                    r8d15 = -1;
                } else {
                    goto addr_10ed2_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax16 + 8);
                goto addr_10ed2_24;
            }
        }
    } else {
        r8d15 = 0;
        if (rdx) {
            *rdx = rax7;
        }
    }
    addr_10e4e_28:
    rax17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rax17) {
        fun_4870();
    } else {
        *reinterpret_cast<int32_t*>(&rax18) = r8d15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        return rax18;
    }
    addr_10ed2_24:
    rdx19 = v14->f8;
    *reinterpret_cast<void***>(rax16) = rbp6;
    r8d15 = 1;
    *reinterpret_cast<void***>(rax16 + 8) = rdx19;
    v14->f8 = rax16;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_10e4e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_11023() {
    void* rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rdx6) {
        fun_4870();
    } else {
        return rax3;
    }
}

void** fun_11083(void** rdi, void** rsi) {
    void** rbx3;
    void* rax4;
    void* v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_11110_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_111c6_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0xd1de]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0xd148]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_4630(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_111c6_5; else 
                goto addr_11110_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v5) - reinterpret_cast<uint64_t>(g28));
    if (rax16) {
        fun_4870();
    } else {
        return r12_7;
    }
}

void fun_11213() {
    __asm__("cli ");
    goto hash_remove;
}

struct s176 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_11223(struct s176* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s177 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s178 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_11253(struct s177* rdi, struct s178* rsi) {
    void** rdx3;
    void** rcx4;
    void** rsi5;
    void** rdi6;
    void** r8_7;
    int64_t rax8;

    __asm__("cli ");
    rdx3 = rsi->f8;
    if (rdi->f8 != rdx3 || (rcx4 = rsi->f10, rdi->f10 != rcx4)) {
        return 0;
    } else {
        rsi5 = rsi->f0;
        rdi6 = rdi->f0;
        rax8 = fun_4660(rdi6, rsi5, rdx3, rcx4, r8_7);
        *reinterpret_cast<unsigned char*>(&rax8) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0);
        return rax8;
    }
}

void fun_11293(void*** rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = *rdi;
    fun_4630(rdi2);
    goto fun_4630;
}

struct s179 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char* f10;
};

struct s179* fun_4790();

void fun_4a00(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_112b3(uint64_t rdi, void** rsi, uint32_t edx, uint64_t rcx, uint64_t r8) {
    void** v6;
    uint64_t v7;
    uint32_t v8;
    void* rax9;
    void* v10;
    uint32_t edx11;
    uint32_t v12;
    uint32_t eax13;
    uint32_t v14;
    uint32_t v15;
    struct s179* rax16;
    void** r15_17;
    void** rax18;
    void** r14_19;
    void** rbp20;
    int1_t cf21;
    unsigned char* v22;
    void** rax23;
    void*** rsp24;
    void** v25;
    uint64_t r8_26;
    uint64_t rdx27;
    uint64_t rcx28;
    uint64_t rax29;
    uint64_t rax30;
    uint64_t rdx31;
    uint64_t rax32;
    uint64_t rdx33;
    int64_t rdi34;
    uint32_t esi35;
    uint32_t r10d36;
    uint64_t v37;
    int64_t rbx38;
    uint64_t rax39;
    int1_t zf40;
    void** rax41;
    void*** rsp42;
    void** rdx43;
    void** r12_44;
    void** rbp45;
    void** r8_46;
    void** r13_47;
    void** rax48;
    void* rsp49;
    void** r12_50;
    void** rax51;
    void** v52;
    void* rsp53;
    uint32_t v54;
    void** rbp55;
    void** rbx56;
    unsigned char* r12_57;
    void** rax58;
    void** rsi59;
    uint64_t rcx60;
    uint64_t rdx61;
    uint64_t rax62;
    uint32_t eax63;
    void** rdx64;
    uint32_t ecx65;
    void* rax66;
    void* rax67;
    uint32_t tmp32_68;
    int1_t cf69;
    void** rbp70;
    void** rax71;
    uint64_t rax72;
    int1_t zf73;
    void** rax74;
    int1_t cf75;
    int1_t below_or_equal76;
    uint64_t rax77;
    void** rax78;
    uint64_t rax79;
    int1_t zf80;
    uint64_t r8_81;
    uint64_t r11_82;
    uint64_t rdx83;
    int64_t rcx84;
    uint64_t r9_85;
    int64_t rax86;
    int32_t eax87;
    int64_t rdx88;
    int64_t rax89;
    uint32_t edx90;
    uint32_t esi91;
    uint64_t rax92;
    int64_t rax93;
    uint32_t esi94;
    int64_t rdx95;
    uint32_t eax96;
    uint64_t rax97;
    uint64_t rdx98;
    uint64_t rdi99;
    uint64_t rax100;
    int32_t eax101;
    uint64_t rax102;
    void* rcx103;
    void* rax104;
    void* rax105;
    uint32_t eax106;
    uint32_t eax107;
    uint32_t edx108;
    void* rsi109;
    uint32_t edx110;
    uint32_t edx111;
    void* rax112;
    void* rsi113;
    void* rax114;
    void* rax115;
    void* rdi116;
    uint32_t eax117;
    uint32_t r9d118;
    uint32_t eax119;
    void* rdx120;
    uint32_t edx121;
    uint32_t edx122;

    __asm__("cli ");
    v6 = rsi;
    v7 = r8;
    v8 = edx;
    rax9 = g28;
    v10 = rax9;
    edx11 = edx & 32;
    v12 = edx11;
    eax13 = edx & 3;
    v14 = eax13;
    v15 = (eax13 - (eax13 + reinterpret_cast<uint1_t>(eax13 < eax13 + reinterpret_cast<uint1_t>(edx11 < 1))) & 0xffffffe8) + reinterpret_cast<int32_t>("v");
    rax16 = fun_4790();
    r15_17 = rax16->f0;
    rax18 = fun_4860(r15_17);
    r14_19 = rax16->f8;
    rbp20 = rax18;
    cf21 = reinterpret_cast<unsigned char>(rax18 - 1) < reinterpret_cast<unsigned char>(16);
    v22 = rax16->f10;
    if (!cf21) {
        rbp20 = reinterpret_cast<void**>(1);
    }
    if (!cf21) {
        r15_17 = reinterpret_cast<void**>(".");
    }
    rax23 = fun_4860(r14_19);
    rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(16)) {
        r14_19 = reinterpret_cast<void**>(0x1bb0a);
    }
    v25 = v6 + 0x287;
    if (r8 > rcx) {
        if (!rcx || (r8_26 = v7 / rcx, !!(v7 % rcx))) {
            addr_11394_9:
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
                __asm__("fadd dword [rip+0xcc76]");
            }
        } else {
            rdx27 = rdi % r8_26;
            rcx28 = rdi / r8_26;
            rax29 = rdx27 + rdx27 * 4;
            rax30 = rax29 + rax29;
            rdx31 = rax30 % r8_26;
            rax32 = rax30 / r8_26;
            rdx33 = rdx31 + rdx31;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (r8_26 <= rdx33) {
                esi35 = 2;
                if (r8_26 < rdx33) {
                    esi35 = 3;
                }
            } else {
                esi35 = 0;
                *reinterpret_cast<unsigned char*>(&esi35) = reinterpret_cast<uint1_t>(!!rdx33);
            }
            r10d36 = v8 & 16;
            if (!r10d36) 
                goto addr_1193b_17; else 
                goto addr_1152c_18;
        }
    } else {
        if (rcx % r8) 
            goto addr_11394_9;
        rcx28 = rcx / r8 * rdi;
        if (__intrinsic()) 
            goto addr_11394_9;
        esi35 = 0;
        *reinterpret_cast<uint32_t*>(&rdi34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        r10d36 = v8 & 16;
        if (r10d36) 
            goto addr_1152c_18; else 
            goto addr_1193b_17;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(v7) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xcc96]");
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) >= reinterpret_cast<int64_t>(0)) {
            addr_113d1_24:
            __asm__("fmulp st1, st0");
            if (!(*reinterpret_cast<unsigned char*>(&v8) & 16)) {
                addr_116c8_25:
                if (v14 == 1) 
                    goto addr_1175a_26;
                __asm__("fld tword [rip+0xccc7]");
                __asm__("fcomip st0, st1");
                if (v14 <= 1) 
                    goto addr_1175a_26;
            } else {
                addr_113de_28:
                __asm__("fild dword [rsp+0x34]");
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                __asm__("fld st0");
                goto addr_113f4_29;
            }
        } else {
            goto addr_11680_31;
        }
    } else {
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) 
            goto addr_11680_31; else 
            goto addr_113d1_24;
    }
    __asm__("fld dword [rip+0xcc55]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax39) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xcc0d]");
    }
    zf40 = v14 == 0;
    if (zf40) 
        goto addr_11731_39;
    __asm__("fstp st1");
    goto addr_1175a_26;
    addr_11731_39:
    __asm__("fxch st0, st1");
    __asm__("fucomip st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf40) {
            addr_1175a_26:
            __asm__("fstp tword [rsp]");
            fun_4c80(v6, 1, -1, "%.0Lf");
            *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
            rax41 = fun_4860(v6, v6);
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            rdx43 = rax41;
            r12_44 = rax41;
            goto addr_11798_43;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax39 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xc6b7]");
        goto addr_1175a_26;
    } else {
        goto addr_1175a_26;
    }
    addr_11798_43:
    rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdx43));
    fun_4b60(rbp45, v6);
    rsp24 = rsp42 - 8 + 8;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) + reinterpret_cast<unsigned char>(r12_44));
    while (1) {
        if (*reinterpret_cast<unsigned char*>(&v8) & 4) {
            addr_115c0_49:
            r13_47 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax48 = fun_4860(r14_19, r14_19);
            rsp49 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            r12_50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<unsigned char>(rbp45));
            r15_17 = rax48;
            rax51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 80);
            v52 = rax51;
            fun_4a00(rax51, rbp45, r12_50, 41);
            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            v54 = *reinterpret_cast<uint32_t*>(&rbx38);
            rbp55 = r8_46;
            rbx56 = r12_50;
            r12_57 = v22;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rax58) = *r12_57;
                *reinterpret_cast<int32_t*>(&rax58 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&rax58)) {
                    if (reinterpret_cast<unsigned char>(r13_47) > reinterpret_cast<unsigned char>(rbx56)) {
                        r13_47 = rbx56;
                    }
                    rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(r13_47));
                    rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rax58) > 0x7e) {
                        r13_47 = rbx56;
                        rsi59 = v52;
                        *reinterpret_cast<int32_t*>(&rbx56) = 0;
                        *reinterpret_cast<int32_t*>(&rbx56 + 4) = 0;
                    } else {
                        if (reinterpret_cast<unsigned char>(rax58) > reinterpret_cast<unsigned char>(rbx56)) {
                            rax58 = rbx56;
                        }
                        rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(rax58));
                        r13_47 = rax58;
                        rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                    }
                    ++r12_57;
                }
                rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp55) - reinterpret_cast<unsigned char>(r13_47));
                fun_4a30(rbp45, rsi59, r13_47, 41);
                rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
                if (!rbx56) 
                    break;
                rbp55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) - reinterpret_cast<unsigned char>(r15_17));
                fun_4a30(rbp55, r14_19, r15_17, 41);
                rsp53 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            }
            *reinterpret_cast<uint32_t*>(&rbx38) = v54;
        }
        addr_117bc_63:
        if (!(*reinterpret_cast<unsigned char*>(&v8) & 0x80)) 
            goto addr_117df_64;
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 0xffffffff) {
            rcx60 = v7;
            if (rcx60 <= 1) {
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                goto addr_117cc_68;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx61) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx61) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx38) = 1;
                *reinterpret_cast<int32_t*>(&rax62) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0;
                do {
                    rax62 = rax62 * rdx61;
                    if (rcx60 <= rax62) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
                } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
                eax63 = v8 & 0x100;
                if (!(v8 & 64)) 
                    goto addr_119a8_73;
            }
        } else {
            addr_117cc_68:
            eax63 = v8 & 0x100;
            if (eax63 | *reinterpret_cast<uint32_t*>(&rbx38)) {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 64)) {
                    addr_119a0_75:
                    if (!*reinterpret_cast<uint32_t*>(&rbx38)) {
                        rdx64 = v25;
                        if (eax63) {
                            addr_119ea_77:
                            *reinterpret_cast<void***>(rdx64) = reinterpret_cast<void**>(66);
                            v25 = rdx64 + 1;
                            goto addr_117df_64;
                        } else {
                            goto addr_117df_64;
                        }
                    } else {
                        addr_119a8_73:
                        rdx64 = v25 + 1;
                        if (v12 || *reinterpret_cast<uint32_t*>(&rbx38) != 1) {
                            rbx38 = *reinterpret_cast<int32_t*>(&rbx38);
                            ecx65 = *reinterpret_cast<unsigned char*>(0x1e388 + rbx38);
                            *reinterpret_cast<void***>(v25) = *reinterpret_cast<void***>(&ecx65);
                            if (eax63) {
                                *reinterpret_cast<uint32_t*>(&r8_46) = v12;
                                *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&r8_46)) {
                                    *reinterpret_cast<void***>(v25 + 1) = reinterpret_cast<void**>(0x69);
                                    rdx64 = v25 + 2;
                                    goto addr_119ea_77;
                                }
                            }
                        } else {
                            *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0x6b);
                            if (eax63) 
                                goto addr_119ea_77; else 
                                goto addr_11c9b_83;
                        }
                    }
                }
            } else {
                addr_117df_64:
                *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0);
                rax66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
                if (rax66) 
                    goto addr_11e7b_85; else 
                    break;
            }
        }
        *reinterpret_cast<signed char*>(v6 + 0x287) = 32;
        v25 = v6 + 0x288;
        goto addr_119a0_75;
        addr_11c9b_83:
        v25 = rdx64;
        goto addr_117df_64;
        addr_11e7b_85:
        rax67 = fun_4870();
        rsp24 = rsp24 - 8 + 8;
        addr_11e80_87:
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 4);
        addr_11c69_88:
        *reinterpret_cast<void***>(r8_46 + 0xffffffffffffffff) = reinterpret_cast<void**>(49);
        rbp45 = r8_46 + 0xffffffffffffffff;
    }
    return rbp45;
    addr_113f4_29:
    while (tmp32_68 = *reinterpret_cast<uint32_t*>(&rbx38) + 1, cf69 = tmp32_68 < *reinterpret_cast<uint32_t*>(&rbx38), *reinterpret_cast<uint32_t*>(&rbx38) = tmp32_68, !cf69) {
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) 
            goto addr_11406_91;
        __asm__("fstp st1");
        __asm__("fxch st0, st2");
    }
    __asm__("fstp st2");
    __asm__("fstp st2");
    addr_11414_94:
    r15_17 = rbp20 + 1;
    __asm__("fdivrp st1, st0");
    rbp70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp20 + 2) + reinterpret_cast<uint1_t>(v12 < 1));
    if (v14 == 1) {
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_4c80(v6, 1, -1, "%.1Lf");
        rax71 = fun_4860(v6, v6);
        rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        __asm__("fld tword [rsp+0x20]");
        rdx43 = rax71;
        if (reinterpret_cast<unsigned char>(rax71) > reinterpret_cast<unsigned char>(rbp70)) {
            __asm__("fld dword [rip+0xc945]");
            __asm__("fmul st1, st0");
            goto addr_11a51_97;
        }
    }
    __asm__("fld tword [rip+0xcf6c]");
    __asm__("fcomip st0, st1");
    if (v14 <= 1) {
        __asm__("fld st0");
        goto addr_118b0_100;
    }
    __asm__("fld dword [rip+0xcef6]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
    }
    v37 = rax72;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax72) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xceae]");
    }
    zf73 = v14 == 0;
    if (zf73) 
        goto addr_11492_107;
    __asm__("fxch st0, st1");
    goto addr_118b0_100;
    addr_11492_107:
    __asm__("fxch st0, st1");
    __asm__("fucomi st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st1");
    } else {
        if (zf73) {
            addr_118b0_100:
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fstp tword [rsp]");
            fun_4c80(v6, 1, -1, "%.1Lf");
            rax74 = fun_4860(v6, v6);
            rdx43 = rax74;
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            __asm__("fld tword [rsp+0x20]");
            cf75 = reinterpret_cast<unsigned char>(rdx43) < reinterpret_cast<unsigned char>(rbp70);
            below_or_equal76 = reinterpret_cast<unsigned char>(rdx43) <= reinterpret_cast<unsigned char>(rbp70);
            if (!below_or_equal76) {
                __asm__("fld dword [rip+0xc8de]");
                __asm__("fmul st1, st0");
                goto addr_11ab8_112;
            } else {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 8)) {
                    __asm__("fstp st0");
                    goto addr_1191a_115;
                } else {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(rdx43) + 0xffffffffffffffff) == 48) {
                        __asm__("fld dword [rip+0xc7be]");
                        cf75 = v14 < 1;
                        below_or_equal76 = v14 <= 1;
                        __asm__("fmul st1, st0");
                        if (v14 == 1) {
                            goto addr_11a51_97;
                        }
                    } else {
                        __asm__("fstp st0");
                        goto addr_1191a_115;
                    }
                }
            }
        } else {
            __asm__("fstp st1");
        }
    }
    rax77 = rax72 + 1;
    v37 = rax77;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax77) >= reinterpret_cast<int64_t>(0)) {
        __asm__("fxch st0, st1");
        goto addr_118b0_100;
    } else {
        __asm__("fadd dword [rip+0xce73]");
        __asm__("fxch st0, st1");
        goto addr_118b0_100;
    }
    addr_11ab8_112:
    __asm__("fld tword [rip+0xc8e2]");
    __asm__("fcomip st0, st2");
    if (below_or_equal76) {
        addr_11a51_97:
        __asm__("fdivp st1, st0");
        __asm__("fstp tword [rsp]");
        fun_4c80(v6, 1, -1, "%.0Lf");
        rax78 = fun_4860(v6, v6);
        r15_17 = reinterpret_cast<void**>(0x11354);
        rsp42 = rsp42 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        rdx43 = rax78;
        r12_44 = rax78;
        goto addr_11798_43;
    } else {
        __asm__("fld dword [rip+0xc870]");
        __asm__("fxch st0, st2");
        __asm__("fcomi st0, st2");
        if (!cf75) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fsubr st2, st0");
            __asm__("fxch st0, st2");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fxch st0, st1");
            rax79 = v37;
            __asm__("btc rax, 0x3f");
        } else {
            __asm__("fstp st2");
            __asm__("fxch st0, st1");
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld st0");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            rax79 = v37;
        }
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rax79) < reinterpret_cast<int64_t>(0)) {
            __asm__("fadd dword [rip+0xc826]");
        }
        zf80 = v14 == 0;
        if (zf80) 
            goto addr_11b16_130;
    }
    __asm__("fstp st1");
    goto addr_11b42_132;
    addr_11b16_130:
    __asm__("fucomi st0, st1");
    __asm__("fstp st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf80) {
            addr_11b42_132:
            __asm__("fxch st0, st1");
            goto addr_11a51_97;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax79 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0xc6ac]");
        __asm__("fxch st0, st1");
        goto addr_11a51_97;
    } else {
        goto addr_11b42_132;
    }
    addr_1191a_115:
    r12_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) - reinterpret_cast<unsigned char>(r15_17));
    goto addr_11798_43;
    addr_11406_91:
    __asm__("fstp st2");
    __asm__("fstp st2");
    goto addr_11414_94;
    addr_11680_31:
    __asm__("fadd dword [rip+0xccae]");
    __asm__("fmulp st1, st0");
    if (*reinterpret_cast<unsigned char*>(&v8) & 16) 
        goto addr_113de_28;
    goto addr_116c8_25;
    addr_1193b_17:
    *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
    goto addr_1153f_140;
    addr_1152c_18:
    *reinterpret_cast<uint32_t*>(&r8_81) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_81) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbx38) = 0;
    r11_82 = r8_81;
    if (r8_81 > rcx28) 
        goto addr_1153f_140;
    do {
        rdx83 = rcx28 % r8_81;
        *reinterpret_cast<int32_t*>(&rcx84) = reinterpret_cast<int32_t>(esi35) >> 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx84) + 4) = 0;
        r9_85 = rcx28 / r8_81;
        *reinterpret_cast<int32_t*>(&rax86) = static_cast<int32_t>(rdx83 + rdx83 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
        eax87 = static_cast<int32_t>(rdi34 + rax86 * 2);
        *reinterpret_cast<uint32_t*>(&rdx88) = eax87 % *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx88) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax89) = eax87 / *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
        edx90 = static_cast<uint32_t>(rcx84 + rdx88 * 2);
        *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax89);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        rcx28 = r9_85;
        esi91 = esi35 + edx90;
        if (*reinterpret_cast<uint32_t*>(&r11_82) > edx90) {
            esi35 = reinterpret_cast<uint1_t>(!!esi91);
        } else {
            esi35 = static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r11_82) < esi91)) + 2;
        }
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (r8_81 > r9_85) 
            break;
    } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
    goto addr_1153f_140;
    if (r9_85 > 9) {
        addr_1153f_140:
        r8_46 = v25;
        if (v14 == 1) {
            rax92 = rcx28;
            *reinterpret_cast<uint32_t*>(&rax93) = *reinterpret_cast<uint32_t*>(&rax92) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax93) + 4) = 0;
            if (reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!(rax93 + esi35))) + *reinterpret_cast<uint32_t*>(&rdi34)) <= reinterpret_cast<int32_t>(5)) {
                goto addr_11578_149;
            }
        } else {
            if (v14) 
                goto addr_11578_149;
            esi94 = esi35 + *reinterpret_cast<uint32_t*>(&rdi34);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(esi94) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(esi94 == 0)) 
                goto addr_11578_149;
        }
    } else {
        if (v14 != 1) {
            if (v14) 
                goto addr_11d3c_154;
            if (!esi35) 
                goto addr_11d3c_154; else 
                goto addr_11cc7_156;
        }
        if (reinterpret_cast<int32_t>((*reinterpret_cast<uint32_t*>(&rax89) & 1) + esi35) > reinterpret_cast<int32_t>(2)) {
            addr_11cc7_156:
            *reinterpret_cast<int32_t*>(&rdx95) = static_cast<int32_t>(rax89 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax89) == 9) {
                rcx28 = r9_85 + 1;
                if (r9_85 == 9) {
                    r8_46 = v25;
                    goto addr_11d0a_160;
                } else {
                    esi35 = 0;
                    goto addr_11d44_162;
                }
            } else {
                eax96 = static_cast<uint32_t>(rdx95 + 48);
                goto addr_11cd6_164;
            }
        } else {
            addr_11d3c_154:
            if (*reinterpret_cast<uint32_t*>(&rax89)) {
                eax96 = *reinterpret_cast<uint32_t*>(&rax89) + 48;
                goto addr_11cd6_164;
            } else {
                addr_11d44_162:
                r8_46 = v25;
                if (*reinterpret_cast<unsigned char*>(&v8) & 8) {
                    addr_11d0c_166:
                    *reinterpret_cast<uint32_t*>(&rdi34) = 0;
                    if (v14 == 1) {
                        goto addr_11578_149;
                    }
                } else {
                    eax96 = 48;
                    goto addr_11cd6_164;
                }
            }
        }
    }
    ++rcx28;
    if (!r10d36) 
        goto addr_11578_149;
    *reinterpret_cast<uint32_t*>(&rax97) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    if (rax97 != rcx28) {
        goto addr_11578_149;
    }
    if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) {
        addr_11578_149:
        rbp45 = r8_46;
    } else {
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (*reinterpret_cast<unsigned char*>(&v8) & 8) 
            goto addr_11c69_88;
        *reinterpret_cast<void***>(r8_46 + 0xffffffffffffffff) = reinterpret_cast<void**>(48);
        r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp20)));
        *reinterpret_cast<uint32_t*>(&rax67) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) 
            goto addr_11e10_175; else 
            goto addr_11dde_176;
    }
    do {
        --rbp45;
        rdx98 = __intrinsic() >> 3;
        rdi99 = rdx98 + rdx98 * 4;
        rax100 = rcx28 - (rdi99 + rdi99);
        eax101 = *reinterpret_cast<int32_t*>(&rax100) + 48;
        *reinterpret_cast<void***>(rbp45) = *reinterpret_cast<void***>(&eax101);
        rax102 = rcx28;
        rcx28 = rdx98;
    } while (rax102 > 9);
    if (!(*reinterpret_cast<unsigned char*>(&v8) & 4)) 
        goto addr_117bc_63; else 
        goto addr_115c0_49;
    addr_11e10_175:
    rcx103 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
    *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax104) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax104) - 8);
    rax105 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rcx103));
    r15_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax105));
    eax106 = *reinterpret_cast<int32_t*>(&rax105) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
    if (eax106 < 8) 
        goto addr_11c69_88;
    eax107 = eax106 & 0xfffffff8;
    edx108 = 0;
    do {
        *reinterpret_cast<uint32_t*>(&rsi109) = edx108;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi109) + 4) = 0;
        edx108 = edx108 + 8;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx103) + reinterpret_cast<uint64_t>(rsi109)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rsi109));
    } while (edx108 < eax107);
    goto addr_11c69_88;
    addr_11dde_176:
    if (*reinterpret_cast<uint32_t*>(&rbp20) & 4) 
        goto addr_11e80_87;
    if (!*reinterpret_cast<uint32_t*>(&rax67)) 
        goto addr_11c69_88;
    edx110 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17));
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx110);
    if (!(*reinterpret_cast<unsigned char*>(&rax67) & 2)) 
        goto addr_11c69_88;
    edx111 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 2);
    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 2) = *reinterpret_cast<int16_t*>(&edx111);
    goto addr_11c69_88;
    addr_11d0a_160:
    esi35 = 0;
    goto addr_11d0c_166;
    addr_11cd6_164:
    *reinterpret_cast<signed char*>(v6 + 0x286) = *reinterpret_cast<signed char*>(&eax96);
    *reinterpret_cast<uint32_t*>(&rax112) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v6 + 0x286) - reinterpret_cast<unsigned char>(rbp20));
    if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) {
        rsi113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax114) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax114) - 8);
        rax115 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rsi113));
        rdi116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax115));
        eax117 = *reinterpret_cast<int32_t*>(&rax115) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
        if (eax117 >= 8) {
            r9d118 = eax117 & 0xfffffff8;
            eax119 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rdx120) = eax119;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx120) + 4) = 0;
                eax119 = eax119 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsi113) + reinterpret_cast<int64_t>(rdx120)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi116) + reinterpret_cast<int64_t>(rdx120));
            } while (eax119 < r9d118);
            goto addr_11d0a_160;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp20) & 4) {
            *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 4);
            goto addr_11d0a_160;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rax112) && (edx121 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17)), *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx121), !!(*reinterpret_cast<unsigned char*>(&rax112) & 2))) {
                edx122 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 2) = *reinterpret_cast<int16_t*>(&edx122);
                goto addr_11d0a_160;
            }
        }
    }
}

int64_t fun_11eb3(void** rdi, uint32_t* rsi, int64_t* rdx) {
    uint32_t* r13_4;
    int64_t* rbp5;
    void** rbx6;
    void* rax7;
    void* v8;
    void** rax9;
    void** rax10;
    uint32_t r12d11;
    int64_t rax12;
    void** rax13;
    int64_t rax14;
    int64_t rdx15;
    int64_t rcx16;
    int32_t edx17;
    void** rcx18;
    void** v19;
    int64_t rdi20;
    int32_t edx21;
    void** rax22;
    uint64_t rax23;
    int64_t rax24;
    void* rdx25;

    __asm__("cli ");
    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rax7 = g28;
    v8 = rax7;
    if (rdi || ((rax9 = fun_46a0("BLOCK_SIZE"), rbx6 = rax9, !!rax9) || (rax10 = fun_46a0("BLOCKSIZE"), rbx6 = rax10, !!rax10))) {
        r12d11 = 0;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx6) == 39)) {
            ++rbx6;
            r12d11 = 4;
        }
        rax12 = argmatch(rbx6, 0x24a10, 0x1e380, rbx6, 0x24a10, 0x1e380);
        if (*reinterpret_cast<int32_t*>(&rax12) >= 0) 
            goto addr_11f16_5;
    } else {
        rax13 = fun_46a0("POSIXLY_CORRECT");
        if (!rax13) {
            *rbp5 = reinterpret_cast<int64_t>("v");
            *reinterpret_cast<int32_t*>(&rax14) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            *r13_4 = 0;
            goto addr_11f2a_8;
        } else {
            *rbp5 = 0x200;
            *reinterpret_cast<int32_t*>(&rax14) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            *r13_4 = 0;
            goto addr_11f2a_8;
        }
    }
    rax14 = xstrtoumax(rbx6, rbx6);
    if (*reinterpret_cast<int32_t*>(&rax14)) {
        *r13_4 = 0;
        rdx15 = *rbp5;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx16) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
        edx17 = static_cast<int32_t>(rcx16 - 48);
        rcx18 = v19;
        if (*reinterpret_cast<unsigned char*>(&edx17) <= 9) {
            goto addr_11fa7_14;
        }
        do {
            if (rcx18 == rbx6) 
                break;
            *reinterpret_cast<uint32_t*>(&rdi20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
            ++rbx6;
            edx21 = static_cast<int32_t>(rdi20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&edx21) > 9);
        goto addr_11fa7_14;
        if (*reinterpret_cast<void***>(rcx18 + 0xffffffffffffffff) == 66) 
            goto addr_12060_18; else 
            goto addr_11f9f_19;
    }
    addr_11fc4_20:
    if (!rdx15) {
        rax22 = fun_46a0("POSIXLY_CORRECT", "POSIXLY_CORRECT");
        rax23 = reinterpret_cast<unsigned char>(rax22) - (reinterpret_cast<unsigned char>(rax22) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax22) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<uint32_t*>(&rax23) & 0x200;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
        *rbp5 = rax24 + 0x200;
        *reinterpret_cast<int32_t*>(&rax14) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    }
    addr_11f2a_8:
    rdx25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
    if (rdx25) {
        fun_4870();
    } else {
        return rax14;
    }
    addr_12060_18:
    r12d11 = r12d11 | 0x180;
    if (*reinterpret_cast<signed char*>(rcx18 + 0xfffffffffffffffe) != 0x69) {
        addr_11fa7_14:
        rdx15 = *rbp5;
        *r13_4 = r12d11;
        goto addr_11fc4_20;
    }
    addr_11fa3_25:
    r12d11 = r12d11 | 32;
    goto addr_11fa7_14;
    addr_11f9f_19:
    *reinterpret_cast<unsigned char*>(&r12d11) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d11) | 0x80);
    goto addr_11fa3_25;
    addr_11f16_5:
    *rbp5 = 1;
    *reinterpret_cast<int32_t*>(&rax14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    *r13_4 = r12d11 | *reinterpret_cast<uint32_t*>(0x1e380 + *reinterpret_cast<int32_t*>(&rax12) * 4);
    goto addr_11f2a_8;
}

void** user_alist = reinterpret_cast<void**>(0);

void*** fun_4820(int64_t rdi);

void*** fun_120a3(void** edi, void** rsi, void* rdx, void** rcx, int64_t r8) {
    void** ebp6;
    void** rbx7;
    void*** rax8;
    int64_t rdi9;
    void** r12_10;
    void*** rax11;
    void** rdi12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    ebp6 = edi;
    rbx7 = user_alist;
    if (rbx7) {
        do {
            if (*reinterpret_cast<void***>(rbx7) == ebp6) 
                break;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
        } while (rbx7);
        goto addr_120e0_4;
    } else {
        goto addr_120e0_4;
    }
    addr_120cd_6:
    *reinterpret_cast<int32_t*>(&rax8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (*reinterpret_cast<void***>(rbx7 + 16)) {
        rax8 = reinterpret_cast<void***>(rbx7 + 16);
    }
    return rax8;
    addr_120e0_4:
    *reinterpret_cast<void***>(&rdi9) = ebp6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    r12_10 = reinterpret_cast<void**>(0x1bb0a);
    rax11 = fun_4820(rdi9);
    *reinterpret_cast<int32_t*>(&rdi12) = 24;
    *reinterpret_cast<int32_t*>(&rdi12 + 4) = 0;
    if (rax11) {
        r12_10 = *rax11;
        rax13 = fun_4860(r12_10);
        rdi12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax13 + 24) & 0xfffffffffffffff8);
    }
    rax14 = xmalloc(rdi12, rsi);
    *reinterpret_cast<void***>(rax14) = ebp6;
    rbx7 = rax14;
    fun_4720(rax14 + 16, r12_10, rdx, rcx, r8);
    rax15 = user_alist;
    user_alist = rbx7;
    *reinterpret_cast<void***>(rbx7 + 8) = rax15;
    goto addr_120cd_6;
}

void** nouser_alist = reinterpret_cast<void**>(0);

void** fun_49f0(void** rdi, void** rsi);

void** fun_12143(void** rdi) {
    void** rbp2;
    void** rbx3;
    void** rbx4;
    void** rsi5;
    void** rax6;
    void** rax7;
    void** rax8;
    void* rdx9;
    void** rcx10;
    int64_t r8_11;
    void** rax12;
    void** rax13;
    uint32_t r12d14;
    void** rdx15;
    void** rcx16;
    void** r8_17;
    int64_t rax18;
    uint32_t r12d19;
    void** rdx20;
    void** rcx21;
    void** r8_22;
    int64_t rax23;

    __asm__("cli ");
    rbp2 = rdi;
    rbx3 = user_alist;
    if (!rbx3) {
        addr_12190_2:
        rbx4 = nouser_alist;
        if (!rbx4) {
            addr_121d8_3:
            rax6 = fun_49f0(rbp2, rsi5);
            rax7 = fun_4860(rbp2, rbp2);
            rax8 = xmalloc(reinterpret_cast<uint64_t>(rax7 + 24) & 0xfffffffffffffff8, rsi5);
            rbx3 = rax8;
            fun_4720(rax8 + 16, rbp2, rdx9, rcx10, r8_11);
            if (!rax6) {
                rax12 = nouser_alist;
                nouser_alist = rbx3;
                *reinterpret_cast<void***>(rbx3 + 8) = rax12;
                return rax6;
            } else {
                *reinterpret_cast<void***>(rbx3) = *reinterpret_cast<void***>(rax6 + 16);
                rax13 = user_alist;
                user_alist = rbx3;
                *reinterpret_cast<void***>(rbx3 + 8) = rax13;
            }
        } else {
            r12d14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp2));
            do {
                if (*reinterpret_cast<void***>(rbx4 + 16) != *reinterpret_cast<void***>(&r12d14)) 
                    continue;
                rsi5 = rbp2;
                rax18 = fun_4660(rbx4 + 16, rsi5, rdx15, rcx16, r8_17);
                if (!*reinterpret_cast<int32_t*>(&rax18)) 
                    goto addr_121c7_9;
                rbx4 = *reinterpret_cast<void***>(rbx4 + 8);
            } while (rbx4);
            goto addr_121d8_3;
        }
    } else {
        r12d19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
        do {
            if (*reinterpret_cast<void***>(rbx3 + 16) != *reinterpret_cast<void***>(&r12d19)) 
                continue;
            rsi5 = rbp2;
            rax23 = fun_4660(rbx3 + 16, rsi5, rdx20, rcx21, r8_22);
            if (!*reinterpret_cast<int32_t*>(&rax23)) 
                break;
            rbx3 = *reinterpret_cast<void***>(rbx3 + 8);
        } while (rbx3);
        goto addr_12190_2;
    }
    return rbx3;
    addr_121c7_9:
    return 0;
}

void** group_alist = reinterpret_cast<void**>(0);

void*** fun_48b0(int64_t rdi);

void*** fun_12253(void** edi, void** rsi, void* rdx, void** rcx, int64_t r8) {
    void** ebp6;
    void** rbx7;
    void*** rax8;
    int64_t rdi9;
    void** r12_10;
    void*** rax11;
    void** rdi12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    ebp6 = edi;
    rbx7 = group_alist;
    if (rbx7) {
        do {
            if (*reinterpret_cast<void***>(rbx7) == ebp6) 
                break;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
        } while (rbx7);
        goto addr_12290_4;
    } else {
        goto addr_12290_4;
    }
    addr_1227d_6:
    *reinterpret_cast<int32_t*>(&rax8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (*reinterpret_cast<void***>(rbx7 + 16)) {
        rax8 = reinterpret_cast<void***>(rbx7 + 16);
    }
    return rax8;
    addr_12290_4:
    *reinterpret_cast<void***>(&rdi9) = ebp6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    r12_10 = reinterpret_cast<void**>(0x1bb0a);
    rax11 = fun_48b0(rdi9);
    *reinterpret_cast<int32_t*>(&rdi12) = 24;
    *reinterpret_cast<int32_t*>(&rdi12 + 4) = 0;
    if (rax11) {
        r12_10 = *rax11;
        rax13 = fun_4860(r12_10);
        rdi12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax13 + 24) & 0xfffffffffffffff8);
    }
    rax14 = xmalloc(rdi12, rsi);
    *reinterpret_cast<void***>(rax14) = ebp6;
    rbx7 = rax14;
    fun_4720(rax14 + 16, r12_10, rdx, rcx, r8);
    rax15 = group_alist;
    group_alist = rbx7;
    *reinterpret_cast<void***>(rbx7 + 8) = rax15;
    goto addr_1227d_6;
}

void** nogroup_alist = reinterpret_cast<void**>(0);

void** fun_4a40(void** rdi, void** rsi);

void** fun_122f3(void** rdi) {
    void** rbp2;
    void** rbx3;
    void** rbx4;
    void** rsi5;
    void** rax6;
    void** rax7;
    void** rax8;
    void* rdx9;
    void** rcx10;
    int64_t r8_11;
    void** rax12;
    void** rax13;
    uint32_t r12d14;
    void** rdx15;
    void** rcx16;
    void** r8_17;
    int64_t rax18;
    uint32_t r12d19;
    void** rdx20;
    void** rcx21;
    void** r8_22;
    int64_t rax23;

    __asm__("cli ");
    rbp2 = rdi;
    rbx3 = group_alist;
    if (!rbx3) {
        addr_12340_2:
        rbx4 = nogroup_alist;
        if (!rbx4) {
            addr_12388_3:
            rax6 = fun_4a40(rbp2, rsi5);
            rax7 = fun_4860(rbp2, rbp2);
            rax8 = xmalloc(reinterpret_cast<uint64_t>(rax7 + 24) & 0xfffffffffffffff8, rsi5);
            rbx3 = rax8;
            fun_4720(rax8 + 16, rbp2, rdx9, rcx10, r8_11);
            if (!rax6) {
                rax12 = nogroup_alist;
                nogroup_alist = rbx3;
                *reinterpret_cast<void***>(rbx3 + 8) = rax12;
                return rax6;
            } else {
                *reinterpret_cast<void***>(rbx3) = *reinterpret_cast<void***>(rax6 + 16);
                rax13 = group_alist;
                group_alist = rbx3;
                *reinterpret_cast<void***>(rbx3 + 8) = rax13;
            }
        } else {
            r12d14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp2));
            do {
                if (*reinterpret_cast<void***>(rbx4 + 16) != *reinterpret_cast<void***>(&r12d14)) 
                    continue;
                rsi5 = rbp2;
                rax18 = fun_4660(rbx4 + 16, rsi5, rdx15, rcx16, r8_17);
                if (!*reinterpret_cast<int32_t*>(&rax18)) 
                    goto addr_12377_9;
                rbx4 = *reinterpret_cast<void***>(rbx4 + 8);
            } while (rbx4);
            goto addr_12388_3;
        }
    } else {
        r12d19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
        do {
            if (*reinterpret_cast<void***>(rbx3 + 16) != *reinterpret_cast<void***>(&r12d19)) 
                continue;
            rsi5 = rbp2;
            rax23 = fun_4660(rbx3 + 16, rsi5, rdx20, rcx21, r8_22);
            if (!*reinterpret_cast<int32_t*>(&rax23)) 
                break;
            rbx3 = *reinterpret_cast<void***>(rbx3 + 8);
        } while (rbx3);
        goto addr_12340_2;
    }
    return rbx3;
    addr_12377_9:
    return 0;
}

struct s180 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_12403(uint64_t rdi, struct s180* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

struct s181 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_124a3(uint64_t rdi, struct s181* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

uint64_t fun_4890(uint32_t* rdi);

uint64_t fun_12503(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_4890(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
    if (rax9) {
        fun_4870();
    } else {
        return r12_7;
    }
}

struct s182 {
    signed char[1] pad1;
    void** f1;
};

struct s182* fun_47b0();

int32_t fun_4780(void** rdi, void** rsi, void** rdx);

void** fun_4bc0();

int32_t fun_4c40();

void** fun_12593(void** rdi, void** rsi, void* rdx, void*** rcx, int32_t r8d, uint32_t r9d) {
    void** r13_7;
    uint32_t ebp8;
    void** rbx9;
    void* v10;
    void*** v11;
    int32_t v12;
    void** rax13;
    void** v14;
    void** r9_15;
    uint64_t rax16;
    void** r12_17;
    void** r15_18;
    void** r14_19;
    struct s182* rax20;
    void** r8_21;
    void** rax22;
    void** rax23;
    void** r8_24;
    void** rdx25;
    struct s182* rax26;
    void** r8_27;
    void** rsi28;
    int32_t eax29;
    void** rax30;
    void** v31;
    void** rax32;
    void* rdx33;
    int32_t eax34;
    void** edi35;
    void** r13_36;
    void** v37;
    int32_t eax38;
    void** rax39;
    void* r12_40;
    void* rdx41;
    void** rbp42;
    void** rdi43;
    void** rdx44;
    void** rax45;
    void** rdx46;
    void** rax47;
    void** r12_48;
    int32_t eax49;
    int32_t eax50;
    void** rax51;

    __asm__("cli ");
    r13_7 = rdi;
    ebp8 = r9d;
    rbx9 = rsi;
    v10 = rdx;
    v11 = rcx;
    v12 = r8d;
    rax13 = fun_4860(rdi);
    v14 = rax13;
    r9_15 = rax13;
    if (*reinterpret_cast<unsigned char*>(&ebp8) & 2 || (rax16 = fun_4850(), r9_15 = rax13, rax16 <= 1)) {
        addr_125d1_2:
        r12_17 = r9_15;
        *reinterpret_cast<int32_t*>(&r15_18) = 0;
        *reinterpret_cast<int32_t*>(&r15_18 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r14_19) = 0;
        *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
        goto addr_125da_3;
    } else {
        rax20 = fun_47b0();
        r9_15 = rax13;
        if (!reinterpret_cast<int1_t>(rax20 == -1)) {
            r8_21 = reinterpret_cast<void**>(&rax20->f1);
            rax22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_21) * 4);
            rax23 = fun_4670(rax22, r13_7);
            r9_15 = r9_15;
            r15_18 = rax23;
            if (!rax23) {
                if (!(*reinterpret_cast<unsigned char*>(&ebp8) & 1)) {
                    addr_1276a_7:
                    *reinterpret_cast<int32_t*>(&r15_18) = 0;
                    *reinterpret_cast<int32_t*>(&r15_18 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                    r8_24 = reinterpret_cast<void**>(0xffffffffffffffff);
                } else {
                    r12_17 = r9_15;
                    *reinterpret_cast<int32_t*>(&r14_19) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                    goto addr_125da_3;
                }
            } else {
                rdx25 = r8_21;
                *reinterpret_cast<int32_t*>(&r14_19) = 0;
                *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                rax26 = fun_47b0();
                r9_15 = r9_15;
                r8_27 = r8_21;
                r12_17 = r9_15;
                if (!rax26) 
                    goto addr_125da_3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax22) + 0xfffffffffffffffc) = 0;
                if (!*reinterpret_cast<void***>(r15_18)) 
                    goto addr_1292b_11; else 
                    goto addr_12803_12;
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&ebp8) & 1) 
                goto addr_125d1_2; else 
                goto addr_1276a_7;
        }
    }
    addr_126bb_14:
    fun_4630(r15_18, r15_18);
    fun_4630(r14_19, r14_19);
    return r8_24;
    addr_1292b_11:
    rsi28 = r8_27;
    eax29 = fun_4780(r15_18, rsi28, rdx25);
    r9_15 = r9_15;
    r12_17 = reinterpret_cast<void**>(static_cast<int64_t>(eax29));
    addr_12943_15:
    rax30 = *v11;
    if (reinterpret_cast<unsigned char>(rax30) >= reinterpret_cast<unsigned char>(r12_17)) {
        *reinterpret_cast<int32_t*>(&r14_19) = 0;
        *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
        goto addr_126eb_17;
    } else {
        v31 = v14 + 1;
    }
    addr_12888_19:
    rax32 = fun_4670(v31, rsi28, v31, rsi28);
    r9_15 = r9_15;
    r14_19 = rax32;
    if (!rax32) {
        r8_24 = reinterpret_cast<void**>(0xffffffffffffffff);
        if (*reinterpret_cast<unsigned char*>(&ebp8) & 1) {
            addr_125da_3:
            rax30 = *v11;
            if (reinterpret_cast<unsigned char>(rax30) >= reinterpret_cast<unsigned char>(r12_17)) {
                addr_126eb_17:
                if (reinterpret_cast<unsigned char>(r12_17) >= reinterpret_cast<unsigned char>(rax30)) {
                    rax30 = r12_17;
                    *reinterpret_cast<uint32_t*>(&rdx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
                } else {
                    rdx33 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax30) - reinterpret_cast<unsigned char>(r12_17));
                    *v11 = r12_17;
                    eax34 = v12;
                    if (eax34) 
                        goto addr_12604_23; else 
                        goto addr_12711_24;
                }
            } else {
                r9_15 = rax30;
                *reinterpret_cast<uint32_t*>(&rdx33) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
            }
        } else {
            goto addr_126bb_14;
        }
    } else {
        edi35 = *reinterpret_cast<void***>(r15_18);
        r13_36 = r15_18;
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(&r12_17 + 4) = 0;
        v37 = *v11;
        if (edi35) {
            do {
                eax38 = fun_4a90();
                if (eax38 != -1) {
                    rax39 = reinterpret_cast<void**>(static_cast<int64_t>(eax38) + reinterpret_cast<unsigned char>(r12_17));
                    if (reinterpret_cast<unsigned char>(v37) < reinterpret_cast<unsigned char>(rax39)) 
                        goto addr_12908_30;
                } else {
                    *reinterpret_cast<void***>(r13_36) = reinterpret_cast<void**>(0xfffd);
                    rax39 = r12_17 + 1;
                    if (reinterpret_cast<unsigned char>(v37) < reinterpret_cast<unsigned char>(rax39)) 
                        goto addr_12904_32;
                }
                r13_36 = r13_36 + 4;
                r12_17 = rax39;
            } while (*reinterpret_cast<int32_t*>(r13_36 + 4));
            goto addr_12908_30;
        } else {
            goto addr_12908_30;
        }
    }
    *v11 = rax30;
    eax34 = v12;
    if (!eax34) {
        addr_12711_24:
        r12_40 = rdx33;
        *reinterpret_cast<uint32_t*>(&rdx33) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
    } else {
        addr_12604_23:
        *reinterpret_cast<int32_t*>(&r12_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_40) + 4) = 0;
        if (eax34 != 1) {
            *reinterpret_cast<uint32_t*>(&rdx41) = *reinterpret_cast<uint32_t*>(&rdx33) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
            r12_40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx33) >> 1);
            rdx33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx41) + reinterpret_cast<uint64_t>(r12_40));
        }
    }
    r8_24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx33) + reinterpret_cast<unsigned char>(r9_15));
    if (*reinterpret_cast<unsigned char*>(&ebp8) & 4) {
        r8_24 = r9_15;
        *reinterpret_cast<uint32_t*>(&rdx33) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0;
    }
    if (ebp8 & 8) {
        *reinterpret_cast<int32_t*>(&r12_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_40) + 4) = 0;
    } else {
        r8_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_24) + reinterpret_cast<uint64_t>(r12_40));
    }
    if (v10) {
        rbp42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx9) + reinterpret_cast<uint64_t>(v10) + 0xffffffffffffffff);
        rdi43 = rbx9;
        if (reinterpret_cast<unsigned char>(rbx9) < reinterpret_cast<unsigned char>(rbp42)) {
            if (rdx33) {
                do {
                    ++rdi43;
                    *reinterpret_cast<void***>(rdi43 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
                    if (!(reinterpret_cast<unsigned char>(rbx9) - reinterpret_cast<unsigned char>(rdi43) + reinterpret_cast<uint64_t>(rdx33))) 
                        break;
                } while (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rdi43));
            }
        }
        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp42) - reinterpret_cast<unsigned char>(rdi43));
        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r9_15)) {
            rdx44 = r9_15;
        }
        rax45 = fun_4b50(rdi43, r13_7, rdx44);
        r8_24 = r8_24;
        rdx46 = rax45;
        if (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rax45)) {
            if (r12_40) {
                do {
                    ++rdx46;
                    *reinterpret_cast<void***>(rdx46 + 0xffffffffffffffff) = reinterpret_cast<void**>(32);
                    if (!(reinterpret_cast<uint64_t>(r12_40) - reinterpret_cast<unsigned char>(rdx46) + reinterpret_cast<unsigned char>(rax45))) 
                        break;
                } while (reinterpret_cast<unsigned char>(rbp42) > reinterpret_cast<unsigned char>(rdx46));
            }
        }
        *reinterpret_cast<void***>(rdx46) = reinterpret_cast<void**>(0);
        goto addr_126bb_14;
    }
    addr_12908_30:
    *reinterpret_cast<void***>(r13_36) = reinterpret_cast<void**>(0);
    r13_7 = r14_19;
    rax47 = fun_4bc0();
    r9_15 = rax47;
    goto addr_125da_3;
    addr_12904_32:
    goto addr_12908_30;
    addr_12803_12:
    r12_48 = r15_18;
    do {
        eax49 = fun_4c40();
        r9_15 = r9_15;
        r8_27 = r8_27;
        if (!eax49) {
            *reinterpret_cast<void***>(r12_48) = reinterpret_cast<void**>(0xfffd);
            *reinterpret_cast<int32_t*>(&r14_19) = 1;
        }
        r12_48 = r12_48 + 4;
    } while (*reinterpret_cast<int32_t*>(r12_48 + 4));
    rsi28 = r8_27;
    eax50 = fun_4780(r15_18, rsi28, rdx25);
    r9_15 = r9_15;
    r12_17 = reinterpret_cast<void**>(static_cast<int64_t>(eax50));
    if (!*reinterpret_cast<signed char*>(&r14_19)) 
        goto addr_12943_15;
    rsi28 = r15_18;
    rax51 = fun_4bc0();
    r9_15 = r9_15;
    v31 = rax51 + 1;
    goto addr_12888_19;
}

void** fun_129a3(void** rdi, void** rsi, int32_t edx, int32_t ecx) {
    void** rdx3;
    void** rcx4;
    void** r14_5;
    int32_t r13d6;
    void** r12_7;
    void** rbx8;
    void** rax9;
    int32_t v10;
    void** v11;
    void** rbp12;
    void** r15_13;
    uint64_t r8_14;
    int64_t r9_15;
    void** rax16;
    void** rdi17;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    *reinterpret_cast<int32_t*>(&rcx4) = ecx;
    __asm__("cli ");
    r14_5 = rdi;
    r13d6 = *reinterpret_cast<int32_t*>(&rdx3);
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    rbx8 = rsi;
    rax9 = *reinterpret_cast<void***>(rsi);
    v10 = *reinterpret_cast<int32_t*>(&rcx4);
    v11 = rax9;
    do {
        rbp12 = rax9 + 1;
        r15_13 = r12_7;
        rax16 = fun_4af0(r12_7, rbp12, rdx3, rcx4, r8_14, r9_15);
        r12_7 = rax16;
        if (!rax16) 
            break;
        *reinterpret_cast<int32_t*>(&r9_15) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_15) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_14) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_14) + 4) = 0;
        rcx4 = rbx8;
        rdx3 = rbp12;
        *reinterpret_cast<void***>(rbx8) = v11;
        rax9 = mbsalign(r14_5, r12_7, r14_5, r12_7);
        if (rax9 == 0xffffffffffffffff) 
            goto addr_12a30_4;
    } while (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(rax9));
    goto addr_12a15_6;
    fun_4630(r15_13, r15_13);
    addr_12a15_6:
    return r12_7;
    addr_12a30_4:
    rdi17 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    fun_4630(rdi17, rdi17);
    goto addr_12a15_6;
}

int32_t fun_4760(int64_t rdi, void** rsi);

int64_t fun_12a53(void** rdi, void** rsi, uint32_t edx) {
    void** r15_4;
    void** rbp5;
    uint32_t v6;
    void* rax7;
    void* v8;
    uint64_t rax9;
    void* rsp10;
    int32_t r12d11;
    void* rax12;
    int64_t rax13;
    uint16_t** rax14;
    uint16_t* rdx15;
    uint32_t ecx16;
    int64_t rax17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    uint32_t v21;
    uint32_t eax22;
    uint32_t eax23;
    void** rsi24;
    void** rax25;
    void** rbx26;
    int32_t eax27;
    int64_t rdi28;
    int32_t v29;
    int32_t eax30;
    uint32_t eax31;
    uint32_t eax32;

    __asm__("cli ");
    r15_4 = rdi;
    rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    v6 = edx;
    rax7 = g28;
    v8 = rax7;
    rax9 = fun_4850();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    if (rax9 <= 1) {
        r12d11 = 0;
        if (reinterpret_cast<unsigned char>(r15_4) >= reinterpret_cast<unsigned char>(rbp5)) {
            addr_12b98_3:
            rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
            if (rax12) {
                fun_4870();
            } else {
                *reinterpret_cast<int32_t*>(&rax13) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                return rax13;
            }
        } else {
            rax14 = fun_4c70(rdi, rsi);
            r12d11 = 0;
            rdx15 = *rax14;
            ecx16 = v6 & 2;
            do {
                *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_4));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                ++r15_4;
                eax18 = rdx15[rax17];
                if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax18) + 1) & 64) {
                    addr_12c09_8:
                    if (r12d11 == 0x7fffffff) 
                        goto addr_12b98_3;
                } else {
                    if (ecx16) 
                        goto addr_12c38_10;
                    if (*reinterpret_cast<unsigned char*>(&eax18) & 2) 
                        continue; else 
                        goto addr_12c09_8;
                }
                ++r12d11;
            } while (rbp5 != r15_4);
        }
    } else {
        r12d11 = 0;
        if (reinterpret_cast<unsigned char>(r15_4) >= reinterpret_cast<unsigned char>(rbp5)) 
            goto addr_12b98_3;
        r13_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 32);
        r14_20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 28);
        v21 = v6 & 2;
        do {
            eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_4));
            if (*reinterpret_cast<signed char*>(&eax22) > 95) {
                eax23 = eax22 - 97;
                if (*reinterpret_cast<unsigned char*>(&eax23) <= 29) {
                    addr_12b83_18:
                    ++r15_4;
                    ++r12d11;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax22) > 64) 
                    goto addr_12b83_18;
                if (*reinterpret_cast<signed char*>(&eax22) > 35) 
                    goto addr_12b78_22; else 
                    goto addr_12acc_23;
            }
            addr_12ad4_24:
            do {
                rsi24 = r15_4;
                rax25 = rpl_mbrtowc(r14_20, rsi24);
                if (rax25 == 0xffffffffffffffff) 
                    break;
                if (rax25 == 0xfffffffffffffffe) 
                    goto addr_12c48_27;
                *reinterpret_cast<int32_t*>(&rbx26) = 1;
                *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
                if (rax25) {
                    rbx26 = rax25;
                }
                eax27 = fun_4a90();
                if (eax27 >= 0) {
                    if (0x7fffffff - r12d11 < eax27) 
                        goto addr_12c60_32;
                    r12d11 = r12d11 + eax27;
                    continue;
                } else {
                    if (v21) 
                        goto addr_12c38_10;
                    *reinterpret_cast<int32_t*>(&rdi28) = v29;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
                    eax30 = fun_4760(rdi28, rsi24);
                    if (eax30) 
                        continue;
                }
                if (r12d11 == 0x7fffffff) 
                    goto addr_12c60_32;
                ++r12d11;
                r15_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_4) + reinterpret_cast<unsigned char>(rbx26));
                eax31 = fun_4c30(r13_19, rsi24);
            } while (!eax31);
            continue;
            if (!(*reinterpret_cast<unsigned char*>(&v6) & 1)) 
                goto addr_12b83_18; else 
                goto addr_12c33_40;
            addr_12c48_27:
            if (*reinterpret_cast<unsigned char*>(&v6) & 1) 
                goto addr_12c38_10;
            ++r12d11;
            r15_4 = rbp5;
            continue;
            addr_12b78_22:
            eax32 = eax22 - 37;
            if (*reinterpret_cast<unsigned char*>(&eax32) > 26) 
                goto addr_12ad4_24; else 
                goto addr_12b83_18;
            addr_12acc_23:
            if (*reinterpret_cast<signed char*>(&eax22) > 31) 
                goto addr_12b83_18; else 
                goto addr_12ad4_24;
        } while (reinterpret_cast<unsigned char>(r15_4) < reinterpret_cast<unsigned char>(rbp5));
        goto addr_12b94_43;
    }
    goto addr_12b98_3;
    addr_12c38_10:
    r12d11 = -1;
    goto addr_12b98_3;
    addr_12b94_43:
    goto addr_12b98_3;
    addr_12c60_32:
    r12d11 = 0x7fffffff;
    goto addr_12b98_3;
    addr_12c33_40:
    goto addr_12c38_10;
}

void fun_12c73(void** rdi, int32_t esi) {
    __asm__("cli ");
    fun_4860(rdi);
    goto mbsnwidth;
}

struct s183 {
    int64_t f0;
    int64_t f8;
};

void fun_12f63(struct s183* rdi, uint64_t rsi, int64_t rdx) {
    int64_t r12_4;
    int64_t rbx5;
    int32_t eax6;

    __asm__("cli ");
    if (rsi > 2) {
        goto mpsort_with_tmp_part_0;
    } else {
        if (rsi != 2 || (r12_4 = rdi->f8, rbx5 = rdi->f0, eax6 = reinterpret_cast<int32_t>(rdx(rbx5, r12_4)), reinterpret_cast<uint1_t>(eax6 < 0) | reinterpret_cast<uint1_t>(eax6 == 0))) {
            return;
        } else {
            rdi->f0 = r12_4;
            rdi->f8 = rbx5;
            return;
        }
    }
}

void** fun_14b33(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9) {
    int64_t v7;
    int64_t v8;
    void** rax9;

    __asm__("cli ");
    rax9 = __strftime_internal_isra_0(rdi, rsi, rdx, rcx, 0, 0, 0xff, r8, r9, v7, v8);
    return rax9;
}

struct s185 {
    int64_t f0;
    int64_t f8;
};

struct s184 {
    int64_t f0;
    struct s185* f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    signed char[8] pad48;
    void** f30;
    signed char[7] pad56;
    int64_t f38;
    signed char[8] pad72;
    int64_t f48;
    unsigned char f50;
};

int64_t obstack_alloc_failed_handler = 0x14b50;

void fun_14b53() {
    void** rax1;
    void** rdi2;
    int64_t rsi3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** v7;
    void** v8;
    void** v9;
    void** v10;
    struct s184* rdi11;
    void** r12_12;
    void** rbp13;
    int64_t rax14;
    int64_t rdi15;
    struct s185* rax16;
    uint64_t rdx17;
    int64_t rdx18;

    __asm__("cli ");
    rax1 = fun_4840();
    rdi2 = stderr;
    *reinterpret_cast<int32_t*>(&rsi3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi3) + 4) = 0;
    fun_4c10(rdi2, 1, "%s\n", rax1, r8_4, r9_5, rax6, __return_address(), v7, v8, v9, v10);
    *reinterpret_cast<uint32_t*>(&rdi11) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    fun_4bf0();
    if (0) {
        *reinterpret_cast<int32_t*>(&r12_12) = 15;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp13) = 16;
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    } else {
        rbp13 = reinterpret_cast<void**>("%s\n");
        r12_12 = reinterpret_cast<void**>(" %s\n");
    }
    rdi11->f30 = r12_12;
    if (0) {
        rsi3 = reinterpret_cast<int64_t>("at");
    }
    rax14 = rdi11->f38;
    rdi11->f0 = rsi3;
    if (!(rdi11->f50 & 1)) {
        rdi15 = rsi3;
        rax16 = reinterpret_cast<struct s185*>(rax14(rdi15));
    } else {
        rdi15 = rdi11->f48;
        rax16 = reinterpret_cast<struct s185*>(rax14(rdi15));
    }
    rdi11->f8 = rax16;
    if (!rax16) {
        obstack_alloc_failed_handler(rdi15);
    } else {
        rdx17 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax16) + reinterpret_cast<unsigned char>(r12_12)) + 16 & reinterpret_cast<uint64_t>(-reinterpret_cast<unsigned char>(rbp13));
        rdi11->f10 = rdx17;
        rdi11->f18 = rdx17;
        rdx18 = rdi11->f0 + reinterpret_cast<int64_t>(rax16);
        rax16->f0 = rdx18;
        rdi11->f20 = rdx18;
        rax16->f8 = 0;
        rdi11->f50 = reinterpret_cast<unsigned char>(rdi11->f50 & 0xf9);
        goto rax6;
    }
}

struct s186 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    signed char[8] pad80;
    unsigned char f50;
};

void fun_14c43(struct s186* rdi) {
    int64_t rcx2;
    int64_t r8_3;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 & 0xfe);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    goto 0x14ba0;
}

struct s187 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_14c63(struct s187* rdi) {
    int64_t rcx2;
    int64_t r8_3;
    int64_t r9_4;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 | 1);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    rdi->f48 = r9_4;
    goto 0x14ba0;
}

struct s189 {
    void* f0;
    struct s189* f8;
};

struct s188 {
    struct s188* f0;
    struct s189* f8;
    void** f10;
    signed char[7] pad24;
    void* f18;
    void* f20;
    signed char[8] pad48;
    void* f30;
    int64_t f38;
    int64_t f40;
    struct s188* f48;
    unsigned char f50;
};

void** fun_14c83(struct s188* rdi, struct s188* rsi) {
    void** rcx3;
    struct s188* rbx4;
    void** r13_5;
    struct s188* tmp64_6;
    struct s189* rbp7;
    struct s188* tmp64_8;
    struct s188* rsi9;
    struct s188* rax10;
    int64_t rax11;
    struct s189* rax12;
    struct s189* r12_13;
    struct s189* rax14;
    void* rax15;
    void** rsi16;
    void** r14_17;
    void** rax18;
    uint32_t edx19;
    int64_t rax20;
    struct s188* rdi21;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx3) = 0;
    *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
    rbx4 = rdi;
    r13_5 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi->f18) - reinterpret_cast<unsigned char>(rdi->f10));
    tmp64_6 = reinterpret_cast<struct s188*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(r13_5));
    rbp7 = rdi->f8;
    *reinterpret_cast<unsigned char*>(&rcx3) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_6) < reinterpret_cast<uint64_t>(rsi));
    tmp64_8 = reinterpret_cast<struct s188*>(reinterpret_cast<uint64_t>(tmp64_6) + reinterpret_cast<int64_t>(rdi->f30));
    rsi9 = tmp64_8;
    *reinterpret_cast<unsigned char*>(&rdi) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_8) < reinterpret_cast<uint64_t>(tmp64_6));
    rax10 = reinterpret_cast<struct s188*>(reinterpret_cast<uint64_t>(rsi9) + (reinterpret_cast<unsigned char>(r13_5) >> 3) + 100);
    if (reinterpret_cast<uint64_t>(rsi9) < reinterpret_cast<uint64_t>(rbx4->f0)) {
        rsi9 = rbx4->f0;
    }
    if (reinterpret_cast<uint64_t>(rax10) >= reinterpret_cast<uint64_t>(rsi9)) {
        rsi9 = rax10;
    }
    if (!rcx3 && (*reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0, !rdi)) {
        rax11 = rbx4->f38;
        if (rbx4->f50 & 1) {
            rdi = rbx4->f48;
            rax12 = reinterpret_cast<struct s189*>(rax11(rdi));
            r12_13 = rax12;
        } else {
            rdi = rsi9;
            rax14 = reinterpret_cast<struct s189*>(rax11(rdi));
            r12_13 = rax14;
        }
        if (r12_13) {
            rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<uint64_t>(rsi9));
            rbx4->f8 = r12_13;
            rsi16 = rbx4->f10;
            r12_13->f8 = rbp7;
            rbx4->f20 = rax15;
            r12_13->f0 = rax15;
            r14_17 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<int64_t>(rbx4->f30) + 16) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)));
            rax18 = fun_4a30(r14_17, rsi16, r13_5, rcx3);
            edx19 = rbx4->f50;
            if (!(*reinterpret_cast<unsigned char*>(&edx19) & 2) && (rax18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)) & reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp7) + reinterpret_cast<int64_t>(rbx4->f30) + 16)), rbx4->f10 == rax18)) {
                r12_13->f8 = rbp7->f8;
                rax20 = rbx4->f40;
                if (!(edx19 & 1)) {
                    rax18 = reinterpret_cast<void**>(rax20(rbp7, rsi16));
                } else {
                    rdi21 = rbx4->f48;
                    rax18 = reinterpret_cast<void**>(rax20(rdi21, rbp7));
                }
            }
            rbx4->f10 = r14_17;
            rbx4->f18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_17) + reinterpret_cast<unsigned char>(r13_5));
            rbx4->f50 = reinterpret_cast<unsigned char>(rbx4->f50 & 0xfd);
            return rax18;
        }
    }
    obstack_alloc_failed_handler(rdi);
}

struct s190 {
    struct s190* f0;
    struct s190* f8;
};

struct s191 {
    signed char[8] pad8;
    struct s190* f8;
};

struct s190* fun_14db3(struct s191* rdi, struct s190* rsi) {
    struct s190* rax3;

    __asm__("cli ");
    rax3 = rdi->f8;
    if (!rax3) {
        return rax3;
    }
    do {
        if (reinterpret_cast<uint64_t>(rsi) <= reinterpret_cast<uint64_t>(rax3)) 
            continue;
        if (reinterpret_cast<uint64_t>(rax3->f0) >= reinterpret_cast<uint64_t>(rsi)) 
            break;
        rax3 = rax3->f8;
    } while (rax3);
    goto addr_14dd3_7;
    return 1;
    addr_14dd3_7:
    return 0;
}

struct s193 {
    struct s193* f0;
    struct s193* f8;
};

struct s192 {
    signed char[8] pad8;
    struct s193* f8;
    struct s193* f10;
    struct s193* f18;
    struct s193* f20;
    signed char[24] pad64;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_14df3(struct s192* rdi, struct s193* rsi) {
    struct s193* r12_3;
    struct s193* rsi4;
    struct s192* rbx5;
    struct s193* rax6;
    struct s193* rbp7;
    int64_t rax8;
    int64_t rdi9;

    __asm__("cli ");
    r12_3 = rsi;
    rsi4 = rdi->f8;
    rbx5 = rdi;
    if (rsi4) {
        while (reinterpret_cast<uint64_t>(rsi4) >= reinterpret_cast<uint64_t>(r12_3) || (rax6 = rsi4->f0, reinterpret_cast<uint64_t>(rax6) < reinterpret_cast<uint64_t>(r12_3))) {
            rbp7 = rsi4->f8;
            rax8 = rbx5->f40;
            if (rbx5->f50 & 1) {
                rdi9 = rbx5->f48;
                rax8(rdi9);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_14e4b_5;
            } else {
                rax8(rsi4);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_14e4b_5;
            }
            rsi4 = rbp7;
        }
    } else {
        goto addr_14e4b_5;
    }
    rbx5->f18 = r12_3;
    rbx5->f10 = r12_3;
    rbx5->f20 = rax6;
    rbx5->f8 = rsi4;
    return;
    addr_14e4b_5:
    if (r12_3) 
        goto 0x4cbf;
    return;
}

struct s195 {
    int64_t f0;
    struct s195* f8;
};

struct s194 {
    signed char[8] pad8;
    struct s195* f8;
};

int64_t fun_14e83(struct s194* rdi) {
    struct s195* rax2;
    int64_t r8_3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_3) + 4) = 0;
    if (rax2) {
        do {
            rdx4 = rax2->f0 - reinterpret_cast<int64_t>(rax2);
            rax2 = rax2->f8;
            r8_3 = r8_3 + rdx4;
        } while (rax2);
    }
    return r8_3;
}

void fun_4c00(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_14eb3(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s11* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_4c00("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_46e0();
    } else {
        rbx3 = rdi;
        rax4 = fun_48e0(rdi, 47);
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_4700(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_16653(int64_t rdi) {
    int64_t rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_46f0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x26520;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_16693(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x26520);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_166b3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x26520);
    }
    *rdi = esi;
    return 0x26520;
}

int64_t fun_166d3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x26520);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s196 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_16713(struct s196* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s196*>(0x26520);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s197 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s197* fun_16733(struct s197* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s197*>(0x26520);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x4cce;
    if (!rdx) 
        goto 0x4cce;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x26520;
}

struct s198 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_16773(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s198* r8) {
    struct s198* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s198*>(0x26520);
    }
    rax7 = fun_46f0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x167a6);
    *rax7 = r15d8;
    return rax13;
}

struct s199 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_167f3(int64_t rdi, int64_t rsi, void*** rdx, struct s199* rcx) {
    struct s199* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s199*>(0x26520);
    }
    rax6 = fun_46f0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x16821);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x1687c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_168e3() {
    __asm__("cli ");
}

void** g25278 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_168f3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_4630(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x26420) {
        fun_4630(rdi7);
        g25278 = reinterpret_cast<void**>(0x26420);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x25270) {
        fun_4630(r12_2);
        slotvec = reinterpret_cast<void**>(0x25270);
    }
    nslots = 1;
    return;
}

void fun_16993() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_169b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_169c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_169e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_16a03(int32_t edi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s19* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x4cd4;
    rcx5 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_4870();
    } else {
        return rax6;
    }
}

void** fun_16a93(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s19* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x4cd9;
    rcx6 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_4870();
    } else {
        return rax7;
    }
}

void** fun_16b23(int32_t edi, int64_t rsi) {
    void* rax3;
    struct s19* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x4cde;
    rcx4 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
    if (rdx6) {
        fun_4870();
    } else {
        return rax5;
    }
}

void** fun_16bb3(int32_t edi, int64_t rsi, int64_t rdx) {
    void* rax4;
    struct s19* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x4ce3;
    rcx5 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_4870();
    } else {
        return rax6;
    }
}

void** fun_16c43(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s19* rsp4;
    void* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xf8d0]");
    __asm__("movdqa xmm1, [rip+0xf8d8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xf8c1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
    if (rdx11) {
        fun_4870();
    } else {
        return rax10;
    }
}

void** fun_16ce3(int64_t rdi, uint32_t esi) {
    struct s19* rsp3;
    void* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xf830]");
    __asm__("movdqa xmm1, [rip+0xf838]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xf821]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx10) {
        fun_4870();
    } else {
        return rax9;
    }
}

void** fun_16d83(int64_t rdi) {
    void* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf790]");
    __asm__("movdqa xmm1, [rip+0xf798]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xf779]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax2) - reinterpret_cast<uint64_t>(g28));
    if (rdx4) {
        fun_4870();
    } else {
        return rax3;
    }
}

void** fun_16e13(int64_t rdi, int64_t rsi) {
    void* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf700]");
    __asm__("movdqa xmm1, [rip+0xf708]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xf6f6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
    if (rdx5) {
        fun_4870();
    } else {
        return rax4;
    }
}

void** fun_16ea3(int32_t edi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s19* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x4ce8;
    rcx5 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_4870();
    } else {
        return rax6;
    }
}

void** fun_16f43(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s19* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf5ca]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xf5c2]");
    __asm__("movdqa xmm2, [rip+0xf5ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x4ced;
    if (!rdx) 
        goto 0x4ced;
    rcx6 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_4870();
    } else {
        return rax7;
    }
}

void** fun_16fe3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void* rcx6;
    struct s19* rcx7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf52a]");
    __asm__("movdqa xmm1, [rip+0xf532]");
    __asm__("movdqa xmm2, [rip+0xf53a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x4cf2;
    if (!rdx) 
        goto 0x4cf2;
    rcx7 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx6) - reinterpret_cast<uint64_t>(g28));
    if (rdx9) {
        fun_4870();
    } else {
        return rax8;
    }
}

void** fun_17093(int64_t rdi, int64_t rsi, int64_t rdx) {
    void* rdx4;
    struct s19* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf47a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xf472]");
    __asm__("movdqa xmm2, [rip+0xf47a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x4cf7;
    if (!rsi) 
        goto 0x4cf7;
    rcx5 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_4870();
    } else {
        return rax6;
    }
}

void** fun_17133(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s19* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xf3da]");
    __asm__("movdqa xmm1, [rip+0xf3e2]");
    __asm__("movdqa xmm2, [rip+0xf3ea]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x4cfc;
    if (!rsi) 
        goto 0x4cfc;
    rcx6 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_4870();
    } else {
        return rax7;
    }
}

void fun_171d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_171e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_17203() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_17223(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s200 {
    signed char[16] pad16;
    void** f10;
};

void** fun_17243(struct s200* rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** rdi7;
    void** r12_8;
    void** rax9;

    __asm__("cli ");
    rdi7 = reinterpret_cast<void**>(&rdi->f10);
    r12_8 = *reinterpret_cast<void***>(rdi7 + 0xfffffffffffffff0);
    if (r12_8 == rdi7) {
        rax9 = fun_4670(rsi, rsi);
        if (rax9) {
            goto fun_4a30;
        }
    } else {
        rax9 = fun_4af0(r12_8, rsi, rdx, rcx, r8, r9);
        if (!rax9) {
            rax9 = r12_8;
        }
    }
    return rax9;
}

struct s201 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_172a3(struct s201* rdi, void** rsi) {
    void** rax3;
    void** rdi4;
    void** r12_5;
    void** rbp6;
    void*** rax7;
    void** rax8;

    __asm__("cli ");
    rax3 = rdi->f8;
    rdi4 = rdi->f0;
    r12_5 = reinterpret_cast<void**>(&rdi->f10);
    rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rax3));
    if (rdi4 != r12_5) {
        fun_4630(rdi4);
        rax3 = rdi->f8;
    }
    if (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(rbp6)) {
        rax7 = fun_46f0();
        *rax7 = reinterpret_cast<void**>(12);
    } else {
        rax8 = fun_4670(rbp6, rsi);
        if (rax8) {
            rdi->f0 = rax8;
            rdi->f8 = rbp6;
            return 1;
        }
    }
    rdi->f0 = r12_5;
    rdi->f8 = reinterpret_cast<void**>("v");
    return 0;
}

struct s202 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_17323(struct s202* rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** rbp10;
    void** rax11;
    int64_t rax12;
    void** rax13;
    void** rcx14;
    void*** rax15;
    void** rax16;

    __asm__("cli ");
    r14_7 = reinterpret_cast<void**>(&rdi->f10);
    r13_8 = rdi->f8;
    r12_9 = rdi->f0;
    rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(r13_8));
    if (r12_9 == r14_7) {
        rax11 = fun_4670(rbp10, rsi);
        if (!rax11) {
            *reinterpret_cast<int32_t*>(&rax12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        } else {
            rax13 = fun_4a30(rax11, r12_9, r13_8, rcx);
            rcx14 = rax13;
            goto addr_1735c_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(rbp10)) {
            rax15 = fun_46f0();
            *rax15 = reinterpret_cast<void**>(12);
            goto addr_173ab_8;
        } else {
            rax16 = fun_4af0(r12_9, rbp10, rdx, rcx, r8, r9);
            rcx14 = rax16;
            if (!rax16) {
                r12_9 = rdi->f0;
                goto addr_173ab_8;
            } else {
                addr_1735c_5:
                rdi->f0 = rcx14;
                *reinterpret_cast<int32_t*>(&rax12) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                rdi->f8 = rbp10;
            }
        }
    }
    addr_17368_11:
    return rax12;
    addr_173ab_8:
    fun_4630(r12_9, r12_9);
    rdi->f0 = r14_7;
    *reinterpret_cast<int32_t*>(&rax12) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
    rdi->f8 = reinterpret_cast<void**>("v");
    goto addr_17368_11;
}

int64_t fun_173e3(int64_t rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    int32_t r13d6;
    void** rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = fun_4b10(rdi);
    if (!rax5) {
        r13d6 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax7 = fun_4860(rax5);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax7)) {
            fun_4a30(rsi, rax5, rax7 + 1, rcx);
            return 0;
        } else {
            r13d6 = 34;
            if (rdx) {
                fun_4a30(rsi, rax5, rdx + 0xffffffffffffffff, rcx);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_17493() {
    __asm__("cli ");
    goto fun_4b10;
}

void** fun_17533(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rax5;
    void** rbx6;
    void** rax7;
    void** rax8;
    void** rcx9;

    __asm__("cli ");
    if (!rdi) {
        rax3 = fun_4670(0x80, rsi);
        r12_4 = rax3;
        if (rax3) {
            *reinterpret_cast<void***>(r12_4) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0);
            return r12_4;
        }
    } else {
        rax5 = fun_4860(rdi);
        rbx6 = rax5 + 1;
        *reinterpret_cast<int32_t*>(&rax7) = 0x76;
        *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx6) >= reinterpret_cast<unsigned char>(0x76)) {
            rax7 = rbx6;
        }
        rax8 = fun_4670(reinterpret_cast<uint64_t>(rax7 + 17) & 0xfffffffffffffff8, rsi);
        r12_4 = rax8;
        if (rax8) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(1);
            fun_4a30(r12_4 + 9, rdi, rbx6, rcx9);
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(rbx6) + 9) = 0;
        }
    }
    return r12_4;
}

void fun_17803(void** rdi) {
    void** rbx2;
    void** rdi3;

    __asm__("cli ");
    if (rdi == 1) {
        return;
    } else {
        rbx2 = rdi;
        if (rdi) {
            do {
                rdi3 = rbx2;
                rbx2 = *reinterpret_cast<void***>(rbx2);
                fun_4630(rdi3);
            } while (rbx2);
        }
        return;
    }
}

int64_t fun_4640(int64_t rdi, void** rsi);

void** fun_17843(void** rdi, int64_t rsi, void** rdx) {
    void** rax4;
    void** rsi5;
    int64_t rax6;
    signed char al7;
    signed char al8;

    __asm__("cli ");
    if (rdi) {
        rax4 = set_tz(rdi);
        if (rax4) {
            rsi5 = rdx;
            rax6 = fun_4640(rsi, rsi5);
            if (!rax6 || (rsi5 = rdx, al7 = save_abbr(rdi, rsi5), al7 == 0)) {
                if (rax4 != 1) {
                    revert_tz_part_0(rax4, rsi5);
                }
            } else {
                if (reinterpret_cast<int1_t>(rax4 == 1) || (al8 = revert_tz_part_0(rax4, rsi5), !!al8)) {
                    return rdx;
                }
            }
        }
        return 0;
    }
}

int64_t rpl_mktime(void** rdi);

int64_t fun_178f3(void** rdi, void** rsi) {
    void** rbp3;
    void* rax4;
    void* rax5;
    void** rax6;
    int64_t r13_7;
    void** r15_8;
    int64_t rax9;
    signed char al10;
    signed char al11;
    void** v12;
    void* rax13;

    __asm__("cli ");
    rbp3 = rsi;
    rax4 = g28;
    if (!rdi) {
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
        if (rax5) {
            fun_4870();
        }
    }
    rax6 = set_tz(rdi);
    if (!rax6) {
        addr_17980_7:
        r13_7 = -1;
    } else {
        r15_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 64) - 8 + 8);
        rax9 = rpl_mktime(r15_8);
        r13_7 = rax9;
        if (!0 || (rsi = r15_8, al10 = save_abbr(rdi, rsi), al10 == 0)) {
            if (rax6 != 1) {
                revert_tz_part_0(rax6, rsi);
                goto addr_17980_7;
            }
        } else {
            if (reinterpret_cast<int1_t>(rax6 == 1) || (al11 = revert_tz_part_0(rax6, rsi), !!al11)) {
                __asm__("movdqa xmm0, [rsp]");
                __asm__("movdqa xmm1, [rsp+0x10]");
                __asm__("movdqa xmm2, [rsp+0x20]");
                __asm__("movups [rbp+0x0], xmm0");
                *reinterpret_cast<void***>(rbp3 + 48) = v12;
                __asm__("movups [rbp+0x10], xmm1");
                __asm__("movups [rbp+0x20], xmm2");
            } else {
                goto addr_17980_7;
            }
        }
    }
    rax13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (!rax13) {
        return r13_7;
    }
}

struct s203 {
    signed char[32] pad32;
    int32_t f20;
};

void fun_17a43(struct s203* rdi) {
    __asm__("cli ");
    rdi->f20 = 0;
    goto 0x19160;
}

struct s204 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    void** f40;
};

void fun_49e0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_17a63(void** rdi, void** rsi, void** rdx, void** rcx, struct s204* r8, void** r9) {
    void** r12_7;
    void** v8;
    void** v9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** rax20;
    void** v21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** rax27;
    void** v28;
    void** v29;
    void** v30;
    void** v31;
    void** v32;
    void** v33;
    void** r10_34;
    void** r9_35;
    void** r8_36;
    void** rcx37;
    void** r15_38;
    void** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_4c10(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_4c10(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_4840();
    fun_4c10(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_49e0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_4840();
    fun_4c10(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_49e0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_4840();
        fun_4c10(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x1ece8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x1ece8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_17ed3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s205 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_17ef3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s205* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s205* rcx8;
    void* rax9;
    void* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
    if (rax18) {
        fun_4870();
    } else {
        return;
    }
}

void fun_17f93(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void* rax15;
    void* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_18036_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_18040_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v16) - reinterpret_cast<uint64_t>(g28));
    if (rax20) {
        fun_4870();
    } else {
        return;
    }
    addr_18036_5:
    goto addr_18040_7;
}

void fun_18073() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** rax9;
    void** r8_10;

    __asm__("cli ");
    rsi1 = stdout;
    fun_49e0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_4840();
    fun_4b20(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8);
    rax9 = fun_4840();
    fun_4b20(1, rax9, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_10);
    fun_4840();
    goto fun_4b20;
}

int64_t fun_4770();

void fun_18113(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_4770();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_18153(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18173(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18193(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_181b3(void** rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** rax7;

    __asm__("cli ");
    rax7 = fun_4af0(rdi, rsi, rdx, rcx, r8, r9);
    if (rax7 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_181e3(void** rdi, uint64_t rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    uint64_t rax7;
    void** rax8;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rsi == 0);
    rax8 = fun_4af0(rdi, rsi | rax7, rdx, rcx, r8, r9);
    if (!rax8) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18213(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_4770();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_18253() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_4770();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18293(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_4770();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_182c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_4770();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18313(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_4770();
            if (rax5) 
                break;
            addr_1835d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_1835d_5;
        rax8 = fun_4770();
        if (rax8) 
            goto addr_18346_9;
        if (rbx4) 
            goto addr_1835d_5;
        addr_18346_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_183a3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_4770();
            if (rax8) 
                break;
            addr_183ea_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_183ea_5;
        rax11 = fun_4770();
        if (rax11) 
            goto addr_183d2_9;
        if (!rbx6) 
            goto addr_183d2_9;
        if (r12_4) 
            goto addr_183ea_5;
        addr_183d2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_18433(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** r13_7;
    void** rdi8;
    void*** r12_9;
    void** rsi10;
    void** rcx11;
    void** rbx12;
    void** rax13;
    void** rbp14;
    void* rbp15;
    void** rax16;

    __asm__("cli ");
    r13_7 = rdi;
    rdi8 = rdx;
    r12_9 = rsi;
    rsi10 = rcx;
    rcx11 = *r12_9;
    rbx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx11) >> 1) + reinterpret_cast<unsigned char>(rcx11));
    if (__intrinsic()) {
        rbx12 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax13 = rsi10;
    if (reinterpret_cast<signed char>(rbx12) <= reinterpret_cast<signed char>(rsi10)) {
        rax13 = rbx12;
    }
    if (reinterpret_cast<signed char>(rsi10) >= reinterpret_cast<signed char>(0)) {
        rbx12 = rax13;
    }
    rbp14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx12) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp15 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_184dd_9:
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) % reinterpret_cast<int64_t>(r8));
            rbx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) / reinterpret_cast<int64_t>(r8));
            rbp14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) - reinterpret_cast<unsigned char>(rdx));
            if (!r13_7) {
                addr_184f0_10:
                *r12_9 = reinterpret_cast<void**>(0);
            }
            addr_18490_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx12) - reinterpret_cast<unsigned char>(rcx11)) >= reinterpret_cast<signed char>(rdi8)) 
                goto addr_184b6_12;
            rcx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx11) + reinterpret_cast<unsigned char>(rdi8));
            rbx12 = rcx11;
            if (__intrinsic()) 
                goto addr_18504_14;
            if (reinterpret_cast<signed char>(rcx11) <= reinterpret_cast<signed char>(rsi10)) 
                goto addr_184ad_16;
            if (reinterpret_cast<signed char>(rsi10) >= reinterpret_cast<signed char>(0)) 
                goto addr_18504_14;
            addr_184ad_16:
            rcx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx11) * r8);
            rbp14 = rcx11;
            if (__intrinsic()) 
                goto addr_18504_14;
            addr_184b6_12:
            rsi10 = rbp14;
            rdi8 = r13_7;
            rax16 = fun_4af0(rdi8, rsi10, rdx, rcx11, r8, r9);
            if (rax16) 
                break;
            if (!r13_7) 
                goto addr_18504_14;
            if (!rbp14) 
                break;
            addr_18504_14:
            xalloc_die();
        }
        *r12_9 = rbx12;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp14) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp15) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp15) + 4) = 0;
            goto addr_184dd_9;
        } else {
            if (!r13_7) 
                goto addr_184f0_10;
            goto addr_18490_11;
        }
    }
}

void fun_18533(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_49b0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18563(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_49b0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_18593(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_49b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_185b3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_49b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_185d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_4a30;
    }
}

void fun_18613(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_4a30;
    }
}

void fun_18653(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_4670(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_4a30;
    }
}

void fun_18693(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_4860(rdi);
    rax4 = fun_4670(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_4a30;
    }
}

void fun_186d3() {
    __asm__("cli ");
    fun_4840();
    fun_4b70();
    fun_46e0();
}

uint64_t fun_18713(void** rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void* rax5;
    int64_t rax6;
    void*** rax7;
    void*** r12_8;
    uint64_t v9;
    void* rax10;
    void*** rax11;

    __asm__("cli ");
    rax5 = g28;
    rax6 = xstrtoumax(rdi, rdi);
    if (*reinterpret_cast<int32_t*>(&rax6)) {
        rax7 = fun_46f0();
        r12_8 = rax7;
        if (*reinterpret_cast<int32_t*>(&rax6) == 1) {
            addr_187b0_3:
            *r12_8 = reinterpret_cast<void**>(75);
        } else {
            if (*reinterpret_cast<int32_t*>(&rax6) == 3) {
                *rax7 = reinterpret_cast<void**>(0);
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
            if (rax10) {
                fun_4870();
            } else {
                return v9;
            }
        }
        rax11 = fun_46f0();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_187b0_3;
        *rax11 = reinterpret_cast<void**>(34);
    }
    quote(rdi, rdi);
    if (*r12_8 != 22) 
        goto addr_187cc_13;
    while (1) {
        addr_187cc_13:
        if (!1) {
        }
        fun_4b70();
    }
}

int64_t xnumtoumax(void** rdi, ...);

int64_t fun_18823(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rax7;

    __asm__("cli ");
    rax7 = xnumtoumax(rdi, rdi);
    return rax7;
}

int32_t fun_4bd0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void** ximemdup(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8);

void** fun_18853() {
    void** r14_1;
    void* rax2;
    void* v3;
    void** rbp4;
    void*** rax5;
    void*** rbx6;
    int32_t eax7;
    void** rax8;
    void** rax9;
    int64_t rax10;
    void** rax11;
    void* rax12;
    void** rax13;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r14_1) = 0;
    *reinterpret_cast<int32_t*>(&r14_1 + 4) = 0;
    rax2 = g28;
    v3 = rax2;
    rbp4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x80 + 16);
    rax5 = fun_46f0();
    rbx6 = rax5;
    while (1) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp4 + 100) - 1) = 0;
        *rbx6 = reinterpret_cast<void**>(0);
        eax7 = fun_4bd0(rbp4, 99, 1, -1, 1);
        if (!eax7) {
            rax8 = fun_4860(rbp4, rbp4);
            rax9 = rax8 + 1;
            if (reinterpret_cast<signed char>(99) > reinterpret_cast<signed char>(rax9)) 
                goto addr_18940_4;
            *rbx6 = reinterpret_cast<void**>(0);
        }
        fun_4630(r14_1, r14_1);
        *reinterpret_cast<void***>(&rax10) = *rbx6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax10)) > reinterpret_cast<unsigned char>(36)) 
            break;
        if (static_cast<int1_t>(0xffffffefffbfeffe >> rax10)) 
            break;
        rax11 = xpalloc();
        rbp4 = rax11;
        r14_1 = rbp4;
    }
    *reinterpret_cast<int32_t*>(&r14_1) = 0;
    *reinterpret_cast<int32_t*>(&r14_1 + 4) = 0;
    addr_18916_10:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v3) - reinterpret_cast<uint64_t>(g28));
    if (rax12) {
        fun_4870();
    } else {
        return r14_1;
    }
    addr_18940_4:
    if (!r14_1) {
        rax13 = ximemdup(rbp4, rax9, 1, -1, 1);
        r14_1 = rax13;
        goto addr_18916_10;
    }
}

void fun_18963(uint32_t edi, int32_t esi) {
    uint32_t ebp3;

    __asm__("cli ");
    ebp3 = exit_failure;
    if (edi > 3) {
        while (1) {
            if (edi != 4) 
                goto 0x4d01;
            if (esi >= 0) {
                addr_189a9_4:
            } else {
                addr_189ff_5:
            }
            fun_4840();
            esi = 0;
            edi = ebp3;
            fun_4b70();
            fun_46e0();
        }
    } else {
        if (edi <= 1) {
            if (edi != 1) {
                goto 0x4d01;
            }
        }
        if (esi < 0) 
            goto addr_189ff_5; else 
            goto addr_189a9_4;
    }
}

uint64_t fun_4b90(void** rdi);

int64_t fun_18a33(void** rdi, void** rsi, uint32_t edx, uint64_t* rcx, void** r8) {
    uint64_t* v6;
    void* rax7;
    void* v8;
    uint16_t* rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void*** rax19;
    void*** r12_20;
    uint16_t** rax21;
    void** rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    void** rax26;
    int64_t rax27;
    void** rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    void** rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        rcx9 = reinterpret_cast<uint16_t*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_4900("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax", r8);
        do {
            fun_4870();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_18da4_6;
                    rbx10 = rbx10 * reinterpret_cast<int64_t>(rcx9);
                } while (!__intrinsic());
            }
            addr_18da4_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_18aed_12:
            *v6 = rbx10;
            addr_18af5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_46f0();
    *rax19 = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_4c70(rdi, rsi);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx9 + rdx23) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_18b2b_21;
    rsi = r15_14;
    rax24 = fun_4b90(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_48a0(r13_18, r13_18), r8 = r8, rax26 == 0))) {
            addr_18b2b_21:
            r12d11 = 4;
            goto addr_18af5_13;
        } else {
            addr_18b69_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = reinterpret_cast<int32_t>("v");
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_48a0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = reinterpret_cast<int32_t>("v"), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = reinterpret_cast<int32_t>("k");
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<uint16_t*>("k");
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x1edf0 + rbp33 * 4) + 0x1edf0;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_18b2b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_18aed_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_18aed_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_48a0(r13_18, r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_18b69_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_18af5_13;
}

struct s206 {
    int32_t f0;
    uint32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    signed char[8] pad32;
    uint32_t f20;
    signed char[12] pad48;
    int64_t f30;
};

uint64_t fun_19163(struct s206* rdi, int64_t rsi, uint64_t* rdx) {
    int64_t rcx4;
    struct s206* v5;
    int64_t v6;
    int64_t rsi7;
    uint64_t* v8;
    void* rax9;
    void* v10;
    int32_t v11;
    uint32_t v12;
    int64_t rdi13;
    int32_t v14;
    int64_t rcx15;
    uint32_t v16;
    int64_t rcx17;
    uint32_t edi18;
    int64_t r15_19;
    int64_t rdx20;
    int64_t rdx21;
    int64_t rdx22;
    uint64_t r12_23;
    int64_t r9_24;
    int64_t rsi25;
    uint64_t rax26;
    int64_t v27;
    int64_t r8_28;
    uint64_t v29;
    int64_t rax30;
    int32_t v31;
    int64_t v32;
    int64_t rcx33;
    uint64_t rax34;
    void* rsp35;
    uint64_t v36;
    uint64_t rbp37;
    struct s21* r13_38;
    uint64_t* r14_39;
    uint64_t v40;
    uint64_t v41;
    int32_t v42;
    uint32_t v43;
    struct s21* rax44;
    int64_t rbx45;
    int32_t v46;
    int64_t v47;
    int64_t rax48;
    int32_t v49;
    int64_t v50;
    int64_t rax51;
    int32_t v52;
    int64_t v53;
    int64_t rax54;
    int32_t v55;
    int64_t v56;
    int32_t v57;
    uint64_t rax58;
    uint64_t r10_59;
    int32_t v60;
    unsigned char dl61;
    uint32_t eax62;
    int32_t v63;
    unsigned char v64;
    uint32_t edi65;
    unsigned char v66;
    int32_t v67;
    int64_t rcx68;
    int32_t v69;
    uint64_t rbp70;
    int64_t r12_71;
    struct s21* v72;
    int64_t r14_73;
    uint64_t* v74;
    void* v75;
    int64_t v76;
    struct s21* v77;
    uint64_t* rax78;
    uint64_t rdx79;
    int64_t rax80;
    int64_t v81;
    void* rax82;
    uint64_t rax83;
    uint64_t v84;
    int32_t v85;
    struct s21* rax86;
    int32_t v87;
    int64_t rax88;
    int32_t v89;
    int64_t v90;
    int64_t rax91;
    int32_t v92;
    int64_t v93;
    int64_t rax94;
    int32_t v95;
    int64_t v96;
    int64_t rax97;
    int32_t v98;
    int64_t v99;
    int32_t v100;
    int64_t rdx101;
    int64_t rax102;
    void*** rax103;
    int32_t ebx104;
    int32_t r15d105;
    int32_t r13d106;
    int64_t rax107;
    void*** rax108;
    int32_t v109;

    __asm__("cli ");
    rcx4 = rdi->f10;
    v5 = rdi;
    v6 = rsi;
    rsi7 = rdi->fc;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    v11 = rdi->f0;
    v12 = rdi->f4;
    rdi13 = rcx4;
    v14 = rdi->f8;
    rcx15 = rcx4 * 0x2aaaaaab >> 33;
    v16 = rdi->f20;
    *reinterpret_cast<int32_t*>(&rcx17) = *reinterpret_cast<int32_t*>(&rcx15) - (*reinterpret_cast<int32_t*>(&rdi13) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    edi18 = *reinterpret_cast<int32_t*>(&rdi13) - (static_cast<uint32_t>(rcx17 + rcx17 * 2) << 2);
    r15_19 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rcx17) - (edi18 >> 31)) + static_cast<int64_t>(rdi->f14);
    *reinterpret_cast<uint32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&r15_19) & 3) && (*reinterpret_cast<uint32_t*>(&rdx20) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0, reinterpret_cast<uint64_t>(0x8f5c28f5c28f5c29 * r15_19 + 0x51eb851eb851eb8) <= 0x28f5c28f5c28f5c)) {
        rdx21 = (__intrinsic() + r15_19 >> 6) - (r15_19 >> 63);
        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<uint1_t>(rdx22 == 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r12_23) = 59;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_23) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_24) = 70;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
    rsi25 = rsi7 + reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(0x1eee0 + ((reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edi18) >> 31) & 12) + edi18 + (rdx20 + (rdx20 + rdx20 * 2) * 4)) * 2) - 1);
    rax26 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v11));
    v27 = rsi25;
    if (*reinterpret_cast<int32_t*>(&rax26) <= 59) {
        r12_23 = rax26;
    }
    if (*reinterpret_cast<int32_t*>(&r12_23) < 0) {
        r12_23 = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
    v29 = *v8;
    *reinterpret_cast<int32_t*>(&rax30) = -*reinterpret_cast<int32_t*>(&v29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    v31 = *reinterpret_cast<int32_t*>(&rax30);
    v32 = rax30;
    *reinterpret_cast<uint32_t*>(&rcx33) = v12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
    rax34 = ydhms_diff(r15_19, rsi25, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), 70, 0, 0, 0, *reinterpret_cast<int32_t*>(&v32));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
    v36 = rax34;
    rbp37 = rax34;
    r13_38 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rsp35) + 0xa0);
    r14_39 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x88);
    v40 = rax34;
    v41 = rax34;
    v42 = 6;
    v43 = 0;
    while (rax44 = ranged_convert(v6, r14_39, r13_38, rcx33, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax44) {
        *reinterpret_cast<int32_t*>(&rbx45) = v46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx45) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
        v47 = rbx45;
        *reinterpret_cast<int32_t*>(&rax48) = v49;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        v50 = rax48;
        *reinterpret_cast<int32_t*>(&rax51) = v52;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        v53 = rax51;
        *reinterpret_cast<int32_t*>(&rax54) = v55;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        v56 = rax54;
        *reinterpret_cast<int32_t*>(&r9_24) = v57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx33) = v12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
        rax58 = ydhms_diff(r15_19, v27, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v56), *reinterpret_cast<int32_t*>(&v53), *reinterpret_cast<int32_t*>(&v50), *reinterpret_cast<int32_t*>(&v47));
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        r10_59 = v40;
        if (!rax58) 
            goto addr_19500_10;
        if (rbp37 == r10_59 || v41 != r10_59) {
            addr_19360_12:
            --v42;
            if (!v42) 
                goto addr_19470_13;
        } else {
            if (v60 < 0) 
                goto addr_193c0_15;
            *reinterpret_cast<uint32_t*>(&rcx33) = v16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            dl61 = reinterpret_cast<uint1_t>(!!v60);
            if (*reinterpret_cast<int32_t*>(&rcx33) < reinterpret_cast<int32_t>(0)) 
                goto addr_194e8_17; else 
                goto addr_193b2_18;
        }
        v41 = rbp37;
        rbp37 = r10_59;
        v40 = rax58 + r10_59;
        eax62 = 0;
        *reinterpret_cast<unsigned char*>(&eax62) = reinterpret_cast<uint1_t>(!!v63);
        v43 = eax62;
        continue;
        addr_194e8_17:
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(dl61)) < reinterpret_cast<int32_t>(v43)) 
            goto addr_19360_12; else 
            goto addr_194f5_20;
        addr_193b2_18:
        *reinterpret_cast<unsigned char*>(&rcx33) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rcx33));
        if (*reinterpret_cast<unsigned char*>(&rcx33) == dl61) 
            goto addr_19360_12; else 
            goto addr_193b9_21;
    }
    goto addr_1947b_22;
    addr_19500_10:
    v64 = reinterpret_cast<uint1_t>(v16 == 0);
    edi65 = v64;
    v66 = reinterpret_cast<uint1_t>(v67 == 0);
    *reinterpret_cast<uint32_t*>(&rcx68) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx68) != *reinterpret_cast<signed char*>(&edi65) && !__intrinsic()) {
        v69 = *reinterpret_cast<int32_t*>(&r12_23);
        rbp70 = r10_59;
        r12_71 = v6;
        v72 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(rsp35) + 0xe0);
        *reinterpret_cast<int32_t*>(&r14_73) = 0x92c70;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
        v74 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x90);
        v75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) + 0x98);
        v76 = r15_19;
        v77 = r13_38;
        goto addr_19594_24;
    }
    addr_193c0_15:
    rax78 = v8;
    *rax78 = r10_59 - (v31 + v36);
    if (v11 == *reinterpret_cast<int32_t*>(&rbx45)) 
        goto addr_1942f_25;
    *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx45) == 60);
    *reinterpret_cast<int32_t*>(&rdx79) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx79) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx79) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v11 < 0) | reinterpret_cast<uint1_t>(v11 == 0));
    if (!__intrinsic()) {
        rax80 = reinterpret_cast<int64_t>(v6(reinterpret_cast<int64_t>(rsp35) + 0xe0, r13_38));
        r10_59 = v11 + ((rdx79 & reinterpret_cast<uint64_t>(rax78)) - r12_23) + r10_59;
        if (!rax80) {
            addr_1947b_22:
            r10_59 = 0xffffffffffffffff;
        } else {
            addr_1942f_25:
            __asm__("movdqa xmm0, [rsp+0xa0]");
            __asm__("movdqa xmm1, [rsp+0xb0]");
            __asm__("movdqa xmm2, [rsp+0xc0]");
            __asm__("movups [rcx], xmm0");
            v5->f30 = v81;
            __asm__("movups [rcx+0x10], xmm1");
            __asm__("movups [rcx+0x20], xmm2");
        }
        rax82 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
        if (rax82) {
            fun_4870();
        } else {
            return r10_59;
        }
    }
    addr_194f5_20:
    goto addr_193c0_15;
    addr_193b9_21:
    goto addr_193c0_15;
    addr_196f2_31:
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    r13_38 = v77;
    r10_59 = rax83 + v84;
    *reinterpret_cast<int32_t*>(&rbx45) = v85;
    goto addr_193c0_15;
    addr_1968b_32:
    goto addr_1947b_22;
    while (rax86 = ranged_convert(r12_71, v74, v72, rcx68, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax86) {
        if (v64 == static_cast<unsigned char>(reinterpret_cast<uint1_t>(v87 == 0)) || v87 < 0) {
            *reinterpret_cast<int32_t*>(&rax88) = v89;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
            v90 = rax88;
            *reinterpret_cast<int32_t*>(&rax91) = v92;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
            v93 = rax91;
            *reinterpret_cast<int32_t*>(&rax94) = v95;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            v96 = rax94;
            *reinterpret_cast<int32_t*>(&rax97) = v98;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            v99 = rax97;
            *reinterpret_cast<int32_t*>(&r9_24) = v100;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_28) = v69;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx68) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx101) = v14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
            rax83 = ydhms_diff(v76, v27, *reinterpret_cast<int32_t*>(&rdx101), *reinterpret_cast<uint32_t*>(&rcx68), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v99), *reinterpret_cast<int32_t*>(&v96), *reinterpret_cast<int32_t*>(&v93), *reinterpret_cast<int32_t*>(&v90));
            rax102 = reinterpret_cast<int64_t>(r12_71(v75, v77, rdx101, rcx68, r8_28, r9_24));
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
            if (rax102) 
                goto addr_196f2_31;
            rax103 = fun_46f0();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            if (*rax103 != 75) 
                goto addr_1968b_32;
        }
        while (1) {
            do {
                ebx104 = ebx104 + r15d105;
                if (r13d106 == 1) 
                    break;
                r13d106 = 1;
                v84 = ebx104 + rbp70;
            } while (__intrinsic());
            break;
            *reinterpret_cast<int32_t*>(&r14_73) = *reinterpret_cast<int32_t*>(&r14_73) + 0x92c70;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r14_73) == 0xdb04f20) 
                goto addr_19690_40;
            addr_19594_24:
            r15d105 = static_cast<int32_t>(r14_73 + r14_73);
            r13d106 = 2;
            ebx104 = -*reinterpret_cast<int32_t*>(&r14_73);
            v84 = ebx104 + rbp70;
            if (!__intrinsic()) 
                break;
        }
    }
    goto addr_1947b_22;
    addr_19690_40:
    r13_38 = v77;
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    rax107 = reinterpret_cast<int64_t>(v6(v72, r13_38));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax107) {
        addr_19470_13:
        rax108 = fun_46f0();
        *rax108 = reinterpret_cast<void**>(75);
        goto addr_1947b_22;
    } else {
        *reinterpret_cast<int32_t*>(&rbx45) = v109;
        r10_59 = rbp70 + static_cast<int64_t>(reinterpret_cast<int32_t>((v64 - v66) * reinterpret_cast<uint32_t>("V")));
        goto addr_193c0_15;
    }
}

void fun_19713(int64_t rdi, void** rsi, int64_t rdx) {
    __asm__("cli ");
    fun_4a50(rdi, rsi, rdx);
    goto 0x19160;
}

int64_t fun_4730();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_19743(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void*** rax5;
    void*** rax6;

    __asm__("cli ");
    rax2 = fun_4730();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_1979e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_46f0();
            *rax5 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_1979e_3;
            rax6 = fun_46f0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax6 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s207 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_4a60(struct s207* rdi);

int32_t fun_4ad0(struct s207* rdi);

int64_t fun_48f0(int64_t rdi, ...);

int32_t rpl_fflush(struct s207* rdi);

int64_t fun_4800(struct s207* rdi);

int64_t fun_197b3(struct s207* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_4a60(rdi);
    if (eax2 >= 0) {
        eax3 = fun_4ad0(rdi);
        if (!(eax3 && (eax4 = fun_4a60(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_48f0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_46f0();
            r12d9 = *rax8;
            rax10 = fun_4800(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_4800;
}

void rpl_fseeko(struct s207* rdi);

void fun_19843(struct s207* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_4ad0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_19893(struct s207* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_4a60(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_48f0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

uint64_t fun_19913(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

void** fun_19953() {
    void** rax1;

    __asm__("cli ");
    rax1 = fun_4ab0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*reinterpret_cast<void***>(rax1)) {
            rax1 = reinterpret_cast<void**>("ASCII");
        }
        return rax1;
    }
}

void fun_19993() {
    __asm__("cli ");
}

void fun_199a7() {
    __asm__("cli ");
    return;
}

void fun_4e5c() {
    eolbyte = 0;
    print_with_color = 0;
    goto 0x4e20;
}

void fun_52d8() {
    goto 0x4e20;
}

void fun_54a2() {
    void** rsi1;
    void** rax2;
    void** rdx3;
    void** rdx4;

    rax2 = xmalloc(16, rsi1);
    rdx3 = optarg;
    *reinterpret_cast<void***>(rax2) = rdx3;
    rdx4 = hide_patterns;
    hide_patterns = rax2;
    *reinterpret_cast<void***>(rax2 + 8) = rdx4;
    goto 0x4e20;
}

void fun_55bd() {
    void** rdi1;
    void** rcx2;
    void** r8_3;
    int32_t eax4;
    int32_t eax5;
    void** rax6;

    rdi1 = optarg;
    eax4 = human_options(rdi1, 0x26328, 0x26320, rcx2, r8_3);
    if (eax4) 
        goto 0x6767;
    eax5 = human_output_opts;
    file_human_output_opts = eax5;
    rax6 = output_block_size;
    file_output_block_size = rax6;
    goto 0x4e20;
}

void fun_55fe() {
    goto 0x4e20;
}

void fun_56dd() {
    numeric_ids = 1;
    goto 0x4e20;
}

void fun_5783() {
    immediate_dirs = 1;
    goto 0x4e20;
}

void fun_5903() {
    dired = 1;
    goto 0x4e20;
}

void fun_69ea() {
}

struct s208 {
    signed char[2] pad2;
    unsigned char f2;
};

struct s209 {
    signed char[3] pad3;
    unsigned char f3;
};

void fun_6a87() {
    int64_t r8_1;
    struct s208* rcx2;
    unsigned char* r9_3;
    struct s209* rcx4;
    int32_t r13d5;
    int32_t r13d6;

    *reinterpret_cast<uint32_t*>(&r8_1) = rcx2->f2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_1) + 4) = 0;
    r9_3 = &rcx4->f3;
    if (*reinterpret_cast<signed char*>(&r8_1) > 70) {
        while (1) {
            r13d5 = static_cast<int32_t>(r8_1 - 97);
            if (*reinterpret_cast<unsigned char*>(&r13d5) > 5) 
                goto 0x695b;
            while (*reinterpret_cast<uint32_t*>(&r8_1) = *r9_3, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_1) + 4) = 0, ++r9_3, *reinterpret_cast<signed char*>(&r8_1) <= 70) {
                addr_6aa0_5:
                if (*reinterpret_cast<signed char*>(&r8_1) <= 64) {
                    r13d6 = static_cast<int32_t>(r8_1 - 48);
                    if (*reinterpret_cast<unsigned char*>(&r13d6) > 9) 
                        goto 0x695b;
                }
            }
        }
    } else {
        goto addr_6aa0_5;
    }
}

void fun_6aea() {
    goto 0x69f3;
}

void fun_8748() {
    return;
}

void fun_8770() {
    return;
}

struct s210 {
    signed char[16] pad16;
    void** f10;
};

void fun_d158() {
    int64_t v1;
    int1_t zf2;
    void** rax3;
    void** rdx4;
    void** v5;
    void** rax6;
    struct s210* r14_7;
    void** rax8;
    void** rbp9;
    void** rsi10;
    void** rax11;
    void** r15_12;
    void* r12_13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    int1_t below_or_equal17;
    void** r13_18;
    void** rbx19;
    void** rdx20;
    void** rbp21;
    void** rdi22;
    void** rax23;
    void** rcx24;
    void** r12_25;
    void** rdi26;
    void** rax27;
    void** r12_28;
    void** r8_29;
    void** r9_30;
    void** rax31;
    int1_t cf32;
    void** rdi33;
    uint32_t edx34;
    void** rax35;
    int64_t v36;

    v1 = reinterpret_cast<int64_t>(__return_address());
    zf2 = line_length == 0;
    if (zf2) {
        goto 0xc530;
    } else {
        rax3 = calculate_columns(0);
        rdx4 = rax3 + reinterpret_cast<unsigned char>(rax3) * 2;
        v5 = rax3;
        rax6 = column_info;
        r14_7 = reinterpret_cast<struct s210*>(reinterpret_cast<uint64_t>(rax6 + reinterpret_cast<unsigned char>(rdx4) * 8) - 24);
        rax8 = sorted_file;
        rbp9 = *reinterpret_cast<void***>(rax8);
        rax11 = length_of_file_name_and_frills(rbp9, rsi10);
        r15_12 = rax11;
        r12_13 = *r14_7->f10;
        print_file_name_and_frills_isra_0(rbp9, 0, rdx4, rcx14, r8_15, r9_16, v1);
        below_or_equal17 = reinterpret_cast<unsigned char>(cwd_n_used) <= reinterpret_cast<unsigned char>(1);
        if (!below_or_equal17) {
            *reinterpret_cast<int32_t*>(&r13_18) = 0;
            *reinterpret_cast<int32_t*>(&r13_18 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbx19) = 1;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
            do {
                rdx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) % reinterpret_cast<unsigned char>(v5));
                rbp21 = rdx20;
                if (!rdx20) {
                    rdi22 = stdout;
                    *reinterpret_cast<uint32_t*>(&rdx20) = eolbyte;
                    *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                    rax23 = *reinterpret_cast<void***>(rdi22 + 40);
                    if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 48))) {
                        *reinterpret_cast<int32_t*>(&r13_18) = 0;
                        *reinterpret_cast<int32_t*>(&r13_18 + 4) = 0;
                        fun_48d0();
                    } else {
                        rcx24 = rax23 + 1;
                        *reinterpret_cast<int32_t*>(&r13_18) = 0;
                        *reinterpret_cast<int32_t*>(&r13_18 + 4) = 0;
                        *reinterpret_cast<void***>(rdi22 + 40) = rcx24;
                        *reinterpret_cast<void***>(rax23) = rdx20;
                    }
                } else {
                    r12_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<unsigned char>(r13_18));
                    rdi26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_18) + reinterpret_cast<unsigned char>(r15_12));
                    r13_18 = r12_25;
                    indent(rdi26, r12_25);
                }
                rax27 = sorted_file;
                r12_28 = *reinterpret_cast<void***>(rax27 + reinterpret_cast<unsigned char>(rbx19) * 8);
                ++rbx19;
                print_file_name_and_frills_isra_0(r12_28, r13_18, rdx20, rcx24, r8_29, r9_30, v1);
                rax31 = length_of_file_name_and_frills(r12_28, r13_18);
                cf32 = reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(cwd_n_used);
                r15_12 = rax31;
                r12_13 = r14_7->f10[reinterpret_cast<unsigned char>(rbp21)];
            } while (cf32);
        }
        rdi33 = stdout;
        edx34 = eolbyte;
        rax35 = *reinterpret_cast<void***>(rdi33 + 40);
        if (reinterpret_cast<unsigned char>(rax35) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi33 + 48))) {
            goto fun_48d0;
        } else {
            *reinterpret_cast<void***>(rdi33 + 40) = rax35 + 1;
            *reinterpret_cast<void***>(rax35) = *reinterpret_cast<void***>(&edx34);
            goto v36;
        }
    }
}

void fun_d250() {
}

void fun_d291() {
    void** v1;
    void** rax2;
    void** rdi3;
    void** rbx4;
    void** rdi5;
    void** tmp64_6;
    void** rsi7;
    void** rax8;
    void** rdx9;
    int1_t below_or_equal10;
    int1_t zf11;
    unsigned char al12;
    void** r13_13;
    void** rcx14;
    void** r8_15;
    void** r9_16;
    void** v17;
    void** r12_18;
    void** rcx19;
    void** r8_20;
    void** r9_21;
    void** v22;
    void** rbp23;
    void** rcx24;
    void** r8_25;
    void** r9_26;
    void** v27;

    v1 = reinterpret_cast<void**>(__return_address());
    while (1) {
        rax2 = sorted_file;
        rdi3 = *reinterpret_cast<void***>(rax2 + reinterpret_cast<unsigned char>(rbx4) * 8);
        print_long_format(rdi3);
        rdi5 = stdout;
        tmp64_6 = dired_pos + 1;
        dired_pos = tmp64_6;
        *reinterpret_cast<uint32_t*>(&rsi7) = eolbyte;
        *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
        rax8 = *reinterpret_cast<void***>(rdi5 + 40);
        if (reinterpret_cast<unsigned char>(rax8) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi5 + 48))) {
            fun_48d0();
        } else {
            rdx9 = rax8 + 1;
            *reinterpret_cast<void***>(rdi5 + 40) = rdx9;
            *reinterpret_cast<void***>(rax8) = rsi7;
        }
        ++rbx4;
        below_or_equal10 = reinterpret_cast<unsigned char>(cwd_n_used) <= reinterpret_cast<unsigned char>(rbx4);
        if (below_or_equal10) 
            goto 0xd47d;
        zf11 = print_with_color == 0;
        if (zf11) 
            continue;
        al12 = is_colored(4);
        if (!al12) 
            continue;
        put_indicator(r13_13, rsi7, rdx9, rcx14, r8_15, r9_16, v1, v17);
        put_indicator(r12_18, rsi7, rdx9, rcx19, r8_20, r9_21, v1, v22);
        put_indicator(rbp23, rsi7, rdx9, rcx24, r8_25, r9_26, v1, v27);
    }
}

void fun_d320() {
    int64_t v1;
    void** rbx2;
    int1_t zf3;
    void** rax4;
    void** rdi5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rdi10;
    uint32_t esi11;
    void** rax12;
    int1_t below_or_equal13;

    v1 = reinterpret_cast<int64_t>(__return_address());
    *reinterpret_cast<int32_t*>(&rbx2) = 0;
    *reinterpret_cast<int32_t*>(&rbx2 + 4) = 0;
    zf3 = cwd_n_used == 0;
    if (zf3) 
        goto 0xd47d;
    do {
        rax4 = sorted_file;
        rdi5 = *reinterpret_cast<void***>(rax4 + reinterpret_cast<unsigned char>(rbx2) * 8);
        print_file_name_and_frills_isra_0(rdi5, 0, rdx6, rcx7, r8_8, r9_9, v1);
        rdi10 = stdout;
        esi11 = eolbyte;
        rax12 = *reinterpret_cast<void***>(rdi10 + 40);
        if (reinterpret_cast<unsigned char>(rax12) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
            fun_48d0();
        } else {
            rdx6 = rax12 + 1;
            *reinterpret_cast<void***>(rdi10 + 40) = rdx6;
            *reinterpret_cast<void***>(rax12) = *reinterpret_cast<void***>(&esi11);
        }
        ++rbx2;
        below_or_equal13 = reinterpret_cast<unsigned char>(cwd_n_used) <= reinterpret_cast<unsigned char>(rbx2);
    } while (!below_or_equal13);
    goto 0xd47d;
}

struct s211 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_d380() {
    int64_t v1;
    int1_t zf2;
    void** rax3;
    void** v4;
    void** rcx5;
    void** rax6;
    struct s211* r15_7;
    void** rax8;
    void** rdx9;
    void* rax10;
    void** rax11;
    void** v12;
    void** r12_13;
    int64_t r13_14;
    void** r14_15;
    void** rax16;
    void** rdi17;
    void** rsi18;
    void** rax19;
    void* rbp20;
    void** r8_21;
    void** r9_22;
    int1_t cf23;
    void** rbp24;
    void** rdi25;
    void** rdi26;
    void** rax27;

    v1 = reinterpret_cast<int64_t>(__return_address());
    zf2 = line_length == 0;
    if (zf2) 
        goto 0xd490;
    rax3 = calculate_columns(1);
    v4 = reinterpret_cast<void**>(0);
    rcx5 = rax3;
    rax6 = column_info;
    r15_7 = reinterpret_cast<struct s211*>(reinterpret_cast<uint64_t>(rax6 + reinterpret_cast<uint64_t>(rax3 + reinterpret_cast<unsigned char>(rax3) * 2) * 8) - 24);
    rax8 = cwd_n_used;
    rdx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) % reinterpret_cast<unsigned char>(rcx5));
    rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) / reinterpret_cast<unsigned char>(rcx5));
    rax11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax10) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax10) < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx9) < reinterpret_cast<unsigned char>(1)))))));
    v12 = rax11;
    if (!rax11) 
        goto 0xd47d;
    while (1) {
        r12_13 = v4;
        *reinterpret_cast<int32_t*>(&r13_14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_14) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r14_15) = 0;
        *reinterpret_cast<int32_t*>(&r14_15 + 4) = 0;
        while (rax16 = sorted_file, rdi17 = *reinterpret_cast<void***>(rax16 + reinterpret_cast<unsigned char>(r12_13) * 8), rax19 = length_of_file_name_and_frills(rdi17, rsi18), rsi18 = r14_15, rbp20 = *reinterpret_cast<void**>(r15_7->f10 + r13_14), r13_14 = r13_14 + 8, print_file_name_and_frills_isra_0(rdi17, rsi18, rdx9, rcx5, r8_21, r9_22, v1), r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) + reinterpret_cast<unsigned char>(v12)), cf23 = reinterpret_cast<unsigned char>(r12_13) < reinterpret_cast<unsigned char>(cwd_n_used), cf23) {
            rbp24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp20) + reinterpret_cast<unsigned char>(r14_15));
            rdi25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(r14_15));
            rsi18 = rbp24;
            r14_15 = rbp24;
            indent(rdi25, rsi18);
        }
        rdi26 = stdout;
        *reinterpret_cast<uint32_t*>(&rdx9) = eolbyte;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        rax27 = *reinterpret_cast<void***>(rdi26 + 40);
        if (reinterpret_cast<unsigned char>(rax27) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi26 + 48))) {
            *reinterpret_cast<uint32_t*>(&rsi18) = reinterpret_cast<unsigned char>(rdx9);
            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
            fun_48d0();
        } else {
            rcx5 = rax27 + 1;
            *reinterpret_cast<void***>(rdi26 + 40) = rcx5;
            *reinterpret_cast<void***>(rax27) = rdx9;
        }
        ++v4;
        if (v12 == v4) 
            goto "???";
    }
}

void fun_131f0(int32_t edi) {
    if (edi != 79) {
        if (edi) 
            goto 0x13670;
    }
}

void fun_136b9(int32_t edi) {
    void* rsp2;
    int64_t rbp3;
    int32_t edx4;
    int32_t v5;
    int32_t r15d6;
    int32_t r15d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    void* rsi13;
    void* rdi14;
    signed char* rcx15;
    int64_t r8_16;
    signed char* rdi17;
    int32_t r10d18;
    void* v19;
    uint64_t r13_20;
    int64_t r14_21;
    signed char v22;
    void* rdx23;
    int64_t r14_24;
    int64_t* r14_25;
    int64_t v26;
    void* r14_27;
    void* rax28;
    void* r14_29;
    void* rdi30;
    uint64_t rax31;
    void* rax32;
    void* rcx33;
    int32_t* r14_34;
    int32_t v35;
    void* r14_36;
    uint32_t eax37;
    unsigned char v38;
    signed char* r14_39;
    uint32_t eax40;
    void* r14_41;
    uint32_t** rax42;
    void* rdx43;
    void* rdi44;
    uint32_t** rcx45;
    int64_t r8_46;
    uint32_t eax47;
    void* r14_48;
    int1_t cf49;
    void** r14_50;
    void* r14_51;
    uint64_t r13_52;
    int64_t r13_53;
    int32_t edx54;
    uint64_t v55;
    uint64_t rdx56;
    int64_t v57;
    int64_t rax58;
    int64_t rax59;
    int32_t r8d60;
    int32_t edx61;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0x13200;
    *reinterpret_cast<int32_t*>(&rbp3) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    edx4 = v5;
    if (reinterpret_cast<uint1_t>(r15d6 < 0) | reinterpret_cast<uint1_t>(r15d7 == 0)) {
    }
    while (1) {
        rax8 = edx4;
        if (*reinterpret_cast<int32_t*>(&rbp3) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx4 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&rbp3) == 1) 
                break;
            if (edx4 - (esi11 + esi11)) 
                goto addr_13727_8;
        }
        *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx4 = *reinterpret_cast<int32_t*>(&rax12) - (edx4 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    goto addr_13735_11;
    addr_13727_8:
    rsi13 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3)));
    rdi14 = rsi13;
    if (!*reinterpret_cast<int32_t*>(&rbp3)) {
        *reinterpret_cast<int32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    } else {
        addr_13735_11:
        rcx15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xb0);
        *reinterpret_cast<int32_t*>(&r8_16) = static_cast<int32_t>(rbp3 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        rdi17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xaf - r8_16);
        goto addr_13752_13;
    }
    addr_13781_14:
    if (!r10d18) {
    }
    if (reinterpret_cast<int64_t>(v19) - r13_20 <= reinterpret_cast<uint64_t>(rsi13)) 
        goto 0x130e0;
    if (r14_21) {
        if (!v22) {
            if (reinterpret_cast<uint64_t>(rsi13) >= 8) {
                rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_24 + 8) & 0xfffffffffffffff8);
                *r14_25 = v26;
                *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r14_27) + reinterpret_cast<uint64_t>(rsi13) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xa8);
                rax28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_29) - reinterpret_cast<uint64_t>(rdx23));
                rdi30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0 - reinterpret_cast<uint64_t>(rax28));
                rax31 = reinterpret_cast<uint64_t>(rax28) + reinterpret_cast<uint64_t>(rsi13) & 0xfffffffffffffff8;
                if (rax31 >= 8) {
                    rax32 = reinterpret_cast<void*>(rax31 & 0xfffffffffffffff8);
                    *reinterpret_cast<int32_t*>(&rcx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
                    do {
                        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdx23) + reinterpret_cast<uint64_t>(rcx33)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi30) + reinterpret_cast<uint64_t>(rcx33));
                        rcx33 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33) + 8);
                    } while (reinterpret_cast<uint64_t>(rcx33) < reinterpret_cast<uint64_t>(rax32));
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&rsi13) & 4) {
                    *r14_34 = v35;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(r14_36) + reinterpret_cast<uint64_t>(rsi13) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xac);
                } else {
                    if (rsi13 && (eax37 = v38, *r14_39 = *reinterpret_cast<signed char*>(&eax37), !!(*reinterpret_cast<unsigned char*>(&rsi13) & 2))) {
                        eax40 = *reinterpret_cast<uint16_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xae);
                        *reinterpret_cast<int16_t*>(reinterpret_cast<int64_t>(r14_41) + reinterpret_cast<uint64_t>(rsi13) - 2) = *reinterpret_cast<int16_t*>(&eax40);
                    }
                }
            }
        } else {
            if (rsi13) {
                rax42 = fun_4690();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
                rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi13) + 0xffffffffffffffff);
                rdi44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0);
                rsi13 = rsi13;
                rcx45 = rax42;
                do {
                    *reinterpret_cast<uint32_t*>(&r8_46) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi44) + reinterpret_cast<uint64_t>(rdx43));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
                    eax47 = (*rcx45)[r8_46];
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_48) + reinterpret_cast<uint64_t>(rdx43)) = *reinterpret_cast<signed char*>(&eax47);
                    cf49 = reinterpret_cast<uint64_t>(rdx43) < 1;
                    rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx43) - 1);
                } while (!cf49);
            }
        }
        r14_50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_51) + reinterpret_cast<uint64_t>(rsi13));
    }
    r13_52 = r13_53 + reinterpret_cast<uint64_t>(rsi13);
    if (edx54 = 9 - *reinterpret_cast<int32_t*>(&rbp3), edx54 < 0) {
        if (v55 != r13_52) 
            goto 0x130a8; else 
            goto "???";
    }
    rdx56 = reinterpret_cast<uint64_t>(static_cast<int64_t>(edx54));
    if (rdx56 >= v57 - r13_52) 
        goto 0x130e0;
    if (r14_50) 
        goto addr_13833_36;
    goto 0x130a8;
    addr_13833_36:
    if (!rdx56) 
        goto 0x130a8;
    if (1) 
        goto addr_14a49_39;
    if (!0) 
        goto addr_13857_41;
    addr_14a49_39:
    fun_4920(r14_50, r14_50);
    goto 0x130a8;
    addr_13857_41:
    fun_4920(r14_50, r14_50);
    goto 0x130a8;
    addr_13752_13:
    while (--rcx15, rax58 = rax8 * 0x66666667 >> 34, *reinterpret_cast<int32_t*>(&rax59) = *reinterpret_cast<int32_t*>(&rax58) - (edx4 >> 31), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0, r8d60 = static_cast<int32_t>(rax59 + rax59 * 4), edx61 = edx4 - (r8d60 + r8d60) + 48, *rcx15 = *reinterpret_cast<signed char*>(&edx61), edx4 = *reinterpret_cast<int32_t*>(&rax59), rcx15 != rdi17) {
        rax8 = *reinterpret_cast<int32_t*>(&rax59);
    }
    goto addr_13781_14;
}

void fun_13870(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13650;
}

void fun_138ad(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

void fun_138c4(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

void fun_138df(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

void fun_138fa(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

int64_t mktime_z(int64_t rdi, void* rsi);

void fun_13968() {
    int64_t v1;
    int64_t rax2;
    int64_t rsi3;
    int32_t* v4;
    int64_t rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0x130eb;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = (__intrinsic() >> 2) - (rcx5 >> 63);
            if (rsi3 < 0) {
            }
        } while (rcx5);
    }
}

void fun_13b55() {
    goto 0x13200;
}

void fun_13b73() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    signed char* r14_11;
    int32_t r15d12;
    signed char* r15_13;
    int64_t r14_14;
    int32_t r10d15;
    int32_t r10d16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x130e0;
        if (!r14_6) 
            goto addr_145e8_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x130e0;
        if (!r14_10) 
            goto addr_13be8_9; else 
            goto addr_13bae_10;
    }
    addr_13be0_11:
    *r14_11 = 9;
    addr_13be8_9:
    goto 0x130a8;
    addr_145e8_4:
    goto addr_13be8_9;
    addr_13bae_10:
    if (r15d12 > 1) {
        r15_13 = reinterpret_cast<signed char*>(r14_14 + (rdx7 - 1));
        if (r10d15 == 48 || r10d16 == 43) {
            r14_11 = r15_13;
            fun_4920(r14_17, r14_17);
            goto addr_13be0_11;
        } else {
            r14_11 = r15_13;
            fun_4920(r14_18, r14_18);
            goto addr_13be0_11;
        }
    }
}

struct s212 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_13c10(int32_t edi) {
    int64_t rcx2;
    struct s212* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r10d7;
    int32_t r10d8;
    int32_t v9;

    if (edi == 69) 
        goto 0x13661;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (r10d7) {
        if (r10d8 != 43) {
            addr_13c76_4:
            goto 0x13643;
        } else {
            addr_14802_5:
        }
        if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) 
            goto 0x1470c;
        goto 0x13643;
    } else {
        if (v9 == 43) 
            goto addr_14802_5; else 
            goto addr_13c76_4;
    }
}

void fun_13c88(int32_t edi) {
    uint32_t r8d2;
    unsigned char v3;
    int64_t rax4;
    int32_t v5;
    void** v6;
    void** r10d7;
    int64_t v8;
    int64_t v9;
    void** rax10;
    void** r10d11;
    void** r10d12;
    uint32_t r8d13;
    int32_t r15d14;
    void** v15;
    void** rdx16;
    int32_t r15d17;
    void** rax18;
    void** r15_19;
    void* v20;
    uint64_t r13_21;
    int64_t r14_22;
    void** v23;
    void* r14_24;
    int64_t v25;
    void** r14_26;
    void** r14_27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    void** v31;
    int64_t v32;

    if (edi) 
        goto 0x13200;
    r8d2 = v3;
    *reinterpret_cast<int32_t*>(&rax4) = v5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rax10 = __strftime_internal_isra_0(0, 0xffffffffffffffff, "%m/%d/%y", v6, *reinterpret_cast<unsigned char*>(&r8d2), r10d7, -1, v8, rax4, v9, __return_address());
    r10d11 = r10d12;
    r8d13 = r8d2;
    if (r10d11 == 45 || r15d14 < 0) {
        v15 = rax10;
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    } else {
        rdx16 = reinterpret_cast<void**>(static_cast<int64_t>(r15d17));
        rax18 = rdx16;
        if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(rdx16)) {
            rax18 = rax10;
        }
        v15 = rax18;
    }
    r15_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v20) - r13_21);
    if (reinterpret_cast<unsigned char>(r15_19) <= reinterpret_cast<unsigned char>(v15)) 
        goto 0x130e0;
    if (r14_22) {
        if (reinterpret_cast<unsigned char>(rdx16) > reinterpret_cast<unsigned char>(rax10)) {
            v23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_24) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx16) - reinterpret_cast<unsigned char>(rax10)));
            if (r10d11 == 48 || r10d11 == 43) {
                v25 = 0x1463e;
                fun_4920(r14_26, r14_26);
                r14_27 = v23;
                r8d13 = r8d13;
                r10d11 = r10d11;
            } else {
                v25 = 0x13d7a;
                fun_4920(r14_28, r14_28);
                r14_27 = v23;
                r10d11 = r10d11;
                r8d13 = r8d13;
            }
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        __strftime_internal_isra_0(r14_27, r15_19, "%m/%d/%y", v31, *reinterpret_cast<unsigned char*>(&r8d13), r10d11, -1, v32, rax29, v25, __return_address());
    }
    goto 0x130a8;
}

void fun_13dfb(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

void fun_13e19(int32_t edi) {
    if (edi == 79) 
        goto 0x13661;
}

void fun_13ea7() {
    goto 0x13890;
}

void fun_13f11(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13650;
}

void fun_13f45() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    int32_t r15d11;
    signed char* r15_12;
    int64_t r14_13;
    int32_t r10d14;
    int32_t r10d15;
    signed char* r14_16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x130e0;
        if (!r14_6) 
            goto addr_14605_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x130e0;
        if (!r14_10) 
            goto 0x13be8;
        if (r15d11 > 1) {
            r15_12 = reinterpret_cast<signed char*>(r14_13 + (rdx7 - 1));
            if (r10d14 == 48 || r10d15 == 43) {
                r14_16 = r15_12;
                fun_4920(r14_17, r14_17);
            } else {
                r14_16 = r15_12;
                fun_4920(r14_18, r14_18);
            }
        }
    }
    *r14_16 = 10;
    goto 0x13be8;
    addr_14605_4:
    goto 0x13be8;
}

void fun_13fdf(int32_t edi) {
    int32_t r10d2;
    int32_t r10d3;
    int32_t v4;

    if (edi == 69) 
        goto 0x13661;
    if (edi == 79) 
        goto 0x13200;
    if (r10d2) {
        if (r10d3 == 43) 
            goto "???";
        goto 0x13643;
    } else {
        if (v4 == 43) {
            goto 0x1474f;
        }
    }
}

void fun_14049() {
    uint32_t edi1;
    unsigned char v2;
    signed char r8b3;
    void** rdi4;
    void** r12_5;
    void** rax6;
    uint32_t r8d7;
    unsigned char r8b8;
    int32_t r10d9;
    int32_t r15d10;
    void** v11;
    void** r15_12;
    int32_t r15d13;
    void** rax14;
    void* v15;
    uint64_t r13_16;
    int64_t r14_17;
    void** r15_18;
    void* r14_19;
    void** r14_20;
    void** r14_21;
    void** r14_22;
    void** r15_23;
    uint32_t** rax24;
    uint32_t** rsi25;
    int64_t rdx26;
    void* r12_27;
    uint32_t eax28;
    int1_t cf29;
    void** r12_30;
    void** rcx31;
    void** r15_32;
    uint32_t** rax33;
    uint32_t** rsi34;
    int64_t rdx35;
    void* r12_36;
    uint32_t eax37;
    int1_t cf38;

    edi1 = v2;
    if (r8b3) {
        edi1 = 0;
    }
    rdi4 = r12_5;
    rax6 = fun_4860(rdi4);
    r8d7 = r8b8;
    if (r10d9 == 45 || r15d10 < 0) {
        v11 = rax6;
        *reinterpret_cast<int32_t*>(&r15_12) = 0;
        *reinterpret_cast<int32_t*>(&r15_12 + 4) = 0;
    } else {
        r15_12 = reinterpret_cast<void**>(static_cast<int64_t>(r15d13));
        rax14 = r15_12;
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(r15_12)) {
            rax14 = rax6;
        }
        v11 = rax14;
    }
    if (reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(v15) - r13_16) <= reinterpret_cast<unsigned char>(v11)) 
        goto 0x130e0;
    if (r14_17) {
        if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(r15_12)) {
            r15_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_19) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_12) - reinterpret_cast<unsigned char>(rax6)));
            if (r10d9 == 48 || r10d9 == 43) {
                rdi4 = r14_20;
                r14_21 = r15_18;
                fun_4920(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            } else {
                rdi4 = r14_22;
                r14_21 = r15_18;
                fun_4920(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            }
        }
        if (*reinterpret_cast<unsigned char*>(&r8d7)) {
            r15_23 = rax6 + 0xffffffffffffffff;
            if (rax6) {
                rax24 = fun_4c60(rdi4, rdi4);
                rsi25 = rax24;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx26) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_27) + reinterpret_cast<unsigned char>(r15_23));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
                    eax28 = (*rsi25)[rdx26];
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<unsigned char>(r15_23)) = *reinterpret_cast<signed char*>(&eax28);
                    cf29 = reinterpret_cast<unsigned char>(r15_23) < reinterpret_cast<unsigned char>(1);
                    --r15_23;
                } while (!cf29);
            }
        } else {
            if (!*reinterpret_cast<signed char*>(&edi1)) {
                fun_4a30(r14_21, r12_30, rax6, rcx31);
            } else {
                r15_32 = rax6 + 0xffffffffffffffff;
                if (rax6) {
                    rax33 = fun_4690();
                    rsi34 = rax33;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx35) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_36) + reinterpret_cast<unsigned char>(r15_32));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx35) + 4) = 0;
                        eax37 = (*rsi34)[rdx35];
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<unsigned char>(r15_32)) = *reinterpret_cast<signed char*>(&eax37);
                        cf38 = reinterpret_cast<unsigned char>(r15_32) < reinterpret_cast<unsigned char>(1);
                        --r15_32;
                    } while (!cf38);
                }
            }
        }
    }
    goto 0x130a8;
}

void fun_1419d(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13890;
}

void fun_141bb(int32_t edi) {
    int32_t r10d2;

    if (edi == 69) 
        goto 0x13200;
    if (!r10d2) {
    }
    goto 0x13890;
}

void fun_141d1(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13650;
}

void fun_14205(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13ef8;
}

struct s213 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s214 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_1421b() {
    uint32_t eax1;
    struct s213* rbx2;
    uint64_t r11_3;
    int64_t rbx4;
    struct s214* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r9_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&r11_3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++r11_3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + r11_3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0x13200;
    if (v5->f20 < 0) 
        goto 0x130a8;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r9_9 = eax8 * 0xffffffff88888889 >> 32;
    if (r11_3 == 2) {
        addr_148e6_10:
        goto 0x13650;
    } else {
        if (r11_3 > 2) {
            if (r11_3 != 3) 
                goto 0x13200;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_148e6_10;
        } else {
            if (!r11_3) {
                goto 0x13650;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r9_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0x13650;
    }
    goto 0x13650;
}

void fun_14309() {
    goto 0x1423c;
}

void fun_14335() {
    goto 0x13c9f;
}

void fun_150e5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    uint32_t ecx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    uint64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    void** rsi73;
    void* v74;
    void** r15_75;
    void** rdx76;
    void* v77;
    int64_t rax78;
    int32_t eax79;
    void* rdi80;
    uint32_t edx81;
    unsigned char v82;
    void* rdi83;
    void* v84;
    uint32_t esi85;
    uint32_t ebp86;
    void** rcx87;
    uint32_t eax88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    void* rdx94;
    void* rcx95;
    void* v96;
    uint16_t** rax97;
    uint1_t zf98;
    int32_t ecx99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    uint64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    uint64_t rax111;
    uint64_t rax112;
    uint32_t r12d113;
    uint32_t ecx114;
    void** v115;
    void* rdx116;
    void* rax117;
    void* v118;
    uint64_t rax119;
    int64_t v120;
    int64_t rax121;
    int64_t rax122;
    int64_t rax123;
    int64_t v124;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_4840();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_4840();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_4860(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_153e3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_153e3_22; else 
                            goto addr_157dd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_1589d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_15bf0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_153e0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_153e0_30; else 
                                goto addr_15c09_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_4860(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_15bf0_28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_4970(rdi39, rsi30, v28, rdi39, rsi30, v28);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_15bf0_28; else 
                            goto addr_1528c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_15d50_39:
                    ecx45 = 0x7d;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_15bd0_40:
                        if (r11_27 == 1) {
                            addr_1575d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_15d18_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = ecx45;
                                goto addr_15397_44;
                            }
                        } else {
                            goto addr_15be0_46;
                        }
                    } else {
                        addr_15d5f_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_1575d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        ecx45 = 0x7b;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_153e3_22:
                                if (v49 != 1) {
                                    addr_15939_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_4860(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_15984_54;
                                    }
                                } else {
                                    goto addr_153f0_56;
                                }
                            } else {
                                addr_15395_57:
                                ebp36 = 0;
                                goto addr_15397_44;
                            }
                        } else {
                            addr_15bc4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_15d5f_47; else 
                                goto addr_15bce_59;
                        }
                    } else {
                        ecx45 = 0x7e;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_1575d_41;
                        if (v49 == 1) 
                            goto addr_153f0_56; else 
                            goto addr_15939_52;
                    }
                }
                addr_15451_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_152e8_63:
                    if (!1 && (edx54 = ecx45, *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<signed char*>(&ecx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_1530d_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_15610_65;
                    } else {
                        addr_15479_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_15cc8_67;
                    }
                } else {
                    goto addr_15470_69;
                }
                addr_15321_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_1536c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&ecx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_15cc8_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_1536c_81;
                }
                addr_15470_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_1530d_64; else 
                    goto addr_15479_66;
                addr_15397_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_1544f_91;
                if (v22) 
                    goto addr_153af_93;
                addr_1544f_91:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_15451_62;
                addr_15984_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_1610b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_1617b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72) + 1);
                        rsi73 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx76) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_15f7f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    eax79 = fun_4c40();
                    if (!eax79) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_4c30(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi80 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx81 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx81) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx81) & reinterpret_cast<unsigned char>(v32));
                addr_15a7e_109:
                if (reinterpret_cast<uint64_t>(rdi80) <= 1) {
                    addr_1543c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx81)) {
                        edx81 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_15a88_112;
                    }
                } else {
                    addr_15a88_112:
                    v82 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi83 = v84;
                    esi85 = 0;
                    ebp86 = reinterpret_cast<unsigned char>(v22);
                    rcx87 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_15b59_114;
                }
                addr_15448_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_1544f_91;
                while (1) {
                    addr_15b59_114:
                    if (*reinterpret_cast<unsigned char*>(&edx81)) {
                        *reinterpret_cast<unsigned char*>(&esi85) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax88 = esi85;
                        if (*reinterpret_cast<signed char*>(&ebp86)) 
                            goto addr_16067_117;
                        eax89 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax89) & *reinterpret_cast<unsigned char*>(&esi85));
                        if (*reinterpret_cast<unsigned char*>(&eax89)) 
                            goto addr_15ac6_119;
                    } else {
                        eax57 = (esi85 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx87)) 
                            goto addr_16075_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_15b47_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_15b47_128;
                        }
                    }
                    addr_15af5_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax90 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) >> 6);
                        eax91 = eax90 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax91);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax92 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax92) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax92) >> 3);
                        eax93 = (eax92 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax93);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx87)) 
                        break;
                    esi85 = edx81;
                    addr_15b47_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi83) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_15ac6_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax89;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_15af5_134;
                }
                ebp36 = v82;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_1536c_81;
                addr_16075_125:
                ebp36 = v82;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_15cc8_67;
                addr_1610b_96:
                rdi80 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx81 = reinterpret_cast<unsigned char>(v32);
                goto addr_15a7e_109;
                addr_1617b_98:
                r11_27 = v65;
                rdi80 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx94 = rdi80;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx95 = v96;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx95) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx94 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx94) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx94));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi80 = rdx94;
                }
                edx81 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_15a7e_109;
                addr_153f0_56:
                rax97 = fun_4c70(rdi39, rsi30, rdi39, rsi30);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi80) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi80) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf98 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax97 + reinterpret_cast<unsigned char>(rax24)) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf98);
                *reinterpret_cast<unsigned char*>(&edx81) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf98) & reinterpret_cast<unsigned char>(v32));
                goto addr_1543c_110;
                addr_15bce_59:
                goto addr_15bd0_40;
                addr_1589d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_153e3_22;
                ecx99 = static_cast<int32_t>(rbx43 - 65);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx99));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_15448_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_15395_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_153e3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_158e2_160;
                if (!v22) 
                    goto addr_15cb7_162; else 
                    goto addr_15ec3_163;
                addr_158e2_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_15cb7_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    ecx45 = 92;
                    goto addr_15cc8_67;
                } else {
                    ecx45 = 92;
                    if (!v32) 
                        goto addr_1578b_166;
                }
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                ebp36 = 0;
                addr_155f3_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_15321_70; else 
                    goto addr_15607_169;
                addr_1578b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_152e8_63;
                goto addr_15470_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        ecx45 = 0x7d;
                        r8d42 = 0;
                        goto addr_15bc4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_15cff_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_153e0_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_152d8_178; else 
                        goto addr_15c82_179;
                }
                ecx45 = 0x7b;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_15bc4_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_153e3_22;
                }
                addr_15cff_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_153e0_30:
                    r8d42 = 0;
                    goto addr_153e3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        ecx45 = 0x7e;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_15451_62;
                    } else {
                        ecx45 = 0x7e;
                        goto addr_15d18_42;
                    }
                }
                addr_152d8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_152e8_63;
                addr_15c82_179:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_15be0_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_15451_62;
                } else {
                    addr_15c92_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_153e3_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_16442_188;
                if (v28) 
                    goto addr_15cb7_162;
                addr_16442_188:
                ecx45 = 92;
                ebp36 = 0;
                goto addr_155f3_168;
                addr_1528c_37:
                if (v22) 
                    goto addr_16283_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_152a3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_15d50_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_15ddb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_153e3_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_152d8_178; else 
                        goto addr_15db7_199;
                }
                ecx45 = 0x7b;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_15bc4_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_153e3_22;
                }
                addr_15ddb_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_153e3_22;
                }
                addr_15db7_199:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_15be0_46;
                goto addr_15c92_186;
                addr_152a3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_153e3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_153e3_22; else 
                    goto addr_152b4_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_15 == 0)) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_1638e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_16214_210;
            if (1) 
                goto addr_16212_212;
            if (!v29) 
                goto addr_15e4e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_4850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_16381_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_15610_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_153cb_219; else 
            goto addr_1562a_220;
        addr_153af_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax88 = reinterpret_cast<unsigned char>(v32);
        addr_153c3_221:
        if (*reinterpret_cast<signed char*>(&eax88)) 
            goto addr_1562a_220; else 
            goto addr_153cb_219;
        addr_15f7f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_1562a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_4850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_15f9d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_4850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_16410_225:
        v31 = r13_34;
        addr_15e76_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_16067_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_153c3_221;
        addr_15ec3_163:
        eax88 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_153c3_221;
        addr_15607_169:
        goto addr_15610_65;
        addr_1638e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_1562a_220;
        goto addr_15f9d_222;
        addr_16214_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (ecx114 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), !!*reinterpret_cast<signed char*>(&ecx114)))) {
            rsi30 = v115;
            rdx116 = r15_15;
            rax117 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx116)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<uint64_t>(rdx116)) = *reinterpret_cast<signed char*>(&ecx114);
                }
                rdx116 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx116) + 1);
                ecx114 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax117) + reinterpret_cast<uint64_t>(rdx116));
            } while (*reinterpret_cast<signed char*>(&ecx114));
            r15_15 = rdx116;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v118) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax119 = v120 - reinterpret_cast<uint64_t>(g28);
        if (!rax119) 
            goto addr_1626e_236;
        fun_4870();
        rsp25 = rsp25 - 8 + 8;
        goto addr_16410_225;
        addr_16212_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_16214_210;
        addr_15e4e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_16214_210;
        } else {
            goto addr_15e76_226;
        }
        addr_16381_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_157dd_24:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1e7ac + rax121 * 4) + 0x1e7ac;
    addr_15c09_32:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1e8ac + rax122 * 4) + 0x1e8ac;
    addr_16283_190:
    addr_153cb_219:
    goto 0x150b0;
    addr_152b4_206:
    *reinterpret_cast<uint32_t*>(&rax123) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax123) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1e6ac + rax123 * 4) + 0x1e6ac;
    addr_1626e_236:
    goto v124;
}

void fun_152d0() {
}

void fun_15488() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x15182;
}

void fun_154e1() {
    goto 0x15182;
}

void fun_155ce() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x15451;
    }
    if (v2) 
        goto 0x15ec3;
    if (!r10_3) 
        goto addr_1602e_5;
    if (!v4) 
        goto addr_15efe_7;
    addr_1602e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_15efe_7:
    goto 0x15304;
}

void fun_155ec() {
}

void fun_15697() {
    signed char v1;

    if (v1) {
        goto 0x1561f;
    } else {
        goto 0x1535a;
    }
}

void fun_156b1() {
    signed char v1;

    if (!v1) 
        goto 0x156aa; else 
        goto "???";
}

void fun_156d8() {
    goto 0x155f3;
}

void fun_15758() {
}

void fun_15770() {
}

void fun_1579f() {
    goto 0x155f3;
}

void fun_157f1() {
    goto 0x15780;
}

void fun_15820() {
    goto 0x15780;
}

void fun_15853() {
    goto 0x15780;
}

void fun_15c20() {
    goto 0x152d8;
}

void fun_15f1e() {
    signed char v1;

    if (v1) 
        goto 0x15ec3;
    goto 0x15304;
}

void fun_15fc5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x15304;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x152e8;
        goto 0x15304;
    }
}

void fun_163e2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x15650;
    } else {
        goto 0x15182;
    }
}

void fun_17b38() {
    fun_4840();
}

void fun_18bdc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x18beb;
}

void fun_18cac() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x18cb9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x18beb;
}

void fun_18cd0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_18cfc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x18beb;
}

void fun_18d1d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x18beb;
}

void fun_18d41() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x18beb;
}

void fun_18d65() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x18cf4;
}

void fun_18d89() {
}

void fun_18da9() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x18cf4;
}

void fun_18dc5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x18cf4;
}

void fun_5d83() {
    goto 0x4e20;
}

int64_t argmatch_die = 0xe690;

int64_t __xargmatch_internal(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_52e9() {
    void** rsi1;
    int64_t r9_2;
    int64_t rax3;

    rsi1 = optarg;
    r9_2 = argmatch_die;
    rax3 = __xargmatch_internal("--time", rsi1, 0x24260, 0x1a710, 4, r9_2);
    time_type = *reinterpret_cast<uint32_t*>(0x1a710 + rax3 * 4);
    goto 0x4e20;
}

void fun_54cd() {
    directories_first = 1;
    goto 0x4e20;
}

void fun_560b() {
    time_type = 2;
    goto 0x4e20;
}

void fun_56f0() {
    goto 0x4e20;
}

void fun_578f() {
    time_type = 1;
    goto 0x4e20;
}

void fun_590f() {
    goto 0x4e20;
}

void fun_6b62() {
    goto 0x69f0;
}

void fun_6af7() {
    goto 0x69f3;
}

int64_t fun_8750() {
    int64_t rdx1;
    int64_t rax2;
    uint32_t eax3;

    *reinterpret_cast<uint32_t*>(&rdx1) = time_type;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rdx1) > 3) 
        goto 0x4c90;
    *reinterpret_cast<uint32_t*>(&rax2) = eax3 | *reinterpret_cast<uint32_t*>("@" + rdx1 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

void fun_d270() {
    int1_t zf1;

    zf1 = cwd_n_used == 0;
    if (!zf1) 
        goto 0xd2e8;
    goto 0xd47d;
}

void fun_13368(int32_t edi) {
    signed char r8b2;

    if (r8b2) {
    }
    if (edi == 69) 
        goto 0x13200; else 
        goto "???";
}

void fun_13685(int32_t edi) {
    signed char r8b2;

    if (edi) 
        goto 0x13200;
    if (r8b2) {
    }
    goto 0x133a2;
}

struct s215 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_134bf(int32_t edi, signed char sil) {
    int64_t rcx3;
    struct s215* v4;
    uint32_t edx5;
    int64_t rax6;
    int32_t r9d7;
    uint64_t rax8;
    int64_t rax9;
    int64_t r8_10;
    int64_t rax11;
    int32_t r9d12;
    int64_t rax13;
    uint32_t eax14;
    uint32_t eax15;
    uint32_t r11d16;
    uint32_t edx17;
    int64_t r11_18;
    uint64_t rax19;
    int64_t rax20;
    int64_t r9_21;
    int32_t eax22;
    int32_t r10d23;
    int32_t v24;
    int64_t rdx25;
    int32_t r8d26;
    int64_t rdx27;
    int32_t r10d28;
    int32_t r10d29;
    int32_t v30;

    if (edi == 69) 
        goto 0x13200;
    *reinterpret_cast<int32_t*>(&rcx3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    r9d7 = static_cast<int32_t>(rcx3 + rax6 - 100);
    rax8 = reinterpret_cast<int32_t>(edx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rax9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax8) + edx5) >> 2) - (reinterpret_cast<int32_t>(edx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_10) = static_cast<int32_t>(rax9 * 8) - *reinterpret_cast<int32_t*>(&rax9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = v4->f1c - edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    if (static_cast<int32_t>(rax11 + r8_10 + 3) < 0) {
        r9d12 = r9d7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&r9d12) & 3) && reinterpret_cast<uint32_t>(r9d12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        eax14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&r9d7) & 3) && (eax14 = 0x16e, reinterpret_cast<uint32_t>(r9d7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            eax15 = reinterpret_cast<uint32_t>(r9d7 / 0x190);
            eax14 = eax15 - (eax15 + reinterpret_cast<uint1_t>(eax15 < eax15 + reinterpret_cast<uint1_t>(!!(r9d7 % 0x190)))) + 0x16e;
        }
        r11d16 = v4->f1c - eax14;
        edx17 = r11d16 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r11_18) = r11d16 - edx17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_18) + 4) = 0;
        rax19 = reinterpret_cast<int32_t>(edx17) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax20) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax19) + edx17) >> 2) - (reinterpret_cast<int32_t>(edx17) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r9_21) = static_cast<int32_t>(rax20 * 8) - *reinterpret_cast<int32_t*>(&rax20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_21) + 4) = 0;
        eax22 = static_cast<int32_t>(r11_18 + r9_21 + 3);
        if (eax22 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax22 >> 31) + 1;
    }
    if (sil == 71) {
        if (r10d23) 
            goto 0x14738;
        if (v24 == 43) 
            goto 0x149d4;
        goto 0x13643;
    } else {
        if (sil != 0x67) {
            goto 0x13890;
        } else {
            rdx25 = *reinterpret_cast<int32_t*>(&rcx3) * 0x51eb851f >> 37;
            r8d26 = *reinterpret_cast<int32_t*>(&rcx3) - (*reinterpret_cast<int32_t*>(&rdx25) - (*reinterpret_cast<int32_t*>(&rcx3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
            rdx27 = r8d26 * 0x51eb851f >> 37;
            if (r8d26 - (*reinterpret_cast<int32_t*>(&rdx27) - (r8d26 >> 31)) * 100 < 0) {
                if (*reinterpret_cast<int32_t*>(&rcx3) >= 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
                    goto 0x14add;
                if (!r10d28) 
                    goto 0x14188; else 
                    goto "???";
            } else {
                if (r10d29) 
                    goto 0x146f5;
                if (v30 == 43) 
                    goto 0x146ff; else 
                    goto "???";
            }
        }
    }
}

void fun_13b5e() {
    int64_t rbx1;
    int64_t rbp2;
    uint64_t rax3;
    int64_t v4;
    uint64_t r13_5;
    int32_t r10d6;
    int32_t r15d7;
    int64_t r14_8;
    uint64_t rdx9;
    int32_t r15d10;
    uint64_t rcx11;
    int64_t r14_12;
    int32_t r15d13;
    uint64_t r15_14;
    int64_t r14_15;
    int32_t r10d16;
    int32_t r10d17;
    uint64_t r14_18;
    void** r14_19;
    void** r14_20;
    uint32_t eax21;
    unsigned char* rbx22;

    if (rbx1 - 1 != rbp2) {
        goto 0x13200;
    }
    rax3 = v4 - r13_5;
    if (r10d6 == 45 || r15d7 < 0) {
        if (rax3 <= 1) 
            goto 0x130e0;
        if (!r14_8) 
            goto addr_1489f_6;
    } else {
        rdx9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d10));
        *reinterpret_cast<int32_t*>(&rcx11) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (rdx9) {
            rcx11 = rdx9;
        }
        if (rcx11 >= rax3) 
            goto 0x130e0;
        if (!r14_12) 
            goto 0x13be8;
        if (r15d13 > 1) {
            r15_14 = r14_15 + (rdx9 - 1);
            if (r10d16 == 48 || r10d17 == 43) {
                r14_18 = r15_14;
                fun_4920(r14_19, r14_19);
            } else {
                r14_18 = r15_14;
                fun_4920(r14_20, r14_20);
            }
        }
    }
    eax21 = *rbx22;
    *reinterpret_cast<signed char*>(r14_18 + 1 - 1) = *reinterpret_cast<signed char*>(&eax21);
    goto 0x13be8;
    addr_1489f_6:
    goto 0x13be8;
}

void fun_13bf0(int32_t edi) {
    signed char r8b2;

    if (edi == 69) 
        goto 0x13200;
    if (r8b2) {
    }
    goto 0x13383;
}

void fun_13dca(int32_t edi, int64_t rsi) {
    int32_t r15d3;
    int32_t r10d4;
    int64_t r15_5;
    uint32_t r8d6;
    unsigned char v7;
    int64_t rax8;
    int32_t v9;
    void** v10;
    int64_t v11;
    int64_t rax12;

    if (edi) 
        goto 0x13200;
    if (r15d3 >= 0 || r10d4) {
        if (static_cast<int32_t>(r15_5 - 6) >= 0) {
        }
        goto 0x13c9f;
    } else {
        r8d6 = v7;
        *reinterpret_cast<int32_t*>(&rax8) = v9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        __strftime_internal_isra_0(0, 0xffffffffffffffff, "%Y-%m-%d", v10, *reinterpret_cast<unsigned char*>(&r8d6), 43, 4, v11, rax8, rax12, __return_address());
        goto 0x13d1d;
    }
}

void fun_13eeb(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
}

void fun_13fc3(int32_t edi) {
    if (edi == 69) 
        goto 0x13200;
    goto 0x13943;
}

struct s216 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_14140(int32_t edi) {
    int64_t rdx2;
    struct s216* v3;
    int64_t rax4;
    int64_t rdx5;
    int32_t r10d6;

    if (edi == 69) 
        goto 0x13661;
    rdx2 = v3->f14;
    rax4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rax4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rax4) >> 31)) * 100 < 0 && *reinterpret_cast<int32_t*>(&rax4) <= 0xfffff893) {
    }
    if (r10d6) 
        goto 0x146f5; else 
        goto "???";
}

void fun_14311() {
    int1_t zf1;
    signed char r8b2;

    zf1 = r8b2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0x13383;
}

void fun_14349() {
    goto 0x14313;
}

void fun_1550e() {
    goto 0x15182;
}

void fun_156e4() {
    goto 0x1569c;
}

void fun_157ab() {
    goto 0x152d8;
}

void fun_157fd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x15780;
    goto 0x153af;
}

void fun_1582f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x1578b;
        goto 0x151b0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x1562a;
        goto 0x153cb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x15fc8;
    if (r10_8 > r15_9) 
        goto addr_15715_9;
    addr_1571a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x15fd3;
    goto 0x15304;
    addr_15715_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_1571a_10;
}

void fun_15862() {
    goto 0x15397;
}

void fun_15c30() {
    goto 0x15397;
}

void fun_163cf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x154ec;
    } else {
        goto 0x15650;
    }
}

void fun_17bf0() {
}

void fun_18c7f() {
    if (__intrinsic()) 
        goto 0x18cb9; else 
        goto "???";
}

void fun_5d8f() {
    goto 0x4e20;
}

void fun_5334() {
    int64_t r9_1;
    void** rsi2;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    __xargmatch_internal("--sort", rsi2, 0x242a0, 0x1a730, 4, r9_1);
    goto 0x4e20;
}

void fun_54d9() {
    goto 0x4e20;
}

void fun_561a() {
    goto 0x4e20;
}

void fun_56fc() {
    print_inode = 1;
    goto 0x4e20;
}

void fun_579e() {
    goto 0x4e20;
}

void fun_591b() {
    void** rsi1;
    void** rsi2;

    add_ignore_pattern("*~", rsi1);
    add_ignore_pattern(".*~", rsi2);
    goto 0x4e20;
}

void fun_6b6c() {
    goto 0x69f0;
}

void fun_6a40(int64_t rdi, int64_t rsi) {
    unsigned char* rcx3;
    unsigned char* r13_4;
    int64_t rdx5;
    int32_t r9d6;
    int64_t r8_7;
    int32_t r9d8;
    int32_t r9d9;

    rcx3 = r13_4;
    *reinterpret_cast<int32_t*>(&rdx5) = r9d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_7) = *rcx3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0;
    r9d8 = static_cast<int32_t>(r8_7 - 48);
    if (*reinterpret_cast<unsigned char*>(&r9d8) > 7) 
        goto 0x695b;
    do {
        ++rcx3;
        *reinterpret_cast<int32_t*>(&rdx5) = static_cast<int32_t>(r8_7 + rdx5 * 8 - 48);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r8_7) = *rcx3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0;
        r9d9 = static_cast<int32_t>(r8_7 - 48);
    } while (*reinterpret_cast<unsigned char*>(&r9d9) <= 7);
    *reinterpret_cast<signed char*>(rsi - 1) = *reinterpret_cast<signed char*>(&rdx5);
    goto 0x6966;
}

void fun_6b04() {
    goto 0x69f3;
}

void fun_14350() {
    goto 0x13c9f;
}

void fun_1586c() {
    goto 0x15807;
}

void fun_15c3a() {
    goto 0x1575d;
}

void fun_17c50() {
    fun_4840();
    goto fun_4c10;
}

void fun_537a() {
    human_output_opts = 0x90;
    file_human_output_opts = 0x90;
    output_block_size = reinterpret_cast<void**>(1);
    file_output_block_size = reinterpret_cast<void**>(1);
    goto 0x4e20;
}

void fun_54f1() {
    int64_t r9_1;
    void** rsi2;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    __xargmatch_internal("--format", rsi2, 0x242e0, 0x1a750, 4, r9_1);
    goto 0x4e20;
}

void fun_5627() {
    print_block_size = 1;
    goto 0x4e20;
}

void fun_5708() {
    human_output_opts = 0xb0;
    file_human_output_opts = 0xb0;
    output_block_size = reinterpret_cast<void**>(1);
    file_output_block_size = reinterpret_cast<void**>(1);
    goto 0x4e20;
}

void fun_57ab() {
    ignore_mode = 2;
    goto 0x4e20;
}

void fun_5938() {
    ignore_mode = 1;
    goto 0x4e20;
}

void fun_6b11() {
    goto 0x69f3;
}

void fun_1553d() {
    goto 0x15182;
}

void fun_15878() {
    goto 0x15807;
}

void fun_15c47() {
    goto 0x157ae;
}

void fun_17c90() {
    fun_4840();
    goto fun_4c10;
}

void fun_53a9() {
    void** rsi1;
    int64_t r9_2;

    rsi1 = optarg;
    r9_2 = argmatch_die;
    __xargmatch_internal("--quoting-style", rsi1, 0x24a40, 0x1e9c0, 4, r9_2);
    goto 0x4e20;
}

void fun_5538() {
    indicator_style = 2;
    goto 0x4e20;
}

void fun_5633() {
    sort_reverse = 1;
    goto 0x4e20;
}

void fun_5737() {
    print_owner = 0;
    goto 0x4e20;
}

void fun_57ba() {
    print_scontext = 1;
    goto 0x4e20;
}

void fun_5947() {
    goto 0x4e20;
}

void fun_6b1e() {
    goto 0x69f3;
}

void fun_1556a() {
    goto 0x15182;
}

void fun_15884() {
    goto 0x15780;
}

void fun_17cd0() {
    fun_4840();
    goto fun_4c10;
}

void fun_53ef(int64_t rdi) {
    int64_t r9_2;
    void** rsi3;
    int64_t rax4;

    r9_2 = argmatch_die;
    rsi3 = optarg;
    rax4 = __xargmatch_internal("--indicator-style", rsi3, 0x249a0, 0x1a770, 4, r9_2);
    indicator_style = *reinterpret_cast<uint32_t*>(0x1a770 + rax4 * 4);
    goto 0x4e20;
}

void fun_5547() {
    dereference = 3;
    goto 0x4e20;
}

void fun_563f() {
    goto 0x4e20;
}

void fun_574a() {
    int64_t v1;

    v1 = reinterpret_cast<int64_t>(__return_address());
    ignore_mode = 2;
    if (!*reinterpret_cast<int32_t*>(&v1)) {
    }
    print_with_color = 0;
    print_hyperlink = 0;
    print_block_size = 0;
    goto 0x4e20;
}

void fun_57c6() {
    goto 0x4e20;
}

int64_t Version = 0x1e218;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9);

void fun_5958() {
    int64_t rax1;
    int64_t rcx2;
    int64_t rsi3;
    int1_t zf4;
    void** rdi5;

    *reinterpret_cast<int32_t*>(&rax1) = ls_mode;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax1) + 4) = 0;
    rcx2 = Version;
    rsi3 = reinterpret_cast<int64_t>("ls");
    if (*reinterpret_cast<int32_t*>(&rax1) != 1 && (zf4 = *reinterpret_cast<int32_t*>(&rax1) == 2, rsi3 = reinterpret_cast<int64_t>("dir"), rax1 = reinterpret_cast<int64_t>("vdir"), !zf4)) {
        rsi3 = reinterpret_cast<int64_t>("vdir");
    }
    rdi5 = stdout;
    version_etc(rdi5, rsi3, "GNU coreutils", rcx2, "Richard M. Stallman", "David MacKenzie", 0, rax1, __return_address());
    fun_4bf0();
    print_hyperlink = 1;
    print_hyperlink = reinterpret_cast<unsigned char>(print_hyperlink & 1);
    goto 0x4e20;
}

void fun_6b2b() {
    goto 0x69f3;
}

void fun_1558c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x15f20;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x15451;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x15451;
    }
    if (v11) 
        goto 0x16283;
    if (r10_12 > r15_13) 
        goto addr_162d3_8;
    addr_162d8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x16011;
    addr_162d3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_162d8_9;
}

struct s217 {
    signed char[24] pad24;
    void** f18;
};

struct s218 {
    signed char[16] pad16;
    void** f10;
};

struct s219 {
    signed char[8] pad8;
    void** f8;
};

void fun_17d20() {
    void** r15_1;
    struct s217* rbx2;
    void** r14_3;
    struct s218* rbx4;
    void** r13_5;
    struct s219* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_4840();
    fun_4c10(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x17d42, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5439() {
    void** rsi1;
    int64_t r9_2;
    int64_t rax3;

    rsi1 = optarg;
    if (!rsi1) 
        goto 0x59b3;
    r9_2 = argmatch_die;
    rax3 = __xargmatch_internal("--hyperlink", rsi1, 0x24200, 0x1a6e0, 4, r9_2);
    if (*reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) == 1) 
        goto 0x59b3;
    if (*reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) != 2) 
        goto 0x59b8;
    stdout_isatty("--hyperlink");
    goto 0x59b8;
}

void fun_5556() {
    void** rsi1;
    int64_t r9_2;
    int64_t rax3;
    uint32_t eax4;
    unsigned char al5;

    rsi1 = optarg;
    if (!rsi1 || (r9_2 = argmatch_die, rax3 = __xargmatch_internal("--color", rsi1, 0x24200, 0x1a6e0, 4, r9_2), *reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) == 1)) {
        eax4 = 1;
    } else {
        eax4 = 0;
        if (*reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) == 2) {
            al5 = stdout_isatty("--color", "--color");
            eax4 = al5;
        }
    }
    print_with_color = *reinterpret_cast<unsigned char*>(&eax4);
    print_with_color = reinterpret_cast<unsigned char>(print_with_color & 1);
    goto 0x4e20;
}

void fun_564c() {
    indicator_style = 1;
    goto 0x4e20;
}

void fun_57d3() {
    goto 0x4e20;
}

void fun_6b38() {
    goto 0x69f3;
}

void fun_17d78() {
    fun_4840();
    goto 0x17d49;
}

void fun_565b() {
    print_group = 0;
    goto 0x4e20;
}

void fun_57e0() {
    void** rdi1;

    fun_4840();
    rdi1 = optarg;
    xnumtoumax(rdi1);
    goto 0x4e20;
}

struct s220 {
    signed char[32] pad32;
    void** f20;
};

struct s221 {
    signed char[24] pad24;
    void** f18;
};

struct s222 {
    signed char[16] pad16;
    void** f10;
};

struct s223 {
    signed char[8] pad8;
    void** f8;
};

struct s224 {
    signed char[40] pad40;
    void** f28;
};

void fun_17db0() {
    void** rcx1;
    struct s220* rbx2;
    void** r15_3;
    struct s221* rbx4;
    void** r14_5;
    struct s222* rbx6;
    void** r13_7;
    struct s223* rbx8;
    void** r12_9;
    void*** rbx10;
    void** v11;
    struct s224* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_4840();
    fun_4c10(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x17de4, __return_address(), rcx1);
    goto v15;
}

void fun_566e() {
    goto 0x4e20;
}

void fun_5826() {
    goto 0x4e20;
}

void fun_17e28() {
    fun_4840();
    goto 0x17deb;
}

void fun_567a() {
    void** rdi1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** rax6;
    void** rdi7;

    rdi1 = optarg;
    rax6 = decode_line_length(rdi1, rsi2, rdx3, rcx4, r8_5);
    if (reinterpret_cast<signed char>(rax6) >= reinterpret_cast<signed char>(0)) 
        goto 0x4e20;
    rdi7 = optarg;
    quote(rdi7);
    fun_4840();
    fun_4b70();
    print_author = 1;
    goto 0x4e20;
}

void fun_5833() {
    recursive = 1;
    goto 0x4e20;
}

void fun_583f() {
    goto 0x4e20;
}

void fun_584c() {
    goto 0x4e20;
}

void fun_5859() {
    dereference = 4;
    goto 0x4e20;
}

void fun_5868() {
    void** rdi1;
    void** rsi2;

    rdi1 = optarg;
    add_ignore_pattern(rdi1, rsi2);
    goto 0x4e20;
}

void fun_5879() {
    dereference = 2;
    goto 0x4e20;
}

void fun_5888() {
    print_group = 0;
    goto 0x4e20;
}

void fun_5894() {
    void** rsi1;
    int64_t r9_2;
    int64_t rax3;
    unsigned char al4;

    rsi1 = optarg;
    if (rsi1 && (r9_2 = argmatch_die, rax3 = __xargmatch_internal("--classify", rsi1, 0x24200, 0x1a6e0, 4, r9_2), *reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) != 1)) {
        if (*reinterpret_cast<int32_t*>(0x1a6e0 + rax3 * 4) != 2) 
            goto 0x4e20;
        al4 = stdout_isatty("--classify", "--classify");
        if (!al4) 
            goto 0x4e20;
    }
    indicator_style = 3;
    goto 0x4e20;
}
