
void __pf_print(PF *pf,char *txt,size_t len)

{
  (*pf->sendfunc)(pf->clientdata,txt,len);
  pf->charcount = pf->charcount + len;
  return;
}



void __pf_endfield(PF *pf)

{
  pf->in_pct = 0;
  pf->size = INTSZ;
  *(undefined4 *)((int)&pf->num + 4) = 0;
  *(undefined4 *)&pf->num = 0;
  pf->sign = 0;
  pf->spacing = 0;
  pf->rightspc = 0;
  pf->fillchar = 0x20;
  pf->base = 0;
  pf->baseprefix = 0;
  pf->altformat = 0;
  return;
}



void __pf_getnum(PF *pf,int ch)

{
  undefined4 *puVar1;
  anon_enum_32_conflictba_for_size aVar2;
  int iVar3;
  int *piVar4;
  undefined4 uVar5;
  int iVar6;
  
  if (ch == 0x70) {
    puVar1 = (undefined4 *)pf->ap;
    pf->ap = puVar1 + 1;
    *(undefined4 *)((int)&pf->num + 4) = *puVar1;
    *(undefined4 *)&pf->num = 0;
    return;
  }
  if (ch == 100) {
    aVar2 = pf->size;
    if (aVar2 == LONGSZ) {
      piVar4 = (int *)pf->ap;
      pf->ap = piVar4 + 1;
      iVar6 = *piVar4;
      iVar3 = iVar6 >> 0x1f;
    }
    else if (aVar2 == INTSZ) {
      piVar4 = (int *)pf->ap;
      pf->ap = piVar4 + 1;
      iVar6 = *piVar4;
      iVar3 = iVar6 >> 0x1f;
    }
    else if (aVar2 == LLONGSZ) {
      piVar4 = (int *)((int)pf->ap + 7U & 0xfffffff8);
      pf->ap = piVar4 + 2;
      iVar3 = *piVar4;
      iVar6 = piVar4[1];
    }
    else if (aVar2 == SIZETSZ) {
      piVar4 = (int *)pf->ap;
      pf->ap = piVar4 + 1;
      iVar6 = *piVar4;
      iVar3 = iVar6 >> 0x1f;
    }
    else {
      iVar3 = 0;
      iVar6 = 0;
    }
    if (iVar3 < 0) {
      pf->sign = -1;
      *(uint *)&pf->num = -(uint)(-iVar6 != 0) - iVar3;
      *(int *)((int)&pf->num + 4) = -iVar6;
      return;
    }
    *(int *)&pf->num = iVar3;
    *(int *)((int)&pf->num + 4) = iVar6;
    return;
  }
  aVar2 = pf->size;
  if (aVar2 == LONGSZ) {
    puVar1 = (undefined4 *)pf->ap;
    pf->ap = puVar1 + 1;
    *(undefined4 *)((int)&pf->num + 4) = *puVar1;
    *(undefined4 *)&pf->num = 0;
    return;
  }
  if (aVar2 != INTSZ) {
    if (aVar2 == LLONGSZ) {
      puVar1 = (undefined4 *)((int)pf->ap + 7U & 0xfffffff8);
      pf->ap = puVar1 + 2;
      uVar5 = *puVar1;
      *(undefined4 *)((int)&pf->num + 4) = puVar1[1];
      *(undefined4 *)&pf->num = uVar5;
      return;
    }
    if (aVar2 != SIZETSZ) {
      return;
    }
    puVar1 = (undefined4 *)pf->ap;
    pf->ap = puVar1 + 1;
    *(undefined4 *)((int)&pf->num + 4) = *puVar1;
    *(undefined4 *)&pf->num = 0;
    return;
  }
  puVar1 = (undefined4 *)pf->ap;
  pf->ap = puVar1 + 1;
  *(undefined4 *)((int)&pf->num + 4) = *puVar1;
  *(undefined4 *)&pf->num = 0;
  return;
}



void __pf_setbase(PF *pf,int ch)

{
  switch(ch) {
  case 100:
  case 0x75:
    pf->base = 10;
    break;
  case 0x6f:
    pf->base = 8;
    break;
  case 0x70:
  case 0x78:
    pf->base = 0x10;
  }
  if ((pf->altformat != 0) || (ch == 0x70)) {
    pf->baseprefix = 1;
  }
  return;
}



void __pf_fill(PF *pf,int spc)

{
  int iVar1;
  char f;
  
  f = (char)pf->fillchar;
  for (iVar1 = 0; iVar1 < spc; iVar1 = iVar1 + 1) {
    __pf_print(pf,&f,1);
  }
  return;
}



void __pf_modifier(PF *pf,int ch)

{
  char *pcVar1;
  char *pcVar2;
  
  if (ch == 0x30) {
    if (pf->spacing < 1) {
      pf->fillchar = 0x30;
      return;
    }
    pf->spacing = pf->spacing * 10;
    return;
  }
  if (ch < 0x31) {
    if (ch == 0x23) {
      pf->altformat = 1;
      return;
    }
    if (ch == 0x2d) {
      pf->rightspc = 1;
      return;
    }
  }
  else {
    if (ch == 0x6c) {
      if (pf->size != LONGSZ) {
        pf->size = LONGSZ;
        return;
      }
      pf->size = LLONGSZ;
      return;
    }
    if (ch == 0x7a) {
      pf->size = SIZETSZ;
      return;
    }
  }
  pcVar2 = (char *)(ch + -0x30);
  if (8 < ch - 0x31U) {
    pcVar1 = "ch>\'0\' && ch<=\'9\'";
    pcVar2 = "../../../common/libc/printf/__printf.c";
    badassert("ch>\'0\' && ch<=\'9\'","../../../common/libc/printf/__printf.c",0xe1,"__pf_modifier")
    ;
    pf = (PF *)pcVar1;
  }
  pf->spacing = (int)(pcVar2 + pf->spacing * 10);
  return;
}



void __pf_printstuff(PF *pf,char *prefix,char *prefix2,char *stuff)

{
  size_t sVar1;
  size_t sVar2;
  size_t sVar3;
  int iVar4;
  int spc;
  
  sVar1 = strlen(prefix);
  sVar2 = strlen(prefix2);
  sVar3 = strlen(stuff);
  iVar4 = sVar1 + sVar2 + sVar3;
  spc = pf->spacing - iVar4;
  if (pf->spacing <= iVar4) {
    spc = 0;
  }
  if (((0 < spc) && (pf->rightspc == 0)) && (pf->fillchar != 0x30)) {
    __pf_fill(pf,spc);
  }
  sVar1 = strlen(prefix);
  __pf_print(pf,prefix,sVar1);
  sVar1 = strlen(prefix2);
  __pf_print(pf,prefix2,sVar1);
  if (((0 < spc) && (pf->rightspc == 0)) && (pf->fillchar == 0x30)) {
    __pf_fill(pf,spc);
  }
  sVar1 = strlen(stuff);
  __pf_print(pf,stuff,sVar1);
  if ((0 < spc) && (pf->rightspc != 0)) {
    __pf_fill(pf,spc);
  }
  return;
}



/* WARNING: Variable defined which should be unmapped: buf */
/* WARNING: Could not reconcile some variable overlaps */

void __pf_printnum(PF *pf)

{
  char *prefix;
  char *pcVar1;
  int iVar2;
  char *stuff;
  ulonglong uVar3;
  char buf [23];
  
  buf[22] = '\0';
  pcVar1 = buf + 0x15;
  do {
    stuff = pcVar1;
    iVar2 = pf->base;
    uVar3 = __umoddi3(buf._0_8_,buf._8_8_);
    *stuff = "0123456789abcdef"[(int)uVar3];
    uVar3 = __udivdi3(buf._0_8_,buf._8_8_);
    pcVar1 = stuff + -1;
  } while (uVar3 != 0);
  if ((pf->baseprefix == 0) || (iVar2 != 0x10)) {
    if (pf->baseprefix == 0) {
      pcVar1 = "";
    }
    else {
      pcVar1 = "";
      if (iVar2 == 8) {
        pcVar1 = "0";
      }
    }
  }
  else {
    pcVar1 = "0x";
  }
  if (pf->sign == 0) {
    prefix = "";
  }
  else {
    prefix = "-";
  }
  __pf_printstuff(pf,prefix,pcVar1,stuff);
  return;
}



void __pf_send(PF *pf,int ch)

{
  char **ppcVar1;
  undefined4 *puVar2;
  char *pcVar3;
  char *pcVar4;
  char c;
  undefined local_17;
  
  pcVar4 = (char *)ch;
  if (ch == 0) {
    pcVar3 = "ch!=0";
    pcVar4 = "../../../common/libc/printf/__printf.c";
    badassert("ch!=0","../../../common/libc/printf/__printf.c",0x1f5,"__pf_send");
    pf = (PF *)pcVar3;
  }
  if ((pf->in_pct == 0) && (pcVar4 != (char *)0x25)) {
    c = (char)pcVar4;
    __pf_print(pf,&c,1);
  }
  else if (pf->in_pct == 0) {
    pf->in_pct = 1;
  }
  else {
    pcVar4 = strchr((char *)&DAT_80022478,ch);
    if (pcVar4 == (char *)0x0) {
      pcVar4 = strchr("doupx",ch);
      if (pcVar4 == (char *)0x0) {
        if (ch == 0x73) {
          ppcVar1 = (char **)pf->ap;
          pf->ap = ppcVar1 + 1;
          pcVar4 = *ppcVar1;
          if (pcVar4 == (char *)0x0) {
            pcVar4 = "(null)";
          }
          __pf_printstuff(pf,"","",pcVar4);
          __pf_endfield(pf);
        }
        else {
          if (ch == 99) {
            puVar2 = (undefined4 *)pf->ap;
            pf->ap = puVar2 + 1;
            c = (char)*puVar2;
          }
          else {
            c = (char)ch;
          }
          local_17 = 0;
          __pf_printstuff(pf,"","",&c);
          __pf_endfield(pf);
        }
      }
      else {
        __pf_getnum(pf,ch);
        __pf_setbase(pf,ch);
        __pf_printnum(pf);
        __pf_endfield(pf);
      }
    }
    else {
      __pf_modifier(pf,ch);
    }
  }
  return;
}



int __vprintf(anon_subr_void_void_ptr_char_ptr_size_t *func,void *clientdata,char *format,va_list ap
             )

{
  int iVar1;
  PF pf;
  
  pf.charcount = 0;
  pf.sendfunc = (anon_subr_void_void_ptr_char_ptr_size_t_for_sendfunc *)func;
  pf.clientdata = clientdata;
  pf.ap = ap;
  __pf_endfield(&pf);
  for (iVar1 = 0; format[iVar1] != 0; iVar1 = iVar1 + 1) {
    __pf_send(&pf,(int)format[iVar1]);
  }
  return pf.charcount;
}



void __snprintf_send(void *mydata,char *data,size_t len)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < len; uVar1 = uVar1 + 1) {
    if (*(uint *)((int)mydata + 8) < *(uint *)((int)mydata + 4)) {
                    /* WARNING: Load size is inaccurate */
      *(char *)(*mydata + *(uint *)((int)mydata + 8)) = data[uVar1];
      *(int *)((int)mydata + 8) = *(int *)((int)mydata + 8) + 1;
    }
  }
  return;
}



int vsnprintf(char *buf,size_t len,char *fmt,va_list ap)

{
  int iVar1;
  SNP snp;
  
  if (len == 0) {
    snp.buflen = 0;
  }
  else {
    snp.buflen = len - 1;
  }
  snp.bufpos = 0;
  snp.buf = buf;
  iVar1 = __vprintf(__snprintf_send,&snp,fmt,ap);
  if (len != 0) {
    buf[snp.bufpos] = '\0';
  }
  return iVar1;
}



int snprintf(char *buf,size_t len,char *fmt,...)

{
  int iVar1;
  undefined4 in_a3;
  undefined4 local_resc;
  va_list ap;
  
  local_resc = in_a3;
  iVar1 = vsnprintf(buf,len,fmt,&local_resc);
  return iVar1;
}



int atoi(char *s)

{
  char cVar1;
  bool bVar2;
  char *pcVar3;
  char *pcVar4;
  char *pcVar5;
  
  do {
    while (pcVar4 = s, cVar1 = *pcVar4, cVar1 == ' ') {
      s = pcVar4 + 1;
    }
    s = pcVar4 + 1;
  } while (cVar1 == '\t');
  if (cVar1 == '-') {
    pcVar4 = pcVar4 + 1;
    bVar2 = true;
  }
  else if (cVar1 == '+') {
    pcVar4 = pcVar4 + 1;
    bVar2 = false;
  }
  else {
    bVar2 = false;
  }
  pcVar5 = (char *)0x0;
  for (; *pcVar4 != 0; pcVar4 = pcVar4 + 1) {
    pcVar3 = strchr("0123456789",(int)*pcVar4);
    if (pcVar3 == (char *)0x0) break;
    pcVar5 = pcVar3 + (int)pcVar5 * 10 + 0x7ffdf380;
  }
  pcVar4 = (char *)-(int)pcVar5;
  if (!bVar2) {
    pcVar4 = pcVar5;
  }
  return (int)pcVar4;
}



void bzero(void *vblock,size_t len)

{
  bool bVar1;
  uint uVar2;
  
  uVar2 = 0;
  if (((uint)vblock & 3) != 0) goto LAB_80000d50;
  uVar2 = 0;
  if ((len & 3) != 0) {
    bVar1 = len != 0;
    while (bVar1) {
      *(undefined *)((int)vblock + uVar2) = 0;
      uVar2 = uVar2 + 1;
LAB_80000d50:
      bVar1 = uVar2 < len;
    }
    return;
  }
  for (; uVar2 < len >> 2; uVar2 = uVar2 + 1) {
    *(undefined4 *)((int)vblock + uVar2 * 4) = 0;
  }
  return;
}



void * memcpy(void *dst,void *src,size_t len)

{
  bool bVar1;
  uint uVar2;
  
  if (((uint)dst & 3) != 0) {
    uVar2 = 0;
    goto LAB_80000dec;
  }
  uVar2 = 0;
  if (((uint)src & 3) != 0) goto LAB_80000dec;
  uVar2 = 0;
  if ((len & 3) != 0) {
    bVar1 = len != 0;
    while (bVar1) {
      *(undefined *)((int)dst + uVar2) = *(undefined *)((int)src + uVar2);
      uVar2 = uVar2 + 1;
LAB_80000dec:
      bVar1 = uVar2 < len;
    }
    return dst;
  }
  for (; uVar2 < len >> 2; uVar2 = uVar2 + 1) {
    *(undefined4 *)((int)dst + uVar2 * 4) = *(undefined4 *)((int)src + uVar2 * 4);
  }
  return dst;
}



void * memmove(void *dst,void *src,size_t len)

{
  bool bVar1;
  void *pvVar2;
  int iVar3;
  uint uVar4;
  
  if (src <= dst) {
    if (((((uint)dst & 3) == 0) && (((uint)src & 3) == 0)) && ((len & 3) == 0)) {
      for (uVar4 = len >> 2; uVar4 != 0; uVar4 = uVar4 - 1) {
        iVar3 = (uVar4 + 0x3fffffff) * 4;
        *(undefined4 *)((int)dst + iVar3) = *(undefined4 *)((int)src + iVar3);
      }
    }
    else {
      while (bVar1 = len != 0, len = len - 1, bVar1) {
        *(undefined *)((int)dst + len) = *(undefined *)((int)src + len);
      }
    }
    return dst;
  }
  pvVar2 = memcpy(dst,src,len);
  return pvVar2;
}



void * memset(void *ptr,int ch,size_t len)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < len; uVar1 = uVar1 + 1) {
    *(char *)((int)ptr + uVar1) = (char)ch;
  }
  return ptr;
}



char * strcat(char *dest,char *src)

{
  size_t sVar1;
  
  sVar1 = strlen(dest);
  strcpy(dest + sVar1,src);
  return dest;
}



char * strchr(char *s,int ch_arg)

{
  for (; *s != '\0'; s = s + 1) {
    if ((char)ch_arg == *s) {
      return s;
    }
  }
  if ((char)ch_arg != '\0') {
    return (char *)0x0;
  }
  return s;
}



int strcmp(char *a,char *b)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  
  iVar1 = 0;
  do {
    iVar2 = iVar1;
    uVar3 = (uint)a[iVar2];
    if (uVar3 == 0) break;
    iVar1 = iVar2 + 1;
  } while (uVar3 == (int)b[iVar2]);
  if (((int)b[iVar2] & 0xffU) < (uVar3 & 0xff)) {
    return 1;
  }
  if (uVar3 != (int)b[iVar2]) {
    return -1;
  }
  return 0;
}



char * strcpy(char *dest,char *src)

{
  int iVar1;
  
  iVar1 = 0;
  while( true ) {
    if (src[iVar1] == '\0') break;
    dest[iVar1] = src[iVar1];
    iVar1 = iVar1 + 1;
  }
  dest[iVar1] = '\0';
  return dest;
}



size_t strlen(char *str)

{
  size_t sVar1;
  size_t sVar2;
  
  sVar1 = 0;
  do {
    sVar2 = sVar1;
    sVar1 = sVar2 + 1;
  } while (str[sVar2] != '\0');
  return sVar2;
}



char * strrchr(char *s,int ch_arg)

{
  bool bVar1;
  size_t sVar2;
  int iVar3;
  
  sVar2 = strlen(s);
  iVar3 = sVar2 + 1;
  do {
    bVar1 = iVar3 == 0;
    iVar3 = iVar3 + -1;
    if (bVar1) {
      return (char *)0x0;
    }
  } while (s[iVar3] != (char)ch_arg);
  return s + iVar3;
}



char * strtok_r(char *string,char *seps,char **context)

{
  char cVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcVar4;
  char *pcVar5;
  
  if (string != (char *)0x0) {
    *context = string;
  }
  pcVar5 = (char *)0x0;
  pcVar2 = *context;
  if (*context != (char *)0x0) {
    do {
      pcVar5 = pcVar2;
      if (*pcVar5 == 0) break;
      pcVar3 = strchr(seps,(int)*pcVar5);
      pcVar2 = pcVar5 + 1;
    } while (pcVar3 != (char *)0x0);
    cVar1 = *pcVar5;
    pcVar2 = pcVar5;
    if ((char *)(int)cVar1 == (char *)0x0) {
      *context = (char *)0x0;
      pcVar5 = (char *)(int)cVar1;
    }
    else {
      do {
        pcVar3 = pcVar2;
        if (*pcVar3 == 0) break;
        pcVar4 = strchr(seps,(int)*pcVar3);
        pcVar2 = pcVar3 + 1;
      } while (pcVar4 == (char *)0x0);
      if (*pcVar3 == '\0') {
        *context = (char *)0x0;
      }
      else {
        *pcVar3 = '\0';
        *context = pcVar3 + 1;
      }
    }
  }
  return pcVar5;
}



int tryattach_emu_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  emu_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_emu_to_lamebus(devunit,bus);
  if (sc == (emu_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("emu%d at lamebus%d",devunit,busunit);
    errcode = config_emu(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_emu = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_ltrace_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  ltrace_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_ltrace_to_lamebus(devunit,bus);
  if (sc == (ltrace_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("ltrace%d at lamebus%d",devunit,busunit);
    errcode = config_ltrace(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_ltrace = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_beep_to_ltimer(int devunit,ltimer_softc *bus,int busunit)

{
  beep_softc *bs;
  int errcode;
  char *pcVar1;
  
  bs = attach_beep_to_ltimer(devunit,bus);
  if (bs == (beep_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("beep%d at ltimer%d",devunit,busunit);
    errcode = config_beep(bs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_beep = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_rtclock_to_ltimer(int devunit,ltimer_softc *bus,int busunit)

{
  rtclock_softc *rtc;
  int errcode;
  char *pcVar1;
  
  rtc = attach_rtclock_to_ltimer(devunit,bus);
  if (rtc == (rtclock_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("rtclock%d at ltimer%d",devunit,busunit);
    errcode = config_rtclock(rtc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_rtclock = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



void autoconf_ltimer(ltimer_softc *bus,int busunit)

{
  if (nextunit_beep < 1) {
    tryattach_beep_to_ltimer(0,bus,busunit);
  }
  if (nextunit_rtclock < 1) {
    tryattach_rtclock_to_ltimer(0,bus,busunit);
  }
  return;
}



int tryattach_ltimer_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  ltimer_softc *lt;
  int errcode;
  char *pcVar1;
  
  lt = attach_ltimer_to_lamebus(devunit,bus);
  if (lt == (ltimer_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("ltimer%d at lamebus%d",devunit,busunit);
    errcode = config_ltimer(lt,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_ltimer = devunit + 1;
      autoconf_ltimer(lt,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_random_to_lrandom(int devunit,lrandom_softc *bus,int busunit)

{
  random_softc *rs;
  int errcode;
  char *pcVar1;
  
  rs = attach_random_to_lrandom(devunit,bus);
  if (rs == (random_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("random%d at lrandom%d",devunit,busunit);
    errcode = config_random(rs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_random = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



void autoconf_lrandom(lrandom_softc *bus,int busunit)

{
  if (nextunit_random < 1) {
    tryattach_random_to_lrandom(0,bus,busunit);
  }
  return;
}



int tryattach_lrandom_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lrandom_softc *lr;
  int errcode;
  char *pcVar1;
  
  lr = attach_lrandom_to_lamebus(devunit,bus);
  if (lr == (lrandom_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lrandom%d at lamebus%d",devunit,busunit);
    errcode = config_lrandom(lr,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lrandom = devunit + 1;
      autoconf_lrandom(lr,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_lhd_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lhd_softc *lh;
  int errcode;
  char *pcVar1;
  
  lh = attach_lhd_to_lamebus(devunit,bus);
  if (lh == (lhd_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lhd%d at lamebus%d",devunit,busunit);
    errcode = config_lhd(lh,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lhd = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



int tryattach_con_to_lser(int devunit,lser_softc *bus,int busunit)

{
  con_softc *cs;
  int errcode;
  char *pcVar1;
  
  cs = attach_con_to_lser(devunit,bus);
  if (cs == (con_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("con%d at lser%d",devunit,busunit);
    errcode = config_con(cs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_con = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



void autoconf_lser(lser_softc *bus,int busunit)

{
  if (nextunit_con < 1) {
    tryattach_con_to_lser(0,bus,busunit);
  }
  return;
}



int tryattach_lser_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lser_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_lser_to_lamebus(devunit,bus);
  if (sc == (lser_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lser%d at lamebus%d",devunit,busunit);
    errcode = config_lser(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lser = devunit + 1;
      autoconf_lser(sc,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}



void autoconf_lamebus(lamebus_softc *bus,int busunit)

{
  int iVar1;
  int iVar2;
  int iVar3;
  
  iVar2 = nextunit_emu;
  do {
    iVar1 = tryattach_emu_to_lamebus(iVar2,bus,busunit);
    iVar2 = iVar2 + 1;
    iVar3 = nextunit_ltrace;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_ltrace_to_lamebus(iVar3,bus,busunit);
    iVar2 = nextunit_ltimer;
    iVar3 = iVar3 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_ltimer_to_lamebus(iVar2,bus,busunit);
    iVar3 = nextunit_lrandom;
    iVar2 = iVar2 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_lrandom_to_lamebus(iVar3,bus,busunit);
    iVar2 = nextunit_lhd;
    iVar3 = iVar3 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_lhd_to_lamebus(iVar2,bus,busunit);
    iVar3 = nextunit_lser;
    iVar2 = iVar2 + 1;
  } while (iVar1 == 0);
  do {
    iVar2 = tryattach_lser_to_lamebus(iVar3,bus,busunit);
    iVar3 = iVar3 + 1;
  } while (iVar2 == 0);
  return;
}



void pseudoconfig(void)

{
  return;
}



int config_beep(beep_softc *bs,int unit)

{
  char *pcVar1;
  
  if (unit == 0) {
    if (the_beep != (beep_softc *)0x0) {
      pcVar1 = "the_beep==NULL";
      badassert("the_beep==NULL","../../dev/generic/beep.c",0x39,"config_beep");
      bs = (beep_softc *)pcVar1;
    }
    the_beep = bs;
    return 0;
  }
  return 0x19;
}



void beep(void)

{
  if (the_beep == (beep_softc *)0x0) {
    kprintf("beep: Warning: no beep device\n");
  }
  else {
    (*the_beep->bs_beep)(the_beep->bs_devdata);
  }
  return;
}



void putch_polled(con_softc *cs,int ch)

{
  (*cs->cs_sendpolled)(cs->cs_devdata,ch);
  return;
}



int con_eachopen(device *dev,int openflags)

{
  return 0;
}



int con_ioctl(device *dev,int op,userptr_t data)

{
  return 8;
}



void putch_delayed(int ch)

{
  char *pcVar1;
  char cVar2;
  char *pcVar3;
  
  cVar2 = (char)ch;
  pcVar3 = (char *)(delayed_outbuf_pos + 1);
  if (0x3ff < delayed_outbuf_pos) {
    cVar2 = -0x4c;
    pcVar3 = "../../dev/generic/console.c";
    badassert("delayed_outbuf_pos < sizeof(delayed_outbuf)","../../dev/generic/console.c",0x5f,
              "putch_delayed");
  }
  pcVar1 = delayed_outbuf + delayed_outbuf_pos;
  delayed_outbuf_pos = (size_t)pcVar3;
  *pcVar1 = cVar2;
  return;
}



void putch_intr(con_softc *cs,int ch)

{
  P(cs->cs_wsem);
  (*cs->cs_send)(cs->cs_devdata,ch);
  return;
}



int getch_intr(con_softc *cs)

{
  byte bVar1;
  
  P(cs->cs_rsem);
  bVar1 = cs->cs_gotchars[cs->cs_gotchars_tail];
  cs->cs_gotchars_tail = cs->cs_gotchars_tail + 1 & 0x1f;
  return (uint)bVar1;
}



int attach_console_to_vfs(con_softc *cs)

{
  device *dev;
  int iVar1;
  
  dev = (device *)kmalloc(0x14);
  if (dev == (device *)0x0) {
    iVar1 = 3;
  }
  else {
    dev->d_ops = &console_devops;
    dev->d_blocks = 0;
    dev->d_blocksize = 1;
    dev->d_data = cs;
    iVar1 = vfs_adddev("con",dev,0);
    if (iVar1 == 0) {
      iVar1 = 0;
    }
    else {
      kfree(dev);
    }
  }
  return iVar1;
}



void con_input(void *vcs,int ch)

{
  uint uVar1;
  
  uVar1 = *(int *)((int)vcs + 0x34) + 1U & 0x1f;
  if (*(uint *)((int)vcs + 0x38) != uVar1) {
    *(char *)((int)vcs + *(int *)((int)vcs + 0x34) + 0x14) = (char)ch;
    *(uint *)((int)vcs + 0x34) = uVar1;
    V(*(semaphore **)((int)vcs + 0xc));
  }
  return;
}



void con_start(void *vcs)

{
  V(*(semaphore **)((int)vcs + 0x10));
  return;
}



void putch(int ch)

{
  int unaff_s7;
  
  if (the_console == (con_softc *)0x0) {
    putch_delayed(ch);
  }
  else if (((*(char *)(unaff_s7 + 0x58) == '\0') && (*(int *)(unaff_s7 + 0x5c) < 1)) &&
          (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) == 0)) {
    putch_intr(the_console,ch);
  }
  else {
    putch_polled(the_console,ch);
  }
  return;
}



void flush_delay_buf(void)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < delayed_outbuf_pos; uVar1 = uVar1 + 1) {
    putch((int)delayed_outbuf[uVar1]);
  }
  delayed_outbuf_pos = 0;
  return;
}



int getch(void)

{
  int iVar1;
  char *cs;
  int unaff_s7;
  
  cs = (char *)the_console;
  if (the_console == (con_softc *)0x0) {
    cs = "cs != NULL";
    badassert("cs != NULL","../../dev/generic/console.c",0xde,"getch");
  }
  if ((*(char *)(unaff_s7 + 0x58) != '\0') || (*(int *)(unaff_s7 + 0x60) != 0)) {
    cs = "!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0";
    badassert("!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0",
              "../../dev/generic/console.c",0xdf,"getch");
  }
  iVar1 = getch_intr((con_softc *)cs);
  return iVar1;
}



int con_io(device *dev,uio *uio)

{
  int iVar1;
  lock *lock;
  char ch;
  
  lock = con_userlock_write;
  if (uio->uio_rw == UIO_READ) {
    lock = con_userlock_read;
  }
  if (lock == (lock *)0x0) {
    badassert("lk != NULL","../../dev/generic/console.c",0x104,"con_io");
  }
  lock_acquire(lock);
  do {
    while( true ) {
      if (uio->uio_resid == 0) goto LAB_8000202c;
      if (uio->uio_rw == UIO_READ) break;
      iVar1 = uiomove(&ch,1,uio);
      if (iVar1 != 0) {
        lock_release(lock);
        return iVar1;
      }
      if (ch == '\n') {
        putch(0xd);
      }
      putch((int)ch);
    }
    iVar1 = getch();
    ch = (char)iVar1;
    if (ch == '\r') {
      ch = '\n';
    }
    iVar1 = uiomove(&ch,1,uio);
    if (iVar1 != 0) {
      lock_release(lock);
      return iVar1;
    }
  } while (ch != '\n');
LAB_8000202c:
  lock_release(lock);
  return 0;
}



int config_con(con_softc *cs,int unit)

{
  semaphore *sem;
  semaphore *sem_00;
  int iVar1;
  lock *lock;
  lock *plVar2;
  char *pcVar3;
  
  if (0 < unit) {
    if (the_console != (con_softc *)0x0) {
      return 0x19;
    }
    pcVar3 = "the_console!=NULL";
    badassert("the_console!=NULL","../../dev/generic/console.c",0x165,"config_con");
    cs = (con_softc *)pcVar3;
  }
  if (the_console != (con_softc *)0x0) {
    badassert("the_console==NULL","../../dev/generic/console.c",0x168,"config_con");
  }
  sem = sem_create("console read",0);
  if (sem == (semaphore *)0x0) {
    iVar1 = 3;
  }
  else {
    sem_00 = sem_create(s_console_write_80022690,1);
    if (sem_00 == (semaphore *)0x0) {
      sem_destroy(sem);
      iVar1 = 3;
    }
    else {
      lock = lock_create("console-lock-read");
      if (lock == (lock *)0x0) {
        sem_destroy(sem);
        sem_destroy(sem_00);
        iVar1 = 3;
      }
      else {
        plVar2 = lock_create("console-lock-write");
        if (plVar2 == (lock *)0x0) {
          lock_destroy(lock);
          sem_destroy(sem);
          sem_destroy(sem_00);
          iVar1 = 3;
        }
        else {
          cs->cs_rsem = sem;
          cs->cs_wsem = sem_00;
          cs->cs_gotchars_head = 0;
          cs->cs_gotchars_tail = 0;
          con_userlock_write = plVar2;
          con_userlock_read = lock;
          the_console = cs;
          flush_delay_buf();
          iVar1 = attach_console_to_vfs(cs);
        }
      }
    }
  }
  return iVar1;
}



int randeachopen(device *dev,int openflags)

{
  if (openflags != 0) {
    return 0x20;
  }
  return 0;
}



int randio(device *dev,uio *uio)

{
  int iVar1;
  
  if (uio->uio_rw == UIO_READ) {
                    /* WARNING: Load size is inaccurate */
    iVar1 = (**(code **)((int)dev->d_data + 0xc))(*dev->d_data);
    return iVar1;
  }
  return 0x20;
}



int randioctl(device *dev,int op,userptr_t data)

{
  return 0x1f;
}



int config_random(random_softc *rs,int unit)

{
  int iVar1;
  char *pcVar2;
  
  if (unit == 0) {
    pcVar2 = (char *)rs;
    if (the_random != (random_softc *)0x0) {
      pcVar2 = "the_random==NULL";
      badassert("the_random==NULL",s_______dev_generic_random_c_800226dc,0x78,"config_random");
    }
    the_random = (random_softc *)pcVar2;
    ((device *)((int)pcVar2 + 0x10))->d_ops = &random_devops;
    ((device *)((int)pcVar2 + 0x10))->d_blocks = 0;
    ((device *)((int)pcVar2 + 0x10))->d_blocksize = 1;
    (rs->rs_dev).d_data = pcVar2;
    iVar1 = vfs_adddev("random",&rs->rs_dev,0);
    return iVar1;
  }
  return 0x19;
}



long random(void)

{
  uint32_t uVar1;
  
  if (the_random == (random_softc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("No random device\n");
  }
  uVar1 = (*the_random->rs_random)(the_random->rs_devdata);
  return uVar1;
}



uint32_t randmax(void)

{
  uint32_t uVar1;
  
  if (the_random == (random_softc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("No random device\n");
  }
  uVar1 = (*the_random->rs_randmax)(the_random->rs_devdata);
  return uVar1;
}



int config_rtclock(rtclock_softc *rtc,int unit)

{
  char *pcVar1;
  
  if (unit == 0) {
    if (the_clock != (rtclock_softc *)0x0) {
      pcVar1 = "the_clock==NULL";
      badassert("the_clock==NULL","../../dev/generic/rtclock.c",0x3c,"config_rtclock");
      rtc = (rtclock_softc *)pcVar1;
    }
    the_clock = rtc;
    return 0;
  }
  return 0x19;
}



void gettime(timespec *ts)

{
  rtclock_softc *prVar1;
  char *pcVar2;
  
  prVar1 = the_clock;
  if (the_clock == (rtclock_softc *)0x0) {
    pcVar2 = "../../dev/generic/rtclock.c";
    badassert("the_clock!=NULL","../../dev/generic/rtclock.c",0x44,"gettime");
    ts = (timespec *)pcVar2;
  }
  (*prVar1->rtc_gettime)(prVar1->rtc_devdata,ts);
  return;
}



beep_softc * attach_beep_to_ltimer(int beepno,ltimer_softc *ls)

{
  beep_softc *pbVar1;
  
  pbVar1 = (beep_softc *)kmalloc(8);
  if (pbVar1 == (beep_softc *)0x0) {
    pbVar1 = (beep_softc *)0x0;
  }
  else {
    pbVar1->bs_devdata = ls;
    pbVar1->bs_beep = ltimer_beep;
  }
  return pbVar1;
}



con_softc * attach_con_to_lser(int consno,lser_softc *ls)

{
  con_softc *pcVar1;
  
  pcVar1 = (con_softc *)kmalloc(0x3c);
  if (pcVar1 == (con_softc *)0x0) {
    pcVar1 = (con_softc *)0x0;
  }
  else {
    pcVar1->cs_devdata = ls;
    pcVar1->cs_send = lser_write;
    pcVar1->cs_sendpolled = lser_writepolled;
    ls->ls_devdata = pcVar1;
    ls->ls_start = con_start;
    ls->ls_input = con_input;
  }
  return pcVar1;
}



emu_softc * attach_emu_to_lamebus(int emuno,lamebus_softc *sc)

{
  uint32_t slot;
  emu_softc *devdata;
  
  slot = lamebus_probe(sc,1,7,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (emu_softc *)0x0;
  }
  else {
    devdata = (emu_softc *)kmalloc(0x1c);
    if (devdata == (emu_softc *)0x0) {
      devdata = (emu_softc *)0x0;
    }
    else {
      devdata->e_busdata = sc;
      devdata->e_buspos = slot;
      devdata->e_unit = emuno;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,emu_irq);
    }
  }
  return devdata;
}



int emufs_eachopen(vnode *v,int openflags)

{
  return 0;
}



int emufs_eachopendir(vnode *v,int openflags)

{
  uint uVar1;
  
  uVar1 = 0x12;
  if (((openflags & 3U) == 0) && (uVar1 = openflags & 0x20, uVar1 != 0)) {
    return 0x12;
  }
  return uVar1;
}



int emufs_ioctl(vnode *v,int op,userptr_t data)

{
  return 8;
}



int emufs_file_gettype(vnode *v,uint32_t *result)

{
  *result = 0x1000;
  return 0;
}



int emufs_dir_gettype(vnode *v,uint32_t *result)

{
  *result = 0x2000;
  return 0;
}



bool emufs_isseekable(vnode *v)

{
  return true;
}



int emufs_fsync(vnode *v)

{
  return 0;
}



int emufs_namefile(vnode *v,uio *uio)

{
  if (*(void **)((int)v->vn_fs->fs_data + 0xc) == v->vn_data) {
    return 0;
  }
  return 1;
}



int emufs_mmap(vnode *v)

{
  return 1;
}



int emufs_symlink(vnode *v,char *contents,char *name)

{
  return 1;
}



int emufs_mkdir(vnode *v,char *name,mode_t mode)

{
  return 1;
}



int emufs_link(vnode *v,char *name,vnode *target)

{
  return 1;
}



int emufs_remove(vnode *v,char *name)

{
  return 1;
}



int emufs_rmdir(vnode *v,char *name)

{
  return 1;
}



int emufs_rename(vnode *v1,char *n1,vnode *v2,char *n2)

{
  return 1;
}



int emufs_void_op_isdir(vnode *v)

{
  return 0x12;
}



int emufs_uio_op_isdir(vnode *v,uio *uio)

{
  return 0x12;
}



int emufs_uio_op_notdir(vnode *v,uio *uio)

{
  return 0x11;
}



int emufs_name_op_notdir(vnode *v,char *name)

{
  return 0x11;
}



int emufs_readlink_notlink(vnode *v,uio *uio)

{
  return 8;
}



int emufs_creat_notdir(vnode *v,char *name,bool excl,mode_t mode,vnode **retval)

{
  return 0x11;
}



int emufs_symlink_notdir(vnode *v,char *contents,char *name)

{
  return 0x11;
}



int emufs_mkdir_notdir(vnode *v,char *name,mode_t mode)

{
  return 0x11;
}



int emufs_link_notdir(vnode *v,char *name,vnode *target)

{
  return 0x11;
}



int emufs_rename_notdir(vnode *v1,char *n1,vnode *v2,char *n2)

{
  return 0x11;
}



int emufs_lookup_notdir(vnode *v,char *pathname,vnode **result)

{
  return 0x11;
}



int emufs_lookparent_notdir(vnode *v,char *pathname,vnode **result,char *buf,size_t len)

{
  return 0x11;
}



int emufs_truncate_isdir(vnode *v,off_t len)

{
  return 0x11;
}



int emufs_sync(fs *fs)

{
  return 0;
}



char * emufs_getvolname(fs *fs)

{
  return (char *)0x0;
}



int emufs_unmount(fs *fs)

{
  return 0x1b;
}



int emufs_getroot(fs *fs,vnode **ret)

{
  char *pcVar1;
  void *pvVar2;
  
  if (fs == (fs *)0x0) {
    fs = (fs *)0x800270fc;
    pcVar1 = s_______dev_lamebus_emu_c_80022750;
    badassert("fs != NULL",s_______dev_lamebus_emu_c_80022750,0x4e1,"emufs_getroot");
    ret = (vnode **)pcVar1;
  }
  pvVar2 = fs->fs_data;
  if (pvVar2 == (void *)0x0) {
    badassert("ef != NULL",s_______dev_lamebus_emu_c_80022750,0x4e5,"emufs_getroot");
  }
  pcVar1 = *(char **)((int)pvVar2 + 0xc);
  if ((vnode *)pcVar1 == (vnode *)0x0) {
    pcVar1 = "ef->ef_root != NULL";
    badassert("ef->ef_root != NULL",s_______dev_lamebus_emu_c_80022750,0x4e6,"emufs_getroot");
  }
  vnode_incref((vnode *)pcVar1);
  *ret = *(vnode **)((int)pvVar2 + 0xc);
  return 0;
}



uint32_t translate_err(emu_softc *sc,uint32_t code)

{
  uint32_t uVar1;
  
  switch(code) {
  default:
    kprintf("emu%d: Unknown result code %d\n",sc->e_unit);
    uVar1 = 4;
    break;
  case 1:
    uVar1 = 0;
    break;
  case 2:
  case 3:
  case 5:
                    /* WARNING: Subroutine does not return */
    panic("emu%d: got fatal result code %d\n",sc->e_unit,code);
  case 4:
    uVar1 = 0x13;
    break;
  case 6:
    uVar1 = 0x16;
    break;
  case 7:
    uVar1 = 0x12;
    break;
  case 8:
    uVar1 = 0x20;
    break;
  case 9:
    uVar1 = 0x1d;
    break;
  case 10:
    uVar1 = 0x24;
    break;
  case 0xb:
    uVar1 = 0x11;
    break;
  case 0xc:
    uVar1 = 0x20;
    break;
  case 0xd:
    uVar1 = 1;
  }
  return uVar1;
}



int emu_waitdone(emu_softc *sc)

{
  uint32_t uVar1;
  
  P(sc->e_sem);
  uVar1 = translate_err(sc,sc->e_result);
  return uVar1;
}



int emu_trunc(emu_softc *sc,uint32_t handle,off_t len)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  int in_a2;
  char *in_a3;
  
  if (in_a2 < 0) {
    pcVar2 = "len >= 0";
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    in_a3 = "emu_trunc";
    badassert("len >= 0",s_______dev_lamebus_emu_c_80022750,0x187,"emu_trunc");
    sc = (emu_softc *)pcVar2;
    handle = (uint32_t)pcVar3;
  }
  lock_acquire(sc->e_lock);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,(uint32_t)in_a3);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,9);
  iVar1 = emu_waitdone(sc);
  lock_release(sc->e_lock);
  return iVar1;
}



int emufs_truncate(vnode *v,off_t len)

{
  int iVar1;
  undefined4 unaff_retaddr;
  undefined4 in_stack_fffffff8;
  
  iVar1 = emu_trunc(*(emu_softc **)((int)v->vn_data + 0x18),*(uint32_t *)((int)v->vn_data + 0x1c),
                    CONCAT44(in_stack_fffffff8,unaff_retaddr));
  return iVar1;
}



int emu_getsize(emu_softc *sc,uint32_t handle,off_t *retval)

{
  int iVar1;
  uint32_t uVar2;
  
  lock_acquire(sc->e_lock);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,8);
  iVar1 = emu_waitdone(sc);
  if (iVar1 == 0) {
    uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
    *(uint32_t *)((int)retval + 4) = uVar2;
    *(undefined4 *)retval = 0;
  }
  lock_release(sc->e_lock);
  return iVar1;
}



int emufs_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  iVar1 = emu_getsize(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),
                      &statbuf->st_size);
  if (iVar1 == 0) {
    vnode_check(v,"gettype");
    iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
    if (iVar1 == 0) {
      statbuf->st_mode = statbuf->st_mode | 0x1a4;
      statbuf->st_nlink = 1;
      statbuf->st_blocks = 0;
      iVar1 = 0;
    }
  }
  return iVar1;
}



int emu_write(emu_softc *sc,uint32_t handle,uint32_t len,uio *uio)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar4 = (char *)uio;
  if (uio->uio_rw != UIO_WRITE) {
    pcVar2 = "uio->uio_rw == UIO_WRITE";
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    len = 0x14e;
    pcVar4 = "emu_write";
    badassert("uio->uio_rw == UIO_WRITE",s_______dev_lamebus_emu_c_80022750,0x14e,"emu_write");
    sc = (emu_softc *)pcVar2;
    handle = (uint32_t)pcVar3;
  }
  if (*(int *)((int)pcVar4 + 8) < 1) {
    lock_acquire(sc->e_lock);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,len);
    lamebus_write_register
              ((lamebus_softc *)sc->e_busdata,sc->e_buspos,4,
               *(uint32_t *)((int)&uio->uio_offset + 4));
    iVar1 = uiomove(sc->e_iobuf,len,uio);
    SYNC(0);
    if (iVar1 == 0) {
      lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,7);
      iVar1 = emu_waitdone(sc);
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar1 = 0x26;
  }
  return iVar1;
}



int emufs_write(vnode *v,uio *uio)

{
  int iVar1;
  uint len;
  uint unaff_s0;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  if (uio->uio_rw == UIO_WRITE) goto LAB_80002c6c;
  badassert("uio->uio_rw==UIO_WRITE",s_______dev_lamebus_emu_c_80022750,0x25c,"emufs_write");
  do {
    len = unaff_s0;
    if (0x4000 < unaff_s0) {
      len = 0x4000;
    }
    iVar1 = emu_write(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),len,uio)
    ;
    if (iVar1 != 0) {
      return iVar1;
    }
    if (unaff_s0 == uio->uio_resid) {
      return 0;
    }
LAB_80002c6c:
    unaff_s0 = uio->uio_resid;
  } while (unaff_s0 != 0);
  return 0;
}



int emu_doread(emu_softc *sc,uint32_t handle,uint32_t len,uint32_t op,uio *uio)

{
  int iVar1;
  uint32_t uVar2;
  char *pcVar3;
  char *pcVar4;
  char *pcVar5;
  void *ptr;
  
  pcVar3 = (char *)sc;
  if (uio->uio_rw != UIO_READ) {
    pcVar3 = "uio->uio_rw == UIO_READ";
    pcVar4 = s_______dev_lamebus_emu_c_80022750;
    len = 0x112;
    pcVar5 = "emu_doread";
    badassert("uio->uio_rw == UIO_READ",s_______dev_lamebus_emu_c_80022750,0x112,"emu_doread");
    handle = (uint32_t)pcVar4;
    op = (uint32_t)pcVar5;
  }
  if (*(int *)&uio->uio_offset < 1) {
    lock_acquire(*(lock **)((int)pcVar3 + 0xc));
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,len);
    lamebus_write_register
              ((lamebus_softc *)sc->e_busdata,sc->e_buspos,4,
               *(uint32_t *)((int)&uio->uio_offset + 4));
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,op);
    iVar1 = emu_waitdone(sc);
    if (iVar1 == 0) {
      SYNC(0);
      ptr = sc->e_iobuf;
      uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
      iVar1 = uiomove(ptr,uVar2,uio);
      uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,4);
      *(uint32_t *)((int)&uio->uio_offset + 4) = uVar2;
      *(undefined4 *)&uio->uio_offset = 0;
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar1 = 0;
  }
  return iVar1;
}



int emu_read(emu_softc *sc,uint32_t handle,uint32_t len,uio *uio)

{
  int iVar1;
  
  iVar1 = emu_doread(sc,handle,len,5,uio);
  return iVar1;
}



int emufs_read(vnode *v,uio *uio)

{
  int iVar1;
  uint len;
  uint unaff_s0;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  if (uio->uio_rw == UIO_READ) goto LAB_80002e8c;
  badassert("uio->uio_rw==UIO_READ",s_______dev_lamebus_emu_c_80022750,0x224,"emufs_read");
  do {
    len = unaff_s0;
    if (0x4000 < unaff_s0) {
      len = 0x4000;
    }
    iVar1 = emu_read(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),len,uio);
    if (iVar1 != 0) {
      return iVar1;
    }
    if (unaff_s0 == uio->uio_resid) {
      return 0;
    }
LAB_80002e8c:
    unaff_s0 = uio->uio_resid;
  } while (unaff_s0 != 0);
  return 0;
}



int emu_readdir(emu_softc *sc,uint32_t handle,uint32_t len,uio *uio)

{
  int iVar1;
  
  iVar1 = emu_doread(sc,handle,len,6,uio);
  return iVar1;
}



int emufs_getdirentry(vnode *v,uio *uio)

{
  void *pvVar1;
  int iVar2;
  char *pcVar3;
  uint32_t len;
  char *uio_00;
  
  pvVar1 = v->vn_data;
  uio_00 = (char *)uio;
  if (uio->uio_rw != UIO_READ) {
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    uio_00 = "emufs_getdirentry";
    badassert("uio->uio_rw==UIO_READ",s_______dev_lamebus_emu_c_80022750,0x246,"emufs_getdirentry");
    uio = (uio *)pcVar3;
  }
  len = uio->uio_resid;
  if (0x4000 < len) {
    len = 0x4000;
  }
  iVar2 = emu_readdir(*(emu_softc **)((int)pvVar1 + 0x18),*(uint32_t *)((int)pvVar1 + 0x1c),len,
                      (uio *)uio_00);
  return iVar2;
}



int emu_close(emu_softc *sc,uint32_t handle)

{
  undefined3 extraout_var;
  bool bVar2;
  int iVar1;
  int iVar3;
  
  bVar2 = lock_do_i_hold(sc->e_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    lock_acquire(sc->e_lock);
  }
  iVar3 = 0;
  while( true ) {
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,4);
    iVar1 = emu_waitdone(sc);
    if ((iVar1 != 0x20) || (9 < iVar3)) break;
    kprintf("emu%d: I/O error on close, retrying\n",sc->e_unit);
    iVar3 = iVar3 + 1;
  }
  if (CONCAT31(extraout_var,bVar2) == 0) {
    lock_release(sc->e_lock);
  }
  return iVar1;
}



int emufs_reclaim(vnode *v)

{
  int iVar1;
  uint uVar2;
  array *a;
  uint uVar3;
  uint index;
  vnode *vn;
  void *pvVar4;
  spinlock *splk;
  
  vn = (vnode *)v->vn_data;
  pvVar4 = v->vn_fs->fs_data;
  vfs_biglock_acquire();
  splk = &vn->vn_countlock;
  lock_acquire(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
  spinlock_acquire(splk);
  iVar1 = vn->vn_refcount;
  if (iVar1 < 2) {
    if (iVar1 != 1) {
      badassert("ev->ev_v.vn_refcount == 1",s_______dev_lamebus_emu_c_80022750,0x1ee,"emufs_reclaim"
               );
    }
    spinlock_release(splk);
    iVar1 = emu_close((emu_softc *)vn[1].vn_refcount,vn[1].vn_countlock.splk_lock);
    if (iVar1 == 0) {
      a = *(array **)((int)pvVar4 + 0x10);
      uVar2 = a->num;
      for (uVar3 = 0; (index = uVar2, uVar3 < uVar2 && (index = uVar3, v != (vnode *)a->v[uVar3]));
          uVar3 = uVar3 + 1) {
      }
      if (index == uVar2) {
                    /* WARNING: Subroutine does not return */
        panic("emu%d: reclaim vnode %u not in vnode pool\n",
              *(undefined4 *)(*(int *)((int)pvVar4 + 8) + 8),vn[1].vn_countlock.splk_lock);
      }
      array_remove(a,index);
      vnode_cleanup(vn);
      lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
      vfs_biglock_release();
      kfree(vn);
      iVar1 = 0;
    }
    else {
      lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
      vfs_biglock_release();
    }
  }
  else {
    vn->vn_refcount = iVar1 + -1;
    spinlock_release(splk);
    lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}



int emu_open(emu_softc *sc,uint32_t handle,char *name,bool create,bool excl,mode_t mode,
            uint32_t *newhandle,int *newisdir)

{
  size_t sVar1;
  int iVar2;
  undefined3 in_register_0000001c;
  uint32_t uVar3;
  
  sVar1 = strlen(name);
  if (sVar1 + 1 < 0x4001) {
    if ((CONCAT31(in_register_0000001c,create) == 0) || (excl == false)) {
      uVar3 = 2;
      if (CONCAT31(in_register_0000001c,create) == 0) {
        uVar3 = 1;
      }
    }
    else {
      uVar3 = 3;
    }
    lock_acquire(sc->e_lock);
    strcpy((char *)sc->e_iobuf,name);
    SYNC(0);
    sVar1 = strlen(name);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,sVar1);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,uVar3);
    iVar2 = emu_waitdone(sc);
    if (iVar2 == 0) {
      uVar3 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0);
      *newhandle = uVar3;
      uVar3 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
      *newisdir = (uint)(uVar3 != 0);
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar2 = 7;
  }
  return iVar2;
}



int emufs_loadvnode(emufs_fs *ef,uint32_t handle,int isdir,emufs_vnode **ret)

{
  uint uVar1;
  emufs_vnode *peVar2;
  int iVar3;
  uint uVar4;
  char *pcVar5;
  vnode_ops *ops;
  vnodearray *pvVar6;
  array *a;
  
  vfs_biglock_acquire();
  lock_acquire(ef->ef_emu->e_lock);
  pvVar6 = ef->ef_vnodes;
  uVar4 = (pvVar6->arr).num;
  uVar1 = 0;
  while (uVar1 < uVar4) {
    pcVar5 = (char *)(uVar1 << 2);
    if (uVar4 <= uVar1) {
      pcVar5 = "index < a->num";
      pvVar6 = (vnodearray *)&DAT_00000064;
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    peVar2 = *(emufs_vnode **)(*(int *)(pcVar5 + (int)(pvVar6->arr).v) + 0x10);
    uVar1 = uVar1 + 1;
    if (peVar2->ev_handle == handle) {
      vnode_incref(&peVar2->ev_v);
      lock_release(ef->ef_emu->e_lock);
      vfs_biglock_release();
      *ret = peVar2;
      return 0;
    }
  }
  peVar2 = (emufs_vnode *)kmalloc(0x20);
  if (peVar2 == (emufs_vnode *)0x0) {
    lock_release(ef->ef_emu->e_lock);
    return 3;
  }
  peVar2->ev_emu = ef->ef_emu;
  peVar2->ev_handle = handle;
  if (isdir == 0) {
    ops = &emufs_fileops;
  }
  else {
    ops = &emufs_dirops;
  }
  iVar3 = vnode_init((vnode *)peVar2,ops,&ef->ef_fs,peVar2);
  if (iVar3 == 0) {
    a = (array *)ef->ef_vnodes;
    uVar1 = a->num;
    iVar3 = array_setsize(a,uVar1 + 1);
    if (iVar3 == 0) {
      a->v[uVar1] = peVar2;
      iVar3 = 0;
    }
    if (iVar3 == 0) {
      lock_release(ef->ef_emu->e_lock);
      vfs_biglock_release();
      *ret = peVar2;
      return 0;
    }
    vnode_cleanup((vnode *)peVar2);
    lock_release(ef->ef_emu->e_lock);
    vfs_biglock_release();
    kfree(peVar2);
    return iVar3;
  }
  lock_release(ef->ef_emu->e_lock);
  vfs_biglock_release();
  kfree(peVar2);
  return iVar3;
}



int emufs_addtovfs(emu_softc *sc,char *devname)

{
  emufs_fs *ef;
  array *a;
  int iVar1;
  
  ef = (emufs_fs *)kmalloc(0x14);
  if (ef == (emufs_fs *)0x0) {
    iVar1 = 3;
  }
  else {
    (ef->ef_fs).fs_data = ef;
    (ef->ef_fs).fs_ops = &emufs_fsops;
    ef->ef_emu = sc;
    ef->ef_root = (emufs_vnode *)0x0;
    a = (array *)kmalloc(0xc);
    if (a == (array *)0x0) {
      a = (array *)0x0;
      ef->ef_vnodes = (vnodearray *)0x0;
    }
    else {
      array_init(a);
      ef->ef_vnodes = (vnodearray *)a;
    }
    if (a == (array *)0x0) {
      kfree(ef);
      iVar1 = 3;
    }
    else {
      iVar1 = emufs_loadvnode(ef,0,1,&ef->ef_root);
      if (iVar1 == 0) {
        if (ef->ef_root == (emufs_vnode *)0x0) {
          devname = "ef->ef_root!=NULL";
          badassert("ef->ef_root!=NULL",s_______dev_lamebus_emu_c_80022750,0x527,"emufs_addtovfs");
        }
        iVar1 = vfs_addfs(devname,(fs *)ef);
        if (iVar1 != 0) {
          vnode_decref(&ef->ef_root->ev_v);
          kfree(ef);
        }
      }
      else {
        kfree(ef);
      }
    }
  }
  return iVar1;
}



int emufs_lookup(vnode *dir,char *pathname,vnode **ret)

{
  int iVar1;
  void *pvVar2;
  emufs_fs *ef;
  emufs_vnode *newguy;
  uint32_t handle;
  int isdir;
  
  pvVar2 = dir->vn_data;
  ef = (emufs_fs *)dir->vn_fs->fs_data;
  iVar1 = emu_open(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),pathname,
                   false,false,0,&handle,&isdir);
  if (iVar1 == 0) {
    iVar1 = emufs_loadvnode(ef,handle,isdir,&newguy);
    if (iVar1 == 0) {
      *ret = &newguy->ev_v;
      iVar1 = 0;
    }
    else {
      emu_close(*(emu_softc **)((int)pvVar2 + 0x18),handle);
    }
  }
  return iVar1;
}



int emufs_lookparent(vnode *dir,char *pathname,vnode **ret,char *buf,size_t len)

{
  char *pcVar1;
  size_t sVar2;
  int iVar3;
  
  pcVar1 = strrchr(pathname,0x2f);
  if (pcVar1 == (char *)0x0) {
    sVar2 = strlen(pathname);
    if (len < sVar2 + 1) {
      iVar3 = 7;
    }
    else {
      vnode_incref(dir);
      *ret = dir;
      strcpy(buf,pathname);
      iVar3 = 0;
    }
  }
  else {
    *pcVar1 = '\0';
    sVar2 = strlen(pcVar1 + 1);
    if (len < sVar2 + 1) {
      iVar3 = 7;
    }
    else {
      strcpy(buf,pcVar1 + 1);
      iVar3 = emufs_lookup(dir,pathname,ret);
    }
  }
  return iVar3;
}



int emufs_creat(vnode *dir,char *name,bool excl,mode_t mode,vnode **ret)

{
  int iVar1;
  void *pvVar2;
  emufs_fs *ef;
  emufs_vnode *newguy;
  uint32_t handle;
  int isdir;
  
  pvVar2 = dir->vn_data;
  ef = (emufs_fs *)dir->vn_fs->fs_data;
  iVar1 = emu_open(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),name,true,
                   excl,mode,&handle,&isdir);
  if (iVar1 == 0) {
    iVar1 = emufs_loadvnode(ef,handle,isdir,&newguy);
    if (iVar1 == 0) {
      *ret = &newguy->ev_v;
      iVar1 = 0;
    }
    else {
      emu_close(*(emu_softc **)((int)pvVar2 + 0x18),handle);
    }
  }
  return iVar1;
}



void emu_irq(void *dev)

{
  uint32_t uVar1;
  
                    /* WARNING: Load size is inaccurate */
  uVar1 = lamebus_read_register(*dev,*(int *)((int)dev + 4),0x10);
  *(uint32_t *)((int)dev + 0x18) = uVar1;
                    /* WARNING: Load size is inaccurate */
  lamebus_write_register(*dev,*(int *)((int)dev + 4),0x10,0);
  V(*(semaphore **)((int)dev + 0x10));
  return;
}



int config_emu(emu_softc *sc,int emuno)

{
  lock *plVar1;
  semaphore *psVar2;
  int iVar3;
  void *pvVar4;
  char name [32];
  
  plVar1 = lock_create("emufs-lock");
  sc->e_lock = plVar1;
  if (plVar1 == (lock *)0x0) {
    iVar3 = 3;
  }
  else {
    psVar2 = sem_create("emufs-sem",0);
    sc->e_sem = psVar2;
    if (psVar2 == (semaphore *)0x0) {
      lock_destroy(sc->e_lock);
      sc->e_lock = (lock *)0x0;
      iVar3 = 3;
    }
    else {
      pvVar4 = lamebus_map_area((lamebus_softc *)sc->e_busdata,sc->e_buspos,0x8000);
      sc->e_iobuf = pvVar4;
      snprintf(name,0x20,"emu%d",emuno);
      iVar3 = emufs_addtovfs(sc,name);
    }
  }
  return iVar3;
}



void lamebus_find_cpus(lamebus_softc *lamebus)

{
  uint32_t uVar1;
  uint32_t uVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  uint uVar6;
  int unaff_s7;
  uint hwnum [32];
  
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7c00);
  uVar2 = lamebus_read_register(lamebus,0x1f,0x7c04);
  if ((uVar1 == 1) && (uVar2 == 1)) {
    lamebus->ls_uniprocessor = 1;
  }
  else {
    uVar1 = lamebus_read_register(lamebus,0x1f,0x7e10);
    uVar2 = lamebus_read_register(lamebus,0x1f,0x7e18);
    uVar6 = 0;
    uVar5 = 0;
    for (uVar3 = 0; uVar4 = 1 << (uVar3 & 0x1f), uVar3 < 0x20; uVar3 = uVar3 + 1) {
      if ((uVar4 & uVar1) != 0) {
        if ((uVar4 & uVar2) != 0) {
          *(uint *)(*(int *)(unaff_s7 + 0x50) + 8) = uVar3;
          uVar6 = uVar5;
        }
        hwnum[uVar5] = uVar3;
        uVar5 = uVar5 + 1;
      }
    }
    for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
      if (uVar3 != uVar6) {
        cpu_create(hwnum[uVar3]);
      }
    }
    for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
      uVar1 = 0;
      if (uVar3 == uVar6) {
        uVar1 = 0xffffffff;
      }
      lamebus_write_register(lamebus,0x1f,(hwnum[uVar3] + 0x20) * 0x400,uVar1);
    }
  }
  return;
}



void lamebus_start_cpus(lamebus_softc *lamebus)

{
  uint32_t val;
  uint32_t uVar1;
  code **ppcVar2;
  uint uVar3;
  uint uVar4;
  code *pcVar5;
  
  if (lamebus->ls_uniprocessor == 0) {
    val = lamebus_read_register(lamebus,0x1f,0x7e10);
    uVar1 = lamebus_read_register(lamebus,0x1f,0x7e18);
    pcVar5 = (code *)0x1;
    for (uVar4 = 0; uVar3 = 1 << (uVar4 & 0x1f), uVar4 < 0x20; uVar4 = uVar4 + 1) {
      if (((uVar3 & val) != 0) && ((uVar3 & uVar1) == 0)) {
        ppcVar2 = (code **)lamebus_map_area(lamebus,0x1f,(uVar4 + 0x20) * 0x400 + 0x300);
        *ppcVar2 = cpu_start_secondary;
        ppcVar2[1] = pcVar5;
        pcVar5 = pcVar5 + 1;
      }
    }
    SYNC(0);
    lamebus_write_register(lamebus,0x1f,0x7e14,val);
  }
  return;
}



int lamebus_probe(lamebus_softc *sc,uint32_t vendorid,uint32_t deviceid,uint32_t lowver,
                 uint32_t *version_ret)

{
  uint32_t uVar1;
  uint uVar2;
  uint32_t uVar3;
  
  spinlock_acquire(&sc->ls_lock);
  uVar2 = 0;
  while( true ) {
    if (0x1f < (int)uVar2) {
      spinlock_release(&sc->ls_lock);
      return -1;
    }
    uVar3 = uVar2 * 0x400;
    if (((((sc->ls_slotsinuse & 1 << (uVar2 & 0x1f)) == 0) &&
         (uVar1 = lamebus_read_register(sc,0x1f,uVar3), vendorid == uVar1)) &&
        (uVar1 = lamebus_read_register(sc,0x1f,uVar3 + 4), deviceid == uVar1)) &&
       (uVar3 = lamebus_read_register(sc,0x1f,uVar3 + 8), lowver <= uVar3)) break;
    uVar2 = uVar2 + 1;
  }
  if (version_ret != (uint32_t *)0x0) {
    *version_ret = uVar3;
  }
  spinlock_release(&sc->ls_lock);
  return uVar2;
}



void lamebus_mark(lamebus_softc *sc,int slot)

{
  char *pcVar1;
  uint uVar2;
  
  uVar2 = 1 << (slot & 0x1fU);
  if (0x1f < (uint)slot) {
    pcVar1 = "slot>=0 && slot < LB_NSLOTS";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x139,"lamebus_mark");
    sc = (lamebus_softc *)pcVar1;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & uVar2) != 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_mark: slot %d already in use\n",slot);
  }
  sc->ls_slotsinuse = uVar2 | sc->ls_slotsinuse;
  spinlock_release(&sc->ls_lock);
  return;
}



void lamebus_unmark(lamebus_softc *sc,int slot)

{
  char *pcVar1;
  uint uVar2;
  
  uVar2 = 1 << (slot & 0x1fU);
  if (0x1f < (uint)slot) {
    pcVar1 = "slot>=0 && slot < LB_NSLOTS";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x14d,"lamebus_unmark");
    sc = (lamebus_softc *)pcVar1;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & uVar2) == 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_mark: slot %d not marked in use\n",slot);
  }
  sc->ls_slotsinuse = sc->ls_slotsinuse & ~uVar2;
  spinlock_release(&sc->ls_lock);
  return;
}



void lamebus_attach_interrupt
               (lamebus_softc *sc,int slot,void *devdata,anon_subr_void_void_ptr *irqfunc)

{
  int iVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  
  if (0x1f < (uint)slot) {
    pcVar3 = "slot>=0 && slot < LB_NSLOTS";
    devdata = (void *)0x164;
    pcVar4 = "lamebus_attach_interrupt";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x164,
              "lamebus_attach_interrupt");
    sc = (lamebus_softc *)pcVar3;
    irqfunc = (anon_subr_void_void_ptr *)pcVar4;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & 1 << (slot & 0x1fU)) == 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_attach_interrupt: slot %d not marked in use\n",slot);
  }
  iVar1 = slot + 0x22;
  if (sc->ls_devdata[slot] != (void *)0x0) {
    badassert("sc->ls_devdata[slot]==NULL","../../dev/lamebus/lamebus.c",0x16d,
              "lamebus_attach_interrupt");
  }
  iVar2 = slot + 2;
  if (sc->ls_devdata[iVar1 + -2] != (void *)0x0) {
    badassert("sc->ls_irqfuncs[slot]==NULL","../../dev/lamebus/lamebus.c",0x16e,
              "lamebus_attach_interrupt");
  }
  sc->ls_devdata[iVar2 + -2] = devdata;
  sc->ls_irqfuncs[slot] = (lb_irqfunc *)irqfunc;
  spinlock_release(&sc->ls_lock);
  return;
}



void lamebus_detach_interrupt(lamebus_softc *sc,int slot)

{
  int iVar1;
  char *pcVar2;
  
  if (0x1f < (uint)slot) {
    pcVar2 = "slot>=0 && slot < LB_NSLOTS";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x17e,
              "lamebus_detach_interrupt");
    sc = (lamebus_softc *)pcVar2;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & 1 << (slot & 0x1fU)) == 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_detach_interrupt: slot %d not marked in use\n",slot);
  }
  iVar1 = slot + 2;
  if (sc->ls_irqfuncs[slot] == (lb_irqfunc *)0x0) {
    badassert("sc->ls_irqfuncs[slot]!=NULL","../../dev/lamebus/lamebus.c",0x187,
              "lamebus_detach_interrupt");
  }
  sc->ls_devdata[iVar1 + -2] = (void *)0x0;
  sc->ls_irqfuncs[slot] = (lb_irqfunc *)0x0;
  spinlock_release(&sc->ls_lock);
  return;
}



void lamebus_mask_interrupt(lamebus_softc *lamebus,int slot)

{
  uint32_t uVar1;
  char *pcVar2;
  
  if (0x1f < (uint)slot) {
    pcVar2 = "slot >= 0 && slot < LB_NSLOTS";
    badassert("slot >= 0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x196,
              "lamebus_mask_interrupt");
    lamebus = (lamebus_softc *)pcVar2;
  }
  spinlock_acquire(&lamebus->ls_lock);
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7e0c);
  lamebus_write_register(lamebus,0x1f,0x7e0c,~(1 << (slot & 0x1fU)) & uVar1);
  spinlock_release(&lamebus->ls_lock);
  return;
}



void lamebus_unmask_interrupt(lamebus_softc *lamebus,int slot)

{
  uint32_t uVar1;
  char *pcVar2;
  
  if (0x1f < (uint)slot) {
    pcVar2 = "slot >= 0 && slot < LB_NSLOTS";
    badassert("slot >= 0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x1a3,
              "lamebus_unmask_interrupt");
    lamebus = (lamebus_softc *)pcVar2;
  }
  spinlock_acquire(&lamebus->ls_lock);
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7e0c);
  lamebus_write_register(lamebus,0x1f,0x7e0c,1 << (slot & 0x1fU) | uVar1);
  spinlock_release(&lamebus->ls_lock);
  return;
}



void lamebus_interrupt(lamebus_softc *lamebus)

{
  uint32_t uVar1;
  char *splk;
  uint uVar2;
  int iVar3;
  void *pvVar4;
  uint uVar5;
  int unaff_s7;
  lb_irqfunc *plVar6;
  
  splk = (char *)lamebus;
  if (lamebus == (lamebus_softc *)0x0) {
    splk = "lamebus != NULL";
    badassert("lamebus != NULL","../../dev/lamebus/lamebus.c",0x1c9,"lamebus_interrupt");
  }
  spinlock_acquire((spinlock *)splk);
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7e04);
  if (uVar1 == 0) {
    kprintf("lamebus: stray interrupt on cpu %u\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
  }
  uVar5 = (uint)(uVar1 == 0);
  uVar2 = 1;
  for (iVar3 = 0; iVar3 < 0x20; iVar3 = iVar3 + 1) {
    if ((uVar1 & uVar2) != 0) {
      if ((lamebus->ls_slotsinuse & uVar2) == 0) {
        lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
        uVar5 = uVar5 + 1;
      }
      else {
        plVar6 = lamebus->ls_irqfuncs[iVar3];
        if (plVar6 == (lb_irqfunc *)0x0) {
          lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
          uVar5 = uVar5 + 1;
        }
        else {
          pvVar4 = lamebus->ls_devdata[iVar3];
          spinlock_release((spinlock *)splk);
          (*plVar6)(pvVar4);
          spinlock_acquire((spinlock *)splk);
          uVar1 = lamebus_read_register(lamebus,0x1f,0x7e04);
        }
      }
    }
    uVar2 = uVar2 << 1;
  }
  if ((uVar5 == 0) && (0 < lamebus_interrupt::duds)) {
    kprintf("lamebus: %d dud interrupts\n");
    lamebus_interrupt::duds = 0;
  }
  if (lamebus_interrupt::duds < 0x2711) {
    spinlock_release(&lamebus->ls_lock);
    return;
  }
                    /* WARNING: Subroutine does not return */
  panic("lamebus: too many (%d) dud interrupts\n");
}



void lamebus_poweroff(lamebus_softc *lamebus)

{
  cpu_irqoff();
  lamebus_write_register(lamebus,0x1f,0x7e08,0);
  cpu_halt();
  return;
}



uint32_t lamebus_ramsize(void)

{
  uint32_t uVar1;
  
  uVar1 = lamebus_read_register((lamebus_softc *)0x0,0x1f,0x7e00);
  return uVar1;
}



void lamebus_assert_ipi(lamebus_softc *lamebus,cpu *target)

{
  if (lamebus->ls_uniprocessor == 0) {
    lamebus_write_register(lamebus,0x1f,(target->c_hardware_number + 0x20) * 0x400 + 4,1);
  }
  return;
}



void lamebus_clear_ipi(lamebus_softc *lamebus,cpu *target)

{
  if (lamebus->ls_uniprocessor == 0) {
    lamebus_write_register(lamebus,0x1f,(target->c_hardware_number + 0x20) * 0x400 + 4,0);
  }
  return;
}



lamebus_softc * lamebus_init(void)

{
  lamebus_softc *splk;
  int iVar1;
  
  splk = (lamebus_softc *)kmalloc(0x110);
  if (splk == (lamebus_softc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_init: Out of memory\n");
  }
  spinlock_init((spinlock *)splk);
  splk->ls_slotsinuse = 0x80000000;
  for (iVar1 = 0; iVar1 < 0x20; iVar1 = iVar1 + 1) {
    splk->ls_devdata[iVar1] = (void *)0x0;
    splk->ls_irqfuncs[iVar1] = (lb_irqfunc *)0x0;
  }
  splk->ls_uniprocessor = 0;
  return splk;
}



lhd_softc * attach_lhd_to_lamebus(int lhdno,lamebus_softc *sc)

{
  uint32_t slot;
  lhd_softc *devdata;
  
  slot = lamebus_probe(sc,1,3,2,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (lhd_softc *)0x0;
  }
  else {
    devdata = (lhd_softc *)kmalloc(0x30);
    if (devdata == (lhd_softc *)0x0) {
      devdata = (lhd_softc *)0x0;
    }
    else {
      devdata->lh_busdata = sc;
      devdata->lh_buspos = slot;
      devdata->lh_unit = lhdno;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,lhd_irq);
    }
  }
  return devdata;
}



int lhd_eachopen(device *d,int openflags)

{
  return 0;
}



int lhd_ioctl(device *d,int op,userptr_t data)

{
  return 0x1f;
}



int lhd_code_to_errno(lhd_softc *lh,int code)

{
  uint uVar1;
  
  uVar1 = code & 0x1d;
  if (uVar1 == 0xc) {
    return 8;
  }
  if (uVar1 != 0x14) {
    if (uVar1 == 4) {
      return 0;
    }
    kprintf("lhd%d: Unknown result code %d\n",lh->lh_unit,code);
    return 4;
  }
  return 0x20;
}



void lhd_iodone(lhd_softc *lh,int err)

{
  lh->lh_result = err;
  V(lh->lh_done);
  return;
}



int lhd_io(device *d,uio *uio)

{
  int iVar1;
  lamebus_softc *plVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  lamebus_softc **pplVar6;
  uint uVar7;
  uint32_t val;
  
  pplVar6 = (lamebus_softc **)d->d_data;
  iVar1 = *(int *)&uio->uio_offset;
  uVar3 = *(uint *)((int)&uio->uio_offset + 4);
  uVar4 = uVar3;
  iVar5 = iVar1;
  if (iVar1 < 0) {
    uVar4 = uVar3 + 0x1ff;
    iVar5 = (uint)(uVar4 < uVar3) + iVar1;
  }
  uVar4 = iVar5 << 0x17 | uVar4 >> 9;
  uVar3 = uVar3 & 0x1ff;
  if (iVar1 < 0) {
    uVar3 = (uVar3 - 1 | 0xfffffe00) + 1;
  }
  uVar7 = uio->uio_resid >> 9;
  if (uVar3 == 0) {
    plVar2 = (lamebus_softc *)&DAT_00000008;
    if (((uio->uio_resid & 0x1ff) == 0) &&
       (plVar2 = (lamebus_softc *)&DAT_00000008, (lamebus_softc *)(uVar4 + uVar7) <= pplVar6[8])) {
      if (uio->uio_rw == UIO_WRITE) {
        val = 3;
      }
      else {
        val = 1;
      }
      uVar3 = 0;
      do {
        if (uVar7 <= uVar3) {
          return 0;
        }
        P((semaphore *)pplVar6[5]);
        if (uio->uio_rw == UIO_WRITE) {
          iVar5 = uiomove(pplVar6[3],0x200,uio);
          SYNC(0);
          if (iVar5 != 0) {
            V((semaphore *)pplVar6[5]);
            return iVar5;
          }
        }
        lamebus_write_register(*pplVar6,(int)pplVar6[1],8,uVar4 + uVar3);
        lamebus_write_register(*pplVar6,(int)pplVar6[1],4,val);
        P((semaphore *)pplVar6[6]);
        plVar2 = pplVar6[4];
        if ((plVar2 == (lamebus_softc *)0x0) && (uio->uio_rw == UIO_READ)) {
          SYNC(0);
          plVar2 = (lamebus_softc *)uiomove(pplVar6[3],0x200,uio);
        }
        V((semaphore *)pplVar6[5]);
        uVar3 = uVar3 + 1;
      } while (plVar2 == (lamebus_softc *)0x0);
    }
  }
  else {
    plVar2 = (lamebus_softc *)&DAT_00000008;
  }
  return (int)plVar2;
}



void lhd_irq(void *vlh)

{
  uint32_t code;
  int err;
  uint uVar1;
  
                    /* WARNING: Load size is inaccurate */
  code = lamebus_read_register(*vlh,*(int *)((int)vlh + 4),4);
  uVar1 = code & 0x1d;
  if (((uVar1 == 0xc) || (uVar1 == 0x14)) || (uVar1 == 4)) {
                    /* WARNING: Load size is inaccurate */
    lamebus_write_register(*vlh,*(int *)((int)vlh + 4),4,0);
    err = lhd_code_to_errno((lhd_softc *)vlh,code);
    lhd_iodone((lhd_softc *)vlh,err);
  }
  return;
}



int config_lhd(lhd_softc *lh,int lhdno)

{
  void *pvVar1;
  semaphore *psVar2;
  int iVar3;
  uint32_t uVar4;
  char name [32];
  
  snprintf(name,0x20,"lhd%d",lhdno);
  pvVar1 = lamebus_map_area((lamebus_softc *)lh->lh_busdata,lh->lh_buspos,0x8000);
  lh->lh_buf = pvVar1;
  psVar2 = sem_create("lhd-clear",1);
  lh->lh_clear = psVar2;
  if (psVar2 == (semaphore *)0x0) {
    iVar3 = 3;
  }
  else {
    psVar2 = sem_create("lhd-done",0);
    lh->lh_done = psVar2;
    if (psVar2 == (semaphore *)0x0) {
      sem_destroy(lh->lh_clear);
      lh->lh_clear = (semaphore *)0x0;
      iVar3 = 3;
    }
    else {
      (lh->lh_dev).d_ops = &lhd_devops;
      uVar4 = lamebus_read_register((lamebus_softc *)lh->lh_busdata,lh->lh_buspos,0);
      (lh->lh_dev).d_blocks = uVar4;
      (lh->lh_dev).d_blocksize = 0x200;
      (lh->lh_dev).d_data = lh;
      iVar3 = vfs_adddev(name,&lh->lh_dev,1);
    }
  }
  return iVar3;
}



lrandom_softc * attach_lrandom_to_lamebus(int lrandomno,lamebus_softc *sc)

{
  uint32_t slot;
  lrandom_softc *plVar1;
  
  slot = lamebus_probe(sc,1,9,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    plVar1 = (lrandom_softc *)0x0;
  }
  else {
    plVar1 = (lrandom_softc *)kmalloc(8);
    if (plVar1 == (lrandom_softc *)0x0) {
      plVar1 = (lrandom_softc *)0x0;
    }
    else {
      plVar1->lr_bus = sc;
      plVar1->lr_buspos = slot;
      lamebus_mark(sc,slot);
    }
  }
  return plVar1;
}



int config_lrandom(lrandom_softc *lr,int lrandomno)

{
  return 0;
}



uint32_t lrandom_random(void *devdata)

{
  uint32_t uVar1;
  
                    /* WARNING: Load size is inaccurate */
  uVar1 = lamebus_read_register(*devdata,*(int *)((int)devdata + 4),0);
  return uVar1;
}



uint32_t lrandom_randmax(void *devdata)

{
  return 0xffffffff;
}



int lrandom_read(void *devdata,uio *uio)

{
  int iVar1;
  uint32_t val;
  
  do {
    if (uio->uio_resid == 0) {
      return 0;
    }
                    /* WARNING: Load size is inaccurate */
    val = lamebus_read_register(*devdata,*(int *)((int)devdata + 4),0);
    iVar1 = uiomove(&val,4,uio);
  } while (iVar1 == 0);
  return iVar1;
}



lser_softc * attach_lser_to_lamebus(int lserno,lamebus_softc *sc)

{
  uint32_t slot;
  lser_softc *devdata;
  
  slot = lamebus_probe(sc,1,4,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (lser_softc *)0x0;
  }
  else {
    devdata = (lser_softc *)kmalloc(0x20);
    if (devdata == (lser_softc *)0x0) {
      devdata = (lser_softc *)0x0;
    }
    else {
      devdata->ls_busdata = sc;
      devdata->ls_buspos = slot;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,lser_irq);
    }
  }
  return devdata;
}



void lser_poll_until_write(lser_softc *sc)

{
  undefined3 extraout_var;
  bool bVar2;
  uint32_t uVar1;
  
  bVar2 = spinlock_do_i_hold(&sc->ls_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_do_i_hold(&sc->ls_lock)","../../dev/lamebus/lser.c",0x79,
              "lser_poll_until_write");
  }
  do {
    uVar1 = lamebus_read_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,4);
  } while ((uVar1 & 2) == 0);
  return;
}



void lser_irq(void *vsc)

{
  bool bVar1;
  bool bVar2;
  uint32_t uVar3;
  
  spinlock_acquire((spinlock *)vsc);
  uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4);
  bVar1 = (uVar3 & 2) != 0;
  if (bVar1) {
    *(undefined *)((int)vsc + 8) = 0;
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,1);
  }
  uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),8);
  bVar2 = (uVar3 & 2) == 0;
  if (bVar2) {
    uVar3 = 0;
  }
  else {
    uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),0);
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),8,1);
  }
  spinlock_release((spinlock *)vsc);
  if ((bVar1) && (*(code **)((int)vsc + 0x18) != (code *)0x0)) {
    (**(code **)((int)vsc + 0x18))(*(undefined4 *)((int)vsc + 0x14));
  }
  if ((!bVar2) && (*(code **)((int)vsc + 0x1c) != (code *)0x0)) {
    (**(code **)((int)vsc + 0x1c))(*(undefined4 *)((int)vsc + 0x14),uVar3);
  }
  return;
}



void lser_write(void *vls,int ch)

{
  spinlock_acquire((spinlock *)vls);
  if (*(char *)((int)vls + 8) != '\0') {
                    /* WARNING: Subroutine does not return */
    panic("lser: Not clear to write\n");
  }
  *(undefined *)((int)vls + 8) = 1;
  lamebus_write_register(*(lamebus_softc **)((int)vls + 0xc),*(int *)((int)vls + 0x10),0,ch);
  spinlock_release((spinlock *)vls);
  return;
}



void lser_writepolled(void *vsc,int ch)

{
  bool bVar1;
  
  spinlock_acquire((spinlock *)vsc);
  bVar1 = *(char *)((int)vsc + 8) == '\0';
  if (bVar1) {
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,0);
  }
  else {
    lser_poll_until_write((lser_softc *)vsc);
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,5);
  }
  lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),0,ch);
  lser_poll_until_write((lser_softc *)vsc);
  if (bVar1) {
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,1);
  }
  spinlock_release((spinlock *)vsc);
  return;
}



int config_lser(lser_softc *sc,int lserno)

{
  spinlock_init(&sc->ls_lock);
  sc->ls_wbusy = false;
  lamebus_write_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,8,1);
  lamebus_write_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,4,1);
  return 0;
}



ltimer_softc * attach_ltimer_to_lamebus(int ltimerno,lamebus_softc *sc)

{
  uint32_t slot;
  ltimer_softc *devdata;
  
  slot = lamebus_probe(sc,1,2,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (ltimer_softc *)0x0;
  }
  else {
    devdata = (ltimer_softc *)kmalloc(0x10);
    if (devdata == (ltimer_softc *)0x0) {
      devdata = (ltimer_softc *)0x0;
    }
    else {
      devdata->lt_bus = sc;
      devdata->lt_buspos = slot;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,ltimer_irq);
    }
  }
  return devdata;
}



int config_ltimer(ltimer_softc *lt,int ltimerno)

{
  lt->lt_hardclock = 0;
  if (havetimerclock == false) {
    havetimerclock = true;
    lt->lt_timerclock = 1;
    lamebus_write_register((lamebus_softc *)lt->lt_bus,lt->lt_buspos,8,1);
    lamebus_write_register((lamebus_softc *)lt->lt_bus,lt->lt_buspos,0x10,1000000);
  }
  return 0;
}



void ltimer_irq(void *vlt)

{
  uint32_t uVar1;
  
  uVar1 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0xc);
  if (uVar1 != 0) {
                    /* WARNING: Load size is inaccurate */
    if (*vlt != 0) {
      hardclock();
    }
    if (*(int *)((int)vlt + 4) != 0) {
      timerclock();
    }
  }
  return;
}



void ltimer_beep(void *vlt)

{
  lamebus_write_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0x14,0x1b8);
  return;
}



void ltimer_gettime(void *vlt,timespec *ts)

{
  int spl;
  uint32_t uVar1;
  uint32_t uVar2;
  
  spl = splx(1);
  uVar1 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0);
  uVar2 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),4);
  ts->tv_nsec = uVar2;
  uVar2 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0);
  splx(spl);
  if (ts->tv_nsec < 5000000) {
    *(uint32_t *)((int)&ts->tv_sec + 4) = uVar2;
    *(undefined4 *)&ts->tv_sec = 0;
  }
  else {
    *(uint32_t *)((int)&ts->tv_sec + 4) = uVar1;
    *(undefined4 *)&ts->tv_sec = 0;
  }
  return;
}



ltrace_softc * attach_ltrace_to_lamebus(int ltraceno,lamebus_softc *sc)

{
  uint32_t slot;
  ltrace_softc *plVar1;
  uint32_t drl;
  
  slot = lamebus_probe(sc,1,8,1,&drl);
  if ((int)slot < 0) {
    plVar1 = (ltrace_softc *)0x0;
  }
  else {
    plVar1 = (ltrace_softc *)kmalloc(0xc);
    if (plVar1 == (ltrace_softc *)0x0) {
      plVar1 = (ltrace_softc *)0x0;
    }
    else {
      plVar1->lt_busdata = sc;
      plVar1->lt_buspos = slot;
      plVar1->lt_canstop = 1 < drl;
      plVar1->lt_canprof = 2 < drl;
      lamebus_mark(sc,slot);
    }
  }
  return plVar1;
}



void ltrace_on(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0,code);
  }
  return;
}



void ltrace_off(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,4,code);
  }
  return;
}



void ltrace_debug(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,8,code);
  }
  return;
}



void ltrace_dump(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0xc,code);
  }
  return;
}



void ltrace_stop(uint32_t code)

{
  if ((the_trace != (ltrace_softc *)0x0) && (the_trace->lt_canstop != false)) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0x10,code);
  }
  return;
}



void ltrace_setprof(uint32_t onoff)

{
  if ((the_trace != (ltrace_softc *)0x0) && (the_trace->lt_canprof != false)) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0x14,onoff);
  }
  return;
}



void ltrace_eraseprof(void)

{
  if ((the_trace != (ltrace_softc *)0x0) && (the_trace->lt_canprof != false)) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0x18,1);
  }
  return;
}



int config_ltrace(ltrace_softc *sc,int ltraceno)

{
  the_trace = sc;
  return 0;
}



random_softc * attach_random_to_lrandom(int randomno,lrandom_softc *ls)

{
  random_softc *prVar1;
  
  prVar1 = (random_softc *)kmalloc(0x24);
  if (prVar1 == (random_softc *)0x0) {
    prVar1 = (random_softc *)0x0;
  }
  else {
    prVar1->rs_devdata = ls;
    prVar1->rs_random = lrandom_random;
    prVar1->rs_randmax = lrandom_randmax;
    prVar1->rs_read = lrandom_read;
  }
  return prVar1;
}



rtclock_softc * attach_rtclock_to_ltimer(int rtclockno,ltimer_softc *ls)

{
  rtclock_softc *prVar1;
  
  prVar1 = (rtclock_softc *)kmalloc(8);
  if (prVar1 == (rtclock_softc *)0x0) {
    prVar1 = (rtclock_softc *)0x0;
  }
  else {
    prVar1->rtc_devdata = ls;
    prVar1->rtc_gettime = ltimer_gettime;
  }
  return prVar1;
}



int semfs_sync(fs *fs)

{
  return 0;
}



char * semfs_getvolname(fs *fs)

{
  return "sem";
}



void semfs_destroy(semfs *semfs)

{
  semfs_semarray *psVar1;
  semfs_direntryarray *psVar2;
  uint uVar3;
  array *paVar4;
  uint uVar5;
  
  uVar5 = (semfs->semfs_sems->arr).num;
  for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
    psVar1 = semfs->semfs_sems;
    if ((psVar1->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    semfs_sem_destroy((semfs_sem *)(psVar1->arr).v[uVar3]);
  }
  array_setsize((array *)semfs->semfs_sems,0);
  uVar5 = (semfs->semfs_dents->arr).num;
  for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
    psVar2 = semfs->semfs_dents;
    if ((psVar2->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    semfs_direntry_destroy((semfs_direntry *)(psVar2->arr).v[uVar3]);
  }
  array_setsize((array *)semfs->semfs_dents,0);
  paVar4 = (array *)semfs->semfs_dents;
  array_cleanup(paVar4);
  kfree(paVar4);
  lock_destroy(semfs->semfs_dirlock);
  paVar4 = (array *)semfs->semfs_sems;
  array_cleanup(paVar4);
  kfree(paVar4);
  paVar4 = (array *)semfs->semfs_vnodes;
  array_cleanup(paVar4);
  kfree(paVar4);
  lock_destroy(semfs->semfs_tablelock);
  kfree(semfs);
  return;
}



int semfs_unmount(fs *fs)

{
  int iVar1;
  semfs *semfs;
  
  semfs = (semfs *)fs->fs_data;
  lock_acquire(semfs->semfs_tablelock);
  if ((semfs->semfs_vnodes->arr).num == 0) {
    lock_release(semfs->semfs_tablelock);
    semfs_destroy(semfs);
    iVar1 = 0;
  }
  else {
    lock_release(semfs->semfs_tablelock);
    iVar1 = 0x1b;
  }
  return iVar1;
}



int semfs_getroot(fs *fs,vnode **ret)

{
  int errcode;
  char *pcVar1;
  vnode *vn;
  
  errcode = semfs_getvnode((semfs *)fs->fs_data,0xffffffff,&vn);
  if (errcode == 0) {
    *ret = vn;
    errcode = 0;
  }
  else {
    pcVar1 = strerror(errcode);
    kprintf("semfs: couldn\'t load root vnode: %s\n",pcVar1);
  }
  return errcode;
}



semfs * semfs_create(void)

{
  semfs *ptr;
  lock *plVar1;
  array *paVar2;
  
  ptr = (semfs *)kmalloc(0x1c);
  if (ptr != (semfs *)0x0) {
    plVar1 = lock_create("semfs_table");
    ptr->semfs_tablelock = plVar1;
    if (plVar1 != (lock *)0x0) {
      paVar2 = (array *)kmalloc(0xc);
      if (paVar2 == (array *)0x0) {
        paVar2 = (array *)0x0;
        ptr->semfs_vnodes = (vnodearray *)0x0;
      }
      else {
        array_init(paVar2);
        ptr->semfs_vnodes = (vnodearray *)paVar2;
      }
      if (paVar2 != (array *)0x0) {
        paVar2 = (array *)kmalloc(0xc);
        if (paVar2 == (array *)0x0) {
          paVar2 = (array *)0x0;
          ptr->semfs_sems = (semfs_semarray *)0x0;
        }
        else {
          array_init(paVar2);
          ptr->semfs_sems = (semfs_semarray *)paVar2;
        }
        if (paVar2 != (array *)0x0) {
          plVar1 = lock_create("semfs_dir");
          ptr->semfs_dirlock = plVar1;
          if (plVar1 != (lock *)0x0) {
            paVar2 = (array *)kmalloc(0xc);
            if (paVar2 == (array *)0x0) {
              paVar2 = (array *)0x0;
              ptr->semfs_dents = (semfs_direntryarray *)0x0;
            }
            else {
              array_init(paVar2);
              ptr->semfs_dents = (semfs_direntryarray *)paVar2;
            }
            if (paVar2 != (array *)0x0) {
              (ptr->semfs_absfs).fs_data = ptr;
              (ptr->semfs_absfs).fs_ops = &semfs_fsops;
              return ptr;
            }
            lock_destroy(ptr->semfs_dirlock);
          }
          paVar2 = (array *)ptr->semfs_sems;
          array_cleanup(paVar2);
          kfree(paVar2);
        }
        paVar2 = (array *)ptr->semfs_vnodes;
        array_cleanup(paVar2);
        kfree(paVar2);
      }
      lock_destroy(ptr->semfs_tablelock);
    }
    kfree(ptr);
  }
  return (semfs *)0x0;
}



void semfs_bootstrap(void)

{
  semfs *fs;
  int errcode;
  char *pcVar1;
  
  fs = semfs_create();
  if (fs == (semfs *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Out of memory creating semfs\n");
  }
  errcode = vfs_addfs("sem",&fs->semfs_absfs);
  if (errcode != 0) {
    pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
    panic("Attaching semfs: %s\n",pcVar1);
  }
  return;
}



semfs_semarray * semfs_semarray_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  return (semfs_semarray *)a;
}



void semfs_semarray_destroy(semfs_semarray *a)

{
  array_cleanup(&a->arr);
  kfree(a);
  return;
}



void semfs_semarray_init(semfs_semarray *a)

{
  array_init(&a->arr);
  return;
}



void semfs_semarray_cleanup(semfs_semarray *a)

{
  array_cleanup(&a->arr);
  return;
}



uint semfs_semarray_num(semfs_semarray *a)

{
  return (a->arr).num;
}



semfs_sem * semfs_semarray_get(semfs_semarray *a,uint index)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    badassert("index < a->num","../../include/array.h",100,"array_get");
    a = (semfs_semarray *)pcVar1;
  }
  return *(semfs_sem **)(pcVar2 + (int)(a->arr).v);
}



void semfs_semarray_set(semfs_semarray *a,uint index,semfs_sem *val)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    val = (semfs_sem *)0x6b;
    badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    a = (semfs_semarray *)pcVar1;
  }
  *(semfs_sem **)(pcVar2 + (int)(a->arr).v) = val;
  return;
}



int semfs_semarray_preallocate(semfs_semarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(&a->arr,num);
  return iVar1;
}



int semfs_semarray_setsize(semfs_semarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_setsize(&a->arr,num);
  return iVar1;
}



int semfs_semarray_add(semfs_semarray *a,semfs_sem *val,uint *index_ret)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = (a->arr).num;
  iVar1 = array_setsize(&a->arr,uVar2 + 1);
  if (iVar1 == 0) {
    (a->arr).v[uVar2] = val;
    if (index_ret == (uint *)0x0) {
      iVar1 = 0;
    }
    else {
      *index_ret = uVar2;
      iVar1 = 0;
    }
  }
  return iVar1;
}



void semfs_semarray_remove(semfs_semarray *a,uint index)

{
  array_remove(&a->arr,index);
  return;
}



semfs_direntryarray * semfs_direntryarray_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  return (semfs_direntryarray *)a;
}



void semfs_direntryarray_destroy(semfs_direntryarray *a)

{
  array_cleanup(&a->arr);
  kfree(a);
  return;
}



void semfs_direntryarray_init(semfs_direntryarray *a)

{
  array_init(&a->arr);
  return;
}



void semfs_direntryarray_cleanup(semfs_direntryarray *a)

{
  array_cleanup(&a->arr);
  return;
}



uint semfs_direntryarray_num(semfs_direntryarray *a)

{
  return (a->arr).num;
}



semfs_direntry * semfs_direntryarray_get(semfs_direntryarray *a,uint index)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    badassert("index < a->num","../../include/array.h",100,"array_get");
    a = (semfs_direntryarray *)pcVar1;
  }
  return *(semfs_direntry **)(pcVar2 + (int)(a->arr).v);
}



void semfs_direntryarray_set(semfs_direntryarray *a,uint index,semfs_direntry *val)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    val = (semfs_direntry *)0x6b;
    badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    a = (semfs_direntryarray *)pcVar1;
  }
  *(semfs_direntry **)(pcVar2 + (int)(a->arr).v) = val;
  return;
}



int semfs_direntryarray_preallocate(semfs_direntryarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(&a->arr,num);
  return iVar1;
}



int semfs_direntryarray_setsize(semfs_direntryarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_setsize(&a->arr,num);
  return iVar1;
}



int semfs_direntryarray_add(semfs_direntryarray *a,semfs_direntry *val,uint *index_ret)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = (a->arr).num;
  iVar1 = array_setsize(&a->arr,uVar2 + 1);
  if (iVar1 == 0) {
    (a->arr).v[uVar2] = val;
    if (index_ret == (uint *)0x0) {
      iVar1 = 0;
    }
    else {
      *index_ret = uVar2;
      iVar1 = 0;
    }
  }
  return iVar1;
}



void semfs_direntryarray_remove(semfs_direntryarray *a,uint index)

{
  array_remove(&a->arr,index);
  return;
}



semfs_sem * semfs_sem_create(char *name)

{
  semfs_sem *ptr;
  lock *plVar1;
  cv *pcVar2;
  char lockname [32];
  char cvname [32];
  
  snprintf(lockname,0x20,"sem:l.%s",name);
  snprintf(cvname,0x20,"sem:%s",name);
  ptr = (semfs_sem *)kmalloc(0x10);
  if (ptr != (semfs_sem *)0x0) {
    plVar1 = lock_create(lockname);
    ptr->sems_lock = plVar1;
    if (plVar1 != (lock *)0x0) {
      pcVar2 = cv_create(cvname);
      ptr->sems_cv = pcVar2;
      if (pcVar2 != (cv *)0x0) {
        ptr->sems_count = 0;
        ptr->sems_hasvnode = false;
        ptr->sems_linked = false;
        return ptr;
      }
      lock_destroy(ptr->sems_lock);
    }
    kfree(ptr);
  }
  return (semfs_sem *)0x0;
}



void semfs_sem_destroy(semfs_sem *sem)

{
  cv_destroy(sem->sems_cv);
  lock_destroy(sem->sems_lock);
  kfree(sem);
  return;
}



int semfs_sem_insert(semfs *semfs,semfs_sem *sem,uint *ret)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar5;
  uint uVar2;
  semfs_sem *psVar3;
  int iVar4;
  uint index;
  semfs_semarray *a;
  
  bVar5 = lock_do_i_hold(semfs->semfs_tablelock);
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("lock_do_i_hold(semfs->semfs_tablelock)","../../fs/semfs/semfs_obj.c",0x61,
              "semfs_sem_insert");
  }
  a = semfs->semfs_sems;
  uVar2 = semfs_semarray_num(a);
  if (uVar2 == 0xffffffff) {
    iVar4 = 0x24;
  }
  else {
    bVar1 = uVar2 != 0;
    index = 0;
    while (bVar1) {
      psVar3 = semfs_semarray_get(a,index);
      if (psVar3 == (semfs_sem *)0x0) {
        semfs_semarray_set(a,index,sem);
        *ret = index;
        return 0;
      }
      bVar1 = index + 1 < uVar2;
      index = index + 1;
    }
    iVar4 = semfs_semarray_add(a,sem,ret);
  }
  return iVar4;
}



semfs_direntry * semfs_direntry_create(char *name,uint semnum)

{
  semfs_direntry *ptr;
  char *pcVar1;
  
  ptr = (semfs_direntry *)kmalloc(8);
  if (ptr == (semfs_direntry *)0x0) {
    ptr = (semfs_direntry *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    ptr->semd_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(ptr);
      ptr = (semfs_direntry *)0x0;
    }
    else {
      ptr->semd_semnum = semnum;
    }
  }
  return ptr;
}



void semfs_direntry_destroy(semfs_direntry *dent)

{
  kfree(dent->semd_name);
  kfree(dent);
  return;
}



int semfs_eachopen(vnode *vn,int openflags)

{
  if (*(int *)((int)vn->vn_data + 0x1c) != -1) {
    return 0;
  }
  if ((openflags & 3U) == 0) {
    if ((openflags & 0x20U) == 0) {
      return 0;
    }
    return 0x12;
  }
  return 0x12;
}



int semfs_ioctl(vnode *vn,int op,userptr_t data)

{
  return 8;
}



int semfs_gettype(vnode *vn,mode_t *ret)

{
  mode_t mVar1;
  
  if (*(int *)((int)vn->vn_data + 0x1c) == -1) {
    mVar1 = 0x2000;
  }
  else {
    mVar1 = 0x1000;
  }
  *ret = mVar1;
  return 0;
}



bool semfs_isseekable(vnode *vn)

{
  if (*(int *)((int)vn->vn_data + 0x1c) != -1) {
    return false;
  }
  return true;
}



int semfs_fsync(vnode *vn)

{
  return 0;
}



int semfs_namefile(vnode *vn,uio *uio)

{
  return 0;
}



semfs_sem * semfs_getsembynum(semfs *semfs,uint semnum)

{
  semfs_semarray *psVar1;
  semfs_sem *psVar2;
  
  lock_acquire(semfs->semfs_tablelock);
  psVar1 = semfs->semfs_sems;
  if ((psVar1->arr).num <= semnum) {
    badassert("index < a->num","../../include/array.h",100,"array_get");
  }
  psVar2 = (semfs_sem *)(psVar1->arr).v[semnum];
  lock_release(semfs->semfs_tablelock);
  return psVar2;
}



semfs_sem * semfs_getsem(semfs_vnode *semv)

{
  semfs_sem *psVar1;
  
  psVar1 = semfs_getsembynum(semv->semv_semfs,semv->semv_semnum);
  return psVar1;
}



semfs_vnode * semfs_vnode_create(semfs *semfs,uint semnum)

{
  semfs_vnode *vn;
  int iVar1;
  vnode_ops *ops;
  
  if (semnum == 0xffffffff) {
    ops = &semfs_dirops;
  }
  else {
    ops = &semfs_semops;
  }
  vn = (semfs_vnode *)kmalloc(0x20);
  if (vn != (semfs_vnode *)0x0) {
    vn->semv_semfs = semfs;
    vn->semv_semnum = semnum;
    iVar1 = vnode_init((vnode *)vn,ops,&semfs->semfs_absfs,vn);
    if (iVar1 == 0) {
      return vn;
    }
    badassert("result == 0","../../fs/semfs/semfs_vnops.c",0x2e7,"semfs_vnode_create");
  }
  return (semfs_vnode *)0x0;
}



void semfs_wakeup(semfs_sem *sem,uint newcount)

{
  if ((sem->sems_count == 0) && (newcount != 0)) {
    if (newcount == 1) {
      cv_signal(sem->sems_cv,sem->sems_lock);
    }
    else {
      cv_broadcast(sem->sems_cv,sem->sems_lock);
    }
  }
  return;
}



int semfs_truncate(vnode *vn,off_t len)

{
  semfs_sem *sem;
  int iVar1;
  int in_a2;
  uint in_a3;
  
  if (-1 < in_a2) {
    if (in_a2 < 1) {
      sem = semfs_getsem((semfs_vnode *)vn->vn_data);
      lock_acquire(sem->sems_lock);
      semfs_wakeup(sem,in_a3);
      sem->sems_count = in_a3;
      lock_release(sem->sems_lock);
      iVar1 = 0;
    }
    else {
      iVar1 = 0x26;
    }
    return iVar1;
  }
  return 8;
}



int semfs_semstat(vnode *vn,stat *buf)

{
  semfs_sem *psVar1;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar1 = semfs_getsem(semv);
  bzero(buf,0x58);
  lock_acquire(psVar1->sems_lock);
  *(uint *)((int)&buf->st_size + 4) = psVar1->sems_count;
  *(undefined4 *)&buf->st_size = 0;
  buf->st_nlink = (ushort)psVar1->sems_linked;
  lock_release(psVar1->sems_lock);
  buf->st_mode = 0x11b6;
  buf->st_blocks = 0;
  buf->st_dev = 0;
  buf->st_ino = semv->semv_semnum;
  return 0;
}



int semfs_dirstat(vnode *vn,stat *buf)

{
  int iVar1;
  
  iVar1 = *(int *)((int)vn->vn_data + 0x18);
  bzero(buf,0x58);
  lock_acquire(*(lock **)(iVar1 + 0x14));
  *(undefined4 *)((int)&buf->st_size + 4) = *(undefined4 *)(*(int *)(iVar1 + 0x18) + 4);
  *(undefined4 *)&buf->st_size = 0;
  lock_release(*(lock **)(iVar1 + 0x14));
  buf->st_mode = 0x26f1;
  buf->st_nlink = 2;
  buf->st_blocks = 0;
  buf->st_dev = 0;
  buf->st_ino = 0xffffffff;
  return 0;
}



int semfs_write(vnode *vn,uio *uio)

{
  semfs_sem *sem;
  uint uVar1;
  uint uVar2;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  sem = semfs_getsem(semv);
  lock_acquire(sem->sems_lock);
  while( true ) {
    if (uio->uio_resid == 0) {
      lock_release(sem->sems_lock);
      return 0;
    }
    uVar1 = sem->sems_count;
    uVar2 = uio->uio_resid + uVar1;
    if (uVar2 < uVar1) break;
    if ((dbflags & 0x100) != 0) {
      kprintf("semfs: sem%u: V, count %u -> %u\n",semv->semv_semnum,uVar1,uVar2);
    }
    semfs_wakeup(sem,uVar2);
    sem->sems_count = uVar2;
    uVar2 = *(uint *)((int)&uio->uio_offset + 4);
    uVar1 = uVar2 + uio->uio_resid;
    *(uint *)&uio->uio_offset = (uint)(uVar1 < uVar2) + *(int *)&uio->uio_offset;
    *(uint *)((int)&uio->uio_offset + 4) = uVar1;
    uio->uio_resid = 0;
  }
  lock_release(sem->sems_lock);
  return 0x26;
}



int semfs_read(vnode *vn,uio *uio)

{
  semfs_sem *psVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar1 = semfs_getsem(semv);
  lock_acquire(psVar1->sems_lock);
  while (uVar4 = uio->uio_resid, uVar4 != 0) {
    uVar3 = psVar1->sems_count;
    if (uVar3 != 0) {
      if (uVar3 < uVar4) {
        uVar4 = uVar3;
      }
      if ((dbflags & 0x100) != 0) {
        kprintf("semfs: sem%u: P, count %u -> %u\n",semv->semv_semnum,uVar3,uVar3 - uVar4);
      }
      psVar1->sems_count = psVar1->sems_count - uVar4;
      uVar2 = *(uint *)((int)&uio->uio_offset + 4);
      uVar3 = uVar2 + uVar4;
      *(uint *)&uio->uio_offset = (uint)(uVar3 < uVar2) + *(int *)&uio->uio_offset;
      *(uint *)((int)&uio->uio_offset + 4) = uVar3;
      uio->uio_resid = uio->uio_resid - uVar4;
    }
    if (uio->uio_resid == 0) break;
    if (psVar1->sems_count == 0) {
      if ((dbflags & 0x100) != 0) {
        kprintf("semfs: sem%u: blocking\n",semv->semv_semnum);
      }
      cv_wait(psVar1->sems_cv,psVar1->sems_lock);
    }
  }
  lock_release(psVar1->sems_lock);
  return 0;
}



int semfs_lookparent(vnode *dirvn,char *path,vnode **resultdirvn,char *namebuf,size_t bufmax)

{
  size_t sVar1;
  int iVar2;
  
  sVar1 = strlen(path);
  if (bufmax < sVar1 + 1) {
    iVar2 = 7;
  }
  else {
    strcpy(namebuf,path);
    vnode_incref(dirvn);
    *resultdirvn = dirvn;
    iVar2 = 0;
  }
  return iVar2;
}



int semfs_remove(vnode *dirvn,char *name)

{
  bool bVar1;
  int iVar2;
  int iVar3;
  semfs_direntryarray *psVar4;
  semfs_sem *sem;
  semfs_semarray *psVar5;
  uint uVar6;
  semfs *semfs;
  semfs_direntry *dent;
  uint uVar7;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar2 = strcmp(name,".");
  iVar3 = 8;
  if (iVar2 != 0) {
    iVar2 = strcmp(name,"..");
    if (iVar2 == 0) {
      iVar3 = 8;
    }
    else {
      uVar6 = 0;
      lock_acquire(semfs->semfs_dirlock);
      uVar7 = (semfs->semfs_dents->arr).num;
      bVar1 = uVar7 != 0;
      while (bVar1) {
        psVar4 = semfs->semfs_dents;
        if ((psVar4->arr).num <= uVar6) {
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        dent = (semfs_direntry *)(psVar4->arr).v[uVar6];
        if ((dent != (semfs_direntry *)0x0) && (iVar2 = strcmp(name,dent->semd_name), iVar2 == 0)) {
          sem = semfs_getsembynum(semfs,dent->semd_semnum);
          lock_acquire(sem->sems_lock);
          if (sem->sems_linked == false) {
            badassert("sem->sems_linked","../../fs/semfs/semfs_vnops.c",0x1f0,"semfs_remove");
          }
          sem->sems_linked = false;
          if (sem->sems_hasvnode == false) {
            lock_acquire(semfs->semfs_tablelock);
            psVar5 = semfs->semfs_sems;
            iVar2 = dent->semd_semnum << 2;
            if ((psVar5->arr).num <= dent->semd_semnum) {
              badassert("index < a->num","../../include/array.h",0x6b,"array_set");
            }
            *(undefined4 *)((int)(psVar5->arr).v + iVar2) = 0;
            lock_release(semfs->semfs_tablelock);
            lock_release(sem->sems_lock);
            semfs_sem_destroy(sem);
          }
          else {
            lock_release(sem->sems_lock);
          }
          psVar4 = semfs->semfs_dents;
          if ((psVar4->arr).num <= uVar6) {
            badassert("index < a->num","../../include/array.h",0x6b,"array_set");
          }
          (psVar4->arr).v[uVar6] = (void *)0x0;
          semfs_direntry_destroy(dent);
          iVar3 = 0;
          goto out;
        }
        uVar6 = uVar6 + 1;
        bVar1 = uVar6 < uVar7;
      }
      iVar3 = 0x13;
out:
      lock_release(semfs->semfs_dirlock);
    }
  }
  return iVar3;
}



int semfs_getdirentry(vnode *dirvn,uio *uio)

{
  int *piVar1;
  size_t n;
  int iVar2;
  int iVar3;
  uint uVar4;
  char *str;
  
  iVar3 = *(int *)((int)dirvn->vn_data + 0x18);
  uVar4 = *(uint *)((int)&uio->uio_offset + 4);
  if (*(int *)&uio->uio_offset < 0) {
    badassert("uio->uio_offset >= 0","../../fs/semfs/semfs_vnops.c",0x137,"semfs_getdirentry");
  }
  lock_acquire(*(lock **)(iVar3 + 0x14));
  piVar1 = *(int **)(iVar3 + 0x18);
  if ((uint)piVar1[1] <= uVar4) {
    iVar2 = 0;
  }
  else {
    if ((uint)piVar1[1] <= uVar4) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    str = **(char ***)(*piVar1 + uVar4 * 4);
    n = strlen(str);
    iVar2 = uiomove(str,n,uio);
  }
  lock_release(*(lock **)(iVar3 + 0x14));
  return iVar2;
}



void semfs_vnode_destroy(semfs_vnode *semv)

{
  vnode_cleanup(&semv->semv_absvn);
  kfree(semv);
  return;
}



int semfs_reclaim(vnode *vn)

{
  int iVar1;
  uint uVar2;
  semfs_semarray *psVar3;
  array *a;
  char *sem;
  uint index;
  semfs *psVar4;
  semfs_vnode *semv;
  spinlock *splk;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar4 = semv->semv_semfs;
  splk = &vn->vn_countlock;
  lock_acquire(psVar4->semfs_tablelock);
  spinlock_acquire(splk);
  if (vn->vn_refcount < 2) {
    spinlock_release(splk);
    a = (array *)psVar4->semfs_vnodes;
    uVar2 = 0;
    do {
      index = uVar2;
      if (a->num <= index) goto LAB_80006b84;
      uVar2 = index + 1;
    } while (vn != (vnode *)a->v[index]);
    array_remove(a,index);
LAB_80006b84:
    uVar2 = semv->semv_semnum;
    if (uVar2 != 0xffffffff) {
      psVar3 = psVar4->semfs_sems;
      iVar1 = uVar2 << 2;
      if ((psVar3->arr).num <= uVar2) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      sem = *(char **)((int)(psVar3->arr).v + iVar1);
      if (*(bool *)((int)sem + 0xc) == false) {
        sem = "sem->sems_hasvnode";
        badassert("sem->sems_hasvnode","../../fs/semfs/semfs_vnops.c",0x277,"semfs_reclaim");
      }
      *(bool *)((int)sem + 0xc) = false;
      if (*(bool *)((int)sem + 0xd) == false) {
        psVar3 = psVar4->semfs_sems;
        iVar1 = semv->semv_semnum << 2;
        if ((psVar3->arr).num <= semv->semv_semnum) {
          sem = "index < a->num";
          badassert("index < a->num","../../include/array.h",0x6b,"array_set");
        }
        *(undefined4 *)((int)(psVar3->arr).v + iVar1) = 0;
        semfs_sem_destroy((semfs_sem *)sem);
      }
    }
    lock_release(psVar4->semfs_tablelock);
    semfs_vnode_destroy(semv);
    iVar1 = 0;
  }
  else {
    vn->vn_refcount = vn->vn_refcount + -1;
    spinlock_release(splk);
    lock_release(psVar4->semfs_tablelock);
    iVar1 = 0x1b;
  }
  return iVar1;
}



int semfs_getvnode(semfs *semfs,uint semnum,vnode **ret)

{
  uint uVar1;
  semfs_vnode *semv;
  int iVar2;
  semfs_semarray *psVar3;
  void *pvVar4;
  undefined uVar5;
  vnode *vn;
  array *a;
  
  lock_acquire(semfs->semfs_tablelock);
  uVar1 = 0;
  while (uVar1 < (semfs->semfs_vnodes->arr).num) {
    vn = (vnode *)(semfs->semfs_vnodes->arr).v[uVar1];
    uVar1 = uVar1 + 1;
    if (*(uint *)((int)vn->vn_data + 0x1c) == semnum) {
      vnode_incref(vn);
      lock_release(semfs->semfs_tablelock);
      *ret = vn;
      return 0;
    }
  }
  semv = semfs_vnode_create(semfs,semnum);
  if (semv == (semfs_vnode *)0x0) {
    lock_release(semfs->semfs_tablelock);
    return 3;
  }
  a = (array *)semfs->semfs_vnodes;
  uVar1 = a->num;
  iVar2 = array_setsize(a,uVar1 + 1);
  if (iVar2 == 0) {
    a->v[uVar1] = semv;
    iVar2 = 0;
  }
  if (iVar2 == 0) {
    if (semnum != 0xffffffff) {
      psVar3 = semfs->semfs_sems;
      if ((psVar3->arr).num <= semnum) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      pvVar4 = (psVar3->arr).v[semnum];
      if (pvVar4 == (void *)0x0) {
        badassert("sem != NULL","../../fs/semfs/semfs_vnops.c",0x317,"semfs_getvnode");
      }
      uVar5 = 1;
      if (*(char *)((int)pvVar4 + 0xc) != '\0') {
        uVar5 = 1;
        badassert("sem->sems_hasvnode == false","../../fs/semfs/semfs_vnops.c",0x318,
                  "semfs_getvnode");
      }
      *(undefined *)((int)pvVar4 + 0xc) = uVar5;
    }
    lock_release(semfs->semfs_tablelock);
    *ret = &semv->semv_absvn;
    return 0;
  }
  semfs_vnode_destroy(semv);
  lock_release(semfs->semfs_tablelock);
  return 3;
}



int semfs_lookup(vnode *dirvn,char *path,vnode **resultvn)

{
  bool bVar1;
  int iVar2;
  semfs_direntryarray *psVar3;
  uint uVar4;
  char **ppcVar5;
  semfs *semfs;
  uint uVar6;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar2 = strcmp(path,".");
  if ((iVar2 == 0) || (iVar2 = strcmp(path,".."), iVar2 == 0)) {
    vnode_incref(dirvn);
    *resultvn = dirvn;
    iVar2 = 0;
  }
  else {
    uVar4 = 0;
    lock_acquire(semfs->semfs_dirlock);
    uVar6 = (semfs->semfs_dents->arr).num;
    bVar1 = uVar6 != 0;
    while (bVar1) {
      psVar3 = semfs->semfs_dents;
      if ((psVar3->arr).num <= uVar4) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppcVar5 = (char **)(psVar3->arr).v[uVar4];
      if ((ppcVar5 != (char **)0x0) && (iVar2 = strcmp(path,*ppcVar5), iVar2 == 0)) {
        iVar2 = semfs_getvnode(semfs,(uint)ppcVar5[1],resultvn);
        lock_release(semfs->semfs_dirlock);
        return iVar2;
      }
      uVar4 = uVar4 + 1;
      bVar1 = uVar4 < uVar6;
    }
    lock_release(semfs->semfs_dirlock);
    iVar2 = 0x13;
  }
  return iVar2;
}



int semfs_creat(vnode *dirvn,char *name,bool excl,mode_t mode,vnode **resultvn)

{
  int iVar1;
  int iVar2;
  semfs_sem *sem;
  semfs_direntry *dent;
  semfs_direntryarray *psVar3;
  semfs_semarray *psVar4;
  uint uVar5;
  semfs *semfs;
  char **ppcVar6;
  uint uVar7;
  array *a;
  uint uVar8;
  uint semnum;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar1 = strcmp(name,".");
  iVar2 = 0x16;
  if (iVar1 != 0) {
    iVar1 = strcmp(name,"..");
    iVar2 = 0x16;
    if (iVar1 != 0) {
      uVar5 = 0;
      lock_acquire(semfs->semfs_dirlock);
      uVar7 = (semfs->semfs_dents->arr).num;
      uVar8 = uVar7;
LAB_80007170:
      do {
        if (uVar7 <= uVar5) {
          sem = semfs_sem_create(name);
          if (sem == (semfs_sem *)0x0) {
            iVar1 = 3;
            goto fail_unlock;
          }
          lock_acquire(semfs->semfs_tablelock);
          iVar2 = semfs_sem_insert(semfs,sem,&semnum);
          lock_release(semfs->semfs_tablelock);
          iVar1 = iVar2;
          if (iVar2 == 0) {
            dent = semfs_direntry_create(name,semnum);
            if (dent != (semfs_direntry *)0x0) {
              if (uVar8 < uVar7) {
                psVar3 = semfs->semfs_dents;
                if ((psVar3->arr).num <= uVar8) {
                  badassert("index < a->num","../../include/array.h",0x6b,"array_set");
                }
                (psVar3->arr).v[uVar8] = dent;
LAB_8000726c:
                iVar1 = semfs_getvnode(semfs,semnum,resultvn);
                if (iVar1 == 0) {
                  sem->sems_linked = true;
                  lock_release(semfs->semfs_dirlock);
                  return 0;
                }
                psVar3 = semfs->semfs_dents;
                if ((psVar3->arr).num <= uVar8) {
                  badassert("index < a->num","../../include/array.h",0x6b,"array_set");
                }
                (psVar3->arr).v[uVar8] = (void *)0x0;
              }
              else {
                a = (array *)semfs->semfs_dents;
                uVar5 = a->num;
                iVar1 = array_setsize(a,uVar5 + 1);
                if (iVar1 == 0) {
                  a->v[uVar5] = dent;
                  iVar1 = iVar2;
                  uVar8 = uVar5;
                }
                if (iVar1 == 0) goto LAB_8000726c;
              }
              semfs_direntry_destroy(dent);
            }
            lock_acquire(semfs->semfs_tablelock);
            psVar4 = semfs->semfs_sems;
            if ((psVar4->arr).num <= semnum) {
              badassert("index < a->num","../../include/array.h",0x6b,"array_set");
            }
            (psVar4->arr).v[semnum] = (void *)0x0;
            lock_release(semfs->semfs_tablelock);
          }
          semfs_sem_destroy(sem);
fail_unlock:
          lock_release(semfs->semfs_dirlock);
          return iVar1;
        }
        psVar3 = semfs->semfs_dents;
        if ((psVar3->arr).num <= uVar5) {
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        ppcVar6 = (char **)(psVar3->arr).v[uVar5];
        if (ppcVar6 == (char **)0x0) {
          if (uVar8 == uVar7) {
            uVar8 = uVar5;
          }
          uVar5 = uVar5 + 1;
          goto LAB_80007170;
        }
        iVar1 = strcmp(*ppcVar6,name);
        uVar5 = uVar5 + 1;
      } while (iVar1 != 0);
      if (excl == false) {
        iVar2 = semfs_getvnode(semfs,(uint)ppcVar6[1],resultvn);
        lock_release(semfs->semfs_dirlock);
      }
      else {
        lock_release(semfs->semfs_dirlock);
        iVar2 = 0x16;
      }
    }
  }
  return iVar2;
}



int sfs_clearblock(sfs_fs *sfs,daddr_t block)

{
  int iVar1;
  
  iVar1 = sfs_writeblock(sfs,block,sfs_clearblock::zeros,0x200);
  return iVar1;
}



int sfs_balloc(sfs_fs *sfs,daddr_t *diskblock)

{
  int iVar1;
  
  iVar1 = bitmap_alloc(sfs->sfs_freemap,diskblock);
  if (iVar1 == 0) {
    sfs->sfs_freemapdirty = true;
    if ((sfs->sfs_sb).sb_nblocks <= *diskblock) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: balloc: invalid block %u\n",(sfs->sfs_sb).sb_volname);
    }
    iVar1 = sfs_clearblock(sfs,*diskblock);
    if (iVar1 != 0) {
      bitmap_unmark(sfs->sfs_freemap,*diskblock);
    }
  }
  return iVar1;
}



void sfs_bfree(sfs_fs *sfs,daddr_t diskblock)

{
  bitmap_unmark(sfs->sfs_freemap,diskblock);
  sfs->sfs_freemapdirty = true;
  return;
}



int sfs_bused(sfs_fs *sfs,daddr_t diskblock)

{
  int iVar1;
  
  if ((sfs->sfs_sb).sb_nblocks <= diskblock) {
                    /* WARNING: Subroutine does not return */
    panic("sfs: %s: sfs_bused called on out of range block %u\n",(sfs->sfs_sb).sb_volname,diskblock)
    ;
  }
  iVar1 = bitmap_isset(sfs->sfs_freemap,diskblock);
  return iVar1;
}



int sfs_bmap(sfs_vnode *sv,uint32_t fileblock,bool doalloc,daddr_t *diskblock)

{
  undefined3 extraout_var;
  bool bVar3;
  uint uVar1;
  int iVar2;
  sfs_fs *sfs;
  daddr_t block;
  daddr_t idblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  bVar3 = vfs_biglock_do_i_hold();
  uVar1 = (uint)(fileblock < 0xf);
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_bmap.c",0x46,"sfs_bmap");
  }
  if (uVar1 == 0) {
    uVar1 = fileblock - 0xf;
    if (uVar1 < 0x80) {
      idblock = (sv->sv_i).sfi_indirect;
      if ((idblock == 0) && (iVar2 = 0, doalloc == false)) {
        *diskblock = 0;
      }
      else {
        if (idblock == 0) {
          iVar2 = sfs_balloc(sfs,&idblock);
          if (iVar2 != 0) {
            return iVar2;
          }
          (sv->sv_i).sfi_indirect = idblock;
          sv->sv_dirty = true;
          bzero(sfs_bmap::idbuf,0x200);
        }
        else {
          iVar2 = sfs_readblock(sfs,idblock,sfs_bmap::idbuf,0x200);
          if (iVar2 != 0) {
            return iVar2;
          }
        }
        block = sfs_bmap::idbuf[uVar1 & 0x7f];
        if ((block == 0) && (doalloc != false)) {
          iVar2 = sfs_balloc(sfs,&block);
          if (iVar2 != 0) {
            return iVar2;
          }
          sfs_bmap::idbuf[uVar1 & 0x7f] = block;
          iVar2 = sfs_writeblock(sfs,idblock,sfs_bmap::idbuf,0x200);
          if (iVar2 != 0) {
            return iVar2;
          }
        }
        if ((block != 0) && (iVar2 = sfs_bused(sfs,block), iVar2 == 0)) {
                    /* WARNING: Subroutine does not return */
          panic("sfs: %s: Data block %u (block %u of file %u) marked free\n",
                (sfs->sfs_sb).sb_volname,block,uVar1,sv->sv_ino);
        }
        *diskblock = block;
        iVar2 = 0;
      }
    }
    else {
      iVar2 = 0x26;
    }
  }
  else {
    block = (sv->sv_i).sfi_direct[fileblock];
    if ((block == 0) && (doalloc != false)) {
      iVar2 = sfs_balloc(sfs,&block);
      if (iVar2 != 0) {
        return iVar2;
      }
      (sv->sv_i).sfi_direct[fileblock] = block;
      sv->sv_dirty = true;
    }
    if ((block != 0) && (iVar2 = sfs_bused(sfs,block), iVar2 == 0)) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: Data block %u (block %u of file %u) marked free\n",(sfs->sfs_sb).sb_volname,
            block,fileblock,sv->sv_ino);
    }
    *diskblock = block;
    iVar2 = 0;
  }
  return iVar2;
}



int sfs_itrunc(sfs_vnode *sv,off_t len)

{
  bool bVar1;
  bool bVar2;
  uint uVar3;
  uint uVar4;
  int in_a2;
  int iVar5;
  uint in_a3;
  sfs_fs *sfs;
  daddr_t dVar6;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  uVar3 = in_a3 + 0x1ff;
  iVar5 = (uint)(uVar3 < in_a3) + in_a2;
  uVar4 = uVar3;
  if (iVar5 < 0) {
    uVar4 = in_a3 + 0x3fe;
    iVar5 = (uint)(uVar4 < uVar3) + iVar5;
  }
  uVar3 = iVar5 << 0x17 | uVar4 >> 9;
  vfs_biglock_acquire();
  for (uVar4 = 0; uVar4 < 0xf; uVar4 = uVar4 + 1) {
    dVar6 = (sv->sv_i).sfi_direct[uVar4];
    if ((uVar3 <= uVar4) && (dVar6 != 0)) {
      sfs_bfree(sfs,dVar6);
      (sv->sv_i).sfi_direct[uVar4] = 0;
      sv->sv_dirty = true;
    }
  }
  dVar6 = (sv->sv_i).sfi_indirect;
  if ((uVar3 < 0x8e) && (dVar6 != 0)) {
    iVar5 = sfs_readblock(sfs,dVar6,sfs_itrunc::idbuf,0x200);
    if (iVar5 != 0) {
      vfs_biglock_release();
      return iVar5;
    }
    bVar2 = false;
    bVar1 = false;
    for (uVar4 = 0; uVar4 < 0x80; uVar4 = uVar4 + 1) {
      iVar5 = uVar4 * 4;
      if (uVar3 < uVar4 + 0xf) {
        iVar5 = uVar4 << 2;
        if (sfs_itrunc::idbuf[uVar4] != 0) {
          sfs_bfree(sfs,sfs_itrunc::idbuf[uVar4]);
          sfs_itrunc::idbuf[uVar4] = 0;
          bVar2 = true;
          iVar5 = uVar4 << 2;
        }
      }
      if (*(int *)((int)sfs_itrunc::idbuf + iVar5) != 0) {
        bVar1 = true;
      }
    }
    if (bVar1) {
      if ((bVar2) && (iVar5 = sfs_writeblock(sfs,dVar6,sfs_itrunc::idbuf,0x200), iVar5 != 0)) {
        vfs_biglock_release();
        return iVar5;
      }
    }
    else {
      sfs_bfree(sfs,dVar6);
      (sv->sv_i).sfi_indirect = 0;
      sv->sv_dirty = true;
    }
  }
  (sv->sv_i).sfi_size = in_a3;
  sv->sv_dirty = true;
  vfs_biglock_release();
  return 0;
}



/* WARNING: Removing unreachable block (ram,0x800079f8) */

int sfs_dir_nentries(sfs_vnode *sv)

{
  uint uVar1;
  char *pcVar2;
  char *pcVar3;
  
  pcVar3 = (char *)((sv->sv_absvn).vn_fs)->fs_data;
  if ((sv->sv_i).sfi_type != 2) {
    pcVar2 = "sv->sv_i.sfi_type == SFS_TYPE_DIR";
    pcVar3 = "../../fs/sfs/sfs_dir.c";
    badassert("sv->sv_i.sfi_type == SFS_TYPE_DIR","../../fs/sfs/sfs_dir.c",0x57,"sfs_dir_nentries");
    sv = (sfs_vnode *)pcVar2;
  }
  uVar1 = (sv->sv_i).sfi_size;
  if ((uVar1 & 0x3f) == 0) {
    return uVar1 >> 6;
  }
                    /* WARNING: Subroutine does not return */
  panic("sfs: %s: directory %u: Invalid size %llu\n",pcVar3 + 0x10,sv->sv_ino);
}



int sfs_readdir(sfs_vnode *sv,int slot,sfs_direntry *sd)

{
  int iVar1;
  
  iVar1 = sfs_metaio(sv,CONCAT44(sd,0x40),(void *)slot,0,slot << 6);
  return iVar1;
}



int sfs_writedir(sfs_vnode *sv,int slot,sfs_direntry *sd)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  
  if (slot < 0) {
    pcVar2 = "slot>=0";
    pcVar3 = "../../fs/sfs/sfs_dir.c";
    sd = (sfs_direntry *)0x45;
    badassert("slot>=0","../../fs/sfs/sfs_dir.c",0x45,"sfs_writedir");
    sv = (sfs_vnode *)pcVar2;
    slot = (int)pcVar3;
  }
  iVar1 = sfs_metaio(sv,CONCAT44(sd,0x40),(void *)slot,0,slot << 6);
  return iVar1;
}



int sfs_dir_findname(sfs_vnode *sv,char *name,uint32_t *ino,int *slot,int *emptyslot)

{
  bool bVar1;
  int iVar2;
  int iVar3;
  int slot_00;
  sfs_direntry tsd;
  
  iVar2 = sfs_dir_nentries(sv);
  slot_00 = 0;
  bVar1 = false;
  while( true ) {
    if (iVar2 <= slot_00) {
      iVar2 = 0;
      if (!bVar1) {
        iVar2 = 0x13;
      }
      return iVar2;
    }
    iVar3 = sfs_readdir(sv,slot_00,&tsd);
    if (iVar3 != 0) break;
    if (tsd.sfd_ino == 0) {
      if (emptyslot != (int *)0x0) {
        *emptyslot = slot_00;
      }
    }
    else {
      tsd.sfd_name[59] = '\0';
      iVar3 = strcmp(tsd.sfd_name,name);
      if (iVar3 == 0) {
        if (bVar1) {
          badassert("found==0","../../fs/sfs/sfs_dir.c",0x85,"sfs_dir_findname");
        }
        if (slot != (int *)0x0) {
          *slot = slot_00;
        }
        bVar1 = true;
        if (ino != (uint32_t *)0x0) {
          *ino = tsd.sfd_ino;
        }
      }
    }
    slot_00 = slot_00 + 1;
  }
  return iVar3;
}



int sfs_dir_link(sfs_vnode *sv,char *name,uint32_t ino,int *slot)

{
  bool bVar1;
  int iVar2;
  size_t sVar3;
  int emptyslot;
  sfs_direntry sd;
  
  emptyslot = -1;
  iVar2 = sfs_dir_findname(sv,name,(uint32_t *)0x0,(int *)0x0,&emptyslot);
  if (((iVar2 == 0) || (iVar2 == 0x13)) && (bVar1 = iVar2 != 0, iVar2 = 0x16, bVar1)) {
    sVar3 = strlen(name);
    iVar2 = 7;
    if (sVar3 + 1 < 0x3d) {
      if (emptyslot < 0) {
        emptyslot = sfs_dir_nentries(sv);
      }
      bzero(&sd,0x40);
      sd.sfd_ino = ino;
      strcpy(sd.sfd_name,name);
      if (slot != (int *)0x0) {
        *slot = emptyslot;
      }
      iVar2 = sfs_writedir(sv,emptyslot,&sd);
    }
  }
  return iVar2;
}



int sfs_dir_unlink(sfs_vnode *sv,int slot)

{
  int iVar1;
  sfs_direntry sd;
  
  bzero(&sd,0x40);
  sd.sfd_ino = 0;
  iVar1 = sfs_writedir(sv,slot,&sd);
  return iVar1;
}



int sfs_lookonce(sfs_vnode *sv,char *name,sfs_vnode **ret,int *slot)

{
  int iVar1;
  sfs_fs *sfs;
  uint32_t ino;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  iVar1 = sfs_dir_findname(sv,name,&ino,slot,(int *)0x0);
  if ((iVar1 == 0) && (iVar1 = sfs_loadvnode(sfs,ino,0,ret), iVar1 == 0)) {
    if (((*ret)->sv_i).sfi_linkcount == 0) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: name %s (inode %u) in dir %u has linkcount 0\n",(sfs->sfs_sb).sb_volname,name,
            (*ret)->sv_ino,sv->sv_ino);
    }
    iVar1 = 0;
  }
  return iVar1;
}



char * sfs_getvolname(fs *fs)

{
  void *pvVar1;
  
  pvVar1 = fs->fs_data;
  vfs_biglock_acquire();
  vfs_biglock_release();
  return (char *)((int)pvVar1 + 0x10);
}



int sfs_sync_vnodes(sfs_fs *sfs)

{
  vnodearray *pvVar1;
  uint uVar2;
  vnode *v;
  uint uVar3;
  
  uVar3 = (sfs->sfs_vnodes->arr).num;
  for (uVar2 = 0; uVar2 < uVar3; uVar2 = uVar2 + 1) {
    pvVar1 = sfs->sfs_vnodes;
    if ((pvVar1->arr).num <= uVar2) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    v = (vnode *)(pvVar1->arr).v[uVar2];
    vnode_check(v,"fsync");
    (*v->vn_ops->vop_fsync)(v);
  }
  return 0;
}



int sfs_sync_superblock(sfs_fs *sfs)

{
  int iVar1;
  
  if (sfs->sfs_superdirty == false) {
    iVar1 = 0;
  }
  else {
    iVar1 = sfs_writeblock(sfs,0,&sfs->sfs_sb,0x200);
    if (iVar1 == 0) {
      sfs->sfs_superdirty = false;
    }
  }
  return iVar1;
}



sfs_fs * sfs_fs_create(void)

{
  sfs_fs *ptr;
  array *a;
  
  ptr = (sfs_fs *)kmalloc(0x21c);
  if (ptr == (sfs_fs *)0x0) {
    ptr = (sfs_fs *)0x0;
  }
  else {
    (ptr->sfs_absfs).fs_data = ptr;
    (ptr->sfs_absfs).fs_ops = &sfs_fsops;
    ptr->sfs_superdirty = false;
    ptr->sfs_device = (device *)0x0;
    a = (array *)kmalloc(0xc);
    if (a == (array *)0x0) {
      a = (array *)0x0;
      ptr->sfs_vnodes = (vnodearray *)0x0;
    }
    else {
      array_init(a);
      ptr->sfs_vnodes = (vnodearray *)a;
    }
    if (a == (array *)0x0) {
      kfree(ptr);
      ptr = (sfs_fs *)0x0;
    }
    else {
      ptr->sfs_freemap = (bitmap *)0x0;
      ptr->sfs_freemapdirty = false;
    }
  }
  return ptr;
}



int sfs_freemapio(sfs_fs *sfs,uio_rw rw)

{
  void *pvVar1;
  int iVar2;
  void *data;
  uint uVar3;
  uint32_t uVar4;
  
  uVar4 = (sfs->sfs_sb).sb_nblocks;
  pvVar1 = bitmap_getdata(sfs->sfs_freemap);
  uVar3 = 0;
  do {
    if (uVar4 + 0xfff >> 0xc <= uVar3) {
      return 0;
    }
    data = (void *)((int)pvVar1 + uVar3 * 0x200);
    if (rw == UIO_READ) {
      iVar2 = sfs_readblock(sfs,uVar3 + 2,data,0x200);
    }
    else {
      iVar2 = sfs_writeblock(sfs,uVar3 + 2,data,0x200);
    }
    uVar3 = uVar3 + 1;
  } while (iVar2 == 0);
  return iVar2;
}



int sfs_sync_freemap(sfs_fs *sfs)

{
  int iVar1;
  
  if (sfs->sfs_freemapdirty == false) {
    iVar1 = 0;
  }
  else {
    iVar1 = sfs_freemapio(sfs,UIO_WRITE);
    if (iVar1 == 0) {
      sfs->sfs_freemapdirty = false;
    }
  }
  return iVar1;
}



int sfs_sync(fs *fs)

{
  int iVar1;
  sfs_fs *sfs;
  
  vfs_biglock_acquire();
  sfs = (sfs_fs *)fs->fs_data;
  iVar1 = sfs_sync_vnodes(sfs);
  if (iVar1 == 0) {
    iVar1 = sfs_sync_freemap(sfs);
    if (iVar1 == 0) {
      iVar1 = sfs_sync_superblock(sfs);
      if (iVar1 == 0) {
        vfs_biglock_release();
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}



void sfs_fs_destroy(sfs_fs *sfs)

{
  array *a;
  
  if (sfs->sfs_freemap != (bitmap *)0x0) {
    bitmap_destroy(sfs->sfs_freemap);
  }
  a = (array *)sfs->sfs_vnodes;
  array_cleanup(a);
  kfree(a);
  if (sfs->sfs_device != (device *)0x0) {
    badassert("sfs->sfs_device == NULL","../../fs/sfs/sfs_fsops.c",0x10a,"sfs_fs_destroy");
  }
  kfree(sfs);
  return;
}



int sfs_domount(void *options,device *dev,fs **ret)

{
  int iVar1;
  sfs_fs *sfs;
  bitmap *pbVar2;
  uint32_t uVar3;
  
  vfs_biglock_acquire();
  if (dev->d_blocksize == 0x200) {
    sfs = sfs_fs_create();
    if (sfs == (sfs_fs *)0x0) {
      vfs_biglock_release();
      iVar1 = 3;
    }
    else {
      sfs->sfs_device = dev;
      iVar1 = sfs_readblock(sfs,0,&sfs->sfs_sb,0x200);
      if (iVar1 == 0) {
        uVar3 = (sfs->sfs_sb).sb_magic;
        if (uVar3 == 0xabadf001) {
          if (dev->d_blocks < (sfs->sfs_sb).sb_nblocks) {
            kprintf("sfs: warning - fs has %u blocks, device has %u\n");
          }
          (sfs->sfs_sb).sb_volname[0x1f] = '\0';
          pbVar2 = bitmap_create((sfs->sfs_sb).sb_nblocks + 0xfff & 0xfffff000);
          sfs->sfs_freemap = pbVar2;
          if (pbVar2 == (bitmap *)0x0) {
            sfs->sfs_device = (device *)0x0;
            sfs_fs_destroy(sfs);
            vfs_biglock_release();
            iVar1 = 3;
          }
          else {
            iVar1 = sfs_freemapio(sfs,UIO_READ);
            if (iVar1 == 0) {
              *ret = &sfs->sfs_absfs;
              vfs_biglock_release();
              iVar1 = 0;
            }
            else {
              sfs->sfs_device = (device *)0x0;
              sfs_fs_destroy(sfs);
              vfs_biglock_release();
            }
          }
        }
        else {
          kprintf("sfs: Wrong magic number in superblock (0x%x, should be 0x%x)\n",uVar3,0xabadf001)
          ;
          sfs->sfs_device = (device *)0x0;
          sfs_fs_destroy(sfs);
          vfs_biglock_release();
          iVar1 = 8;
        }
      }
      else {
        sfs->sfs_device = (device *)0x0;
        sfs_fs_destroy(sfs);
        vfs_biglock_release();
      }
    }
  }
  else {
    vfs_biglock_release();
    kprintf("sfs: Cannot mount on device with blocksize %zu\n",dev->d_blocksize);
    iVar1 = 0x1a;
  }
  return iVar1;
}



int sfs_unmount(fs *fs)

{
  int iVar1;
  sfs_fs *sfs;
  
  sfs = (sfs_fs *)fs->fs_data;
  vfs_biglock_acquire();
  if ((sfs->sfs_vnodes->arr).num == 0) {
    if (sfs->sfs_superdirty != false) {
      badassert("sfs->sfs_superdirty == false","../../fs/sfs/sfs_fsops.c",0x122,"sfs_unmount");
    }
    if (sfs->sfs_freemapdirty != false) {
      badassert("sfs->sfs_freemapdirty == false","../../fs/sfs/sfs_fsops.c",0x123,"sfs_unmount");
    }
    sfs->sfs_device = (device *)0x0;
    sfs_fs_destroy(sfs);
    vfs_biglock_release();
    iVar1 = 0;
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}



int sfs_mount(char *device)

{
  int iVar1;
  
  iVar1 = vfs_mount(device,(void *)0x0,sfs_domount);
  return iVar1;
}



int sfs_sync_inode(sfs_vnode *sv)

{
  int iVar1;
  
  iVar1 = 0;
  if ((sv->sv_dirty != false) &&
     (iVar1 = sfs_writeblock((sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data,sv->sv_ino,&sv->sv_i,0x200),
     iVar1 == 0)) {
    sv->sv_dirty = false;
  }
  return iVar1;
}



int sfs_reclaim(vnode *v)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  array *a;
  uint uVar4;
  uint index;
  undefined4 unaff_s0;
  sfs_vnode *sv;
  sfs_fs *sfs;
  spinlock *splk;
  undefined4 in_stack_ffffffe8;
  
  sv = (sfs_vnode *)v->vn_data;
  sfs = (sfs_fs *)v->vn_fs->fs_data;
  vfs_biglock_acquire();
  splk = &v->vn_countlock;
  spinlock_acquire(splk);
  iVar1 = v->vn_refcount;
  if (iVar1 == 1) {
    spinlock_release(splk);
    if (((sv->sv_i).sfi_linkcount == 0) &&
       (iVar1 = sfs_itrunc(sv,CONCAT44(in_stack_ffffffe8,unaff_s0)), iVar1 != 0)) {
      vfs_biglock_release();
    }
    else {
      iVar1 = sfs_sync_inode(sv);
      if (iVar1 == 0) {
        if ((sv->sv_i).sfi_linkcount == 0) {
          sfs_bfree(sfs,sv->sv_ino);
        }
        a = (array *)sfs->sfs_vnodes;
        uVar3 = a->num;
        uVar4 = 0;
        while ((index = uVar3, uVar4 < uVar3 &&
               (index = uVar4, *(sfs_vnode **)((int)a->v[uVar4] + 0x10) != sv))) {
          uVar4 = uVar4 + 1;
        }
        if (index == uVar3) {
                    /* WARNING: Subroutine does not return */
          panic("sfs: %s: reclaim vnode %u not in vnode pool\n",(sfs->sfs_sb).sb_volname,sv->sv_ino)
          ;
        }
        array_remove(a,index);
        vnode_cleanup((vnode *)sv);
        vfs_biglock_release();
        kfree(sv);
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
  }
  else {
    iVar2 = iVar1 + -1;
    if (iVar1 < 2) {
      badassert("v->vn_refcount>1","../../fs/sfs/sfs_inode.c",0x57,"sfs_reclaim");
      iVar2 = iVar1;
    }
    v->vn_refcount = iVar2;
    spinlock_release(splk);
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}



int sfs_loadvnode(sfs_fs *sfs,uint32_t ino,int forcetype,sfs_vnode **ret)

{
  uint16_t uVar1;
  vnodearray *pvVar2;
  sfs_vnode *psVar3;
  int iVar4;
  undefined uVar5;
  vnode_ops *ops;
  uint uVar6;
  array *a;
  uint uVar7;
  
  uVar7 = (sfs->sfs_vnodes->arr).num;
  uVar6 = 0;
  while (uVar6 < uVar7) {
    pvVar2 = sfs->sfs_vnodes;
    if ((pvVar2->arr).num <= uVar6) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    psVar3 = *(sfs_vnode **)((int)(pvVar2->arr).v[uVar6] + 0x10);
    iVar4 = sfs_bused(sfs,psVar3->sv_ino);
    if (iVar4 == 0) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: Found inode %u in unallocated block\n",(sfs->sfs_sb).sb_volname,psVar3->sv_ino
           );
    }
    uVar6 = uVar6 + 1;
    if (psVar3->sv_ino == ino) {
      if (forcetype != 0) {
        badassert("forcetype==SFS_TYPE_INVAL","../../fs/sfs/sfs_inode.c",0xb1,"sfs_loadvnode");
      }
      vnode_incref(&psVar3->sv_absvn);
      *ret = psVar3;
      return 0;
    }
  }
  psVar3 = (sfs_vnode *)kmalloc(0x220);
  if (psVar3 == (sfs_vnode *)0x0) {
    return 3;
  }
  iVar4 = sfs_bused(sfs,ino);
  if (iVar4 != 0) {
    iVar4 = sfs_readblock(sfs,ino,&psVar3->sv_i,0x200);
    if (iVar4 != 0) {
      kfree(psVar3);
      return iVar4;
    }
    psVar3->sv_dirty = false;
    if (forcetype != 0) {
      uVar5 = true;
      if ((psVar3->sv_i).sfi_type != 0) {
        uVar5 = 1;
        badassert("sv->sv_i.sfi_type == SFS_TYPE_INVAL","../../fs/sfs/sfs_inode.c",0xd6,
                  "sfs_loadvnode");
      }
      (psVar3->sv_i).sfi_type = (uint16_t)forcetype;
      psVar3->sv_dirty = (bool)uVar5;
    }
    uVar1 = (psVar3->sv_i).sfi_type;
    if (uVar1 == 1) {
      ops = &sfs_fileops;
    }
    else {
      if (uVar1 != 2) {
                    /* WARNING: Subroutine does not return */
        panic("sfs: %s: loadvnode: Invalid inode type (inode %u, type %u)\n",
              (sfs->sfs_sb).sb_volname,ino);
      }
      ops = &sfs_dirops;
    }
    iVar4 = vnode_init((vnode *)psVar3,ops,&sfs->sfs_absfs,psVar3);
    if (iVar4 == 0) {
      psVar3->sv_ino = ino;
      a = (array *)sfs->sfs_vnodes;
      uVar6 = a->num;
      iVar4 = array_setsize(a,uVar6 + 1);
      if (iVar4 == 0) {
        a->v[uVar6] = psVar3;
        iVar4 = 0;
      }
      if (iVar4 == 0) {
        *ret = psVar3;
        return 0;
      }
      vnode_cleanup((vnode *)psVar3);
      kfree(psVar3);
      return iVar4;
    }
    kfree(psVar3);
    return iVar4;
  }
                    /* WARNING: Subroutine does not return */
  panic("sfs: %s: Tried to load inode %u from unallocated block\n",(sfs->sfs_sb).sb_volname,ino);
}



int sfs_makeobj(sfs_fs *sfs,int type,sfs_vnode **ret)

{
  int iVar1;
  uint32_t ino;
  
  iVar1 = sfs_balloc(sfs,&ino);
  if ((iVar1 == 0) && (iVar1 = sfs_loadvnode(sfs,ino,type,ret), iVar1 != 0)) {
    sfs_bfree(sfs,ino);
  }
  return iVar1;
}



int sfs_getroot(fs *fs,vnode **ret)

{
  int iVar1;
  sfs_fs *sfs;
  sfs_vnode *sv;
  
  sfs = (sfs_fs *)fs->fs_data;
  vfs_biglock_acquire();
  iVar1 = sfs_loadvnode(sfs,1,0,&sv);
  if (iVar1 == 0) {
    if ((sv->sv_i).sfi_type == 2) {
      vfs_biglock_release();
      *ret = &sv->sv_absvn;
      iVar1 = 0;
    }
    else {
      kprintf("sfs: %s: getroot: not directory (type %u)\n",(sfs->sfs_sb).sb_volname);
      vfs_biglock_release();
      iVar1 = 8;
    }
  }
  else {
    kprintf("sfs: %s: getroot: Cannot load root vnode\n",(sfs->sfs_sb).sb_volname);
    vfs_biglock_release();
  }
  return iVar1;
}



int sfs_rwblock(sfs_fs *sfs,uio *uio)

{
  char *pcVar1;
  undefined3 extraout_var;
  bool bVar5;
  int iVar2;
  uint uVar3;
  int iVar4;
  uint uVar6;
  
  bVar5 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0x41,"sfs_rwblock");
  }
  if ((*(uint *)(iVar2 + -0x64c0) & 0x200) != 0) {
    if (uio->uio_rw == UIO_READ) {
      pcVar1 = "console read";
    }
    else {
      pcVar1 = s_console_write_80022690;
    }
    iVar2 = *(int *)&uio->uio_offset;
    uVar3 = *(uint *)((int)&uio->uio_offset + 4);
    uVar6 = uVar3;
    if (iVar2 < 0) {
      uVar6 = uVar3 + 0x1ff;
      iVar2 = (uint)(uVar6 < uVar3) + iVar2;
    }
    kprintf("sfs: %s %llu\n",pcVar1 + 8,iVar2 >> 9,iVar2 << 0x17 | uVar6 >> 9);
  }
  iVar2 = 0;
  while( true ) {
    iVar4 = (*sfs->sfs_device->d_ops->devop_io)(sfs->sfs_device,uio);
    if (iVar4 == 8) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: DEVOP_IO returned EINVAL\n",(sfs->sfs_sb).sb_volname);
    }
    if (iVar4 != 0x20) break;
    if (iVar2 == 0) {
      iVar2 = 1;
      iVar4 = *(int *)&uio->uio_offset;
      uVar3 = *(uint *)((int)&uio->uio_offset + 4);
      uVar6 = uVar3;
      if (iVar4 < 0) {
        uVar6 = uVar3 + 0x1ff;
        iVar4 = (uint)(uVar6 < uVar3) + iVar4;
      }
      kprintf("sfs: %s: block %llu I/O error, retrying\n",(sfs->sfs_sb).sb_volname,iVar4 >> 9,
              iVar4 << 0x17 | uVar6 >> 9);
    }
    else {
      if (9 < iVar2) {
        iVar4 = *(int *)&uio->uio_offset;
        uVar3 = *(uint *)((int)&uio->uio_offset + 4);
        uVar6 = uVar3;
        if (iVar4 < 0) {
          uVar6 = uVar3 + 0x1ff;
          iVar4 = (uint)(uVar6 < uVar3) + iVar4;
        }
        kprintf("sfs: %s: block %llu I/O error, giving up after %d retries\n",
                (sfs->sfs_sb).sb_volname,iVar4 >> 9,iVar4 << 0x17 | uVar6 >> 9,iVar2);
        return 0x20;
      }
      iVar2 = iVar2 + 1;
    }
  }
  return iVar4;
}



int sfs_blockio(sfs_vnode *sv,uio *uio)

{
  size_t sVar1;
  uint uVar2;
  char *n;
  uint uVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  sfs_fs *sfs;
  int iVar7;
  int iVar8;
  daddr_t diskblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  iVar5 = *(int *)&uio->uio_offset;
  uVar2 = *(uint *)((int)&uio->uio_offset + 4);
  uVar6 = uVar2;
  if (iVar5 < 0) {
    uVar6 = uVar2 + 0x1ff;
    iVar5 = (uint)(uVar6 < uVar2) + iVar5;
  }
  iVar5 = sfs_bmap(sv,iVar5 << 0x17 | uVar6 >> 9,uio->uio_rw == UIO_WRITE,&diskblock);
  if (iVar5 == 0) {
    if (diskblock == 0) {
      n = (char *)0x200;
      if (uio->uio_rw != UIO_READ) {
        n = "uio->uio_rw == UIO_READ";
        badassert("uio->uio_rw == UIO_READ","../../fs/sfs/sfs_io.c",0x100,"sfs_blockio");
      }
      iVar5 = uiomovezeros((size_t)n,uio);
    }
    else {
      iVar7 = *(int *)&uio->uio_offset;
      iVar8 = *(int *)((int)&uio->uio_offset + 4);
      *(undefined4 *)&uio->uio_offset = 0;
      *(daddr_t *)((int)&uio->uio_offset + 4) = diskblock * 0x200;
      uVar6 = uio->uio_resid;
      sVar1 = 0x200;
      if (uVar6 < 0x200) {
        badassert("uio->uio_resid >= SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x10f,"sfs_blockio");
      }
      uio->uio_resid = sVar1;
      iVar5 = sfs_rwblock(sfs,uio);
      uVar3 = *(uint *)((int)&uio->uio_offset + 4);
      uVar2 = uVar3 + diskblock * -0x200;
      uVar4 = uVar2 + iVar8;
      *(uint *)&uio->uio_offset =
           (uint)(uVar4 < uVar2) + (*(int *)&uio->uio_offset - (uint)(uVar3 < uVar2)) + iVar7;
      *(uint *)((int)&uio->uio_offset + 4) = uVar4;
      uio->uio_resid = uVar6 + (uio->uio_resid - 0x200);
    }
  }
  return iVar5;
}



int sfs_readblock(sfs_fs *sfs,daddr_t block,void *data,size_t len)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  iovec iov;
  uio ku;
  
  if (len != 0x200) {
    pcVar2 = "len == SFS_BLOCKSIZE";
    pcVar3 = "../../fs/sfs/sfs_io.c";
    data = (void *)0x71;
    badassert("len == SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x71,"sfs_readblock");
    sfs = (sfs_fs *)pcVar2;
    block = (daddr_t)pcVar3;
  }
  uio_kinit(&iov,&ku,data,0x200,CONCAT44(block >> 0x17,block << 9),UIO_READ);
  iVar1 = sfs_rwblock(sfs,&ku);
  return iVar1;
}



int sfs_writeblock(sfs_fs *sfs,daddr_t block,void *data,size_t len)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  iovec iov;
  uio ku;
  
  if (len != 0x200) {
    pcVar2 = "len == SFS_BLOCKSIZE";
    pcVar3 = "../../fs/sfs/sfs_io.c";
    data = (void *)0x80;
    badassert("len == SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x80,"sfs_writeblock");
    sfs = (sfs_fs *)pcVar2;
    block = (daddr_t)pcVar3;
  }
  uio_kinit(&iov,&ku,data,0x200,CONCAT44(block >> 0x17,block << 9),UIO_WRITE);
  iVar1 = sfs_rwblock(sfs,&ku);
  return iVar1;
}



int sfs_partialio(sfs_vnode *sv,uio *uio,uint32_t skipstart,uint32_t len)

{
  undefined3 extraout_var;
  bool bVar1;
  uint uVar2;
  uint uVar3;
  int iVar4;
  sfs_fs *sfs;
  uio_rw uVar5;
  daddr_t diskblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  uVar5 = uio->uio_rw;
  if (0x200 < skipstart + len) {
    badassert("skipstart + len <= SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0xaa,"sfs_partialio");
  }
  bVar1 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0xad,"sfs_partialio");
  }
  iVar4 = *(int *)&uio->uio_offset;
  uVar2 = *(uint *)((int)&uio->uio_offset + 4);
  uVar3 = uVar2;
  if (iVar4 < 0) {
    uVar3 = uVar2 + 0x1ff;
    iVar4 = (uint)(uVar3 < uVar2) + iVar4;
  }
  iVar4 = sfs_bmap(sv,iVar4 << 0x17 | uVar3 >> 9,uVar5 == UIO_WRITE,&diskblock);
  if (iVar4 == 0) {
    if (diskblock == 0) {
      if (uio->uio_rw != UIO_READ) {
        badassert("uio->uio_rw == UIO_READ","../../fs/sfs/sfs_io.c",0xbd,"sfs_partialio");
      }
      bzero(sfs_partialio::iobuf,0x200);
    }
    else {
      iVar4 = sfs_readblock(sfs,diskblock,sfs_partialio::iobuf,0x200);
      if (iVar4 != 0) {
        return iVar4;
      }
    }
    iVar4 = uiomove(sfs_partialio::iobuf + skipstart,len,uio);
    if (iVar4 == 0) {
      if (uio->uio_rw == UIO_WRITE) {
        iVar4 = sfs_writeblock(sfs,diskblock,sfs_partialio::iobuf,0x200);
      }
      else {
        iVar4 = 0;
      }
    }
  }
  return iVar4;
}



int sfs_io(sfs_vnode *sv,uio *uio)

{
  int iVar1;
  uint32_t skipstart;
  uint uVar2;
  int iVar3;
  uint uVar4;
  char *len;
  uint uVar5;
  uint uVar6;
  
  uVar5 = uio->uio_resid;
  if (uio->uio_rw == UIO_READ) {
    uVar6 = (sv->sv_i).sfi_size;
    iVar1 = *(int *)&uio->uio_offset;
    uVar2 = *(uint *)((int)&uio->uio_offset + 4);
    uVar4 = uVar2 + uVar5;
    iVar3 = (uint)(uVar4 < uVar2) + iVar1;
    if (-1 < iVar1) {
      if (iVar1 != 0) {
        return 0;
      }
      if (uVar6 <= uVar2) {
        return 0;
      }
    }
    if ((iVar3 < 1) && ((iVar3 != 0 || (uVar4 <= uVar6)))) {
      uVar4 = 0;
    }
    else {
      uVar4 = uVar4 - uVar6;
      if (uVar5 <= uVar4) {
        badassert("uio->uio_resid > extraresid","../../fs/sfs/sfs_io.c",0x13d,"sfs_io");
      }
      uio->uio_resid = uVar5 - uVar4;
    }
  }
  else {
    uVar4 = 0;
  }
  skipstart = *(uint *)((int)&uio->uio_offset + 4) & 0x1ff;
  if (*(int *)&uio->uio_offset < 0) {
    skipstart = (skipstart - 1 | 0xfffffe00) + 1;
  }
  if (skipstart == 0) {
    iVar1 = 0;
  }
  else {
    uVar2 = uio->uio_resid;
    if (0x200 - skipstart <= uio->uio_resid) {
      uVar2 = 0x200 - skipstart;
    }
    iVar1 = sfs_partialio(sv,uio,skipstart,uVar2);
    if (iVar1 != 0) goto out;
  }
  uVar2 = uio->uio_resid;
  if (uVar2 != 0) {
    if ((*(uint *)((int)&uio->uio_offset + 4) & 0x1ff) != 0) {
      badassert("uio->uio_offset % SFS_BLOCKSIZE == 0","../../fs/sfs/sfs_io.c",0x161,"sfs_io");
    }
    uVar6 = 0;
    do {
      if (uVar2 >> 9 <= uVar6) {
        len = (char *)uio->uio_resid;
        if ((char *)0x1ff < len) {
          len = "sfs_io";
          badassert("uio->uio_resid < SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x16d,"sfs_io");
        }
        if (len != (char *)0x0) {
          iVar1 = sfs_partialio(sv,uio,0,(uint32_t)len);
        }
        break;
      }
      iVar1 = sfs_blockio(sv,uio);
      uVar6 = uVar6 + 1;
    } while (iVar1 == 0);
  }
out:
  if ((uio->uio_resid != uVar5) && (uio->uio_rw == UIO_WRITE)) {
    uVar5 = *(uint *)((int)&uio->uio_offset + 4);
    if ((0 < *(int *)&uio->uio_offset) ||
       ((*(int *)&uio->uio_offset == 0 && ((sv->sv_i).sfi_size < uVar5)))) {
      (sv->sv_i).sfi_size = uVar5;
      sv->sv_dirty = true;
    }
  }
  uio->uio_resid = uio->uio_resid + uVar4;
  return iVar1;
}



int sfs_metaio(sfs_vnode *sv,off_t actualpos,void *data,size_t len,uio_rw rw)

{
  undefined3 extraout_var;
  bool bVar4;
  uio_rw uVar1;
  uint uVar2;
  int iVar3;
  size_t sVar5;
  sfs_fs *sfs;
  int in_stack_00000018;
  daddr_t local_28 [3];
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  bVar4 = vfs_biglock_do_i_hold();
  sVar5 = len;
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0x1ab,"sfs_metaio");
  }
  uVar1 = rw;
  if ((int)len < 0) {
    uVar1 = rw + 0x1ff;
    sVar5 = (uVar1 < rw) + len;
  }
  uVar2 = rw & 0x1ff;
  if ((int)len < 0) {
    uVar2 = (uVar2 - 1 | 0xfffffe00) + 1;
  }
  iVar3 = sfs_bmap(sv,sVar5 << 0x17 | uVar1 >> 9,in_stack_00000018 == 1,local_28);
  if (iVar3 == 0) {
    if (local_28[0] == 0) {
      if (in_stack_00000018 != 0) {
        badassert("rw == UIO_READ","../../fs/sfs/sfs_io.c",0x1ba,"sfs_metaio");
      }
      bzero(actualpos._0_4_,actualpos._4_4_);
      iVar3 = 0;
    }
    else {
      iVar3 = sfs_readblock(sfs,local_28[0],sfs_metaio::metaiobuf,0x200);
      if (iVar3 == 0) {
        if (in_stack_00000018 == 0) {
          memcpy(actualpos._0_4_,sfs_metaio::metaiobuf + uVar2,actualpos._4_4_);
          iVar3 = 0;
        }
        else {
          memcpy(sfs_metaio::metaiobuf + uVar2,actualpos._0_4_,actualpos._4_4_);
          iVar3 = sfs_writeblock(sfs,local_28[0],sfs_metaio::metaiobuf,0x200);
          if (iVar3 == 0) {
            uVar2 = actualpos._4_4_ + rw;
            iVar3 = (uVar2 < actualpos._4_4_) + len;
            if ((iVar3 < 1) && ((iVar3 != 0 || (uVar2 <= (sv->sv_i).sfi_size)))) {
              iVar3 = 0;
            }
            else {
              (sv->sv_i).sfi_size = uVar2;
              sv->sv_dirty = true;
              iVar3 = 0;
            }
          }
        }
      }
    }
  }
  return iVar3;
}



int sfs_eachopen(vnode *v,int openflags)

{
  return 0;
}



int sfs_eachopendir(vnode *v,int openflags)

{
  uint uVar1;
  
  uVar1 = 0x12;
  if (((openflags & 3U) == 0) && (uVar1 = openflags & 0x20, uVar1 != 0)) {
    return 0x12;
  }
  return uVar1;
}



int sfs_ioctl(vnode *v,int op,userptr_t data)

{
  return 8;
}



bool sfs_isseekable(vnode *v)

{
  return true;
}



int sfs_mmap(vnode *v)

{
  return 1;
}



int sfs_truncate(vnode *v,off_t len)

{
  int iVar1;
  undefined4 unaff_retaddr;
  undefined4 in_stack_fffffff8;
  
  iVar1 = sfs_itrunc((sfs_vnode *)v->vn_data,CONCAT44(in_stack_fffffff8,unaff_retaddr));
  return iVar1;
}



int sfs_fsync(vnode *v)

{
  int iVar1;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  iVar1 = sfs_sync_inode(sv);
  vfs_biglock_release();
  return iVar1;
}



int sfs_gettype(vnode *v,uint32_t *ret)

{
  short sVar1;
  void *pvVar2;
  void *pvVar3;
  
  pvVar2 = v->vn_data;
  pvVar3 = v->vn_fs->fs_data;
  vfs_biglock_acquire();
  sVar1 = *(short *)((int)pvVar2 + 0x1c);
  if (sVar1 == 1) {
    *ret = 0x1000;
    vfs_biglock_release();
  }
  else {
    if (sVar1 != 2) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: gettype: Invalid inode type (inode %u, type %u)\n",(int)pvVar3 + 0x10,
            *(undefined4 *)((int)pvVar2 + 0x218));
    }
    *ret = 0x2000;
    vfs_biglock_release();
  }
  return 0;
}



int sfs_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  vnode_check(v,"gettype");
  iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
  if (iVar1 == 0) {
    *(undefined4 *)((int)&statbuf->st_size + 4) = *(undefined4 *)((int)pvVar2 + 0x18);
    *(undefined4 *)&statbuf->st_size = 0;
    statbuf->st_nlink = *(nlink_t *)((int)pvVar2 + 0x1e);
    statbuf->st_blocks = 0;
    iVar1 = 0;
  }
  return iVar1;
}



int sfs_namefile(vnode *vv,uio *uio)

{
  if (*(int *)((int)vv->vn_data + 0x218) != 1) {
    badassert("sv->sv_ino == SFS_ROOTDIR_INO","../../fs/sfs/sfs_vnops.c",0x10c,"sfs_namefile");
  }
  return 0;
}



int sfs_write(vnode *v,uio *uio)

{
  int iVar1;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)v->vn_data;
  if (uio->uio_rw != UIO_WRITE) {
    badassert("uio->uio_rw==UIO_WRITE","../../fs/sfs/sfs_vnops.c",0x7a,"sfs_write");
  }
  vfs_biglock_acquire();
  iVar1 = sfs_io(sv,uio);
  vfs_biglock_release();
  return iVar1;
}



int sfs_read(vnode *v,uio *uio)

{
  int iVar1;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)v->vn_data;
  if (uio->uio_rw != UIO_READ) {
    badassert("uio->uio_rw==UIO_READ","../../fs/sfs/sfs_vnops.c",0x67,"sfs_read");
  }
  vfs_biglock_acquire();
  iVar1 = sfs_io(sv,uio);
  vfs_biglock_release();
  return iVar1;
}



int sfs_lookparent(vnode *v,char *path,vnode **ret,char *buf,size_t buflen)

{
  int iVar1;
  size_t sVar2;
  vnode *vn;
  
  vn = (vnode *)v->vn_data;
  vfs_biglock_acquire();
  if (*(short *)&vn[1].vn_countlock.splk_lock == 2) {
    sVar2 = strlen(path);
    if (buflen < sVar2 + 1) {
      vfs_biglock_release();
      iVar1 = 7;
    }
    else {
      strcpy(buf,path);
      vnode_incref(vn);
      *ret = vn;
      vfs_biglock_release();
      iVar1 = 0;
    }
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x11;
  }
  return iVar1;
}



int sfs_lookup(vnode *v,char *path,vnode **ret)

{
  int iVar1;
  sfs_vnode *sv;
  sfs_vnode *final;
  
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  if ((sv->sv_i).sfi_type == 2) {
    iVar1 = sfs_lookonce(sv,path,&final,(int *)0x0);
    if (iVar1 == 0) {
      *ret = &final->sv_absvn;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x11;
  }
  return iVar1;
}



int sfs_link(vnode *dir,char *name,vnode *file)

{
  int iVar1;
  void *pvVar2;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)dir->vn_data;
  pvVar2 = file->vn_data;
  if (file->vn_fs != dir->vn_fs) {
    name = "../../fs/sfs/sfs_vnops.c";
    badassert("file->vn_fs == dir->vn_fs","../../fs/sfs/sfs_vnops.c",0x16a,"sfs_link");
  }
  vfs_biglock_acquire();
  if (*(short *)((int)pvVar2 + 0x1c) == 2) {
    vfs_biglock_release();
    iVar1 = 8;
  }
  else {
    iVar1 = sfs_dir_link(sv,name,*(uint32_t *)((int)pvVar2 + 0x218),(int *)0x0);
    if (iVar1 == 0) {
      *(short *)((int)pvVar2 + 0x1e) = *(short *)((int)pvVar2 + 0x1e) + 1;
      *(undefined *)((int)pvVar2 + 0x21c) = 1;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  return iVar1;
}



int sfs_remove(vnode *dir,char *name)

{
  uint16_t uVar1;
  int iVar2;
  uint16_t uVar3;
  sfs_vnode *psVar4;
  sfs_vnode *victim;
  int slot;
  
  psVar4 = (sfs_vnode *)dir->vn_data;
  vfs_biglock_acquire();
  iVar2 = sfs_lookonce(psVar4,name,&victim,&slot);
  if (iVar2 == 0) {
    iVar2 = sfs_dir_unlink(psVar4,slot);
    if (iVar2 == 0) {
      uVar1 = (victim->sv_i).sfi_linkcount;
      uVar3 = uVar1 - 1;
      psVar4 = victim;
      if (uVar1 == 0) {
        badassert("victim->sv_i.sfi_linkcount > 0","../../fs/sfs/sfs_vnops.c",0x19c,"sfs_remove");
      }
      (psVar4->sv_i).sfi_linkcount = uVar3;
      psVar4->sv_dirty = true;
    }
    vnode_decref(&victim->sv_absvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return iVar2;
}



int sfs_rename(vnode *d1,char *n1,vnode *d2,char *n2)

{
  uint16_t uVar1;
  uint32_t uVar2;
  int errcode;
  sfs_vnode *psVar3;
  uint16_t uVar4;
  int errcode_00;
  char *pcVar5;
  sfs_vnode *sv;
  void *pvVar6;
  int iVar7;
  sfs_vnode *g1;
  int slot1;
  int slot2;
  
  sv = (sfs_vnode *)d1->vn_data;
  pvVar6 = ((sv->sv_absvn).vn_fs)->fs_data;
  vfs_biglock_acquire();
  uVar2 = 1;
  if (d1 != d2) {
    badassert("d1==d2","../../fs/sfs/sfs_vnops.c",0x1bb,"sfs_rename");
  }
  pcVar5 = (char *)sv;
  if (sv->sv_ino != uVar2) {
    pcVar5 = "sv->sv_ino == SFS_ROOTDIR_INO";
    badassert("sv->sv_ino == SFS_ROOTDIR_INO","../../fs/sfs/sfs_vnops.c",0x1bc,"sfs_rename");
  }
  errcode = sfs_lookonce((sfs_vnode *)pcVar5,n1,&g1,&slot1);
  if (errcode == 0) {
    psVar3 = g1;
    pcVar5 = (char *)sv;
    if ((g1->sv_i).sfi_type != 1) {
      pcVar5 = "g1->sv_i.sfi_type == SFS_TYPE_FILE";
      badassert("g1->sv_i.sfi_type == SFS_TYPE_FILE","../../fs/sfs/sfs_vnops.c",0x1c6,"sfs_rename");
    }
    errcode = sfs_dir_link((sfs_vnode *)pcVar5,n2,psVar3->sv_ino,&slot2);
    if (errcode == 0) {
      (g1->sv_i).sfi_linkcount = (g1->sv_i).sfi_linkcount + 1;
      g1->sv_dirty = true;
      errcode = sfs_dir_unlink(sv,slot1);
      if (errcode == 0) {
        uVar1 = (g1->sv_i).sfi_linkcount;
        uVar4 = uVar1 - 1;
        if (uVar1 == 0) {
          pcVar5 = "g1->sv_i.sfi_linkcount>0";
          uVar4 = uVar1;
          badassert("g1->sv_i.sfi_linkcount>0","../../fs/sfs/sfs_vnops.c",0x1e3,"sfs_rename");
          g1 = (sfs_vnode *)pcVar5;
        }
        (g1->sv_i).sfi_linkcount = uVar4;
        g1->sv_dirty = true;
        vnode_decref(&g1->sv_absvn);
        vfs_biglock_release();
        return 0;
      }
      errcode_00 = sfs_dir_unlink(sv,slot2);
      if (errcode_00 != 0) {
        iVar7 = (int)pvVar6 + 0x10;
        pcVar5 = strerror(errcode);
        kprintf("sfs: %s: rename: %s\n",iVar7,pcVar5);
        pcVar5 = strerror(errcode_00);
        kprintf("sfs: %s: rename: while cleaning up: %s\n",iVar7,pcVar5);
                    /* WARNING: Subroutine does not return */
        panic("sfs: %s: rename: Cannot recover\n",iVar7);
      }
      (g1->sv_i).sfi_linkcount = (g1->sv_i).sfi_linkcount - 1;
    }
    vnode_decref(&g1->sv_absvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return errcode;
}



int sfs_creat(vnode *v,char *name,bool excl,mode_t mode,vnode **ret)

{
  int iVar1;
  sfs_vnode *sv;
  sfs_fs *sfs;
  sfs_vnode *newguy;
  uint32_t ino;
  
  sfs = (sfs_fs *)v->vn_fs->fs_data;
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  iVar1 = sfs_dir_findname(sv,name,&ino,(int *)0x0,(int *)0x0);
  if ((iVar1 == 0) || (iVar1 == 0x13)) {
    if (iVar1 == 0) {
      if (excl == false) {
        iVar1 = sfs_loadvnode(sfs,ino,0,&newguy);
        if (iVar1 == 0) {
          *ret = &newguy->sv_absvn;
          vfs_biglock_release();
          iVar1 = 0;
        }
        else {
          vfs_biglock_release();
        }
      }
      else {
        vfs_biglock_release();
        iVar1 = 0x16;
      }
    }
    else {
      iVar1 = sfs_makeobj(sfs,1,&newguy);
      if (iVar1 == 0) {
        iVar1 = sfs_dir_link(sv,name,newguy->sv_ino,(int *)0x0);
        if (iVar1 == 0) {
          (newguy->sv_i).sfi_linkcount = (newguy->sv_i).sfi_linkcount + 1;
          newguy->sv_dirty = true;
          *ret = &newguy->sv_absvn;
          vfs_biglock_release();
          iVar1 = 0;
        }
        else {
          vnode_decref(&newguy->sv_absvn);
          vfs_biglock_release();
        }
      }
      else {
        vfs_biglock_release();
      }
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}



void item_init(void)

{
  tmp = (Item)kmalloc(0xc);
  return;
}



Item ITEMscan(pid_t pid,vaddr_t addr,int index)

{
  Item piVar1;
  
  piVar1 = tmp;
  (tmp->key).kaddr = addr;
  (piVar1->key).kpid = pid;
  piVar1->index = index;
  return piVar1;
}



Item ITEMsetvoid(void)

{
  Item piVar1;
  vaddr_t vVar2;
  
  piVar1 = (Item)kmalloc(0xc);
  vVar2 = 0xffffffff;
  if (piVar1 == (Item)0x0) {
    badassert("tmp != NULL","../../hash/item.c",0x3b,"ITEMsetvoid");
  }
  (piVar1->key).kpid = vVar2;
  (piVar1->key).kaddr = vVar2;
  piVar1->index = -2;
  return piVar1;
}



Item ITEMsetnull(void)

{
  return (Item)0x0;
}



int KEYcompare(Key k1,Key k2)

{
  int in_a0;
  int in_a1;
  int in_a2;
  int in_a3;
  
  if (in_a1 != in_a3) {
    return -1;
  }
  if (in_a0 != in_a2) {
    return -1;
  }
  return 0;
}



Key KEYget(Item data)

{
  vaddr_t vVar1;
  pid_t *in_a1;
  
  vVar1 = in_a1[1];
  (data->key).kpid = *in_a1;
  (data->key).kaddr = vVar1;
  return (Key)CONCAT44(data,vVar1);
}



/* WARNING: Variable defined which should be unmapped: k1 */
/* WARNING: Variable defined which should be unmapped: k2 */

int ITEMcheckvoid(Item data)

{
  int iVar1;
  Key k1;
  Key k2;
  
  KEYget((Item)&k1);
  iVar1 = KEYcompare(k1,(Key)0xffffffffffffffff);
  return (uint)(iVar1 == 0);
}



int is_prime(int n)

{
  int iVar1;
  int iVar2;
  
  iVar1 = 2;
  do {
    if (iVar1 == 0) {
      trap(0x1c00);
    }
    if (n / iVar1 < iVar1) {
      return 1;
    }
    iVar2 = n % iVar1;
    if (iVar1 == 0) {
      trap(0x1c00);
    }
    iVar1 = iVar1 + 1;
  } while (iVar2 != 0);
  return 0;
}



int first_prime(int n)

{
  int iVar1;
  int n_00;
  
  do {
    n_00 = n;
    iVar1 = is_prime(n_00);
    n = n_00 + 1;
  } while (iVar1 == 0);
  return n_00;
}



link NEW(Item item,link next)

{
  link pSVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  int iVar5;
  
  iVar5 = 0;
  do {
    pSVar1 = link_list + free_link;
    iVar2 = free_link + 1;
    if ((pSVar1->item).index == -1) {
      if (pSVar1 == (link)0x0) {
        pcVar3 = "x != NULL";
        pcVar4 = "../../hash/st.c";
        badassert("x != NULL","../../hash/st.c",0x121,"NEW");
        item = (Item)pcVar3;
        next = (link)pcVar4;
      }
      (pSVar1->item).index = item->index;
      (pSVar1->item).key.kaddr = (item->key).kaddr;
      (pSVar1->item).key.kpid = (item->key).kpid;
      pSVar1->next = next;
      return pSVar1;
    }
    iVar5 = iVar5 + 1;
    free_link = iVar2;
    if (n_entries <= iVar2) {
      free_link = 0;
    }
    iVar2 = 0;
  } while (iVar5 < n_entries);
  for (; iVar2 < n_entries; iVar2 = iVar2 + 1) {
    pSVar1 = link_list + iVar2;
    kprintf("Index: %d addr %d pid %d\n",(pSVar1->item).index,(pSVar1->item).key.kaddr >> 0xc,
            (pSVar1->item).key.kpid);
  }
                    /* WARNING: Subroutine does not return */
  panic("No free entry in hash table\n");
}



ST STinit(int maxN)

{
  ST psVar1;
  int iVar2;
  link *ppSVar3;
  Item item;
  link pSVar4;
  int iVar5;
  
  psVar1 = (ST)kmalloc(0xc);
  if (psVar1 == (ST)0x0) {
    badassert("st != NULL","../../hash/st.c",0x13e,"STinit");
  }
  iVar2 = first_prime(maxN);
  kprintf("Hash Table dimension: %d\n",iVar2);
  link_list = (link)kmalloc(iVar2 << 4);
  item_init();
  for (iVar5 = 0; iVar5 < iVar2; iVar5 = iVar5 + 1) {
    link_list[iVar5].item.index = -1;
  }
  free_link = 0;
  n_entries = iVar2;
  psVar1->M = iVar2;
  ppSVar3 = (link *)kmalloc(iVar2 << 2);
  psVar1->heads = ppSVar3;
  if (ppSVar3 == (link *)0x0) {
    badassert("st->heads != NULL","../../hash/st.c",0x14c,"STinit");
  }
  item = ITEMsetvoid();
  pSVar4 = NEW(item,(link)0x0);
  psVar1->z = pSVar4;
  for (iVar2 = 0; iVar2 < psVar1->M; iVar2 = iVar2 + 1) {
    psVar1->heads[iVar2] = psVar1->z;
  }
  return psVar1;
}



int hashU(Key v,int M)

{
  uint uVar1;
  uint in_a1;
  uint in_a2;
  
  for (uVar1 = 10; uVar1 <= in_a1; uVar1 = uVar1 * 10) {
  }
  if (in_a2 == 0) {
    trap(0x1c00);
  }
  return (in_a1 + M * uVar1) % in_a2;
}



void STinsert(ST st,Item item)

{
  int iVar1;
  link pSVar2;
  link *ppSVar3;
  int in_stack_ffffffe8;
  vaddr_t in_stack_ffffffec;
  
  KEYget((Item)&stack0xffffffe8);
  iVar1 = hashU((Key)CONCAT44(in_stack_ffffffe8,in_stack_ffffffec),in_stack_ffffffe8);
  ppSVar3 = st->heads;
  pSVar2 = NEW(item,ppSVar3[iVar1]);
  ppSVar3[iVar1] = pSVar2;
  return;
}



/* WARNING: Variable defined which should be unmapped: KEY */

Item searchST(link t,Key k,link z)

{
  int iVar1;
  link in_a3;
  undefined4 unaff_s0;
  Key KEY;
  int in_stack_fffffff0;
  
  if (t == in_a3) {
    t = (link)ITEMsetnull();
  }
  else {
    KEYget((Item)&KEY);
    iVar1 = KEYcompare(KEY,(Key)CONCAT44(in_stack_fffffff0,unaff_s0));
    if (iVar1 != 0) {
      t = (link)searchST(t->next,KEY,z);
    }
  }
  return &t->item;
}



/* WARNING: Variable defined which should be unmapped: k */

int STsearch(ST st,pid_t pid,vaddr_t addr)

{
  int iVar1;
  Item piVar2;
  Key k;
  
  iVar1 = hashU((Key)CONCAT44(pid,addr),pid);
  piVar2 = searchST(st->heads[iVar1],(Key)CONCAT44(pid,addr),(link)pid);
  if (piVar2 == (Item)0x0) {
    iVar1 = -1;
  }
  else {
    iVar1 = piVar2->index;
  }
  return iVar1;
}



link deleteR(link x,Key k)

{
  int iVar1;
  link pSVar2;
  int unaff_s0;
  undefined4 unaff_retaddr;
  Key in_stack_fffffff0;
  
  if (x == (link)0x0) {
    x = (link)0x0;
  }
  else {
    KEYget((Item)&stack0xfffffff0);
    iVar1 = KEYcompare(in_stack_fffffff0,(Key)CONCAT44(unaff_s0,unaff_retaddr));
    if (iVar1 == 0) {
      pSVar2 = x->next;
      (x->item).index = -1;
      x = pSVar2;
    }
    else {
      pSVar2 = deleteR(x->next,in_stack_fffffff0);
      x->next = pSVar2;
    }
  }
  return x;
}



/* WARNING: Variable defined which should be unmapped: k */

void STdelete(ST st,pid_t pid,vaddr_t addr)

{
  int iVar1;
  link pSVar2;
  link *ppSVar3;
  Key k;
  
  iVar1 = hashU((Key)CONCAT44(pid,addr),pid);
  ppSVar3 = st->heads;
  pSVar2 = deleteR(ppSVar3[iVar1],(Key)CONCAT44(pid,addr));
  ppSVar3[iVar1] = pSVar2;
  return;
}



void visitR(link h,link z)

{
  if (h != z) {
    visitR(h->next,z);
  }
  return;
}



void STdisplay(ST st)

{
  int iVar1;
  
  for (iVar1 = 0; iVar1 < st->M; iVar1 = iVar1 + 1) {
    kprintf("st->heads[%d]: %d",iVar1,(st->heads[iVar1]->item).key.kaddr);
    visitR(st->heads[iVar1],st->z);
    kprintf("\n");
  }
  return;
}



uint array_num(array *a)

{
  return a->num;
}



void * array_get(array *a,uint index)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if (a->num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    badassert("index < a->num","../../include/array.h",100,"array_get");
    a = (array *)pcVar1;
  }
  return *(void **)((int)a->v + (int)pcVar2);
}



void array_set(array *a,uint index,void *val)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if (a->num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    val = (void *)0x6b;
    badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    a = (array *)pcVar1;
  }
  *(void **)((int)a->v + (int)pcVar2) = val;
  return;
}



uint stringarray_num(stringarray *a)

{
  uint uVar1;
  
  uVar1 = array_num(&a->arr);
  return uVar1;
}



char * stringarray_get(stringarray *a,uint index)

{
  char *pcVar1;
  
  pcVar1 = (char *)array_get(&a->arr,index);
  return pcVar1;
}



void stringarray_set(stringarray *a,uint index,char *val)

{
  array_set(&a->arr,index,val);
  return;
}



void array_init(array *a)

{
  a->max = 0;
  a->num = 0;
  a->v = (void **)0x0;
  return;
}



stringarray * stringarray_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  return (stringarray *)a;
}



void stringarray_init(stringarray *a)

{
  array_init(&a->arr);
  return;
}



array * array_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a != (array *)0x0) {
    array_init(a);
  }
  return a;
}



void array_cleanup(array *a)

{
  char *pcVar1;
  
  pcVar1 = (char *)a;
  if (a->num != 0) {
    pcVar1 = "a->num == 0";
    badassert("a->num == 0","../../lib/array.c",0x47,"array_cleanup");
  }
  kfree(*(void ***)pcVar1);
  a->v = (void **)0x0;
  return;
}



void stringarray_destroy(stringarray *a)

{
  array_cleanup(&a->arr);
  kfree(a);
  return;
}



void stringarray_cleanup(stringarray *a)

{
  array_cleanup(&a->arr);
  return;
}



void array_destroy(array *a)

{
  array_cleanup(a);
  kfree(a);
  return;
}



int array_preallocate(array *a,uint num)

{
  bool bVar1;
  int iVar2;
  void **dst;
  uint uVar3;
  
  uVar3 = a->max;
  if (uVar3 < num) {
    while (uVar3 < num) {
      bVar1 = uVar3 == 0;
      uVar3 = uVar3 << 1;
      if (bVar1) {
        uVar3 = 4;
      }
    }
    dst = (void **)kmalloc(uVar3 << 2);
    if (dst == (void **)0x0) {
      iVar2 = 3;
    }
    else {
      memcpy(dst,a->v,a->num << 2);
      kfree(a->v);
      a->v = dst;
      a->max = uVar3;
      iVar2 = 0;
    }
  }
  else {
    iVar2 = 0;
  }
  return iVar2;
}



int stringarray_preallocate(stringarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(&a->arr,num);
  return iVar1;
}



int array_setsize(array *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(a,num);
  if (iVar1 == 0) {
    a->num = num;
  }
  return iVar1;
}



int array_add(array *a,void *val,uint *index_ret)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = a->num;
  iVar1 = array_setsize(a,uVar2 + 1);
  if (iVar1 == 0) {
    a->v[uVar2] = val;
    if (index_ret == (uint *)0x0) {
      iVar1 = 0;
    }
    else {
      *index_ret = uVar2;
      iVar1 = 0;
    }
  }
  return iVar1;
}



int stringarray_add(stringarray *a,char *val,uint *index_ret)

{
  int iVar1;
  
  iVar1 = array_add(&a->arr,val,index_ret);
  return iVar1;
}



int stringarray_setsize(stringarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_setsize(&a->arr,num);
  return iVar1;
}



void array_remove(array *a,uint index)

{
  char *pcVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar1 = (char *)a->num;
  pcVar3 = (char *)a;
  pcVar4 = (char *)index;
  if ((char *)a->max < pcVar1) {
    pcVar3 = "a->num <= a->max";
    pcVar4 = "../../lib/array.c";
    badassert("a->num <= a->max","../../lib/array.c",0x81,"array_remove");
  }
  iVar2 = (int)pcVar1 - (int)pcVar4;
  if (pcVar1 <= pcVar4) {
    pcVar3 = "index < a->num";
    pcVar4 = "../../lib/array.c";
    badassert("index < a->num","../../lib/array.c",0x82,"array_remove");
  }
  memmove(*(void ***)pcVar3 + (int)pcVar4,*(void ***)pcVar3 + (int)(pcVar4 + 1),(iVar2 + -1) * 4);
  a->num = a->num - 1;
  return;
}



void stringarray_remove(stringarray *a,uint index)

{
  array_remove(&a->arr,index);
  return;
}



bitmap * bitmap_create(uint nbits)

{
  bitmap *ptr;
  uchar *vblock;
  uint uVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar4 = (char *)(nbits + 7 >> 3);
  ptr = (bitmap *)kmalloc(8);
  if (ptr == (bitmap *)0x0) {
    ptr = (bitmap *)0x0;
  }
  else {
    vblock = (uchar *)kmalloc((size_t)pcVar4);
    ptr->v = vblock;
    if (vblock == (uchar *)0x0) {
      kfree(ptr);
      ptr = (bitmap *)0x0;
    }
    else {
      bzero(vblock,(size_t)pcVar4);
      ptr->nbits = nbits;
      if ((char *)(nbits >> 3) < pcVar4) {
        pcVar4 = pcVar4 + -1;
        uVar1 = nbits + (int)pcVar4 * -8;
        if ((char *)(nbits >> 3) != pcVar4) {
          pcVar4 = s_______lib_bitmap_c_800235b4;
          badassert("nbits / BITS_PER_WORD == words-1",s_______lib_bitmap_c_800235b4,0x51,
                    "bitmap_create");
        }
        if (uVar1 - 1 < 7) {
          pcVar2 = (char *)0x1;
          goto LAB_8000ad00;
        }
        pcVar2 = "overbits > 0 && overbits < BITS_PER_WORD";
        pcVar4 = s_______lib_bitmap_c_800235b4;
        pcVar3 = "bitmap_create";
        badassert("overbits > 0 && overbits < BITS_PER_WORD",s_______lib_bitmap_c_800235b4,0x52,
                  "bitmap_create");
        do {
          ptr->v[(int)pcVar4] = (byte)pcVar3 | ptr->v[(int)pcVar4];
          uVar1 = uVar1 + 1;
LAB_8000ad00:
          pcVar3 = (char *)((int)pcVar2 << (uVar1 & 0x1f));
        } while (uVar1 < 8);
      }
    }
  }
  return ptr;
}



void * bitmap_getdata(bitmap *b)

{
  return b->v;
}



int bitmap_alloc(bitmap *b,uint *index)

{
  int iVar1;
  char *pcVar2;
  uint uVar3;
  char *pcVar4;
  char *pcVar5;
  byte *pbVar6;
  char *pcVar7;
  char *pcVar8;
  uint uVar9;
  int iVar10;
  
  pcVar7 = (char *)(b->nbits + 7 >> 3);
  uVar9 = 0xff;
  pcVar8 = (char *)0x0;
  pcVar4 = (char *)b;
  pcVar5 = (char *)index;
  do {
    do {
      pcVar2 = pcVar8;
      if (pcVar7 <= pcVar2) {
        return 0x24;
      }
      pbVar6 = *(uchar **)((int)pcVar4 + 4) + (int)pcVar2;
      uVar3 = (uint)*pbVar6;
      pcVar8 = pcVar2 + 1;
    } while (uVar3 == uVar9);
    iVar10 = 1;
    for (pcVar8 = (char *)0x0; uVar9 = iVar10 << ((uint)pcVar8 & 0x1f), pcVar8 < (char *)0x8;
        pcVar8 = pcVar8 + 1) {
      if ((uVar3 & uVar9 & 0xff) == 0) {
        uVar3 = uVar3 | uVar9 & 0xff;
        *pbVar6 = (byte)uVar3;
        iVar1 = (int)pcVar2 * 8;
        *(char **)pcVar5 = pcVar8 + iVar1;
        pcVar2 = (char *)*(uint *)pcVar4;
        if (pcVar8 + iVar1 < pcVar2) {
          return 0;
        }
        pcVar4 = "*index < b->nbits";
        pcVar5 = s_______lib_bitmap_c_800235b4;
        pbVar6 = (byte *)0x71;
        pcVar8 = "bitmap_alloc";
        badassert("*index < b->nbits",s_______lib_bitmap_c_800235b4,0x71,"bitmap_alloc");
      }
    }
    pcVar4 = "0";
    pcVar5 = s_______lib_bitmap_c_800235b4;
    pcVar7 = "bitmap_alloc";
    badassert("0",s_______lib_bitmap_c_800235b4,0x75,"bitmap_alloc");
    pcVar8 = pcVar2;
  } while( true );
}



void bitmap_mark(bitmap *b,uint index)

{
  uint uVar1;
  byte *pbVar2;
  char *pcVar3;
  char *pcVar4;
  uint uVar5;
  byte bVar6;
  
  uVar1 = index >> 3;
  if (b->nbits <= index) {
    pcVar3 = "index < b->nbits";
    pcVar4 = s_______lib_bitmap_c_800235b4;
    badassert("index < b->nbits",s_______lib_bitmap_c_800235b4,0x8c,"bitmap_mark");
    b = (bitmap *)pcVar3;
    index = (uint)pcVar4;
  }
  uVar5 = 1 << (index & 7) & 0xff;
  pbVar2 = b->v + uVar1;
  bVar6 = *pbVar2 | (byte)uVar5;
  if ((*pbVar2 & uVar5) != 0) {
    bVar6 = 0xb4;
    badassert("(b->v[ix] & mask)==0",s_______lib_bitmap_c_800235b4,0x8f,"bitmap_mark");
  }
  *pbVar2 = bVar6;
  return;
}



void bitmap_unmark(bitmap *b,uint index)

{
  uint uVar1;
  byte *pbVar2;
  byte bVar3;
  char *pcVar4;
  char *pcVar5;
  uint uVar6;
  byte bVar7;
  
  uVar1 = index >> 3;
  if (b->nbits <= index) {
    pcVar4 = "index < b->nbits";
    pcVar5 = s_______lib_bitmap_c_800235b4;
    badassert("index < b->nbits",s_______lib_bitmap_c_800235b4,0x99,"bitmap_unmark");
    b = (bitmap *)pcVar4;
    index = (uint)pcVar5;
  }
  uVar6 = 1 << (index & 7) & 0xff;
  pbVar2 = b->v + uVar1;
  bVar3 = *pbVar2;
  bVar7 = ~(byte)uVar6;
  if ((bVar3 & uVar6) == 0) {
    bVar7 = 0xb4;
    badassert("(b->v[ix] & mask)!=0",s_______lib_bitmap_c_800235b4,0x9c,"bitmap_unmark");
  }
  *pbVar2 = bVar3 & bVar7;
  return;
}



int bitmap_isset(bitmap *b,uint index)

{
  return 1 << (index & 7) & (uint)b->v[index >> 3];
}



void bitmap_destroy(bitmap *b)

{
  kfree(b->v);
  kfree(b);
  return;
}



uint16_t bswap16(uint16_t val)

{
  return val << 8 | (ushort)((uint)val >> 8);
}



uint32_t bswap32(uint32_t val)

{
  return val << 0x18 | (val & 0xff00) << 8 | val >> 8 & 0xff00 | val >> 0x18;
}



uint64_t bswap64(uint64_t val)

{
  uint in_a0;
  uint in_a1;
  
  return CONCAT44(in_a1 << 0x18 | (in_a1 & 0xff00) << 8 | (in_a1 & 0xff0000) >> 8 | in_a1 >> 0x18 |
                  (in_a0 & 0xff) << 8,(in_a0 & 0xff0000) >> 8 | in_a0 >> 0x18);
}



uint16_t ntohs(uint16_t val)

{
  return val;
}



uint16_t htons(uint16_t val)

{
  return val;
}



uint32_t ntohl(uint32_t val)

{
  return val;
}



uint32_t htonl(uint32_t val)

{
  return val;
}



uint64_t ntohll(uint64_t val)

{
  undefined4 in_a0;
  undefined4 in_a1;
  
  return CONCAT44(in_a0,in_a1);
}



uint64_t htonll(uint64_t val)

{
  undefined4 in_a0;
  undefined4 in_a1;
  
  return CONCAT44(in_a0,in_a1);
}



void join32to64(uint32_t x1,uint32_t x2,uint64_t *y2)

{
  *(uint32_t *)y2 = x1;
  *(uint32_t *)((int)y2 + 4) = x2;
  return;
}



void split64to32(uint64_t x,uint32_t *y1,uint32_t *y2)

{
  uint32_t **in_a2;
  uint32_t **in_a3;
  
  *in_a2 = y1;
  *in_a3 = y2;
  return;
}



void backsp(void)

{
  putch(8);
  putch(0x20);
  putch(8);
  return;
}



void kgets(char *buf,size_t maxlen)

{
  int ch;
  uint uVar1;
  
  uVar1 = 0;
  while ((ch = getch(), ch != 10 && (ch != 0xd))) {
    if ((ch - 0x20U < 0x5f) && (uVar1 < maxlen - 1)) {
      putch(ch);
      buf[uVar1] = (char)ch;
      uVar1 = uVar1 + 1;
    }
    else if (((ch == 8) || (ch == 0x7f)) && (uVar1 != 0)) {
      uVar1 = uVar1 - 1;
      backsp();
    }
    else {
      if (ch == 3) {
        putch(0x5e);
        putch(0x43);
        putch(10);
        uVar1 = 0;
        goto LAB_8000b2b0;
      }
      if (ch == 0x12) {
        buf[uVar1] = '\0';
        kprintf("^R\n%s",buf);
      }
      else if (ch == 0x15) {
        for (; uVar1 != 0; uVar1 = uVar1 - 1) {
          backsp();
        }
      }
      else if (ch == 0x17) {
        while ((uVar1 != 0 && (buf[uVar1 - 1] == ' '))) {
          backsp();
          uVar1 = uVar1 - 1;
        }
        while ((uVar1 != 0 && (buf[uVar1 - 1] != ' '))) {
          backsp();
          uVar1 = uVar1 - 1;
        }
      }
      else {
        beep();
      }
    }
  }
  putch(10);
LAB_8000b2b0:
  buf[uVar1] = '\0';
  return;
}



void console_send(void *junk,char *data,size_t len)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < len; uVar1 = uVar1 + 1) {
    putch((int)data[uVar1]);
  }
  return;
}



int kprintf(char *fmt,...)

{
  bool bVar1;
  int iVar2;
  undefined4 in_a1;
  undefined4 in_a2;
  undefined4 in_a3;
  int unaff_s7;
  undefined4 local_res4;
  undefined4 local_res8;
  undefined4 local_resc;
  va_list ap;
  
  if (kprintf_lock == (lock *)0x0) {
    bVar1 = false;
  }
  else if (*(char *)(unaff_s7 + 0x58) == '\0') {
    if (*(int *)(unaff_s7 + 0x5c) == 0) {
      bVar1 = false;
      if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) == 0) {
        bVar1 = true;
      }
    }
    else {
      bVar1 = false;
    }
  }
  else {
    bVar1 = false;
  }
  local_res4 = in_a1;
  local_res8 = in_a2;
  local_resc = in_a3;
  if (bVar1) {
    lock_acquire(kprintf_lock);
  }
  else {
    spinlock_acquire(&kprintf_spinlock);
  }
  iVar2 = __vprintf(console_send,(void *)0x0,fmt,&local_res4);
  if (bVar1) {
    lock_release(kprintf_lock);
  }
  else {
    spinlock_release(&kprintf_spinlock);
  }
  return iVar2;
}



void panic(char *fmt,...)

{
  undefined4 in_a1;
  undefined4 in_a2;
  undefined4 in_a3;
  undefined4 local_res4;
  undefined4 local_res8;
  undefined4 local_resc;
  va_list ap;
  
  local_res4 = in_a1;
  local_res8 = in_a2;
  local_resc = in_a3;
  if (panic::evil == 0) {
    panic::evil = 1;
    splx(1);
  }
  if (panic::evil == 1) {
    panic::evil = 2;
    thread_panic();
  }
  if (panic::evil == 2) {
    panic::evil = 3;
    kprintf("panic: ");
    __vprintf(console_send,(void *)0x0,fmt,&local_res4);
  }
  if (panic::evil == 3) {
    panic::evil = 4;
    ltrace_stop(0);
  }
  if (panic::evil == 4) {
    panic::evil = 5;
    vfs_sync();
  }
  if (panic::evil == 5) {
    panic::evil = 6;
    mainbus_panic();
  }
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}



void badassert(char *expr,char *file,int line,char *func)

{
                    /* WARNING: Subroutine does not return */
  panic("Assertion failed: %s, at %s:%d (%s)\n",expr,file,line,func);
}



void kprintf_bootstrap(void)

{
  if (kprintf_lock != (lock *)0x0) {
    badassert("kprintf_lock == NULL","../../lib/kprintf.c",0x42,"kprintf_bootstrap");
  }
  kprintf_lock = lock_create("kprintf_lock");
  if (kprintf_lock == (lock *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Could not create kprintf_lock\n");
  }
  spinlock_init(&kprintf_spinlock);
  return;
}



char * kstrdup(char *s)

{
  size_t sVar1;
  char *dest;
  
  sVar1 = strlen(s);
  dest = (char *)kmalloc(sVar1 + 1);
  if (dest == (char *)0x0) {
    dest = (char *)0x0;
  }
  else {
    strcpy(dest,s);
  }
  return dest;
}



char * strerror(int errcode)

{
  if ((uint)errcode < 0x41) {
    return sys_errlist[errcode];
  }
                    /* WARNING: Subroutine does not return */
  panic("Invalid error code %d\n",errcode);
}



void timespec_add(timespec *ts1,timespec *ts2,timespec *ret)

{
  uint uVar1;
  int iVar2;
  uint uVar3;
  int iVar4;
  
  iVar4 = ts1->tv_nsec + ts2->tv_nsec;
  ret->tv_nsec = iVar4;
  uVar1 = *(uint *)((int)&ts1->tv_sec + 4);
  uVar3 = uVar1 + *(int *)((int)&ts2->tv_sec + 4);
  iVar2 = (uint)(uVar3 < uVar1) + *(int *)&ts1->tv_sec + *(int *)&ts2->tv_sec;
  *(int *)&ret->tv_sec = iVar2;
  *(uint *)((int)&ret->tv_sec + 4) = uVar3;
  if (999999999 < iVar4) {
    ret->tv_nsec = iVar4 + -1000000000;
    *(uint *)&ret->tv_sec = (uint)(uVar3 + 1 < uVar3) + iVar2;
    *(uint *)((int)&ret->tv_sec + 4) = uVar3 + 1;
  }
  return;
}



/* WARNING: Could not reconcile some variable overlaps */

void timespec_sub(timespec *ts1,timespec *ts2,timespec *ret)

{
  undefined4 uVar1;
  int iVar2;
  uint uVar3;
  timespec r;
  
  r.tv_sec._0_4_ = *(int *)&ts1->tv_sec;
  uVar3 = *(uint *)((int)&ts1->tv_sec + 4);
  r.tv_nsec = ts1->tv_nsec;
  uVar1 = *(undefined4 *)&ts1->field_0xc;
  iVar2 = ts2->tv_nsec;
  r.tv_sec._4_4_ = uVar3;
  if (r.tv_nsec < iVar2) {
    r.tv_nsec = r.tv_nsec + 1000000000;
    r.tv_sec._4_4_ = uVar3 - 1;
    r.tv_sec._0_4_ = (uint)(r.tv_sec._4_4_ < uVar3) + r.tv_sec._0_4_ + -1;
  }
  uVar3 = r.tv_sec._4_4_ - *(int *)((int)&ts2->tv_sec + 4);
  *(uint *)&ret->tv_sec = (r.tv_sec._0_4_ - *(int *)&ts2->tv_sec) - (uint)(r.tv_sec._4_4_ < uVar3);
  *(uint *)((int)&ret->tv_sec + 4) = uVar3;
  ret->tv_nsec = r.tv_nsec - iVar2;
  *(undefined4 *)&ret->field_0xc = uVar1;
  return;
}



int uiomove(void *ptr,size_t n,uio *uio)

{
  addrspace *paVar1;
  uint uVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  uio *puVar6;
  addrspace *paVar7;
  iovec *piVar8;
  
  if (UIO_WRITE < uio->uio_rw) {
                    /* WARNING: Subroutine does not return */
    panic("uiomove: Invalid uio_rw %d\n");
  }
  puVar6 = uio;
  if (uio->uio_segflg == UIO_SYSSPACE) {
    if (uio->uio_space == (addrspace *)0x0) goto LAB_8000b9dc;
    puVar6 = (uio *)0x34;
    badassert("uio->uio_space == NULL","../../lib/uio.c",0x34,"uiomove");
  }
  paVar7 = puVar6->uio_space;
  paVar1 = proc_getas();
  if (paVar7 == paVar1) goto LAB_8000b9dc;
  badassert("uio->uio_space == proc_getas()","../../lib/uio.c",0x37,"uiomove");
  do {
    piVar8 = uio->uio_iov;
    uVar2 = piVar8->iov_len;
    if (n < piVar8->iov_len) {
      uVar2 = n;
    }
    if (uVar2 == 0) {
      uio->uio_iov = piVar8 + 1;
      uVar2 = uio->uio_iovcnt - 1;
      uio->uio_iovcnt = uVar2;
      if (uVar2 == 0) {
                    /* WARNING: Subroutine does not return */
        panic("uiomove: ran out of buffers\n");
      }
    }
    else {
      if (uio->uio_segflg < UIO_SYSSPACE) {
        if (uio->uio_rw == UIO_READ) {
          iVar3 = copyout(ptr,(userptr_t)piVar8->field_0,uVar2);
        }
        else {
          iVar3 = copyin((const_userptr_t)piVar8->field_0,ptr,uVar2);
        }
        if (iVar3 != 0) {
          return iVar3;
        }
        piVar8->field_0 = piVar8->field_0 + uVar2;
      }
      else {
        if (uio->uio_segflg != UIO_SYSSPACE) {
                    /* WARNING: Subroutine does not return */
          panic("uiomove: Invalid uio_segflg %d\n");
        }
        if (uio->uio_rw == UIO_READ) {
          memmove((void *)piVar8->field_0,ptr,uVar2);
        }
        else {
          memmove(ptr,(void *)piVar8->field_0,uVar2);
        }
        piVar8->field_0 = piVar8->field_0 + uVar2;
      }
      piVar8->iov_len = piVar8->iov_len - uVar2;
      uio->uio_resid = uio->uio_resid - uVar2;
      uVar5 = *(uint *)((int)&uio->uio_offset + 4);
      uVar4 = uVar5 + uVar2;
      *(uint *)&uio->uio_offset = (uint)(uVar4 < uVar5) + *(int *)&uio->uio_offset;
      *(uint *)((int)&uio->uio_offset + 4) = uVar4;
      ptr = (void *)((int)ptr + uVar2);
      n = n - uVar2;
    }
LAB_8000b9dc:
    if (n == 0) {
      return 0;
    }
  } while (uio->uio_resid != 0);
  return 0;
}



int uiomovezeros(size_t n,uio *uio)

{
  uio_rw uVar1;
  int iVar2;
  uint unaff_s0;
  size_t n_00;
  char *unaff_s3;
  
  uVar1 = uio->uio_rw;
  if (uVar1 == UIO_READ) {
    unaff_s3 = uiomovezeros::zeros;
    goto LAB_8000ba98;
  }
  badassert("uio->uio_rw == UIO_READ","../../lib/uio.c",0x82,"uiomovezeros");
  n = unaff_s0;
  while( true ) {
    n_00 = 0x10;
    if (uVar1 != UIO_READ) {
      n_00 = n;
    }
    iVar2 = uiomove(unaff_s3,n_00,uio);
    n = n - n_00;
    if (iVar2 != 0) break;
LAB_8000ba98:
    uVar1 = (uio_rw)(n < 0x10);
    if (n == 0) {
      return 0;
    }
  }
  return iVar2;
}



void uio_kinit(iovec *iov,uio *u,void *kbuf,size_t len,off_t pos,uio_rw rw)

{
  iov->field_0 = kbuf;
  iov->iov_len = len;
  u->uio_iov = iov;
  u->uio_iovcnt = 1;
  *(undefined4 *)((int)&u->uio_offset + 4) = pos._4_4_;
  *(undefined4 *)&u->uio_offset = pos._0_4_;
  u->uio_resid = len;
  u->uio_segflg = UIO_SYSSPACE;
  u->uio_rw = rw;
  u->uio_space = (addrspace *)0x0;
  return;
}



int shutdown(int __fd,int __how)

{
  int iVar1;
  
  kprintf("Shutting down.\n");
  vm_shutdown();
  vfs_clearbootfs();
  vfs_clearcurdir();
  vfs_unmountall();
  thread_shutdown();
  iVar1 = splx(1);
  return iVar1;
}



void boot(void)

{
  int unaff_s7;
  
  kprintf("\n");
  kprintf("OS/161 base system version %s\n",&UNK_80023dbc);
  kprintf("%s",
          "Copyright (c) 2000, 2001-2005, 2008-2011, 2013, 2014\n   President and Fellows of Harvard College.  All rights reserved.\n"
         );
  kprintf("\n");
  kprintf("Celada-Macori-Perticone %s (%s #%d)\n","0","PAGING",buildversion);
  kprintf("\n");
  ram_bootstrap();
  vm_bootstrap();
  proc_bootstrap();
  thread_bootstrap();
  hardclock_bootstrap();
  vfs_bootstrap();
  kheap_nextgeneration();
  kprintf("Device probe...\n");
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    badassert("curthread->t_curspl > 0","../../main/main.c",0x7b,"boot");
  }
  mainbus_bootstrap();
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../main/main.c",0x7d,"boot");
  }
  pseudoconfig();
  kprintf("\n");
  kheap_nextgeneration();
  kprintf_bootstrap();
  thread_start_cpus();
  vfs_setbootfs((char *)&DAT_80023e48);
  kheap_nextgeneration();
  init_swapfile();
  return;
}



int sys_reboot(int code)

{
  int in_a1;
  
  if ((uint)code < 3) {
    shutdown(code,in_a1);
    if (code == 1) {
      kprintf("The system is halted.\n");
      mainbus_halt();
    }
    else if (code == 2) {
      kprintf("The system is halted.\n");
      mainbus_poweroff();
    }
    else if (code == 0) {
      kprintf("Rebooting...\n");
      mainbus_reboot();
    }
                    /* WARNING: Subroutine does not return */
    panic("reboot operation failed\n");
  }
  return 8;
}



void kmain(char *arguments)

{
  boot();
  menu(arguments);
  return;
}



void showmenu(char *name,char **x)

{
  int iVar1;
  int iVar2;
  char **ppcVar3;
  int iVar4;
  int iVar5;
  
  kprintf("\n");
  kprintf("%s\n",name);
  iVar1 = 0;
  iVar4 = 0;
  while (ppcVar3 = x + iVar1, iVar1 = iVar1 + 1, *ppcVar3 != (char *)0x0) {
    iVar4 = iVar4 + 1;
  }
  iVar5 = (iVar4 + 1) / 2;
  for (iVar1 = 0; iVar1 < iVar5; iVar1 = iVar1 + 1) {
    kprintf("    %-36s",x[iVar1]);
    iVar2 = iVar1 + iVar5;
    if (iVar2 < iVar4) {
      kprintf("%s",x[iVar2]);
    }
    kprintf("\n");
  }
  kprintf("\n");
  return;
}



int cmd_opsmenu(int n,char **a)

{
  showmenu("OS/161 operations menu",opsmenu);
  return 0;
}



int cmd_mainmenu(int n,char **a)

{
  showmenu("OS/161 kernel menu",mainmenu);
  return 0;
}



int cmd_testmenu(int n,char **a)

{
  showmenu("OS/161 tests menu",testmenu);
  kprintf("    (1) These tests will fail until you finish the synch assignment.\n");
  kprintf("\n");
  return 0;
}



int cmd_panic(int nargs,char **args)

{
                    /* WARNING: Subroutine does not return */
  panic("User requested panic\n");
}



int cmd_dispatch(char *cmd)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  timespec before;
  timespec after;
  timespec duration;
  char *args [16];
  char *context;
  
  pcVar2 = strtok_r(cmd," \t",&context);
  iVar4 = 0;
  while (pcVar2 != (char *)0x0) {
    if (0xf < iVar4) {
      kprintf("Command line has too many words\n");
      return 0xe;
    }
    args[iVar4] = pcVar2;
    pcVar2 = strtok_r((char *)0x0," \t",&context);
    iVar4 = iVar4 + 1;
  }
  iVar1 = 0;
  if (iVar4 == 0) {
    iVar4 = 0;
  }
  else {
    do {
      while( true ) {
        iVar5 = iVar1;
        pcVar2 = cmdtable[iVar5].name;
        if (pcVar2 == (char *)0x0) {
          kprintf("%s: Command not found\n",args[0]);
          return 8;
        }
        if (*pcVar2 != '\0') break;
        iVar1 = iVar5 + 1;
      }
      iVar3 = strcmp(args[0],pcVar2);
      iVar1 = iVar5 + 1;
    } while (iVar3 != 0);
    if (cmdtable[iVar5].func == (anon_subr_int_int_char_ptr_ptr_for_func *)0x0) {
      badassert("cmdtable[i].func != NULL","../../main/menu.c",0x304,"cmd_dispatch");
    }
    gettime(&before);
    iVar4 = (*cmdtable[iVar5].func)(iVar4,args);
    gettime(&after);
    timespec_sub(&after,&before,&duration);
    kprintf("Operation took %llu.%09lu seconds\n");
  }
  return iVar4;
}



void menu_execute(char *line,int isargs)

{
  char *pcVar1;
  int errcode;
  char *context;
  
  pcVar1 = strtok_r(line,";",&context);
  do {
    if (pcVar1 == (char *)0x0) {
      return;
    }
    if (isargs != 0) {
      kprintf("OS/161 kernel: %s\n",pcVar1);
    }
    errcode = cmd_dispatch(pcVar1);
    if (errcode != 0) {
      pcVar1 = strerror(errcode);
      kprintf("Menu command failed: %s\n",pcVar1);
      if (isargs != 0) {
                    /* WARNING: Subroutine does not return */
        panic("Failure processing kernel arguments\n");
      }
    }
    pcVar1 = strtok_r((char *)0x0,";",&context);
  } while( true );
}



int cmd_kheapdump(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 1) {
    kheap_dump();
  }
  else if ((nargs == 2) && (iVar1 = strcmp(args[1],"all"), iVar1 == 0)) {
    kheap_dumpall();
  }
  else {
    kprintf("Usage: khdump [all]\n");
  }
  return 0;
}



int cmd_kheapgeneration(int nargs,char **args)

{
  kheap_nextgeneration();
  return 0;
}



int cmd_kheapstats(int nargs,char **args)

{
  kheap_printstats();
  return 0;
}



int cmd_sync(int nargs,char **args)

{
  vfs_sync();
  return 0;
}



int cmd_quit(int nargs,char **args)

{
  vfs_sync();
  sys_reboot(2);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}



void cmd_deadlockthread(void *ptr,ulong num)

{
  do {
    lock_acquire(*(lock **)((int)ptr + 4));
                    /* WARNING: Load size is inaccurate */
    lock_acquire(*ptr);
    kprintf("+");
                    /* WARNING: Load size is inaccurate */
    lock_release(*ptr);
    lock_release(*(lock **)((int)ptr + 4));
  } while( true );
}



int cmd_debug(int nargs,char **args)

{
  mainbus_debugger();
  return 0;
}



int cmd_pwd(int nargs,char **args)

{
  int errcode;
  char *pcVar1;
  char buf [1025];
  iovec iov;
  uio ku;
  
  uio_kinit(&iov,&ku,buf,0x400,0,UIO_READ);
  errcode = vfs_getcwd(&ku);
  if (errcode == 0) {
    buf[0x400 - ku.uio_resid] = '\0';
    kprintf("%s\n",buf);
    errcode = 0;
  }
  else {
    pcVar1 = strerror(errcode);
    kprintf("vfs_getcwd failed (%s)\n",pcVar1);
  }
  return errcode;
}



int cmd_chdir(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 2) {
    iVar1 = vfs_chdir(args[1]);
  }
  else {
    kprintf("Usage: cd directory\n");
    iVar1 = 8;
  }
  return iVar1;
}



int cmd_bootfs(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  char *str;
  
  if (nargs == 2) {
    str = args[1];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
    }
    iVar1 = vfs_setbootfs(str);
  }
  else {
    kprintf("Usage: bootfs device\n");
    iVar1 = 8;
  }
  return iVar1;
}



int cmd_unmount(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  char *str;
  
  if (nargs == 2) {
    str = args[1];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
    }
    iVar1 = vfs_unmount(str);
  }
  else {
    kprintf("Usage: unmount device:\n");
    iVar1 = 8;
  }
  return iVar1;
}



int cmd_mount(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  bool bVar3;
  char *b;
  char *str;
  
  if (nargs == 3) {
    b = args[1];
    str = args[2];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
    }
    bVar3 = false;
    do {
      if (bVar3) {
        kprintf("Unknown filesystem type %s\n",b);
        return 8;
      }
      iVar1 = strcmp(mounttable[0].name,b);
      bVar3 = true;
    } while (iVar1 != 0);
    iVar1 = sfs_mount(str);
  }
  else {
    kprintf("Usage: mount fstype device:\n");
    iVar1 = 8;
  }
  return iVar1;
}



int common_prog(int nargs,char **args)

{
  proc *proc;
  char *pcVar1;
  int result;
  
  proc = proc_create_runprogram(*args);
  if (proc == (proc *)0x0) {
    result = 3;
  }
  else {
    result = thread_fork(*args,proc,cmd_progthread,args,nargs);
    if (result == 0) {
      result = sys_waitpid(proc->p_pid,(userptr_t)&result,0);
      if (result == -1) {
        kprintf("waiting for process failed\n");
        result = 0;
      }
      else {
        kprintf("Process returned with exit value %d\n",result);
        result = 0;
      }
    }
    else {
      pcVar1 = strerror(result);
      kprintf("thread_fork failed: %s\n",pcVar1);
      proc_destroy(proc);
    }
  }
  return result;
}



int cmd_prog(int nargs,char **args)

{
  int iVar1;
  
  if (nargs < 2) {
    kprintf("Usage: p program [arguments]\n");
    iVar1 = 8;
  }
  else {
    iVar1 = common_prog(nargs + -1,args + 1);
  }
  return iVar1;
}



int cmd_shell(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 1) {
    *args = "/bin/sh";
    iVar1 = common_prog(1,args);
  }
  else {
    kprintf("Usage: s\n");
    iVar1 = 8;
  }
  return iVar1;
}



void cmd_progthread(void *ptr,ulong nargs)

{
  size_t sVar1;
  int errcode;
  char *pcVar2;
  char *pcVar3;
  char progname [128];
  
  if (nargs == 0) {
    pcVar2 = "nargs >= 1";
    pcVar3 = "../../main/menu.c";
    badassert("nargs >= 1","../../main/menu.c",0x53,"cmd_progthread");
    ptr = pcVar2;
    nargs = (ulong)pcVar3;
  }
  if ((char *)0x2 < nargs) {
    kprintf("Warning: argument passing from menu not supported\n");
  }
                    /* WARNING: Load size is inaccurate */
  sVar1 = strlen(*ptr);
  if (0x7f < sVar1) {
    badassert("strlen(args[0]) < sizeof(progname)","../../main/menu.c",0x5b,"cmd_progthread");
  }
                    /* WARNING: Load size is inaccurate */
  strcpy(progname,*ptr);
  errcode = runprogram(progname);
  if (errcode != 0) {
                    /* WARNING: Load size is inaccurate */
    pcVar3 = *ptr;
    pcVar2 = strerror(errcode);
    kprintf("Running program %s failed: %s\n",pcVar3,pcVar2);
    sys__exit(-1);
  }
  return;
}



void menu(char *args)

{
  char buf [64];
  
  menu_execute(args,1);
  do {
    kprintf("OS/161 kernel [? for menu]: ");
    kgets(buf,0x40);
    menu_execute(buf,0);
  } while( true );
}



void proc_end_waitpid(proc *proc)

{
  int iVar1;
  
  spinlock_acquire(&processTable.lk);
  iVar1 = proc->p_pid << 2;
  if (99 < proc->p_pid - 1U) {
    badassert("i > 0 && i <= MAX_PROC","../../proc/proc.c",0x58,"proc_end_waitpid");
  }
  *(undefined4 *)((int)processTable.proc + iVar1) = 0;
  spinlock_release(&processTable.lk);
  cv_destroy(proc->p_cv);
  lock_destroy(proc->p_wlock);
  return;
}



void proc_init_waitpid(proc *proc,char *name)

{
  int iVar1;
  int iVar2;
  cv *pcVar3;
  lock *plVar4;
  
  spinlock_acquire(&processTable.lk);
  iVar1 = processTable.last_i + 1;
  proc->p_pid = 0;
  if (99 < iVar1) {
    iVar1 = 1;
  }
  do {
    if (processTable.last_i == iVar1) {
LAB_8000cb14:
      spinlock_release(&processTable.lk);
      if (proc->p_pid != 0) {
        proc->p_status = 0;
        pcVar3 = cv_create(name);
        proc->p_cv = pcVar3;
        plVar4 = lock_create(name);
        proc->p_wlock = plVar4;
        return;
      }
                    /* WARNING: Subroutine does not return */
      panic("too many processes. proc table is full\n");
    }
    iVar2 = iVar1 + 1;
    if (processTable.proc[iVar1] == (proc *)0x0) {
      processTable.proc[iVar1] = proc;
      processTable.last_i = iVar1;
      proc->p_pid = iVar1;
      goto LAB_8000cb14;
    }
    iVar1 = iVar2;
    if (99 < iVar2) {
      iVar1 = 1;
    }
  } while( true );
}



proc * proc_create(char *name)

{
  proc *proc;
  char *pcVar1;
  
  proc = (proc *)kmalloc(0x264);
  if (proc == (proc *)0x0) {
    proc = (proc *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    proc->p_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(proc);
      proc = (proc *)0x0;
    }
    else {
      proc->p_numthreads = 0;
      spinlock_init(&proc->p_lock);
      proc->p_addrspace = (addrspace *)0x0;
      proc->p_cwd = (vnode *)0x0;
      proc->finish = 0;
      proc->last_victim = -1;
      proc_init_waitpid(proc,name);
      bzero(proc->fileTable,0x200);
    }
  }
  return proc;
}



addrspace * pid_getas(pid_t pid)

{
  uint uVar1;
  char *pcVar2;
  int unaff_s7;
  
  uVar1 = pid - 1;
  if (*(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c) != pid) {
    pcVar2 = "pid == curproc->p_pid";
    badassert("pid == curproc->p_pid","../../proc/proc.c",0x97,"pid_getas");
    pid = (pid_t)pcVar2;
  }
  pcVar2 = (char *)(pid << 2);
  if (99 < uVar1) {
    pcVar2 = "pid > 0 && pid < MAX_PROC + 1";
    badassert("pid > 0 && pid < MAX_PROC + 1","../../proc/proc.c",0x98,"pid_getas");
  }
  return *(addrspace **)(*(int *)((int)processTable.proc + (int)pcVar2) + 0x10);
}



void proc_bootstrap(void)

{
  kproc = proc_create("[kernel]");
  if (kproc == (proc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("proc_create for kproc failed\n");
  }
  spinlock_init(&processTable.lk);
  processTable.active = 1;
  return;
}



proc * proc_create_runprogram(char *name)

{
  proc *ppVar1;
  vnode *vn;
  int unaff_s7;
  
  ppVar1 = proc_create(name);
  if (ppVar1 == (proc *)0x0) {
    ppVar1 = (proc *)0x0;
  }
  else {
    ppVar1->p_addrspace = (addrspace *)0x0;
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    if (vn != (vnode *)0x0) {
      vnode_incref(vn);
      ppVar1->p_cwd = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    }
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  }
  return ppVar1;
}



int proc_addthread(proc *proc,thread *t)

{
  int spl;
  char *pcVar1;
  
  if (t->t_proc != (proc *)0x0) {
    pcVar1 = "t->t_proc == NULL";
    badassert("t->t_proc == NULL","../../proc/proc.c",0x16f,"proc_addthread");
    proc = (proc *)pcVar1;
  }
  spinlock_acquire(&proc->p_lock);
  proc->p_numthreads = proc->p_numthreads + 1;
  spinlock_release(&proc->p_lock);
  spl = splx(1);
  t->t_proc = proc;
  splx(spl);
  return 0;
}



void proc_remthread(thread *t)

{
  uint uVar1;
  int spl;
  proc *ppVar2;
  
  ppVar2 = t->t_proc;
  if (ppVar2 == (proc *)0x0) {
    badassert("proc != NULL","../../proc/proc.c",0x18b,"proc_remthread");
  }
  spinlock_acquire(&ppVar2->p_lock);
  uVar1 = ppVar2->p_numthreads - 1;
  if (ppVar2->p_numthreads == 0) {
    badassert("proc->p_numthreads > 0","../../proc/proc.c",0x18e,"proc_remthread");
  }
  ppVar2->p_numthreads = uVar1;
  spinlock_release(&ppVar2->p_lock);
  spl = splx(1);
  t->t_proc = (proc *)0x0;
  splx(spl);
  return;
}



addrspace * proc_getas(void)

{
  int iVar1;
  addrspace *paVar2;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    paVar2 = (addrspace *)0x0;
  }
  else {
    spinlock_acquire((spinlock *)(iVar1 + 8));
    paVar2 = *(addrspace **)(iVar1 + 0x10);
    spinlock_release((spinlock *)(iVar1 + 8));
  }
  return paVar2;
}



addrspace * proc_setas(addrspace *newas)

{
  int iVar1;
  addrspace *paVar2;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    badassert("proc != NULL","../../proc/proc.c",0x1ba,"proc_setas");
  }
  spinlock_acquire((spinlock *)(iVar1 + 8));
  paVar2 = *(addrspace **)(iVar1 + 0x10);
  *(addrspace **)(iVar1 + 0x10) = newas;
  spinlock_release((spinlock *)(iVar1 + 8));
  return paVar2;
}



void proc_destroy(proc *proc)

{
  char *pcVar1;
  addrspace *as;
  int unaff_s7;
  
  pcVar1 = (char *)proc;
  if (proc == (proc *)0x0) {
    pcVar1 = "proc != NULL";
    badassert("proc != NULL","../../proc/proc.c",0xdc,"proc_destroy");
  }
  if ((proc *)pcVar1 == kproc) {
    pcVar1 = "proc != kproc";
    badassert("proc != kproc","../../proc/proc.c",0xdd,"proc_destroy");
  }
  if (*(vnode **)((int)pcVar1 + 0x14) != (vnode *)0x0) {
    vnode_decref(*(vnode **)((int)pcVar1 + 0x14));
    proc->p_cwd = (vnode *)0x0;
  }
  as = proc->p_addrspace;
  if (as != (addrspace *)0x0) {
    if (*(proc **)(unaff_s7 + 0x54) == proc) {
      as_deactivate();
      as = proc_setas((addrspace *)0x0);
    }
    else {
      proc->p_addrspace = (addrspace *)0x0;
    }
    as_destroy(as);
  }
  if (proc->p_numthreads != 0) {
    badassert("proc->p_numthreads == 0","../../proc/proc.c",0x120,"proc_destroy");
  }
  spinlock_cleanup(&proc->p_lock);
  proc_end_waitpid(proc);
  kfree(proc->p_name);
  kfree(proc);
  return;
}



int proc_wait(proc *proc)

{
  char *pcVar1;
  int iVar2;
  
  pcVar1 = (char *)proc;
  if (proc == (proc *)0x0) {
    pcVar1 = "proc != NULL";
    badassert("proc != NULL","../../proc/proc.c",0x1c8,"proc_wait");
  }
  if ((proc *)pcVar1 == kproc) {
    pcVar1 = "proc != kproc";
    badassert("proc != kproc","../../proc/proc.c",0x1c9,"proc_wait");
  }
  lock_acquire(*(lock **)((int)pcVar1 + 0x24));
  if (proc->finish == 0) {
    cv_wait(proc->p_cv,proc->p_wlock);
  }
  lock_release(proc->p_wlock);
  iVar2 = proc->p_status;
  proc_destroy(proc);
  return iVar2;
}



proc * proc_search_pid(pid_t pid)

{
  proc *ppVar1;
  
  if (99 < (uint)pid) {
    badassert("pid >= 0 && pid < MAX_PROC","../../proc/proc.c",0x1e3,"proc_search_pid");
  }
  spinlock_acquire(&processTable.lk);
  ppVar1 = processTable.proc[pid];
  spinlock_release(&processTable.lk);
  if (ppVar1->p_pid != pid) {
    badassert("p->p_pid == pid","../../proc/proc.c",0x1e7,"proc_search_pid");
  }
  return ppVar1;
}



int file_write(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  vnode *v;
  vnode **ppvVar2;
  int unaff_s7;
  iovec iov;
  uio u;
  
  if ((uint)fd < 0x81) {
    ppvVar2 = *(vnode ***)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
    if (ppvVar2 == (vnode **)0x0) {
      iVar1 = -1;
    }
    else {
      v = *ppvVar2;
      u.uio_iov = &iov;
      if (v == (vnode *)0x0) {
        iVar1 = -1;
      }
      else {
        u.uio_iovcnt = 1;
        u.uio_offset._4_4_ = ppvVar2[3];
        u.uio_offset._0_4_ = ppvVar2[2];
        u.uio_segflg = UIO_USERISPACE;
        u.uio_rw = UIO_WRITE;
        u.uio_space = *(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10);
        iov.field_0 = buf_ptr;
        iov.iov_len = size;
        u.uio_resid = size;
        vnode_check(v,s_console_write_80022690 + 8);
        iVar1 = (*v->vn_ops->vop_write)(v,&u);
        if (iVar1 == 0) {
          ppvVar2[3] = u.uio_offset._4_4_;
          ppvVar2[2] = u.uio_offset._0_4_;
          iVar1 = size - u.uio_resid;
        }
      }
    }
    return iVar1;
  }
  return -1;
}



int file_read(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  vnode *v;
  vnode **ppvVar2;
  int unaff_s7;
  iovec iov;
  uio u;
  
  if ((uint)fd < 0x81) {
    ppvVar2 = *(vnode ***)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
    if (ppvVar2 == (vnode **)0x0) {
      iVar1 = -1;
    }
    else {
      v = *ppvVar2;
      u.uio_iov = &iov;
      if (v == (vnode *)0x0) {
        iVar1 = -1;
      }
      else {
        u.uio_iovcnt = 1;
        u.uio_offset._4_4_ = ppvVar2[3];
        u.uio_offset._0_4_ = ppvVar2[2];
        u.uio_segflg = UIO_USERISPACE;
        u.uio_rw = UIO_READ;
        u.uio_space = *(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10);
        iov.field_0 = buf_ptr;
        iov.iov_len = size;
        u.uio_resid = size;
        vnode_check(v,"read");
        iVar1 = (*v->vn_ops->vop_read)(v,&u);
        if (iVar1 == 0) {
          ppvVar2[3] = u.uio_offset._4_4_;
          ppvVar2[2] = u.uio_offset._0_4_;
          iVar1 = size - u.uio_resid;
        }
      }
    }
    return iVar1;
  }
  return -1;
}



void openfileIncrRefCount(openfile *of)

{
  if (of != (openfile *)0x0) {
    of->countRef = of->countRef + 1;
  }
  return;
}



bool filedes_is_seekable(openfile *of)

{
  undefined uVar1;
  openfile *poVar2;
  
  poVar2 = of;
  if (of == (openfile *)0x0) {
    poVar2 = (openfile *)&DAT_80024b6c;
    badassert((char *)&DAT_80024b6c,"../../syscall/file_syscalls.c",0xf9,"filedes_is_seekable");
  }
  if (poVar2->vn == (vnode *)0x0) {
    uVar1 = 0;
  }
  else {
    vnode_check(poVar2->vn,"isseekable");
    uVar1 = (*of->vn->vn_ops->vop_isseekable)(of->vn);
  }
  return (bool)uVar1;
}



int file_seek(openfile *of,off_t offset,int whence,int *errcode)

{
  undefined3 extraout_var;
  bool bVar2;
  int iVar1;
  undefined4 in_a3;
  
  if ((of == (openfile *)0x0) ||
     (bVar2 = filedes_is_seekable(of), CONCAT31(extraout_var,bVar2) == 0)) {
    *offset._4_4_ = 0x1e;
    iVar1 = -1;
  }
  else if (offset._0_4_ == 0) {
    if ((int)errcode < 0) {
      *offset._4_4_ = 8;
      iVar1 = -1;
    }
    else {
      *(int **)&of->offset = errcode;
      *(undefined4 *)((int)&of->offset + 4) = in_a3;
      iVar1 = 0;
    }
  }
  else {
    *offset._4_4_ = 8;
    kprintf("Error in file_seek: different seek start position not implemented\n");
    iVar1 = -1;
  }
  return iVar1;
}



int sys_lseek(int fd,off_t offset,int whence,int *retval)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  int in_a3;
  openfile *of;
  undefined4 uVar4;
  int unaff_s7;
  int local_20 [3];
  
  of = *(openfile **)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
  if (of == (openfile *)0x0) {
    kprintf("Error in sys_lseek: fd %d for process %d, file not open\n",fd,
            *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
    *offset._4_4_ = -1;
    local_20[0] = 0x1e;
  }
  else {
    local_20[0] = 0;
    iVar1 = file_seek(of,offset & 0xffffffff00000000U | ZEXT48(local_20),whence,retval);
    iVar3 = local_20[0];
    if ((iVar1 == 0) && (local_20[0] == 0)) {
      iVar3 = *(int *)((int)&of->offset + 4);
      if ((*(int **)&of->offset != retval) || (iVar3 != in_a3)) {
        badassert("of->offset == offset","../../syscall/file_syscalls.c",0xd7,"sys_lseek");
      }
      *offset._4_4_ = iVar3;
      local_20[0] = 0;
    }
    else {
      if ((dbflags & 2) != 0) {
        uVar4 = *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c);
        pcVar2 = strerror(local_20[0]);
        kprintf("Error in sys_lseek: fd %d for process %d, code=%d, err=%s\n",fd,uVar4,iVar3,pcVar2)
        ;
      }
      *offset._4_4_ = -1;
    }
  }
  return local_20[0];
}



int sys_open(userptr_t path,int openflags,mode_t mode,int *errp)

{
  int iVar1;
  openfile *poVar2;
  int unaff_s7;
  vnode *v;
  
  iVar1 = vfs_open(&path->_dummy,openflags,mode,&v);
  if (iVar1 == 0) {
    for (iVar1 = 0; iVar1 < 0x500; iVar1 = iVar1 + 1) {
      if (systemFileTable[iVar1].vn == (vnode *)0x0) {
        poVar2 = systemFileTable + iVar1;
        poVar2->vn = v;
        *(undefined4 *)((int)&systemFileTable[iVar1].offset + 4) = 0;
        *(undefined4 *)&systemFileTable[iVar1].offset = 0;
        systemFileTable[iVar1].countRef = 1;
        goto LAB_8000d7b4;
      }
    }
    poVar2 = (openfile *)0x0;
LAB_8000d7b4:
    iVar1 = 3;
    if (poVar2 == (openfile *)0x0) {
      *errp = 0x1d;
    }
    else {
      for (; iVar1 < 0x80; iVar1 = iVar1 + 1) {
        if (*(int *)(*(int *)(unaff_s7 + 0x54) + (iVar1 + 0x18) * 4) == 0) {
          *(openfile **)(*(int *)(unaff_s7 + 0x54) + (iVar1 + 0x18) * 4) = poVar2;
          return iVar1;
        }
      }
      *errp = 0x1c;
    }
    vfs_close(v);
  }
  else {
    *errp = 0x13;
  }
  return -1;
}



int sys_close(int fd)

{
  vnode **ppvVar1;
  vnode *pvVar2;
  int unaff_s7;
  
  if (0x80 < (uint)fd) {
    return -1;
  }
  ppvVar1 = *(vnode ***)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
  if (ppvVar1 == (vnode **)0x0) {
    return -1;
  }
  *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4) = 0;
  pvVar2 = (vnode *)((int)&ppvVar1[4][-1].vn_ops + 3);
  ppvVar1[4] = pvVar2;
  if (pvVar2 != (vnode *)0x0) {
    return 0;
  }
  pvVar2 = *ppvVar1;
  *ppvVar1 = (vnode *)0x0;
  if (pvVar2 != (vnode *)0x0) {
    vfs_close(pvVar2);
    return 0;
  }
  return -1;
}



int sys_write(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  
  if (fd - 1U < 2) {
    spinlock_acquire(&sp);
    for (iVar1 = 0; iVar1 < (int)size; iVar1 = iVar1 + 1) {
      putch((int)buf_ptr[iVar1]._dummy);
    }
    spinlock_release(&sp);
  }
  else {
    size = file_write(fd,buf_ptr,size);
  }
  return size;
}



int sys_read(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  int iVar2;
  
  if (fd == 0) {
    for (iVar2 = 0; iVar2 < (int)size; iVar2 = iVar2 + 1) {
      iVar1 = getch();
      buf_ptr[iVar2]._dummy = (char)iVar1;
      if ((char)iVar1 < '\0') {
        return iVar2;
      }
    }
  }
  else {
    size = file_read(fd,buf_ptr,size);
  }
  return size;
}



int load_elf(vnode *v,vaddr_t *entrypoint)

{
  addrspace *as;
  int iVar1;
  Elf_Ehdr *pEVar2;
  int iVar3;
  undefined4 *puVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  int unaff_s7;
  Elf_Ehdr eh;
  Elf_Phdr ph;
  iovec iov;
  uio ku;
  
  as = proc_getas();
  uio_kinit(&iov,&ku,&eh,0x34,0,UIO_READ);
  vnode_check(v,"read");
  iVar1 = (*v->vn_ops->vop_read)(v,&ku);
  if (iVar1 == 0) {
    if (ku.uio_resid == 0) {
      iVar1 = 0xd;
      if ((((eh.e_ident._0_4_ == 0x7f454c46) &&
           (iVar1 = 0xd, (eh.e_ident._4_4_ & 0xffffff00) == 0x1020100)) &&
          (iVar1 = 0xd, eh.e_version == 1)) && (iVar1 = 0xd, eh._16_4_ == 0x20008)) {
        pEVar2 = &eh;
        puVar4 = (undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x2c);
        do {
          uVar7 = *(undefined4 *)(pEVar2->e_ident + 4);
          uVar6 = *(undefined4 *)(pEVar2->e_ident + 8);
          uVar5 = *(undefined4 *)(pEVar2->e_ident + 0xc);
          *puVar4 = *(undefined4 *)pEVar2->e_ident;
          puVar4[1] = uVar7;
          puVar4[2] = uVar6;
          puVar4[3] = uVar5;
          pEVar2 = (Elf_Ehdr *)&pEVar2->e_type;
          puVar4 = puVar4 + 4;
        } while (pEVar2 != (Elf_Ehdr *)&eh.e_shnum);
        *puVar4 = *(undefined4 *)pEVar2;
        iVar1 = 0;
        do {
          while( true ) {
            if ((int)(uint)eh.e_phnum <= iVar1) {
              *entrypoint = eh.e_entry;
              return 0;
            }
            uio_kinit(&iov,&ku,&ph,0x20,(ulonglong)(eh.e_phoff + (uint)eh.e_phentsize * iVar1),
                      UIO_READ);
            vnode_check(v,"read");
            iVar3 = (*v->vn_ops->vop_read)(v,&ku);
            if (iVar3 != 0) {
              return iVar3;
            }
            if (ku.uio_resid != 0) {
              kprintf("ELF: short read on phdr - file truncated?\n");
              return 0xd;
            }
            if (ph.p_type != 1) break;
            iVar3 = as_define_region(as,ph.p_vaddr,ph.p_memsz,ph.p_flags & 4,ph.p_flags & 2,
                                     ph.p_flags & 1);
            if (iVar3 != 0) {
              return iVar3;
            }
LAB_8000dc40:
            iVar1 = iVar1 + 1;
          }
          if ((ph.p_type == 0) || (ph.p_type == 6)) goto LAB_8000dc40;
          iVar1 = iVar1 + 1;
        } while (ph.p_type == 0x70000000);
        kprintf("loadelf: unknown segment type %d\n");
        iVar1 = 0xd;
      }
    }
    else {
      kprintf("ELF: short read on header - file truncated?\n");
      iVar1 = 0xd;
    }
  }
  return iVar1;
}



void call_enter_forked_process(void *tfv,ulong dummy)

{
  enter_forked_process((trapframe *)tfv);
                    /* WARNING: Subroutine does not return */
  panic("enter_forked_process returned (should not happen)\n");
}



void sys__exit(int status)

{
  proc *ppVar1;
  pid_t pid;
  thread *unaff_s7;
  
  ppVar1 = unaff_s7->t_proc;
  pid = ppVar1->p_pid;
  free_ipt_process(pid);
  free_swap_table(pid);
  ppVar1->p_status = status & 0xff;
  proc_remthread(unaff_s7);
  lock_acquire(ppVar1->p_wlock);
  cv_signal(ppVar1->p_cv,ppVar1->p_wlock);
  lock_release(ppVar1->p_wlock);
                    /* WARNING: Subroutine does not return */
  ppVar1->finish = 1;
  thread_exit();
}



int sys_waitpid(pid_t pid,userptr_t statusp,int options)

{
  proc *proc;
  int iVar1;
  
  proc = proc_search_pid(pid);
  if (proc == (proc *)0x0) {
    badassert("p != NULL",s_______syscall_proc_syscalls_c_80024d30,0x50,"sys_waitpid");
  }
  if (proc == (proc *)0x0) {
    pid = -1;
  }
  else {
    iVar1 = proc_wait(proc);
    if (statusp != (userptr_t)0x0) {
      *(int *)statusp = iVar1;
    }
  }
  return pid;
}



pid_t sys_getpid(void)

{
  int iVar1;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    badassert("curproc != NULL",s_______syscall_proc_syscalls_c_80024d30,100,"sys_getpid");
  }
  return *(pid_t *)(iVar1 + 0x1c);
}



int sys_fork(trapframe *ctf,pid_t *retval)

{
  char **ppcVar1;
  proc *proc;
  int iVar2;
  void *dst;
  undefined4 *puVar3;
  undefined4 uVar4;
  char *pcVar5;
  undefined4 uVar6;
  Elf_Ehdr *pEVar7;
  undefined4 uVar8;
  char **unaff_s7;
  
  ppcVar1 = (char **)unaff_s7[0x15];
  if (ppcVar1 == (char **)0x0) {
    pcVar5 = s_______syscall_proc_syscalls_c_80024d30;
    badassert("curproc != NULL",s_______syscall_proc_syscalls_c_80024d30,0x7c,"sys_fork");
    retval = (pid_t *)pcVar5;
  }
  proc = proc_create_runprogram(*ppcVar1);
  if (proc == (proc *)0x0) {
    iVar2 = 3;
  }
  else {
    pcVar5 = unaff_s7[0x15];
    puVar3 = (undefined4 *)(pcVar5 + 0x2c);
    pEVar7 = &proc->p_eh;
    do {
      uVar8 = puVar3[1];
      uVar6 = puVar3[2];
      uVar4 = puVar3[3];
      *(undefined4 *)pEVar7->e_ident = *puVar3;
      *(undefined4 *)(pEVar7->e_ident + 4) = uVar8;
      *(undefined4 *)(pEVar7->e_ident + 8) = uVar6;
      *(undefined4 *)(pEVar7->e_ident + 0xc) = uVar4;
      puVar3 = puVar3 + 4;
      pEVar7 = (Elf_Ehdr *)&pEVar7->e_type;
    } while (puVar3 != (undefined4 *)(pcVar5 + 0x5c));
    *(undefined4 *)pEVar7 = *puVar3;
    as_copy(*(addrspace **)(unaff_s7[0x15] + 0x10),&proc->p_addrspace,
            *(pid_t *)(unaff_s7[0x15] + 0x1c),proc->p_pid);
    if (proc->p_addrspace == (addrspace *)0x0) {
      proc_destroy(proc);
      iVar2 = 3;
    }
    else {
      dst = kmalloc(0x8c);
      if (dst == (void *)0x0) {
        proc_destroy(proc);
        iVar2 = 3;
      }
      else {
        memcpy(dst,ctf,0x8c);
        iVar2 = thread_fork(*unaff_s7,proc,call_enter_forked_process,dst,0);
        if (iVar2 == 0) {
          *retval = proc->p_pid;
          iVar2 = 0;
        }
        else {
          proc_destroy(proc);
          kfree(dst);
          iVar2 = 3;
        }
      }
    }
  }
  return iVar2;
}



int runprogram(char *progname)

{
  vaddr_t vVar1;
  addrspace *paVar2;
  vnode *v;
  vaddr_t entrypoint;
  vaddr_t stackptr;
  
  vVar1 = vfs_open(progname,0,0,&v);
  if (vVar1 == 0) {
    paVar2 = proc_getas();
    if (paVar2 != (addrspace *)0x0) {
      badassert("proc_getas() == NULL","../../syscall/runprogram.c",0x45,"runprogram");
    }
    paVar2 = as_create();
    if (paVar2 == (addrspace *)0x0) {
      vfs_close(v);
      vVar1 = 3;
    }
    else {
      proc_setas(paVar2);
      as_activate();
      vVar1 = load_elf(v,&entrypoint);
      if (vVar1 == 0) {
        vfs_close(v);
        vVar1 = as_define_stack(paVar2,&stackptr);
        if (vVar1 == 0) {
          enter_new_process(0,(userptr_t)0x0,(userptr_t)0x0,stackptr,entrypoint);
          vVar1 = entrypoint;
        }
      }
      else {
        vfs_close(v);
      }
    }
  }
  return vVar1;
}



int sys___time(userptr_t user_seconds_ptr,userptr_t user_nanoseconds_ptr)

{
  int iVar1;
  timespec ts;
  
  gettime(&ts);
  iVar1 = copyout(&ts,user_seconds_ptr,8);
  if (iVar1 == 0) {
    iVar1 = copyout(&ts.tv_nsec,user_nanoseconds_ptr,4);
  }
  return iVar1;
}



void testa(array *a)

{
  int iVar1;
  uint uVar2;
  char *pcVar3;
  int **ppiVar4;
  int *piVar5;
  int iVar6;
  char *pcVar7;
  uint uVar8;
  void **unaff_s2;
  void *pvVar9;
  int testarray [73];
  
  for (iVar1 = 0; piVar5 = (int *)(iVar1 * 4), iVar1 < 0x49; iVar1 = iVar1 + 1) {
    testarray[iVar1] = iVar1;
  }
  uVar2 = a->num;
  uVar8 = 0;
  if (uVar2 == 0) goto LAB_8000e1cc;
  badassert("n==0","../../test/arraytest.c",0x33,"testa");
  do {
    piVar5 = testarray;
    pvVar9 = (void *)((int)piVar5 + uVar2);
    uVar2 = a->num;
    iVar1 = array_setsize(a,uVar2 + 1);
    unaff_s2 = (void **)(uVar2 * 4);
    if (iVar1 == 0) {
      unaff_s2 = a->v + uVar2;
      *unaff_s2 = pvVar9;
      iVar1 = 0;
    }
    if (iVar1 != 0) {
      badassert("r==0","../../test/arraytest.c",0x37,"testa");
    }
    uVar8 = uVar8 + 1;
    if (uVar8 != a->num) {
      badassert("n==i+1","../../test/arraytest.c",0x39,"testa");
    }
LAB_8000e1cc:
    uVar2 = uVar8 << 2;
  } while ((int)uVar8 < 0x49);
  pcVar7 = (char *)a->num;
  pcVar3 = (char *)0x0;
  if (pcVar7 == (char *)0x49) goto LAB_8000e270;
  pcVar7 = "../../test/arraytest.c";
  badassert("n==TESTSIZE","../../test/arraytest.c",0x3c,"testa");
  do {
    iVar1 = (int)pcVar3 << 2;
    if (piVar5 == (int *)0x0) {
      pcVar7 = "../../include/array.h";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (**(char ***)((int)a->v + iVar1) != pcVar3) {
      pcVar7 = "../../test/arraytest.c";
      badassert("*p == i","../../test/arraytest.c",0x40,"testa");
    }
    pcVar3 = pcVar3 + 1;
LAB_8000e270:
    piVar5 = (int *)(uint)(pcVar3 < pcVar7);
  } while ((int)pcVar3 < 0x49);
  iVar1 = 0;
  if (pcVar7 != (char *)0x49) {
    badassert("n==TESTSIZE","../../test/arraytest.c",0x43,"testa");
    goto LAB_8000e2a8;
  }
  unaff_s2 = (void **)0x49;
  for (; iVar1 < 0x124; iVar1 = iVar1 + 1) {
LAB_8000e2a8:
    uVar2 = random();
    pcVar3 = (char *)(uVar2 % (uint)unaff_s2);
    if (unaff_s2 == (void **)0x0) {
      trap(0x1c00);
    }
    pcVar7 = pcVar3;
    if ((char *)a->num <= pcVar3) {
      pcVar7 = "index < a->num";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
                    /* WARNING: Load size is inaccurate */
    if (*a->v[(int)pcVar3] != pcVar7) {
      badassert("*p == i","../../test/arraytest.c",0x48,"testa");
    }
  }
  uVar2 = 0;
  if (a->num != 0x49) {
    pcVar3 = "n==TESTSIZE";
    iVar1 = 0x4b;
    badassert("n==TESTSIZE","../../test/arraytest.c",0x4b,"testa");
    goto LAB_8000e37c;
  }
  iVar1 = 0x48;
  for (; pcVar3 = (char *)(iVar1 - uVar2), (int)uVar2 < 0x49; uVar2 = uVar2 + 1) {
LAB_8000e37c:
    pcVar3 = (char *)(testarray + (int)pcVar3);
    iVar6 = uVar2 << 2;
    if (a->num <= uVar2) {
      pcVar3 = "index < a->num";
      iVar1 = 0x6b;
      badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    }
    *(char **)((int)a->v + iVar6) = pcVar3;
  }
  uVar2 = 0;
  pcVar3 = (char *)0x48;
  while ((int)uVar2 < 0x49) {
    iVar1 = uVar2 << 2;
    if (a->num <= uVar2) {
      pcVar3 = "../../include/array.h";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    iVar6 = (int)pcVar3 - uVar2;
    uVar2 = uVar2 + 1;
    if (**(int **)((int)a->v + iVar1) != iVar6) {
      pcVar3 = "../../test/arraytest.c";
      badassert("*p == TESTSIZE-i-1","../../test/arraytest.c",0x53,"testa");
    }
  }
  iVar1 = array_setsize(a,0x24);
  uVar2 = 0;
  if (iVar1 == 0) {
    pcVar3 = (char *)0x48;
    for (; (int)uVar2 < 0x24; uVar2 = uVar2 + 1) {
LAB_8000e4a4:
      iVar1 = uVar2 << 2;
      if (a->num <= uVar2) {
        pcVar3 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      if (**(int **)((int)a->v + iVar1) != (int)pcVar3 - uVar2) {
        pcVar3 = "../../test/arraytest.c";
        badassert("*p == TESTSIZE-i-1","../../test/arraytest.c",0x5b,"testa");
      }
    }
    array_remove(a,1);
    uVar2 = 1;
    pcVar3 = (char *)0x47;
    while ((int)uVar2 < 0x23) {
      iVar1 = uVar2 << 2;
      if (a->num <= uVar2) {
        pcVar3 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      iVar6 = (int)pcVar3 - uVar2;
      uVar2 = uVar2 + 1;
      if (**(int **)((int)a->v + iVar1) != iVar6) {
        pcVar3 = "../../test/arraytest.c";
        badassert("*p == TESTSIZE-i-2","../../test/arraytest.c",0x62,"testa");
      }
    }
    if (a->num == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
                    /* WARNING: Load size is inaccurate */
    pcVar3 = (char *)a;
    if (**a->v != 0x48) {
      pcVar3 = "*p == TESTSIZE-1";
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x65,"testa");
    }
    array_setsize((array *)pcVar3,2);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    pcVar3 = (char *)(uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      pcVar3 = "*p == TESTSIZE-1";
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x69,"testa");
    }
    if (pcVar3 != (char *)0x0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    uVar2 = (uint)(uVar2 < 2);
    if (*ppiVar4[1] != 0x46) {
      badassert("*p == TESTSIZE-3","../../test/arraytest.c",0x6b,"testa");
    }
    pcVar3 = (char *)a;
    if (uVar2 != 0) {
      pcVar3 = "index < a->num";
      badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    }
    ppiVar4[1] = (int *)0x0;
    array_setsize((array *)pcVar3,2);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    uVar2 = (uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x70,"testa");
    }
    if (uVar2 != 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar3 = (char *)a;
    if (ppiVar4[1] != (int *)0x0) {
      pcVar3 = "p==NULL";
      badassert("p==NULL","../../test/arraytest.c",0x72,"testa");
    }
    array_setsize((array *)pcVar3,0x2da);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    uVar2 = (uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x76,"testa");
    }
    if (uVar2 != 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (ppiVar4[1] != (int *)0x0) {
      badassert("p==NULL","../../test/arraytest.c",0x78,"testa");
    }
    return;
  }
  pcVar3 = "../../test/arraytest.c";
  badassert("r==0","../../test/arraytest.c",0x57,"testa");
  goto LAB_8000e4a4;
}



int arraytest(int nargs,char **args)

{
  array *a;
  array *a_00;
  
  kprintf("Beginning array test...\n");
  a = array_create();
  a_00 = a;
  if (a == (array *)0x0) {
    badassert("a != NULL","../../test/arraytest.c",0x85,"arraytest");
  }
  testa(a_00);
  array_setsize(a,0);
  testa(a);
  array_setsize(a,0);
  array_destroy(a);
  kprintf("Array test complete\n");
  return 0;
}



int arraytest2(int nargs,char **args)

{
  array *a;
  int iVar1;
  void *pvVar2;
  char *pcVar3;
  void **in_v1;
  int iVar4;
  void *pvVar5;
  char *pcVar6;
  char *pcVar7;
  void **ppvVar8;
  uint unaff_s1;
  uint uVar9;
  uint unaff_s3;
  void *unaff_s4;
  
  kprintf("Beginning large array test...\n");
  a = array_create();
  if (a != (array *)0x0) {
    unaff_s3 = 0;
    unaff_s1 = 0;
    unaff_s4 = (void *)0xc0ffee;
    goto LAB_8000ea54;
  }
  pcVar6 = "a != NULL";
  badassert("a != NULL","../../test/arraytest.c",0xa4,"arraytest2");
  do {
    uVar9 = a->num;
    iVar1 = array_setsize((array *)pcVar6,uVar9 + 1);
    if (iVar1 == 0) {
      in_v1 = a->v;
      in_v1[uVar9] = unaff_s4;
      iVar1 = 0;
      unaff_s3 = uVar9;
    }
    if (iVar1 != 0) {
      badassert("result == 0","../../test/arraytest.c",0xaa,"arraytest2");
    }
    if (unaff_s1 != unaff_s3) {
      badassert("x == i","../../test/arraytest.c",0xab,"arraytest2");
    }
    unaff_s1 = unaff_s1 + 1;
LAB_8000ea54:
    pcVar6 = (char *)a;
  } while (unaff_s1 < 3000);
  pcVar7 = (char *)a->num;
  pcVar6 = (char *)0x0;
  if (pcVar7 != (char *)0xbb8) {
    pcVar7 = "../../test/arraytest.c";
    iVar1 = 0xad;
    badassert("array_num(a) == BIGTESTSIZE","../../test/arraytest.c",0xad,"arraytest2");
    do {
      iVar4 = (int)pcVar6 << 2;
      if (in_v1 == (void **)0x0) {
        pcVar7 = "../../include/array.h";
        iVar1 = 100;
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      if (*(int *)((int)a->v + iVar4) != iVar1) {
        pcVar7 = "../../test/arraytest.c";
        iVar1 = 0xb1;
        badassert("array_get(a, i) == p","../../test/arraytest.c",0xb1,"arraytest2");
      }
      pcVar6 = pcVar6 + 1;
LAB_8000eb00:
      in_v1 = (void **)(uint)(pcVar6 < pcVar7);
    } while (pcVar6 < (char *)0xbb8);
    iVar1 = array_setsize(a,0);
    pcVar6 = (char *)a;
    if (iVar1 != 0) {
      pcVar6 = "result == 0";
      badassert("result == 0","../../test/arraytest.c",0xb6,"arraytest2");
    }
    iVar1 = array_setsize((array *)pcVar6,3000);
    uVar9 = 0;
    if (iVar1 != 0) {
      pcVar6 = "result == 0";
      iVar1 = 0xba;
      badassert("result == 0","../../test/arraytest.c",0xba,"arraytest2");
      do {
        pcVar6 = pcVar6 + iVar1 + uVar9;
        if (a->num <= uVar9) {
          pcVar6 = "index < a->num";
          iVar1 = 0x6b;
          badassert("index < a->num","../../include/array.h",0x6b,"array_set");
        }
        a->v[uVar9] = pcVar6;
        uVar9 = uVar9 + 1;
LAB_8000ebc4:
        pcVar6 = (char *)(uVar9 << 1);
      } while (uVar9 < 3000);
      uVar9 = 0;
      pcVar6 = (char *)0xb007;
      while (uVar9 < 3000) {
        iVar1 = uVar9 << 2;
        if (a->num <= uVar9) {
          pcVar6 = "../../include/array.h";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        iVar4 = uVar9 * 3;
        uVar9 = uVar9 + 1;
        if (pcVar6 + iVar4 != *(char **)((int)a->v + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          badassert("array_get(a, i) == NTH(i)","../../test/arraytest.c",0xc1,"arraytest2");
        }
      }
      array_remove(a,1);
      pcVar6 = (char *)a->num;
      pvVar2 = (void *)0xb007;
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppvVar8 = a->v;
      pvVar5 = *ppvVar8;
      pcVar7 = (char *)0xbb7;
      if (pvVar5 != pvVar2) {
        pcVar6 = "../../test/arraytest.c";
        ppvVar8 = (void **)0xc6;
        badassert("array_get(a, 0) == NTH(0)","../../test/arraytest.c",0xc6,"arraytest2");
      }
      pcVar3 = (char *)0x1;
      if (pcVar6 == pcVar7) goto LAB_8000ed60;
      pcVar6 = "../../test/arraytest.c";
      ppvVar8 = (void **)0xc7;
      pcVar7 = "arraytest2";
      badassert("array_num(a) == BIGTESTSIZE-1","../../test/arraytest.c",199,"arraytest2");
      do {
        iVar1 = (int)pcVar3 << 2;
        if (pvVar5 == (void *)0x0) {
          pcVar6 = "../../include/array.h";
          ppvVar8 = (void **)&DAT_00000064;
          pcVar7 = "array_get";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        pcVar3 = pcVar3 + 1;
        if (pcVar7 + (int)pcVar3 * 3 != *(char **)((int)ppvVar8 + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          ppvVar8 = (void **)0xc9;
          badassert("array_get(a, i) == NTH(i+1)","../../test/arraytest.c",0xc9,"arraytest2");
LAB_8000ed60:
          pcVar7 = (char *)0xb007;
        }
        pvVar5 = (void *)(uint)(pcVar3 < pcVar6);
      } while (pcVar3 < (char *)0xbb7);
      iVar1 = array_setsize(a,6000);
      if (iVar1 != 0) {
        badassert("result == 0","../../test/arraytest.c",0xce,"arraytest2");
      }
      pcVar6 = (char *)a->num;
      pvVar2 = (void *)0xb007;
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppvVar8 = a->v;
      pvVar5 = *ppvVar8;
      pcVar7 = (char *)0x1;
      if (pvVar5 == pvVar2) goto LAB_8000ee70;
      pcVar6 = "../../test/arraytest.c";
      ppvVar8 = (void **)0xcf;
      pcVar3 = "arraytest2";
      badassert("array_get(a, 0) == NTH(0)","../../test/arraytest.c",0xcf,"arraytest2");
      do {
        iVar1 = (int)pcVar7 << 2;
        if (pvVar5 == (void *)0x0) {
          pcVar6 = "../../include/array.h";
          ppvVar8 = (void **)&DAT_00000064;
          pcVar3 = "array_get";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        pcVar7 = pcVar7 + 1;
        if (pcVar3 + (int)pcVar7 * 3 != *(char **)((int)ppvVar8 + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          ppvVar8 = (void **)0xd1;
          badassert("array_get(a, i) == NTH(i+1)","../../test/arraytest.c",0xd1,"arraytest2");
LAB_8000ee70:
          pcVar3 = (char *)0xb007;
        }
        pvVar5 = (void *)(uint)(pcVar7 < pcVar6);
      } while (pcVar7 < (char *)0xbb7);
      iVar1 = array_setsize(a,0);
      if (iVar1 != 0) {
        badassert("result == 0","../../test/arraytest.c",0xd6,"arraytest2");
      }
      array_destroy(a);
      kprintf("Done.\n");
      return 0;
    }
    iVar1 = 0xb007;
    goto LAB_8000ebc4;
  }
  iVar1 = 0xc0ffee;
  goto LAB_8000eb00;
}



int bitmaptest(int nargs,char **args)

{
  long lVar1;
  bitmap *b;
  uint32_t uVar2;
  char *pcVar3;
  char *pcVar4;
  int iVar5;
  uint unaff_s1;
  uint uVar6;
  uint uVar7;
  char data [533];
  uint32_t x;
  
  kprintf("Starting bitmap test...\n");
  for (iVar5 = 0; iVar5 < 0x215; iVar5 = iVar5 + 1) {
    lVar1 = random();
    data[iVar5] = (byte)lVar1 & 1;
  }
  b = bitmap_create(0x215);
  if (b != (bitmap *)0x0) {
    unaff_s1 = 0;
    goto LAB_8000efa4;
  }
  pcVar4 = "b != NULL";
  badassert("b != NULL","../../test/bitmaptest.c",0x37,"bitmaptest");
  do {
    iVar5 = bitmap_isset((bitmap *)pcVar4,unaff_s1);
    if (iVar5 != 0) {
      badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x3a,"bitmaptest");
    }
    unaff_s1 = unaff_s1 + 1;
LAB_8000efa4:
    pcVar4 = (char *)b;
  } while ((int)unaff_s1 < 0x215);
  for (uVar6 = 0; (int)uVar6 < 0x215; uVar6 = uVar6 + 1) {
    if (data[uVar6] != '\0') {
      bitmap_mark(b,uVar6);
    }
  }
  uVar6 = 0;
LAB_8000f068:
  uVar7 = uVar6;
  if ((int)uVar7 < 0x215) {
    pcVar4 = (char *)b;
    if (data[uVar7] != '\0') goto code_r0x8000f000;
    goto LAB_8000f034;
  }
  uVar6 = 0;
  while ((int)uVar6 < 0x215) {
    if (data[uVar6] == '\0') {
      bitmap_mark(b,uVar6);
      uVar6 = uVar6 + 1;
    }
    else {
      bitmap_unmark(b,uVar6);
      uVar6 = uVar6 + 1;
    }
  }
  uVar6 = 0;
LAB_8000f13c:
  uVar7 = uVar6;
  if (0x214 < (int)uVar7) {
    while (iVar5 = bitmap_alloc(b,&x), iVar5 == 0) {
      pcVar4 = (char *)x;
      if (0x214 < x) {
        pcVar4 = "../../test/bitmaptest.c";
        badassert("x < TESTSIZE","../../test/bitmaptest.c",0x5d,"bitmaptest");
      }
      iVar5 = bitmap_isset(b,(uint)pcVar4);
      pcVar4 = data;
      if (iVar5 == 0) {
        pcVar4 = "bitmap_isset(b, x)";
        badassert("bitmap_isset(b, x)","../../test/bitmaptest.c",0x5e,"bitmaptest");
      }
      pcVar3 = data;
      uVar2 = x;
      if (pcVar4[x] != '\x01') {
        badassert("data[x]==1","../../test/bitmaptest.c",0x5f,"bitmaptest");
      }
      pcVar3[uVar2] = '\0';
    }
    uVar6 = 0;
    while (uVar7 = uVar6, (int)uVar7 < 0x215) {
      iVar5 = bitmap_isset(b,uVar7);
      pcVar4 = data;
      if (iVar5 == 0) {
        pcVar4 = "bitmap_isset(b, i)";
        badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",100,"bitmaptest");
      }
      uVar6 = uVar7 + 1;
      if (pcVar4[uVar7] != '\0') {
        badassert("data[i]==0","../../test/bitmaptest.c",0x65,"bitmaptest");
        uVar6 = uVar7;
      }
    }
    kprintf("Bitmap test complete\n");
    return 0;
  }
  pcVar4 = (char *)b;
  if (data[uVar7] != '\0') goto code_r0x8000f0d4;
  goto LAB_8000f108;
code_r0x8000f000:
  iVar5 = bitmap_isset(b,uVar7);
  uVar6 = uVar7 + 1;
  if (iVar5 == 0) {
    pcVar4 = "bitmap_isset(b, i)";
    badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",0x44,"bitmaptest");
LAB_8000f034:
    iVar5 = bitmap_isset((bitmap *)pcVar4,uVar7);
    uVar6 = uVar7 + 1;
    if (iVar5 != 0) {
      badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x47,"bitmaptest");
      uVar6 = uVar7;
    }
  }
  goto LAB_8000f068;
code_r0x8000f0d4:
  iVar5 = bitmap_isset(b,uVar7);
  uVar6 = uVar7 + 1;
  if (iVar5 != 0) {
    pcVar4 = "bitmap_isset(b, i)==0";
    badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x55,"bitmaptest");
LAB_8000f108:
    iVar5 = bitmap_isset((bitmap *)pcVar4,uVar7);
    uVar6 = uVar7 + 1;
    if (iVar5 == 0) {
      badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",0x58,"bitmaptest");
      uVar6 = uVar7;
    }
  }
  goto LAB_8000f13c;
}



int checkfilesystem(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  char *str;
  
  if (nargs == 2) {
    str = args[1];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
      iVar1 = 0;
    }
    else {
      iVar1 = 0;
    }
  }
  else {
    kprintf("Usage: fs[123456] filesystem:\n");
    iVar1 = 8;
  }
  return iVar1;
}



/* WARNING: Removing unreachable block (ram,0x8000f338) */

void rotate(char *str,int amt)

{
  int iVar1;
  char *in_v1;
  char *pcVar2;
  char cVar3;
  char *pcVar4;
  int in_t0;
  int iVar5;
  uint uVar6;
  
  iVar1 = 0x1a;
  iVar5 = (amt + 0xa28) % 0x1a;
  if (-1 < iVar5) {
    pcVar2 = (char *)0x0;
    in_t0 = 0x1a;
    while( true ) {
      in_v1 = str + (int)pcVar2;
      iVar1 = (int)*in_v1;
      pcVar4 = (char *)(iVar1 + -0x41);
      if (iVar1 == 0) break;
LAB_8000f368:
      cVar3 = (char)iVar1;
      if (pcVar4 < (char *)0x1a) {
        uVar6 = (iVar1 + -0x41 + iVar5) % in_t0;
        if (in_t0 == 0) {
          trap(0x1c00);
        }
        cVar3 = (char)uVar6 + 'A';
        if (0x19 < uVar6) {
          str = "ch>=\'A\' && ch<=\'Z\'";
          pcVar2 = "../../test/fstest.c";
          cVar3 = '^';
          badassert("ch>=\'A\' && ch<=\'Z\'","../../test/fstest.c",0x5e,"rotate");
        }
      }
      *in_v1 = cVar3;
      pcVar2 = pcVar2 + 1;
    }
    return;
  }
  str = "amt>=0";
  pcVar2 = "../../test/fstest.c";
  pcVar4 = "rotate";
  badassert("amt>=0","../../test/fstest.c",0x55,"rotate");
  goto LAB_8000f368;
}



void fstest_makename(char *buf,size_t buflen,char *fs,char *namesuffix)

{
  size_t sVar1;
  
  snprintf(buf,buflen,"%s:%s%s",fs,"fstest.tmp",namesuffix);
  sVar1 = strlen(buf);
  if (buflen <= sVar1) {
    badassert("strlen(buf) < buflen","../../test/fstest.c",0x6c,"fstest_makename");
  }
  return;
}



int fstest_write(char *fs,char *namesuffix,int stridesize,int stridepos)

{
  bool bVar1;
  size_t sVar2;
  int iVar3;
  char *pcVar4;
  int iVar5;
  uint uVar6;
  int iVar7;
  int iVar8;
  vnode *vn;
  char name [32];
  char buf [32];
  iovec iov;
  uio ku;
  
  sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
  pcVar4 = name;
  if (0x1f < sVar2) {
    pcVar4 = "sizeof(buf) > strlen(SLOGAN)";
    badassert("sizeof(buf) > strlen(SLOGAN)","../../test/fstest.c",0x96,"fstest_write");
  }
  fstest_makename(pcVar4,0x20,fs,namesuffix);
  iVar5 = 5;
  if (stridesize == 1) {
    iVar5 = 0x15;
  }
  strcpy(buf,name);
  iVar3 = vfs_open(buf,iVar5,0x1b4,&vn);
  iVar5 = 0;
  if (iVar3 == 0) {
    uVar6 = 0;
    iVar7 = 0;
    iVar8 = 0;
    for (iVar3 = 0; iVar3 < 0x2d0; iVar3 = iVar3 + 1) {
      if (stridesize == 0) {
        trap(0x1c00);
      }
      if (iVar3 % stridesize == stridepos) {
        strcpy(buf,"HODIE MIHI - CRAS TIBI\n");
        rotate(buf,iVar3);
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        uio_kinit(&iov,&ku,buf,sVar2,CONCAT44(iVar5,uVar6),UIO_WRITE);
        vnode_check(vn,s_console_write_80022690 + 8);
        iVar5 = (*vn->vn_ops->vop_write)(vn,&ku);
        if (iVar5 != 0) {
          pcVar4 = strerror(iVar5);
          kprintf("%s: Write error: %s\n",name,pcVar4);
          vfs_close(vn);
          vfs_remove(name);
          return -1;
        }
        if (ku.uio_resid != 0) {
          kprintf("%s: Short write: %lu bytes left over\n",name);
          vfs_close(vn);
          vfs_remove(name);
          return -1;
        }
        iVar7 = iVar7 + (ku.uio_offset._4_4_ - uVar6);
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        iVar8 = iVar8 + sVar2;
        uVar6 = ku.uio_offset._4_4_;
        iVar5 = ku.uio_offset._0_4_;
      }
      else {
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        bVar1 = uVar6 + sVar2 < uVar6;
        uVar6 = uVar6 + sVar2;
        iVar5 = (uint)bVar1 + iVar5;
      }
    }
    vfs_close(vn);
    if (iVar7 == iVar8) {
      kprintf("%s: %lu bytes written\n",name,iVar7);
      iVar5 = 0;
    }
    else {
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      kprintf("%s: %lu bytes written, should have been %lu!\n",name,iVar7,sVar2 * 0x2d0);
      vfs_remove(name);
      iVar5 = -1;
    }
  }
  else {
    pcVar4 = strerror(iVar3);
    kprintf("Could not open %s for write: %s\n",name,pcVar4);
    iVar5 = -1;
  }
  return iVar5;
}



int fstest_remove(char *fs,char *namesuffix)

{
  int iVar1;
  char *pcVar2;
  char name [32];
  char buf [32];
  
  fstest_makename(name,0x20,fs,namesuffix);
  strcpy(buf,name);
  iVar1 = vfs_remove(buf);
  if (iVar1 == 0) {
    iVar1 = 0;
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not remove %s: %s\n",name,pcVar2);
    iVar1 = -1;
  }
  return iVar1;
}



int fstest_read(char *fs,char *namesuffix)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  size_t sVar4;
  uint uVar5;
  vnode *vn;
  char name [32];
  char buf [32];
  iovec iov;
  uio ku;
  
  fstest_makename(name,0x20,fs,namesuffix);
  strcpy(buf,name);
  iVar1 = vfs_open(buf,0,0x1b4,&vn);
  if (iVar1 == 0) {
    uVar5 = 0;
    for (iVar1 = 0; iVar1 < 0x2d0; iVar1 = iVar1 + 1) {
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar4,(ulonglong)uVar5,UIO_READ);
      vnode_check(vn,"read");
      iVar3 = (*vn->vn_ops->vop_read)(vn,&ku);
      if (iVar3 != 0) {
        pcVar2 = strerror(iVar3);
        kprintf("%s: Read error: %s\n",name,pcVar2);
        vfs_close(vn);
        return -1;
      }
      if (ku.uio_resid != 0) {
        kprintf("%s: Short read: %lu bytes left over\n",name);
        vfs_close(vn);
        return -1;
      }
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      buf[sVar4] = '\0';
      rotate(buf,-iVar1);
      iVar3 = strcmp(buf,"HODIE MIHI - CRAS TIBI\n");
      if (iVar3 != 0) {
        kprintf("%s: Test failed: line %d mismatched: %s\n",name,iVar1 + 1,buf);
        vfs_close(vn);
        return -1;
      }
      uVar5 = ku.uio_offset._4_4_;
    }
    vfs_close(vn);
    sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
    if (sVar4 * 0x2d0 == uVar5) {
      kprintf("%s: %lu bytes read\n",name,uVar5);
      iVar1 = 0;
    }
    else {
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      kprintf("%s: %lu bytes read, should have been %lu!\n",name,uVar5,sVar4 * 0x2d0);
      iVar1 = -1;
    }
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not open test file for read: %s\n",pcVar2);
    iVar1 = -1;
  }
  return iVar1;
}



void dofstest(char *filesys)

{
  int iVar1;
  
  kprintf("*** Starting filesystem test on %s:\n",filesys);
  iVar1 = fstest_write(filesys,"",1,0);
  if (iVar1 == 0) {
    iVar1 = fstest_read(filesys,"");
    if (iVar1 == 0) {
      iVar1 = fstest_remove(filesys,"");
      if (iVar1 == 0) {
        kprintf("*** Filesystem test done\n");
      }
      else {
        kprintf("*** Test failed\n");
      }
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    kprintf("*** Test failed\n");
  }
  return;
}



void readstress_thread(void *fs,ulong num)

{
  int iVar1;
  
  iVar1 = fstest_read((char *)fs,"");
  if (iVar1 != 0) {
    kprintf("*** Thread %lu: failed\n",num);
  }
  V(threadsem);
  return;
}



void writestress_thread(void *fs,ulong num)

{
  int iVar1;
  char numstr [8];
  
  snprintf(numstr,8,"%lu",num);
  iVar1 = fstest_write((char *)fs,numstr,1,0);
  if (iVar1 == 0) {
    iVar1 = fstest_read((char *)fs,numstr);
    if (iVar1 == 0) {
      iVar1 = fstest_remove((char *)fs,numstr);
      if (iVar1 != 0) {
        kprintf("*** Thread %lu: failed\n",num);
      }
      kprintf("*** Thread %lu: done\n",num);
      V(threadsem);
    }
    else {
      kprintf("*** Thread %lu: failed\n",num);
      V(threadsem);
    }
  }
  else {
    kprintf("*** Thread %lu: failed\n",num);
    V(threadsem);
  }
  return;
}



void writestress2_thread(void *fs,ulong num)

{
  int iVar1;
  
  iVar1 = fstest_write((char *)fs,"",0xc,num);
  if (iVar1 == 0) {
    V(threadsem);
  }
  else {
    kprintf("*** Thread %lu: failed\n",num);
    V(threadsem);
  }
  return;
}



void longstress_thread(void *fs,ulong num)

{
  int iVar1;
  int iVar2;
  int iVar3;
  char numstr [16];
  
  iVar1 = 0;
  do {
    iVar3 = iVar1;
    if (0x1f < iVar3) {
      V(threadsem);
      return;
    }
    snprintf(numstr,0x10,"%lu-%d",num,iVar3);
    iVar1 = fstest_write((char *)fs,numstr,1,0);
    if (iVar1 != 0) {
      kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
      V(threadsem);
      return;
    }
    iVar1 = fstest_read((char *)fs,numstr);
    if (iVar1 != 0) {
      kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
      V(threadsem);
      return;
    }
    iVar2 = fstest_remove((char *)fs,numstr);
    iVar1 = iVar3 + 1;
  } while (iVar2 == 0);
  kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
  V(threadsem);
  return;
}



void createstress_thread(void *fs,ulong num)

{
  char *pcVar1;
  size_t sVar2;
  size_t sVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  char namesuffix [16];
  char name [32];
  char buf [32];
  vnode *vn;
  iovec iov;
  uio ku;
  
  iVar6 = 0;
  iVar5 = 0;
  while (iVar5 < 0x18) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    fstest_makename(name,0x20,(char *)fs,namesuffix);
    strcpy(buf,name);
    iVar4 = vfs_open(buf,0x15,0x1b4,&vn);
    if (iVar4 == 0) {
      strcpy(buf,"HODIE MIHI - CRAS TIBI\n");
      rotate(buf,iVar5);
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar2,0,UIO_WRITE);
      vnode_check(vn,s_console_write_80022690 + 8);
      iVar4 = (*vn->vn_ops->vop_write)(vn,&ku);
      vfs_close(vn);
      sVar2 = ku.uio_offset._4_4_;
      if (iVar4 == 0) {
        if (ku.uio_resid == 0) {
          sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
          if (sVar3 == sVar2) {
            iVar6 = iVar6 + 1;
            iVar5 = iVar5 + 1;
          }
          else {
            sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
            kprintf("%s: %lu bytes written, expected %lu!\n",name,sVar2,sVar3);
            iVar5 = iVar5 + 1;
          }
        }
        else {
          kprintf("%s: Short write: %lu bytes left over\n",name);
          iVar5 = iVar5 + 1;
        }
      }
      else {
        pcVar1 = strerror(iVar4);
        kprintf("%s: Write error: %s\n",name,pcVar1);
        iVar5 = iVar5 + 1;
      }
    }
    else {
      pcVar1 = strerror(iVar4);
      kprintf("Could not open %s for write: %s\n",name,pcVar1);
      iVar5 = iVar5 + 1;
    }
  }
  kprintf("Thread %lu: %u files written\n",num,iVar6);
  iVar6 = 0;
  iVar5 = 0;
  while (iVar5 < 0x18) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    fstest_makename(name,0x20,(char *)fs,namesuffix);
    strcpy(buf,name);
    iVar4 = vfs_open(buf,0,0x1b4,&vn);
    if (iVar4 == 0) {
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar2,0,UIO_READ);
      vnode_check(vn,"read");
      iVar4 = (*vn->vn_ops->vop_read)(vn,&ku);
      vfs_close(vn);
      if (iVar4 == 0) {
        if (ku.uio_resid == 0) {
          sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
          buf[sVar2] = '\0';
          rotate(buf,-iVar5);
          iVar4 = strcmp(buf,"HODIE MIHI - CRAS TIBI\n");
          sVar2 = ku.uio_offset._4_4_;
          if (iVar4 == 0) {
            sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
            if (sVar3 == sVar2) {
              iVar6 = iVar6 + 1;
              iVar5 = iVar5 + 1;
            }
            else {
              sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
              kprintf("%s: %lu bytes read, expected %lu!\n",name,sVar2,sVar3);
              iVar5 = iVar5 + 1;
            }
          }
          else {
            kprintf("%s: Test failed: file mismatched: %s\n",name,buf);
            iVar5 = iVar5 + 1;
          }
        }
        else {
          kprintf("%s: Short read: %lu bytes left over\n",name);
          iVar5 = iVar5 + 1;
        }
      }
      else {
        pcVar1 = strerror(iVar4);
        kprintf("%s: Read error: %s\n",name,pcVar1);
        iVar5 = iVar5 + 1;
      }
    }
    else {
      pcVar1 = strerror(iVar4);
      kprintf("Could not open %s for read: %s\n",name,pcVar1);
      iVar5 = iVar5 + 1;
    }
  }
  kprintf("Thread %lu: %u files read\n",num,iVar6);
  iVar6 = 0;
  for (iVar5 = 0; iVar5 < 0x18; iVar5 = iVar5 + 1) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    iVar4 = fstest_remove((char *)fs,namesuffix);
    if (iVar4 == 0) {
      iVar6 = iVar6 + 1;
    }
  }
  kprintf("Thread %lu: %u files removed\n",num,iVar6);
  V(threadsem);
  return;
}



void init_threadsem(void)

{
  if ((threadsem == (semaphore *)0x0) &&
     (threadsem = sem_create("fstestsem",0), threadsem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("fstest: sem_create failed\n");
  }
  return;
}



void doreadstress(char *filesys)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  
  init_threadsem();
  kprintf("*** Starting fs read stress test on %s:\n",filesys);
  iVar1 = fstest_write(filesys,"",1,0);
  data2 = 0;
  if (iVar1 == 0) {
    for (; (int)data2 < 0xc; data2 = data2 + 1) {
      iVar1 = thread_fork("readstress",(proc *)0x0,readstress_thread,filesys,data2);
      if (iVar1 != 0) {
        pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
        panic("readstress: thread_fork failed: %s\n",pcVar2);
      }
    }
    for (iVar1 = 0; iVar1 < 0xc; iVar1 = iVar1 + 1) {
      P(threadsem);
    }
    iVar1 = fstest_remove(filesys,"");
    if (iVar1 == 0) {
      kprintf("*** fs read stress test done\n");
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    kprintf("*** Test failed\n");
  }
  return;
}



void dowritestress(char *filesys)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  init_threadsem();
  kprintf("*** Starting fs write stress test on %s:\n",filesys);
  data2 = 0;
  do {
    if (0xb < (int)data2) {
      for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
        P(threadsem);
      }
      kprintf("*** fs write stress test done\n");
      return;
    }
    iVar2 = thread_fork("writestress",(proc *)0x0,writestress_thread,filesys,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("thread_fork failed %s\n",pcVar1);
}



void dowritestress2(char *filesys)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  char name [32];
  vnode *vn;
  
  init_threadsem();
  kprintf("*** Starting fs write stress test 2 on %s:\n",filesys);
  fstest_makename(name,0x20,filesys,"");
  iVar1 = vfs_open(name,0x15,0x1b4,&vn);
  if (iVar1 == 0) {
    data2 = 0;
    vfs_close(vn);
    while ((int)data2 < 0xc) {
      iVar1 = thread_fork("writestress2",(proc *)0x0,writestress2_thread,filesys,data2);
      data2 = data2 + 1;
      if (iVar1 != 0) {
        pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
        panic("writestress2: thread_fork failed: %s\n",pcVar2);
      }
    }
    for (iVar1 = 0; iVar1 < 0xc; iVar1 = iVar1 + 1) {
      P(threadsem);
    }
    iVar1 = fstest_read(filesys,"");
    if (iVar1 == 0) {
      iVar1 = fstest_remove(filesys,"");
      if (iVar1 != 0) {
        kprintf("*** Test failed\n");
      }
      kprintf("*** fs write stress test 2 done\n");
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not create test file: %s\n",pcVar2);
    kprintf("*** Test failed\n");
  }
  return;
}



void dolongstress(char *filesys)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  init_threadsem();
  kprintf("*** Starting fs long stress test on %s:\n",filesys);
  data2 = 0;
  do {
    if (0xb < (int)data2) {
      for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
        P(threadsem);
      }
      kprintf("*** fs long stress test done\n");
      return;
    }
    iVar2 = thread_fork("longstress",(proc *)0x0,longstress_thread,filesys,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("longstress: thread_fork failed %s\n",pcVar1);
}



void docreatestress(char *filesys)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  init_threadsem();
  kprintf("*** Starting fs create stress test on %s:\n",filesys);
  data2 = 0;
  do {
    if (0xb < (int)data2) {
      for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
        P(threadsem);
      }
      kprintf("*** fs create stress test done\n");
      return;
    }
    iVar2 = thread_fork("createstress",(proc *)0x0,createstress_thread,filesys,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("createstress: thread_fork failed %s\n",pcVar1);
}



int fstest(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    dofstest(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int readstress(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    doreadstress(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int writestress(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    dowritestress(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int writestress2(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    dowritestress2(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int longstress(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    dolongstress(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int createstress(int nargs,char **args)

{
  int iVar1;
  
  iVar1 = checkfilesystem(nargs,args);
  if (iVar1 == 0) {
    docreatestress(args[1]);
    iVar1 = 0;
  }
  return iVar1;
}



int printfile(int nargs,char **args)

{
  bool bVar1;
  int iVar2;
  char *pcVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  vnode *rv;
  vnode *wv;
  iovec iov;
  uio ku;
  char buf [128];
  char outfile [16];
  
  if (nargs == 2) {
    strcpy(outfile,"con:");
    iVar2 = vfs_open(args[1],0,0x1b4,&rv);
    if (iVar2 == 0) {
      iVar2 = vfs_open(outfile,1,0x1b4,&wv);
      if (iVar2 == 0) {
        bVar1 = false;
        uVar7 = 0;
        uVar6 = 0;
        uVar5 = 0;
        uVar4 = 0;
        while (!bVar1) {
          uio_kinit(&iov,&ku,buf,0x80,CONCAT44(uVar4,uVar5),UIO_READ);
          vnode_check(rv,"read");
          iVar2 = (*rv->vn_ops->vop_read)(rv,&ku);
          uVar5 = ku.uio_offset._4_4_;
          uVar4 = ku.uio_offset._0_4_;
          if (iVar2 != 0) {
            pcVar3 = strerror(iVar2);
            kprintf("Read error: %s\n",pcVar3);
            break;
          }
          if (ku.uio_resid != 0) {
            bVar1 = true;
          }
          uio_kinit(&iov,&ku,buf,0x80 - ku.uio_resid,CONCAT44(uVar6,uVar7),UIO_WRITE);
          vnode_check(wv,s_console_write_80022690 + 8);
          iVar2 = (*wv->vn_ops->vop_write)(wv,&ku);
          uVar7 = ku.uio_offset._4_4_;
          uVar6 = ku.uio_offset._0_4_;
          if (iVar2 != 0) {
            pcVar3 = strerror(iVar2);
            kprintf("Write error: %s\n",pcVar3);
            break;
          }
          if (ku.uio_resid != 0) {
            kprintf("Warning: short write\n");
          }
        }
        vfs_close(wv);
        vfs_close(rv);
        iVar2 = 0;
      }
      else {
        pcVar3 = strerror(iVar2);
        kprintf("printfile: output: %s\n",pcVar3);
        vfs_close(rv);
      }
    }
    else {
      pcVar3 = strerror(iVar2);
      kprintf("printfile: %s\n",pcVar3);
    }
  }
  else {
    kprintf("Usage: pf filename\n");
    iVar2 = 8;
  }
  return iVar2;
}



void kmallocthread(void *sm,ulong num)

{
  void *pvVar1;
  int iVar2;
  void *ptr;
  void *ptr_00;
  
  iVar2 = 0;
  ptr = (void *)0x0;
  ptr_00 = (void *)0x0;
  do {
    if (0x4af < iVar2) {
done:
      if (ptr != (void *)0x0) {
        kfree(ptr);
      }
      if (ptr_00 != (void *)0x0) {
        kfree(ptr_00);
      }
      if (sm != (void *)0x0) {
        V((semaphore *)sm);
      }
      return;
    }
    pvVar1 = kmalloc(0x3e5);
    if (pvVar1 == (void *)0x0) {
      if (sm == (void *)0x0) {
        kprintf("kmalloc returned null; test failed.\n");
      }
      else {
        kprintf("thread %lu: kmalloc returned NULL\n",num);
      }
      goto done;
    }
    if (ptr != (void *)0x0) {
      kfree(ptr);
    }
    iVar2 = iVar2 + 1;
    ptr = ptr_00;
    ptr_00 = pvVar1;
  } while( true );
}



/* WARNING: Removing unreachable block (ram,0x80010dd8) */
/* WARNING: Removing unreachable block (ram,0x80010df0) */

void kmalloctest4thread(void *sm,ulong num)

{
  uint uVar1;
  void *pvVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  void *ptrs [5];
  
  for (uVar1 = 0; uVar1 < 5; uVar1 = uVar1 + 1) {
    ptrs[uVar1] = (void *)0x0;
  }
  uVar3 = 0;
  uVar1 = 2;
  uVar4 = 0;
  while( true ) {
    if (0x4af < uVar3) {
      for (uVar1 = 0; uVar1 < 5; uVar1 = uVar1 + 1) {
        if (ptrs[uVar1] != (void *)0x0) {
          kfree(ptrs[uVar1]);
        }
      }
      V((semaphore *)sm);
      return;
    }
    if (ptrs[uVar1] != (void *)0x0) {
      kfree(ptrs[uVar1]);
      ptrs[uVar1] = (void *)0x0;
    }
    uVar5 = kmalloctest4thread::sizes[uVar4];
    pvVar2 = kmalloc(uVar5 << 0xc);
    ptrs[uVar4] = pvVar2;
    if (pvVar2 == (void *)0x0) break;
    uVar4 = (uVar4 + 1) % 5;
    uVar1 = (uVar1 + 1) % 5;
    uVar3 = uVar3 + 1;
  }
                    /* WARNING: Subroutine does not return */
  panic("kmalloctest4: thread %lu: allocating %u pages failed\n",num,uVar5);
}



int kmalloctest(int nargs,char **args)

{
  kprintf("Starting kmalloc test...\n");
  kmallocthread((void *)0x0,0);
  kprintf("kmalloc test done\n");
  return 0;
}



int kmallocstress(int nargs,char **args)

{
  semaphore *sem;
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  sem = sem_create("kmallocstress",0);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("kmallocstress: sem_create failed\n");
  }
  kprintf("Starting kmalloc stress test...\n");
  data2 = 0;
  do {
    if (7 < (int)data2) {
      for (iVar2 = 0; iVar2 < 8; iVar2 = iVar2 + 1) {
        P(sem);
      }
      sem_destroy(sem);
      kprintf("kmalloc stress test done\n");
      return 0;
    }
    iVar2 = thread_fork("kmallocstress",(proc *)0x0,kmallocthread,sem,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("kmallocstress: thread_fork failed: %s\n",pcVar1);
}



/* WARNING: Removing unreachable block (ram,0x80011164) */

int kmalloctest3(int nargs,char **args)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  void *ptr;
  void *pvVar4;
  uint uVar5;
  char *pcVar6;
  uint uVar7;
  uint uVar8;
  byte *pbVar9;
  uint uVar10;
  uint uVar11;
  int iVar12;
  uint uVar13;
  int local_38;
  
  if (nargs == 2) {
    uVar2 = atoi(args[1]);
    uVar3 = uVar2 * 4 + 0x3ff >> 10;
    kprintf("kmalloctest3: %u objects, %u pointer blocks\n",uVar2,uVar3);
    ptr = kmalloc(uVar3 << 2);
    if (ptr == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
      panic("kmalloctest3: failed on pointer block array\n");
    }
    for (uVar7 = 0; uVar10 = 0, uVar7 < uVar3; uVar7 = uVar7 + 1) {
      pvVar4 = kmalloc(0x400);
      *(void **)((int)ptr + uVar7 * 4) = pvVar4;
      if (pvVar4 == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
        panic("kmalloctest3: failed on pointer block %u\n",uVar7);
      }
    }
    iVar1 = 0;
    uVar7 = 0;
    pbVar9 = (byte *)0x0;
    iVar12 = 0;
    for (; uVar10 < uVar2; uVar10 = uVar10 + 1) {
      uVar13 = kmalloctest3::sizes[uVar7];
      pvVar4 = kmalloc(uVar13);
      if (pvVar4 == (void *)0x0) {
        kprintf("kmalloctest3: failed on object %u size %u\n",uVar10,uVar13);
        kprintf("kmalloctest3: pos %u in pointer block %u\n",pbVar9,iVar12);
        kprintf("kmalloctest3: total so far %zu\n",iVar1);
                    /* WARNING: Subroutine does not return */
        panic("kmalloctest3: failed.\n");
      }
      for (uVar5 = 0; uVar5 < uVar13; uVar5 = uVar5 + 1) {
        *(char *)((int)pvVar4 + uVar5) = (char)uVar10;
      }
      *(void **)(*(int *)((int)ptr + iVar12 * 4) + (int)pbVar9 * 4) = pvVar4;
      pbVar9 = pbVar9 + 1;
      if ((byte *)0xff < pbVar9) {
        iVar12 = iVar12 + 1;
        pbVar9 = (byte *)0x0;
      }
      iVar1 = iVar1 + uVar13;
      uVar7 = (uVar7 + 1) % 5;
    }
    kprintf("kmalloctest3: %zu bytes allocated\n",iVar1);
    uVar13 = 0;
    uVar7 = 0;
    local_38 = 0;
    for (uVar10 = 0; iVar12 = uVar13 * 4, uVar10 < uVar2; uVar10 = uVar10 + 1) {
      uVar11 = kmalloctest3::sizes[uVar13];
      uVar5 = *(uint *)((int)ptr + local_38 * 4);
      pcVar6 = *(char **)(uVar5 + uVar7 * 4);
      uVar8 = 0;
      if (pcVar6 != (char *)0x0) {
        uVar5 = uVar10 & 0xff;
        goto LAB_80011280;
      }
      pcVar6 = "ptr != NULL";
      badassert("ptr != NULL","../../test/kmalloctest.c",0x101,"kmalloctest3");
      do {
        if (*pbVar9 != uVar5) {
          kprintf("kmalloctest3: failed on object %u size %u\n",uVar10,uVar11);
          kprintf("kmalloctest3: pos %u in pointer block %u\n",uVar7,local_38);
          kprintf("kmalloctest3: at object offset %u\n",uVar8);
          kprintf("kmalloctest3: expected 0x%x, found 0x%x\n",(uint)*pbVar9,uVar10 & 0xff);
                    /* WARNING: Subroutine does not return */
          panic("kmalloctest3: failed.\n");
        }
        uVar8 = uVar8 + 1;
LAB_80011280:
        pbVar9 = (byte *)(pcVar6 + uVar8);
      } while (uVar8 < uVar11);
      uVar7 = uVar7 + 1;
      kfree(pcVar6);
      if (0xff < uVar7) {
        local_38 = local_38 + 1;
        uVar7 = 0;
      }
      uVar5 = 5;
      if (iVar1 == 0) {
        badassert("totalsize > 0","../../test/kmalloctest.c",0x115,"kmalloctest3");
      }
      iVar1 = iVar1 - uVar11;
      uVar13 = (uVar13 + 1) % uVar5;
      if (uVar5 == 0) {
        trap(0x1c00);
      }
    }
    uVar2 = 0;
    if (iVar1 == 0) goto LAB_8001136c;
    badassert("totalsize == 0","../../test/kmalloctest.c",0x119,"kmalloctest3");
    do {
      pcVar6 = *(char **)((int)ptr + iVar12);
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "ptrblocks[i] != NULL";
        badassert("ptrblocks[i] != NULL","../../test/kmalloctest.c",0x11d,"kmalloctest3");
      }
      uVar2 = uVar2 + 1;
      kfree(pcVar6);
LAB_8001136c:
      iVar12 = uVar2 << 2;
    } while (uVar2 < uVar3);
    kfree(ptr);
    kprintf("kmalloctest3: passed\n");
    iVar1 = 0;
  }
  else {
    kprintf("kmalloctest3: usage: km3 numobjects\n");
    iVar1 = 8;
  }
  return iVar1;
}



int kmalloctest4(int nargs,char **args)

{
  semaphore *sem;
  int errcode;
  char *pcVar1;
  uint uVar2;
  
  kprintf("Starting multipage kmalloc test...\n");
  sem = sem_create("kmalloctest4",0);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("kmalloctest4: sem_create failed\n");
  }
  uVar2 = 0;
  while( true ) {
    if (5 < uVar2) {
      for (uVar2 = 0; uVar2 < 6; uVar2 = uVar2 + 1) {
        P(sem);
      }
      sem_destroy(sem);
      kprintf("Multipage kmalloc test done\n");
      return 0;
    }
    errcode = thread_fork("kmalloctest4",(proc *)0x0,kmalloctest4thread,sem,uVar2);
    if (errcode != 0) break;
    uVar2 = uVar2 + 1;
  }
  pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
  panic("kmallocstress: thread_fork failed: %s\n",pcVar1);
}



bool spinlock_not_held(spinlock *splk)

{
  return splk->splk_holder == (cpu *)0x0;
}



semaphore * makesem(uint count)

{
  semaphore *psVar1;
  
  psVar1 = sem_create("some-silly-name",count);
  if (psVar1 == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semunit: whoops: sem_create failed\n");
  }
  return psVar1;
}



void ok(void)

{
  kprintf("Test passed; now cleaning up.\n");
  return;
}



void makewaiter(semaphore *sem)

{
  int iVar1;
  
  spinlock_acquire(&waiters_lock);
  waiters_running = waiters_running + 1;
  spinlock_release(&waiters_lock);
  iVar1 = thread_fork("semunit waiter",(proc *)0x0,waiter,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semunit: thread_fork failed\n");
  }
  kprintf("Sleeping for waiter to run\n");
  clocksleep(1);
  return;
}



void waiter(void *vsem,ulong junk)

{
  uint uVar1;
  
  P((semaphore *)vsem);
  spinlock_acquire(&waiters_lock);
  uVar1 = waiters_running - 1;
  if (waiters_running == 0) {
    badassert("waiters_running > 0","../../test/semunit.c",100,"waiter");
  }
  waiters_running = uVar1;
  spinlock_release(&waiters_lock);
  return;
}



void semu17_sub(void *semv,ulong junk)

{
  char *pcVar1;
  thread *unaff_s7;
  
  semu17_thread = unaff_s7;
  if (*(int *)((int)semv + 0x10) != 0) {
    pcVar1 = "sem->sem_count == 0";
    badassert("sem->sem_count == 0","../../test/semunit.c",0x27b,"semu17_sub");
    semv = pcVar1;
  }
  P((semaphore *)semv);
  return;
}



void do_semu89(bool interrupthandler)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  undefined uVar3;
  undefined3 in_register_00000010;
  char *a;
  wchan *pwVar4;
  int unaff_s7;
  
  sem = makesem(0);
  a = sem->sem_name;
  pwVar4 = sem->sem_wchan;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x14b,"do_semu89");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x14c,"do_semu89");
  }
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    uVar3 = 1;
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      uVar3 = 1;
      badassert("curthread->t_in_interrupt == false","../../test/semunit.c",0x157,"do_semu89");
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar3;
  }
  V(sem);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    if (*(char *)(unaff_s7 + 0x58) == '\0') {
      badassert("curthread->t_in_interrupt == true","../../test/semunit.c",0x15e,"do_semu89");
    }
    *(undefined *)(unaff_s7 + 0x58) = 0;
  }
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x163,"do_semu89");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x164,"do_semu89");
  }
  if (sem->sem_wchan != pwVar4) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x165,"do_semu89");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x166,"do_semu89");
  }
  if (sem->sem_count != 1) {
    badassert("sem->sem_count == 1","../../test/semunit.c",0x167,"do_semu89");
  }
  ok();
  sem_destroy(sem);
  return;
}



int do_semu1011(bool interrupthandler)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  undefined uVar3;
  undefined3 in_register_00000010;
  char *a;
  wchan *pwVar4;
  int unaff_s7;
  
  sem = makesem(0);
  makewaiter(sem);
  a = sem->sem_name;
  pwVar4 = sem->sem_wchan;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x199,"do_semu1011");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x19a,"do_semu1011");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 1) {
    badassert("waiters_running == 1","../../test/semunit.c",0x19c,"do_semu1011");
  }
  spinlock_release(&waiters_lock);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    uVar3 = 1;
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      uVar3 = 1;
      badassert("curthread->t_in_interrupt == false","../../test/semunit.c",0x1a1,"do_semu1011");
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar3;
  }
  V(sem);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    if (*(char *)(unaff_s7 + 0x58) == '\0') {
      badassert("curthread->t_in_interrupt == true","../../test/semunit.c",0x1a8,"do_semu1011");
    }
    *(undefined *)(unaff_s7 + 0x58) = 0;
  }
  clocksleep(1);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x1b0,"do_semu1011");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x1b1,"do_semu1011");
  }
  if (sem->sem_wchan != pwVar4) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x1b2,"do_semu1011");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x1b3,"do_semu1011");
  }
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x1b4,"do_semu1011");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 0) {
    badassert("waiters_running == 0","../../test/semunit.c",0x1b6,"do_semu1011");
  }
  spinlock_release(&waiters_lock);
  ok();
  sem_destroy(sem);
  return 0;
}



void semu1213(bool interrupthandler)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  undefined uVar3;
  undefined3 in_register_00000010;
  char *a;
  wchan *pwVar4;
  int unaff_s7;
  
  sem = makesem(0);
  makewaiter(sem);
  makewaiter(sem);
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x1ec,"semu1213");
  }
  pwVar4 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x1ee,"semu1213");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 2) {
    badassert("waiters_running == 2","../../test/semunit.c",0x1f0,"semu1213");
  }
  spinlock_release(&waiters_lock);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    uVar3 = 1;
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      uVar3 = 1;
      badassert("curthread->t_in_interrupt == false","../../test/semunit.c",0x1f5,"semu1213");
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar3;
  }
  V(sem);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    if (*(char *)(unaff_s7 + 0x58) == '\0') {
      badassert("curthread->t_in_interrupt == true","../../test/semunit.c",0x1fc,"semu1213");
    }
    *(undefined *)(unaff_s7 + 0x58) = 0;
  }
  clocksleep(1);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x204,"semu1213");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x205,"semu1213");
  }
  if (sem->sem_wchan != pwVar4) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x206,"semu1213");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x207,"semu1213");
  }
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x208,"semu1213");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 1) {
    badassert("waiters_running == 1","../../test/semunit.c",0x20a,"semu1213");
  }
  spinlock_release(&waiters_lock);
  ok();
  V(sem);
  clocksleep(1);
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 0) {
    badassert("waiters_running == 0","../../test/semunit.c",0x212,"semu1213");
  }
  spinlock_release(&waiters_lock);
  sem_destroy(sem);
  return;
}



void semu19_sub(void *semv,ulong junk)

{
  kprintf("semu19: waiting for parent to sleep\n");
  clocksleep(1);
  V((semaphore *)semv);
  return;
}



int semu1(int nargs,char **args)

{
  undefined3 extraout_var;
  semaphore *sem;
  int iVar1;
  int iVar2;
  bool bVar3;
  
  sem = sem_create("some-silly-name",0x38);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semu1: whoops: sem_create failed\n");
  }
  iVar1 = strcmp(sem->sem_name,"some-silly-name");
  iVar2 = -0x7ffe0000;
  if (iVar1 != 0) {
    badassert("!strcmp(sem->sem_name, name)","../../test/semunit.c",0xa6,"semu1");
  }
  if (sem->sem_name == (char *)(iVar2 + 0x5af4)) {
    badassert("sem->sem_name != name","../../test/semunit.c",0xa7,"semu1");
  }
  if (sem->sem_wchan == (wchan *)0x0) {
    badassert("sem->sem_wchan != NULL","../../test/semunit.c",0xa8,"semu1");
  }
  bVar3 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0xa9,"semu1");
  }
  if (sem->sem_count != 0x38) {
    badassert("sem->sem_count == 56","../../test/semunit.c",0xaa,"semu1");
  }
  ok();
  sem_destroy(sem);
  return 0;
}



int semu2(int nargs,char **args)

{
  kprintf("This should crash with a kernel null dereference\n");
  sem_create((char *)0x0,0x2c);
                    /* WARNING: Subroutine does not return */
  panic("semu2: sem_create accepted a null name\n");
}



int semu3(int nargs,char **args)

{
  kprintf("This should assert that sem != NULL\n");
  sem_destroy((semaphore *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("semu3: sem_destroy accepted a null semaphore\n");
}



int semu4(int nargs,char **args)

{
  semaphore *sem;
  
  sem = makesem(0);
  sem->sem_count = sem->sem_count - 1;
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../test/semunit.c",0xe0,"semu4");
  }
  ok();
  sem_destroy(sem);
  return 0;
}



int semu5(int nargs,char **args)

{
  semaphore *sem;
  
  sem = sem_create("some-silly-name",0xf0000000);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semu5: sem_create failed\n");
  }
  if (sem->sem_count != 0xf0000000) {
    badassert("sem->sem_count == 0xf0000000U","../../test/semunit.c",0xf8,"semu5");
  }
  ok();
  sem_destroy(sem);
  return 0;
}



int semu6(int nargs,char **args)

{
  semaphore *sem;
  
  sem = makesem(0);
  makewaiter(sem);
  kprintf("This should assert that the wchan\'s threadlist is empty\n");
  sem_destroy(sem);
                    /* WARNING: Subroutine does not return */
  panic("semu6: wchan_destroy with waiters succeeded\n");
}



int semu7(int nargs,char **args)

{
  semaphore *sem;
  spinlock lk;
  
  sem = makesem(0);
  spinlock_init(&lk);
  spinlock_acquire(&lk);
  V(sem);
  V(sem);
  V(sem);
  ok();
  spinlock_release(&lk);
  spinlock_cleanup(&lk);
  sem_destroy(sem);
  return 0;
}



int semu8(int nargs,char **args)

{
  do_semu89(false);
  return 0;
}



int semu9(int nargs,char **args)

{
  do_semu89(true);
  return 0;
}



int semu10(int nargs,char **args)

{
  do_semu1011(false);
  return 0;
}



int semu11(int nargs,char **args)

{
  do_semu1011(true);
  return 0;
}



int semu12(int nargs,char **args)

{
  semu1213(false);
  return 0;
}



int semu13(int nargs,char **args)

{
  semu1213(true);
  return 0;
}



int semu14(int nargs,char **args)

{
  semaphore *sem;
  
  kprintf("This should assert that sem_count is > 0.\n");
  sem = makesem(0);
  sem->sem_count = sem->sem_count - 1;
  V(sem);
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x23d,"semu14");
  }
                    /* WARNING: Subroutine does not return */
  panic("semu14: V tolerated count wraparound\n");
}



int semu15(int nargs,char **args)

{
  kprintf("This should assert that the semaphore isn\'t null.\n");
  V((semaphore *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("semu15: V tolerated null semaphore\n");
}



int semu16(int nargs,char **args)

{
  semaphore *sem;
  spinlock lk;
  
  sem = makesem(1);
  spinlock_init(&lk);
  spinlock_acquire(&lk);
  P(sem);
  ok();
  spinlock_release(&lk);
  spinlock_cleanup(&lk);
  sem_destroy(sem);
  return 0;
}



int semu17(int nargs,char **args)

{
  semaphore *sem;
  int iVar1;
  thread *ptVar2;
  
  semu17_thread = (thread *)0x0;
  sem = makesem(0);
  iVar1 = thread_fork("semu17_sub",(proc *)0x0,semu17_sub,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semu17: whoops: thread_fork failed\n");
  }
  kprintf("Waiting for subthread...\n");
  clocksleep(1);
  ptVar2 = semu17_thread;
  if (semu17_thread == (thread *)0x0) {
    badassert("semu17_thread != NULL","../../test/semunit.c",0x293,"semu17");
  }
  if (ptVar2->t_state != S_SLEEP) {
    badassert("semu17_thread->t_state == S_SLEEP","../../test/semunit.c",0x294,"semu17");
  }
  ok();
  V(sem);
  clocksleep(1);
  sem_destroy(sem);
  semu17_thread = (thread *)0x0;
  return 0;
}



int semu18(int nargs,char **args)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  char *a;
  wchan *pwVar3;
  
  sem = makesem(1);
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2b3,"semu18");
  }
  pwVar3 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2b5,"semu18");
  }
  if (sem->sem_count != 1) {
    badassert("sem->sem_count == 1","../../test/semunit.c",0x2b6,"semu18");
  }
  P(sem);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",699,"semu18");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",700,"semu18");
  }
  if (sem->sem_wchan != pwVar3) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x2bd,"semu18");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2be,"semu18");
  }
  iVar1 = 0;
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2bf,"semu18");
  }
  return iVar1;
}



int semu19(int nargs,char **args)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  char *a;
  wchan *pwVar3;
  
  sem = makesem(0);
  iVar1 = thread_fork("semu19_sub",(proc *)0x0,semu19_sub,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semu19: whoops: thread_fork failed\n");
  }
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2f1,"semu19");
  }
  pwVar3 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2f3,"semu19");
  }
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2f4,"semu19");
  }
  P(sem);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x2f9,"semu19");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2fa,"semu19");
  }
  if (sem->sem_wchan != pwVar3) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x2fb,"semu19");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2fc,"semu19");
  }
  iVar1 = 0;
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2fd,"semu19");
  }
  return iVar1;
}



int semu20(int nargs,char **args)

{
  semaphore *sem;
  int unaff_s7;
  
  kprintf("This should assert that we aren\'t in an interrupt\n");
  sem = makesem(0);
  *(undefined *)(unaff_s7 + 0x58) = 1;
  P(sem);
                    /* WARNING: Subroutine does not return */
  panic("semu20: P tolerated being in an interrupt handler\n");
}



int semu21(int nargs,char **args)

{
  semaphore *sem;
  int unaff_s7;
  
  kprintf("This should assert that we aren\'t in an interrupt\n");
  sem = makesem(1);
  *(undefined *)(unaff_s7 + 0x58) = 1;
  P(sem);
                    /* WARNING: Subroutine does not return */
  panic("semu21: P tolerated being in an interrupt handler\n");
}



int semu22(int nargs,char **args)

{
  kprintf("This should assert that the semaphore isn\'t null.\n");
  P((semaphore *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("semu22: P tolerated null semaphore\n");
}



void semtestthread(void *junk,ulong num)

{
  int iVar1;
  
  P(testsem);
  kprintf("Thread %2lu: ",num);
  for (iVar1 = 0; iVar1 < 0x3f; iVar1 = iVar1 + 1) {
    kprintf("%c",num + 0x40);
  }
  kprintf("\n");
  V(donesem);
  return;
}



void inititems(void)

{
  if ((testsem == (semaphore *)0x0) &&
     (testsem = sem_create("testsem",2), testsem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: sem_create failed\n");
  }
  if ((testlock == (lock *)0x0) && (testlock = lock_create("testlock"), testlock == (lock *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: lock_create failed\n");
  }
  if ((testcv == (cv *)0x0) && (testcv = cv_create("testlock"), testcv == (cv *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: cv_create failed\n");
  }
  if ((donesem == (semaphore *)0x0) &&
     (donesem = sem_create("donesem",0), donesem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: sem_create failed\n");
  }
  return;
}



void fail(ulong num,char *msg)

{
  kprintf("thread %lu: Mismatch on %s\n",num,msg);
  kprintf("Test failed\n");
  lock_release(testlock);
  V(donesem);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}



void sleepthread(void *junk1,ulong junk2)

{
  bool bVar1;
  uint uVar2;
  lock **pplVar3;
  cv **ppcVar4;
  uint uVar5;
  
  for (uVar5 = 0; uVar2 = 0, uVar5 < 0x28; uVar5 = uVar5 + 1) {
    bVar1 = true;
    while (bVar1) {
      pplVar3 = testlocks + uVar2;
      ppcVar4 = testcvs + uVar2;
      lock_acquire(*pplVar3);
      uVar2 = uVar2 + 1;
      V(gatesem);
      cv_wait(*ppcVar4,*pplVar3);
      lock_release(*pplVar3);
      bVar1 = uVar2 < 0xfa;
    }
    kprintf("sleepthread: %u\n",uVar5);
  }
  V(exitsem);
  return;
}



/* WARNING: Could not reconcile some variable overlaps */

void cvtestthread(void *junk,ulong num)

{
  int iVar1;
  int j;
  timespec ts1;
  timespec ts2;
  
  iVar1 = 0;
  do {
    if (4 < iVar1) {
      V(donesem);
      return;
    }
    lock_acquire(testlock);
    while (testval1 != num) {
      gettime(&ts1);
      cv_wait(testcv,testlock);
      gettime(&ts2);
      timespec_sub(&ts2,&ts1,&ts2);
      if (((ts2.tv_sec._0_4_ | ts2.tv_sec._4_4_) == 0) && (ts2.tv_nsec < 80000)) {
        kprintf("cv_wait took only %u ns\n");
        kprintf("That\'s too fast... you must be busy-looping\n");
        V(donesem);
                    /* WARNING: Subroutine does not return */
        thread_exit();
      }
    }
    kprintf("Thread %lu\n",num);
    testval1 = testval1 + 0x1f & 0x1f;
    for (j = 0; j < 3000; j = j + 1) {
    }
    iVar1 = iVar1 + 1;
    cv_broadcast(testcv,testlock);
    lock_release(testlock);
  } while( true );
}



void wakethread(void *junk1,ulong junk2)

{
  bool bVar1;
  uint uVar2;
  lock **pplVar3;
  cv **ppcVar4;
  uint uVar5;
  
  for (uVar5 = 0; uVar2 = 0, uVar5 < 0x28; uVar5 = uVar5 + 1) {
    bVar1 = true;
    while (bVar1) {
      P(gatesem);
      pplVar3 = testlocks + uVar2;
      ppcVar4 = testcvs + uVar2;
      lock_acquire(*pplVar3);
      uVar2 = uVar2 + 1;
      cv_signal(*ppcVar4,*pplVar3);
      lock_release(*pplVar3);
      bVar1 = uVar2 < 0xfa;
    }
    kprintf("wakethread: %u\n",uVar5);
  }
  V(exitsem);
  return;
}



int semtest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting semaphore test...\n");
  kprintf("If this hangs, it\'s broken: ");
  P(testsem);
  data2 = 0;
  P(testsem);
  kprintf("ok\n");
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        V(testsem);
        P(donesem);
      }
      V(testsem);
      V(testsem);
      kprintf("Semaphore test done.\n");
      return 0;
    }
    iVar2 = thread_fork("semtest",(proc *)0x0,semtestthread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("semtest: thread_fork failed: %s\n",pcVar1);
}



int locktest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting lock test...\n");
  data2 = 0;
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        P(donesem);
      }
      kprintf("Lock test done.\n");
      return 0;
    }
    iVar2 = thread_fork("synchtest",(proc *)0x0,(anon_subr_void_void_ptr_ulong *)&locktestthread,
                        (void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("locktest: thread_fork failed: %s\n",pcVar1);
}



int cvtest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting CV test...\n");
  kprintf("Threads should print out in reverse order.\n");
  testval1 = 0x1f;
  data2 = 0;
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        P(donesem);
      }
      kprintf("CV test done\n");
      return 0;
    }
    iVar2 = thread_fork("synchtest",(proc *)0x0,cvtestthread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("cvtest: thread_fork failed: %s\n",pcVar1);
}



int cvtest2(int nargs,char **args)

{
  lock *plVar1;
  cv *pcVar2;
  int iVar3;
  uint uVar4;
  
  for (uVar4 = 0; uVar4 < 0xfa; uVar4 = uVar4 + 1) {
    plVar1 = lock_create("cvtest2 lock");
    testlocks[uVar4] = plVar1;
    pcVar2 = cv_create("cvtest2 cv");
    testcvs[uVar4] = pcVar2;
  }
  gatesem = sem_create("gatesem",0);
  exitsem = sem_create("exitsem",0);
  kprintf("cvtest2...\n");
  iVar3 = thread_fork("cvtest2",(proc *)0x0,sleepthread,(void *)0x0,0);
  if (iVar3 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("cvtest2: thread_fork failed\n");
  }
  iVar3 = thread_fork("cvtest2",(proc *)0x0,wakethread,(void *)0x0,0);
  if (iVar3 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("cvtest2: thread_fork failed\n");
  }
  P(exitsem);
  P(exitsem);
  sem_destroy(exitsem);
  sem_destroy(gatesem);
  gatesem = (semaphore *)0x0;
  exitsem = (semaphore *)0x0;
  for (uVar4 = 0; uVar4 < 0xfa; uVar4 = uVar4 + 1) {
    lock_destroy(testlocks[uVar4]);
    cv_destroy(testcvs[uVar4]);
    testlocks[uVar4] = (lock *)0x0;
    testcvs[uVar4] = (cv *)0x0;
  }
  kprintf("cvtest2 done\n");
  return 0;
}



thread * fakethread_create(char *name)

{
  thread *t;
  char *pcVar1;
  
  t = (thread *)kmalloc(100);
  if (t == (thread *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("threadlisttest: Out of memory\n");
  }
  bzero(t,100);
  pcVar1 = kstrdup(name);
  t->t_name = pcVar1;
  if (pcVar1 == (char *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("threadlisttest: Out of memory\n");
  }
  t->t_stack = (void *)0xbaabaa;
  threadlistnode_init(&t->t_listnode,t);
  return t;
}



void threadlisttest_a(void)

{
  undefined3 extraout_var;
  bool bVar1;
  threadlist tl;
  
  threadlist_init(&tl);
  bVar1 = threadlist_isempty(&tl);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("threadlist_isempty(&tl)","../../test/threadlisttest.c",0x7f,"threadlisttest_a");
  }
  threadlist_cleanup(&tl);
  return;
}



void threadlisttest_f(void)

{
  thread *ptVar1;
  int iVar2;
  thread **pptVar3;
  int iVar4;
  char *pcVar5;
  int iVar6;
  uint uVar7;
  threadlist tl;
  
  threadlist_init(&tl);
  for (uVar7 = 0; uVar7 < 7; uVar7 = uVar7 + 1) {
    threadlist_addtail(&tl,fakethreads[uVar7]);
  }
  if (tl.tl_count != 7) {
    badassert("tl.tl_count == NUMNAMES","../../test/threadlisttest.c",0x121,"threadlisttest_f");
  }
  iVar2 = 0;
  pcVar5 = (char *)fakethreads;
  for (ptVar1 = (tl.tl_head.tln_next)->tln_self; ptVar1 != (thread *)0x0;
      ptVar1 = ((ptVar1->t_listnode).tln_next)->tln_self) {
    pptVar3 = (thread **)((int)pcVar5 + iVar2 * 4);
    iVar2 = iVar2 + 1;
    if (*pptVar3 != ptVar1) {
      pcVar5 = "../../test/threadlisttest.c";
      badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x125,"threadlisttest_f");
    }
  }
  if (iVar2 != 7) {
    badassert("i == NUMNAMES","../../test/threadlisttest.c",0x128,"threadlisttest_f");
  }
  iVar2 = 0;
  iVar6 = 6;
  pcVar5 = (char *)fakethreads;
  for (ptVar1 = (tl.tl_tail.tln_prev)->tln_self; iVar4 = iVar6 - iVar2, ptVar1 != (thread *)0x0;
      ptVar1 = ((ptVar1->t_listnode).tln_prev)->tln_self) {
    iVar2 = iVar2 + 1;
    if (*(thread **)((int)pcVar5 + iVar4 * 4) != ptVar1) {
      pcVar5 = "../../test/threadlisttest.c";
      iVar6 = 300;
      badassert("t == fakethreads[NUMNAMES - i - 1]","../../test/threadlisttest.c",300,
                "threadlisttest_f");
    }
  }
  uVar7 = 0;
  if (iVar2 == 7) {
    for (; uVar7 < 7; uVar7 = uVar7 + 1) {
LAB_800137c0:
      ptVar1 = threadlist_remhead(&tl);
      if (fakethreads[uVar7] != ptVar1) {
        badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x133,"threadlisttest_f");
      }
    }
    if (tl.tl_count != 0) {
      badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x135,"threadlisttest_f");
    }
    return;
  }
  badassert("i == NUMNAMES","../../test/threadlisttest.c",0x12f,"threadlisttest_f");
  goto LAB_800137c0;
}



void check_order(threadlist *tl,bool rev)

{
  uint uVar1;
  char *a;
  undefined3 in_register_00000014;
  int iVar2;
  thread *ptVar3;
  char *b;
  char string0 [4];
  char stringN [4];
  
  iVar2 = CONCAT31(in_register_00000014,rev);
  string0 = 0x2e2e2e00;
  stringN = 0x7e7e7e00;
  if (iVar2 == 0) {
    a = string0;
    b = stringN;
  }
  else {
    a = stringN;
    b = string0;
  }
  for (ptVar3 = ((tl->tl_head).tln_next)->tln_self; ptVar3 != (thread *)0x0;
      ptVar3 = ((ptVar3->t_listnode).tln_next)->tln_self) {
    uVar1 = strcmp(a,ptVar3->t_name);
    if (iVar2 == 0) {
      uVar1 = uVar1 >> 0x1f;
    }
    else {
      uVar1 = (uint)(0 < (int)uVar1);
    }
    if (uVar1 == 0) {
      badassert("rev ? (cmp > 0) : (cmp < 0)","../../test/threadlisttest.c",0x6e,"check_order");
    }
    a = ptVar3->t_name;
  }
  uVar1 = strcmp(a,b);
  if (iVar2 == 0) {
    uVar1 = uVar1 >> 0x1f;
  }
  else {
    uVar1 = (uint)(0 < (int)uVar1);
  }
  if (uVar1 == 0) {
    badassert("rev ? (cmp > 0) : (cmp < 0)","../../test/threadlisttest.c",0x72,"check_order");
  }
  return;
}



void threadlisttest_b(void)

{
  thread *ptVar1;
  int iVar2;
  char *t;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  check_order(&tl,false);
  check_order(&tl,true);
  if (tl.tl_count != 1) {
    badassert("tl.tl_count == 1","../../test/threadlisttest.c",0x8f,"threadlisttest_b");
  }
  ptVar1 = threadlist_remhead(&tl);
  iVar2 = -0x7ffd0000;
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x91,"threadlisttest_b");
  }
  t = *(char **)(iVar2 + -0x5b20);
  if ((thread *)t != ptVar1) {
    t = "../../test/threadlisttest.c";
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0x92,"threadlisttest_b");
  }
  threadlist_addtail(&tl,(thread *)t);
  check_order(&tl,false);
  check_order(&tl,true);
  if (tl.tl_count != 1) {
    badassert("tl.tl_count == 1","../../test/threadlisttest.c",0x97,"threadlisttest_b");
  }
  ptVar1 = threadlist_remtail(&tl);
  iVar2 = -0x7ffd0000;
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x99,"threadlisttest_b");
  }
  if (*(thread **)(iVar2 + -0x5b20) != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0x9a,"threadlisttest_b");
  }
  threadlist_cleanup(&tl);
  return;
}



void threadlisttest_c(void)

{
  thread *ptVar1;
  char *t;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addhead(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xaa,"threadlisttest_c");
  }
  check_order(&tl,true);
  ptVar1 = threadlist_remhead(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xaf,"threadlisttest_c");
  }
  ptVar1 = threadlist_remhead(&tl);
  t = (char *)fakethreads[0];
  if (fakethreads[0] != ptVar1) {
    t = "../../test/threadlisttest.c";
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xb1,"threadlisttest_c");
  }
  if (tl.tl_count != 0) {
    t = "../../test/threadlisttest.c";
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xb2,"threadlisttest_c");
  }
  threadlist_addtail(&tl,(thread *)t);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xb6,"threadlisttest_c");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xbb,"threadlisttest_c");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xbd,"threadlisttest_c");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xbe,"threadlisttest_c");
  }
  threadlist_cleanup(&tl);
  return;
}



void threadlisttest_d(void)

{
  thread *ptVar1;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xce,"threadlisttest_d");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remhead(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xd3,"threadlisttest_d");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xd5,"threadlisttest_d");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xd6,"threadlisttest_d");
  }
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xda,"threadlisttest_d");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xdf,"threadlisttest_d");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xe1,"threadlisttest_d");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xe2,"threadlisttest_d");
  }
  threadlist_cleanup(&tl);
  return;
}



void threadlisttest_e(void)

{
  thread *ptVar1;
  thread *ptVar2;
  char *tl_00;
  uint uVar3;
  thread **unaff_s1;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[1]);
  threadlist_addtail(&tl,fakethreads[3]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xf3,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertafter(&tl,fakethreads[3],fakethreads[4]);
  if (tl.tl_count != 3) {
    badassert("tl.tl_count == 3","../../test/threadlisttest.c",0xf7,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertbefore(&tl,fakethreads[0],fakethreads[1]);
  if (tl.tl_count != 4) {
    badassert("tl.tl_count == 4","../../test/threadlisttest.c",0xfb,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertafter(&tl,fakethreads[1],fakethreads[2]);
  tl_00 = (char *)&tl;
  if (tl.tl_count != 5) {
    tl_00 = "tl.tl_count == 5";
    badassert("tl.tl_count == 5","../../test/threadlisttest.c",0xff,"threadlisttest_e");
  }
  check_order((threadlist *)tl_00,false);
  ptVar2 = fakethreads[3];
  if (((fakethreads[4]->t_listnode).tln_prev)->tln_self != fakethreads[3]) {
    badassert("fakethreads[4]->t_listnode.tln_prev->tln_self == fakethreads[3]",
              "../../test/threadlisttest.c",0x103,"threadlisttest_e");
  }
  ptVar1 = fakethreads[2];
  if (((ptVar2->t_listnode).tln_prev)->tln_self != fakethreads[2]) {
    badassert("fakethreads[3]->t_listnode.tln_prev->tln_self == fakethreads[2]",
              "../../test/threadlisttest.c",0x105,"threadlisttest_e");
  }
  ptVar2 = fakethreads[1];
  if (((ptVar1->t_listnode).tln_prev)->tln_self != fakethreads[1]) {
    badassert("fakethreads[2]->t_listnode.tln_prev->tln_self == fakethreads[1]",
              "../../test/threadlisttest.c",0x107,"threadlisttest_e");
  }
  uVar3 = 0;
  if (((ptVar2->t_listnode).tln_prev)->tln_self != fakethreads[0]) {
    badassert("fakethreads[1]->t_listnode.tln_prev->tln_self == fakethreads[0]",
              "../../test/threadlisttest.c",0x109,"threadlisttest_e");
    do {
      ptVar2 = threadlist_remhead(&tl);
      if (unaff_s1[uVar3] != ptVar2) {
        badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x10d,"threadlisttest_e");
      }
      uVar3 = uVar3 + 1;
LAB_80014200:
    } while (uVar3 < 5);
    if (tl.tl_count != 0) {
      badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x10f,"threadlisttest_e");
    }
    threadlist_cleanup(&tl);
    return;
  }
  unaff_s1 = fakethreads;
  goto LAB_80014200;
}



void fakethread_destroy(thread *t)

{
  char *pcVar1;
  
  pcVar1 = (char *)t;
  if (t->t_stack != (void *)0xbaabaa) {
    pcVar1 = "t->t_stack == FAKE_MAGIC";
    badassert("t->t_stack == FAKE_MAGIC","../../test/threadlisttest.c",0x55,"fakethread_destroy");
  }
  threadlistnode_cleanup((threadlistnode *)((int)pcVar1 + 0x3c));
  kfree(t->t_name);
  kfree(t);
  return;
}



int threadlisttest(int nargs,char **args)

{
  bool bVar1;
  thread *ptVar2;
  uint uVar3;
  thread **pptVar4;
  
  kprintf("Testing threadlists...\n");
  for (uVar3 = 0; uVar3 < 7; uVar3 = uVar3 + 1) {
    ptVar2 = fakethread_create(names[uVar3]);
    fakethreads[uVar3] = ptVar2;
  }
  uVar3 = 0;
  threadlisttest_a();
  threadlisttest_b();
  threadlisttest_c();
  threadlisttest_d();
  threadlisttest_e();
  threadlisttest_f();
  bVar1 = true;
  while (bVar1) {
    pptVar4 = fakethreads + uVar3;
    uVar3 = uVar3 + 1;
    fakethread_destroy(*pptVar4);
    *pptVar4 = (thread *)0x0;
    bVar1 = uVar3 < 7;
  }
  kprintf("Done.\n");
  return 0;
}



void init_sem(void)

{
  if ((tsem == (semaphore *)0x0) && (tsem = sem_create("tsem",0), tsem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("threadtest: sem_create failed\n");
  }
  return;
}



void runthreads(int doloud)

{
  char *pcVar1;
  code *entrypoint;
  ulong data2;
  int iVar2;
  char name [16];
  
  data2 = 0;
  do {
    if (7 < (int)data2) {
      for (iVar2 = 0; iVar2 < 8; iVar2 = iVar2 + 1) {
        P(tsem);
      }
      return;
    }
    snprintf(name,0x10,"threadtest%d",data2);
    entrypoint = loudthread;
    if (doloud == 0) {
      entrypoint = quietthread;
    }
    iVar2 = thread_fork(name,(proc *)0x0,entrypoint,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("threadtest: thread_fork failed %s)\n",pcVar1);
}



void quietthread(void *junk,ulong num)

{
  int i;
  
  putch(num + 0x30);
  for (i = 0; i < 200000; i = i + 1) {
  }
  putch(num + 0x30);
  V(tsem);
  return;
}



void loudthread(void *junk,ulong num)

{
  int iVar1;
  
  for (iVar1 = 0; iVar1 < 0x78; iVar1 = iVar1 + 1) {
    putch(num + 0x30);
  }
  V(tsem);
  return;
}



int threadtest(int nargs,char **args)

{
  init_sem();
  kprintf("Starting thread test...\n");
  runthreads(1);
  kprintf("\nThread test done.\n");
  return 0;
}



int threadtest2(int nargs,char **args)

{
  init_sem();
  kprintf("Starting thread test 2...\n");
  runthreads(0);
  kprintf("\nThread test 2 done.\n");
  return 0;
}



void setup(void)

{
  char *name;
  wchan *pwVar1;
  int iVar2;
  char tmp [16];
  
  if (wakersem == (semaphore *)0x0) {
    wakersem = sem_create("wakersem",1);
    donesem = sem_create("donesem",0);
    for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
      spinlock_init(spinlocks + iVar2);
      snprintf(tmp,0x10,"wc%d",iVar2);
      name = kstrdup(tmp);
      pwVar1 = wchan_create(name);
      waitchans[iVar2] = pwVar1;
    }
    wakerdone = 0;
    return;
  }
  wakerdone = 0;
  return;
}



void make_sleepalots(int howmany)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  char name [16];
  
  data2 = 0;
  while ((int)data2 < howmany) {
    snprintf(name,0x10,"sleepalot%d",data2);
    iVar1 = thread_fork(name,(proc *)0x0,sleepalot_thread,(void *)0x0,data2);
    data2 = data2 + 1;
    if (iVar1 != 0) {
      pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
      panic("thread_fork failed: %s\n",pcVar2);
    }
  }
  iVar1 = thread_fork("waker",(proc *)0x0,waker_thread,(void *)0x0,0);
  if (iVar1 != 0) {
    pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
    panic("thread_fork failed: %s\n",pcVar2);
  }
  return;
}



void make_computes(int howmany)

{
  int errcode;
  char *pcVar1;
  ulong data2;
  char name [16];
  
  data2 = 0;
  do {
    if (howmany <= (int)data2) {
      return;
    }
    snprintf(name,0x10,"compute%d",data2);
    errcode = thread_fork(name,(proc *)0x0,compute_thread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (errcode == 0);
  pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
  panic("thread_fork failed: %s\n",pcVar1);
}



void finish(int howmanytotal)

{
  int iVar1;
  
  for (iVar1 = 0; iVar1 < howmanytotal; iVar1 = iVar1 + 1) {
    P(donesem);
  }
  P(wakersem);
  wakerdone = 1;
  V(wakersem);
  P(donesem);
  return;
}



void runtest3(int nsleeps,int ncomputes)

{
  setup();
  kprintf("Starting thread test 3 (%d [sleepalots], %d {computes}, 1 waker)\n",nsleeps,ncomputes);
  make_sleepalots(nsleeps);
  make_computes(ncomputes);
  finish(nsleeps + ncomputes);
  kprintf("\nThread test 3 done\n");
  return;
}



/* WARNING: Removing unreachable block (ram,0x80014a28) */

void waker_thread(void *junk1,ulong junk2)

{
  bool bVar1;
  int iVar2;
  uint uVar3;
  spinlock *splk;
  int iVar4;
  wchan *wc;
  
  while( true ) {
    P(wakersem);
    iVar2 = wakerdone;
    V(wakersem);
    iVar4 = 0;
    if (iVar2 != 0) break;
    bVar1 = true;
    while (bVar1) {
      iVar4 = iVar4 + 1;
      uVar3 = random();
      splk = spinlocks + uVar3 % 0xc;
      wc = waitchans[uVar3 % 0xc];
      spinlock_acquire(splk);
      wchan_wakeall(wc,splk);
      spinlock_release(splk);
      thread_yield();
      bVar1 = iVar4 < 100;
    }
  }
  V(donesem);
  return;
}



/* WARNING: Removing unreachable block (ram,0x80014b1c) */

void sleepalot_thread(void *junk,ulong num)

{
  bool bVar1;
  uint uVar2;
  spinlock *splk;
  int iVar3;
  wchan *wc;
  int iVar4;
  
  for (iVar4 = 0; iVar3 = 0, iVar4 < 0x14; iVar4 = iVar4 + 1) {
    bVar1 = true;
    while (bVar1) {
      iVar3 = iVar3 + 1;
      uVar2 = random();
      splk = spinlocks + uVar2 % 0xc;
      wc = waitchans[uVar2 % 0xc];
      spinlock_acquire(splk);
      wchan_sleep(wc,splk);
      spinlock_release(splk);
      bVar1 = iVar3 < 4;
    }
    kprintf("[%lu]",num);
  }
  V(donesem);
  return;
}



void compute_thread(void *junk1,ulong num)

{
  bool bVar1;
  void *ptr;
  void *ptr_00;
  void *ptr_01;
  long lVar2;
  int iVar3;
  int iVar4;
  char cVar5;
  uint uVar6;
  int iVar7;
  int unaff_s2;
  int unaff_s4;
  int unaff_s5;
  int unaff_s6;
  int unaff_s8;
  char *local_30;
  
  ptr = kmalloc(0x1324);
  if (ptr == (void *)0x0) {
    badassert("m1 != NULL","../../test/tt3.c",0xb0,"compute_thread");
  }
  ptr_00 = kmalloc(0x1324);
  if (ptr_00 == (void *)0x0) {
    badassert("m2 != NULL","../../test/tt3.c",0xb2,"compute_thread");
  }
  ptr_01 = kmalloc(0x1324);
  if (ptr_01 != (void *)0x0) {
    unaff_s8 = 0;
    local_30 = "{%lu: %u}";
    bVar1 = true;
    goto LAB_80014de4;
  }
  badassert("m3 != NULL","../../test/tt3.c",0xb4,"compute_thread");
  do {
    lVar2 = random();
    iVar4 = (unaff_s5 - unaff_s4) + unaff_s2;
    *(char *)((int)ptr + iVar4) = (char)((uint)lVar2 >> 0x10);
    *(char *)((int)ptr_00 + iVar4) = (char)lVar2;
    unaff_s2 = unaff_s2 + 1;
    while (0x45 < unaff_s2) {
      unaff_s6 = unaff_s6 + 1;
      while (unaff_s2 = 0, 0x45 < unaff_s6) {
        for (iVar4 = 0; iVar4 < 0x46; iVar4 = iVar4 + 1) {
          for (iVar7 = 0; iVar3 = 0, iVar7 < 0x46; iVar7 = iVar7 + 1) {
            cVar5 = '\0';
            for (; iVar3 < 0x46; iVar3 = iVar3 + 1) {
              cVar5 = cVar5 + *(char *)((int)ptr + iVar4 * 0x46 + iVar3) *
                              *(char *)((int)ptr_00 + iVar3 * 0x46 + iVar7);
            }
            *(char *)((int)ptr_01 + iVar4 * 0x46 + iVar7) = cVar5;
          }
        }
        uVar6 = 0;
        for (iVar4 = 0; iVar4 < 0x46; iVar4 = iVar4 + 1) {
          uVar6 = uVar6 + (int)*(char *)((int)ptr_01 + iVar4 * 0x47) & 0xff;
        }
        unaff_s8 = unaff_s8 + 1;
        kprintf(local_30,num,uVar6);
        thread_yield();
        bVar1 = unaff_s8 < 10;
LAB_80014de4:
        unaff_s6 = 0;
        if (!bVar1) {
          kfree(ptr);
          kfree(ptr_00);
          kfree(ptr_01);
          V(donesem);
          return;
        }
      }
      unaff_s4 = unaff_s6 * 10;
      unaff_s5 = unaff_s6 * 0x50;
    }
  } while( true );
}



int threadtest3(int nargs,char **args)

{
  int iVar1;
  int ncomputes;
  
  if (nargs == 1) {
    runtest3(5,2);
    iVar1 = 0;
  }
  else if (nargs == 3) {
    iVar1 = atoi(args[1]);
    ncomputes = atoi(args[2]);
    runtest3(iVar1,ncomputes);
    iVar1 = 0;
  }
  else {
    kprintf("Usage: tt3 [sleepthreads computethreads]\n");
    iVar1 = 1;
  }
  return iVar1;
}



void hardclock_bootstrap(void)

{
  spinlock_init(&lbolt_lock);
  lbolt = wchan_create("lbolt");
  if (lbolt == (wchan *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Couldn\'t create lbolt\n");
  }
  return;
}



void timerclock(void)

{
  spinlock_acquire(&lbolt_lock);
  wchan_wakeall(lbolt,&lbolt_lock);
  spinlock_release(&lbolt_lock);
  return;
}



void hardclock(void)

{
  int unaff_s7;
  
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x2c) = *(int *)(*(int *)(unaff_s7 + 0x50) + 0x2c) + 1;
  if ((*(uint *)(*(int *)(unaff_s7 + 0x50) + 0x2c) & 0xf) == 0) {
    thread_consider_migration();
  }
  if ((*(uint *)(*(int *)(unaff_s7 + 0x50) + 0x2c) & 3) == 0) {
    schedule();
  }
  thread_yield();
  return;
}



void clocksleep(int num_secs)

{
  spinlock_acquire(&lbolt_lock);
  for (; 0 < num_secs; num_secs = num_secs + -1) {
    wchan_sleep(lbolt,&lbolt_lock);
  }
  spinlock_release(&lbolt_lock);
  return;
}



void spinlock_data_set(spinlock_data_t *sd,uint val)

{
  *sd = val;
  return;
}



spinlock_data_t spinlock_data_get(spinlock_data_t *sd)

{
  return *sd;
}



/* WARNING: Removing unreachable block (ram,0x8001507c) */

spinlock_data_t spinlock_data_testandset(spinlock_data_t *sd)

{
  spinlock_data_t sVar1;
  
  sVar1 = *sd;
  *sd = 1;
  return sVar1;
}



void membar_any_any(void)

{
  SYNC(0);
  return;
}



void membar_load_load(void)

{
  membar_any_any();
  return;
}



void membar_store_store(void)

{
  membar_any_any();
  return;
}



void membar_store_any(void)

{
  membar_any_any();
  return;
}



void membar_any_store(void)

{
  membar_any_any();
  return;
}



void spinlock_init(spinlock *splk)

{
  spinlock_data_set(&splk->splk_lock,0);
  splk->splk_holder = (cpu *)0x0;
  return;
}



void spinlock_cleanup(spinlock *splk)

{
  spinlock_data_t sVar1;
  char *pcVar2;
  
  if (splk->splk_holder != (cpu *)0x0) {
    pcVar2 = "splk->splk_holder == NULL";
    badassert("splk->splk_holder == NULL","../../thread/spinlock.c",0x40,"spinlock_cleanup");
    splk = (spinlock *)pcVar2;
  }
  sVar1 = spinlock_data_get(&splk->splk_lock);
  if (sVar1 != 0) {
    badassert("spinlock_data_get(&splk->splk_lock) == 0","../../thread/spinlock.c",0x41,
              "spinlock_cleanup");
  }
  return;
}



void spinlock_acquire(spinlock *splk)

{
  spinlock_data_t sVar1;
  cpu *pcVar2;
  int unaff_s7;
  
  splraise(0,1);
  pcVar2 = (cpu *)0x0;
  if (unaff_s7 != 0) {
    pcVar2 = **(cpu ***)(unaff_s7 + 0x50);
    if (splk->splk_holder == pcVar2) {
                    /* WARNING: Subroutine does not return */
      panic("Deadlock on spinlock %p\n",splk);
    }
    pcVar2->c_spinlocks = pcVar2->c_spinlocks + 1;
  }
  do {
    do {
      sVar1 = spinlock_data_get(&splk->splk_lock);
    } while (sVar1 != 0);
    sVar1 = spinlock_data_testandset(&splk->splk_lock);
  } while (sVar1 != 0);
  membar_store_any();
  splk->splk_holder = pcVar2;
  return;
}



void spinlock_release(spinlock *splk)

{
  cpu **ppcVar1;
  cpu *pcVar2;
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    ppcVar1 = *(cpu ***)(unaff_s7 + 0x50);
    if (splk->splk_holder != *ppcVar1) {
      badassert("splk->splk_holder == curcpu->c_self","../../thread/spinlock.c",0x84,
                "spinlock_release");
    }
    pcVar2 = (cpu *)((int)&ppcVar1[0xc][-1].c_ipi_lock.splk_holder + 3);
    if (ppcVar1[0xc] == (cpu *)0x0) {
      badassert("curcpu->c_spinlocks > 0","../../thread/spinlock.c",0x85,"spinlock_release");
    }
    ppcVar1[0xc] = pcVar2;
  }
  splk->splk_holder = (cpu *)0x0;
  membar_any_store();
  spinlock_data_set(&splk->splk_lock,0);
  spllower(1,0);
  return;
}



bool spinlock_do_i_hold(spinlock *splk)

{
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    return splk->splk_holder == **(cpu ***)(unaff_s7 + 0x50);
  }
  return true;
}



void splraise(int oldspl,int newspl)

{
  char *pcVar1;
  int unaff_s7;
  
  if (oldspl != 0) {
    pcVar1 = "../../thread/spl.c";
    badassert("oldspl == IPL_NONE","../../thread/spl.c",0x5c,"splraise");
    newspl = (int)pcVar1;
  }
  if ((char *)newspl != (char *)0x1) {
    badassert("newspl == IPL_HIGH","../../thread/spl.c",0x5d,"splraise");
  }
  if (unaff_s7 != 0) {
    if (*(int *)(unaff_s7 + 0x60) == 0) {
      cpu_irqoff();
    }
    *(int *)(unaff_s7 + 0x60) = *(int *)(unaff_s7 + 0x60) + 1;
  }
  return;
}



void spllower(int oldspl,int newspl)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  
  if (oldspl != 1) {
    pcVar2 = "../../thread/spl.c";
    badassert("oldspl == IPL_HIGH","../../thread/spl.c",0x70,"spllower");
    newspl = (int)pcVar2;
  }
  if ((char *)newspl != (char *)0x0) {
    badassert("newspl == IPL_NONE","../../thread/spl.c",0x71,"spllower");
  }
  if ((unaff_s7 != 0) &&
     (iVar1 = *(int *)(unaff_s7 + 0x60) + -1, *(int *)(unaff_s7 + 0x60) = iVar1, iVar1 == 0)) {
    cpu_irqon();
  }
  return;
}



int splx(int spl)

{
  int iVar1;
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    iVar1 = *(int *)(unaff_s7 + 0x5c);
    if (iVar1 < spl) {
      splraise(iVar1,spl);
      iVar1 = *(int *)(unaff_s7 + 0x5c);
      *(int *)(unaff_s7 + 0x5c) = spl;
      spl = iVar1;
    }
    else if (spl < iVar1) {
      *(int *)(unaff_s7 + 0x5c) = spl;
      spllower(iVar1,spl);
      spl = iVar1;
    }
  }
  return spl;
}



int spl0(void)

{
  int iVar1;
  
  iVar1 = splx(0);
  return iVar1;
}



int splhigh(void)

{
  int iVar1;
  
  iVar1 = splx(1);
  return iVar1;
}



semaphore * sem_create(char *name,uint initial_count)

{
  semaphore *ptr;
  char *name_00;
  wchan *pwVar1;
  
  ptr = (semaphore *)kmalloc(0x14);
  if (ptr == (semaphore *)0x0) {
    ptr = (semaphore *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->sem_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (semaphore *)0x0;
    }
    else {
      pwVar1 = wchan_create(name_00);
      ptr->sem_wchan = pwVar1;
      if (pwVar1 == (wchan *)0x0) {
        kfree(ptr->sem_name);
        kfree(ptr);
        ptr = (semaphore *)0x0;
      }
      else {
        spinlock_init(&ptr->sem_lock);
        ptr->sem_count = initial_count;
      }
    }
  }
  return ptr;
}



void sem_destroy(semaphore *sem)

{
  char *pcVar1;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x51,"sem_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  wchan_destroy(sem->sem_wchan);
  kfree(sem->sem_name);
  kfree(sem);
  return;
}



void P(semaphore *sem)

{
  char *pcVar1;
  spinlock *splk;
  int unaff_s7;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x5d,"P");
  }
  if (*(char *)(unaff_s7 + 0x58) != '\0') {
    pcVar1 = "curthread->t_in_interrupt == false";
    badassert("curthread->t_in_interrupt == false","../../thread/synch.c",0x65,"P");
  }
  splk = (spinlock *)((int)pcVar1 + 8);
  spinlock_acquire(splk);
  while (sem->sem_count == 0) {
    wchan_sleep(sem->sem_wchan,splk);
  }
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../thread/synch.c",0x78,"P");
  }
  sem->sem_count = sem->sem_count - 1;
  spinlock_release(splk);
  return;
}



void V(semaphore *sem)

{
  char *pcVar1;
  spinlock *splk;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x80,"V");
  }
  splk = (spinlock *)((int)pcVar1 + 8);
  spinlock_acquire(splk);
  sem->sem_count = sem->sem_count + 1;
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../thread/synch.c",0x85,"V");
  }
  wchan_wakeone(sem->sem_wchan,splk);
  spinlock_release(splk);
  return;
}



lock * lock_create(char *name)

{
  lock *ptr;
  char *name_00;
  semaphore *psVar1;
  
  ptr = (lock *)kmalloc(0x14);
  if (ptr == (lock *)0x0) {
    ptr = (lock *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->lk_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (lock *)0x0;
    }
    else {
      psVar1 = sem_create(name_00,1);
      ptr->lk_sem = psVar1;
      if (psVar1 == (semaphore *)0x0) {
        kfree(ptr->lk_name);
        kfree(ptr);
        ptr = (lock *)0x0;
      }
      else {
        ptr->lk_owner = (thread *)0x0;
        spinlock_init(&ptr->lk_lock);
      }
    }
  }
  return ptr;
}



void lock_destroy(lock *lock)

{
  char *pcVar1;
  
  pcVar1 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar1 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xb6,"lock_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  sem_destroy(lock->lk_sem);
  kfree(lock->lk_name);
  kfree(lock);
  return;
}



bool lock_do_i_hold(lock *lock)

{
  thread *ptVar1;
  thread *unaff_s7;
  
  spinlock_acquire(&lock->lk_lock);
  ptVar1 = lock->lk_owner;
  spinlock_release(&lock->lk_lock);
  return ptVar1 == unaff_s7;
}



void lock_acquire(lock *lock)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  bool bVar1;
  char *lock_00;
  thread *unaff_s7;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    lock_00 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xca,"lock_acquire");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  if (CONCAT31(extraout_var,bVar1) != 0) {
    kprintf("AAACKK!\n");
  }
  bVar1 = lock_do_i_hold(lock);
  if (CONCAT31(extraout_var_00,bVar1) != 0) {
    badassert("!(lock_do_i_hold(lock))","../../thread/synch.c",0xce,"lock_acquire");
  }
  if (unaff_s7->t_in_interrupt != false) {
    badassert("curthread->t_in_interrupt == false","../../thread/synch.c",0xd0,"lock_acquire");
  }
  P(lock->lk_sem);
  spinlock_acquire(&lock->lk_lock);
  if (lock->lk_owner != (thread *)0x0) {
    badassert("lock->lk_owner == NULL","../../thread/synch.c",0xe2,"lock_acquire");
  }
  lock->lk_owner = unaff_s7;
  spinlock_release(&lock->lk_lock);
  return;
}



void lock_release(lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *lock_00;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    lock_00 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xee,"lock_release");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0xef,"lock_release");
  }
  spinlock_acquire(&lock->lk_lock);
  lock->lk_owner = (thread *)0x0;
  V(lock->lk_sem);
  spinlock_release(&lock->lk_lock);
  return;
}



cv * cv_create(char *name)

{
  cv *ptr;
  char *name_00;
  wchan *pwVar1;
  
  ptr = (cv *)kmalloc(0x10);
  if (ptr == (cv *)0x0) {
    ptr = (cv *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->cv_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (cv *)0x0;
    }
    else {
      pwVar1 = wchan_create(name_00);
      ptr->cv_wchan = pwVar1;
      if (pwVar1 == (wchan *)0x0) {
        kfree(ptr->cv_name);
        kfree(ptr);
        ptr = (cv *)0x0;
      }
      else {
        spinlock_init(&ptr->cv_lock);
      }
    }
  }
  return ptr;
}



void cv_destroy(cv *cv)

{
  char *pcVar1;
  
  pcVar1 = (char *)cv;
  if (cv == (cv *)0x0) {
    pcVar1 = "cv != NULL";
    badassert("cv != NULL","../../thread/synch.c",0x13d,"cv_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  wchan_destroy(cv->cv_wchan);
  kfree(cv->cv_name);
  kfree(cv);
  return;
}



void cv_wait(cv *cv,lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  char *lock_00;
  spinlock *splk;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar2 = "lock != NULL";
    lock_00 = "../../thread/synch.c";
    badassert("lock != NULL","../../thread/synch.c",0x14d,"cv_wait");
    cv = (cv *)pcVar2;
  }
  if (cv == (cv *)0x0) {
    lock_00 = "../../thread/synch.c";
    badassert("cv != NULL","../../thread/synch.c",0x14e,"cv_wait");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  splk = &cv->cv_lock;
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0x14f,"cv_wait");
  }
  spinlock_acquire(splk);
  lock_release(lock);
  wchan_sleep(cv->cv_wchan,splk);
  spinlock_release(splk);
  lock_acquire(lock);
  return;
}



void cv_signal(cv *cv,lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  char *lock_00;
  spinlock *splk;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar2 = "lock != NULL";
    lock_00 = "../../thread/synch.c";
    badassert("lock != NULL","../../thread/synch.c",0x166,"cv_signal");
    cv = (cv *)pcVar2;
  }
  if (cv == (cv *)0x0) {
    lock_00 = "../../thread/synch.c";
    badassert("cv != NULL","../../thread/synch.c",0x167,"cv_signal");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  splk = &cv->cv_lock;
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0x168,"cv_signal");
  }
  spinlock_acquire(splk);
  wchan_wakeone(cv->cv_wchan,splk);
  spinlock_release(splk);
  return;
}



void cv_broadcast(cv *cv,lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  char *lock_00;
  spinlock *splk;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar2 = "lock != NULL";
    lock_00 = "../../thread/synch.c";
    badassert("lock != NULL","../../thread/synch.c",0x178,"cv_broadcast");
    cv = (cv *)pcVar2;
  }
  if (cv == (cv *)0x0) {
    lock_00 = "../../thread/synch.c";
    badassert("cv != NULL","../../thread/synch.c",0x179,"cv_broadcast");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  splk = &cv->cv_lock;
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0x17a,"cv_broadcast");
  }
  spinlock_acquire(splk);
  wchan_wakeall(cv->cv_wchan,splk);
  spinlock_release(splk);
  return;
}



void thread_checkstack_init(thread *thread)

{
  *(undefined4 *)thread->t_stack = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 4) = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 8) = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 0xc) = 0xbaadf00d;
  return;
}



void thread_checkstack(thread *thread)

{
  int *piVar1;
  uint uVar2;
  uint uVar3;
  
  piVar1 = (int *)thread->t_stack;
  if (piVar1 != (int *)0x0) {
    uVar2 = 0xbaad0000;
    if (*piVar1 != -0x45520ff3) {
      badassert("((uint32_t*)thread->t_stack)[0] == THREAD_STACK_MAGIC","../../thread/thread.c",0x68
                ,"thread_checkstack");
    }
    uVar3 = 0xbaad0000;
    if (piVar1[1] != (uVar2 | 0xf00d)) {
      badassert("((uint32_t*)thread->t_stack)[1] == THREAD_STACK_MAGIC","../../thread/thread.c",0x69
                ,"thread_checkstack");
    }
    if (piVar1[2] != (uVar3 | 0xf00d)) {
      badassert("((uint32_t*)thread->t_stack)[2] == THREAD_STACK_MAGIC","../../thread/thread.c",0x6a
                ,"thread_checkstack");
    }
    if (piVar1[3] != -0x45520ff3) {
      badassert("((uint32_t*)thread->t_stack)[3] == THREAD_STACK_MAGIC","../../thread/thread.c",0x6b
                ,"thread_checkstack");
    }
  }
  return;
}



thread * thread_create(char *name)

{
  thread *t;
  char *pcVar1;
  
  t = (thread *)kmalloc(100);
  if (t == (thread *)0x0) {
    t = (thread *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    t->t_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(t);
      t = (thread *)0x0;
    }
    else {
      t->t_wchan_name = "NEW";
      t->t_state = S_READY;
      thread_machdep_init(&t->t_machdep);
      threadlistnode_init(&t->t_listnode,t);
      t->t_stack = (void *)0x0;
      t->t_context = (switchframe *)0x0;
      t->t_cpu = (cpu *)0x0;
      t->t_proc = (proc *)0x0;
      t->t_in_interrupt = false;
      t->t_curspl = 1;
      t->t_iplhigh_count = 1;
    }
  }
  return t;
}



void thread_destroy(thread *thread)

{
  char *pcVar1;
  char *pcVar2;
  thread *unaff_s7;
  
  pcVar1 = (char *)thread;
  if (thread == unaff_s7) {
    pcVar1 = "thread != curthread";
    badassert("thread != curthread","../../thread/thread.c",0x10b,"thread_destroy");
  }
  pcVar2 = (char *)0x80020000;
  if (*(threadstate_t *)((int)pcVar1 + 8) == S_RUN) {
    pcVar1 = "thread->t_state != S_RUN";
    pcVar2 = "../../thread/thread.c";
    badassert("thread->t_state != S_RUN","../../thread/thread.c",0x10c,"thread_destroy");
  }
  if (*(proc **)((int)pcVar1 + 0x54) != (proc *)0x0) {
    pcVar1 = "thread->t_proc == NULL";
    badassert("thread->t_proc == NULL",pcVar2 + 0x6b28,0x114,"thread_destroy");
  }
  if (*(void **)((int)pcVar1 + 0x48) != (void *)0x0) {
    kfree(*(void **)((int)pcVar1 + 0x48));
  }
  threadlistnode_cleanup(&thread->t_listnode);
  thread_machdep_cleanup(&thread->t_machdep);
  thread->t_wchan_name = "DESTROYED";
  kfree(thread->t_name);
  kfree(thread);
  return;
}



void exorcise(void)

{
  thread *thread;
  thread *unaff_s7;
  
  while (thread = threadlist_remhead(&unaff_s7->t_cpu->c_zombies), thread != (thread *)0x0) {
    if (thread == unaff_s7) {
      badassert("z != curthread","../../thread/thread.c",0x12f,"exorcise");
    }
    if (thread->t_state != S_ZOMBIE) {
      badassert("z->t_state == S_ZOMBIE","../../thread/thread.c",0x130,"exorcise");
    }
    thread_destroy(thread);
  }
  return;
}



threadarray * threadarray_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  return (threadarray *)a;
}



void threadarray_destroy(threadarray *a)

{
  array_cleanup(&a->arr);
  kfree(a);
  return;
}



void threadarray_init(threadarray *a)

{
  array_init(&a->arr);
  return;
}



void threadarray_cleanup(threadarray *a)

{
  array_cleanup(&a->arr);
  return;
}



uint threadarray_num(threadarray *a)

{
  return (a->arr).num;
}



thread * threadarray_get(threadarray *a,uint index)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    badassert("index < a->num","../../include/array.h",100,"array_get");
    a = (threadarray *)pcVar1;
  }
  return *(thread **)(pcVar2 + (int)(a->arr).v);
}



void threadarray_set(threadarray *a,uint index,thread *val)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    val = (thread *)0x6b;
    badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    a = (threadarray *)pcVar1;
  }
  *(thread **)(pcVar2 + (int)(a->arr).v) = val;
  return;
}



int threadarray_preallocate(threadarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(&a->arr,num);
  return iVar1;
}



int threadarray_setsize(threadarray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_setsize(&a->arr,num);
  return iVar1;
}



int threadarray_add(threadarray *a,thread *val,uint *index_ret)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = (a->arr).num;
  iVar1 = array_setsize(&a->arr,uVar2 + 1);
  if (iVar1 == 0) {
    (a->arr).v[uVar2] = val;
    if (index_ret == (uint *)0x0) {
      iVar1 = 0;
    }
    else {
      *index_ret = uVar2;
      iVar1 = 0;
    }
  }
  return iVar1;
}



void threadarray_remove(threadarray *a,uint index)

{
  array_remove(&a->arr,index);
  return;
}



cpu * cpu_create(uint hardware_number)

{
  uint uVar1;
  cpu *c;
  int iVar2;
  char *pcVar3;
  thread *ptVar4;
  void *pvVar5;
  int unaff_s7;
  char namebuf [16];
  
  c = (cpu *)kmalloc(0xac);
  if (c == (cpu *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: Out of memory\n");
  }
  c->c_self = c;
  c->c_hardware_number = hardware_number;
  c->c_curthread = (thread *)0x0;
  threadlist_init(&c->c_zombies);
  c->c_hardclocks = 0;
  c->c_spinlocks = 0;
  c->c_isidle = false;
  threadlist_init(&c->c_runqueue);
  spinlock_init(&c->c_runqueue_lock);
  c->c_ipi_pending = 0;
  c->c_numshootdown = 0;
  spinlock_init(&c->c_ipi_lock);
  uVar1 = allcpus.arr.num;
  iVar2 = array_setsize(&allcpus.arr,allcpus.arr.num + 1);
  if (iVar2 == 0) {
    allcpus.arr.v[uVar1] = c;
    if (c == (cpu *)0xfffffffc) {
      iVar2 = 0;
    }
    else {
      c->c_number = uVar1;
      iVar2 = 0;
    }
  }
  if (iVar2 != 0) {
    pcVar3 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: array_add: %s\n",pcVar3);
  }
  snprintf(namebuf,0x10,"<boot #%d>",c->c_number);
  ptVar4 = thread_create(namebuf);
  c->c_curthread = ptVar4;
  if (ptVar4 == (thread *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: thread_create failed\n");
  }
  ptVar4->t_cpu = c;
  if (c->c_number != 0) {
    ptVar4 = c->c_curthread;
    pvVar5 = kmalloc(0x1000);
    ptVar4->t_stack = pvVar5;
    if (c->c_curthread->t_stack == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
      panic("cpu_create: couldn\'t allocate stack");
    }
    thread_checkstack_init(c->c_curthread);
  }
  if (unaff_s7 == 0) {
    ptVar4 = c->c_curthread;
    ptVar4->t_cpu = c;
    c->c_curthread = ptVar4;
  }
  iVar2 = proc_addthread(kproc,c->c_curthread);
  if (iVar2 != 0) {
    pcVar3 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: proc_addthread:: %s\n",pcVar3);
  }
  cpu_machdep_init(c);
  return c;
}



void thread_bootstrap(void)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  
  array_init(&allcpus.arr);
  if (unaff_s7 != 0) {
    badassert("CURCPU_EXISTS() == false","../../thread/thread.c",0x180,"thread_bootstrap");
  }
  cpu_create(0);
  if (unaff_s7 == 0) {
    badassert("CURCPU_EXISTS() == true","../../thread/thread.c",0x182,"thread_bootstrap");
  }
  if (*(int *)(unaff_s7 + 0x50) == 0) {
    badassert("curcpu != NULL","../../thread/thread.c",0x185,"thread_bootstrap");
  }
  if (unaff_s7 == 0) {
    badassert("curthread != NULL","../../thread/thread.c",0x186,"thread_bootstrap");
  }
  iVar1 = *(int *)(unaff_s7 + 0x54);
  iVar2 = -0x7ffd0000;
  if (iVar1 == 0) {
    badassert("curthread->t_proc != NULL","../../thread/thread.c",0x187,"thread_bootstrap");
  }
  if (iVar1 != *(int *)(iVar2 + -0x73e0)) {
    badassert(s_curthread__t_proc____kproc_80026d8c,"../../thread/thread.c",0x188,"thread_bootstrap"
             );
  }
  return;
}



void thread_start_cpus(void)

{
  uint uVar1;
  char buf [64];
  
  cpu_identify(buf,0x40);
  kprintf("cpu0: %s\n",buf);
  cpu_startup_sem = sem_create("cpu_hatch",0);
  mainbus_start_cpus();
  for (uVar1 = 0; uVar1 < allcpus.arr.num - 1; uVar1 = uVar1 + 1) {
    P(cpu_startup_sem);
  }
  sem_destroy(cpu_startup_sem);
  cpu_startup_sem = (semaphore *)0x0;
  return;
}



void schedule(void)

{
  return;
}



wchan * wchan_create(char *name)

{
  wchan *pwVar1;
  
  pwVar1 = (wchan *)kmalloc(0x20);
  if (pwVar1 == (wchan *)0x0) {
    pwVar1 = (wchan *)0x0;
  }
  else {
    threadlist_init(&pwVar1->wc_threads);
    pwVar1->wc_name = name;
  }
  return pwVar1;
}



void wchan_destroy(wchan *wc)

{
  threadlist_cleanup(&wc->wc_threads);
  kfree(wc);
  return;
}



bool wchan_isempty(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x449,"wchan_isempty");
  }
  bVar1 = threadlist_isempty(&wc->wc_threads);
  return bVar1;
}



void ipi_send(cpu *target,int code)

{
  char *pcVar1;
  
  if (0x1f < (uint)code) {
    pcVar1 = "code >= 0 && code < 32";
    badassert("code >= 0 && code < 32","../../thread/thread.c",0x45b,"ipi_send");
    target = (cpu *)pcVar1;
  }
  spinlock_acquire(&target->c_ipi_lock);
  target->c_ipi_pending = target->c_ipi_pending | 1 << (code & 0x1fU);
  mainbus_send_ipi(target);
  spinlock_release(&target->c_ipi_lock);
  return;
}



void thread_make_runnable(thread *target,bool already_have_lock)

{
  undefined3 extraout_var;
  bool bVar1;
  undefined3 in_register_00000014;
  cpu *target_00;
  int unaff_s7;
  
  target_00 = target->t_cpu;
  if (CONCAT31(in_register_00000014,already_have_lock) != 0) {
    bVar1 = spinlock_do_i_hold(&target_00->c_runqueue_lock);
    if (CONCAT31(extraout_var,bVar1) != 0) goto LAB_80016900;
    badassert("spinlock_do_i_hold(&targetcpu->c_runqueue_lock)","../../thread/thread.c",0x1cd,
              "thread_make_runnable");
  }
  spinlock_acquire(&target_00->c_runqueue_lock);
LAB_80016900:
  target->t_state = S_READY;
  threadlist_addtail(&target_00->c_runqueue,target);
  if ((target_00->c_isidle != false) && (**(cpu ***)(unaff_s7 + 0x50) != target_00)) {
    ipi_send(target_00,2);
  }
  if (CONCAT31(in_register_00000014,already_have_lock) == 0) {
    spinlock_release(&target_00->c_runqueue_lock);
  }
  return;
}



int thread_fork(char *name,proc *proc,anon_subr_void_void_ptr_ulong *entrypoint,void *data1,
               ulong data2)

{
  thread *thread;
  void *pvVar1;
  int iVar2;
  int unaff_s7;
  
  thread = thread_create(name);
  if (thread == (thread *)0x0) {
    iVar2 = 3;
  }
  else {
    pvVar1 = kmalloc(0x1000);
    thread->t_stack = pvVar1;
    if (pvVar1 == (void *)0x0) {
      thread_destroy(thread);
      iVar2 = 3;
    }
    else {
      thread_checkstack_init(thread);
      thread->t_cpu = *(cpu **)(unaff_s7 + 0x50);
      if (proc == (proc *)0x0) {
        proc = *(proc **)(unaff_s7 + 0x54);
      }
      iVar2 = proc_addthread(proc,thread);
      if (iVar2 == 0) {
        thread->t_iplhigh_count = thread->t_iplhigh_count + 1;
        switchframe_init(thread,entrypoint,data1,data2);
        thread_make_runnable(thread,false);
        iVar2 = 0;
      }
      else {
        thread_destroy(thread);
      }
    }
  }
  return iVar2;
}



void thread_switch(threadstate_t newstate,wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  int spl;
  bool bVar2;
  thread *ptVar1;
  cpu *pcVar3;
  thread *unaff_s7;
  
  spl = splx(1);
  pcVar3 = unaff_s7->t_cpu;
  if (pcVar3->c_isidle == false) {
    thread_checkstack(unaff_s7);
    spinlock_acquire(&pcVar3->c_runqueue_lock);
    if ((newstate == S_READY) &&
       (bVar2 = threadlist_isempty(&unaff_s7->t_cpu->c_runqueue), CONCAT31(extraout_var,bVar2) != 0)
       ) {
      spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
      splx(spl);
    }
    else {
      if (newstate == S_READY) {
        thread_make_runnable(unaff_s7,true);
        unaff_s7->t_state = S_READY;
      }
      else {
        if (newstate == S_RUN) {
                    /* WARNING: Subroutine does not return */
          panic("Illegal S_RUN in thread_switch\n");
        }
        if (newstate == S_SLEEP) {
          unaff_s7->t_wchan_name = wc->wc_name;
          threadlist_addtail(&wc->wc_threads,unaff_s7);
          spinlock_release(lk);
          unaff_s7->t_state = S_SLEEP;
        }
        else if (newstate == S_ZOMBIE) {
          unaff_s7->t_wchan_name = "ZOMBIE";
          threadlist_addtail(&unaff_s7->t_cpu->c_zombies,unaff_s7);
          unaff_s7->t_state = S_ZOMBIE;
        }
        else {
          unaff_s7->t_state = newstate;
        }
      }
      unaff_s7->t_cpu->c_isidle = true;
      while (ptVar1 = threadlist_remhead(&unaff_s7->t_cpu->c_runqueue), ptVar1 == (thread *)0x0) {
        spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
        cpu_idle();
        spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
      }
      unaff_s7->t_cpu->c_isidle = false;
      unaff_s7->t_cpu->c_curthread = ptVar1;
      switchframe_switch(&unaff_s7->t_context,&ptVar1->t_context);
      unaff_s7->t_wchan_name = (char *)0x0;
      unaff_s7->t_state = S_RUN;
      spinlock_release(&ptVar1->t_cpu->c_runqueue_lock);
      as_activate();
      exorcise();
      splx(spl);
    }
  }
  else {
    splx(spl);
  }
  return;
}



void thread_exit(void)

{
  thread *unaff_s7;
  
  if (unaff_s7->t_proc != (proc *)0x0) {
    proc_remthread(unaff_s7);
  }
  if (unaff_s7->t_proc != (proc *)0x0) {
    badassert("cur->t_proc == NULL","../../thread/thread.c",0x323,"thread_exit");
  }
  thread_checkstack(unaff_s7);
  splx(1);
  thread_switch(S_ZOMBIE,(wchan *)0x0,(spinlock *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("braaaaaaaiiiiiiiiiiinssssss\n");
}



void cpu_hatch(uint software_number)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  char buf [64];
  
  iVar1 = *(int *)(unaff_s7 + 0x50);
  if (iVar1 == 0) {
    pcVar2 = "curcpu != NULL";
    badassert("curcpu != NULL","../../thread/thread.c",0x19a,"cpu_hatch");
    software_number = (uint)pcVar2;
  }
  pcVar2 = (char *)software_number;
  if (unaff_s7 == 0) {
    pcVar2 = "curthread != NULL";
    badassert("curthread != NULL","../../thread/thread.c",0x19b,"cpu_hatch");
  }
  if (*(char **)(iVar1 + 4) != pcVar2) {
    badassert("curcpu->c_number == software_number","../../thread/thread.c",0x19c,"cpu_hatch");
  }
  splx(0);
  cpu_identify(buf,0x40);
  kprintf("cpu%u: %s\n",software_number,buf);
  V(cpu_startup_sem);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}



void thread_startup(anon_subr_void_void_ptr_ulong *entrypoint,void *data1,ulong data2)

{
  int unaff_s7;
  
  *(undefined4 *)(unaff_s7 + 4) = 0;
  *(undefined4 *)(unaff_s7 + 8) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
  as_activate();
  exorcise();
  splx(0);
  (*entrypoint)(data1,data2);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}



void thread_yield(void)

{
  thread_switch(S_READY,(wchan *)0x0,(spinlock *)0x0);
  return;
}



void wchan_sleep(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  int unaff_s7;
  
  if (*(char *)(unaff_s7 + 0x58) != '\0') {
    pcVar2 = "../../thread/thread.c";
    badassert("!curthread->t_in_interrupt","../../thread/thread.c",0x3f6,"wchan_sleep");
    lk = (spinlock *)pcVar2;
  }
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x3f9,"wchan_sleep");
  }
  if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) != 1) {
    pcVar2 = "../../thread/thread.c";
    badassert("curcpu->c_spinlocks == 1","../../thread/thread.c",0x3fc,"wchan_sleep");
    wc = (wchan *)pcVar2;
  }
  thread_switch(S_SLEEP,wc,lk);
  spinlock_acquire(lk);
  return;
}



void wchan_wakeone(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  thread *target;
  
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x40a,"wchan_wakeone");
  }
  target = threadlist_remhead(&wc->wc_threads);
  if (target != (thread *)0x0) {
    thread_make_runnable(target,false);
  }
  return;
}



void wchan_wakeall(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar2;
  thread *ptVar1;
  threadlist list;
  
  bVar2 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x428,"wchan_wakeall");
  }
  threadlist_init(&list);
  while( true ) {
    ptVar1 = threadlist_remhead(&wc->wc_threads);
    if (ptVar1 == (thread *)0x0) break;
    threadlist_addtail(&list,ptVar1);
  }
  while (ptVar1 = threadlist_remhead(&list), ptVar1 != (thread *)0x0) {
    thread_make_runnable(ptVar1,false);
  }
  threadlist_cleanup(&list);
  return;
}



void thread_consider_migration(void)

{
  uint uVar1;
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  int iVar2;
  bool bVar4;
  thread *ptVar3;
  uint uVar5;
  cpu *pcVar6;
  uint uVar7;
  int iVar8;
  uint uVar9;
  thread *unaff_s7;
  threadlist victims;
  
  uVar1 = allcpus.arr.num;
  iVar8 = 0;
  uVar9 = 0;
  for (uVar5 = 0; uVar5 < uVar1; uVar5 = uVar5 + 1) {
    iVar2 = uVar5 << 2;
    if (allcpus.arr.num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar6 = *(cpu **)((int)allcpus.arr.v + iVar2);
    spinlock_acquire(&pcVar6->c_runqueue_lock);
    uVar7 = (pcVar6->c_runqueue).tl_count;
    iVar8 = iVar8 + uVar7;
    if (unaff_s7->t_cpu->c_self == pcVar6) {
      uVar9 = uVar7;
    }
    spinlock_release(&pcVar6->c_runqueue_lock);
  }
  uVar5 = ((iVar8 + uVar1) - 1) / uVar1;
  if (uVar1 == 0) {
    trap(0x1c00);
  }
  uVar7 = uVar9 - uVar5;
  if (uVar5 <= uVar9) {
    threadlist_init(&victims);
    spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
    for (uVar9 = 0; uVar9 < uVar7; uVar9 = uVar9 + 1) {
      ptVar3 = threadlist_remtail(&unaff_s7->t_cpu->c_runqueue);
      threadlist_addhead(&victims,ptVar3);
    }
    spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
    uVar9 = 0;
    while ((uVar9 < uVar1 && (uVar7 != 0))) {
      iVar8 = uVar9 << 2;
      if (allcpus.arr.num <= uVar9) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      pcVar6 = *(cpu **)((int)allcpus.arr.v + iVar8);
      if (unaff_s7->t_cpu->c_self != pcVar6) {
        spinlock_acquire(&pcVar6->c_runqueue_lock);
        while (((pcVar6->c_runqueue).tl_count < uVar5 && (uVar7 != 0))) {
          ptVar3 = threadlist_remhead(&victims);
          if (ptVar3 == unaff_s7) {
            threadlist_addtail(&victims,ptVar3);
            uVar7 = uVar7 - 1;
          }
          else {
            ptVar3->t_cpu = pcVar6;
            threadlist_addtail(&pcVar6->c_runqueue,ptVar3);
            if ((dbflags & 0x10) != 0) {
              kprintf("Migrated thread %s: cpu %u -> %u",ptVar3->t_name,unaff_s7->t_cpu->c_number,
                      pcVar6->c_number);
            }
            uVar7 = uVar7 - 1;
            if (pcVar6->c_isidle != false) {
              ipi_send(pcVar6,2);
            }
          }
        }
        spinlock_release(&pcVar6->c_runqueue_lock);
      }
      uVar9 = uVar9 + 1;
    }
    bVar4 = threadlist_isempty(&victims);
    if (CONCAT31(extraout_var,bVar4) == 0) {
      spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
      while (ptVar3 = threadlist_remhead(&victims), ptVar3 != (thread *)0x0) {
        threadlist_addtail(&unaff_s7->t_cpu->c_runqueue,ptVar3);
      }
      spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
    }
    bVar4 = threadlist_isempty(&victims);
    if (CONCAT31(extraout_var_00,bVar4) == 0) {
      badassert("threadlist_isempty(&victims)","../../thread/thread.c",0x3bf,
                "thread_consider_migration");
    }
    threadlist_cleanup(&victims);
  }
  return;
}



void ipi_broadcast(int code)

{
  int iVar1;
  uint uVar2;
  int unaff_s7;
  
  for (uVar2 = 0; uVar2 < allcpus.arr.num; uVar2 = uVar2 + 1) {
    iVar1 = uVar2 << 2;
    if (allcpus.arr.num <= uVar2) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (**(cpu ***)(unaff_s7 + 0x50) != *(cpu **)((int)allcpus.arr.v + iVar1)) {
      ipi_send(*(cpu **)((int)allcpus.arr.v + iVar1),code);
    }
  }
  return;
}



void thread_panic(void)

{
  int unaff_s7;
  
  ipi_broadcast(0);
  *(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 0x50) = 0;
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x3c) = *(int *)(unaff_s7 + 0x50) + 0x44;
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x44) = *(int *)(unaff_s7 + 0x50) + 0x38;
  return;
}



void thread_shutdown(void)

{
  ipi_broadcast(1);
  return;
}



void ipi_tlbshootdown(cpu *target,tlbshootdown *mapping)

{
  uint uVar1;
  
  spinlock_acquire(&target->c_ipi_lock);
  uVar1 = target->c_numshootdown;
  if (uVar1 == 0x10) {
                    /* WARNING: Subroutine does not return */
    panic("ipi_tlbshootdown: Too many shootdowns queued\n");
  }
  target->c_shootdown[uVar1].ts_placeholder = mapping->ts_placeholder;
  target->c_numshootdown = uVar1 + 1;
  target->c_ipi_pending = target->c_ipi_pending | 8;
  mainbus_send_ipi(target);
  spinlock_release(&target->c_ipi_lock);
  return;
}



void interprocessor_interrupt(void)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
  uVar2 = *(uint *)(*(int *)(unaff_s7 + 0x50) + 0x5c);
  if ((uVar2 & 1) != 0) {
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
    cpu_halt();
  }
  if ((uVar2 & 2) != 0) {
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
    if (*(char *)(*(int *)(unaff_s7 + 0x50) + 0x34) == '\0') {
      kprintf("cpu%d: offline: warning: not idle\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    }
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
    kprintf("cpu%d: offline.\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    cpu_halt();
  }
  uVar3 = 0;
  if ((uVar2 & 8) != 0) {
    for (; iVar1 = *(int *)(unaff_s7 + 0x50), uVar3 < *(uint *)(iVar1 + 0xa0); uVar3 = uVar3 + 1) {
      vm_tlbshootdown((tlbshootdown *)(iVar1 + (uVar3 + 0x18) * 4));
    }
    *(undefined4 *)(iVar1 + 0xa0) = 0;
  }
  *(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 0x5c) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
  return;
}



void threadlist_insertafternode(threadlistnode *onlist,thread *t)

{
  (t->t_listnode).tln_prev = onlist;
  (t->t_listnode).tln_next = onlist->tln_next;
  onlist->tln_next = &t->t_listnode;
  ((t->t_listnode).tln_next)->tln_prev = &t->t_listnode;
  return;
}



void threadlist_insertbeforenode(thread *t,threadlistnode *onlist)

{
  threadlistnode *ptVar1;
  
  ptVar1 = onlist->tln_prev;
  (t->t_listnode).tln_prev = ptVar1;
  (t->t_listnode).tln_next = onlist;
  ptVar1->tln_next = &t->t_listnode;
  ((t->t_listnode).tln_next)->tln_prev = &t->t_listnode;
  return;
}



void threadlist_removenode(threadlistnode *tln)

{
  tln->tln_prev->tln_next = tln->tln_next;
  tln->tln_next->tln_prev = tln->tln_prev;
  tln->tln_prev = (threadlistnode *)0x0;
  tln->tln_next = (threadlistnode *)0x0;
  return;
}



void threadlistnode_init(threadlistnode *tln,thread *t)

{
  char *pcVar1;
  char *pcVar2;
  
  if (t == (thread *)0x0) {
    pcVar1 = s_t____NULL_80026f88;
    pcVar2 = "../../thread/threadlist.c";
    badassert(s_t____NULL_80026f88,"../../thread/threadlist.c",0x2b,"threadlistnode_init");
    tln = (threadlistnode *)pcVar1;
    t = (thread *)pcVar2;
  }
  tln->tln_next = (threadlistnode *)0x0;
  tln->tln_prev = (threadlistnode *)0x0;
  tln->tln_self = t;
  return;
}



void threadlistnode_cleanup(threadlistnode *tln)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)0x80020000;
  pcVar1 = (char *)tln;
  if (tln->tln_next != (threadlistnode *)0x0) {
    pcVar1 = "tln->tln_next == NULL";
    pcVar2 = "../../thread/threadlist.c";
    badassert("tln->tln_next == NULL","../../thread/threadlist.c",0x37,"threadlistnode_cleanup");
  }
  if (*(threadlistnode **)pcVar1 != (threadlistnode *)0x0) {
    pcVar1 = "tln->tln_prev == NULL";
    badassert("tln->tln_prev == NULL",pcVar2 + 0x6f94,0x38,"threadlistnode_cleanup");
  }
  if (*(thread **)((int)pcVar1 + 8) == (thread *)0x0) {
    badassert("tln->tln_self != NULL","../../thread/threadlist.c",0x39,"threadlistnode_cleanup");
  }
  return;
}



void threadlist_init(threadlist *tl)

{
  (tl->tl_head).tln_next = &tl->tl_tail;
  (tl->tl_head).tln_prev = (threadlistnode *)0x0;
  (tl->tl_tail).tln_next = (threadlistnode *)0x0;
  (tl->tl_tail).tln_prev = &tl->tl_head;
  (tl->tl_head).tln_self = (thread *)0x0;
  (tl->tl_tail).tln_self = (thread *)0x0;
  tl->tl_count = 0;
  return;
}



bool threadlist_isempty(threadlist *tl)

{
  return tl->tl_count == 0;
}



void threadlist_cleanup(threadlist *tl)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = threadlist_isempty(tl);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("threadlist_isempty(tl)","../../thread/threadlist.c",0x55,"threadlist_cleanup");
  }
  if (tl->tl_count != 0) {
    badassert("tl->tl_count == 0","../../thread/threadlist.c",0x56,"threadlist_cleanup");
  }
  return;
}



void threadlist_addhead(threadlist *tl,thread *t)

{
  threadlist_insertafternode(&tl->tl_head,t);
  tl->tl_count = tl->tl_count + 1;
  return;
}



void threadlist_addtail(threadlist *tl,thread *t)

{
  threadlist_insertbeforenode(t,&tl->tl_tail);
  tl->tl_count = tl->tl_count + 1;
  return;
}



thread * threadlist_remhead(threadlist *tl)

{
  thread *ptVar1;
  threadlistnode *tln;
  
  tln = (tl->tl_head).tln_next;
  if (tln->tln_next == (threadlistnode *)0x0) {
    ptVar1 = (thread *)0x0;
  }
  else {
    threadlist_removenode(tln);
    tl->tl_count = tl->tl_count - 1;
    ptVar1 = tln->tln_self;
  }
  return ptVar1;
}



thread * threadlist_remtail(threadlist *tl)

{
  thread *ptVar1;
  threadlistnode *tln;
  
  tln = (tl->tl_tail).tln_prev;
  if (tln->tln_prev == (threadlistnode *)0x0) {
    ptVar1 = (thread *)0x0;
  }
  else {
    threadlist_removenode(tln);
    tl->tl_count = tl->tl_count - 1;
    ptVar1 = tln->tln_self;
  }
  return ptVar1;
}



void threadlist_insertafter(threadlist *tl,thread *onlist,thread *addee)

{
  threadlist_insertafternode(&onlist->t_listnode,addee);
  tl->tl_count = tl->tl_count + 1;
  return;
}



void threadlist_insertbefore(threadlist *tl,thread *addee,thread *onlist)

{
  threadlist_insertbeforenode(addee,&onlist->t_listnode);
  tl->tl_count = tl->tl_count + 1;
  return;
}



void threadlist_remove(threadlist *tl,thread *t)

{
  threadlist_removenode(&t->t_listnode);
  tl->tl_count = tl->tl_count - 1;
  return;
}



int dev_eachopen(vnode *v,int flags)

{
  int iVar1;
  
  if ((flags & 0x3cU) == 0) {
                    /* WARNING: Load size is inaccurate */
    iVar1 = (***v->vn_data)();
    return iVar1;
  }
  return 8;
}



int dev_reclaim(vnode *v)

{
  return 0;
}



int dev_tryseek(device *d,off_t pos)

{
  int iVar1;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  uint uVar2;
  longlong lVar3;
  undefined4 in_stack_ffffffe8;
  
  uVar2 = d->d_blocks;
  iVar1 = 0;
  if (uVar2 != 0) {
    lVar3 = __moddi3(CONCAT44(in_stack_ffffffe8,unaff_s0),CONCAT44(unaff_s1,unaff_s2));
    iVar1 = 8;
    if (lVar3 == 0) {
      lVar3 = __divdi3(CONCAT44(in_stack_ffffffe8,unaff_s0),CONCAT44(unaff_s1,unaff_s2));
      if ((lVar3 < 0) || (((int)((ulonglong)lVar3 >> 0x20) == 0 && ((uint)lVar3 < uVar2)))) {
        iVar1 = 0;
      }
      else {
        iVar1 = 8;
      }
    }
  }
  return iVar1;
}



int dev_ioctl(vnode *v,int op,userptr_t data)

{
  int iVar1;
  
                    /* WARNING: Load size is inaccurate */
  iVar1 = (**(code **)(*v->vn_data + 8))();
  return iVar1;
}



int dev_gettype(vnode *v,mode_t *ret)

{
  if (*(int *)((int)v->vn_data + 4) == 0) {
    *ret = 0x6000;
  }
  else {
    *ret = 0x7000;
  }
  return 0;
}



bool dev_isseekable(vnode *v)

{
  int iVar1;
  
  iVar1 = *(int *)((int)v->vn_data + 4);
  if (iVar1 != 0) {
    iVar1 = 1;
  }
  return SUB41(iVar1,0);
}



int null_fsync(vnode *v)

{
  return 0;
}



int dev_mmap(vnode *v)

{
  return 1;
}



int dev_truncate(vnode *v,off_t len)

{
  int iVar1;
  int in_a2;
  int in_a3;
  
  iVar1 = *(int *)((int)v->vn_data + 4);
  if (iVar1 == 0) {
    return 8;
  }
  if ((in_a2 == 0) && (iVar1 * *(int *)((int)v->vn_data + 8) == in_a3)) {
    return 0;
  }
  return 8;
}



int dev_namefile(vnode *v,uio *uio)

{
  return 0;
}



int dev_lookup(vnode *dir,char *pathname,vnode **result)

{
  size_t sVar1;
  int iVar2;
  
  sVar1 = strlen(pathname);
  if (sVar1 == 0) {
    vnode_incref(dir);
    *result = dir;
    iVar2 = 0;
  }
  else {
    iVar2 = 0x13;
  }
  return iVar2;
}



int dev_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  if (*(int *)((int)pvVar2 + 4) == 0) {
    *(undefined4 *)((int)&statbuf->st_size + 4) = 0;
    *(undefined4 *)&statbuf->st_size = 0;
  }
  else {
    *(int *)((int)&statbuf->st_size + 4) = *(int *)((int)pvVar2 + 4) * *(int *)((int)pvVar2 + 8);
    *(undefined4 *)&statbuf->st_size = 0;
    statbuf->st_blksize = *(blksize_t *)((int)pvVar2 + 8);
  }
  vnode_check(v,"gettype");
  iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
  if (iVar1 == 0) {
    statbuf->st_mode = statbuf->st_mode | 0x180;
    statbuf->st_nlink = 1;
    statbuf->st_blocks = *(blkcnt_t *)((int)pvVar2 + 4);
    statbuf->st_dev = 0;
    statbuf->st_rdev = *(dev_t *)((int)pvVar2 + 0xc);
    iVar1 = 0;
  }
  return iVar1;
}



int dev_write(vnode *v,uio *uio)

{
  int iVar1;
  char *pcVar2;
  undefined4 unaff_s0;
  device *d;
  undefined4 in_stack_fffffff0;
  
  d = (device *)v->vn_data;
  iVar1 = dev_tryseek(d,CONCAT44(in_stack_fffffff0,unaff_s0));
  if (iVar1 == 0) {
    pcVar2 = (char *)d;
    if (uio->uio_rw != UIO_WRITE) {
      pcVar2 = "uio->uio_rw == UIO_WRITE";
      badassert("uio->uio_rw == UIO_WRITE","../../vfs/device.c",0x8a,"dev_write");
    }
    iVar1 = (*d->d_ops->devop_io)((device *)pcVar2,uio);
  }
  return iVar1;
}



int dev_read(vnode *v,uio *uio)

{
  int iVar1;
  char *pcVar2;
  undefined4 unaff_s0;
  device *d;
  undefined4 in_stack_fffffff0;
  
  d = (device *)v->vn_data;
  iVar1 = dev_tryseek(d,CONCAT44(in_stack_fffffff0,unaff_s0));
  if (iVar1 == 0) {
    pcVar2 = (char *)d;
    if (uio->uio_rw != UIO_READ) {
      pcVar2 = "uio->uio_rw == UIO_READ";
      badassert("uio->uio_rw == UIO_READ","../../vfs/device.c",0x77,"dev_read");
    }
    iVar1 = (*d->d_ops->devop_io)((device *)pcVar2,uio);
  }
  return iVar1;
}



vnode * dev_create_vnode(device *dev)

{
  vnode *vn;
  int errcode;
  char *pcVar1;
  
  vn = (vnode *)kmalloc(0x18);
  if (vn == (vnode *)0x0) {
    vn = (vnode *)0x0;
  }
  else {
    errcode = vnode_init(vn,&dev_vnode_ops,(fs *)0x0,dev);
    if (errcode != 0) {
      pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
      panic("While creating vnode for device: vnode_init: %s\n",pcVar1);
    }
  }
  return vn;
}



void dev_uncreate_vnode(vnode *vn)

{
  char *vn_00;
  
  vn_00 = (char *)vn;
  if (vn->vn_ops != &dev_vnode_ops) {
    vn_00 = "vn->vn_ops == &dev_vnode_ops";
    badassert("vn->vn_ops == &dev_vnode_ops","../../vfs/device.c",0x181,"dev_uncreate_vnode");
  }
  vnode_cleanup((vnode *)vn_00);
  kfree(vn);
  return;
}



int nullopen(device *dev,int openflags)

{
  return 0;
}



int nullio(device *dev,uio *uio)

{
  if (uio->uio_rw == UIO_WRITE) {
    uio->uio_resid = 0;
  }
  return 0;
}



int nullioctl(device *dev,int op,userptr_t data)

{
  return 8;
}



void devnull_create(void)

{
  device *dev;
  int errcode;
  char *pcVar1;
  
  dev = (device *)kmalloc(0x14);
  if (dev == (device *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Could not add null device: out of memory\n");
  }
  dev->d_ops = &null_devops;
  dev->d_blocks = 0;
  dev->d_blocksize = 1;
  dev->d_devnumber = 0;
  dev->d_data = (void *)0x0;
  errcode = vfs_adddev("null",dev,0);
  if (errcode != 0) {
    pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
    panic("Could not add null device: %s\n",pcVar1);
  }
  return;
}



int vfs_getcurdir(vnode **ret)

{
  vnode *vn;
  int iVar1;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
  if (vn == (vnode *)0x0) {
    iVar1 = 0x13;
  }
  else {
    vnode_incref(vn);
    *ret = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    iVar1 = 0;
  }
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  return iVar1;
}



int vfs_setcurdir(vnode *dir)

{
  int iVar1;
  vnode *vn;
  int unaff_s7;
  mode_t vtype;
  
  vnode_check(dir,"gettype");
  iVar1 = (*dir->vn_ops->vop_gettype)(dir,&vtype);
  if ((iVar1 == 0) && (iVar1 = 0x11, vtype == 0x2000)) {
    vnode_incref(dir);
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14) = dir;
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    if (vn == (vnode *)0x0) {
      iVar1 = 0;
    }
    else {
      vnode_decref(vn);
      iVar1 = 0;
    }
  }
  return iVar1;
}



int vfs_clearcurdir(void)

{
  vnode *vn;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
  *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x14) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  if (vn != (vnode *)0x0) {
    vnode_decref(vn);
  }
  return 0;
}



int vfs_chdir(char *path)

{
  int iVar1;
  vnode *vn;
  
  iVar1 = vfs_lookup(path,&vn);
  if (iVar1 == 0) {
    iVar1 = vfs_setcurdir(vn);
    vnode_decref(vn);
  }
  return iVar1;
}



int vfs_getcwd(uio *uio)

{
  int iVar1;
  size_t n;
  char *pcVar2;
  vnode *cwd;
  char colon;
  
  colon = ':';
  if (uio->uio_rw != UIO_READ) {
    badassert("uio->uio_rw==UIO_READ","../../vfs/vfscwd.c",0x97,"vfs_getcwd");
  }
  iVar1 = vfs_getcurdir(&cwd);
  if (iVar1 == 0) {
    pcVar2 = (char *)cwd->vn_fs;
    if ((fs *)pcVar2 == (fs *)0x0) {
      pcVar2 = "cwd->vn_fs != NULL";
      badassert("cwd->vn_fs != NULL","../../vfs/vfscwd.c",0x9f,"vfs_getcwd");
    }
    pcVar2 = (*(*(fs_ops **)((int)pcVar2 + 4))->fsop_getvolname)((fs *)pcVar2);
    if (pcVar2 == (char *)0x0) {
      vfs_biglock_acquire();
      pcVar2 = vfs_getdevname(cwd->vn_fs);
      vfs_biglock_release();
    }
    if (pcVar2 == (char *)0x0) {
      badassert("name != NULL","../../vfs/vfscwd.c",0xa7,"vfs_getcwd");
    }
    n = strlen(pcVar2);
    iVar1 = uiomove(pcVar2,n,uio);
    if ((iVar1 == 0) && (iVar1 = uiomove(&colon,1,uio), iVar1 == 0)) {
      vnode_check(cwd,"namefile");
      iVar1 = (*cwd->vn_ops->vop_namefile)(cwd,uio);
    }
    vnode_decref(cwd);
  }
  return iVar1;
}



int vopfail_uio_notdir(vnode *vn,uio *uio)

{
  return 0x11;
}



int vopfail_uio_isdir(vnode *vn,uio *uio)

{
  return 0x12;
}



int vopfail_uio_inval(vnode *vn,uio *uio)

{
  return 8;
}



int vopfail_uio_nosys(vnode *vn,uio *uio)

{
  return 1;
}



int vopfail_mmap_isdir(vnode *vn)

{
  return 0x12;
}



int vopfail_mmap_perm(vnode *vn)

{
  return 9;
}



int vopfail_mmap_nosys(vnode *vn)

{
  return 1;
}



int vopfail_truncate_isdir(vnode *vn,off_t pos)

{
  return 0x12;
}



int vopfail_creat_notdir(vnode *vn,char *name,bool excl,mode_t mode,vnode **result)

{
  return 0x11;
}



int vopfail_symlink_notdir(vnode *vn,char *contents,char *name)

{
  return 0x11;
}



int vopfail_symlink_nosys(vnode *vn,char *contents,char *name)

{
  return 1;
}



int vopfail_mkdir_notdir(vnode *vn,char *name,mode_t mode)

{
  return 0x11;
}



int vopfail_mkdir_nosys(vnode *vn,char *name,mode_t mode)

{
  return 1;
}



int vopfail_link_notdir(vnode *dir,char *name,vnode *file)

{
  return 0x11;
}



int vopfail_link_nosys(vnode *dir,char *name,vnode *file)

{
  return 1;
}



int vopfail_string_notdir(vnode *vn,char *name)

{
  return 0x11;
}



int vopfail_string_nosys(vnode *vn,char *name)

{
  return 1;
}



int vopfail_rename_notdir(vnode *fromdir,char *fromname,vnode *todir,char *toname)

{
  return 0x11;
}



int vopfail_rename_nosys(vnode *fromdir,char *fromname,vnode *todir,char *toname)

{
  return 1;
}



int vopfail_lookup_notdir(vnode *vn,char *path,vnode **result)

{
  return 0x11;
}



int vopfail_lookparent_notdir(vnode *vn,char *path,vnode **result,char *buf,size_t len)

{
  return 0x11;
}



char * mkrawname(char *name)

{
  size_t sVar1;
  char *dest;
  
  sVar1 = strlen(name);
  dest = (char *)kmalloc(sVar1 + 4);
  if (dest == (char *)0x0) {
    dest = (char *)0x0;
  }
  else {
    strcpy(dest,name);
    strcat(dest,"raw");
  }
  return dest;
}



vnodearray * vnodearray_create(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  return (vnodearray *)a;
}



void vnodearray_destroy(vnodearray *a)

{
  array_cleanup(&a->arr);
  kfree(a);
  return;
}



void vnodearray_init(vnodearray *a)

{
  array_init(&a->arr);
  return;
}



void vnodearray_cleanup(vnodearray *a)

{
  array_cleanup(&a->arr);
  return;
}



uint vnodearray_num(vnodearray *a)

{
  return (a->arr).num;
}



vnode * vnodearray_get(vnodearray *a,uint index)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    badassert("index < a->num","../../include/array.h",100,"array_get");
    a = (vnodearray *)pcVar1;
  }
  return *(vnode **)(pcVar2 + (int)(a->arr).v);
}



void vnodearray_set(vnodearray *a,uint index,vnode *val)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)(index << 2);
  if ((a->arr).num <= index) {
    pcVar1 = "index < a->num";
    pcVar2 = "../../include/array.h";
    val = (vnode *)0x6b;
    badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    a = (vnodearray *)pcVar1;
  }
  *(vnode **)(pcVar2 + (int)(a->arr).v) = val;
  return;
}



int vnodearray_preallocate(vnodearray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(&a->arr,num);
  return iVar1;
}



int vnodearray_setsize(vnodearray *a,uint num)

{
  int iVar1;
  
  iVar1 = array_setsize(&a->arr,num);
  return iVar1;
}



int vnodearray_add(vnodearray *a,vnode *val,uint *index_ret)

{
  int iVar1;
  uint uVar2;
  
  uVar2 = (a->arr).num;
  iVar1 = array_setsize(&a->arr,uVar2 + 1);
  if (iVar1 == 0) {
    (a->arr).v[uVar2] = val;
    if (index_ret == (uint *)0x0) {
      iVar1 = 0;
    }
    else {
      *index_ret = uVar2;
      iVar1 = 0;
    }
  }
  return iVar1;
}



void vnodearray_remove(vnodearray *a,uint index)

{
  array_remove(&a->arr,index);
  return;
}



void vfs_bootstrap(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  knowndevs = (knowndevarray *)a;
  if (a == (array *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vfs: Could not create knowndevs array\n");
  }
  vfs_biglock = lock_create("vfs_biglock");
  if (vfs_biglock == (lock *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vfs: Could not create vfs big lock\n");
  }
  vfs_biglock_depth = 0;
  devnull_create();
  semfs_bootstrap();
  return;
}



void vfs_biglock_acquire(void)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = lock_do_i_hold(vfs_biglock);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    lock_acquire(vfs_biglock);
  }
  else if (vfs_biglock_depth == 0) {
    lock_acquire(vfs_biglock);
  }
  vfs_biglock_depth = vfs_biglock_depth + 1;
  return;
}



void vfs_biglock_release(void)

{
  undefined3 extraout_var;
  bool bVar3;
  int iVar1;
  uint uVar2;
  
  bVar3 = lock_do_i_hold(vfs_biglock);
  iVar1 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("lock_do_i_hold(vfs_biglock)","../../vfs/vfslist.c",0x9b,"vfs_biglock_release");
  }
  uVar2 = *(int *)(iVar1 + -0x737c) - 1;
  if (*(int *)(iVar1 + -0x737c) == 0) {
    badassert("vfs_biglock_depth > 0","../../vfs/vfslist.c",0x9c,"vfs_biglock_release");
  }
  vfs_biglock_depth = uVar2;
  if (uVar2 == 0) {
    lock_release(vfs_biglock);
  }
  return;
}



bool vfs_biglock_do_i_hold(void)

{
  bool bVar1;
  
  bVar1 = lock_do_i_hold(vfs_biglock);
  return bVar1;
}



int badnames(char *n1,char *n2,char *n3)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar5;
  int iVar2;
  knowndevarray *pkVar3;
  char *pcVar4;
  uint uVar6;
  uint uVar7;
  char **ppcVar8;
  
  bVar5 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x169,"badnames");
  }
  uVar7 = *(uint *)(*(int *)(iVar2 + -0x7374) + 4);
  uVar6 = 0;
  while( true ) {
    if (uVar7 <= uVar6) {
      return 0;
    }
    pkVar3 = knowndevs;
    if ((knowndevs->arr).num <= uVar6) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppcVar8 = (char **)(pkVar3->arr).v[uVar6];
    if (ppcVar8[4] + -1 < (char *)0xfffffffe) {
      pcVar4 = (char *)(**(code **)(*(int *)(ppcVar8[4] + 4) + 4))();
      if (pcVar4 == (char *)0x0) {
        bVar1 = false;
      }
      else if (n1 == (char *)0x0) {
        bVar1 = false;
      }
      else {
        iVar2 = strcmp(pcVar4,n1);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n2);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
        else {
          if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
            iVar2 = strcmp(pcVar4,n3);
            bVar1 = iVar2 == 0;
          }
          if (bVar1) {
            bVar1 = true;
          }
        }
      }
      if (bVar1) {
        return 1;
      }
    }
    pcVar4 = ppcVar8[1];
    bVar1 = false;
    if ((pcVar4 != (char *)0x0) && (n1 != (char *)0x0)) {
      iVar2 = strcmp(pcVar4,n1);
      bVar1 = iVar2 == 0;
    }
    if (bVar1) {
      bVar1 = true;
    }
    else {
      if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
        iVar2 = strcmp(pcVar4,n2);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n3);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
      }
    }
    if (bVar1) break;
    pcVar4 = *ppcVar8;
    if (pcVar4 == (char *)0x0) {
      bVar1 = false;
    }
    else if (n1 == (char *)0x0) {
      bVar1 = false;
    }
    else {
      iVar2 = strcmp(pcVar4,n1);
      bVar1 = iVar2 == 0;
    }
    if (bVar1) {
      bVar1 = true;
    }
    else {
      if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
        iVar2 = strcmp(pcVar4,n2);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n3);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
      }
    }
    uVar6 = uVar6 + 1;
    if (bVar1) {
      return 1;
    }
  }
  return 1;
}



int vfs_doadd(char *dname,int mountable,device *dev,fs *fs)

{
  knowndevarray *pkVar1;
  char *name;
  char *n2;
  char *n3;
  char **ptr;
  vnode *vn;
  int iVar2;
  uint uVar3;
  
  vfs_biglock_acquire();
  name = kstrdup(dname);
  if (name == (char *)0x0) {
    iVar2 = 3;
    vn = (vnode *)0x0;
    ptr = (char **)0x0;
    n2 = (char *)0x0;
  }
  else {
    if (mountable == 0) {
      n2 = (char *)0x0;
    }
    else {
      n2 = mkrawname(name);
      if (n2 == (char *)0x0) {
        iVar2 = 3;
        vn = (vnode *)0x0;
        ptr = (char **)0x0;
        goto fail;
      }
    }
    vn = dev_create_vnode(dev);
    if (vn == (vnode *)0x0) {
      iVar2 = 3;
      ptr = (char **)0x0;
    }
    else {
      ptr = (char **)kmalloc(0x14);
      if (ptr == (char **)0x0) {
        iVar2 = 3;
      }
      else {
        *ptr = name;
        ptr[1] = n2;
        ptr[2] = (char *)dev;
        ptr[3] = (char *)vn;
        ptr[4] = (char *)fs;
        if (fs == (fs *)0x0) {
          n3 = (char *)0x0;
        }
        else {
          n3 = (*fs->fs_ops->fsop_getvolname)(fs);
        }
        iVar2 = badnames(name,n2,n3);
        pkVar1 = knowndevs;
        if (iVar2 == 0) {
          uVar3 = (knowndevs->arr).num;
          iVar2 = array_setsize(&knowndevs->arr,uVar3 + 1);
          if (iVar2 == 0) {
            (pkVar1->arr).v[uVar3] = ptr;
            iVar2 = 0;
          }
          else {
            uVar3 = 0;
          }
          if (iVar2 == 0) {
            if (dev != (device *)0x0) {
              dev->d_devnumber = uVar3 + 1;
            }
            vfs_biglock_release();
            return 0;
          }
        }
        else {
          iVar2 = 0x16;
        }
      }
    }
  }
fail:
  if (name != (char *)0x0) {
    kfree(name);
  }
  if (n2 != (char *)0x0) {
    kfree(n2);
  }
  if (vn != (vnode *)0x0) {
    dev_uncreate_vnode(vn);
  }
  if (ptr != (char **)0x0) {
    kfree(ptr);
  }
  vfs_biglock_release();
  return iVar2;
}



int findmount(char *devname,knowndev **result)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar4;
  int iVar2;
  knowndevarray *pkVar3;
  uint uVar5;
  knowndev *pkVar6;
  uint uVar7;
  
  bVar4 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x200,"findmount");
  }
  uVar7 = *(uint *)(*(int *)(iVar2 + -0x7374) + 4);
  bVar1 = false;
  for (uVar5 = 0; (!bVar1 && (uVar5 < uVar7)); uVar5 = uVar5 + 1) {
    pkVar3 = knowndevs;
    if ((knowndevs->arr).num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pkVar6 = (knowndev *)(pkVar3->arr).v[uVar5];
    if ((pkVar6->kd_rawname != (char *)0x0) && (iVar2 = strcmp(devname,pkVar6->kd_name), iVar2 == 0)
       ) {
      *result = pkVar6;
      bVar1 = true;
    }
  }
  iVar2 = 0;
  if (!bVar1) {
    iVar2 = 0x19;
  }
  return iVar2;
}



int vfs_sync(void)

{
  knowndevarray *pkVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  
  vfs_biglock_acquire();
  uVar4 = (knowndevs->arr).num;
  for (uVar3 = 0; uVar3 < uVar4; uVar3 = uVar3 + 1) {
    pkVar1 = knowndevs;
    if ((knowndevs->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    iVar2 = *(int *)((int)(pkVar1->arr).v[uVar3] + 0x10);
    if (iVar2 - 1U < 0xfffffffe) {
      (***(code ***)(iVar2 + 4))();
    }
  }
  vfs_biglock_release();
  return 0;
}



int vfs_getroot(char *devname,vnode **ret)

{
  undefined3 extraout_var;
  bool bVar3;
  int iVar1;
  knowndevarray *pkVar2;
  char *a;
  char **ppcVar4;
  uint uVar5;
  uint uVar6;
  
  bVar3 = vfs_biglock_do_i_hold();
  iVar1 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0xcb,"vfs_getroot");
  }
  uVar6 = *(uint *)(*(int *)(iVar1 + -0x7374) + 4);
  uVar5 = 0;
  while( true ) {
    if (uVar6 <= uVar5) {
      return 0x19;
    }
    pkVar2 = knowndevs;
    if ((knowndevs->arr).num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppcVar4 = (char **)(pkVar2->arr).v[uVar5];
    if (ppcVar4[4] + -1 < (char *)0xfffffffe) {
      a = (char *)(**(code **)(*(int *)(ppcVar4[4] + 4) + 4))();
      iVar1 = strcmp(*ppcVar4,devname);
      if ((iVar1 == 0) || ((a != (char *)0x0 && (iVar1 = strcmp(a,devname), iVar1 == 0)))) {
        iVar1 = (**(code **)(*(int *)(ppcVar4[4] + 4) + 8))(ppcVar4[4],ret);
        return iVar1;
      }
    }
    else if ((ppcVar4[1] != (char *)0x0) && (iVar1 = strcmp(*ppcVar4,devname), iVar1 == 0)) {
      return 0x1a;
    }
    iVar1 = strcmp(*ppcVar4,devname);
    if (iVar1 == 0) {
      if (ppcVar4[4] != (char *)0x0) {
        badassert("kd->kd_fs==NULL","../../vfs/vfslist.c",0xf0,"vfs_getroot");
      }
      if (ppcVar4[1] != (char *)0x0) {
        badassert("kd->kd_rawname==NULL","../../vfs/vfslist.c",0xf1,"vfs_getroot");
      }
      if (ppcVar4[2] == (char *)0x0) {
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0xf2,"vfs_getroot");
      }
      vnode_incref((vnode *)ppcVar4[3]);
      *ret = (vnode *)ppcVar4[3];
      return 0;
    }
    if ((ppcVar4[1] != (char *)0x0) && (iVar1 = strcmp(ppcVar4[1],devname), iVar1 == 0)) break;
    uVar5 = uVar5 + 1;
  }
  if (ppcVar4[2] == (char *)0x0) {
    badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0xfd,"vfs_getroot");
  }
  vnode_incref((vnode *)ppcVar4[3]);
  *ret = (vnode *)ppcVar4[3];
  return 0;
}



char * vfs_getdevname(fs *fs)

{
  undefined3 extraout_var;
  bool bVar2;
  char *pcVar1;
  knowndevarray *pkVar3;
  char *pcVar4;
  char *pcVar5;
  
  if (fs == (fs *)0x0) {
    badassert("fs != NULL","../../vfs/vfslist.c",0x11a,"vfs_getdevname");
  }
  bVar2 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x11c,"vfs_getdevname");
  }
  pcVar5 = (char *)(knowndevs->arr).num;
  pcVar1 = (char *)0x0;
  pkVar3 = knowndevs;
  do {
    if (pcVar5 <= pcVar1) {
      return (char *)0x0;
    }
    pcVar4 = (char *)((int)pcVar1 << 2);
    if ((char *)(pkVar3->arr).num <= pcVar1) {
      pcVar4 = "index < a->num";
      pcVar5 = "array_get";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar1 = pcVar1 + 1;
  } while ((fs *)(*(char ***)(pcVar4 + (int)(pkVar3->arr).v))[4] != fs);
  return **(char ***)(pcVar4 + (int)(pkVar3->arr).v);
}



int vfs_adddev(char *devname,device *dev,int mountable)

{
  int iVar1;
  
  iVar1 = vfs_doadd(devname,mountable,dev,(fs *)0x0);
  return iVar1;
}



int vfs_addfs(char *devname,fs *fs)

{
  int iVar1;
  
  iVar1 = vfs_doadd(devname,0,(device *)0x0,fs);
  return iVar1;
}



int vfs_mount(char *devname,void *data,anon_subr_int_void_ptr_device_ptr_fs_ptr_ptr *mountfunc)

{
  int iVar1;
  knowndev *pkVar2;
  fs *pfVar3;
  char *pcVar4;
  char *pcVar5;
  knowndev *kd;
  fs *fs;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    if (kd->kd_fs == (fs *)0x0) {
      pkVar2 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x22e,"vfs_mount");
      }
      pcVar5 = (char *)pkVar2->kd_device;
      if ((device *)pcVar5 == (device *)0x0) {
        pcVar4 = "kd->kd_device != NULL";
        pcVar5 = "../../vfs/vfslist.c";
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0x22f,"vfs_mount");
        data = pcVar4;
      }
      iVar1 = (*mountfunc)(data,(device *)pcVar5,&fs);
      if (iVar1 == 0) {
        pfVar3 = (fs *)0xffffffff;
        pcVar5 = (char *)fs;
        if (fs == (fs *)0x0) {
          pcVar5 = "fs != NULL";
          badassert("fs != NULL","../../vfs/vfslist.c",0x237,"vfs_mount");
        }
        if ((fs *)pcVar5 == pfVar3) {
          pcVar5 = "fs != SWAP_FS";
          badassert("fs != SWAP_FS","../../vfs/vfslist.c",0x238,"vfs_mount");
        }
        kd->kd_fs = (fs *)pcVar5;
        pcVar5 = (*(*(fs_ops **)((int)pcVar5 + 4))->fsop_getvolname)((fs *)pcVar5);
        if (pcVar5 == (char *)0x0) {
          pcVar5 = kd->kd_name;
        }
        kprintf("vfs: Mounted %s: on %s\n",pcVar5,kd->kd_name);
        vfs_biglock_release();
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
    else {
      vfs_biglock_release();
      iVar1 = 0x1b;
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}



int vfs_swapon(char *devname,vnode **ret)

{
  size_t sVar1;
  int iVar2;
  knowndev *pkVar3;
  char *ptr;
  knowndev *kd;
  
  sVar1 = strlen(devname);
  ptr = (char *)0x0;
  if (sVar1 != 0) {
    if (devname[sVar1 - 1] == ':') {
      devname = kstrdup(devname);
      if (devname == (char *)0x0) {
        return 3;
      }
      devname[sVar1 - 1] = '\0';
      ptr = devname;
    }
    else {
      ptr = (char *)0x0;
    }
  }
  vfs_biglock_acquire();
  iVar2 = findmount(devname,&kd);
  if (iVar2 == 0) {
    if (kd->kd_fs == (fs *)0x0) {
      pkVar3 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x267,"vfs_swapon");
      }
      if (pkVar3->kd_device == (device *)0x0) {
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0x268,"vfs_swapon");
      }
      kprintf("vfs: Swap attached to %s\n",pkVar3->kd_name);
      kd->kd_fs = (fs *)0xffffffff;
      vnode_incref(kd->kd_vnode);
      *ret = kd->kd_vnode;
    }
    else {
      iVar2 = 0x1b;
    }
  }
  vfs_biglock_release();
  if (ptr != (char *)0x0) {
    kfree(ptr);
  }
  return iVar2;
}



/* WARNING: Removing unreachable block (ram,0x800197dc) */

int vfs_unmount(char *devname)

{
  int iVar1;
  knowndev *pkVar2;
  char *pcVar3;
  char *pcVar4;
  knowndev *kd;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    pcVar3 = (char *)kd->kd_fs;
    if ((undefined *)((int)&((fs *)((int)pcVar3 + -8))->fs_ops + 3U) < (undefined *)0xfffffffe) {
      pcVar4 = (char *)0x80020000;
      pkVar2 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        pcVar3 = "kd->kd_rawname != NULL";
        pcVar4 = "../../vfs/vfslist.c";
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x28e,"vfs_unmount");
      }
      if (pkVar2->kd_device == (device *)0x0) {
        pcVar3 = "kd->kd_device != NULL";
        badassert("kd->kd_device != NULL",pcVar4 + 0x719c,0x28f,"vfs_unmount");
      }
      iVar1 = (*(*(fs_ops **)((int)pcVar3 + 4))->fsop_sync)((fs *)pcVar3);
      if ((iVar1 == 0) && (iVar1 = (*kd->kd_fs->fs_ops->fsop_unmount)(kd->kd_fs), iVar1 == 0)) {
        kprintf("vfs: Unmounted %s:\n",kd->kd_name);
        kd->kd_fs = (fs *)0x0;
      }
    }
    else {
      iVar1 = 8;
    }
  }
  vfs_biglock_release();
  return iVar1;
}



/* WARNING: Removing unreachable block (ram,0x80019874) */

int vfs_swapoff(char *devname)

{
  int iVar1;
  knowndev *kd;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    if (kd->kd_fs == (fs *)0xffffffff) {
      kprintf("vfs: Swap detached from %s:\n",kd->kd_name);
      kd->kd_fs = (fs *)0x0;
    }
    else {
      iVar1 = 8;
    }
  }
  vfs_biglock_release();
  return iVar1;
}



int vfs_unmountall(void)

{
  knowndevarray *pkVar1;
  int iVar2;
  char *pcVar3;
  undefined4 *puVar4;
  uint uVar5;
  uint uVar6;
  undefined4 uVar7;
  
  vfs_biglock_acquire();
  uVar6 = (knowndevs->arr).num;
  uVar5 = 0;
LAB_80019aa8:
  do {
    while( true ) {
      if (uVar6 <= uVar5) {
        vfs_biglock_release();
        return 0;
      }
      pkVar1 = knowndevs;
      if ((knowndevs->arr).num <= uVar5) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      puVar4 = (undefined4 *)(pkVar1->arr).v[uVar5];
      if ((puVar4[1] != 0) && (puVar4[4] != 0)) break;
LAB_80019aa4:
      uVar5 = uVar5 + 1;
    }
    if (puVar4[4] == -1) {
      puVar4[4] = 0;
      goto LAB_80019aa4;
    }
    kprintf("vfs: Unmounting %s:\n",*puVar4);
    iVar2 = (***(code ***)(puVar4[4] + 4))();
    if (iVar2 != 0) {
      uVar7 = *puVar4;
      pcVar3 = strerror(iVar2);
      kprintf("vfs: Warning: sync failed for %s: %s, trying again\n",uVar7,pcVar3);
      iVar2 = (***(code ***)(puVar4[4] + 4))();
      if (iVar2 != 0) {
        uVar7 = *puVar4;
        pcVar3 = strerror(iVar2);
        kprintf("vfs: Warning: sync failed second time for %s: %s, giving up...\n",uVar7,pcVar3);
        uVar5 = uVar5 + 1;
        goto LAB_80019aa8;
      }
    }
    iVar2 = (**(code **)(*(int *)(puVar4[4] + 4) + 0xc))();
    if (iVar2 == 0x1b) {
      kprintf("vfs: Cannot unmount %s: (busy)\n",*puVar4);
      uVar5 = uVar5 + 1;
    }
    else {
      if (iVar2 == 0) {
        puVar4[4] = 0;
        goto LAB_80019aa4;
      }
      uVar7 = *puVar4;
      pcVar3 = strerror(iVar2);
      kprintf("vfs: Warning: unmount failed for %s: %s, already synced, dropping...\n",uVar7,pcVar3)
      ;
      uVar5 = uVar5 + 1;
    }
  } while( true );
}



void change_bootfs(vnode *newvn)

{
  bool bVar1;
  vnode *vn;
  
  vn = bootfs_vnode;
  bVar1 = bootfs_vnode != (vnode *)0x0;
  bootfs_vnode = newvn;
  if (bVar1) {
    vnode_decref(vn);
  }
  return;
}



int getdevice(char *path,char **subpath,vnode **startvn)

{
  char cVar1;
  undefined3 extraout_var;
  bool bVar4;
  int iVar2;
  int iVar3;
  int iVar5;
  fs *pfVar6;
  int iVar7;
  vnode *vn;
  
  bVar4 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslookup.c",0x88,"getdevice");
  }
  iVar7 = (int)*path;
  if (iVar7 == 0) {
    iVar7 = 8;
  }
  else {
    for (iVar2 = 0; cVar1 = path[iVar2], cVar1 != '\0'; iVar2 = iVar2 + 1) {
      if (cVar1 == ':') {
        iVar5 = -1;
        iVar3 = iVar2;
        goto LAB_80019bc8;
      }
      if (cVar1 == '/') {
        iVar3 = -1;
        iVar5 = iVar2;
        goto LAB_80019bc8;
      }
    }
    iVar3 = -1;
    iVar5 = -1;
LAB_80019bc8:
    if ((iVar3 < 0) && (iVar5 != 0)) {
      *subpath = path;
      iVar7 = vfs_getcurdir(startvn);
    }
    else if (iVar3 < 1) {
      iVar2 = 0x2f;
      if ((iVar3 != 0) && (iVar5 != 0)) {
        iVar7 = 0xc3;
        badassert("colon==0 || slash==0","../../vfs/vfslookup.c",0xc3,"getdevice");
      }
      if (iVar7 == iVar2) {
        if (bootfs_vnode == (vnode *)0x0) {
          return 0x13;
        }
        vnode_incref(bootfs_vnode);
        *startvn = bootfs_vnode;
      }
      else {
        if (iVar7 != 0x3a) {
          badassert("path[0]==\':\'","../../vfs/vfslookup.c",0xcd,"getdevice");
        }
        iVar7 = vfs_getcurdir(&vn);
        if (iVar7 != 0) {
          return iVar7;
        }
        pfVar6 = vn->vn_fs;
        if (pfVar6 == (fs *)0x0) {
          pfVar6 = (fs *)&DAT_800273d8;
          badassert("vn->vn_fs!=NULL","../../vfs/vfslookup.c",0xd8,"getdevice");
        }
        iVar7 = (*pfVar6->fs_ops->fsop_getroot)(pfVar6,startvn);
        vnode_decref(vn);
        if (iVar7 != 0) {
          return iVar7;
        }
      }
      for (; iVar7 = 0, path[1] == '/'; path = path + 1) {
      }
      *subpath = path + 1;
    }
    else {
      path[iVar3] = '\0';
      do {
        iVar7 = iVar3 + 1;
        iVar3 = iVar3 + 1;
      } while (path[iVar7] == '/');
      *subpath = path + iVar7;
      iVar7 = vfs_getroot(path,startvn);
    }
  }
  return iVar7;
}



int vfs_setbootfs(char *fsname)

{
  char *str;
  size_t sVar1;
  int iVar2;
  char tmp [256];
  vnode *newguy;
  
  vfs_biglock_acquire();
  snprintf(tmp,0xff,"%s",fsname);
  str = strchr(tmp,0x3a);
  if (str == (char *)0x0) {
    strcat(tmp,":");
  }
  else {
    sVar1 = strlen(str);
    if (sVar1 != 0) {
      vfs_biglock_release();
      return 8;
    }
  }
  iVar2 = vfs_chdir(tmp);
  if (iVar2 == 0) {
    iVar2 = vfs_getcurdir(&newguy);
    if (iVar2 == 0) {
      change_bootfs(newguy);
      vfs_biglock_release();
      iVar2 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar2;
}



void vfs_clearbootfs(void)

{
  vfs_biglock_acquire();
  change_bootfs((vnode *)0x0);
  vfs_biglock_release();
  return;
}



int vfs_lookparent(char *path,vnode **retval,char *buf,size_t buflen)

{
  int iVar1;
  size_t sVar2;
  char *local_res0;
  vnode *startvn;
  
  local_res0 = path;
  vfs_biglock_acquire();
  iVar1 = getdevice(local_res0,(char **)register0x00000074,&startvn);
  if (iVar1 == 0) {
    sVar2 = strlen(local_res0);
    if (sVar2 == 0) {
      iVar1 = 8;
    }
    else {
      vnode_check(startvn,"lookparent");
      iVar1 = (*startvn->vn_ops->vop_lookparent)(startvn,local_res0,retval,buf,buflen);
    }
    vnode_decref(startvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}



int vfs_lookup(char *path,vnode **retval)

{
  int iVar1;
  size_t sVar2;
  char *local_res0;
  vnode *startvn;
  
  local_res0 = path;
  vfs_biglock_acquire();
  iVar1 = getdevice(local_res0,(char **)register0x00000074,&startvn);
  if (iVar1 == 0) {
    sVar2 = strlen(local_res0);
    if (sVar2 == 0) {
      *retval = startvn;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vnode_check(startvn,"lookup");
      iVar1 = (*startvn->vn_ops->vop_lookup)(startvn,local_res0,retval);
      vnode_decref(startvn);
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}



/* WARNING: Removing unreachable block (ram,0x8001a1e8) */

int vfs_open(char *path,int openflags,mode_t mode,vnode **ret)

{
  bool bVar1;
  int iVar2;
  char *v;
  vnode **in_stack_fffffed8;
  undefined4 in_stack_fffffedc;
  vnode *vn;
  char name [256];
  vnode *dir;
  
  vn = (vnode *)0x0;
  if ((openflags & 3U) == 0) {
    bVar1 = false;
  }
  else {
    bVar1 = true;
    if (2 < (openflags & 3U)) {
      return 8;
    }
  }
  if ((openflags & 4U) == 0) {
    iVar2 = vfs_lookup(path,&vn);
  }
  else {
    iVar2 = vfs_lookparent(path,&dir,name,0x100);
    if (iVar2 != 0) {
      return iVar2;
    }
    vnode_check(dir,"creat");
    in_stack_fffffed8 = &vn;
    iVar2 = (*dir->vn_ops->vop_creat)(dir,name,(openflags & 8U) != 0,mode,in_stack_fffffed8);
    vnode_decref(dir);
  }
  if (iVar2 == 0) {
    v = (char *)vn;
    if (vn == (vnode *)0x0) {
      v = s_vn____NULL_80027408;
      badassert(s_vn____NULL_80027408,"../../vfs/vfspath.c",0x58,"vfs_open");
    }
    vnode_check((vnode *)v,"eachopen");
    iVar2 = (*vn->vn_ops->vop_eachopen)(vn,openflags);
    if (iVar2 == 0) {
      if ((openflags & 0x10U) != 0) {
        if (bVar1) {
          vnode_check(vn,"truncate");
          iVar2 = (*vn->vn_ops->vop_truncate)(vn,CONCAT44(in_stack_fffffed8,in_stack_fffffedc));
        }
        else {
          iVar2 = 8;
        }
        if (iVar2 != 0) {
          vnode_decref(vn);
          return iVar2;
        }
      }
      *ret = vn;
      iVar2 = 0;
    }
    else {
      vnode_decref(vn);
    }
  }
  return iVar2;
}



void vfs_close(vnode *vn)

{
  vnode_decref(vn);
  return;
}



int vfs_remove(char *path)

{
  int iVar1;
  vnode *dir;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&dir,name,0x100);
  if (iVar1 == 0) {
    vnode_check(dir,"remove");
    iVar1 = (*dir->vn_ops->vop_remove)(dir,name);
    vnode_decref(dir);
  }
  return iVar1;
}



int vfs_rename(char *oldpath,char *newpath)

{
  int iVar1;
  vnode *olddir;
  char oldname [256];
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookparent(oldpath,&olddir,oldname,0x100);
  if (iVar1 == 0) {
    iVar1 = vfs_lookparent(newpath,&newdir,newname,0x100);
    if (iVar1 == 0) {
      if (((olddir->vn_fs == (fs *)0x0) || (newdir->vn_fs == (fs *)0x0)) ||
         (olddir->vn_fs != newdir->vn_fs)) {
        vnode_decref(newdir);
        vnode_decref(olddir);
        iVar1 = 0x18;
      }
      else {
        vnode_check(olddir,"rename");
        iVar1 = (*olddir->vn_ops->vop_rename)(olddir,oldname,newdir,newname);
        vnode_decref(newdir);
        vnode_decref(olddir);
      }
    }
    else {
      vnode_decref(olddir);
    }
  }
  return iVar1;
}



int vfs_link(char *oldpath,char *newpath)

{
  int iVar1;
  vnode *oldfile;
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookup(oldpath,&oldfile);
  if (iVar1 == 0) {
    iVar1 = vfs_lookparent(newpath,&newdir,newname,0x100);
    if (iVar1 == 0) {
      if (((oldfile->vn_fs == (fs *)0x0) || (newdir->vn_fs == (fs *)0x0)) ||
         (oldfile->vn_fs != newdir->vn_fs)) {
        vnode_decref(newdir);
        vnode_decref(oldfile);
        iVar1 = 0x18;
      }
      else {
        vnode_check(newdir,"link");
        iVar1 = (*newdir->vn_ops->vop_link)(newdir,newname,oldfile);
        vnode_decref(newdir);
        vnode_decref(oldfile);
      }
    }
    else {
      vnode_decref(oldfile);
    }
  }
  return iVar1;
}



int vfs_symlink(char *contents,char *path)

{
  int iVar1;
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookparent(path,&newdir,newname,0x100);
  if (iVar1 == 0) {
    vnode_check(newdir,"symlink");
    iVar1 = (*newdir->vn_ops->vop_symlink)(newdir,newname,contents);
    vnode_decref(newdir);
  }
  return iVar1;
}



int vfs_readlink(char *path,uio *uio)

{
  int iVar1;
  vnode *vn;
  
  iVar1 = vfs_lookup(path,&vn);
  if (iVar1 == 0) {
    vnode_check(vn,"readlink");
    iVar1 = (*vn->vn_ops->vop_readlink)(vn,uio);
    vnode_decref(vn);
  }
  return iVar1;
}



int vfs_mkdir(char *path,mode_t mode)

{
  int iVar1;
  vnode *parent;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&parent,name,0x100);
  if (iVar1 == 0) {
    vnode_check(parent,"mkdir");
    iVar1 = (*parent->vn_ops->vop_mkdir)(parent,name,mode);
    vnode_decref(parent);
  }
  return iVar1;
}



int vfs_rmdir(char *path)

{
  int iVar1;
  vnode *parent;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&parent,name,0x100);
  if (iVar1 == 0) {
    vnode_check(parent,"rmdir");
    iVar1 = (*parent->vn_ops->vop_rmdir)(parent,name);
    vnode_decref(parent);
  }
  return iVar1;
}



int vnode_init(vnode *vn,vnode_ops *ops,fs *fs,void *fsdata)

{
  char *pcVar1;
  char *pcVar2;
  char *pcVar3;
  
  pcVar1 = (char *)vn;
  pcVar2 = (char *)ops;
  if (vn == (vnode *)0x0) {
    pcVar1 = s_vn____NULL_80027408;
    pcVar2 = "../../vfs/vnode.c";
    fs = (fs *)0x2f;
    pcVar3 = "vnode_init";
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",0x2f,"vnode_init");
    fsdata = pcVar3;
  }
  if ((vnode_ops *)pcVar2 == (vnode_ops *)0x0) {
    pcVar1 = s_ops____NULL_80027488;
    pcVar2 = "../../vfs/vnode.c";
    badassert(s_ops____NULL_80027488,"../../vfs/vnode.c",0x30,"vnode_init");
  }
  *(char **)((int)pcVar1 + 0x14) = pcVar2;
  *(int *)pcVar1 = 1;
  spinlock_init((spinlock *)((int)pcVar1 + 4));
  vn->vn_fs = fs;
  vn->vn_data = fsdata;
  return 0;
}



void vnode_cleanup(vnode *vn)

{
  char *pcVar1;
  
  pcVar1 = (char *)vn;
  if (vn->vn_refcount != 1) {
    pcVar1 = "vn->vn_refcount == 1";
    badassert("vn->vn_refcount == 1","../../vfs/vnode.c",0x40,"vnode_cleanup");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 4));
  vn->vn_ops = (vnode_ops *)0x0;
  vn->vn_refcount = 0;
  vn->vn_fs = (fs *)0x0;
  vn->vn_data = (void *)0x0;
  return;
}



void vnode_incref(vnode *vn)

{
  char *pcVar1;
  
  pcVar1 = (char *)vn;
  if (vn == (vnode *)0x0) {
    pcVar1 = s_vn____NULL_80027408;
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",0x52,"vnode_incref");
  }
  spinlock_acquire((spinlock *)((int)pcVar1 + 4));
  vn->vn_refcount = vn->vn_refcount + 1;
  spinlock_release((spinlock *)((int)pcVar1 + 4));
  return;
}



void vnode_check(vnode *v,char *opstr)

{
  vnode_ops *pvVar1;
  int iVar2;
  
  if (v == (vnode *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: null vnode\n");
  }
  if (v == (vnode *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef vnode\n");
  }
  pvVar1 = v->vn_ops;
  if (pvVar1 == (vnode_ops *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: null ops pointer\n");
  }
  if (pvVar1 == (vnode_ops *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef ops pointer\n");
  }
  if (pvVar1->vop_magic != 0xa2b3c4d5) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: ops with bad magic number %lx\n");
  }
  if (v->vn_fs == (fs *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef fs pointer\n");
  }
  spinlock_acquire(&v->vn_countlock);
  iVar2 = v->vn_refcount;
  if (iVar2 < 0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: negative refcount %d\n",opstr);
  }
  if (iVar2 == 0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: zero refcount\n",opstr);
  }
  if (0x100000 < iVar2) {
    kprintf("vnode_check: vop_%s: warning: large refcount %d\n",opstr);
  }
  spinlock_release(&v->vn_countlock);
  return;
}



void vnode_decref(vnode *vn)

{
  int iVar1;
  uint uVar2;
  char *pcVar3;
  
  pcVar3 = (char *)vn;
  if (vn == (vnode *)0x0) {
    pcVar3 = s_vn____NULL_80027408;
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",100,"vnode_decref");
  }
  spinlock_acquire((spinlock *)((int)pcVar3 + 4));
  iVar1 = vn->vn_refcount;
  uVar2 = (uint)(iVar1 < 2);
  if (iVar1 < 1) {
    badassert("vn->vn_refcount > 0","../../vfs/vnode.c",0x68,"vnode_decref");
  }
  if (uVar2 == 0) {
    vn->vn_refcount = iVar1 + -1;
  }
  spinlock_release((spinlock *)((int)pcVar3 + 4));
  if (uVar2 != 0) {
    vnode_check(vn,"reclaim");
    iVar1 = (*vn->vn_ops->vop_reclaim)(vn);
    if ((iVar1 != 0) && (iVar1 != 0x1b)) {
      pcVar3 = strerror(iVar1);
      kprintf("vfs: Warning: VOP_RECLAIM: %s\n",pcVar3);
    }
  }
  return;
}



void vm_can_sleep(void)

{
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) != 0) {
      badassert("curcpu->c_spinlocks == 0",s_______vm_addrspace_c_80027688,0x6d,"vm_can_sleep");
    }
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      badassert("curthread->t_in_interrupt == 0",s_______vm_addrspace_c_80027688,0x70,"vm_can_sleep"
               );
    }
  }
  return;
}



int isTableActive(void)

{
  int iVar1;
  
  spinlock_acquire(&freemem_lock);
  iVar1 = allocTableActive;
  spinlock_release(&freemem_lock);
  return iVar1;
}



vaddr_t alloc_kpages(uint npages)

{
  paddr_t pVar1;
  vaddr_t vVar2;
  uint uVar3;
  
  vm_can_sleep();
  pVar1 = getppages(npages,1);
  if (pVar1 == 0) {
    vVar2 = 0;
  }
  else {
    for (uVar3 = 0; uVar3 < npages; uVar3 = uVar3 + 1) {
      ipt_kadd(-2,uVar3 * 0x1000 + pVar1,0);
    }
    vVar2 = pVar1 + 0x80000000;
  }
  return vVar2;
}



void free_kpages(vaddr_t addr)

{
  int iVar1;
  char *first_page;
  int iVar2;
  uint addr_00;
  
  iVar1 = isTableActive();
  if (iVar1 != 0) {
    addr_00 = addr + 0x80000000;
    first_page = (char *)(addr_00 >> 0xc);
    if (nRamFrames <= (int)first_page) {
      first_page = s_______vm_addrspace_c_80027688;
      badassert("nRamFrames > first",s_______vm_addrspace_c_80027688,0x8f,"free_kpages");
    }
    iVar1 = freeppages(addr_00,(long)first_page);
    for (iVar2 = 0; iVar2 < iVar1; iVar2 = iVar2 + 1) {
      ipt_kadd(-1,iVar2 * 0x1000 + addr_00,0);
    }
  }
  return;
}



void vm_bootstrap(void)

{
  paddr_t pVar1;
  int iVar2;
  vaddr_t addr;
  
  pVar1 = ram_getsize();
  if ((int)pVar1 < 0) {
    pVar1 = pVar1 + 0xfff;
  }
  nRamFrames = (int)pVar1 >> 0xc;
  pVar1 = ram_getsize();
  kprintf("ram_getsize(): %d\n",pVar1);
  kprintf("RamFrames: %d\n",nRamFrames);
  iVar2 = create_ipt();
  if ((iVar2 != -1) && (iVar2 = init_freeRamFrames(nRamFrames), iVar2 == 0)) {
    iVar2 = init_allocSize(nRamFrames);
    if (iVar2 == 0) {
      spinlock_acquire(&freemem_lock);
      allocTableActive = 1;
      spinlock_release(&freemem_lock);
      pVar1 = ram_getfirstfreeafterbootstrap();
      if ((int)pVar1 < 0) {
        pVar1 = pVar1 + 0xfff;
      }
      addr = alloc_kpages(nRamFrames - ((int)pVar1 >> 0xc));
      free_kpages(addr);
      init_instrumentation();
    }
    else {
      destroy_freeRamFrames();
    }
  }
  return;
}



addrspace * as_create(void)

{
  addrspace *paVar1;
  
  paVar1 = (addrspace *)kmalloc(0x10);
  if (paVar1 == (addrspace *)0x0) {
    paVar1 = (addrspace *)0x0;
  }
  else {
    paVar1->as_vbase1 = 0;
    paVar1->as_vbase2 = 0;
  }
  return paVar1;
}



void as_destroy(addrspace *as)

{
  char *pcVar1;
  
  if (as == (addrspace *)0x0) {
    pcVar1 = "as != NULL";
    badassert("as != NULL",s_______vm_addrspace_c_80027688,0xf4,"as_destroy");
    as = (addrspace *)pcVar1;
  }
  kfree(as);
  return;
}



void as_activate(void)

{
  addrspace *paVar1;
  int spl;
  int iVar2;
  
  paVar1 = proc_getas();
  if (paVar1 != (addrspace *)0x0) {
    spl = splx(1);
    for (iVar2 = 0; iVar2 < 0x40; iVar2 = iVar2 + 1) {
      tlb_write((iVar2 + 0x80000) * 0x1000,0,iVar2);
    }
    splx(spl);
    increase(3);
  }
  return;
}



void as_deactivate(void)

{
  return;
}



int as_define_region(addrspace *as,vaddr_t vaddr,size_t sz,int readable,int writeable,int executable
                    )

{
  vaddr_t vVar1;
  uint uVar2;
  
  vm_can_sleep();
  vVar1 = as->as_vbase1;
  uVar2 = (vaddr & 0xfff) + sz + 0xfff >> 0xc;
  if (vVar1 == 0) {
    as->as_vbase1 = vaddr & 0xfffff000;
    as->as_npages1 = uVar2;
  }
  else {
    vVar1 = as->as_vbase2;
    if (vVar1 == 0) {
      as->as_vbase2 = vaddr & 0xfffff000;
      as->as_npages2 = uVar2;
    }
    else {
      kprintf("dumbvm: Warning: too many regions\n");
      vVar1 = 1;
    }
  }
  return vVar1;
}



int as_prepare_load(uint npages)

{
  paddr_t pVar1;
  
  pVar1 = getppages(npages,0);
  return pVar1;
}



int as_copy(addrspace *old,addrspace **ret,pid_t old_pid,pid_t new_pid)

{
  addrspace *paVar1;
  vaddr_t vVar2;
  paddr_t pVar3;
  int iVar4;
  paddr_t pVar5;
  
  paVar1 = as_create();
  if (paVar1 == (addrspace *)0x0) {
    iVar4 = 3;
  }
  else {
    if (old == (addrspace *)0x0) {
      badassert("old != NULL",s_______vm_addrspace_c_80027688,0xb7,"as_copy");
    }
    vVar2 = old->as_vbase1;
    if (vVar2 == 0) {
      badassert("old->as_vbase1 != 0",s_______vm_addrspace_c_80027688,0xb8,"as_copy");
    }
    if (old->as_npages1 == 0) {
      badassert("old->as_npages1 > 0",s_______vm_addrspace_c_80027688,0xb9,"as_copy");
    }
    if (old->as_vbase2 == 0) {
      badassert("old->as_vbase2 != 0",s_______vm_addrspace_c_80027688,0xba,"as_copy");
    }
    if (old->as_npages2 == 0) {
      badassert("old->as_npages2 > 0",s_______vm_addrspace_c_80027688,0xbb,"as_copy");
    }
    paVar1->as_vbase1 = vVar2;
    paVar1->as_npages1 = old->as_npages1;
    paVar1->as_vbase2 = old->as_vbase2;
    paVar1->as_npages2 = old->as_npages2;
    for (iVar4 = 0; iVar4 < (int)paVar1->as_npages2; iVar4 = iVar4 + 1) {
      pVar5 = ipt_lookup(old_pid,paVar1->as_vbase2 + iVar4 * 0x1000);
      if (pVar5 != 0) {
        pVar3 = as_prepare_load(1);
        memmove((void *)(pVar3 + 0x80000000),(void *)(pVar5 + 0x80000000),0x1000);
        ipt_add(new_pid,pVar3,iVar4 * 0x1000 + paVar1->as_vbase2);
      }
    }
    pVar5 = 1;
    iVar4 = 0;
    while (pVar5 != 0) {
      vVar2 = (0x7ffff - iVar4) * 0x1000;
      pVar5 = ipt_lookup(old_pid,vVar2);
      if (pVar5 != 0) {
        pVar3 = as_prepare_load(1);
        memmove((void *)(pVar3 + 0x80000000),(void *)(pVar5 + 0x80000000),0x1000);
        ipt_add(new_pid,pVar3,vVar2);
      }
      iVar4 = iVar4 + 1;
    }
    duplicate_swap_pages(old_pid,new_pid);
    *ret = paVar1;
    iVar4 = 0;
  }
  return iVar4;
}



int as_complete_load(addrspace *as)

{
  vm_can_sleep();
  return 0;
}



int as_define_stack(addrspace *as,vaddr_t *stackptr)

{
  char *pcVar1;
  
  if (as == (addrspace *)0x0) {
    pcVar1 = s_______vm_addrspace_c_80027688;
    badassert("as != NULL",s_______vm_addrspace_c_80027688,0x158,"as_define_stack");
    stackptr = (vaddr_t *)pcVar1;
  }
  *stackptr = 0x80000000;
  return 0;
}



void vm_shutdown(void)

{
  print_statistics();
  return;
}



void SetBit(int *A,int k)

{
  int iVar1;
  
  iVar1 = k;
  if (k < 0) {
    iVar1 = k + 0x1f;
  }
  A[iVar1 >> 5] = A[iVar1 >> 5] | 1 << (k & 0x1fU);
  return;
}



void ClearBit(int *A,int k)

{
  int iVar1;
  
  iVar1 = k;
  if (k < 0) {
    iVar1 = k + 0x1f;
  }
  A[iVar1 >> 5] = A[iVar1 >> 5] & ~(1 << (k & 0x1fU));
  return;
}



int TestBit(int *A,int k)

{
  int iVar1;
  uint uVar2;
  
  iVar1 = k;
  if (k < 0) {
    iVar1 = k + 0x1f;
  }
  uVar2 = A[iVar1 >> 5] >> (k & 0x1fU) & 1;
  if (uVar2 != 0) {
    uVar2 = 1;
  }
  return uVar2;
}



paddr_t getfreeppages(ulong npages)

{
  int *A;
  int iVar1;
  paddr_t pVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  
  iVar1 = isTableActive();
  pVar2 = 0;
  if (iVar1 != 0) {
    spinlock_acquire(&freemem_lock);
    iVar1 = -1;
    for (iVar5 = 0; A = freeRamFrames, iVar5 < nRamFrames; iVar5 = iVar5 + 1) {
      iVar3 = TestBit(freeRamFrames,iVar5);
      if (iVar3 != 0) {
        iVar3 = TestBit(A,iVar5 + -1);
        if ((iVar5 == 0) || (iVar4 = iVar5 - iVar1, iVar3 == 0)) {
          iVar4 = 0;
          iVar1 = iVar5;
        }
        if ((int)npages <= iVar4 + 1) goto LAB_8001b37c;
      }
    }
    iVar1 = -1;
LAB_8001b37c:
    if (iVar1 < 0) {
      pVar2 = 0;
    }
    else {
      for (iVar5 = iVar1; iVar5 < (int)(iVar1 + npages); iVar5 = iVar5 + 1) {
        ClearBit(freeRamFrames,iVar5);
      }
      allocSize[iVar1] = npages;
      pVar2 = iVar1 << 0xc;
    }
    spinlock_release(&freemem_lock);
  }
  return pVar2;
}



void as_zero_region(paddr_t paddr,uint npages)

{
  bzero((void *)(paddr + 0x80000000),npages << 0xc);
  return;
}



void print_freeRamFrames(void)

{
  int iVar1;
  int k;
  
  kprintf("Frame Status\n");
  for (k = 0; k < nRamFrames; k = k + 1) {
    iVar1 = TestBit(freeRamFrames,k);
    kprintf("  %d    %d\n",k,iVar1);
  }
  return;
}



int init_freeRamFrames(int ramFrames)

{
  uint uVar1;
  int k;
  
  nRamFrames = ramFrames;
  freeRamFrames = (int *)kmalloc((((uint)ramFrames >> 2) + 1) * 4);
  if (freeRamFrames == (int *)0x0) {
    uVar1 = 1;
  }
  else {
    for (k = 0; uVar1 = (uint)(k < nRamFrames), uVar1 != 0; k = k + 1) {
      ClearBit(freeRamFrames,k);
    }
  }
  return uVar1;
}



void destroy_freeRamFrames(void)

{
  freeRamFrames = (int *)0x0;
  return;
}



int init_allocSize(int ramFrames)

{
  ulong *puVar1;
  int iVar2;
  
  iVar2 = nRamFrames;
  if (ramFrames != nRamFrames) {
    badassert("ramFrames == nRamFrames","../../vm/coremap.c",0x56,"init_allocSize");
  }
  puVar1 = (ulong *)kmalloc(iVar2 << 2);
  allocSize = puVar1;
  if (puVar1 == (ulong *)0x0) {
    iVar2 = 1;
  }
  else {
    for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
      puVar1[iVar2] = 0;
    }
    iVar2 = 0;
  }
  return iVar2;
}



paddr_t getppages(ulong npages,int kmem)

{
  char *paddr;
  int iVar1;
  addrspace *as;
  char *paddr_00;
  vaddr_t vaddr;
  pid_t pid_victim;
  
  paddr = (char *)getfreeppages(npages);
  if (paddr == (char *)0x0) {
    spinlock_acquire(&stealmem_lock);
    paddr = (char *)ram_stealmem(npages);
    spinlock_release(&stealmem_lock);
  }
  if ((paddr != (char *)0x0) && (iVar1 = isTableActive(), iVar1 != 0)) {
    spinlock_acquire(&freemem_lock);
    allocSize[(uint)paddr >> 0xc] = npages;
    spinlock_release(&freemem_lock);
  }
  paddr_00 = paddr;
  if (paddr == (char *)0x0) {
    iVar1 = isTableActive();
    if (iVar1 != 0) {
      spinlock_acquire(&freemem_lock);
      if (kmem == 0) {
        if (npages != 1) {
          badassert("npages == 1","../../vm/coremap.c",0xba,"getppages");
        }
        pid_victim = 0;
      }
      else {
        pid_victim = 1;
      }
      if ((kmem != 0) && (npages != 1)) {
                    /* WARNING: Subroutine does not return */
        panic("No contiguous %ld free ram frames for kernel allocation",npages);
      }
      if (kmem == 0) {
        paddr = (char *)get_victim(&vaddr,&pid_victim);
      }
      if (paddr == (char *)0x0) {
                    /* WARNING: Subroutine does not return */
        panic(
             "It was not possible to find a page victim.\nAre you allocating more kernel memory than the available ram size?"
             );
      }
      as = pid_getas(pid_victim);
      iVar1 = address_segment(vaddr,as);
      spinlock_release(&freemem_lock);
      iVar1 = swap_out((paddr_t)paddr,vaddr,iVar1,pid_victim);
      if (iVar1 != 0) {
        return 0;
      }
    }
    paddr_00 = paddr;
    if (paddr == (char *)0x0) {
      paddr_00 = "paddr != 0";
      badassert("paddr != 0","../../vm/coremap.c",0xdd,"getppages");
    }
  }
  as_zero_region((paddr_t)paddr_00,npages);
  return (paddr_t)paddr;
}



int freeppages(paddr_t addr,long first_page)

{
  ulong uVar1;
  uint k;
  int iVar2;
  ulong uVar3;
  
  uVar3 = allocSize[first_page];
  uVar1 = isTableActive();
  if (uVar1 != 0) {
    k = addr >> 0xc;
    if (allocSize == (ulong *)0x0) {
      badassert("allocSize != NULL","../../vm/coremap.c",0xeb,"freeppages");
    }
    if (nRamFrames <= (int)k) {
      badassert("nRamFrames > first","../../vm/coremap.c",0xec,"freeppages");
    }
    spinlock_acquire(&freemem_lock);
    iVar2 = k + uVar3;
    for (; (int)k < iVar2; k = k + 1) {
      SetBit(freeRamFrames,k);
    }
    spinlock_release(&freemem_lock);
    uVar1 = uVar3;
  }
  return uVar1;
}



void init_instrumentation(void)

{
  swap_in_pages = 0;
  swap_out_pages = 0;
  faults_with_elf_load = 0;
  faults_with_load = 0;
  new_pages_zeroed = 0;
  tlb_reloads = 0;
  tlb_invalidations = 0;
  tlb_misses_full = 0;
  tlb_misses_free = 0;
  tlb_misses = 0;
  return;
}



void increase(long indicator)

{
  spinlock_acquire(&instr_lock);
  switch(indicator) {
  case 0:
    tlb_misses = tlb_misses + 1;
    break;
  case 1:
    tlb_misses_free = tlb_misses_free + 1;
    break;
  case 2:
    tlb_misses_full = tlb_misses_full + 1;
    break;
  case 3:
    tlb_invalidations = tlb_invalidations + 1;
    break;
  case 4:
    tlb_reloads = tlb_reloads + 1;
    break;
  case 5:
    faults_with_load = faults_with_load + 1;
    break;
  case 6:
    faults_with_elf_load = faults_with_elf_load + 1;
    break;
  case 7:
    swap_out_pages = swap_out_pages + 1;
    break;
  case 8:
    swap_in_pages = swap_in_pages + 1;
    break;
  case 9:
    new_pages_zeroed = new_pages_zeroed + 1;
  }
  spinlock_release(&instr_lock);
  return;
}



void print_statistics(void)

{
  bool bVar1;
  bool bVar2;
  bool bVar3;
  
  kprintf("\n\nVirtual memory statistics:\n\n");
  kprintf("----------------------------------------\n");
  kprintf("TLB Faults: %ld                         \n",tlb_misses);
  kprintf("----------------------------------------\n");
  kprintf("TLB Faults with Free: %ld               \n",tlb_misses_free);
  kprintf("----------------------------------------\n");
  kprintf("TLB Faults with Replace: %ld            \n",tlb_misses_full);
  kprintf("----------------------------------------\n");
  kprintf("TLB Invalidations: %ld                  \n",tlb_invalidations);
  kprintf("----------------------------------------\n");
  kprintf("TLB Reloads: %ld                        \n",tlb_reloads);
  kprintf("----------------------------------------\n");
  kprintf("Page Faults (Zeroed): %ld               \n",new_pages_zeroed);
  kprintf("----------------------------------------\n");
  kprintf("Page Faults (Disk): %ld                 \n",faults_with_load);
  kprintf("----------------------------------------\n");
  kprintf("Page Faults from ELF: %ld               \n",faults_with_elf_load);
  kprintf("----------------------------------------\n");
  kprintf("Page Faults from Swapfile: %ld          \n",swap_in_pages);
  kprintf("----------------------------------------\n");
  kprintf("Swapfile Writes: %ld                    \n",swap_out_pages);
  kprintf("----------------------------------------\n\n");
  bVar1 = tlb_misses_free + tlb_misses_full == tlb_misses;
  if (!bVar1) {
    kprintf("\nWarning: TLB Faults with Free + TLB Faults with Replace != TLB Faults\n");
  }
  bVar2 = tlb_reloads + faults_with_load + new_pages_zeroed != tlb_misses;
  if (bVar2) {
    kprintf("\nWarning: TLB Reloads + Page Faults (Disk) + Page Faults (Zeroed) != TLB Faults \n");
  }
  bVar3 = faults_with_elf_load + swap_in_pages != faults_with_load;
  if (bVar3) {
    kprintf("\nWarning: Page Faults from ELF %ld + Swapfile Writes %ld != Page Faults (Disk) %ld\n",
            faults_with_elf_load,swap_out_pages);
  }
  if (!bVar3 && (!bVar2 && bVar1)) {
    kprintf("All sums are correct.\n\n");
  }
  return;
}



void fill_deadbeef(void *vptr,size_t len)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < len >> 2; uVar1 = uVar1 + 1) {
    *(undefined4 *)((int)vptr + uVar1 * 4) = 0xdeadbeef;
  }
  return;
}



void subpage_stats(pageref *pr)

{
  undefined3 extraout_var;
  bool bVar4;
  uint uVar1;
  int iVar2;
  undefined4 *puVar3;
  int in_v1;
  uint uVar5;
  char *pcVar6;
  char *pcVar7;
  undefined4 uVar8;
  uint uVar9;
  uint32_t freemap [8];
  
  bVar4 = spinlock_do_i_hold(&kmalloc_spinlock);
  uVar1 = CONCAT31(extraout_var,bVar4);
  if (uVar1 == 0) {
    badassert("spinlock_do_i_hold(&kmalloc_spinlock)","../../vm/kmalloc.c",0x2f4,"subpage_stats");
    do {
      *(undefined4 *)((int)freemap + in_v1) = 0;
      uVar1 = uVar1 + 1;
LAB_8001bd38:
      in_v1 = uVar1 << 2;
    } while (uVar1 < 8);
    pcVar7 = (char *)(pr->pageaddr_and_blocktype & 0xfffff000);
    uVar1 = pr->pageaddr_and_blocktype & 0xfff;
    iVar2 = uVar1 << 2;
    if (7 < uVar1) {
      pcVar7 = "../../vm/kmalloc.c";
      badassert("blktype >= 0 && blktype < NSIZES","../../vm/kmalloc.c",0x2fd,"subpage_stats");
    }
    uVar1 = *(uint *)((int)sizes + iVar2);
    uVar9 = 0x1000 / uVar1;
    if (uVar1 == 0) {
      trap(0x1c00);
    }
    uVar5 = 0xffff;
    if (0x100 < uVar9) {
      pcVar7 = "../../vm/kmalloc.c";
      uVar1 = 0x301;
      badassert("n <= 32 * ARRAYCOUNT(freemap)","../../vm/kmalloc.c",0x301,"subpage_stats");
    }
    puVar3 = (undefined4 *)(pcVar7 + pr->freelist_offset);
    if (pr->freelist_offset != uVar5) {
      iVar2 = 1;
      for (; puVar3 != (undefined4 *)0x0; puVar3 = (undefined4 *)*puVar3) {
        uVar5 = (uint)((int)puVar3 - (int)pcVar7) / uVar1;
        if (uVar1 == 0) {
          trap(0x1c00);
        }
        pcVar6 = (char *)freemap;
        if (uVar9 <= uVar5) {
          pcVar6 = s_index_n_80027c58;
          pcVar7 = "../../vm/kmalloc.c";
          uVar1 = 0x30a;
          badassert(s_index_n_80027c58,"../../vm/kmalloc.c",0x30a,"subpage_stats");
        }
        *(uint *)((int)pcVar6 + (uVar5 >> 5) * 4) =
             *(uint *)((int)pcVar6 + (uVar5 >> 5) * 4) | iVar2 << (uVar5 & 0x1f);
      }
    }
    kprintf("at 0x%08lx: size %-4lu  %u/%u free\n");
    kprintf("   ");
    for (uVar1 = 0; uVar1 < uVar9; uVar1 = uVar1 + 1) {
      uVar8 = 0x2e;
      if ((freemap[uVar1 >> 5] & 1 << (uVar1 & 0x1f)) == 0) {
        uVar8 = 0x2a;
      }
      kprintf("%c",uVar8);
      if (((uVar1 & 0x3f) == 0x3f) && (uVar1 < uVar9 - 1)) {
        kprintf("\n   ");
      }
    }
    kprintf("\n");
    return;
  }
  uVar1 = 0;
  goto LAB_8001bd38;
}



void remove_lists(pageref *pr,int blktype)

{
  pageref *ppVar1;
  pageref *ppVar2;
  pageref **pppVar3;
  char *pcVar4;
  char *pcVar5;
  
  pcVar5 = (char *)(blktype << 2);
  if (7 < (uint)blktype) {
    pcVar4 = "blktype>=0 && blktype<NSIZES";
    pcVar5 = "../../vm/kmalloc.c";
    badassert("blktype>=0 && blktype<NSIZES","../../vm/kmalloc.c",0x33c,"remove_lists");
    pr = (pageref *)pcVar4;
  }
  ppVar1 = (pageref *)((int)sizebases + (int)pcVar5);
  do {
    ppVar2 = ppVar1;
    ppVar1 = ppVar2->next_samesize;
    if (ppVar1 == (pageref *)0x0) goto LAB_8001bf84;
  } while (ppVar1 != pr);
  ppVar2->next_samesize = pr->next_samesize;
LAB_8001bf84:
  pppVar3 = &allbase;
  while( true ) {
    ppVar1 = *pppVar3;
    if (ppVar1 == (pageref *)0x0) {
      return;
    }
    if (pr == ppVar1) break;
    pppVar3 = &ppVar1->next_all;
  }
  *pppVar3 = pr->next_all;
  return;
}



void freepageref(pageref *p)

{
  uint uVar1;
  uint uVar2;
  uint uVar3;
  char *pcVar4;
  char *pcVar5;
  char *pcVar6;
  uint uVar7;
  
  uVar1 = 0;
  pcVar4 = (char *)p;
  pcVar6 = (char *)kheaproots;
LAB_8001c0c4:
  do {
    if (0xf < uVar1) {
      badassert("0","../../vm/kmalloc.c",0x150,"freepageref");
      return;
    }
    pcVar5 = (char *)((int)pcVar6 + uVar1 * 0x28);
    uVar2 = (int)pcVar4 - (int)*(pagerefpage **)pcVar5;
    if (*(pagerefpage **)pcVar5 == (pagerefpage *)0x0) {
      uVar2 = *(uint *)((int)pcVar5 + 0x24);
      uVar1 = uVar1 + 1;
      if (uVar2 == 0) goto LAB_8001c0c4;
      pcVar4 = "root->numinuse == 0";
      pcVar5 = "../../vm/kmalloc.c";
      pcVar6 = "freepageref";
      badassert("root->numinuse == 0","../../vm/kmalloc.c",0x13e,"freepageref");
    }
    uVar2 = (int)uVar2 >> 4;
    uVar1 = uVar1 + 1;
    if (uVar2 < 0x100) {
      uVar7 = uVar2 >> 5;
      uVar2 = 1 << (uVar2 & 0x1f);
      uVar1 = ((uint32_t *)((int)pcVar5 + 4))[uVar7];
      uVar3 = ~uVar2;
      if ((uVar1 & uVar2) == 0) {
        pcVar5 = "../../vm/kmalloc.c";
        badassert("(root->pagerefs_inuse[i] & k) != 0","../../vm/kmalloc.c",0x148,"freepageref");
      }
      ((uint32_t *)((int)pcVar5 + 4))[uVar7] = uVar1 & uVar3;
      uVar1 = *(uint *)((int)pcVar5 + 0x24) - 1;
      if (*(uint *)((int)pcVar5 + 0x24) == 0) {
        pcVar5 = "../../vm/kmalloc.c";
        badassert("root->numinuse > 0","../../vm/kmalloc.c",0x14a,"freepageref");
      }
      *(uint *)((int)pcVar5 + 0x24) = uVar1;
      return;
    }
  } while( true );
}



void allocpagerefpage(kheap_root *root)

{
  pagerefpage *addr;
  
  if (root->page != (pagerefpage *)0x0) {
    badassert("root->page == NULL","../../vm/kmalloc.c",0xe0,"allocpagerefpage");
  }
  spinlock_release(&kmalloc_spinlock);
  addr = (pagerefpage *)alloc_kpages(1);
  spinlock_acquire(&kmalloc_spinlock);
  if (addr == (pagerefpage *)0x0) {
    kprintf("kmalloc: Couldn\'t get a pageref page\n");
  }
  else {
    if (((uint)addr & 0xfff) != 0) {
      badassert("va % PAGE_SIZE == 0","../../vm/kmalloc.c",0xee,"allocpagerefpage");
    }
    if (root->page != (pagerefpage *)0x0) {
      spinlock_release(&kmalloc_spinlock);
      free_kpages((vaddr_t)addr);
      spinlock_acquire(&kmalloc_spinlock);
      if (root->page != (pagerefpage *)0x0) {
        return;
      }
      badassert("root->page != NULL","../../vm/kmalloc.c",0xf6,"allocpagerefpage");
    }
    root->page = addr;
  }
  return;
}



pageref * allocpageref(void)

{
  uint uVar1;
  pagerefpage *ppVar2;
  char *pcVar3;
  char *pcVar4;
  kheap_root *pkVar5;
  char *pcVar6;
  int iVar7;
  kheap_root *root;
  uint uVar8;
  
  uVar1 = 0;
  pkVar5 = kheaproots;
  pcVar6 = (char *)0xffffffff;
  do {
    if (0xf < uVar1) {
      return (pageref *)0x0;
    }
    root = pkVar5 + uVar1;
    pcVar3 = (char *)root->numinuse;
    if (pcVar3 < (char *)0x100) {
      uVar8 = 0;
      while (uVar8 < 8) {
        pcVar4 = (char *)root->pagerefs_inuse[uVar8];
        iVar7 = 0;
        if (pcVar4 == pcVar6) {
          uVar8 = uVar8 + 1;
        }
        else {
          for (uVar1 = 1; uVar1 != 0; uVar1 = uVar1 << 1) {
            if ((uVar1 & (uint)pcVar4) == 0) {
              root->pagerefs_inuse[uVar8] = uVar1 | (uint)pcVar4;
              root->numinuse = (uint)(pcVar3 + 1);
              ppVar2 = root->page;
              if (ppVar2 == (pagerefpage *)0x0) {
                allocpagerefpage(root);
                ppVar2 = root->page;
              }
              if (ppVar2 != (pagerefpage *)0x0) {
                return ppVar2->refs + uVar8 * 0x20 + iVar7;
              }
              return (pageref *)0x0;
            }
            iVar7 = iVar7 + 1;
          }
          pcVar3 = "0";
          pkVar5 = (kheap_root *)0x124;
          pcVar6 = "allocpageref";
          badassert("0","../../vm/kmalloc.c",0x124,"allocpageref");
        }
      }
    }
    uVar1 = uVar1 + 1;
  } while( true );
}



void * subpage_kmalloc(size_t sz)

{
  char *pcVar1;
  freelist *addr;
  char **ppcVar2;
  uint uVar3;
  pageref *ppVar4;
  char *pcVar5;
  uint16_t uVar6;
  uint uVar7;
  uint uVar8;
  freelist *fl;
  int i;
  
  uVar3 = 0;
  do {
    uVar7 = uVar3;
    if (7 < uVar7) {
                    /* WARNING: Subroutine does not return */
      panic("Subpage allocator cannot handle allocation of size %zu\n");
    }
    uVar3 = uVar7 + 1;
  } while (sizes[uVar7] < sz);
  spinlock_acquire(&kmalloc_spinlock);
  for (ppVar4 = sizebases[uVar7]; ppVar4 != (pageref *)0x0; ppVar4 = ppVar4->next_samesize) {
    if ((ppVar4->pageaddr_and_blocktype & 0xfff) != uVar7) {
      badassert("PR_BLOCKTYPE(pr) == blktype","../../vm/kmalloc.c",0x395,"subpage_kmalloc");
    }
    if (ppVar4->nfree != 0) goto doalloc;
  }
  spinlock_release(&kmalloc_spinlock);
  addr = (freelist *)alloc_kpages(1);
  if (addr == (freelist *)0x0) {
    kprintf("kmalloc: Subpage allocator couldn\'t get a page\n");
    ppcVar2 = (char **)0x0;
  }
  else {
    if (((uint)addr & 0xfff) != 0) {
      badassert("prpage % PAGE_SIZE == 0","../../vm/kmalloc.c",0x3cd,"subpage_kmalloc");
    }
    spinlock_acquire(&kmalloc_spinlock);
    ppVar4 = allocpageref();
    if (ppVar4 == (pageref *)0x0) {
      spinlock_release(&kmalloc_spinlock);
      free_kpages((vaddr_t)addr);
      kprintf("kmalloc: Subpage allocator couldn\'t get pageref\n");
      ppcVar2 = (char **)0x0;
    }
    else {
      ppVar4->pageaddr_and_blocktype = (uint)addr & 0xfffff000 | uVar7 & 0xfff;
      uVar3 = sizes[uVar7];
      if (uVar3 == 0) {
        trap(0x1c00);
      }
      ppVar4->nfree = (uint16_t)(0x1000 / uVar3);
      addr->next = (freelist *)0x0;
      fl = addr;
      for (i = 1; i < (int)(uint)ppVar4->nfree; i = i + 1) {
        fl = (freelist *)((int)&addr->next + uVar3 * i);
        *fl = (freelist)((int)&addr->next + uVar3 * (i + -1));
        if (*(freelist **)fl == fl) {
          badassert("fl != fl->next","../../vm/kmalloc.c",0x3ec,"subpage_kmalloc");
        }
      }
      uVar8 = (int)fl - (int)addr & 0xffff;
      ppVar4->freelist_offset = (uint16_t)uVar8;
      if (uVar8 != uVar3 * (ppVar4->nfree - 1)) {
        badassert("pr->freelist_offset == (pr->nfree-1)*sizes[blktype]","../../vm/kmalloc.c",0x3f0,
                  "subpage_kmalloc");
      }
      ppVar4->next_samesize = sizebases[uVar7];
      sizebases[uVar7] = ppVar4;
      ppVar4->next_all = allbase;
      allbase = ppVar4;
doalloc:
      uVar3 = (uint)ppVar4->freelist_offset;
      pcVar5 = (char *)0xfffff000;
      if (0xfff < uVar3) {
        pcVar5 = "pr->freelist_offset < PAGE_SIZE";
        badassert("pr->freelist_offset < PAGE_SIZE","../../vm/kmalloc.c",0x39c,"subpage_kmalloc");
      }
      pcVar5 = (char *)(ppVar4->pageaddr_and_blocktype & (uint)pcVar5);
      ppcVar2 = (char **)(pcVar5 + uVar3);
      pcVar1 = *ppcVar2;
      uVar6 = ppVar4->nfree - 1;
      ppVar4->nfree = uVar6;
      if (pcVar1 == (char *)0x0) {
        if (uVar6 != 0) {
          badassert("pr->nfree == 0","../../vm/kmalloc.c",0x3ac,"subpage_kmalloc");
        }
        ppVar4->freelist_offset = 0xffff;
      }
      else {
        if (uVar6 == 0) {
          pcVar5 = "pr->nfree > 0";
          badassert("pr->nfree > 0","../../vm/kmalloc.c",0x3a6,"subpage_kmalloc");
        }
        if (0xfff < (uint)((int)pcVar1 - (int)pcVar5)) {
          pcVar5 = "fla - prpage < PAGE_SIZE";
          pcVar1 = "../../vm/kmalloc.c";
          badassert("fla - prpage < PAGE_SIZE","../../vm/kmalloc.c",0x3a8,"subpage_kmalloc");
        }
        ppVar4->freelist_offset = (short)pcVar1 - (short)pcVar5;
      }
      spinlock_release(&kmalloc_spinlock);
    }
  }
  return ppcVar2;
}



int subpage_kfree(void *ptr)

{
  uint uVar1;
  void *pvVar2;
  char *pcVar3;
  uint uVar4;
  pageref *pr;
  uint blktype;
  void *addr;
  uint uVar5;
  char *pcVar6;
  
  spinlock_acquire(&kmalloc_spinlock);
  addr = (void *)0x0;
  blktype = 0;
  uVar4 = 0xfffff000;
  for (pr = allbase; pr != (pageref *)0x0; pr = pr->next_all) {
    addr = (void *)(pr->pageaddr_and_blocktype & uVar4);
    blktype = pr->pageaddr_and_blocktype & 0xfff;
    uVar5 = (uint)(blktype < 8);
    if (uVar5 == 0) {
      badassert("blktype >= 0 && blktype < NSIZES","../../vm/kmalloc.c",0x433,"subpage_kfree");
    }
    uVar1 = (uint)(ptr < addr);
    if (uVar5 == 0) {
      badassert("blktype>=0 && blktype<NSIZES","../../vm/kmalloc.c",0x436,"subpage_kfree");
    }
    if ((uVar1 == 0) && (ptr < (void *)((int)addr + 0x1000U))) break;
  }
  uVar4 = (int)ptr - (int)addr;
  if (pr == (pageref *)0x0) {
    spinlock_release(&kmalloc_spinlock);
    return -1;
  }
  if (uVar4 < 0x1000) {
    uVar5 = sizes[blktype];
    if (uVar5 == 0) {
      trap(0x1c00);
    }
    if (uVar4 % uVar5 == 0) {
      fill_deadbeef(ptr,uVar5);
      pvVar2 = (void *)((uint)pr->freelist_offset + (int)addr);
      if (pr->freelist_offset == 0xffff) {
        *(undefined4 *)ptr = 0;
      }
      else {
        *(void **)ptr = pvVar2;
        if (ptr == pvVar2) {
          badassert("fl != fl->next","../../vm/kmalloc.c",0x46e,"subpage_kfree");
        }
      }
      pr->freelist_offset = (uint16_t)uVar4;
      pcVar3 = (char *)(pr->nfree + 1 & 0xffff);
      pcVar6 = (char *)(0x1000 / uVar5);
      if (uVar5 == 0) {
        trap(0x1c00);
      }
      pr->nfree = (uint16_t)pcVar3;
      if (pcVar6 < pcVar3) {
        pcVar6 = "pr->nfree <= PAGE_SIZE / sizes[blktype]";
        badassert("pr->nfree <= PAGE_SIZE / sizes[blktype]","../../vm/kmalloc.c",0x474,
                  "subpage_kfree");
      }
      if (pcVar3 == pcVar6) {
        remove_lists(pr,blktype);
        freepageref(pr);
        spinlock_release(&kmalloc_spinlock);
        free_kpages((vaddr_t)addr);
        return 0;
      }
      spinlock_release(&kmalloc_spinlock);
      return 0;
    }
  }
                    /* WARNING: Subroutine does not return */
  panic("kfree: subpage free of invalid addr %p\n",ptr);
}



void kheap_nextgeneration(void)

{
  return;
}



void kheap_dump(void)

{
  kprintf("Enable LABELS in kmalloc.c to use this functionality.\n");
  return;
}



void kheap_dumpall(void)

{
  kprintf("Enable LABELS in kmalloc.c to use this functionality.\n");
  return;
}



void kheap_printstats(void)

{
  pageref *pr;
  
  spinlock_acquire(&kmalloc_spinlock);
  kprintf("Subpage allocator status:\n");
  for (pr = allbase; pr != (pageref *)0x0; pr = pr->next_all) {
    subpage_stats(pr);
  }
  spinlock_release(&kmalloc_spinlock);
  return;
}



void * kmalloc(size_t sz)

{
  void *pvVar1;
  char *pcVar2;
  
  if (0x7ff < sz) {
    pvVar1 = (void *)alloc_kpages(sz + 0xfff >> 0xc);
    if (pvVar1 == (void *)0x0) {
      return (void *)0x0;
    }
    if (((uint)pvVar1 & 0xfff) == 0) {
      return pvVar1;
    }
    pcVar2 = "address % PAGE_SIZE == 0";
    badassert("address % PAGE_SIZE == 0","../../vm/kmalloc.c",0x4ac,"kmalloc");
    sz = (size_t)pcVar2;
  }
  pvVar1 = subpage_kmalloc(sz);
  return pvVar1;
}



void kfree(void *ptr)

{
  int iVar1;
  
  if (ptr != (void *)0x0) {
    iVar1 = subpage_kfree(ptr);
    if (iVar1 != 0) {
      if (((uint)ptr & 0xfff) != 0) {
        badassert("(vaddr_t)ptr%PAGE_SIZE==0","../../vm/kmalloc.c",0x4c4,"kfree");
      }
      free_kpages((vaddr_t)ptr);
    }
  }
  return;
}



void print_ipt(void)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x21,"print_ipt");
  }
  kprintf("PID: %d\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
  kprintf("<< IPT >>\n");
  for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
    iVar1 = ipt[iVar2].pid;
    if (iVar1 != -1) {
      kprintf("%d -   %d   - %d\n",iVar2,iVar1,ipt[iVar2].vaddr >> 0xc);
    }
  }
  return;
}



int create_ipt(void)

{
  paddr_t pVar1;
  int iVar2;
  char *maxN;
  
  pVar1 = ram_getsize();
  if ((int)pVar1 < 0) {
    pVar1 = pVar1 + 0xfff;
  }
  nRamFrames = (int)pVar1 >> 0xc;
  maxN = (char *)nRamFrames;
  if ((char *)nRamFrames == (char *)0x0) {
    maxN = "nRamFrames != 0";
    badassert("nRamFrames != 0","../../vm/pt.c",0x72,"create_ipt");
  }
  ipt_hash = STinit((int)maxN);
  ipt = (ipt_entry *)kmalloc(nRamFrames << 3);
  if (ipt == (ipt_entry *)0x0) {
    iVar2 = -1;
  }
  else if (ipt_hash == (ST)0x0) {
    iVar2 = -1;
  }
  else {
    spinlock_acquire(&ipt_lock);
    for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
      ipt[iVar2].pid = -1;
    }
    ipt_active = 1;
    spinlock_release(&ipt_lock);
    iVar2 = 0;
  }
  return iVar2;
}



paddr_t ipt_lookup(pid_t pid,vaddr_t vaddr)

{
  int iVar1;
  char *pcVar2;
  paddr_t pVar3;
  
  if (pid < 0) {
    pcVar2 = "../../vm/pt.c";
    badassert("pid >= 0","../../vm/pt.c",0x8c,"ipt_lookup");
    vaddr = (vaddr_t)pcVar2;
  }
  if ((char *)vaddr == (char *)0x0) {
    badassert("vaddr != 0","../../vm/pt.c",0x8d,"ipt_lookup");
  }
  spinlock_acquire(&ipt_lock);
  iVar1 = -0x7ffd0000;
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x8f,"ipt_lookup");
  }
  iVar1 = STsearch(*(ST *)(iVar1 + -0x7304),pid,vaddr);
  pVar3 = iVar1 << 0xc;
  if (iVar1 == -1) {
    pVar3 = 0;
  }
  spinlock_release(&ipt_lock);
  return pVar3;
}



int ipt_add(pid_t pid,paddr_t paddr,vaddr_t vaddr)

{
  Item item;
  int iVar1;
  char *pcVar2;
  vaddr_t vVar3;
  uint index;
  pid_t *ppVar4;
  
  pcVar2 = (char *)paddr;
  if (pid < 0) {
    pcVar2 = "../../vm/pt.c";
    vaddr = 0xaa;
    badassert("pid >= 0","../../vm/pt.c",0xaa,"ipt_add");
  }
  vVar3 = vaddr;
  if (pcVar2 == (char *)0x0) {
    pcVar2 = "../../vm/pt.c";
    vVar3 = 0xab;
    badassert("paddr != 0","../../vm/pt.c",0xab,"ipt_add");
  }
  index = (uint)pcVar2 >> 0xc;
  if (vVar3 == 0) {
    badassert("vaddr != 0","../../vm/pt.c",0xac,"ipt_add");
  }
  if (nRamFrames <= (int)index) {
    badassert("frame_index < nRamFrames","../../vm/pt.c",0xaf,"ipt_add");
  }
  spinlock_acquire(&ipt_lock);
  if (ipt_active != 0) {
    item = ITEMscan(pid,vaddr,index);
    iVar1 = -0x7ffd0000;
    if (ipt_active == 0) {
      badassert("ipt_active","../../vm/pt.c",0xb6,"ipt_add");
    }
    ppVar4 = (pid_t *)(*(int *)(iVar1 + -0x72f0) + index * 8);
    *ppVar4 = pid;
    ppVar4[1] = vaddr;
    STinsert(ipt_hash,item);
  }
  spinlock_release(&ipt_lock);
  return 0;
}



int ipt_kadd(pid_t pid,paddr_t paddr,vaddr_t vaddr)

{
  char *pcVar1;
  ipt_entry *piVar2;
  
  if (1 < pid + 2U) {
    pcVar1 = "../../vm/pt.c";
    vaddr = 0xc6;
    badassert("pid == -2 || pid == -1","../../vm/pt.c",0xc6,"ipt_kadd");
    paddr = (paddr_t)pcVar1;
  }
  if (nRamFrames <= (int)(paddr >> 0xc)) {
    badassert("frame_index < nRamFrames","../../vm/pt.c",200,"ipt_kadd");
  }
  spinlock_acquire(&ipt_lock);
  if (ipt_active != 0) {
    piVar2 = ipt + (paddr >> 0xc);
    piVar2->pid = pid;
    piVar2->vaddr = vaddr;
  }
  spinlock_release(&ipt_lock);
  return 0;
}



void free_ipt_process(pid_t pid)

{
  int *piVar1;
  int iVar2;
  int first_page;
  int unaff_s1;
  int unaff_s2;
  int unaff_s4;
  int unaff_s5;
  int unaff_s6;
  
  spinlock_acquire(&ipt_lock);
  first_page = 0;
  if (ipt_active != 0) {
    unaff_s4 = -0x7ffd0000;
    unaff_s2 = -0x7ffd0000;
    unaff_s6 = -1;
    unaff_s5 = -0x7ffd0000;
    goto LAB_8001d128;
  }
  badassert("ipt_active","../../vm/pt.c",0xdf,"free_ipt_process");
  do {
    piVar1 = (int *)(*(int *)(unaff_s2 + -0x72f0) + unaff_s1);
    if (*piVar1 == pid) {
      *piVar1 = unaff_s6;
      iVar2 = freeppages(first_page << 0xc,first_page);
      if (iVar2 == 0) {
                    /* WARNING: Subroutine does not return */
        panic("Trying to free ipt entry while VM not active");
      }
      STdelete(*(ST *)(unaff_s5 + -0x7304),pid,
               *(vaddr_t *)(*(int *)(unaff_s2 + -0x72f0) + unaff_s1 + 4));
    }
    first_page = first_page + 1;
LAB_8001d128:
    unaff_s1 = first_page << 3;
  } while (first_page < *(int *)(unaff_s4 + -0x72f4));
  spinlock_release(&ipt_lock);
  return;
}



int hash_delete(pid_t pid,vaddr_t vaddr)

{
  char *pcVar1;
  
  if (pid < 1) {
    pcVar1 = "../../vm/pt.c";
    badassert("pid > 0","../../vm/pt.c",0xf3,"hash_delete");
    vaddr = (vaddr_t)pcVar1;
  }
  if ((int)vaddr < 0) {
    vaddr = 0xf4;
    badassert("vaddr < 0x80000000","../../vm/pt.c",0xf4,"hash_delete");
  }
  STdelete(ipt_hash,pid,vaddr);
  return 0;
}



paddr_t get_victim(vaddr_t *vaddr,pid_t *pid)

{
  pid_t *ppVar1;
  int iVar2;
  pid_t pid_00;
  int iVar3;
  int iVar4;
  int unaff_s7;
  
  spinlock_acquire(&ipt_lock);
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x37,"get_victim");
  }
  iVar3 = *(int *)(*(int *)(unaff_s7 + 0x54) + 0x260);
  iVar2 = 0;
  while( true ) {
    if (nRamFrames <= iVar2) {
      spinlock_release(&ipt_lock);
      return 0;
    }
    iVar4 = iVar3 << 3;
    if (iVar3 == nRamFrames) {
      *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x260) = 0xffffffff;
      iVar3 = 0;
      iVar4 = 0;
    }
    ppVar1 = (pid_t *)((int)&ipt->pid + iVar4);
    iVar2 = iVar2 + 1;
    if (*ppVar1 == *(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c)) break;
    iVar3 = iVar3 + 1;
  }
  *(int *)(*(int *)(unaff_s7 + 0x54) + 0x260) = iVar3 + 1;
  *vaddr = ppVar1[1];
  pid_00 = *ppVar1;
  *pid = pid_00;
  hash_delete(pid_00,*vaddr);
  *(undefined4 *)((int)&ipt->pid + iVar4) = 0xfffffffe;
  iVar2 = splx(1);
  iVar4 = tlb_probe(*(undefined4 *)((int)&ipt->vaddr + iVar4),0);
  if (-1 < iVar4) {
    tlb_write((iVar4 + 0x80000) * 0x1000,0,iVar4);
  }
  splx(iVar2);
  spinlock_release(&ipt_lock);
  return iVar3 << 0xc;
}



void hash_print(void)

{
  STdisplay(ipt_hash);
  return;
}



int load_segment(addrspace *as,vnode *v,off_t offset,vaddr_t vaddr,size_t memsize,size_t filesize,
                int is_executable,paddr_t paddr)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  iovec local_48;
  undefined local_40 [12];
  char *local_34;
  uint local_30;
  uio_seg local_2c;
  uio_rw local_28;
  addrspace *local_24;
  
  if (*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10) != as) {
    vaddr = 0x2b;
    pcVar2 = "load_segment";
    badassert("curproc->p_addrspace == as","../../vm/segments.c",0x2b,"load_segment");
    memsize = (size_t)pcVar2;
  }
  if (offset._4_4_ < filesize) {
    kprintf("ELF: warning: segment filesize > segment memsize\n");
    filesize = offset._4_4_;
  }
  if ((dbflags & 0x40) != 0) {
    kprintf("ELF: Loading %lu bytes to 0x%lx\n",filesize,offset._0_4_);
  }
  local_48.field_0 = (offset._0_4_ & 0xfff) + paddr + -0x80000000;
  local_48.iov_len = offset._4_4_;
  local_40._0_4_ = &local_48;
  local_40._4_4_ = 1;
  local_2c = UIO_SYSSPACE;
  local_28 = UIO_READ;
  local_24 = (addrspace *)0x0;
  local_40._8_4_ = vaddr;
  local_34 = (char *)memsize;
  local_30 = filesize;
  vnode_check(v,"read");
  iVar1 = (*v->vn_ops->vop_read)(v,(uio *)local_40);
  if ((iVar1 == 0) && (local_30 != 0)) {
    kprintf("ELF: short read on segment - file truncated?\n");
    iVar1 = 0xd;
  }
  return iVar1;
}



int load_page(vaddr_t page_offset_from_segbase,vaddr_t vaddr,int segment,paddr_t paddr)

{
  char **ppcVar1;
  int iVar2;
  Elf_Ehdr *pEVar3;
  char *pcVar4;
  char *pcVar5;
  char *pcVar6;
  char **ppcVar7;
  uint uVar8;
  int unaff_s7;
  vnode *v;
  iovec iov;
  uio ku;
  Elf_Ehdr eh;
  Elf_Phdr ph;
  
  ppcVar7 = *(char ***)(unaff_s7 + 0x54);
  ppcVar1 = ppcVar7 + 0xb;
  pEVar3 = &eh;
  do {
    pcVar6 = ppcVar1[1];
    pcVar5 = ppcVar1[2];
    pcVar4 = ppcVar1[3];
    *(char **)pEVar3->e_ident = *ppcVar1;
    *(char **)(pEVar3->e_ident + 4) = pcVar6;
    *(char **)(pEVar3->e_ident + 8) = pcVar5;
    *(char **)(pEVar3->e_ident + 0xc) = pcVar4;
    ppcVar1 = ppcVar1 + 4;
    pEVar3 = (Elf_Ehdr *)&pEVar3->e_type;
  } while (ppcVar1 != ppcVar7 + 0x17);
  *(char **)pEVar3 = *ppcVar1;
  iVar2 = vfs_open(*ppcVar7,0,0,&v);
  if (iVar2 == 0) {
    uio_kinit(&iov,&ku,&ph,0x20,(ulonglong)(eh.e_phoff + (uint)eh.e_phentsize * segment),UIO_READ);
    vnode_check(v,"read");
    iVar2 = (*v->vn_ops->vop_read)(v,&ku);
    if (iVar2 == 0) {
      if (ku.uio_resid == 0) {
        if (ph.p_type == 1) {
          uVar8 = ph.p_vaddr & 0xfff;
          if ((ph.p_vaddr & 0xfffff000) + page_offset_from_segbase != vaddr) {
            badassert("vaddr == ((ph.p_vaddr & PAGE_FRAME) + page_offset_from_segbase)",
                      "../../vm/segments.c",199,"load_page");
          }
          if (page_offset_from_segbase < ph.p_filesz + uVar8) {
            increase(5);
            increase(6);
            if (page_offset_from_segbase == 0) {
              if ((ph.p_vaddr & 0xfffff000) + 0x1000 <= ph.p_vaddr + ph.p_filesz) {
                ph.p_filesz = ((ph.p_vaddr & 0xfffff000) - ph.p_vaddr) + 0x1000;
              }
            }
            else {
              iVar2 = uVar8 + ph.p_filesz;
              if (iVar2 - page_offset_from_segbase < 0x1000) {
                page_offset_from_segbase =
                     page_offset_from_segbase + ((ph.p_vaddr & 0xfffff000) - ph.p_vaddr);
                ph.p_filesz = iVar2 - page_offset_from_segbase;
                ph.p_vaddr = vaddr;
              }
              else {
                page_offset_from_segbase =
                     ((ph.p_vaddr & 0xfffff000) + page_offset_from_segbase) - ph.p_vaddr;
                ph.p_filesz = 0x1000;
                ph.p_vaddr = vaddr;
              }
            }
            iVar2 = load_segment(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10),v,
                                 CONCAT44(ph.p_vaddr,0x1000),0,
                                 ph.p_offset + page_offset_from_segbase,ph.p_filesz,ph.p_flags & 1,
                                 paddr);
          }
          else {
            increase(9);
          }
          if (iVar2 == 0) {
            iVar2 = as_complete_load(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
          }
        }
        else {
          iVar2 = 0;
          if ((ph.p_type != 0) && (iVar2 = 0, ph.p_type != 6)) {
            if (ph.p_type == 0x70000000) {
              iVar2 = 0;
            }
            else {
              kprintf("loadelf: unknown segment type %d\n");
              iVar2 = 0xd;
            }
          }
        }
      }
      else {
        kprintf("ELF: short read on phdr - file truncated?\n");
        iVar2 = 0xd;
      }
    }
  }
  return iVar2;
}



int file_write_paddr(vnode *vn,paddr_t buf_ptr,size_t size,off_t offset)

{
  int iVar1;
  iovec iov;
  uio u;
  
  iov.field_0 = buf_ptr + 0x80000000;
  u.uio_iov = &iov;
  u.uio_iovcnt = 1;
  u.uio_segflg = UIO_SYSSPACE;
  u.uio_rw = UIO_WRITE;
  u.uio_space = (addrspace *)0x0;
  iov.iov_len = size;
  u.uio_resid = size;
  u.uio_offset = offset;
  vnode_check(vn,s_console_write_80022690 + 8);
  iVar1 = (*vn->vn_ops->vop_write)(vn,&u);
  if (iVar1 == 0) {
    iVar1 = size - u.uio_resid;
  }
  return iVar1;
}



int file_read_paddr(vnode *vn,paddr_t buf_ptr,size_t size,off_t offset)

{
  size_t sVar1;
  iovec iov;
  uio u;
  
  iov.field_0 = buf_ptr + 0x80000000;
  u.uio_iov = &iov;
  u.uio_iovcnt = 1;
  u.uio_segflg = UIO_SYSSPACE;
  u.uio_rw = UIO_READ;
  u.uio_space = (addrspace *)0x0;
  iov.iov_len = size;
  u.uio_resid = size;
  u.uio_offset = offset;
  vnode_check(vn,"read");
  sVar1 = (*vn->vn_ops->vop_read)(vn,&u);
  if ((sVar1 == 0) && (sVar1 = size, u.uio_resid != 0)) {
    kprintf("SWAPPING IN: short read on page - problems reading?\n");
    sVar1 = 6;
  }
  return sVar1;
}



void print_swap_internal(void)

{
  int iVar1;
  
  kprintf("<< SWAP TABLE >>\n");
  for (iVar1 = 0; iVar1 < 0x900; iVar1 = iVar1 + 1) {
    if (swap_table[iVar1].pid != -1) {
      kprintf("%d -   %d   - %d\n",iVar1,swap_table[iVar1].pid,swap_table[iVar1].page >> 0xc);
    }
  }
  return;
}



void init_swapfile(void)

{
  int iVar1;
  
  iVar1 = vfs_open("./SWAPFILE",0x16,0x309,&v);
  if (iVar1 == -1) {
    badassert("result != -1",s_______vm_swapfile_c_800281fc,0x1c4,"init_swapfile");
  }
  spinlock_acquire(&swap_lock);
  for (iVar1 = 0; iVar1 < 0x900; iVar1 = iVar1 + 1) {
    swap_table[iVar1].pid = -1;
  }
  spinlock_release(&swap_lock);
  return;
}



int swap_in(vaddr_t page,paddr_t paddr)

{
  int iVar1;
  int iVar2;
  int iVar3;
  int unaff_s7;
  
  iVar3 = *(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c);
  spinlock_acquire(&swap_lock);
  iVar1 = 0;
  do {
    while( true ) {
      iVar2 = iVar1;
      if (0x8ff < iVar2) {
        spinlock_release(&swap_lock);
        return 1;
      }
      if (swap_table[iVar2].pid == iVar3) break;
      iVar1 = iVar2 + 1;
    }
    iVar1 = iVar2 + 1;
  } while (swap_table[iVar2].page != page);
  spinlock_release(&swap_lock);
  iVar1 = file_read_paddr(v,paddr,0x1000,(longlong)(iVar2 << 0xc));
  spinlock_acquire(&swap_lock);
  swap_table[iVar2].pid = -1;
  spinlock_release(&swap_lock);
  if (iVar1 != 0x1000) {
    badassert("result == PAGE_SIZE",s_______vm_swapfile_c_800281fc,0x206,"swap_in");
  }
  increase(8);
  increase(5);
  return 0;
}



/* WARNING: Removing unreachable block (ram,0x8001dcf4) */

int swap_out(paddr_t paddr,vaddr_t vaddr,int segment_victim,pid_t pid_victim)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  int iVar4;
  
  if (pid_victim == -1) {
    pcVar2 = "pid_victim != -1";
    pcVar3 = s_______vm_swapfile_c_800281fc;
    segment_victim = 0x245;
    badassert("pid_victim != -1",s_______vm_swapfile_c_800281fc,0x245,"swap_out");
    paddr = (paddr_t)pcVar2;
    vaddr = (vaddr_t)pcVar3;
  }
  if (segment_victim != 1) {
    spinlock_acquire(&swap_lock);
    iVar1 = 0;
    do {
      iVar4 = iVar1;
      if (0x8ff < iVar4) {
        spinlock_release(&swap_lock);
                    /* WARNING: Subroutine does not return */
        panic("Out of swapspace\n");
      }
      iVar1 = iVar4 + 1;
    } while (swap_table[iVar4].pid != -1);
    swap_table[iVar4].pid = -2;
    spinlock_release(&swap_lock);
    iVar1 = file_write_paddr(v,paddr,0x1000,(longlong)(iVar4 << 0xc));
    spinlock_acquire(&swap_lock);
    if (iVar1 != 0x1000) {
                    /* WARNING: Subroutine does not return */
      panic("Unable to swap page out");
    }
    swap_table[iVar4].pid = pid_victim;
    swap_table[iVar4].page = vaddr;
    spinlock_release(&swap_lock);
    increase(7);
  }
  return 0;
}



void free_swap_table(pid_t pid)

{
  int iVar1;
  int in_v1;
  char *pcVar2;
  char *pcVar3;
  swap_entry *psVar4;
  
  spinlock_acquire(&swap_lock);
  iVar1 = 0;
  if (pid < 0) {
    pcVar2 = "pid >= 0";
    pcVar3 = s_______vm_swapfile_c_800281fc;
    badassert("pid >= 0",s_______vm_swapfile_c_800281fc,0x276,"free_swap_table");
    psVar4 = (swap_entry *)pcVar3;
    do {
      if (*(int *)((int)&psVar4->pid + in_v1) == pid) {
        psVar4[iVar1].pid = (pid_t)pcVar2;
      }
      iVar1 = iVar1 + 1;
LAB_8001de00:
      in_v1 = iVar1 << 3;
    } while (iVar1 < 0x900);
    spinlock_release(&swap_lock);
    return;
  }
  psVar4 = swap_table;
  pcVar2 = (char *)0xffffffff;
  goto LAB_8001de00;
}



void print_swap(void)

{
  int iVar1;
  
  spinlock_acquire(&swap_lock);
  kprintf("<< SWAP TABLE >>\n");
  for (iVar1 = 0; iVar1 < 0x900; iVar1 = iVar1 + 1) {
    kprintf("%d -   %d   - %d\n",iVar1,swap_table[iVar1].pid,swap_table[iVar1].page >> 0xc);
  }
  spinlock_release(&swap_lock);
  return;
}



/* WARNING: Removing unreachable block (ram,0x8001e008) */

void duplicate_swap_pages(pid_t old_pid,pid_t new_pid)

{
  bool bVar1;
  paddr_t buf_ptr;
  int iVar2;
  int iVar3;
  int iVar4;
  
  buf_ptr = as_prepare_load(1);
  spinlock_acquire(&swap_lock);
  iVar4 = 0;
  do {
    while( true ) {
      if (0x8ff < iVar4) {
        spinlock_release(&swap_lock);
        freeppages(buf_ptr,buf_ptr >> 0xc);
        return;
      }
      iVar3 = 0;
      if (swap_table[iVar4].pid == old_pid) break;
      iVar4 = iVar4 + 1;
    }
    bVar1 = true;
    while (bVar1) {
      if (swap_table[iVar3].pid == -1) {
        swap_table[iVar3].pid = new_pid;
        swap_table[iVar3].page = swap_table[iVar4].page;
        print_swap_internal();
        spinlock_release(&swap_lock);
        iVar2 = file_read_paddr(v,buf_ptr,0x1000,(longlong)(iVar4 << 0xc));
        if (iVar2 != 0x1000) {
                    /* WARNING: Subroutine does not return */
          panic("Unable to read page from swap file");
        }
        iVar2 = file_write_paddr(v,buf_ptr,0x1000,(longlong)(iVar3 << 0xc));
        if (iVar2 != 0x1000) {
                    /* WARNING: Subroutine does not return */
          panic("Unable to swap page out for fork");
        }
        spinlock_acquire(&swap_lock);
        break;
      }
      iVar3 = iVar3 + 1;
      bVar1 = iVar3 < 0x900;
    }
    iVar4 = iVar4 + 1;
    if (iVar3 == 0x900) {
                    /* WARNING: Subroutine does not return */
      panic("Swap file is full");
    }
  } while( true );
}



int address_segment(vaddr_t faultaddress,addrspace *as)

{
  char *pcVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  undefined4 *puVar5;
  size_t sVar6;
  int iVar7;
  char *pcVar8;
  int unaff_s7;
  
  pcVar4 = (char *)faultaddress;
  if (*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10) != as) {
    pcVar4 = "as == curproc->p_addrspace";
    as = (addrspace *)&DAT_800282e8;
    badassert("as == curproc->p_addrspace",(char *)&DAT_800282e8,0x50,"address_segment");
  }
  if (as == (addrspace *)0x0) {
    pcVar4 = "as != NULL";
    as = (addrspace *)&DAT_800282e8;
    badassert("as != NULL",(char *)&DAT_800282e8,0x53,"address_segment");
  }
  pcVar1 = (char *)as->as_vbase1;
  if (pcVar1 == (char *)0x0) {
    pcVar4 = "as->as_vbase1 != 0";
    as = (addrspace *)&DAT_800282e8;
    badassert("as->as_vbase1 != 0",(char *)&DAT_800282e8,0x54,"address_segment");
  }
  sVar6 = as->as_npages1;
  pcVar8 = (char *)0x80020000;
  if (sVar6 == 0) {
    pcVar4 = "as->as_npages1 != 0";
    as = (addrspace *)&DAT_800282e8;
    sVar6 = 0x55;
    pcVar8 = "address_segment";
    badassert("as->as_npages1 != 0",(char *)&DAT_800282e8,0x55,"address_segment");
  }
  pcVar3 = (char *)as->as_vbase2;
  if (pcVar3 == (char *)0x0) {
    pcVar4 = "as->as_vbase2 != 0";
    as = (addrspace *)&DAT_800282e8;
    sVar6 = 0x56;
    badassert("as->as_vbase2 != 0",(char *)&DAT_800282e8,0x56,pcVar8 + 0x20ac);
  }
  puVar5 = (undefined4 *)as->as_npages2;
  if (puVar5 == (undefined4 *)0x0) {
    pcVar4 = "as->as_npages2 != 0";
    puVar5 = &DAT_800282e8;
    sVar6 = 0x57;
    badassert("as->as_npages2 != 0",(char *)&DAT_800282e8,0x57,"address_segment");
  }
  if (pcVar1 != (char *)((uint)pcVar1 & 0xfffff000)) {
    pcVar4 = "(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1";
    puVar5 = &DAT_800282e8;
    sVar6 = 0x58;
    badassert("(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1",(char *)&DAT_800282e8,0x58,
              "address_segment");
  }
  iVar7 = sVar6 << 0xc;
  if (pcVar3 != (char *)((uint)pcVar3 & 0xfffff000)) {
    pcVar4 = "(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2";
    puVar5 = &DAT_800282e8;
    iVar7 = 0x59;
    badassert("(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2",(char *)&DAT_800282e8,0x59,
              "address_segment");
  }
  if ((((pcVar4 < pcVar1) || (iVar2 = 1, pcVar1 + iVar7 <= pcVar4)) &&
      ((pcVar4 < pcVar3 || (iVar2 = 2, pcVar3 + (int)puVar5 * 0x1000 <= pcVar4)))) &&
     ((iVar2 = 6, (char *)0x7ffedfff < pcVar4 && (iVar2 = 3, (char *)0x7fffffff < pcVar4)))) {
    iVar2 = 6;
  }
  return iVar2;
}



int tlb_get_rr_victim(void)

{
  uint uVar1;
  
  uVar1 = tlb_get_rr_victim::next_victim;
  tlb_get_rr_victim::next_victim = tlb_get_rr_victim::next_victim + 1 & 0x3f;
  increase(2);
  return uVar1;
}



void update_tlb(vaddr_t faultaddress,paddr_t paddr)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  int unaff_s7;
  uint32_t ehi1;
  uint32_t elo1;
  
  iVar1 = address_segment(faultaddress,*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
  if (iVar1 == 1) {
    uVar3 = paddr & 0xfffff9ff | 0x200;
  }
  else {
    uVar3 = paddr | 0x600;
  }
  iVar1 = splx(1);
  iVar2 = 0;
  do {
    if (0x3f < iVar2) {
LAB_8001e388:
      if (iVar2 == 0x40) {
        iVar2 = tlb_get_rr_victim();
        tlb_write(faultaddress,uVar3,iVar2);
      }
      splx(iVar1);
      return;
    }
    tlb_read(&ehi1,&elo1,iVar2);
    if ((elo1 & 0x200) == 0) {
      increase(1);
      tlb_write(faultaddress,uVar3,iVar2);
      goto LAB_8001e388;
    }
    iVar2 = iVar2 + 1;
  } while( true );
}



int vm_fault(int faulttype,vaddr_t faultaddress)

{
  int iVar1;
  addrspace *as;
  paddr_t paddr;
  uint paddr_00;
  int iVar2;
  undefined4 *paddr_01;
  char *page;
  undefined4 *paddr_02;
  char *faultaddress_00;
  vaddr_t vVar3;
  int unaff_s7;
  
  faultaddress_00 = (char *)(faultaddress & 0xfffff000);
  if ((dbflags & 0x20) != 0) {
    kprintf("dumbvm: fault: 0x%x\n",faultaddress_00);
  }
  iVar1 = 8;
  if (-1 < faulttype) {
    if (faulttype < 2) {
      iVar1 = 6;
      if (*(int *)(unaff_s7 + 0x54) != 0) {
        as = proc_getas();
        if (as == (addrspace *)0x0) {
          iVar1 = 6;
        }
        else {
          iVar1 = address_segment((vaddr_t)faultaddress_00,as);
          if (iVar1 == 6) {
            kprintf("PID: %d\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
            kprintf("SEGMENTATION FAULT: process exited\n");
            sys__exit(-1);
                    /* WARNING: Subroutine does not return */
            panic("VM: got SEGMENTATION FAULT, should not get here\n");
          }
          increase(0);
          spinlock_acquire(&tlb_fault_lock);
          paddr = ipt_lookup(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),(vaddr_t)faultaddress_00);
          if (paddr == 0) {
            if (iVar1 - 1U < 2) {
              if (iVar1 == 1) {
                vVar3 = as->as_vbase1;
              }
              else {
                vVar3 = as->as_vbase2;
              }
              spinlock_release(&tlb_fault_lock);
              paddr_00 = as_prepare_load(1);
              if (paddr_00 == 0) {
                badassert("paddr != 0",(char *)&DAT_800282e8,0xd4,"vm_fault");
              }
              iVar2 = 1;
              if ((paddr_00 & 0xfffff000) != paddr_00) {
                badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0xd7,"vm_fault");
              }
              if (iVar1 == iVar2) {
                iVar2 = 1;
              }
              else {
                iVar2 = swap_in((vaddr_t)faultaddress_00,paddr_00);
              }
              as_complete_load(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
              if ((iVar2 == 0) ||
                 (iVar1 = load_page((int)faultaddress_00 - vVar3,(vaddr_t)faultaddress_00,iVar1,
                                    paddr_00), iVar1 == 0)) {
                spinlock_acquire(&tlb_fault_lock);
                iVar1 = ipt_add(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),paddr_00,
                                (vaddr_t)faultaddress_00);
                if (iVar1 == 0) {
                  update_tlb((vaddr_t)faultaddress_00,paddr_00);
                  spinlock_release(&tlb_fault_lock);
                  iVar1 = 0;
                }
                else {
                  iVar1 = -1;
                }
              }
              else {
                iVar1 = -1;
              }
            }
            else {
              spinlock_release(&tlb_fault_lock);
              paddr_01 = (undefined4 *)as_prepare_load(1);
              page = faultaddress_00;
              if ((undefined4 *)((uint)paddr_01 & 0xfffff000) != paddr_01) {
                page = "(paddr & PAGE_FRAME) == paddr";
                badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0x105,"vm_fault");
              }
              iVar1 = swap_in((vaddr_t)page,(paddr_t)paddr_01);
              if (iVar1 != 0) {
                increase(9);
              }
              spinlock_acquire(&tlb_fault_lock);
              paddr_02 = paddr_01;
              if (paddr_01 == (undefined4 *)0x0) {
                paddr_02 = &DAT_800282e8;
                badassert("paddr != 0",(char *)&DAT_800282e8,0x10e,"vm_fault");
              }
              iVar1 = ipt_add(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),(paddr_t)paddr_02,
                              (vaddr_t)faultaddress_00);
              if (iVar1 == 0) {
                update_tlb((vaddr_t)faultaddress_00,(paddr_t)paddr_01);
                spinlock_release(&tlb_fault_lock);
                iVar1 = 0;
              }
              else {
                iVar1 = -1;
              }
            }
          }
          else {
            increase(4);
            if ((paddr & 0xfffff000) != paddr) {
              badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0x123,"vm_fault");
            }
            update_tlb((vaddr_t)faultaddress_00,paddr);
            spinlock_release(&tlb_fault_lock);
            iVar1 = 0;
          }
        }
      }
    }
    else {
      if (faulttype == 2) {
        kprintf("VM_FAULT_READONLY: process exited\n");
        sys__exit(-1);
                    /* WARNING: Subroutine does not return */
        panic("VM: got VM_FAULT_READONLY, should not get here\n");
      }
      iVar1 = 8;
    }
  }
  return iVar1;
}



void vm_tlbshootdown(tlbshootdown *ts)

{
                    /* WARNING: Subroutine does not return */
  panic("dumbvm tried to do tlb shootdown?!\n");
}



void kill_curthread(vaddr_t epc,uint code,vaddr_t vaddr)

{
  uint uVar1;
  char *pcVar2;
  
  uVar1 = (uint)(code < 0xd);
  if (uVar1 == 0) {
    pcVar2 = "../../arch/mips/locore/trap.c";
    badassert("code < NTRAPCODES","../../arch/mips/locore/trap.c",0x51,"kill_curthread");
    code = (uint)pcVar2;
  }
  if (uVar1 != 0) {
                    /* WARNING: Could not recover jumptable at 0x8001e7ec. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    (*(code *)(&PTR_LAB_800220c0)[code])();
    return;
  }
  kprintf("Fatal user mode trap %u sig %d (%s, epc 0x%x, vaddr 0x%x)\n");
                    /* WARNING: Subroutine does not return */
  panic("I don\'t know how to handle this\n");
}



/* WARNING: Removing unreachable block (ram,0x8001e964) */

void mips_trap(trapframe *tf)

{
  undefined uVar1;
  bool bVar2;
  uint uVar3;
  trapframe *ptVar4;
  int iVar5;
  int iVar6;
  trapframe *ptVar7;
  undefined4 uVar8;
  char *pcVar9;
  uint uVar10;
  uint code;
  vaddr_t unaff_s7;
  
  uVar10 = tf->tf_cause & 0x3c;
  code = uVar10 >> 2;
  uVar3 = tf->tf_status;
  pcVar9 = (char *)tf;
  if (0xc < code) {
    pcVar9 = "code < NTRAPCODES";
    badassert("code < NTRAPCODES","../../arch/mips/locore/trap.c",0x93,"mips_trap");
  }
  if (unaff_s7 != 0) {
    ptVar4 = *(trapframe **)(unaff_s7 + 0x48);
    if (ptVar4 != (trapframe *)0x0) {
      ptVar7 = (trapframe *)&ptVar4[0x1d].tf_a0;
      if (pcVar9 <= ptVar4) {
        pcVar9 = "(vaddr_t)tf > (vaddr_t)curthread->t_stack";
        badassert("(vaddr_t)tf > (vaddr_t)curthread->t_stack","../../arch/mips/locore/trap.c",0x97,
                  "mips_trap");
      }
      if (ptVar7 <= pcVar9) {
        badassert("(vaddr_t)tf < (vaddr_t)(curthread->t_stack + STACK_SIZE)",
                  "../../arch/mips/locore/trap.c",0x99,"mips_trap");
      }
    }
  }
  if (code == 0) {
    uVar1 = *(undefined *)(unaff_s7 + 0x58);
    *(undefined *)(unaff_s7 + 0x58) = 1;
    if (*(int *)(unaff_s7 + 0x5c) == 0) {
      iVar5 = *(int *)(unaff_s7 + 0x60);
      uVar8 = 1;
      if (iVar5 != 0) {
        badassert("curthread->t_iplhigh_count == 0","../../arch/mips/locore/trap.c",0xb7,"mips_trap"
                 );
      }
      *(undefined4 *)(unaff_s7 + 0x5c) = uVar8;
      *(int *)(unaff_s7 + 0x60) = iVar5 + 1;
      bVar2 = true;
    }
    else {
      bVar2 = false;
    }
    mainbus_interrupt(tf);
    if (bVar2) {
      iVar5 = 1;
      if (*(int *)(unaff_s7 + 0x5c) != 1) {
        badassert("curthread->t_curspl == IPL_HIGH","../../arch/mips/locore/trap.c",0xc3,"mips_trap"
                 );
      }
      iVar6 = *(int *)(unaff_s7 + 0x60) + -1;
      if (*(int *)(unaff_s7 + 0x60) != iVar5) {
        badassert("curthread->t_iplhigh_count == 1","../../arch/mips/locore/trap.c",0xc4,"mips_trap"
                 );
      }
      *(int *)(unaff_s7 + 0x60) = iVar6;
      *(undefined4 *)(unaff_s7 + 0x5c) = 0;
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar1;
    goto done2;
  }
  iVar5 = splx(1);
  splx(iVar5);
  switch(uVar10) {
  case 0:
  case 0x10:
  case 0x14:
    goto switchD_8001eb24_caseD_0;
  case 4:
    iVar5 = vm_fault(2,tf->tf_vaddr);
    break;
  case 8:
    iVar5 = vm_fault(0,tf->tf_vaddr);
    break;
  case 0xc:
    iVar5 = vm_fault(1,tf->tf_vaddr);
    break;
  case 0x18:
  case 0x1c:
                    /* WARNING: Subroutine does not return */
    panic("Bus error exception, PC=0x%x\n",tf->tf_epc);
  default:
    if (*(int *)(unaff_s7 + 0x5c) != 0) {
      badassert("curthread->t_curspl == 0","../../arch/mips/locore/trap.c",0xde,"mips_trap");
    }
    iVar5 = -0x7ffd0000;
    if (*(int *)(unaff_s7 + 0x60) != 0) {
      badassert("curthread->t_iplhigh_count == 0","../../arch/mips/locore/trap.c",0xdf,"mips_trap");
    }
    if ((*(uint *)(iVar5 + -0x64c0) & 2) != 0) {
      kprintf("syscall: #%d, args %x %x %x %x\n",tf->tf_v0,tf->tf_a0,tf->tf_a1,tf->tf_a2,tf->tf_a3);
    }
    syscall(tf);
    goto done;
  }
  if (iVar5 != 0) {
switchD_8001eb24_caseD_0:
    if ((uVar3 & 8) != 0) {
      kill_curthread(tf->tf_epc,code,tf->tf_vaddr);
    }
    if ((unaff_s7 == 0) || (*(uint32_t *)(unaff_s7 + 0xc) == 0)) {
      kprintf("panic: Fatal exception %u (%s) in kernel mode\n",code,
              *(undefined4 *)((int)trapcodenames + uVar10));
      kprintf("panic: EPC 0x%x, exception vaddr 0x%x\n",tf->tf_epc,tf->tf_vaddr);
                    /* WARNING: Subroutine does not return */
      panic("I can\'t handle this... I think I\'ll just die now...\n");
    }
    tf->tf_epc = *(uint32_t *)(unaff_s7 + 0xc);
  }
done:
  cpu_irqoff();
done2:
  if (*(int *)(unaff_s7 + 0x48) != 0) {
    cputhreads[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = unaff_s7;
    cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = *(int *)(unaff_s7 + 0x48) + 0x1000;
    if (((cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] - 1 ^ (uint)tf) & 0xfffff000) != 0) {
      badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)",
                "../../arch/mips/locore/trap.c",0x160,"mips_trap");
    }
  }
  return;
}



void mips_usermode(trapframe *tf)

{
  char *tf_00;
  char *ctf;
  char *in_a1;
  char *whence;
  uint32_t in_a2;
  char *in_a3;
  uint32_t uVar1;
  vaddr_t unaff_s7;
  uint32_t uStack232;
  uint32_t auStack228 [2];
  char *pcStack220;
  uint32_t uStack216;
  undefined4 uStack212;
  trapframe tStack192;
  trapframe *ptStack44;
  
  splx(0);
  cpu_irqoff();
  cputhreads[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = unaff_s7;
  cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = *(int *)(unaff_s7 + 0x48) + 0x1000;
  if (((cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] - 1 ^ (uint)tf) & 0xfffff000) != 0) {
    in_a1 = "../../arch/mips/locore/trap.c";
    in_a2 = 0x193;
    in_a3 = "mips_usermode";
    badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)",
              "../../arch/mips/locore/trap.c",0x193,"mips_usermode");
  }
  tStack192.tf_a0 = (uint32_t)tf;
  asm_usermode();
  ptStack44 = tf;
  bzero(&tStack192,0x8c);
  tStack192.tf_status = 0xff0c;
  uStack212 = 0x8001ee08;
  tf_00 = (char *)&tStack192;
  tStack192.tf_a1 = (uint32_t)in_a1;
  tStack192.tf_a2 = in_a2;
  tStack192.tf_sp = (uint32_t)in_a3;
  mips_usermode((trapframe *)tf_00);
  pcStack220 = in_a3;
  uStack216 = in_a2;
  if (unaff_s7 == 0) {
    tf_00 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
  }
  ctf = tf_00;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    ctf = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    ctf = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar1 = *(uint32_t *)((int)ctf + 0x1c);
  uStack232 = 0;
  switch(uVar1) {
  case 0:
    auStack228[0] = sys_fork((trapframe *)ctf,(pid_t *)&uStack232);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar1);
    if (uVar1 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    auStack228[0] = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)ctf + 0x24));
    auStack228[0] = 0;
    break;
  case 4:
    uStack232 = sys_waitpid(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                            *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 5:
    uStack232 = sys_getpid();
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x2d:
    uStack232 = sys_open(*(userptr_t *)((int)ctf + 0x24),*(uint32_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c),(int *)auStack228);
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x31:
    uStack232 = sys_close(*(uint32_t *)((int)ctf + 0x24));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x32:
    uStack232 = sys_read(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x37:
    uStack232 = sys_write(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                          *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x3b:
    auStack228[0] =
         sys_lseek(*(uint32_t *)((int)ctf + 0x24),
                   CONCAT44(*(uint32_t *)((int)tf_00 + 0x2c),&uStack232),(int)whence,(int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    auStack228[0] = sys___time(*(userptr_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28));
    break;
  case 0x77:
    auStack228[0] = sys_reboot(*(uint32_t *)((int)ctf + 0x24));
  }
  if (auStack228[0] == 0) {
    *(uint32_t *)((int)tf_00 + 0x1c) = uStack232;
    *(uint32_t *)((int)tf_00 + 0x30) = 0;
  }
  else {
    *(uint32_t *)((int)tf_00 + 0x1c) = auStack228[0];
    *(uint32_t *)((int)tf_00 + 0x30) = 1;
  }
  *(uint32_t *)((int)tf_00 + 0x88) = *(uint32_t *)((int)tf_00 + 0x88) + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}



void enter_new_process(int argc,userptr_t argv,userptr_t env,vaddr_t stack,vaddr_t entry)

{
  char *tf_00;
  char *ctf;
  char *whence;
  uint32_t uVar1;
  int unaff_s7;
  uint32_t uStack208;
  uint32_t auStack204 [2];
  vaddr_t vStack196;
  userptr_t p_Stack192;
  undefined4 uStack188;
  trapframe tf;
  
  bzero(&tf,0x8c);
  tf.tf_status = 0xff0c;
  tf.tf_epc = entry;
  uStack188 = 0x8001ee08;
  tf_00 = (char *)&tf;
  tf.tf_a0 = argc;
  tf.tf_a1 = (uint32_t)argv;
  tf.tf_a2 = (uint32_t)env;
  tf.tf_sp = stack;
  mips_usermode((trapframe *)tf_00);
  vStack196 = stack;
  p_Stack192 = env;
  if (unaff_s7 == 0) {
    tf_00 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
  }
  ctf = tf_00;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    ctf = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    ctf = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar1 = *(uint32_t *)((int)ctf + 0x1c);
  uStack208 = 0;
  switch(uVar1) {
  case 0:
    auStack204[0] = sys_fork((trapframe *)ctf,(pid_t *)&uStack208);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar1);
    if (uVar1 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    auStack204[0] = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)ctf + 0x24));
    auStack204[0] = 0;
    break;
  case 4:
    uStack208 = sys_waitpid(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                            *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 5:
    uStack208 = sys_getpid();
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x2d:
    uStack208 = sys_open(*(userptr_t *)((int)ctf + 0x24),*(uint32_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c),(int *)auStack204);
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x31:
    uStack208 = sys_close(*(uint32_t *)((int)ctf + 0x24));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x32:
    uStack208 = sys_read(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x37:
    uStack208 = sys_write(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                          *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x3b:
    auStack204[0] =
         sys_lseek(*(uint32_t *)((int)ctf + 0x24),
                   CONCAT44(*(uint32_t *)((int)tf_00 + 0x2c),&uStack208),(int)whence,(int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    auStack204[0] = sys___time(*(userptr_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28));
    break;
  case 0x77:
    auStack204[0] = sys_reboot(*(uint32_t *)((int)ctf + 0x24));
  }
  if (auStack204[0] == 0) {
    *(uint32_t *)((int)tf_00 + 0x1c) = uStack208;
    *(uint32_t *)((int)tf_00 + 0x30) = 0;
  }
  else {
    *(uint32_t *)((int)tf_00 + 0x1c) = auStack204[0];
    *(uint32_t *)((int)tf_00 + 0x30) = 1;
  }
  *(uint32_t *)((int)tf_00 + 0x88) = *(uint32_t *)((int)tf_00 + 0x88) + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}



void syscall(trapframe *tf)

{
  char *pcVar1;
  char *whence;
  uint32_t uVar2;
  int unaff_s7;
  int32_t retval;
  int err;
  
  if (unaff_s7 == 0) {
    pcVar1 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
    tf = (trapframe *)pcVar1;
  }
  pcVar1 = (char *)tf;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    pcVar1 = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    pcVar1 = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar2 = *(uint32_t *)((int)pcVar1 + 0x1c);
  retval = 0;
  switch(uVar2) {
  case 0:
    err = sys_fork((trapframe *)pcVar1,&retval);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar2);
    if (uVar2 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    err = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)pcVar1 + 0x24));
    err = 0;
    break;
  case 4:
    retval = sys_waitpid(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 5:
    retval = sys_getpid();
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x2d:
    retval = sys_open(*(userptr_t *)((int)pcVar1 + 0x24),tf->tf_a1,tf->tf_a2,&err);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x31:
    retval = sys_close(*(uint32_t *)((int)pcVar1 + 0x24));
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x32:
    retval = sys_read(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x37:
    retval = sys_write(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x3b:
    err = sys_lseek(*(uint32_t *)((int)pcVar1 + 0x24),CONCAT44(tf->tf_a2,&retval),(int)whence,
                    (int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    err = sys___time(*(userptr_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1);
    break;
  case 0x77:
    err = sys_reboot(*(uint32_t *)((int)pcVar1 + 0x24));
  }
  if (err == 0) {
    tf->tf_v0 = retval;
    tf->tf_a3 = 0;
  }
  else {
    tf->tf_v0 = err;
    tf->tf_a3 = 1;
  }
  tf->tf_epc = tf->tf_epc + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}



void enter_forked_process(trapframe *tf)

{
  trapframe *ptVar1;
  trapframe *ptVar2;
  trapframe *ptVar3;
  trapframe *ptVar4;
  uint32_t uVar5;
  uint32_t uVar6;
  uint32_t uVar7;
  trapframe forkedTf;
  
  ptVar3 = tf;
  ptVar1 = &forkedTf;
  do {
    ptVar4 = ptVar1;
    ptVar2 = ptVar3;
    uVar7 = ptVar2->tf_status;
    uVar6 = ptVar2->tf_cause;
    uVar5 = ptVar2->tf_lo;
    ptVar4->tf_vaddr = ptVar2->tf_vaddr;
    ptVar4->tf_status = uVar7;
    ptVar4->tf_cause = uVar6;
    ptVar4->tf_lo = uVar5;
    ptVar3 = (trapframe *)&ptVar2->tf_hi;
    ptVar1 = (trapframe *)&ptVar4->tf_hi;
  } while (ptVar3 != (trapframe *)&tf->tf_sp);
  uVar6 = ptVar2->tf_ra;
  uVar5 = ptVar2->tf_at;
  ptVar4->tf_hi = *(uint32_t *)ptVar3;
  ptVar4->tf_ra = uVar6;
  ptVar4->tf_at = uVar5;
  forkedTf.tf_v0 = 0;
  forkedTf.tf_a3 = 0;
  forkedTf.tf_epc = forkedTf.tf_epc + 4;
  as_activate();
  mips_usermode(&forkedTf);
  setCopReg(0,Status,Status | 1,0);
  setCopReg(0,Status,Status & 0xfffffffe,0);
  return;
}



void cpu_irqonoff(void)

{
  setCopReg(0,Status,Status | 1,0);
  setCopReg(0,Status,Status & 0xfffffffe,0);
  return;
}



void cpu_machdep_init(cpu *c)

{
  uint uVar1;
  void *pvVar2;
  char *pcVar3;
  
  uVar1 = c->c_number;
  if (0x1f < uVar1) {
    pcVar3 = "c->c_number < MAXCPUS";
    badassert("c->c_number < MAXCPUS","../../arch/mips/thread/cpu.c",0x50,"cpu_machdep_init");
    c = (cpu *)pcVar3;
  }
  pvVar2 = c->c_curthread->t_stack;
  if (pvVar2 != (void *)0x0) {
    cpustacks[uVar1] = (int)pvVar2 + 0x1000;
    cputhreads[c->c_number] = (vaddr_t)c->c_curthread;
  }
  return;
}



void cpu_identify(char *buf,size_t max)

{
  if (PRId == 0xa1) {
    snprintf(buf,max,"MIPS/161 (System/161 2.x) features 0x%x",EBase);
    if (cop0_reg15_2 != 0) {
      kprintf("WARNING: unknown CPU incompatible features 0x%x\n");
    }
  }
  else if (PRId == 0x3ff) {
    snprintf(buf,max,"MIPS/161 (System/161 1.x and pre-2.x)");
  }
  else {
    snprintf(buf,max,"32-bit MIPS (unknown type, CPU ID 0x%x)");
  }
  return;
}



void cpu_irqon(void)

{
  setCopReg(0,Status,Status | 1,0);
  return;
}



void cpu_irqoff(void)

{
  setCopReg(0,Status,Status & 0xfffffffe,0);
  return;
}



void cpu_idle(void)

{
  wait(0);
  cpu_irqonoff();
  return;
}



void cpu_halt(void)

{
  cpu_irqoff();
  do {
    wait(0);
  } while( true );
}



void switchframe_init(thread *thread,anon_subr_void_void_ptr_ulong *entrypoint,void *data1,
                     ulong data2)

{
  void *pvVar1;
  
  pvVar1 = thread->t_stack;
  bzero((switchframe *)((int)pvVar1 + 0xfd8),0x28);
  *(anon_subr_void_void_ptr_ulong **)((int)pvVar1 + 0xfd8) = entrypoint;
  *(void **)((int)pvVar1 + 0xfdc) = data1;
  *(ulong *)((int)pvVar1 + 0xfe0) = data2;
  *(code **)((int)pvVar1 + 0xffc) = mips_threadstart;
  thread->t_context = (switchframe *)((int)pvVar1 + 0xfd8);
  return;
}



void switchframe_switch(undefined4 param_1)

{
  *(undefined **)param_1 = &stack0xffffffd8;
  return;
}



void thread_machdep_init(thread_machdep *tm)

{
  tm->tm_badfaultfunc = (badfaultfunc_t *)0x0;
  return;
}



void thread_machdep_cleanup(thread_machdep *tm)

{
  if (tm->tm_badfaultfunc != (badfaultfunc_t *)0x0) {
    badassert("tm->tm_badfaultfunc == NULL","../../arch/mips/thread/thread_machdep.c",0x30,
              "thread_machdep_cleanup");
  }
  return;
}



void mips_threadstart(void)

{
  anon_subr_void_void_ptr_ulong *unaff_s0;
  void *unaff_s1;
  ulong unaff_s2;
  
  thread_startup(unaff_s0,unaff_s1,unaff_s2);
  return;
}



void ram_bootstrap(void)

{
  lastpaddr = mainbus_ramsize();
  if (0x20000000 < lastpaddr) {
    lastpaddr = 0x20000000;
  }
  firstpaddr = firstfree + 0x80000000;
  kprintf("%uk physical memory available\n",lastpaddr - firstpaddr >> 10);
  return;
}



paddr_t ram_stealmem(ulong npages)

{
  paddr_t pVar1;
  uint uVar2;
  
  pVar1 = firstpaddr;
  uVar2 = firstpaddr + npages * 0x1000;
  if (uVar2 <= lastpaddr) {
    firstpaddr = uVar2;
    return pVar1;
  }
  return 0;
}



paddr_t ram_getsize(void)

{
  return lastpaddr;
}



paddr_t ram_getfirstfreeafterbootstrap(void)

{
  return firstpaddr;
}



paddr_t ram_getfirstfree(void)

{
  paddr_t pVar1;
  
  pVar1 = firstpaddr;
  lastpaddr = 0;
  firstpaddr = 0;
  return pVar1;
}



longlong __adddi3(longlong a,longlong b)

{
  int in_a0;
  int in_a1;
  int in_a2;
  uint in_a3;
  
  return CONCAT44(in_a2 + in_a0 + (uint)(in_a3 + in_a1 < in_a3),in_a3 + in_a1);
}



longlong __anddi3(longlong a,longlong b)

{
  uint in_a0;
  uint in_a1;
  uint in_a2;
  uint in_a3;
  
  return CONCAT44(in_a2 & in_a0,in_a3 & in_a1);
}



longlong __ashldi3(longlong a,uint shift)

{
  uint in_a1;
  uint in_a2;
  
  if (in_a2 == 0) {
    return CONCAT44(shift,in_a1);
  }
  if (0x1f < in_a2) {
    return (ulonglong)(in_a1 << (in_a2 & 0x1f)) << 0x20;
  }
  return CONCAT44(shift << (in_a2 & 0x1f) | in_a1 >> (-in_a2 & 0x1f),in_a1 << (in_a2 & 0x1f));
}



longlong __ashrdi3(longlong a,uint shift)

{
  uint in_a1;
  uint in_a2;
  
  if (in_a2 == 0) {
    return CONCAT44(shift,in_a1);
  }
  if (0x1f < in_a2) {
    return CONCAT44((int)shift >> 0x1f,(int)shift >> (in_a2 & 0x1f));
  }
  return CONCAT44((int)shift >> (in_a2 & 0x1f),in_a1 >> (in_a2 & 0x1f) | shift << (-in_a2 & 0x1f));
}



int __cmpdi2(longlong a,longlong b)

{
  int in_a0;
  uint in_a1;
  int in_a2;
  uint in_a3;
  
  if (in_a0 < in_a2) {
    return 0;
  }
  if (in_a2 < in_a0) {
    return 2;
  }
  if (in_a1 < in_a3) {
    return 0;
  }
  if (in_a3 < in_a1) {
    return 2;
  }
  return 1;
}



longlong __divdi3(longlong a,longlong b)

{
  bool bVar1;
  ulonglong *in_a0;
  int in_a1;
  int in_a2;
  undefined4 unaff_s0;
  undefined4 unaff_retaddr;
  ulonglong uVar2;
  uint in_stack_fffffff4;
  
  bVar1 = (int)in_a0 < 0;
  if (bVar1) {
    in_a0 = (ulonglong *)(-(uint)(in_a1 != 0) - (int)in_a0);
  }
  if (in_a2 < 0) {
    bVar1 = !bVar1;
  }
  uVar2 = __qdivrem((ulonglong)in_stack_fffffff4,CONCAT44(unaff_s0,unaff_retaddr),in_a0);
  if (bVar1) {
    uVar2 = CONCAT44(-(uint)(-(int)uVar2 != 0) - (int)(uVar2 >> 0x20),-(int)uVar2);
  }
  return uVar2;
}



longlong __iordi3(longlong a,longlong b)

{
  uint in_a0;
  uint in_a1;
  uint in_a2;
  uint in_a3;
  
  return CONCAT44(in_a2 | in_a0,in_a3 | in_a1);
}



longlong __lshldi3(longlong a,uint shift)

{
  uint in_a1;
  uint in_a2;
  
  if (in_a2 == 0) {
    return CONCAT44(shift,in_a1);
  }
  if (0x1f < in_a2) {
    return (ulonglong)(in_a1 << (in_a2 & 0x1f)) << 0x20;
  }
  return CONCAT44(shift << (in_a2 & 0x1f) | in_a1 >> (-in_a2 & 0x1f),in_a1 << (in_a2 & 0x1f));
}



longlong __lshrdi3(longlong a,uint shift)

{
  uint in_a1;
  uint in_a2;
  
  if (in_a2 == 0) {
    return CONCAT44(shift,in_a1);
  }
  if (0x1f < in_a2) {
    return (longlong)(shift >> (in_a2 & 0x1f));
  }
  return CONCAT44(shift >> (in_a2 & 0x1f),in_a1 >> (in_a2 & 0x1f) | shift << (-in_a2 & 0x1f));
}



longlong __moddi3(longlong a,longlong b)

{
  bool bVar1;
  ulonglong *in_a0;
  int in_a1;
  undefined4 in_stack_ffffffec;
  int in_stack_fffffff0;
  int in_stack_fffffff4;
  
  bVar1 = (int)in_a0 < 0;
  if (bVar1) {
    in_a0 = (ulonglong *)(-(uint)(in_a1 != 0) - (int)in_a0);
  }
  __qdivrem(CONCAT44(&stack0xfffffff0,in_stack_ffffffec),
            CONCAT44(in_stack_fffffff0,in_stack_fffffff4),in_a0);
  if (bVar1) {
    in_stack_fffffff4 = -in_stack_fffffff4;
    in_stack_fffffff0 = -(uint)(in_stack_fffffff4 != 0) - in_stack_fffffff0;
  }
  return CONCAT44(in_stack_fffffff0,in_stack_fffffff4);
}



longlong __lmulq(uint u,uint v)

{
  bool bVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  int iVar7;
  uint uVar8;
  
  uVar3 = u >> 0x10;
  uVar4 = u & 0xffff;
  uVar2 = v >> 0x10;
  uVar6 = v & 0xffff;
  uVar8 = uVar4 * uVar6;
  bVar1 = uVar3 < uVar4;
  if ((uVar3 != 0) || (uVar2 != 0)) {
    if (bVar1) {
      iVar5 = uVar4 - uVar3;
    }
    else {
      iVar5 = uVar3 - uVar4;
    }
    if (uVar6 < uVar2) {
      iVar7 = uVar2 - uVar6;
      bVar1 = !bVar1;
    }
    else {
      iVar7 = uVar6 - uVar2;
    }
    uVar4 = iVar5 * iVar7;
    uVar3 = uVar3 * uVar2;
    uVar2 = uVar3 * 0x10000;
    if (bVar1) {
      uVar6 = uVar2 + uVar4 * -0x10000;
      iVar5 = -((uVar4 >> 0x10) + (uint)(uVar2 < uVar6));
    }
    else {
      uVar6 = uVar4 * 0x10000 + uVar2;
      iVar5 = (uVar4 >> 0x10) + (uint)(uVar6 < uVar2);
    }
    uVar2 = uVar8 * 0x10000 + uVar6;
    uVar4 = uVar2 + uVar8;
    bVar1 = uVar4 < uVar8;
    uVar2 = (uVar8 >> 0x10) + (uint)(uVar2 < uVar6) + (uVar3 >> 0x10) + uVar3 + iVar5;
    uVar8 = uVar4;
    if (bVar1) {
      return CONCAT44(uVar2 + 1,uVar4);
    }
  }
  return CONCAT44(uVar2,uVar8);
}



longlong __muldi3(longlong a,longlong b)

{
  bool bVar1;
  bool bVar2;
  uint in_a0;
  uint in_a1;
  uint in_a2;
  uint in_a3;
  int iVar3;
  int iVar4;
  ulonglong uVar5;
  
  bVar1 = (int)in_a0 < 0;
  if (bVar1) {
    in_a1 = -in_a1;
    in_a0 = -(uint)(in_a1 != 0) - in_a0;
  }
  if ((int)in_a2 < 0) {
    in_a3 = -in_a3;
    in_a2 = -(uint)(in_a3 != 0) - in_a2;
    bVar1 = !bVar1;
  }
  if ((in_a0 == 0) && (in_a2 == 0)) {
    uVar5 = __lmulq(in_a1,in_a3);
  }
  else {
    uVar5 = __lmulq(in_a1,in_a3);
    bVar2 = in_a0 < in_a1;
    if (bVar2) {
      iVar4 = in_a1 - in_a0;
    }
    else {
      iVar4 = in_a0 - in_a1;
    }
    if (in_a3 < in_a2) {
      iVar3 = in_a2 - in_a3;
      bVar2 = !bVar2;
    }
    else {
      iVar3 = in_a3 - in_a2;
    }
    iVar4 = iVar4 * iVar3;
    if (bVar2) {
      iVar4 = -iVar4;
    }
    uVar5 = uVar5 & 0xffffffff |
            (ulonglong)(iVar4 + in_a0 * in_a2 + (int)uVar5 + (int)(uVar5 >> 0x20)) << 0x20;
  }
  if (bVar1) {
    uVar5 = CONCAT44(-(uint)(-(int)uVar5 != 0) - (int)(uVar5 >> 0x20),-(int)uVar5);
  }
  return uVar5;
}



longlong __negdi2(longlong a)

{
  int in_a0;
  int in_a1;
  
  return CONCAT44(-(uint)(-in_a1 != 0) - in_a0,-in_a1);
}



longlong __one_cmpldi2(longlong a)

{
  uint in_a0;
  uint in_a1;
  
  return CONCAT44(~in_a0,~in_a1);
}



void shl(digit *p,int len,int sh)

{
  int iVar1;
  digit *pdVar2;
  
  iVar1 = 0;
  while (iVar1 < len) {
    pdVar2 = p + iVar1;
    iVar1 = iVar1 + 1;
    *pdVar2 = *pdVar2 << (sh & 0x1fU) & 0xffff | p[iVar1] >> (0x10U - sh & 0x1f);
  }
  p[iVar1] = p[iVar1] << (sh & 0x1fU) & 0xffff;
  return;
}



ulonglong __qdivrem(ulonglong ull,ulonglong vll,ulonglong *arq)

{
  bool bVar1;
  uint uVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  ulonglong *in_a1;
  int iVar6;
  ulonglong *in_a2;
  ulonglong *in_a3;
  uint *puVar7;
  int iVar8;
  undefined *puVar9;
  int iVar10;
  digit *p;
  int iVar11;
  uint uVar12;
  uint uVar13;
  digit local_60;
  uint local_5c [4];
  undefined auStack76 [4];
  uint local_48 [5];
  int local_34;
  uint local_30;
  int local_2c;
  uint local_28;
  
  if (((uint)in_a2 | (uint)in_a3) == 0) {
    uVar12 = 1 / __qdivrem::lexical_block_0::zero;
    if (__qdivrem::lexical_block_0::zero == 0) {
      trap(0x1c00);
    }
    uVar4 = uVar12;
    if (ull._0_4_ != (ulonglong **)0x0) {
      *ull._0_4_ = arq;
      ull._0_4_[1] = in_a1;
    }
  }
  else if ((arq < in_a2) || ((in_a2 == arq && (in_a1 < in_a3)))) {
    uVar12 = 0;
    if (ull._0_4_ == (ulonglong **)0x0) {
      uVar4 = 0;
    }
    else {
      *ull._0_4_ = arq;
      ull._0_4_[1] = in_a1;
      uVar4 = 0;
    }
  }
  else {
    local_60 = 0;
    local_5c[0] = (uint)arq >> 0x10;
    local_5c[1] = (uint)arq & 0xffff;
    local_5c[2] = (uint)in_a1 >> 0x10;
    local_5c[3] = (uint)in_a1 & 0xffff;
    local_48[0] = (uint)in_a2 >> 0x10;
    local_48[1] = (uint)in_a2 & 0xffff;
    local_48[2] = (uint)in_a3 >> 0x10;
    local_48[3] = (uint)in_a3 & 0xffff;
    puVar9 = auStack76;
    iVar10 = 4;
    while( true ) {
      uVar4 = *(uint *)(puVar9 + 4);
      if (uVar4 != 0) break;
      if (iVar10 + -1 == 1) {
        uVar4 = *(uint *)(puVar9 + 8);
        if (uVar4 == 0) {
          trap(0x1c00);
        }
        uVar12 = (uint)arq & 0xffff | local_5c[0] % uVar4 << 0x10;
        if (uVar4 == 0) {
          trap(0x1c00);
        }
        uVar2 = (uint)in_a1 >> 0x10 | uVar12 % uVar4 << 0x10;
        if (uVar4 == 0) {
          trap(0x1c00);
        }
        uVar13 = (uint)in_a1 & 0xffff | uVar2 % uVar4 << 0x10;
        if (uVar4 == 0) {
          trap(0x1c00);
        }
        if (ull._0_4_ != (ulonglong **)0x0) {
          ull._0_4_[1] = (ulonglong *)(uVar13 % uVar4);
          *ull._0_4_ = (ulonglong *)0x0;
        }
        uVar12 = local_5c[0] / uVar4 << 0x10 | uVar12 / uVar4;
        uVar4 = uVar2 / uVar4 << 0x10 | uVar13 / uVar4;
        goto LAB_8001fff0;
      }
      puVar9 = puVar9 + 4;
      iVar10 = iVar10 + -1;
    }
    iVar11 = 4 - iVar10;
    for (p = &local_60; p[1] == 0; p = p + 1) {
      iVar11 = iVar11 + -1;
    }
    iVar6 = 4 - iVar11;
    while( true ) {
      if (iVar6 + -1 < 0) break;
      local_48[iVar6 + 3] = 0;
      iVar6 = iVar6 + -1;
    }
    uVar12 = 0;
    while (bVar1 = uVar4 < 0x8000, uVar4 = uVar4 << 1, bVar1) {
      uVar12 = uVar12 + 1;
    }
    if (0 < (int)uVar12) {
      shl(p,iVar11 + iVar10,uVar12);
      shl((digit *)(puVar9 + 4),iVar10 + -1,uVar12);
    }
    uVar4 = *(uint *)(puVar9 + 4);
    iVar8 = *(int *)(puVar9 + 8);
    iVar6 = 0;
    do {
      puVar7 = p + iVar6;
      uVar2 = p[iVar6 + 1];
      if (*puVar7 == uVar4) {
        uVar13 = 0x10000;
        goto qhat_too_big;
      }
      uVar2 = *puVar7 << 0x10 | uVar2;
      uVar13 = uVar2 / uVar4;
      uVar2 = uVar2 % uVar4;
      if (uVar4 == 0) {
        trap(0x1c00);
      }
      uVar5 = uVar2 << 0x10;
      while ((uVar5 | p[iVar6 + 2]) < iVar8 * uVar13) {
qhat_too_big:
        uVar2 = uVar2 + uVar4;
        uVar13 = uVar13 - 1;
        if (0xffff < uVar2) break;
        uVar5 = uVar2 * 0x10000;
      }
      uVar2 = 0;
      for (iVar3 = iVar10; 0 < iVar3; iVar3 = iVar3 + -1) {
        uVar2 = (p[iVar3 + iVar6] - *(int *)(puVar9 + iVar3 * 4) * uVar13) - uVar2;
        p[iVar3 + iVar6] = uVar2 & 0xffff;
        uVar2 = -(uVar2 >> 0x10) & 0xffff;
      }
      uVar5 = *puVar7;
      *puVar7 = uVar5 - uVar2 & 0xffff;
      if (uVar5 - uVar2 >> 0x10 != 0) {
        uVar13 = uVar13 - 1;
        uVar2 = 0;
        for (iVar3 = iVar10; 0 < iVar3; iVar3 = iVar3 + -1) {
          uVar2 = p[iVar3 + iVar6] + *(int *)(puVar9 + iVar3 * 4) + uVar2;
          p[iVar3 + iVar6] = uVar2 & 0xffff;
          uVar2 = uVar2 >> 0x10;
        }
        *puVar7 = *puVar7 + uVar2 & 0xffff;
      }
      iVar3 = iVar6 + 4;
      iVar6 = iVar6 + 1;
      local_48[(4 - iVar11) + iVar3] = uVar13;
    } while (iVar6 <= iVar11);
    if (ull._0_4_ != (ulonglong **)0x0) {
      iVar10 = iVar11 + iVar10;
      if (uVar12 != 0) {
        for (; iVar11 < iVar10; iVar10 = iVar10 + -1) {
          p[iVar10] = p[iVar10] >> (uVar12 & 0x1f) |
                      p[iVar10 + 0x3fffffff] << (0x10 - uVar12 & 0x1f) & 0xffff;
        }
        p[iVar10] = 0;
      }
      *ull._0_4_ = (ulonglong *)(local_5c[0] << 0x10 | local_5c[1]);
      ull._0_4_[1] = (ulonglong *)(local_5c[2] << 0x10 | local_5c[3]);
    }
    uVar12 = local_34 << 0x10 | local_30;
    uVar4 = local_2c << 0x10 | local_28;
  }
LAB_8001fff0:
  return CONCAT44(uVar12,uVar4);
}



longlong __subdi3(longlong a,longlong b)

{
  int in_a0;
  uint in_a1;
  int in_a2;
  int in_a3;
  
  return CONCAT44((in_a0 - in_a2) - (uint)(in_a1 < in_a1 - in_a3),in_a1 - in_a3);
}



int __ucmpdi2(ulonglong a,ulonglong b)

{
  uint in_a0;
  uint in_a1;
  uint in_a2;
  uint in_a3;
  
  if (in_a0 < in_a2) {
    return 0;
  }
  if (in_a2 < in_a0) {
    return 2;
  }
  if (in_a1 < in_a3) {
    return 0;
  }
  if (in_a3 < in_a1) {
    return 2;
  }
  return 1;
}



ulonglong __udivdi3(ulonglong a,ulonglong b)

{
  ulonglong *in_a0;
  undefined4 unaff_retaddr;
  ulonglong uVar1;
  uint in_stack_fffffff4;
  undefined4 in_stack_fffffff8;
  
  uVar1 = __qdivrem((ulonglong)in_stack_fffffff4,CONCAT44(in_stack_fffffff8,unaff_retaddr),in_a0);
  return uVar1;
}



ulonglong __umoddi3(ulonglong a,ulonglong b)

{
  ulonglong *in_a0;
  undefined4 in_stack_ffffffec;
  ulonglong in_stack_fffffff0;
  
  __qdivrem(CONCAT44(&stack0xfffffff0,in_stack_ffffffec),in_stack_fffffff0,in_a0);
  return in_stack_fffffff0;
}



longlong __xordi3(longlong a,longlong b)

{
  uint in_a0;
  uint in_a1;
  uint in_a2;
  uint in_a3;
  
  return CONCAT44(in_a2 ^ in_a0,in_a3 ^ in_a1);
}



int setjmp(__jmp_buf_tag *__env)

{
  int unaff_s0;
  int unaff_s1;
  int unaff_s2;
  int unaff_s3;
  int unaff_s4;
  ulong unaff_s5;
  ulong unaff_s6;
  ulong unaff_s7;
  ulong unaff_s8;
  int unaff_retaddr;
  undefined auStackX0 [16];
  
  __env->__jmpbuf[0] = (int)register0x00000074;
  __env->__jmpbuf[1] = unaff_retaddr;
  __env->__jmpbuf[2] = unaff_s0;
  __env->__jmpbuf[3] = unaff_s1;
  __env->__jmpbuf[4] = unaff_s2;
  __env->__jmpbuf[5] = unaff_s3;
  __env->__mask_was_saved = unaff_s4;
  (__env->__saved_mask).__val[0] = unaff_s5;
  (__env->__saved_mask).__val[1] = unaff_s6;
  (__env->__saved_mask).__val[2] = unaff_s7;
  (__env->__saved_mask).__val[3] = unaff_s8;
  return 0;
}



void longjmp(__jmp_buf_tag *__env,int __val)

{
  return;
}



int copycheck(const_userptr_t userptr,size_t len,size_t *stoplen)

{
  *stoplen = len;
  if (userptr + (len - 1) < userptr) {
    return 6;
  }
  if (-1 < (int)userptr) {
    if ((int)(userptr + (len - 1)) < 0) {
      *stoplen = 0x80000000 - (int)userptr;
      return 0;
    }
    return 0;
  }
  return 6;
}



int copystr(char *dest,char *src,size_t maxlen,size_t stoplen,size_t *gotlen)

{
  uint uVar1;
  uint uVar2;
  
  uVar1 = 0;
  while ((uVar2 = uVar1, uVar2 < maxlen && (uVar2 < stoplen))) {
    dest[uVar2] = src[uVar2];
    uVar1 = uVar2 + 1;
    if (src[uVar2] == '\0') {
      if (gotlen != (size_t *)0x0) {
        *gotlen = uVar2 + 1;
        return 0;
      }
      return 0;
    }
  }
  if (stoplen < maxlen) {
    return 6;
  }
  return 7;
}



void copyfail(void)

{
  int unaff_s7;
  
                    /* WARNING: Subroutine does not return */
  longjmp((__jmp_buf_tag *)(unaff_s7 + 0x10),1);
}



int copyin(const_userptr_t usersrc,void *dest,size_t len)

{
  int iVar1;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(usersrc,len,&stoplen);
  if ((iVar1 == 0) && (iVar1 = 6, stoplen == len)) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar1 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    if (iVar1 == 0) {
      memcpy(dest,usersrc,len);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 6;
    }
  }
  return iVar1;
}



int copyout(void *src,userptr_t userdest,size_t len)

{
  int iVar1;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(userdest,len,&stoplen);
  if ((iVar1 == 0) && (iVar1 = 6, stoplen == len)) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar1 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    if (iVar1 == 0) {
      memcpy(userdest,src,len);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 6;
    }
  }
  return iVar1;
}



int copyinstr(const_userptr_t usersrc,char *dest,size_t len,size_t *actual)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(usersrc,len,&stoplen);
  if (iVar1 == 0) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar2 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    iVar1 = 6;
    if (iVar2 == 0) {
      iVar1 = copystr(dest,&usersrc->_dummy,len,stoplen,actual);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
  }
  return iVar1;
}



int copyoutstr(char *src,userptr_t userdest,size_t len,size_t *actual)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(userdest,len,&stoplen);
  if (iVar1 == 0) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar2 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    iVar1 = 6;
    if (iVar2 == 0) {
      iVar1 = copystr(&userdest->_dummy,src,len,stoplen,actual);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
  }
  return iVar1;
}



void mips_flushicache(void)

{
  return;
}



void mips_utlb_handler(undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4)

{
  undefined4 in_at;
  undefined4 in_v0;
  undefined4 in_v1;
  undefined4 in_t0;
  undefined4 in_t1;
  undefined4 in_t2;
  undefined4 in_t3;
  undefined4 in_t4;
  undefined4 in_t5;
  undefined4 in_t6;
  undefined4 in_t7;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  undefined4 unaff_s3;
  undefined4 unaff_s4;
  undefined4 unaff_s5;
  undefined4 unaff_s6;
  undefined4 unaff_s7;
  undefined4 in_t8;
  undefined4 in_t9;
  undefined *puVar1;
  undefined4 unaff_s8;
  undefined4 unaff_retaddr;
  undefined4 in_hi;
  undefined4 in_lo;
  undefined auStackX0 [16];
  
  puVar1 = (undefined *)register0x00000074;
  if ((Status & 8) != 0) {
    puVar1 = (undefined *)cpustacks[Context >> 0x15];
  }
  *(undefined4 *)(puVar1 + -0xc) = unaff_s8;
  *(BADSPACEBASE **)(puVar1 + -0x10) = register0x00000074;
  *(undefined4 *)(puVar1 + -0x14) = 0x80030ba0;
  *(undefined4 *)(puVar1 + -8) = EPC;
  *(undefined4 *)(puVar1 + -0x18) = in_t9;
  *(undefined4 *)(puVar1 + -0x1c) = in_t8;
  *(undefined4 *)(puVar1 + -0x20) = unaff_s7;
  *(undefined4 *)(puVar1 + -0x24) = unaff_s6;
  *(undefined4 *)(puVar1 + -0x28) = unaff_s5;
  *(undefined4 *)(puVar1 + -0x2c) = unaff_s4;
  *(undefined4 *)(puVar1 + -0x30) = unaff_s3;
  *(undefined4 *)(puVar1 + -0x34) = unaff_s2;
  *(undefined4 *)(puVar1 + -0x38) = unaff_s1;
  *(undefined4 *)(puVar1 + -0x3c) = unaff_s0;
  *(undefined4 *)(puVar1 + -0x40) = in_t7;
  *(undefined4 *)(puVar1 + -0x44) = in_t6;
  *(undefined4 *)(puVar1 + -0x48) = in_t5;
  *(undefined4 *)(puVar1 + -0x4c) = in_t4;
  *(undefined4 *)(puVar1 + -0x50) = in_t3;
  *(undefined4 *)(puVar1 + -0x54) = in_t2;
  *(undefined4 *)(puVar1 + -0x58) = in_t1;
  *(undefined4 *)(puVar1 + -0x5c) = in_t0;
  *(undefined4 *)(puVar1 + -0x60) = param_4;
  *(undefined4 *)(puVar1 + -100) = param_3;
  *(undefined4 *)(puVar1 + -0x68) = param_2;
  *(undefined4 *)(puVar1 + -0x6c) = param_1;
  *(undefined4 *)(puVar1 + -0x70) = in_v1;
  *(undefined4 *)(puVar1 + -0x74) = in_v0;
  *(undefined4 *)(puVar1 + -0x78) = in_at;
  *(undefined4 *)(puVar1 + -0x7c) = unaff_retaddr;
  *(undefined4 *)(puVar1 + -0x80) = in_hi;
  *(undefined4 *)(puVar1 + -0x84) = in_lo;
  *(uint *)(puVar1 + -0x8c) = Status;
  *(undefined4 *)(puVar1 + -0x90) = BadVAddr;
  *(undefined4 *)(puVar1 + -0x88) = Cause;
  mips_trap((trapframe *)(puVar1 + -0x90));
  setCopReg(0,Status,*(undefined4 *)(puVar1 + -0x8c),0);
                    /* WARNING: Could not recover jumptable at 0x80020650. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  Status = Status & 0xfffffff0 | (Status & 0x3c) >> 2;
  (**(code **)(puVar1 + -8))
            (*(undefined4 *)(puVar1 + -0x6c),*(undefined4 *)(puVar1 + -0x68),
             *(undefined4 *)(puVar1 + -100),*(undefined4 *)(puVar1 + -0x60));
  return;
}



void mips_general_handler
               (undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4)

{
  undefined4 in_at;
  undefined4 in_v0;
  undefined4 in_v1;
  undefined4 in_t0;
  undefined4 in_t1;
  undefined4 in_t2;
  undefined4 in_t3;
  undefined4 in_t4;
  undefined4 in_t5;
  undefined4 in_t6;
  undefined4 in_t7;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  undefined4 unaff_s3;
  undefined4 unaff_s4;
  undefined4 unaff_s5;
  undefined4 unaff_s6;
  undefined4 unaff_s7;
  undefined4 in_t8;
  undefined4 in_t9;
  undefined *puVar1;
  undefined4 unaff_s8;
  undefined4 unaff_retaddr;
  undefined4 in_hi;
  undefined4 in_lo;
  undefined auStackX0 [16];
  
  puVar1 = (undefined *)register0x00000074;
  if ((Status & 8) != 0) {
    puVar1 = (undefined *)cpustacks[Context >> 0x15];
  }
  *(undefined4 *)(puVar1 + -0xc) = unaff_s8;
  *(BADSPACEBASE **)(puVar1 + -0x10) = register0x00000074;
  *(undefined4 *)(puVar1 + -0x14) = 0x80030ba0;
  *(undefined4 *)(puVar1 + -8) = EPC;
  *(undefined4 *)(puVar1 + -0x18) = in_t9;
  *(undefined4 *)(puVar1 + -0x1c) = in_t8;
  *(undefined4 *)(puVar1 + -0x20) = unaff_s7;
  *(undefined4 *)(puVar1 + -0x24) = unaff_s6;
  *(undefined4 *)(puVar1 + -0x28) = unaff_s5;
  *(undefined4 *)(puVar1 + -0x2c) = unaff_s4;
  *(undefined4 *)(puVar1 + -0x30) = unaff_s3;
  *(undefined4 *)(puVar1 + -0x34) = unaff_s2;
  *(undefined4 *)(puVar1 + -0x38) = unaff_s1;
  *(undefined4 *)(puVar1 + -0x3c) = unaff_s0;
  *(undefined4 *)(puVar1 + -0x40) = in_t7;
  *(undefined4 *)(puVar1 + -0x44) = in_t6;
  *(undefined4 *)(puVar1 + -0x48) = in_t5;
  *(undefined4 *)(puVar1 + -0x4c) = in_t4;
  *(undefined4 *)(puVar1 + -0x50) = in_t3;
  *(undefined4 *)(puVar1 + -0x54) = in_t2;
  *(undefined4 *)(puVar1 + -0x58) = in_t1;
  *(undefined4 *)(puVar1 + -0x5c) = in_t0;
  *(undefined4 *)(puVar1 + -0x60) = param_4;
  *(undefined4 *)(puVar1 + -100) = param_3;
  *(undefined4 *)(puVar1 + -0x68) = param_2;
  *(undefined4 *)(puVar1 + -0x6c) = param_1;
  *(undefined4 *)(puVar1 + -0x70) = in_v1;
  *(undefined4 *)(puVar1 + -0x74) = in_v0;
  *(undefined4 *)(puVar1 + -0x78) = in_at;
  *(undefined4 *)(puVar1 + -0x7c) = unaff_retaddr;
  *(undefined4 *)(puVar1 + -0x80) = in_hi;
  *(undefined4 *)(puVar1 + -0x84) = in_lo;
  *(uint *)(puVar1 + -0x8c) = Status;
  *(undefined4 *)(puVar1 + -0x90) = BadVAddr;
  *(undefined4 *)(puVar1 + -0x88) = Cause;
  mips_trap((trapframe *)(puVar1 + -0x90));
  setCopReg(0,Status,*(undefined4 *)(puVar1 + -0x8c),0);
                    /* WARNING: Could not recover jumptable at 0x80020650. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  Status = Status & 0xfffffff0 | (Status & 0x3c) >> 2;
  (**(code **)(puVar1 + -8))
            (*(undefined4 *)(puVar1 + -0x6c),*(undefined4 *)(puVar1 + -0x68),
             *(undefined4 *)(puVar1 + -100),*(undefined4 *)(puVar1 + -0x60));
  return;
}



void mips_general_end(undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4)

{
  undefined4 in_at;
  undefined4 in_v0;
  undefined4 in_v1;
  undefined4 in_t0;
  undefined4 in_t1;
  undefined4 in_t2;
  undefined4 in_t3;
  undefined4 in_t4;
  undefined4 in_t5;
  undefined4 in_t6;
  undefined4 in_t7;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  undefined4 unaff_s3;
  undefined4 unaff_s4;
  undefined4 unaff_s5;
  undefined4 unaff_s6;
  undefined4 unaff_s7;
  undefined4 in_t8;
  undefined4 in_t9;
  undefined *puVar1;
  undefined4 unaff_s8;
  undefined4 unaff_retaddr;
  undefined4 in_hi;
  undefined4 in_lo;
  undefined auStackX0 [16];
  
  puVar1 = (undefined *)register0x00000074;
  if ((Status & 8) != 0) {
    puVar1 = (undefined *)cpustacks[Context >> 0x15];
  }
  *(undefined4 *)(puVar1 + -0xc) = unaff_s8;
  *(BADSPACEBASE **)(puVar1 + -0x10) = register0x00000074;
  *(undefined4 *)(puVar1 + -0x14) = 0x80030ba0;
  *(undefined4 *)(puVar1 + -8) = EPC;
  *(undefined4 *)(puVar1 + -0x18) = in_t9;
  *(undefined4 *)(puVar1 + -0x1c) = in_t8;
  *(undefined4 *)(puVar1 + -0x20) = unaff_s7;
  *(undefined4 *)(puVar1 + -0x24) = unaff_s6;
  *(undefined4 *)(puVar1 + -0x28) = unaff_s5;
  *(undefined4 *)(puVar1 + -0x2c) = unaff_s4;
  *(undefined4 *)(puVar1 + -0x30) = unaff_s3;
  *(undefined4 *)(puVar1 + -0x34) = unaff_s2;
  *(undefined4 *)(puVar1 + -0x38) = unaff_s1;
  *(undefined4 *)(puVar1 + -0x3c) = unaff_s0;
  *(undefined4 *)(puVar1 + -0x40) = in_t7;
  *(undefined4 *)(puVar1 + -0x44) = in_t6;
  *(undefined4 *)(puVar1 + -0x48) = in_t5;
  *(undefined4 *)(puVar1 + -0x4c) = in_t4;
  *(undefined4 *)(puVar1 + -0x50) = in_t3;
  *(undefined4 *)(puVar1 + -0x54) = in_t2;
  *(undefined4 *)(puVar1 + -0x58) = in_t1;
  *(undefined4 *)(puVar1 + -0x5c) = in_t0;
  *(undefined4 *)(puVar1 + -0x60) = param_4;
  *(undefined4 *)(puVar1 + -100) = param_3;
  *(undefined4 *)(puVar1 + -0x68) = param_2;
  *(undefined4 *)(puVar1 + -0x6c) = param_1;
  *(undefined4 *)(puVar1 + -0x70) = in_v1;
  *(undefined4 *)(puVar1 + -0x74) = in_v0;
  *(undefined4 *)(puVar1 + -0x78) = in_at;
  *(undefined4 *)(puVar1 + -0x7c) = unaff_retaddr;
  *(undefined4 *)(puVar1 + -0x80) = in_hi;
  *(undefined4 *)(puVar1 + -0x84) = in_lo;
  *(uint *)(puVar1 + -0x8c) = Status;
  *(undefined4 *)(puVar1 + -0x90) = BadVAddr;
  *(undefined4 *)(puVar1 + -0x88) = Cause;
  mips_trap((trapframe *)(puVar1 + -0x90));
  setCopReg(0,Status,*(undefined4 *)(puVar1 + -0x8c),0);
                    /* WARNING: Could not recover jumptable at 0x80020650. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  Status = Status & 0xfffffff0 | (Status & 0x3c) >> 2;
  (**(code **)(puVar1 + -8))
            (*(undefined4 *)(puVar1 + -0x6c),*(undefined4 *)(puVar1 + -0x68),
             *(undefined4 *)(puVar1 + -100),*(undefined4 *)(puVar1 + -0x60));
  return;
}



void common_exception(undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4)

{
  undefined4 in_at;
  undefined4 in_v0;
  undefined4 in_v1;
  undefined4 in_t0;
  undefined4 in_t1;
  undefined4 in_t2;
  undefined4 in_t3;
  undefined4 in_t4;
  undefined4 in_t5;
  undefined4 in_t6;
  undefined4 in_t7;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  undefined4 unaff_s3;
  undefined4 unaff_s4;
  undefined4 unaff_s5;
  undefined4 unaff_s6;
  undefined4 unaff_s7;
  undefined4 in_t8;
  undefined4 in_t9;
  undefined *puVar1;
  undefined4 unaff_s8;
  undefined4 unaff_retaddr;
  undefined4 in_hi;
  undefined4 in_lo;
  undefined auStackX0 [16];
  
  puVar1 = (undefined *)register0x00000074;
  if ((Status & 8) != 0) {
    puVar1 = (undefined *)cpustacks[Context >> 0x15];
  }
  *(undefined4 *)(puVar1 + -0xc) = unaff_s8;
  *(BADSPACEBASE **)(puVar1 + -0x10) = register0x00000074;
  *(BADSPACEBASE **)(puVar1 + -0x14) = register0x00000070;
  *(undefined4 *)(puVar1 + -8) = EPC;
  *(undefined4 *)(puVar1 + -0x18) = in_t9;
  *(undefined4 *)(puVar1 + -0x1c) = in_t8;
  *(undefined4 *)(puVar1 + -0x20) = unaff_s7;
  *(undefined4 *)(puVar1 + -0x24) = unaff_s6;
  *(undefined4 *)(puVar1 + -0x28) = unaff_s5;
  *(undefined4 *)(puVar1 + -0x2c) = unaff_s4;
  *(undefined4 *)(puVar1 + -0x30) = unaff_s3;
  *(undefined4 *)(puVar1 + -0x34) = unaff_s2;
  *(undefined4 *)(puVar1 + -0x38) = unaff_s1;
  *(undefined4 *)(puVar1 + -0x3c) = unaff_s0;
  *(undefined4 *)(puVar1 + -0x40) = in_t7;
  *(undefined4 *)(puVar1 + -0x44) = in_t6;
  *(undefined4 *)(puVar1 + -0x48) = in_t5;
  *(undefined4 *)(puVar1 + -0x4c) = in_t4;
  *(undefined4 *)(puVar1 + -0x50) = in_t3;
  *(undefined4 *)(puVar1 + -0x54) = in_t2;
  *(undefined4 *)(puVar1 + -0x58) = in_t1;
  *(undefined4 *)(puVar1 + -0x5c) = in_t0;
  *(undefined4 *)(puVar1 + -0x60) = param_4;
  *(undefined4 *)(puVar1 + -100) = param_3;
  *(undefined4 *)(puVar1 + -0x68) = param_2;
  *(undefined4 *)(puVar1 + -0x6c) = param_1;
  *(undefined4 *)(puVar1 + -0x70) = in_v1;
  *(undefined4 *)(puVar1 + -0x74) = in_v0;
  *(undefined4 *)(puVar1 + -0x78) = in_at;
  *(undefined4 *)(puVar1 + -0x7c) = unaff_retaddr;
  *(undefined4 *)(puVar1 + -0x80) = in_hi;
  *(undefined4 *)(puVar1 + -0x84) = in_lo;
  *(uint *)(puVar1 + -0x8c) = Status;
  *(undefined4 *)(puVar1 + -0x90) = BadVAddr;
  *(undefined4 *)(puVar1 + -0x88) = Cause;
  mips_trap((trapframe *)(puVar1 + -0x90));
  setCopReg(0,Status,*(undefined4 *)(puVar1 + -0x8c),0);
                    /* WARNING: Could not recover jumptable at 0x80020650. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  Status = Status & 0xfffffff0 | (Status & 0x3c) >> 2;
  (**(code **)(puVar1 + -8))
            (*(undefined4 *)(puVar1 + -0x6c),*(undefined4 *)(puVar1 + -0x68),
             *(undefined4 *)(puVar1 + -100),*(undefined4 *)(puVar1 + -0x60));
  return;
}



void asm_usermode(int param_1)

{
  setCopReg(0,Status,*(undefined4 *)(param_1 + 4),0);
                    /* WARNING: Could not recover jumptable at 0x80020650. Too many branches */
                    /* WARNING: Treating indirect jump as call */
  Status = Status & 0xfffffff0 | (Status & 0x3c) >> 2;
  (**(code **)(param_1 + 0x88))
            (*(undefined4 *)(param_1 + 0x24),*(undefined4 *)(param_1 + 0x28),
             *(undefined4 *)(param_1 + 0x2c),*(undefined4 *)(param_1 + 0x30));
  return;
}



void tlb_random(undefined4 param_1,undefined4 param_2)

{
  setCopReg(0,EntryHi,param_1,0);
  setCopReg(0,EntryLo0,param_2,0);
  TLB_write_random_entry(Random,EntryHi,EntryLo0,EntryLo1,PageMask);
  return;
}



void tlb_write(undefined4 param_1,undefined4 param_2,int param_3)

{
  setCopReg(0,EntryHi,param_1,0);
  setCopReg(0,EntryLo0,param_2,0);
  setCopReg(0,Index,param_3 << 8,0);
  TLB_write_indexed_entry(Index,EntryHi,EntryLo0,EntryLo1,PageMask);
  return;
}



void tlb_read(undefined4 *param_1,undefined4 *param_2,int param_3)

{
  setCopReg(0,Index,param_3 << 8,0);
  EntryHi = TLB_read_indexed_entryHi(Index);
  EntryLo0 = TLB_read_indexed_entryLo0(Index);
  EntryLo1 = TLB_read_indexed_entryLo1(Index);
  PageMask = TLB_read_indexed_entryPageMask(Index);
  *param_1 = EntryHi;
  *param_2 = EntryLo0;
  return;
}



int tlb_probe(undefined4 param_1,undefined4 param_2)

{
  setCopReg(0,EntryHi,param_1,0);
  setCopReg(0,EntryLo0,param_2,0);
  Index = TLB_probe_for_matching_entry(EntryHi);
  if ((int)Index < 0) {
    return -1;
  }
  return (int)(Index & 0x3f00) >> 8;
}



void tlb_reset(void)

{
  int iVar1;
  int iVar2;
  
  iVar1 = 0;
  iVar2 = -0x7f000000;
  do {
    do {
      setCopReg(0,EntryLo0,0,0);
      setCopReg(0,EntryHi,iVar2,0);
      Index = TLB_probe_for_matching_entry(EntryHi);
      iVar2 = iVar2 + 0x1000;
    } while (-1 < Index);
    setCopReg(0,Index,iVar1,0);
    iVar1 = iVar1 + 0x100;
    TLB_write_indexed_entry(Index,EntryHi,EntryLo0,EntryLo1,PageMask);
  } while (iVar1 != 0x4000);
  return;
}



void mips_timer_set(uint32_t count)

{
  setCopReg(0,Compare,count,0);
  return;
}



void mainbus_bootstrap(void)

{
  int unaff_s7;
  
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    badassert("curthread->t_curspl > 0","../../arch/sys161/dev/lamebus_machdep.c",0x5a,
              "mainbus_bootstrap");
  }
  lamebus = lamebus_init();
  lamebus_find_cpus(lamebus);
  kprintf("lamebus0 (system main bus)\n");
  splx(0);
  autoconf_lamebus(lamebus,0);
  mips_timer_set(250000);
  return;
}



void mainbus_start_cpus(void)

{
  lamebus_start_cpus(lamebus);
  return;
}



void * lamebus_map_area(lamebus_softc *bus,int slot,uint32_t offset)

{
  char *pcVar1;
  
  pcVar1 = (char *)(slot << 0x10);
  if (0x1f < (uint)slot) {
    pcVar1 = "../../arch/sys161/dev/lamebus_machdep.c";
    offset = 0x8f;
    badassert("slot >= 0 && slot < LB_NSLOTS","../../arch/sys161/dev/lamebus_machdep.c",0x8f,
              "lamebus_map_area");
  }
  return pcVar1 + offset + 0xbfe00000;
}



uint32_t lamebus_read_register(lamebus_softc *bus,int slot,uint32_t offset)

{
  uint32_t *puVar1;
  
  puVar1 = (uint32_t *)lamebus_map_area(bus,slot,offset);
  SYNC(0);
  return *puVar1;
}



void lamebus_write_register(lamebus_softc *bus,int slot,uint32_t offset,uint32_t val)

{
  uint32_t *puVar1;
  
  puVar1 = (uint32_t *)lamebus_map_area(bus,slot,offset);
  *puVar1 = val;
  SYNC(0);
  return;
}



void mainbus_poweroff(void)

{
  lamebus_poweroff(lamebus);
  return;
}



void mainbus_reboot(void)

{
  kprintf("Cannot reboot - powering off instead, sorry.\n");
  mainbus_poweroff();
  return;
}



void mainbus_halt(void)

{
  cpu_halt();
  return;
}



void mainbus_panic(void)

{
  mainbus_poweroff();
  return;
}



uint32_t mainbus_ramsize(void)

{
  uint32_t uVar1;
  
  uVar1 = lamebus_ramsize();
  if (0x1fc00000 < uVar1) {
    uVar1 = 0x1fc00000;
  }
  return uVar1;
}



void mainbus_send_ipi(cpu *target)

{
  lamebus_assert_ipi(lamebus,target);
  return;
}



void mainbus_debugger(void)

{
  ltrace_stop(0);
  return;
}



void mainbus_interrupt(trapframe *tf)

{
  bool bVar1;
  bool bVar2;
  bool bVar3;
  char *pcVar4;
  uint uVar5;
  int unaff_s7;
  
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    pcVar4 = "curthread->t_curspl > 0";
    badassert("curthread->t_curspl > 0","../../arch/sys161/dev/lamebus_machdep.c",0x130,
              "mainbus_interrupt");
    tf = (trapframe *)pcVar4;
  }
  uVar5 = tf->tf_cause;
  bVar1 = (uVar5 & 0x400) == 0;
  if (!bVar1) {
    lamebus_interrupt(lamebus);
  }
  bVar2 = (uVar5 & 0x800) != 0;
  if (bVar2) {
    interprocessor_interrupt();
    lamebus_clear_ipi(lamebus,*(cpu **)(unaff_s7 + 0x50));
  }
  bVar3 = (uVar5 & 0x8000) != 0;
  if (bVar3) {
    mips_timer_set(250000);
    hardclock();
  }
  if ((!bVar3 && (!bVar2 && bVar1)) && ((uVar5 & 0xff00) != 0)) {
                    /* WARNING: Subroutine does not return */
    panic("Unknown interrupt; cause register is %08x\n",uVar5);
  }
  return;
}



void __start(char *param_1)

{
  size_t sVar1;
  uint uVar2;
  
  strcpy(&_end,param_1);
  sVar1 = strlen(&_end);
  uVar2 = sVar1 + 0x80037940 & 0xfffff000;
  firstfree = uVar2 + 0x1000;
  *(undefined4 *)(uVar2 + 0xffc) = 0;
  memmove(&Elf32_Ehdr_80000000,mips_utlb_handler,8);
  memmove(&DAT_80000080,mips_general_handler,8);
  mips_flushicache();
  tlb_reset();
  setCopReg(0,Status,0xff00,0);
  setCopReg(0,Context,0,0);
  kmain(&_end);
                    /* WARNING: Subroutine does not return */
  panic("kmain returned\n");
}



void cpu_start_secondary(uint param_1)

{
  vaddr_t vVar1;
  
  vVar1 = cpustacks[param_1];
  tlb_reset();
  setCopReg(0,Status,0xff00,0);
  setCopReg(0,Context,param_1 << 0x15,0);
  setCopReg(0,Compare,100000,0);
  *(undefined4 *)(vVar1 - 4) = 0;
  cpu_hatch(param_1);
  return;
}


