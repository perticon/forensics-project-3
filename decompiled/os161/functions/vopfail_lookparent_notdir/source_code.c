vopfail_lookparent_notdir(struct vnode *vn, char *path, struct vnode **result,
			  char *buf, size_t len)
{
	(void)vn;
	(void)path;
	(void)result;
	(void)buf;
	(void)len;
	return ENOTDIR;
}