static struct swap_entry *get_free_entry(void)
{
    struct swap_entry *tmp;

    tmp = free_list_head->next;
    if (tmp == free_list_tail)
    {
        panic("no free entry in swaptable");
    }
    free_list_head->next = tmp->next;

    return tmp;
}