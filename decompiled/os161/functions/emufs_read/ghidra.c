int emufs_read(vnode *v,uio *uio)

{
  int iVar1;
  uint len;
  uint unaff_s0;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  if (uio->uio_rw == UIO_READ) goto LAB_80002e8c;
  badassert("uio->uio_rw==UIO_READ",s_______dev_lamebus_emu_c_80022750,0x224,"emufs_read");
  do {
    len = unaff_s0;
    if (0x4000 < unaff_s0) {
      len = 0x4000;
    }
    iVar1 = emu_read(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),len,uio);
    if (iVar1 != 0) {
      return iVar1;
    }
    if (unaff_s0 == uio->uio_resid) {
      return 0;
    }
LAB_80002e8c:
    unaff_s0 = uio->uio_resid;
  } while (unaff_s0 != 0);
  return 0;
}