int32_t emufs_read(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2_ = *(int32_t *)((int32_t)v2 + 16); // 0x80002e20
    int32_t v3; // 0x80002e0c
    if (*(int32_t *)(v1 + 24) == 0) {
        // 0x80002e0c
        v3 = *(int32_t *)(v1 + 16);
        goto lab_0x80002e8c;
    } else {
        // 0x80002e34
        badassert("uio->uio_rw==UIO_READ", "../../dev/lamebus/emu.c", 548, "emufs_read");
        goto lab_0x80002e54;
    }
  lab_0x80002e8c:;
    int32_t v4 = v3; // 0x80002e98
    if (v3 == 0) {
        // 0x80002e9c
        return 0;
    }
    goto lab_0x80002e54;
  lab_0x80002e54:;
    uint32_t v5 = v4;
    int32_t v6 = *(int32_t *)(v2_ + 24); // 0x80002e64
    int32_t v7 = *(int32_t *)(v2_ + 28); // 0x80002e68
    int32_t v8 = emu_read((int32_t *)v6, v7, v5 < 0x4000 ? v5 : 0x4000, uio); // 0x80002e70
    int32_t result = v8; // 0x80002e78
    if (v8 != 0) {
        // 0x80002e9c
        return result;
    }
    int32_t v9 = *(int32_t *)(v1 + 16); // 0x80002e7c
    v3 = v9;
    if (v5 == v9) {
        // 0x80002e9c
        return 0;
    }
    goto lab_0x80002e8c;
}