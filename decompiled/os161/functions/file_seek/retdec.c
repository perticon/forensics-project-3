int32_t file_seek(int32_t * of, int64_t offset, int32_t whence, int32_t * errcode) {
    // 0x8000d4e8
    int32_t v1; // 0x8000d4e8
    if (of == NULL || !filedes_is_seekable(of)) {
        // 0x8000d518
        *(int32_t *)v1 = 30;
        // 0x8000d590
        return -1;
    }
    if (errcode != NULL) {
        // 0x8000d550
        *(int32_t *)v1 = 8;
        kprintf("Error in file_seek: different seek start position not implemented\n");
        // 0x8000d590
        return -1;
    }
    int32_t v2 = offset;
    int32_t result; // 0x8000d4e8
    if (v2 > -1) {
        int32_t v3 = (int32_t)of;
        *(int32_t *)(v3 + 8) = v2;
        *(int32_t *)(v3 + 12) = whence;
        result = 0;
    } else {
        // 0x8000d570
        *(int32_t *)v1 = 8;
        result = -1;
    }
    // 0x8000d590
    return result;
}