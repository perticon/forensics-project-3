int file_seek(openfile *of,off_t offset,int whence,int *errcode)

{
  undefined3 extraout_var;
  bool bVar2;
  int iVar1;
  undefined4 in_a3;
  
  if ((of == (openfile *)0x0) ||
     (bVar2 = filedes_is_seekable(of), CONCAT31(extraout_var,bVar2) == 0)) {
    *offset._4_4_ = 0x1e;
    iVar1 = -1;
  }
  else if (offset._0_4_ == 0) {
    if ((int)errcode < 0) {
      *offset._4_4_ = 8;
      iVar1 = -1;
    }
    else {
      *(int **)&of->offset = errcode;
      *(undefined4 *)((int)&of->offset + 4) = in_a3;
      iVar1 = 0;
    }
  }
  else {
    *offset._4_4_ = 8;
    kprintf("Error in file_seek: different seek start position not implemented\n");
    iVar1 = -1;
  }
  return iVar1;
}