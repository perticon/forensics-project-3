int file_seek(struct openfile *of, off_t offset, int whence, int *errcode) {
	if (!of || !filedes_is_seekable(of)) {
		*errcode = EBADF;
		return -1;
	}
	off_t new_offset = offset;

	switch(whence) {
  case SEEK_SET:
		new_offset = offset;
		break;
	default:
		*errcode = EINVAL;
    kprintf("Error in file_seek: different seek start position not implemented\n");
		return -1;
	}

	if (new_offset < 0) {
		*errcode = EINVAL;
		return -1;
	}
	DEBUGASSERT(of->offset >= 0);
	of->offset = new_offset;
	return 0;
}