int32_t cmd_bootfs(int32_t nargs, char ** args) {
    if (nargs != 2) {
        // 0x8000c5a8
        kprintf("Usage: bootfs device\n");
        // 0x8000c5f4
        return 8;
    }
    int32_t v1 = *(int32_t *)((int32_t)args + 4); // 0x8000c5bc
    char * v2 = (char *)v1; // 0x8000c5c4
    int32_t v3 = strlen(v2); // 0x8000c5c4
    int32_t v4 = v1 - 1;
    if (*(char *)(v4 + v3) == 58) {
        // 0x8000c5dc
        *(char *)(strlen(v2) + v4) = 0;
    }
    // 0x8000c5f4
    return vfs_setbootfs(v2);
}