cmd_bootfs(int nargs, char **args)
{
	char *device;

	if (nargs != 2)
	{
		kprintf("Usage: bootfs device\n");
		return EINVAL;
	}

	device = args[1];

	/* Allow (but do not require) colon after device name */
	if (device[strlen(device) - 1] == ':')
	{
		device[strlen(device) - 1] = 0;
	}

	return vfs_setbootfs(device);
}