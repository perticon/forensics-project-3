int cmd_bootfs(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  char *str;
  
  if (nargs == 2) {
    str = args[1];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
    }
    iVar1 = vfs_setbootfs(str);
  }
  else {
    kprintf("Usage: bootfs device\n");
    iVar1 = 8;
  }
  return iVar1;
}