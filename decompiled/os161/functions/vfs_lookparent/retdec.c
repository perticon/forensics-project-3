int32_t vfs_lookparent(char * path, int32_t * retval, char * buf, int32_t buflen) {
    char * v1 = path; // 0x80019e8c
    vfs_biglock_acquire();
    int32_t * startvn; // bp-32, 0x80019e74
    int32_t result = getdevice(path, &v1, (int32_t *)&startvn); // 0x80019eac
    if (result != 0) {
        // 0x80019eb8
        vfs_biglock_release();
        // 0x80019f34
        return result;
    }
    int32_t * v2 = startvn; // 0x80019ed8
    int32_t result2 = 8; // 0x80019ed8
    if (strlen(v1) != 0) {
        // 0x80019edc
        vnode_check(startvn, "lookparent");
        v2 = startvn;
        result2 = *(int32_t *)(*(int32_t *)((int32_t)v2 + 20) + 92);
    }
    // 0x80019f1c
    vnode_decref(v2);
    vfs_biglock_release();
    // 0x80019f34
    return result2;
}