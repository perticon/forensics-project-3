int vfs_lookparent(char *path,vnode **retval,char *buf,size_t buflen)

{
  int iVar1;
  size_t sVar2;
  char *local_res0;
  vnode *startvn;
  
  local_res0 = path;
  vfs_biglock_acquire();
  iVar1 = getdevice(local_res0,(char **)register0x00000074,&startvn);
  if (iVar1 == 0) {
    sVar2 = strlen(local_res0);
    if (sVar2 == 0) {
      iVar1 = 8;
    }
    else {
      vnode_check(startvn,"lookparent");
      iVar1 = (*startvn->vn_ops->vop_lookparent)(startvn,local_res0,retval,buf,buflen);
    }
    vnode_decref(startvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}