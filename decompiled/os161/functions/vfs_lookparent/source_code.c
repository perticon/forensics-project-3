vfs_lookparent(char *path, struct vnode **retval,
	       char *buf, size_t buflen)
{
	struct vnode *startvn;
	int result;

	vfs_biglock_acquire();

	result = getdevice(path, &path, &startvn);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	if (strlen(path)==0) {
		/*
		 * It does not make sense to use just a device name in
		 * a context where "lookparent" is the desired
		 * operation.
		 */
		result = EINVAL;
	}
	else {
		result = VOP_LOOKPARENT(startvn, path, retval, buf, buflen);
	}

	VOP_DECREF(startvn);

	vfs_biglock_release();
	return result;
}