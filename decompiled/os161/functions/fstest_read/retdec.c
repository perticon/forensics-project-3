int32_t fstest_read(char * fs, char * namesuffix) {
    char buf[32]; // bp-100, 0x8000f7d0
    char name[32]; // bp-132, 0x8000f7d0
    // 0x8000f7d0
    fstest_makename(name, 32, fs, namesuffix);
    strcpy(buf, name);
    int32_t * vn; // bp-136, 0x8000f7d0
    int32_t v1 = vfs_open(buf, 0, 436, &vn); // 0x8000f818
    if (v1 != 0) {
        // 0x8000f824
        kprintf("Could not open test file for read: %s\n", strerror(v1));
        // 0x8000fa08
        return -1;
    }
    // 0x8000f978
    int32_t v2; // bp-168, 0x8000f7d0
    int32_t v3 = (int32_t)&v2 + 68;
    int32_t v4 = 0;
    int32_t v5 = strlen("HODIE MIHI - CRAS TIBI\n"); // 0x8000f848
    int32_t iov; // bp-68, 0x8000f7d0
    int32_t ku; // bp-56, 0x8000f7d0
    uio_kinit(&iov, &ku, buf, v5, 0, 0);
    vnode_check(vn, "read");
    int32_t v6 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 12); // 0x8000f888
    int32_t result; // 0x8000f7d0
    while (v6 == 0) {
        int32_t v7; // 0x8000f7d0
        if (v7 != 0) {
            // 0x8000f8e0
            kprintf("%s: Short read: %lu bytes left over\n", name, v7);
            vfs_close(vn);
            result = -1;
            return result;
        }
        // 0x8000f900
        *(char *)(v3 + strlen("HODIE MIHI - CRAS TIBI\n")) = 0;
        rotate(buf, -v4);
        int32_t v8 = strcmp(buf, "HODIE MIHI - CRAS TIBI\n"); // 0x8000f924
        v4++;
        if (v8 != 0) {
            // 0x8000f930
            kprintf("%s: Test failed: line %d mismatched: %s\n", name, v4, buf);
            vfs_close(vn);
            result = -1;
            return result;
        }
        // 0x8000f978
        if (v4 >= 720) {
            // 0x8000f984
            vfs_close(vn);
            if (720 * strlen("HODIE MIHI - CRAS TIBI\n") == v7) {
                // 0x8000f9f4
                kprintf("%s: %lu bytes read\n", name, v7);
                result = 0;
                return result;
            } else {
                int32_t v9 = strlen("HODIE MIHI - CRAS TIBI\n"); // 0x8000f9c0
                kprintf("%s: %lu bytes read, should have been %lu!\n", name, v7, 720 * v9);
                result = -1;
                return result;
            }
        }
        v5 = strlen("HODIE MIHI - CRAS TIBI\n");
        uio_kinit(&iov, &ku, buf, v5, 0, 0);
        vnode_check(vn, "read");
        v6 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 12);
    }
    char * v10 = strerror(v6); // 0x8000f8a4
    kprintf("%s: Read error: %s\n", name, v10);
    vfs_close(vn);
    result = -1;
  lab_0x8000fa08:
    // 0x8000fa08
    return result;
}