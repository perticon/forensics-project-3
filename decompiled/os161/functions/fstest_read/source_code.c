fstest_read(const char *fs, const char *namesuffix)
{
	struct vnode *vn;
	int err;
	int i;
	size_t bytes=0;
	char name[32];
	char buf[32];
	struct iovec iov;
	struct uio ku;

	MAKENAME();

	/* vfs_open destroys the string it's passed */
	strcpy(buf, name);
	err = vfs_open(buf, O_RDONLY, 0664, &vn);
	if (err) {
		kprintf("Could not open test file for read: %s\n",
			strerror(err));
		return -1;
	}

	for (i=0; i<NCHUNKS; i++) {
		uio_kinit(&iov, &ku, buf, strlen(SLOGAN), bytes, UIO_READ);
		err = VOP_READ(vn, &ku);
		if (err) {
			kprintf("%s: Read error: %s\n", name, strerror(err));
			vfs_close(vn);
			return -1;
		}

		if (ku.uio_resid > 0) {
			kprintf("%s: Short read: %lu bytes left over\n", name,
				(unsigned long) ku.uio_resid);
			vfs_close(vn);
			return -1;
		}
		buf[strlen(SLOGAN)] = 0;
		rotate(buf, -i);
		if (strcmp(buf, SLOGAN)) {
			kprintf("%s: Test failed: line %d mismatched: %s\n",
				name, i+1, buf);
			vfs_close(vn);
			return -1;
		}

		bytes = ku.uio_offset;
	}

	vfs_close(vn);

	if (bytes != NCHUNKS*strlen(SLOGAN)) {
		kprintf("%s: %lu bytes read, should have been %lu!\n",
			name, (unsigned long) bytes,
			(unsigned long) (NCHUNKS*strlen(SLOGAN)));
		return -1;
	}
	kprintf("%s: %lu bytes read\n", name, (unsigned long) bytes);
	return 0;
}