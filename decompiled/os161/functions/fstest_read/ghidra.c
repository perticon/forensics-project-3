int fstest_read(char *fs,char *namesuffix)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  size_t sVar4;
  uint uVar5;
  vnode *vn;
  char name [32];
  char buf [32];
  iovec iov;
  uio ku;
  
  fstest_makename(name,0x20,fs,namesuffix);
  strcpy(buf,name);
  iVar1 = vfs_open(buf,0,0x1b4,&vn);
  if (iVar1 == 0) {
    uVar5 = 0;
    for (iVar1 = 0; iVar1 < 0x2d0; iVar1 = iVar1 + 1) {
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar4,(ulonglong)uVar5,UIO_READ);
      vnode_check(vn,"read");
      iVar3 = (*vn->vn_ops->vop_read)(vn,&ku);
      if (iVar3 != 0) {
        pcVar2 = strerror(iVar3);
        kprintf("%s: Read error: %s\n",name,pcVar2);
        vfs_close(vn);
        return -1;
      }
      if (ku.uio_resid != 0) {
        kprintf("%s: Short read: %lu bytes left over\n",name);
        vfs_close(vn);
        return -1;
      }
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      buf[sVar4] = '\0';
      rotate(buf,-iVar1);
      iVar3 = strcmp(buf,"HODIE MIHI - CRAS TIBI\n");
      if (iVar3 != 0) {
        kprintf("%s: Test failed: line %d mismatched: %s\n",name,iVar1 + 1,buf);
        vfs_close(vn);
        return -1;
      }
      uVar5 = ku.uio_offset._4_4_;
    }
    vfs_close(vn);
    sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
    if (sVar4 * 0x2d0 == uVar5) {
      kprintf("%s: %lu bytes read\n",name,uVar5);
      iVar1 = 0;
    }
    else {
      sVar4 = strlen("HODIE MIHI - CRAS TIBI\n");
      kprintf("%s: %lu bytes read, should have been %lu!\n",name,uVar5,sVar4 * 0x2d0);
      iVar1 = -1;
    }
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not open test file for read: %s\n",pcVar2);
    iVar1 = -1;
  }
  return iVar1;
}