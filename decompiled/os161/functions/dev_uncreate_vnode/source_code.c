dev_uncreate_vnode(struct vnode *vn)
{
	KASSERT(vn->vn_ops == &dev_vnode_ops);
	vnode_cleanup(vn);
	kfree(vn);
}