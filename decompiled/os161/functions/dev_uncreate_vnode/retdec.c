void dev_uncreate_vnode(int32_t * vn) {
    int32_t v1 = (int32_t)vn;
    int32_t v2 = v1; // 0x80017ee0
    if (*(int32_t *)(v1 + 20) != (int32_t)&dev_vnode_ops) {
        // 0x80017ee4
        badassert("vn->vn_ops == &dev_vnode_ops", "../../vfs/device.c", 385, "dev_uncreate_vnode");
        v2 = (int32_t)"vn->vn_ops == &dev_vnode_ops";
    }
    // 0x80017f04
    vnode_cleanup((int32_t *)v2);
    kfree((char *)vn);
}