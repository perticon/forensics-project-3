void dev_uncreate_vnode(vnode *vn)

{
  char *vn_00;
  
  vn_00 = (char *)vn;
  if (vn->vn_ops != &dev_vnode_ops) {
    vn_00 = "vn->vn_ops == &dev_vnode_ops";
    badassert("vn->vn_ops == &dev_vnode_ops","../../vfs/device.c",0x181,"dev_uncreate_vnode");
  }
  vnode_cleanup((vnode *)vn_00);
  kfree(vn);
  return;
}