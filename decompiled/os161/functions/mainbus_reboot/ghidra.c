void mainbus_reboot(void)

{
  kprintf("Cannot reboot - powering off instead, sorry.\n");
  mainbus_poweroff();
  return;
}