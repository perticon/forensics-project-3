void mainbus_reboot(void) {
    // 0x800208ec
    kprintf("Cannot reboot - powering off instead, sorry.\n");
    mainbus_poweroff();
}