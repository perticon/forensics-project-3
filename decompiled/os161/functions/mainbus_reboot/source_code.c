mainbus_reboot(void)
{
	/*
	 * The MIPS doesn't appear to have any on-chip reset.
	 * LAMEbus doesn't have a reset control, so we just
	 * power off instead of rebooting. This would not be
	 * so great in a real system, but it's fine for what
	 * we're doing.
	 */
	kprintf("Cannot reboot - powering off instead, sorry.\n");
	mainbus_poweroff();
}