int sfs_bmap(sfs_vnode *sv,uint32_t fileblock,bool doalloc,daddr_t *diskblock)

{
  undefined3 extraout_var;
  bool bVar3;
  uint uVar1;
  int iVar2;
  sfs_fs *sfs;
  daddr_t block;
  daddr_t idblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  bVar3 = vfs_biglock_do_i_hold();
  uVar1 = (uint)(fileblock < 0xf);
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_bmap.c",0x46,"sfs_bmap");
  }
  if (uVar1 == 0) {
    uVar1 = fileblock - 0xf;
    if (uVar1 < 0x80) {
      idblock = (sv->sv_i).sfi_indirect;
      if ((idblock == 0) && (iVar2 = 0, doalloc == false)) {
        *diskblock = 0;
      }
      else {
        if (idblock == 0) {
          iVar2 = sfs_balloc(sfs,&idblock);
          if (iVar2 != 0) {
            return iVar2;
          }
          (sv->sv_i).sfi_indirect = idblock;
          sv->sv_dirty = true;
          bzero(sfs_bmap::idbuf,0x200);
        }
        else {
          iVar2 = sfs_readblock(sfs,idblock,sfs_bmap::idbuf,0x200);
          if (iVar2 != 0) {
            return iVar2;
          }
        }
        block = sfs_bmap::idbuf[uVar1 & 0x7f];
        if ((block == 0) && (doalloc != false)) {
          iVar2 = sfs_balloc(sfs,&block);
          if (iVar2 != 0) {
            return iVar2;
          }
          sfs_bmap::idbuf[uVar1 & 0x7f] = block;
          iVar2 = sfs_writeblock(sfs,idblock,sfs_bmap::idbuf,0x200);
          if (iVar2 != 0) {
            return iVar2;
          }
        }
        if ((block != 0) && (iVar2 = sfs_bused(sfs,block), iVar2 == 0)) {
                    /* WARNING: Subroutine does not return */
          panic("sfs: %s: Data block %u (block %u of file %u) marked free\n",
                (sfs->sfs_sb).sb_volname,block,uVar1,sv->sv_ino);
        }
        *diskblock = block;
        iVar2 = 0;
      }
    }
    else {
      iVar2 = 0x26;
    }
  }
  else {
    block = (sv->sv_i).sfi_direct[fileblock];
    if ((block == 0) && (doalloc != false)) {
      iVar2 = sfs_balloc(sfs,&block);
      if (iVar2 != 0) {
        return iVar2;
      }
      (sv->sv_i).sfi_direct[fileblock] = block;
      sv->sv_dirty = true;
    }
    if ((block != 0) && (iVar2 = sfs_bused(sfs,block), iVar2 == 0)) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: Data block %u (block %u of file %u) marked free\n",(sfs->sfs_sb).sb_volname,
            block,fileblock,sv->sv_ino);
    }
    *diskblock = block;
    iVar2 = 0;
  }
  return iVar2;
}