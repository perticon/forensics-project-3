int32_t sfs_bmap(int32_t * sv, uint32_t fileblock, bool doalloc, int32_t * diskblock) {
    int32_t v1 = (int32_t)sv;
    int32_t v2 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80007514
    int32_t block; // bp-40, 0x800074e0
    if (vfs_biglock_do_i_hold()) {
        if (fileblock >= 15) {
            uint32_t v3 = fileblock - 15; // 0x800075f4
            if (v3 >= 128) {
                // 0x8000773c
                return 38;
            }
            int32_t * v4 = (int32_t *)(v1 + 92); // 0x80007604
            int32_t v5 = *v4; // 0x80007604
            int32_t idblock = v5; // bp-36, 0x80007610
            if (v5 == 0 && !doalloc) {
                // 0x8000761c
                *diskblock = 0;
                // 0x8000773c
                return 0;
            }
            int32_t * v6 = (int32_t *)v2;
            if (v5 == 0) {
                int32_t result = sfs_balloc(v6, &idblock); // 0x80007630
                if (result != 0) {
                    // 0x8000773c
                    return result;
                }
                // 0x8000763c
                *v4 = idblock;
                *(char *)(v1 + 540) = 1;
                bzero((char *)&g24, 512);
            } else {
                int32_t result2 = sfs_readblock(v6, v5, (char *)&g24, 512); // 0x80007670
                if (result2 != 0) {
                    // 0x8000773c
                    return result2;
                }
            }
            int32_t * v7 = (int32_t *)((4 * v3 & 508) + (int32_t)&g24); // 0x8000768c
            int32_t v8 = *v7; // 0x8000768c
            block = v8;
            int32_t v9 = v8; // 0x80007698
            if (v8 == 0 && doalloc) {
                int32_t result3 = sfs_balloc(v6, &block); // 0x800076a8
                if (result3 != 0) {
                    // 0x8000773c
                    return result3;
                }
                // 0x800076b4
                *v7 = block;
                int32_t result4 = sfs_writeblock(v6, idblock, (char *)&g24, 512); // 0x800076d8
                if (result4 != 0) {
                    // 0x8000773c
                    return result4;
                }
                // 0x800076e4
                v9 = block;
            }
            // 0x800076e4
            if (v9 != 0) {
                // 0x800076f4
                if (sfs_bused(v6, v9) == 0) {
                    int32_t v10 = *(int32_t *)(v1 + 536); // 0x80007704
                    panic("sfs: %s: Data block %u (block %u of file %u) marked free\n", (char *)(v2 + 16), block, v3, v10);
                }
            }
            // 0x80007724
            *diskblock = block;
            // 0x8000773c
            return 0;
        }
    } else {
        // 0x80007548
        badassert("vfs_biglock_do_i_hold()", "../../fs/sfs/sfs_bmap.c", 70, "sfs_bmap");
    }
    int32_t * v11 = (int32_t *)(v1 + 32 + 4 * fileblock); // 0x80007558
    int32_t v12 = *v11; // 0x80007558
    block = v12;
    block = v12;
    if (v12 == 0 && doalloc) {
        int32_t result5 = sfs_balloc((int32_t *)v2, &block); // 0x80007574
        if (result5 != 0) {
            // 0x8000773c
            return result5;
        }
        // 0x80007580
        *v11 = block;
        *(char *)(v1 + 540) = 1;
    }
    // 0x800075a0
    if (block != 0) {
        // 0x800075b0
        if (sfs_bused((int32_t *)v2, block) == 0) {
            int32_t v13 = *(int32_t *)(v1 + 536); // 0x800075c0
            panic("sfs: %s: Data block %u (block %u of file %u) marked free\n", (char *)(v2 + 16), block, fileblock, v13);
        }
    }
    // 0x800075e0
    *diskblock = block;
    // 0x8000773c
    return 0;
}