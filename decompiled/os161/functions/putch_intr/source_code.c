putch_intr(struct con_softc *cs, int ch)
{
	P(cs->cs_wsem);
	cs->cs_send(cs->cs_devdata, ch);
}