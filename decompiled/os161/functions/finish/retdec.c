void finish(uint32_t howmanytotal) {
    int32_t v1 = 0; // 0x800148ec
    if (howmanytotal > 0) {
        v1++;
        P(donesem);
        while (v1 < howmanytotal) {
            // 0x800148d8
            v1++;
            P(donesem);
        }
    }
    // 0x800148f0
    P(wakersem);
    wakerdone = 1;
    V(wakersem);
    P(donesem);
}