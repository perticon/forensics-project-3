int32_t array_setsize(int32_t * a, int32_t num) {
    int32_t result = array_preallocate(a, num); // 0x8000aa58
    if (result == 0) {
        // 0x8000aa64
        *(int32_t *)((int32_t)a + 4) = num;
    }
    // 0x8000aa68
    return result;
}