array_setsize(struct array *a, unsigned num)
{
	int result;

	result = array_preallocate(a, num);
	if (result) {
		return result;
	}
	a->num = num;

	return 0;
}