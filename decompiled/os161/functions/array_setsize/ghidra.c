int array_setsize(array *a,uint num)

{
  int iVar1;
  
  iVar1 = array_preallocate(a,num);
  if (iVar1 == 0) {
    a->num = num;
  }
  return iVar1;
}