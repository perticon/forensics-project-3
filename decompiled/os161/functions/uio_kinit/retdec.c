void uio_kinit(int32_t * iov, int32_t * u, char * kbuf, int32_t len, int64_t pos, int32_t rw) {
    int32_t v1 = (int32_t)u;
    int32_t v2 = (int32_t)iov;
    *iov = (int32_t)kbuf;
    *(int32_t *)(v2 + 4) = len;
    *u = v2;
    *(int32_t *)(v1 + 4) = 1;
    *(int32_t *)(v1 + 8) = (int32_t)pos;
    *(int32_t *)(v1 + 16) = len;
    *(int32_t *)(v1 + 20) = 2;
    *(int32_t *)(v1 + 24) = rw;
    *(int32_t *)(v1 + 28) = 0;
}