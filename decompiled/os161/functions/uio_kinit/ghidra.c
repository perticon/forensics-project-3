void uio_kinit(iovec *iov,uio *u,void *kbuf,size_t len,off_t pos,uio_rw rw)

{
  iov->field_0 = kbuf;
  iov->iov_len = len;
  u->uio_iov = iov;
  u->uio_iovcnt = 1;
  *(undefined4 *)((int)&u->uio_offset + 4) = pos._4_4_;
  *(undefined4 *)&u->uio_offset = pos._0_4_;
  u->uio_resid = len;
  u->uio_segflg = UIO_SYSSPACE;
  u->uio_rw = rw;
  u->uio_space = (addrspace *)0x0;
  return;
}