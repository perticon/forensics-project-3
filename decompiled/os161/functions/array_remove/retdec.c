void array_remove(int32_t * a, int32_t index) {
    int32_t v1 = (int32_t)a;
    int32_t * v2 = (int32_t *)(v1 + 4); // 0x8000ab40
    uint32_t v3 = *v2; // 0x8000ab40
    int32_t v4 = v3; // 0x8000ab54
    int32_t v5 = v1; // 0x8000ab54
    int32_t v6 = index; // 0x8000ab54
    if (*(int32_t *)(v1 + 8) < v3) {
        // 0x8000ab58
        badassert("a->num <= a->max", "../../lib/array.c", 129, "array_remove");
        v4 = &g41;
        v5 = (int32_t)"a->num <= a->max";
        v6 = (int32_t)"../../lib/array.c";
    }
    int32_t v7 = v4 - v6; // 0x8000ab80
    int32_t v8 = v5; // 0x8000ab80
    int32_t v9 = v6; // 0x8000ab80
    if (v4 <= v6) {
        // 0x8000ab84
        badassert("index < a->num", "../../lib/array.c", 130, "array_remove");
        v7 = &g41;
        v8 = (int32_t)"index < a->num";
        v9 = (int32_t)"../../lib/array.c";
    }
    int32_t v10 = *(int32_t *)v8; // 0x8000aba8
    memmove((char *)(v10 + 4 * v9), (int32_t *)(v10 + 4 * (int32_t)"./../lib/array.c"), 4 * v7 - 4);
    *v2 = *v2 - 1;
}