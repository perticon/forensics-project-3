void array_remove(array *a,uint index)

{
  char *pcVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar1 = (char *)a->num;
  pcVar3 = (char *)a;
  pcVar4 = (char *)index;
  if ((char *)a->max < pcVar1) {
    pcVar3 = "a->num <= a->max";
    pcVar4 = "../../lib/array.c";
    badassert("a->num <= a->max","../../lib/array.c",0x81,"array_remove");
  }
  iVar2 = (int)pcVar1 - (int)pcVar4;
  if (pcVar1 <= pcVar4) {
    pcVar3 = "index < a->num";
    pcVar4 = "../../lib/array.c";
    badassert("index < a->num","../../lib/array.c",0x82,"array_remove");
  }
  memmove(*(void ***)pcVar3 + (int)pcVar4,*(void ***)pcVar3 + (int)(pcVar4 + 1),(iVar2 + -1) * 4);
  a->num = a->num - 1;
  return;
}