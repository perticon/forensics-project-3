vfs_swapoff(const char *devname)
{
	struct knowndev *kd;
	int result;

	vfs_biglock_acquire();

	result = findmount(devname, &kd);
	if (result) {
		goto fail;
	}

	if (kd->kd_fs != SWAP_FS) {
		result = EINVAL;
		goto fail;
	}

	kprintf("vfs: Swap detached from %s:\n", kd->kd_name);

	/* drop it */
	kd->kd_fs = NULL;

	KASSERT(result==0);

 fail:
	vfs_biglock_release();
	return result;
}