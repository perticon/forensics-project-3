int vfs_swapoff(char *devname)

{
  int iVar1;
  knowndev *kd;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    if (kd->kd_fs == (fs *)0xffffffff) {
      kprintf("vfs: Swap detached from %s:\n",kd->kd_name);
      kd->kd_fs = (fs *)0x0;
    }
    else {
      iVar1 = 8;
    }
  }
  vfs_biglock_release();
  return iVar1;
}