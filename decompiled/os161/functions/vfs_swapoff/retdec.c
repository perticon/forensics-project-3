int32_t vfs_swapoff(char * devname) {
    // 0x8001981c
    vfs_biglock_acquire();
    int32_t * kd; // bp-16, 0x8001981c
    int32_t result = findmount(devname, &kd); // 0x80019838
    if (result != 0) {
        // 0x80019898
        vfs_biglock_release();
        return result;
    }
    int32_t result2 = 8; // 0x80019858
    if (*(int32_t *)((int32_t)kd + 16) == -1) {
        // 0x8001985c
        kprintf("vfs: Swap detached from %s:\n", (char *)*kd);
        *(int32_t *)((int32_t)kd + 16) = 0;
        result2 = 0;
    }
    // 0x80019898
    vfs_biglock_release();
    return result2;
}