vopfail_truncate_isdir(struct vnode *vn, off_t pos)
{
	(void)vn;
	(void)pos;
	return EISDIR;
}