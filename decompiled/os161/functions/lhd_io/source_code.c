lhd_io(struct device *d, struct uio *uio)
{
	struct lhd_softc *lh = d->d_data;

	uint32_t sector = uio->uio_offset / LHD_SECTSIZE;
	uint32_t sectoff = uio->uio_offset % LHD_SECTSIZE;
	uint32_t len = uio->uio_resid / LHD_SECTSIZE;
	uint32_t lenoff = uio->uio_resid % LHD_SECTSIZE;
	uint32_t i;
	uint32_t statval = LHD_WORKING;
	int result;

	/* Don't allow I/O that isn't sector-aligned. */
	if (sectoff != 0 || lenoff != 0) {
		return EINVAL;
	}

	/* Don't allow I/O past the end of the disk. */
	/* XXX this check can overflow */
	if (sector+len > lh->lh_dev.d_blocks) {
		return EINVAL;
	}

	/* Set up the value to write into the status register. */
	if (uio->uio_rw==UIO_WRITE) {
		statval |= LHD_ISWRITE;
	}

	/* Loop over all the sectors we were asked to do. */
	for (i=0; i<len; i++) {

		/* Wait until nobody else is using the device. */
		P(lh->lh_clear);

		/*
		 * Are we writing? If so, transfer the data to the
		 * on-card buffer.
		 */
		if (uio->uio_rw == UIO_WRITE) {
			result = uiomove(lh->lh_buf, LHD_SECTSIZE, uio);
			membar_store_store();
			if (result) {
				V(lh->lh_clear);
				return result;
			}
		}

		/* Tell it what sector we want... */
		lhd_wreg(lh, LHD_REG_SECT, sector+i);

		/* and start the operation. */
		lhd_wreg(lh, LHD_REG_STAT, statval);

		/* Now wait until the interrupt handler tells us we're done. */
		P(lh->lh_done);

		/* Get the result value saved by the interrupt handler. */
		result = lh->lh_result;

		/*
		 * Are we reading? If so, and if we succeeded,
		 * transfer the data out of the on-card buffer.
		 */
		if (result==0 && uio->uio_rw==UIO_READ) {
			membar_load_load();
			result = uiomove(lh->lh_buf, LHD_SECTSIZE, uio);
		}

		/* Tell another thread it's cleared to go ahead. */
		V(lh->lh_clear);

		/* If we failed, return the error. */
		if (result) {
			return result;
		}
	}

	return 0;
}