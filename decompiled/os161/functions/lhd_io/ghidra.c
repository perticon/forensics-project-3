int lhd_io(device *d,uio *uio)

{
  int iVar1;
  lamebus_softc *plVar2;
  uint uVar3;
  uint uVar4;
  int iVar5;
  lamebus_softc **pplVar6;
  uint uVar7;
  uint32_t val;
  
  pplVar6 = (lamebus_softc **)d->d_data;
  iVar1 = *(int *)&uio->uio_offset;
  uVar3 = *(uint *)((int)&uio->uio_offset + 4);
  uVar4 = uVar3;
  iVar5 = iVar1;
  if (iVar1 < 0) {
    uVar4 = uVar3 + 0x1ff;
    iVar5 = (uint)(uVar4 < uVar3) + iVar1;
  }
  uVar4 = iVar5 << 0x17 | uVar4 >> 9;
  uVar3 = uVar3 & 0x1ff;
  if (iVar1 < 0) {
    uVar3 = (uVar3 - 1 | 0xfffffe00) + 1;
  }
  uVar7 = uio->uio_resid >> 9;
  if (uVar3 == 0) {
    plVar2 = (lamebus_softc *)&DAT_00000008;
    if (((uio->uio_resid & 0x1ff) == 0) &&
       (plVar2 = (lamebus_softc *)&DAT_00000008, (lamebus_softc *)(uVar4 + uVar7) <= pplVar6[8])) {
      if (uio->uio_rw == UIO_WRITE) {
        val = 3;
      }
      else {
        val = 1;
      }
      uVar3 = 0;
      do {
        if (uVar7 <= uVar3) {
          return 0;
        }
        P((semaphore *)pplVar6[5]);
        if (uio->uio_rw == UIO_WRITE) {
          iVar5 = uiomove(pplVar6[3],0x200,uio);
          SYNC(0);
          if (iVar5 != 0) {
            V((semaphore *)pplVar6[5]);
            return iVar5;
          }
        }
        lamebus_write_register(*pplVar6,(int)pplVar6[1],8,uVar4 + uVar3);
        lamebus_write_register(*pplVar6,(int)pplVar6[1],4,val);
        P((semaphore *)pplVar6[6]);
        plVar2 = pplVar6[4];
        if ((plVar2 == (lamebus_softc *)0x0) && (uio->uio_rw == UIO_READ)) {
          SYNC(0);
          plVar2 = (lamebus_softc *)uiomove(pplVar6[3],0x200,uio);
        }
        V((semaphore *)pplVar6[5]);
        uVar3 = uVar3 + 1;
      } while (plVar2 == (lamebus_softc *)0x0);
    }
  }
  else {
    plVar2 = (lamebus_softc *)&DAT_00000008;
  }
  return (int)plVar2;
}