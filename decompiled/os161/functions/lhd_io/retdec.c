int32_t lhd_io(int32_t * d, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)((int32_t)d + 16); // 0x800046d0
    int32_t v3 = *(int32_t *)(v1 + 8); // 0x800046d4
    uint32_t v4 = *(int32_t *)(v1 + 12); // 0x800046d8
    int32_t v5; // 0x800046a4
    int32_t v6; // 0x800046a4
    if (v3 > -1) {
        // 0x800046f4
        v5 = v4 / 512 | 0x800000 * v3;
        v6 = v4 % 512;
    } else {
        uint32_t v7 = v4 + 511;
        v5 = 0x800000 * (v3 + (int32_t)(v4 > 0xfffffe00)) | v7 / 512;
        v6 = (v7 | -512) + 1;
    }
    uint32_t v8 = *(int32_t *)(v1 + 16); // 0x80004718
    if ((v8 % 512 || v6) != 0) {
        // 0x80004860
        return 8;
    }
    uint32_t v9 = v8 / 512; // 0x80004720
    if (*(int32_t *)(v2 + 32) < v9 + v5) {
        // 0x80004860
        return 8;
    }
    int32_t * v10 = (int32_t *)(v1 + 24); // 0x8000474c
    int32_t * v11 = (int32_t *)(v2 + 20);
    int32_t * v12 = (int32_t *)(v2 + 12);
    int32_t * v13 = (int32_t *)v2;
    int32_t * v14 = (int32_t *)(v2 + 4);
    int32_t v15 = 0; // 0x80004770
    int32_t result = 0; // 0x80004848
    while (v15 < v9) {
        // 0x80004774
        P((int32_t *)*v11);
        if (*v10 == 1) {
            int32_t v16 = uiomove((char *)*v12, 512, uio); // 0x80004798
            if (__asm_sync() != 0) {
                // 0x800047ac
                V((int32_t *)*v11);
                result = v16;
                return result;
            }
        }
        // 0x800047c0
        lamebus_write_register((int32_t *)*v13, *v14, 8, v15 + v5);
        lamebus_write_register((int32_t *)*v13, *v14, 4, *v10 == 1 ? 3 : 1);
        P((int32_t *)*(int32_t *)(v2 + 24));
        int32_t v17 = *(int32_t *)(v2 + 16); // 0x800047f4
        int32_t v18 = v17; // 0x80004800
        if (v17 == 0) {
            // 0x80004804
            v18 = 0;
            if (*v10 == 0) {
                // 0x80004814
                __asm_sync();
                v18 = uiomove((char *)*v12, 512, uio);
            }
        }
        // 0x8000482c
        V((int32_t *)*v11);
        v15++;
        result = v18;
        if (v18 != 0) {
            // break -> 0x80004860
            break;
        }
        result = 0;
    }
  lab_0x80004860:
    // 0x80004860
    return result;
}