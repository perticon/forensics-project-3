init_threadsem(void)
{
	if (threadsem==NULL) {
		threadsem = sem_create("fstestsem", 0);
		if (threadsem == NULL) {
			panic("fstest: sem_create failed\n");
		}
	}
}