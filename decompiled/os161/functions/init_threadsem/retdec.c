void init_threadsem(void) {
    // 0x80010280
    if (threadsem != NULL) {
        // 0x800102c8
        return;
    }
    int32_t * v1 = sem_create("fstestsem", 0); // 0x800102a4
    *(int32_t *)&threadsem = (int32_t)v1;
    if (v1 == NULL) {
        // 0x800102b4
        panic("fstest: sem_create failed\n");
    }
}