void init_threadsem(void)

{
  if ((threadsem == (semaphore *)0x0) &&
     (threadsem = sem_create("fstestsem",0), threadsem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("fstest: sem_create failed\n");
  }
  return;
}