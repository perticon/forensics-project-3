int32_t vfs_unmount(char * devname) {
    // 0x800196cc
    vfs_biglock_acquire();
    int32_t * kd; // bp-16, 0x800196cc
    int32_t result = findmount(devname, &kd); // 0x800196e8
    if (result != 0) {
        // 0x80019800
        vfs_biglock_release();
        return result;
    }
    int32_t v1 = (int32_t)kd; // 0x800196f4
    int32_t v2 = *(int32_t *)(v1 + 16); // 0x800196fc
    if (v2 >= 0xffffffff) {
        // 0x80019800
        vfs_biglock_release();
        return 8;
    }
    int32_t v3 = v1; // 0x80019720
    int32_t v4 = v2; // 0x80019720
    char * v5 = (char *)-0x7ffd8e64; // 0x80019720
    if (*(int32_t *)(v1 + 4) == 0) {
        // 0x80019724
        badassert("kd->kd_rawname != NULL", "../../vfs/vfslist.c", 654, "vfs_unmount");
        v3 = &g41;
        v4 = (int32_t)"kd->kd_rawname != NULL";
        v5 = (char *)((int32_t)"../../vfs/vfslist.c" + 0x719c);
    }
    int32_t v6 = v4; // 0x8001974c
    if (*(int32_t *)(v3 + 8) == 0) {
        // 0x80019750
        badassert("kd->kd_device != NULL", v5, 655, "vfs_unmount");
        v6 = (int32_t)"kd->kd_device != NULL";
    }
    int32_t result2 = *(int32_t *)*(int32_t *)(v6 + 4); // 0x80019770
    if (result2 != 0) {
        // 0x80019800
        vfs_biglock_release();
        return result2;
    }
    int32_t v7 = *(int32_t *)(*(int32_t *)((int32_t)kd + 16) + 4); // 0x80019798
    int32_t v8 = *(int32_t *)(v7 + 12); // 0x800197a0
    int32_t result3 = v8; // 0x800197b4
    if (v8 == 0) {
        // 0x800197b8
        kprintf("vfs: Unmounted %s:\n", (char *)*kd);
        *(int32_t *)((int32_t)kd + 16) = 0;
        result3 = 0;
    }
    // 0x80019800
    vfs_biglock_release();
    return result3;
}