int vfs_unmount(char *devname)

{
  int iVar1;
  knowndev *pkVar2;
  char *pcVar3;
  char *pcVar4;
  knowndev *kd;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    pcVar3 = (char *)kd->kd_fs;
    if ((undefined *)((int)&((fs *)((int)pcVar3 + -8))->fs_ops + 3U) < (undefined *)0xfffffffe) {
      pcVar4 = (char *)0x80020000;
      pkVar2 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        pcVar3 = "kd->kd_rawname != NULL";
        pcVar4 = "../../vfs/vfslist.c";
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x28e,"vfs_unmount");
      }
      if (pkVar2->kd_device == (device *)0x0) {
        pcVar3 = "kd->kd_device != NULL";
        badassert("kd->kd_device != NULL",pcVar4 + 0x719c,0x28f,"vfs_unmount");
      }
      iVar1 = (*(*(fs_ops **)((int)pcVar3 + 4))->fsop_sync)((fs *)pcVar3);
      if ((iVar1 == 0) && (iVar1 = (*kd->kd_fs->fs_ops->fsop_unmount)(kd->kd_fs), iVar1 == 0)) {
        kprintf("vfs: Unmounted %s:\n",kd->kd_name);
        kd->kd_fs = (fs *)0x0;
      }
    }
    else {
      iVar1 = 8;
    }
  }
  vfs_biglock_release();
  return iVar1;
}