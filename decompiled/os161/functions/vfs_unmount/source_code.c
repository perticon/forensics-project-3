vfs_unmount(const char *devname)
{
	struct knowndev *kd;
	int result;

	vfs_biglock_acquire();

	result = findmount(devname, &kd);
	if (result) {
		goto fail;
	}

	if (kd->kd_fs == NULL || kd->kd_fs == SWAP_FS) {
		result = EINVAL;
		goto fail;
	}
	KASSERT(kd->kd_rawname != NULL);
	KASSERT(kd->kd_device != NULL);

	/* sync the fs */
	result = FSOP_SYNC(kd->kd_fs);
	if (result) {
		goto fail;
	}

	result = FSOP_UNMOUNT(kd->kd_fs);
	if (result) {
		goto fail;
	}

	kprintf("vfs: Unmounted %s:\n", kd->kd_name);

	/* now drop the filesystem */
	kd->kd_fs = NULL;

	KASSERT(result==0);

 fail:
	vfs_biglock_release();
	return result;
}