int sfs_partialio(sfs_vnode *sv,uio *uio,uint32_t skipstart,uint32_t len)

{
  undefined3 extraout_var;
  bool bVar1;
  uint uVar2;
  uint uVar3;
  int iVar4;
  sfs_fs *sfs;
  uio_rw uVar5;
  daddr_t diskblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  uVar5 = uio->uio_rw;
  if (0x200 < skipstart + len) {
    badassert("skipstart + len <= SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0xaa,"sfs_partialio");
  }
  bVar1 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0xad,"sfs_partialio");
  }
  iVar4 = *(int *)&uio->uio_offset;
  uVar2 = *(uint *)((int)&uio->uio_offset + 4);
  uVar3 = uVar2;
  if (iVar4 < 0) {
    uVar3 = uVar2 + 0x1ff;
    iVar4 = (uint)(uVar3 < uVar2) + iVar4;
  }
  iVar4 = sfs_bmap(sv,iVar4 << 0x17 | uVar3 >> 9,uVar5 == UIO_WRITE,&diskblock);
  if (iVar4 == 0) {
    if (diskblock == 0) {
      if (uio->uio_rw != UIO_READ) {
        badassert("uio->uio_rw == UIO_READ","../../fs/sfs/sfs_io.c",0xbd,"sfs_partialio");
      }
      bzero(sfs_partialio::iobuf,0x200);
    }
    else {
      iVar4 = sfs_readblock(sfs,diskblock,sfs_partialio::iobuf,0x200);
      if (iVar4 != 0) {
        return iVar4;
      }
    }
    iVar4 = uiomove(sfs_partialio::iobuf + skipstart,len,uio);
    if (iVar4 == 0) {
      if (uio->uio_rw == UIO_WRITE) {
        iVar4 = sfs_writeblock(sfs,diskblock,sfs_partialio::iobuf,0x200);
      }
      else {
        iVar4 = 0;
      }
    }
  }
  return iVar4;
}