int32_t sfs_partialio(int32_t * sv, int32_t * uio, int32_t skipstart, int32_t len) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)*(int32_t *)((int32_t)sv + 12); // 0x80008f00
    int32_t * v3 = (int32_t *)(v1 + 24); // 0x80008f04
    if (len + skipstart >= 513) {
        // 0x80008f20
        badassert("skipstart + len <= SFS_BLOCKSIZE", "../../fs/sfs/sfs_io.c", 170, "sfs_partialio");
    }
    // 0x80008f40
    if (!vfs_biglock_do_i_hold()) {
        // 0x80008f50
        badassert("vfs_biglock_do_i_hold()", "../../fs/sfs/sfs_io.c", 173, "sfs_partialio");
    }
    uint32_t v4 = *(int32_t *)(v1 + 8); // 0x80008f6c
    uint32_t v5 = *(int32_t *)(v1 + 12); // 0x80008f70
    int32_t v6 = v4; // 0x80008f7c
    int32_t v7 = v5; // 0x80008f7c
    if (v4 <= 0xffffffff) {
        // 0x80008f80
        v6 = v4 + (int32_t)(v5 > 0xfffffe00);
        v7 = v5 + 511;
    }
    // 0x80008f8c
    int32_t diskblock; // bp-40, 0x80008ec8
    int32_t result = sfs_bmap(sv, v7 / 512 | 0x800000 * v6, *v3 == 1, &diskblock); // 0x80008fa4
    if (result != 0) {
        // 0x80009068
        return result;
    }
    // 0x80008fb0
    if (diskblock == 0) {
        // 0x80008fc0
        if (*v3 != 0) {
            // 0x80008fd0
            badassert("uio->uio_rw == UIO_READ", "../../fs/sfs/sfs_io.c", 189, "sfs_partialio");
        }
        // 0x80008fec
        bzero((char *)&g26, 512);
    } else {
        int32_t result2 = sfs_readblock((int32_t *)v2, diskblock, (char *)&g26, 512); // 0x80009010
        if (result2 != 0) {
            // 0x80009068
            return result2;
        }
    }
    int32_t result3 = uiomove((char *)(skipstart + (int32_t)&g26), len, uio); // 0x8000902c
    if (result3 != 0) {
        // 0x80009068
        return result3;
    }
    int32_t result4 = 0; // 0x80009044
    if (*v3 == 1) {
        // 0x80009048
        result4 = sfs_writeblock((int32_t *)v2, diskblock, (char *)&g26, 512);
    }
    // 0x80009068
    return result4;
}