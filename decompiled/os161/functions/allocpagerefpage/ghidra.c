void allocpagerefpage(kheap_root *root)

{
  pagerefpage *addr;
  
  if (root->page != (pagerefpage *)0x0) {
    badassert("root->page == NULL","../../vm/kmalloc.c",0xe0,"allocpagerefpage");
  }
  spinlock_release(&kmalloc_spinlock);
  addr = (pagerefpage *)alloc_kpages(1);
  spinlock_acquire(&kmalloc_spinlock);
  if (addr == (pagerefpage *)0x0) {
    kprintf("kmalloc: Couldn\'t get a pageref page\n");
  }
  else {
    if (((uint)addr & 0xfff) != 0) {
      badassert("va % PAGE_SIZE == 0","../../vm/kmalloc.c",0xee,"allocpagerefpage");
    }
    if (root->page != (pagerefpage *)0x0) {
      spinlock_release(&kmalloc_spinlock);
      free_kpages((vaddr_t)addr);
      spinlock_acquire(&kmalloc_spinlock);
      if (root->page != (pagerefpage *)0x0) {
        return;
      }
      badassert("root->page != NULL","../../vm/kmalloc.c",0xf6,"allocpagerefpage");
    }
    root->page = addr;
  }
  return;
}