void allocpagerefpage(int32_t * root) {
    if (root != NULL) {
        // 0x8001c124
        badassert("root->page == NULL", "../../vm/kmalloc.c", 224, "allocpagerefpage");
    }
    // 0x8001c144
    spinlock_release(&kmalloc_spinlock);
    uint32_t v1 = alloc_kpages(1); // 0x8001c154
    spinlock_acquire(&kmalloc_spinlock);
    if (v1 == 0) {
        // 0x8001c16c
        kprintf("kmalloc: Couldn't get a pageref page\n");
        // 0x8001c1fc
        return;
    }
    if (v1 % 0x1000 != 0) {
        // 0x8001c188
        badassert("va % PAGE_SIZE == 0", "../../vm/kmalloc.c", 238, "allocpagerefpage");
    }
    // 0x8001c1b4
    spinlock_release(&kmalloc_spinlock);
    free_kpages(v1);
    spinlock_acquire(&kmalloc_spinlock);
}