int semu1(int nargs,char **args)

{
  undefined3 extraout_var;
  semaphore *sem;
  int iVar1;
  int iVar2;
  bool bVar3;
  
  sem = sem_create("some-silly-name",0x38);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semu1: whoops: sem_create failed\n");
  }
  iVar1 = strcmp(sem->sem_name,"some-silly-name");
  iVar2 = -0x7ffe0000;
  if (iVar1 != 0) {
    badassert("!strcmp(sem->sem_name, name)","../../test/semunit.c",0xa6,"semu1");
  }
  if (sem->sem_name == (char *)(iVar2 + 0x5af4)) {
    badassert("sem->sem_name != name","../../test/semunit.c",0xa7,"semu1");
  }
  if (sem->sem_wchan == (wchan *)0x0) {
    badassert("sem->sem_wchan != NULL","../../test/semunit.c",0xa8,"semu1");
  }
  bVar3 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0xa9,"semu1");
  }
  if (sem->sem_count != 0x38) {
    badassert("sem->sem_count == 56","../../test/semunit.c",0xaa,"semu1");
  }
  ok();
  sem_destroy(sem);
  return 0;
}