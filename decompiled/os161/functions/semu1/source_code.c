semu1(int nargs, char **args)
{
	struct semaphore *sem;
	const char *name = NAMESTRING;

	(void)nargs; (void)args;

	sem = sem_create(name, 56);
	if (sem == NULL) {
		panic("semu1: whoops: sem_create failed\n");
	}
	KASSERT(!strcmp(sem->sem_name, name));
	KASSERT(sem->sem_name != name);
	KASSERT(sem->sem_wchan != NULL);
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 56);

	ok();
	/* clean up */
	sem_destroy(sem);
	return 0;
}