int32_t semu1(int32_t nargs, char ** args) {
    int32_t * v1 = sem_create("some-silly-name", 56); // 0x80011ef8
    int32_t v2 = (int32_t)v1; // 0x80011ef8
    int32_t v3 = v2; // 0x80011f00
    if (v1 == NULL) {
        // 0x80011f04
        panic("semu1: whoops: sem_create failed\n");
        v3 = &g41;
    }
    int32_t v4 = -0x7ffda50c; // 0x80011f24
    if (strcmp((char *)*(int32_t *)v3, "some-silly-name") != 0) {
        // 0x80011f28
        badassert("!strcmp(sem->sem_name, name)", "../../test/semunit.c", 166, "semu1");
        v4 = (int32_t)&g41 + 0x5af4;
    }
    // 0x80011f48
    if (*v1 == v4) {
        // 0x80011f58
        badassert("sem->sem_name != name", "../../test/semunit.c", 167, "semu1");
    }
    // 0x80011f74
    if (*(int32_t *)(v2 + 4) == 0) {
        // 0x80011f84
        badassert("sem->sem_wchan != NULL", "../../test/semunit.c", 168, "semu1");
    }
    // 0x80011fa0
    if (!spinlock_not_held((int32_t *)(v2 + 8))) {
        // 0x80011fb0
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 169, "semu1");
    }
    // 0x80011fcc
    if (*(int32_t *)(v2 + 16) != 56) {
        // 0x80011fdc
        badassert("sem->sem_count == 56", "../../test/semunit.c", 170, "semu1");
    }
    // 0x80011ff8
    ok();
    sem_destroy(v1);
    return 0;
}