void thread_start_cpus(void) {
    char buf[64]; // bp-80, 0x80016684
    // 0x80016684
    cpu_identify(buf, 64);
    kprintf("cpu0: %s\n", buf);
    int32_t * v1 = sem_create("cpu_hatch", 0); // 0x800166c0
    *(int32_t *)&cpu_startup_sem = (int32_t)v1;
    mainbus_start_cpus();
    int32_t v2 = 0; // 0x80016704
    if (g34 == 1) {
        // 0x80016708
        sem_destroy(cpu_startup_sem);
        *(int32_t *)&cpu_startup_sem = 0;
        return;
    }
    v2++;
    P(cpu_startup_sem);
    int32_t * v3 = cpu_startup_sem;
    int32_t * v4 = v3; // 0x80016704
    while (v2 < g34 - 1) {
        // 0x800166e0
        v2++;
        P(v3);
        v3 = cpu_startup_sem;
        v4 = v3;
    }
    // 0x80016708
    sem_destroy(v4);
    *(int32_t *)&cpu_startup_sem = 0;
}