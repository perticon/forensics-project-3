void thread_start_cpus(void)

{
  uint uVar1;
  char buf [64];
  
  cpu_identify(buf,0x40);
  kprintf("cpu0: %s\n",buf);
  cpu_startup_sem = sem_create("cpu_hatch",0);
  mainbus_start_cpus();
  for (uVar1 = 0; uVar1 < allcpus.arr.num - 1; uVar1 = uVar1 + 1) {
    P(cpu_startup_sem);
  }
  sem_destroy(cpu_startup_sem);
  cpu_startup_sem = (semaphore *)0x0;
  return;
}