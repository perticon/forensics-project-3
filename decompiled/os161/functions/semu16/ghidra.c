int semu16(int nargs,char **args)

{
  semaphore *sem;
  spinlock lk;
  
  sem = makesem(1);
  spinlock_init(&lk);
  spinlock_acquire(&lk);
  P(sem);
  ok();
  spinlock_release(&lk);
  spinlock_cleanup(&lk);
  sem_destroy(sem);
  return 0;
}