int32_t semu16(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(1); // 0x800123a0
    int32_t lk; // bp-16, 0x80012390
    spinlock_init(&lk);
    spinlock_acquire(&lk);
    P(v1);
    ok();
    spinlock_release(&lk);
    spinlock_cleanup(&lk);
    sem_destroy(v1);
    return 0;
}