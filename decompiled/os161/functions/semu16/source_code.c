semu16(int nargs, char **args)
{
	struct semaphore *sem;
	struct spinlock lk;

	(void)nargs; (void)args;

	sem = makesem(1);

	/* As above, check for improper blocking by taking a spinlock. */
	spinlock_init(&lk);
	spinlock_acquire(&lk);

	P(sem);

	ok();
	spinlock_release(&lk);
	spinlock_cleanup(&lk);
	sem_destroy(sem);
	return 0;
}