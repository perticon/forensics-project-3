void increase(int32_t indicator) {
    // 0x8001b914
    spinlock_acquire(&instr_lock);
    g37 = indicator;
    switch (indicator) {
        case 0: {
            // 0x8001b958
            tlb_misses++;
            // break -> 0x8001ba44
            break;
        }
        case 1: {
            // 0x8001b970
            tlb_misses_free++;
            // break -> 0x8001ba44
            break;
        }
        case 2: {
            // 0x8001b988
            tlb_misses_full++;
            // break -> 0x8001ba44
            break;
        }
        case 3: {
            // 0x8001b9a0
            tlb_invalidations++;
            // break -> 0x8001ba44
            break;
        }
        case 4: {
            // 0x8001b9b8
            tlb_reloads++;
            // break -> 0x8001ba44
            break;
        }
        case 5: {
            // 0x8001b9d0
            faults_with_load++;
            // break -> 0x8001ba44
            break;
        }
        case 6: {
            // 0x8001b9e8
            faults_with_elf_load++;
            // break -> 0x8001ba44
            break;
        }
        case 7: {
            // 0x8001ba00
            swap_out_pages++;
            // break -> 0x8001ba44
            break;
        }
        case 8: {
            // 0x8001ba18
            swap_in_pages++;
            // break -> 0x8001ba44
            break;
        }
        case 9: {
            // 0x8001ba30
            new_pages_zeroed++;
            // break -> 0x8001ba44
            break;
        }
    }
    // 0x8001ba44
    spinlock_release(&instr_lock);
}