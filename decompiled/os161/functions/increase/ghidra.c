void increase(long indicator)

{
  spinlock_acquire(&instr_lock);
  switch(indicator) {
  case 0:
    tlb_misses = tlb_misses + 1;
    break;
  case 1:
    tlb_misses_free = tlb_misses_free + 1;
    break;
  case 2:
    tlb_misses_full = tlb_misses_full + 1;
    break;
  case 3:
    tlb_invalidations = tlb_invalidations + 1;
    break;
  case 4:
    tlb_reloads = tlb_reloads + 1;
    break;
  case 5:
    faults_with_load = faults_with_load + 1;
    break;
  case 6:
    faults_with_elf_load = faults_with_elf_load + 1;
    break;
  case 7:
    swap_out_pages = swap_out_pages + 1;
    break;
  case 8:
    swap_in_pages = swap_in_pages + 1;
    break;
  case 9:
    new_pages_zeroed = new_pages_zeroed + 1;
  }
  spinlock_release(&instr_lock);
  return;
}