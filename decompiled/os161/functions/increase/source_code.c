void increase(long int indicator)
{

    spinlock_acquire(&instr_lock);

    switch (indicator)
    {
    case TLB_MISS:
        tlb_misses++;
        break;

    case TLB_MISS_FREE:
        tlb_misses_free++;
        break;

    case TLB_MISS_FULL:
        tlb_misses_full++;
        break;

    case TLB_INVALIDATION:
        tlb_invalidations++;
        break;

    case TLB_RELOAD:
        tlb_reloads++;
        break;

    case FAULT_WITH_LOAD:
        faults_with_load++;
        break;

    case FAULT_WITH_ELF_LOAD:
        faults_with_elf_load++;
        break;

    case SWAP_OUT_PAGE:
        swap_out_pages++;
        break;

    case SWAP_IN_PAGE:
        swap_in_pages++;
        break;

    case NEW_PAGE_ZEROED:
        new_pages_zeroed++;
        break;

    default:

        break;
    }

    spinlock_release(&instr_lock);

    return;
}