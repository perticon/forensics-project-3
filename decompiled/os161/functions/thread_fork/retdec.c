int32_t thread_fork(char * name, int32_t * proc, void (*entrypoint)(char *, int32_t), char * data1, int32_t data2) {
    int32_t * v1 = thread_create(name); // 0x8001698c
    if (v1 == NULL) {
        // 0x80016a34
        return 3;
    }
    int32_t v2 = (int32_t)v1; // 0x8001698c
    char * v3 = kmalloc(0x1000); // 0x8001699c
    *(int32_t *)(v2 + 72) = (int32_t)v3;
    if (v3 == NULL) {
        // 0x800169a8
        thread_destroy(v1);
        // 0x80016a34
        return 3;
    }
    // 0x800169b8
    thread_checkstack_init(v1);
    int32_t v4; // 0x80016968
    *(int32_t *)(v2 + 80) = *(int32_t *)(v4 + 80);
    int32_t v5 = (int32_t)proc; // 0x800169c8
    if (proc == NULL) {
        // 0x800169cc
        v5 = *(int32_t *)(v4 + 84);
    }
    int32_t v6 = proc_addthread((int32_t *)v5, v1); // 0x800169dc
    int32_t result; // 0x80016968
    if (v6 == 0) {
        int32_t * v7 = (int32_t *)(v2 + 96); // 0x800169f8
        *v7 = *v7 + 1;
        switchframe_init(v1, entrypoint, data1, data2);
        thread_make_runnable(v1, false);
        result = 0;
    } else {
        // 0x800169e8
        thread_destroy(v1);
        result = v6;
    }
    // 0x80016a34
    return result;
}