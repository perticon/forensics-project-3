int thread_fork(char *name,proc *proc,anon_subr_void_void_ptr_ulong *entrypoint,void *data1,
               ulong data2)

{
  thread *thread;
  void *pvVar1;
  int iVar2;
  int unaff_s7;
  
  thread = thread_create(name);
  if (thread == (thread *)0x0) {
    iVar2 = 3;
  }
  else {
    pvVar1 = kmalloc(0x1000);
    thread->t_stack = pvVar1;
    if (pvVar1 == (void *)0x0) {
      thread_destroy(thread);
      iVar2 = 3;
    }
    else {
      thread_checkstack_init(thread);
      thread->t_cpu = *(cpu **)(unaff_s7 + 0x50);
      if (proc == (proc *)0x0) {
        proc = *(proc **)(unaff_s7 + 0x54);
      }
      iVar2 = proc_addthread(proc,thread);
      if (iVar2 == 0) {
        thread->t_iplhigh_count = thread->t_iplhigh_count + 1;
        switchframe_init(thread,entrypoint,data1,data2);
        thread_make_runnable(thread,false);
        iVar2 = 0;
      }
      else {
        thread_destroy(thread);
      }
    }
  }
  return iVar2;
}