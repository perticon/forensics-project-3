int load_segment(addrspace *as,vnode *v,off_t offset,vaddr_t vaddr,size_t memsize,size_t filesize,
                int is_executable,paddr_t paddr)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  iovec local_48;
  undefined local_40 [12];
  char *local_34;
  uint local_30;
  uio_seg local_2c;
  uio_rw local_28;
  addrspace *local_24;
  
  if (*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10) != as) {
    vaddr = 0x2b;
    pcVar2 = "load_segment";
    badassert("curproc->p_addrspace == as","../../vm/segments.c",0x2b,"load_segment");
    memsize = (size_t)pcVar2;
  }
  if (offset._4_4_ < filesize) {
    kprintf("ELF: warning: segment filesize > segment memsize\n");
    filesize = offset._4_4_;
  }
  if ((dbflags & 0x40) != 0) {
    kprintf("ELF: Loading %lu bytes to 0x%lx\n",filesize,offset._0_4_);
  }
  local_48.field_0 = (offset._0_4_ & 0xfff) + paddr + -0x80000000;
  local_48.iov_len = offset._4_4_;
  local_40._0_4_ = &local_48;
  local_40._4_4_ = 1;
  local_2c = UIO_SYSSPACE;
  local_28 = UIO_READ;
  local_24 = (addrspace *)0x0;
  local_40._8_4_ = vaddr;
  local_34 = (char *)memsize;
  local_30 = filesize;
  vnode_check(v,"read");
  iVar1 = (*v->vn_ops->vop_read)(v,(uio *)local_40);
  if ((iVar1 == 0) && (local_30 != 0)) {
    kprintf("ELF: short read on segment - file truncated?\n");
    iVar1 = 0xd;
  }
  return iVar1;
}