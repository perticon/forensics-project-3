int32_t load_segment(int32_t * as, int32_t * v2, int64_t offset, int32_t vaddr, uint32_t memsize, uint32_t filesize, int32_t is_executable, int32_t paddr) {
    // 0x8001d3b0
    int32_t v1; // 0x8001d3b0
    if (*(int32_t *)(*(int32_t *)(v1 + 84) + 16) != (int32_t)as) {
        // 0x8001d3f4
        badassert("curproc->p_addrspace == as", "../../vm/segments.c", 43, "load_segment");
    }
    int32_t v2_ = filesize; // 0x8001d420
    if (memsize < filesize) {
        // 0x8001d424
        kprintf("ELF: warning: segment filesize > segment memsize\n");
        v2_ = memsize;
    }
    // 0x8001d434
    if ((dbflags & 64) != 0) {
        // 0x8001d44c
        kprintf("ELF: Loading %lu bytes to 0x%lx\n", v2_, vaddr);
    }
    // 0x8001d45c
    vnode_check(v2, "read");
    int32_t v3 = *(int32_t *)(*(int32_t *)((int32_t)v2 + 20) + 12); // 0x8001d4c0
    int32_t result = v3; // 0x8001d4d4
    if (v2_ != 0 && v3 == 0) {
        // 0x8001d4e8
        kprintf("ELF: short read on segment - file truncated?\n");
        result = 13;
    }
    // 0x8001d4f8
    return result;
}