void cv_broadcast(int32_t * cv, int32_t * lock) {
    int32_t v1; // 0x80015d70
    if (lock == NULL) {
        // 0x80015da4
        badassert("lock != NULL", "../../thread/synch.c", 376, "cv_broadcast");
        v1 = (int32_t)"../../thread/synch.c";
    } else {
        // 0x80015da4
        v1 = (int32_t)lock;
        if (cv == NULL) {
            // 0x80015dac
            badassert("cv != NULL", "../../thread/synch.c", 377, "cv_broadcast");
            v1 = (int32_t)"../../thread/synch.c";
        }
    }
    // 0x80015dcc
    if (!lock_do_i_hold((int32_t *)v1)) {
        // 0x80015ddc
        badassert("lock_do_i_hold(lock)", "../../thread/synch.c", 378, "cv_broadcast");
    }
    // 0x80015dfc
    spinlock_acquire((int32_t *)"NULL");
    wchan_wakeall((int32_t *)0x203d2120, (int32_t *)"NULL");
    spinlock_release((int32_t *)"NULL");
}