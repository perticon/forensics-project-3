cv_broadcast(struct cv *cv, struct lock *lock)
{
	// Write this
#if OPT_SYNCH
        KASSERT(lock != NULL);
	KASSERT(cv != NULL);
	KASSERT(lock_do_i_hold(lock));
	/* G.Cabodi - 2019: see comment on spinlocks in cv_signal */
	spinlock_acquire(&cv->cv_lock);
	wchan_wakeall(cv->cv_wchan,&cv->cv_lock);
	spinlock_release(&cv->cv_lock);
#endif
	(void)cv;    // suppress warning until code gets written
	(void)lock;  // suppress warning until code gets written
}