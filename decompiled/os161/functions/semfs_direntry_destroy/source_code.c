semfs_direntry_destroy(struct semfs_direntry *dent)
{
	kfree(dent->semd_name);
	kfree(dent);
}