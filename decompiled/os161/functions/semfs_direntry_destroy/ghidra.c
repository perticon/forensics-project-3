void semfs_direntry_destroy(semfs_direntry *dent)

{
  kfree(dent->semd_name);
  kfree(dent);
  return;
}