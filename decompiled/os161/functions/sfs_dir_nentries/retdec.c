int32_t sfs_dir_nentries(int32_t * sv) {
    int32_t v1 = (int32_t)sv;
    int32_t v2 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x800079a0
    if (*(int16_t *)(v1 + 28) != 2) {
        // 0x800079a4
        badassert("sv->sv_i.sfi_type == SFS_TYPE_DIR", "../../fs/sfs/sfs_dir.c", 87, "sfs_dir_nentries");
        v2 = (int32_t)"../../fs/sfs/sfs_dir.c";
    }
    // 0x800079c0
    panic("sfs: %s: directory %u: Invalid size %llu\n", (char *)(v2 + 16), 0x6f6e2075);
    return ((int32_t)&g41 > -1 ? 0x4000000 * (int32_t)&g41 : 0x4000000 * (int32_t)((int32_t)"lid size %llu\n" < 0x5059545f)) | ((int32_t)&g41 > -1 ? 0x1416551 : (int32_t)"lid size %llu\n" >> 6);
}