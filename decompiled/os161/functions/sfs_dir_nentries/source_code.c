sfs_dir_nentries(struct sfs_vnode *sv)
{
	struct sfs_fs *sfs = sv->sv_absvn.vn_fs->fs_data;
	off_t size;

	KASSERT(sv->sv_i.sfi_type == SFS_TYPE_DIR);

	size = sv->sv_i.sfi_size;
	if (size % sizeof(struct sfs_direntry) != 0) {
		panic("sfs: %s: directory %u: Invalid size %llu\n",
		      sfs->sfs_sb.sb_volname, sv->sv_ino, size);
	}

	return size / sizeof(struct sfs_direntry);
}