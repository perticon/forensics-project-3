int sfs_dir_nentries(sfs_vnode *sv)

{
  uint uVar1;
  char *pcVar2;
  char *pcVar3;
  
  pcVar3 = (char *)((sv->sv_absvn).vn_fs)->fs_data;
  if ((sv->sv_i).sfi_type != 2) {
    pcVar2 = "sv->sv_i.sfi_type == SFS_TYPE_DIR";
    pcVar3 = "../../fs/sfs/sfs_dir.c";
    badassert("sv->sv_i.sfi_type == SFS_TYPE_DIR","../../fs/sfs/sfs_dir.c",0x57,"sfs_dir_nentries");
    sv = (sfs_vnode *)pcVar2;
  }
  uVar1 = (sv->sv_i).sfi_size;
  if ((uVar1 & 0x3f) == 0) {
    return uVar1 >> 6;
  }
                    /* WARNING: Subroutine does not return */
  panic("sfs: %s: directory %u: Invalid size %llu\n",pcVar3 + 0x10,sv->sv_ino);
}