random_softc * attach_random_to_lrandom(int randomno,lrandom_softc *ls)

{
  random_softc *prVar1;
  
  prVar1 = (random_softc *)kmalloc(0x24);
  if (prVar1 == (random_softc *)0x0) {
    prVar1 = (random_softc *)0x0;
  }
  else {
    prVar1->rs_devdata = ls;
    prVar1->rs_random = lrandom_random;
    prVar1->rs_randmax = lrandom_randmax;
    prVar1->rs_read = lrandom_read;
  }
  return prVar1;
}