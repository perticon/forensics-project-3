int tryattach_random_to_lrandom(int devunit,lrandom_softc *bus,int busunit)

{
  random_softc *rs;
  int errcode;
  char *pcVar1;
  
  rs = attach_random_to_lrandom(devunit,bus);
  if (rs == (random_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("random%d at lrandom%d",devunit,busunit);
    errcode = config_random(rs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_random = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}