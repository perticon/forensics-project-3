tryattach_random_to_lrandom(int devunit, struct lrandom_softc *bus, int busunit)
{
	struct random_softc *dev;
	int result;

	dev = attach_random_to_lrandom(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("random%d at lrandom%d", devunit, busunit);
	result = config_random(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_random = devunit+1;
	autoconf_random(dev, devunit);
	return 0;
}