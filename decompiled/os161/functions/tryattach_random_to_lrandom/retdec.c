int32_t tryattach_random_to_lrandom(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_random_to_lrandom(devunit, bus); // 0x800015a0
    if (v1 == NULL) {
        // 0x80001618
        return -1;
    }
    // 0x800015ac
    kprintf("random%d at lrandom%d", devunit, busunit);
    int32_t v2 = config_random(v1, devunit); // 0x800015c8
    int32_t result; // 0x80001584
    if (v2 == 0) {
        // 0x800015f4
        kprintf("\n");
        nextunit_random = devunit + 1;
        result = 0;
    } else {
        // 0x800015d4
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x80001618
    return result;
}