int init_allocSize(int ramFrames)
{
    /* dimension of freeRamFrames and allocSize must be equal */

    KASSERT(ramFrames == nRamFrames);
    int i;
    allocSize = kmalloc(sizeof(unsigned long) * nRamFrames);
    if (allocSize == NULL)
        return 1;

    for (i = 0; i < nRamFrames; i++)
    {
        allocSize[i] = 0;
    }

    return 0;
}