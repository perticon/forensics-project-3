int init_allocSize(int ramFrames)

{
  ulong *puVar1;
  int iVar2;
  
  iVar2 = nRamFrames;
  if (ramFrames != nRamFrames) {
    badassert("ramFrames == nRamFrames","../../vm/coremap.c",0x56,"init_allocSize");
  }
  puVar1 = (ulong *)kmalloc(iVar2 << 2);
  allocSize = puVar1;
  if (puVar1 == (ulong *)0x0) {
    iVar2 = 1;
  }
  else {
    for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
      puVar1[iVar2] = 0;
    }
    iVar2 = 0;
  }
  return iVar2;
}