int32_t init_allocSize(int32_t ramFrames) {
    int32_t v1 = ramFrames; // 0x8001b56c
    if (g20 != ramFrames) {
        // 0x8001b570
        badassert("ramFrames == nRamFrames", "../../vm/coremap.c", 86, "init_allocSize");
        v1 = &g41;
    }
    char * v2 = kmalloc(4 * v1); // 0x8001b590
    int32_t v3 = (int32_t)v2; // 0x8001b590
    *(int32_t *)&allocSize = v3;
    if (v2 == NULL) {
        // 0x8001b5d8
        return 1;
    }
    // 0x8001b5b8
    for (int32_t i = 0; i < g20; i++) {
        // 0x8001b5ac
        *(int32_t *)(4 * i + v3) = 0;
    }
    // 0x8001b5d8
    return 0;
}