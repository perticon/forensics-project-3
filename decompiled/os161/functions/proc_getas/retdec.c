int32_t * proc_getas(void) {
    // 0x8000cec4
    int32_t v1; // 0x8000cec4
    int32_t v2 = *(int32_t *)(v1 + 84); // 0x8000ced4
    int32_t * result = NULL; // 0x8000cee0
    if (v2 != 0) {
        int32_t * v3 = (int32_t *)(v2 + 8); // 0x8000cee8
        spinlock_acquire(v3);
        spinlock_release(v3);
        result = (int32_t *)*(int32_t *)(v2 + 16);
    }
    // 0x8000cf04
    return result;
}