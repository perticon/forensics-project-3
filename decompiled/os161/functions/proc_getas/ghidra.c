addrspace * proc_getas(void)

{
  int iVar1;
  addrspace *paVar2;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    paVar2 = (addrspace *)0x0;
  }
  else {
    spinlock_acquire((spinlock *)(iVar1 + 8));
    paVar2 = *(addrspace **)(iVar1 + 0x10);
    spinlock_release((spinlock *)(iVar1 + 8));
  }
  return paVar2;
}