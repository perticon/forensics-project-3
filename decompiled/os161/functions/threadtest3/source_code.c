threadtest3(int nargs, char **args)
{
	if (nargs==1) {
		runtest3(5, 2);
	}
	else if (nargs==3) {
		runtest3(atoi(args[1]), atoi(args[2]));
	}
	else {
		kprintf("Usage: tt3 [sleepthreads computethreads]\n");
		return 1;
	}
	return 0;
}