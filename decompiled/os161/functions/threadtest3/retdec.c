int32_t threadtest3(int32_t nargs, char ** args) {
    int32_t result; // 0x80014e40
    switch (nargs) {
        case 1: {
            // 0x80014e58
            runtest3(5, 2);
            result = 0;
            // break -> 0x80014eb4
            break;
        }
        case 3: {
            int32_t v1 = (int32_t)args;
            int32_t v2 = atoi((char *)*(int32_t *)(v1 + 4)); // 0x80014e80
            runtest3(v2, atoi((char *)*(int32_t *)(v1 + 8)));
            result = 0;
            // break -> 0x80014eb4
            break;
        }
        default: {
            // 0x80014ea4
            kprintf("Usage: tt3 [sleepthreads computethreads]\n");
            result = 1;
            // break -> 0x80014eb4
            break;
        }
    }
    // 0x80014eb4
    return result;
}