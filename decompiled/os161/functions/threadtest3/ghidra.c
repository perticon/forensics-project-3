int threadtest3(int nargs,char **args)

{
  int iVar1;
  int ncomputes;
  
  if (nargs == 1) {
    runtest3(5,2);
    iVar1 = 0;
  }
  else if (nargs == 3) {
    iVar1 = atoi(args[1]);
    ncomputes = atoi(args[2]);
    runtest3(iVar1,ncomputes);
    iVar1 = 0;
  }
  else {
    kprintf("Usage: tt3 [sleepthreads computethreads]\n");
    iVar1 = 1;
  }
  return iVar1;
}