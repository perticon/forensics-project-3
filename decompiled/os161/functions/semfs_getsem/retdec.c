int32_t * semfs_getsem(int32_t * semv) {
    int32_t v1 = (int32_t)semv;
    int32_t v2 = *(int32_t *)(v1 + 28); // 0x80006128
    return semfs_getsembynum((int32_t *)*(int32_t *)(v1 + 24), v2);
}