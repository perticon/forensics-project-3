semfs_sem * semfs_getsem(semfs_vnode *semv)

{
  semfs_sem *psVar1;
  
  psVar1 = semfs_getsembynum(semv->semv_semfs,semv->semv_semnum);
  return psVar1;
}