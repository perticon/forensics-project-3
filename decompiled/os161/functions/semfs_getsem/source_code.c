semfs_getsem(struct semfs_vnode *semv)
{
	struct semfs *semfs = semv->semv_semfs;

	return semfs_getsembynum(semfs, semv->semv_semnum);
}