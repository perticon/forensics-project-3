void ipi_broadcast(int32_t code) {
    // 0x80017330
    if (g34 == 0) {
        // 0x800173d8
        return;
    }
    int32_t v1 = g34; // 0x80017330
    for (int32_t i = 0; i < v1; i++) {
        int32_t v2 = *(int32_t *)(allcpus + 4 * i); // 0x80017398
        int32_t v3; // 0x80017330
        if (*(int32_t *)*(int32_t *)(v3 + 80) != v2) {
            // 0x800173b4
            ipi_send((int32_t *)v2, code);
            v1 = g34;
        }
    }
}