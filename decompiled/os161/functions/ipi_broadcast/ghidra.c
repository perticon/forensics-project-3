void ipi_broadcast(int code)

{
  int iVar1;
  uint uVar2;
  int unaff_s7;
  
  for (uVar2 = 0; uVar2 < allcpus.arr.num; uVar2 = uVar2 + 1) {
    iVar1 = uVar2 << 2;
    if (allcpus.arr.num <= uVar2) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (**(cpu ***)(unaff_s7 + 0x50) != *(cpu **)((int)allcpus.arr.v + iVar1)) {
      ipi_send(*(cpu **)((int)allcpus.arr.v + iVar1),code);
    }
  }
  return;
}