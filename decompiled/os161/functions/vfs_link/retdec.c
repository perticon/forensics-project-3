int32_t vfs_link(char * oldpath, char * newpath) {
    char newname[256]; // bp-264, 0x8001a3a0
    // 0x8001a3a0
    int32_t * oldfile; // bp-272, 0x8001a3a0
    int32_t result = vfs_lookup(oldpath, (int32_t *)&oldfile); // 0x8001a3b4
    if (result != 0) {
        // 0x8001a488
        return result;
    }
    // 0x8001a3c0
    int32_t * newdir; // bp-268, 0x8001a3a0
    int32_t result2 = vfs_lookparent(newpath, (int32_t *)&newdir, newname, 256); // 0x8001a3cc
    if (result2 != 0) {
        // 0x8001a3d8
        vnode_decref(oldfile);
        // 0x8001a488
        return result2;
    }
    int32_t v1 = *(int32_t *)((int32_t)oldfile + 12); // 0x8001a3f4
    if (v1 != 0) {
        int32_t v2 = *(int32_t *)((int32_t)newdir + 12); // 0x8001a40c
        if (v2 != 0 == v1 == v2) {
            // 0x8001a444
            vnode_check(newdir, "link");
            vnode_decref(newdir);
            vnode_decref(oldfile);
            // 0x8001a488
            return *(int32_t *)(*(int32_t *)((int32_t)newdir + 20) + 72);
        }
    }
    // 0x8001a428
    vnode_decref(newdir);
    vnode_decref(oldfile);
    // 0x8001a488
    return 24;
}