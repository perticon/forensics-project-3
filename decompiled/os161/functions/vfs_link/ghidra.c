int vfs_link(char *oldpath,char *newpath)

{
  int iVar1;
  vnode *oldfile;
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookup(oldpath,&oldfile);
  if (iVar1 == 0) {
    iVar1 = vfs_lookparent(newpath,&newdir,newname,0x100);
    if (iVar1 == 0) {
      if (((oldfile->vn_fs == (fs *)0x0) || (newdir->vn_fs == (fs *)0x0)) ||
         (oldfile->vn_fs != newdir->vn_fs)) {
        vnode_decref(newdir);
        vnode_decref(oldfile);
        iVar1 = 0x18;
      }
      else {
        vnode_check(newdir,"link");
        iVar1 = (*newdir->vn_ops->vop_link)(newdir,newname,oldfile);
        vnode_decref(newdir);
        vnode_decref(oldfile);
      }
    }
    else {
      vnode_decref(oldfile);
    }
  }
  return iVar1;
}