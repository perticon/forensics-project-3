con_softc * attach_con_to_lser(int consno,lser_softc *ls)

{
  con_softc *pcVar1;
  
  pcVar1 = (con_softc *)kmalloc(0x3c);
  if (pcVar1 == (con_softc *)0x0) {
    pcVar1 = (con_softc *)0x0;
  }
  else {
    pcVar1->cs_devdata = ls;
    pcVar1->cs_send = lser_write;
    pcVar1->cs_sendpolled = lser_writepolled;
    ls->ls_devdata = pcVar1;
    ls->ls_start = con_start;
    ls->ls_input = con_input;
  }
  return pcVar1;
}