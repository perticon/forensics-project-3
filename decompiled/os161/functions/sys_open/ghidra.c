int sys_open(userptr_t path,int openflags,mode_t mode,int *errp)

{
  int iVar1;
  openfile *poVar2;
  int unaff_s7;
  vnode *v;
  
  iVar1 = vfs_open(&path->_dummy,openflags,mode,&v);
  if (iVar1 == 0) {
    for (iVar1 = 0; iVar1 < 0x500; iVar1 = iVar1 + 1) {
      if (systemFileTable[iVar1].vn == (vnode *)0x0) {
        poVar2 = systemFileTable + iVar1;
        poVar2->vn = v;
        *(undefined4 *)((int)&systemFileTable[iVar1].offset + 4) = 0;
        *(undefined4 *)&systemFileTable[iVar1].offset = 0;
        systemFileTable[iVar1].countRef = 1;
        goto LAB_8000d7b4;
      }
    }
    poVar2 = (openfile *)0x0;
LAB_8000d7b4:
    iVar1 = 3;
    if (poVar2 == (openfile *)0x0) {
      *errp = 0x1d;
    }
    else {
      for (; iVar1 < 0x80; iVar1 = iVar1 + 1) {
        if (*(int *)(*(int *)(unaff_s7 + 0x54) + (iVar1 + 0x18) * 4) == 0) {
          *(openfile **)(*(int *)(unaff_s7 + 0x54) + (iVar1 + 0x18) * 4) = poVar2;
          return iVar1;
        }
      }
      *errp = 0x1c;
    }
    vfs_close(v);
  }
  else {
    *errp = 0x13;
  }
  return -1;
}