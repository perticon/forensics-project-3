int sys_open(userptr_t path, int openflags, mode_t mode, int *errp)
{
  int fd, i;
  struct vnode *v;
  struct openfile *of = NULL;
  ;
  int result;

  result = vfs_open((char *)path, openflags, mode, &v);
  if (result)
  {
    *errp = ENOENT;
    return -1;
  }
  /* search system open file table */
  for (i = 0; i < SYSTEM_OPEN_MAX; i++)
  {
    if (systemFileTable[i].vn == NULL)
    {
      of = &systemFileTable[i];
      of->vn = v;
      of->offset = 0; // TODO: handle offset with append
      of->countRef = 1;
      break;
    }
  }
  if (of == NULL)
  {
    // no free slot in system open file table
    *errp = ENFILE;
  }
  else
  {
    for (fd = STDERR_FILENO + 1; fd < OPEN_MAX; fd++)
    {
      if (curproc->fileTable[fd] == NULL)
      {
        curproc->fileTable[fd] = of;
        return fd;
      }
    }
    // no free slot in process open file table
    *errp = EMFILE;
  }

  vfs_close(v);
  return -1;
}