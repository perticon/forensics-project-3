int32_t sys_open(int32_t * path, int32_t openflags, int32_t mode, int32_t * errp) {
    // 0x8000d710
    int32_t * v2; // bp-16, 0x8000d710
    int32_t v1 = vfs_open((char *)path, openflags, mode, &v2); // 0x8000d724
    int32_t v2_ = 0; // 0x8000d72c
    if (v1 != 0) {
        // 0x8000d730
        *errp = 19;
        // 0x8000d81c
        return -1;
    }
    int32_t v3 = 24 * v2_; // 0x8000d744
    int32_t v4 = v3 + (int32_t)&systemFileTable; // 0x8000d748
    int32_t * v5 = (int32_t *)v4; // 0x8000d74c
    int32_t v6; // 0x8000d710
    while (*v5 != 0) {
        // 0x8000d7a4
        v2_++;
        v6 = 29;
        if (v2_ >= 1280) {
            goto lab_0x8000d80c_2;
        }
        v3 = 24 * v2_;
        v4 = v3 + (int32_t)&systemFileTable;
        v5 = (int32_t *)v4;
    }
    // 0x8000d7b4
    *v5 = (int32_t)v2;
    *(int32_t *)(v3 + (int32_t)&systemFileTable + 12) = 0;
    *(int32_t *)(v3 + (int32_t)&systemFileTable + 8) = 0;
    *(int32_t *)(v3 + (int32_t)&systemFileTable + 16) = 1;
    v6 = 29;
    if (v4 == 0) {
      lab_0x8000d80c_2:
        // 0x8000d80c
        *errp = v6;
        vfs_close(v2);
        // 0x8000d81c
        return -1;
    }
    int32_t result = 3;
    int32_t v7; // 0x8000d710
    int32_t * v8 = (int32_t *)(*(int32_t *)(v7 + 84) + 96 + 4 * result); // 0x8000d7d4
    while (*v8 != 0) {
        int32_t v9 = result + 1; // 0x8000d7f4
        v6 = 28;
        if (v9 >= 128) {
            goto lab_0x8000d80c_2;
        }
        result = v9;
        v8 = (int32_t *)(*(int32_t *)(v7 + 84) + 96 + 4 * result);
    }
    // 0x8000d7e4
    *v8 = v4;
    // 0x8000d81c
    return result;
}