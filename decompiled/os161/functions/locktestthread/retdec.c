void locktestthread(char * junk, int32_t num) {
    int32_t v1 = num * num; // 0x80012b7c
    int32_t v2 = num % 3;
    int32_t v3 = 0; // 0x80012ce4
    lock_acquire(testlock);
    testval1 = num;
    testval2 = v1;
    testval3 = v2;
    int32_t v4 = v2; // 0x80012c44
    if (v1 % 3 != v2 * v2 % 3) {
        // 0x80012c54
        fail(num, "testval2/testval3");
        if (testval1 % 3 != testval3) {
            // 0x80012c7c
            fail(num, "testval3/testval1");
        }
        // 0x80012c88
        v4 = num;
        if (testval1 != num) {
            // 0x80012c98
            fail(num, "testval1/num");
            v4 = num;
        }
    }
    int32_t v5 = v4; // 0x80012cb0
    if (v1 != testval2) {
        // 0x80012cb4
        fail(num, "testval2/num");
        v5 = num;
    }
    // 0x80012cc0
    if (v5 != testval3) {
        // 0x80012cd0
        fail(num, "testval3/num");
    }
    // 0x80012cdc
    v3++;
    lock_release(testlock);
    while (v3 < 120) {
        // 0x80012bfc
        lock_acquire(testlock);
        testval1 = num;
        testval2 = v1;
        testval3 = v2;
        v4 = v2;
        if (v1 % 3 != v2 * v2 % 3) {
            // 0x80012c54
            fail(num, "testval2/testval3");
            if (testval1 % 3 != testval3) {
                // 0x80012c7c
                fail(num, "testval3/testval1");
            }
            // 0x80012c88
            v4 = num;
            if (testval1 != num) {
                // 0x80012c98
                fail(num, "testval1/num");
                v4 = num;
            }
        }
        // 0x80012ca4
        v5 = v4;
        if (v1 != testval2) {
            // 0x80012cb4
            fail(num, "testval2/num");
            v5 = num;
        }
        // 0x80012cc0
        if (v5 != testval3) {
            // 0x80012cd0
            fail(num, "testval3/num");
        }
        // 0x80012cdc
        v3++;
        lock_release(testlock);
    }
    // 0x80012cf4
    V((int32_t *)g16);
}