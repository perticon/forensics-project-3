int32_t semfs_creat(int32_t * dirvn, char * name, bool excl, int32_t mode, int32_t * resultvn) {
    int32_t v1 = *(int32_t *)(*(int32_t *)((int32_t)dirvn + 16) + 24); // 0x80007060
    if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) {
        // 0x80007374
        return 22;
    }
    int32_t * v2 = (int32_t *)(v1 + 20); // 0x80007094
    lock_acquire((int32_t *)*v2);
    int32_t * v3 = (int32_t *)(v1 + 24); // 0x800070a0
    uint32_t v4 = *(int32_t *)(*v3 + 4); // 0x800070a8
    int32_t v5 = 0; // 0x800070b0
    int32_t v6 = v4; // 0x800070b0
    uint32_t v7; // 0x8000702c
    int32_t result; // 0x8000702c
    while (true) {
        // 0x80007170
        v7 = v6;
        uint32_t v8 = v5;
        if (v8 >= v4) {
            // break (via goto) -> 0x8000717c
            goto lab_0x8000717c;
        }
        int32_t v9 = *v3; // 0x800070b4
        int32_t v10 = v9; // 0x800070cc
        if (v8 >= *(int32_t *)(v9 + 4)) {
            // 0x800070d0
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v10 = &g41;
        }
        int32_t v11 = *(int32_t *)(*(int32_t *)v10 + 4 * v8); // 0x800070f8
        while (v11 != 0) {
            // 0x80007118
            if (strcmp((char *)*(int32_t *)v11, name) == 0) {
                if (excl) {
                    // 0x80007134
                    lock_release((int32_t *)*v2);
                    result = 22;
                    return result;
                } else {
                    int32_t v12 = semfs_getvnode((int32_t *)v1, *(int32_t *)(v11 + 4), resultvn); // 0x80007154
                    lock_release((int32_t *)*v2);
                    result = v12;
                    return result;
                }
            }
            v8++;
            if (v8 >= v4) {
                // break (via goto) -> 0x8000717c
                goto lab_0x8000717c;
            }
            // 0x800070b4
            v9 = *v3;
            v10 = v9;
            if (v8 >= *(int32_t *)(v9 + 4)) {
                // 0x800070d0
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v10 = &g41;
            }
            // 0x800070ec
            v11 = *(int32_t *)(*(int32_t *)v10 + 4 * v8);
        }
        // 0x80007108
        v6 = v7 == v4 ? v8 : v7;
        v5 = v8 + 1;
    }
  lab_0x8000717c:;
    int32_t * v13 = semfs_sem_create(name); // 0x80007180
    int32_t result2 = 3; // 0x80007188
    int32_t v14; // 0x8000702c
    int32_t v15; // 0x8000702c
    int32_t v16; // 0x8000702c
    int32_t v17; // 0x8000702c
    int32_t semnum; // bp-40, 0x8000702c
    int32_t * v18; // 0x8000718c
    int32_t * v19; // 0x800071a4
    int32_t * v20; // 0x800071c4
    if (v13 == NULL) {
        goto lab_0x80007364;
    } else {
        // 0x8000718c
        v18 = (int32_t *)(v1 + 8);
        lock_acquire((int32_t *)*v18);
        v19 = (int32_t *)v1;
        int32_t v21 = semfs_sem_insert(v19, v13, &semnum); // 0x800071a4
        lock_release((int32_t *)*v18);
        v16 = v21;
        if (v21 == 0) {
            // 0x800071bc
            v20 = semfs_direntry_create(name, semnum);
            v15 = 0;
            if (v20 == NULL) {
                goto lab_0x800072f0;
            } else {
                int32_t v22 = (int32_t)v20; // 0x800071c4
                int32_t v23 = *v3;
                uint32_t v24 = *(int32_t *)(v23 + 4);
                if (v7 >= v4) {
                    int32_t * v25 = (int32_t *)v23; // 0x8000723c
                    int32_t v26 = array_setsize(v25, v24 + 1); // 0x8000723c
                    v14 = v26;
                    if (v26 == 0) {
                        // 0x80007264
                        *(int32_t *)(*v25 + 4 * v24) = v22;
                        v17 = v24;
                        goto lab_0x8000726c;
                    } else {
                        goto lab_0x800072e8;
                    }
                } else {
                    int32_t v27 = v23; // 0x800071f4
                    if (v7 >= v24) {
                        // 0x800071f8
                        badassert("index < a->num", "../../include/array.h", 107, "array_set");
                        v27 = &g41;
                    }
                    // 0x80007214
                    *(int32_t *)(*(int32_t *)v27 + 4 * v7) = v22;
                    v17 = v7;
                    goto lab_0x8000726c;
                }
            }
        } else {
            goto lab_0x80007350;
        }
    }
  lab_0x80007364:
    // 0x80007364
    lock_release((int32_t *)*v2);
    // 0x80007374
    return result2;
  lab_0x80007350:
    // 0x80007350
    semfs_sem_destroy(v13);
    result2 = v16;
    goto lab_0x80007364;
  lab_0x800072f0:
    // 0x800072f0
    lock_acquire((int32_t *)*v18);
    int32_t v28 = *(int32_t *)(v1 + 16); // 0x800072fc
    int32_t v29 = semnum; // 0x80007314
    if (semnum >= *(int32_t *)(v28 + 4)) {
        // 0x80007318
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v29 = &g41;
    }
    // 0x80007334
    *(int32_t *)(*(int32_t *)v28 + 4 * v29) = 0;
    lock_release((int32_t *)*v18);
    v16 = v15;
    goto lab_0x80007350;
  lab_0x800072e8:
    // 0x800072e8
    semfs_direntry_destroy(v20);
    v15 = v14;
    goto lab_0x800072f0;
  lab_0x8000726c:;
    int32_t v30 = semfs_getvnode(v19, semnum, resultvn); // 0x80007278
    if (v30 == 0) {
        // 0x80007284
        *(char *)((int32_t)v13 + 13) = 1;
        int32_t v31 = *v2; // 0x8000728c
        lock_release((int32_t *)v31);
        result = 0;
      lab_0x80007374:
        // 0x80007374
        return result;
    }
    int32_t v32 = *v3; // 0x800072a0
    int32_t v33 = v32; // 0x800072b8
    if (v17 >= *(int32_t *)(v32 + 4)) {
        // 0x800072bc
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v33 = &g41;
    }
    // 0x800072d8
    *(int32_t *)(*(int32_t *)v33 + 4 * v17) = 0;
    v14 = v30;
    goto lab_0x800072e8;
}