int semfs_creat(vnode *dirvn,char *name,bool excl,mode_t mode,vnode **resultvn)

{
  int iVar1;
  int iVar2;
  semfs_sem *sem;
  semfs_direntry *dent;
  semfs_direntryarray *psVar3;
  semfs_semarray *psVar4;
  uint uVar5;
  semfs *semfs;
  char **ppcVar6;
  uint uVar7;
  array *a;
  uint uVar8;
  uint semnum;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar1 = strcmp(name,".");
  iVar2 = 0x16;
  if (iVar1 != 0) {
    iVar1 = strcmp(name,"..");
    iVar2 = 0x16;
    if (iVar1 != 0) {
      uVar5 = 0;
      lock_acquire(semfs->semfs_dirlock);
      uVar7 = (semfs->semfs_dents->arr).num;
      uVar8 = uVar7;
LAB_80007170:
      do {
        if (uVar7 <= uVar5) {
          sem = semfs_sem_create(name);
          if (sem == (semfs_sem *)0x0) {
            iVar1 = 3;
            goto fail_unlock;
          }
          lock_acquire(semfs->semfs_tablelock);
          iVar2 = semfs_sem_insert(semfs,sem,&semnum);
          lock_release(semfs->semfs_tablelock);
          iVar1 = iVar2;
          if (iVar2 == 0) {
            dent = semfs_direntry_create(name,semnum);
            if (dent != (semfs_direntry *)0x0) {
              if (uVar8 < uVar7) {
                psVar3 = semfs->semfs_dents;
                if ((psVar3->arr).num <= uVar8) {
                  badassert("index < a->num","../../include/array.h",0x6b,"array_set");
                }
                (psVar3->arr).v[uVar8] = dent;
LAB_8000726c:
                iVar1 = semfs_getvnode(semfs,semnum,resultvn);
                if (iVar1 == 0) {
                  sem->sems_linked = true;
                  lock_release(semfs->semfs_dirlock);
                  return 0;
                }
                psVar3 = semfs->semfs_dents;
                if ((psVar3->arr).num <= uVar8) {
                  badassert("index < a->num","../../include/array.h",0x6b,"array_set");
                }
                (psVar3->arr).v[uVar8] = (void *)0x0;
              }
              else {
                a = (array *)semfs->semfs_dents;
                uVar5 = a->num;
                iVar1 = array_setsize(a,uVar5 + 1);
                if (iVar1 == 0) {
                  a->v[uVar5] = dent;
                  iVar1 = iVar2;
                  uVar8 = uVar5;
                }
                if (iVar1 == 0) goto LAB_8000726c;
              }
              semfs_direntry_destroy(dent);
            }
            lock_acquire(semfs->semfs_tablelock);
            psVar4 = semfs->semfs_sems;
            if ((psVar4->arr).num <= semnum) {
              badassert("index < a->num","../../include/array.h",0x6b,"array_set");
            }
            (psVar4->arr).v[semnum] = (void *)0x0;
            lock_release(semfs->semfs_tablelock);
          }
          semfs_sem_destroy(sem);
fail_unlock:
          lock_release(semfs->semfs_dirlock);
          return iVar1;
        }
        psVar3 = semfs->semfs_dents;
        if ((psVar3->arr).num <= uVar5) {
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        ppcVar6 = (char **)(psVar3->arr).v[uVar5];
        if (ppcVar6 == (char **)0x0) {
          if (uVar8 == uVar7) {
            uVar8 = uVar5;
          }
          uVar5 = uVar5 + 1;
          goto LAB_80007170;
        }
        iVar1 = strcmp(*ppcVar6,name);
        uVar5 = uVar5 + 1;
      } while (iVar1 != 0);
      if (excl == false) {
        iVar2 = semfs_getvnode(semfs,(uint)ppcVar6[1],resultvn);
        lock_release(semfs->semfs_dirlock);
      }
      else {
        lock_release(semfs->semfs_dirlock);
        iVar2 = 0x16;
      }
    }
  }
  return iVar2;
}