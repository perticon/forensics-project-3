semfs_creat(struct vnode *dirvn, const char *name, bool excl, mode_t mode,
	    struct vnode **resultvn)
{
	struct semfs_vnode *dirsemv = dirvn->vn_data;
	struct semfs *semfs = dirsemv->semv_semfs;
	struct semfs_direntry *dent;
	struct semfs_sem *sem;
	unsigned i, num, empty, semnum;
	int result;

	(void)mode;
	if (!strcmp(name, ".") || !strcmp(name, "..")) {
		return EEXIST;
	}

	lock_acquire(semfs->semfs_dirlock);
	num = semfs_direntryarray_num(semfs->semfs_dents);
	empty = num;
	for (i=0; i<num; i++) {
		dent = semfs_direntryarray_get(semfs->semfs_dents, i);
		if (dent == NULL) {
			if (empty == num) {
				empty = i;
			}
			continue;
		}
		if (!strcmp(dent->semd_name, name)) {
			/* found */
			if (excl) {
				lock_release(semfs->semfs_dirlock);
				return EEXIST;
			}
			result = semfs_getvnode(semfs, dent->semd_semnum,
						resultvn);
			lock_release(semfs->semfs_dirlock);
			return result;
		}
	}

	/* create it */
	sem = semfs_sem_create(name);
	if (sem == NULL) {
		result = ENOMEM;
		goto fail_unlock;
	}
	lock_acquire(semfs->semfs_tablelock);
	result = semfs_sem_insert(semfs, sem, &semnum);
	lock_release(semfs->semfs_tablelock);
	if (result) {
		goto fail_uncreate;
	}

	dent = semfs_direntry_create(name, semnum);
	if (dent == NULL) {
		goto fail_uninsert;
	}

	if (empty < num) {
		semfs_direntryarray_set(semfs->semfs_dents, empty, dent);
	}
	else {
		result = semfs_direntryarray_add(semfs->semfs_dents, dent,
						 &empty);
		if (result) {
			goto fail_undent;
		}
	}

	result = semfs_getvnode(semfs, semnum, resultvn);
	if (result) {
		goto fail_undir;
	}

	sem->sems_linked = true;
	lock_release(semfs->semfs_dirlock);
	return 0;

 fail_undir:
	semfs_direntryarray_set(semfs->semfs_dents, empty, NULL);
 fail_undent:
	semfs_direntry_destroy(dent);
 fail_uninsert:
	lock_acquire(semfs->semfs_tablelock);
	semfs_semarray_set(semfs->semfs_sems, semnum, NULL);
	lock_release(semfs->semfs_tablelock);
 fail_uncreate:
	semfs_sem_destroy(sem);
 fail_unlock:
	lock_release(semfs->semfs_dirlock);
	return result;
}