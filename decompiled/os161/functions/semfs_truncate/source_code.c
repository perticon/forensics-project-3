semfs_truncate(struct vnode *vn, off_t len)
{
	/* We should just use UINT_MAX but we don't have it in the kernel */
	const unsigned max = (unsigned)-1;

	struct semfs_vnode *semv = vn->vn_data;
	struct semfs_sem *sem;
	unsigned newcount;

	if (len < 0) {
		return EINVAL;
	}
	if (len > (off_t)max) {
		return EFBIG;
	}
	newcount = len;

	sem = semfs_getsem(semv);

	lock_acquire(sem->sems_lock);
	semfs_wakeup(sem, newcount);
	sem->sems_count = newcount;
	lock_release(sem->sems_lock);

	return 0;
}