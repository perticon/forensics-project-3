int32_t semfs_truncate(int32_t * vn, int64_t len) {
    int32_t v1 = len;
    if (v1 < 0) {
        // 0x800062b0
        return 8;
    }
    int32_t result = 38; // 0x80006274
    if (v1 == 0) {
        int32_t * v2 = semfs_getsem((int32_t *)*(int32_t *)((int32_t)vn + 16)); // 0x8000627c
        lock_acquire((int32_t *)*v2);
        int32_t v3; // 0x80006254
        semfs_wakeup(v2, v3);
        lock_release((int32_t *)*v2);
        result = 0;
    }
    // 0x800062b0
    return result;
}