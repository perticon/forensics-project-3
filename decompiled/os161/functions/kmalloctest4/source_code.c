kmalloctest4(int nargs, char **args)
{
	struct semaphore *sem;
	unsigned nthreads;
	unsigned i;
	int result;

	(void)nargs;
	(void)args;

	kprintf("Starting multipage kmalloc test...\n");
#if OPT_DUMBVM
	kprintf("(This test will not work with dumbvm)\n");
#endif

	sem = sem_create("kmalloctest4", 0);
	if (sem == NULL) {
		panic("kmalloctest4: sem_create failed\n");
	}

	/* use 6 instead of 8 threads */
	nthreads =  (3*NTHREADS)/4;

	for (i=0; i<nthreads; i++) {
		result = thread_fork("kmalloctest4", NULL,
				     kmalloctest4thread, sem, i);
		if (result) {
			panic("kmallocstress: thread_fork failed: %s\n",
			      strerror(result));
		}
	}

	for (i=0; i<nthreads; i++) {
		P(sem);
	}


	sem_destroy(sem);
	kprintf("Multipage kmalloc test done\n");
	return 0;
}