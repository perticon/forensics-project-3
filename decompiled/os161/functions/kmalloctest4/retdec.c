int32_t kmalloctest4(int32_t nargs, char ** args) {
    // 0x800113c4
    kprintf("Starting multipage kmalloc test...\n");
    int32_t * v1 = sem_create("kmalloctest4", 0); // 0x800113f4
    int32_t v2 = 0; // 0x800113fc
    int32_t v3 = (int32_t)&g2 + 3320; // 0x800113fc
    if (v1 == NULL) {
        // 0x80011400
        panic("kmalloctest4: sem_create failed\n");
        goto lab_0x8001140c;
    } else {
        goto lab_0x8001145c;
    }
  lab_0x8001145c:;
    int32_t v4 = v2; // 0x80011464
    int32_t v5 = v3; // 0x80011464
    if (v2 >= 6) {
        P(v1);
        int32_t v6 = 1; // 0x80011478
        int32_t v7 = v6; // 0x80011484
        while (v6 < 6) {
            // 0x80011470
            P(v1);
            v6 = v7 + 1;
            v7 = v6;
        }
        // 0x80011488
        sem_destroy(v1);
        kprintf("Multipage kmalloc test done\n");
        return 0;
    }
    goto lab_0x8001140c;
  lab_0x8001140c:
    // 0x8001140c
    v3 = v5;
    int32_t v8 = thread_fork("kmalloctest4", NULL, (void (*)(char *, int32_t))v3, (char *)v1, v4); // 0x8001141c
    if (v8 != 0) {
        // 0x80011428
        panic("kmallocstress: thread_fork failed: %s\n", strerror(v8));
    }
    // 0x80011440
    v2 = v4 + 1;
    goto lab_0x8001145c;
}