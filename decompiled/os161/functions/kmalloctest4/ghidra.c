int kmalloctest4(int nargs,char **args)

{
  semaphore *sem;
  int errcode;
  char *pcVar1;
  uint uVar2;
  
  kprintf("Starting multipage kmalloc test...\n");
  sem = sem_create("kmalloctest4",0);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("kmalloctest4: sem_create failed\n");
  }
  uVar2 = 0;
  while( true ) {
    if (5 < uVar2) {
      for (uVar2 = 0; uVar2 < 6; uVar2 = uVar2 + 1) {
        P(sem);
      }
      sem_destroy(sem);
      kprintf("Multipage kmalloc test done\n");
      return 0;
    }
    errcode = thread_fork("kmalloctest4",(proc *)0x0,kmalloctest4thread,sem,uVar2);
    if (errcode != 0) break;
    uVar2 = uVar2 + 1;
  }
  pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
  panic("kmallocstress: thread_fork failed: %s\n",pcVar1);
}