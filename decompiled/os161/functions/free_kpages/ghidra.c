void free_kpages(vaddr_t addr)

{
  int iVar1;
  char *first_page;
  int iVar2;
  uint addr_00;
  
  iVar1 = isTableActive();
  if (iVar1 != 0) {
    addr_00 = addr + 0x80000000;
    first_page = (char *)(addr_00 >> 0xc);
    if (nRamFrames <= (int)first_page) {
      first_page = s_______vm_addrspace_c_80027688;
      badassert("nRamFrames > first",s_______vm_addrspace_c_80027688,0x8f,"free_kpages");
    }
    iVar1 = freeppages(addr_00,(long)first_page);
    for (iVar2 = 0; iVar2 < iVar1; iVar2 = iVar2 + 1) {
      ipt_kadd(-1,iVar2 * 0x1000 + addr_00,0);
    }
  }
  return;
}