void free_kpages(vaddr_t addr)
{
  int i, npages;

  if (isTableActive())
  {
    paddr_t paddr = addr - MIPS_KSEG0;
    long first = paddr / PAGE_SIZE;
    KASSERT(nRamFrames > first);
    npages = freeppages(paddr, first);
    for (i = 0; i < npages; i++)
    {
      ipt_kadd(-1, paddr + i * PAGE_SIZE, 0);
    }
  }
}