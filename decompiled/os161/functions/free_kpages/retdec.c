void free_kpages(int32_t addr) {
    // 0x8001ab50
    if (isTableActive() == 0) {
        // 0x8001abe4
        return;
    }
    uint32_t v1 = -addr; // 0x8001ab74
    uint32_t v2 = v1 / 0x1000; // 0x8001ab78
    int32_t v3 = v2; // 0x8001ab90
    if (v2 >= g19) {
        // 0x8001ab94
        badassert("nRamFrames > first", "../../vm/addrspace.c", 143, "free_kpages");
        v3 = (int32_t)"../../vm/addrspace.c";
    }
    // 0x8001abb0
    for (int32_t i = 0; i < freeppages(v1, v3); i++) {
        // 0x8001abc4
        ipt_kadd(-1, 0x1000 * i - addr, 0);
    }
}