randeachopen(struct device *dev, int openflags)
{
	(void)dev;

	if (openflags != O_RDONLY) {
		return EIO;
	}

	return 0;
}