con_input(void *vcs, int ch)
{
	struct con_softc *cs = vcs;
	unsigned nexthead;

	nexthead = (cs->cs_gotchars_head + 1) % CONSOLE_INPUT_BUFFER_SIZE;
	if (nexthead == cs->cs_gotchars_tail) {
		/* overflow; drop character */
		return;
	}

	cs->cs_gotchars[cs->cs_gotchars_head] = ch;
	cs->cs_gotchars_head = nexthead;

	V(cs->cs_rsem);
}