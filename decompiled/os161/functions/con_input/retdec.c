void con_input(char * vcs, int32_t ch) {
    int32_t v1 = (int32_t)vcs;
    int32_t * v2 = (int32_t *)(v1 + 52); // 0x80001cec
    int32_t v3 = *v2; // 0x80001cec
    uint32_t v4 = (v3 + 1) % 32; // 0x80001cf8
    if (*(int32_t *)(v1 + 56) != v4) {
        // 0x80001d0c
        *(char *)(v1 + 20 + v3) = (char)ch;
        *v2 = v4;
        V((int32_t *)*(int32_t *)(v1 + 12));
    }
}