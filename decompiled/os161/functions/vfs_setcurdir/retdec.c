int32_t vfs_setcurdir(int32_t * dir) {
    int32_t v1 = (int32_t)dir;
    vnode_check(dir, "gettype");
    int32_t result = *(int32_t *)(*(int32_t *)(v1 + 20) + 36); // 0x80018084
    if (result != 0) {
        // 0x800180fc
        return result;
    }
    // 0x8001809c
    int32_t v2; // 0x8001805c
    if (v2 != 0x2000) {
        // 0x800180fc
        return 17;
    }
    // 0x800180b0
    vnode_incref(dir);
    int32_t * v3 = (int32_t *)(v2 + 84); // 0x800180b8
    spinlock_acquire((int32_t *)(*v3 + 8));
    int32_t * v4 = (int32_t *)(*v3 + 20); // 0x800180cc
    int32_t v5 = *v4; // 0x800180cc
    *v4 = v1;
    spinlock_release((int32_t *)(*v3 + 8));
    if (v5 != 0) {
        // 0x800180e8
        vnode_decref((int32_t *)v5);
    }
    // 0x800180fc
    return 0;
}