int vfs_setcurdir(vnode *dir)

{
  int iVar1;
  vnode *vn;
  int unaff_s7;
  mode_t vtype;
  
  vnode_check(dir,"gettype");
  iVar1 = (*dir->vn_ops->vop_gettype)(dir,&vtype);
  if ((iVar1 == 0) && (iVar1 = 0x11, vtype == 0x2000)) {
    vnode_incref(dir);
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14) = dir;
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    if (vn == (vnode *)0x0) {
      iVar1 = 0;
    }
    else {
      vnode_decref(vn);
      iVar1 = 0;
    }
  }
  return iVar1;
}