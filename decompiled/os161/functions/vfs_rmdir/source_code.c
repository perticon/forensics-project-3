vfs_rmdir(char *path)
{
	struct vnode *parent;
	char name[NAME_MAX+1];
	int result;

	result = vfs_lookparent(path, &parent, name, sizeof(name));
	if (result) {
		return result;
	}

	result = VOP_RMDIR(parent, name);

	VOP_DECREF(parent);

	return result;
}