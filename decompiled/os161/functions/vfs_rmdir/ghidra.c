int vfs_rmdir(char *path)

{
  int iVar1;
  vnode *parent;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&parent,name,0x100);
  if (iVar1 == 0) {
    vnode_check(parent,"rmdir");
    iVar1 = (*parent->vn_ops->vop_rmdir)(parent,name);
    vnode_decref(parent);
  }
  return iVar1;
}