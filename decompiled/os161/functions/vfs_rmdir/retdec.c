int32_t vfs_rmdir(char * path) {
    char name[256]; // bp-268, 0x8001a5f0
    // 0x8001a5f0
    int32_t * parent; // bp-272, 0x8001a5f0
    int32_t v1 = vfs_lookparent(path, (int32_t *)&parent, name, 256); // 0x8001a608
    int32_t result = v1; // 0x8001a610
    if (v1 == 0) {
        // 0x8001a614
        vnode_check(parent, "rmdir");
        result = *(int32_t *)(*(int32_t *)((int32_t)parent + 20) + 80);
        vnode_decref(parent);
    }
    // 0x8001a650
    return result;
}