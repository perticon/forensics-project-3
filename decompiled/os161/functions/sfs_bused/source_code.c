sfs_bused(struct sfs_fs *sfs, daddr_t diskblock)
{
	if (diskblock >= sfs->sfs_sb.sb_nblocks) {
		panic("sfs: %s: sfs_bused called on out of range block %u\n",
		      sfs->sfs_sb.sb_volname, diskblock);
	}
	return bitmap_isset(sfs->sfs_freemap, diskblock);
}