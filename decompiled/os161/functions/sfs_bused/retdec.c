int32_t sfs_bused(int32_t * sfs, uint32_t diskblock) {
    int32_t v1 = (int32_t)sfs;
    int32_t v2 = diskblock; // 0x800074b0
    if (*(int32_t *)(v1 + 12) <= diskblock) {
        // 0x800074b4
        v2 = v1 + 16;
        panic("sfs: %s: sfs_bused called on out of range block %u\n", (char *)v2, diskblock);
    }
    // 0x800074c4
    return bitmap_isset((int32_t *)0x75252073, v2);
}