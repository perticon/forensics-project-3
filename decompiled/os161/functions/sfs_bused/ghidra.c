int sfs_bused(sfs_fs *sfs,daddr_t diskblock)

{
  int iVar1;
  
  if ((sfs->sfs_sb).sb_nblocks <= diskblock) {
                    /* WARNING: Subroutine does not return */
    panic("sfs: %s: sfs_bused called on out of range block %u\n",(sfs->sfs_sb).sb_volname,diskblock)
    ;
  }
  iVar1 = bitmap_isset(sfs->sfs_freemap,diskblock);
  return iVar1;
}