uint32_t lhd_rdreg(struct lhd_softc *lh, uint32_t reg)
{
	return bus_read_register(lh->lh_busdata, lh->lh_buspos, reg);
}