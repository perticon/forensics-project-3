uint32_t bswap32(uint32_t val)

{
  return val << 0x18 | (val & 0xff00) << 8 | val >> 8 & 0xff00 | val >> 0x18;
}