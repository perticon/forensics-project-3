int32_t bswap32(int32_t val) {
    // 0x8000aff8
    return llvm_bswap_i32(val);
}