sfs_truncate(struct vnode *v, off_t len)
{
	struct sfs_vnode *sv = v->vn_data;

	return sfs_itrunc(sv, len);
}