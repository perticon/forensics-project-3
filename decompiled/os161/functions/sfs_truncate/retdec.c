int32_t sfs_truncate(int32_t * v2, int64_t len) {
    int32_t v1 = *(int32_t *)((int32_t)v2 + 16); // 0x80009530
    return sfs_itrunc((int32_t *)v1, 0x100000000 * len >> 32);
}