int32_t cmd_kheapdump(int32_t nargs, char ** args) {
    switch (nargs) {
        case 1: {
            // 0x8000c214
            kheap_dump();
            // 0x8000c268
            return 0;
        }
        case 2: {
            // 0x8000c230
            if (strcmp((char *)*(int32_t *)((int32_t)args + 4), "all") != 0) {
                // 0x8000c25c
                kprintf("Usage: khdump [all]\n");
                // 0x8000c268
                return 0;
            }
            // break -> 0x8000c248
            break;
        }
        default: {
            // 0x8000c25c
            kprintf("Usage: khdump [all]\n");
            // 0x8000c268
            return 0;
        }
    }
    // 0x8000c248
    kheap_dumpall();
    // 0x8000c268
    return 0;
}