int cmd_kheapdump(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 1) {
    kheap_dump();
  }
  else if ((nargs == 2) && (iVar1 = strcmp(args[1],"all"), iVar1 == 0)) {
    kheap_dumpall();
  }
  else {
    kprintf("Usage: khdump [all]\n");
  }
  return 0;
}