cmd_kheapdump(int nargs, char **args)
{
	if (nargs == 1)
	{
		kheap_dump();
	}
	else if (nargs == 2 && !strcmp(args[1], "all"))
	{
		kheap_dumpall();
	}
	else
	{
		kprintf("Usage: khdump [all]\n");
	}

	return 0;
}