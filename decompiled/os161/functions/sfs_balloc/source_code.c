sfs_balloc(struct sfs_fs *sfs, daddr_t *diskblock)
{
	int result;

	result = bitmap_alloc(sfs->sfs_freemap, diskblock);
	if (result) {
		return result;
	}
	sfs->sfs_freemapdirty = true;

	if (*diskblock >= sfs->sfs_sb.sb_nblocks) {
		panic("sfs: %s: balloc: invalid block %u\n",
		      sfs->sfs_sb.sb_volname, *diskblock);
	}

	/* Clear block before returning it */
	result = sfs_clearblock(sfs, *diskblock);
	if (result) {
		bitmap_unmark(sfs->sfs_freemap, *diskblock);
	}
	return result;
}