int32_t sfs_balloc(int32_t * sfs, int32_t * diskblock) {
    int32_t v1 = (int32_t)sfs;
    int32_t * v2 = (int32_t *)(v1 + 532); // 0x800073e0
    int32_t result = bitmap_alloc((int32_t *)*v2, diskblock); // 0x800073e8
    if (result != 0) {
        // 0x80007448
        return result;
    }
    uint32_t v3 = (int32_t)diskblock;
    *(char *)(v1 + 536) = 1;
    int32_t v4 = v1; // 0x80007410
    if (*(int32_t *)(v1 + 12) <= v3) {
        // 0x80007414
        panic("sfs: %s: balloc: invalid block %u\n", (char *)(v1 + 16), v3);
        v4 = (int32_t)"sfs: %s: balloc: invalid block %u\n";
    }
    // 0x80007424
    sfs_clearblock((int32_t *)v4, v3);
    int32_t result2 = 0; // 0x80007430
    if (result2 != 0) {
        // 0x80007434
        bitmap_unmark((int32_t *)*v2, v3);
    }
    // 0x80007448
    return result2;
}