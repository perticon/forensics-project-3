int sfs_balloc(sfs_fs *sfs,daddr_t *diskblock)

{
  int iVar1;
  
  iVar1 = bitmap_alloc(sfs->sfs_freemap,diskblock);
  if (iVar1 == 0) {
    sfs->sfs_freemapdirty = true;
    if ((sfs->sfs_sb).sb_nblocks <= *diskblock) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: balloc: invalid block %u\n",(sfs->sfs_sb).sb_volname);
    }
    iVar1 = sfs_clearblock(sfs,*diskblock);
    if (iVar1 != 0) {
      bitmap_unmark(sfs->sfs_freemap,*diskblock);
    }
  }
  return iVar1;
}