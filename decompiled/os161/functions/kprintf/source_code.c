kprintf(const char *fmt, ...)
{
	int chars;
	va_list ap;
	bool dolock;

	dolock = kprintf_lock != NULL
		&& curthread->t_in_interrupt == false
		&& curthread->t_curspl == 0
		&& curcpu->c_spinlocks == 0;

	if (dolock) {
		lock_acquire(kprintf_lock);
	}
	else {
		spinlock_acquire(&kprintf_spinlock);
	}

	va_start(ap, fmt);
	chars = __vprintf(console_send, NULL, fmt, ap);
	va_end(ap);

	if (dolock) {
		lock_release(kprintf_lock);
	}
	else {
		spinlock_release(&kprintf_spinlock);
	}

	return chars;
}