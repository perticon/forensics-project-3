int kprintf(char *fmt,...)

{
  bool bVar1;
  int iVar2;
  undefined4 in_a1;
  undefined4 in_a2;
  undefined4 in_a3;
  int unaff_s7;
  undefined4 local_res4;
  undefined4 local_res8;
  undefined4 local_resc;
  va_list ap;
  
  if (kprintf_lock == (lock *)0x0) {
    bVar1 = false;
  }
  else if (*(char *)(unaff_s7 + 0x58) == '\0') {
    if (*(int *)(unaff_s7 + 0x5c) == 0) {
      bVar1 = false;
      if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) == 0) {
        bVar1 = true;
      }
    }
    else {
      bVar1 = false;
    }
  }
  else {
    bVar1 = false;
  }
  local_res4 = in_a1;
  local_res8 = in_a2;
  local_resc = in_a3;
  if (bVar1) {
    lock_acquire(kprintf_lock);
  }
  else {
    spinlock_acquire(&kprintf_spinlock);
  }
  iVar2 = __vprintf(console_send,(void *)0x0,fmt,&local_res4);
  if (bVar1) {
    lock_release(kprintf_lock);
  }
  else {
    spinlock_release(&kprintf_spinlock);
  }
  return iVar2;
}