int32_t kprintf(char * fmt, ...) {
    // 0x8000b338
    int32_t v1; // 0x8000b338
    if (kprintf_lock != NULL) {
        // 0x8000b36c
        int32_t v2; // 0x8000b338
        if (*(char *)(v2 + 88) == 0) {
            // 0x8000b37c
            if (*(int32_t *)(v2 + 92) == 0) {
                // 0x8000b3c8
                if (*(int32_t *)(*(int32_t *)(v2 + 80) + 48) == 0) {
                    // 0x8000b410
                    lock_acquire(kprintf_lock);
                    int32_t result = __vprintf((void (*)(char *, char *, int32_t))((int32_t)&g2 - 0x4d20), NULL, fmt, (char *)&v1); // 0x8000b404
                    lock_release(kprintf_lock);
                    // 0x8000b438
                    return result;
                }
            }
        }
    }
    // 0x8000b428
    spinlock_acquire(&kprintf_spinlock);
    int32_t result2 = __vprintf((void (*)(char *, char *, int32_t))((int32_t)&g2 - 0x4d20), NULL, fmt, (char *)&v1); // 0x8000b404
    spinlock_release(&kprintf_spinlock);
    // 0x8000b438
    return result2;
}