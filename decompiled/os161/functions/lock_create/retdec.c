int32_t * lock_create(char * name) {
    char * v1 = kmalloc(20); // 0x800157f0
    if (v1 == NULL) {
        // 0x80015864
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x80015800
    *(int32_t *)v1 = (int32_t)v2;
    if (v2 == NULL) {
        // 0x8001580c
        kfree(v1);
        // 0x80015864
        return NULL;
    }
    int32_t v3 = (int32_t)v1; // 0x800157f0
    int32_t * v4 = sem_create(v2, 1); // 0x80015824
    *(int32_t *)(v3 + 4) = (int32_t)v4;
    int32_t * result; // 0x800157d8
    if (v4 == NULL) {
        // 0x80015830
        kfree((char *)*(int32_t *)v1);
        kfree(v1);
        result = NULL;
    } else {
        // 0x8001584c
        *(int32_t *)(v3 + 16) = 0;
        spinlock_init((int32_t *)(v3 + 8));
        result = (int32_t *)v1;
    }
    // 0x80015864
    return result;
}