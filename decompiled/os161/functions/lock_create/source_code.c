lock_create(const char *name)
{
        struct lock *lock;

        lock = kmalloc(sizeof(*lock));
        if (lock == NULL) {
                return NULL;
        }

        lock->lk_name = kstrdup(name);
        if (lock->lk_name == NULL) {
                kfree(lock);
                return NULL;
        }

        // add stuff here as needed

#if OPT_SYNCH
#if USE_SEMAPHORE_FOR_LOCK
        lock->lk_sem = sem_create(lock->lk_name,1);
	if (lock->lk_sem == NULL) {
#else
	lock->lk_wchan = wchan_create(lock->lk_name);
	if (lock->lk_wchan == NULL) {
#endif
	  kfree(lock->lk_name);
	  kfree(lock);
	  return NULL;
	}
	lock->lk_owner = NULL;
	spinlock_init(&lock->lk_lock);
#endif	
        return lock;
}