lock * lock_create(char *name)

{
  lock *ptr;
  char *name_00;
  semaphore *psVar1;
  
  ptr = (lock *)kmalloc(0x14);
  if (ptr == (lock *)0x0) {
    ptr = (lock *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->lk_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (lock *)0x0;
    }
    else {
      psVar1 = sem_create(name_00,1);
      ptr->lk_sem = psVar1;
      if (psVar1 == (semaphore *)0x0) {
        kfree(ptr->lk_name);
        kfree(ptr);
        ptr = (lock *)0x0;
      }
      else {
        ptr->lk_owner = (thread *)0x0;
        spinlock_init(&ptr->lk_lock);
      }
    }
  }
  return ptr;
}