void array_cleanup(array *a)

{
  char *pcVar1;
  
  pcVar1 = (char *)a;
  if (a->num != 0) {
    pcVar1 = "a->num == 0";
    badassert("a->num == 0","../../lib/array.c",0x47,"array_cleanup");
  }
  kfree(*(void ***)pcVar1);
  a->v = (void **)0x0;
  return;
}