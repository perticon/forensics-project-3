array_cleanup(struct array *a)
{
	/*
	 * Require array to be empty - helps avoid memory leaks since
	 * we don't/can't free anything any contents may be pointing
	 * to.
	 */
	ARRAYASSERT(a->num == 0);
	kfree(a->v);
#ifdef ARRAYS_CHECKED
	a->v = NULL;
#endif
}