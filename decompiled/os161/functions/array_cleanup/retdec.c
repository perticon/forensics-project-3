void array_cleanup(int32_t * a) {
    // 0x8000a8a8
    if (*(int32_t *)((int32_t)a + 4) != 0) {
        // 0x8000a8c4
        badassert("a->num == 0", "../../lib/array.c", 71, "array_cleanup");
    }
    // 0x8000a8e4
    kfree((char *)0x6e3e2d61);
    *a = 0;
}