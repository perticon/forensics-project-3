static void print_recursive(struct swap_entry *next)
{

    if (next == free_list_tail)
    {
        return;
    }

    kprintf("%llu -   %d   - %d\n", next->file_offset / PAGE_SIZE, next->pid, next->page / PAGE_SIZE);

    print_recursive(next->next);
}