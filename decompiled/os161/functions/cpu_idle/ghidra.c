void cpu_idle(void)

{
  wait(0);
  cpu_irqonoff();
  return;
}