void flush_delay_buf(void) {
    // 0x80001dec
    if (delayed_outbuf_pos == 0) {
        // 0x80001e34
        delayed_outbuf_pos = 0;
        return;
    }
    int32_t v1 = 0;
    int32_t v2 = v1 + 1; // 0x80001e1c
    putch((int32_t)*(char *)(v1 + (int32_t)&delayed_outbuf));
    while (v2 < delayed_outbuf_pos) {
        // 0x80001e14
        v1 = v2;
        v2 = v1 + 1;
        putch((int32_t)*(char *)(v1 + (int32_t)&delayed_outbuf));
    }
    // 0x80001e34
    delayed_outbuf_pos = 0;
}