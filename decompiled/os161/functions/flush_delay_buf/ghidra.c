void flush_delay_buf(void)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < delayed_outbuf_pos; uVar1 = uVar1 + 1) {
    putch((int)delayed_outbuf[uVar1]);
  }
  delayed_outbuf_pos = 0;
  return;
}