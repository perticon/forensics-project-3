flush_delay_buf(void)
{
	size_t i;
	for (i=0; i<delayed_outbuf_pos; i++) {
		putch(delayed_outbuf[i]);
	}
	delayed_outbuf_pos = 0;
}