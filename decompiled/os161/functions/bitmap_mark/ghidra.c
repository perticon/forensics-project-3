void bitmap_mark(bitmap *b,uint index)

{
  uint uVar1;
  byte *pbVar2;
  char *pcVar3;
  char *pcVar4;
  uint uVar5;
  byte bVar6;
  
  uVar1 = index >> 3;
  if (b->nbits <= index) {
    pcVar3 = "index < b->nbits";
    pcVar4 = s_______lib_bitmap_c_800235b4;
    badassert("index < b->nbits",s_______lib_bitmap_c_800235b4,0x8c,"bitmap_mark");
    b = (bitmap *)pcVar3;
    index = (uint)pcVar4;
  }
  uVar5 = 1 << (index & 7) & 0xff;
  pbVar2 = b->v + uVar1;
  bVar6 = *pbVar2 | (byte)uVar5;
  if ((*pbVar2 & uVar5) != 0) {
    bVar6 = 0xb4;
    badassert("(b->v[ix] & mask)==0",s_______lib_bitmap_c_800235b4,0x8f,"bitmap_mark");
  }
  *pbVar2 = bVar6;
  return;
}