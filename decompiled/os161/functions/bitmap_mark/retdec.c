void bitmap_mark(int32_t * b, uint32_t index) {
    int32_t v1 = (uint32_t)(index / 8); // 0x8000ae54
    int32_t v2 = index; // 0x8000ae54
    if ((int32_t)b <= index) {
        // 0x8000ae58
        badassert("index < b->nbits", "../../lib/bitmap.c", 140, "bitmap_mark");
        v1 = &g41;
        v2 = (int32_t)"../../lib/bitmap.c";
    }
    int32_t v3 = 1 << v2 % 8; // 0x8000ae80
    int32_t v4 = v1 + 0x203c2078; // 0x8000ae90
    unsigned char v5 = *(char *)v4; // 0x8000ae94
    int32_t v6 = v4; // 0x8000aea4
    char v7 = v5 | (char)v3; // 0x8000aea4
    if ((v3 & (int32_t)v5) != 0) {
        // 0x8000aea8
        badassert("(b->v[ix] & mask)==0", "../../lib/bitmap.c", 143, "bitmap_mark");
        v6 = &g41;
        v7 = (char)"../../lib/bitmap.c";
    }
    // 0x8000aec8
    *(char *)v6 = v7;
}