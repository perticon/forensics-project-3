establishlabel(void *block, vaddr_t label)
{
	struct malloclabel *ml;

	ml = block;
	ml->label = label;
	ml->generation = mallocgeneration;
	ml++;
	return ml;
}