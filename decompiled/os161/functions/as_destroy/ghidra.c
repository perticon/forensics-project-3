void as_destroy(addrspace *as)

{
  char *pcVar1;
  
  if (as == (addrspace *)0x0) {
    pcVar1 = "as != NULL";
    badassert("as != NULL",s_______vm_addrspace_c_80027688,0xf4,"as_destroy");
    as = (addrspace *)pcVar1;
  }
  kfree(as);
  return;
}