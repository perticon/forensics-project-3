void as_destroy(struct addrspace *as)
{
  KASSERT(as != NULL);
  kfree(as);
}