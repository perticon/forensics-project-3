void as_destroy(int32_t * as) {
    int32_t v1 = (int32_t)as; // 0x8001ad38
    if (as == NULL) {
        // 0x8001ad3c
        badassert("as != NULL", "../../vm/addrspace.c", 244, "as_destroy");
        v1 = (int32_t)"as != NULL";
    }
    // 0x8001ad5c
    kfree((char *)v1);
}