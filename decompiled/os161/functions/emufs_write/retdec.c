int32_t emufs_write(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2_ = *(int32_t *)((int32_t)v2 + 16); // 0x80002c00
    int32_t v3; // 0x80002bec
    if (*(int32_t *)(v1 + 24) == 1) {
        // 0x80002bec
        v3 = *(int32_t *)(v1 + 16);
        goto lab_0x80002c6c;
    } else {
        // 0x80002c14
        badassert("uio->uio_rw==UIO_WRITE", "../../dev/lamebus/emu.c", 604, "emufs_write");
        goto lab_0x80002c34;
    }
  lab_0x80002c6c:;
    int32_t v4 = v3; // 0x80002c78
    if (v3 == 0) {
        // 0x80002c7c
        return 0;
    }
    goto lab_0x80002c34;
  lab_0x80002c34:;
    uint32_t v5 = v4;
    int32_t v6 = *(int32_t *)(v2_ + 24); // 0x80002c44
    int32_t v7 = *(int32_t *)(v2_ + 28); // 0x80002c48
    int32_t v8 = emu_write((int32_t *)v6, v7, v5 < 0x4000 ? v5 : 0x4000, uio); // 0x80002c50
    int32_t result = v8; // 0x80002c58
    if (v8 != 0) {
        // 0x80002c7c
        return result;
    }
    int32_t v9 = *(int32_t *)(v1 + 16); // 0x80002c5c
    v3 = v9;
    if (v5 == v9) {
        // 0x80002c7c
        return 0;
    }
    goto lab_0x80002c6c;
}