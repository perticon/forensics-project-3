config_lhd(struct lhd_softc *lh, int lhdno)
{
	char name[32];

	/* Figure out what our name is. */
	snprintf(name, sizeof(name), "lhd%d", lhdno);

	/* Get a pointer to the on-chip buffer. */
	lh->lh_buf = bus_map_area(lh->lh_busdata, lh->lh_buspos, LHD_BUFFER);

	/* Create the semaphores. */
	lh->lh_clear = sem_create("lhd-clear", 1);
	if (lh->lh_clear == NULL) {
		return ENOMEM;
	}
	lh->lh_done = sem_create("lhd-done", 0);
	if (lh->lh_done == NULL) {
		sem_destroy(lh->lh_clear);
		lh->lh_clear = NULL;
		return ENOMEM;
	}

	/* Set up the VFS device structure. */
	lh->lh_dev.d_ops = &lhd_devops;
	lh->lh_dev.d_blocks = bus_read_register(lh->lh_busdata, lh->lh_buspos,
						LHD_REG_NSECT);
	lh->lh_dev.d_blocksize = LHD_SECTSIZE;
	lh->lh_dev.d_data = lh;

	/* Add the VFS device structure to the VFS device list. */
	return vfs_adddev(name, &lh->lh_dev, 1);
}