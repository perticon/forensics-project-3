int config_lhd(lhd_softc *lh,int lhdno)

{
  void *pvVar1;
  semaphore *psVar2;
  int iVar3;
  uint32_t uVar4;
  char name [32];
  
  snprintf(name,0x20,"lhd%d",lhdno);
  pvVar1 = lamebus_map_area((lamebus_softc *)lh->lh_busdata,lh->lh_buspos,0x8000);
  lh->lh_buf = pvVar1;
  psVar2 = sem_create("lhd-clear",1);
  lh->lh_clear = psVar2;
  if (psVar2 == (semaphore *)0x0) {
    iVar3 = 3;
  }
  else {
    psVar2 = sem_create("lhd-done",0);
    lh->lh_done = psVar2;
    if (psVar2 == (semaphore *)0x0) {
      sem_destroy(lh->lh_clear);
      lh->lh_clear = (semaphore *)0x0;
      iVar3 = 3;
    }
    else {
      (lh->lh_dev).d_ops = &lhd_devops;
      uVar4 = lamebus_read_register((lamebus_softc *)lh->lh_busdata,lh->lh_buspos,0);
      (lh->lh_dev).d_blocks = uVar4;
      (lh->lh_dev).d_blocksize = 0x200;
      (lh->lh_dev).d_data = lh;
      iVar3 = vfs_adddev(name,&lh->lh_dev,1);
    }
  }
  return iVar3;
}