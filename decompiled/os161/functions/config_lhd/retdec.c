int32_t config_lhd(int32_t * lh, int32_t lhdno) {
    char name[32]; // bp-40, 0x80004914
    int32_t v1 = (int32_t)lh;
    snprintf(name, 32, "lhd%d", lhdno);
    int32_t * v2 = (int32_t *)(v1 + 4); // 0x80004940
    char * v3 = lamebus_map_area((int32_t *)&name, *v2, 0x8000); // 0x80004948
    *(int32_t *)(v1 + 12) = (int32_t)v3;
    int32_t * v4 = sem_create("lhd-clear", 1); // 0x8000495c
    int32_t * v5 = (int32_t *)(v1 + 20); // 0x80004964
    *v5 = (int32_t)v4;
    if (v4 == NULL) {
        // 0x800049e0
        return 3;
    }
    int32_t * v6 = sem_create("lhd-done", 0); // 0x80004974
    *(int32_t *)(v1 + 24) = (int32_t)v6;
    int32_t result; // 0x80004914
    if (v6 == NULL) {
        // 0x80004980
        sem_destroy((int32_t *)*v5);
        *v5 = 0;
        result = 3;
    } else {
        int32_t * v7 = (int32_t *)(v1 + 28); // 0x800049a0
        *v7 = (int32_t)&lhd_devops;
        *(int32_t *)(v1 + 32) = lamebus_read_register((int32_t *)"lhd-done", *v2, 0);
        *(int32_t *)(v1 + 36) = 512;
        *(int32_t *)(v1 + 44) = v1;
        result = vfs_adddev(name, v7, 1);
    }
    // 0x800049e0
    return result;
}