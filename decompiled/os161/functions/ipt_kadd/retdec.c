int32_t ipt_kadd(uint32_t pid, int32_t paddr, int32_t vaddr) {
    int32_t v1 = paddr; // 0x8001cf94
    int32_t v2 = vaddr; // 0x8001cf94
    if (pid <= 0xfffffffd) {
        // 0x8001cf98
        badassert("pid == -2 || pid == -1", "../../vm/pt.c", 198, "ipt_kadd");
        v1 = (int32_t)"../../vm/pt.c";
        v2 = 198;
    }
    uint32_t v3 = v1 / 0x1000; // 0x8001cfb8
    if (v3 >= nRamFrames) {
        // 0x8001cfd4
        badassert("frame_index < nRamFrames", "../../vm/pt.c", 200, "ipt_kadd");
    }
    // 0x8001cff4
    spinlock_acquire(&ipt_lock);
    if (ipt_active != 0) {
        int32_t v4 = 8 * v3 + (int32_t)ipt; // 0x8001d020
        *(int32_t *)v4 = pid;
        *(int32_t *)(v4 + 4) = v2;
    }
    // 0x8001d02c
    spinlock_release(&ipt_lock);
    return 0;
}