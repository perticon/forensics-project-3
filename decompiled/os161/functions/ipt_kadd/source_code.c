int ipt_kadd(pid_t pid, paddr_t paddr, vaddr_t vaddr)
{
    int frame_index;
    KASSERT(pid == -2 || pid == -1);
    frame_index = paddr / PAGE_SIZE;
    KASSERT(frame_index < nRamFrames);

    spinlock_acquire(&ipt_lock);
    if (ipt_active)
    {
        ipt[frame_index].pid = pid;
        ipt[frame_index].vaddr = vaddr;
    }
    spinlock_release(&ipt_lock);

    return 0;
}