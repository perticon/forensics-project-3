int ipt_kadd(pid_t pid,paddr_t paddr,vaddr_t vaddr)

{
  char *pcVar1;
  ipt_entry *piVar2;
  
  if (1 < pid + 2U) {
    pcVar1 = "../../vm/pt.c";
    vaddr = 0xc6;
    badassert("pid == -2 || pid == -1","../../vm/pt.c",0xc6,"ipt_kadd");
    paddr = (paddr_t)pcVar1;
  }
  if (nRamFrames <= (int)(paddr >> 0xc)) {
    badassert("frame_index < nRamFrames","../../vm/pt.c",200,"ipt_kadd");
  }
  spinlock_acquire(&ipt_lock);
  if (ipt_active != 0) {
    piVar2 = ipt + (paddr >> 0xc);
    piVar2->pid = pid;
    piVar2->vaddr = vaddr;
  }
  spinlock_release(&ipt_lock);
  return 0;
}