void dofstest(char *filesys)

{
  int iVar1;
  
  kprintf("*** Starting filesystem test on %s:\n",filesys);
  iVar1 = fstest_write(filesys,"",1,0);
  if (iVar1 == 0) {
    iVar1 = fstest_read(filesys,"");
    if (iVar1 == 0) {
      iVar1 = fstest_remove(filesys,"");
      if (iVar1 == 0) {
        kprintf("*** Filesystem test done\n");
      }
      else {
        kprintf("*** Test failed\n");
      }
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    kprintf("*** Test failed\n");
  }
  return;
}