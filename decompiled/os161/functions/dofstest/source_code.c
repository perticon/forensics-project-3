dofstest(const char *filesys)
{
	kprintf("*** Starting filesystem test on %s:\n", filesys);

	if (fstest_write(filesys, "", 1, 0)) {
		kprintf("*** Test failed\n");
		return;
	}

	if (fstest_read(filesys, "")) {
		kprintf("*** Test failed\n");
		return;
	}

	if (fstest_remove(filesys, "")) {
		kprintf("*** Test failed\n");
		return;
	}

	kprintf("*** Filesystem test done\n");
}