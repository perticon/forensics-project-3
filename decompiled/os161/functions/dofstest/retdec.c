void dofstest(char * filesys) {
    // 0x8000fa24
    kprintf("*** Starting filesystem test on %s:\n", filesys);
    if (fstest_write(filesys, (char *)&g10, 1, 0) != 0 || fstest_read(filesys, (char *)&g10) != 0) {
        // 0x8000fa64
        kprintf("*** Test failed\n");
        // 0x8000fad4
        return;
    }
    // 0x8000faa0
    if (fstest_remove(filesys, (char *)&g10) == 0) {
        // 0x8000fac8
        kprintf("*** Filesystem test done\n");
    } else {
        // 0x8000fab4
        kprintf("*** Test failed\n");
    }
}