bool threadlist_isempty(threadlist *tl)

{
  return tl->tl_count == 0;
}