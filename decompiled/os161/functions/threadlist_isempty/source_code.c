threadlist_isempty(struct threadlist *tl)
{
	DEBUGASSERT(tl != NULL);

	return (tl->tl_count == 0);
}