bool threadlist_isempty(int32_t * tl) {
    // 0x8001778c
    return *(int32_t *)((int32_t)tl + 24) == 0;
}