establishguardband(void *block, size_t clientsize, size_t blocksize)
{
	vaddr_t lowguard, lowsize, data, enddata, highguard, highsize, i;

	KASSERT(clientsize + GUARD_OVERHEAD <= blocksize);
	KASSERT(clientsize < 65536U);

	lowguard = (vaddr_t)block;
	lowsize = lowguard + 2;
	data = lowsize + 2;
	enddata = data + clientsize;
	highguard = lowguard + blocksize - 4;
	highsize = highguard + 2;

	*(uint16_t *)lowguard = GUARD_HALFWORD;
	*(uint16_t *)lowsize = clientsize;
	for (i=data; i<enddata; i++) {
		*(uint8_t *)i = GUARD_RETBYTE;
	}
	for (i=enddata; i<highguard; i++) {
		*(uint8_t *)i = GUARD_FILLBYTE;
	}
	*(uint16_t *)highguard = GUARD_HALFWORD;
	*(uint16_t *)highsize = clientsize;

	return (void *)data;
}