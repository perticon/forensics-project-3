void cpu_halt(void)

{
  cpu_irqoff();
  do {
    wait(0);
  } while( true );
}