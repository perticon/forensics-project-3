void cpu_halt(void) {
    // 0x8001f350
    cpu_irqoff();
    while (true) {
        // 0x8001f360
        __asm_wait();
    }
}