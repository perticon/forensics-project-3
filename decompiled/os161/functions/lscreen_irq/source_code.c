lscreen_irq(void *vsc)
{
	struct lscreen_softc *sc = vsc;
	uint32_t ch, x;

	spinlock_acquire(&sc->ls_lock);

	x = bus_read_register(sc->ls_busdata, sc->ls_buspos, LSCR_REG_RIRQ);
	if (x & LSCR_IRQ_ACTIVE) {
		ch = bus_read_register(sc->ls_busdata, sc->ls_buspos,
				       LSCR_REG_CHAR);
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSCR_REG_RIRQ, LSCR_IRQ_ENABLE);

		spinlock_release(&sc->ls_lock);
		if (sc->ls_input) {
			sc->ls_input(sc->ls_devdata, ch);
		}
	}
	else {
		spinlock_release(&sc->ls_lock);
	}
}