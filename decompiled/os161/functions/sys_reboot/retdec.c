int32_t sys_reboot(uint32_t code) {
    if (code >= 3) {
        // 0x8000bd68
        return 8;
    }
    // 0x8000bcec
    shutdown();
    switch (code) {
        case 1: {
            // 0x8000bd18
            kprintf("The system is halted.\n");
            mainbus_halt();
            // break -> 0x8000bd60
            break;
        }
        case 2: {
            // 0x8000bd4c
            kprintf("The system is halted.\n");
            mainbus_poweroff();
            // break -> 0x8000bd60
            break;
        }
        case 0: {
            // 0x8000bd34
            kprintf("Rebooting...\n");
            mainbus_reboot();
            // break -> 0x8000bd60
            break;
        }
    }
    // 0x8000bd60
    panic("reboot operation failed\n");
    // 0x8000bd68
    return 8;
}