int sys_reboot(int code)

{
  int in_a1;
  
  if ((uint)code < 3) {
    shutdown(code,in_a1);
    if (code == 1) {
      kprintf("The system is halted.\n");
      mainbus_halt();
    }
    else if (code == 2) {
      kprintf("The system is halted.\n");
      mainbus_poweroff();
    }
    else if (code == 0) {
      kprintf("Rebooting...\n");
      mainbus_reboot();
    }
                    /* WARNING: Subroutine does not return */
    panic("reboot operation failed\n");
  }
  return 8;
}