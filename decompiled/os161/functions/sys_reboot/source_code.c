sys_reboot(int code)
{
	switch (code) {
	    case RB_REBOOT:
	    case RB_HALT:
	    case RB_POWEROFF:
		break;
	    default:
		return EINVAL;
	}

	shutdown();

	switch (code) {
	    case RB_HALT:
		kprintf("The system is halted.\n");
		mainbus_halt();
		break;
	    case RB_REBOOT:
		kprintf("Rebooting...\n");
		mainbus_reboot();
		break;
	    case RB_POWEROFF:
		kprintf("The system is halted.\n");
		mainbus_poweroff();
		break;
	}

	panic("reboot operation failed\n");
	return 0;
}