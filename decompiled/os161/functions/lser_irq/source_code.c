lser_irq(void *vsc)
{
	struct lser_softc *sc = vsc;
	uint32_t x;
	bool clear_to_write = false;
	bool got_a_read = false;
	uint32_t ch = 0;

	spinlock_acquire(&sc->ls_lock);

	x = bus_read_register(sc->ls_busdata, sc->ls_buspos, LSER_REG_WIRQ);
	if (x & LSER_IRQ_ACTIVE) {
		x = LSER_IRQ_ENABLE;
		sc->ls_wbusy = 0;
		clear_to_write = true;
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSER_REG_WIRQ, x);
	}

	x = bus_read_register(sc->ls_busdata, sc->ls_buspos, LSER_REG_RIRQ);
	if (x & LSER_IRQ_ACTIVE) {
		x = LSER_IRQ_ENABLE;
		ch = bus_read_register(sc->ls_busdata, sc->ls_buspos,
				       LSER_REG_CHAR);
		got_a_read = true;
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSER_REG_RIRQ, x);
	}

	spinlock_release(&sc->ls_lock);

	if (clear_to_write && sc->ls_start != NULL) {
		sc->ls_start(sc->ls_devdata);
	}
	if (got_a_read && sc->ls_input != NULL) {
		sc->ls_input(sc->ls_devdata, ch);
	}
}