void lser_irq(void *vsc)

{
  bool bVar1;
  bool bVar2;
  uint32_t uVar3;
  
  spinlock_acquire((spinlock *)vsc);
  uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4);
  bVar1 = (uVar3 & 2) != 0;
  if (bVar1) {
    *(undefined *)((int)vsc + 8) = 0;
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,1);
  }
  uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),8);
  bVar2 = (uVar3 & 2) == 0;
  if (bVar2) {
    uVar3 = 0;
  }
  else {
    uVar3 = lamebus_read_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),0);
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),8,1);
  }
  spinlock_release((spinlock *)vsc);
  if ((bVar1) && (*(code **)((int)vsc + 0x18) != (code *)0x0)) {
    (**(code **)((int)vsc + 0x18))(*(undefined4 *)((int)vsc + 0x14));
  }
  if ((!bVar2) && (*(code **)((int)vsc + 0x1c) != (code *)0x0)) {
    (**(code **)((int)vsc + 0x1c))(*(undefined4 *)((int)vsc + 0x14),uVar3);
  }
  return;
}