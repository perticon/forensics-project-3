void lser_irq(char * vsc) {
    int32_t v1 = (int32_t)vsc;
    spinlock_acquire((int32_t *)vsc);
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x80004c54
    int32_t * v3 = (int32_t *)(v1 + 16); // 0x80004c58
    if ((lamebus_read_register((int32_t *)*v2, *v3, 4) & 2) != 0) {
        // 0x80004c70
        *(char *)(v1 + 8) = 0;
        lamebus_write_register((int32_t *)*v2, *v3, 4, 1);
    }
    // 0x80004c94
    if ((lamebus_read_register((int32_t *)*v2, *v3, 8) & 2) != 0) {
        // 0x80004cb0
        lamebus_read_register((int32_t *)*v2, *v3, 0);
        lamebus_write_register((int32_t *)*v2, *v3, 8, 1);
    }
    // 0x80004ce8
    spinlock_release((int32_t *)vsc);
}