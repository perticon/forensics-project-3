int emufs_getdirentry(vnode *v,uio *uio)

{
  void *pvVar1;
  int iVar2;
  char *pcVar3;
  uint32_t len;
  char *uio_00;
  
  pvVar1 = v->vn_data;
  uio_00 = (char *)uio;
  if (uio->uio_rw != UIO_READ) {
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    uio_00 = "emufs_getdirentry";
    badassert("uio->uio_rw==UIO_READ",s_______dev_lamebus_emu_c_80022750,0x246,"emufs_getdirentry");
    uio = (uio *)pcVar3;
  }
  len = uio->uio_resid;
  if (0x4000 < len) {
    len = 0x4000;
  }
  iVar2 = emu_readdir(*(emu_softc **)((int)pvVar1 + 0x18),*(uint32_t *)((int)pvVar1 + 0x1c),len,
                      (uio *)uio_00);
  return iVar2;
}