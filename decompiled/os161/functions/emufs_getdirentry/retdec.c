int32_t emufs_getdirentry(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2_ = *(int32_t *)((int32_t)v2 + 16); // 0x80002ef0
    int32_t v3 = v1; // 0x80002ef0
    if (*(int32_t *)(v1 + 24) != 0) {
        // 0x80002ef4
        badassert("uio->uio_rw==UIO_READ", "../../dev/lamebus/emu.c", 582, "emufs_getdirentry");
        v2_ = &g41;
        v3 = (int32_t)"emufs_getdirentry";
    }
    int32_t v4 = *(int32_t *)(v2_ + 24); // 0x80002f2c
    int32_t v5 = *(int32_t *)(v2_ + 28); // 0x80002f30
    return emu_readdir((int32_t *)v4, v5, 0x4000, (int32_t *)v3);
}