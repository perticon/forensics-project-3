void proc_destroy(int32_t * proc) {
    int32_t v1 = (int32_t)proc;
    int32_t v2 = v1; // 0x8000cfac
    if (proc == NULL) {
        // 0x8000cfb0
        badassert("proc != NULL", "../../proc/proc.c", 220, "proc_destroy");
        v2 = (int32_t)"proc != NULL";
    }
    int32_t v3 = v2; // 0x8000cfe0
    if (v2 == (int32_t)kproc) {
        // 0x8000cfe4
        badassert("proc != kproc", "../../proc/proc.c", 221, "proc_destroy");
        v3 = (int32_t)"proc != kproc";
    }
    int32_t v4 = *(int32_t *)(v3 + 20); // 0x8000d000
    if (v4 != 0) {
        // 0x8000d010
        vnode_decref((int32_t *)v4);
        *(int32_t *)(v1 + 20) = 0;
    }
    int32_t * v5 = (int32_t *)(v1 + 16); // 0x8000d01c
    int32_t v6 = *v5; // 0x8000d01c
    if (v6 != 0) {
        // 0x8000d02c
        int32_t v7; // 0x8000cf9c
        int32_t v8; // 0x8000cf9c
        if (*(int32_t *)(v8 + 84) == v1) {
            // 0x8000d03c
            as_deactivate();
            v7 = (int32_t)proc_setas(NULL);
        } else {
            // 0x8000d054
            *v5 = 0;
            v7 = v6;
        }
        // 0x8000d058
        as_destroy((int32_t *)v7);
    }
    // 0x8000d060
    if (*(int32_t *)(v1 + 4) != 0) {
        // 0x8000d070
        badassert("proc->p_numthreads == 0", "../../proc/proc.c", 288, "proc_destroy");
    }
    // 0x8000d08c
    spinlock_cleanup((int32_t *)(v1 + 8));
    proc_end_waitpid(proc);
    kfree((char *)proc);
    kfree((char *)proc);
}