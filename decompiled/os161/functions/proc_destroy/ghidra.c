void proc_destroy(proc *proc)

{
  char *pcVar1;
  addrspace *as;
  int unaff_s7;
  
  pcVar1 = (char *)proc;
  if (proc == (proc *)0x0) {
    pcVar1 = "proc != NULL";
    badassert("proc != NULL","../../proc/proc.c",0xdc,"proc_destroy");
  }
  if ((proc *)pcVar1 == kproc) {
    pcVar1 = "proc != kproc";
    badassert("proc != kproc","../../proc/proc.c",0xdd,"proc_destroy");
  }
  if (*(vnode **)((int)pcVar1 + 0x14) != (vnode *)0x0) {
    vnode_decref(*(vnode **)((int)pcVar1 + 0x14));
    proc->p_cwd = (vnode *)0x0;
  }
  as = proc->p_addrspace;
  if (as != (addrspace *)0x0) {
    if (*(proc **)(unaff_s7 + 0x54) == proc) {
      as_deactivate();
      as = proc_setas((addrspace *)0x0);
    }
    else {
      proc->p_addrspace = (addrspace *)0x0;
    }
    as_destroy(as);
  }
  if (proc->p_numthreads != 0) {
    badassert("proc->p_numthreads == 0","../../proc/proc.c",0x120,"proc_destroy");
  }
  spinlock_cleanup(&proc->p_lock);
  proc_end_waitpid(proc);
  kfree(proc->p_name);
  kfree(proc);
  return;
}