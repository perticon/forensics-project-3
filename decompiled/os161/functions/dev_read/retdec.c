int32_t dev_read(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t * v2_ = (int32_t *)*(int32_t *)((int32_t)v2 + 16); // 0x80017de0
    int32_t result = dev_tryseek(v2_, (int64_t)*(int32_t *)(v1 + 8)); // 0x80017de0
    if (result != 0) {
        // 0x80017e34
        return result;
    }
    // 0x80017dec
    if (*(int32_t *)(v1 + 24) != 0) {
        // 0x80017dfc
        badassert("uio->uio_rw == UIO_READ", "../../vfs/device.c", 119, "dev_read");
    }
    // 0x80017e34
    return *(int32_t *)(*v2_ + 4);
}