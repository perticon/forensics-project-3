int dev_read(vnode *v,uio *uio)

{
  int iVar1;
  char *pcVar2;
  undefined4 unaff_s0;
  device *d;
  undefined4 in_stack_fffffff0;
  
  d = (device *)v->vn_data;
  iVar1 = dev_tryseek(d,CONCAT44(in_stack_fffffff0,unaff_s0));
  if (iVar1 == 0) {
    pcVar2 = (char *)d;
    if (uio->uio_rw != UIO_READ) {
      pcVar2 = "uio->uio_rw == UIO_READ";
      badassert("uio->uio_rw == UIO_READ","../../vfs/device.c",0x77,"dev_read");
    }
    iVar1 = (*d->d_ops->devop_io)((device *)pcVar2,uio);
  }
  return iVar1;
}