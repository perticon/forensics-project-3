int32_t cmd_unmount(int32_t nargs, char ** args) {
    if (nargs != 2) {
        // 0x8000c618
        kprintf("Usage: unmount device:\n");
        // 0x8000c664
        return 8;
    }
    int32_t v1 = *(int32_t *)((int32_t)args + 4); // 0x8000c62c
    char * v2 = (char *)v1; // 0x8000c634
    int32_t v3 = strlen(v2); // 0x8000c634
    int32_t v4 = v1 - 1;
    if (*(char *)(v4 + v3) == 58) {
        // 0x8000c64c
        *(char *)(strlen(v2) + v4) = 0;
    }
    // 0x8000c664
    return vfs_unmount(v2);
}