cmd_unmount(int nargs, char **args)
{
	char *device;

	if (nargs != 2)
	{
		kprintf("Usage: unmount device:\n");
		return EINVAL;
	}

	device = args[1];

	/* Allow (but do not require) colon after device name */
	if (device[strlen(device) - 1] == ':')
	{
		device[strlen(device) - 1] = 0;
	}

	return vfs_unmount(device);
}