int blocktype(size_t clientsz)
{
	unsigned i;
	for (i=0; i<NSIZES; i++) {
		if (clientsz <= sizes[i]) {
			return i;
		}
	}

	panic("Subpage allocator cannot handle allocation of size %zu\n",
	      clientsz);

	// keep compiler happy
	return 0;
}