int bitmap_isset(bitmap *b,uint index)

{
  return 1 << (index & 7) & (uint)b->v[index >> 3];
}