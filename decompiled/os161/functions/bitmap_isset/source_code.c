bitmap_isset(struct bitmap *b, unsigned index)
{
        unsigned ix;
        WORD_TYPE mask;

        bitmap_translate(index, &ix, &mask);
        return (b->v[ix] & mask);
}