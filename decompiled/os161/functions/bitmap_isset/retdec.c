int32_t bitmap_isset(int32_t * b, uint32_t index) {
    unsigned char v1 = *(char *)(*(int32_t *)((int32_t)b + 4) + index / 8); // 0x8000af9c
    return 1 << index % 8 & (int32_t)v1;
}