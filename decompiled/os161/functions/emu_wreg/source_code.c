emu_wreg(struct emu_softc *sc, uint32_t reg, uint32_t val)
{
	bus_write_register(sc->e_busdata, sc->e_buspos, reg, val);
}