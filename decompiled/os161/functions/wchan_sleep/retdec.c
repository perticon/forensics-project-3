void wchan_sleep(int32_t * wc, int32_t * lk) {
    int32_t v1 = (int32_t)lk; // 0x80016e34
    int32_t v2; // 0x80016e18
    if (*(char *)(v2 + 88) != 0) {
        // 0x80016e38
        badassert("!curthread->t_in_interrupt", "../../thread/thread.c", 1014, "wchan_sleep");
        v1 = (int32_t)"../../thread/thread.c";
    }
    int32_t * v3 = (int32_t *)v1; // 0x80016e60
    if (!spinlock_do_i_hold(v3)) {
        // 0x80016e6c
        badassert("spinlock_do_i_hold(lk)", "../../thread/thread.c", 1017, "wchan_sleep");
    }
    int32_t v4 = (int32_t)wc; // 0x80016e9c
    if (*(int32_t *)(*(int32_t *)(v2 + 80) + 48) != 1) {
        // 0x80016ea0
        badassert("curcpu->c_spinlocks == 1", "../../thread/thread.c", 1020, "wchan_sleep");
        v4 = (int32_t)"../../thread/thread.c";
    }
    // 0x80016ec0
    thread_switch(2, (int32_t *)v4, v3);
    spinlock_acquire(v3);
}