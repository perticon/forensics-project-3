void wchan_sleep(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  int unaff_s7;
  
  if (*(char *)(unaff_s7 + 0x58) != '\0') {
    pcVar2 = "../../thread/thread.c";
    badassert("!curthread->t_in_interrupt","../../thread/thread.c",0x3f6,"wchan_sleep");
    lk = (spinlock *)pcVar2;
  }
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x3f9,"wchan_sleep");
  }
  if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) != 1) {
    pcVar2 = "../../thread/thread.c";
    badassert("curcpu->c_spinlocks == 1","../../thread/thread.c",0x3fc,"wchan_sleep");
    wc = (wchan *)pcVar2;
  }
  thread_switch(S_SLEEP,wc,lk);
  spinlock_acquire(lk);
  return;
}