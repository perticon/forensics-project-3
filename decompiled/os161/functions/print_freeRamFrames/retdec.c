void print_freeRamFrames(void) {
    // 0x8001b42c
    kprintf("Frame Status\n");
    if ((uint32_t)g20 <= 0) {
        // 0x8001b49c
        return;
    }
    int32_t v1 = 0;
    kprintf("  %d    %d\n", v1, TestBit(freeRamFrames, v1));
    int32_t v2 = v1 + 1; // 0x8001b484
    while (v2 < g20) {
        // 0x8001b468
        v1 = v2;
        kprintf("  %d    %d\n", v1, TestBit(freeRamFrames, v1));
        v2 = v1 + 1;
    }
}