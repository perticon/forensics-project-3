void print_freeRamFrames(void)
{
    int i;
    kprintf("Frame Status\n");

    for (i = 0; i < nRamFrames; i++)
    {
        kprintf("  %d    %d\n", i, TestBit(freeRamFrames, i));
    }
}