void print_freeRamFrames(void)

{
  int iVar1;
  int k;
  
  kprintf("Frame Status\n");
  for (k = 0; k < nRamFrames; k = k + 1) {
    iVar1 = TestBit(freeRamFrames,k);
    kprintf("  %d    %d\n",k,iVar1);
  }
  return;
}