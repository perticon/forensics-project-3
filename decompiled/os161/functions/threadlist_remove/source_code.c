threadlist_remove(struct threadlist *tl, struct thread *t)
{
	threadlist_removenode(&t->t_listnode);
	DEBUGASSERT(tl->tl_count > 0);
	tl->tl_count--;
}