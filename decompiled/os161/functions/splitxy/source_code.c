splitxy(uint32_t xy, unsigned *x, unsigned *y)
{
	*x = xy >> 16;
	*y = xy & 0xffff;
}