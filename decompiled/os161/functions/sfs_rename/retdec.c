int32_t sfs_rename(int32_t * d1, char * n1, int32_t * d2, char * n2) {
    int32_t v1 = *(int32_t *)((int32_t)d1 + 16); // 0x80009b00
    vfs_biglock_acquire();
    int32_t v2 = 1; // 0x80009b20
    if (d1 != d2) {
        // 0x80009b24
        badassert("d1==d2", "../../fs/sfs/sfs_vnops.c", 443, "sfs_rename");
        v2 = &g41;
    }
    int32_t v3 = v1; // 0x80009b50
    if (*(int32_t *)(v1 + 536) != v2) {
        // 0x80009b54
        badassert("sv->sv_ino == SFS_ROOTDIR_INO", "../../fs/sfs/sfs_vnops.c", 444, "sfs_rename");
        v3 = (int32_t)"sv->sv_ino == SFS_ROOTDIR_INO";
    }
    // 0x80009b74
    int32_t * g1; // bp-48, 0x80009ad4
    int32_t slot1; // bp-44, 0x80009ad4
    int32_t result = sfs_lookonce((int32_t *)v3, n1, &g1, &slot1); // 0x80009b80
    if (result != 0) {
        // 0x80009b8c
        vfs_biglock_release();
        // 0x80009d0c
        return result;
    }
    int32_t v4 = (int32_t)g1; // 0x80009b9c
    int32_t v5 = v4; // 0x80009bb0
    int32_t v6 = v1; // 0x80009bb0
    if (*(int16_t *)(v4 + 28) != 1) {
        // 0x80009bb4
        badassert("g1->sv_i.sfi_type == SFS_TYPE_FILE", "../../fs/sfs/sfs_vnops.c", 454, "sfs_rename");
        v5 = &g41;
        v6 = (int32_t)"g1->sv_i.sfi_type == SFS_TYPE_FILE";
    }
    int32_t v7 = *(int32_t *)(v5 + 536); // 0x80009bd8
    int32_t slot2; // bp-40, 0x80009ad4
    int32_t v8 = sfs_dir_link((int32_t *)v6, n2, v7, &slot2); // 0x80009be0
    int32_t result2 = v8; // 0x80009be8
    if (v8 == 0) {
        int32_t v9 = (int32_t)g1; // 0x80009bec
        int16_t * v10 = (int16_t *)(v9 + 30); // 0x80009bf4
        *v10 = *v10 + 1;
        *(char *)(v9 + 540) = 1;
        int32_t * v11 = (int32_t *)v1; // 0x80009c14
        result2 = sfs_dir_unlink(v11, slot1);
        if (result2 == 0) {
            int32_t v12 = (int32_t)g1; // 0x80009c20
            int16_t v13 = *(int16_t *)(v12 + 30); // 0x80009c28
            int16_t v14 = v13 - 1; // 0x80009c34
            int32_t v15 = v12; // 0x80009c34
            if (v13 == 0) {
                // 0x80009c38
                badassert("g1->sv_i.sfi_linkcount>0", "../../fs/sfs/sfs_vnops.c", 483, "sfs_rename");
                v14 = &g41;
                v15 = (int32_t)"g1->sv_i.sfi_linkcount>0";
            }
            // 0x80009c5c
            *(int16_t *)(v15 + 30) = v14;
            *(char *)(v15 + 540) = 1;
            vnode_decref((int32_t *)v15);
            vfs_biglock_release();
            // 0x80009d0c
            return 0;
        }
        int32_t v16 = sfs_dir_unlink(v11, slot2); // 0x80009c84
        if (v16 != 0) {
            char * v17 = (char *)(*(int32_t *)*(int32_t *)(v1 + 12) + 16); // 0x80009cac
            kprintf("sfs: %s: rename: %s\n", v17, strerror(result2));
            kprintf("sfs: %s: rename: while cleaning up: %s\n", v17, strerror(v16));
            panic("sfs: %s: rename: Cannot recover\n", v17);
        }
        int16_t * v18 = (int16_t *)((int32_t)g1 + 30); // 0x80009ce4
        *v18 = *v18 - 1;
    }
    // 0x80009cf4
    vnode_decref(g1);
    vfs_biglock_release();
    // 0x80009d0c
    return result2;
}