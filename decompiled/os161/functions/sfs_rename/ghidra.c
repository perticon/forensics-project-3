int sfs_rename(vnode *d1,char *n1,vnode *d2,char *n2)

{
  uint16_t uVar1;
  uint32_t uVar2;
  int errcode;
  sfs_vnode *psVar3;
  uint16_t uVar4;
  int errcode_00;
  char *pcVar5;
  sfs_vnode *sv;
  void *pvVar6;
  int iVar7;
  sfs_vnode *g1;
  int slot1;
  int slot2;
  
  sv = (sfs_vnode *)d1->vn_data;
  pvVar6 = ((sv->sv_absvn).vn_fs)->fs_data;
  vfs_biglock_acquire();
  uVar2 = 1;
  if (d1 != d2) {
    badassert("d1==d2","../../fs/sfs/sfs_vnops.c",0x1bb,"sfs_rename");
  }
  pcVar5 = (char *)sv;
  if (sv->sv_ino != uVar2) {
    pcVar5 = "sv->sv_ino == SFS_ROOTDIR_INO";
    badassert("sv->sv_ino == SFS_ROOTDIR_INO","../../fs/sfs/sfs_vnops.c",0x1bc,"sfs_rename");
  }
  errcode = sfs_lookonce((sfs_vnode *)pcVar5,n1,&g1,&slot1);
  if (errcode == 0) {
    psVar3 = g1;
    pcVar5 = (char *)sv;
    if ((g1->sv_i).sfi_type != 1) {
      pcVar5 = "g1->sv_i.sfi_type == SFS_TYPE_FILE";
      badassert("g1->sv_i.sfi_type == SFS_TYPE_FILE","../../fs/sfs/sfs_vnops.c",0x1c6,"sfs_rename");
    }
    errcode = sfs_dir_link((sfs_vnode *)pcVar5,n2,psVar3->sv_ino,&slot2);
    if (errcode == 0) {
      (g1->sv_i).sfi_linkcount = (g1->sv_i).sfi_linkcount + 1;
      g1->sv_dirty = true;
      errcode = sfs_dir_unlink(sv,slot1);
      if (errcode == 0) {
        uVar1 = (g1->sv_i).sfi_linkcount;
        uVar4 = uVar1 - 1;
        if (uVar1 == 0) {
          pcVar5 = "g1->sv_i.sfi_linkcount>0";
          uVar4 = uVar1;
          badassert("g1->sv_i.sfi_linkcount>0","../../fs/sfs/sfs_vnops.c",0x1e3,"sfs_rename");
          g1 = (sfs_vnode *)pcVar5;
        }
        (g1->sv_i).sfi_linkcount = uVar4;
        g1->sv_dirty = true;
        vnode_decref(&g1->sv_absvn);
        vfs_biglock_release();
        return 0;
      }
      errcode_00 = sfs_dir_unlink(sv,slot2);
      if (errcode_00 != 0) {
        iVar7 = (int)pvVar6 + 0x10;
        pcVar5 = strerror(errcode);
        kprintf("sfs: %s: rename: %s\n",iVar7,pcVar5);
        pcVar5 = strerror(errcode_00);
        kprintf("sfs: %s: rename: while cleaning up: %s\n",iVar7,pcVar5);
                    /* WARNING: Subroutine does not return */
        panic("sfs: %s: rename: Cannot recover\n",iVar7);
      }
      (g1->sv_i).sfi_linkcount = (g1->sv_i).sfi_linkcount - 1;
    }
    vnode_decref(&g1->sv_absvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return errcode;
}