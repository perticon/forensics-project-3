void sfs_bfree(sfs_fs *sfs,daddr_t diskblock)

{
  bitmap_unmark(sfs->sfs_freemap,diskblock);
  sfs->sfs_freemapdirty = true;
  return;
}