void sfs_bfree(int32_t * sfs, int32_t diskblock) {
    int32_t v1 = (int32_t)sfs;
    bitmap_unmark((int32_t *)*(int32_t *)(v1 + 532), diskblock);
    *(char *)(v1 + 536) = 1;
}