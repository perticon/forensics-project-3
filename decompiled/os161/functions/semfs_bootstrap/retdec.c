void semfs_bootstrap(void) {
    int32_t * v1 = semfs_create(); // 0x80005880
    int32_t v2 = (int32_t)v1; // 0x80005888
    if (v1 == NULL) {
        // 0x8000588c
        panic("Out of memory creating semfs\n");
        v2 = &g41;
    }
    int32_t v3 = vfs_addfs("sem", (int32_t *)v2); // 0x800058a0
    if (v3 != 0) {
        // 0x800058ac
        panic("Attaching semfs: %s\n", strerror(v3));
    }
}