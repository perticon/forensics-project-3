void semfs_bootstrap(void)

{
  semfs *fs;
  int errcode;
  char *pcVar1;
  
  fs = semfs_create();
  if (fs == (semfs *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Out of memory creating semfs\n");
  }
  errcode = vfs_addfs("sem",&fs->semfs_absfs);
  if (errcode != 0) {
    pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
    panic("Attaching semfs: %s\n",pcVar1);
  }
  return;
}