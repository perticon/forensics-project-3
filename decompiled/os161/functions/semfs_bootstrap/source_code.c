semfs_bootstrap(void)
{
	struct semfs *semfs;
	int result;

	semfs = semfs_create();
	if (semfs == NULL) {
		panic("Out of memory creating semfs\n");
	}
	result = vfs_addfs("sem", &semfs->semfs_absfs);
	if (result) {
		panic("Attaching semfs: %s\n", strerror(result));
	}
}