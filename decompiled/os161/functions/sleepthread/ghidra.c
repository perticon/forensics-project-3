void sleepthread(void *junk1,ulong junk2)

{
  bool bVar1;
  uint uVar2;
  lock **pplVar3;
  cv **ppcVar4;
  uint uVar5;
  
  for (uVar5 = 0; uVar2 = 0, uVar5 < 0x28; uVar5 = uVar5 + 1) {
    bVar1 = true;
    while (bVar1) {
      pplVar3 = testlocks + uVar2;
      ppcVar4 = testcvs + uVar2;
      lock_acquire(*pplVar3);
      uVar2 = uVar2 + 1;
      V(gatesem);
      cv_wait(*ppcVar4,*pplVar3);
      lock_release(*pplVar3);
      bVar1 = uVar2 < 0xfa;
    }
    kprintf("sleepthread: %u\n",uVar5);
  }
  V(exitsem);
  return;
}