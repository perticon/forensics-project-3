void sleepthread(char * junk1, int32_t junk2) {
    for (int32_t i = 0; i < 40; i++) {
        int32_t v1 = 0; // 0x80012d2c
        int32_t v2 = 0; // 0x80012d90
        int32_t * v3 = (int32_t *)(v1 + (int32_t)&testlocks); // 0x80012d7c
        lock_acquire((int32_t *)*v3);
        v2++;
        V(gatesem);
        int32_t v4 = *(int32_t *)(v1 + (int32_t)&testcvs); // 0x80012d94
        cv_wait((int32_t *)v4, (int32_t *)*v3);
        lock_release((int32_t *)*v3);
        v1 = 4 * v2;
        while (v2 < 250) {
            // 0x80012d78
            v3 = (int32_t *)(v1 + (int32_t)&testlocks);
            lock_acquire((int32_t *)*v3);
            v2++;
            V(gatesem);
            v4 = *(int32_t *)(v1 + (int32_t)&testcvs);
            cv_wait((int32_t *)v4, (int32_t *)*v3);
            lock_release((int32_t *)*v3);
            v1 = 4 * v2;
        }
        // 0x80012dc4
        kprintf("sleepthread: %u\n", i);
    }
    // 0x80012de0
    V(exitsem);
}