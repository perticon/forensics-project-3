int emufs_reclaim(vnode *v)

{
  int iVar1;
  uint uVar2;
  array *a;
  uint uVar3;
  uint index;
  vnode *vn;
  void *pvVar4;
  spinlock *splk;
  
  vn = (vnode *)v->vn_data;
  pvVar4 = v->vn_fs->fs_data;
  vfs_biglock_acquire();
  splk = &vn->vn_countlock;
  lock_acquire(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
  spinlock_acquire(splk);
  iVar1 = vn->vn_refcount;
  if (iVar1 < 2) {
    if (iVar1 != 1) {
      badassert("ev->ev_v.vn_refcount == 1",s_______dev_lamebus_emu_c_80022750,0x1ee,"emufs_reclaim"
               );
    }
    spinlock_release(splk);
    iVar1 = emu_close((emu_softc *)vn[1].vn_refcount,vn[1].vn_countlock.splk_lock);
    if (iVar1 == 0) {
      a = *(array **)((int)pvVar4 + 0x10);
      uVar2 = a->num;
      for (uVar3 = 0; (index = uVar2, uVar3 < uVar2 && (index = uVar3, v != (vnode *)a->v[uVar3]));
          uVar3 = uVar3 + 1) {
      }
      if (index == uVar2) {
                    /* WARNING: Subroutine does not return */
        panic("emu%d: reclaim vnode %u not in vnode pool\n",
              *(undefined4 *)(*(int *)((int)pvVar4 + 8) + 8),vn[1].vn_countlock.splk_lock);
      }
      array_remove(a,index);
      vnode_cleanup(vn);
      lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
      vfs_biglock_release();
      kfree(vn);
      iVar1 = 0;
    }
    else {
      lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
      vfs_biglock_release();
    }
  }
  else {
    vn->vn_refcount = iVar1 + -1;
    spinlock_release(splk);
    lock_release(*(lock **)(*(int *)((int)pvVar4 + 8) + 0xc));
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}