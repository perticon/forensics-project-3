int32_t emufs_reclaim(int32_t * v2) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(v1 + 16); // 0x80003054
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80003060
    vfs_biglock_acquire();
    int32_t * v4 = (int32_t *)(v3 + 8); // 0x8000306c
    lock_acquire((int32_t *)*(int32_t *)(*v4 + 12));
    int32_t * v5 = (int32_t *)(v2_ + 4); // 0x80003084
    spinlock_acquire(v5);
    int32_t * v6 = (int32_t *)v2_; // 0x80003088
    uint32_t v7 = *v6; // 0x80003088
    if (v7 >= 2) {
        // 0x8000309c
        *v6 = v7 - 1;
        spinlock_release(v5);
        lock_release((int32_t *)*(int32_t *)(*v4 + 12));
        vfs_biglock_release();
        // 0x800031d4
        return 27;
    }
    if (v7 != 1) {
        // 0x800030d8
        badassert("ev->ev_v.vn_refcount == 1", "../../dev/lamebus/emu.c", 494, "emufs_reclaim");
    }
    // 0x800030f4
    spinlock_release(v5);
    int32_t * v8 = (int32_t *)(v2_ + 28); // 0x80003100
    int32_t result = emu_close((int32_t *)*(int32_t *)(v2_ + 24), *v8); // 0x80003108
    if (result != 0) {
        // 0x80003114
        lock_release((int32_t *)*(int32_t *)(*v4 + 12));
        vfs_biglock_release();
        // 0x800031d4
        return result;
    }
    int32_t v9 = *(int32_t *)(v3 + 16); // 0x80003138
    uint32_t v10 = *(int32_t *)(v9 + 4); // 0x80003140
    int32_t v11; // 0x8000303c
    int32_t v12; // 0x8000303c
    if (v10 == 0) {
        goto lab_0x80003184;
    } else {
        int32_t v13 = 0;
        while (*(int32_t *)(4 * v13 + *(int32_t *)v9) != v1) {
            int32_t v14 = v13 + 1; // 0x80003168
            if (v14 >= v10) {
                goto lab_0x80003184;
            }
            v13 = v14;
        }
        // 0x8000317c
        v11 = v9;
        v12 = v13;
        if (v13 == v10) {
            goto lab_0x80003184;
        } else {
            goto lab_0x8000319c;
        }
    }
  lab_0x80003184:;
    int32_t v15 = *(int32_t *)(*v4 + 8); // 0x8000318c
    panic("emu%d: reclaim vnode %u not in vnode pool\n", v15, *v8);
    v11 = (int32_t)"emu%d: reclaim vnode %u not in vnode pool\n";
    v12 = v15;
    goto lab_0x8000319c;
  lab_0x8000319c:
    // 0x8000319c
    array_remove((int32_t *)v11, v12);
    vnode_cleanup(v6);
    lock_release((int32_t *)*(int32_t *)(*v4 + 12));
    vfs_biglock_release();
    kfree((char *)v2_);
    // 0x800031d4
    return 0;
}