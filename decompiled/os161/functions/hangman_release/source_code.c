hangman_release(struct hangman_actor *a,
		struct hangman_lockable *l)
{
	if (l == &hangman_lock.splk_hangman) {
		/* don't recurse */
		return;
	}

	spinlock_acquire(&hangman_lock);

	if (a->a_waiting != NULL) {
		spinlock_release(&hangman_lock);
		panic("hangman_release: waiting for something?\n");
	}
	if (l->l_holding != a) {
		spinlock_release(&hangman_lock);
		panic("hangman_release: not the holder\n");
	}

	l->l_holding = NULL;

	spinlock_release(&hangman_lock);
}