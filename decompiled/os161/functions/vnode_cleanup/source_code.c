vnode_cleanup(struct vnode *vn)
{
	KASSERT(vn->vn_refcount == 1);

	spinlock_cleanup(&vn->vn_countlock);

	vn->vn_ops = NULL;
	vn->vn_refcount = 0;
	vn->vn_fs = NULL;
	vn->vn_data = NULL;
}