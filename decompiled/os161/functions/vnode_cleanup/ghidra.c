void vnode_cleanup(vnode *vn)

{
  char *pcVar1;
  
  pcVar1 = (char *)vn;
  if (vn->vn_refcount != 1) {
    pcVar1 = "vn->vn_refcount == 1";
    badassert("vn->vn_refcount == 1","../../vfs/vnode.c",0x40,"vnode_cleanup");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 4));
  vn->vn_ops = (vnode_ops *)0x0;
  vn->vn_refcount = 0;
  vn->vn_fs = (fs *)0x0;
  vn->vn_data = (void *)0x0;
  return;
}