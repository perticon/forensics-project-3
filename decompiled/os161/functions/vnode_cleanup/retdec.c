void vnode_cleanup(int32_t * vn) {
    if (vn != (int32_t *)1) {
        // 0x8001a71c
        badassert("vn->vn_refcount == 1", "../../vfs/vnode.c", 64, "vnode_cleanup");
    }
    int32_t v1 = (int32_t)vn;
    spinlock_cleanup((int32_t *)"vn_refcount == 1");
    *(int32_t *)(v1 + 20) = 0;
    *vn = 0;
    *(int32_t *)(v1 + 12) = 0;
    *(int32_t *)(v1 + 16) = 0;
}