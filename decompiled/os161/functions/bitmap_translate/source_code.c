bitmap_translate(unsigned bitno, unsigned *ix, WORD_TYPE *mask)
{
        unsigned offset;
        *ix = bitno / BITS_PER_WORD;
        offset = bitno % BITS_PER_WORD;
        *mask = ((WORD_TYPE)1) << offset;
}