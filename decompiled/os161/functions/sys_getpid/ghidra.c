pid_t sys_getpid(void)

{
  int iVar1;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    badassert("curproc != NULL",s_______syscall_proc_syscalls_c_80024d30,100,"sys_getpid");
  }
  return *(pid_t *)(iVar1 + 0x1c);
}