vfs_setbootfs(const char *fsname)
{
	char tmp[NAME_MAX+1];
	char *s;
	int result;
	struct vnode *newguy;

	vfs_biglock_acquire();

	snprintf(tmp, sizeof(tmp)-1, "%s", fsname);
	s = strchr(tmp, ':');
	if (s) {
		/* If there's a colon, it must be at the end */
		if (strlen(s)>0) {
			vfs_biglock_release();
			return EINVAL;
		}
	}
	else {
		strcat(tmp, ":");
	}

	result = vfs_chdir(tmp);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	result = vfs_getcurdir(&newguy);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	change_bootfs(newguy);

	vfs_biglock_release();
	return 0;
}