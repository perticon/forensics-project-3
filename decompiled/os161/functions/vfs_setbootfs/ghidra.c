int vfs_setbootfs(char *fsname)

{
  char *str;
  size_t sVar1;
  int iVar2;
  char tmp [256];
  vnode *newguy;
  
  vfs_biglock_acquire();
  snprintf(tmp,0xff,"%s",fsname);
  str = strchr(tmp,0x3a);
  if (str == (char *)0x0) {
    strcat(tmp,":");
  }
  else {
    sVar1 = strlen(str);
    if (sVar1 != 0) {
      vfs_biglock_release();
      return 8;
    }
  }
  iVar2 = vfs_chdir(tmp);
  if (iVar2 == 0) {
    iVar2 = vfs_getcurdir(&newguy);
    if (iVar2 == 0) {
      change_bootfs(newguy);
      vfs_biglock_release();
      iVar2 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar2;
}