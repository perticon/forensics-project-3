int32_t vfs_setbootfs(char * fsname) {
    char tmp2[256]; // bp-272, 0x80019d70
    // 0x80019d70
    vfs_biglock_acquire();
    snprintf(tmp2, 255, "%s", fsname);
    char * v1 = strchr(tmp2, 58); // 0x80019da4
    if (v1 == NULL) {
        // 0x80019dd0
        strcat(tmp2, ":");
    } else {
        // 0x80019db0
        if (strlen(v1) != 0) {
            // 0x80019dc0
            vfs_biglock_release();
            // 0x80019e34
            return 8;
        }
    }
    int32_t result = vfs_chdir(tmp2); // 0x80019de0
    if (result != 0) {
        // 0x80019dec
        vfs_biglock_release();
        // 0x80019e34
        return result;
    }
    // 0x80019dfc
    int32_t * newguy; // bp-16, 0x80019d70
    int32_t v2 = vfs_getcurdir(&newguy); // 0x80019e00
    int32_t result2; // 0x80019d70
    if (v2 == 0) {
        // 0x80019e1c
        change_bootfs(newguy);
        vfs_biglock_release();
        result2 = 0;
    } else {
        // 0x80019e0c
        vfs_biglock_release();
        result2 = v2;
    }
    // 0x80019e34
    return result2;
}