autoconf_ltimer(struct ltimer_softc *bus, int busunit)
{
	(void)bus; (void)busunit;
	{
		if (nextunit_beep <= 0) {
			tryattach_beep_to_ltimer(0, bus, busunit);
		}
	}
	{
		if (nextunit_rtclock <= 0) {
			tryattach_rtclock_to_ltimer(0, bus, busunit);
		}
	}
}