void autoconf_ltimer(ltimer_softc *bus,int busunit)

{
  if (nextunit_beep < 1) {
    tryattach_beep_to_ltimer(0,bus,busunit);
  }
  if (nextunit_rtclock < 1) {
    tryattach_rtclock_to_ltimer(0,bus,busunit);
  }
  return;
}