void autoconf_ltimer(int32_t * bus, int32_t busunit) {
    // 0x80001460
    if ((uint32_t)nextunit_beep <= 0) {
        // 0x80001488
        tryattach_beep_to_ltimer(0, bus, busunit);
    }
    // 0x80001498
    if (nextunit_rtclock <= 0) {
        // 0x800014ac
        tryattach_rtclock_to_ltimer(0, bus, busunit);
    }
}