void hardclock(void)

{
  int unaff_s7;
  
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x2c) = *(int *)(*(int *)(unaff_s7 + 0x50) + 0x2c) + 1;
  if ((*(uint *)(*(int *)(unaff_s7 + 0x50) + 0x2c) & 0xf) == 0) {
    thread_consider_migration();
  }
  if ((*(uint *)(*(int *)(unaff_s7 + 0x50) + 0x2c) & 3) == 0) {
    schedule();
  }
  thread_yield();
  return;
}