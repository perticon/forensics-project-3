void hardclock(void) {
    // 0x80014f58
    int32_t v1; // 0x80014f58
    int32_t * v2 = (int32_t *)(v1 + 80); // 0x80014f60
    int32_t * v3 = (int32_t *)(*v2 + 44); // 0x80014f68
    *v3 = *v3 + 1;
    uint32_t v4 = *(int32_t *)(*v2 + 44); // 0x80014f80
    int32_t v5 = v4; // 0x80014f90
    if (v4 % 16 == 0) {
        // 0x80014f94
        thread_consider_migration();
        v5 = *(int32_t *)(*v2 + 44);
    }
    // 0x80014f9c
    if (v5 % 4 == 0) {
        // 0x80014fb8
        schedule();
    }
    // 0x80014fc0
    thread_yield();
}