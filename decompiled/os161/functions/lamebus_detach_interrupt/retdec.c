void lamebus_detach_interrupt(int32_t * sc, uint32_t slot) {
    int32_t v1 = (int32_t)sc; // 0x8000400c
    if (slot >= 32) {
        // 0x80004010
        badassert("slot>=0 && slot < LB_NSLOTS", "../../dev/lamebus/lamebus.c", 382, "lamebus_detach_interrupt");
        v1 = (int32_t)"slot>=0 && slot < LB_NSLOTS";
    }
    int32_t * v2 = (int32_t *)v1; // 0x80004034
    spinlock_acquire(v2);
    int32_t v3 = slot + 34; // 0x80004048
    if ((1 << slot & 0x73202626) == 0) {
        // 0x8000404c
        panic("lamebus_detach_interrupt: slot %d not marked in use\n", slot);
        v3 = &g41;
    }
    int32_t v4 = v1 + 4;
    int32_t v5 = slot + 2; // 0x80004070
    if (*(int32_t *)(4 * v3 + v4) == 0) {
        // 0x80004074
        badassert("sc->ls_irqfuncs[slot]!=NULL", "../../dev/lamebus/lamebus.c", 391, "lamebus_detach_interrupt");
        v5 = &g41;
    }
    // 0x80004094
    *(int32_t *)(4 * v5 + v4) = 0;
    *(int32_t *)(4 * slot + 140 + v1) = 0;
    spinlock_release(v2);
}