void lamebus_detach_interrupt(lamebus_softc *sc,int slot)

{
  int iVar1;
  char *pcVar2;
  
  if (0x1f < (uint)slot) {
    pcVar2 = "slot>=0 && slot < LB_NSLOTS";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x17e,
              "lamebus_detach_interrupt");
    sc = (lamebus_softc *)pcVar2;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & 1 << (slot & 0x1fU)) == 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_detach_interrupt: slot %d not marked in use\n",slot);
  }
  iVar1 = slot + 2;
  if (sc->ls_irqfuncs[slot] == (lb_irqfunc *)0x0) {
    badassert("sc->ls_irqfuncs[slot]!=NULL","../../dev/lamebus/lamebus.c",0x187,
              "lamebus_detach_interrupt");
  }
  sc->ls_devdata[iVar1 + -2] = (void *)0x0;
  sc->ls_irqfuncs[slot] = (lb_irqfunc *)0x0;
  spinlock_release(&sc->ls_lock);
  return;
}