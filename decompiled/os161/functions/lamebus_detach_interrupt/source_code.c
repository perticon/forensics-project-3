lamebus_detach_interrupt(struct lamebus_softc *sc, int slot)
{
	uint32_t mask = ((uint32_t)1) << slot;
	KASSERT(slot>=0 && slot < LB_NSLOTS);

	spinlock_acquire(&sc->ls_lock);

	if ((sc->ls_slotsinuse & mask)==0) {
		panic("lamebus_detach_interrupt: slot %d not marked in use\n",
		      slot);
	}

	KASSERT(sc->ls_irqfuncs[slot]!=NULL);

	sc->ls_devdata[slot] = NULL;
	sc->ls_irqfuncs[slot] = NULL;

	spinlock_release(&sc->ls_lock);
}