bool filedes_is_seekable(openfile *of)

{
  undefined uVar1;
  openfile *poVar2;
  
  poVar2 = of;
  if (of == (openfile *)0x0) {
    poVar2 = (openfile *)&DAT_80024b6c;
    badassert((char *)&DAT_80024b6c,"../../syscall/file_syscalls.c",0xf9,"filedes_is_seekable");
  }
  if (poVar2->vn == (vnode *)0x0) {
    uVar1 = 0;
  }
  else {
    vnode_check(poVar2->vn,"isseekable");
    uVar1 = (*of->vn->vn_ops->vop_isseekable)(of->vn);
  }
  return (bool)uVar1;
}