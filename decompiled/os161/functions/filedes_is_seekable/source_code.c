bool filedes_is_seekable(struct openfile *of) {
	KASSERT(of);
	// if (file_des->ftype != FILEDES_TYPE_REG) return false;
	if (!of->vn) return false;
	return VOP_ISSEEKABLE(of->vn);
}