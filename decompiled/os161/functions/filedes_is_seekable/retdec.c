bool filedes_is_seekable(int32_t * of) {
    if (of == NULL) {
        // 0x8000d474
        badassert("of", "../../syscall/file_syscalls.c", 249, "filedes_is_seekable");
    }
    // 0x8000d494
    vnode_check((int32_t *)0x666f, "isseekable");
    return *(int32_t *)(*(int32_t *)0x6683 + 40) % 2 != 0;
}