void spinlock_cleanup(spinlock *splk)

{
  spinlock_data_t sVar1;
  char *pcVar2;
  
  if (splk->splk_holder != (cpu *)0x0) {
    pcVar2 = "splk->splk_holder == NULL";
    badassert("splk->splk_holder == NULL","../../thread/spinlock.c",0x40,"spinlock_cleanup");
    splk = (spinlock *)pcVar2;
  }
  sVar1 = spinlock_data_get(&splk->splk_lock);
  if (sVar1 != 0) {
    badassert("spinlock_data_get(&splk->splk_lock) == 0","../../thread/spinlock.c",0x41,
              "spinlock_cleanup");
  }
  return;
}