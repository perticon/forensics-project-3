spinlock_cleanup(struct spinlock *splk)
{
	KASSERT(splk->splk_holder == NULL);
	KASSERT(spinlock_data_get(&splk->splk_lock) == 0);
}