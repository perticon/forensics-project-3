void spinlock_cleanup(int32_t * splk) {
    int32_t v1 = (int32_t)splk;
    int32_t v2 = v1; // 0x80015158
    if (*(int32_t *)(v1 + 4) != 0) {
        // 0x8001515c
        badassert("splk->splk_holder == NULL", "../../thread/spinlock.c", 64, "spinlock_cleanup");
        v2 = (int32_t)"splk->splk_holder == NULL";
    }
    // 0x80015178
    if (spinlock_data_get((int32_t *)v2) != 0) {
        // 0x80015188
        badassert("spinlock_data_get(&splk->splk_lock) == 0", "../../thread/spinlock.c", 65, "spinlock_cleanup");
    }
}