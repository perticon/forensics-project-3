vfs_biglock_release(void)
{
	KASSERT(lock_do_i_hold(vfs_biglock));
	KASSERT(vfs_biglock_depth > 0);
	vfs_biglock_depth--;
	if (vfs_biglock_depth == 0) {
		lock_release(vfs_biglock);
	}
}