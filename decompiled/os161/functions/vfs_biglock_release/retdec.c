void vfs_biglock_release(void) {
    bool v1 = lock_do_i_hold(vfs_biglock); // 0x800187a8
    int32_t v2 = (int32_t)&g35; // 0x800187b0
    if (!v1) {
        // 0x800187b4
        badassert("lock_do_i_hold(vfs_biglock)", "../../vfs/vfslist.c", 155, "vfs_biglock_release");
        v2 = &g41;
    }
    int32_t v3 = *(int32_t *)(v2 - 0x737c); // 0x800187d4
    if (v3 == 0) {
        // 0x80018804
        badassert("vfs_biglock_depth > 0", "../../vfs/vfslist.c", 156, "vfs_biglock_release");
        vfs_biglock_depth = &g41;
        // 0x80018820
        return;
    }
    int32_t v4 = v3 - 1; // 0x800187e0
    vfs_biglock_depth = v4;
    if (v4 == 0) {
        // 0x80018810
        lock_release(vfs_biglock);
    }
}