void vfs_biglock_release(void)

{
  undefined3 extraout_var;
  bool bVar3;
  int iVar1;
  uint uVar2;
  
  bVar3 = lock_do_i_hold(vfs_biglock);
  iVar1 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("lock_do_i_hold(vfs_biglock)","../../vfs/vfslist.c",0x9b,"vfs_biglock_release");
  }
  uVar2 = *(int *)(iVar1 + -0x737c) - 1;
  if (*(int *)(iVar1 + -0x737c) == 0) {
    badassert("vfs_biglock_depth > 0","../../vfs/vfslist.c",0x9c,"vfs_biglock_release");
  }
  vfs_biglock_depth = uVar2;
  if (uVar2 == 0) {
    lock_release(vfs_biglock);
  }
  return;
}