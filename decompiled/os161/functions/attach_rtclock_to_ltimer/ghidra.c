rtclock_softc * attach_rtclock_to_ltimer(int rtclockno,ltimer_softc *ls)

{
  rtclock_softc *prVar1;
  
  prVar1 = (rtclock_softc *)kmalloc(8);
  if (prVar1 == (rtclock_softc *)0x0) {
    prVar1 = (rtclock_softc *)0x0;
  }
  else {
    prVar1->rtc_devdata = ls;
    prVar1->rtc_gettime = ltimer_gettime;
  }
  return prVar1;
}