static void remove_swap_list(struct swap_entry *entry)
{

    struct swap_entry *prev, *next;

    KASSERT(entry != swap_list && entry != free_list_tail);

    prev = entry->previous;
    next = entry->next;
    prev->next = next;
    next->previous = prev;
}