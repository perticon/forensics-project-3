int32_t tryattach_lser_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_lser_to_lamebus(devunit, bus); // 0x800018d4
    if (v1 == NULL) {
        // 0x80001958
        return -1;
    }
    // 0x800018e0
    kprintf("lser%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_lser(v1, devunit); // 0x800018fc
    int32_t result; // 0x800018b8
    if (v2 == 0) {
        // 0x80001928
        kprintf("\n");
        nextunit_lser = devunit + 1;
        autoconf_lser(v1, devunit);
        result = 0;
    } else {
        // 0x80001908
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x80001958
    return result;
}