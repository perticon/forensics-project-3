int tryattach_lser_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lser_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_lser_to_lamebus(devunit,bus);
  if (sc == (lser_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lser%d at lamebus%d",devunit,busunit);
    errcode = config_lser(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lser = devunit + 1;
      autoconf_lser(sc,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}