tryattach_lser_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct lser_softc *dev;
	int result;

	dev = attach_lser_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("lser%d at lamebus%d", devunit, busunit);
	result = config_lser(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_lser = devunit+1;
	autoconf_lser(dev, devunit);
	return 0;
}