int32_t config_random(int32_t * rs, int32_t unit) {
    if (unit != 0) {
        // 0x800022cc
        return 25;
    }
    int32_t v1 = (int32_t)rs;
    int32_t v2 = v1; // 0x80002268
    int32_t v3 = v1; // 0x80002268
    if (the_random != NULL) {
        // 0x8000226c
        badassert("the_random==NULL", "../../dev/generic/random.c", 120, "config_random");
        v2 = &g41;
        v3 = (int32_t)"the_random==NULL";
    }
    // 0x8000228c
    *(int32_t *)&the_random = v3;
    g9 = &random_devops;
    *(int32_t *)"../../dev/generic/random.c" = 0;
    *(int32_t *)"./dev/generic/random.c" = 1;
    *(int32_t *)(v2 + 32) = v3;
    return vfs_adddev("random", (int32_t *)(v2 + 16), 0);
}