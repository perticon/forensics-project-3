int config_random(random_softc *rs,int unit)

{
  int iVar1;
  char *pcVar2;
  
  if (unit == 0) {
    pcVar2 = (char *)rs;
    if (the_random != (random_softc *)0x0) {
      pcVar2 = "the_random==NULL";
      badassert("the_random==NULL",s_______dev_generic_random_c_800226dc,0x78,"config_random");
    }
    the_random = (random_softc *)pcVar2;
    ((device *)((int)pcVar2 + 0x10))->d_ops = &random_devops;
    ((device *)((int)pcVar2 + 0x10))->d_blocks = 0;
    ((device *)((int)pcVar2 + 0x10))->d_blocksize = 1;
    (rs->rs_dev).d_data = pcVar2;
    iVar1 = vfs_adddev("random",&rs->rs_dev,0);
    return iVar1;
  }
  return 0x19;
}