config_random(struct random_softc *rs, int unit)
{
	int result;

	/* We use only the first random device. */
	if (unit!=0) {
		return ENODEV;
	}

	KASSERT(the_random==NULL);
	the_random = rs;

	rs->rs_dev.d_ops = &random_devops;
	rs->rs_dev.d_blocks = 0;
	rs->rs_dev.d_blocksize = 1;
	rs->rs_dev.d_data = rs;

	/* Add the VFS device structure to the VFS device list. */
	result = vfs_adddev("random", &rs->rs_dev, 0);
	if (result) {
		return result;
	}

	return 0;
}