void lamebus_write_register(lamebus_softc *bus,int slot,uint32_t offset,uint32_t val)

{
  uint32_t *puVar1;
  
  puVar1 = (uint32_t *)lamebus_map_area(bus,slot,offset);
  *puVar1 = val;
  SYNC(0);
  return;
}