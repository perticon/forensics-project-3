void lamebus_write_register(int32_t * bus, int32_t slot, int32_t offset, int32_t val) {
    // 0x80020898
    *(int32_t *)lamebus_map_area(bus, slot, offset) = val;
    __asm_sync();
}