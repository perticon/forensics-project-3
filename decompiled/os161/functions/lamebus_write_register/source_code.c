lamebus_write_register(struct lamebus_softc *bus, int slot,
		       uint32_t offset, uint32_t val)
{
	uint32_t *ptr;

	ptr = lamebus_map_area(bus, slot, offset);
	*ptr = val;

	/*
	 * Make sure the store happens before we do anything else to
	 * the device.
	 */
	membar_store_store();
}