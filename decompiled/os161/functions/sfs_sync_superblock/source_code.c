sfs_sync_superblock(struct sfs_fs *sfs)
{
	int result;

	if (sfs->sfs_superdirty) {
		result = sfs_writeblock(sfs, SFS_SUPER_BLOCK, &sfs->sfs_sb,
					sizeof(sfs->sfs_sb));
		if (result) {
			return result;
		}
		sfs->sfs_superdirty = false;
	}
	return 0;
}