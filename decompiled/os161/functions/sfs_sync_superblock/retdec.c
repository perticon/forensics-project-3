int32_t sfs_sync_superblock(int32_t * sfs) {
    int32_t v1 = (int32_t)sfs;
    char * v2 = (char *)(v1 + 520); // 0x80007edc
    if (*v2 == 0) {
        // 0x80007f10
        return 0;
    }
    int32_t v3 = sfs_writeblock(sfs, 0, (char *)(v1 + 8), 512); // 0x80007ef8
    int32_t result = v3; // 0x80007f00
    if (v3 == 0) {
        // 0x80007f04
        *v2 = 0;
        result = 0;
    }
    // 0x80007f10
    return result;
}