int sfs_sync_superblock(sfs_fs *sfs)

{
  int iVar1;
  
  if (sfs->sfs_superdirty == false) {
    iVar1 = 0;
  }
  else {
    iVar1 = sfs_writeblock(sfs,0,&sfs->sfs_sb,0x200);
    if (iVar1 == 0) {
      sfs->sfs_superdirty = false;
    }
  }
  return iVar1;
}