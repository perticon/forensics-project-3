int32_t emufs_namefile(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(*(int32_t *)*(int32_t *)(v1 + 12) + 12); // 0x8000261c
    return v2_ != *(int32_t *)(v1 + 16);
}