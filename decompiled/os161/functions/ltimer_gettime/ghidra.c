void ltimer_gettime(void *vlt,timespec *ts)

{
  int spl;
  uint32_t uVar1;
  uint32_t uVar2;
  
  spl = splx(1);
  uVar1 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0);
  uVar2 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),4);
  ts->tv_nsec = uVar2;
  uVar2 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0);
  splx(spl);
  if (ts->tv_nsec < 5000000) {
    *(uint32_t *)((int)&ts->tv_sec + 4) = uVar2;
    *(undefined4 *)&ts->tv_sec = 0;
  }
  else {
    *(uint32_t *)((int)&ts->tv_sec + 4) = uVar1;
    *(undefined4 *)&ts->tv_sec = 0;
  }
  return;
}