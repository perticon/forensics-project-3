void ltimer_gettime(char * vlt, int32_t * ts) {
    int32_t v1 = (int32_t)ts;
    int32_t v2 = (int32_t)vlt;
    int32_t v3 = splx(1); // 0x800050b4
    int32_t * v4 = (int32_t *)(v2 + 8); // 0x800050bc
    int32_t * v5 = (int32_t *)(v2 + 12); // 0x800050c0
    int32_t v6 = lamebus_read_register((int32_t *)*v4, *v5, 0); // 0x800050c8
    int32_t v7 = lamebus_read_register((int32_t *)*v4, *v5, 4); // 0x800050dc
    int32_t * v8 = (int32_t *)(v1 + 8); // 0x800050e0
    *v8 = v7;
    int32_t v9 = lamebus_read_register((int32_t *)*v4, *v5, 0); // 0x800050f0
    splx(v3);
    *(int32_t *)(v1 + 4) = *v8 >= 0x4c4b40 ? v6 : v9;
    *ts = 0;
}