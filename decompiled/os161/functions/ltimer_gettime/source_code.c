ltimer_gettime(void *vlt, struct timespec *ts)
{
	struct ltimer_softc *lt = vlt;
	uint32_t secs1, secs2;
	int spl;

	/*
	 * Read the seconds twice, on either side of the nanoseconds.
	 * If nsecs is small, use the *later* value of seconds, in case
	 * the nanoseconds turned over between the time we got the earlier
	 * value and the time we got nsecs.
	 *
	 * Note that the clock in the ltimer device is accurate down
	 * to a single processor cycle, so this might actually matter
	 * now and then.
	 *
	 * Do it with interrupts off on the current processor to avoid
	 * getting garbage if we get an interrupt among the register
	 * reads.
	 */

	spl = splhigh();

	secs1 = bus_read_register(lt->lt_bus, lt->lt_buspos,
				  LT_REG_SEC);
	ts->tv_nsec = bus_read_register(lt->lt_bus, lt->lt_buspos,
				   LT_REG_NSEC);
	secs2 = bus_read_register(lt->lt_bus, lt->lt_buspos,
				  LT_REG_SEC);

	splx(spl);

	if (ts->tv_nsec < 5000000) {
		ts->tv_sec = secs2;
	}
	else {
		ts->tv_sec = secs1;
	}
}