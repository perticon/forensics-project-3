void print_swap_internal(void) {
    // 0x8001d978
    kprintf("<< SWAP TABLE >>\n");
    int32_t v1 = 0;
    int32_t v2 = 8 * v1; // 0x8001d9b0
    int32_t v3 = *(int32_t *)(v2 + (int32_t)&swap_table); // 0x8001d9b8
    uint32_t v4; // 0x8001d9cc
    if (v3 != -1) {
        // 0x8001d9c8
        v4 = *(int32_t *)(v2 + (int32_t)&swap_table + 4);
        kprintf("%d -   %d   - %d\n", v1, v3, v4 / 0x1000);
    }
    int32_t v5 = v1 + 1; // 0x8001d9e0
    while (v5 < 2304) {
        // 0x8001d9b0
        v1 = v5;
        v2 = 8 * v1;
        v3 = *(int32_t *)(v2 + (int32_t)&swap_table);
        if (v3 != -1) {
            // 0x8001d9c8
            v4 = *(int32_t *)(v2 + (int32_t)&swap_table + 4);
            kprintf("%d -   %d   - %d\n", v1, v3, v4 / 0x1000);
        }
        // 0x8001d9e0
        v5 = v1 + 1;
    }
}