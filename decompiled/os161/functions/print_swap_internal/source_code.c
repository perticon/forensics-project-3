static void print_swap_internal(void)
{

    kprintf("<< SWAP TABLE >>\n");

    for (int i = 0; i < ENTRIES; i++)
    {
        if (swap_table[i].pid != -1)
        {
            kprintf("%d -   %d   - %d\n", i, swap_table[i].pid, swap_table[i].page / PAGE_SIZE);
        }
    }
}