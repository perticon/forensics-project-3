void print_swap_internal(void)

{
  int iVar1;
  
  kprintf("<< SWAP TABLE >>\n");
  for (iVar1 = 0; iVar1 < 0x900; iVar1 = iVar1 + 1) {
    if (swap_table[iVar1].pid != -1) {
      kprintf("%d -   %d   - %d\n",iVar1,swap_table[iVar1].pid,swap_table[iVar1].page >> 0xc);
    }
  }
  return;
}