void switchframe_init(int32_t * thread, void (*entrypoint)(char *, int32_t), char * data1, int32_t data2) {
    int32_t v1 = (int32_t)thread;
    int32_t v2 = *(int32_t *)(v1 + 72); // 0x8001f3a0
    int32_t v3 = v2 + 4056; // 0x8001f3a8
    bzero((char *)v3, 40);
    *(int32_t *)v3 = (int32_t)entrypoint;
    *(int32_t *)(v2 + 4060) = (int32_t)data1;
    *(int32_t *)(v2 + 4064) = data2;
    *(int32_t *)(v2 + 4092) = -0x7ffe0b40;
    *(int32_t *)(v1 + 76) = v3;
}