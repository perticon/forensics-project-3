void switchframe_init(thread *thread,anon_subr_void_void_ptr_ulong *entrypoint,void *data1,
                     ulong data2)

{
  void *pvVar1;
  
  pvVar1 = thread->t_stack;
  bzero((switchframe *)((int)pvVar1 + 0xfd8),0x28);
  *(anon_subr_void_void_ptr_ulong **)((int)pvVar1 + 0xfd8) = entrypoint;
  *(void **)((int)pvVar1 + 0xfdc) = data1;
  *(ulong *)((int)pvVar1 + 0xfe0) = data2;
  *(code **)((int)pvVar1 + 0xffc) = mips_threadstart;
  thread->t_context = (switchframe *)((int)pvVar1 + 0xfd8);
  return;
}