dev_eachopen(struct vnode *v, int flags)
{
	struct device *d = v->vn_data;

	if (flags & (O_CREAT | O_TRUNC | O_EXCL | O_APPEND)) {
		return EINVAL;
	}

	return DEVOP_EACHOPEN(d, flags);
}