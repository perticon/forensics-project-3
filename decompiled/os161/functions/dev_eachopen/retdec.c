int32_t dev_eachopen(int32_t * v2, int32_t flags) {
    if ((flags & 60) == 0) {
        // 0x80017a10
        return *(int32_t *)*(int32_t *)*(int32_t *)((int32_t)v2 + 16);
    }
    // 0x80017a38
    return 8;
}