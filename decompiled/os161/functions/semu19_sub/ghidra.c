void semu19_sub(void *semv,ulong junk)

{
  kprintf("semu19: waiting for parent to sleep\n");
  clocksleep(1);
  V((semaphore *)semv);
  return;
}