semu19_sub(void *semv,  unsigned long junk)
{
	struct semaphore *sem = semv;

	(void)junk;

	kprintf("semu19: waiting for parent to sleep\n");
	clocksleep(1);
	/*
	 * We could assert here that the parent *is* sleeping; but for
	 * that we'd need its thread pointer and it's not worth the
	 * trouble.
	 */
	V(sem);
}