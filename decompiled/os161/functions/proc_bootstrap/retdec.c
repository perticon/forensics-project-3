void proc_bootstrap(void) {
    int32_t * v1 = proc_create("[kernel]"); // 0x8000cca8
    *(int32_t *)&kproc = (int32_t)v1;
    if (v1 == NULL) {
        // 0x8000ccb8
        panic("proc_create for kproc failed\n");
    }
    // 0x8000ccc4
    spinlock_init(&g29);
    processTable = 1;
}