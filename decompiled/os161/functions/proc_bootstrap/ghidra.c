void proc_bootstrap(void)

{
  kproc = proc_create("[kernel]");
  if (kproc == (proc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("proc_create for kproc failed\n");
  }
  spinlock_init(&processTable.lk);
  processTable.active = 1;
  return;
}