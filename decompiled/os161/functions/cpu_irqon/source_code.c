cpu_irqon(void)
{
        uint32_t x;

        GET_STATUS(x);
        x |= CST_IEc;
        SET_STATUS(x);
}