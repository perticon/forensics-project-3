void cpu_irqon(void) {
    // 0x8001f2fc
    int32_t v1; // 0x8001f2fc
    __asm_mfc0(v1, v1, 0);
    __asm_mtc0(v1 | 1, v1, 0);
}