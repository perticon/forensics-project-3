semfs_eachopen(struct vnode *vn, int openflags)
{
	struct semfs_vnode *semv = vn->vn_data;

	if (semv->semv_semnum == SEMFS_ROOTDIR) {
		if ((openflags & O_ACCMODE) != O_RDONLY) {
			return EISDIR;
		}
		if (openflags & O_APPEND) {
			return EISDIR;
		}
	}

	return 0;
}