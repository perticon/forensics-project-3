int semfs_eachopen(vnode *vn,int openflags)

{
  if (*(int *)((int)vn->vn_data + 0x1c) != -1) {
    return 0;
  }
  if ((openflags & 3U) == 0) {
    if ((openflags & 0x20U) == 0) {
      return 0;
    }
    return 0x12;
  }
  return 0x12;
}