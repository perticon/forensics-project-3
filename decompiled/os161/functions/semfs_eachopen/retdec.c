int32_t semfs_eachopen(int32_t * vn, uint32_t openflags) {
    // 0x80005fd0
    if (*(int32_t *)(*(int32_t *)((int32_t)vn + 16) + 28) != -1) {
        // 0x80006004
        return 0;
    }
    // 0x80005fe8
    if ((openflags & 35) == 0) {
        // 0x80005ffc
        return openflags % 4;
    }
    // 0x80006004
    return 18;
}