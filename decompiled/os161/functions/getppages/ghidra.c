paddr_t getppages(ulong npages,int kmem)

{
  char *paddr;
  int iVar1;
  addrspace *as;
  char *paddr_00;
  vaddr_t vaddr;
  pid_t pid_victim;
  
  paddr = (char *)getfreeppages(npages);
  if (paddr == (char *)0x0) {
    spinlock_acquire(&stealmem_lock);
    paddr = (char *)ram_stealmem(npages);
    spinlock_release(&stealmem_lock);
  }
  if ((paddr != (char *)0x0) && (iVar1 = isTableActive(), iVar1 != 0)) {
    spinlock_acquire(&freemem_lock);
    allocSize[(uint)paddr >> 0xc] = npages;
    spinlock_release(&freemem_lock);
  }
  paddr_00 = paddr;
  if (paddr == (char *)0x0) {
    iVar1 = isTableActive();
    if (iVar1 != 0) {
      spinlock_acquire(&freemem_lock);
      if (kmem == 0) {
        if (npages != 1) {
          badassert("npages == 1","../../vm/coremap.c",0xba,"getppages");
        }
        pid_victim = 0;
      }
      else {
        pid_victim = 1;
      }
      if ((kmem != 0) && (npages != 1)) {
                    /* WARNING: Subroutine does not return */
        panic("No contiguous %ld free ram frames for kernel allocation",npages);
      }
      if (kmem == 0) {
        paddr = (char *)get_victim(&vaddr,&pid_victim);
      }
      if (paddr == (char *)0x0) {
                    /* WARNING: Subroutine does not return */
        panic(
             "It was not possible to find a page victim.\nAre you allocating more kernel memory than the available ram size?"
             );
      }
      as = pid_getas(pid_victim);
      iVar1 = address_segment(vaddr,as);
      spinlock_release(&freemem_lock);
      iVar1 = swap_out((paddr_t)paddr,vaddr,iVar1,pid_victim);
      if (iVar1 != 0) {
        return 0;
      }
    }
    paddr_00 = paddr;
    if (paddr == (char *)0x0) {
      paddr_00 = "paddr != 0";
      badassert("paddr != 0","../../vm/coremap.c",0xdd,"getppages");
    }
  }
  as_zero_region((paddr_t)paddr_00,npages);
  return (paddr_t)paddr;
}