int32_t getppages(int32_t npages, int32_t kmem) {
    int32_t v1 = getfreeppages(npages); // 0x8001b608
    int32_t v2 = v1; // 0x8001b610
    int32_t pid_victim; // bp-28, 0x8001b5e8
    int32_t v3; // 0x8001b5e8
    int32_t vaddr; // bp-32, 0x8001b5e8
    if (v1 == 0) {
        // 0x8001b634
        spinlock_acquire(&stealmem_lock);
        v2 = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);
        if (v2 == 0) {
            // 0x8001b684
            if (isTableActive() == 0) {
                goto lab_0x8001b76c;
            } else {
                // 0x8001b694
                spinlock_acquire(&freemem_lock);
                if (kmem == 0) {
                    if (npages != 1) {
                        // 0x8001b6ac
                        badassert("npages == 1", "../../vm/coremap.c", 186, "getppages");
                    }
                    // 0x8001b70c
                    pid_victim = 0;
                    int32_t v4 = get_victim(&vaddr, &pid_victim); // 0x8001b704
                    v3 = v4;
                    if (v4 == 0) {
                        // 0x8001b714
                        panic("It was not possible to find a page victim.\nAre you allocating more kernel memory than the available ram size?");
                        v3 = 0;
                        goto lab_0x8001b720;
                    } else {
                        goto lab_0x8001b720;
                    }
                } else {
                    // 0x8001b6d4
                    pid_victim = 1;
                    if (npages != 1) {
                        // 0x8001b6ec
                        panic("No contiguous %ld free ram frames for kernel allocation", npages);
                    }
                    // 0x8001b714
                    panic("It was not possible to find a page victim.\nAre you allocating more kernel memory than the available ram size?");
                    v3 = 0;
                    goto lab_0x8001b720;
                }
            }
        } else {
            goto lab_0x8001b63c;
        }
    } else {
        goto lab_0x8001b63c;
    }
  lab_0x8001b63c:;
    int32_t v5 = v2; // 0x8001b648
    int32_t result = v2; // 0x8001b648
    if (isTableActive() != 0) {
        // 0x8001b64c
        spinlock_acquire(&freemem_lock);
        *(int32_t *)(4 * v2 / 0x1000 + (int32_t)allocSize) = npages;
        spinlock_release(&freemem_lock);
        v5 = v2;
        result = v2;
    }
    goto lab_0x8001b78c;
  lab_0x8001b78c:
    // 0x8001b78c
    as_zero_region(v5, npages);
    // 0x8001b798
    return result;
  lab_0x8001b76c:
    // 0x8001b76c
    badassert("paddr != 0", "../../vm/coremap.c", 221, "getppages");
    v5 = (int32_t)"paddr != 0";
    result = 0;
    goto lab_0x8001b78c;
  lab_0x8001b720:;
    int32_t v6 = address_segment(vaddr, pid_getas(pid_victim)); // 0x8001b734
    spinlock_release(&freemem_lock);
    if (swap_out(v3, vaddr, v6, pid_victim) != 0) {
        // 0x8001b798
        return 0;
    }
    // 0x8001b764
    v5 = v3;
    result = v3;
    if (v3 == 0) {
        goto lab_0x8001b76c;
    } else {
        goto lab_0x8001b78c;
    }
}