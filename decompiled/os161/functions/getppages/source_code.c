getppages(unsigned long npages, int kmem)
{
    paddr_t paddr;
    vaddr_t vaddr;
    pid_t pid_victim;
    struct addrspace *as_victim;
    int victim_segment, result;

    /* try freed pages first */
    paddr = getfreeppages(npages);
    if (paddr == 0)
    {
        /* call stealmem */
        spinlock_acquire(&stealmem_lock);
        paddr = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);
    }

    /* save length allocated */
    if (paddr != 0 && isTableActive())
    {
        spinlock_acquire(&freemem_lock);
        allocSize[paddr / PAGE_SIZE] = npages;
        spinlock_release(&freemem_lock);
    }

    /* If neither getfreeppages and ram_stealmem do no return pages, swap out a page */
    if (paddr == 0 && isTableActive())
    {
        spinlock_acquire(&freemem_lock);
        /* we can only get one page at a time with swapping, otherwise in case of swap problems occur */
        if (!kmem)
        {
            KASSERT(npages == 1);
            pid_victim = 0;
        }
        else
        {
            pid_victim = 1;
        }
        /* cannot swap contiguous pages, so in case of kernel, panic */
        if (paddr == 0 && kmem && npages != 1)
        {
            panic("No contiguous %ld free ram frames for kernel allocation", npages);
        }
        /* get physical and virtual address of victim */
        if (!kmem)
        {
            paddr = get_victim(&vaddr, &pid_victim);
        }
        /* no victim found */
        if (paddr == 0)
        {
            panic("It was not possible to find a page victim.\nAre you allocating more kernel memory than the available ram size?");
        }
        /* get address space of the process whose page is the victim */
        as_victim = pid_getas(pid_victim);
        /* get in which segment the page is */
        victim_segment = address_segment(vaddr, as_victim);
        spinlock_release(&freemem_lock);
        /* swap page out */
        result = swap_out(paddr, vaddr, victim_segment, pid_victim);
        if (result)
        {
            return 0;
        }
    }

    KASSERT(paddr != 0);
    /* zero fill the allocated page(s) */
    as_zero_region(paddr, npages);

    return paddr;
}