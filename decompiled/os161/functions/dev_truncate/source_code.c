dev_truncate(struct vnode *v, off_t len)
{
	struct device *d = v->vn_data;

	/*
	 * Allow truncating to the object's own size, if it has one.
	 */
	if (d->d_blocks > 0 && (off_t)(d->d_blocks*d->d_blocksize) == len) {
		return 0;
	}

	return EINVAL;
}