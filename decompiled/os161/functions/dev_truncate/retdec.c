int32_t dev_truncate(int32_t * v2, int64_t len) {
    int32_t v1 = *(int32_t *)((int32_t)v2 + 16); // 0x80017ba0
    int32_t v2_ = *(int32_t *)(v1 + 4); // 0x80017ba8
    int32_t result = 8; // 0x80017bb4
    if ((int32_t)len == 0 == (v2_ != 0)) {
        // 0x80017bc4
        int32_t v3; // 0x80017ba0
        result = *(int32_t *)(v1 + 8) * v2_ == v3 ? 0 : 8;
    }
    // 0x80017bd0
    return result;
}