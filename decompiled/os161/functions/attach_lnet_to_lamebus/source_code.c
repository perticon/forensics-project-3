attach_lnet_to_lamebus(int lnetno, struct lamebus_softc *sc)
{
	int slot = lamebus_probe(sc, LB_VENDOR_CS161, LBCS161_NET,
				 LOW_VERSION, HIGH_VERSION);
	if (slot < 0) {
		return NULL;
	}

	kprintf("lnet%d: No network support in system\n", lnetno);

	return NULL;
}