void cpu_hatch(int32_t software_number) {
    char buf[64]; // bp-72, 0x80016cc4
    // 0x80016cc4
    int32_t v1; // 0x80016cc4
    int32_t v2 = *(int32_t *)(v1 + 80); // 0x80016cd0
    int32_t v3 = software_number; // 0x80016cdc
    int32_t v4 = v2; // 0x80016cdc
    if (v2 == 0) {
        // 0x80016ce0
        badassert("curcpu != NULL", "../../thread/thread.c", 410, "cpu_hatch");
        v3 = (int32_t)"curcpu != NULL";
        v4 = &g41;
    }
    int32_t v5 = v3; // 0x80016d00
    int32_t v6 = v4; // 0x80016d00
    if (v1 == 0) {
        // 0x80016d04
        badassert("curthread != NULL", "../../thread/thread.c", 411, "cpu_hatch");
        v5 = (int32_t)"curthread != NULL";
        v6 = &g41;
    }
    // 0x80016d24
    if (*(int32_t *)(v6 + 4) != v5) {
        // 0x80016d34
        badassert("curcpu->c_number == software_number", "../../thread/thread.c", 412, "cpu_hatch");
    }
    // 0x80016d50
    splx(0);
    cpu_identify(buf, 64);
    kprintf("cpu%u: %s\n", v3, buf);
    V(cpu_startup_sem);
    thread_exit();
}