void cpu_hatch(uint software_number)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  char buf [64];
  
  iVar1 = *(int *)(unaff_s7 + 0x50);
  if (iVar1 == 0) {
    pcVar2 = "curcpu != NULL";
    badassert("curcpu != NULL","../../thread/thread.c",0x19a,"cpu_hatch");
    software_number = (uint)pcVar2;
  }
  pcVar2 = (char *)software_number;
  if (unaff_s7 == 0) {
    pcVar2 = "curthread != NULL";
    badassert("curthread != NULL","../../thread/thread.c",0x19b,"cpu_hatch");
  }
  if (*(char **)(iVar1 + 4) != pcVar2) {
    badassert("curcpu->c_number == software_number","../../thread/thread.c",0x19c,"cpu_hatch");
  }
  splx(0);
  cpu_identify(buf,0x40);
  kprintf("cpu%u: %s\n",software_number,buf);
  V(cpu_startup_sem);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}