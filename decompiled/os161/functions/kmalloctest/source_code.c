kmalloctest(int nargs, char **args)
{
	(void)nargs;
	(void)args;

	kprintf("Starting kmalloc test...\n");
	kmallocthread(NULL, 0);
	kprintf("kmalloc test done\n");

	return 0;
}