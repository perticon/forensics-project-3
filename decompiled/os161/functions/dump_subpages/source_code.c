dump_subpages(unsigned generation)
{
	struct pageref *pr;
	int i;

	kprintf("Remaining allocations from generation %u:\n", generation);
	for (i=0; i<NSIZES; i++) {
		for (pr = sizebases[i]; pr != NULL; pr = pr->next_samesize) {
			dump_subpage(pr, generation);
		}
	}
}