void testa(int32_t * a) {
    int32_t testarray[73]; // bp-320, 0x8000e0d0
    int32_t v1 = (int32_t)&testarray;
    int32_t v2 = 0; // 0x8000e100
    *(int32_t *)v1 = v2;
    v2++;
    while (v2 < 73) {
        // 0x8000e0f4
        *(int32_t *)(4 * v2 + v1) = v2;
        v2++;
    }
    int32_t v3 = (int32_t)a;
    int32_t * v4 = (int32_t *)(v3 + 4); // 0x8000e110
    int32_t v5 = v1; // 0x8000e11c
    int32_t v6 = 0; // 0x8000e11c
    int32_t v7; // 0x8000e0d0
    int32_t v8; // 0x8000e0d0
    if (*v4 == 0) {
        goto lab_0x8000e1c8;
    } else {
        // 0x8000e120
        badassert("n==0", "../../test/arraytest.c", 51, "testa");
        v8 = &g41;
        v7 = 0;
        goto lab_0x8000e140;
    }
  lab_0x8000e1c8:;
    int32_t v9 = v6; // 0x8000e1c8
    int32_t v10 = v5; // 0x8000e1c8
    int32_t v11; // 0x8000e0d0
    int32_t v12 = v11; // 0x8000e1c8
    goto lab_0x8000e1cc;
  lab_0x8000e1cc:
    // 0x8000e1cc
    v8 = 4 * v9;
    v7 = v9;
    int32_t v13; // 0x8000e0d0
    int32_t v14; // 0x8000e0d0
    int32_t v15; // 0x8000e0d0
    int32_t v16; // 0x8000e0d0
    int32_t v17; // 0x8000e0d0
    int32_t v18; // 0x8000e0d0
    int32_t v19; // 0x8000e0d0
    if (v9 >= 73) {
        // 0x8000e1d4
        v19 = 0;
        v15 = v10;
        v17 = 73;
        if (*v4 == 73) {
            goto lab_0x8000e270;
        } else {
            // 0x8000e204
            badassert("n==TESTSIZE", "../../test/arraytest.c", 60, "testa");
            v13 = 4 * (int32_t)&g41;
            v18 = &g41;
            v14 = (int32_t)"n==TESTSIZE";
            v16 = (int32_t)"../../test/arraytest.c";
            goto lab_0x8000e22c;
        }
    } else {
        goto lab_0x8000e140;
    }
  lab_0x8000e140:;
    int32_t v20 = *v4; // 0x8000e148
    int32_t v21 = array_setsize(a, v20 + 1); // 0x8000e154
    int32_t v22 = 4 * v20; // 0x8000e15c
    int32_t v23; // 0x8000e0d0
    int32_t v24; // 0x8000e0d0
    if (v21 == 0) {
        int32_t v25 = v22 + v3; // 0x8000e168
        *(int32_t *)v25 = v8 + v1;
        v24 = v25;
        v23 = -0x7ffe0000;
    } else {
        // 0x8000e17c
        badassert("r==0", "../../test/arraytest.c", 55, "testa");
        v24 = v22;
        v23 = (int32_t)"r==0";
    }
    int32_t v26 = v24;
    int32_t v27 = *v4; // 0x8000e198
    v6 = v7 + 1;
    v9 = v27;
    v10 = v23;
    v12 = v26;
    if (v6 == v27) {
        goto lab_0x8000e1cc;
    } else {
        // 0x8000e1a8
        badassert("n==i+1", "../../test/arraytest.c", 57, "testa");
        v5 = (int32_t)"n==i+1";
        v11 = v26;
        goto lab_0x8000e1c8;
    }
  lab_0x8000e270:;
    int32_t v28 = v17;
    int32_t v29; // 0x8000e0d0
    int32_t v30; // 0x8000e0d0
    int32_t v31; // 0x8000e0d0
    int32_t v32; // 0x8000e0d0
    if (v19 < 73) {
        int32_t v33 = 4 * v19; // 0x8000e208
        v13 = v33;
        v18 = v19;
        v14 = v15;
        v16 = v28;
        if (v19 >= v28) {
            // 0x8000e20c
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v13 = v33;
            v18 = &g41;
            v14 = (int32_t)"index < a->num";
            v16 = (int32_t)"../../include/array.h";
        }
        goto lab_0x8000e22c;
    } else {
        // 0x8000e27c
        v30 = 0;
        v32 = 73;
        if (v28 == 73) {
            goto lab_0x8000e340;
        } else {
            // 0x8000e288
            badassert("n==TESTSIZE", "../../test/arraytest.c", 67, "testa");
            v29 = 0;
            v31 = v12;
            goto lab_0x8000e2a8;
        }
    }
  lab_0x8000e22c:;
    int32_t v34 = v18; // 0x8000e24c
    int32_t v35 = -0x7ffe0000; // 0x8000e24c
    int32_t v36 = v16; // 0x8000e24c
    if (*(int32_t *)*(int32_t *)(v14 + v13) != v18) {
        // 0x8000e250
        badassert("*p == i", "../../test/arraytest.c", 64, "testa");
        v34 = &g41;
        v35 = (int32_t)"*p == i";
        v36 = (int32_t)"../../test/arraytest.c";
    }
    // 0x8000e26c
    v19 = v34 + 1;
    v15 = v35;
    v17 = v36;
    goto lab_0x8000e270;
  lab_0x8000e340:
    // 0x8000e340
    v29 = v30;
    v31 = v32;
    int32_t v37; // 0x8000e0d0
    int32_t v38; // 0x8000e0d0
    int32_t v39; // 0x8000e0d0
    int32_t v40; // 0x8000e0d0
    int32_t v41; // 0x8000e0d0
    if (v30 < 292) {
        goto lab_0x8000e2a8;
    } else {
        // 0x8000e34c
        v41 = 0;
        v39 = 72;
        if (*v4 == 73) {
            goto lab_0x8000e3d8;
        } else {
            // 0x8000e35c
            badassert("n==TESTSIZE", "../../test/arraytest.c", 75, "testa");
            v40 = &g41;
            v37 = (int32_t)"n==TESTSIZE";
            v38 = 75;
            goto lab_0x8000e37c;
        }
    }
  lab_0x8000e2a8:;
    uint32_t v42 = random() % v31; // 0x8000e2b4
    int32_t v43 = v42; // 0x8000e2d0
    if (v42 >= *v4) {
        // 0x8000e2d4
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v43 = (int32_t)"index < a->num";
    }
    int32_t v44 = v43;
    if (*(int32_t *)*(int32_t *)(v44 + 4 * v42) != v44) {
        // 0x8000e318
        badassert("*p == i", "../../test/arraytest.c", 72, "testa");
    }
    // 0x8000e334
    v30 = v29 + 1;
    v32 = v31;
    goto lab_0x8000e340;
  lab_0x8000e3d8:
    // 0x8000e3d8
    v40 = v41;
    v37 = v39 - v41;
    v38 = v39;
    int32_t v45 = 0; // 0x8000e3e0
    int32_t v46; // 0x8000e0d0
    int32_t v47; // 0x8000e0d0
    int32_t v48; // 0x8000e0d0
    int32_t v49; // 0x8000e0d0
    int32_t v50; // 0x8000e0d0
    if (v41 < 73) {
        goto lab_0x8000e37c;
    } else {
        int32_t v51 = v45; // 0x8000e400
        int32_t v52 = v3; // 0x8000e400
        int32_t v53 = 72; // 0x8000e400
        if (v45 >= *v4) {
            // 0x8000e404
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v51 = &g41;
            v52 = (int32_t)"index < a->num";
            v53 = (int32_t)"../../include/array.h";
        }
        int32_t v54 = *(int32_t *)(v52 + 4 * v45); // 0x8000e430
        int32_t v55 = v51 + 1; // 0x8000e444
        int32_t v56 = v53; // 0x8000e444
        if (*(int32_t *)v54 != v53 - v51) {
            // 0x8000e448
            badassert("*p == TESTSIZE-i-1", "../../test/arraytest.c", 83, "testa");
            v55 = &g41;
            v56 = (int32_t)"../../test/arraytest.c";
        }
        // 0x8000e468
        v45 = v55;
        while (v55 < 73) {
            // 0x8000e3f0
            v51 = v45;
            v52 = v3;
            v53 = v56;
            if (v45 >= *v4) {
                // 0x8000e404
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v51 = &g41;
                v52 = (int32_t)"index < a->num";
                v53 = (int32_t)"../../include/array.h";
            }
            // 0x8000e424
            v54 = *(int32_t *)(v52 + 4 * v45);
            v55 = v51 + 1;
            v56 = v53;
            if (*(int32_t *)v54 != v53 - v51) {
                // 0x8000e448
                badassert("*p == TESTSIZE-i-1", "../../test/arraytest.c", 83, "testa");
                v55 = &g41;
                v56 = (int32_t)"../../test/arraytest.c";
            }
            // 0x8000e468
            v45 = v55;
        }
        // 0x8000e474
        v50 = 0;
        v48 = 72;
        if (array_setsize(a, 36) == 0) {
            goto lab_0x8000e524;
        } else {
            // 0x8000e484
            badassert("r==0", "../../test/arraytest.c", 87, "testa");
            v49 = &g41;
            v46 = (int32_t)"r==0";
            v47 = (int32_t)"../../test/arraytest.c";
            goto lab_0x8000e4a4;
        }
    }
  lab_0x8000e37c:;
    int32_t v57 = v40; // 0x8000e398
    int32_t v58 = 4 * v37 + v1; // 0x8000e398
    int32_t v59 = v38; // 0x8000e398
    if (v40 >= *v4) {
        // 0x8000e39c
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v57 = &g41;
        v58 = (int32_t)"index < a->num";
        v59 = 107;
    }
    int32_t v60 = v58;
    *(int32_t *)(v60 + 4 * v40) = v60;
    v41 = v57 + 1;
    v39 = v59;
    goto lab_0x8000e3d8;
  lab_0x8000e524:
    // 0x8000e524
    v49 = v50;
    v46 = v3;
    v47 = v48;
    int32_t v61; // 0x8000e0d0
    if (v50 < 36) {
        goto lab_0x8000e4a4;
    } else {
        // 0x8000e530
        array_remove(a, 1);
        int32_t v62 = 1;
        int32_t v63 = v62; // 0x8000e554
        int32_t v64 = v3; // 0x8000e554
        int32_t v65 = 71; // 0x8000e554
        if (v62 >= *v4) {
            // 0x8000e558
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v63 = &g41;
            v64 = (int32_t)"index < a->num";
            v65 = (int32_t)"../../include/array.h";
        }
        int32_t v66 = *(int32_t *)*(int32_t *)(v64 + 4 * v62); // 0x8000e58c
        int32_t v67 = v63 + 1; // 0x8000e598
        int32_t v68 = v66; // 0x8000e598
        int32_t v69 = v65; // 0x8000e598
        if (v66 != v65 - v63) {
            // 0x8000e59c
            badassert("*p == TESTSIZE-i-2", "../../test/arraytest.c", 98, "testa");
            v67 = &g41;
            v68 = (int32_t)"*p == TESTSIZE-i-2";
            v69 = (int32_t)"../../test/arraytest.c";
        }
        // 0x8000e5bc
        v62 = v67;
        int32_t v70 = *v4;
        while (v62 < 35) {
            // 0x8000e544
            v63 = v62;
            v64 = v68;
            v65 = v69;
            if (v62 >= v70) {
                // 0x8000e558
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v63 = &g41;
                v64 = (int32_t)"index < a->num";
                v65 = (int32_t)"../../include/array.h";
            }
            // 0x8000e578
            v66 = *(int32_t *)*(int32_t *)(v64 + 4 * v62);
            v67 = v63 + 1;
            v68 = v66;
            v69 = v65;
            if (v66 != v65 - v63) {
                // 0x8000e59c
                badassert("*p == TESTSIZE-i-2", "../../test/arraytest.c", 98, "testa");
                v67 = &g41;
                v68 = (int32_t)"*p == TESTSIZE-i-2";
                v69 = (int32_t)"../../test/arraytest.c";
            }
            // 0x8000e5bc
            v62 = v67;
            v70 = *v4;
        }
        if (v70 == 0) {
            // 0x8000e5d8
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v61 = v3;
            if ((int32_t)"index < a->num" == 72) {
                goto lab_0x8000e634;
            } else {
                // 0x8000e614
                badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 101, "testa");
                v61 = (int32_t)"*p == TESTSIZE-1";
                goto lab_0x8000e634;
            }
        } else {
            // 0x8000e614
            badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 101, "testa");
            v61 = (int32_t)"*p == TESTSIZE-1";
            goto lab_0x8000e634;
        }
    }
  lab_0x8000e4a4:;
    int32_t v71 = v49; // 0x8000e4b4
    int32_t v72 = v46; // 0x8000e4b4
    int32_t v73 = v47; // 0x8000e4b4
    if (v49 >= *v4) {
        // 0x8000e4b8
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v71 = &g41;
        v72 = (int32_t)"index < a->num";
        v73 = (int32_t)"../../include/array.h";
    }
    int32_t v74 = *(int32_t *)(v72 + 4 * v49); // 0x8000e4e4
    int32_t v75 = v71; // 0x8000e4f8
    int32_t v76 = v73; // 0x8000e4f8
    if (*(int32_t *)v74 != v73 - v71) {
        // 0x8000e4fc
        badassert("*p == TESTSIZE-i-1", "../../test/arraytest.c", 91, "testa");
        v75 = &g41;
        v76 = (int32_t)"../../test/arraytest.c";
    }
    // 0x8000e518
    v50 = v75 + 1;
    v48 = v76;
    goto lab_0x8000e524;
  lab_0x8000e634:
    // 0x8000e634
    array_setsize((int32_t *)v61, 2);
    int32_t v77 = *v4; // 0x8000e63c
    bool v78; // 0x8000e0d0
    bool v79; // 0x8000e0d0
    if (v77 == 0) {
        // 0x8000e668
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v78 = true;
        v79 = true;
        if ((int32_t)"index < a->num" == 72) {
            goto lab_0x8000e6b0;
        } else {
            goto lab_0x8000e6a8;
        }
    } else {
        // 0x8000e668
        v78 = v77 == 1;
        goto lab_0x8000e6a8;
    }
  lab_0x8000e6b0:
    // 0x8000e6b0
    badassert("index < a->num", "../../include/array.h", 100, "array_get");
    if (*(int32_t *)*(int32_t *)((int32_t)&g41 + 4) != 70) {
        // 0x8000e6e4
        badassert("*p == TESTSIZE-3", "../../test/arraytest.c", 107, "testa");
    }
    int32_t v80 = v3; // 0x8000e708
    if (v79) {
        // 0x8000e70c
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v80 = (int32_t)"index < a->num";
    }
    // 0x8000e72c
    *(int32_t *)((int32_t)&g41 + 4) = 0;
    array_setsize((int32_t *)v80, 2);
    int32_t v81 = *v4; // 0x8000e738
    if (v81 == 0) {
        // 0x8000e764
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        if ((int32_t)"index < a->num" != 72) {
            // 0x8000e7a4
            badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 112, "testa");
        }
        // 0x8000e7ac
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        goto lab_0x8000e7c8;
    } else {
        // 0x8000e7a4
        badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 112, "testa");
        if (v81 != 1) {
            goto lab_0x8000e7c8;
        } else {
            // 0x8000e7ac
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            goto lab_0x8000e7c8;
        }
    }
  lab_0x8000e6a8:
    // 0x8000e6a8
    badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 105, "testa");
    v79 = v78;
    goto lab_0x8000e6b0;
  lab_0x8000e7c8:;
    int32_t v82 = v3; // 0x8000e7d4
    if (*(int32_t *)((int32_t)&g41 + 4) != 0) {
        // 0x8000e7d8
        badassert("p==NULL", "../../test/arraytest.c", 114, "testa");
        v82 = (int32_t)"p==NULL";
    }
    // 0x8000e7f8
    array_setsize((int32_t *)v82, 730);
    int32_t v83 = *v4; // 0x8000e800
    if (v83 == 0) {
        // 0x8000e82c
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        if ((int32_t)"index < a->num" != 72) {
            // 0x8000e86c
            badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 118, "testa");
        }
        // 0x8000e874
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        goto lab_0x8000e890;
    } else {
        // 0x8000e86c
        badassert("*p == TESTSIZE-1", "../../test/arraytest.c", 118, "testa");
        if (v83 != 1) {
            goto lab_0x8000e890;
        } else {
            // 0x8000e874
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            goto lab_0x8000e890;
        }
    }
  lab_0x8000e890:
    // 0x8000e890
    if (*(int32_t *)((int32_t)&g41 + 4) != 0) {
        // 0x8000e8a0
        badassert("p==NULL", "../../test/arraytest.c", 120, "testa");
    }
}