void testa(array *a)

{
  int iVar1;
  uint uVar2;
  char *pcVar3;
  int **ppiVar4;
  int *piVar5;
  int iVar6;
  char *pcVar7;
  uint uVar8;
  void **unaff_s2;
  void *pvVar9;
  int testarray [73];
  
  for (iVar1 = 0; piVar5 = (int *)(iVar1 * 4), iVar1 < 0x49; iVar1 = iVar1 + 1) {
    testarray[iVar1] = iVar1;
  }
  uVar2 = a->num;
  uVar8 = 0;
  if (uVar2 == 0) goto LAB_8000e1cc;
  badassert("n==0","../../test/arraytest.c",0x33,"testa");
  do {
    piVar5 = testarray;
    pvVar9 = (void *)((int)piVar5 + uVar2);
    uVar2 = a->num;
    iVar1 = array_setsize(a,uVar2 + 1);
    unaff_s2 = (void **)(uVar2 * 4);
    if (iVar1 == 0) {
      unaff_s2 = a->v + uVar2;
      *unaff_s2 = pvVar9;
      iVar1 = 0;
    }
    if (iVar1 != 0) {
      badassert("r==0","../../test/arraytest.c",0x37,"testa");
    }
    uVar8 = uVar8 + 1;
    if (uVar8 != a->num) {
      badassert("n==i+1","../../test/arraytest.c",0x39,"testa");
    }
LAB_8000e1cc:
    uVar2 = uVar8 << 2;
  } while ((int)uVar8 < 0x49);
  pcVar7 = (char *)a->num;
  pcVar3 = (char *)0x0;
  if (pcVar7 == (char *)0x49) goto LAB_8000e270;
  pcVar7 = "../../test/arraytest.c";
  badassert("n==TESTSIZE","../../test/arraytest.c",0x3c,"testa");
  do {
    iVar1 = (int)pcVar3 << 2;
    if (piVar5 == (int *)0x0) {
      pcVar7 = "../../include/array.h";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (**(char ***)((int)a->v + iVar1) != pcVar3) {
      pcVar7 = "../../test/arraytest.c";
      badassert("*p == i","../../test/arraytest.c",0x40,"testa");
    }
    pcVar3 = pcVar3 + 1;
LAB_8000e270:
    piVar5 = (int *)(uint)(pcVar3 < pcVar7);
  } while ((int)pcVar3 < 0x49);
  iVar1 = 0;
  if (pcVar7 != (char *)0x49) {
    badassert("n==TESTSIZE","../../test/arraytest.c",0x43,"testa");
    goto LAB_8000e2a8;
  }
  unaff_s2 = (void **)0x49;
  for (; iVar1 < 0x124; iVar1 = iVar1 + 1) {
LAB_8000e2a8:
    uVar2 = random();
    pcVar3 = (char *)(uVar2 % (uint)unaff_s2);
    if (unaff_s2 == (void **)0x0) {
      trap(0x1c00);
    }
    pcVar7 = pcVar3;
    if ((char *)a->num <= pcVar3) {
      pcVar7 = "index < a->num";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
                    /* WARNING: Load size is inaccurate */
    if (*a->v[(int)pcVar3] != pcVar7) {
      badassert("*p == i","../../test/arraytest.c",0x48,"testa");
    }
  }
  uVar2 = 0;
  if (a->num != 0x49) {
    pcVar3 = "n==TESTSIZE";
    iVar1 = 0x4b;
    badassert("n==TESTSIZE","../../test/arraytest.c",0x4b,"testa");
    goto LAB_8000e37c;
  }
  iVar1 = 0x48;
  for (; pcVar3 = (char *)(iVar1 - uVar2), (int)uVar2 < 0x49; uVar2 = uVar2 + 1) {
LAB_8000e37c:
    pcVar3 = (char *)(testarray + (int)pcVar3);
    iVar6 = uVar2 << 2;
    if (a->num <= uVar2) {
      pcVar3 = "index < a->num";
      iVar1 = 0x6b;
      badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    }
    *(char **)((int)a->v + iVar6) = pcVar3;
  }
  uVar2 = 0;
  pcVar3 = (char *)0x48;
  while ((int)uVar2 < 0x49) {
    iVar1 = uVar2 << 2;
    if (a->num <= uVar2) {
      pcVar3 = "../../include/array.h";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    iVar6 = (int)pcVar3 - uVar2;
    uVar2 = uVar2 + 1;
    if (**(int **)((int)a->v + iVar1) != iVar6) {
      pcVar3 = "../../test/arraytest.c";
      badassert("*p == TESTSIZE-i-1","../../test/arraytest.c",0x53,"testa");
    }
  }
  iVar1 = array_setsize(a,0x24);
  uVar2 = 0;
  if (iVar1 == 0) {
    pcVar3 = (char *)0x48;
    for (; (int)uVar2 < 0x24; uVar2 = uVar2 + 1) {
LAB_8000e4a4:
      iVar1 = uVar2 << 2;
      if (a->num <= uVar2) {
        pcVar3 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      if (**(int **)((int)a->v + iVar1) != (int)pcVar3 - uVar2) {
        pcVar3 = "../../test/arraytest.c";
        badassert("*p == TESTSIZE-i-1","../../test/arraytest.c",0x5b,"testa");
      }
    }
    array_remove(a,1);
    uVar2 = 1;
    pcVar3 = (char *)0x47;
    while ((int)uVar2 < 0x23) {
      iVar1 = uVar2 << 2;
      if (a->num <= uVar2) {
        pcVar3 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      iVar6 = (int)pcVar3 - uVar2;
      uVar2 = uVar2 + 1;
      if (**(int **)((int)a->v + iVar1) != iVar6) {
        pcVar3 = "../../test/arraytest.c";
        badassert("*p == TESTSIZE-i-2","../../test/arraytest.c",0x62,"testa");
      }
    }
    if (a->num == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
                    /* WARNING: Load size is inaccurate */
    pcVar3 = (char *)a;
    if (**a->v != 0x48) {
      pcVar3 = "*p == TESTSIZE-1";
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x65,"testa");
    }
    array_setsize((array *)pcVar3,2);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    pcVar3 = (char *)(uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      pcVar3 = "*p == TESTSIZE-1";
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x69,"testa");
    }
    if (pcVar3 != (char *)0x0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    uVar2 = (uint)(uVar2 < 2);
    if (*ppiVar4[1] != 0x46) {
      badassert("*p == TESTSIZE-3","../../test/arraytest.c",0x6b,"testa");
    }
    pcVar3 = (char *)a;
    if (uVar2 != 0) {
      pcVar3 = "index < a->num";
      badassert("index < a->num","../../include/array.h",0x6b,"array_set");
    }
    ppiVar4[1] = (int *)0x0;
    array_setsize((array *)pcVar3,2);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    uVar2 = (uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x70,"testa");
    }
    if (uVar2 != 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar3 = (char *)a;
    if (ppiVar4[1] != (int *)0x0) {
      pcVar3 = "p==NULL";
      badassert("p==NULL","../../test/arraytest.c",0x72,"testa");
    }
    array_setsize((array *)pcVar3,0x2da);
    uVar2 = a->num;
    if (uVar2 == 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppiVar4 = (int **)a->v;
    uVar2 = (uint)(uVar2 < 2);
    if (**ppiVar4 != 0x48) {
      badassert("*p == TESTSIZE-1","../../test/arraytest.c",0x76,"testa");
    }
    if (uVar2 != 0) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    if (ppiVar4[1] != (int *)0x0) {
      badassert("p==NULL","../../test/arraytest.c",0x78,"testa");
    }
    return;
  }
  pcVar3 = "../../test/arraytest.c";
  badassert("r==0","../../test/arraytest.c",0x57,"testa");
  goto LAB_8000e4a4;
}