testa(struct array *a)
{
	int testarray[TESTSIZE];
	int i, j, n, r, *p;

	for (i=0; i<TESTSIZE; i++) {
		testarray[i]=i;
	}

	n = array_num(a);
	KASSERT(n==0);

	for (i=0; i<TESTSIZE; i++) {
		r = array_add(a, &testarray[i], NULL);
		KASSERT(r==0);
		n = array_num(a);
		KASSERT(n==i+1);
	}
	n = array_num(a);
	KASSERT(n==TESTSIZE);

	for (i=0; i<TESTSIZE; i++) {
		p = array_get(a, i);
		KASSERT(*p == i);
	}
	n = array_num(a);
	KASSERT(n==TESTSIZE);

	for (j=0; j<TESTSIZE*4; j++) {
		i = random()%TESTSIZE;
		p = array_get(a, i);
		KASSERT(*p == i);
	}
	n = array_num(a);
	KASSERT(n==TESTSIZE);

	for (i=0; i<TESTSIZE; i++) {
		array_set(a, i, &testarray[TESTSIZE-i-1]);
	}

	for (i=0; i<TESTSIZE; i++) {
		p = array_get(a, i);
		KASSERT(*p == TESTSIZE-i-1);
	}

	r = array_setsize(a, TESTSIZE/2);
	KASSERT(r==0);

	for (i=0; i<TESTSIZE/2; i++) {
		p = array_get(a, i);
		KASSERT(*p == TESTSIZE-i-1);
	}

	array_remove(a, 1);

	for (i=1; i<TESTSIZE/2 - 1; i++) {
		p = array_get(a, i);
		KASSERT(*p == TESTSIZE-i-2);
	}
	p = array_get(a, 0);
	KASSERT(*p == TESTSIZE-1);

	array_setsize(a, 2);
	p = array_get(a, 0);
	KASSERT(*p == TESTSIZE-1);
	p = array_get(a, 1);
	KASSERT(*p == TESTSIZE-3);

	array_set(a, 1, NULL);
	array_setsize(a, 2);
	p = array_get(a, 0);
	KASSERT(*p == TESTSIZE-1);
	p = array_get(a, 1);
	KASSERT(p==NULL);

	array_setsize(a, TESTSIZE*10);
	p = array_get(a, 0);
	KASSERT(*p == TESTSIZE-1);
	p = array_get(a, 1);
	KASSERT(p==NULL);
}