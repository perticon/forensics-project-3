gettime(struct timespec *ts)
{
	KASSERT(the_clock!=NULL);
	the_clock->rtc_gettime(the_clock->rtc_devdata, ts);
}