void gettime(timespec *ts)

{
  rtclock_softc *prVar1;
  char *pcVar2;
  
  prVar1 = the_clock;
  if (the_clock == (rtclock_softc *)0x0) {
    pcVar2 = "../../dev/generic/rtclock.c";
    badassert("the_clock!=NULL","../../dev/generic/rtclock.c",0x44,"gettime");
    ts = (timespec *)pcVar2;
  }
  (*prVar1->rtc_gettime)(prVar1->rtc_devdata,ts);
  return;
}