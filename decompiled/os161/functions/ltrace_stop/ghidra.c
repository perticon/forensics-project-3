void ltrace_stop(uint32_t code)

{
  if ((the_trace != (ltrace_softc *)0x0) && (the_trace->lt_canstop != false)) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0x10,code);
  }
  return;
}