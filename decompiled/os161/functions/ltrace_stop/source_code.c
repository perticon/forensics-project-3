ltrace_stop(uint32_t code)
{
	if (the_trace != NULL && the_trace->lt_canstop) {
		bus_write_register(the_trace->lt_busdata, the_trace->lt_buspos,
				   LTRACE_REG_STOP, code);
	}
}