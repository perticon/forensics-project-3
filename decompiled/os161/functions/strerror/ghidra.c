char * strerror(int errcode)

{
  if ((uint)errcode < 0x41) {
    return sys_errlist[errcode];
  }
                    /* WARNING: Subroutine does not return */
  panic("Invalid error code %d\n",errcode);
}