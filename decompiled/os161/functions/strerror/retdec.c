char * strerror(uint32_t errcode) {
    if (errcode < 65) {
        // 0x8000b670
        return (char *)*(int32_t *)(4 * errcode + (int32_t)&sys_errlist);
    }
    // 0x8000b68c
    panic("Invalid error code %d\n", errcode);
    return (char *)&g41;
}