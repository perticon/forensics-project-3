proc * proc_search_pid(pid_t pid)

{
  proc *ppVar1;
  
  if (99 < (uint)pid) {
    badassert("pid >= 0 && pid < MAX_PROC","../../proc/proc.c",0x1e3,"proc_search_pid");
  }
  spinlock_acquire(&processTable.lk);
  ppVar1 = processTable.proc[pid];
  spinlock_release(&processTable.lk);
  if (ppVar1->p_pid != pid) {
    badassert("p->p_pid == pid","../../proc/proc.c",0x1e7,"proc_search_pid");
  }
  return ppVar1;
}