int32_t * proc_search_pid(uint32_t pid) {
    if (pid >= 100) {
        // 0x8000d1a4
        badassert("pid >= 0 && pid < MAX_PROC", "../../proc/proc.c", 483, "proc_search_pid");
    }
    // 0x8000d1c4
    spinlock_acquire(&g29);
    int32_t v1 = *(int32_t *)(4 * pid + (int32_t)&processTable + 4); // 0x8000d1e0
    spinlock_release(&g29);
    int32_t v2 = v1; // 0x8000d1f8
    if (*(int32_t *)(v1 + 28) != pid) {
        // 0x8000d1fc
        badassert("p->p_pid == pid", "../../proc/proc.c", 487, "proc_search_pid");
        v2 = &g41;
    }
    // 0x8000d21c
    return (int32_t *)v2;
}