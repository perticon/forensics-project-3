int32_t sfs_rwblock(int32_t * sfs, int32_t * uio) {
    bool v1 = vfs_biglock_do_i_hold(); // 0x80008a8c
    int32_t v2 = (int32_t)&g35; // 0x80008a94
    if (!v1) {
        // 0x80008a98
        badassert("vfs_biglock_do_i_hold()", "../../fs/sfs/sfs_io.c", 65, "sfs_rwblock");
        v2 = &g41;
    }
    int32_t v3 = (int32_t)uio;
    int32_t * v4; // 0x80008a60
    if ((*(int32_t *)(v2 - 0x64c0) & 512) == 0) {
        // 0x80008ab8
        v4 = (int32_t *)(v3 + 8);
    } else {
        int32_t v5 = *(int32_t *)(v3 + 24); // 0x80008acc
        int32_t * v6 = (int32_t *)(v3 + 8);
        uint32_t v7 = *v6; // 0x80008aec
        int32_t v8 = v7; // 0x80008afc
        if (v7 <= 0xffffffff) {
            // 0x80008b00
            v8 = v7 + (int32_t)(*(int32_t *)(v3 + 12) > 0xfffffe00);
        }
        int32_t v9 = v5 == 0 ? (int32_t)"read" : (int32_t)"write";
        kprintf("sfs: %s %llu\n", (char *)v9, (int64_t)(v8 >> 9));
        v4 = v6;
    }
    int32_t v10 = (int32_t)sfs;
    int32_t * v11 = (int32_t *)(v3 + 12);
    char * v12 = (char *)(v10 + 16);
    int32_t v13 = 0; // 0x80008b38
    int32_t result; // 0x80008b4c
    while (true) {
      lab_0x80008b3c:
        // 0x80008b3c
        result = *(int32_t *)(*(int32_t *)*(int32_t *)(v10 + 524) + 4);
        switch (result) {
            case 8: {
                // 0x80008b64
                panic("sfs: %s: DEVOP_IO returned EINVAL\n", v12);
                // 0x80008c24
                return result;
            }
            case 32: {
                int32_t v14 = v13;
                if (v14 == 0) {
                    uint32_t v15 = *v4; // 0x80008b8c
                    int32_t v16 = v15; // 0x80008b9c
                    if (v15 <= 0xffffffff) {
                        // 0x80008ba0
                        v16 = v15 + (int32_t)(*v11 > 0xfffffe00);
                    }
                    // 0x80008bac
                    kprintf("sfs: %s: block %llu I/O error, retrying\n", v12, (int64_t)(v16 >> 9));
                    v13 = 1;
                } else {
                    if (v14 >= 10) {
                        int32_t v17 = *v4; // 0x80008be0
                        if (v17 > -1) {
                            // 0x80008c00
                            kprintf("sfs: %s: block %llu I/O error, giving up after %d retries\n", v12, (int64_t)(v17 >> 9), v14);
                            return result;
                        }
                        int32_t v18 = v17 + (int32_t)(*v11 > 0xfffffe00);
                        kprintf("sfs: %s: block %llu I/O error, giving up after %d retries\n", v12, (int64_t)(v18 >> 9), v14);
                        return result;
                    }
                    // 0x80008bd8
                    v13 = v14 + 1;
                }
                // 0x80008b3c
                goto lab_0x80008b3c;
            }
            default: {
                return result;
            }
        }
    }
  lab_0x80008c24:
    // 0x80008c24
    return result;
}