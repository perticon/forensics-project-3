sfs_rwblock(struct sfs_fs *sfs, struct uio *uio)
{
	int result;
	int tries=0;

	KASSERT(vfs_biglock_do_i_hold());

	DEBUG(DB_SFS, "sfs: %s %llu\n",
	      uio->uio_rw == UIO_READ ? "read" : "write",
	      uio->uio_offset / SFS_BLOCKSIZE);

 retry:
	result = DEVOP_IO(sfs->sfs_device, uio);
	if (result == EINVAL) {
		/*
		 * This means the sector we requested was out of range,
		 * or the seek address we gave wasn't sector-aligned,
		 * or a couple of other things that are our fault.
		 */
		panic("sfs: %s: DEVOP_IO returned EINVAL\n",
		      sfs->sfs_sb.sb_volname);
	}
	if (result == EIO) {
		if (tries == 0) {
			tries++;
			kprintf("sfs: %s: block %llu I/O error, retrying\n",
				sfs->sfs_sb.sb_volname,
				uio->uio_offset / SFS_BLOCKSIZE);
			goto retry;
		}
		else if (tries < 10) {
			tries++;
			goto retry;
		}
		else {
			kprintf("sfs: %s: block %llu I/O error, giving up "
				"after %d retries\n",
				sfs->sfs_sb.sb_volname,
				uio->uio_offset / SFS_BLOCKSIZE, tries);
		}
	}
	return result;
}