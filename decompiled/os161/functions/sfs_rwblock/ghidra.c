int sfs_rwblock(sfs_fs *sfs,uio *uio)

{
  char *pcVar1;
  undefined3 extraout_var;
  bool bVar5;
  int iVar2;
  uint uVar3;
  int iVar4;
  uint uVar6;
  
  bVar5 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0x41,"sfs_rwblock");
  }
  if ((*(uint *)(iVar2 + -0x64c0) & 0x200) != 0) {
    if (uio->uio_rw == UIO_READ) {
      pcVar1 = "console read";
    }
    else {
      pcVar1 = s_console_write_80022690;
    }
    iVar2 = *(int *)&uio->uio_offset;
    uVar3 = *(uint *)((int)&uio->uio_offset + 4);
    uVar6 = uVar3;
    if (iVar2 < 0) {
      uVar6 = uVar3 + 0x1ff;
      iVar2 = (uint)(uVar6 < uVar3) + iVar2;
    }
    kprintf("sfs: %s %llu\n",pcVar1 + 8,iVar2 >> 9,iVar2 << 0x17 | uVar6 >> 9);
  }
  iVar2 = 0;
  while( true ) {
    iVar4 = (*sfs->sfs_device->d_ops->devop_io)(sfs->sfs_device,uio);
    if (iVar4 == 8) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: DEVOP_IO returned EINVAL\n",(sfs->sfs_sb).sb_volname);
    }
    if (iVar4 != 0x20) break;
    if (iVar2 == 0) {
      iVar2 = 1;
      iVar4 = *(int *)&uio->uio_offset;
      uVar3 = *(uint *)((int)&uio->uio_offset + 4);
      uVar6 = uVar3;
      if (iVar4 < 0) {
        uVar6 = uVar3 + 0x1ff;
        iVar4 = (uint)(uVar6 < uVar3) + iVar4;
      }
      kprintf("sfs: %s: block %llu I/O error, retrying\n",(sfs->sfs_sb).sb_volname,iVar4 >> 9,
              iVar4 << 0x17 | uVar6 >> 9);
    }
    else {
      if (9 < iVar2) {
        iVar4 = *(int *)&uio->uio_offset;
        uVar3 = *(uint *)((int)&uio->uio_offset + 4);
        uVar6 = uVar3;
        if (iVar4 < 0) {
          uVar6 = uVar3 + 0x1ff;
          iVar4 = (uint)(uVar6 < uVar3) + iVar4;
        }
        kprintf("sfs: %s: block %llu I/O error, giving up after %d retries\n",
                (sfs->sfs_sb).sb_volname,iVar4 >> 9,iVar4 << 0x17 | uVar6 >> 9,iVar2);
        return 0x20;
      }
      iVar2 = iVar2 + 1;
    }
  }
  return iVar4;
}