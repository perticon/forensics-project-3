int create_ipt(void)

{
  paddr_t pVar1;
  int iVar2;
  char *maxN;
  
  pVar1 = ram_getsize();
  if ((int)pVar1 < 0) {
    pVar1 = pVar1 + 0xfff;
  }
  nRamFrames = (int)pVar1 >> 0xc;
  maxN = (char *)nRamFrames;
  if ((char *)nRamFrames == (char *)0x0) {
    maxN = "nRamFrames != 0";
    badassert("nRamFrames != 0","../../vm/pt.c",0x72,"create_ipt");
  }
  ipt_hash = STinit((int)maxN);
  ipt = (ipt_entry *)kmalloc(nRamFrames << 3);
  if (ipt == (ipt_entry *)0x0) {
    iVar2 = -1;
  }
  else if (ipt_hash == (ST)0x0) {
    iVar2 = -1;
  }
  else {
    spinlock_acquire(&ipt_lock);
    for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
      ipt[iVar2].pid = -1;
    }
    ipt_active = 1;
    spinlock_release(&ipt_lock);
    iVar2 = 0;
  }
  return iVar2;
}