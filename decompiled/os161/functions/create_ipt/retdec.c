int32_t create_ipt(void) {
    int32_t v1 = ram_getsize(); // 0x8001cc20
    int32_t v2 = (v1 > -1 ? v1 : v1 + 4095) >> 12; // 0x8001cc30
    nRamFrames = v2;
    int32_t v3 = v2; // 0x8001cc3c
    if (v2 == 0) {
        // 0x8001cc40
        badassert("nRamFrames != 0", "../../vm/pt.c", 114, "create_ipt");
        v3 = (int32_t)"nRamFrames != 0";
    }
    int32_t * v4 = STinit(v3); // 0x8001cc64
    *(int32_t *)&ipt_hash = (int32_t)v4;
    char * v5 = kmalloc(8 * nRamFrames); // 0x8001cc7c
    *(int32_t *)&ipt = (int32_t)v5;
    if (v5 == NULL || ipt_hash == NULL) {
        // 0x8001cd10
        return -1;
    }
    // 0x8001cca0
    spinlock_acquire(&ipt_lock);
    int32_t v6 = 0; // 0x8001cce0
    if (nRamFrames > 0) {
        *(int32_t *)(8 * v6 + (int32_t)ipt) = -1;
        v6++;
        while (v6 < nRamFrames) {
            // 0x8001ccbc
            *(int32_t *)(8 * v6 + (int32_t)ipt) = -1;
            v6++;
        }
    }
    // 0x8001cce4
    ipt_active = 1;
    spinlock_release(&ipt_lock);
    // 0x8001cd10
    return 0;
}