int create_ipt(void)
{
    int i;
    nRamFrames = ((int)ram_getsize()) / PAGE_SIZE;
    KASSERT(nRamFrames != 0);
    ipt_hash = STinit(nRamFrames);

    ipt = kmalloc(sizeof(struct ipt_entry) * nRamFrames);

    if (ipt == NULL || ipt_hash == NULL)
    {
        return -1;
    }
    spinlock_acquire(&ipt_lock);
    for (i = 0; i < nRamFrames; i++)
    {
        ipt[i].pid = -1;
    }
    ipt_active = 1;
    spinlock_release(&ipt_lock);

    return 0;
}