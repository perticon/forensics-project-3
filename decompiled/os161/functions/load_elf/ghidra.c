int load_elf(vnode *v,vaddr_t *entrypoint)

{
  addrspace *as;
  int iVar1;
  Elf_Ehdr *pEVar2;
  int iVar3;
  undefined4 *puVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  int unaff_s7;
  Elf_Ehdr eh;
  Elf_Phdr ph;
  iovec iov;
  uio ku;
  
  as = proc_getas();
  uio_kinit(&iov,&ku,&eh,0x34,0,UIO_READ);
  vnode_check(v,"read");
  iVar1 = (*v->vn_ops->vop_read)(v,&ku);
  if (iVar1 == 0) {
    if (ku.uio_resid == 0) {
      iVar1 = 0xd;
      if ((((eh.e_ident._0_4_ == 0x7f454c46) &&
           (iVar1 = 0xd, (eh.e_ident._4_4_ & 0xffffff00) == 0x1020100)) &&
          (iVar1 = 0xd, eh.e_version == 1)) && (iVar1 = 0xd, eh._16_4_ == 0x20008)) {
        pEVar2 = &eh;
        puVar4 = (undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x2c);
        do {
          uVar7 = *(undefined4 *)(pEVar2->e_ident + 4);
          uVar6 = *(undefined4 *)(pEVar2->e_ident + 8);
          uVar5 = *(undefined4 *)(pEVar2->e_ident + 0xc);
          *puVar4 = *(undefined4 *)pEVar2->e_ident;
          puVar4[1] = uVar7;
          puVar4[2] = uVar6;
          puVar4[3] = uVar5;
          pEVar2 = (Elf_Ehdr *)&pEVar2->e_type;
          puVar4 = puVar4 + 4;
        } while (pEVar2 != (Elf_Ehdr *)&eh.e_shnum);
        *puVar4 = *(undefined4 *)pEVar2;
        iVar1 = 0;
        do {
          while( true ) {
            if ((int)(uint)eh.e_phnum <= iVar1) {
              *entrypoint = eh.e_entry;
              return 0;
            }
            uio_kinit(&iov,&ku,&ph,0x20,(ulonglong)(eh.e_phoff + (uint)eh.e_phentsize * iVar1),
                      UIO_READ);
            vnode_check(v,"read");
            iVar3 = (*v->vn_ops->vop_read)(v,&ku);
            if (iVar3 != 0) {
              return iVar3;
            }
            if (ku.uio_resid != 0) {
              kprintf("ELF: short read on phdr - file truncated?\n");
              return 0xd;
            }
            if (ph.p_type != 1) break;
            iVar3 = as_define_region(as,ph.p_vaddr,ph.p_memsz,ph.p_flags & 4,ph.p_flags & 2,
                                     ph.p_flags & 1);
            if (iVar3 != 0) {
              return iVar3;
            }
LAB_8000dc40:
            iVar1 = iVar1 + 1;
          }
          if ((ph.p_type == 0) || (ph.p_type == 6)) goto LAB_8000dc40;
          iVar1 = iVar1 + 1;
        } while (ph.p_type == 0x70000000);
        kprintf("loadelf: unknown segment type %d\n");
        iVar1 = 0xd;
      }
    }
    else {
      kprintf("ELF: short read on header - file truncated?\n");
      iVar1 = 0xd;
    }
  }
  return iVar1;
}