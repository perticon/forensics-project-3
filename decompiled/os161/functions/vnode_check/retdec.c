void vnode_check(int32_t * v2, char * opstr) {
    int32_t v1 = (int32_t)v2; // 0x8001a7f0
    if (v2 == NULL) {
        // 0x8001a7f4
        panic("vnode_check: vop_%s: null vnode\n", opstr);
        v1 = (int32_t)"vnode_check: vop_%s: null vnode\n";
    }
    int32_t v2_ = v1; // 0x8001a80c
    if (v1 == -0x21524111) {
        // 0x8001a810
        panic("vnode_check: vop_%s: deadbeef vnode\n", opstr);
        v2_ = (int32_t)"vnode_check: vop_%s: deadbeef vnode\n";
    }
    int32_t v3 = *(int32_t *)(v2_ + 20); // 0x8001a81c
    int32_t v4 = v3; // 0x8001a828
    int32_t v5 = v2_; // 0x8001a828
    if (v3 == 0) {
        // 0x8001a82c
        panic("vnode_check: vop_%s: null ops pointer\n", opstr);
        v4 = &g41;
        v5 = (int32_t)"vnode_check: vop_%s: null ops pointer\n";
    }
    int32_t v6 = v4; // 0x8001a840
    int32_t v7 = v5; // 0x8001a840
    if (v4 == -0x21524111) {
        // 0x8001a844
        panic("vnode_check: vop_%s: deadbeef ops pointer\n", opstr);
        v6 = &g41;
        v7 = (int32_t)"vnode_check: vop_%s: deadbeef ops pointer\n";
    }
    int32_t v8 = *(int32_t *)v6; // 0x8001a850
    int32_t v9 = -0x21524111; // 0x8001a860
    int32_t v10 = v7; // 0x8001a860
    if (v8 != -0x5d4c3b2b) {
        // 0x8001a864
        panic("vnode_check: vop_%s: ops with bad magic number %lx\n", opstr, v8);
        v9 = (int32_t)&g41 | 0xbeef;
        v10 = (int32_t)"vnode_check: vop_%s: ops with bad magic number %lx\n";
    }
    // 0x8001a870
    if (*(int32_t *)(v10 + 12) == v9) {
        // 0x8001a880
        panic("vnode_check: vop_%s: deadbeef fs pointer\n", opstr);
    }
    uint32_t v11 = v10 + 4; // 0x8001a87c
    int32_t * v12 = (int32_t *)v11; // 0x8001a890
    spinlock_acquire(v12);
    if (v11 <= 0xffffffff) {
        // 0x8001a8c8
        panic("vnode_check: vop_%s: negative refcount %d\n", opstr, v11);
        // 0x8001a8e4
        spinlock_release(v12);
        return;
    }
    int32_t v13 = 0x100001; // 0x8001a8b4
    if (v11 == 0) {
        // 0x8001a8b8
        panic("vnode_check: vop_%s: zero refcount\n", opstr);
        v13 = (int32_t)&g41 + 1;
    }
    // 0x8001a8c8
    if (v11 >= v13) {
        // 0x8001a8d8
        kprintf("vnode_check: vop_%s: warning: large refcount %d\n", opstr, v11);
    }
    // 0x8001a8e4
    spinlock_release(v12);
}