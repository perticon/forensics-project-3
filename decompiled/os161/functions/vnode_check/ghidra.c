void vnode_check(vnode *v,char *opstr)

{
  vnode_ops *pvVar1;
  int iVar2;
  
  if (v == (vnode *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: null vnode\n");
  }
  if (v == (vnode *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef vnode\n");
  }
  pvVar1 = v->vn_ops;
  if (pvVar1 == (vnode_ops *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: null ops pointer\n");
  }
  if (pvVar1 == (vnode_ops *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef ops pointer\n");
  }
  if (pvVar1->vop_magic != 0xa2b3c4d5) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: ops with bad magic number %lx\n");
  }
  if (v->vn_fs == (fs *)0xdeadbeef) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: deadbeef fs pointer\n");
  }
  spinlock_acquire(&v->vn_countlock);
  iVar2 = v->vn_refcount;
  if (iVar2 < 0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: negative refcount %d\n",opstr);
  }
  if (iVar2 == 0) {
                    /* WARNING: Subroutine does not return */
    panic("vnode_check: vop_%s: zero refcount\n",opstr);
  }
  if (0x100000 < iVar2) {
    kprintf("vnode_check: vop_%s: warning: large refcount %d\n",opstr);
  }
  spinlock_release(&v->vn_countlock);
  return;
}