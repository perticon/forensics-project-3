int swap_in(vaddr_t page, paddr_t paddr)
{

    int result, i;
    pid_t pid;
    pid = curproc->p_pid;

    spinlock_acquire(&swap_lock);
    /* page must be in swap file */
    for (i = 0; i < ENTRIES; i++)
    {
        if (swap_table[i].pid == pid && swap_table[i].page == page)
        {
            spinlock_release(&swap_lock);
            result = file_read_paddr(v, paddr, PAGE_SIZE, i * PAGE_SIZE);

            spinlock_acquire(&swap_lock);
            swap_table[i].pid = -1;
#if DUMPIN
            kprintf("Swapping in PID %d PAGE %d\n", curproc->p_pid, page / PAGE_SIZE);
#endif
            spinlock_release(&swap_lock);

            KASSERT(result == PAGE_SIZE);
            increase(SWAP_IN_PAGE);
            increase(FAULT_WITH_LOAD);
            return 0;
        }
    }
    spinlock_release(&swap_lock);
    return 1;
}