int swap_in(vaddr_t page,paddr_t paddr)

{
  int iVar1;
  int iVar2;
  int iVar3;
  int unaff_s7;
  
  iVar3 = *(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c);
  spinlock_acquire(&swap_lock);
  iVar1 = 0;
  do {
    while( true ) {
      iVar2 = iVar1;
      if (0x8ff < iVar2) {
        spinlock_release(&swap_lock);
        return 1;
      }
      if (swap_table[iVar2].pid == iVar3) break;
      iVar1 = iVar2 + 1;
    }
    iVar1 = iVar2 + 1;
  } while (swap_table[iVar2].page != page);
  spinlock_release(&swap_lock);
  iVar1 = file_read_paddr(v,paddr,0x1000,(longlong)(iVar2 << 0xc));
  spinlock_acquire(&swap_lock);
  swap_table[iVar2].pid = -1;
  spinlock_release(&swap_lock);
  if (iVar1 != 0x1000) {
    badassert("result == PAGE_SIZE",s_______vm_swapfile_c_800281fc,0x206,"swap_in");
  }
  increase(8);
  increase(5);
  return 0;
}