int32_t swap_in(int32_t page, int32_t paddr) {
    // 0x8001daa8
    spinlock_acquire(&swap_lock);
    int32_t v1 = 0;
    int32_t result; // 0x8001daa8
    int32_t v2; // 0x8001db14
    while (true) {
        int32_t v3 = v1;
        int32_t v4 = 8 * v3; // 0x8001dbc4
        int32_t v5 = *(int32_t *)(v4 + (int32_t)&swap_table); // 0x8001daf4
        int32_t v6; // 0x8001daa8
        int32_t v7; // 0x8001daa8
        if (v5 == *(int32_t *)(*(int32_t *)(v7 + 84) + 28)) {
            // 0x8001db04
            v2 = v3 + 1;
            v6 = v2;
            if (*(int32_t *)(v4 + (int32_t)&swap_table + 4) == page) {
                // break -> 0x8001db18
                break;
            }
        } else {
            // 0x8001dbb8
            v6 = v3 + 1;
        }
        // 0x8001dbbc
        v1 = v6;
        if (v1 >= 2304) {
            // 0x8001dbc8
            spinlock_release(&swap_lock);
            result = 1;
            return result;
        }
    }
    // 0x8001db18
    spinlock_release(&swap_lock);
    int32_t v8 = file_read_paddr(v, paddr, 0x1000, (int64_t)&g41); // 0x8001db48
    spinlock_acquire(&swap_lock);
    *(int32_t *)(8 * v2 + (int32_t)&swap_table - 8) = -1;
    spinlock_release(&swap_lock);
    if (v8 != 0x1000) {
        // 0x8001db84
        badassert("result == PAGE_SIZE", "../../vm/swapfile.c", 518, "swap_in");
    }
    // 0x8001dba0
    increase(8);
    increase(5);
    result = 0;
  lab_0x8001dbd8:
    // 0x8001dbd8
    return result;
}