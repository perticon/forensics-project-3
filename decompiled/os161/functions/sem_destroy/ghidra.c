void sem_destroy(semaphore *sem)

{
  char *pcVar1;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x51,"sem_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  wchan_destroy(sem->sem_wchan);
  kfree(sem->sem_name);
  kfree(sem);
  return;
}