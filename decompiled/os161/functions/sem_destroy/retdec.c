void sem_destroy(int32_t * sem) {
    if (sem == NULL) {
        // 0x800155ec
        badassert("sem != NULL", "../../thread/synch.c", 81, "sem_destroy");
    }
    // 0x8001560c
    spinlock_cleanup((int32_t *)"ULL");
    int32_t v1 = *(int32_t *)((int32_t)sem + 4); // 0x80015614
    wchan_destroy((int32_t *)v1);
    kfree((char *)v1);
    kfree((char *)sem);
}