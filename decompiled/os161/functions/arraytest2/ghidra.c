int arraytest2(int nargs,char **args)

{
  array *a;
  int iVar1;
  void *pvVar2;
  char *pcVar3;
  void **in_v1;
  int iVar4;
  void *pvVar5;
  char *pcVar6;
  char *pcVar7;
  void **ppvVar8;
  uint unaff_s1;
  uint uVar9;
  uint unaff_s3;
  void *unaff_s4;
  
  kprintf("Beginning large array test...\n");
  a = array_create();
  if (a != (array *)0x0) {
    unaff_s3 = 0;
    unaff_s1 = 0;
    unaff_s4 = (void *)0xc0ffee;
    goto LAB_8000ea54;
  }
  pcVar6 = "a != NULL";
  badassert("a != NULL","../../test/arraytest.c",0xa4,"arraytest2");
  do {
    uVar9 = a->num;
    iVar1 = array_setsize((array *)pcVar6,uVar9 + 1);
    if (iVar1 == 0) {
      in_v1 = a->v;
      in_v1[uVar9] = unaff_s4;
      iVar1 = 0;
      unaff_s3 = uVar9;
    }
    if (iVar1 != 0) {
      badassert("result == 0","../../test/arraytest.c",0xaa,"arraytest2");
    }
    if (unaff_s1 != unaff_s3) {
      badassert("x == i","../../test/arraytest.c",0xab,"arraytest2");
    }
    unaff_s1 = unaff_s1 + 1;
LAB_8000ea54:
    pcVar6 = (char *)a;
  } while (unaff_s1 < 3000);
  pcVar7 = (char *)a->num;
  pcVar6 = (char *)0x0;
  if (pcVar7 != (char *)0xbb8) {
    pcVar7 = "../../test/arraytest.c";
    iVar1 = 0xad;
    badassert("array_num(a) == BIGTESTSIZE","../../test/arraytest.c",0xad,"arraytest2");
    do {
      iVar4 = (int)pcVar6 << 2;
      if (in_v1 == (void **)0x0) {
        pcVar7 = "../../include/array.h";
        iVar1 = 100;
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      if (*(int *)((int)a->v + iVar4) != iVar1) {
        pcVar7 = "../../test/arraytest.c";
        iVar1 = 0xb1;
        badassert("array_get(a, i) == p","../../test/arraytest.c",0xb1,"arraytest2");
      }
      pcVar6 = pcVar6 + 1;
LAB_8000eb00:
      in_v1 = (void **)(uint)(pcVar6 < pcVar7);
    } while (pcVar6 < (char *)0xbb8);
    iVar1 = array_setsize(a,0);
    pcVar6 = (char *)a;
    if (iVar1 != 0) {
      pcVar6 = "result == 0";
      badassert("result == 0","../../test/arraytest.c",0xb6,"arraytest2");
    }
    iVar1 = array_setsize((array *)pcVar6,3000);
    uVar9 = 0;
    if (iVar1 != 0) {
      pcVar6 = "result == 0";
      iVar1 = 0xba;
      badassert("result == 0","../../test/arraytest.c",0xba,"arraytest2");
      do {
        pcVar6 = pcVar6 + iVar1 + uVar9;
        if (a->num <= uVar9) {
          pcVar6 = "index < a->num";
          iVar1 = 0x6b;
          badassert("index < a->num","../../include/array.h",0x6b,"array_set");
        }
        a->v[uVar9] = pcVar6;
        uVar9 = uVar9 + 1;
LAB_8000ebc4:
        pcVar6 = (char *)(uVar9 << 1);
      } while (uVar9 < 3000);
      uVar9 = 0;
      pcVar6 = (char *)0xb007;
      while (uVar9 < 3000) {
        iVar1 = uVar9 << 2;
        if (a->num <= uVar9) {
          pcVar6 = "../../include/array.h";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        iVar4 = uVar9 * 3;
        uVar9 = uVar9 + 1;
        if (pcVar6 + iVar4 != *(char **)((int)a->v + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          badassert("array_get(a, i) == NTH(i)","../../test/arraytest.c",0xc1,"arraytest2");
        }
      }
      array_remove(a,1);
      pcVar6 = (char *)a->num;
      pvVar2 = (void *)0xb007;
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppvVar8 = a->v;
      pvVar5 = *ppvVar8;
      pcVar7 = (char *)0xbb7;
      if (pvVar5 != pvVar2) {
        pcVar6 = "../../test/arraytest.c";
        ppvVar8 = (void **)0xc6;
        badassert("array_get(a, 0) == NTH(0)","../../test/arraytest.c",0xc6,"arraytest2");
      }
      pcVar3 = (char *)0x1;
      if (pcVar6 == pcVar7) goto LAB_8000ed60;
      pcVar6 = "../../test/arraytest.c";
      ppvVar8 = (void **)0xc7;
      pcVar7 = "arraytest2";
      badassert("array_num(a) == BIGTESTSIZE-1","../../test/arraytest.c",199,"arraytest2");
      do {
        iVar1 = (int)pcVar3 << 2;
        if (pvVar5 == (void *)0x0) {
          pcVar6 = "../../include/array.h";
          ppvVar8 = (void **)&DAT_00000064;
          pcVar7 = "array_get";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        pcVar3 = pcVar3 + 1;
        if (pcVar7 + (int)pcVar3 * 3 != *(char **)((int)ppvVar8 + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          ppvVar8 = (void **)0xc9;
          badassert("array_get(a, i) == NTH(i+1)","../../test/arraytest.c",0xc9,"arraytest2");
LAB_8000ed60:
          pcVar7 = (char *)0xb007;
        }
        pvVar5 = (void *)(uint)(pcVar3 < pcVar6);
      } while (pcVar3 < (char *)0xbb7);
      iVar1 = array_setsize(a,6000);
      if (iVar1 != 0) {
        badassert("result == 0","../../test/arraytest.c",0xce,"arraytest2");
      }
      pcVar6 = (char *)a->num;
      pvVar2 = (void *)0xb007;
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "../../include/array.h";
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppvVar8 = a->v;
      pvVar5 = *ppvVar8;
      pcVar7 = (char *)0x1;
      if (pvVar5 == pvVar2) goto LAB_8000ee70;
      pcVar6 = "../../test/arraytest.c";
      ppvVar8 = (void **)0xcf;
      pcVar3 = "arraytest2";
      badassert("array_get(a, 0) == NTH(0)","../../test/arraytest.c",0xcf,"arraytest2");
      do {
        iVar1 = (int)pcVar7 << 2;
        if (pvVar5 == (void *)0x0) {
          pcVar6 = "../../include/array.h";
          ppvVar8 = (void **)&DAT_00000064;
          pcVar3 = "array_get";
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        pcVar7 = pcVar7 + 1;
        if (pcVar3 + (int)pcVar7 * 3 != *(char **)((int)ppvVar8 + iVar1)) {
          pcVar6 = "../../test/arraytest.c";
          ppvVar8 = (void **)0xd1;
          badassert("array_get(a, i) == NTH(i+1)","../../test/arraytest.c",0xd1,"arraytest2");
LAB_8000ee70:
          pcVar3 = (char *)0xb007;
        }
        pvVar5 = (void *)(uint)(pcVar7 < pcVar6);
      } while (pcVar7 < (char *)0xbb7);
      iVar1 = array_setsize(a,0);
      if (iVar1 != 0) {
        badassert("result == 0","../../test/arraytest.c",0xd6,"arraytest2");
      }
      array_destroy(a);
      kprintf("Done.\n");
      return 0;
    }
    iVar1 = 0xb007;
    goto LAB_8000ebc4;
  }
  iVar1 = 0xc0ffee;
  goto LAB_8000eb00;
}