arraytest2(int nargs, char **args)
{
	struct array *a;
	void *p;
	unsigned i, x;
	int result;

	(void)nargs;
	(void)args;

	/* Silence warning with gcc 4.8 -Og (but not -O2) */
	x = 0;

	kprintf("Beginning large array test...\n");
	a = array_create();
	KASSERT(a != NULL);

	/* 1. Fill it one at a time. */
	p = (void *)0xc0ffee;
	for (i=0; i<BIGTESTSIZE; i++) {
		result = array_add(a, p, &x);
		KASSERT(result == 0);
		KASSERT(x == i);
	}
	KASSERT(array_num(a) == BIGTESTSIZE);

	/* 2. Check the contents */
	for (i=0; i<BIGTESTSIZE; i++) {
		KASSERT(array_get(a, i) == p);
	}

	/* 3. Clear it */
	result = array_setsize(a, 0);
	KASSERT(result == 0);

	/* 4. Set the size and initialize with array_set */
	result = array_setsize(a, BIGTESTSIZE);
	KASSERT(result == 0);
	for (i=0; i<BIGTESTSIZE; i++) {
		array_set(a, i, NTH(i));
	}

	/* 5. Check the contents again */
	for (i=0; i<BIGTESTSIZE; i++) {
		KASSERT(array_get(a, i) == NTH(i));
	}

	/* 6. Zot an entry and check the contents */
	array_remove(a, 1);
	KASSERT(array_get(a, 0) == NTH(0));
	KASSERT(array_num(a) == BIGTESTSIZE-1);
	for (i=1; i<BIGTESTSIZE-1; i++) {
		KASSERT(array_get(a, i) == NTH(i+1));
	}

	/* 7. Double the size and check the preexisting contents */
	result = array_setsize(a, BIGTESTSIZE*2);
	KASSERT(result == 0);
	KASSERT(array_get(a, 0) == NTH(0));
	for (i=1; i<BIGTESTSIZE-1; i++) {
		KASSERT(array_get(a, i) == NTH(i+1));
	}

	/* done */
	result = array_setsize(a, 0);
	KASSERT(result == 0);
	array_destroy(a);

	kprintf("Done.\n");

	return 0;
}