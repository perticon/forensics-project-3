int32_t arraytest2(int32_t nargs, char ** args) {
    // 0x8000e970
    kprintf("Beginning large array test...\n");
    int32_t * v1 = array_create(); // 0x8000e99c
    int32_t v2 = (int32_t)v1; // 0x8000e99c
    int32_t v3 = 0; // 0x8000e9a4
    int32_t v4 = 0; // 0x8000e9a4
    int32_t v5 = 0xc0ffee; // 0x8000e9a4
    int32_t v6; // 0x8000e970
    if (v1 == NULL) {
        // 0x8000e9a8
        badassert("a != NULL", "../../test/arraytest.c", 164, "arraytest2");
        v6 = (int32_t)"a != NULL";
        goto lab_0x8000e9c8;
    } else {
        goto lab_0x8000ea54;
    }
  lab_0x8000ea54:;
    // 0x8000ea54
    int32_t v7; // 0x8000e970
    int32_t v8 = v7;
    int32_t v9 = v8; // 0x8000ea5c
    v6 = v2;
    int32_t v10 = v3; // 0x8000ea5c
    int32_t v11 = v4; // 0x8000ea5c
    int32_t v12 = v5; // 0x8000ea5c
    int32_t v13; // 0x8000e970
    int32_t v14; // 0x8000e970
    int32_t v15; // 0x8000e970
    int32_t v16; // 0x8000e970
    int32_t v17; // 0x8000e970
    int32_t v18; // 0x8000e970
    int32_t v19; // 0x8000e970
    int32_t * v20; // 0x8000ea60
    if (v3 < 3000) {
        goto lab_0x8000e9c8;
    } else {
        // 0x8000ea60
        v20 = (int32_t *)(v2 + 4);
        v18 = 0;
        v14 = 3000;
        v16 = 0xc0ffee;
        if (*v20 == 3000) {
            goto lab_0x8000eb00;
        } else {
            // 0x8000ea70
            badassert("array_num(a) == BIGTESTSIZE", "../../test/arraytest.c", 173, "arraytest2");
            v17 = &g41;
            v19 = v8;
            v13 = (int32_t)"../../test/arraytest.c";
            v15 = 173;
            goto lab_0x8000ea90;
        }
    }
  lab_0x8000e9c8:
    // 0x8000e9c8
    v5 = v12;
    int32_t v21 = *(int32_t *)(v2 + 4); // 0x8000e9c8
    int32_t v22; // 0x8000e970
    int32_t v23; // 0x8000e970
    if (array_setsize((int32_t *)v6, v21 + 1) == 0) {
        int32_t v24 = *v1; // 0x8000e9dc
        *(int32_t *)(v24 + 4 * v21) = v5;
        v22 = v21;
        v23 = v24;
    } else {
        // 0x8000e9fc
        badassert("result == 0", "../../test/arraytest.c", 170, "arraytest2");
        v22 = v11;
        v23 = v9;
    }
    // 0x8000ea18
    v4 = v22;
    if (v10 != v4) {
        // 0x8000ea20
        badassert("x == i", "../../test/arraytest.c", 171, "arraytest2");
    }
    // 0x8000ea3c
    v7 = v23;
    v3 = v10 + 1;
    goto lab_0x8000ea54;
  lab_0x8000eb00:
    // 0x8000eb00
    v17 = v18;
    v19 = v18 < v14;
    v13 = v14;
    v15 = v16;
    int32_t v25; // 0x8000e970
    int32_t v26; // 0x8000e970
    int32_t v27; // 0x8000e970
    int32_t v28; // 0x8000e970
    int32_t v29; // 0x8000e970
    if (v18 < 3000) {
        goto lab_0x8000ea90;
    } else {
        int32_t v30 = v2; // 0x8000eb1c
        if (array_setsize(v1, 0) != 0) {
            // 0x8000eb20
            badassert("result == 0", "../../test/arraytest.c", 182, "arraytest2");
            v30 = (int32_t)"result == 0";
        }
        // 0x8000eb40
        v29 = 0;
        v27 = 0xb007;
        if (array_setsize((int32_t *)v30, 3000) == 0) {
            goto lab_0x8000ebc4;
        } else {
            // 0x8000eb50
            badassert("result == 0", "../../test/arraytest.c", 186, "arraytest2");
            v28 = &g41;
            v25 = (int32_t)"result == 0";
            v26 = 186;
            goto lab_0x8000eb70;
        }
    }
  lab_0x8000ea90:;
    int32_t v31 = v17; // 0x8000ea94
    int32_t v32 = v13; // 0x8000ea94
    int32_t v33 = v15; // 0x8000ea94
    if (v19 == 0) {
        // 0x8000ea98
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v31 = &g41;
        v32 = (int32_t)"../../include/array.h";
        v33 = 100;
    }
    int32_t v34 = v31; // 0x8000ead0
    int32_t v35 = v32; // 0x8000ead0
    int32_t v36 = v33; // 0x8000ead0
    if (*(int32_t *)(*v1 + 4 * v17) != v33) {
        // 0x8000ead4
        badassert("array_get(a, i) == p", "../../test/arraytest.c", 177, "arraytest2");
        v34 = &g41;
        v35 = (int32_t)"../../test/arraytest.c";
        v36 = 177;
    }
    // 0x8000eaf0
    v18 = v34 + 1;
    v14 = v35;
    v16 = v36;
    goto lab_0x8000eb00;
  lab_0x8000ebc4:
    // 0x8000ebc4
    v28 = v29;
    v25 = 2 * v29;
    v26 = v27;
    int32_t v37 = 0; // 0x8000ebcc
    int32_t v38; // 0x8000e970
    int32_t v39; // 0x8000e970
    int32_t v40; // 0x8000e970
    int32_t v41; // 0x8000e970
    int32_t v42; // 0x8000e970
    int32_t v43; // 0x8000e970
    int32_t v44; // 0x8000e970
    int32_t v45; // 0x8000e970
    if (v29 < 3000) {
        goto lab_0x8000eb70;
    } else {
        int32_t v46 = v37; // 0x8000ebec
        int32_t v47 = 0xb007; // 0x8000ebec
        if (v37 >= *v20) {
            // 0x8000ebf0
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v46 = &g41;
            v47 = (int32_t)"../../include/array.h";
        }
        int32_t v48 = v46 + 1; // 0x8000ec30
        int32_t v49 = v47; // 0x8000ec30
        if (3 * v46 + v47 != *(int32_t *)(*v1 + 4 * v37)) {
            // 0x8000ec34
            badassert("array_get(a, i) == NTH(i)", "../../test/arraytest.c", 193, "arraytest2");
            v48 = &g41;
            v49 = (int32_t)"../../test/arraytest.c";
        }
        // 0x8000ec54
        v37 = v48;
        while (v48 < 3000) {
            // 0x8000ebdc
            v46 = v37;
            v47 = v49;
            if (v37 >= *v20) {
                // 0x8000ebf0
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v46 = &g41;
                v47 = (int32_t)"../../include/array.h";
            }
            // 0x8000ec10
            v48 = v46 + 1;
            v49 = v47;
            if (3 * v46 + v47 != *(int32_t *)(*v1 + 4 * v37)) {
                // 0x8000ec34
                badassert("array_get(a, i) == NTH(i)", "../../test/arraytest.c", 193, "arraytest2");
                v48 = &g41;
                v49 = (int32_t)"../../test/arraytest.c";
            }
            // 0x8000ec54
            v37 = v48;
        }
        // 0x8000ec60
        array_remove(v1, 1);
        int32_t v50 = *v20; // 0x8000ec68
        int32_t v51 = 0xb007; // 0x8000ec74
        int32_t v52 = v50; // 0x8000ec74
        if (v50 == 0) {
            // 0x8000ec78
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v51 = &g41;
            v52 = (int32_t)"../../include/array.h";
        }
        int32_t v53 = *v1; // 0x8000ec98
        int32_t v54 = *(int32_t *)v53; // 0x8000eca0
        int32_t v55 = 2999; // 0x8000ecac
        int32_t v56 = v52; // 0x8000ecac
        int32_t v57 = v53; // 0x8000ecac
        if (v54 != v51) {
            // 0x8000ecb0
            badassert("array_get(a, 0) == NTH(0)", "../../test/arraytest.c", 198, "arraytest2");
            v55 = &g41;
            v56 = (int32_t)"../../test/arraytest.c";
            v57 = 198;
        }
        // 0x8000ecd0
        v44 = 1;
        v38 = v55;
        v40 = v57;
        if (v56 == v55) {
            goto lab_0x8000ed60;
        } else {
            // 0x8000ecd8
            badassert("array_num(a) == BIGTESTSIZE-1", "../../test/arraytest.c", 199, "arraytest2");
            v43 = &g41;
            v45 = v54;
            v39 = (int32_t)"../../test/arraytest.c";
            v41 = 199;
            v42 = (int32_t)"arraytest2";
            goto lab_0x8000ecf8;
        }
    }
  lab_0x8000eb70:;
    int32_t v58 = v28; // 0x8000eb84
    int32_t v59 = v25 + v28 + v26; // 0x8000eb84
    int32_t v60 = v26; // 0x8000eb84
    if (v28 >= *v20) {
        // 0x8000eb88
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v58 = &g41;
        v59 = (int32_t)"index < a->num";
        v60 = 107;
    }
    // 0x8000eba8
    *(int32_t *)(*v1 + 4 * v58) = v59;
    v29 = v58 + 1;
    v27 = v60;
    goto lab_0x8000ebc4;
  lab_0x8000ed60:;
    int32_t v61 = v44; // 0x8000ed64
    int32_t v62 = v38; // 0x8000ed64
    int32_t v63 = v40; // 0x8000ed64
    int32_t v64 = 0xb007; // 0x8000ed64
    goto lab_0x8000ed68;
  lab_0x8000ed68:
    // 0x8000ed68
    v43 = v61;
    v45 = v61 < v62;
    v39 = v62;
    v41 = v63;
    v42 = v64;
    int32_t v65; // 0x8000e970
    int32_t v66; // 0x8000e970
    int32_t v67; // 0x8000e970
    int32_t v68; // 0x8000e970
    int32_t v69; // 0x8000e970
    int32_t v70; // 0x8000e970
    int32_t v71; // 0x8000e970
    int32_t v72; // 0x8000e970
    if (v61 >= 2999) {
        // 0x8000ed70
        if (array_setsize(v1, 0x1770) != 0) {
            // 0x8000ed84
            badassert("result == 0", "../../test/arraytest.c", 206, "arraytest2");
        }
        int32_t v73 = *v20; // 0x8000eda0
        int32_t v74 = 0xb007; // 0x8000edac
        int32_t v75 = v73; // 0x8000edac
        if (v73 == 0) {
            // 0x8000edb0
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v74 = &g41;
            v75 = (int32_t)"../../include/array.h";
        }
        int32_t v76 = *v1; // 0x8000edd0
        int32_t v77 = *(int32_t *)v76; // 0x8000edd8
        v71 = 1;
        v66 = v75;
        v68 = v76;
        if (v77 == v74) {
            goto lab_0x8000ee70;
        } else {
            // 0x8000ede8
            badassert("array_get(a, 0) == NTH(0)", "../../test/arraytest.c", 207, "arraytest2");
            v70 = &g41;
            v72 = v77;
            v65 = (int32_t)"../../test/arraytest.c";
            v67 = 207;
            v69 = (int32_t)"arraytest2";
            goto lab_0x8000ee08;
        }
    } else {
        goto lab_0x8000ecf8;
    }
  lab_0x8000ecf8:;
    int32_t v78 = v43; // 0x8000ecfc
    int32_t v79 = v39; // 0x8000ecfc
    int32_t v80 = v41; // 0x8000ecfc
    int32_t v81 = v42; // 0x8000ecfc
    if (v45 == 0) {
        // 0x8000ed00
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v78 = &g41;
        v79 = (int32_t)"../../include/array.h";
        v80 = 100;
        v81 = (int32_t)"array_get";
    }
    int32_t v82 = v78 + 1; // 0x8000ed28
    v61 = v82;
    v62 = v79;
    v63 = v80;
    v64 = v81;
    if (v81 + 3 * v82 == *(int32_t *)(v80 + 4 * v43)) {
        goto lab_0x8000ed68;
    } else {
        // 0x8000ed40
        badassert("array_get(a, i) == NTH(i+1)", "../../test/arraytest.c", 201, "arraytest2");
        v44 = &g41;
        v38 = (int32_t)"../../test/arraytest.c";
        v40 = 201;
        goto lab_0x8000ed60;
    }
  lab_0x8000ee70:;
    int32_t v83 = v71; // 0x8000ee74
    int32_t v84 = v66; // 0x8000ee74
    int32_t v85 = v68; // 0x8000ee74
    int32_t v86 = 0xb007; // 0x8000ee74
    goto lab_0x8000ee78;
  lab_0x8000ee78:
    // 0x8000ee78
    v70 = v83;
    v72 = v83 < v84;
    v65 = v84;
    v67 = v85;
    v69 = v86;
    if (v83 >= 2999) {
        // 0x8000ee80
        if (array_setsize(v1, 0) != 0) {
            // 0x8000ee94
            badassert("result == 0", "../../test/arraytest.c", 214, "arraytest2");
        }
        // 0x8000eeb0
        array_destroy(v1);
        kprintf("Done.\n");
        return 0;
    }
    goto lab_0x8000ee08;
  lab_0x8000ee08:;
    int32_t v87 = v70; // 0x8000ee0c
    int32_t v88 = v65; // 0x8000ee0c
    int32_t v89 = v67; // 0x8000ee0c
    int32_t v90 = v69; // 0x8000ee0c
    if (v72 == 0) {
        // 0x8000ee10
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v87 = &g41;
        v88 = (int32_t)"../../include/array.h";
        v89 = 100;
        v90 = (int32_t)"array_get";
    }
    int32_t v91 = v87 + 1; // 0x8000ee38
    v83 = v91;
    v84 = v88;
    v85 = v89;
    v86 = v90;
    if (v90 + 3 * v91 == *(int32_t *)(v89 + 4 * v70)) {
        goto lab_0x8000ee78;
    } else {
        // 0x8000ee50
        badassert("array_get(a, i) == NTH(i+1)", "../../test/arraytest.c", 209, "arraytest2");
        v71 = &g41;
        v66 = (int32_t)"../../test/arraytest.c";
        v68 = 209;
        goto lab_0x8000ee70;
    }
}