void cv_signal(int32_t * cv, int32_t * lock) {
    int32_t v1; // 0x80015cb4
    if (lock == NULL) {
        // 0x80015ce8
        badassert("lock != NULL", "../../thread/synch.c", 358, "cv_signal");
        v1 = (int32_t)"../../thread/synch.c";
    } else {
        // 0x80015ce8
        v1 = (int32_t)lock;
        if (cv == NULL) {
            // 0x80015cf0
            badassert("cv != NULL", "../../thread/synch.c", 359, "cv_signal");
            v1 = (int32_t)"../../thread/synch.c";
        }
    }
    // 0x80015d10
    if (!lock_do_i_hold((int32_t *)v1)) {
        // 0x80015d20
        badassert("lock_do_i_hold(lock)", "../../thread/synch.c", 360, "cv_signal");
    }
    // 0x80015d40
    spinlock_acquire((int32_t *)"NULL");
    wchan_wakeone((int32_t *)0x203d2120, (int32_t *)"NULL");
    spinlock_release((int32_t *)"NULL");
}