void cv_signal(cv *cv,lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  char *lock_00;
  spinlock *splk;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar2 = "lock != NULL";
    lock_00 = "../../thread/synch.c";
    badassert("lock != NULL","../../thread/synch.c",0x166,"cv_signal");
    cv = (cv *)pcVar2;
  }
  if (cv == (cv *)0x0) {
    lock_00 = "../../thread/synch.c";
    badassert("cv != NULL","../../thread/synch.c",0x167,"cv_signal");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  splk = &cv->cv_lock;
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0x168,"cv_signal");
  }
  spinlock_acquire(splk);
  wchan_wakeone(cv->cv_wchan,splk);
  spinlock_release(splk);
  return;
}