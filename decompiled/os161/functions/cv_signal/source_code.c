cv_signal(struct cv *cv, struct lock *lock)
{
        // Write this
#if OPT_SYNCH
        KASSERT(lock != NULL);
	KASSERT(cv != NULL);
	KASSERT(lock_do_i_hold(lock));
	/* g.Cabodi - 2019: here the spinlock is NOT required, as no atomic operation 
	   has to be done. The spinlock is just acquired because needed by wakeone */
	spinlock_acquire(&cv->cv_lock);
	wchan_wakeone(cv->cv_wchan,&cv->cv_lock);
	spinlock_release(&cv->cv_lock);
#endif
	(void)cv;    // suppress warning until code gets written
	(void)lock;  // suppress warning until code gets written
}