int cmd_mount(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  bool bVar3;
  char *b;
  char *str;
  
  if (nargs == 3) {
    b = args[1];
    str = args[2];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
    }
    bVar3 = false;
    do {
      if (bVar3) {
        kprintf("Unknown filesystem type %s\n",b);
        return 8;
      }
      iVar1 = strcmp(mounttable[0].name,b);
      bVar3 = true;
    } while (iVar1 != 0);
    iVar1 = sfs_mount(str);
  }
  else {
    kprintf("Usage: mount fstype device:\n");
    iVar1 = 8;
  }
  return iVar1;
}