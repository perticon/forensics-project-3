cmd_mount(int nargs, char **args)
{
	char *fstype;
	char *device;
	unsigned i;

	if (nargs != 3)
	{
		kprintf("Usage: mount fstype device:\n");
		return EINVAL;
	}

	fstype = args[1];
	device = args[2];

	/* Allow (but do not require) colon after device name */
	if (device[strlen(device) - 1] == ':')
	{
		device[strlen(device) - 1] = 0;
	}

	for (i = 0; i < ARRAYCOUNT(mounttable); i++)
	{
		if (!strcmp(mounttable[i].name, fstype))
		{
			return mounttable[i].func(device);
		}
	}
	kprintf("Unknown filesystem type %s\n", fstype);
	return EINVAL;
}