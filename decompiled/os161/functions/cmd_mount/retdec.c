int32_t cmd_mount(int32_t nargs, char ** args) {
    if (nargs != 3) {
        // 0x8000c694
        kprintf("Usage: mount fstype device:\n");
        // 0x8000c730
        return 8;
    }
    int32_t v1 = (int32_t)args;
    int32_t v2 = *(int32_t *)(v1 + 8); // 0x8000c6ac
    char * v3 = (char *)v2; // 0x8000c6b4
    int32_t v4 = strlen(v3); // 0x8000c6b4
    int32_t v5 = v2 - 1;
    if (*(char *)(v5 + v4) == 58) {
        // 0x8000c6cc
        *(char *)(strlen(v3) + v5) = 0;
    }
    char * v6 = (char *)*(int32_t *)(v1 + 4);
    int32_t result; // 0x8000c674
    if (strcmp((char *)-0x7ffdb5e0, v6) == 0) {
        // 0x8000c704
        result = sfs_mount(v3);
    } else {
        // 0x8000c71c
        kprintf("Unknown filesystem type %s\n", v6);
        result = 8;
    }
    // 0x8000c730
    return result;
}