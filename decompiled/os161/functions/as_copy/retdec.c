int32_t as_copy(int32_t * old, int32_t ** ret, int32_t old_pid, int32_t new_pid) {
    int32_t * v1 = as_create(); // 0x8001aeec
    if (v1 == NULL) {
        // 0x8001b0f0
        return 3;
    }
    int32_t v2 = -0x7ffe0000; // 0x8001aefc
    if (old == NULL) {
        // 0x8001af00
        badassert("old != NULL", "../../vm/addrspace.c", 183, "as_copy");
        v2 = (int32_t)"old != NULL";
    }
    int32_t v3 = (int32_t)old;
    int32_t * v4 = (int32_t *)(v3 + 4); // 0x8001af48
    int32_t v5 = v2; // 0x8001af54
    if (*v4 == 0) {
        // 0x8001af58
        badassert("old->as_npages1 > 0", "../../vm/addrspace.c", 185, "as_copy");
        v5 = &g41;
    }
    int32_t * v6 = (int32_t *)(v3 + 8); // 0x8001af74
    int32_t v7 = v5; // 0x8001af80
    if (*v6 == 0) {
        // 0x8001af84
        badassert("old->as_vbase2 != 0", "../../vm/addrspace.c", 186, "as_copy");
        v7 = &g41;
    }
    int32_t * v8 = (int32_t *)(v3 + 12); // 0x8001afa0
    int32_t v9 = v7; // 0x8001afac
    if (*v8 == 0) {
        // 0x8001afb0
        badassert("old->as_npages2 > 0", "../../vm/addrspace.c", 187, "as_copy");
        v9 = &g41;
    }
    int32_t v10 = (int32_t)v1; // 0x8001aeec
    *v1 = v9;
    *(int32_t *)(v10 + 4) = *v4;
    int32_t * v11 = (int32_t *)(v10 + 8); // 0x8001afe4
    *v11 = *v6;
    int32_t v12 = *v8; // 0x8001afe8
    int32_t * v13 = (int32_t *)(v10 + 12); // 0x8001aff0
    *v13 = v12;
    int32_t v14 = 0; // 0x8001b05c
    if (v12 > 0) {
        int32_t v15 = 0x1000 * v14; // 0x8001b004
        int32_t v16 = ipt_lookup(old_pid, *v11 + v15); // 0x8001b00c
        int32_t v17; // 0x8001b01c
        if (v16 != 0) {
            // 0x8001b018
            v17 = as_prepare_load(1);
            memmove((char *)-v17, (int32_t *)-v16, 0x1000);
            ipt_add(new_pid, v17, *v11 + v15);
        }
        // 0x8001b048
        v14++;
        while (v14 < *v13) {
            // 0x8001b000
            v15 = 0x1000 * v14;
            v16 = ipt_lookup(old_pid, *v11 + v15);
            if (v16 != 0) {
                // 0x8001b018
                v17 = as_prepare_load(1);
                memmove((char *)-v17, (int32_t *)-v16, 0x1000);
                ipt_add(new_pid, v17, *v11 + v15);
            }
            // 0x8001b048
            v14++;
        }
    }
    int32_t v18 = ipt_lookup(old_pid, 0x7ffff000); // 0x8001b080
    int32_t v19 = 0; // 0x8001b08c
    if (v18 != 0) {
        int32_t v20 = as_prepare_load(1); // 0x8001b094
        memmove((char *)-v20, (int32_t *)-v18, 0x1000);
        ipt_add(new_pid, v20, 0x7ffff000);
        v19++;
        int32_t v21 = 0x7ffff000 - 0x1000 * v19; // 0x8001b074
        int32_t v22 = ipt_lookup(old_pid, v21); // 0x8001b080
        while (v22 != 0) {
            // 0x8001b0c0
            v20 = as_prepare_load(1);
            memmove((char *)-v20, (int32_t *)-v22, 0x1000);
            ipt_add(new_pid, v20, v21);
            v19++;
            v21 = 0x7ffff000 - 0x1000 * v19;
            v22 = ipt_lookup(old_pid, v21);
        }
    }
    // 0x8001b0cc
    duplicate_swap_pages(old_pid, new_pid);
    *(int32_t *)ret = v10;
    // 0x8001b0f0
    return 0;
}