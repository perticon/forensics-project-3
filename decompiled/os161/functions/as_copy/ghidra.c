int as_copy(addrspace *old,addrspace **ret,pid_t old_pid,pid_t new_pid)

{
  addrspace *paVar1;
  vaddr_t vVar2;
  paddr_t pVar3;
  int iVar4;
  paddr_t pVar5;
  
  paVar1 = as_create();
  if (paVar1 == (addrspace *)0x0) {
    iVar4 = 3;
  }
  else {
    if (old == (addrspace *)0x0) {
      badassert("old != NULL",s_______vm_addrspace_c_80027688,0xb7,"as_copy");
    }
    vVar2 = old->as_vbase1;
    if (vVar2 == 0) {
      badassert("old->as_vbase1 != 0",s_______vm_addrspace_c_80027688,0xb8,"as_copy");
    }
    if (old->as_npages1 == 0) {
      badassert("old->as_npages1 > 0",s_______vm_addrspace_c_80027688,0xb9,"as_copy");
    }
    if (old->as_vbase2 == 0) {
      badassert("old->as_vbase2 != 0",s_______vm_addrspace_c_80027688,0xba,"as_copy");
    }
    if (old->as_npages2 == 0) {
      badassert("old->as_npages2 > 0",s_______vm_addrspace_c_80027688,0xbb,"as_copy");
    }
    paVar1->as_vbase1 = vVar2;
    paVar1->as_npages1 = old->as_npages1;
    paVar1->as_vbase2 = old->as_vbase2;
    paVar1->as_npages2 = old->as_npages2;
    for (iVar4 = 0; iVar4 < (int)paVar1->as_npages2; iVar4 = iVar4 + 1) {
      pVar5 = ipt_lookup(old_pid,paVar1->as_vbase2 + iVar4 * 0x1000);
      if (pVar5 != 0) {
        pVar3 = as_prepare_load(1);
        memmove((void *)(pVar3 + 0x80000000),(void *)(pVar5 + 0x80000000),0x1000);
        ipt_add(new_pid,pVar3,iVar4 * 0x1000 + paVar1->as_vbase2);
      }
    }
    pVar5 = 1;
    iVar4 = 0;
    while (pVar5 != 0) {
      vVar2 = (0x7ffff - iVar4) * 0x1000;
      pVar5 = ipt_lookup(old_pid,vVar2);
      if (pVar5 != 0) {
        pVar3 = as_prepare_load(1);
        memmove((void *)(pVar3 + 0x80000000),(void *)(pVar5 + 0x80000000),0x1000);
        ipt_add(new_pid,pVar3,vVar2);
      }
      iVar4 = iVar4 + 1;
    }
    duplicate_swap_pages(old_pid,new_pid);
    *ret = paVar1;
    iVar4 = 0;
  }
  return iVar4;
}