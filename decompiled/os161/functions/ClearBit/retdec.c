void ClearBit(int32_t * A, int32_t k) {
    int32_t v1 = k + 31;
    int32_t v2 = k & -0x7fffffe1; // 0x8001b218
    int32_t * v3 = (int32_t *)(4 * ((k > -1 ? k : v1) >> 5) + (int32_t)A); // 0x8001b240
    *v3 = *v3 & (-1 << (v2 > -1 ? v2 : (v1 | -32) + 1)) - 1;
}