void ClearBit(int *A,int k)

{
  int iVar1;
  
  iVar1 = k;
  if (k < 0) {
    iVar1 = k + 0x1f;
  }
  A[iVar1 >> 5] = A[iVar1 >> 5] & ~(1 << (k & 0x1fU));
  return;
}