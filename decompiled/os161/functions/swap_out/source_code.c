int swap_out(paddr_t paddr, vaddr_t vaddr, int segment_victim, pid_t pid_victim)
{

    int result, i;

    KASSERT(pid_victim != -1);

    /*if the page to swap out is in the segment, do not swap out */
    if (segment_victim == 1)
    {
        return 0;
    }

    spinlock_acquire(&swap_lock);
    /* iterate though the swap_table to find a free entry */
    for (i = 0; i < ENTRIES; i++)
    {
        /* if pid of entry is -1, it is free */
        if (swap_table[i].pid == -1)
        {
            /* set pid of entry to -2, so that no one else can select the entry as free */
            swap_table[i].pid = -2;

#if DUMPOUT
            kprintf("Start swapping out PID %d PAGE %d\n", pid_victim, vaddr / PAGE_SIZE);
#endif
            spinlock_release(&swap_lock);
            /* actually write on file */
            result = file_write_paddr(v, paddr, PAGE_SIZE, i * PAGE_SIZE);
            spinlock_acquire(&swap_lock);
            if (result != PAGE_SIZE)
            {
                panic("Unable to swap page out");
            }

            KASSERT(result >= 0);

            swap_table[i].pid = pid_victim;
            swap_table[i].page = vaddr;

            spinlock_release(&swap_lock);
            increase(SWAP_OUT_PAGE);
            return 0;
        }
    }
    spinlock_release(&swap_lock);

    panic("Out of swapspace\n");
}