int32_t swap_out(int32_t paddr, int32_t vaddr, int32_t segment_victim, int32_t pid_victim) {
    int32_t v1; // 0x8001dbf4
    int32_t v2; // 0x8001dbf4
    if (pid_victim == -1) {
        // 0x8001dc3c
        badassert("pid_victim != -1", "../../vm/swapfile.c", 581, "swap_out");
        v2 = (int32_t)"../../vm/swapfile.c";
        v1 = (int32_t)"pid_victim != -1";
    } else {
        // 0x8001dc3c
        v2 = vaddr;
        v1 = paddr;
        if (segment_victim == 1) {
            // 0x8001dd6c
            return 0;
        }
    }
    // 0x8001dc4c
    spinlock_acquire(&swap_lock);
    int32_t v3 = 0; // 0x8001dc68
    int32_t v4; // 0x8001dd4c
    int32_t * v5; // 0x8001dc70
    while (v3 < 2304) {
        // 0x8001dc6c
        v4 = 8 * v3;
        v5 = (int32_t *)(v4 + (int32_t)&swap_table);
        v3++;
        if (*v5 == -1) {
            // 0x8001dc80
            *v5 = -2;
            spinlock_release(&swap_lock);
            int32_t v6 = file_write_paddr(v, v1, 0x1000, (int64_t)&g41); // 0x8001dcc8
            spinlock_acquire(&swap_lock);
            if (v6 == 0x1000) {
                goto lab_0x8001dd10;
            } else {
                // 0x8001dcec
                panic("Unable to swap page out");
                if (v6 > -1) {
                    goto lab_0x8001dd10;
                } else {
                    // 0x8001dcf4
                    badassert("result >= 0", "../../vm/swapfile.c", 611, "swap_out");
                    goto lab_0x8001dd10;
                }
            }
        }
    }
    // 0x8001dd50
    spinlock_release(&swap_lock);
    panic("Out of swapspace\n");
    // 0x8001dd6c
    return 0;
  lab_0x8001dd10:
    // 0x8001dd10
    *v5 = pid_victim;
    *(int32_t *)(v4 + (int32_t)&swap_table + 4) = v2;
    spinlock_release(&swap_lock);
    increase(7);
    return 0;
}