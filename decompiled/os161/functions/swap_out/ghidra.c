int swap_out(paddr_t paddr,vaddr_t vaddr,int segment_victim,pid_t pid_victim)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  int iVar4;
  
  if (pid_victim == -1) {
    pcVar2 = "pid_victim != -1";
    pcVar3 = s_______vm_swapfile_c_800281fc;
    segment_victim = 0x245;
    badassert("pid_victim != -1",s_______vm_swapfile_c_800281fc,0x245,"swap_out");
    paddr = (paddr_t)pcVar2;
    vaddr = (vaddr_t)pcVar3;
  }
  if (segment_victim != 1) {
    spinlock_acquire(&swap_lock);
    iVar1 = 0;
    do {
      iVar4 = iVar1;
      if (0x8ff < iVar4) {
        spinlock_release(&swap_lock);
                    /* WARNING: Subroutine does not return */
        panic("Out of swapspace\n");
      }
      iVar1 = iVar4 + 1;
    } while (swap_table[iVar4].pid != -1);
    swap_table[iVar4].pid = -2;
    spinlock_release(&swap_lock);
    iVar1 = file_write_paddr(v,paddr,0x1000,(longlong)(iVar4 << 0xc));
    spinlock_acquire(&swap_lock);
    if (iVar1 != 0x1000) {
                    /* WARNING: Subroutine does not return */
      panic("Unable to swap page out");
    }
    swap_table[iVar4].pid = pid_victim;
    swap_table[iVar4].page = vaddr;
    spinlock_release(&swap_lock);
    increase(7);
  }
  return 0;
}