void bitmap_unmark(int32_t * b, uint32_t index) {
    int32_t v1 = (uint32_t)(index / 8); // 0x8000aef4
    int32_t v2 = index; // 0x8000aef4
    if ((int32_t)b <= index) {
        // 0x8000aef8
        badassert("index < b->nbits", "../../lib/bitmap.c", 153, "bitmap_unmark");
        v1 = &g41;
        v2 = (int32_t)"../../lib/bitmap.c";
    }
    int32_t v3 = 1 << v2 % 8; // 0x8000af20
    int32_t v4 = v1 + 0x203c2078; // 0x8000af30
    unsigned char v5 = *(char *)v4; // 0x8000af34
    int32_t v6 = v4; // 0x8000af44
    char v7 = -1 - (char)v3; // 0x8000af44
    if ((v3 & (int32_t)v5) == 0) {
        // 0x8000af48
        badassert("(b->v[ix] & mask)!=0", "../../lib/bitmap.c", 156, "bitmap_unmark");
        v6 = &g41;
        v7 = (char)"../../lib/bitmap.c";
    }
    // 0x8000af68
    *(char *)v6 = v7 & v5;
}