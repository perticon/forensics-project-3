void bitmap_unmark(bitmap *b,uint index)

{
  uint uVar1;
  byte *pbVar2;
  byte bVar3;
  char *pcVar4;
  char *pcVar5;
  uint uVar6;
  byte bVar7;
  
  uVar1 = index >> 3;
  if (b->nbits <= index) {
    pcVar4 = "index < b->nbits";
    pcVar5 = s_______lib_bitmap_c_800235b4;
    badassert("index < b->nbits",s_______lib_bitmap_c_800235b4,0x99,"bitmap_unmark");
    b = (bitmap *)pcVar4;
    index = (uint)pcVar5;
  }
  uVar6 = 1 << (index & 7) & 0xff;
  pbVar2 = b->v + uVar1;
  bVar3 = *pbVar2;
  bVar7 = ~(byte)uVar6;
  if ((bVar3 & uVar6) == 0) {
    bVar7 = 0xb4;
    badassert("(b->v[ix] & mask)!=0",s_______lib_bitmap_c_800235b4,0x9c,"bitmap_unmark");
  }
  *pbVar2 = bVar3 & bVar7;
  return;
}