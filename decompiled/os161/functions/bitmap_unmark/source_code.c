bitmap_unmark(struct bitmap *b, unsigned index)
{
        unsigned ix;
        WORD_TYPE mask;

        KASSERT(index < b->nbits);
        bitmap_translate(index, &ix, &mask);

        KASSERT((b->v[ix] & mask)!=0);
        b->v[ix] &= ~mask;
}