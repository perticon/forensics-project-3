int as_complete_load(struct addrspace *as)
{
  vm_can_sleep();
  (void)as;
  return 0;
}