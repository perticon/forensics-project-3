int32_t * proc_create_runprogram(char * name) {
    int32_t * result = proc_create(name); // 0x8000ccf8
    if (result == NULL) {
        // 0x8000cd60
        return result;
    }
    int32_t v1 = (int32_t)result; // 0x8000ccf8
    *(int32_t *)(v1 + 16) = 0;
    int32_t v2; // 0x8000ccec
    int32_t * v3 = (int32_t *)(v2 + 84); // 0x8000cd08
    spinlock_acquire((int32_t *)(*v3 + 8));
    int32_t v4 = *v3; // 0x8000cd14
    int32_t v5 = *(int32_t *)(v4 + 20); // 0x8000cd1c
    int32_t v6 = v4; // 0x8000cd28
    if (v5 != 0) {
        // 0x8000cd2c
        vnode_incref((int32_t *)v5);
        *(int32_t *)(v1 + 20) = *(int32_t *)(*v3 + 20);
        v6 = *v3;
    }
    // 0x8000cd48
    spinlock_release((int32_t *)(v6 + 8));
    // 0x8000cd60
    return result;
}