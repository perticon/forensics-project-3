proc * proc_create_runprogram(char *name)

{
  proc *ppVar1;
  vnode *vn;
  int unaff_s7;
  
  ppVar1 = proc_create(name);
  if (ppVar1 == (proc *)0x0) {
    ppVar1 = (proc *)0x0;
  }
  else {
    ppVar1->p_addrspace = (addrspace *)0x0;
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
    vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    if (vn != (vnode *)0x0) {
      vnode_incref(vn);
      ppVar1->p_cwd = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    }
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  }
  return ppVar1;
}