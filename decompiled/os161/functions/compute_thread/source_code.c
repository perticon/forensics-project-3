compute_thread(void *junk1, unsigned long num)
{
	struct matrix {
		char m[DIM][DIM];
	};
	struct matrix *m1, *m2, *m3;
	unsigned char tot;
	int i, j, k, m;
	uint32_t rand;

	(void)junk1;

	m1 = kmalloc(sizeof(struct matrix));
	KASSERT(m1 != NULL);
	m2 = kmalloc(sizeof(struct matrix));
	KASSERT(m2 != NULL);
	m3 = kmalloc(sizeof(struct matrix));
	KASSERT(m3 != NULL);

	for (m=0; m<COMPUTE_ITERS; m++) {

		for (i=0; i<DIM; i++) {
			for (j=0; j<DIM; j++) {
				rand = random();
				m1->m[i][j] = rand >> 16;
				m2->m[i][j] = rand & 0xffff;
			}
		}

		for (i=0; i<DIM; i++) {
			for (j=0; j<DIM; j++) {
				tot = 0;
				for (k=0; k<DIM; k++) {
					tot += m1->m[i][k] * m2->m[k][j];
				}
				m3->m[i][j] = tot;
			}
		}

		tot = 0;
		for (i=0; i<DIM; i++) {
			tot += m3->m[i][i];
		}

		kprintf("{%lu: %u}", num, (unsigned) tot);
		thread_yield();
	}

	kfree(m1);
	kfree(m2);
	kfree(m3);

	V(donesem);
}