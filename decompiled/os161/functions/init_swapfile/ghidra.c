void init_swapfile(void)

{
  int iVar1;
  
  iVar1 = vfs_open("./SWAPFILE",0x16,0x309,&v);
  if (iVar1 == -1) {
    badassert("result != -1",s_______vm_swapfile_c_800281fc,0x1c4,"init_swapfile");
  }
  spinlock_acquire(&swap_lock);
  for (iVar1 = 0; iVar1 < 0x900; iVar1 = iVar1 + 1) {
    swap_table[iVar1].pid = -1;
  }
  spinlock_release(&swap_lock);
  return;
}