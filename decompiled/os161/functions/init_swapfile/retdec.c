void init_swapfile(void) {
    // 0x8001da08
    if (vfs_open("./SWAPFILE", 22, 777, &v) == -1) {
        // 0x8001da38
        badassert("result != -1", "../../vm/swapfile.c", 452, "init_swapfile");
    }
    // 0x8001da54
    spinlock_acquire(&swap_lock);
    for (int32_t i = 0; i < 2304; i++) {
        // 0x8001da74
        *(int32_t *)(8 * i + (int32_t)&swap_table) = -1;
    }
    // 0x8001da8c
    spinlock_release(&swap_lock);
}