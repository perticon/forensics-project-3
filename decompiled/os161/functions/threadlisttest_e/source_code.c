threadlisttest_e(void)
{
	struct threadlist tl;
	struct thread *t;
	unsigned i;

	threadlist_init(&tl);

	threadlist_addhead(&tl, fakethreads[1]);
	threadlist_addtail(&tl, fakethreads[3]);
	KASSERT(tl.tl_count == 2);
	check_order(&tl, false);

	threadlist_insertafter(&tl, fakethreads[3], fakethreads[4]);
	KASSERT(tl.tl_count == 3);
	check_order(&tl, false);

	threadlist_insertbefore(&tl, fakethreads[0], fakethreads[1]);
	KASSERT(tl.tl_count == 4);
	check_order(&tl, false);

	threadlist_insertafter(&tl, fakethreads[1], fakethreads[2]);
	KASSERT(tl.tl_count == 5);
	check_order(&tl, false);

	KASSERT(fakethreads[4]->t_listnode.tln_prev->tln_self ==
		fakethreads[3]);
	KASSERT(fakethreads[3]->t_listnode.tln_prev->tln_self ==
		fakethreads[2]);
	KASSERT(fakethreads[2]->t_listnode.tln_prev->tln_self ==
		fakethreads[1]);
	KASSERT(fakethreads[1]->t_listnode.tln_prev->tln_self ==
		fakethreads[0]);

	for (i=0; i<5; i++) {
		t = threadlist_remhead(&tl);
		KASSERT(t == fakethreads[i]);
	}
	KASSERT(tl.tl_count == 0);

	threadlist_cleanup(&tl);
}