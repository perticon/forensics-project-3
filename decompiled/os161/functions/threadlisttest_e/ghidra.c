void threadlisttest_e(void)

{
  thread *ptVar1;
  thread *ptVar2;
  char *tl_00;
  uint uVar3;
  thread **unaff_s1;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[1]);
  threadlist_addtail(&tl,fakethreads[3]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xf3,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertafter(&tl,fakethreads[3],fakethreads[4]);
  if (tl.tl_count != 3) {
    badassert("tl.tl_count == 3","../../test/threadlisttest.c",0xf7,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertbefore(&tl,fakethreads[0],fakethreads[1]);
  if (tl.tl_count != 4) {
    badassert("tl.tl_count == 4","../../test/threadlisttest.c",0xfb,"threadlisttest_e");
  }
  check_order(&tl,false);
  threadlist_insertafter(&tl,fakethreads[1],fakethreads[2]);
  tl_00 = (char *)&tl;
  if (tl.tl_count != 5) {
    tl_00 = "tl.tl_count == 5";
    badassert("tl.tl_count == 5","../../test/threadlisttest.c",0xff,"threadlisttest_e");
  }
  check_order((threadlist *)tl_00,false);
  ptVar2 = fakethreads[3];
  if (((fakethreads[4]->t_listnode).tln_prev)->tln_self != fakethreads[3]) {
    badassert("fakethreads[4]->t_listnode.tln_prev->tln_self == fakethreads[3]",
              "../../test/threadlisttest.c",0x103,"threadlisttest_e");
  }
  ptVar1 = fakethreads[2];
  if (((ptVar2->t_listnode).tln_prev)->tln_self != fakethreads[2]) {
    badassert("fakethreads[3]->t_listnode.tln_prev->tln_self == fakethreads[2]",
              "../../test/threadlisttest.c",0x105,"threadlisttest_e");
  }
  ptVar2 = fakethreads[1];
  if (((ptVar1->t_listnode).tln_prev)->tln_self != fakethreads[1]) {
    badassert("fakethreads[2]->t_listnode.tln_prev->tln_self == fakethreads[1]",
              "../../test/threadlisttest.c",0x107,"threadlisttest_e");
  }
  uVar3 = 0;
  if (((ptVar2->t_listnode).tln_prev)->tln_self != fakethreads[0]) {
    badassert("fakethreads[1]->t_listnode.tln_prev->tln_self == fakethreads[0]",
              "../../test/threadlisttest.c",0x109,"threadlisttest_e");
    do {
      ptVar2 = threadlist_remhead(&tl);
      if (unaff_s1[uVar3] != ptVar2) {
        badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x10d,"threadlisttest_e");
      }
      uVar3 = uVar3 + 1;
LAB_80014200:
    } while (uVar3 < 5);
    if (tl.tl_count != 0) {
      badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x10f,"threadlisttest_e");
    }
    threadlist_cleanup(&tl);
    return;
  }
  unaff_s1 = fakethreads;
  goto LAB_80014200;
}