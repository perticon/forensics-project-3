void threadlisttest_e(void) {
    // 0x80013f44
    int32_t tl; // bp-48, 0x80013f44
    threadlist_init(&tl);
    threadlist_addhead(&tl, (int32_t *)g30);
    threadlist_addtail(&tl, (int32_t *)g32);
    int32_t v1; // 0x80013f44
    if (v1 != 2) {
        // 0x80013f90
        badassert("tl.tl_count == 2", "../../test/threadlisttest.c", 243, "threadlisttest_e");
    }
    // 0x80013fac
    check_order(&tl, false);
    threadlist_insertafter(&tl, (int32_t *)g32, (int32_t *)g33);
    if (v1 != 3) {
        // 0x80013fe4
        badassert("tl.tl_count == 3", "../../test/threadlisttest.c", 247, "threadlisttest_e");
    }
    // 0x80014000
    check_order(&tl, false);
    int32_t v2 = *(int32_t *)&fakethreads; // 0x80014014
    threadlist_insertbefore(&tl, (int32_t *)v2, (int32_t *)g30);
    if (v1 != 4) {
        // 0x80014038
        badassert("tl.tl_count == 4", "../../test/threadlisttest.c", 251, "threadlisttest_e");
    }
    // 0x80014054
    check_order(&tl, false);
    threadlist_insertafter(&tl, (int32_t *)g30, (int32_t *)g31);
    int32_t v3 = &tl; // 0x80014088
    if (v1 != 5) {
        // 0x8001408c
        badassert("tl.tl_count == 5", "../../test/threadlisttest.c", 255, "threadlisttest_e");
        v3 = (int32_t)"tl.tl_count == 5";
    }
    // 0x800140ac
    check_order((int32_t *)v3, false);
    int32_t v4 = *(int32_t *)(*(int32_t *)(g33 + 60) + 8); // 0x800140cc
    int32_t v5 = v4; // 0x800140dc
    if (v4 != g32) {
        // 0x800140e0
        badassert("fakethreads[4]->t_listnode.tln_prev->tln_self == fakethreads[3]", "../../test/threadlisttest.c", 259, "threadlisttest_e");
        v5 = &g41;
    }
    int32_t v6 = *(int32_t *)(*(int32_t *)(v5 + 60) + 8); // 0x80014104
    int32_t v7 = v6; // 0x80014118
    if (v6 != g31) {
        // 0x8001411c
        badassert("fakethreads[3]->t_listnode.tln_prev->tln_self == fakethreads[2]", "../../test/threadlisttest.c", 261, "threadlisttest_e");
        v7 = &g41;
    }
    int32_t v8 = *(int32_t *)(*(int32_t *)(v7 + 60) + 8); // 0x80014140
    int32_t v9 = v8; // 0x80014154
    if (v8 != g30) {
        // 0x80014158
        badassert("fakethreads[2]->t_listnode.tln_prev->tln_self == fakethreads[1]", "../../test/threadlisttest.c", 263, "threadlisttest_e");
        v9 = &g41;
    }
    int32_t v10 = *(int32_t *)&fakethreads; // 0x80014184
    int32_t v11 = 0; // 0x80014190
    int32_t v12 = (int32_t)&fakethreads; // 0x80014190
    int32_t v13; // 0x80013f44
    if (*(int32_t *)(*(int32_t *)(v9 + 60) + 8) == v10) {
        goto lab_0x80014200;
    } else {
        // 0x80014194
        badassert("fakethreads[1]->t_listnode.tln_prev->tln_self == fakethreads[0]", "../../test/threadlisttest.c", 265, "threadlisttest_e");
        v13 = 0;
        goto lab_0x800141b4;
    }
  lab_0x80014200:
    // 0x80014200
    v13 = v11;
    int32_t v14 = v12; // 0x80014208
    if (v11 >= 5) {
        if (v1 != 0) {
            // 0x8001421c
            badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 271, "threadlisttest_e");
        }
        // 0x80014238
        threadlist_cleanup(&tl);
        return;
    }
    goto lab_0x800141b4;
  lab_0x800141b4:
    // 0x800141b4
    v12 = v14;
    int32_t * v15 = threadlist_remhead(&tl); // 0x800141b8
    if (*(int32_t *)(4 * v13 + v12) != (int32_t)v15) {
        // 0x800141d4
        badassert("t == fakethreads[i]", "../../test/threadlisttest.c", 269, "threadlisttest_e");
    }
    // 0x800141f0
    v11 = v13 + 1;
    goto lab_0x80014200;
}