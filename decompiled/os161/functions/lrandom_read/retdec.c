int32_t lrandom_read(char * devdata, int32_t * uio) {
    int32_t v1 = (int32_t)devdata;
    int32_t result = 0; // 0x80004b10
    while (*(int32_t *)((int32_t)uio + 16) != 0) {
        int32_t val = lamebus_read_register((int32_t *)v1, *(int32_t *)(v1 + 4), 0); // bp-24, 0x80004ae8
        int32_t v2 = uiomove((char *)&val, 4, uio); // 0x80004af8
        result = v2;
        if (v2 != 0) {
            // break -> 0x80004b14
            break;
        }
        result = 0;
    }
    // 0x80004b14
    return result;
}