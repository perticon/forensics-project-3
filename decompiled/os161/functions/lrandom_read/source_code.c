lrandom_read(void *devdata, struct uio *uio)
{
	struct lrandom_softc *lr = devdata;
	uint32_t val;
	int result;

	while (uio->uio_resid > 0) {
		val = bus_read_register(lr->lr_bus, lr->lr_buspos,
					  LR_REG_RAND);
		result = uiomove(&val, sizeof(val), uio);
		if (result) {
			return result;
		}
	}

	return 0;
}