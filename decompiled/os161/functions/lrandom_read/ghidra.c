int lrandom_read(void *devdata,uio *uio)

{
  int iVar1;
  uint32_t val;
  
  do {
    if (uio->uio_resid == 0) {
      return 0;
    }
                    /* WARNING: Load size is inaccurate */
    val = lamebus_read_register(*devdata,*(int *)((int)devdata + 4),0);
    iVar1 = uiomove(&val,4,uio);
  } while (iVar1 == 0);
  return iVar1;
}