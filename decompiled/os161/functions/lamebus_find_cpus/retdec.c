void lamebus_find_cpus(int32_t * lamebus2) {
    int32_t hwnum[32]; // bp-152, 0x80003a10
    int32_t v1 = lamebus_read_register(lamebus2, 31, 0x7c00); // 0x80003a34
    if (v1 == 1 == lamebus_read_register(lamebus2, 31, 0x7c04) == 1) {
        // 0x80003a60
        *(int32_t *)((int32_t)lamebus2 + 268) = 1;
        // 0x80003b68
        return;
    }
    int32_t v2 = lamebus_read_register(lamebus2, 31, 0x7e10); // 0x80003a74
    int32_t v3 = lamebus_read_register(lamebus2, 31, 0x7e18); // 0x80003a88
    int32_t v4 = (int32_t)&hwnum;
    uint32_t v5 = 0;
    int32_t v6 = 0;
    int32_t v7 = 0;
    int32_t v8 = 1;
    int32_t v9 = v6; // 0x80003aa8
    int32_t v10 = v7; // 0x80003aa8
    int32_t v11; // 0x80003a10
    if ((v8 & v2) != 0) {
        // 0x80003aac
        v10 = v7;
        if ((v8 & v3) != 0) {
            // 0x80003ab4
            *(int32_t *)(*(int32_t *)(v11 + 80) + 8) = v5;
            v10 = v6;
        }
        // 0x80003ac8
        *(int32_t *)(4 * v6 + v4) = v5;
        v9 = v6 + 1;
    }
    int32_t v12 = v10;
    uint32_t v13 = v9;
    int32_t v14 = v5 + 1; // 0x80003ad8
    int32_t v15 = 2 << v5; // 0x80003ae4
    while (v14 < 32) {
        // 0x80003aa0
        v5 = v14;
        v6 = v13;
        v7 = v12;
        v8 = v15;
        v9 = v6;
        int32_t v16 = v7; // 0x80003aa8
        if ((v8 & v2) != 0) {
            // 0x80003aac
            v10 = v7;
            if ((v8 & v3) != 0) {
                // 0x80003ab4
                *(int32_t *)(*(int32_t *)(v11 + 80) + 8) = v5;
                v10 = v6;
            }
            // 0x80003ac8
            *(int32_t *)(4 * v6 + v4) = v5;
            v9 = v6 + 1;
            v16 = v10;
        }
        // 0x80003ad8
        v12 = v16;
        v13 = v9;
        v14 = v5 + 1;
        v15 = 2 << v5;
    }
    // 0x80003b10
    if (v13 == 0) {
        // 0x80003b68
        return;
    }
    int32_t v17 = 0;
    if (v17 != v12) {
        // 0x80003af8
        cpu_create(*(int32_t *)(4 * v17 + v4));
    }
    int32_t v18 = v17 + 1; // 0x80003b0c
    int32_t v19 = 0; // 0x80003b18
    while (v18 < v13) {
        // 0x80003af0
        v17 = v18;
        if (v17 != v12) {
            // 0x80003af8
            cpu_create(*(int32_t *)(4 * v17 + v4));
        }
        // 0x80003b0c
        v18 = v17 + 1;
        v19 = 0;
    }
    int32_t v20 = *(int32_t *)(4 * v19 + v4); // 0x80003b3c
    lamebus_write_register(lamebus2, 31, 1024 * v20 + 0x8000, (int32_t)(v19 == v12));
    int32_t v21 = v19 + 1; // 0x80003b58
    v19 = v21;
    while (v21 < v13) {
        // 0x80003b24
        v20 = *(int32_t *)(4 * v19 + v4);
        lamebus_write_register(lamebus2, 31, 1024 * v20 + 0x8000, (int32_t)(v19 == v12));
        v21 = v19 + 1;
        v19 = v21;
    }
}