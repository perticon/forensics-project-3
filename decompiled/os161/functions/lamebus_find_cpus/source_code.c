lamebus_find_cpus(struct lamebus_softc *lamebus)
{
	uint32_t mainboard_vid, mainboard_did;
	uint32_t cpumask, self, bit, val;
	unsigned i, numcpus, bootcpu;
	unsigned hwnum[32];

	mainboard_vid = read_cfg_register(lamebus, LB_CONTROLLER_SLOT,
					  CFGREG_VID);
	mainboard_did = read_cfg_register(lamebus, LB_CONTROLLER_SLOT,
					  CFGREG_DID);
	if (mainboard_vid == LB_VENDOR_CS161 &&
	    mainboard_did == LBCS161_UPBUSCTL) {
		/* Old uniprocessor mainboard; no cpu registers. */
		lamebus->ls_uniprocessor = 1;
		return;
	}

	cpumask = read_ctl_register(lamebus, CTLREG_CPUS);
	self = read_ctl_register(lamebus, CTLREG_SELF);

	numcpus = 0;
	bootcpu = 0;
	for (i=0; i<32; i++) {
		bit = (uint32_t)1 << i;
		if ((cpumask & bit) != 0) {
			if (self & bit) {
				bootcpu = numcpus;
				curcpu->c_hardware_number = i;
			}
			hwnum[numcpus] = i;
			numcpus++;
		}
	}

	for (i=0; i<numcpus; i++) {
		if (i != bootcpu) {
			cpu_create(hwnum[i]);
		}
	}

	/*
	 * By default, route all interrupts only to the boot cpu. We
	 * could be arbitrarily more elaborate, up to things like
	 * dynamic load balancing.
	 */

	for (i=0; i<numcpus; i++) {
		if (i != bootcpu) {
			val = 0;
		}
		else {
			val = 0xffffffff;
		}
		write_ctlcpu_register(lamebus, hwnum[i], CTLCPU_CIRQE, val);
	}
}