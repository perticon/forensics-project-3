void lamebus_find_cpus(lamebus_softc *lamebus)

{
  uint32_t uVar1;
  uint32_t uVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  uint uVar6;
  int unaff_s7;
  uint hwnum [32];
  
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7c00);
  uVar2 = lamebus_read_register(lamebus,0x1f,0x7c04);
  if ((uVar1 == 1) && (uVar2 == 1)) {
    lamebus->ls_uniprocessor = 1;
  }
  else {
    uVar1 = lamebus_read_register(lamebus,0x1f,0x7e10);
    uVar2 = lamebus_read_register(lamebus,0x1f,0x7e18);
    uVar6 = 0;
    uVar5 = 0;
    for (uVar3 = 0; uVar4 = 1 << (uVar3 & 0x1f), uVar3 < 0x20; uVar3 = uVar3 + 1) {
      if ((uVar4 & uVar1) != 0) {
        if ((uVar4 & uVar2) != 0) {
          *(uint *)(*(int *)(unaff_s7 + 0x50) + 8) = uVar3;
          uVar6 = uVar5;
        }
        hwnum[uVar5] = uVar3;
        uVar5 = uVar5 + 1;
      }
    }
    for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
      if (uVar3 != uVar6) {
        cpu_create(hwnum[uVar3]);
      }
    }
    for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
      uVar1 = 0;
      if (uVar3 == uVar6) {
        uVar1 = 0xffffffff;
      }
      lamebus_write_register(lamebus,0x1f,(hwnum[uVar3] + 0x20) * 0x400,uVar1);
    }
  }
  return;
}