void sfs_fs_destroy(sfs_fs *sfs)

{
  array *a;
  
  if (sfs->sfs_freemap != (bitmap *)0x0) {
    bitmap_destroy(sfs->sfs_freemap);
  }
  a = (array *)sfs->sfs_vnodes;
  array_cleanup(a);
  kfree(a);
  if (sfs->sfs_device != (device *)0x0) {
    badassert("sfs->sfs_device == NULL","../../fs/sfs/sfs_fsops.c",0x10a,"sfs_fs_destroy");
  }
  kfree(sfs);
  return;
}