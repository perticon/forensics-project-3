void sfs_fs_destroy(int32_t * sfs) {
    int32_t v1 = (int32_t)sfs;
    int32_t v2 = *(int32_t *)(v1 + 532); // 0x80008168
    if (v2 != 0) {
        // 0x80008178
        bitmap_destroy((int32_t *)v2);
    }
    int32_t v3 = *(int32_t *)(v1 + 528); // 0x80008180
    array_cleanup((int32_t *)v3);
    kfree((char *)v3);
    if (*(int32_t *)(v1 + 524) != 0) {
        // 0x800081a4
        badassert("sfs->sfs_device == NULL", "../../fs/sfs/sfs_fsops.c", 266, "sfs_fs_destroy");
    }
    // 0x800081c0
    kfree((char *)sfs);
}