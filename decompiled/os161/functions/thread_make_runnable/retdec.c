void thread_make_runnable(int32_t * target, bool already_have_lock) {
    int32_t v1 = (int32_t)target;
    int32_t v2 = *(int32_t *)(v1 + 80); // 0x800168b8
    int32_t * v3 = (int32_t *)(v2 + 84);
    if (!already_have_lock) {
        // 0x800168f4
        spinlock_acquire(v3);
        goto lab_0x80016900;
    } else {
        // 0x800168c4
        if (spinlock_do_i_hold(v3)) {
            goto lab_0x80016900;
        } else {
            // 0x800168d4
            badassert("spinlock_do_i_hold(&targetcpu->c_runqueue_lock)", "../../thread/thread.c", 461, "thread_make_runnable");
            // 0x800168f4
            spinlock_acquire(v3);
            goto lab_0x80016900;
        }
    }
  lab_0x80016900:
    // 0x80016900
    *(int32_t *)(v1 + 8) = 1;
    threadlist_addtail((int32_t *)(v2 + 56), target);
    if (*(char *)(v2 + 52) != 0) {
        // 0x80016920
        int32_t v4; // 0x800168a0
        if (*(int32_t *)*(int32_t *)(v4 + 80) != v2) {
            // 0x80016938
            ipi_send((int32_t *)v2, 2);
        }
    }
    if (!already_have_lock) {
        // 0x80016948
        spinlock_release(v3);
    }
}