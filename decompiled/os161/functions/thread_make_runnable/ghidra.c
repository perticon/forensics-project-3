void thread_make_runnable(thread *target,bool already_have_lock)

{
  undefined3 extraout_var;
  bool bVar1;
  undefined3 in_register_00000014;
  cpu *target_00;
  int unaff_s7;
  
  target_00 = target->t_cpu;
  if (CONCAT31(in_register_00000014,already_have_lock) != 0) {
    bVar1 = spinlock_do_i_hold(&target_00->c_runqueue_lock);
    if (CONCAT31(extraout_var,bVar1) != 0) goto LAB_80016900;
    badassert("spinlock_do_i_hold(&targetcpu->c_runqueue_lock)","../../thread/thread.c",0x1cd,
              "thread_make_runnable");
  }
  spinlock_acquire(&target_00->c_runqueue_lock);
LAB_80016900:
  target->t_state = S_READY;
  threadlist_addtail(&target_00->c_runqueue,target);
  if ((target_00->c_isidle != false) && (**(cpu ***)(unaff_s7 + 0x50) != target_00)) {
    ipi_send(target_00,2);
  }
  if (CONCAT31(in_register_00000014,already_have_lock) == 0) {
    spinlock_release(&target_00->c_runqueue_lock);
  }
  return;
}