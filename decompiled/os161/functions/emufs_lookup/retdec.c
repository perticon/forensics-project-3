int32_t emufs_lookup(int32_t * dir, char * pathname, int32_t * ret) {
    int32_t v1 = (int32_t)dir;
    int32_t v2 = *(int32_t *)(v1 + 16); // 0x800036c8
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x800036d4
    int32_t * v4 = (int32_t *)(v2 + 24); // 0x800036d8
    int32_t v5 = *(int32_t *)(v2 + 28); // 0x800036dc
    int32_t handle; // bp-28, 0x800036ac
    int32_t isdir; // bp-24, 0x800036ac
    int32_t result = emu_open((int32_t *)*v4, v5, pathname, false, false, 0, &handle, &isdir); // 0x80003700
    if (result != 0) {
        // 0x8000374c
        return result;
    }
    // 0x8000370c
    int32_t * newguy; // bp-32, 0x800036ac
    int32_t v6 = emufs_loadvnode((int32_t *)v3, handle, isdir, &newguy); // 0x80003718
    int32_t result2; // 0x800036ac
    if (v6 == 0) {
        // 0x8000373c
        *ret = (int32_t)newguy;
        result2 = 0;
    } else {
        // 0x80003724
        emu_close((int32_t *)*v4, handle);
        result2 = v6;
    }
    // 0x8000374c
    return result2;
}