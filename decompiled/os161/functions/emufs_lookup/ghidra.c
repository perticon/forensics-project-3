int emufs_lookup(vnode *dir,char *pathname,vnode **ret)

{
  int iVar1;
  void *pvVar2;
  emufs_fs *ef;
  emufs_vnode *newguy;
  uint32_t handle;
  int isdir;
  
  pvVar2 = dir->vn_data;
  ef = (emufs_fs *)dir->vn_fs->fs_data;
  iVar1 = emu_open(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),pathname,
                   false,false,0,&handle,&isdir);
  if (iVar1 == 0) {
    iVar1 = emufs_loadvnode(ef,handle,isdir,&newguy);
    if (iVar1 == 0) {
      *ret = &newguy->ev_v;
      iVar1 = 0;
    }
    else {
      emu_close(*(emu_softc **)((int)pvVar2 + 0x18),handle);
    }
  }
  return iVar1;
}