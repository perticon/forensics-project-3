void visitR(link h, link z)
{
    if (h == z)
        return;

    visitR(h->next, z);

    return;
}