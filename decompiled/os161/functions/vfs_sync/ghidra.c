int vfs_sync(void)

{
  knowndevarray *pkVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  
  vfs_biglock_acquire();
  uVar4 = (knowndevs->arr).num;
  for (uVar3 = 0; uVar3 < uVar4; uVar3 = uVar3 + 1) {
    pkVar1 = knowndevs;
    if ((knowndevs->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    iVar2 = *(int *)((int)(pkVar1->arr).v[uVar3] + 0x10);
    if (iVar2 - 1U < 0xfffffffe) {
      (***(code ***)(iVar2 + 4))();
    }
  }
  vfs_biglock_release();
  return 0;
}