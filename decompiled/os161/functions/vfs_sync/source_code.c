vfs_sync(void)
{
	struct knowndev *dev;
	unsigned i, num;

	vfs_biglock_acquire();

	num = knowndevarray_num(knowndevs);
	for (i=0; i<num; i++) {
		dev = knowndevarray_get(knowndevs, i);
		if (dev->kd_fs != NULL && dev->kd_fs != SWAP_FS) {
			/*result =*/ FSOP_SYNC(dev->kd_fs);
		}
	}

	vfs_biglock_release();

	return 0;
}