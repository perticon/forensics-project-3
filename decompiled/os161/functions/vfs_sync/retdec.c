int32_t vfs_sync(void) {
    // 0x80018ea8
    vfs_biglock_acquire();
    int32_t v1 = 0; // 0x80018f64
    if (bootfs_vnode == NULL) {
        // 0x80018f68
        vfs_biglock_release();
        return 0;
    }
    if (v1 >= (int32_t)bootfs_vnode) {
        // 0x80018ef8
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
    }
    // 0x80018f14
    v1++;
    while (v1 < (int32_t)bootfs_vnode) {
        if (v1 >= (int32_t)bootfs_vnode) {
            // 0x80018ef8
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
        }
        // 0x80018f14
        v1++;
    }
    // 0x80018f68
    vfs_biglock_release();
    return 0;
}