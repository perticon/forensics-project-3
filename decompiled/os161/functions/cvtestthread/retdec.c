void cvtestthread(char * junk, int32_t num) {
    int32_t v1 = 0; // 0x80012f70
    lock_acquire(testlock);
    int32_t ts1; // bp-64, 0x80012e1c
    int32_t ts2; // bp-48, 0x80012e1c
    int32_t v2; // 0x80012e1c
    if (testval1 != num) {
        gettime(&ts1);
        cv_wait(testcv, testlock);
        gettime(&ts2);
        timespec_sub(&ts2, &ts1, &ts2);
        if (v2 < 0x13880 && (ts2 || v2) == 0) {
            // 0x80012ed8
            kprintf("cv_wait took only %u ns\n", v2);
            kprintf("That's too fast... you must be busy-looping\n");
            V((int32_t *)g16);
            thread_exit();
        }
        while (testval1 != num) {
            // 0x80012e7c
            gettime(&ts1);
            cv_wait(testcv, testlock);
            gettime(&ts2);
            timespec_sub(&ts2, &ts1, &ts2);
            if (v2 < 0x13880 && (ts2 || v2) == 0) {
                // 0x80012ed8
                kprintf("cv_wait took only %u ns\n", v2);
                kprintf("That's too fast... you must be busy-looping\n");
                V((int32_t *)g16);
                thread_exit();
            }
        }
    }
    // 0x80012f14
    kprintf("Thread %lu\n", num);
    testval1 = (testval1 + 31) % 32;
    v1++;
    cv_broadcast(testcv, testlock);
    lock_release(testlock);
    while (v1 < 5) {
        // 0x80012e68
        lock_acquire(testlock);
        if (testval1 != num) {
            gettime(&ts1);
            cv_wait(testcv, testlock);
            gettime(&ts2);
            timespec_sub(&ts2, &ts1, &ts2);
            if (v2 < 0x13880 && (ts2 || v2) == 0) {
                // 0x80012ed8
                kprintf("cv_wait took only %u ns\n", v2);
                kprintf("That's too fast... you must be busy-looping\n");
                V((int32_t *)g16);
                thread_exit();
            }
            while (testval1 != num) {
                // 0x80012e7c
                gettime(&ts1);
                cv_wait(testcv, testlock);
                gettime(&ts2);
                timespec_sub(&ts2, &ts1, &ts2);
                if (v2 < 0x13880 && (ts2 || v2) == 0) {
                    // 0x80012ed8
                    kprintf("cv_wait took only %u ns\n", v2);
                    kprintf("That's too fast... you must be busy-looping\n");
                    V((int32_t *)g16);
                    thread_exit();
                }
            }
        }
        // 0x80012f14
        kprintf("Thread %lu\n", num);
        testval1 = (testval1 + 31) % 32;
        v1++;
        cv_broadcast(testcv, testlock);
        lock_release(testlock);
    }
    // 0x80012f8c
    V((int32_t *)g16);
}