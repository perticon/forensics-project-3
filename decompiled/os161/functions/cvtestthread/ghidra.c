void cvtestthread(void *junk,ulong num)

{
  int iVar1;
  int j;
  timespec ts1;
  timespec ts2;
  
  iVar1 = 0;
  do {
    if (4 < iVar1) {
      V(donesem);
      return;
    }
    lock_acquire(testlock);
    while (testval1 != num) {
      gettime(&ts1);
      cv_wait(testcv,testlock);
      gettime(&ts2);
      timespec_sub(&ts2,&ts1,&ts2);
      if (((ts2.tv_sec._0_4_ | ts2.tv_sec._4_4_) == 0) && (ts2.tv_nsec < 80000)) {
        kprintf("cv_wait took only %u ns\n");
        kprintf("That\'s too fast... you must be busy-looping\n");
        V(donesem);
                    /* WARNING: Subroutine does not return */
        thread_exit();
      }
    }
    kprintf("Thread %lu\n",num);
    testval1 = testval1 + 0x1f & 0x1f;
    for (j = 0; j < 3000; j = j + 1) {
    }
    iVar1 = iVar1 + 1;
    cv_broadcast(testcv,testlock);
    lock_release(testlock);
  } while( true );
}