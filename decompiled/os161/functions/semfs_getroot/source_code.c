semfs_getroot(struct fs *fs, struct vnode **ret)
{
	struct semfs *semfs = fs->fs_data;
	struct vnode *vn;
	int result;

	result = semfs_getvnode(semfs, SEMFS_ROOTDIR, &vn);
	if (result) {
		kprintf("semfs: couldn't load root vnode: %s\n",
			strerror(result));
		return result;
	}
	*ret = vn;
	return 0;
}