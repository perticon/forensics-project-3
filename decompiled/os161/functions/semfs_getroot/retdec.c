int32_t semfs_getroot(int32_t * fs, int32_t ** ret) {
    // 0x800056ac
    int32_t * vn; // bp-24, 0x800056ac
    int32_t result = semfs_getvnode(fs, -1, (int32_t *)&vn); // 0x800056cc
    if (result == 0) {
        // 0x800056f8
        *(int32_t *)ret = (int32_t)vn;
    } else {
        // 0x800056d8
        kprintf("semfs: couldn't load root vnode: %s\n", strerror(result));
    }
    // 0x80005708
    return result;
}