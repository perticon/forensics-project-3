int semfs_getroot(fs *fs,vnode **ret)

{
  int errcode;
  char *pcVar1;
  vnode *vn;
  
  errcode = semfs_getvnode((semfs *)fs->fs_data,0xffffffff,&vn);
  if (errcode == 0) {
    *ret = vn;
    errcode = 0;
  }
  else {
    pcVar1 = strerror(errcode);
    kprintf("semfs: couldn\'t load root vnode: %s\n",pcVar1);
  }
  return errcode;
}