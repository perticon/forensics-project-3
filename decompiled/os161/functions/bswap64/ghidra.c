uint64_t bswap64(uint64_t val)

{
  uint in_a0;
  uint in_a1;
  
  return CONCAT44(in_a1 << 0x18 | (in_a1 & 0xff00) << 8 | (in_a1 & 0xff0000) >> 8 | in_a1 >> 0x18 |
                  (in_a0 & 0xff) << 8,(in_a0 & 0xff0000) >> 8 | in_a0 >> 0x18);
}