int64_t bswap64(int64_t val) {
    // 0x8000b020
    int32_t v1; // 0x8000b020
    return llvm_bswap_i32(v1) | 256 * (int32_t)val & 0xff00;
}