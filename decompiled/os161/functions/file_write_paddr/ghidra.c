int file_write_paddr(vnode *vn,paddr_t buf_ptr,size_t size,off_t offset)

{
  int iVar1;
  iovec iov;
  uio u;
  
  iov.field_0 = buf_ptr + 0x80000000;
  u.uio_iov = &iov;
  u.uio_iovcnt = 1;
  u.uio_segflg = UIO_SYSSPACE;
  u.uio_rw = UIO_WRITE;
  u.uio_space = (addrspace *)0x0;
  iov.iov_len = size;
  u.uio_resid = size;
  u.uio_offset = offset;
  vnode_check(vn,s_console_write_80022690 + 8);
  iVar1 = (*vn->vn_ops->vop_write)(vn,&u);
  if (iVar1 == 0) {
    iVar1 = size - u.uio_resid;
  }
  return iVar1;
}