int32_t file_write_paddr(int32_t * vn, int32_t buf_ptr, int32_t size, int64_t offset) {
    // 0x8001d810
    vnode_check(vn, "write");
    return *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 24);
}