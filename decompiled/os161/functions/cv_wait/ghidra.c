void cv_wait(cv *cv,lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *pcVar2;
  char *lock_00;
  spinlock *splk;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar2 = "lock != NULL";
    lock_00 = "../../thread/synch.c";
    badassert("lock != NULL","../../thread/synch.c",0x14d,"cv_wait");
    cv = (cv *)pcVar2;
  }
  if (cv == (cv *)0x0) {
    lock_00 = "../../thread/synch.c";
    badassert("cv != NULL","../../thread/synch.c",0x14e,"cv_wait");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  splk = &cv->cv_lock;
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0x14f,"cv_wait");
  }
  spinlock_acquire(splk);
  lock_release(lock);
  wchan_sleep(cv->cv_wchan,splk);
  spinlock_release(splk);
  lock_acquire(lock);
  return;
}