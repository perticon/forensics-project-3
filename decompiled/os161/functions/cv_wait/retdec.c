void cv_wait(int32_t * cv, int32_t * lock) {
    int32_t v1; // 0x80015bdc
    if (lock == NULL) {
        // 0x80015c18
        badassert("lock != NULL", "../../thread/synch.c", 333, "cv_wait");
        v1 = (int32_t)"../../thread/synch.c";
    } else {
        // 0x80015c18
        v1 = (int32_t)lock;
        if (cv == NULL) {
            // 0x80015c20
            badassert("cv != NULL", "../../thread/synch.c", 334, "cv_wait");
            v1 = (int32_t)"../../thread/synch.c";
        }
    }
    // 0x80015c40
    if (!lock_do_i_hold((int32_t *)v1)) {
        // 0x80015c50
        badassert("lock_do_i_hold(lock)", "../../thread/synch.c", 335, "cv_wait");
    }
    // 0x80015c70
    spinlock_acquire((int32_t *)"NULL");
    lock_release(lock);
    wchan_sleep((int32_t *)0x203d2120, (int32_t *)"NULL");
    spinlock_release((int32_t *)"NULL");
    lock_acquire(lock);
}