cv_wait(struct cv *cv, struct lock *lock)
{
        // Write this
#if OPT_SYNCH
        KASSERT(lock != NULL);
	KASSERT(cv != NULL);
	KASSERT(lock_do_i_hold(lock));

	spinlock_acquire(&cv->cv_lock);
	/* G.Cabodi - 2019: spinlock already owned as atomic lock_release+wchan_sleep
	   needed */
	lock_release(lock);
	wchan_sleep(cv->cv_wchan,&cv->cv_lock);
	spinlock_release(&cv->cv_lock);
	/* G.Cabodi - 2019: spinlock already  released to avoid ownership while
	   (possibly) going to wait state in lock_acquire. 
	   Atomicity wakeup+lock_acquire not guaranteed (but not necessary!) */
	lock_acquire(lock);
#endif

        (void)cv;    // suppress warning until code gets written
        (void)lock;  // suppress warning until code gets written
}