int vfs_mkdir(char *path,mode_t mode)

{
  int iVar1;
  vnode *parent;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&parent,name,0x100);
  if (iVar1 == 0) {
    vnode_check(parent,"mkdir");
    iVar1 = (*parent->vn_ops->vop_mkdir)(parent,name,mode);
    vnode_decref(parent);
  }
  return iVar1;
}