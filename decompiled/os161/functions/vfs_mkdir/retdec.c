int32_t vfs_mkdir(char * path, int32_t mode) {
    char name[256]; // bp-268, 0x8001a57c
    // 0x8001a57c
    int32_t * parent; // bp-272, 0x8001a57c
    int32_t v1 = vfs_lookparent(path, (int32_t *)&parent, name, 256); // 0x8001a598
    int32_t result = v1; // 0x8001a5a0
    if (v1 == 0) {
        // 0x8001a5a4
        vnode_check(parent, "mkdir");
        result = *(int32_t *)(*(int32_t *)((int32_t)parent + 20) + 68);
        vnode_decref(parent);
    }
    // 0x8001a5e0
    return result;
}