vfs_mkdir(char *path, mode_t mode)
{
	struct vnode *parent;
	char name[NAME_MAX+1];
	int result;

	result = vfs_lookparent(path, &parent, name, sizeof(name));
	if (result) {
		return result;
	}

	result = VOP_MKDIR(parent, name, mode);

	VOP_DECREF(parent);

	return result;
}