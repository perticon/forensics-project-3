int sfs_namefile(vnode *vv,uio *uio)

{
  if (*(int *)((int)vv->vn_data + 0x218) != 1) {
    badassert("sv->sv_ino == SFS_ROOTDIR_INO","../../fs/sfs/sfs_vnops.c",0x10c,"sfs_namefile");
  }
  return 0;
}