sfs_namefile(struct vnode *vv, struct uio *uio)
{
	struct sfs_vnode *sv = vv->vn_data;
	KASSERT(sv->sv_ino == SFS_ROOTDIR_INO);

	/* send back the empty string - just return */

	(void)uio;

	return 0;
}