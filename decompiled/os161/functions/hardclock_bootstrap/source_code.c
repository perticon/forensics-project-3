hardclock_bootstrap(void)
{
	spinlock_init(&lbolt_lock);
	lbolt = wchan_create("lbolt");
	if (lbolt == NULL) {
		panic("Couldn't create lbolt\n");
	}
}