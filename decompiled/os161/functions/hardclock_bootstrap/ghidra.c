void hardclock_bootstrap(void)

{
  spinlock_init(&lbolt_lock);
  lbolt = wchan_create("lbolt");
  if (lbolt == (wchan *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Couldn\'t create lbolt\n");
  }
  return;
}