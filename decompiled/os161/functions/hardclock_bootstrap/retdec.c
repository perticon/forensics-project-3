void hardclock_bootstrap(void) {
    // 0x80014ed0
    spinlock_init(&lbolt_lock);
    int32_t * v1 = wchan_create("lbolt"); // 0x80014eec
    *(int32_t *)&lbolt = (int32_t)v1;
    if (v1 == NULL) {
        // 0x80014efc
        panic("Couldn't create lbolt\n");
    }
}