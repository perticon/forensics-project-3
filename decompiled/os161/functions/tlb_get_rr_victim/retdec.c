int32_t tlb_get_rr_victim(void) {
    int32_t result = g21; // 0x8001e29c
    g21 = (result + 1) % 64;
    increase(2);
    return result;
}