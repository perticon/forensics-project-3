int tlb_get_rr_victim(void)

{
  uint uVar1;
  
  uVar1 = tlb_get_rr_victim::next_victim;
  tlb_get_rr_victim::next_victim = tlb_get_rr_victim::next_victim + 1 & 0x3f;
  increase(2);
  return uVar1;
}