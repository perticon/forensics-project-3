bool spinlock_not_held(spinlock *splk)

{
  return splk->splk_holder == (cpu *)0x0;
}