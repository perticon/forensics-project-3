bool spinlock_not_held(int32_t * splk) {
    // 0x800114c0
    return *(int32_t *)((int32_t)splk + 4) == 0;
}