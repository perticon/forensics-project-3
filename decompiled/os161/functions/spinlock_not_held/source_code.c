spinlock_not_held(struct spinlock *splk)
{
	return splk->splk_holder == NULL;
}