int sfs_getroot(fs *fs,vnode **ret)

{
  int iVar1;
  sfs_fs *sfs;
  sfs_vnode *sv;
  
  sfs = (sfs_fs *)fs->fs_data;
  vfs_biglock_acquire();
  iVar1 = sfs_loadvnode(sfs,1,0,&sv);
  if (iVar1 == 0) {
    if ((sv->sv_i).sfi_type == 2) {
      vfs_biglock_release();
      *ret = &sv->sv_absvn;
      iVar1 = 0;
    }
    else {
      kprintf("sfs: %s: getroot: not directory (type %u)\n",(sfs->sfs_sb).sb_volname);
      vfs_biglock_release();
      iVar1 = 8;
    }
  }
  else {
    kprintf("sfs: %s: getroot: Cannot load root vnode\n",(sfs->sfs_sb).sb_volname);
    vfs_biglock_release();
  }
  return iVar1;
}