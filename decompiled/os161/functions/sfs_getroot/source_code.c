sfs_getroot(struct fs *fs, struct vnode **ret)
{
	struct sfs_fs *sfs = fs->fs_data;
	struct sfs_vnode *sv;
	int result;

	vfs_biglock_acquire();

	result = sfs_loadvnode(sfs, SFS_ROOTDIR_INO, SFS_TYPE_INVAL, &sv);
	if (result) {
		kprintf("sfs: %s: getroot: Cannot load root vnode\n",
			sfs->sfs_sb.sb_volname);
		vfs_biglock_release();
		return result;
	}

	if (sv->sv_i.sfi_type != SFS_TYPE_DIR) {
		kprintf("sfs: %s: getroot: not directory (type %u)\n",
			sfs->sfs_sb.sb_volname, sv->sv_i.sfi_type);
		vfs_biglock_release();
		return EINVAL;
	}

	vfs_biglock_release();

	*ret = &sv->sv_absvn;
	return 0;
}