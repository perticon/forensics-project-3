int32_t sfs_getroot(int32_t * fs, int32_t ** ret) {
    int32_t v1 = (int32_t)fs;
    vfs_biglock_acquire();
    int32_t * sv; // bp-24, 0x8000899c
    int32_t result = sfs_loadvnode(fs, 1, 0, &sv); // 0x800089cc
    if (result != 0) {
        // 0x800089d8
        kprintf("sfs: %s: getroot: Cannot load root vnode\n", (char *)(v1 + 16));
        vfs_biglock_release();
        // 0x80008a44
        return result;
    }
    uint16_t v2 = *(int16_t *)((int32_t)sv + 28); // 0x80008a00
    int32_t result2; // 0x8000899c
    if (v2 == 2) {
        // 0x80008a2c
        vfs_biglock_release();
        *(int32_t *)ret = (int32_t)sv;
        result2 = 0;
    } else {
        // 0x80008a10
        kprintf("sfs: %s: getroot: not directory (type %u)\n", (char *)(v1 + 16), (int32_t)v2);
        vfs_biglock_release();
        result2 = 8;
    }
    // 0x80008a44
    return result2;
}