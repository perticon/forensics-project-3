void enter_new_process(int argc,userptr_t argv,userptr_t env,vaddr_t stack,vaddr_t entry)

{
  char *tf_00;
  char *ctf;
  char *whence;
  uint32_t uVar1;
  int unaff_s7;
  uint32_t uStack208;
  uint32_t auStack204 [2];
  vaddr_t vStack196;
  userptr_t p_Stack192;
  undefined4 uStack188;
  trapframe tf;
  
  bzero(&tf,0x8c);
  tf.tf_status = 0xff0c;
  tf.tf_epc = entry;
  uStack188 = 0x8001ee08;
  tf_00 = (char *)&tf;
  tf.tf_a0 = argc;
  tf.tf_a1 = (uint32_t)argv;
  tf.tf_a2 = (uint32_t)env;
  tf.tf_sp = stack;
  mips_usermode((trapframe *)tf_00);
  vStack196 = stack;
  p_Stack192 = env;
  if (unaff_s7 == 0) {
    tf_00 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
  }
  ctf = tf_00;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    ctf = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    ctf = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar1 = *(uint32_t *)((int)ctf + 0x1c);
  uStack208 = 0;
  switch(uVar1) {
  case 0:
    auStack204[0] = sys_fork((trapframe *)ctf,(pid_t *)&uStack208);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar1);
    if (uVar1 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    auStack204[0] = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)ctf + 0x24));
    auStack204[0] = 0;
    break;
  case 4:
    uStack208 = sys_waitpid(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                            *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 5:
    uStack208 = sys_getpid();
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x2d:
    uStack208 = sys_open(*(userptr_t *)((int)ctf + 0x24),*(uint32_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c),(int *)auStack204);
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x31:
    uStack208 = sys_close(*(uint32_t *)((int)ctf + 0x24));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x32:
    uStack208 = sys_read(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x37:
    uStack208 = sys_write(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                          *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack208 < 0) {
      auStack204[0] = 1;
    }
    else {
      auStack204[0] = 0;
    }
    break;
  case 0x3b:
    auStack204[0] =
         sys_lseek(*(uint32_t *)((int)ctf + 0x24),
                   CONCAT44(*(uint32_t *)((int)tf_00 + 0x2c),&uStack208),(int)whence,(int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    auStack204[0] = sys___time(*(userptr_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28));
    break;
  case 0x77:
    auStack204[0] = sys_reboot(*(uint32_t *)((int)ctf + 0x24));
  }
  if (auStack204[0] == 0) {
    *(uint32_t *)((int)tf_00 + 0x1c) = uStack208;
    *(uint32_t *)((int)tf_00 + 0x30) = 0;
  }
  else {
    *(uint32_t *)((int)tf_00 + 0x1c) = auStack204[0];
    *(uint32_t *)((int)tf_00 + 0x30) = 1;
  }
  *(uint32_t *)((int)tf_00 + 0x88) = *(uint32_t *)((int)tf_00 + 0x88) + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}