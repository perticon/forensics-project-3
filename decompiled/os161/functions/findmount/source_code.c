findmount(const char *devname, struct knowndev **result)
{
	struct knowndev *dev;
	unsigned i, num;
	bool found = false;

	KASSERT(vfs_biglock_do_i_hold());

	num = knowndevarray_num(knowndevs);
	for (i=0; !found && i<num; i++) {
		dev = knowndevarray_get(knowndevs, i);
		if (dev->kd_rawname==NULL) {
			/* not mountable/unmountable */
			continue;
		}

		if (!strcmp(devname, dev->kd_name)) {
			*result = dev;
			found = true;
		}
	}

	return found ? 0 : ENODEV;
}