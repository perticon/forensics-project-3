int32_t findmount(char * devname, int32_t ** result) {
    bool v1 = vfs_biglock_do_i_hold(); // 0x80018da0
    int32_t v2 = (int32_t)&g35; // 0x80018da8
    if (!v1) {
        // 0x80018dac
        badassert("vfs_biglock_do_i_hold()", "../../vfs/vfslist.c", 512, "findmount");
        v2 = &g41;
    }
    uint32_t v3 = *(int32_t *)(*(int32_t *)(v2 - 0x7374) + 4); // 0x80018dd4
    if (v3 == 0) {
        // .loopexit
        return 25;
    }
    int32_t v4 = 0; // 0x80018e60
    int32_t v5 = (int32_t)knowndevs; // 0x80018e00
    if (v4 >= (int32_t)bootfs_vnode) {
        // 0x80018e04
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v5 = &g41;
    }
    int32_t v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v4); // 0x80018e2c
    int32_t result2; // 0x80018d74
    if (*(int32_t *)(v6 + 4) != 0) {
        // 0x80018e44
        if (strcmp(devname, (char *)*(int32_t *)v6) == 0) {
            // 0x80018e74
            *(int32_t *)result = v6;
            result2 = 0;
            return result2;
        }
    }
    // 0x80018e60
    v4++;
    result2 = 25;
    while (v4 < v3) {
        // 0x80018de8
        v5 = (int32_t)knowndevs;
        if (v4 >= (int32_t)bootfs_vnode) {
            // 0x80018e04
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v5 = &g41;
        }
        // 0x80018e20
        v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v4);
        if (*(int32_t *)(v6 + 4) != 0) {
            // 0x80018e44
            if (strcmp(devname, (char *)*(int32_t *)v6) == 0) {
                // 0x80018e74
                *(int32_t *)result = v6;
                result2 = 0;
                return result2;
            }
        }
        // 0x80018e60
        v4++;
        result2 = 25;
    }
  lab__loopexit:
    // .loopexit
    return result2;
}