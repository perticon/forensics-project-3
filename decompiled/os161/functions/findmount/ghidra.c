int findmount(char *devname,knowndev **result)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar4;
  int iVar2;
  knowndevarray *pkVar3;
  uint uVar5;
  knowndev *pkVar6;
  uint uVar7;
  
  bVar4 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x200,"findmount");
  }
  uVar7 = *(uint *)(*(int *)(iVar2 + -0x7374) + 4);
  bVar1 = false;
  for (uVar5 = 0; (!bVar1 && (uVar5 < uVar7)); uVar5 = uVar5 + 1) {
    pkVar3 = knowndevs;
    if ((knowndevs->arr).num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pkVar6 = (knowndev *)(pkVar3->arr).v[uVar5];
    if ((pkVar6->kd_rawname != (char *)0x0) && (iVar2 = strcmp(devname,pkVar6->kd_name), iVar2 == 0)
       ) {
      *result = pkVar6;
      bVar1 = true;
    }
  }
  iVar2 = 0;
  if (!bVar1) {
    iVar2 = 0x19;
  }
  return iVar2;
}