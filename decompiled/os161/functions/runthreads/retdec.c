void runthreads(int32_t doloud) {
    char name[16]; // bp-40, 0x80014400
    int32_t v1 = doloud == 0 ? (int32_t)&g2 + 0x44dc : (int32_t)&g2 + 0x4550;
    void (*v2)(char *, int32_t) = (void (*)(char *, int32_t))v1;
    int32_t v3 = 0;
    snprintf(name, 16, "threadtest%d", v3);
    int32_t v4 = thread_fork(name, NULL, v2, NULL, v3); // 0x80014464
    int32_t v5 = v4; // 0x8001446c
    int32_t v6; // 0x80014464
    if (v4 != 0) {
        panic("threadtest: thread_fork failed %s)\n", strerror(v5));
        snprintf(name, 16, "threadtest%d", v3);
        v6 = thread_fork(name, NULL, v2, NULL, v3);
        v5 = v6;
        while (v6 != 0) {
            // 0x80014470
            panic("threadtest: thread_fork failed %s)\n", strerror(v5));
            snprintf(name, 16, "threadtest%d", v3);
            v6 = thread_fork(name, NULL, v2, NULL, v3);
            v5 = v6;
        }
    }
    int32_t v7 = v3 + 1; // 0x8001446c
    while (v7 < 8) {
        // 0x80014438
        v3 = v7;
        snprintf(name, 16, "threadtest%d", v3);
        v4 = thread_fork(name, NULL, v2, NULL, v3);
        v5 = v4;
        if (v4 != 0) {
            panic("threadtest: thread_fork failed %s)\n", strerror(v5));
            snprintf(name, 16, "threadtest%d", v3);
            v6 = thread_fork(name, NULL, v2, NULL, v3);
            v5 = v6;
            while (v6 != 0) {
                // 0x80014470
                panic("threadtest: thread_fork failed %s)\n", strerror(v5));
                snprintf(name, 16, "threadtest%d", v3);
                v6 = thread_fork(name, NULL, v2, NULL, v3);
                v5 = v6;
            }
        }
        // 0x8001448c
        v7 = v3 + 1;
    }
    int32_t v8 = 1; // 0x800144ac
    P(tsem);
    int32_t v9 = v8; // 0x800144b8
    while (v8 < 8) {
        // 0x800144a4
        v8 = v9 + 1;
        P(tsem);
        v9 = v8;
    }
}