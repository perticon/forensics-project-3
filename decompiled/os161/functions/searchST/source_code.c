Item searchST(link t, Key k, link z)
{
    int comparison;

    if (t == z)
        return ITEMsetnull();

    Key KEY = KEYget(&(t->item));

    comparison = (KEYcompare(KEY, k));

    if (comparison == 0)
        return &t->item;

    return (searchST(t->next, k, z));
}