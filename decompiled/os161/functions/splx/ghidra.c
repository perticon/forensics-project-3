int splx(int spl)

{
  int iVar1;
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    iVar1 = *(int *)(unaff_s7 + 0x5c);
    if (iVar1 < spl) {
      splraise(iVar1,spl);
      iVar1 = *(int *)(unaff_s7 + 0x5c);
      *(int *)(unaff_s7 + 0x5c) = spl;
      spl = iVar1;
    }
    else if (spl < iVar1) {
      *(int *)(unaff_s7 + 0x5c) = spl;
      spllower(iVar1,spl);
      spl = iVar1;
    }
  }
  return spl;
}