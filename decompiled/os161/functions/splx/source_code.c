splx(int spl)
{
	struct thread *cur = curthread;
	int ret;

	if (!CURCPU_EXISTS()) {
		/* before curcpu initialization; interrupts are off anyway */
		return spl;
	}

	if (cur->t_curspl < spl) {
		/* turning interrupts off */
		splraise(cur->t_curspl, spl);
		ret = cur->t_curspl;
		cur->t_curspl = spl;
	}
	else if (cur->t_curspl > spl) {
		/* turning interrupts on */
		ret = cur->t_curspl;
		cur->t_curspl = spl;
		spllower(ret, spl);
	}
	else {
		/* do nothing */
		ret = spl;
	}

	return ret;
}