int32_t splx(int32_t spl) {
    // 0x80015464
    int32_t v1; // 0x80015464
    if (v1 == 0) {
        // 0x800154d4
        return spl;
    }
    int32_t * v2 = (int32_t *)(v1 + 92); // 0x80015480
    int32_t v3 = *v2; // 0x80015480
    if (v3 < spl) {
        // 0x80015494
        splraise(v3, spl);
        *v2 = spl;
        // 0x800154d4
        return *v2;
    }
    // 0x800154b0
    if (spl > spl) {
        // 0x800154bc
        *v2 = spl;
        spllower(spl, spl);
    }
    // 0x800154d4
    return spl;
}