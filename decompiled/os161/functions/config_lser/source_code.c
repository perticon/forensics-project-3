config_lser(struct lser_softc *sc, int lserno)
{
	(void)lserno;

	/*
	 * Enable interrupting.
	 */

	spinlock_init(&sc->ls_lock);
	sc->ls_wbusy = false;

	bus_write_register(sc->ls_busdata, sc->ls_buspos,
			   LSER_REG_RIRQ, LSER_IRQ_ENABLE);
	bus_write_register(sc->ls_busdata, sc->ls_buspos,
			   LSER_REG_WIRQ, LSER_IRQ_ENABLE);

	return 0;
}