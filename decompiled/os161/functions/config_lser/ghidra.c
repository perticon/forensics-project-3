int config_lser(lser_softc *sc,int lserno)

{
  spinlock_init(&sc->ls_lock);
  sc->ls_wbusy = false;
  lamebus_write_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,8,1);
  lamebus_write_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,4,1);
  return 0;
}