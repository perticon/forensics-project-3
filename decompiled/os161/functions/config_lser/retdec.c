int32_t config_lser(int32_t * sc, int32_t lserno) {
    int32_t v1 = (int32_t)sc;
    spinlock_init(sc);
    *(char *)(v1 + 8) = 0;
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x80004e9c
    int32_t * v3 = (int32_t *)(v1 + 16); // 0x80004ea0
    lamebus_write_register((int32_t *)*v2, *v3, 8, 1);
    lamebus_write_register((int32_t *)*v2, *v3, 4, 1);
    return 0;
}