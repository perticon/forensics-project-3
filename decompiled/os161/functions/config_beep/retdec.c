int32_t config_beep(int32_t * bs, int32_t unit) {
    if (unit != 0) {
        // 0x80001ae0
        return 25;
    }
    int32_t v1 = (int32_t)bs; // 0x80001aa8
    if (the_beep != NULL) {
        // 0x80001aac
        badassert("the_beep==NULL", "../../dev/generic/beep.c", 57, "config_beep");
        v1 = (int32_t)"the_beep==NULL";
    }
    // 0x80001ad0
    *(int32_t *)&the_beep = v1;
    while (true) {
        // continue -> 0x80001ad0
    }
}