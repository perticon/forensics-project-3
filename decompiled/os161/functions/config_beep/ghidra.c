int config_beep(beep_softc *bs,int unit)

{
  char *pcVar1;
  
  if (unit == 0) {
    if (the_beep != (beep_softc *)0x0) {
      pcVar1 = "the_beep==NULL";
      badassert("the_beep==NULL","../../dev/generic/beep.c",0x39,"config_beep");
      bs = (beep_softc *)pcVar1;
    }
    the_beep = bs;
    return 0;
  }
  return 0x19;
}