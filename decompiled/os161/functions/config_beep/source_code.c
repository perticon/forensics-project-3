config_beep(struct beep_softc *bs, int unit)
{
	/* We use only the first beep device. */
	if (unit!=0) {
		return ENODEV;
	}

	KASSERT(the_beep==NULL);
	the_beep = bs;
	return 0;
}