proc_create(const char *name)
{
	struct proc *proc;

	proc = kmalloc(sizeof(*proc));
	if (proc == NULL)
	{
		return NULL;
	}
	proc->p_name = kstrdup(name);
	if (proc->p_name == NULL)
	{
		kfree(proc);
		return NULL;
	}

	proc->p_numthreads = 0;
	spinlock_init(&proc->p_lock);

	/* VM fields */
	proc->p_addrspace = NULL;

	/* VFS fields */
	proc->p_cwd = NULL;

	/* status init */
	#if OPT_WAITPID
	proc->finish = 0;
	#endif

	#if OPT_PAGING
	proc->last_victim=-1;
	#endif

	proc_init_waitpid(proc, name);

#if OPT_PAGING
	bzero(proc->fileTable, OPEN_MAX * sizeof(struct openfile *));
#endif
	return proc;
}