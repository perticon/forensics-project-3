proc * proc_create(char *name)

{
  proc *proc;
  char *pcVar1;
  
  proc = (proc *)kmalloc(0x264);
  if (proc == (proc *)0x0) {
    proc = (proc *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    proc->p_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(proc);
      proc = (proc *)0x0;
    }
    else {
      proc->p_numthreads = 0;
      spinlock_init(&proc->p_lock);
      proc->p_addrspace = (addrspace *)0x0;
      proc->p_cwd = (vnode *)0x0;
      proc->finish = 0;
      proc->last_victim = -1;
      proc_init_waitpid(proc,name);
      bzero(proc->fileTable,0x200);
    }
  }
  return proc;
}