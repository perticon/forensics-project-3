int32_t * proc_create(char * name) {
    char * v1 = kmalloc(612); // 0x8000cb80
    if (v1 == NULL) {
        // 0x8000cbf0
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x8000cb90
    *(int32_t *)v1 = (int32_t)v2;
    int32_t * result; // 0x8000cb68
    if (v2 == NULL) {
        // 0x8000cb9c
        kfree(v1);
        result = NULL;
    } else {
        int32_t v3 = (int32_t)v1; // 0x8000cb80
        *(int32_t *)(v3 + 4) = 0;
        spinlock_init((int32_t *)(v3 + 8));
        *(int32_t *)(v3 + 16) = 0;
        *(int32_t *)(v3 + 20) = 0;
        *(int32_t *)(v3 + 40) = 0;
        *(int32_t *)(v3 + 608) = -1;
        proc_init_waitpid((int32_t *)v1, name);
        bzero((char *)(v3 + 96), 512);
        result = (int32_t *)v1;
    }
    // 0x8000cbf0
    return result;
}