int32_t fstest_remove(char * fs, char * namesuffix) {
    char buf[32]; // bp-40, 0x8000f760
    char name[32]; // bp-72, 0x8000f760
    // 0x8000f760
    fstest_makename(name, 32, fs, namesuffix);
    strcpy(buf, name);
    int32_t v1 = vfs_remove(buf); // 0x8000f78c
    int32_t result = 0; // 0x8000f794
    if (v1 != 0) {
        char * v2 = strerror(v1); // 0x8000f79c
        kprintf("Could not remove %s: %s\n", name, v2);
        result = -1;
    }
    // 0x8000f7c0
    return result;
}