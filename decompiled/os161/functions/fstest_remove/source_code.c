fstest_remove(const char *fs, const char *namesuffix)
{
	char name[32];
	char buf[32];
	int err;

	MAKENAME();

	strcpy(buf, name);
	err = vfs_remove(buf);
	if (err) {
		kprintf("Could not remove %s: %s\n", name, strerror(err));
		return -1;
	}

	return 0;
}