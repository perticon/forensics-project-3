int fstest_remove(char *fs,char *namesuffix)

{
  int iVar1;
  char *pcVar2;
  char name [32];
  char buf [32];
  
  fstest_makename(name,0x20,fs,namesuffix);
  strcpy(buf,name);
  iVar1 = vfs_remove(buf);
  if (iVar1 == 0) {
    iVar1 = 0;
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not remove %s: %s\n",name,pcVar2);
    iVar1 = -1;
  }
  return iVar1;
}