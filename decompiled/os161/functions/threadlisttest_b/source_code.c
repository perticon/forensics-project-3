threadlisttest_b(void)
{
	struct threadlist tl;
	struct thread *t;

	threadlist_init(&tl);

	threadlist_addhead(&tl, fakethreads[0]);
	check_order(&tl, false);
	check_order(&tl, true);
	KASSERT(tl.tl_count == 1);
	t = threadlist_remhead(&tl);
	KASSERT(tl.tl_count == 0);
	KASSERT(t == fakethreads[0]);

	threadlist_addtail(&tl, fakethreads[0]);
	check_order(&tl, false);
	check_order(&tl, true);
	KASSERT(tl.tl_count == 1);
	t = threadlist_remtail(&tl);
	KASSERT(tl.tl_count == 0);
	KASSERT(t == fakethreads[0]);

	threadlist_cleanup(&tl);
}