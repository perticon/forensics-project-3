void threadlisttest_b(void)

{
  thread *ptVar1;
  int iVar2;
  char *t;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  check_order(&tl,false);
  check_order(&tl,true);
  if (tl.tl_count != 1) {
    badassert("tl.tl_count == 1","../../test/threadlisttest.c",0x8f,"threadlisttest_b");
  }
  ptVar1 = threadlist_remhead(&tl);
  iVar2 = -0x7ffd0000;
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x91,"threadlisttest_b");
  }
  t = *(char **)(iVar2 + -0x5b20);
  if ((thread *)t != ptVar1) {
    t = "../../test/threadlisttest.c";
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0x92,"threadlisttest_b");
  }
  threadlist_addtail(&tl,(thread *)t);
  check_order(&tl,false);
  check_order(&tl,true);
  if (tl.tl_count != 1) {
    badassert("tl.tl_count == 1","../../test/threadlisttest.c",0x97,"threadlisttest_b");
  }
  ptVar1 = threadlist_remtail(&tl);
  iVar2 = -0x7ffd0000;
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x99,"threadlisttest_b");
  }
  if (*(thread **)(iVar2 + -0x5b20) != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0x9a,"threadlisttest_b");
  }
  threadlist_cleanup(&tl);
  return;
}