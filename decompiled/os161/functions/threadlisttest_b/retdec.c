void threadlisttest_b(void) {
    // 0x8001397c
    int32_t tl; // bp-40, 0x8001397c
    threadlist_init(&tl);
    threadlist_addhead(&tl, (int32_t *)*(int32_t *)&fakethreads);
    check_order(&tl, false);
    check_order(&tl, true);
    bool v1; // 0x8001397c
    int32_t v2; // 0x8001397c
    int32_t v3; // 0x8001397c
    if (v3 == 1) {
        // 0x800139e4
        threadlist_remhead(&tl);
        goto lab_0x800139fc;
    } else {
        // 0x800139e4
        badassert("tl.tl_count == 1", "../../test/threadlisttest.c", 143, "threadlisttest_b");
        int32_t * v4 = threadlist_remhead(&tl); // 0x800139e8
        v1 = true;
        v2 = (int32_t)v4;
        if (v3 == 0) {
            goto lab_0x80013a1c;
        } else {
            goto lab_0x800139fc;
        }
    }
  lab_0x800139fc:
    // 0x800139fc
    badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 145, "threadlisttest_b");
    v1 = false;
    v2 = &g41;
    goto lab_0x80013a1c;
  lab_0x80013a1c:;
    int32_t v5 = v2; // 0x80013a28
    if (*(int32_t *)&fakethreads != v2) {
        // 0x80013a2c
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 146, "threadlisttest_b");
        v5 = (int32_t)"../../test/threadlisttest.c";
    }
    // 0x80013a48
    threadlist_addtail(&tl, (int32_t *)v5);
    check_order(&tl, false);
    check_order(&tl, true);
    if (v3 != 1) {
        // 0x80013a7c
        badassert("tl.tl_count == 1", "../../test/threadlisttest.c", 151, "threadlisttest_b");
    }
    int32_t v6 = (int32_t)threadlist_remtail(&tl); // 0x80013aac
    if (!v1) {
        // 0x80013ab0
        badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 153, "threadlisttest_b");
        v6 = &g41;
    }
    // 0x80013ad0
    if (*(int32_t *)&fakethreads != v6) {
        // 0x80013ae0
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 154, "threadlisttest_b");
    }
    // 0x80013afc
    threadlist_cleanup(&tl);
}