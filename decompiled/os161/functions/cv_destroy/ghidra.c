void cv_destroy(cv *cv)

{
  char *pcVar1;
  
  pcVar1 = (char *)cv;
  if (cv == (cv *)0x0) {
    pcVar1 = "cv != NULL";
    badassert("cv != NULL","../../thread/synch.c",0x13d,"cv_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  wchan_destroy(cv->cv_wchan);
  kfree(cv->cv_name);
  kfree(cv);
  return;
}