void cv_destroy(int32_t * cv) {
    if (cv == NULL) {
        // 0x80015b84
        badassert("cv != NULL", "../../thread/synch.c", 317, "cv_destroy");
    }
    // 0x80015ba4
    spinlock_cleanup((int32_t *)"LL");
    int32_t v1 = *(int32_t *)((int32_t)cv + 4); // 0x80015bac
    wchan_destroy((int32_t *)v1);
    kfree((char *)v1);
    kfree((char *)cv);
}