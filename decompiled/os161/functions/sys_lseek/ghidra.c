int sys_lseek(int fd,off_t offset,int whence,int *retval)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  int in_a3;
  openfile *of;
  undefined4 uVar4;
  int unaff_s7;
  int local_20 [3];
  
  of = *(openfile **)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
  if (of == (openfile *)0x0) {
    kprintf("Error in sys_lseek: fd %d for process %d, file not open\n",fd,
            *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
    *offset._4_4_ = -1;
    local_20[0] = 0x1e;
  }
  else {
    local_20[0] = 0;
    iVar1 = file_seek(of,offset & 0xffffffff00000000U | ZEXT48(local_20),whence,retval);
    iVar3 = local_20[0];
    if ((iVar1 == 0) && (local_20[0] == 0)) {
      iVar3 = *(int *)((int)&of->offset + 4);
      if ((*(int **)&of->offset != retval) || (iVar3 != in_a3)) {
        badassert("of->offset == offset","../../syscall/file_syscalls.c",0xd7,"sys_lseek");
      }
      *offset._4_4_ = iVar3;
      local_20[0] = 0;
    }
    else {
      if ((dbflags & 2) != 0) {
        uVar4 = *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c);
        pcVar2 = strerror(local_20[0]);
        kprintf("Error in sys_lseek: fd %d for process %d, code=%d, err=%s\n",fd,uVar4,iVar3,pcVar2)
        ;
      }
      *offset._4_4_ = -1;
    }
  }
  return local_20[0];
}