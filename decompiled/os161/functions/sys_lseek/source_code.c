int sys_lseek(int fd, off_t offset, int whence, int *retval) {
  struct openfile *of=NULL;; 	

  of = curproc->fileTable[fd];

  if (of == NULL) {
    kprintf("Error in sys_lseek: fd %d for process %d, file not open\n",
      fd, curproc->p_pid);
    *retval = -1;
    return EBADF;
  }
  int errcode = 0;
  int res = file_seek(of, offset, whence, &errcode);
  if (res != 0 || errcode != 0) {
    DEBUG(DB_SYSCALL, "Error in sys_lseek: fd %d for process %d, code=%d, err=%s\n",
      fd, curproc->p_pid, errcode, strerror(errcode));
    *retval = -1;
    return errcode;
  }

  KASSERT(of->offset == offset);

  *retval = of->offset;
  return 0;
}