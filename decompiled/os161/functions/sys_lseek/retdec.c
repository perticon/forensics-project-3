int32_t sys_lseek(int32_t fd, int64_t offset, int32_t whence, int32_t * retval) {
    // 0x8000d5a8
    int32_t v1; // 0x8000d5a8
    int32_t * v2 = (int32_t *)(v1 + 84); // 0x8000d5c0
    int32_t v3 = *v2; // 0x8000d5c0
    int32_t v4 = *(int32_t *)(4 * fd + 96 + v3); // 0x8000d5d0
    if (v4 == 0) {
        // 0x8000d5e0
        kprintf("Error in sys_lseek: fd %d for process %d, file not open\n", fd, *(int32_t *)(v3 + 28));
        *(int32_t *)v1 = -1;
        // 0x8000d6f4
        return 30;
    }
    int32_t v5 = offset;
    if (file_seek((int32_t *)v4, (int64_t)v5, whence, retval) != 0) {
        // 0x8000d64c
        if ((dbflags & 2) != 0) {
            int32_t v6 = *(int32_t *)(*v2 + 28); // 0x8000d668
            kprintf("Error in sys_lseek: fd %d for process %d, code=%d, err=%s\n", fd, v6, 0, strerror(0));
        }
        // 0x8000d694
        *(int32_t *)v1 = -1;
        // 0x8000d6f4
        return 0;
    }
    int32_t v7 = whence; // 0x8000d6bc
    if (*(int32_t *)(v4 + 8) == v5 != (*(int32_t *)(v4 + 12) == whence)) {
        // 0x8000d6c8
        badassert("of->offset == offset", "../../syscall/file_syscalls.c", 215, "sys_lseek");
        v7 = &g41;
    }
    // 0x8000d6e4
    *(int32_t *)v1 = v7;
    // 0x8000d6f4
    return 0;
}