int tryattach_ltimer_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  ltimer_softc *lt;
  int errcode;
  char *pcVar1;
  
  lt = attach_ltimer_to_lamebus(devunit,bus);
  if (lt == (ltimer_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("ltimer%d at lamebus%d",devunit,busunit);
    errcode = config_ltimer(lt,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_ltimer = devunit + 1;
      autoconf_ltimer(lt,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}