int32_t tryattach_ltimer_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_ltimer_to_lamebus(devunit, bus); // 0x800014e8
    if (v1 == NULL) {
        // 0x8000156c
        return -1;
    }
    // 0x800014f4
    kprintf("ltimer%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_ltimer(v1, devunit); // 0x80001510
    int32_t result; // 0x800014cc
    if (v2 == 0) {
        // 0x8000153c
        kprintf("\n");
        nextunit_ltimer = devunit + 1;
        autoconf_ltimer(v1, devunit);
        result = 0;
    } else {
        // 0x8000151c
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x8000156c
    return result;
}