tryattach_ltimer_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct ltimer_softc *dev;
	int result;

	dev = attach_ltimer_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("ltimer%d at lamebus%d", devunit, busunit);
	result = config_ltimer(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_ltimer = devunit+1;
	autoconf_ltimer(dev, devunit);
	return 0;
}