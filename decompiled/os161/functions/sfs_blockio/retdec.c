int32_t sfs_blockio(int32_t * sv, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)*(int32_t *)((int32_t)sv + 12); // 0x80008c78
    int32_t * v3 = (int32_t *)(v1 + 24); // 0x80008c7c
    int32_t * v4 = (int32_t *)(v1 + 8); // 0x80008c8c
    uint32_t v5 = *v4; // 0x80008c8c
    int32_t * v6 = (int32_t *)(v1 + 12); // 0x80008c90
    uint32_t v7 = *v6; // 0x80008c90
    int32_t v8 = v5; // 0x80008c9c
    int32_t v9 = v7; // 0x80008c9c
    if (v5 <= 0xffffffff) {
        // 0x80008ca0
        v8 = v5 + (int32_t)(v7 > 0xfffffe00);
        v9 = v7 + 511;
    }
    // 0x80008cac
    int32_t diskblock; // bp-40, 0x80008c4c
    int32_t result = sfs_bmap(sv, v9 / 512 | 0x800000 * v8, *v3 == 1, &diskblock); // 0x80008cbc
    if (result != 0) {
        // 0x80008db0
        return result;
    }
    // 0x80008cc8
    if (diskblock == 0) {
        int32_t v10 = 512; // 0x80008ce4
        if (*v3 != 0) {
            // 0x80008ce8
            badassert("uio->uio_rw == UIO_READ", "../../fs/sfs/sfs_io.c", 256, "sfs_blockio");
            v10 = (int32_t)"uio->uio_rw == UIO_READ";
        }
        // 0x80008db0
        return uiomovezeros(v10, uio);
    }
    uint32_t v11 = 512 * diskblock; // 0x80008cd4
    *v4 = 0;
    *v6 = v11;
    int32_t * v12 = (int32_t *)(v1 + 16); // 0x80008d28
    uint32_t v13 = *v12; // 0x80008d28
    int32_t v14 = 512; // 0x80008d38
    if (v13 < 512) {
        // 0x80008d3c
        badassert("uio->uio_resid >= SFS_BLOCKSIZE", "../../fs/sfs/sfs_io.c", 271, "sfs_blockio");
        v14 = &g41;
    }
    // 0x80008d5c
    *v12 = v14;
    int32_t result2 = sfs_rwblock((int32_t *)v2, uio); // 0x80008d68
    uint32_t v15 = *v6; // 0x80008d70
    uint32_t v16 = v15 - v11; // 0x80008d78
    uint32_t v17 = v16 + *v6; // 0x80008d84
    *v4 = 2 * *v4 + (int32_t)(v15 < v11) + (int32_t)(v17 < v16);
    *v6 = v17;
    *v12 = v13 - 512 + *v12;
    // 0x80008db0
    return result2;
}