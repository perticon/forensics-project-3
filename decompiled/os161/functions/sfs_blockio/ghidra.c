int sfs_blockio(sfs_vnode *sv,uio *uio)

{
  size_t sVar1;
  uint uVar2;
  char *n;
  uint uVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  sfs_fs *sfs;
  int iVar7;
  int iVar8;
  daddr_t diskblock;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  iVar5 = *(int *)&uio->uio_offset;
  uVar2 = *(uint *)((int)&uio->uio_offset + 4);
  uVar6 = uVar2;
  if (iVar5 < 0) {
    uVar6 = uVar2 + 0x1ff;
    iVar5 = (uint)(uVar6 < uVar2) + iVar5;
  }
  iVar5 = sfs_bmap(sv,iVar5 << 0x17 | uVar6 >> 9,uio->uio_rw == UIO_WRITE,&diskblock);
  if (iVar5 == 0) {
    if (diskblock == 0) {
      n = (char *)0x200;
      if (uio->uio_rw != UIO_READ) {
        n = "uio->uio_rw == UIO_READ";
        badassert("uio->uio_rw == UIO_READ","../../fs/sfs/sfs_io.c",0x100,"sfs_blockio");
      }
      iVar5 = uiomovezeros((size_t)n,uio);
    }
    else {
      iVar7 = *(int *)&uio->uio_offset;
      iVar8 = *(int *)((int)&uio->uio_offset + 4);
      *(undefined4 *)&uio->uio_offset = 0;
      *(daddr_t *)((int)&uio->uio_offset + 4) = diskblock * 0x200;
      uVar6 = uio->uio_resid;
      sVar1 = 0x200;
      if (uVar6 < 0x200) {
        badassert("uio->uio_resid >= SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x10f,"sfs_blockio");
      }
      uio->uio_resid = sVar1;
      iVar5 = sfs_rwblock(sfs,uio);
      uVar3 = *(uint *)((int)&uio->uio_offset + 4);
      uVar2 = uVar3 + diskblock * -0x200;
      uVar4 = uVar2 + iVar8;
      *(uint *)&uio->uio_offset =
           (uint)(uVar4 < uVar2) + (*(int *)&uio->uio_offset - (uint)(uVar3 < uVar2)) + iVar7;
      *(uint *)((int)&uio->uio_offset + 4) = uVar4;
      uio->uio_resid = uVar6 + (uio->uio_resid - 0x200);
    }
  }
  return iVar5;
}