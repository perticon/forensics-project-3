void timerclock(void) {
    // 0x80014f18
    spinlock_acquire(&lbolt_lock);
    wchan_wakeall(lbolt, &lbolt_lock);
    spinlock_release(&lbolt_lock);
}