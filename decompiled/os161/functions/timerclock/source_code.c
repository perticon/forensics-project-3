timerclock(void)
{
	/* Just broadcast on lbolt */
	spinlock_acquire(&lbolt_lock);
	wchan_wakeall(lbolt, &lbolt_lock);
	spinlock_release(&lbolt_lock);
}