int32_t vfs_chdir(char * path) {
    // 0x80018168
    int32_t * vn; // bp-16, 0x80018168
    int32_t v1 = vfs_lookup(path, (int32_t *)&vn); // 0x80018178
    int32_t result = v1; // 0x80018180
    if (v1 == 0) {
        // 0x80018184
        result = vfs_setcurdir(vn);
        vnode_decref(vn);
    }
    // 0x800181a0
    return result;
}