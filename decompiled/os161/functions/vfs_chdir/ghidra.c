int vfs_chdir(char *path)

{
  int iVar1;
  vnode *vn;
  
  iVar1 = vfs_lookup(path,&vn);
  if (iVar1 == 0) {
    iVar1 = vfs_setcurdir(vn);
    vnode_decref(vn);
  }
  return iVar1;
}