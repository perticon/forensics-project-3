int sys_write(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  
  if (fd - 1U < 2) {
    spinlock_acquire(&sp);
    for (iVar1 = 0; iVar1 < (int)size; iVar1 = iVar1 + 1) {
      putch((int)buf_ptr[iVar1]._dummy);
    }
    spinlock_release(&sp);
  }
  else {
    size = file_write(fd,buf_ptr,size);
  }
  return size;
}