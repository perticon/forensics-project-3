int32_t sys_write(int32_t fd, int32_t * buf_ptr, int32_t size) {
    if ((uint32_t)(fd - 1) >= 2) {
        // 0x8000d948
        return file_write(fd, buf_ptr, size);
    }
    // 0x8000d90c
    spinlock_acquire(&sp);
    if (size > 0) {
        int32_t v1 = 0;
        int32_t v2 = v1 + 1; // 0x8000d928
        putch((int32_t)*(char *)(v1 + (int32_t)buf_ptr));
        while (v2 < size) {
            // 0x8000d920
            v1 = v2;
            v2 = v1 + 1;
            putch((int32_t)*(char *)(v1 + (int32_t)buf_ptr));
        }
    }
    // 0x8000d938
    spinlock_release(&sp);
    // 0x8000d948
    return size;
}