int sys_write(int fd, userptr_t buf_ptr, size_t size)
{
  int i;
  char *p = (char *)buf_ptr;
  

  if (fd != STDOUT_FILENO && fd != STDERR_FILENO)
  {
#if OPT_PAGING
    return file_write(fd, buf_ptr, size);
#else
    kprintf("sys_write supported only to stdout\n");
    return -1;
#endif
  }
  spinlock_acquire(&sp);
  for (i = 0; i < (int)size; i++)
  {
    putch(p[i]);
  }
  spinlock_release(&sp);
  return (int)size;
}