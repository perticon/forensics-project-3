cmd_dispatch(char *cmd)
{
	struct timespec before, after, duration;
	char *args[MAXMENUARGS];
	int nargs = 0;
	char *word;
	char *context;
	int i, result;

	for (word = strtok_r(cmd, " \t", &context);
		 word != NULL;
		 word = strtok_r(NULL, " \t", &context))
	{

		if (nargs >= MAXMENUARGS)
		{
			kprintf("Command line has too many words\n");
			return E2BIG;
		}
		args[nargs++] = word;
	}

	if (nargs == 0)
	{
		return 0;
	}

	for (i = 0; cmdtable[i].name; i++)
	{
		if (*cmdtable[i].name && !strcmp(args[0], cmdtable[i].name))
		{
			KASSERT(cmdtable[i].func != NULL);

			gettime(&before);

			result = cmdtable[i].func(nargs, args);

			gettime(&after);
			timespec_sub(&after, &before, &duration);

			kprintf("Operation took %llu.%09lu seconds\n",
					(unsigned long long)duration.tv_sec,
					(unsigned long)duration.tv_nsec);

			return result;
		}
	}

	kprintf("%s: Command not found\n", args[0]);
	return EINVAL;
}