int32_t cmd_dispatch(char * cmd) {
    char * args[16]; // bp-88, 0x8000bf88
    // 0x8000bf88
    char * context; // bp-24, 0x8000bf88
    char * v1 = strtok_r(cmd, " \t", &context); // 0x8000bfa8
    if (v1 == NULL) {
        // 0x8000c11c
        return 0;
    }
    // 0x8000bfbc
    int32_t before; // bp-136, 0x8000bf88
    int32_t v2 = (int32_t)&before + 48;
    char * v3 = v1; // 0x8000bff4
    int32_t v4 = 0; // 0x8000bf88
    int32_t v5; // 0x8000bf88
    int32_t v6; // 0x8000bf88
    int32_t v7; // 0x8000bf88
    while (v4 < 16) {
        // 0x8000bfd8
        *(int32_t *)(v2 + 4 * v4) = (int32_t)v3;
        v3 = strtok_r(NULL, " \t", &context);
        v4++;
        if (v3 == NULL) {
            // 0x8000c0ec
            v5 = -0x7ffd8784;
            v6 = 0;
            v7 = 0;
            goto lab_0x8000c018;
        }
    }
    // 0x8000bfc4
    kprintf("Command line has too many words\n");
    int32_t result = 14; // 0x8000bfd4
    // 0x8000c11c
    return result;
  lab_0x8000c018:;
    char * v8 = (char *)v5; // 0x8000c018
    int32_t * v9; // 0x8000c050
    if (*v8 == 0) {
        goto lab_0x8000c0ec_2;
    } else {
        // 0x8000c028
        if (strcmp((char *)*(int32_t *)&args, v8) == 0) {
            // 0x8000c03c
            v9 = (int32_t *)(v6 + (int32_t)&cmdtable + 4);
            if (*v9 == 0) {
                // 0x8000c060
                badassert("cmdtable[i].func != NULL", "../../main/menu.c", 772, "cmd_dispatch");
                goto lab_0x8000c07c;
            } else {
                goto lab_0x8000c07c;
            }
        } else {
            goto lab_0x8000c0ec_2;
        }
    }
  lab_0x8000c0ec_2:;
    int32_t v10 = v7 + 1;
    int32_t v11 = 8 * v10; // 0x8000c0ec
    int32_t v12 = *(int32_t *)(v11 + (int32_t)&cmdtable); // 0x8000c0f4
    v5 = v12;
    v6 = v11;
    v7 = v10;
    if (v12 == 0) {
        // 0x8000c104
        kprintf("%s: Command not found\n", (char *)*(int32_t *)&args);
        result = 8;
        return result;
    } else {
        goto lab_0x8000c018;
    }
  lab_0x8000c07c:
    // 0x8000c07c
    gettime(&before);
    int32_t after; // bp-120, 0x8000bf88
    gettime(&after);
    int32_t duration; // bp-104, 0x8000bf88
    timespec_sub(&after, &before, &duration);
    int32_t v13; // 0x8000bf88
    kprintf("Operation took %llu.%09lu seconds\n", (int64_t)duration, v13);
    result = *v9;
    return result;
}