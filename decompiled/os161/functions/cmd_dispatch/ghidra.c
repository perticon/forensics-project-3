int cmd_dispatch(char *cmd)

{
  int iVar1;
  char *pcVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  timespec before;
  timespec after;
  timespec duration;
  char *args [16];
  char *context;
  
  pcVar2 = strtok_r(cmd," \t",&context);
  iVar4 = 0;
  while (pcVar2 != (char *)0x0) {
    if (0xf < iVar4) {
      kprintf("Command line has too many words\n");
      return 0xe;
    }
    args[iVar4] = pcVar2;
    pcVar2 = strtok_r((char *)0x0," \t",&context);
    iVar4 = iVar4 + 1;
  }
  iVar1 = 0;
  if (iVar4 == 0) {
    iVar4 = 0;
  }
  else {
    do {
      while( true ) {
        iVar5 = iVar1;
        pcVar2 = cmdtable[iVar5].name;
        if (pcVar2 == (char *)0x0) {
          kprintf("%s: Command not found\n",args[0]);
          return 8;
        }
        if (*pcVar2 != '\0') break;
        iVar1 = iVar5 + 1;
      }
      iVar3 = strcmp(args[0],pcVar2);
      iVar1 = iVar5 + 1;
    } while (iVar3 != 0);
    if (cmdtable[iVar5].func == (anon_subr_int_int_char_ptr_ptr_for_func *)0x0) {
      badassert("cmdtable[i].func != NULL","../../main/menu.c",0x304,"cmd_dispatch");
    }
    gettime(&before);
    iVar4 = (*cmdtable[iVar5].func)(iVar4,args);
    gettime(&after);
    timespec_sub(&after,&before,&duration);
    kprintf("Operation took %llu.%09lu seconds\n");
  }
  return iVar4;
}