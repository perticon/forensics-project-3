int32_t sfs_lookparent(int32_t * v2, char * path, int32_t * ret, char * buf, uint32_t buflen) {
    int32_t v1 = *(int32_t *)((int32_t)v2 + 16); // 0x8000980c
    vfs_biglock_acquire();
    if (*(int16_t *)(v1 + 28) != 2) {
        // 0x80009828
        vfs_biglock_release();
        // 0x80009884
        return 17;
    }
    // 0x80009838
    int32_t result; // 0x800097ec
    if (strlen(path) + 1 <= buflen) {
        // 0x80009868
        strcpy(buf, path);
        vnode_incref((int32_t *)v1);
        *ret = v1;
        vfs_biglock_release();
        result = 0;
    } else {
        // 0x80009858
        vfs_biglock_release();
        result = 7;
    }
    // 0x80009884
    return result;
}