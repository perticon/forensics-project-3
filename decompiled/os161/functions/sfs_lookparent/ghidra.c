int sfs_lookparent(vnode *v,char *path,vnode **ret,char *buf,size_t buflen)

{
  int iVar1;
  size_t sVar2;
  vnode *vn;
  
  vn = (vnode *)v->vn_data;
  vfs_biglock_acquire();
  if (*(short *)&vn[1].vn_countlock.splk_lock == 2) {
    sVar2 = strlen(path);
    if (buflen < sVar2 + 1) {
      vfs_biglock_release();
      iVar1 = 7;
    }
    else {
      strcpy(buf,path);
      vnode_incref(vn);
      *ret = vn;
      vfs_biglock_release();
      iVar1 = 0;
    }
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x11;
  }
  return iVar1;
}