void clocksleep(uint32_t num_secs) {
    // 0x80014fd8
    spinlock_acquire(&lbolt_lock);
    int32_t v1 = num_secs; // 0x80015020
    if (num_secs <= 0) {
        // 0x80015024
        spinlock_release(&lbolt_lock);
        return;
    }
    wchan_sleep(lbolt, &lbolt_lock);
    v1--;
    while (v1 > 0) {
        // 0x8001500c
        wchan_sleep(lbolt, &lbolt_lock);
        v1--;
    }
    // 0x80015024
    spinlock_release(&lbolt_lock);
}