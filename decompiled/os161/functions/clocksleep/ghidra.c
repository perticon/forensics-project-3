void clocksleep(int num_secs)

{
  spinlock_acquire(&lbolt_lock);
  for (; 0 < num_secs; num_secs = num_secs + -1) {
    wchan_sleep(lbolt,&lbolt_lock);
  }
  spinlock_release(&lbolt_lock);
  return;
}