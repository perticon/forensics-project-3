clocksleep(int num_secs)
{
	spinlock_acquire(&lbolt_lock);
	while (num_secs > 0) {
		wchan_sleep(lbolt, &lbolt_lock);
		num_secs--;
	}
	spinlock_release(&lbolt_lock);
}