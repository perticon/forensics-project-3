int32_t sfs_readblock(int32_t * sfs, int32_t block, char * data, int32_t len) {
    int32_t v1 = (int32_t)sfs; // 0x80008de4
    int32_t v2 = block; // 0x80008de4
    int32_t v3 = (int32_t)data; // 0x80008de4
    if (len != 512) {
        // 0x80008de8
        badassert("len == SFS_BLOCKSIZE", "../../fs/sfs/sfs_io.c", 113, "sfs_readblock");
        v1 = (int32_t)"len == SFS_BLOCKSIZE";
        v2 = (int32_t)"../../fs/sfs/sfs_io.c";
        v3 = 113;
    }
    // 0x80008e08
    int32_t iov; // bp-48, 0x80008dd4
    int32_t ku; // bp-40, 0x80008dd4
    uio_kinit(&iov, &ku, (char *)v3, 512, (int64_t)(v2 / 0x800000), 0);
    return sfs_rwblock((int32_t *)v1, &ku);
}