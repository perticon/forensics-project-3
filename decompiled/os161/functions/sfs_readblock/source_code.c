sfs_readblock(struct sfs_fs *sfs, daddr_t block, void *data, size_t len)
{
	struct iovec iov;
	struct uio ku;

	KASSERT(len == SFS_BLOCKSIZE);

	SFSUIO(&iov, &ku, data, block, UIO_READ);
	return sfs_rwblock(sfs, &ku);
}