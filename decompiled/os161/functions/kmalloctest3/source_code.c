kmalloctest3(int nargs, char **args)
{
#define NUM_KM3_SIZES 5
	static const unsigned sizes[NUM_KM3_SIZES] = { 32, 41, 109, 86, 9 };
	unsigned numptrs;
	size_t ptrspace;
	size_t blocksize;
	unsigned numptrblocks;
	void ***ptrblocks;
	unsigned curblock, curpos, cursizeindex, cursize;
	size_t totalsize;
	unsigned i, j;
	unsigned char *ptr;

	if (nargs != 2) {
		kprintf("kmalloctest3: usage: km3 numobjects\n");
		return EINVAL;
	}

	/* Figure out how many pointers we'll get and the space they need. */
	numptrs = atoi(args[1]);
	ptrspace = numptrs * sizeof(void *);

	/* Figure out how many blocks in the lower tier. */
	blocksize = PAGE_SIZE / 4;
	numptrblocks = DIVROUNDUP(ptrspace, blocksize);

	kprintf("kmalloctest3: %u objects, %u pointer blocks\n",
		numptrs, numptrblocks);

	/* Allocate the upper tier. */
	ptrblocks = kmalloc(numptrblocks * sizeof(ptrblocks[0]));
	if (ptrblocks == NULL) {
		panic("kmalloctest3: failed on pointer block array\n");
	}
	/* Allocate the lower tier. */
	for (i=0; i<numptrblocks; i++) {
		ptrblocks[i] = kmalloc(blocksize);
		if (ptrblocks[i] == NULL) {
			panic("kmalloctest3: failed on pointer block %u\n", i);
		}
	}

	/* Allocate the objects. */
	curblock = 0;
	curpos = 0;
	cursizeindex = 0;
	totalsize = 0;
	for (i=0; i<numptrs; i++) {
		cursize = sizes[cursizeindex];
		ptr = kmalloc(cursize);
		if (ptr == NULL) {
			kprintf("kmalloctest3: failed on object %u size %u\n",
				i, cursize);
			kprintf("kmalloctest3: pos %u in pointer block %u\n",
				curpos, curblock);
			kprintf("kmalloctest3: total so far %zu\n", totalsize);
			panic("kmalloctest3: failed.\n");
		}
		/* Fill the object with its number. */
		for (j=0; j<cursize; j++) {
			ptr[j] = (unsigned char) i;
		}
		/* Move to the next slot in the tree. */
		ptrblocks[curblock][curpos] = ptr;
		curpos++;
		if (curpos >= blocksize / sizeof(void *)) {
			curblock++;
			curpos = 0;
		}
		/* Update the running total, and rotate the size. */
		totalsize += cursize;
		cursizeindex = (cursizeindex + 1) % NUM_KM3_SIZES;
	}

	kprintf("kmalloctest3: %zu bytes allocated\n", totalsize);

	/* Free the objects. */
	curblock = 0;
	curpos = 0;
	cursizeindex = 0;
	for (i=0; i<numptrs; i++) {
		cursize = sizes[cursizeindex];
		ptr = ptrblocks[curblock][curpos];
		KASSERT(ptr != NULL);
		for (j=0; j<cursize; j++) {
			if (ptr[j] == (unsigned char) i) {
				continue;
			}
			kprintf("kmalloctest3: failed on object %u size %u\n",
				i, cursize);
			kprintf("kmalloctest3: pos %u in pointer block %u\n",
				curpos, curblock);
			kprintf("kmalloctest3: at object offset %u\n", j);
			kprintf("kmalloctest3: expected 0x%x, found 0x%x\n",
				ptr[j], (unsigned char) i);
			panic("kmalloctest3: failed.\n");
		}
		kfree(ptr);
		curpos++;
		if (curpos >= blocksize / sizeof(void *)) {
			curblock++;
			curpos = 0;
		}
		KASSERT(totalsize > 0);
		totalsize -= cursize;
		cursizeindex = (cursizeindex + 1) % NUM_KM3_SIZES;
	}
	KASSERT(totalsize == 0);

	/* Free the lower tier. */
	for (i=0; i<numptrblocks; i++) {
		KASSERT(ptrblocks[i] != NULL);
		kfree(ptrblocks[i]);
	}
	/* Free the upper tier. */
	kfree(ptrblocks);

	kprintf("kmalloctest3: passed\n");
	return 0;
}