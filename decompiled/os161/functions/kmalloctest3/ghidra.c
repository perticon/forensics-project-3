int kmalloctest3(int nargs,char **args)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  void *ptr;
  void *pvVar4;
  uint uVar5;
  char *pcVar6;
  uint uVar7;
  uint uVar8;
  byte *pbVar9;
  uint uVar10;
  uint uVar11;
  int iVar12;
  uint uVar13;
  int local_38;
  
  if (nargs == 2) {
    uVar2 = atoi(args[1]);
    uVar3 = uVar2 * 4 + 0x3ff >> 10;
    kprintf("kmalloctest3: %u objects, %u pointer blocks\n",uVar2,uVar3);
    ptr = kmalloc(uVar3 << 2);
    if (ptr == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
      panic("kmalloctest3: failed on pointer block array\n");
    }
    for (uVar7 = 0; uVar10 = 0, uVar7 < uVar3; uVar7 = uVar7 + 1) {
      pvVar4 = kmalloc(0x400);
      *(void **)((int)ptr + uVar7 * 4) = pvVar4;
      if (pvVar4 == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
        panic("kmalloctest3: failed on pointer block %u\n",uVar7);
      }
    }
    iVar1 = 0;
    uVar7 = 0;
    pbVar9 = (byte *)0x0;
    iVar12 = 0;
    for (; uVar10 < uVar2; uVar10 = uVar10 + 1) {
      uVar13 = kmalloctest3::sizes[uVar7];
      pvVar4 = kmalloc(uVar13);
      if (pvVar4 == (void *)0x0) {
        kprintf("kmalloctest3: failed on object %u size %u\n",uVar10,uVar13);
        kprintf("kmalloctest3: pos %u in pointer block %u\n",pbVar9,iVar12);
        kprintf("kmalloctest3: total so far %zu\n",iVar1);
                    /* WARNING: Subroutine does not return */
        panic("kmalloctest3: failed.\n");
      }
      for (uVar5 = 0; uVar5 < uVar13; uVar5 = uVar5 + 1) {
        *(char *)((int)pvVar4 + uVar5) = (char)uVar10;
      }
      *(void **)(*(int *)((int)ptr + iVar12 * 4) + (int)pbVar9 * 4) = pvVar4;
      pbVar9 = pbVar9 + 1;
      if ((byte *)0xff < pbVar9) {
        iVar12 = iVar12 + 1;
        pbVar9 = (byte *)0x0;
      }
      iVar1 = iVar1 + uVar13;
      uVar7 = (uVar7 + 1) % 5;
    }
    kprintf("kmalloctest3: %zu bytes allocated\n",iVar1);
    uVar13 = 0;
    uVar7 = 0;
    local_38 = 0;
    for (uVar10 = 0; iVar12 = uVar13 * 4, uVar10 < uVar2; uVar10 = uVar10 + 1) {
      uVar11 = kmalloctest3::sizes[uVar13];
      uVar5 = *(uint *)((int)ptr + local_38 * 4);
      pcVar6 = *(char **)(uVar5 + uVar7 * 4);
      uVar8 = 0;
      if (pcVar6 != (char *)0x0) {
        uVar5 = uVar10 & 0xff;
        goto LAB_80011280;
      }
      pcVar6 = "ptr != NULL";
      badassert("ptr != NULL","../../test/kmalloctest.c",0x101,"kmalloctest3");
      do {
        if (*pbVar9 != uVar5) {
          kprintf("kmalloctest3: failed on object %u size %u\n",uVar10,uVar11);
          kprintf("kmalloctest3: pos %u in pointer block %u\n",uVar7,local_38);
          kprintf("kmalloctest3: at object offset %u\n",uVar8);
          kprintf("kmalloctest3: expected 0x%x, found 0x%x\n",(uint)*pbVar9,uVar10 & 0xff);
                    /* WARNING: Subroutine does not return */
          panic("kmalloctest3: failed.\n");
        }
        uVar8 = uVar8 + 1;
LAB_80011280:
        pbVar9 = (byte *)(pcVar6 + uVar8);
      } while (uVar8 < uVar11);
      uVar7 = uVar7 + 1;
      kfree(pcVar6);
      if (0xff < uVar7) {
        local_38 = local_38 + 1;
        uVar7 = 0;
      }
      uVar5 = 5;
      if (iVar1 == 0) {
        badassert("totalsize > 0","../../test/kmalloctest.c",0x115,"kmalloctest3");
      }
      iVar1 = iVar1 - uVar11;
      uVar13 = (uVar13 + 1) % uVar5;
      if (uVar5 == 0) {
        trap(0x1c00);
      }
    }
    uVar2 = 0;
    if (iVar1 == 0) goto LAB_8001136c;
    badassert("totalsize == 0","../../test/kmalloctest.c",0x119,"kmalloctest3");
    do {
      pcVar6 = *(char **)((int)ptr + iVar12);
      if (pcVar6 == (char *)0x0) {
        pcVar6 = "ptrblocks[i] != NULL";
        badassert("ptrblocks[i] != NULL","../../test/kmalloctest.c",0x11d,"kmalloctest3");
      }
      uVar2 = uVar2 + 1;
      kfree(pcVar6);
LAB_8001136c:
      iVar12 = uVar2 << 2;
    } while (uVar2 < uVar3);
    kfree(ptr);
    kprintf("kmalloctest3: passed\n");
    iVar1 = 0;
  }
  else {
    kprintf("kmalloctest3: usage: km3 numobjects\n");
    iVar1 = 8;
  }
  return iVar1;
}