int32_t kmalloctest3(int32_t nargs, char ** args) {
    if (nargs != 2) {
        // 0x80010fdc
        kprintf("kmalloctest3: usage: km3 numobjects\n");
        // 0x80011398
        return 8;
    }
    uint32_t v1 = atoi((char *)*(int32_t *)((int32_t)args + 4)); // 0x80010ff8
    uint32_t v2 = (4 * v1 + 1023) / 1024; // 0x80011008
    kprintf("kmalloctest3: %u objects, %u pointer blocks\n", v1, v2);
    char * v3 = kmalloc(4 * v2); // 0x8001102c
    int32_t v4 = (int32_t)v3; // 0x8001102c
    int32_t v5 = 0; // 0x80011034
    if (v3 == NULL) {
        // 0x80011038
        panic("kmalloctest3: failed on pointer block array\n");
        goto lab_0x80011044;
    } else {
        goto lab_0x80011078;
    }
  lab_0x80011210:;
    // 0x80011210
    int32_t v6; // 0x80010fac
    int32_t v7 = v6;
    int32_t v8; // 0x80010fac
    int32_t v9 = v8;
    int32_t v10; // 0x80010fac
    char * v11 = (char *)v10; // 0x80011210
    int32_t v12; // 0x80010fac
    int32_t v13; // 0x80010fac
    int32_t v14; // 0x80010fac
    int32_t v15; // 0x80010fac
    uint32_t v16; // 0x80010fac
    int32_t v17; // 0x80010fac
    int32_t v18; // 0x80010fac
    uint32_t v19; // 0x800111c0
    if (v9 == (int32_t)*v11) {
        // 0x80011220
        v18 = v9;
        v13 = v12;
        v14 = v7 + 1;
    } else {
        // 0x80011228
        kprintf("kmalloctest3: failed on object %u size %u\n", v16, v19);
        kprintf("kmalloctest3: pos %u in pointer block %u\n", v15, v17);
        kprintf("kmalloctest3: at object offset %u\n", v7);
        int32_t v20 = v16 % 256;
        kprintf("kmalloctest3: expected 0x%x, found 0x%x\n", (int32_t)*v11, v20);
        panic("kmalloctest3: failed.\n");
        v18 = v20;
        v13 = (int32_t)"kmalloctest3: failed.\n";
        v14 = v7;
    }
    goto lab_0x80011280;
  lab_0x80011280:;
    int32_t v21 = v13;
    int32_t v22 = v14 + v21; // 0x80011288
    v8 = v18;
    v12 = v21;
    v6 = v14;
    v10 = v22;
    int32_t v23; // 0x80010fac
    int32_t v24; // 0x80010fac
    int32_t v25; // 0x80010fac
    int32_t v26; // 0x80010fac
    int32_t v27; // 0x80010fac
    int32_t v28; // 0x80010fac
    int32_t v29; // 0x800112dc
    if (v14 < v19) {
        goto lab_0x80011210;
    } else {
        // 0x8001128c
        kfree((char *)v21);
        if (v25 == 0) {
            // 0x800112bc
            badassert("totalsize > 0", "../../test/kmalloctest.c", 277, "kmalloctest3");
        }
        uint32_t v30 = v15 + 1; // 0x80011290
        v29 = v25 - v19;
        int32_t v31 = v16 + 1; // 0x800112f4
        v27 = (v27 + 1) % 5;
        v26 = v31;
        v25 = v29;
        v24 = v30 < 256 ? v30 : 0;
        v23 = v22;
        v28 = v17 + (int32_t)(v30 >= 256);
        if (v31 >= v1) {
            // break -> 0x8001130c
            goto lab_0x8001130c;
        }
        goto lab_0x800111b4_2;
    }
  lab_0x80011100:;
    // 0x80011100
    int32_t v32; // 0x80010fac
    char v33; // 0x80010fac
    *(char *)v32 = v33;
    int32_t v34; // 0x80010fac
    int32_t v35 = v34; // 0x80011108
    int32_t v36; // 0x80010fac
    int32_t v37 = v36; // 0x80011108
    int32_t v38; // 0x80010fac
    int32_t v39 = v38 + 1; // 0x80011108
    char v40 = v33; // 0x80011108
    int32_t v41; // 0x80010fac
    int32_t v42 = v41; // 0x80011108
    goto lab_0x80011114;
  lab_0x80011114:;
    int32_t v43 = v42;
    int32_t v44 = v37;
    int32_t v45 = v35;
    v34 = v45;
    v36 = v44;
    v38 = v39;
    v32 = v39 + v44;
    v33 = v40;
    v41 = v43;
    int32_t v46; // 0x80010fac
    int32_t v47; // 0x80010fac
    int32_t v48; // 0x80010fac
    int32_t v49; // 0x80010fac
    int32_t v50; // 0x80010fac
    int32_t v51; // 0x80010fac
    int32_t v52; // 0x80010fac
    int32_t v53; // 0x80010fac
    int32_t v54; // 0x80010fac
    int32_t v55; // 0x80010fac
    int32_t v56; // 0x80011150
    uint32_t v57; // 0x800110ac
    if (v39 < v57) {
        goto lab_0x80011100;
    } else {
        // 0x80011120
        *(int32_t *)(*(int32_t *)(4 * v54 + v4) + 4 * v48) = v44;
        uint32_t v58 = v48 + 1; // 0x80011138
        v55 = v58 < 256 ? v58 : 0;
        v56 = v57 + v52;
        int32_t v59 = v45 + 1; // 0x8001116c
        v53 = v54 + (int32_t)(v58 >= 256);
        v51 = v56;
        v50 = (v50 + 1) % 5;
        v49 = v59;
        v47 = v55;
        v46 = v43;
        if (v59 >= v1) {
            // break -> 0x800111b4
            goto lab_0x800111b4_3;
        }
        goto lab_0x800110a8;
    }
  lab_0x80011078:;
    int32_t v60 = v5; // 0x80011088
    int32_t v61; // 0x80010fac
    int32_t v62; // 0x80010fac
    int32_t v63; // 0x80010fac
    if (v5 < v2) {
        goto lab_0x80011044;
    } else {
        // 0x80011170
        v53 = 0;
        v51 = 0;
        v50 = 0;
        v49 = 0;
        v47 = 0;
        v46 = v2;
        if (v1 == 0) {
            // 0x80011184
            kprintf("kmalloctest3: %zu bytes allocated\n", (int64_t)v2);
            v62 = 0;
            goto lab_0x8001136c;
        } else {
            while (true) {
              lab_0x800110a8:
                // 0x800110a8
                v48 = v47;
                int32_t v64 = v49;
                v52 = v51;
                v54 = v53;
                v57 = *(int32_t *)(4 * v50 + (int32_t)L" )mV\t");
                char * v65 = kmalloc(v57); // 0x800110b4
                if (v65 == NULL) {
                    // 0x800110c0
                    kprintf("kmalloctest3: failed on object %u size %u\n", v64, v57);
                    kprintf("kmalloctest3: pos %u in pointer block %u\n", v48, v54);
                    kprintf("kmalloctest3: total so far %zu\n", (int64_t)v54);
                    panic("kmalloctest3: failed.\n");
                    v34 = v64;
                    v36 = &g41;
                    v38 = v1;
                    v32 = (int32_t)"kmalloctest3: failed.\n";
                    v33 = v52;
                    v41 = v54;
                    goto lab_0x80011100;
                } else {
                    // 0x8001110c
                    v35 = v64;
                    v37 = (int32_t)v65;
                    v39 = 0;
                    v40 = v64;
                    v42 = v46;
                    goto lab_0x80011114;
                }
            }
          lab_0x800111b4_3:
            // 0x800111b4
            kprintf("kmalloctest3: %zu bytes allocated\n", (int64_t)v43);
            v27 = 0;
            v26 = 0;
            v25 = v56;
            v24 = 0;
            v23 = v55;
            v28 = 0;
            while (true) {
              lab_0x800111b4_2:
                // 0x800111b4
                v17 = v28;
                v15 = v24;
                v16 = v26;
                v19 = *(int32_t *)(4 * v27 + (int32_t)L" )mV\t");
                int32_t v66 = *(int32_t *)(4 * v17 + v4); // 0x800111d4
                int32_t v67 = *(int32_t *)(v66 + 4 * v15); // 0x800111e0
                if (v67 == 0) {
                    // 0x800111f0
                    badassert("ptr != NULL", "../../test/kmalloctest.c", 257, "kmalloctest3");
                    v8 = v66;
                    v12 = (int32_t)"ptr != NULL";
                    v6 = 0;
                    v10 = v23;
                    goto lab_0x80011210;
                } else {
                    // 0x800111b4
                    v18 = v16 % 256;
                    v13 = v67;
                    v14 = 0;
                    goto lab_0x80011280;
                }
            }
          lab_0x8001130c:
            // 0x8001130c
            v62 = 0;
            if (v29 == 0) {
                goto lab_0x8001136c;
            } else {
                // 0x80011314
                badassert("totalsize == 0", "../../test/kmalloctest.c", 281, "kmalloctest3");
                v63 = &g41;
                v61 = 0;
                goto lab_0x80011334;
            }
        }
    }
  lab_0x80011044:;
    int32_t v68 = v60;
    char * v69 = kmalloc(1024); // 0x80011050
    *(int32_t *)(4 * v68 + v4) = (int32_t)v69;
    if (v69 == NULL) {
        // 0x8001105c
        panic("kmalloctest3: failed on pointer block %u\n", v68);
    }
    // 0x8001106c
    v5 = v68 + 1;
    goto lab_0x80011078;
  lab_0x8001136c:
    // 0x8001136c
    v63 = 4 * v62;
    v61 = v62;
    if (v62 >= v2) {
        // 0x80011380
        kfree(v3);
        kprintf("kmalloctest3: passed\n");
        // 0x80011398
        return 0;
    }
    goto lab_0x80011334;
  lab_0x80011334:;
    int32_t v70 = *(int32_t *)(v63 + v4); // 0x80011338
    int32_t v71 = v70; // 0x80011344
    if (v70 == 0) {
        // 0x80011348
        badassert("ptrblocks[i] != NULL", "../../test/kmalloctest.c", 285, "kmalloctest3");
        v71 = (int32_t)"ptrblocks[i] != NULL";
    }
    // 0x80011364
    kfree((char *)v71);
    v62 = v61 + 1;
    goto lab_0x8001136c;
}