void kheap_printstats(void) {
    // 0x8001c9f8
    spinlock_acquire(&kmalloc_spinlock);
    kprintf("Subpage allocator status:\n");
    if (allbase == NULL) {
        // 0x8001ca44
        spinlock_release(&kmalloc_spinlock);
        return;
    }
    int32_t v1 = (int32_t)allbase; // 0x8001ca34
    subpage_stats((int32_t *)v1);
    v1 += 4;
    while (v1 != 0) {
        // 0x8001ca2c
        subpage_stats((int32_t *)v1);
        v1 += 4;
    }
    // 0x8001ca44
    spinlock_release(&kmalloc_spinlock);
}