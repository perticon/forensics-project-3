void kheap_printstats(void)

{
  pageref *pr;
  
  spinlock_acquire(&kmalloc_spinlock);
  kprintf("Subpage allocator status:\n");
  for (pr = allbase; pr != (pageref *)0x0; pr = pr->next_all) {
    subpage_stats(pr);
  }
  spinlock_release(&kmalloc_spinlock);
  return;
}