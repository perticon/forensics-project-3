lscreen_write(void *vsc, int ch)
{
	struct lscreen_softc *sc = vsc;
	int ccx, ccy;

	spinlock_acquire(&sc->ls_lock);

	switch (ch) {
	    case '\n': lscreen_newline(sc); break;
	    default: lscreen_char(sc, ch); break;
	}

	/*
	 * ccx/ccy = corrected cursor position
	 * (The cursor marks the next space text will appear in. But
	 * at the very end of the line, it should not move off the edge.)
	 */
	ccx = sc->ls_cx;
	ccy = sc->ls_cy;
	if (ccx==sc->ls_width) {
		ccx--;
	}

	/* Set the cursor position */
	bus_write_register(sc->ls_busdata, sc->ls_buspos,
			   LSCR_REG_POSN, mergexy(ccx, ccy));

	spinlock_release(&sc->ls_lock);
}