int32_t locktest(int32_t nargs, char ** args) {
    // 0x800131d0
    inititems();
    kprintf("Starting lock test...\n");
    int32_t v1 = 0;
    int32_t v2 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1); // 0x8001321c
    int32_t v3 = v2; // 0x80013224
    int32_t v4; // 0x8001321c
    if (v2 != 0) {
        panic("locktest: thread_fork failed: %s\n", strerror(v3));
        v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x80013228
            panic("locktest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x80013224
    while (v5 < 32) {
        // 0x8001320c
        v1 = v5;
        v2 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("locktest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80013228
                panic("locktest: thread_fork failed: %s\n", strerror(v3));
                v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2b44), NULL, v1);
                v3 = v4;
            }
        }
        // 0x80013244
        v5 = v1 + 1;
    }
    int32_t v6 = 1; // 0x80013264
    P((int32_t *)g16);
    int32_t v7 = v6; // 0x80013270
    while (v6 < 32) {
        // 0x8001325c
        v6 = v7 + 1;
        P((int32_t *)g16);
        v7 = v6;
    }
    // 0x80013274
    kprintf("Lock test done.\n");
    return 0;
}