int locktest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting lock test...\n");
  data2 = 0;
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        P(donesem);
      }
      kprintf("Lock test done.\n");
      return 0;
    }
    iVar2 = thread_fork("synchtest",(proc *)0x0,(anon_subr_void_void_ptr_ulong *)&locktestthread,
                        (void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("locktest: thread_fork failed: %s\n",pcVar1);
}