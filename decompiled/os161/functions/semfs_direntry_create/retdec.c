int32_t * semfs_direntry_create(char * name, int32_t semnum) {
    char * v1 = kmalloc(8); // 0x80005f48
    if (v1 == NULL) {
        // 0x80005f84
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x80005f58
    *(int32_t *)v1 = (int32_t)v2;
    int32_t * result; // 0x80005f28
    if (v2 == NULL) {
        // 0x80005f64
        kfree(v1);
        result = NULL;
    } else {
        // 0x80005f74
        *(int32_t *)((int32_t)v1 + 4) = semnum;
        result = (int32_t *)v1;
    }
    // 0x80005f84
    return result;
}