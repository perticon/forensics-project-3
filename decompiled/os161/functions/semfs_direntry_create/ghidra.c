semfs_direntry * semfs_direntry_create(char *name,uint semnum)

{
  semfs_direntry *ptr;
  char *pcVar1;
  
  ptr = (semfs_direntry *)kmalloc(8);
  if (ptr == (semfs_direntry *)0x0) {
    ptr = (semfs_direntry *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    ptr->semd_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(ptr);
      ptr = (semfs_direntry *)0x0;
    }
    else {
      ptr->semd_semnum = semnum;
    }
  }
  return ptr;
}