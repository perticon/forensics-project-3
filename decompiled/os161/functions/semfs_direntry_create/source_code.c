semfs_direntry_create(const char *name, unsigned semnum)
{
	struct semfs_direntry *dent;

	dent = kmalloc(sizeof(*dent));
	if (dent == NULL) {
		return NULL;
	}
	dent->semd_name = kstrdup(name);
	if (dent->semd_name == NULL) {
		kfree(dent);
		return NULL;
	}
	dent->semd_semnum = semnum;
	return dent;
}