int tryattach_ltrace_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  ltrace_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_ltrace_to_lamebus(devunit,bus);
  if (sc == (ltrace_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("ltrace%d at lamebus%d",devunit,busunit);
    errcode = config_ltrace(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_ltrace = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}