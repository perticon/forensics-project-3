tryattach_ltrace_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct ltrace_softc *dev;
	int result;

	dev = attach_ltrace_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("ltrace%d at lamebus%d", devunit, busunit);
	result = config_ltrace(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_ltrace = devunit+1;
	autoconf_ltrace(dev, devunit);
	return 0;
}