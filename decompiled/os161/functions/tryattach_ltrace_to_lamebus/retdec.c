int32_t tryattach_ltrace_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_ltrace_to_lamebus(devunit, bus); // 0x80001278
    if (v1 == NULL) {
        // 0x800012f0
        return -1;
    }
    // 0x80001284
    kprintf("ltrace%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_ltrace(v1, devunit); // 0x800012a0
    int32_t result; // 0x8000125c
    if (v2 == 0) {
        // 0x800012cc
        kprintf("\n");
        nextunit_ltrace = devunit + 1;
        result = 0;
    } else {
        // 0x800012ac
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x800012f0
    return result;
}