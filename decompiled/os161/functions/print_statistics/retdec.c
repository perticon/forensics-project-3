void print_statistics(void) {
    // 0x8001ba60
    kprintf("\n\nVirtual memory statistics:\n\n");
    kprintf("----------------------------------------\n");
    kprintf("TLB Faults: %ld                         \n", tlb_misses);
    kprintf("----------------------------------------\n");
    kprintf("TLB Faults with Free: %ld               \n", tlb_misses_free);
    kprintf("----------------------------------------\n");
    kprintf("TLB Faults with Replace: %ld            \n", tlb_misses_full);
    kprintf("----------------------------------------\n");
    kprintf("TLB Invalidations: %ld                  \n", tlb_invalidations);
    kprintf("----------------------------------------\n");
    kprintf("TLB Reloads: %ld                        \n", tlb_reloads);
    kprintf("----------------------------------------\n");
    kprintf("Page Faults (Zeroed): %ld               \n", new_pages_zeroed);
    kprintf("----------------------------------------\n");
    kprintf("Page Faults (Disk): %ld                 \n", faults_with_load);
    kprintf("----------------------------------------\n");
    kprintf("Page Faults from ELF: %ld               \n", faults_with_elf_load);
    kprintf("----------------------------------------\n");
    kprintf("Page Faults from Swapfile: %ld          \n", swap_in_pages);
    kprintf("----------------------------------------\n");
    kprintf("Swapfile Writes: %ld                    \n", swap_out_pages);
    kprintf("----------------------------------------\n\n");
    int32_t v1 = tlb_misses_full + tlb_misses_free; // 0x8001bbb8
    int32_t v2 = v1; // 0x8001bbc8
    int32_t v3 = 1; // 0x8001bbc8
    if (v1 != tlb_misses) {
        // 0x8001bbcc
        kprintf("\nWarning: TLB Faults with Free + TLB Faults with Replace != TLB Faults\n");
        v2 = tlb_misses;
        v3 = 0;
    }
    int32_t v4 = faults_with_load; // 0x8001bc18
    int32_t v5 = v3; // 0x8001bc18
    if (faults_with_load + tlb_reloads + new_pages_zeroed != v2) {
        // 0x8001bc1c
        kprintf("\nWarning: TLB Reloads + Page Faults (Disk) + Page Faults (Zeroed) != TLB Faults \n");
        v4 = faults_with_load;
        v5 = 0;
    }
    // 0x8001bc28
    if (swap_in_pages + faults_with_elf_load != v4) {
        // 0x8001bc6c
        kprintf("\nWarning: Page Faults from ELF %ld + Swapfile Writes %ld != Page Faults (Disk) %ld\n", faults_with_elf_load, swap_out_pages, v4);
        // 0x8001bc80
        return;
    }
    // 0x8001bc6c
    if (v5 != 0) {
        // 0x8001bc74
        kprintf("All sums are correct.\n\n");
    }
}