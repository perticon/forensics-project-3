int as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
                     int readable, int writeable, int executable)
{
  size_t npages;

  vm_can_sleep();

  /* Align the region. First, the base... */
  sz += vaddr & ~(vaddr_t)PAGE_FRAME;
  vaddr &= PAGE_FRAME;

  /* ...and now the length. */
  sz = (sz + PAGE_SIZE - 1) & PAGE_FRAME;

  npages = sz / PAGE_SIZE;

  /* We don't use these - all text's segments are read-only and all the others read-write */
  (void)readable;
  (void)writeable;
  (void)executable;

  if (as->as_vbase1 == 0)
  {
    as->as_vbase1 = vaddr;
    as->as_npages1 = npages;
    return 0;
  }

  if (as->as_vbase2 == 0)
  {
    as->as_vbase2 = vaddr;
    as->as_npages2 = npages;
    return 0;
  }

  /*
	 * Support for more than two regions is not available.
	 */
  kprintf("dumbvm: Warning: too many regions\n");
  return ENOSYS;
}