int32_t as_define_region(int32_t * as, uint32_t vaddr, int32_t sz, int32_t readable, int32_t writeable, int32_t executable) {
    // 0x8001adfc
    vm_can_sleep();
    if (as == NULL) {
        // 0x8001ae44
        abort();
        // UNREACHABLE
    }
    int32_t v1 = (int32_t)as;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x8001ae50
    int32_t result; // 0x8001adfc
    if (*v2 == 0) {
        // 0x8001ae60
        *v2 = vaddr & -0x1000;
        *(int32_t *)(v1 + 12) = (sz + 4095 + vaddr % 0x1000) / 0x1000;
        result = 0;
    } else {
        // 0x8001ae6c
        kprintf("dumbvm: Warning: too many regions\n");
        result = 1;
    }
    // 0x8001ae7c
    return result;
}