int as_define_region(addrspace *as,vaddr_t vaddr,size_t sz,int readable,int writeable,int executable
                    )

{
  vaddr_t vVar1;
  uint uVar2;
  
  vm_can_sleep();
  vVar1 = as->as_vbase1;
  uVar2 = (vaddr & 0xfff) + sz + 0xfff >> 0xc;
  if (vVar1 == 0) {
    as->as_vbase1 = vaddr & 0xfffff000;
    as->as_npages1 = uVar2;
  }
  else {
    vVar1 = as->as_vbase2;
    if (vVar1 == 0) {
      as->as_vbase2 = vaddr & 0xfffff000;
      as->as_npages2 = uVar2;
    }
    else {
      kprintf("dumbvm: Warning: too many regions\n");
      vVar1 = 1;
    }
  }
  return vVar1;
}