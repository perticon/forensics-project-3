vopfail_uio_nosys(struct vnode *vn, struct uio *uio)
{
	(void)vn;
	(void)uio;
	return ENOSYS;
}