int32_t vfs_getcwd(int32_t * uio) {
    char colon = 58; // bp-20, 0x800181c4
    if (*(int32_t *)((int32_t)uio + 24) != 0) {
        // 0x800181d8
        badassert("uio->uio_rw==UIO_READ", "../../vfs/vfscwd.c", 151, "vfs_getcwd");
    }
    // 0x800181f8
    int32_t * cwd; // bp-24, 0x800181b0
    int32_t result = vfs_getcurdir(&cwd); // 0x800181fc
    if (result != 0) {
        // 0x80018320
        return result;
    }
    int32_t v1 = *(int32_t *)((int32_t)cwd + 12); // 0x80018210
    int32_t v2 = v1; // 0x8001821c
    if (v1 == 0) {
        // 0x80018220
        badassert("cwd->vn_fs != NULL", "../../vfs/vfscwd.c", 159, "vfs_getcwd");
        v2 = (int32_t)"cwd->vn_fs != NULL";
    }
    int32_t v3 = *(int32_t *)(*(int32_t *)(v2 + 4) + 4); // 0x80018244
    int32_t v4 = v3; // 0x80018258
    if (v3 == 0) {
        // 0x80018280
        vfs_biglock_acquire();
        char * v5 = vfs_getdevname((int32_t *)*(int32_t *)((int32_t)cwd + 12)); // 0x80018274
        vfs_biglock_release();
        v4 = (int32_t)v5;
        if (v5 == NULL) {
            // 0x80018288
            badassert("name != NULL", "../../vfs/vfscwd.c", 167, "vfs_getcwd");
            v4 = 0;
        }
    }
    char * v6 = (char *)v4; // 0x800182a8
    int32_t v7 = uiomove(v6, strlen(v6), uio); // 0x800182b8
    int32_t result2 = v7; // 0x800182c0
    if (v7 == 0) {
        int32_t v8 = uiomove(&colon, 1, uio); // 0x800182d0
        result2 = v8;
        if (v8 == 0) {
            // 0x800182dc
            vnode_check(cwd, "namefile");
            result2 = *(int32_t *)(*(int32_t *)((int32_t)cwd + 20) + 56);
        }
    }
    // 0x80018310
    vnode_decref(cwd);
    // 0x80018320
    return result2;
}