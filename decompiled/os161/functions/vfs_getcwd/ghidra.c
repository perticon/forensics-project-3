int vfs_getcwd(uio *uio)

{
  int iVar1;
  size_t n;
  char *pcVar2;
  vnode *cwd;
  char colon;
  
  colon = ':';
  if (uio->uio_rw != UIO_READ) {
    badassert("uio->uio_rw==UIO_READ","../../vfs/vfscwd.c",0x97,"vfs_getcwd");
  }
  iVar1 = vfs_getcurdir(&cwd);
  if (iVar1 == 0) {
    pcVar2 = (char *)cwd->vn_fs;
    if ((fs *)pcVar2 == (fs *)0x0) {
      pcVar2 = "cwd->vn_fs != NULL";
      badassert("cwd->vn_fs != NULL","../../vfs/vfscwd.c",0x9f,"vfs_getcwd");
    }
    pcVar2 = (*(*(fs_ops **)((int)pcVar2 + 4))->fsop_getvolname)((fs *)pcVar2);
    if (pcVar2 == (char *)0x0) {
      vfs_biglock_acquire();
      pcVar2 = vfs_getdevname(cwd->vn_fs);
      vfs_biglock_release();
    }
    if (pcVar2 == (char *)0x0) {
      badassert("name != NULL","../../vfs/vfscwd.c",0xa7,"vfs_getcwd");
    }
    n = strlen(pcVar2);
    iVar1 = uiomove(pcVar2,n,uio);
    if ((iVar1 == 0) && (iVar1 = uiomove(&colon,1,uio), iVar1 == 0)) {
      vnode_check(cwd,"namefile");
      iVar1 = (*cwd->vn_ops->vop_namefile)(cwd,uio);
    }
    vnode_decref(cwd);
  }
  return iVar1;
}