int32_t vfs_rename(char * oldpath, char * newpath) {
    char newname[256]; // bp-264, 0x8001a29c
    char oldname[256]; // bp-524, 0x8001a29c
    // 0x8001a29c
    int32_t * olddir; // bp-528, 0x8001a29c
    int32_t result = vfs_lookparent(oldpath, (int32_t *)&olddir, oldname, 256); // 0x8001a2b8
    if (result != 0) {
        // 0x8001a390
        return result;
    }
    // 0x8001a2c4
    int32_t * newdir; // bp-268, 0x8001a29c
    int32_t result2 = vfs_lookparent(newpath, (int32_t *)&newdir, newname, 256); // 0x8001a2d0
    if (result2 != 0) {
        // 0x8001a2dc
        vnode_decref(olddir);
        // 0x8001a390
        return result2;
    }
    int32_t v1 = *(int32_t *)((int32_t)olddir + 12); // 0x8001a2f8
    if (v1 != 0) {
        int32_t v2 = *(int32_t *)((int32_t)newdir + 12); // 0x8001a310
        if (v2 != 0 == v1 == v2) {
            // 0x8001a348
            vnode_check(olddir, "rename");
            vnode_decref(newdir);
            vnode_decref(olddir);
            // 0x8001a390
            return *(int32_t *)(*(int32_t *)((int32_t)olddir + 20) + 84);
        }
    }
    // 0x8001a328
    vnode_decref(newdir);
    vnode_decref(olddir);
    // 0x8001a390
    return 24;
}