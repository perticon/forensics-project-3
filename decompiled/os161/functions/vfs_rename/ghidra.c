int vfs_rename(char *oldpath,char *newpath)

{
  int iVar1;
  vnode *olddir;
  char oldname [256];
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookparent(oldpath,&olddir,oldname,0x100);
  if (iVar1 == 0) {
    iVar1 = vfs_lookparent(newpath,&newdir,newname,0x100);
    if (iVar1 == 0) {
      if (((olddir->vn_fs == (fs *)0x0) || (newdir->vn_fs == (fs *)0x0)) ||
         (olddir->vn_fs != newdir->vn_fs)) {
        vnode_decref(newdir);
        vnode_decref(olddir);
        iVar1 = 0x18;
      }
      else {
        vnode_check(olddir,"rename");
        iVar1 = (*olddir->vn_ops->vop_rename)(olddir,oldname,newdir,newname);
        vnode_decref(newdir);
        vnode_decref(olddir);
      }
    }
    else {
      vnode_decref(olddir);
    }
  }
  return iVar1;
}