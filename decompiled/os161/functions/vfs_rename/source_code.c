vfs_rename(char *oldpath, char *newpath)
{
	struct vnode *olddir;
	char oldname[NAME_MAX+1];
	struct vnode *newdir;
	char newname[NAME_MAX+1];
	int result;

	result = vfs_lookparent(oldpath, &olddir, oldname, sizeof(oldname));
	if (result) {
		return result;
	}
	result = vfs_lookparent(newpath, &newdir, newname, sizeof(newname));
	if (result) {
		VOP_DECREF(olddir);
		return result;
	}

	if (olddir->vn_fs==NULL || newdir->vn_fs==NULL ||
	    olddir->vn_fs != newdir->vn_fs) {
		VOP_DECREF(newdir);
		VOP_DECREF(olddir);
		return EXDEV;
	}

	result = VOP_RENAME(olddir, oldname, newdir, newname);

	VOP_DECREF(newdir);
	VOP_DECREF(olddir);

	return result;
}