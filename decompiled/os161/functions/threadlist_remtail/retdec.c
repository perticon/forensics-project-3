int32_t * threadlist_remtail(int32_t * tl) {
    int32_t v1 = (int32_t)tl;
    int32_t v2 = *(int32_t *)(v1 + 12); // 0x800178f0
    int32_t * v3 = (int32_t *)v2; // 0x800178f8
    int32_t * result = NULL; // 0x80017904
    if (*v3 != 0) {
        // 0x80017908
        threadlist_removenode(v3);
        int32_t * v4 = (int32_t *)(v1 + 24); // 0x80017910
        *v4 = *v4 - 1;
        result = (int32_t *)*(int32_t *)(v2 + 8);
    }
    // 0x80017930
    return result;
}