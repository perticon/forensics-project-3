thread * threadlist_remtail(threadlist *tl)

{
  thread *ptVar1;
  threadlistnode *tln;
  
  tln = (tl->tl_tail).tln_prev;
  if (tln->tln_prev == (threadlistnode *)0x0) {
    ptVar1 = (thread *)0x0;
  }
  else {
    threadlist_removenode(tln);
    tl->tl_count = tl->tl_count - 1;
    ptVar1 = tln->tln_self;
  }
  return ptVar1;
}