threadlist_remtail(struct threadlist *tl)
{
	struct threadlistnode *tln;

	DEBUGASSERT(tl != NULL);

	tln = tl->tl_tail.tln_prev;
	if (tln->tln_prev == NULL) {
		/* list was empty  */
		return NULL;
	}
	threadlist_removenode(tln);
	DEBUGASSERT(tl->tl_count > 0);
	tl->tl_count--;
	return tln->tln_self;
}