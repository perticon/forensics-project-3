void semfs_wakeup(semfs_sem *sem,uint newcount)

{
  if ((sem->sems_count == 0) && (newcount != 0)) {
    if (newcount == 1) {
      cv_signal(sem->sems_cv,sem->sems_lock);
    }
    else {
      cv_broadcast(sem->sems_cv,sem->sems_lock);
    }
  }
  return;
}