semfs_wakeup(struct semfs_sem *sem, unsigned newcount)
{
	if (sem->sems_count > 0 || newcount == 0) {
		return;
	}
	if (newcount == 1) {
		cv_signal(sem->sems_cv, sem->sems_lock);
	}
	else {
		cv_broadcast(sem->sems_cv, sem->sems_lock);
	}
}