void semfs_wakeup(int32_t * sem, int32_t newcount) {
    int32_t v1 = (int32_t)sem;
    if (newcount == 0 | *(int32_t *)(v1 + 8) != 0) {
        // 0x8000624c
        return;
    }
    int32_t * v2 = (int32_t *)*(int32_t *)(v1 + 4);
    if (newcount == 1) {
        // 0x8000621c
        cv_signal(v2, v2);
    } else {
        // 0x80006234
        cv_broadcast(v2, v2);
    }
}