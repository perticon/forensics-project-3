cmd_deadlockthread(void *ptr, unsigned long num)
{
	struct deadlock *dl = ptr;

	(void)num;

	/* If it doesn't wedge right away, keep trying... */
	while (1)
	{
		lock_acquire(dl->lock2);
		lock_acquire(dl->lock1);
		kprintf("+");
		lock_release(dl->lock1);
		lock_release(dl->lock2);
	}
}