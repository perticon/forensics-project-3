void cmd_deadlockthread(char * ptr, int32_t num) {
    int32_t * v1 = (int32_t *)((int32_t)ptr + 4); // 0x8000c44c
    while (true) {
        int32_t * v2 = (int32_t *)*v1; // 0x8000c454
        lock_acquire(v2);
        lock_acquire(v2);
        kprintf("+");
        lock_release((int32_t *)"+");
        lock_release((int32_t *)*v1);
    }
}