void bitmap_destroy(bitmap *b)

{
  kfree(b->v);
  kfree(b);
  return;
}