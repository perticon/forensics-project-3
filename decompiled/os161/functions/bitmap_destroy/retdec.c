void bitmap_destroy(int32_t * b) {
    // 0x8000afa8
    kfree((char *)*(int32_t *)((int32_t)b + 4));
    kfree((char *)b);
}