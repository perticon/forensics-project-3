int32_t getch_intr(int32_t * cs) {
    int32_t v1 = (int32_t)cs;
    P((int32_t *)*(int32_t *)(v1 + 12));
    int32_t * v2 = (int32_t *)(v1 + 56); // 0x80001c34
    int32_t v3 = *v2; // 0x80001c34
    *v2 = (v3 + 1) % 32;
    return (int32_t)*(char *)(v1 + 20 + v3);
}