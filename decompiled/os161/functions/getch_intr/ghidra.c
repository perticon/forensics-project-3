int getch_intr(con_softc *cs)

{
  byte bVar1;
  
  P(cs->cs_rsem);
  bVar1 = cs->cs_gotchars[cs->cs_gotchars_tail];
  cs->cs_gotchars_tail = cs->cs_gotchars_tail + 1 & 0x1f;
  return (uint)bVar1;
}