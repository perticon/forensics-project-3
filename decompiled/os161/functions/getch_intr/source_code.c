getch_intr(struct con_softc *cs)
{
	unsigned char ret;

	P(cs->cs_rsem);
	ret = cs->cs_gotchars[cs->cs_gotchars_tail];
	cs->cs_gotchars_tail =
		(cs->cs_gotchars_tail + 1) % CONSOLE_INPUT_BUFFER_SIZE;
	return ret;
}