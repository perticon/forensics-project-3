int32_t cmd_pwd(int32_t nargs, char ** args) {
    char buf[1025]; // bp-1080, 0x8000c4b0
    // 0x8000c4b0
    int32_t iov; // bp-52, 0x8000c4b0
    int32_t ku; // bp-40, 0x8000c4b0
    uio_kinit(&iov, &ku, buf, 1024, 0, 0);
    int32_t result = vfs_getcwd(&ku); // 0x8000c4e8
    if (result == 0) {
        // 0x8000c514
        int32_t v1; // 0x8000c4b0
        *(char *)(1024 - v1 + (int32_t)&buf) = 0;
        kprintf("%s\n", buf);
    } else {
        // 0x8000c4f4
        kprintf("vfs_getcwd failed (%s)\n", strerror(result));
    }
    // 0x8000c544
    return result;
}