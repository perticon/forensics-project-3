cmd_pwd(int nargs, char **args)
{
	char buf[PATH_MAX + 1];
	int result;
	struct iovec iov;
	struct uio ku;

	(void)nargs;
	(void)args;

	uio_kinit(&iov, &ku, buf, sizeof(buf) - 1, 0, UIO_READ);
	result = vfs_getcwd(&ku);
	if (result)
	{
		kprintf("vfs_getcwd failed (%s)\n", strerror(result));
		return result;
	}

	/* null terminate */
	buf[sizeof(buf) - 1 - ku.uio_resid] = 0;

	/* print it */
	kprintf("%s\n", buf);

	return 0;
}