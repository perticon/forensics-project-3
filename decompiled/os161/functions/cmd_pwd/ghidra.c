int cmd_pwd(int nargs,char **args)

{
  int errcode;
  char *pcVar1;
  char buf [1025];
  iovec iov;
  uio ku;
  
  uio_kinit(&iov,&ku,buf,0x400,0,UIO_READ);
  errcode = vfs_getcwd(&ku);
  if (errcode == 0) {
    buf[0x400 - ku.uio_resid] = '\0';
    kprintf("%s\n",buf);
    errcode = 0;
  }
  else {
    pcVar1 = strerror(errcode);
    kprintf("vfs_getcwd failed (%s)\n",pcVar1);
  }
  return errcode;
}