semfs_sem * semfs_sem_create(char *name)

{
  semfs_sem *ptr;
  lock *plVar1;
  cv *pcVar2;
  char lockname [32];
  char cvname [32];
  
  snprintf(lockname,0x20,"sem:l.%s",name);
  snprintf(cvname,0x20,"sem:%s",name);
  ptr = (semfs_sem *)kmalloc(0x10);
  if (ptr != (semfs_sem *)0x0) {
    plVar1 = lock_create(lockname);
    ptr->sems_lock = plVar1;
    if (plVar1 != (lock *)0x0) {
      pcVar2 = cv_create(cvname);
      ptr->sems_cv = pcVar2;
      if (pcVar2 != (cv *)0x0) {
        ptr->sems_count = 0;
        ptr->sems_hasvnode = false;
        ptr->sems_linked = false;
        return ptr;
      }
      lock_destroy(ptr->sems_lock);
    }
    kfree(ptr);
  }
  return (semfs_sem *)0x0;
}