int32_t * semfs_sem_create(char * name) {
    char cvname[32]; // bp-40, 0x80005d48
    char lockname[32]; // bp-72, 0x80005d48
    // 0x80005d48
    snprintf(lockname, 32, "sem:l.%s", name);
    snprintf(cvname, 32, "sem:%s", name);
    char * v1 = kmalloc(16); // 0x80005d8c
    if (v1 == NULL) {
        // 0x80005dec
        return NULL;
    }
    int32_t * v2 = lock_create(lockname); // 0x80005d9c
    *(int32_t *)v1 = (int32_t)v2;
    if (v2 == NULL) {
        // 0x80005dd8
        kfree(v1);
        // 0x80005dec
        return NULL;
    }
    int32_t v3 = (int32_t)v1; // 0x80005d8c
    int32_t * v4 = cv_create(cvname); // 0x80005dac
    *(int32_t *)(v3 + 4) = (int32_t)v4;
    if (v4 == NULL) {
        // 0x80005dcc
        lock_destroy((int32_t *)*(int32_t *)v1);
        // 0x80005dd8
        kfree(v1);
        // 0x80005dec
        return NULL;
    }
    // 0x80005db8
    *(int32_t *)(v3 + 8) = 0;
    *(char *)(v3 + 12) = 0;
    *(char *)(v3 + 13) = 0;
    // 0x80005dec
    return (int32_t *)v1;
}