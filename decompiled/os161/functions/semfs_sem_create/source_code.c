semfs_sem_create(const char *name)
{
	struct semfs_sem *sem;
	char lockname[32];
	char cvname[32];

	snprintf(lockname, sizeof(lockname), "sem:l.%s", name);
	snprintf(cvname, sizeof(cvname), "sem:%s", name);

	sem = kmalloc(sizeof(*sem));
	if (sem == NULL) {
		goto fail_return;
	}
	sem->sems_lock = lock_create(lockname);
	if (sem->sems_lock == NULL) {
		goto fail_sem;
	}
	sem->sems_cv = cv_create(cvname);
	if (sem->sems_cv == NULL) {
		goto fail_lock;
	}
	sem->sems_count = 0;
	sem->sems_hasvnode = false;
	sem->sems_linked = false;
	return sem;

 fail_lock:
	lock_destroy(sem->sems_lock);
 fail_sem:
	kfree(sem);
 fail_return:
	return NULL;
}