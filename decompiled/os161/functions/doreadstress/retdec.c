void doreadstress(char * filesys) {
    // 0x800102d0
    init_threadsem();
    kprintf("*** Starting fs read stress test on %s:\n", filesys);
    int32_t v1 = fstest_write(filesys, (char *)&g10, 1, 0); // 0x80010314
    int32_t v2 = 0; // 0x8001031c
    if (v1 != 0) {
        // 0x80010320
        kprintf("*** Test failed\n");
        // 0x800103e4
        return;
    }
    int32_t v3 = thread_fork("readstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1308), filesys, v2); // 0x80010344
    if (v3 != 0) {
        // 0x80010350
        panic("readstress: thread_fork failed: %s\n", strerror(v3));
    }
    // 0x80010368
    v2++;
    while (v2 < 12) {
        // 0x80010334
        v3 = thread_fork("readstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1308), filesys, v2);
        if (v3 != 0) {
            // 0x80010350
            panic("readstress: thread_fork failed: %s\n", strerror(v3));
        }
        // 0x80010368
        v2++;
    }
    int32_t v4 = 1; // 0x800103a0
    P(threadsem);
    int32_t v5 = v4; // 0x800103ac
    while (v4 < 12) {
        // 0x80010398
        v4 = v5 + 1;
        P(threadsem);
        v5 = v4;
    }
    // 0x800103b0
    if (fstest_remove(filesys, (char *)&g10) == 0) {
        // 0x800103d8
        kprintf("*** fs read stress test done\n");
    } else {
        // 0x800103c4
        kprintf("*** Test failed\n");
    }
}