void doreadstress(char *filesys)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  
  init_threadsem();
  kprintf("*** Starting fs read stress test on %s:\n",filesys);
  iVar1 = fstest_write(filesys,"",1,0);
  data2 = 0;
  if (iVar1 == 0) {
    for (; (int)data2 < 0xc; data2 = data2 + 1) {
      iVar1 = thread_fork("readstress",(proc *)0x0,readstress_thread,filesys,data2);
      if (iVar1 != 0) {
        pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
        panic("readstress: thread_fork failed: %s\n",pcVar2);
      }
    }
    for (iVar1 = 0; iVar1 < 0xc; iVar1 = iVar1 + 1) {
      P(threadsem);
    }
    iVar1 = fstest_remove(filesys,"");
    if (iVar1 == 0) {
      kprintf("*** fs read stress test done\n");
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    kprintf("*** Test failed\n");
  }
  return;
}