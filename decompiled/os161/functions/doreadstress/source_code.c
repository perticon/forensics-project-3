doreadstress(const char *filesys)
{
	int i, err;

	init_threadsem();

	kprintf("*** Starting fs read stress test on %s:\n", filesys);

	if (fstest_write(filesys, "", 1, 0)) {
		kprintf("*** Test failed\n");
		return;
	}

	for (i=0; i<NTHREADS; i++) {
		err = thread_fork("readstress", NULL,
				  readstress_thread, (char *)filesys, i);
		if (err) {
			panic("readstress: thread_fork failed: %s\n",
			      strerror(err));
		}
	}

	for (i=0; i<NTHREADS; i++) {
		P(threadsem);
	}

	if (fstest_remove(filesys, "")) {
		kprintf("*** Test failed\n");
		return;
	}

	kprintf("*** fs read stress test done\n");
}