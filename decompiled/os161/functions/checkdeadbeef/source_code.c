checkdeadbeef(void *block, size_t blocksize)
{
	uint32_t *ptr = block;
	size_t i;

	for (i=1; i < blocksize/sizeof(uint32_t); i++) {
		KASSERT(ptr[i] == 0xdeadbeef);
	}
}