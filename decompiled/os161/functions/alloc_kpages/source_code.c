vaddr_t alloc_kpages(unsigned npages)
{
  paddr_t pa;
  unsigned i;

  vm_can_sleep();
  pa = getppages(npages, 1);
  if (pa == 0)
  {
    return 0;
  }
  for (i = 0; i < npages; i++)
  {
    ipt_kadd(-2, pa + i * PAGE_SIZE, 0);
  }
  return PADDR_TO_KVADDR(pa);
}