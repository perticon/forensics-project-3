int32_t alloc_kpages(uint32_t npages) {
    // 0x8001aad0
    vm_can_sleep();
    int32_t v1 = getppages(npages, 1); // 0x8001aaf4
    if (v1 == 0) {
        // 0x8001ab38
        return 0;
    }
    // 0x8001ab1c
    for (int32_t i = 0; i < npages; i++) {
        // 0x8001ab08
        ipt_kadd(-2, 0x1000 * i + v1, 0);
    }
    // 0x8001ab38
    return -v1;
}