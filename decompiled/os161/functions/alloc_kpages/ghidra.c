vaddr_t alloc_kpages(uint npages)

{
  paddr_t pVar1;
  vaddr_t vVar2;
  uint uVar3;
  
  vm_can_sleep();
  pVar1 = getppages(npages,1);
  if (pVar1 == 0) {
    vVar2 = 0;
  }
  else {
    for (uVar3 = 0; uVar3 < npages; uVar3 = uVar3 + 1) {
      ipt_kadd(-2,uVar3 * 0x1000 + pVar1,0);
    }
    vVar2 = pVar1 + 0x80000000;
  }
  return vVar2;
}