int32_t hash_delete(uint32_t pid, int32_t vaddr) {
    int32_t v1 = pid; // 0x8001d17c
    int32_t v2 = vaddr; // 0x8001d17c
    if (pid <= 0) {
        // 0x8001d180
        badassert("pid > 0", "../../vm/pt.c", 243, "hash_delete");
        v1 = &g41;
        v2 = (int32_t)"../../vm/pt.c";
    }
    int32_t v3 = v1; // 0x8001d1a4
    int32_t v4 = v2; // 0x8001d1a4
    if (v2 <= 0xffffffff) {
        // 0x8001d1a8
        badassert("vaddr < 0x80000000", "../../vm/pt.c", 244, "hash_delete");
        v3 = &g41;
        v4 = 244;
    }
    // 0x8001d1c8
    STdelete(ipt_hash, v3, v4);
    return 0;
}