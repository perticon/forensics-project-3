int hash_delete(pid_t pid,vaddr_t vaddr)

{
  char *pcVar1;
  
  if (pid < 1) {
    pcVar1 = "../../vm/pt.c";
    badassert("pid > 0","../../vm/pt.c",0xf3,"hash_delete");
    vaddr = (vaddr_t)pcVar1;
  }
  if ((int)vaddr < 0) {
    vaddr = 0xf4;
    badassert("vaddr < 0x80000000","../../vm/pt.c",0xf4,"hash_delete");
  }
  STdelete(ipt_hash,pid,vaddr);
  return 0;
}