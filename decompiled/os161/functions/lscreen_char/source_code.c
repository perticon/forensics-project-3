lscreen_char(struct lscreen_softc *sc, int ch)
{
	if (sc->ls_cx >= sc->ls_width) {
		lscreen_newline(sc);
	}

	sc->ls_screen[sc->ls_cy*sc->ls_width + sc->ls_cx] = ch;
	sc->ls_cx++;
}