int cmd_shell(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 1) {
    *args = "/bin/sh";
    iVar1 = common_prog(1,args);
  }
  else {
    kprintf("Usage: s\n");
    iVar1 = 8;
  }
  return iVar1;
}