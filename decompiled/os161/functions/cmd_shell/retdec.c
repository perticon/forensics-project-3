int32_t cmd_shell(int32_t nargs, char ** args) {
    int32_t result; // 0x8000c86c
    if (nargs == 1) {
        // 0x8000c890
        *(int32_t *)args = (int32_t)"/bin/sh";
        result = common_prog(1, args);
    } else {
        // 0x8000c87c
        kprintf("Usage: s\n");
        result = 8;
    }
    // 0x8000c8a0
    return result;
}