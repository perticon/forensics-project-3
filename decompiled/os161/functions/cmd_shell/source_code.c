cmd_shell(int nargs, char **args)
{
	(void)args;
	if (nargs != 1)
	{
		kprintf("Usage: s\n");
		return EINVAL;
	}

	args[0] = (char *)_PATH_SHELL;

	return common_prog(nargs, args);
}