void join32to64(uint32_t x1,uint32_t x2,uint64_t *y2)

{
  *(uint32_t *)y2 = x1;
  *(uint32_t *)((int)y2 + 4) = x2;
  return;
}