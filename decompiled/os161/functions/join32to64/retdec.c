void join32to64(int32_t x1, int32_t x2, int64_t * y2) {
    // 0x8000b0a8
    *(int32_t *)y2 = x1;
    *(int32_t *)((int32_t)y2 + 4) = x2;
}