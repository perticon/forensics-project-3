join32to64(uint32_t x1, uint32_t x2, uint64_t *y2)
{
#if _BYTE_ORDER == _BIG_ENDIAN
	*y2 = ((uint64_t)x1 << 32) | (uint64_t)x2;
#elif _BYTE_ORDER == _LITTLE_ENDIAN
	*y2 = (uint64_t)x1 | ((uint64_t)x2 << 32);
#else
#error "Eh?"
#endif
}