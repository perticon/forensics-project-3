void lock_acquire(int32_t * lock) {
    int32_t v1 = (int32_t)lock;
    int32_t v2 = v1; // 0x80015948
    if (lock == NULL) {
        // 0x8001594c
        badassert("lock != NULL", "../../thread/synch.c", 202, "lock_acquire");
        v2 = (int32_t)"lock != NULL";
    }
    // 0x8001596c
    if (lock_do_i_hold((int32_t *)v2)) {
        // 0x8001597c
        kprintf("AAACKK!\n");
    }
    // 0x80015984
    if (lock_do_i_hold(lock)) {
        // 0x80015994
        badassert("!(lock_do_i_hold(lock))", "../../thread/synch.c", 206, "lock_acquire");
    }
    // 0x800159b0
    int32_t v3; // 0x80015934
    if (*(char *)(v3 + 88) != 0) {
        // 0x800159c0
        badassert("curthread->t_in_interrupt == false", "../../thread/synch.c", 208, "lock_acquire");
    }
    // 0x800159dc
    P((int32_t *)*(int32_t *)(v1 + 4));
    int32_t * v4 = (int32_t *)(v1 + 8); // 0x800159ec
    spinlock_acquire(v4);
    if (*(int32_t *)(v1 + 16) != 0) {
        // 0x80015a00
        badassert("lock->lk_owner == NULL", "../../thread/synch.c", 226, "lock_acquire");
    }
    // 0x80015a1c
    spinlock_release(v4);
}