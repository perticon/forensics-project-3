lock_acquire(struct lock *lock)
{
        // Write this
#if OPT_SYNCH
        KASSERT(lock != NULL);
	if (lock_do_i_hold(lock)) {
	  kprintf("AAACKK!\n");
	}
	KASSERT(!(lock_do_i_hold(lock)));

        KASSERT(curthread->t_in_interrupt == false);

#if USE_SEMAPHORE_FOR_LOCK
/*
 *  G.Cabodi - 2019: P BEFORE(!!!) spinlock acquire. OS161 forbids sleeping/realeasing
 *  the CPU while owning a spinlocks: this could be a cause of deadlock. 
 *  THE spinlock passed to wchan_wait is the only one allowed. 
 *  This is checked in various parts of the code (see for instance wchan_sleep.
 *  as P may result in "wait", it cannot be called while owning the spinlock.
 */
        P(lock->lk_sem);
	spinlock_acquire(&lock->lk_lock);        
#else
	spinlock_acquire(&lock->lk_lock);        
	while (lock->lk_owner != NULL) {
	  wchan_sleep(lock->lk_wchan, &lock->lk_lock);
        }
#endif
        KASSERT(lock->lk_owner == NULL);
        lock->lk_owner=curthread;
	spinlock_release(&lock->lk_lock);
#endif
        (void)lock;  // suppress warning until code gets written
}