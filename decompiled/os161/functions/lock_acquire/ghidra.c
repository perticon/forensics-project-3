void lock_acquire(lock *lock)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  bool bVar1;
  char *lock_00;
  thread *unaff_s7;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    lock_00 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xca,"lock_acquire");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  if (CONCAT31(extraout_var,bVar1) != 0) {
    kprintf("AAACKK!\n");
  }
  bVar1 = lock_do_i_hold(lock);
  if (CONCAT31(extraout_var_00,bVar1) != 0) {
    badassert("!(lock_do_i_hold(lock))","../../thread/synch.c",0xce,"lock_acquire");
  }
  if (unaff_s7->t_in_interrupt != false) {
    badassert("curthread->t_in_interrupt == false","../../thread/synch.c",0xd0,"lock_acquire");
  }
  P(lock->lk_sem);
  spinlock_acquire(&lock->lk_lock);
  if (lock->lk_owner != (thread *)0x0) {
    badassert("lock->lk_owner == NULL","../../thread/synch.c",0xe2,"lock_acquire");
  }
  lock->lk_owner = unaff_s7;
  spinlock_release(&lock->lk_lock);
  return;
}