static void update_tlb(vaddr_t faultaddress, paddr_t paddr)
{

	int i;
	uint32_t ehi, elo;
	uint32_t ehi1, elo1;
	int victim;
	int spl;
	/* Disable interrupts on this CPU while frobbing the TLB. */

	ehi = faultaddress;

	if (address_segment(faultaddress, curproc->p_addrspace) == 1)
	{
		elo = (paddr & ~TLBLO_DIRTY) | TLBLO_VALID;
	}
	else
	{
		elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
	}

	spl = splhigh();

	/* add entry in the TLB */
	for (i = 0; i < NUM_TLB; i++)
	{
		tlb_read(&ehi1, &elo1, i);
		if (elo1 & TLBLO_VALID)
		{
			continue;
		}

		increase(TLB_MISS_FREE);
		tlb_write(ehi, elo, i);

		break;
	}
	/* if all entry are occupied, find a victim and replace it */
	if (i == NUM_TLB)
	{
		/*select a victim to be replaced*/
		victim = tlb_get_rr_victim();
		tlb_write(ehi, elo, victim);
	}
	splx(spl);
}