void update_tlb(int32_t faultaddress, int32_t paddr) {
    // 0x8001e2cc
    int32_t v1; // 0x8001e2cc
    int32_t v2 = *(int32_t *)(*(int32_t *)(v1 + 84) + 16); // 0x8001e2f4
    int32_t v3 = address_segment(faultaddress, (int32_t *)v2) == 1 ? paddr & -1537 | 512 : paddr | 1536;
    int32_t v4 = splx(1); // 0x8001e320
    int32_t v5 = 0;
    int32_t ehi1; // bp-32, 0x8001e2cc
    int32_t elo1; // bp-28, 0x8001e2cc
    tlb_read(&ehi1, &elo1, v5);
    while ((elo1 & 512) != 0) {
        int32_t v6 = v5 + 1; // 0x8001e354
        if (v6 >= 64) {
            // 0x8001e390
            tlb_write(faultaddress, v3, tlb_get_rr_victim());
            goto lab_0x8001e3a8;
        }
        v5 = v6;
        tlb_read(&ehi1, &elo1, v5);
    }
    // 0x8001e388
    increase(1);
    tlb_write(faultaddress, v3, v5);
  lab_0x8001e3a8:
    // 0x8001e3a8
    splx(v4);
}