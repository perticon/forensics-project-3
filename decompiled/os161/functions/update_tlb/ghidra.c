void update_tlb(vaddr_t faultaddress,paddr_t paddr)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  int unaff_s7;
  uint32_t ehi1;
  uint32_t elo1;
  
  iVar1 = address_segment(faultaddress,*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
  if (iVar1 == 1) {
    uVar3 = paddr & 0xfffff9ff | 0x200;
  }
  else {
    uVar3 = paddr | 0x600;
  }
  iVar1 = splx(1);
  iVar2 = 0;
  do {
    if (0x3f < iVar2) {
LAB_8001e388:
      if (iVar2 == 0x40) {
        iVar2 = tlb_get_rr_victim();
        tlb_write(faultaddress,uVar3,iVar2);
      }
      splx(iVar1);
      return;
    }
    tlb_read(&ehi1,&elo1,iVar2);
    if ((elo1 & 0x200) == 0) {
      increase(1);
      tlb_write(faultaddress,uVar3,iVar2);
      goto LAB_8001e388;
    }
    iVar2 = iVar2 + 1;
  } while( true );
}