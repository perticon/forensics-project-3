int32_t semu21(int32_t nargs, char ** args) {
    // 0x800128f4
    kprintf("This should assert that we aren't in an interrupt\n");
    int32_t * v1 = makesem(1); // 0x8001290c
    int32_t v2; // 0x800128f4
    *(char *)(v2 + 88) = 1;
    P(v1);
    panic("semu21: P tolerated being in an interrupt handler\n");
    return &g41;
}