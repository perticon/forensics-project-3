semu21(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	kprintf("This should assert that we aren't in an interrupt\n");

	sem = makesem(1);
	/* as above */
	curthread->t_in_interrupt = true;
	P(sem);
	panic("semu21: P tolerated being in an interrupt handler\n");
	return 0;
}