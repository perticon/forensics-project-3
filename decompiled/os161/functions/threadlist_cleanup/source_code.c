threadlist_cleanup(struct threadlist *tl)
{
	DEBUGASSERT(tl != NULL);
	DEBUGASSERT(tl->tl_head.tln_next == &tl->tl_tail);
	DEBUGASSERT(tl->tl_head.tln_prev == NULL);
	DEBUGASSERT(tl->tl_tail.tln_next == NULL);
	DEBUGASSERT(tl->tl_tail.tln_prev == &tl->tl_head);
	DEBUGASSERT(tl->tl_head.tln_self == NULL);
	DEBUGASSERT(tl->tl_tail.tln_self == NULL);

	KASSERT(threadlist_isempty(tl));
	KASSERT(tl->tl_count == 0);

	/* nothing (else) to do */
}