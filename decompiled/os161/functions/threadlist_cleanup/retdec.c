void threadlist_cleanup(int32_t * tl) {
    // 0x80017798
    if (!threadlist_isempty(tl)) {
        // 0x800177b4
        badassert("threadlist_isempty(tl)", "../../thread/threadlist.c", 85, "threadlist_cleanup");
    }
    // 0x800177d0
    if (*(int32_t *)((int32_t)tl + 24) != 0) {
        // 0x800177e0
        badassert("tl->tl_count == 0", "../../thread/threadlist.c", 86, "threadlist_cleanup");
    }
}