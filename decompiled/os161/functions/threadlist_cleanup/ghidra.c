void threadlist_cleanup(threadlist *tl)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = threadlist_isempty(tl);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("threadlist_isempty(tl)","../../thread/threadlist.c",0x55,"threadlist_cleanup");
  }
  if (tl->tl_count != 0) {
    badassert("tl->tl_count == 0","../../thread/threadlist.c",0x56,"threadlist_cleanup");
  }
  return;
}