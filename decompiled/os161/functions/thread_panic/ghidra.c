void thread_panic(void)

{
  int unaff_s7;
  
  ipi_broadcast(0);
  *(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 0x50) = 0;
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x3c) = *(int *)(unaff_s7 + 0x50) + 0x44;
  *(int *)(*(int *)(unaff_s7 + 0x50) + 0x44) = *(int *)(unaff_s7 + 0x50) + 0x38;
  return;
}