void menu(char *args)
{
	char buf[64];

	menu_execute(args, 1);

	while (1)
	{
		kprintf("OS/161 kernel [? for menu]: ");
		kgets(buf, sizeof(buf));
		menu_execute(buf, 0);
	}
}