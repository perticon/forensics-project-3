int dev_write(vnode *v,uio *uio)

{
  int iVar1;
  char *pcVar2;
  undefined4 unaff_s0;
  device *d;
  undefined4 in_stack_fffffff0;
  
  d = (device *)v->vn_data;
  iVar1 = dev_tryseek(d,CONCAT44(in_stack_fffffff0,unaff_s0));
  if (iVar1 == 0) {
    pcVar2 = (char *)d;
    if (uio->uio_rw != UIO_WRITE) {
      pcVar2 = "uio->uio_rw == UIO_WRITE";
      badassert("uio->uio_rw == UIO_WRITE","../../vfs/device.c",0x8a,"dev_write");
    }
    iVar1 = (*d->d_ops->devop_io)((device *)pcVar2,uio);
  }
  return iVar1;
}