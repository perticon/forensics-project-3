dev_write(struct vnode *v, struct uio *uio)
{
	struct device *d = v->vn_data;
	int result;

	result = dev_tryseek(d, uio->uio_offset);
	if (result) {
		return result;
	}

	KASSERT(uio->uio_rw == UIO_WRITE);
	return DEVOP_IO(d, uio);
}