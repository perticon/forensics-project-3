int32_t dev_write(int32_t * v2, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t * v2_ = (int32_t *)*(int32_t *)((int32_t)v2 + 16); // 0x80017d54
    int32_t result = dev_tryseek(v2_, (int64_t)*(int32_t *)(v1 + 8)); // 0x80017d54
    if (result != 0) {
        // 0x80017da8
        return result;
    }
    // 0x80017d60
    if (*(int32_t *)(v1 + 24) != 1) {
        // 0x80017d70
        badassert("uio->uio_rw == UIO_WRITE", "../../vfs/device.c", 138, "dev_write");
    }
    // 0x80017da8
    return *(int32_t *)(*v2_ + 4);
}