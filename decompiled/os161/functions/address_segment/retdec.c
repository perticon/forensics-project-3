int32_t address_segment(int32_t faultaddress, int32_t * as) {
    int32_t v1 = (int32_t)as;
    int32_t v2; // 0x8001e0b0
    int32_t v3; // 0x8001e0b0
    int32_t v4; // 0x8001e0b0
    if (*(int32_t *)(*(int32_t *)(v4 + 84) + 16) == v1) {
        // 0x8001e0ec
        v2 = faultaddress;
        v3 = v1;
        if (as == NULL) {
            // 0x8001e0f4
            badassert("as != NULL", "../../vm/vm_tlb.c", 83, "address_segment");
            v2 = (int32_t)"as != NULL";
            v3 = (int32_t)"../../vm/vm_tlb.c";
        }
    } else {
        // 0x8001e0ec
        badassert("as == curproc->p_addrspace", "../../vm/vm_tlb.c", 80, "address_segment");
        v2 = (int32_t)"as == curproc->p_addrspace";
        v3 = (int32_t)"../../vm/vm_tlb.c";
    }
    uint32_t v5 = *(int32_t *)(v3 + 8); // 0x8001e168
    int32_t v6 = v2; // 0x8001e174
    int32_t v7 = v3; // 0x8001e174
    int32_t v8 = 0x62f2e000; // 0x8001e174
    int32_t v9 = 0x2e2f2e2e; // 0x8001e174
    if (v5 == 0) {
        // 0x8001e178
        badassert("as->as_vbase2 != 0", "../../vm/vm_tlb.c", 86, (char *)-0x7ffddf54);
        v6 = (int32_t)"as->as_vbase2 != 0";
        v7 = (int32_t)"../../vm/vm_tlb.c";
        v8 = 0x56000;
        v9 = &g41;
    }
    int32_t v10 = *(int32_t *)(v7 + 12); // 0x8001e194
    int32_t v11 = v6; // 0x8001e1a0
    int32_t v12 = v10; // 0x8001e1a0
    int32_t v13 = v8; // 0x8001e1a0
    int32_t v14 = v9; // 0x8001e1a0
    if (v10 == 0) {
        // 0x8001e1a4
        badassert("as->as_npages2 != 0", "../../vm/vm_tlb.c", 87, "address_segment");
        v11 = (int32_t)"as->as_npages2 != 0";
        v12 = (int32_t)"../../vm/vm_tlb.c";
        v13 = 0x57000;
        v14 = &g41;
    }
    int32_t v15 = v14;
    int32_t v16 = v11; // 0x8001e1cc
    int32_t v17 = v12; // 0x8001e1cc
    int32_t v18 = v13; // 0x8001e1cc
    int32_t v19 = v15; // 0x8001e1cc
    if (v15 != (v15 & -0x1000)) {
        // 0x8001e1d0
        badassert("(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1", "../../vm/vm_tlb.c", 88, "address_segment");
        v16 = (int32_t)"(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1";
        v17 = (int32_t)"../../vm/vm_tlb.c";
        v18 = 0x58000;
        v19 = &g41;
    }
    int32_t v20 = v16; // 0x8001e1f8
    int32_t v21 = v17; // 0x8001e1f8
    int32_t v22 = v18; // 0x8001e1f8
    int32_t v23 = v19; // 0x8001e1f8
    if (v5 != (v5 & -0x1000)) {
        // 0x8001e1fc
        badassert("(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2", "../../vm/vm_tlb.c", 89, "address_segment");
        v20 = (int32_t)"(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2";
        v21 = (int32_t)"../../vm/vm_tlb.c";
        v22 = 89;
        v23 = &g41;
    }
    uint32_t v24 = v23;
    uint32_t v25 = v20;
    if (v25 >= v24 == v25 < v24 + v22) {
        // 0x8001e27c
        return 1;
    }
    // 0x8001e23c
    if (v25 >= v5 == v25 < 0x1000 * v21 + v5) {
        // 0x8001e27c
        return 2;
    }
    int32_t result = 6; // 0x8001e264
    if (v25 >= 0x7ffee000) {
        // 0x8001e268
        result = v25 > -1 ? 3 : 6;
    }
    // 0x8001e27c
    return result;
}