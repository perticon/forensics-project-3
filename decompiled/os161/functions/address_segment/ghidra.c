int address_segment(vaddr_t faultaddress,addrspace *as)

{
  char *pcVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  undefined4 *puVar5;
  size_t sVar6;
  int iVar7;
  char *pcVar8;
  int unaff_s7;
  
  pcVar4 = (char *)faultaddress;
  if (*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10) != as) {
    pcVar4 = "as == curproc->p_addrspace";
    as = (addrspace *)&DAT_800282e8;
    badassert("as == curproc->p_addrspace",(char *)&DAT_800282e8,0x50,"address_segment");
  }
  if (as == (addrspace *)0x0) {
    pcVar4 = "as != NULL";
    as = (addrspace *)&DAT_800282e8;
    badassert("as != NULL",(char *)&DAT_800282e8,0x53,"address_segment");
  }
  pcVar1 = (char *)as->as_vbase1;
  if (pcVar1 == (char *)0x0) {
    pcVar4 = "as->as_vbase1 != 0";
    as = (addrspace *)&DAT_800282e8;
    badassert("as->as_vbase1 != 0",(char *)&DAT_800282e8,0x54,"address_segment");
  }
  sVar6 = as->as_npages1;
  pcVar8 = (char *)0x80020000;
  if (sVar6 == 0) {
    pcVar4 = "as->as_npages1 != 0";
    as = (addrspace *)&DAT_800282e8;
    sVar6 = 0x55;
    pcVar8 = "address_segment";
    badassert("as->as_npages1 != 0",(char *)&DAT_800282e8,0x55,"address_segment");
  }
  pcVar3 = (char *)as->as_vbase2;
  if (pcVar3 == (char *)0x0) {
    pcVar4 = "as->as_vbase2 != 0";
    as = (addrspace *)&DAT_800282e8;
    sVar6 = 0x56;
    badassert("as->as_vbase2 != 0",(char *)&DAT_800282e8,0x56,pcVar8 + 0x20ac);
  }
  puVar5 = (undefined4 *)as->as_npages2;
  if (puVar5 == (undefined4 *)0x0) {
    pcVar4 = "as->as_npages2 != 0";
    puVar5 = &DAT_800282e8;
    sVar6 = 0x57;
    badassert("as->as_npages2 != 0",(char *)&DAT_800282e8,0x57,"address_segment");
  }
  if (pcVar1 != (char *)((uint)pcVar1 & 0xfffff000)) {
    pcVar4 = "(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1";
    puVar5 = &DAT_800282e8;
    sVar6 = 0x58;
    badassert("(as->as_vbase1 & PAGE_FRAME) == as->as_vbase1",(char *)&DAT_800282e8,0x58,
              "address_segment");
  }
  iVar7 = sVar6 << 0xc;
  if (pcVar3 != (char *)((uint)pcVar3 & 0xfffff000)) {
    pcVar4 = "(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2";
    puVar5 = &DAT_800282e8;
    iVar7 = 0x59;
    badassert("(as->as_vbase2 & PAGE_FRAME) == as->as_vbase2",(char *)&DAT_800282e8,0x59,
              "address_segment");
  }
  if ((((pcVar4 < pcVar1) || (iVar2 = 1, pcVar1 + iVar7 <= pcVar4)) &&
      ((pcVar4 < pcVar3 || (iVar2 = 2, pcVar3 + (int)puVar5 * 0x1000 <= pcVar4)))) &&
     ((iVar2 = 6, (char *)0x7ffedfff < pcVar4 && (iVar2 = 3, (char *)0x7fffffff < pcVar4)))) {
    iVar2 = 6;
  }
  return iVar2;
}