void thread_switch(threadstate_t newstate,wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  int spl;
  bool bVar2;
  thread *ptVar1;
  cpu *pcVar3;
  thread *unaff_s7;
  
  spl = splx(1);
  pcVar3 = unaff_s7->t_cpu;
  if (pcVar3->c_isidle == false) {
    thread_checkstack(unaff_s7);
    spinlock_acquire(&pcVar3->c_runqueue_lock);
    if ((newstate == S_READY) &&
       (bVar2 = threadlist_isempty(&unaff_s7->t_cpu->c_runqueue), CONCAT31(extraout_var,bVar2) != 0)
       ) {
      spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
      splx(spl);
    }
    else {
      if (newstate == S_READY) {
        thread_make_runnable(unaff_s7,true);
        unaff_s7->t_state = S_READY;
      }
      else {
        if (newstate == S_RUN) {
                    /* WARNING: Subroutine does not return */
          panic("Illegal S_RUN in thread_switch\n");
        }
        if (newstate == S_SLEEP) {
          unaff_s7->t_wchan_name = wc->wc_name;
          threadlist_addtail(&wc->wc_threads,unaff_s7);
          spinlock_release(lk);
          unaff_s7->t_state = S_SLEEP;
        }
        else if (newstate == S_ZOMBIE) {
          unaff_s7->t_wchan_name = "ZOMBIE";
          threadlist_addtail(&unaff_s7->t_cpu->c_zombies,unaff_s7);
          unaff_s7->t_state = S_ZOMBIE;
        }
        else {
          unaff_s7->t_state = newstate;
        }
      }
      unaff_s7->t_cpu->c_isidle = true;
      while (ptVar1 = threadlist_remhead(&unaff_s7->t_cpu->c_runqueue), ptVar1 == (thread *)0x0) {
        spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
        cpu_idle();
        spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
      }
      unaff_s7->t_cpu->c_isidle = false;
      unaff_s7->t_cpu->c_curthread = ptVar1;
      switchframe_switch(&unaff_s7->t_context,&ptVar1->t_context);
      unaff_s7->t_wchan_name = (char *)0x0;
      unaff_s7->t_state = S_RUN;
      spinlock_release(&ptVar1->t_cpu->c_runqueue_lock);
      as_activate();
      exorcise();
      splx(spl);
    }
  }
  else {
    splx(spl);
  }
  return;
}