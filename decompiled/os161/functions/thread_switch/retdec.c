void thread_switch(int32_t newstate, int32_t * wc, int32_t * lk) {
    int32_t v1 = splx(1); // 0x80016a80
    int32_t v2; // 0x80016a50
    int32_t * v3 = (int32_t *)(v2 + 80); // 0x80016a84
    int32_t v4 = *v3; // 0x80016a84
    if (*(char *)(v4 + 52) != 0) {
        // 0x80016a9c
        splx(v1);
        // 0x80016c24
        return;
    }
    int32_t * v5 = (int32_t *)v2; // 0x80016ab4
    thread_checkstack(v5);
    spinlock_acquire((int32_t *)(v4 + 84));
    int32_t v6; // 0x80016a50
    switch (newstate) {
        case 1: {
            // 0x80016acc
            if (threadlist_isempty((int32_t *)(*v3 + 56))) {
                // 0x80016ae0
                spinlock_release((int32_t *)(*v3 + 84));
                splx(v1);
                // 0x80016c24
                return;
            }
            goto lab_0x80016b30;
        }
        case 0: {
            // 0x80016b24
            panic("Illegal S_RUN in thread_switch\n");
            v6 = (int32_t)"Illegal S_RUN in thread_switch\n";
            goto lab_0x80016b30;
        }
        case 2: {
            int32_t v7 = (int32_t)wc;
            *(int32_t *)(v2 + 4) = v7;
            threadlist_addtail((int32_t *)(v7 + 4), v5);
            spinlock_release(lk);
            *(int32_t *)(v2 + 8) = 2;
            goto lab_0x80016b88;
        }
        case 3: {
            // 0x80016b68
            *(int32_t *)(v2 + 4) = (int32_t)"ZOMBIE";
            threadlist_addtail((int32_t *)(*v3 + 16), v5);
            *(int32_t *)(v2 + 8) = 3;
            goto lab_0x80016b88;
        }
        default: {
            // 0x80016b1c
            *(int32_t *)(v2 + 8) = newstate;
            goto lab_0x80016b88;
        }
    }
  lab_0x80016b88:
    // 0x80016b88
    *(char *)(*v3 + 52) = 1;
    int32_t * v8 = threadlist_remhead((int32_t *)(*v3 + 56)); // 0x80016b9c
    int32_t v9 = *v3;
    int32_t v10 = v9; // 0x80016a50
    int32_t * v11 = v8; // 0x80016a50
    int32_t v12 = v9; // 0x80016a50
    if (v8 == NULL) {
        spinlock_release((int32_t *)(v10 + 84));
        cpu_idle();
        spinlock_acquire((int32_t *)(*v3 + 84));
        int32_t * v13 = threadlist_remhead((int32_t *)(*v3 + 56)); // 0x80016b9c
        int32_t v14 = *v3;
        v10 = v14;
        v11 = v13;
        v12 = v14;
        while (v13 == NULL) {
            // 0x80016ba8
            spinlock_release((int32_t *)(v10 + 84));
            cpu_idle();
            spinlock_acquire((int32_t *)(*v3 + 84));
            v13 = threadlist_remhead((int32_t *)(*v3 + 56));
            v14 = *v3;
            v10 = v14;
            v11 = v13;
            v12 = v14;
        }
    }
    int32_t v15 = (int32_t)v11;
    *(char *)(v12 + 52) = 0;
    *(int32_t *)(*v3 + 12) = v15;
    switchframe_switch(v2 + 76, v15 + 76);
    *(int32_t *)(v2 + 4) = 0;
    *(int32_t *)(v2 + 8) = 0;
    spinlock_release((int32_t *)(*(int32_t *)(v15 + 80) + 84));
    as_activate();
    exorcise();
    splx(v1);
  lab_0x80016b30:
    // 0x80016b30
    thread_make_runnable((int32_t *)v6, true);
    *(int32_t *)(v2 + 8) = newstate;
    goto lab_0x80016b88;
}