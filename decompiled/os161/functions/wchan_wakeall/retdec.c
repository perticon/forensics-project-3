void wchan_wakeall(int32_t * wc, int32_t * lk) {
    // 0x80016f4c
    if (!spinlock_do_i_hold(lk)) {
        // 0x80016f6c
        badassert("spinlock_do_i_hold(lk)", "../../thread/thread.c", 1064, "wchan_wakeall");
    }
    // 0x80016f88
    int32_t list; // bp-40, 0x80016f4c
    threadlist_init(&list);
    int32_t * v1 = (int32_t *)((int32_t)wc + 4); // 0x80016fa4
    int32_t * v2 = threadlist_remhead(v1); // 0x80016fa4
    if (v2 != NULL) {
        threadlist_addtail(&list, v2);
        int32_t * v3 = threadlist_remhead(v1); // 0x80016fa4
        while (v3 != NULL) {
            // 0x80016f98
            threadlist_addtail(&list, v3);
            v3 = threadlist_remhead(v1);
        }
    }
    int32_t * v4 = threadlist_remhead(&list); // 0x80016fc4
    if (v4 == NULL) {
        // 0x80016fd0
        threadlist_cleanup(&list);
        return;
    }
    thread_make_runnable(v4, false);
    int32_t * v5 = threadlist_remhead(&list); // 0x80016fc4
    while (v5 != NULL) {
        // 0x80016fb8
        thread_make_runnable(v5, false);
        v5 = threadlist_remhead(&list);
    }
    // 0x80016fd0
    threadlist_cleanup(&list);
}