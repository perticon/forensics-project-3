void wchan_wakeall(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar2;
  thread *ptVar1;
  threadlist list;
  
  bVar2 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x428,"wchan_wakeall");
  }
  threadlist_init(&list);
  while( true ) {
    ptVar1 = threadlist_remhead(&wc->wc_threads);
    if (ptVar1 == (thread *)0x0) break;
    threadlist_addtail(&list,ptVar1);
  }
  while (ptVar1 = threadlist_remhead(&list), ptVar1 != (thread *)0x0) {
    thread_make_runnable(ptVar1,false);
  }
  threadlist_cleanup(&list);
  return;
}