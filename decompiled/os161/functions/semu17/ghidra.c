int semu17(int nargs,char **args)

{
  semaphore *sem;
  int iVar1;
  thread *ptVar2;
  
  semu17_thread = (thread *)0x0;
  sem = makesem(0);
  iVar1 = thread_fork("semu17_sub",(proc *)0x0,semu17_sub,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semu17: whoops: thread_fork failed\n");
  }
  kprintf("Waiting for subthread...\n");
  clocksleep(1);
  ptVar2 = semu17_thread;
  if (semu17_thread == (thread *)0x0) {
    badassert("semu17_thread != NULL","../../test/semunit.c",0x293,"semu17");
  }
  if (ptVar2->t_state != S_SLEEP) {
    badassert("semu17_thread->t_state == S_SLEEP","../../test/semunit.c",0x294,"semu17");
  }
  ok();
  V(sem);
  clocksleep(1);
  sem_destroy(sem);
  semu17_thread = (thread *)0x0;
  return 0;
}