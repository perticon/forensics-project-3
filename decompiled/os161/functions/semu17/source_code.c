semu17(int nargs, char **args)
{
	struct semaphore *sem;
	int result;

	(void)nargs; (void)args;

	semu17_thread = NULL;

	sem = makesem(0);
	result = thread_fork("semu17_sub", NULL, semu17_sub, sem, 0);
	if (result) {
		panic("semu17: whoops: thread_fork failed\n");
	}
	kprintf("Waiting for subthread...\n");
	clocksleep(1);

	/* The subthread should be blocked. */
	KASSERT(semu17_thread != NULL);
	KASSERT(semu17_thread->t_state == S_SLEEP);

	/* Clean up. */
	ok();
	V(sem);
	clocksleep(1);
	sem_destroy(sem);
	semu17_thread = NULL;
	return 0;
}