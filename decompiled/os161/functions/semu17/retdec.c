int32_t semu17(int32_t nargs, char ** args) {
    // 0x800123f4
    *(int32_t *)&semu17_thread = 0;
    int32_t * v1 = makesem(0); // 0x8001240c
    int32_t v2 = thread_fork("semu17_sub", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x1634), (char *)v1, 0); // 0x80012430
    if (v2 != 0) {
        // 0x8001243c
        panic("semu17: whoops: thread_fork failed\n");
    }
    // 0x80012448
    kprintf("Waiting for subthread...\n");
    clocksleep(1);
    int32_t v3 = (int32_t)semu17_thread; // 0x8001246c
    if (semu17_thread == NULL) {
        // 0x80012470
        badassert("semu17_thread != NULL", "../../test/semunit.c", 659, "semu17");
        v3 = &g41;
    }
    // 0x8001248c
    if (*(int32_t *)(v3 + 8) != 2) {
        // 0x8001249c
        badassert("semu17_thread->t_state == S_SLEEP", "../../test/semunit.c", 660, "semu17");
    }
    // 0x800124b8
    ok();
    V(v1);
    clocksleep(1);
    sem_destroy(v1);
    *(int32_t *)&semu17_thread = 0;
    return 0;
}