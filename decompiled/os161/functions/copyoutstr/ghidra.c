int copyoutstr(char *src,userptr_t userdest,size_t len,size_t *actual)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(userdest,len,&stoplen);
  if (iVar1 == 0) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar2 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    iVar1 = 6;
    if (iVar2 == 0) {
      iVar1 = copystr(&userdest->_dummy,src,len,stoplen,actual);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
    }
  }
  return iVar1;
}