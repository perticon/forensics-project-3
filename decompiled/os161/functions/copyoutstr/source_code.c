copyoutstr(const char *src, userptr_t userdest, size_t len, size_t *actual)
{
	int result;
	size_t stoplen;

	result = copycheck(userdest, len, &stoplen);
	if (result) {
		return result;
	}

	curthread->t_machdep.tm_badfaultfunc = copyfail;

	result = setjmp(curthread->t_machdep.tm_copyjmp);
	if (result) {
		curthread->t_machdep.tm_badfaultfunc = NULL;
		return EFAULT;
	}

	result = copystr((char *)userdest, src, len, stoplen, actual);

	curthread->t_machdep.tm_badfaultfunc = NULL;
	return result;
}