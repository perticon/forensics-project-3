int32_t copyoutstr(char * src, int32_t * userdest, int32_t len, int32_t * actual) {
    // 0x800203f4
    int32_t stoplen; // bp-16, 0x800203f4
    int32_t result = copycheck(userdest, len, &stoplen); // 0x80020418
    if (result != 0) {
        // 0x80020470
        return result;
    }
    // 0x80020424
    int32_t v1; // 0x800203f4
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x8002042c
    *v2 = -0x7ffdfdcc;
    int32_t result2; // 0x800203f4
    if (setjmp(v1 + 16) == 0) {
        int32_t v3 = copystr((char *)userdest, src, len, stoplen, actual); // 0x80020468
        *v2 = 0;
        result2 = v3;
    } else {
        // 0x80020440
        *v2 = 0;
        result2 = 6;
    }
    // 0x80020470
    return result2;
}