void semfs_sem_destroy(semfs_sem *sem)

{
  cv_destroy(sem->sems_cv);
  lock_destroy(sem->sems_lock);
  kfree(sem);
  return;
}