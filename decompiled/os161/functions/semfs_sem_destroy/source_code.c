semfs_sem_destroy(struct semfs_sem *sem)
{
	cv_destroy(sem->sems_cv);
	lock_destroy(sem->sems_lock);
	kfree(sem);
}