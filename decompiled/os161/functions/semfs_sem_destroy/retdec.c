void semfs_sem_destroy(int32_t * sem) {
    int32_t * v1 = (int32_t *)*(int32_t *)((int32_t)sem + 4); // 0x80005e14
    cv_destroy(v1);
    lock_destroy(v1);
    kfree((char *)sem);
}