int32_t * attach_beep_to_ltimer(int32_t beepno, int32_t * ls) {
    char * v1 = kmalloc(8); // 0x80002444
    int32_t * result = NULL; // 0x8000244c
    if (v1 != NULL) {
        // 0x80002450
        *(int32_t *)v1 = (int32_t)ls;
        *(int32_t *)((int32_t)v1 + 4) = -0x7fffafa0;
        result = (int32_t *)v1;
    }
    // 0x80002464
    return result;
}