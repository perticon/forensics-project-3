beep_softc * attach_beep_to_ltimer(int beepno,ltimer_softc *ls)

{
  beep_softc *pbVar1;
  
  pbVar1 = (beep_softc *)kmalloc(8);
  if (pbVar1 == (beep_softc *)0x0) {
    pbVar1 = (beep_softc *)0x0;
  }
  else {
    pbVar1->bs_devdata = ls;
    pbVar1->bs_beep = ltimer_beep;
  }
  return pbVar1;
}