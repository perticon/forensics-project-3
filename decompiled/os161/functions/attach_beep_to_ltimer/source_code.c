attach_beep_to_ltimer(int beepno, struct ltimer_softc *ls)
{
	struct beep_softc *bs = kmalloc(sizeof(struct beep_softc));
	if (bs==NULL) {
		return NULL;
	}

	(void)beepno;  // unused

	bs->bs_devdata = ls;
	bs->bs_beep = ltimer_beep;

	return bs;
}