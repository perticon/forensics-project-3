lamebus_clear_ipi(struct lamebus_softc *lamebus, struct cpu *target)
{
	if (lamebus->ls_uniprocessor) {
		return;
	}
	write_ctlcpu_register(lamebus, target->c_hardware_number,
			      CTLCPU_CIPI, 0);
}