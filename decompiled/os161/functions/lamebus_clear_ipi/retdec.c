void lamebus_clear_ipi(int32_t * lamebus2, int32_t * target) {
    // 0x80004478
    if (*(int32_t *)((int32_t)lamebus2 + 268) == 0) {
        int32_t v1 = *(int32_t *)((int32_t)target + 8); // 0x80004490
        lamebus_write_register(lamebus2, 31, 1024 * v1 + 0x8004, 0);
    }
}