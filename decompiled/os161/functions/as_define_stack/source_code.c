int as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
  KASSERT(as != NULL);
  *stackptr = USERSTACK;
  return 0;
}