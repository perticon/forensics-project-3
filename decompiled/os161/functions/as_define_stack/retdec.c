int32_t as_define_stack(int32_t * as, int32_t * stackptr) {
    int32_t v1 = (int32_t)stackptr; // 0x8001b144
    if (as == NULL) {
        // 0x8001b148
        badassert("as != NULL", "../../vm/addrspace.c", 344, "as_define_stack");
        v1 = (int32_t)"../../vm/addrspace.c";
    }
    // 0x8001b16c
    *(int32_t *)v1 = -0x80000000;
    while (true) {
        // continue -> 0x8001b16c
    }
}