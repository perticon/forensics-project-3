int32_t * proc_setas(int32_t * newas) {
    // 0x8000cf18
    int32_t v1; // 0x8000cf18
    int32_t v2 = *(int32_t *)(v1 + 84); // 0x8000cf30
    if (v2 == 0) {
        // 0x8000cf40
        badassert("proc != NULL", "../../proc/proc.c", 442, "proc_setas");
    }
    int32_t * v3 = (int32_t *)(v2 + 8); // 0x8000cf68
    spinlock_acquire(v3);
    int32_t * v4 = (int32_t *)(v2 + 16); // 0x8000cf6c
    *v4 = (int32_t)newas;
    spinlock_release(v3);
    return (int32_t *)*v4;
}