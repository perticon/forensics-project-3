addrspace * proc_setas(addrspace *newas)

{
  int iVar1;
  addrspace *paVar2;
  int unaff_s7;
  
  iVar1 = *(int *)(unaff_s7 + 0x54);
  if (iVar1 == 0) {
    badassert("proc != NULL","../../proc/proc.c",0x1ba,"proc_setas");
  }
  spinlock_acquire((spinlock *)(iVar1 + 8));
  paVar2 = *(addrspace **)(iVar1 + 0x10);
  *(addrspace **)(iVar1 + 0x10) = newas;
  spinlock_release((spinlock *)(iVar1 + 8));
  return paVar2;
}