lamebus_map_area(struct lamebus_softc *bus, int slot, uint32_t offset)
{
	uint32_t address;

	(void)bus;   // not needed

	KASSERT(slot >= 0 && slot < LB_NSLOTS);

	address = LB_BASEADDR + slot*LB_SLOT_SIZE + offset;
	return (void *)address;
}