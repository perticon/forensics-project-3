void * lamebus_map_area(lamebus_softc *bus,int slot,uint32_t offset)

{
  char *pcVar1;
  
  pcVar1 = (char *)(slot << 0x10);
  if (0x1f < (uint)slot) {
    pcVar1 = "../../arch/sys161/dev/lamebus_machdep.c";
    offset = 0x8f;
    badassert("slot >= 0 && slot < LB_NSLOTS","../../arch/sys161/dev/lamebus_machdep.c",0x8f,
              "lamebus_map_area");
  }
  return pcVar1 + offset + 0xbfe00000;
}