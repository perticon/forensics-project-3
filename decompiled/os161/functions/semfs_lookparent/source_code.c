semfs_lookparent(struct vnode *dirvn, char *path,
		 struct vnode **resultdirvn, char *namebuf, size_t bufmax)
{
        if (strlen(path)+1 > bufmax) {
                return ENAMETOOLONG;
        }
        strcpy(namebuf, path);

        VOP_INCREF(dirvn);
        *resultdirvn = dirvn;
	return 0;
}