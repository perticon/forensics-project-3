int32_t semfs_lookparent(int32_t * dirvn, char * path, int32_t * resultdirvn, char * namebuf, uint32_t bufmax) {
    int32_t result = 7; // 0x800066d0
    if (strlen(path) + 1 <= bufmax) {
        // 0x800066d4
        strcpy(namebuf, path);
        vnode_incref(dirvn);
        *resultdirvn = (int32_t)dirvn;
        result = 0;
    }
    // 0x800066f4
    return result;
}