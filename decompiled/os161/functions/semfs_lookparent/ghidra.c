int semfs_lookparent(vnode *dirvn,char *path,vnode **resultdirvn,char *namebuf,size_t bufmax)

{
  size_t sVar1;
  int iVar2;
  
  sVar1 = strlen(path);
  if (bufmax < sVar1 + 1) {
    iVar2 = 7;
  }
  else {
    strcpy(namebuf,path);
    vnode_incref(dirvn);
    *resultdirvn = dirvn;
    iVar2 = 0;
  }
  return iVar2;
}