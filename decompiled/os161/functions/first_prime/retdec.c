int32_t first_prime(int32_t n) {
    int32_t result = n;
    int32_t v1 = is_prime(result); // 0x8000a06c
    int32_t v2 = result + 1; // 0x8000a074
    while (v1 == 0) {
        // 0x8000a068
        result = v2;
        v1 = is_prime(result);
        v2 = result + 1;
    }
    // 0x8000a078
    return result;
}