static int first_prime(int n)
{
    while( !is_prime(n) )
        n++;
    
    return n;
}