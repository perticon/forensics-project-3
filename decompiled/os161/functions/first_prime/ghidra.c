int first_prime(int n)

{
  int iVar1;
  int n_00;
  
  do {
    n_00 = n;
    iVar1 = is_prime(n_00);
    n = n_00 + 1;
  } while (iVar1 == 0);
  return n_00;
}