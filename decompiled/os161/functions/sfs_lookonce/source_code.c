sfs_lookonce(struct sfs_vnode *sv, const char *name,
		struct sfs_vnode **ret,
		int *slot)
{
	struct sfs_fs *sfs = sv->sv_absvn.vn_fs->fs_data;
	uint32_t ino;
	int result;

	result = sfs_dir_findname(sv, name, &ino, slot, NULL);
	if (result) {
		return result;
	}

	result = sfs_loadvnode(sfs, ino, SFS_TYPE_INVAL, ret);
	if (result) {
		return result;
	}

	if ((*ret)->sv_i.sfi_linkcount == 0) {
		panic("sfs: %s: name %s (inode %u) in dir %u has "
		      "linkcount 0\n", sfs->sfs_sb.sb_volname,
		      name, (*ret)->sv_ino, sv->sv_ino);
	}

	return 0;
}