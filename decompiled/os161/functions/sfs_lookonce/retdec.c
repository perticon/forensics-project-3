int32_t sfs_lookonce(int32_t * sv, char * name, int32_t ** ret, int32_t * slot) {
    int32_t v1 = (int32_t)sv;
    int32_t v2 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80007d30
    int32_t ino; // bp-32, 0x80007d04
    int32_t result = sfs_dir_findname(sv, name, &ino, slot, NULL); // 0x80007d3c
    if (result != 0) {
        // 0x80007d9c
        return result;
    }
    int32_t result2 = sfs_loadvnode((int32_t *)v2, ino, 0, ret); // 0x80007d54
    if (result2 != 0) {
        // 0x80007d9c
        return result2;
    }
    // 0x80007d60
    if (*(int16_t *)30 == 0) {
        int32_t v3 = *(int32_t *)536; // 0x80007d78
        int32_t v4 = *(int32_t *)(v1 + 536); // 0x80007d7c
        panic("sfs: %s: name %s (inode %u) in dir %u has linkcount 0\n", (char *)(v2 + 16), name, v3, v4);
    }
    // 0x80007d9c
    return 0;
}