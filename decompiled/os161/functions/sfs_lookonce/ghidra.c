int sfs_lookonce(sfs_vnode *sv,char *name,sfs_vnode **ret,int *slot)

{
  int iVar1;
  sfs_fs *sfs;
  uint32_t ino;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  iVar1 = sfs_dir_findname(sv,name,&ino,slot,(int *)0x0);
  if ((iVar1 == 0) && (iVar1 = sfs_loadvnode(sfs,ino,0,ret), iVar1 == 0)) {
    if (((*ret)->sv_i).sfi_linkcount == 0) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: name %s (inode %u) in dir %u has linkcount 0\n",(sfs->sfs_sb).sb_volname,name,
            (*ret)->sv_ino,sv->sv_ino);
    }
    iVar1 = 0;
  }
  return iVar1;
}