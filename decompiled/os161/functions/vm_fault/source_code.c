int vm_fault(int faulttype, vaddr_t faultaddress)
{
	paddr_t paddr;
	struct addrspace *as;
	int segment;
	int result;

	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype)
	{
	case VM_FAULT_READONLY:
		/* We always create pages read-write, so we can't get this */
		/* terminate the process instead of panicking */
		kprintf("VM_FAULT_READONLY: process exited\n");
		sys__exit(-1);

		/* should not get here */
		panic("VM: got VM_FAULT_READONLY, should not get here\n");
	case VM_FAULT_READ:
	case VM_FAULT_WRITE:
		break;
	default:
		return EINVAL;
	}

	if (curproc == NULL)
	{
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = proc_getas();
	if (as == NULL)
	{
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* get in which segment the faulting address is */
	segment = address_segment(faultaddress, as);
	if (segment == EFAULT)
	{
		kprintf("PID: %d\n", curproc->p_pid);
		kprintf("SEGMENTATION FAULT: process exited\n");
		sys__exit(-1);

		/* should not get here */
		panic("VM: got SEGMENTATION FAULT, should not get here\n");
		return EFAULT;
	}

	increase(TLB_MISS);

	spinlock_acquire(&tlb_fault_lock);
	/* check if page is in memory */
	paddr = ipt_lookup(curproc->p_pid, faultaddress);

	/* if it is in memory, paddr will be different than 0 */

	if (paddr == 0)
	/* page not in ipt */
	{
		/* are we in code or data segment? then, we should load the needed page */
		if (segment == 1 || segment == 2)
		{
			/* 
			 * page_offset_from_segbase will store the offset of the desired page 
			 * from the offset of the segment 
			 */
			vaddr_t page_offset_from_segbase;
			/* 
			 * faultaddress is at page multiple, if we subtract the segment address 
			 * we find the offset from the segment base 
			 */
			page_offset_from_segbase = faultaddress - (segment == 1 ? as->as_vbase1 : as->as_vbase2);
			spinlock_release(&tlb_fault_lock);

			/* as_prepare_load is a wrapper for getppages() -> will allocate a page and return the offset */

			paddr = as_prepare_load(1);
			KASSERT(paddr != 0);

			/* make sure it's page-aligned */
			KASSERT((paddr & PAGE_FRAME) == paddr);

			/* look in the swapfile (if the faulting address is not in code segment) */

			if (segment != 1)
			{
				result = swap_in(faultaddress, paddr);
			}
			else
			{
				result = 1;
			}

			as_complete_load(curproc->p_addrspace);
			if (result)
			{
				/* load page at vaddr = faultaddress if not in swapfile */

				result = load_page(page_offset_from_segbase, faultaddress, segment, paddr);
				if (result)
				{
					return -1;
				}
			}
			spinlock_acquire(&tlb_fault_lock);
			result = ipt_add(curproc->p_pid, paddr, faultaddress);

			if (result)
			{
				return -1;
			}

			update_tlb(faultaddress, paddr);

			spinlock_release(&tlb_fault_lock);

			return 0;
		}
		else
		{
			spinlock_release(&tlb_fault_lock);

			/* as_prepare_load is a wrapper for getppages() -> will allocate a page and return the offset */
			paddr = as_prepare_load(1);

			/* make sure it's page-aligned */
			KASSERT((paddr & PAGE_FRAME) == paddr);

			result = swap_in(faultaddress, paddr);
			if (result)
			{
				increase(NEW_PAGE_ZEROED);
			}
			spinlock_acquire(&tlb_fault_lock);

			KASSERT(paddr != 0);
		
			result = ipt_add(curproc->p_pid, paddr, faultaddress);

			if (result)
			{
				return -1;
			}

			update_tlb(faultaddress, paddr);

			spinlock_release(&tlb_fault_lock);
		}

		return 0;
	}
	else
	{

		increase(TLB_RELOAD);
		/* make sure it's page-aligned */
		KASSERT((paddr & PAGE_FRAME) == paddr);
		
		update_tlb(faultaddress, paddr);
		spinlock_release(&tlb_fault_lock);

		return 0;
	}
}