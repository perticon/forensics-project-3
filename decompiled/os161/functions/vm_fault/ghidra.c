int vm_fault(int faulttype,vaddr_t faultaddress)

{
  int iVar1;
  addrspace *as;
  paddr_t paddr;
  uint paddr_00;
  int iVar2;
  undefined4 *paddr_01;
  char *page;
  undefined4 *paddr_02;
  char *faultaddress_00;
  vaddr_t vVar3;
  int unaff_s7;
  
  faultaddress_00 = (char *)(faultaddress & 0xfffff000);
  if ((dbflags & 0x20) != 0) {
    kprintf("dumbvm: fault: 0x%x\n",faultaddress_00);
  }
  iVar1 = 8;
  if (-1 < faulttype) {
    if (faulttype < 2) {
      iVar1 = 6;
      if (*(int *)(unaff_s7 + 0x54) != 0) {
        as = proc_getas();
        if (as == (addrspace *)0x0) {
          iVar1 = 6;
        }
        else {
          iVar1 = address_segment((vaddr_t)faultaddress_00,as);
          if (iVar1 == 6) {
            kprintf("PID: %d\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
            kprintf("SEGMENTATION FAULT: process exited\n");
            sys__exit(-1);
                    /* WARNING: Subroutine does not return */
            panic("VM: got SEGMENTATION FAULT, should not get here\n");
          }
          increase(0);
          spinlock_acquire(&tlb_fault_lock);
          paddr = ipt_lookup(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),(vaddr_t)faultaddress_00);
          if (paddr == 0) {
            if (iVar1 - 1U < 2) {
              if (iVar1 == 1) {
                vVar3 = as->as_vbase1;
              }
              else {
                vVar3 = as->as_vbase2;
              }
              spinlock_release(&tlb_fault_lock);
              paddr_00 = as_prepare_load(1);
              if (paddr_00 == 0) {
                badassert("paddr != 0",(char *)&DAT_800282e8,0xd4,"vm_fault");
              }
              iVar2 = 1;
              if ((paddr_00 & 0xfffff000) != paddr_00) {
                badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0xd7,"vm_fault");
              }
              if (iVar1 == iVar2) {
                iVar2 = 1;
              }
              else {
                iVar2 = swap_in((vaddr_t)faultaddress_00,paddr_00);
              }
              as_complete_load(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
              if ((iVar2 == 0) ||
                 (iVar1 = load_page((int)faultaddress_00 - vVar3,(vaddr_t)faultaddress_00,iVar1,
                                    paddr_00), iVar1 == 0)) {
                spinlock_acquire(&tlb_fault_lock);
                iVar1 = ipt_add(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),paddr_00,
                                (vaddr_t)faultaddress_00);
                if (iVar1 == 0) {
                  update_tlb((vaddr_t)faultaddress_00,paddr_00);
                  spinlock_release(&tlb_fault_lock);
                  iVar1 = 0;
                }
                else {
                  iVar1 = -1;
                }
              }
              else {
                iVar1 = -1;
              }
            }
            else {
              spinlock_release(&tlb_fault_lock);
              paddr_01 = (undefined4 *)as_prepare_load(1);
              page = faultaddress_00;
              if ((undefined4 *)((uint)paddr_01 & 0xfffff000) != paddr_01) {
                page = "(paddr & PAGE_FRAME) == paddr";
                badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0x105,"vm_fault");
              }
              iVar1 = swap_in((vaddr_t)page,(paddr_t)paddr_01);
              if (iVar1 != 0) {
                increase(9);
              }
              spinlock_acquire(&tlb_fault_lock);
              paddr_02 = paddr_01;
              if (paddr_01 == (undefined4 *)0x0) {
                paddr_02 = &DAT_800282e8;
                badassert("paddr != 0",(char *)&DAT_800282e8,0x10e,"vm_fault");
              }
              iVar1 = ipt_add(*(pid_t *)(*(int *)(unaff_s7 + 0x54) + 0x1c),(paddr_t)paddr_02,
                              (vaddr_t)faultaddress_00);
              if (iVar1 == 0) {
                update_tlb((vaddr_t)faultaddress_00,(paddr_t)paddr_01);
                spinlock_release(&tlb_fault_lock);
                iVar1 = 0;
              }
              else {
                iVar1 = -1;
              }
            }
          }
          else {
            increase(4);
            if ((paddr & 0xfffff000) != paddr) {
              badassert("(paddr & PAGE_FRAME) == paddr",(char *)&DAT_800282e8,0x123,"vm_fault");
            }
            update_tlb((vaddr_t)faultaddress_00,paddr);
            spinlock_release(&tlb_fault_lock);
            iVar1 = 0;
          }
        }
      }
    }
    else {
      if (faulttype == 2) {
        kprintf("VM_FAULT_READONLY: process exited\n");
        sys__exit(-1);
                    /* WARNING: Subroutine does not return */
        panic("VM: got VM_FAULT_READONLY, should not get here\n");
      }
      iVar1 = 8;
    }
  }
  return iVar1;
}