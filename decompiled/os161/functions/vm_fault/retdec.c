int32_t vm_fault(uint32_t faulttype, int32_t faultaddress) {
    int32_t v1 = faultaddress & -0x1000; // 0x8001e408
    if ((dbflags & 32) != 0) {
        // 0x8001e40c
        kprintf("dumbvm: fault: 0x%x\n", v1);
    }
    // 0x8001e41c
    if (faulttype < 0) {
        // 0x8001e768
        return 8;
    }
    if (faulttype >= 2) {
        // 0x8001e430
        if (faulttype != 2) {
            // 0x8001e768
            return 8;
        }
        // 0x8001e438
        kprintf("VM_FAULT_READONLY: process exited\n");
        sys__exit(-1);
        panic("VM: got VM_FAULT_READONLY, should not get here\n");
    }
    // 0x8001e454
    int32_t v2; // 0x8001e3d0
    int32_t * v3 = (int32_t *)(v2 + 84); // 0x8001e454
    if (*v3 == 0) {
        // 0x8001e768
        return 6;
    }
    int32_t * v4 = proc_getas(); // 0x8001e468
    if (v4 == NULL) {
        // 0x8001e768
        return 6;
    }
    int32_t v5 = address_segment(v1, v4); // 0x8001e47c
    if (v5 == 6) {
        // 0x8001e490
        kprintf("PID: %d\n", *(int32_t *)(*v3 + 28));
        kprintf("SEGMENTATION FAULT: process exited\n");
        sys__exit(-1);
        panic("VM: got SEGMENTATION FAULT, should not get here\n");
    }
    // 0x8001e4c4
    increase(0);
    spinlock_acquire(&tlb_fault_lock);
    int32_t v6 = ipt_lookup(*(int32_t *)(*v3 + 28), v1); // 0x8001e4e8
    if (v6 != 0) {
        // 0x8001e6f0
        increase(4);
        if ((v6 & -0x1000) != v6) {
            // 0x8001e708
            badassert("(paddr & PAGE_FRAME) == paddr", "../../vm/vm_tlb.c", 291, "vm_fault");
        }
        // 0x8001e724
        update_tlb(v1, v6);
        spinlock_release(&tlb_fault_lock);
        // 0x8001e768
        return 0;
    }
    int32_t result; // 0x8001e3d0
    if (v5 >= 3) {
        // 0x8001e628
        spinlock_release(&tlb_fault_lock);
        int32_t v7 = as_prepare_load(1); // 0x8001e634
        int32_t v8 = v1; // 0x8001e648
        if ((v7 & -0x1000) != v7) {
            // 0x8001e64c
            badassert("(paddr & PAGE_FRAME) == paddr", "../../vm/vm_tlb.c", 261, "vm_fault");
            v8 = (int32_t)"(paddr & PAGE_FRAME) == paddr";
        }
        // 0x8001e66c
        if (swap_in(v8, v7) != 0) {
            // 0x8001e67c
            increase(9);
        }
        // 0x8001e684
        spinlock_acquire(&tlb_fault_lock);
        int32_t v9 = v7; // 0x8001e694
        if (v7 == 0) {
            // 0x8001e698
            badassert("paddr != 0", "../../vm/vm_tlb.c", 270, "vm_fault");
            v9 = (int32_t)"../../vm/vm_tlb.c";
        }
        // 0x8001e6b8
        result = -1;
        if (ipt_add(*(int32_t *)(*v3 + 28), v9, v1) == 0) {
            // 0x8001e6d4
            update_tlb(v1, v7);
            spinlock_release(&tlb_fault_lock);
            result = 0;
        }
        // 0x8001e768
        return result;
    }
    int32_t * v10 = v5 == 1 ? v4 : (int32_t *)((int32_t)v4 + 8);
    spinlock_release(&tlb_fault_lock);
    int32_t v11 = as_prepare_load(1); // 0x8001e534
    if (v11 == 0) {
        // 0x8001e540
        badassert("paddr != 0", "../../vm/vm_tlb.c", 212, "vm_fault");
    }
    int32_t v12 = 1; // 0x8001e56c
    if ((v11 & -0x1000) != v11) {
        // 0x8001e570
        badassert("(paddr & PAGE_FRAME) == paddr", "../../vm/vm_tlb.c", 215, "vm_fault");
        v12 = &g41;
    }
    // 0x8001e590
    if (v5 == v12) {
        // 0x8001e5c8
        as_complete_load((int32_t *)*(int32_t *)(*v3 + 16));
        goto lab_0x8001e5c8_2;
    } else {
        int32_t v13 = swap_in(v1, v11); // 0x8001e59c
        as_complete_load((int32_t *)*(int32_t *)(*v3 + 16));
        if (v13 == 0) {
            goto lab_0x8001e5e4;
        } else {
            goto lab_0x8001e5c8_2;
        }
    }
  lab_0x8001e5c8_2:
    // 0x8001e5c8
    if (load_page(v1 - *v10, v1, v5, v11) != 0) {
        // 0x8001e768
        return -1;
    }
    goto lab_0x8001e5e4;
  lab_0x8001e5e4:
    // 0x8001e5e4
    spinlock_acquire(&tlb_fault_lock);
    result = -1;
    if (ipt_add(*(int32_t *)(*v3 + 28), v11, v1) == 0) {
        // 0x8001e60c
        update_tlb(v1, v11);
        spinlock_release(&tlb_fault_lock);
        result = 0;
    }
    // 0x8001e768
    return result;
}