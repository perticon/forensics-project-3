void spinlock_acquire(int32_t * splk) {
    int32_t v1 = (int32_t)splk;
    splraise(0, 1);
    int32_t v2 = 0; // 0x800151d8
    int32_t v3; // 0x800151b4
    if (v3 != 0) {
        // 0x800151dc
        v2 = *(int32_t *)*(int32_t *)(v3 + 80);
        if (*(int32_t *)(v1 + 4) == v2) {
            // 0x800151f8
            panic("Deadlock on spinlock %p\n", splk);
        }
        int32_t * v4 = (int32_t *)(v2 + 48); // 0x80015204
        *v4 = *v4 + 1;
    }
    // 0x80015214
    while (true) {
        // 0x80015214
        if (spinlock_data_get(splk) == 0) {
            // 0x80015224
            if (spinlock_data_testandset(splk) == 0) {
                // break -> 0x80015234
                break;
            }
        }
    }
    // 0x80015234
    membar_store_any();
    *(int32_t *)(v1 + 4) = v2;
}