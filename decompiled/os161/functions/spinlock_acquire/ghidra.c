void spinlock_acquire(spinlock *splk)

{
  spinlock_data_t sVar1;
  cpu *pcVar2;
  int unaff_s7;
  
  splraise(0,1);
  pcVar2 = (cpu *)0x0;
  if (unaff_s7 != 0) {
    pcVar2 = **(cpu ***)(unaff_s7 + 0x50);
    if (splk->splk_holder == pcVar2) {
                    /* WARNING: Subroutine does not return */
      panic("Deadlock on spinlock %p\n",splk);
    }
    pcVar2->c_spinlocks = pcVar2->c_spinlocks + 1;
  }
  do {
    do {
      sVar1 = spinlock_data_get(&splk->splk_lock);
    } while (sVar1 != 0);
    sVar1 = spinlock_data_testandset(&splk->splk_lock);
  } while (sVar1 != 0);
  membar_store_any();
  splk->splk_holder = pcVar2;
  return;
}