spinlock_acquire(struct spinlock *splk)
{
	struct cpu *mycpu;

	splraise(IPL_NONE, IPL_HIGH);

	/* this must work before curcpu initialization */
	if (CURCPU_EXISTS()) {
		mycpu = curcpu->c_self;
		if (splk->splk_holder == mycpu) {
			panic("Deadlock on spinlock %p\n", splk);
		}
		mycpu->c_spinlocks++;

		HANGMAN_WAIT(&curcpu->c_hangman, &splk->splk_hangman);
	}
	else {
		mycpu = NULL;
	}

	while (1) {
		/*
		 * Do test-test-and-set, that is, read first before
		 * doing test-and-set, to reduce bus contention.
		 *
		 * Test-and-set is a machine-level atomic operation
		 * that writes 1 into the lock word and returns the
		 * previous value. If that value was 0, the lock was
		 * previously unheld and we now own it. If it was 1,
		 * we don't.
		 */
		if (spinlock_data_get(&splk->splk_lock) != 0) {
			continue;
		}
		if (spinlock_data_testandset(&splk->splk_lock) != 0) {
			continue;
		}
		break;
	}

	membar_store_any();
	splk->splk_holder = mycpu;

	if (CURCPU_EXISTS()) {
		HANGMAN_ACQUIRE(&curcpu->c_hangman, &splk->splk_hangman);
	}
}