attach_con_to_lscreen(int consno, struct lscreen_softc *ls)
{
	struct con_softc *cs = kmalloc(sizeof(struct con_softc));
	if (cs==NULL) {
		return NULL;
	}

	cs->cs_devdata = ls;
	cs->cs_send = lscreen_write;
	cs->cs_sendpolled = lscreen_write;

	ls->ls_devdata = cs;
	ls->ls_start = con_start;
	ls->ls_input = con_input;

	return cs;
}