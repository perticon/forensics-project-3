void threadlisttest_f(void)

{
  thread *ptVar1;
  int iVar2;
  thread **pptVar3;
  int iVar4;
  char *pcVar5;
  int iVar6;
  uint uVar7;
  threadlist tl;
  
  threadlist_init(&tl);
  for (uVar7 = 0; uVar7 < 7; uVar7 = uVar7 + 1) {
    threadlist_addtail(&tl,fakethreads[uVar7]);
  }
  if (tl.tl_count != 7) {
    badassert("tl.tl_count == NUMNAMES","../../test/threadlisttest.c",0x121,"threadlisttest_f");
  }
  iVar2 = 0;
  pcVar5 = (char *)fakethreads;
  for (ptVar1 = (tl.tl_head.tln_next)->tln_self; ptVar1 != (thread *)0x0;
      ptVar1 = ((ptVar1->t_listnode).tln_next)->tln_self) {
    pptVar3 = (thread **)((int)pcVar5 + iVar2 * 4);
    iVar2 = iVar2 + 1;
    if (*pptVar3 != ptVar1) {
      pcVar5 = "../../test/threadlisttest.c";
      badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x125,"threadlisttest_f");
    }
  }
  if (iVar2 != 7) {
    badassert("i == NUMNAMES","../../test/threadlisttest.c",0x128,"threadlisttest_f");
  }
  iVar2 = 0;
  iVar6 = 6;
  pcVar5 = (char *)fakethreads;
  for (ptVar1 = (tl.tl_tail.tln_prev)->tln_self; iVar4 = iVar6 - iVar2, ptVar1 != (thread *)0x0;
      ptVar1 = ((ptVar1->t_listnode).tln_prev)->tln_self) {
    iVar2 = iVar2 + 1;
    if (*(thread **)((int)pcVar5 + iVar4 * 4) != ptVar1) {
      pcVar5 = "../../test/threadlisttest.c";
      iVar6 = 300;
      badassert("t == fakethreads[NUMNAMES - i - 1]","../../test/threadlisttest.c",300,
                "threadlisttest_f");
    }
  }
  uVar7 = 0;
  if (iVar2 == 7) {
    for (; uVar7 < 7; uVar7 = uVar7 + 1) {
LAB_800137c0:
      ptVar1 = threadlist_remhead(&tl);
      if (fakethreads[uVar7] != ptVar1) {
        badassert("t == fakethreads[i]","../../test/threadlisttest.c",0x133,"threadlisttest_f");
      }
    }
    if (tl.tl_count != 0) {
      badassert("tl.tl_count == 0","../../test/threadlisttest.c",0x135,"threadlisttest_f");
    }
    return;
  }
  badassert("i == NUMNAMES","../../test/threadlisttest.c",0x12f,"threadlisttest_f");
  goto LAB_800137c0;
}