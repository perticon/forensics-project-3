threadlisttest_f(void)
{
	struct threadlist tl;
	struct thread *t;
	unsigned i;

	threadlist_init(&tl);

	for (i=0; i<NUMNAMES; i++) {
		threadlist_addtail(&tl, fakethreads[i]);
	}
	KASSERT(tl.tl_count == NUMNAMES);

	i=0;
	THREADLIST_FORALL(t, tl) {
		KASSERT(t == fakethreads[i]);
		i++;
	}
	KASSERT(i == NUMNAMES);

	i=0;
	THREADLIST_FORALL_REV(t, tl) {
		KASSERT(t == fakethreads[NUMNAMES - i - 1]);
		i++;
	}
	KASSERT(i == NUMNAMES);

	for (i=0; i<NUMNAMES; i++) {
		t = threadlist_remhead(&tl);
		KASSERT(t == fakethreads[i]);
	}
	KASSERT(tl.tl_count == 0);
}