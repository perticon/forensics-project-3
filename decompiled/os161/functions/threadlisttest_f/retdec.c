void threadlisttest_f(void) {
    // 0x8001361c
    int32_t tl; // bp-48, 0x8001361c
    threadlist_init(&tl);
    for (int32_t i = 0; i < 7; i++) {
        int32_t v1 = *(int32_t *)(4 * i + (int32_t)&fakethreads); // 0x80013648
        threadlist_addtail(&tl, (int32_t *)v1);
    }
    int32_t v2; // 0x8001361c
    if (v2 != 7) {
        // 0x80013678
        badassert("tl.tl_count == NUMNAMES", "../../test/threadlisttest.c", 289, "threadlisttest_f");
    }
    int32_t v3 = *(int32_t *)(v2 + 8);
    int32_t v4 = 0; // 0x800136f8
    if (v3 == 0) {
        // 0x80013708
        badassert("i == NUMNAMES", "../../test/threadlisttest.c", 296, "threadlisttest_f");
        goto lab_0x80013724;
    } else {
        int32_t v5 = (int32_t)&fakethreads;
        int32_t v6 = v3; // 0x800136c0
        int32_t v7 = v5; // 0x800136c0
        if (*(int32_t *)(4 * v4 + v5) != v3) {
            // 0x800136c4
            badassert("t == fakethreads[i]", "../../test/threadlisttest.c", 293, "threadlisttest_f");
            v6 = &g41;
            v7 = (int32_t)"../../test/threadlisttest.c";
        }
        // 0x800136e4
        v4++;
        int32_t v8 = *(int32_t *)(*(int32_t *)(v6 + 64) + 8);
        while (v8 != 0) {
            // 0x800136b0
            v5 = v7;
            v6 = v8;
            v7 = v5;
            if (*(int32_t *)(4 * v4 + v5) != v8) {
                // 0x800136c4
                badassert("t == fakethreads[i]", "../../test/threadlisttest.c", 293, "threadlisttest_f");
                v6 = &g41;
                v7 = (int32_t)"../../test/threadlisttest.c";
            }
            // 0x800136e4
            v4++;
            v8 = *(int32_t *)(*(int32_t *)(v6 + 64) + 8);
        }
        if (v4 == 7) {
            goto lab_0x80013724;
        } else {
            // 0x80013708
            badassert("i == NUMNAMES", "../../test/threadlisttest.c", 296, "threadlisttest_f");
            goto lab_0x80013724;
        }
    }
  lab_0x80013724:;
    int32_t v9 = *(int32_t *)(v2 + 8);
    int32_t v10 = 6; // 0x80013790
    int32_t v11 = (int32_t)&fakethreads; // 0x80013790
    int32_t v12 = 0; // 0x80013790
    int32_t v13; // 0x8001361c
    int32_t v14; // 0x8001361c
    if (v9 == 0) {
        // 0x800137a0
        badassert("i == NUMNAMES", "../../test/threadlisttest.c", 303, "threadlisttest_f");
        v13 = 0;
        goto lab_0x800137c0;
    } else {
        int32_t v15 = v9; // 0x80013758
        int32_t v16 = v11; // 0x80013758
        int32_t v17 = v10; // 0x80013758
        if (*(int32_t *)(4 * (v10 - v12) + v11) != v9) {
            // 0x8001375c
            badassert("t == fakethreads[NUMNAMES - i - 1]", "../../test/threadlisttest.c", 300, "threadlisttest_f");
            v15 = &g41;
            v16 = (int32_t)"../../test/threadlisttest.c";
            v17 = 300;
        }
        int32_t v18 = v12 + 1; // 0x80013758
        int32_t v19 = *(int32_t *)(*(int32_t *)(v15 + 60) + 8);
        int32_t v20 = v19; // 0x80013790
        v10 = v17;
        v11 = v16;
        v12 = v18;
        while (v19 != 0) {
            // 0x80013744
            v15 = v20;
            v16 = v11;
            v17 = v10;
            if (*(int32_t *)(4 * (v10 - v12) + v11) != v20) {
                // 0x8001375c
                badassert("t == fakethreads[NUMNAMES - i - 1]", "../../test/threadlisttest.c", 300, "threadlisttest_f");
                v15 = &g41;
                v16 = (int32_t)"../../test/threadlisttest.c";
                v17 = 300;
            }
            // 0x8001377c
            v18 = v12 + 1;
            v19 = *(int32_t *)(*(int32_t *)(v15 + 60) + 8);
            v20 = v19;
            v10 = v17;
            v11 = v16;
            v12 = v18;
        }
        // 0x80013794
        v14 = 0;
        if (v18 == 7) {
            goto lab_0x8001380c;
        } else {
            // 0x800137a0
            badassert("i == NUMNAMES", "../../test/threadlisttest.c", 303, "threadlisttest_f");
            v13 = 0;
            goto lab_0x800137c0;
        }
    }
  lab_0x800137c0:;
    int32_t * v21 = threadlist_remhead(&tl); // 0x800137c4
    int32_t v22 = *(int32_t *)(4 * v13 + (int32_t)&fakethreads); // 0x800137d0
    if (v22 != (int32_t)v21) {
        // 0x800137e0
        badassert("t == fakethreads[i]", "../../test/threadlisttest.c", 307, "threadlisttest_f");
    }
    // 0x800137fc
    v14 = v13 + 1;
    goto lab_0x8001380c;
  lab_0x8001380c:
    // 0x8001380c
    v13 = v14;
    if (v14 >= 7) {
        if (v2 != 0) {
            // 0x80013828
            badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 309, "threadlisttest_f");
        }
        // 0x80013844
        return;
    }
    goto lab_0x800137c0;
}