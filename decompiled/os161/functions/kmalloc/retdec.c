char * kmalloc(uint32_t sz) {
    // 0x8001ca5c
    if (sz < 2048) {
        // 0x8001cab8
        return (char *)(int32_t)subpage_kmalloc((int32_t)sz);
    }
    int32_t v1 = alloc_kpages((sz + 4095) / 0x1000); // 0x8001ca74
    if (v1 == 0) {
        // 0x8001cab8
        return (char *)0;
    }
    // 0x8001ca80
    if (v1 % 0x1000 == 0) {
        // 0x8001cab8
        return (char *)v1;
    }
    // 0x8001ca88
    badassert("address % PAGE_SIZE == 0", "../../vm/kmalloc.c", 1196, "kmalloc");
    // 0x8001cab8
    return (char *)(int32_t)subpage_kmalloc((int32_t)"address % PAGE_SIZE == 0");
}