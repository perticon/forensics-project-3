void * kmalloc(size_t sz)

{
  void *pvVar1;
  char *pcVar2;
  
  if (0x7ff < sz) {
    pvVar1 = (void *)alloc_kpages(sz + 0xfff >> 0xc);
    if (pvVar1 == (void *)0x0) {
      return (void *)0x0;
    }
    if (((uint)pvVar1 & 0xfff) == 0) {
      return pvVar1;
    }
    pcVar2 = "address % PAGE_SIZE == 0";
    badassert("address % PAGE_SIZE == 0","../../vm/kmalloc.c",0x4ac,"kmalloc");
    sz = (size_t)pcVar2;
  }
  pvVar1 = subpage_kmalloc(sz);
  return pvVar1;
}