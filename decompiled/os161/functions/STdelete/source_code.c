void STdelete(ST st, pid_t pid, vaddr_t addr)
{
    Key k;
    k.kpid = pid;
    k.kaddr = addr;
    int i = hashU(k, st->M);
    st->heads[i] = deleteR(st->heads[i], k);
            return;
}