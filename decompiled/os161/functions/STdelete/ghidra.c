void STdelete(ST st,pid_t pid,vaddr_t addr)

{
  int iVar1;
  link pSVar2;
  link *ppSVar3;
  Key k;
  
  iVar1 = hashU((Key)CONCAT44(pid,addr),pid);
  ppSVar3 = st->heads;
  pSVar2 = deleteR(ppSVar3[iVar1],(Key)CONCAT44(pid,addr));
  ppSVar3[iVar1] = pSVar2;
  return;
}