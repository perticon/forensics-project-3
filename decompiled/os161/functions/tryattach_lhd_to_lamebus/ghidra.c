int tryattach_lhd_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lhd_softc *lh;
  int errcode;
  char *pcVar1;
  
  lh = attach_lhd_to_lamebus(devunit,bus);
  if (lh == (lhd_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lhd%d at lamebus%d",devunit,busunit);
    errcode = config_lhd(lh,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lhd = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}