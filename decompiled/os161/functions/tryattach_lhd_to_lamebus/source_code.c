tryattach_lhd_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct lhd_softc *dev;
	int result;

	dev = attach_lhd_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("lhd%d at lamebus%d", devunit, busunit);
	result = config_lhd(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_lhd = devunit+1;
	autoconf_lhd(dev, devunit);
	return 0;
}