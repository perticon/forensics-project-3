int32_t tryattach_lhd_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_lhd_to_lamebus(devunit, bus); // 0x80001740
    if (v1 == NULL) {
        // 0x800017b8
        return -1;
    }
    // 0x8000174c
    kprintf("lhd%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_lhd(v1, devunit); // 0x80001768
    int32_t result; // 0x80001724
    if (v2 == 0) {
        // 0x80001794
        kprintf("\n");
        nextunit_lhd = devunit + 1;
        result = 0;
    } else {
        // 0x80001774
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x800017b8
    return result;
}