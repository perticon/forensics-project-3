int32_t tryattach_lrandom_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_lrandom_to_lamebus(devunit, bus); // 0x80001688
    if (v1 == NULL) {
        // 0x8000170c
        return -1;
    }
    // 0x80001694
    kprintf("lrandom%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_lrandom(v1, devunit); // 0x800016b0
    int32_t result; // 0x8000166c
    if (v2 == 0) {
        // 0x800016dc
        kprintf("\n");
        nextunit_lrandom = devunit + 1;
        autoconf_lrandom(v1, devunit);
        result = 0;
    } else {
        // 0x800016bc
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x8000170c
    return result;
}