int tryattach_lrandom_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  lrandom_softc *lr;
  int errcode;
  char *pcVar1;
  
  lr = attach_lrandom_to_lamebus(devunit,bus);
  if (lr == (lrandom_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("lrandom%d at lamebus%d",devunit,busunit);
    errcode = config_lrandom(lr,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_lrandom = devunit + 1;
      autoconf_lrandom(lr,devunit);
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}