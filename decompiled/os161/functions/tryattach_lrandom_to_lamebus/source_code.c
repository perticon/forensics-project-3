tryattach_lrandom_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct lrandom_softc *dev;
	int result;

	dev = attach_lrandom_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("lrandom%d at lamebus%d", devunit, busunit);
	result = config_lrandom(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_lrandom = devunit+1;
	autoconf_lrandom(dev, devunit);
	return 0;
}