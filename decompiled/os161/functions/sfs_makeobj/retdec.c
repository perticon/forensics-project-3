int32_t sfs_makeobj(int32_t * sfs, int32_t type, int32_t ** ret) {
    // 0x8000892c
    int32_t ino; // bp-24, 0x8000892c
    int32_t result = sfs_balloc(sfs, &ino); // 0x80008950
    if (result != 0) {
        // 0x80008984
        return result;
    }
    // 0x8000895c
    sfs_loadvnode(sfs, ino, type, ret);
    int32_t result2 = 0; // 0x80008970
    if (result2 != 0) {
        // 0x80008974
        sfs_bfree(sfs, ino);
    }
    // 0x80008984
    return result2;
}