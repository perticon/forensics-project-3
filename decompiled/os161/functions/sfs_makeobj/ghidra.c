int sfs_makeobj(sfs_fs *sfs,int type,sfs_vnode **ret)

{
  int iVar1;
  uint32_t ino;
  
  iVar1 = sfs_balloc(sfs,&ino);
  if ((iVar1 == 0) && (iVar1 = sfs_loadvnode(sfs,ino,type,ret), iVar1 != 0)) {
    sfs_bfree(sfs,ino);
  }
  return iVar1;
}