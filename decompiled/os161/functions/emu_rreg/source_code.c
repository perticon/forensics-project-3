emu_rreg(struct emu_softc *sc, uint32_t reg)
{
	return bus_read_register(sc->e_busdata, sc->e_buspos, reg);
}