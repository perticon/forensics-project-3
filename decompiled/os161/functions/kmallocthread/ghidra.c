void kmallocthread(void *sm,ulong num)

{
  void *pvVar1;
  int iVar2;
  void *ptr;
  void *ptr_00;
  
  iVar2 = 0;
  ptr = (void *)0x0;
  ptr_00 = (void *)0x0;
  do {
    if (0x4af < iVar2) {
done:
      if (ptr != (void *)0x0) {
        kfree(ptr);
      }
      if (ptr_00 != (void *)0x0) {
        kfree(ptr_00);
      }
      if (sm != (void *)0x0) {
        V((semaphore *)sm);
      }
      return;
    }
    pvVar1 = kmalloc(0x3e5);
    if (pvVar1 == (void *)0x0) {
      if (sm == (void *)0x0) {
        kprintf("kmalloc returned null; test failed.\n");
      }
      else {
        kprintf("thread %lu: kmalloc returned NULL\n",num);
      }
      goto done;
    }
    if (ptr != (void *)0x0) {
      kfree(ptr);
    }
    iVar2 = iVar2 + 1;
    ptr = ptr_00;
    ptr_00 = pvVar1;
  } while( true );
}