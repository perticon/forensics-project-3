void kmallocthread(char * sm, int32_t num) {
    int32_t v1 = 0; // 0x80010c8c
    int32_t v2 = 0;
    int32_t v3 = 0;
    char * v4 = kmalloc(997); // 0x80010c3c
    int32_t v5; // 0x80010c00
    int32_t v6; // 0x80010c00
    while (v4 != NULL) {
        // 0x80010c7c
        if (v2 != 0) {
            // 0x80010c84
            kfree((char *)v2);
        }
        int32_t v7 = (int32_t)v4; // 0x80010c3c
        v1++;
        v5 = v3;
        v6 = v7;
        if (v1 >= 1200) {
            goto lab_0x80010ca4;
        }
        v2 = v3;
        v3 = v7;
        v4 = kmalloc(997);
    }
    if (sm == NULL) {
        // 0x80010c68
        kprintf("kmalloc returned null; test failed.\n");
        v5 = v2;
        v6 = v3;
    } else {
        // 0x80010c50
        kprintf("thread %lu: kmalloc returned NULL\n", num);
        v5 = v2;
        v6 = v3;
    }
  lab_0x80010ca4:
    // 0x80010ca4
    if (v5 != 0) {
        // 0x80010cac
        kfree((char *)v5);
    }
    if (v6 != 0) {
        // 0x80010cbc
        kfree((char *)v6);
    }
    if (sm != NULL) {
        // 0x80010ccc
        V((int32_t *)sm);
    }
}