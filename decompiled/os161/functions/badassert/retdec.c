void badassert(char * expr, char * file, int32_t line, char * func) {
    // 0x8000b568
    panic("Assertion failed: %s, at %s:%d (%s)\n", expr, file, line, func);
}