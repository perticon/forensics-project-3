int32_t semu5(int32_t nargs, char ** args) {
    int32_t * v1 = sem_create("some-silly-name", -0x10000000); // 0x80012100
    int32_t v2 = (int32_t)v1; // 0x80012108
    if (v1 == NULL) {
        // 0x8001210c
        panic("semu5: sem_create failed\n");
        v2 = &g41;
    }
    // 0x80012118
    if (*(int32_t *)(v2 + 16) != -0x10000000) {
        // 0x80012128
        badassert("sem->sem_count == 0xf0000000U", "../../test/semunit.c", 248, "semu5");
    }
    // 0x80012144
    ok();
    sem_destroy(v1);
    return 0;
}