semu5(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	sem = sem_create(NAMESTRING, 0xf0000000U);
	if (sem == NULL) {
		/* This might not be an innocuous malloc shortage. */
		panic("semu5: sem_create failed\n");
	}
	KASSERT(sem->sem_count == 0xf0000000U);

	/* Clean up. */
	ok();
	sem_destroy(sem);
	return 0;
}