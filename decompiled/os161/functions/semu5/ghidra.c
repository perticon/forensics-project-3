int semu5(int nargs,char **args)

{
  semaphore *sem;
  
  sem = sem_create("some-silly-name",0xf0000000);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semu5: sem_create failed\n");
  }
  if (sem->sem_count != 0xf0000000) {
    badassert("sem->sem_count == 0xf0000000U","../../test/semunit.c",0xf8,"semu5");
  }
  ok();
  sem_destroy(sem);
  return 0;
}