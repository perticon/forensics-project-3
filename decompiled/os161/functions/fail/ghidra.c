void fail(ulong num,char *msg)

{
  kprintf("thread %lu: Mismatch on %s\n",num,msg);
  kprintf("Test failed\n");
  lock_release(testlock);
  V(donesem);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}