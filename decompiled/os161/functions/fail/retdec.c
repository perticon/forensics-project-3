void fail(int32_t num, char * msg) {
    // 0x80012af0
    kprintf("thread %lu: Mismatch on %s\n", num, msg);
    kprintf("Test failed\n");
    lock_release(testlock);
    V((int32_t *)g16);
    thread_exit();
}