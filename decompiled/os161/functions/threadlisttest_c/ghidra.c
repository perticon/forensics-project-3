void threadlisttest_c(void)

{
  thread *ptVar1;
  char *t;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addhead(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xaa,"threadlisttest_c");
  }
  check_order(&tl,true);
  ptVar1 = threadlist_remhead(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xaf,"threadlisttest_c");
  }
  ptVar1 = threadlist_remhead(&tl);
  t = (char *)fakethreads[0];
  if (fakethreads[0] != ptVar1) {
    t = "../../test/threadlisttest.c";
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xb1,"threadlisttest_c");
  }
  if (tl.tl_count != 0) {
    t = "../../test/threadlisttest.c";
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xb2,"threadlisttest_c");
  }
  threadlist_addtail(&tl,(thread *)t);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xb6,"threadlisttest_c");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xbb,"threadlisttest_c");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xbd,"threadlisttest_c");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xbe,"threadlisttest_c");
  }
  threadlist_cleanup(&tl);
  return;
}