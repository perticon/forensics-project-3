threadlisttest_c(void)
{
	struct threadlist tl;
	struct thread *t;

	threadlist_init(&tl);

	threadlist_addhead(&tl, fakethreads[0]);
	threadlist_addhead(&tl, fakethreads[1]);
	KASSERT(tl.tl_count == 2);

	check_order(&tl, true);

	t = threadlist_remhead(&tl);
	KASSERT(t == fakethreads[1]);
	t = threadlist_remhead(&tl);
	KASSERT(t == fakethreads[0]);
	KASSERT(tl.tl_count == 0);

	threadlist_addtail(&tl, fakethreads[0]);
	threadlist_addtail(&tl, fakethreads[1]);
	KASSERT(tl.tl_count == 2);

	check_order(&tl, false);

	t = threadlist_remtail(&tl);
	KASSERT(t == fakethreads[1]);
	t = threadlist_remtail(&tl);
	KASSERT(t == fakethreads[0]);
	KASSERT(tl.tl_count == 0);

	threadlist_cleanup(&tl);
}