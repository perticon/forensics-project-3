void threadlisttest_c(void) {
    // 0x80013b14
    int32_t tl; // bp-40, 0x80013b14
    threadlist_init(&tl);
    threadlist_addhead(&tl, (int32_t *)*(int32_t *)&fakethreads);
    threadlist_addhead(&tl, (int32_t *)g30);
    int32_t v1; // 0x80013b14
    if (v1 != 2) {
        // 0x80013b5c
        badassert("tl.tl_count == 2", "../../test/threadlisttest.c", 170, "threadlisttest_c");
    }
    // 0x80013b78
    check_order(&tl, true);
    int32_t * v2 = threadlist_remhead(&tl); // 0x80013b88
    if (g30 != (int32_t)v2) {
        // 0x80013ba0
        badassert("t == fakethreads[1]", "../../test/threadlisttest.c", 175, "threadlisttest_c");
    }
    int32_t v3 = (int32_t)threadlist_remhead(&tl); // 0x80013bc0
    int32_t v4 = v3; // 0x80013bd4
    if (*(int32_t *)&fakethreads != v3) {
        // 0x80013bd8
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 177, "threadlisttest_c");
        v4 = (int32_t)"../../test/threadlisttest.c";
    }
    int32_t v5 = v4; // 0x80013c00
    if (v1 != 0) {
        // 0x80013c04
        badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 178, "threadlisttest_c");
        v5 = (int32_t)"../../test/threadlisttest.c";
    }
    // 0x80013c20
    threadlist_addtail(&tl, (int32_t *)v5);
    threadlist_addtail(&tl, (int32_t *)g30);
    if (v1 != 2) {
        // 0x80013c4c
        badassert("tl.tl_count == 2", "../../test/threadlisttest.c", 182, "threadlisttest_c");
    }
    // 0x80013c68
    check_order(&tl, false);
    int32_t * v6 = threadlist_remtail(&tl); // 0x80013c78
    if (g30 != (int32_t)v6) {
        // 0x80013c90
        badassert("t == fakethreads[1]", "../../test/threadlisttest.c", 187, "threadlisttest_c");
    }
    int32_t * v7 = threadlist_remtail(&tl); // 0x80013cb0
    if (*(int32_t *)&fakethreads != (int32_t)v7) {
        // 0x80013cc8
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 189, "threadlisttest_c");
    }
    if (v1 != 0) {
        // 0x80013cf4
        badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 190, "threadlisttest_c");
    }
    // 0x80013d10
    threadlist_cleanup(&tl);
}