void exorcise(void) {
    // 0x800160d0
    int32_t v1; // 0x800160d0
    int32_t * v2 = (int32_t *)(v1 + 80); // 0x8001613c
    int32_t * v3 = threadlist_remhead((int32_t *)(*v2 + 16)); // 0x80016144
    if (v3 == NULL) {
        // 0x80016150
        return;
    }
    int32_t v4 = (int32_t)v3;
    int32_t v5 = v4; // 0x800160e8
    if (v1 == v4) {
        // 0x800160ec
        badassert("z != curthread", "../../thread/thread.c", 303, "exorcise");
        v5 = &g41;
    }
    int32_t v6 = v5; // 0x80016114
    if (*(int32_t *)(v5 + 8) != 3) {
        // 0x80016118
        badassert("z->t_state == S_ZOMBIE", "../../thread/thread.c", 304, "exorcise");
        v6 = &g41;
    }
    // 0x80016134
    thread_destroy((int32_t *)v6);
    int32_t * v7 = threadlist_remhead((int32_t *)(*v2 + 16)); // 0x80016144
    while (v7 != NULL) {
        // 0x800160e4
        v4 = (int32_t)v7;
        v5 = v4;
        if (v1 == v4) {
            // 0x800160ec
            badassert("z != curthread", "../../thread/thread.c", 303, "exorcise");
            v5 = &g41;
        }
        // 0x80016108
        v6 = v5;
        if (*(int32_t *)(v5 + 8) != 3) {
            // 0x80016118
            badassert("z->t_state == S_ZOMBIE", "../../thread/thread.c", 304, "exorcise");
            v6 = &g41;
        }
        // 0x80016134
        thread_destroy((int32_t *)v6);
        v7 = threadlist_remhead((int32_t *)(*v2 + 16));
    }
}