void exorcise(void)

{
  thread *thread;
  thread *unaff_s7;
  
  while (thread = threadlist_remhead(&unaff_s7->t_cpu->c_zombies), thread != (thread *)0x0) {
    if (thread == unaff_s7) {
      badassert("z != curthread","../../thread/thread.c",0x12f,"exorcise");
    }
    if (thread->t_state != S_ZOMBIE) {
      badassert("z->t_state == S_ZOMBIE","../../thread/thread.c",0x130,"exorcise");
    }
    thread_destroy(thread);
  }
  return;
}