int semfs_getdirentry(vnode *dirvn,uio *uio)

{
  int *piVar1;
  size_t n;
  int iVar2;
  int iVar3;
  uint uVar4;
  char *str;
  
  iVar3 = *(int *)((int)dirvn->vn_data + 0x18);
  uVar4 = *(uint *)((int)&uio->uio_offset + 4);
  if (*(int *)&uio->uio_offset < 0) {
    badassert("uio->uio_offset >= 0","../../fs/semfs/semfs_vnops.c",0x137,"semfs_getdirentry");
  }
  lock_acquire(*(lock **)(iVar3 + 0x14));
  piVar1 = *(int **)(iVar3 + 0x18);
  if ((uint)piVar1[1] <= uVar4) {
    iVar2 = 0;
  }
  else {
    if ((uint)piVar1[1] <= uVar4) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    str = **(char ***)(*piVar1 + uVar4 * 4);
    n = strlen(str);
    iVar2 = uiomove(str,n,uio);
  }
  lock_release(*(lock **)(iVar3 + 0x14));
  return iVar2;
}