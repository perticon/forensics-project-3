semfs_getdirentry(struct vnode *dirvn, struct uio *uio)
{
	struct semfs_vnode *dirsemv = dirvn->vn_data;
	struct semfs *semfs = dirsemv->semv_semfs;
	struct semfs_direntry *dent;
	unsigned num, pos;
	int result;

	KASSERT(uio->uio_offset >= 0);
	pos = uio->uio_offset;

	lock_acquire(semfs->semfs_dirlock);

	num = semfs_direntryarray_num(semfs->semfs_dents);
	if (pos >= num) {
		/* EOF */
		result = 0;
	}
	else {
		dent = semfs_direntryarray_get(semfs->semfs_dents, pos);
		result = uiomove(dent->semd_name, strlen(dent->semd_name),
				 uio);
	}

	lock_release(semfs->semfs_dirlock);
	return result;
}