int32_t semfs_getdirentry(int32_t * dirvn, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)(*(int32_t *)((int32_t)dirvn + 16) + 24); // 0x800069a8
    uint32_t v3 = *(int32_t *)(v1 + 12); // 0x800069b0
    if (*(int32_t *)(v1 + 8) <= 0xffffffff) {
        // 0x800069bc
        badassert("uio->uio_offset >= 0", "../../fs/semfs/semfs_vnops.c", 311, "semfs_getdirentry");
    }
    int32_t * v4 = (int32_t *)(v2 + 20); // 0x800069dc
    lock_acquire((int32_t *)*v4);
    int32_t v5 = *(int32_t *)(v2 + 24); // 0x800069e8
    int32_t result = 0; // 0x80006a00
    if (v3 < *(int32_t *)(v5 + 4)) {
        int32_t v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v3); // 0x80006a34
        char * v7 = (char *)*(int32_t *)v6; // 0x80006a44
        result = uiomove(v7, strlen(v7), uio);
    }
    // 0x80006a64
    lock_release((int32_t *)*v4);
    return result;
}