int emufs_loadvnode(emufs_fs *ef,uint32_t handle,int isdir,emufs_vnode **ret)

{
  uint uVar1;
  emufs_vnode *peVar2;
  int iVar3;
  uint uVar4;
  char *pcVar5;
  vnode_ops *ops;
  vnodearray *pvVar6;
  array *a;
  
  vfs_biglock_acquire();
  lock_acquire(ef->ef_emu->e_lock);
  pvVar6 = ef->ef_vnodes;
  uVar4 = (pvVar6->arr).num;
  uVar1 = 0;
  while (uVar1 < uVar4) {
    pcVar5 = (char *)(uVar1 << 2);
    if (uVar4 <= uVar1) {
      pcVar5 = "index < a->num";
      pvVar6 = (vnodearray *)&DAT_00000064;
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    peVar2 = *(emufs_vnode **)(*(int *)(pcVar5 + (int)(pvVar6->arr).v) + 0x10);
    uVar1 = uVar1 + 1;
    if (peVar2->ev_handle == handle) {
      vnode_incref(&peVar2->ev_v);
      lock_release(ef->ef_emu->e_lock);
      vfs_biglock_release();
      *ret = peVar2;
      return 0;
    }
  }
  peVar2 = (emufs_vnode *)kmalloc(0x20);
  if (peVar2 == (emufs_vnode *)0x0) {
    lock_release(ef->ef_emu->e_lock);
    return 3;
  }
  peVar2->ev_emu = ef->ef_emu;
  peVar2->ev_handle = handle;
  if (isdir == 0) {
    ops = &emufs_fileops;
  }
  else {
    ops = &emufs_dirops;
  }
  iVar3 = vnode_init((vnode *)peVar2,ops,&ef->ef_fs,peVar2);
  if (iVar3 == 0) {
    a = (array *)ef->ef_vnodes;
    uVar1 = a->num;
    iVar3 = array_setsize(a,uVar1 + 1);
    if (iVar3 == 0) {
      a->v[uVar1] = peVar2;
      iVar3 = 0;
    }
    if (iVar3 == 0) {
      lock_release(ef->ef_emu->e_lock);
      vfs_biglock_release();
      *ret = peVar2;
      return 0;
    }
    vnode_cleanup((vnode *)peVar2);
    lock_release(ef->ef_emu->e_lock);
    vfs_biglock_release();
    kfree(peVar2);
    return iVar3;
  }
  lock_release(ef->ef_emu->e_lock);
  vfs_biglock_release();
  kfree(peVar2);
  return iVar3;
}