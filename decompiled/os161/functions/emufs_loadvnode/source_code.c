emufs_loadvnode(struct emufs_fs *ef, uint32_t handle, int isdir,
		struct emufs_vnode **ret)
{
	struct vnode *v;
	struct emufs_vnode *ev;
	unsigned i, num;
	int result;

	vfs_biglock_acquire();
	lock_acquire(ef->ef_emu->e_lock);

	num = vnodearray_num(ef->ef_vnodes);
	for (i=0; i<num; i++) {
		v = vnodearray_get(ef->ef_vnodes, i);
		ev = v->vn_data;
		if (ev->ev_handle == handle) {
			/* Found */

			VOP_INCREF(&ev->ev_v);

			lock_release(ef->ef_emu->e_lock);
			vfs_biglock_release();
			*ret = ev;
			return 0;
		}
	}

	/* Didn't have one; create it */

	ev = kmalloc(sizeof(struct emufs_vnode));
	if (ev==NULL) {
		lock_release(ef->ef_emu->e_lock);
		return ENOMEM;
	}

	ev->ev_emu = ef->ef_emu;
	ev->ev_handle = handle;

	result = vnode_init(&ev->ev_v, isdir ? &emufs_dirops : &emufs_fileops,
			    &ef->ef_fs, ev);
	if (result) {
		lock_release(ef->ef_emu->e_lock);
		vfs_biglock_release();
		kfree(ev);
		return result;
	}

	result = vnodearray_add(ef->ef_vnodes, &ev->ev_v, NULL);
	if (result) {
		/* note: vnode_cleanup undoes vnode_init - it does not kfree */
		vnode_cleanup(&ev->ev_v);
		lock_release(ef->ef_emu->e_lock);
		vfs_biglock_release();
		kfree(ev);
		return result;
	}

	lock_release(ef->ef_emu->e_lock);
	vfs_biglock_release();

	*ret = ev;
	return 0;
}