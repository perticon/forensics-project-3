int32_t emufs_loadvnode(int32_t * ef, int32_t handle, int32_t isdir, int32_t ** ret) {
    int32_t v1 = (int32_t)ef;
    vfs_biglock_acquire();
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x8000337c
    lock_acquire((int32_t *)*(int32_t *)(*v2 + 12));
    int32_t * v3 = (int32_t *)(v1 + 16); // 0x80003390
    int32_t v4 = *v3; // 0x80003390
    int32_t v5 = 0; // 0x800033a0
    int32_t result; // 0x80003348
    while (v5 < *(int32_t *)(v4 + 4)) {
        int32_t v6 = *(int32_t *)(*(int32_t *)(*(int32_t *)v4 + 4 * v5) + 16); // 0x800033e0
        v5++;
        if (*(int32_t *)(v6 + 28) == handle) {
            // 0x800033f8
            vnode_incref((int32_t *)v6);
            lock_release((int32_t *)*(int32_t *)(*v2 + 12));
            vfs_biglock_release();
            *(int32_t *)ret = v6;
            result = 0;
            return result;
        }
    }
    char * v7 = kmalloc(32); // 0x80003434
    int32_t v8 = *v2;
    if (v7 == NULL) {
        // 0x80003440
        lock_release((int32_t *)*(int32_t *)(v8 + 12));
        // 0x80003564
        return 3;
    }
    int32_t v9 = (int32_t)v7; // 0x80003434
    *(int32_t *)(v9 + 24) = v8;
    *(int32_t *)(v9 + 28) = handle;
    int32_t v10 = isdir == 0 ? (int32_t)&emufs_fileops : (int32_t)&emufs_dirops;
    int32_t result2 = vnode_init((int32_t *)v7, (int32_t *)v10, ef, v7); // 0x80003490
    if (result2 != 0) {
        // 0x8000349c
        lock_release((int32_t *)*(int32_t *)(*v2 + 12));
        vfs_biglock_release();
        kfree(v7);
        // 0x80003564
        return result2;
    }
    int32_t v11 = *v3; // 0x800034c8
    int32_t v12 = *(int32_t *)(v11 + 4); // 0x800034d0
    int32_t * v13 = (int32_t *)v11; // 0x800034dc
    int32_t v14 = array_setsize(v13, v12 + 1); // 0x800034dc
    if (v14 == 0) {
        // 0x80003540
        *(int32_t *)(*v13 + 4 * v12) = v9;
        lock_release((int32_t *)*(int32_t *)(*v2 + 12));
        vfs_biglock_release();
        *(int32_t *)ret = v9;
        result = 0;
    } else {
        // 0x8000350c
        vnode_cleanup((int32_t *)v7);
        lock_release((int32_t *)*(int32_t *)(*v2 + 12));
        vfs_biglock_release();
        kfree(v7);
        result = v14;
    }
  lab_0x80003564:
    // 0x80003564
    return result;
}