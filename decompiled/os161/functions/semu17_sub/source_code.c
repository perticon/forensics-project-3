semu17_sub(void *semv, unsigned long junk)
{
	struct semaphore *sem = semv;

	(void)junk;

	semu17_thread = curthread;

	/* precondition */
	KASSERT(sem->sem_count == 0);

	P(sem);
}