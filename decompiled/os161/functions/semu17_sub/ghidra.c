void semu17_sub(void *semv,ulong junk)

{
  char *pcVar1;
  thread *unaff_s7;
  
  semu17_thread = unaff_s7;
  if (*(int *)((int)semv + 0x10) != 0) {
    pcVar1 = "sem->sem_count == 0";
    badassert("sem->sem_count == 0","../../test/semunit.c",0x27b,"semu17_sub");
    semv = pcVar1;
  }
  P((semaphore *)semv);
  return;
}