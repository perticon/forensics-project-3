void semu17_sub(char * semv, int32_t junk) {
    int32_t v1 = (int32_t)semv;
    int32_t v2 = v1; // 0x80011650
    if (*(int32_t *)(v1 + 16) != 0) {
        // 0x80011654
        badassert("sem->sem_count == 0", "../../test/semunit.c", 635, "semu17_sub");
        v2 = (int32_t)"sem->sem_count == 0";
    }
    // 0x80011670
    P((int32_t *)v2);
}