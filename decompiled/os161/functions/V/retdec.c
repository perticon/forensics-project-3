void V(int32_t * sem) {
    if (sem == NULL) {
        // 0x80015748
        badassert("sem != NULL", "../../thread/synch.c", 128, "V");
    }
    int32_t v1 = (int32_t)sem;
    spinlock_acquire((int32_t *)"ULL");
    int32_t * v2 = (int32_t *)(v1 + 16); // 0x80015774
    int32_t v3 = *v2 + 1; // 0x8001577c
    *v2 = v3;
    if (v3 == 0) {
        // 0x80015794
        badassert("sem->sem_count > 0", "../../thread/synch.c", 133, "V");
    }
    // 0x800157b0
    wchan_wakeone((int32_t *)*(int32_t *)(v1 + 4), (int32_t *)"ULL");
    spinlock_release((int32_t *)"ULL");
}