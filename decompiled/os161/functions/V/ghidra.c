void V(semaphore *sem)

{
  char *pcVar1;
  spinlock *splk;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x80,"V");
  }
  splk = (spinlock *)((int)pcVar1 + 8);
  spinlock_acquire(splk);
  sem->sem_count = sem->sem_count + 1;
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../thread/synch.c",0x85,"V");
  }
  wchan_wakeone(sem->sem_wchan,splk);
  spinlock_release(splk);
  return;
}