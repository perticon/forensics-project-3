dev_namefile(struct vnode *v, struct uio *uio)
{
	/*
	 * The name of a device is always just "device:". The VFS
	 * layer puts in the device name for us, so we don't need to
	 * do anything further.
	 */

	(void)v;
	(void)uio;

	return 0;
}