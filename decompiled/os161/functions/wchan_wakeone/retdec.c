void wchan_wakeone(int32_t * wc, int32_t * lk) {
    // 0x80016ee8
    if (!spinlock_do_i_hold(lk)) {
        // 0x80016f08
        badassert("spinlock_do_i_hold(lk)", "../../thread/thread.c", 1034, "wchan_wakeone");
    }
    int32_t * v1 = threadlist_remhead((int32_t *)((int32_t)wc + 4)); // 0x80016f28
    if (v1 != NULL) {
        // 0x80016f34
        thread_make_runnable(v1, false);
    }
}