void wchan_wakeone(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  thread *target;
  
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x40a,"wchan_wakeone");
  }
  target = threadlist_remhead(&wc->wc_threads);
  if (target != (thread *)0x0) {
    thread_make_runnable(target,false);
  }
  return;
}