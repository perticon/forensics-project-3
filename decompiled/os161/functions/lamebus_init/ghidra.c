lamebus_softc * lamebus_init(void)

{
  lamebus_softc *splk;
  int iVar1;
  
  splk = (lamebus_softc *)kmalloc(0x110);
  if (splk == (lamebus_softc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_init: Out of memory\n");
  }
  spinlock_init((spinlock *)splk);
  splk->ls_slotsinuse = 0x80000000;
  for (iVar1 = 0; iVar1 < 0x20; iVar1 = iVar1 + 1) {
    splk->ls_devdata[iVar1] = (void *)0x0;
    splk->ls_irqfuncs[iVar1] = (lb_irqfunc *)0x0;
  }
  splk->ls_uniprocessor = 0;
  return splk;
}