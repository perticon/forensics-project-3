int32_t * lamebus_init(void) {
    char * v1 = kmalloc(272); // 0x800044d0
    int32_t v2 = (int32_t)v1; // 0x800044d0
    int32_t v3 = v2; // 0x800044d8
    if (v1 == NULL) {
        // 0x800044dc
        panic("lamebus_init: Out of memory\n");
        v3 = &g41;
    }
    // 0x800044e8
    spinlock_init((int32_t *)v3);
    *(int32_t *)(v2 + 8) = -0x80000000;
    for (int32_t i = 0; i < 32; i++) {
        int32_t v4 = 4 * i + v2;
        *(int32_t *)(v4 + 12) = 0;
        *(int32_t *)(v4 + 140) = 0;
    }
    // 0x8000452c
    *(int32_t *)(v2 + 268) = 0;
    return (int32_t *)v1;
}