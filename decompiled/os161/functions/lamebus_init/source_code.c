lamebus_init(void)
{
	struct lamebus_softc *lamebus;
	int i;

	/* Allocate space for lamebus data */
	lamebus = kmalloc(sizeof(struct lamebus_softc));
	if (lamebus==NULL) {
		panic("lamebus_init: Out of memory\n");
	}

	spinlock_init(&lamebus->ls_lock);

	/*
	 * Initialize the LAMEbus data structure.
	 */
	lamebus->ls_slotsinuse = 1 << LB_CONTROLLER_SLOT;

	for (i=0; i<LB_NSLOTS; i++) {
		lamebus->ls_devdata[i] = NULL;
		lamebus->ls_irqfuncs[i] = NULL;
	}

	lamebus->ls_uniprocessor = 0;

	return lamebus;
}