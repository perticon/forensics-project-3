int32_t copyinstr(int32_t * usersrc, char * dest, int32_t len, int32_t * actual) {
    // 0x8002036c
    int32_t stoplen; // bp-16, 0x8002036c
    int32_t result = copycheck(usersrc, len, &stoplen); // 0x8002038c
    if (result != 0) {
        // 0x800203e4
        return result;
    }
    // 0x80020398
    int32_t v1; // 0x8002036c
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x800203a0
    *v2 = -0x7ffdfdcc;
    int32_t result2; // 0x8002036c
    if (setjmp(v1 + 16) == 0) {
        int32_t v3 = copystr(dest, (char *)usersrc, len, stoplen, actual); // 0x800203dc
        *v2 = 0;
        result2 = v3;
    } else {
        // 0x800203b4
        *v2 = 0;
        result2 = 6;
    }
    // 0x800203e4
    return result2;
}