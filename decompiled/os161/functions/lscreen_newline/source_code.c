lscreen_newline(struct lscreen_softc *sc)
{
	if (sc->ls_cy >= sc->ls_height-1) {
		/*
		 * Scroll
		 */

		memmove(sc->ls_screen, sc->ls_screen + sc->ls_width,
			sc->ls_width * (sc->ls_height-1));
		bzero(sc->ls_screen + sc->ls_width * (sc->ls_height-1),
		      sc->ls_width);
	}
	else {
		sc->ls_cy++;
	}
	sc->ls_cx=0;
}