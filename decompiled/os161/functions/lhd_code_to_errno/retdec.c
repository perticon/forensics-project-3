int32_t lhd_code_to_errno(int32_t * lh, int32_t code) {
    int32_t result = 8; // 0x80004610
    switch ((int5_t)code & -3) {
        case -12: {
            // 0x80004638
            result = 32;
        }
        case 12: {
            // 0x80004638
            return result;
        }
        case 4: {
            // 0x80004638
            result = 0;
            return result;
        }
        default: {
            // 0x80004648
            kprintf("lhd%d: Unknown result code %d\n", *(int32_t *)((int32_t)lh + 8), code);
            return 4;
        }
    }
}