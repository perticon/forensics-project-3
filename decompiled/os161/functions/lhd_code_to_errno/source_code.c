int lhd_code_to_errno(struct lhd_softc *lh, int code)
{
	switch (code & LHD_STATEMASK) {
	    case LHD_OK: return 0;
	    case LHD_INVSECT: return EINVAL;
	    case LHD_MEDIA: return EIO;
	}
	kprintf("lhd%d: Unknown result code %d\n", lh->lh_unit, code);
	return EAGAIN;
}