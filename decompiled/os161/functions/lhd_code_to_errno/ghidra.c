int lhd_code_to_errno(lhd_softc *lh,int code)

{
  uint uVar1;
  
  uVar1 = code & 0x1d;
  if (uVar1 == 0xc) {
    return 8;
  }
  if (uVar1 != 0x14) {
    if (uVar1 == 4) {
      return 0;
    }
    kprintf("lhd%d: Unknown result code %d\n",lh->lh_unit,code);
    return 4;
  }
  return 0x20;
}