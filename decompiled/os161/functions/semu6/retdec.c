int32_t semu6(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(0); // 0x80012178
    makewaiter(v1);
    kprintf("This should assert that the wchan's threadlist is empty\n");
    sem_destroy(v1);
    panic("semu6: wchan_destroy with waiters succeeded\n");
    return &g41;
}