int semu6(int nargs,char **args)

{
  semaphore *sem;
  
  sem = makesem(0);
  makewaiter(sem);
  kprintf("This should assert that the wchan\'s threadlist is empty\n");
  sem_destroy(sem);
                    /* WARNING: Subroutine does not return */
  panic("semu6: wchan_destroy with waiters succeeded\n");
}