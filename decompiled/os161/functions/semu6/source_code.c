semu6(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	sem = makesem(0);
	makewaiter(sem);
	kprintf("This should assert that the wchan's threadlist is empty\n");
	sem_destroy(sem);
	panic("semu6: wchan_destroy with waiters succeeded\n");
	return 0;
}