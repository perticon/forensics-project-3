int32_t sfs_writeblock(int32_t * sfs, int32_t block, char * data, int32_t len) {
    int32_t v1 = (int32_t)sfs; // 0x80008e5c
    int32_t v2 = block; // 0x80008e5c
    int32_t v3 = (int32_t)data; // 0x80008e5c
    if (len != 512) {
        // 0x80008e60
        badassert("len == SFS_BLOCKSIZE", "../../fs/sfs/sfs_io.c", 128, "sfs_writeblock");
        v1 = (int32_t)"len == SFS_BLOCKSIZE";
        v2 = (int32_t)"../../fs/sfs/sfs_io.c";
        v3 = 128;
    }
    // 0x80008e80
    int32_t iov; // bp-48, 0x80008e4c
    int32_t ku; // bp-40, 0x80008e4c
    uio_kinit(&iov, &ku, (char *)v3, 512, (int64_t)(v2 / 0x800000), 1);
    return sfs_rwblock((int32_t *)v1, &ku);
}