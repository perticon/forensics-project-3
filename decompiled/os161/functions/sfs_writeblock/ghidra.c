int sfs_writeblock(sfs_fs *sfs,daddr_t block,void *data,size_t len)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  iovec iov;
  uio ku;
  
  if (len != 0x200) {
    pcVar2 = "len == SFS_BLOCKSIZE";
    pcVar3 = "../../fs/sfs/sfs_io.c";
    data = (void *)0x80;
    badassert("len == SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x80,"sfs_writeblock");
    sfs = (sfs_fs *)pcVar2;
    block = (daddr_t)pcVar3;
  }
  uio_kinit(&iov,&ku,data,0x200,CONCAT44(block >> 0x17,block << 9),UIO_WRITE);
  iVar1 = sfs_rwblock(sfs,&ku);
  return iVar1;
}