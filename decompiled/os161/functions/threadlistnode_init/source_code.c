threadlistnode_init(struct threadlistnode *tln, struct thread *t)
{
	DEBUGASSERT(tln != NULL);
	KASSERT(t != NULL);

	tln->tln_next = NULL;
	tln->tln_prev = NULL;
	tln->tln_self = t;
}