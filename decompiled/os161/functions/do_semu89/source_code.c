do_semu89(bool interrupthandler)
{
	struct semaphore *sem;
	struct wchan *wchan;
	const char *name;

	sem = makesem(0);

	/* check preconditions */
	name = sem->sem_name;
	wchan = sem->sem_wchan;
	KASSERT(!strcmp(name, NAMESTRING));
	KASSERT(spinlock_not_held(&sem->sem_lock));

	/*
	 * The right way to this is to set up an actual interrupt,
	 * e.g. an interprocessor interrupt, and hook onto it to run
	 * the V() in the actual interrupt handler. However, that
	 * requires a good bit of infrastructure that we don't
	 * have. Instead we'll fake it by explicitly setting
	 * curthread->t_in_interrupt.
	 */
	if (interrupthandler) {
		KASSERT(curthread->t_in_interrupt == false);
		curthread->t_in_interrupt = true;
	}

	V(sem);

	if (interrupthandler) {
		KASSERT(curthread->t_in_interrupt == true);
		curthread->t_in_interrupt = false;
	}

	/* check postconditions */
	KASSERT(name == sem->sem_name);
	KASSERT(!strcmp(name, NAMESTRING));
	KASSERT(wchan == sem->sem_wchan);
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 1);

	/* clean up */
	ok();
	sem_destroy(sem);
}