void do_semu89(bool interrupthandler) {
    int32_t * v1 = makesem(0); // 0x800116ac
    int32_t v2 = (int32_t)v1; // 0x800116ac
    int32_t v3 = *v1; // 0x800116b4
    int32_t * v4 = (int32_t *)(v2 + 4); // 0x800116b8
    if (strcmp((char *)v3, "some-silly-name") != 0) {
        // 0x800116d4
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 331, "do_semu89");
    }
    int32_t * v5 = (int32_t *)(v2 + 8); // 0x800116f8
    if (!spinlock_not_held(v5)) {
        // 0x80011704
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 332, "do_semu89");
    }
    if (!interrupthandler) {
        // 0x8001175c
        V(v1);
    } else {
        // 0x80011728
        int32_t v6; // 0x80011688
        char * v7 = (char *)(v6 + 88);
        char v8 = 1; // 0x80011734
        if (*v7 != 0) {
            // 0x80011738
            badassert("curthread->t_in_interrupt == false", "../../test/semunit.c", 343, "do_semu89");
            v8 = &g41;
        }
        // 0x8001176c
        *v7 = v8;
        V(v1);
        if (*v7 == 0) {
            // 0x8001177c
            badassert("curthread->t_in_interrupt == true", "../../test/semunit.c", 350, "do_semu89");
        }
        // 0x80011798
        *v7 = 0;
    }
    int32_t v9 = v3; // 0x800117a8
    if (*v1 != v3) {
        // 0x800117ac
        badassert("name == sem->sem_name", "../../test/semunit.c", 355, "do_semu89");
        v9 = (int32_t)"name == sem->sem_name";
    }
    // 0x800117cc
    if (strcmp((char *)v9, "some-silly-name") != 0) {
        // 0x800117e0
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 356, "do_semu89");
    }
    // 0x800117fc
    if (*v4 != *v4) {
        // 0x8001180c
        badassert("wchan == sem->sem_wchan", "../../test/semunit.c", 357, "do_semu89");
    }
    // 0x80011828
    if (!spinlock_not_held(v5)) {
        // 0x80011838
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 358, "do_semu89");
    }
    // 0x80011854
    if (*(int32_t *)(v2 + 16) != 1) {
        // 0x80011864
        badassert("sem->sem_count == 1", "../../test/semunit.c", 359, "do_semu89");
    }
    // 0x80011880
    ok();
    sem_destroy(v1);
}