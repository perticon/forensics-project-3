void do_semu89(bool interrupthandler)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  undefined uVar3;
  undefined3 in_register_00000010;
  char *a;
  wchan *pwVar4;
  int unaff_s7;
  
  sem = makesem(0);
  a = sem->sem_name;
  pwVar4 = sem->sem_wchan;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x14b,"do_semu89");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x14c,"do_semu89");
  }
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    uVar3 = 1;
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      uVar3 = 1;
      badassert("curthread->t_in_interrupt == false","../../test/semunit.c",0x157,"do_semu89");
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar3;
  }
  V(sem);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    if (*(char *)(unaff_s7 + 0x58) == '\0') {
      badassert("curthread->t_in_interrupt == true","../../test/semunit.c",0x15e,"do_semu89");
    }
    *(undefined *)(unaff_s7 + 0x58) = 0;
  }
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x163,"do_semu89");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x164,"do_semu89");
  }
  if (sem->sem_wchan != pwVar4) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x165,"do_semu89");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x166,"do_semu89");
  }
  if (sem->sem_count != 1) {
    badassert("sem->sem_count == 1","../../test/semunit.c",0x167,"do_semu89");
  }
  ok();
  sem_destroy(sem);
  return;
}