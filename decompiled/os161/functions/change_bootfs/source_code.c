change_bootfs(struct vnode *newvn)
{
	struct vnode *oldvn;

	oldvn = bootfs_vnode;
	bootfs_vnode = newvn;

	if (oldvn != NULL) {
		VOP_DECREF(oldvn);
	}
}