void change_bootfs(int32_t * newvn) {
    // 0x80019af0
    *(int32_t *)&bootfs_vnode = (int32_t)newvn;
    if (bootfs_vnode != NULL) {
        // 0x80019b04
        vnode_decref(bootfs_vnode);
    }
}