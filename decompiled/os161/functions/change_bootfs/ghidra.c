void change_bootfs(vnode *newvn)

{
  bool bVar1;
  vnode *vn;
  
  vn = bootfs_vnode;
  bVar1 = bootfs_vnode != (vnode *)0x0;
  bootfs_vnode = newvn;
  if (bVar1) {
    vnode_decref(vn);
  }
  return;
}