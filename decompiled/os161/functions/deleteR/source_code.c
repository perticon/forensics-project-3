link deleteR(link x, Key k)
{
    if (x == NULL)
        return NULL;

    if ((KEYcompare(KEYget(&(x->item)), k)) == 0)
    {
        link t = x->next;
        x->item.index = -1;
        return t;
    }

    x->next = deleteR(x->next, k);

    return x;
}