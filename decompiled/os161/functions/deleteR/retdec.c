int32_t * deleteR(int32_t * x, int32_t k) {
    // 0x8000a50c
    if (x == NULL) {
        // 0x8000a584
        return (int32_t *)0;
    }
    int32_t v1 = (int32_t)x;
    int32_t v2; // bp-16, 0x8000a50c
    KEYget(&v2);
    int32_t v3; // 0x8000a50c
    int32_t v4 = KEYcompare(v2, v3); // 0x8000a548
    int32_t * v5 = (int32_t *)(v1 + 12);
    int32_t v6 = *v5;
    int32_t v7; // 0x8000a50c
    if (v4 == 0) {
        // 0x8000a554
        *(int32_t *)(v1 + 8) = -1;
        v7 = v6;
    } else {
        // 0x8000a560
        *v5 = (int32_t)deleteR((int32_t *)v6, k);
        v7 = v1;
    }
    // 0x8000a584
    return (int32_t *)v7;
}