link deleteR(link x,Key k)

{
  int iVar1;
  link pSVar2;
  int unaff_s0;
  undefined4 unaff_retaddr;
  Key in_stack_fffffff0;
  
  if (x == (link)0x0) {
    x = (link)0x0;
  }
  else {
    KEYget((Item)&stack0xfffffff0);
    iVar1 = KEYcompare(in_stack_fffffff0,(Key)CONCAT44(unaff_s0,unaff_retaddr));
    if (iVar1 == 0) {
      pSVar2 = x->next;
      (x->item).index = -1;
      x = pSVar2;
    }
    else {
      pSVar2 = deleteR(x->next,in_stack_fffffff0);
      x->next = pSVar2;
    }
  }
  return x;
}