dolongstress(const char *filesys)
{
	int i, err;

	init_threadsem();

	kprintf("*** Starting fs long stress test on %s:\n", filesys);

	for (i=0; i<NTHREADS; i++) {
		err = thread_fork("longstress", NULL,
				  longstress_thread, (char *)filesys, i);
		if (err) {
			panic("longstress: thread_fork failed %s\n",
			      strerror(err));
		}
	}

	for (i=0; i<NTHREADS; i++) {
		P(threadsem);
	}

	kprintf("*** fs long stress test done\n");
}