void dolongstress(char * filesys) {
    // 0x8001065c
    init_threadsem();
    kprintf("*** Starting fs long stress test on %s:\n", filesys);
    int32_t v1 = 0;
    int32_t v2 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1); // 0x800106b4
    int32_t v3 = v2; // 0x800106bc
    int32_t v4; // 0x800106b4
    if (v2 != 0) {
        panic("longstress: thread_fork failed %s\n", strerror(v3));
        v4 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x800106c0
            panic("longstress: thread_fork failed %s\n", strerror(v3));
            v4 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x800106bc
    while (v5 < 12) {
        // 0x800106a4
        v1 = v5;
        v2 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("longstress: thread_fork failed %s\n", strerror(v3));
            v4 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x800106c0
                panic("longstress: thread_fork failed %s\n", strerror(v3));
                v4 = thread_fork("longstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 856), filesys, v1);
                v3 = v4;
            }
        }
        // 0x800106dc
        v5 = v1 + 1;
    }
    int32_t v6 = 1; // 0x800106fc
    P(threadsem);
    int32_t v7 = v6; // 0x80010708
    while (v6 < 12) {
        // 0x800106f4
        v6 = v7 + 1;
        P(threadsem);
        v7 = v6;
    }
    // 0x8001070c
    kprintf("*** fs long stress test done\n");
}