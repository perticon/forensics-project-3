void lamebus_attach_interrupt(int32_t * sc, uint32_t slot, char * devdata, void (*irqfunc)(char *)) {
    int32_t v1 = (int32_t)sc; // 0x80003edc
    int32_t v2 = (int32_t)devdata; // 0x80003edc
    int32_t v3 = (int32_t)irqfunc; // 0x80003edc
    if (slot >= 32) {
        // 0x80003ee0
        badassert("slot>=0 && slot < LB_NSLOTS", "../../dev/lamebus/lamebus.c", 356, "lamebus_attach_interrupt");
        v1 = (int32_t)"slot>=0 && slot < LB_NSLOTS";
        v2 = 356;
        v3 = (int32_t)"lamebus_attach_interrupt";
    }
    int32_t * v4 = (int32_t *)v1; // 0x80003f0c
    spinlock_acquire(v4);
    int32_t v5 = slot + 2; // 0x80003f20
    int32_t v6 = v5; // 0x80003f20
    if ((1 << slot & 0x73202626) == 0) {
        // 0x80003f24
        panic("lamebus_attach_interrupt: slot %d not marked in use\n", slot);
        v6 = &g41;
    }
    int32_t v7 = v1 + 4;
    int32_t v8 = slot + 34; // 0x80003f48
    if (*(int32_t *)(4 * v6 + v7) != 0) {
        // 0x80003f4c
        badassert("sc->ls_devdata[slot]==NULL", "../../dev/lamebus/lamebus.c", 365, "lamebus_attach_interrupt");
        v8 = &g41;
    }
    int32_t v9 = v5; // 0x80003f80
    if (*(int32_t *)(4 * v8 + v7) != 0) {
        // 0x80003f84
        badassert("sc->ls_irqfuncs[slot]==NULL", "../../dev/lamebus/lamebus.c", 366, "lamebus_attach_interrupt");
        v9 = &g41;
    }
    // 0x80003fa4
    *(int32_t *)(4 * v9 + v7) = v2;
    *(int32_t *)(4 * slot + 140 + v1) = v3;
    spinlock_release(v4);
}