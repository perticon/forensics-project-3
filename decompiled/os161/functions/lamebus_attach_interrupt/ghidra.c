void lamebus_attach_interrupt
               (lamebus_softc *sc,int slot,void *devdata,anon_subr_void_void_ptr *irqfunc)

{
  int iVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  
  if (0x1f < (uint)slot) {
    pcVar3 = "slot>=0 && slot < LB_NSLOTS";
    devdata = (void *)0x164;
    pcVar4 = "lamebus_attach_interrupt";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x164,
              "lamebus_attach_interrupt");
    sc = (lamebus_softc *)pcVar3;
    irqfunc = (anon_subr_void_void_ptr *)pcVar4;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & 1 << (slot & 0x1fU)) == 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_attach_interrupt: slot %d not marked in use\n",slot);
  }
  iVar1 = slot + 0x22;
  if (sc->ls_devdata[slot] != (void *)0x0) {
    badassert("sc->ls_devdata[slot]==NULL","../../dev/lamebus/lamebus.c",0x16d,
              "lamebus_attach_interrupt");
  }
  iVar2 = slot + 2;
  if (sc->ls_devdata[iVar1 + -2] != (void *)0x0) {
    badassert("sc->ls_irqfuncs[slot]==NULL","../../dev/lamebus/lamebus.c",0x16e,
              "lamebus_attach_interrupt");
  }
  sc->ls_devdata[iVar2 + -2] = devdata;
  sc->ls_irqfuncs[slot] = (lb_irqfunc *)irqfunc;
  spinlock_release(&sc->ls_lock);
  return;
}