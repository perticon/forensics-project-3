void splraise(int oldspl,int newspl)

{
  char *pcVar1;
  int unaff_s7;
  
  if (oldspl != 0) {
    pcVar1 = "../../thread/spl.c";
    badassert("oldspl == IPL_NONE","../../thread/spl.c",0x5c,"splraise");
    newspl = (int)pcVar1;
  }
  if ((char *)newspl != (char *)0x1) {
    badassert("newspl == IPL_HIGH","../../thread/spl.c",0x5d,"splraise");
  }
  if (unaff_s7 != 0) {
    if (*(int *)(unaff_s7 + 0x60) == 0) {
      cpu_irqoff();
    }
    *(int *)(unaff_s7 + 0x60) = *(int *)(unaff_s7 + 0x60) + 1;
  }
  return;
}