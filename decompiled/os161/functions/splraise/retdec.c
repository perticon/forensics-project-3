void splraise(int32_t oldspl, int32_t newspl) {
    int32_t v1 = newspl; // 0x8001534c
    if (oldspl != 0) {
        // 0x80015350
        badassert("oldspl == IPL_NONE", "../../thread/spl.c", 92, "splraise");
        v1 = (int32_t)"../../thread/spl.c";
    }
    // 0x80015370
    if (v1 != 1) {
        // 0x8001537c
        badassert("newspl == IPL_HIGH", "../../thread/spl.c", 93, "splraise");
    }
    int32_t v2; // 0x80015340
    if (v2 == 0) {
        // 0x800153cc
        return;
    }
    int32_t * v3 = (int32_t *)(v2 + 96); // 0x800153a4
    int32_t v4 = *v3; // 0x800153a4
    int32_t v5 = v4; // 0x800153b0
    if (v4 == 0) {
        // 0x800153b4
        cpu_irqoff();
        v5 = *v3;
    }
    // 0x800153bc
    *v3 = v5 + 1;
}