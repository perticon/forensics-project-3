void check_order(threadlist *tl,bool rev)

{
  uint uVar1;
  char *a;
  undefined3 in_register_00000014;
  int iVar2;
  thread *ptVar3;
  char *b;
  char string0 [4];
  char stringN [4];
  
  iVar2 = CONCAT31(in_register_00000014,rev);
  string0 = 0x2e2e2e00;
  stringN = 0x7e7e7e00;
  if (iVar2 == 0) {
    a = string0;
    b = stringN;
  }
  else {
    a = stringN;
    b = string0;
  }
  for (ptVar3 = ((tl->tl_head).tln_next)->tln_self; ptVar3 != (thread *)0x0;
      ptVar3 = ((ptVar3->t_listnode).tln_next)->tln_self) {
    uVar1 = strcmp(a,ptVar3->t_name);
    if (iVar2 == 0) {
      uVar1 = uVar1 >> 0x1f;
    }
    else {
      uVar1 = (uint)(0 < (int)uVar1);
    }
    if (uVar1 == 0) {
      badassert("rev ? (cmp > 0) : (cmp < 0)","../../test/threadlisttest.c",0x6e,"check_order");
    }
    a = ptVar3->t_name;
  }
  uVar1 = strcmp(a,b);
  if (iVar2 == 0) {
    uVar1 = uVar1 >> 0x1f;
  }
  else {
    uVar1 = (uint)(0 < (int)uVar1);
  }
  if (uVar1 == 0) {
    badassert("rev ? (cmp > 0) : (cmp < 0)","../../test/threadlisttest.c",0x72,"check_order");
  }
  return;
}