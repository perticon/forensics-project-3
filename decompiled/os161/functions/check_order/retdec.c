void check_order(int32_t * tl, bool rev) {
    char string0[4]; // bp-24, 0x80013858
    char stringN[4]; // bp-20, 0x80013858
    // 0x80013858
    string0 = {0, 0, 0, 0};
    stringN = {0, 0, 0, 0};
    int32_t v1 = (int32_t)(!rev ? &string0 : &stringN);
    int32_t v2 = *(int32_t *)(*(int32_t *)((int32_t)tl + 4) + 8);
    int32_t v3 = v2; // 0x80013920
    int32_t v4 = v1; // 0x80013920
    if (v2 != 0) {
        int32_t * v5 = (int32_t *)v3; // 0x800138c4
        int32_t v6 = strcmp((char *)v1, (char *)*v5); // 0x800138cc
        if (!((!rev ? v6 < 0 : v6 > 0))) {
            // 0x800138ec
            badassert("rev ? (cmp > 0) : (cmp < 0)", "../../test/threadlisttest.c", 110, "check_order");
        }
        int32_t v7 = *v5; // 0x80013908
        v3 = *(int32_t *)(*(int32_t *)(v3 + 64) + 8);
        v4 = v7;
        while (v3 != 0) {
            // 0x800138c4
            v5 = (int32_t *)v3;
            v6 = strcmp((char *)v7, (char *)*v5);
            if (!((!rev ? v6 < 0 : v6 > 0))) {
                // 0x800138ec
                badassert("rev ? (cmp > 0) : (cmp < 0)", "../../test/threadlisttest.c", 110, "check_order");
            }
            // 0x80013908
            v7 = *v5;
            v3 = *(int32_t *)(*(int32_t *)(v3 + 64) + 8);
            v4 = v7;
        }
    }
    int32_t v8 = strcmp((char *)v4, &!rev ? &stringN : &string0[0]); // 0x80013928
    if (!((!rev ? v8 < 0 : v8 > 0))) {
        // 0x80013948
        badassert("rev ? (cmp > 0) : (cmp < 0)", "../../test/threadlisttest.c", 114, "check_order");
    }
}