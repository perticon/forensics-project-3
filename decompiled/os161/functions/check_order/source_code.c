check_order(struct threadlist *tl, bool rev)
{
	const char string0[] = "...";
	const char stringN[] = "~~~";

	struct thread *t;
	const char *first = rev ? stringN : string0;
	const char *last = rev ? string0 : stringN;
	const char *prev;
	int cmp;

	prev = first;
	THREADLIST_FORALL(t, *tl) {
		cmp = strcmp(prev, t->t_name);
		KASSERT(rev ? (cmp > 0) : (cmp < 0));
		prev = t->t_name;
	}
	cmp = strcmp(prev, last);
	KASSERT(rev ? (cmp > 0) : (cmp < 0));
}