int copycheck(const_userptr_t userptr,size_t len,size_t *stoplen)

{
  *stoplen = len;
  if (userptr + (len - 1) < userptr) {
    return 6;
  }
  if (-1 < (int)userptr) {
    if ((int)(userptr + (len - 1)) < 0) {
      *stoplen = 0x80000000 - (int)userptr;
      return 0;
    }
    return 0;
  }
  return 6;
}