int32_t copycheck(int32_t * userptr, int32_t len, int32_t * stoplen) {
    uint32_t v1 = (int32_t)userptr;
    *stoplen = len;
    int32_t v2 = len - 1 + v1; // 0x80020168
    if (userptr < NULL || v2 < v1) {
        // 0x8002019c
        return 6;
    }
    // 0x80020180
    if (v2 > -1) {
        // 0x8002019c
        return 0;
    }
    // 0x80020188
    *stoplen = -0x80000000 - v1;
    return 0;
}