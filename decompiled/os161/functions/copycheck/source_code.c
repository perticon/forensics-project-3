copycheck(const_userptr_t userptr, size_t len, size_t *stoplen)
{
	vaddr_t bot, top;

	*stoplen = len;

	bot = (vaddr_t) userptr;
	top = bot+len-1;

	if (top < bot) {
		/* addresses wrapped around */
		return EFAULT;
	}

	if (bot >= USERSPACETOP) {
		/* region is within the kernel */
		return EFAULT;
	}

	if (top >= USERSPACETOP) {
		/* region overlaps the kernel. adjust the max length. */
		*stoplen = USERSPACETOP - bot;
	}

	return 0;
}