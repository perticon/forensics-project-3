write_ctlcpu_register(struct lamebus_softc *lb, unsigned hw_cpunum,
		      uint32_t offset, uint32_t val)
{
	offset += LB_CTLCPU_OFFSET + hw_cpunum * LB_CTLCPU_SIZE;
	lamebus_write_register(lb, LB_CONTROLLER_SLOT, offset, val);
}