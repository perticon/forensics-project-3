int proc_wait(struct proc *proc)
{
#if OPT_WAITPID
	int return_status;
	/* NULL and kernel proc forbidden */
	KASSERT(proc != NULL);
	KASSERT(proc != kproc);

	/* wait on semaphore or condition variable */

	lock_acquire(proc->p_wlock);
	if (proc->finish == 0)
	{
		cv_wait(proc->p_cv, proc->p_wlock);
	}
	lock_release(proc->p_wlock);

	return_status = proc->p_status;
	proc_destroy(proc);
	return return_status;
#else
	/* this doesn't synchronize */
	(void)proc;
	return 0;
#endif
}