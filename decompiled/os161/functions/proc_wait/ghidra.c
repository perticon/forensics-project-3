int proc_wait(proc *proc)

{
  char *pcVar1;
  int iVar2;
  
  pcVar1 = (char *)proc;
  if (proc == (proc *)0x0) {
    pcVar1 = "proc != NULL";
    badassert("proc != NULL","../../proc/proc.c",0x1c8,"proc_wait");
  }
  if ((proc *)pcVar1 == kproc) {
    pcVar1 = "proc != kproc";
    badassert("proc != kproc","../../proc/proc.c",0x1c9,"proc_wait");
  }
  lock_acquire(*(lock **)((int)pcVar1 + 0x24));
  if (proc->finish == 0) {
    cv_wait(proc->p_cv,proc->p_wlock);
  }
  lock_release(proc->p_wlock);
  iVar2 = proc->p_status;
  proc_destroy(proc);
  return iVar2;
}