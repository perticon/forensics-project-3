int32_t proc_wait(int32_t * proc) {
    int32_t v1 = (int32_t)proc;
    int32_t v2 = v1; // 0x8000d0d4
    if (proc == NULL) {
        // 0x8000d0d8
        badassert("proc != NULL", "../../proc/proc.c", 456, "proc_wait");
        v2 = (int32_t)"proc != NULL";
    }
    int32_t v3 = v2; // 0x8000d108
    if (v2 == (int32_t)kproc) {
        // 0x8000d10c
        badassert("proc != kproc", "../../proc/proc.c", 457, "proc_wait");
        v3 = (int32_t)"proc != kproc";
    }
    // 0x8000d128
    lock_acquire((int32_t *)*(int32_t *)(v3 + 36));
    int32_t * v4; // 0x8000d0c0
    if (*(int32_t *)(v1 + 40) == 0) {
        int32_t * v5 = (int32_t *)(v1 + 36);
        cv_wait((int32_t *)*(int32_t *)(v1 + 32), (int32_t *)*v5);
        v4 = v5;
    } else {
        // 0x8000d128
        v4 = (int32_t *)(v1 + 36);
    }
    // 0x8000d154
    lock_release((int32_t *)*v4);
    proc_destroy(proc);
    return *(int32_t *)(v1 + 24);
}