int semu4(int nargs,char **args)

{
  semaphore *sem;
  
  sem = makesem(0);
  sem->sem_count = sem->sem_count - 1;
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../test/semunit.c",0xe0,"semu4");
  }
  ok();
  sem_destroy(sem);
  return 0;
}