semu4(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	/* Create a semaphore with count 0. */
	sem = makesem(0);
	/* Decrement the count. */
	sem->sem_count--;
	/* This value should be positive. */
	KASSERT(sem->sem_count > 0);

	/* Clean up. */
	ok();
	sem_destroy(sem);
	return 0;
}