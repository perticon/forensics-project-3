int32_t semu4(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(0); // 0x80012080
    int32_t * v2 = (int32_t *)((int32_t)v1 + 16); // 0x80012088
    int32_t v3 = *v2 - 1; // 0x80012090
    *v2 = v3;
    if (v3 == 0) {
        // 0x800120a8
        badassert("sem->sem_count > 0", "../../test/semunit.c", 224, "semu4");
    }
    // 0x800120c4
    ok();
    sem_destroy(v1);
    return 0;
}