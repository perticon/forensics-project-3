int sfs_loadvnode(sfs_fs *sfs,uint32_t ino,int forcetype,sfs_vnode **ret)

{
  uint16_t uVar1;
  vnodearray *pvVar2;
  sfs_vnode *psVar3;
  int iVar4;
  undefined uVar5;
  vnode_ops *ops;
  uint uVar6;
  array *a;
  uint uVar7;
  
  uVar7 = (sfs->sfs_vnodes->arr).num;
  uVar6 = 0;
  while (uVar6 < uVar7) {
    pvVar2 = sfs->sfs_vnodes;
    if ((pvVar2->arr).num <= uVar6) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    psVar3 = *(sfs_vnode **)((int)(pvVar2->arr).v[uVar6] + 0x10);
    iVar4 = sfs_bused(sfs,psVar3->sv_ino);
    if (iVar4 == 0) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: Found inode %u in unallocated block\n",(sfs->sfs_sb).sb_volname,psVar3->sv_ino
           );
    }
    uVar6 = uVar6 + 1;
    if (psVar3->sv_ino == ino) {
      if (forcetype != 0) {
        badassert("forcetype==SFS_TYPE_INVAL","../../fs/sfs/sfs_inode.c",0xb1,"sfs_loadvnode");
      }
      vnode_incref(&psVar3->sv_absvn);
      *ret = psVar3;
      return 0;
    }
  }
  psVar3 = (sfs_vnode *)kmalloc(0x220);
  if (psVar3 == (sfs_vnode *)0x0) {
    return 3;
  }
  iVar4 = sfs_bused(sfs,ino);
  if (iVar4 != 0) {
    iVar4 = sfs_readblock(sfs,ino,&psVar3->sv_i,0x200);
    if (iVar4 != 0) {
      kfree(psVar3);
      return iVar4;
    }
    psVar3->sv_dirty = false;
    if (forcetype != 0) {
      uVar5 = true;
      if ((psVar3->sv_i).sfi_type != 0) {
        uVar5 = 1;
        badassert("sv->sv_i.sfi_type == SFS_TYPE_INVAL","../../fs/sfs/sfs_inode.c",0xd6,
                  "sfs_loadvnode");
      }
      (psVar3->sv_i).sfi_type = (uint16_t)forcetype;
      psVar3->sv_dirty = (bool)uVar5;
    }
    uVar1 = (psVar3->sv_i).sfi_type;
    if (uVar1 == 1) {
      ops = &sfs_fileops;
    }
    else {
      if (uVar1 != 2) {
                    /* WARNING: Subroutine does not return */
        panic("sfs: %s: loadvnode: Invalid inode type (inode %u, type %u)\n",
              (sfs->sfs_sb).sb_volname,ino);
      }
      ops = &sfs_dirops;
    }
    iVar4 = vnode_init((vnode *)psVar3,ops,&sfs->sfs_absfs,psVar3);
    if (iVar4 == 0) {
      psVar3->sv_ino = ino;
      a = (array *)sfs->sfs_vnodes;
      uVar6 = a->num;
      iVar4 = array_setsize(a,uVar6 + 1);
      if (iVar4 == 0) {
        a->v[uVar6] = psVar3;
        iVar4 = 0;
      }
      if (iVar4 == 0) {
        *ret = psVar3;
        return 0;
      }
      vnode_cleanup((vnode *)psVar3);
      kfree(psVar3);
      return iVar4;
    }
    kfree(psVar3);
    return iVar4;
  }
                    /* WARNING: Subroutine does not return */
  panic("sfs: %s: Tried to load inode %u from unallocated block\n",(sfs->sfs_sb).sb_volname,ino);
}