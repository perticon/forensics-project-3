int32_t sfs_loadvnode(int32_t * sfs, int32_t ino, int32_t forcetype, int32_t ** ret) {
    int32_t v1 = (int32_t)sfs;
    int32_t * v2 = (int32_t *)(v1 + 528); // 0x800086b0
    char * v3 = (char *)(v1 + 16);
    int32_t v4 = 0; // 0x800086c0
    int32_t v5; // 0x80008710
    while (v4 < *(int32_t *)(*v2 + 4)) {
        int32_t v6 = *v2; // 0x800086c4
        int32_t v7 = v6; // 0x800086dc
        if (v4 >= *(int32_t *)(v6 + 4)) {
            // 0x800086e0
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v7 = &g41;
        }
        int32_t v8 = *(int32_t *)v7; // 0x800086fc
        v5 = *(int32_t *)(*(int32_t *)(v8 + 4 * v4) + 16);
        int32_t * v9 = (int32_t *)(v5 + 536); // 0x80008718
        if (sfs_bused(sfs, *v9) == 0) {
            // 0x8000872c
            panic("sfs: %s: Found inode %u in unallocated block\n", v3, *v9);
        }
        // 0x8000873c
        v4++;
        if (*v9 == ino) {
            if (forcetype == 0) {
                goto lab_0x80008770;
            } else {
                // 0x80008754
                badassert("forcetype==SFS_TYPE_INVAL", "../../fs/sfs/sfs_inode.c", 177, "sfs_loadvnode");
                goto lab_0x80008770;
            }
        }
    }
    char * v10 = kmalloc(544); // 0x80008794
    if (v10 == NULL) {
        // 0x80008904
        return 3;
    }
    int32_t v11 = v1; // 0x800087b0
    if (sfs_bused(sfs, ino) == 0) {
        // 0x800087b4
        panic("sfs: %s: Tried to load inode %u from unallocated block\n", v3, ino);
        v11 = (int32_t)"sfs: %s: Tried to load inode %u from unallocated block\n";
    }
    int32_t v12 = (int32_t)v10; // 0x80008794
    int32_t result = sfs_readblock((int32_t *)v11, ino, (char *)(v12 + 24), 512); // 0x800087d4
    if (result != 0) {
        // 0x800087e0
        kfree(v10);
        // 0x80008904
        return result;
    }
    char * v13 = (char *)(v12 + 540); // 0x800087f4
    *v13 = 0;
    int16_t * v14 = (int16_t *)(v12 + 28);
    if (forcetype != 0) {
        char v15 = 1; // 0x80008804
        if (*v14 != 0) {
            // 0x80008808
            badassert("sv->sv_i.sfi_type == SFS_TYPE_INVAL", "../../fs/sfs/sfs_inode.c", 214, "sfs_loadvnode");
            v15 = &g41;
        }
        // 0x80008828
        *v14 = (int16_t)forcetype;
        *v13 = v15;
    }
    uint16_t v16 = *v14; // 0x80008830
    int32_t v17 = &sfs_fileops; // 0x8000867c
    switch (v16) {
        case 2: {
            // 0x8000886c
            v17 = &sfs_dirops;
        }
        case 1: {
          lab_0x8000886c:;
            int32_t result2 = vnode_init((int32_t *)v10, (int32_t *)v17, sfs, v10); // 0x80008878
            if (result2 != 0) {
                // 0x80008884
                kfree(v10);
                // 0x80008904
                return result2;
            }
            // break -> 0x80008894
            break;
        }
        default: {
            // 0x80008848
            panic("sfs: %s: loadvnode: Invalid inode type (inode %u, type %u)\n", v3, ino, (int32_t)v16);
            v17 = &sfs_fileops;
            // branch (via goto) -> 0x8000886c
            goto lab_0x8000886c;
        }
    }
    // 0x80008894
    *(int32_t *)(v12 + 536) = ino;
    int32_t v18 = *v2; // 0x80008898
    int32_t v19 = *(int32_t *)(v18 + 4); // 0x800088a0
    int32_t * v20 = (int32_t *)v18; // 0x800088ac
    int32_t v21 = array_setsize(v20, v19 + 1); // 0x800088ac
    int32_t result3; // 0x8000867c
    if (v21 == 0) {
        // 0x800088f4
        *(int32_t *)(*v20 + 4 * v19) = v12;
        *(int32_t *)ret = v12;
        result3 = 0;
    } else {
        // 0x800088dc
        vnode_cleanup((int32_t *)v10);
        kfree(v10);
        result3 = v21;
    }
    // 0x80008904
    return result3;
  lab_0x80008770:
    // 0x80008770
    vnode_incref((int32_t *)v5);
    *(int32_t *)ret = v5;
    result3 = 0;
    return result3;
}