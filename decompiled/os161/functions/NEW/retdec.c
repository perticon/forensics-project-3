int32_t * NEW(int32_t * item, int32_t * next) {
    int32_t v1 = (int32_t)item; // 0x8000a0bc
    int32_t v2 = (int32_t)next; // 0x8000a0bc
    int32_t v3 = 0; // 0x8000a0bc
    int32_t v4 = (int32_t)&g35; // 0x8000a0bc
    int32_t v5; // 0x8000a090
    int32_t v6; // 0x8000a090
    int32_t v7; // 0x8000a15c
    while (true) {
        // 0x8000a148
        v6 = v2;
        v5 = v1;
        int32_t * v8 = (int32_t *)(v4 - 0x73f8); // 0x8000a148
        int32_t v9 = *v8;
        v7 = 16 * v9 + (int32_t)link_list;
        if (*(int32_t *)(v7 + 8) == -1) {
            // break (via goto) -> 0x8000a170
            goto lab_0x8000a170;
        }
        int32_t v10 = v9 + 1; // 0x8000a16c
        *v8 = v10;
        int32_t v11 = v3 + 1; // 0x8000a0d4
        v9 = v10 < n_entries ? v10 : 0;
        *v8 = v9;
        int32_t v12 = v11; // 0x8000a0e4
        while (v11 < n_entries) {
            // 0x8000a148
            v7 = 16 * v9 + (int32_t)link_list;
            if (*(int32_t *)(v7 + 8) == -1) {
                // break (via goto) -> 0x8000a170
                goto lab_0x8000a170;
            }
            // 0x8000a0c0
            v10 = v9 + 1;
            *v8 = v10;
            v11 = v12 + 1;
            v9 = v10 < n_entries ? v10 : 0;
            *v8 = v9;
            v12 = v11;
        }
        int32_t v13 = 0; // 0x8000a138
        v2 = v6;
        v3 = v11;
        if (n_entries > 0) {
            int32_t v14 = 16 * v13 + (int32_t)link_list; // 0x8000a0f8
            int32_t v15 = *(int32_t *)(v14 + 8); // 0x8000a104
            int32_t v16 = *(int32_t *)v14; // 0x8000a108
            int32_t v17 = *(int32_t *)(v14 + 4) / 0x1000; // 0x8000a110
            kprintf("Index: %d addr %d pid %d\n", v15, v17, v16);
            int32_t v18 = v13 + 1; // 0x8000a118
            v13 = v18;
            v2 = v15;
            v3 = v17;
            v4 = v16;
            while (v18 < n_entries) {
                // 0x8000a0f0
                v14 = 16 * v13 + (int32_t)link_list;
                v15 = *(int32_t *)(v14 + 8);
                v16 = *(int32_t *)v14;
                v17 = *(int32_t *)(v14 + 4) / 0x1000;
                kprintf("Index: %d addr %d pid %d\n", v15, v17, v16);
                v18 = v13 + 1;
                v13 = v18;
                v2 = v15;
                v3 = v17;
                v4 = v16;
            }
        }
        // 0x8000a13c
        panic("No free entry in hash table\n");
        v1 = (int32_t)"No free entry in hash table\n";
    }
  lab_0x8000a170:;
    int32_t v19 = v7; // 0x8000a174
    int32_t v20 = v5; // 0x8000a174
    int32_t v21 = v6; // 0x8000a174
    if (v7 == 0) {
        // 0x8000a178
        badassert("x != NULL", "../../hash/st.c", 289, "NEW");
        v19 = &g41;
        v20 = (int32_t)"x != NULL";
        v21 = (int32_t)"../../hash/st.c";
    }
    // 0x8000a194
    *(int32_t *)(v19 + 8) = *(int32_t *)(v20 + 8);
    *(int32_t *)(v19 + 4) = *(int32_t *)(v20 + 4);
    int32_t * result = (int32_t *)v19; // 0x8000a1b4
    *result = *(int32_t *)v20;
    *(int32_t *)(v19 + 12) = v21;
    return result;
}