link NEW(Item item,link next)

{
  link pSVar1;
  int iVar2;
  char *pcVar3;
  char *pcVar4;
  int iVar5;
  
  iVar5 = 0;
  do {
    pSVar1 = link_list + free_link;
    iVar2 = free_link + 1;
    if ((pSVar1->item).index == -1) {
      if (pSVar1 == (link)0x0) {
        pcVar3 = "x != NULL";
        pcVar4 = "../../hash/st.c";
        badassert("x != NULL","../../hash/st.c",0x121,"NEW");
        item = (Item)pcVar3;
        next = (link)pcVar4;
      }
      (pSVar1->item).index = item->index;
      (pSVar1->item).key.kaddr = (item->key).kaddr;
      (pSVar1->item).key.kpid = (item->key).kpid;
      pSVar1->next = next;
      return pSVar1;
    }
    iVar5 = iVar5 + 1;
    free_link = iVar2;
    if (n_entries <= iVar2) {
      free_link = 0;
    }
    iVar2 = 0;
  } while (iVar5 < n_entries);
  for (; iVar2 < n_entries; iVar2 = iVar2 + 1) {
    pSVar1 = link_list + iVar2;
    kprintf("Index: %d addr %d pid %d\n",(pSVar1->item).index,(pSVar1->item).key.kaddr >> 0xc,
            (pSVar1->item).key.kpid);
  }
                    /* WARNING: Subroutine does not return */
  panic("No free entry in hash table\n");
}