link NEW(Item item, link next)
{

    int j = 0;
    while (link_list[free_link].item.index != -1)
    {
        free_link++;
        j++;
        if (free_link >= n_entries)
        {
            free_link = 0;
        }
        if (j >= n_entries)
        {            
            for(int i=0; i<n_entries; i++){
                kprintf("Index: %d addr %d pid %d\n", link_list[i].item.index, link_list[i].item.key.kaddr/PAGE_SIZE, link_list[i].item.key.kpid);
            }
            panic("No free entry in hash table\n");
        }
    }
    link x = &link_list[free_link];
    KASSERT(x != NULL);

    x->item.index = item->index;
    x->item.key.kaddr = item->key.kaddr;
    x->item.key.kpid = item->key.kpid;
    x->next = next;
    return x;
}