void sys__exit(int status)
{

 

#if OPT_WAITPID
  struct proc *p = curproc;
  pid_t pid = p->p_pid;
   /* thread exits. proc data structure will be lost */
  free_ipt_process(pid);
 // hash_print();
  /* free swap_table entries when process exits */
  free_swap_table(pid);
  p->p_status = status & 0xff; /* just lower 8 bits returned */
  proc_remthread(curthread);
  lock_acquire(p->p_wlock);
  cv_signal(p->p_cv, p->p_wlock);
  lock_release(p->p_wlock);
  p->finish = 1;

#else
  /* get address space of current process and destroy */
  struct addrspace *as = proc_getas();
  as_destroy(as);
#endif


  #if PRINT_TABLES
  print_ipt();
  print_freeRamFrames();
  hash_print();
  print_swap();
#endif
  thread_exit();
  






  panic("thread_exit returned (should not happen)\n");
  (void)status; // TODO: status handling
}