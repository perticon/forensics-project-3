void sys__exit(uint32_t status) {
    // 0x8000dcac
    int32_t v1; // 0x8000dcac
    int32_t v2 = *(int32_t *)(v1 + 84); // 0x8000dcc4
    int32_t v3 = *(int32_t *)(v2 + 28); // 0x8000dccc
    free_ipt_process(v3);
    free_swap_table(v3);
    *(int32_t *)(v2 + 24) = status % 256;
    proc_remthread((int32_t *)v1);
    int32_t * v4 = (int32_t *)(v2 + 36); // 0x8000dcf0
    lock_acquire((int32_t *)*v4);
    cv_signal((int32_t *)*(int32_t *)(v2 + 32), (int32_t *)*v4);
    lock_release((int32_t *)*v4);
    *(int32_t *)(v2 + 40) = 1;
    thread_exit();
}