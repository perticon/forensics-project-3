void sys__exit(int status)

{
  proc *ppVar1;
  pid_t pid;
  thread *unaff_s7;
  
  ppVar1 = unaff_s7->t_proc;
  pid = ppVar1->p_pid;
  free_ipt_process(pid);
  free_swap_table(pid);
  ppVar1->p_status = status & 0xff;
  proc_remthread(unaff_s7);
  lock_acquire(ppVar1->p_wlock);
  cv_signal(ppVar1->p_cv,ppVar1->p_wlock);
  lock_release(ppVar1->p_wlock);
                    /* WARNING: Subroutine does not return */
  ppVar1->finish = 1;
  thread_exit();
}