int32_t emu_open(int32_t * sc, int32_t handle, char * name, bool create, bool excl, int32_t mode, int32_t * newhandle, int32_t * newisdir) {
    // 0x800031f0
    if (strlen(name) >= 0x4000) {
        // 0x80003328
        return 7;
    }
    int32_t v1 = (int32_t)sc;
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x80003264
    lock_acquire((int32_t *)*v2);
    strcpy((char *)*(int32_t *)(v1 + 20), name);
    __asm_sync();
    int32_t v3 = strlen(name); // 0x80003284
    int32_t * v4 = (int32_t *)(v1 + 4); // 0x8000328c
    lamebus_write_register((int32_t *)name, *v4, 8, v3);
    lamebus_write_register((int32_t *)name, *v4, 0, handle);
    lamebus_write_register((int32_t *)name, *v4, 12, create == excl ? 3 : !create ? 1 : 2);
    int32_t result = emu_waitdone(sc); // 0x800032c8
    if (result == 0) {
        // 0x800032d4
        int32_t v5; // 0x800031f0
        *(int32_t *)v5 = lamebus_read_register(sc, *v4, 0);
        *(int32_t *)v5 = (int32_t)(lamebus_read_register(sc, *v4, 8) != 0);
    }
    // 0x80003310
    lock_release((int32_t *)*v2);
    // 0x80003328
    return result;
}