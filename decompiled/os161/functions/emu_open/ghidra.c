int emu_open(emu_softc *sc,uint32_t handle,char *name,bool create,bool excl,mode_t mode,
            uint32_t *newhandle,int *newisdir)

{
  size_t sVar1;
  int iVar2;
  undefined3 in_register_0000001c;
  uint32_t uVar3;
  
  sVar1 = strlen(name);
  if (sVar1 + 1 < 0x4001) {
    if ((CONCAT31(in_register_0000001c,create) == 0) || (excl == false)) {
      uVar3 = 2;
      if (CONCAT31(in_register_0000001c,create) == 0) {
        uVar3 = 1;
      }
    }
    else {
      uVar3 = 3;
    }
    lock_acquire(sc->e_lock);
    strcpy((char *)sc->e_iobuf,name);
    SYNC(0);
    sVar1 = strlen(name);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,sVar1);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,uVar3);
    iVar2 = emu_waitdone(sc);
    if (iVar2 == 0) {
      uVar3 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0);
      *newhandle = uVar3;
      uVar3 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
      *newisdir = (uint)(uVar3 != 0);
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar2 = 7;
  }
  return iVar2;
}