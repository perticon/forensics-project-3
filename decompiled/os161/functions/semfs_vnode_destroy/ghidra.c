void semfs_vnode_destroy(semfs_vnode *semv)

{
  vnode_cleanup(&semv->semv_absvn);
  kfree(semv);
  return;
}