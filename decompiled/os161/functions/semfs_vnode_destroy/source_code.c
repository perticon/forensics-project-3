semfs_vnode_destroy(struct semfs_vnode *semv)
{
	vnode_cleanup(&semv->semv_absvn);
	kfree(semv);
}