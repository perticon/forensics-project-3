lock_release(struct lock *lock)
{
        // Write this
#if OPT_SYNCH
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));
	spinlock_acquire(&lock->lk_lock);
        lock->lk_owner=NULL;
	/*  G.Cabodi - 2019: no problem here owning a spinlock, as V/wchan_wakeone 
	    do not lead to wait state */
#if USE_SEMAPHORE_FOR_LOCK
        V(lock->lk_sem);
#else
        wchan_wakeone(lock->lk_wchan, &lock->lk_lock);
#endif
	spinlock_release(&lock->lk_lock);
#endif

        (void)lock;  // suppress warning until code gets written
}