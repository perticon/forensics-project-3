void lock_release(lock *lock)

{
  undefined3 extraout_var;
  bool bVar1;
  char *lock_00;
  
  lock_00 = (char *)lock;
  if (lock == (lock *)0x0) {
    lock_00 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xee,"lock_release");
  }
  bVar1 = lock_do_i_hold((lock *)lock_00);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("lock_do_i_hold(lock)","../../thread/synch.c",0xef,"lock_release");
  }
  spinlock_acquire(&lock->lk_lock);
  lock->lk_owner = (thread *)0x0;
  V(lock->lk_sem);
  spinlock_release(&lock->lk_lock);
  return;
}