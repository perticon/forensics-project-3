void lock_release(int32_t * lock) {
    int32_t v1 = (int32_t)lock;
    int32_t v2 = v1; // 0x80015a50
    if (lock == NULL) {
        // 0x80015a54
        badassert("lock != NULL", "../../thread/synch.c", 238, "lock_release");
        v2 = (int32_t)"lock != NULL";
    }
    // 0x80015a74
    if (!lock_do_i_hold((int32_t *)v2)) {
        // 0x80015a84
        badassert("lock_do_i_hold(lock)", "../../thread/synch.c", 239, "lock_release");
    }
    int32_t * v3 = (int32_t *)(v1 + 8); // 0x80015aa8
    spinlock_acquire(v3);
    *(int32_t *)(v1 + 16) = 0;
    V((int32_t *)*(int32_t *)(v1 + 4));
    spinlock_release(v3);
}