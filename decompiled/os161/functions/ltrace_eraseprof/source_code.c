ltrace_eraseprof(void)
{
	if (the_trace != NULL && the_trace->lt_canprof) {
		bus_write_register(the_trace->lt_busdata, the_trace->lt_buspos,
				   LTRACE_REG_PROFCL, 1);
	}
}