void ltrace_eraseprof(void)

{
  if ((the_trace != (ltrace_softc *)0x0) && (the_trace->lt_canprof != false)) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,0x18,1);
  }
  return;
}