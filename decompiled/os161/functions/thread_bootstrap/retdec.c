void thread_bootstrap(void) {
    // 0x80016564
    array_init(&allcpus);
    int32_t v1; // 0x80016564
    if (v1 == 0) {
        // 0x800165ac
        cpu_create(0);
        badassert("CURCPU_EXISTS() == true", "../../thread/thread.c", 386, "thread_bootstrap");
    } else {
        // 0x8001659c
        badassert("CURCPU_EXISTS() == false", "../../thread/thread.c", 384, "thread_bootstrap");
        cpu_create(0);
    }
    // 0x800165c8
    if (*(int32_t *)(v1 + 80) == 0) {
        // 0x800165d8
        badassert("curcpu != NULL", "../../thread/thread.c", 389, "thread_bootstrap");
    }
    if (v1 == 0) {
        // 0x800165fc
        badassert("curthread != NULL", "../../thread/thread.c", 390, "thread_bootstrap");
    }
    int32_t v2 = *(int32_t *)(v1 + 84); // 0x80016618
    int32_t v3 = v2; // 0x80016624
    if (v2 == 0) {
        // 0x80016628
        badassert("curthread->t_proc != NULL", "../../thread/thread.c", 391, "thread_bootstrap");
        v3 = &g41;
    }
    // 0x80016648
    if (v3 != (int32_t)kproc) {
        // 0x80016658
        badassert("curthread->t_proc == kproc", "../../thread/thread.c", 392, "thread_bootstrap");
    }
}