void thread_bootstrap(void)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  
  array_init(&allcpus.arr);
  if (unaff_s7 != 0) {
    badassert("CURCPU_EXISTS() == false","../../thread/thread.c",0x180,"thread_bootstrap");
  }
  cpu_create(0);
  if (unaff_s7 == 0) {
    badassert("CURCPU_EXISTS() == true","../../thread/thread.c",0x182,"thread_bootstrap");
  }
  if (*(int *)(unaff_s7 + 0x50) == 0) {
    badassert("curcpu != NULL","../../thread/thread.c",0x185,"thread_bootstrap");
  }
  if (unaff_s7 == 0) {
    badassert("curthread != NULL","../../thread/thread.c",0x186,"thread_bootstrap");
  }
  iVar1 = *(int *)(unaff_s7 + 0x54);
  iVar2 = -0x7ffd0000;
  if (iVar1 == 0) {
    badassert("curthread->t_proc != NULL","../../thread/thread.c",0x187,"thread_bootstrap");
  }
  if (iVar1 != *(int *)(iVar2 + -0x73e0)) {
    badassert(s_curthread__t_proc____kproc_80026d8c,"../../thread/thread.c",0x188,"thread_bootstrap"
             );
  }
  return;
}