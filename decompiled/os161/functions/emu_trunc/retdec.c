int32_t emu_trunc(int32_t * sc, int32_t handle, int64_t len) {
    int32_t v1 = (int32_t)sc; // 0x800028b8
    int32_t v2 = handle; // 0x800028b8
    int32_t v3; // 0x800028a4
    if ((int32_t)len <= 0xffffffff) {
        // 0x800028bc
        badassert("len >= 0", "../../dev/lamebus/emu.c", 391, "emu_trunc");
        v1 = (int32_t)"len >= 0";
        v2 = (int32_t)"../../dev/lamebus/emu.c";
        v3 = (int32_t)"emu_trunc";
    }
    // 0x800028dc
    lock_acquire((int32_t *)0x74746567);
    lamebus_write_register((int32_t *)0x206e656c, 0x30203d3e, 0, v2);
    lamebus_write_register((int32_t *)0x206e656c, 0x30203d3e, 8, v3);
    lamebus_write_register((int32_t *)0x206e656c, 0x30203d3e, 12, 9);
    int32_t result = emu_waitdone((int32_t *)v1); // 0x80002930
    lock_release((int32_t *)0x74746567);
    return result;
}