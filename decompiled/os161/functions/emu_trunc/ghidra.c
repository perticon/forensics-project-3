int emu_trunc(emu_softc *sc,uint32_t handle,off_t len)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  int in_a2;
  char *in_a3;
  
  if (in_a2 < 0) {
    pcVar2 = "len >= 0";
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    in_a3 = "emu_trunc";
    badassert("len >= 0",s_______dev_lamebus_emu_c_80022750,0x187,"emu_trunc");
    sc = (emu_softc *)pcVar2;
    handle = (uint32_t)pcVar3;
  }
  lock_acquire(sc->e_lock);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,(uint32_t)in_a3);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,9);
  iVar1 = emu_waitdone(sc);
  lock_release(sc->e_lock);
  return iVar1;
}