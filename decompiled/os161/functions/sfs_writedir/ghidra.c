int sfs_writedir(sfs_vnode *sv,int slot,sfs_direntry *sd)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  
  if (slot < 0) {
    pcVar2 = "slot>=0";
    pcVar3 = "../../fs/sfs/sfs_dir.c";
    sd = (sfs_direntry *)0x45;
    badassert("slot>=0","../../fs/sfs/sfs_dir.c",0x45,"sfs_writedir");
    sv = (sfs_vnode *)pcVar2;
    slot = (int)pcVar3;
  }
  iVar1 = sfs_metaio(sv,CONCAT44(sd,0x40),(void *)slot,0,slot << 6);
  return iVar1;
}