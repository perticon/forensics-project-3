int32_t sfs_writedir(int32_t * sv, uint32_t slot, int32_t * sd) {
    int32_t v1 = (int32_t)sv; // 0x80007a58
    int32_t v2 = slot; // 0x80007a58
    int32_t v3 = (int32_t)sd; // 0x80007a58
    if (slot <= 0xffffffff) {
        // 0x80007a5c
        badassert("slot>=0", "../../fs/sfs/sfs_dir.c", 69, "sfs_writedir");
        v1 = (int32_t)"slot>=0";
        v2 = (int32_t)"../../fs/sfs/sfs_dir.c";
        v3 = 69;
    }
    // 0x80007a7c
    return sfs_metaio((int32_t *)v1, 0, (char *)(64 * v2), v3, 64);
}