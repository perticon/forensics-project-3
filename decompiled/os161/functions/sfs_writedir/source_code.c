sfs_writedir(struct sfs_vnode *sv, int slot, struct sfs_direntry *sd)
{
	off_t actualpos;

	/* Compute the actual position in the directory. */
	KASSERT(slot>=0);
	actualpos = slot * sizeof(struct sfs_direntry);

	return sfs_metaio(sv, actualpos, sd, sizeof(*sd), UIO_WRITE);
}