static void link_list_init(int maxN)
{

    int i;
    link tmp;
    free_list_head = kmalloc(sizeof(struct STnode));
    free_list_tail = kmalloc(sizeof(struct STnode));
    free_list_head->next = free_list_tail;

    for (i = 0; i < maxN; i++)
    {
        tmp = kmalloc(sizeof(struct STnode));
        tmp->item.index = -1;
        add_free_link(tmp);
    }
}