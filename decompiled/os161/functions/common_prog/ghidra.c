int common_prog(int nargs,char **args)

{
  proc *proc;
  char *pcVar1;
  int result;
  
  proc = proc_create_runprogram(*args);
  if (proc == (proc *)0x0) {
    result = 3;
  }
  else {
    result = thread_fork(*args,proc,cmd_progthread,args,nargs);
    if (result == 0) {
      result = sys_waitpid(proc->p_pid,(userptr_t)&result,0);
      if (result == -1) {
        kprintf("waiting for process failed\n");
        result = 0;
      }
      else {
        kprintf("Process returned with exit value %d\n",result);
        result = 0;
      }
    }
    else {
      pcVar1 = strerror(result);
      kprintf("thread_fork failed: %s\n",pcVar1);
      proc_destroy(proc);
    }
  }
  return result;
}