int32_t common_prog(int32_t nargs, char ** args) {
    int32_t * v1 = proc_create_runprogram((char *)args); // 0x8000c76c
    if (v1 == NULL) {
        // 0x8000c814
        return 3;
    }
    int32_t v2 = thread_fork((char *)args, v1, (void (*)(char *, int32_t))((int32_t)&g2 - 0x3750), (char *)args, nargs); // 0x8000c790
    int32_t result = v2; // bp-24, 0x8000c798
    if (v2 != 0) {
        // 0x8000c79c
        kprintf("thread_fork failed: %s\n", strerror(v2));
        proc_destroy(v1);
        // 0x8000c814
        return result;
    }
    int32_t v3 = sys_waitpid(*(int32_t *)((int32_t)v1 + 28), &result, 0); // 0x8000c7d4
    result = v3;
    if (v3 == -1) {
        // 0x8000c7e4
        kprintf("waiting for process failed\n");
    } else {
        // 0x8000c7f8
        kprintf("Process returned with exit value %d\n", v3);
    }
    // 0x8000c814
    return 0;
}