common_prog(int nargs, char **args)
{
	struct proc *proc;
	int result;

	/* Create a process for the new program to run in. */
	proc = proc_create_runprogram(args[0] /* name */);
	if (proc == NULL)
	{
		return ENOMEM;
	}

	result = thread_fork(args[0] /* thread name */,
						 proc /* new process */,
						 cmd_progthread /* thread function */,
						 args /* thread arg */, nargs /* thread arg */);
	if (result)
	{
		kprintf("thread_fork failed: %s\n", strerror(result));
		proc_destroy(proc);
		return result;
	}

#if OPT_WAITPID
	/* wait for the newly created process */
	result = sys_waitpid(proc->p_pid, (userptr_t)&result, 0);

		if ((result) == -1)
	{
		kprintf("waiting for process failed\n");
		return 0;
	}

	kprintf("Process returned with exit value %d\n", result);
#endif
	/*
	 * The new process will be destroyed when the program exits...
	 * once you write the code for handling that.
	 */

	return 0;
}