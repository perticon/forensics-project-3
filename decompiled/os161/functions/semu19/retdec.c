int32_t semu19(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(0); // 0x800126dc
    int32_t v2 = thread_fork("semu19_sub", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x1ea4), (char *)v1, 0); // 0x80012700
    if (v2 != 0) {
        // 0x8001270c
        panic("semu19: whoops: thread_fork failed\n");
    }
    int32_t v3 = *v1; // 0x80012714
    if (strcmp((char *)v3, "some-silly-name") != 0) {
        // 0x80012734
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 753, "semu19");
    }
    int32_t v4 = (int32_t)v1; // 0x800126dc
    int32_t * v5 = (int32_t *)(v4 + 4); // 0x80012754
    int32_t * v6 = (int32_t *)(v4 + 8); // 0x8001275c
    if (!spinlock_not_held(v6)) {
        // 0x80012768
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 755, "semu19");
    }
    int32_t * v7 = (int32_t *)(v4 + 16); // 0x80012784
    if (*v7 != 0) {
        // 0x80012794
        badassert("sem->sem_count == 0", "../../test/semunit.c", 756, "semu19");
    }
    // 0x800127b0
    P(v1);
    int32_t v8 = v3; // 0x800127c4
    if (*v1 != v3) {
        // 0x800127c8
        badassert("name == sem->sem_name", "../../test/semunit.c", 761, "semu19");
        v8 = (int32_t)"name == sem->sem_name";
    }
    // 0x800127e8
    if (strcmp((char *)v8, "some-silly-name") != 0) {
        // 0x800127fc
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 762, "semu19");
    }
    // 0x80012818
    if (*v5 != *v5) {
        // 0x80012828
        badassert("wchan == sem->sem_wchan", "../../test/semunit.c", 763, "semu19");
    }
    // 0x80012844
    if (!spinlock_not_held(v6)) {
        // 0x80012854
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 764, "semu19");
    }
    int32_t result = 0; // 0x8001287c
    if (*v7 != 0) {
        // 0x80012880
        badassert("sem->sem_count == 0", "../../test/semunit.c", 765, "semu19");
        result = &g41;
    }
    // 0x800128a0
    return result;
}