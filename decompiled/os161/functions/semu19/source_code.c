semu19(int nargs, char **args)
{
	struct semaphore *sem;
	struct wchan *wchan;
	const char *name;
	int result;

	(void)nargs; (void)args;

	sem = makesem(0);
	result = thread_fork("semu19_sub", NULL, semu19_sub, sem, 0);
	if (result) {
		panic("semu19: whoops: thread_fork failed\n");
	}

	/* preconditions */
	name = sem->sem_name;
	KASSERT(!strcmp(name, NAMESTRING));
	wchan = sem->sem_wchan;
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 0);

	P(sem);

	/* postconditions */
	KASSERT(name == sem->sem_name);
	KASSERT(!strcmp(name, NAMESTRING));
	KASSERT(wchan == sem->sem_wchan);
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 0);

	return 0;
}