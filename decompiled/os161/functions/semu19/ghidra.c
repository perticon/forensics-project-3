int semu19(int nargs,char **args)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  char *a;
  wchan *pwVar3;
  
  sem = makesem(0);
  iVar1 = thread_fork("semu19_sub",(proc *)0x0,semu19_sub,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semu19: whoops: thread_fork failed\n");
  }
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2f1,"semu19");
  }
  pwVar3 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2f3,"semu19");
  }
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2f4,"semu19");
  }
  P(sem);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x2f9,"semu19");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2fa,"semu19");
  }
  if (sem->sem_wchan != pwVar3) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x2fb,"semu19");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2fc,"semu19");
  }
  iVar1 = 0;
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2fd,"semu19");
  }
  return iVar1;
}