attach_console_to_vfs(struct con_softc *cs)
{
	struct device *dev;
	int result;

	dev = kmalloc(sizeof(*dev));
	if (dev==NULL) {
		return ENOMEM;
	}

	dev->d_ops = &console_devops;
	dev->d_blocks = 0;
	dev->d_blocksize = 1;
	dev->d_data = cs;

	result = vfs_adddev("con", dev, 0);
	if (result) {
		kfree(dev);
		return result;
	}

	return 0;
}