int attach_console_to_vfs(con_softc *cs)

{
  device *dev;
  int iVar1;
  
  dev = (device *)kmalloc(0x14);
  if (dev == (device *)0x0) {
    iVar1 = 3;
  }
  else {
    dev->d_ops = &console_devops;
    dev->d_blocks = 0;
    dev->d_blocksize = 1;
    dev->d_data = cs;
    iVar1 = vfs_adddev("con",dev,0);
    if (iVar1 == 0) {
      iVar1 = 0;
    }
    else {
      kfree(dev);
    }
  }
  return iVar1;
}