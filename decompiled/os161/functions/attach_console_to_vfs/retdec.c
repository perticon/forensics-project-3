int32_t attach_console_to_vfs(int32_t * cs) {
    char * v1 = kmalloc(20); // 0x80001c78
    if (v1 == NULL) {
        // 0x80001cd8
        return 3;
    }
    int32_t v2 = (int32_t)v1; // 0x80001c78
    *(int32_t *)v1 = (int32_t)&console_devops;
    *(int32_t *)(v2 + 4) = 0;
    *(int32_t *)(v2 + 8) = 1;
    *(int32_t *)(v2 + 16) = (int32_t)cs;
    vfs_adddev("con", (int32_t *)v1, 0);
    int32_t result = 0; // 0x80001cb8
    if (result != 0) {
        // 0x80001cbc
        kfree(v1);
    }
    // 0x80001cd8
    return result;
}