docreatestress(const char *filesys)
{
	int i, err;

	init_threadsem();

	kprintf("*** Starting fs create stress test on %s:\n", filesys);

	for (i=0; i<NTHREADS; i++) {
		err = thread_fork("createstress", NULL,
				  createstress_thread, (char *)filesys, i);
		if (err) {
			panic("createstress: thread_fork failed %s\n",
			      strerror(err));
		}
	}

	for (i=0; i<NTHREADS; i++) {
		P(threadsem);
	}

	kprintf("*** fs create stress test done\n");
}