int32_t printfile(int32_t nargs, char ** args) {
    char buf[128]; // bp-184, 0x8001096c
    char outfile[16]; // bp-56, 0x8001096c
    // 0x8001096c
    if (nargs != 2) {
        // 0x8001099c
        kprintf("Usage: pf filename\n");
        // 0x80010bc8
        return 8;
    }
    // 0x800109b0
    strcpy(outfile, "con:");
    int32_t v1 = *(int32_t *)((int32_t)args + 4); // 0x800109c4
    int32_t * rv; // bp-232, 0x8001096c
    int32_t result = vfs_open((char *)v1, 0, 436, &rv); // 0x800109d4
    if (result != 0) {
        // 0x800109e0
        kprintf("printfile: %s\n", strerror(result));
        // 0x80010bc8
        return result;
    }
    // 0x80010a00
    int32_t * wv; // bp-228, 0x8001096c
    int32_t result2 = vfs_open(outfile, 1, 436, &wv); // 0x80010a10
    if (result2 != 0) {
        // 0x80010a1c
        kprintf("printfile: output: %s\n", strerror(result2));
        vfs_close(rv);
        // 0x80010bc8
        return result2;
    }
    bool v2 = true; // 0x8001096c
    while (true) {
        if (!v2) {
            // break (via goto) -> 0x80010bac
            goto lab_0x80010bac;
        }
        int64_t v3 = 0;
        int32_t iov; // bp-224, 0x8001096c
        int32_t ku; // bp-216, 0x8001096c
        uio_kinit(&iov, &ku, buf, 128, v3, 0);
        vnode_check(rv, "read");
        int32_t v4 = *(int32_t *)(*(int32_t *)((int32_t)rv + 20) + 12); // 0x80010a80
        if (v4 != 0) {
            // 0x80010a98
            kprintf("Read error: %s\n", strerror(v4));
            goto lab_0x80010bac;
        }
        // 0x80010ab8
        int32_t v5; // 0x8001096c
        uio_kinit(&iov, &ku, buf, 128 - v5, v3, 1);
        vnode_check(wv, "write");
        int32_t v6 = *(int32_t *)(*(int32_t *)((int32_t)wv + 20) + 24); // 0x80010b18
        if (v6 != 0) {
            // 0x80010b30
            kprintf("Write error: %s\n", strerror(v6));
            goto lab_0x80010bac;
        }
        // 0x80010ba4
        while (v5 == 0) {
            if (!v2) {
                // break (via goto) -> 0x80010bac
                goto lab_0x80010bac;
            }
            // 0x80010a48
            v3 = 0;
            uio_kinit(&iov, &ku, buf, 128, v3, 0);
            vnode_check(rv, "read");
            v4 = *(int32_t *)(*(int32_t *)((int32_t)rv + 20) + 12);
            if (v4 != 0) {
                // 0x80010a98
                kprintf("Read error: %s\n", strerror(v4));
                goto lab_0x80010bac;
            }
            // 0x80010ab8
            uio_kinit(&iov, &ku, buf, 128 - v5, v3, 1);
            vnode_check(wv, "write");
            v6 = *(int32_t *)(*(int32_t *)((int32_t)wv + 20) + 24);
            if (v6 != 0) {
                // 0x80010b30
                kprintf("Write error: %s\n", strerror(v6));
                goto lab_0x80010bac;
            }
        }
        // 0x80010b68
        kprintf("Warning: short write\n");
        v2 = false;
    }
  lab_0x80010bac:
    // 0x80010bac
    vfs_close(wv);
    vfs_close(rv);
    // 0x80010bc8
    return 0;
}