int printfile(int nargs,char **args)

{
  bool bVar1;
  int iVar2;
  char *pcVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  vnode *rv;
  vnode *wv;
  iovec iov;
  uio ku;
  char buf [128];
  char outfile [16];
  
  if (nargs == 2) {
    strcpy(outfile,"con:");
    iVar2 = vfs_open(args[1],0,0x1b4,&rv);
    if (iVar2 == 0) {
      iVar2 = vfs_open(outfile,1,0x1b4,&wv);
      if (iVar2 == 0) {
        bVar1 = false;
        uVar7 = 0;
        uVar6 = 0;
        uVar5 = 0;
        uVar4 = 0;
        while (!bVar1) {
          uio_kinit(&iov,&ku,buf,0x80,CONCAT44(uVar4,uVar5),UIO_READ);
          vnode_check(rv,"read");
          iVar2 = (*rv->vn_ops->vop_read)(rv,&ku);
          uVar5 = ku.uio_offset._4_4_;
          uVar4 = ku.uio_offset._0_4_;
          if (iVar2 != 0) {
            pcVar3 = strerror(iVar2);
            kprintf("Read error: %s\n",pcVar3);
            break;
          }
          if (ku.uio_resid != 0) {
            bVar1 = true;
          }
          uio_kinit(&iov,&ku,buf,0x80 - ku.uio_resid,CONCAT44(uVar6,uVar7),UIO_WRITE);
          vnode_check(wv,s_console_write_80022690 + 8);
          iVar2 = (*wv->vn_ops->vop_write)(wv,&ku);
          uVar7 = ku.uio_offset._4_4_;
          uVar6 = ku.uio_offset._0_4_;
          if (iVar2 != 0) {
            pcVar3 = strerror(iVar2);
            kprintf("Write error: %s\n",pcVar3);
            break;
          }
          if (ku.uio_resid != 0) {
            kprintf("Warning: short write\n");
          }
        }
        vfs_close(wv);
        vfs_close(rv);
        iVar2 = 0;
      }
      else {
        pcVar3 = strerror(iVar2);
        kprintf("printfile: output: %s\n",pcVar3);
        vfs_close(rv);
      }
    }
    else {
      pcVar3 = strerror(iVar2);
      kprintf("printfile: %s\n",pcVar3);
    }
  }
  else {
    kprintf("Usage: pf filename\n");
    iVar2 = 8;
  }
  return iVar2;
}