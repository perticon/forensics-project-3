printfile(int nargs, char **args)
{
	struct vnode *rv, *wv;
	struct iovec iov;
	struct uio ku;
	off_t rpos=0, wpos=0;
	char buf[128];
	char outfile[16];
	int result;
	int done=0;

	if (nargs != 2) {
		kprintf("Usage: pf filename\n");
		return EINVAL;
	}

	/* vfs_open destroys the string it's passed; make a copy */
	strcpy(outfile, "con:");

	result = vfs_open(args[1], O_RDONLY, 0664, &rv);
	if (result) {
		kprintf("printfile: %s\n", strerror(result));
		return result;
	}

	result = vfs_open(outfile, O_WRONLY, 0664, &wv);
	if (result) {
		kprintf("printfile: output: %s\n", strerror(result));
		vfs_close(rv);
		return result;
	}

	while (!done) {
		uio_kinit(&iov, &ku, buf, sizeof(buf), rpos, UIO_READ);
		result = VOP_READ(rv, &ku);
		if (result) {
			kprintf("Read error: %s\n", strerror(result));
			break;
		}
		rpos = ku.uio_offset;

		if (ku.uio_resid > 0) {
			done = 1;
		}

		uio_kinit(&iov, &ku, buf, sizeof(buf)-ku.uio_resid, wpos,
			  UIO_WRITE);
		result = VOP_WRITE(wv, &ku);
		if (result) {
			kprintf("Write error: %s\n", strerror(result));
			break;
		}
		wpos = ku.uio_offset;

		if (ku.uio_resid > 0) {
			kprintf("Warning: short write\n");
		}
	}

	vfs_close(wv);
	vfs_close(rv);

	return 0;
}