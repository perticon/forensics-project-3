bool spinlock_do_i_hold(int32_t * splk) {
    // 0x80015304
    int32_t v1; // 0x80015304
    if (v1 == 0) {
        // 0x8001532c
        return true;
    }
    int32_t v2 = *(int32_t *)*(int32_t *)(v1 + 80); // 0x80015318
    return *(int32_t *)((int32_t)splk + 4) == v2;
}