spinlock_do_i_hold(struct spinlock *splk)
{
	if (!CURCPU_EXISTS()) {
		return true;
	}

	/* Assume we can read splk_holder atomically enough for this to work */
	return (splk->splk_holder == curcpu->c_self);
}