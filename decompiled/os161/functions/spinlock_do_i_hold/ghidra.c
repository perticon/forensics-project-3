bool spinlock_do_i_hold(spinlock *splk)

{
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    return splk->splk_holder == **(cpu ***)(unaff_s7 + 0x50);
  }
  return true;
}