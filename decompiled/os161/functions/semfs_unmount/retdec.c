int32_t semfs_unmount(int32_t * fs) {
    int32_t v1 = (int32_t)fs;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x8000564c
    lock_acquire((int32_t *)*v2);
    lock_release((int32_t *)*v2);
    int32_t result = 27; // 0x8000566c
    if (*(int32_t *)(*(int32_t *)(v1 + 12) + 4) == 0) {
        // 0x80005684
        semfs_destroy(fs);
        result = 0;
    }
    // 0x8000569c
    return result;
}