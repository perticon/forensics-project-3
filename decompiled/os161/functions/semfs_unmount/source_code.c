semfs_unmount(struct fs *fs)
{
	struct semfs *semfs = fs->fs_data;

	lock_acquire(semfs->semfs_tablelock);
	if (vnodearray_num(semfs->semfs_vnodes) > 0) {
		lock_release(semfs->semfs_tablelock);
		return EBUSY;
	}

	lock_release(semfs->semfs_tablelock);
	semfs_destroy(semfs);

	return 0;
}