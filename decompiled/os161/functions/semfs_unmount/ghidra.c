int semfs_unmount(fs *fs)

{
  int iVar1;
  semfs *semfs;
  
  semfs = (semfs *)fs->fs_data;
  lock_acquire(semfs->semfs_tablelock);
  if ((semfs->semfs_vnodes->arr).num == 0) {
    lock_release(semfs->semfs_tablelock);
    semfs_destroy(semfs);
    iVar1 = 0;
  }
  else {
    lock_release(semfs->semfs_tablelock);
    iVar1 = 0x1b;
  }
  return iVar1;
}