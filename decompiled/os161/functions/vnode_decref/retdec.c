void vnode_decref(int32_t * vn) {
    if (vn == NULL) {
        // 0x8001a920
        badassert("vn != NULL", "../../vfs/vnode.c", 100, "vnode_decref");
    }
    // 0x8001a940
    spinlock_acquire((int32_t *)"= NULL");
    int32_t v1 = (int32_t)"= NULL"; // 0x8001a958
    if ("= NULL" <= NULL) {
        // 0x8001a95c
        badassert("vn->vn_refcount > 0", "../../vfs/vnode.c", 104, "vnode_decref");
        v1 = &g41;
    }
    if ((int32_t)"= NULL" >= 2) {
        // 0x8001a984
        *vn = v1 - 1;
        spinlock_release((int32_t *)"= NULL");
        // 0x8001a9f0
        return;
    }
    // 0x8001a994
    spinlock_release((int32_t *)"= NULL");
    vnode_check(vn, "reclaim");
    int32_t v2 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 8); // 0x8001a9b8
    switch (v2) {
        default: {
            // 0x8001a9d8
            kprintf("vfs: Warning: VOP_RECLAIM: %s\n", strerror(v2));
        }
        case 0: {
        }
        case 27: {
            // 0x8001a9f0
            return;
        }
    }
}