void vnode_decref(vnode *vn)

{
  int iVar1;
  uint uVar2;
  char *pcVar3;
  
  pcVar3 = (char *)vn;
  if (vn == (vnode *)0x0) {
    pcVar3 = s_vn____NULL_80027408;
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",100,"vnode_decref");
  }
  spinlock_acquire((spinlock *)((int)pcVar3 + 4));
  iVar1 = vn->vn_refcount;
  uVar2 = (uint)(iVar1 < 2);
  if (iVar1 < 1) {
    badassert("vn->vn_refcount > 0","../../vfs/vnode.c",0x68,"vnode_decref");
  }
  if (uVar2 == 0) {
    vn->vn_refcount = iVar1 + -1;
  }
  spinlock_release((spinlock *)((int)pcVar3 + 4));
  if (uVar2 != 0) {
    vnode_check(vn,"reclaim");
    iVar1 = (*vn->vn_ops->vop_reclaim)(vn);
    if ((iVar1 != 0) && (iVar1 != 0x1b)) {
      pcVar3 = strerror(iVar1);
      kprintf("vfs: Warning: VOP_RECLAIM: %s\n",pcVar3);
    }
  }
  return;
}