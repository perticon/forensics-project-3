vnode_decref(struct vnode *vn)
{
	bool destroy;
	int result;

	KASSERT(vn != NULL);

	spinlock_acquire(&vn->vn_countlock);

	KASSERT(vn->vn_refcount > 0);
	if (vn->vn_refcount > 1) {
		vn->vn_refcount--;
		destroy = false;
	}
	else {
		/* Don't decrement; pass the reference to VOP_RECLAIM. */
		destroy = true;
	}
	spinlock_release(&vn->vn_countlock);

	if (destroy) {
		result = VOP_RECLAIM(vn);
		if (result != 0 && result != EBUSY) {
			// XXX: lame.
			kprintf("vfs: Warning: VOP_RECLAIM: %s\n",
				strerror(result));
		}
	}
}