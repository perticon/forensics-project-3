int sfs_freemapio(sfs_fs *sfs,uio_rw rw)

{
  void *pvVar1;
  int iVar2;
  void *data;
  uint uVar3;
  uint32_t uVar4;
  
  uVar4 = (sfs->sfs_sb).sb_nblocks;
  pvVar1 = bitmap_getdata(sfs->sfs_freemap);
  uVar3 = 0;
  do {
    if (uVar4 + 0xfff >> 0xc <= uVar3) {
      return 0;
    }
    data = (void *)((int)pvVar1 + uVar3 * 0x200);
    if (rw == UIO_READ) {
      iVar2 = sfs_readblock(sfs,uVar3 + 2,data,0x200);
    }
    else {
      iVar2 = sfs_writeblock(sfs,uVar3 + 2,data,0x200);
    }
    uVar3 = uVar3 + 1;
  } while (iVar2 == 0);
  return iVar2;
}