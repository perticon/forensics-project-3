int32_t sfs_freemapio(int32_t * sfs, int32_t rw) {
    int32_t v1 = (int32_t)sfs;
    int32_t v2 = (int32_t)bitmap_getdata((int32_t *)*(int32_t *)(v1 + 532)); // 0x80007ff8
    int32_t v3 = 0; // 0x80008004
    int32_t result = 0; // 0x80008048
    while (v3 < (*(int32_t *)(v1 + 12) + 4095) / 0x1000) {
        int32_t v4 = v3 + 2;
        char * v5 = (char *)(512 * v3 + v2);
        int32_t v6; // 0x80007fc0
        if (rw == 0) {
            // 0x80008010
            v6 = sfs_readblock(sfs, v4, v5, 512);
        } else {
            // 0x80008028
            v6 = sfs_writeblock(sfs, v4, v5, 512);
        }
        // 0x80008038
        v3++;
        result = v6;
        if (v6 != 0) {
            // break -> 0x80008050
            break;
        }
        result = 0;
    }
    // 0x80008050
    return result;
}