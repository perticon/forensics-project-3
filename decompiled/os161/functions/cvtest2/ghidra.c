int cvtest2(int nargs,char **args)

{
  lock *plVar1;
  cv *pcVar2;
  int iVar3;
  uint uVar4;
  
  for (uVar4 = 0; uVar4 < 0xfa; uVar4 = uVar4 + 1) {
    plVar1 = lock_create("cvtest2 lock");
    testlocks[uVar4] = plVar1;
    pcVar2 = cv_create("cvtest2 cv");
    testcvs[uVar4] = pcVar2;
  }
  gatesem = sem_create("gatesem",0);
  exitsem = sem_create("exitsem",0);
  kprintf("cvtest2...\n");
  iVar3 = thread_fork("cvtest2",(proc *)0x0,sleepthread,(void *)0x0,0);
  if (iVar3 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("cvtest2: thread_fork failed\n");
  }
  iVar3 = thread_fork("cvtest2",(proc *)0x0,wakethread,(void *)0x0,0);
  if (iVar3 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("cvtest2: thread_fork failed\n");
  }
  P(exitsem);
  P(exitsem);
  sem_destroy(exitsem);
  sem_destroy(gatesem);
  gatesem = (semaphore *)0x0;
  exitsem = (semaphore *)0x0;
  for (uVar4 = 0; uVar4 < 0xfa; uVar4 = uVar4 + 1) {
    lock_destroy(testlocks[uVar4]);
    cv_destroy(testcvs[uVar4]);
    testlocks[uVar4] = (lock *)0x0;
    testcvs[uVar4] = (cv *)0x0;
  }
  kprintf("cvtest2 done\n");
  return 0;
}