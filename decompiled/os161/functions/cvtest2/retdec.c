int32_t cvtest2(int32_t nargs, char ** args) {
    for (int32_t i = 0; i < 250; i++) {
        int32_t * v1 = lock_create("cvtest2 lock"); // 0x800133c0
        int32_t v2 = 4 * i; // 0x800133c4
        *(int32_t *)(v2 + (int32_t)&testlocks) = (int32_t)v1;
        int32_t * v3 = cv_create("cvtest2 cv"); // 0x800133d4
        *(int32_t *)(v2 + (int32_t)&testcvs) = (int32_t)v3;
    }
    int32_t * v4 = sem_create("gatesem", 0); // 0x800133f8
    *(int32_t *)&gatesem = (int32_t)v4;
    int32_t * v5 = sem_create("exitsem", 0); // 0x80013410
    *(int32_t *)&exitsem = (int32_t)v5;
    kprintf("cvtest2...\n");
    int32_t v6 = thread_fork("cvtest2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2d2c), NULL, 0); // 0x80013444
    if (v6 != 0) {
        // 0x80013450
        panic("cvtest2: thread_fork failed\n");
    }
    int32_t v7 = thread_fork("cvtest2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2fc0), NULL, 0); // 0x80013474
    if (v7 != 0) {
        // 0x80013480
        panic("cvtest2: thread_fork failed\n");
    }
    // 0x80013488
    P(exitsem);
    P(exitsem);
    sem_destroy(exitsem);
    sem_destroy(gatesem);
    *(int32_t *)&gatesem = 0;
    *(int32_t *)&exitsem = 0;
    int32_t v8 = 0; // 0x80013378
    int32_t v9 = 0; // 0x800134e8
    int32_t * v10 = (int32_t *)(v8 + (int32_t)&testlocks); // 0x800134d4
    lock_destroy((int32_t *)*v10);
    int32_t * v11 = (int32_t *)(v8 + (int32_t)&testcvs); // 0x800134e0
    v9++;
    cv_destroy((int32_t *)*v11);
    *v10 = 0;
    *v11 = 0;
    v8 = 4 * v9;
    while (v9 < 250) {
        // 0x800134d0
        v10 = (int32_t *)(v8 + (int32_t)&testlocks);
        lock_destroy((int32_t *)*v10);
        v11 = (int32_t *)(v8 + (int32_t)&testcvs);
        v9++;
        cv_destroy((int32_t *)*v11);
        *v10 = 0;
        *v11 = 0;
        v8 = 4 * v9;
    }
    // 0x80013500
    kprintf("cvtest2 done\n");
    return 0;
}