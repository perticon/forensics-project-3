int is_prime(int n)

{
  int iVar1;
  int iVar2;
  
  iVar1 = 2;
  do {
    if (iVar1 == 0) {
      trap(0x1c00);
    }
    if (n / iVar1 < iVar1) {
      return 1;
    }
    iVar2 = n % iVar1;
    if (iVar1 == 0) {
      trap(0x1c00);
    }
    iVar1 = iVar1 + 1;
  } while (iVar2 != 0);
  return 0;
}