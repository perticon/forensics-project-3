int32_t is_prime(int32_t n) {
    int32_t v1 = 2;
    int32_t result = 1; // 0x8000a048
    while (n / v1 >= v1) {
        // 0x8000a018
        result = 0;
        if (n % v1 == 0) {
            // break -> 0x8000a04c
            break;
        }
        v1++;
        result = 1;
    }
    // 0x8000a04c
    return result;
}