int32_t TestBit(int32_t * A, int32_t k) {
    int32_t v1 = k + 31;
    int32_t v2 = *(int32_t *)(4 * ((k > -1 ? k : v1) >> 5) + (int32_t)A); // 0x8001b26c
    int32_t v3 = k & -0x7fffffe1; // 0x8001b278
    return (v2 & 1 << (v3 > -1 ? v3 : (v1 | -32) + 1)) != 0;
}