int TestBit(int *A,int k)

{
  int iVar1;
  uint uVar2;
  
  iVar1 = k;
  if (k < 0) {
    iVar1 = k + 0x1f;
  }
  uVar2 = A[iVar1 >> 5] >> (k & 0x1fU) & 1;
  if (uVar2 != 0) {
    uVar2 = 1;
  }
  return uVar2;
}