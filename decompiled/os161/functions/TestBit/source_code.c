static int TestBit(int *A, int k)
{
    if ((A[k / 32] & (1 << (k % 32))) != 0)
        return 1;
    else
        return 0;
}