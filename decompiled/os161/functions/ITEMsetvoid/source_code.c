Item ITEMsetvoid(void) {  
  Item tmp = (Item) kmalloc(sizeof(struct item));
  KASSERT(tmp != NULL);
  tmp->key.kpid = -1;
  tmp->key.kaddr = -1;
  tmp->index = -2;
  return tmp;
}