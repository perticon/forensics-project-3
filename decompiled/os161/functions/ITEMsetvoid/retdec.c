int32_t * ITEMsetvoid(void) {
    char * v1 = kmalloc(12); // 0x80009f10
    int32_t v2 = (int32_t)v1; // 0x80009f18
    if (v1 == NULL) {
        // 0x80009f1c
        badassert("tmp != NULL", "../../hash/item.c", 59, "ITEMsetvoid");
        v2 = &g41;
    }
    int32_t * result = (int32_t *)v2; // 0x80009f3c
    *result = -1;
    *(int32_t *)(v2 + 4) = -1;
    *(int32_t *)(v2 + 8) = -2;
    return result;
}