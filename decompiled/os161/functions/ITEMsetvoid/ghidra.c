Item ITEMsetvoid(void)

{
  Item piVar1;
  vaddr_t vVar2;
  
  piVar1 = (Item)kmalloc(0xc);
  vVar2 = 0xffffffff;
  if (piVar1 == (Item)0x0) {
    badassert("tmp != NULL","../../hash/item.c",0x3b,"ITEMsetvoid");
  }
  (piVar1->key).kpid = vVar2;
  (piVar1->key).kaddr = vVar2;
  piVar1->index = -2;
  return piVar1;
}