putch_polled(struct con_softc *cs, int ch)
{
	cs->cs_sendpolled(cs->cs_devdata, ch);
}