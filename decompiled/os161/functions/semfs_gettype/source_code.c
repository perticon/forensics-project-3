semfs_gettype(struct vnode *vn, mode_t *ret)
{
	struct semfs_vnode *semv = vn->vn_data;

	*ret = semv->semv_semnum == SEMFS_ROOTDIR ? S_IFDIR : S_IFREG;
	return 0;
}