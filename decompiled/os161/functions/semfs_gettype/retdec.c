int32_t semfs_gettype(int32_t * vn, int32_t * ret) {
    int32_t v1 = *(int32_t *)(*(int32_t *)((int32_t)vn + 16) + 28); // 0x8000602c
    *ret = v1 == -1 ? 0x2000 : 0x1000;
    return 0;
}