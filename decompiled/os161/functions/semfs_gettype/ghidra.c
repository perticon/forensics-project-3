int semfs_gettype(vnode *vn,mode_t *ret)

{
  mode_t mVar1;
  
  if (*(int *)((int)vn->vn_data + 0x1c) == -1) {
    mVar1 = 0x2000;
  }
  else {
    mVar1 = 0x1000;
  }
  *ret = mVar1;
  return 0;
}