sleepalot_thread(void *junk, unsigned long num)
{
	int i, j;

	(void)junk;

	for (i=0; i<SLEEPALOT_PRINTS; i++) {
		for (j=0; j<SLEEPALOT_ITERS; j++) {
			unsigned n;
			struct spinlock *lk;
			struct wchan *wc;

			n = random() % NWAITCHANS;
			lk = &spinlocks[n];
			wc = waitchans[n];
			spinlock_acquire(lk);
			wchan_sleep(wc, lk);
			spinlock_release(lk);
		}
		kprintf("[%lu]", num);
	}
	V(donesem);
}