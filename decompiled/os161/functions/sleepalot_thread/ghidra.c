void sleepalot_thread(void *junk,ulong num)

{
  bool bVar1;
  uint uVar2;
  spinlock *splk;
  int iVar3;
  wchan *wc;
  int iVar4;
  
  for (iVar4 = 0; iVar3 = 0, iVar4 < 0x14; iVar4 = iVar4 + 1) {
    bVar1 = true;
    while (bVar1) {
      iVar3 = iVar3 + 1;
      uVar2 = random();
      splk = spinlocks + uVar2 % 0xc;
      wc = waitchans[uVar2 % 0xc];
      spinlock_acquire(splk);
      wchan_sleep(wc,splk);
      spinlock_release(splk);
      bVar1 = iVar3 < 4;
    }
    kprintf("[%lu]",num);
  }
  V(donesem);
  return;
}