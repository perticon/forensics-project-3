int semfs_write(vnode *vn,uio *uio)

{
  semfs_sem *sem;
  uint uVar1;
  uint uVar2;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  sem = semfs_getsem(semv);
  lock_acquire(sem->sems_lock);
  while( true ) {
    if (uio->uio_resid == 0) {
      lock_release(sem->sems_lock);
      return 0;
    }
    uVar1 = sem->sems_count;
    uVar2 = uio->uio_resid + uVar1;
    if (uVar2 < uVar1) break;
    if ((dbflags & 0x100) != 0) {
      kprintf("semfs: sem%u: V, count %u -> %u\n",semv->semv_semnum,uVar1,uVar2);
    }
    semfs_wakeup(sem,uVar2);
    sem->sems_count = uVar2;
    uVar2 = *(uint *)((int)&uio->uio_offset + 4);
    uVar1 = uVar2 + uio->uio_resid;
    *(uint *)&uio->uio_offset = (uint)(uVar1 < uVar2) + *(int *)&uio->uio_offset;
    *(uint *)((int)&uio->uio_offset + 4) = uVar1;
    uio->uio_resid = 0;
  }
  lock_release(sem->sems_lock);
  return 0x26;
}