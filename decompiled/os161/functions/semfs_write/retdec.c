int32_t semfs_write(int32_t * vn, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)((int32_t)vn + 16); // 0x80006428
    int32_t * v3 = semfs_getsem((int32_t *)v2); // 0x80006430
    lock_acquire((int32_t *)*v3);
    int32_t * v4 = (int32_t *)(v1 + 16); // 0x800064d8
    int32_t v5 = *v4; // 0x800064d8
    if (v5 == 0) {
        // 0x800064e8
        lock_release((int32_t *)*v3);
        // 0x800064f8
        return 0;
    }
    int32_t * v6 = (int32_t *)((int32_t)v3 + 8); // 0x80006450
    uint32_t v7 = *v6; // 0x80006450
    uint32_t v8 = v7 + v5; // 0x80006458
    if (v8 < v7) {
        // 0x80006468
        lock_release((int32_t *)*v3);
        // 0x800064f8
        return 38;
    }
    // 0x8000647c
    if ((dbflags & 256) != 0) {
        // 0x80006490
        kprintf("semfs: sem%u: V, count %u -> %u\n", *(int32_t *)(v2 + 28), v7, v8);
    }
    int32_t * v9 = (int32_t *)(v1 + 8);
    int32_t * v10 = (int32_t *)(v1 + 12);
    semfs_wakeup(v3, v8);
    *v6 = v8;
    uint32_t v11 = *v10; // 0x800064b8
    uint32_t v12 = v11 + *v4; // 0x800064c0
    *v9 = *v9 + (int32_t)(v12 < v11);
    *v10 = v12;
    *v4 = 0;
    // 0x800064e8
    lock_release((int32_t *)*v3);
    // 0x800064f8
    return 0;
}