semfs_write(struct vnode *vn, struct uio *uio)
{
	struct semfs_vnode *semv = vn->vn_data;
	struct semfs_sem *sem;
	unsigned newcount;

	sem = semfs_getsem(semv);

	lock_acquire(sem->sems_lock);
	while (uio->uio_resid > 0) {
		newcount = sem->sems_count + uio->uio_resid;
		if (newcount < sem->sems_count) {
			/* overflow */
			lock_release(sem->sems_lock);
			return EFBIG;
		}
		DEBUG(DB_SEMFS, "semfs: sem%u: V, count %u -> %u\n",
		      semv->semv_semnum, sem->sems_count, newcount);
		semfs_wakeup(sem, newcount);
		sem->sems_count = newcount;
		uio->uio_offset += uio->uio_resid;
		uio->uio_resid = 0;
	}
	lock_release(sem->sems_lock);
	return 0;
}