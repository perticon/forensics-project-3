vopfail_link_nosys(struct vnode *dir, const char *name, struct vnode *file)
{
	(void)dir;
	(void)name;
	(void)file;
	return ENOSYS;
}