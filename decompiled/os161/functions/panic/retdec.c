void panic(char * fmt, ...) {
    int32_t v1 = g15; // 0x8000b478
    if (g15 == 0) {
        // 0x8000b47c
        g15 = 1;
        splx(1);
        v1 = g15;
    }
    int32_t v2 = v1; // 0x8000b49c
    if (v1 == 1) {
        // 0x8000b4a0
        g15 = 2;
        thread_panic();
        v2 = g15;
    }
    int32_t v3 = v2; // 0x8000b4c0
    if (v2 == 2) {
        // 0x8000b4c4
        g15 = 3;
        kprintf("panic: ");
        int32_t v4; // 0x8000b44c
        __vprintf((void (*)(char *, char *, int32_t))((int32_t)&g2 - 0x4d20), NULL, fmt, (char *)&v4);
        v3 = g15;
    }
    int32_t v5 = v3; // 0x8000b504
    if (v3 == 3) {
        // 0x8000b508
        g15 = 4;
        ltrace_stop(0);
        v5 = g15;
    }
    int32_t v6 = v5; // 0x8000b528
    if (v5 == 4) {
        // 0x8000b52c
        g15 = 5;
        vfs_sync();
        v6 = g15;
    }
    // 0x8000b540
    if (v6 == 5) {
        // 0x8000b550
        g15 = 6;
        mainbus_panic();
    }
    while (true) {
        // continue -> 0x8000b560
    }
}