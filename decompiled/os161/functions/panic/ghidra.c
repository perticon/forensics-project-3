void panic(char *fmt,...)

{
  undefined4 in_a1;
  undefined4 in_a2;
  undefined4 in_a3;
  undefined4 local_res4;
  undefined4 local_res8;
  undefined4 local_resc;
  va_list ap;
  
  local_res4 = in_a1;
  local_res8 = in_a2;
  local_resc = in_a3;
  if (panic::evil == 0) {
    panic::evil = 1;
    splx(1);
  }
  if (panic::evil == 1) {
    panic::evil = 2;
    thread_panic();
  }
  if (panic::evil == 2) {
    panic::evil = 3;
    kprintf("panic: ");
    __vprintf(console_send,(void *)0x0,fmt,&local_res4);
  }
  if (panic::evil == 3) {
    panic::evil = 4;
    ltrace_stop(0);
  }
  if (panic::evil == 4) {
    panic::evil = 5;
    vfs_sync();
  }
  if (panic::evil == 5) {
    panic::evil = 6;
    mainbus_panic();
  }
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}