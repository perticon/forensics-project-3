panic(const char *fmt, ...)
{
	va_list ap;

	/*
	 * When we reach panic, the system is usually fairly screwed up.
	 * It's not entirely uncommon for anything else we try to do
	 * here to trigger more panics.
	 *
	 * This variable makes sure that if we try to do something here,
	 * and it causes another panic, *that* panic doesn't try again;
	 * trying again almost inevitably causes infinite recursion.
	 *
	 * This is not excessively paranoid - these things DO happen!
	 */
	static volatile int evil;

	if (evil == 0) {
		evil = 1;

		/*
		 * Not only do we not want to be interrupted while
		 * panicking, but we also want the console to be
		 * printing in polling mode so as not to do context
		 * switches. So turn interrupts off on this CPU.
		 */
		splhigh();
	}

	if (evil == 1) {
		evil = 2;

		/* Kill off other threads and halt other CPUs. */
		thread_panic();
	}

	if (evil == 2) {
		evil = 3;

		/* Print the message. */
		kprintf("panic: ");
		va_start(ap, fmt);
		__vprintf(console_send, NULL, fmt, ap);
		va_end(ap);
	}

	if (evil == 3) {
		evil = 4;

		/* Drop to the debugger. */
		ltrace_stop(0);
	}

	if (evil == 4) {
		evil = 5;

		/* Try to sync the disks. */
		vfs_sync();
	}

	if (evil == 5) {
		evil = 6;

		/* Shut down or reboot the system. */
		mainbus_panic();
	}

	/*
	 * Last resort, just in case.
	 */

	for (;;);
}