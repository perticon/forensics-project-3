int sfs_dir_findname(sfs_vnode *sv,char *name,uint32_t *ino,int *slot,int *emptyslot)

{
  bool bVar1;
  int iVar2;
  int iVar3;
  int slot_00;
  sfs_direntry tsd;
  
  iVar2 = sfs_dir_nentries(sv);
  slot_00 = 0;
  bVar1 = false;
  while( true ) {
    if (iVar2 <= slot_00) {
      iVar2 = 0;
      if (!bVar1) {
        iVar2 = 0x13;
      }
      return iVar2;
    }
    iVar3 = sfs_readdir(sv,slot_00,&tsd);
    if (iVar3 != 0) break;
    if (tsd.sfd_ino == 0) {
      if (emptyslot != (int *)0x0) {
        *emptyslot = slot_00;
      }
    }
    else {
      tsd.sfd_name[59] = '\0';
      iVar3 = strcmp(tsd.sfd_name,name);
      if (iVar3 == 0) {
        if (bVar1) {
          badassert("found==0","../../fs/sfs/sfs_dir.c",0x85,"sfs_dir_findname");
        }
        if (slot != (int *)0x0) {
          *slot = slot_00;
        }
        bVar1 = true;
        if (ino != (uint32_t *)0x0) {
          *ino = tsd.sfd_ino;
        }
      }
    }
    slot_00 = slot_00 + 1;
  }
  return iVar3;
}