int32_t sfs_dir_findname(int32_t * sv, char * name, int32_t * ino, int32_t * slot, int32_t * emptyslot) {
    uint32_t v1 = sfs_dir_nentries(sv); // 0x80007ae8
    if (v1 <= 0) {
      lab_0x80007b98_2:
        // 0x80007ba4
        return 19;
    }
    int32_t v2 = 0;
    int32_t v3 = 0;
    int32_t tsd; // bp-104, 0x80007aac
    int32_t v4 = sfs_readdir(sv, v3, &tsd); // 0x80007b04
    int32_t result = v4; // 0x80007b0c
    while (v4 == 0) {
        int32_t v5 = v2;
        int32_t v6; // 0x80007aac
        if (tsd == 0) {
            // 0x80007b20
            v6 = v5;
            if (emptyslot != NULL) {
                // 0x80007b28
                *emptyslot = v3;
                v6 = v5;
            }
        } else {
            // 0x80007b30
            int32_t v7; // bp-100, 0x80007aac
            int32_t v8 = strcmp((char *)&v7, name); // 0x80007b38
            v6 = v5;
            if (v8 == 0) {
                if (v5 != 0) {
                    // 0x80007b4c
                    badassert("found==0", "../../fs/sfs/sfs_dir.c", 133, "sfs_dir_findname");
                }
                if (slot != NULL) {
                    // 0x80007b70
                    *slot = v3;
                }
                // 0x80007b74
                v6 = 1;
                if (ino != NULL) {
                    // 0x80007b7c
                    *ino = tsd;
                    v6 = 1;
                }
            }
        }
        // 0x80007b88
        v2 = v6;
        int32_t v9 = v3 + 1; // 0x80007b88
        if (v9 >= v1) {
            // 0x80007b98
            result = 0;
            if (v2 == 0) {
                return 19;
            } else {
                return result;
            }
        }
        v3 = v9;
        v4 = sfs_readdir(sv, v3, &tsd);
        result = v4;
    }
  lab_0x80007ba4:
    // 0x80007ba4
    return result;
}