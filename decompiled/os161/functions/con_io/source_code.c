con_io(struct device *dev, struct uio *uio)
{
	int result;
	char ch;
	struct lock *lk;

	(void)dev;  // unused

	if (uio->uio_rw==UIO_READ) {
		lk = con_userlock_read;
	}
	else {
		lk = con_userlock_write;
	}

	KASSERT(lk != NULL);
	lock_acquire(lk);

	while (uio->uio_resid > 0) {
		if (uio->uio_rw==UIO_READ) {
			ch = getch();
			if (ch=='\r') {
				ch = '\n';
			}
			result = uiomove(&ch, 1, uio);
			if (result) {
				lock_release(lk);
				return result;
			}
			if (ch=='\n') {
				break;
			}
		}
		else {
			result = uiomove(&ch, 1, uio);
			if (result) {
				lock_release(lk);
				return result;
			}
			if (ch=='\n') {
				putch('\r');
			}
			putch(ch);
		}
	}
	lock_release(lk);
	return 0;
}