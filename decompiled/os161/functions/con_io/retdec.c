int32_t con_io(int32_t * dev, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t * v2 = (int32_t *)(v1 + 24); // 0x80001f00
    int32_t * v3 = *v2 == 0 ? con_userlock_read : con_userlock_write;
    if (v3 == NULL) {
        // 0x80001f34
        badassert("lk != NULL", "../../dev/generic/console.c", 260, "con_io");
    }
    // 0x80001f50
    lock_acquire(v3);
    int32_t * v4 = (int32_t *)(v1 + 16); // 0x8000201c
    if (*v4 == 0) {
        // 0x8000202c
        lock_release(v3);
        // 0x80002038
        return 0;
    }
    int32_t v5; // 0x80001fa0
    int32_t result; // 0x80001fdc
    while (true) {
        // 0x80001f64
        char ch; // bp-32, 0x80001ee4
        if (*v2 == 0) {
            uint32_t v6 = getch(); // 0x80001f78
            ch = v6 % 256 == 13 ? 10 : (char)v6;
            v5 = uiomove(&ch, 1, uio);
            if (v5 != 0) {
                // break -> 0x80001fac
                break;
            }
            // 0x80001fbc
            if (ch == 10) {
                // 0x8000202c
                lock_release(v3);
                // 0x80002038
                return 0;
            }
        } else {
            // 0x80001fd4
            result = uiomove(&ch, 1, uio);
            if (result != 0) {
                // 0x80001fe8
                lock_release(v3);
                return result;
            }
            char v7 = ch; // 0x80002004
            if (ch == 10) {
                // 0x80002008
                putch(13);
                v7 = ch;
            }
            // 0x80002010
            putch((int32_t)v7);
        }
        // 0x8000201c
        if (*v4 == 0) {
            // 0x8000202c
            lock_release(v3);
            // 0x80002038
            return 0;
        }
    }
    // 0x80001fac
    lock_release(v3);
    result = v5;
  lab_0x80002038:
    // 0x80002038
    return result;
}