int con_io(device *dev,uio *uio)

{
  int iVar1;
  lock *lock;
  char ch;
  
  lock = con_userlock_write;
  if (uio->uio_rw == UIO_READ) {
    lock = con_userlock_read;
  }
  if (lock == (lock *)0x0) {
    badassert("lk != NULL","../../dev/generic/console.c",0x104,"con_io");
  }
  lock_acquire(lock);
  do {
    while( true ) {
      if (uio->uio_resid == 0) goto LAB_8000202c;
      if (uio->uio_rw == UIO_READ) break;
      iVar1 = uiomove(&ch,1,uio);
      if (iVar1 != 0) {
        lock_release(lock);
        return iVar1;
      }
      if (ch == '\n') {
        putch(0xd);
      }
      putch((int)ch);
    }
    iVar1 = getch();
    ch = (char)iVar1;
    if (ch == '\r') {
      ch = '\n';
    }
    iVar1 = uiomove(&ch,1,uio);
    if (iVar1 != 0) {
      lock_release(lock);
      return iVar1;
    }
  } while (ch != '\n');
LAB_8000202c:
  lock_release(lock);
  return 0;
}