void cpu_identify(char * buf, int32_t max) {
    // 0x8001f27c
    int32_t v1; // 0x8001f27c
    __asm_mfc0(v1, v1, 0);
    switch (v1) {
        case 161: {
            // 0x8001f2b0
            __asm_mfc0(161, v1, 1);
            snprintf(buf, max, "MIPS/161 (System/161 2.x) features 0x%x", 161);
            __asm_mfc0(max, v1, 2);
            if (max != 0) {
                // 0x8001f2d0
                kprintf("WARNING: unknown CPU incompatible features 0x%x\n", max);
            }
            // break -> 0x8001f2ec
            break;
        }
        case 1023: {
            // 0x8001f29c
            snprintf(buf, max, "MIPS/161 (System/161 1.x and pre-2.x)");
            // 0x8001f2ec
            return;
        }
        default: {
            // 0x8001f2e0
            snprintf(buf, max, "32-bit MIPS (unknown type, CPU ID 0x%x)", v1);
            // 0x8001f2ec
            return;
        }
    }
}