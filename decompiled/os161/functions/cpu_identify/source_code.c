cpu_identify(char *buf, size_t max)
{
	uint32_t prid;
	uint32_t features;

	prid = cpu_getprid();
	switch (prid) {
	    case SYS161_PRID_ORIG:
		snprintf(buf, max, "MIPS/161 (System/161 1.x and pre-2.x)");
		break;
	    case SYS161_PRID_2X:
		features = cpu_getfeatures();
		snprintf(buf, max, "MIPS/161 (System/161 2.x) features 0x%x",
			 features);
		features = cpu_getifeatures();
		if (features != 0) {
			kprintf("WARNING: unknown CPU incompatible features "
				"0x%x\n", features);
		}
		break;
	    default:
		snprintf(buf, max, "32-bit MIPS (unknown type, CPU ID 0x%x)",
			 prid);
		break;
	}
}