void cpu_identify(char *buf,size_t max)

{
  if (PRId == 0xa1) {
    snprintf(buf,max,"MIPS/161 (System/161 2.x) features 0x%x",EBase);
    if (cop0_reg15_2 != 0) {
      kprintf("WARNING: unknown CPU incompatible features 0x%x\n");
    }
  }
  else if (PRId == 0x3ff) {
    snprintf(buf,max,"MIPS/161 (System/161 1.x and pre-2.x)");
  }
  else {
    snprintf(buf,max,"32-bit MIPS (unknown type, CPU ID 0x%x)");
  }
  return;
}