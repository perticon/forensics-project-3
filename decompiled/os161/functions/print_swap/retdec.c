void print_swap(void) {
    // 0x8001de28
    spinlock_acquire(&swap_lock);
    kprintf("<< SWAP TABLE >>\n");
    for (int32_t i = 0; i < 2304; i++) {
        int32_t v1 = 8 * i; // 0x8001de94
        uint32_t v2 = *(int32_t *)(v1 + (int32_t)&swap_table + 4); // 0x8001de70
        int32_t v3 = *(int32_t *)(v1 + (int32_t)&swap_table); // 0x8001de7c
        kprintf("%d -   %d   - %d\n", i, v3, v2 / 0x1000);
    }
    // 0x8001de98
    spinlock_release(&swap_lock);
}