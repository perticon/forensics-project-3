void print_swap(void)
{

    spinlock_acquire(&swap_lock);

    kprintf("<< SWAP TABLE >>\n");

    for (int i = 0; i < ENTRIES; i++)
    {
        kprintf("%d -   %d   - %d\n", i, swap_table[i].pid, swap_table[i].page / PAGE_SIZE);
    }

    spinlock_release(&swap_lock);
}