void syscall(trapframe *tf)

{
  char *pcVar1;
  char *whence;
  uint32_t uVar2;
  int unaff_s7;
  int32_t retval;
  int err;
  
  if (unaff_s7 == 0) {
    pcVar1 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
    tf = (trapframe *)pcVar1;
  }
  pcVar1 = (char *)tf;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    pcVar1 = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    pcVar1 = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar2 = *(uint32_t *)((int)pcVar1 + 0x1c);
  retval = 0;
  switch(uVar2) {
  case 0:
    err = sys_fork((trapframe *)pcVar1,&retval);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar2);
    if (uVar2 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    err = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)pcVar1 + 0x24));
    err = 0;
    break;
  case 4:
    retval = sys_waitpid(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 5:
    retval = sys_getpid();
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x2d:
    retval = sys_open(*(userptr_t *)((int)pcVar1 + 0x24),tf->tf_a1,tf->tf_a2,&err);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x31:
    retval = sys_close(*(uint32_t *)((int)pcVar1 + 0x24));
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x32:
    retval = sys_read(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x37:
    retval = sys_write(*(uint32_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1,tf->tf_a2);
    if (retval < 0) {
      err = 1;
    }
    else {
      err = 0;
    }
    break;
  case 0x3b:
    err = sys_lseek(*(uint32_t *)((int)pcVar1 + 0x24),CONCAT44(tf->tf_a2,&retval),(int)whence,
                    (int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    err = sys___time(*(userptr_t *)((int)pcVar1 + 0x24),(userptr_t)tf->tf_a1);
    break;
  case 0x77:
    err = sys_reboot(*(uint32_t *)((int)pcVar1 + 0x24));
  }
  if (err == 0) {
    tf->tf_v0 = retval;
    tf->tf_a3 = 0;
  }
  else {
    tf->tf_v0 = err;
    tf->tf_a3 = 1;
  }
  tf->tf_epc = tf->tf_epc + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}