void syscall(struct trapframe *tf)
{
	int callno;
	int32_t retval;
	int err;

	KASSERT(curthread != NULL);
	KASSERT(curthread->t_curspl == 0);
	KASSERT(curthread->t_iplhigh_count == 0);

	callno = tf->tf_v0;

	/*
	 * Initialize retval to 0. Many of the system calls don't
	 * really return a value, just 0 for success and -1 on
	 * error. Since retval is the value returned on success,
	 * initialize it to 0 by default; thus it's not necessary to
	 * deal with it except for calls that return other values,
	 * like write.
	 */

	retval = 0;

	switch (callno)
	{
	case SYS_reboot:
		err = sys_reboot(tf->tf_a0);
		break;

	case SYS___time:
		err = sys___time((userptr_t)tf->tf_a0,
						 (userptr_t)tf->tf_a1);
		break;

#if OPT_WAITPID

	case SYS_waitpid:
		retval = sys_waitpid((pid_t)tf->tf_a0,
							 (userptr_t)tf->tf_a1,
							 (int)tf->tf_a2);
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;
	case SYS_getpid:
		retval = sys_getpid();
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;

#endif

#if OPT_PAGING
	case SYS_open:
		retval = sys_open((userptr_t)tf->tf_a0,
						  (int)tf->tf_a1,
						  (mode_t)tf->tf_a2, &err);
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;

	case SYS_close:
		retval = sys_close((int)tf->tf_a0);
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;


#if OPT_FORK
	    case SYS_fork:
	        err = sys_fork(tf,&retval);
                break;
#endif

	case SYS_remove:
		/* just ignore: do nothing */
		retval = 0;
		break;

	case SYS_lseek:
		err = sys_lseek((int)tf->tf_a0, (off_t)tf->tf_a1, (int)tf->tf_a2, &retval);
		break;

	case SYS_write:
		retval = sys_write((int)tf->tf_a0,
						   (userptr_t)tf->tf_a1,
						   (size_t)tf->tf_a2);
		/* error: function not implemented */
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;

	case SYS_read:
		retval = sys_read((int)tf->tf_a0,
						  (userptr_t)tf->tf_a1,
						  (size_t)tf->tf_a2);
		/* error: function not implemented */
		if (retval < 0)
			err = ENOSYS;
		else
			err = 0;
		break;

	case SYS__exit:
		/* TODO: just avoid crash */
		sys__exit((int)tf->tf_a0);
		err = 0;
		break;
#endif

	default:
		kprintf("Unknown syscall %d\n", callno);
		if (callno == 3)
		{
			kprintf("Unknown syscall %d\n", callno);
		}
		err = ENOSYS;
		break;
	}

	if (err)
	{
		/*
		 * Return the error code. This gets converted at
		 * userlevel to a return value of -1 and the error
		 * code in errno.
		 */
		tf->tf_v0 = err;
		tf->tf_a3 = 1; /* signal an error */
	}
	else
	{
		/* Success. */
		tf->tf_v0 = retval;
		tf->tf_a3 = 0; /* signal no error */
	}

	/*
	 * Now, advance the program counter, to avoid restarting
	 * the syscall over and over again.
	 */

	tf->tf_epc += 4;

	/* Make sure the syscall code didn't forget to lower spl */
	KASSERT(curthread->t_curspl == 0);
	/* ...or leak any spinlocks */
	KASSERT(curthread->t_iplhigh_count == 0);
}