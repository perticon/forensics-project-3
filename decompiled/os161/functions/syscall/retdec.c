void syscall(int32_t * tf) {
    int32_t v1 = (int32_t)tf; // 0x8001ee20
    int32_t v2; // 0x8001ee10
    if (v2 == 0) {
        // 0x8001ee24
        badassert("curthread != NULL", "../../arch/mips/syscall/syscall.c", 89, "syscall");
        v1 = (int32_t)"curthread != NULL";
    }
    int32_t * v3 = (int32_t *)(v2 + 92); // 0x8001ee44
    int32_t v4 = v1; // 0x8001ee50
    if (*v3 != 0) {
        // 0x8001ee54
        badassert("curthread->t_curspl == 0", "../../arch/mips/syscall/syscall.c", 90, "syscall");
        v4 = (int32_t)"curthread->t_curspl == 0";
    }
    int32_t * v5 = (int32_t *)(v2 + 96); // 0x8001ee74
    int32_t v6 = v4; // 0x8001ee80
    if (*v5 != 0) {
        // 0x8001ee84
        badassert("curthread->t_iplhigh_count == 0", "../../arch/mips/syscall/syscall.c", 91, "syscall");
        v6 = (int32_t)"curthread->t_iplhigh_count == 0";
    }
    int32_t v7 = *(int32_t *)(v6 + 28); // 0x8001eea0
    int32_t retval = 0; // bp-24, 0x8001eeb0
    g40 = v7;
    int32_t v8; // 0x8001ee10
    int32_t err; // bp-20, 0x8001ee10
    switch (v7) {
        case 0: {
            int32_t v9 = sys_fork((int32_t *)v6, &retval); // 0x8001efb0
            err = v9;
            v8 = v9;
            // break -> 0x8001f090
            break;
        }
        case 3: {
            // 0x8001f048
            sys__exit(*(int32_t *)(v6 + 36));
            err = 0;
            v8 = 0;
            // break -> 0x8001f090
            break;
        }
        case 4: {
            int32_t v10 = sys_waitpid(*(int32_t *)(v6 + 36), (int32_t *)0x4c554e20, 0x4c000000); // 0x8001ef10
            retval = v10;
            if (v10 > -1) {
                // 0x8001ef28
                err = 0;
                v8 = 0;
            } else {
                // 0x8001ef1c
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 5: {
            int32_t v11 = sys_getpid(); // 0x8001ef34
            retval = v11;
            if (v11 > -1) {
                // 0x8001ef4c
                err = 0;
                v8 = 0;
            } else {
                // 0x8001ef40
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 45: {
            int32_t v12 = *(int32_t *)(v6 + 36); // 0x8001ef54
            int32_t v13 = sys_open((int32_t *)v12, 0x4c554e20, 0x4c000000, &err); // 0x8001ef64
            retval = v13;
            if (v13 > -1) {
                // 0x8001ef7c
                err = 0;
                v8 = 0;
            } else {
                // 0x8001ef70
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 49: {
            int32_t v14 = sys_close(*(int32_t *)(v6 + 36)); // 0x8001ef8c
            retval = v14;
            if (v14 > -1) {
                // 0x8001efa4
                err = 0;
                v8 = 0;
            } else {
                // 0x8001ef98
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 50: {
            int32_t v15 = sys_read(*(int32_t *)(v6 + 36), (int32_t *)0x4c554e20, 0x4c000000); // 0x8001f028
            retval = v15;
            if (v15 > -1) {
                // 0x8001f040
                err = 0;
                v8 = 0;
            } else {
                // 0x8001f034
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 55: {
            int32_t v16 = sys_write(*(int32_t *)(v6 + 36), (int32_t *)0x4c554e20, 0x4c000000); // 0x8001eff8
            retval = v16;
            if (v16 > -1) {
                // 0x8001f010
                err = 0;
                v8 = 0;
            } else {
                // 0x8001f004
                err = 1;
                v8 = 1;
            }
            // break -> 0x8001f090
            break;
        }
        case 59: {
            int32_t v17 = sys_lseek(*(int32_t *)(v6 + 36), 0, 0x4c554e20, (int32_t *)0x4c000000); // 0x8001efdc
            err = v17;
            v8 = v17;
            // break -> 0x8001f090
            break;
        }
        case 68: {
            // 0x8001eea0
            v8 = err;
            // break -> 0x8001f090
            break;
        }
        case 113: {
            int32_t v18 = *(int32_t *)(v6 + 36); // 0x8001eee8
            int32_t v19 = sys___time((int32_t *)v18, (int32_t *)0x4c554e20); // 0x8001eef4
            err = v19;
            v8 = v19;
            // break -> 0x8001f090
            break;
        }
        case 119: {
            int32_t v20 = sys_reboot(*(int32_t *)(v6 + 36)); // 0x8001eedc
            err = v20;
            v8 = v20;
            // break -> 0x8001f090
            break;
        }
        default: {
            // 0x8001f05c
            kprintf("Unknown syscall %d\n", v7);
            err = 1;
            v8 = 1;
            // break -> 0x8001f090
            break;
        }
    }
    int32_t v21 = v8; // 0x8001f090
    *(int32_t *)"d->t_proc != NULL" = v21 == 0 ? retval : v21;
    *(int32_t *)"curthread->t_proc == kproc" = (int32_t)(v21 != 0);
    *(int32_t *)" code < 32" = (int32_t)"e < 32";
    if (*v3 != 0) {
        // 0x8001f0e0
        badassert("curthread->t_curspl == 0", "../../arch/mips/syscall/syscall.c", 237, "syscall");
    }
    // 0x8001f0fc
    if (*v5 != 0) {
        // 0x8001f10c
        badassert("curthread->t_iplhigh_count == 0", "../../arch/mips/syscall/syscall.c", 239, "syscall");
    }
}