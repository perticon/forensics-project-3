threadlist_insertbeforenode(struct thread *t, struct threadlistnode *onlist)
{
	struct threadlistnode *addee;

	addee = &t->t_listnode;

	DEBUGASSERT(addee->tln_prev == NULL);
	DEBUGASSERT(addee->tln_next == NULL);

	addee->tln_prev = onlist->tln_prev;
	addee->tln_next = onlist;
	addee->tln_prev->tln_next = addee;
	addee->tln_next->tln_prev = addee;
}