void threadlist_insertbeforenode(thread *t,threadlistnode *onlist)

{
  threadlistnode *ptVar1;
  
  ptVar1 = onlist->tln_prev;
  (t->t_listnode).tln_prev = ptVar1;
  (t->t_listnode).tln_next = onlist;
  ptVar1->tln_next = &t->t_listnode;
  ((t->t_listnode).tln_next)->tln_prev = &t->t_listnode;
  return;
}