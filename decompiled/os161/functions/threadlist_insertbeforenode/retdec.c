void threadlist_insertbeforenode(int32_t * t, int32_t * onlist) {
    int32_t v1 = (int32_t)onlist;
    int32_t v2 = (int32_t)t;
    int32_t v3 = v2 + 60; // 0x80017644
    *(int32_t *)v3 = v1;
    int32_t * v4 = (int32_t *)(v2 + 64); // 0x80017654
    *v4 = v1;
    *(int32_t *)(v1 + 4) = v3;
    *(int32_t *)*v4 = v3;
}