samestring(const char *a, const char *b)
{
	if (a==NULL || b==NULL) {
		return 0;
	}
	return !strcmp(a, b);
}