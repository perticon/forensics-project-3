int sfs_creat(vnode *v,char *name,bool excl,mode_t mode,vnode **ret)

{
  int iVar1;
  sfs_vnode *sv;
  sfs_fs *sfs;
  sfs_vnode *newguy;
  uint32_t ino;
  
  sfs = (sfs_fs *)v->vn_fs->fs_data;
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  iVar1 = sfs_dir_findname(sv,name,&ino,(int *)0x0,(int *)0x0);
  if ((iVar1 == 0) || (iVar1 == 0x13)) {
    if (iVar1 == 0) {
      if (excl == false) {
        iVar1 = sfs_loadvnode(sfs,ino,0,&newguy);
        if (iVar1 == 0) {
          *ret = &newguy->sv_absvn;
          vfs_biglock_release();
          iVar1 = 0;
        }
        else {
          vfs_biglock_release();
        }
      }
      else {
        vfs_biglock_release();
        iVar1 = 0x16;
      }
    }
    else {
      iVar1 = sfs_makeobj(sfs,1,&newguy);
      if (iVar1 == 0) {
        iVar1 = sfs_dir_link(sv,name,newguy->sv_ino,(int *)0x0);
        if (iVar1 == 0) {
          (newguy->sv_i).sfi_linkcount = (newguy->sv_i).sfi_linkcount + 1;
          newguy->sv_dirty = true;
          *ret = &newguy->sv_absvn;
          vfs_biglock_release();
          iVar1 = 0;
        }
        else {
          vnode_decref(&newguy->sv_absvn);
          vfs_biglock_release();
        }
      }
      else {
        vfs_biglock_release();
      }
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}