int32_t sfs_creat(int32_t * v2, char * name, bool excl, int32_t mode, int32_t * ret) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80009d58
    vfs_biglock_acquire();
    int32_t * v3 = (int32_t *)*(int32_t *)(v1 + 16); // 0x80009d7c
    int32_t ino; // bp-28, 0x80009d30
    int32_t result3 = sfs_dir_findname(v3, name, &ino, NULL, NULL); // 0x80009d7c
    int32_t * newguy; // bp-32, 0x80009d30
    int32_t result; // 0x80009d30
    switch (result3) {
        case 0: {
            if (excl) {
                // 0x80009db4
                vfs_biglock_release();
                // 0x80009ea0
                return 22;
            }
            int32_t v4 = sfs_loadvnode((int32_t *)v2_, ino, 0, &newguy); // 0x80009dd8
            if (v4 == 0) {
                // 0x80009df4
                *ret = (int32_t)newguy;
                vfs_biglock_release();
                result = 0;
            } else {
                // 0x80009de4
                vfs_biglock_release();
                result = v4;
            }
            // 0x80009ea0
            return result;
        }
        case 19: {
            int32_t result2 = sfs_makeobj((int32_t *)v2_, 1, &newguy); // 0x80009e14
            if (result2 != 0) {
                // 0x80009e20
                vfs_biglock_release();
                // 0x80009ea0
                return result2;
            }
            int32_t v5 = sfs_dir_link(v3, name, *(int32_t *)((int32_t)newguy + 536), NULL); // 0x80009e48
            if (v5 == 0) {
                int32_t v6 = (int32_t)newguy; // 0x80009e70
                int16_t * v7 = (int16_t *)(v6 + 30); // 0x80009e78
                *v7 = *v7 + 1;
                *(char *)(v6 + 540) = 1;
                *ret = v6;
                vfs_biglock_release();
                result = 0;
            } else {
                // 0x80009e54
                vnode_decref(newguy);
                vfs_biglock_release();
                result = v5;
            }
            // 0x80009ea0
            return result;
        }
    }
    // 0x80009d94
    vfs_biglock_release();
    // 0x80009ea0
    return result3;
}