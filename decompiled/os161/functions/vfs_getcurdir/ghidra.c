int vfs_getcurdir(vnode **ret)

{
  vnode *vn;
  int iVar1;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
  if (vn == (vnode *)0x0) {
    iVar1 = 0x13;
  }
  else {
    vnode_incref(vn);
    *ret = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
    iVar1 = 0;
  }
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  return iVar1;
}