int32_t vfs_getcurdir(int32_t ** ret) {
    // 0x80017fe0
    int32_t v1; // 0x80017fe0
    int32_t * v2 = (int32_t *)(v1 + 84); // 0x80017ff0
    spinlock_acquire((int32_t *)(*v2 + 8));
    int32_t v3 = *v2; // 0x80017ffc
    int32_t v4 = *(int32_t *)(v3 + 20); // 0x80018004
    int32_t v5 = v3; // 0x80018010
    int32_t result = 19; // 0x80018010
    if (v4 != 0) {
        // 0x80018014
        vnode_incref((int32_t *)v4);
        *(int32_t *)ret = *(int32_t *)(*v2 + 20);
        v5 = *v2;
        result = 0;
    }
    // 0x8001803c
    spinlock_release((int32_t *)(v5 + 8));
    return result;
}