int copyout(void *src,userptr_t userdest,size_t len)

{
  int iVar1;
  int unaff_s7;
  size_t stoplen;
  
  iVar1 = copycheck(userdest,len,&stoplen);
  if ((iVar1 == 0) && (iVar1 = 6, stoplen == len)) {
    *(code **)(unaff_s7 + 0xc) = copyfail;
    iVar1 = setjmp((__jmp_buf_tag *)(unaff_s7 + 0x10));
    if (iVar1 == 0) {
      memcpy(userdest,src,len);
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 0;
    }
    else {
      *(undefined4 *)(unaff_s7 + 0xc) = 0;
      iVar1 = 6;
    }
  }
  return iVar1;
}