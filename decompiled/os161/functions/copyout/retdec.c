int32_t copyout(int32_t * src, int32_t * userdest, int32_t len) {
    // 0x800202d8
    int32_t stoplen; // bp-16, 0x800202d8
    int32_t result = copycheck(userdest, len, &stoplen); // 0x800202f8
    if (result != 0) {
        // 0x8002035c
        return result;
    }
    // 0x80020304
    if (stoplen != len) {
        // 0x8002035c
        return 6;
    }
    // 0x80020318
    int32_t v1; // 0x800202d8
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x80020320
    *v2 = -0x7ffdfdcc;
    int32_t result2; // 0x800202d8
    if (setjmp(v1 + 16) == 0) {
        // 0x80020340
        memcpy((char *)userdest, src, len);
        *v2 = 0;
        result2 = 0;
    } else {
        // 0x80020334
        *v2 = 0;
        result2 = 6;
    }
    // 0x8002035c
    return result2;
}