copyout(const void *src, userptr_t userdest, size_t len)
{
	int result;
	size_t stoplen;

	result = copycheck(userdest, len, &stoplen);
	if (result) {
		return result;
	}
	if (stoplen != len) {
		/* Single block, can't legally truncate it. */
		return EFAULT;
	}

	curthread->t_machdep.tm_badfaultfunc = copyfail;

	result = setjmp(curthread->t_machdep.tm_copyjmp);
	if (result) {
		curthread->t_machdep.tm_badfaultfunc = NULL;
		return EFAULT;
	}

	memcpy((void *)userdest, src, len);

	curthread->t_machdep.tm_badfaultfunc = NULL;
	return 0;
}