int vfs_getroot(char *devname,vnode **ret)

{
  undefined3 extraout_var;
  bool bVar3;
  int iVar1;
  knowndevarray *pkVar2;
  char *a;
  char **ppcVar4;
  uint uVar5;
  uint uVar6;
  
  bVar3 = vfs_biglock_do_i_hold();
  iVar1 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar3) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0xcb,"vfs_getroot");
  }
  uVar6 = *(uint *)(*(int *)(iVar1 + -0x7374) + 4);
  uVar5 = 0;
  while( true ) {
    if (uVar6 <= uVar5) {
      return 0x19;
    }
    pkVar2 = knowndevs;
    if ((knowndevs->arr).num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppcVar4 = (char **)(pkVar2->arr).v[uVar5];
    if (ppcVar4[4] + -1 < (char *)0xfffffffe) {
      a = (char *)(**(code **)(*(int *)(ppcVar4[4] + 4) + 4))();
      iVar1 = strcmp(*ppcVar4,devname);
      if ((iVar1 == 0) || ((a != (char *)0x0 && (iVar1 = strcmp(a,devname), iVar1 == 0)))) {
        iVar1 = (**(code **)(*(int *)(ppcVar4[4] + 4) + 8))(ppcVar4[4],ret);
        return iVar1;
      }
    }
    else if ((ppcVar4[1] != (char *)0x0) && (iVar1 = strcmp(*ppcVar4,devname), iVar1 == 0)) {
      return 0x1a;
    }
    iVar1 = strcmp(*ppcVar4,devname);
    if (iVar1 == 0) {
      if (ppcVar4[4] != (char *)0x0) {
        badassert("kd->kd_fs==NULL","../../vfs/vfslist.c",0xf0,"vfs_getroot");
      }
      if (ppcVar4[1] != (char *)0x0) {
        badassert("kd->kd_rawname==NULL","../../vfs/vfslist.c",0xf1,"vfs_getroot");
      }
      if (ppcVar4[2] == (char *)0x0) {
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0xf2,"vfs_getroot");
      }
      vnode_incref((vnode *)ppcVar4[3]);
      *ret = (vnode *)ppcVar4[3];
      return 0;
    }
    if ((ppcVar4[1] != (char *)0x0) && (iVar1 = strcmp(ppcVar4[1],devname), iVar1 == 0)) break;
    uVar5 = uVar5 + 1;
  }
  if (ppcVar4[2] == (char *)0x0) {
    badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0xfd,"vfs_getroot");
  }
  vnode_incref((vnode *)ppcVar4[3]);
  *ret = (vnode *)ppcVar4[3];
  return 0;
}