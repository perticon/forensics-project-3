int32_t vfs_getroot(char * devname, int32_t * ret) {
    bool v1 = vfs_biglock_do_i_hold(); // 0x80018fb8
    int32_t v2 = (int32_t)&g35; // 0x80018fc0
    if (!v1) {
        // 0x80018fc4
        badassert("vfs_biglock_do_i_hold()", "../../vfs/vfslist.c", 203, "vfs_getroot");
        v2 = &g41;
    }
    uint32_t v3 = *(int32_t *)(*(int32_t *)(v2 - 0x7374) + 4); // 0x80018fec
    if (v3 == 0) {
        // 0x80019228
        return 25;
    }
    int32_t v4 = 0; // 0x80019214
    int32_t result; // 0x80018f8c
    int32_t v5; // 0x80019040
    int32_t * v6; // 0x80019048
    while (true) {
        int32_t v7 = (int32_t)knowndevs; // 0x80019014
        if (v4 >= (int32_t)bootfs_vnode) {
            // 0x80019018
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v7 = &g41;
        }
        // 0x80019034
        v5 = *(int32_t *)(*(int32_t *)v7 + 4 * v4);
        v6 = (int32_t *)(v5 + 16);
        int32_t v8 = *v6; // 0x80019048
        int32_t * v9; // 0x80018f8c
        if (v8 >= 0xffffffff) {
            int32_t * v10 = (int32_t *)v5;
            v9 = v10;
            if (*(int32_t *)(v5 + 4) != 0) {
                // 0x800190e0
                v9 = v10;
                if (strcmp((char *)*v10, devname) == 0) {
                    // 0x80019228
                    return 26;
                }
            }
        } else {
            int32_t v11 = *(int32_t *)(*(int32_t *)(v8 + 4) + 4); // 0x80019068
            int32_t * v12 = (int32_t *)v5;
            if (strcmp((char *)*v12, devname) == 0) {
                // break -> 0x800190a8
                break;
            }
            // 0x80019090
            v9 = v12;
            if (v11 != 0) {
                // 0x80019098
                v9 = v12;
                if (strcmp((char *)v11, devname) == 0) {
                    // break -> 0x800190a8
                    break;
                }
            }
        }
        // 0x800190f4
        if (strcmp((char *)*v9, devname) == 0) {
            // 0x80019108
            if (*v6 == 0) {
                goto lab_0x80019134;
            } else {
                // 0x80019118
                badassert("kd->kd_fs==NULL", "../../vfs/vfslist.c", 240, "vfs_getroot");
                goto lab_0x80019134;
            }
        }
        int32_t v13 = *(int32_t *)(v5 + 4); // 0x800191a4
        if (v13 != 0) {
            // 0x800191b4
            if (strcmp((char *)v13, devname) == 0) {
                // 0x800191c4
                if (*(int32_t *)(v5 + 8) == 0) {
                    // 0x800191d4
                    badassert("kd->kd_device != NULL", "../../vfs/vfslist.c", 253, "vfs_getroot");
                    goto lab_0x800191f0;
                } else {
                    goto lab_0x800191f0;
                }
            }
        }
        // 0x80019208
        v4++;
        result = 25;
        if (v4 >= v3) {
          lab_0x80019228:
            // 0x80019228
            return result;
        }
    }
    // 0x80019228
    return *(int32_t *)(*(int32_t *)(*v6 + 4) + 8);
  lab_0x80019134:
    // 0x80019134
    if (*(int32_t *)(v5 + 4) == 0) {
        goto lab_0x80019160;
    } else {
        // 0x80019144
        badassert("kd->kd_rawname==NULL", "../../vfs/vfslist.c", 241, "vfs_getroot");
        goto lab_0x80019160;
    }
  lab_0x80019160:
    // 0x80019160
    if (*(int32_t *)(v5 + 8) == 0) {
        // 0x80019170
        badassert("kd->kd_device != NULL", "../../vfs/vfslist.c", 242, "vfs_getroot");
        goto lab_0x8001918c;
    } else {
        goto lab_0x8001918c;
    }
  lab_0x8001918c:;
    int32_t * v14 = (int32_t *)(v5 + 12); // 0x8001918c
    vnode_incref((int32_t *)*v14);
    *ret = *v14;
    result = 0;
    return result;
  lab_0x800191f0:;
    int32_t * v15 = (int32_t *)(v5 + 12); // 0x800191f0
    vnode_incref((int32_t *)*v15);
    *ret = *v15;
    result = 0;
    goto lab_0x80019228;
}