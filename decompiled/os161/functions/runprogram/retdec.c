int32_t runprogram(char * progname) {
    // 0x8000df70
    int32_t * v2; // bp-32, 0x8000df70
    int32_t result = vfs_open(progname, 0, 0, &v2); // 0x8000df8c
    if (result != 0) {
        // 0x8000e05c
        return result;
    }
    // 0x8000df98
    if (proc_getas() != NULL) {
        // 0x8000dfa8
        badassert("proc_getas() == NULL", "../../syscall/runprogram.c", 69, "runprogram");
    }
    int32_t * v1 = as_create(); // 0x8000dfc8
    if (v1 == NULL) {
        // 0x8000dfd4
        vfs_close(v2);
        // 0x8000e05c
        return 3;
    }
    // 0x8000dfe8
    proc_setas(v1);
    as_activate();
    int32_t entrypoint; // bp-28, 0x8000df70
    int32_t result2 = load_elf(v2, &entrypoint); // 0x8000e000
    vfs_close(v2);
    if (result2 != 0) {
        // 0x8000e05c
        return result2;
    }
    // 0x8000e020
    int32_t stackptr; // bp-24, 0x8000df70
    int32_t v2_ = as_define_stack(v1, &stackptr); // 0x8000e034
    int32_t result3 = v2_; // 0x8000e03c
    if (v2_ == 0) {
        // 0x8000e040
        enter_new_process(0, NULL, NULL, stackptr, entrypoint);
        result3 = &g41;
    }
    // 0x8000e05c
    return result3;
}