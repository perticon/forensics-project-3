int runprogram(char *progname)

{
  vaddr_t vVar1;
  addrspace *paVar2;
  vnode *v;
  vaddr_t entrypoint;
  vaddr_t stackptr;
  
  vVar1 = vfs_open(progname,0,0,&v);
  if (vVar1 == 0) {
    paVar2 = proc_getas();
    if (paVar2 != (addrspace *)0x0) {
      badassert("proc_getas() == NULL","../../syscall/runprogram.c",0x45,"runprogram");
    }
    paVar2 = as_create();
    if (paVar2 == (addrspace *)0x0) {
      vfs_close(v);
      vVar1 = 3;
    }
    else {
      proc_setas(paVar2);
      as_activate();
      vVar1 = load_elf(v,&entrypoint);
      if (vVar1 == 0) {
        vfs_close(v);
        vVar1 = as_define_stack(paVar2,&stackptr);
        if (vVar1 == 0) {
          enter_new_process(0,(userptr_t)0x0,(userptr_t)0x0,stackptr,entrypoint);
          vVar1 = entrypoint;
        }
      }
      else {
        vfs_close(v);
      }
    }
  }
  return vVar1;
}