void free_ipt_process(int32_t pid) {
    // 0x8001d054
    spinlock_acquire(&ipt_lock);
    int32_t v1 = 0; // 0x8001d098
    int32_t v2 = -1; // 0x8001d098
    int32_t v3; // 0x8001d054
    if (ipt_active == 0) {
        // 0x8001d09c
        badassert("ipt_active", "../../vm/pt.c", 223, "free_ipt_process");
        v3 = 0;
        goto lab_0x8001d0bc;
    } else {
        goto lab_0x8001d128;
    }
  lab_0x8001d128:
    // 0x8001d128
    v3 = v1;
    int32_t v4 = 8 * v1; // 0x8001d138
    int32_t v5 = v2; // 0x8001d138
    if (v1 >= nRamFrames) {
        // 0x8001d13c
        spinlock_release(&ipt_lock);
        return;
    }
    goto lab_0x8001d0bc;
  lab_0x8001d0bc:
    // 0x8001d0bc
    v2 = v5;
    int32_t v6 = v4;
    int32_t v7 = v3;
    int32_t * v8 = (int32_t *)(v6 + (int32_t)ipt); // 0x8001d0c8
    if (*v8 == pid) {
        // 0x8001d0d8
        *v8 = v2;
        if (freeppages(0x1000 * v7, v7) == 0) {
            // 0x8001d0ec
            panic("Trying to free ipt entry while VM not active");
        }
        // 0x8001d0f4
        STdelete(ipt_hash, pid, *(int32_t *)(v6 + 4 + (int32_t)ipt));
    }
    // 0x8001d110
    v1 = v7 + 1;
    goto lab_0x8001d128;
}