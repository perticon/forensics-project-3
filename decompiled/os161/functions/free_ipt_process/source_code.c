void free_ipt_process(pid_t pid)
{
    int i, result;
    spinlock_acquire(&ipt_lock);
    KASSERT(ipt_active);

    for (i = 0; i < nRamFrames; i++)
    {
        if (ipt[i].pid == pid)
        {
            ipt[i].pid = -1;
            result = freeppages(i * PAGE_SIZE, i);
            if (result == 0)
            {
                panic("Trying to free ipt entry while VM not active");
            }
            STdelete(ipt_hash, pid, ipt[i].vaddr);
        }
    }
    spinlock_release(&ipt_lock);
}