void free_ipt_process(pid_t pid)

{
  int *piVar1;
  int iVar2;
  int first_page;
  int unaff_s1;
  int unaff_s2;
  int unaff_s4;
  int unaff_s5;
  int unaff_s6;
  
  spinlock_acquire(&ipt_lock);
  first_page = 0;
  if (ipt_active != 0) {
    unaff_s4 = -0x7ffd0000;
    unaff_s2 = -0x7ffd0000;
    unaff_s6 = -1;
    unaff_s5 = -0x7ffd0000;
    goto LAB_8001d128;
  }
  badassert("ipt_active","../../vm/pt.c",0xdf,"free_ipt_process");
  do {
    piVar1 = (int *)(*(int *)(unaff_s2 + -0x72f0) + unaff_s1);
    if (*piVar1 == pid) {
      *piVar1 = unaff_s6;
      iVar2 = freeppages(first_page << 0xc,first_page);
      if (iVar2 == 0) {
                    /* WARNING: Subroutine does not return */
        panic("Trying to free ipt entry while VM not active");
      }
      STdelete(*(ST *)(unaff_s5 + -0x7304),pid,
               *(vaddr_t *)(*(int *)(unaff_s2 + -0x72f0) + unaff_s1 + 4));
    }
    first_page = first_page + 1;
LAB_8001d128:
    unaff_s1 = first_page << 3;
  } while (first_page < *(int *)(unaff_s4 + -0x72f4));
  spinlock_release(&ipt_lock);
  return;
}