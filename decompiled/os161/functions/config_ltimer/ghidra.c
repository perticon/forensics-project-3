int config_ltimer(ltimer_softc *lt,int ltimerno)

{
  lt->lt_hardclock = 0;
  if (havetimerclock == false) {
    havetimerclock = true;
    lt->lt_timerclock = 1;
    lamebus_write_register((lamebus_softc *)lt->lt_bus,lt->lt_buspos,8,1);
    lamebus_write_register((lamebus_softc *)lt->lt_bus,lt->lt_buspos,0x10,1000000);
  }
  return 0;
}