int32_t config_ltimer(int32_t * lt, int32_t ltimerno) {
    // 0x80004f80
    *lt = 0;
    if (*(char *)&havetimerclock == 0) {
        int32_t v1 = (int32_t)lt;
        *(char *)&havetimerclock = 1;
        *(int32_t *)(v1 + 4) = 1;
        int32_t * v2 = (int32_t *)(v1 + 8); // 0x80004fb8
        int32_t * v3 = (int32_t *)(v1 + 12); // 0x80004fbc
        lamebus_write_register((int32_t *)*v2, *v3, 8, 1);
        lamebus_write_register((int32_t *)*v2, *v3, 16, 0xf4240);
    }
    // 0x80004fe4
    return 0;
}