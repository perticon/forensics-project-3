config_ltimer(struct ltimer_softc *lt, int ltimerno)
{
	/*
	 * Running on System/161 2.x, we always use the processor
	 * on-chip timer for hardclock and we don't need ltimer as
	 * hardclock.
	 *
	 * Ideally there should be code here that will use an ltimer
	 * for hardclock if nothing else is available; e.g. if we
	 * wanted to make OS/161 2.x run on System/161 1.x. However,
	 * that requires a good bit more infrastructure for handling
	 * timers than we have and it doesn't seem worthwhile.
	 *
	 * It would also require some hacking, because all CPUs need
	 * to receive timer interrupts. (Exercise: how would you make
	 * sure all CPUs receive exactly one timer interrupt? Remember
	 * that LAMEbus uses level-triggered interrupts, so the
	 * hardware interrupt line will cause repeated interrupts if
	 * it's not reset on the device; but if it's reset on the
	 * device before all CPUs manage to see it, those CPUs won't
	 * be interrupted at all.)
	 *
	 * Note that the beep and rtclock devices *do* attach to
	 * ltimer.
	 */
	(void)ltimerno;
	lt->lt_hardclock = 0;

	/*
	 * We do, however, use ltimer for the timer clock, since the
	 * on-chip timer can't do that.
	 */
	if (!havetimerclock) {
		havetimerclock = true;
		lt->lt_timerclock = 1;

		/* Wire it to go off once every second. */
		bus_write_register(lt->lt_bus, lt->lt_buspos, LT_REG_ROE, 1);
		bus_write_register(lt->lt_bus, lt->lt_buspos, LT_REG_COUNT,
				   LT_GRANULARITY);
	}

	return 0;
}