void thread_machdep_cleanup(thread_machdep *tm)

{
  if (tm->tm_badfaultfunc != (badfaultfunc_t *)0x0) {
    badassert("tm->tm_badfaultfunc == NULL","../../arch/mips/thread/thread_machdep.c",0x30,
              "thread_machdep_cleanup");
  }
  return;
}