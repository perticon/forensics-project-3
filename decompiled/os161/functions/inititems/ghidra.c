void inititems(void)

{
  if ((testsem == (semaphore *)0x0) &&
     (testsem = sem_create("testsem",2), testsem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: sem_create failed\n");
  }
  if ((testlock == (lock *)0x0) && (testlock = lock_create("testlock"), testlock == (lock *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: lock_create failed\n");
  }
  if ((testcv == (cv *)0x0) && (testcv = cv_create("testlock"), testcv == (cv *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: cv_create failed\n");
  }
  if ((donesem == (semaphore *)0x0) &&
     (donesem = sem_create("donesem",0), donesem == (semaphore *)0x0)) {
                    /* WARNING: Subroutine does not return */
    panic("synchtest: sem_create failed\n");
  }
  return;
}