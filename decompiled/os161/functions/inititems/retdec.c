void inititems(void) {
    // 0x800129f4
    if (testsem == NULL) {
        int32_t * v1 = sem_create("testsem", 2); // 0x80012a1c
        *(int32_t *)&testsem = (int32_t)v1;
        if (v1 == NULL) {
            // 0x80012a2c
            panic("synchtest: sem_create failed\n");
        }
    }
    // 0x80012a3c
    if (testlock == NULL) {
        int32_t * v2 = lock_create("testlock"); // 0x80012a54
        *(int32_t *)&testlock = (int32_t)v2;
        if (v2 == NULL) {
            // 0x80012a64
            panic("synchtest: lock_create failed\n");
        }
    }
    // 0x80012a74
    if (testcv == NULL) {
        int32_t * v3 = cv_create("testlock"); // 0x80012a8c
        *(int32_t *)&testcv = (int32_t)v3;
        if (v3 == NULL) {
            // 0x80012a9c
            panic("synchtest: cv_create failed\n");
        }
    }
    // 0x80012aac
    if (g16 != 0) {
        // 0x80012ae0
        return;
    }
    int32_t * v4 = sem_create("donesem", 0); // 0x80012ac4
    g16 = (int32_t)v4;
    if (v4 == NULL) {
        // 0x80012ad4
        panic("synchtest: sem_create failed\n");
    }
}