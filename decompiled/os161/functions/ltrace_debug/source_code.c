ltrace_debug(uint32_t code)
{
	if (the_trace != NULL) {
		bus_write_register(the_trace->lt_busdata, the_trace->lt_buspos,
				   LTRACE_REG_DEBUG, code);
	}
}