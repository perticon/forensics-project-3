void ltrace_debug(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,8,code);
  }
  return;
}