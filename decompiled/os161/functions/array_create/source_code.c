array_create(void)
{
	struct array *a;

	a = kmalloc(sizeof(*a));
	if (a != NULL) {
		array_init(a);
	}
	return a;
}