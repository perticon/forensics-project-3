vopfail_mkdir_notdir(struct vnode *vn, const char *name, mode_t mode)
{
	(void)vn;
	(void)name;
	(void)mode;
	return ENOTDIR;
}