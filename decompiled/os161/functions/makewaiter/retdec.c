void makewaiter(int32_t * sem) {
    // 0x80011528
    spinlock_acquire(&waiters_lock);
    waiters_running++;
    spinlock_release(&waiters_lock);
    int32_t v1 = thread_fork("semunit waiter", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x15c0), (char *)sem, 0); // 0x80011580
    if (v1 != 0) {
        // 0x8001158c
        panic("semunit: thread_fork failed\n");
    }
    // 0x80011598
    kprintf("Sleeping for waiter to run\n");
    clocksleep(1);
}