makewaiter(struct semaphore *sem)
{
	int result;

	spinlock_acquire(&waiters_lock);
	waiters_running++;
	spinlock_release(&waiters_lock);

	result = thread_fork("semunit waiter", NULL, waiter, sem, 0);
	if (result) {
		panic("semunit: thread_fork failed\n");
	}
	kprintf("Sleeping for waiter to run\n");
	clocksleep(1);
}