void makewaiter(semaphore *sem)

{
  int iVar1;
  
  spinlock_acquire(&waiters_lock);
  waiters_running = waiters_running + 1;
  spinlock_release(&waiters_lock);
  iVar1 = thread_fork("semunit waiter",(proc *)0x0,waiter,sem,0);
  if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
    panic("semunit: thread_fork failed\n");
  }
  kprintf("Sleeping for waiter to run\n");
  clocksleep(1);
  return;
}