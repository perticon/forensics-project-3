dev_reclaim(struct vnode *v)
{
	(void)v;
	/* nothing - device continues to exist even when not in use */
	return 0;
}