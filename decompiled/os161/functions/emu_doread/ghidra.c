int emu_doread(emu_softc *sc,uint32_t handle,uint32_t len,uint32_t op,uio *uio)

{
  int iVar1;
  uint32_t uVar2;
  char *pcVar3;
  char *pcVar4;
  char *pcVar5;
  void *ptr;
  
  pcVar3 = (char *)sc;
  if (uio->uio_rw != UIO_READ) {
    pcVar3 = "uio->uio_rw == UIO_READ";
    pcVar4 = s_______dev_lamebus_emu_c_80022750;
    len = 0x112;
    pcVar5 = "emu_doread";
    badassert("uio->uio_rw == UIO_READ",s_______dev_lamebus_emu_c_80022750,0x112,"emu_doread");
    handle = (uint32_t)pcVar4;
    op = (uint32_t)pcVar5;
  }
  if (*(int *)&uio->uio_offset < 1) {
    lock_acquire(*(lock **)((int)pcVar3 + 0xc));
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,len);
    lamebus_write_register
              ((lamebus_softc *)sc->e_busdata,sc->e_buspos,4,
               *(uint32_t *)((int)&uio->uio_offset + 4));
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,op);
    iVar1 = emu_waitdone(sc);
    if (iVar1 == 0) {
      SYNC(0);
      ptr = sc->e_iobuf;
      uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
      iVar1 = uiomove(ptr,uVar2,uio);
      uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,4);
      *(uint32_t *)((int)&uio->uio_offset + 4) = uVar2;
      *(undefined4 *)&uio->uio_offset = 0;
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar1 = 0;
  }
  return iVar1;
}