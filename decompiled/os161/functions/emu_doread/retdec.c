int32_t emu_doread(int32_t * sc, int32_t handle, int32_t len, int32_t op, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = handle; // 0x80002cc4
    int32_t v3 = len; // 0x80002cc4
    int32_t v4 = op; // 0x80002cc4
    if (*(int32_t *)(v1 + 24) != 0) {
        // 0x80002cc8
        badassert("uio->uio_rw == UIO_READ", "../../dev/lamebus/emu.c", 274, "emu_doread");
        v2 = (int32_t)"../../dev/lamebus/emu.c";
        v3 = 274;
        v4 = (int32_t)"emu_doread";
    }
    int32_t * v5 = (int32_t *)(v1 + 8); // 0x80002cf0
    if (*v5 > 0) {
        // 0x80002dc8
        return 0;
    }
    int32_t v6 = (int32_t)sc;
    lock_acquire((int32_t *)0x55203d3d);
    int32_t * v7 = (int32_t *)(v6 + 4); // 0x80002d10
    lamebus_write_register((int32_t *)0x55203d3d, *v7, 0, v2);
    lamebus_write_register((int32_t *)0x55203d3d, *v7, 8, v3);
    int32_t * v8 = (int32_t *)(v1 + 12); // 0x80002d3c
    lamebus_write_register((int32_t *)0x55203d3d, *v7, 4, *v8);
    lamebus_write_register((int32_t *)0x55203d3d, *v7, 12, v4);
    int32_t v9 = emu_waitdone(sc); // 0x80002d60
    int32_t result = v9; // 0x80002d68
    if (v9 == 0) {
        // 0x80002d6c
        __asm_sync();
        int32_t v10 = *(int32_t *)(v6 + 20); // 0x80002d70
        result = uiomove((char *)v10, lamebus_read_register(sc, *v7, 8), uio);
        *v8 = lamebus_read_register((int32_t *)v10, *v7, 4);
        *v5 = 0;
    }
    // 0x80002db0
    lock_release((int32_t *)*(int32_t *)(v6 + 12));
    // 0x80002dc8
    return result;
}