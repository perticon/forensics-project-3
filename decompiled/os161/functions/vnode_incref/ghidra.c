void vnode_incref(vnode *vn)

{
  char *pcVar1;
  
  pcVar1 = (char *)vn;
  if (vn == (vnode *)0x0) {
    pcVar1 = s_vn____NULL_80027408;
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",0x52,"vnode_incref");
  }
  spinlock_acquire((spinlock *)((int)pcVar1 + 4));
  vn->vn_refcount = vn->vn_refcount + 1;
  spinlock_release((spinlock *)((int)pcVar1 + 4));
  return;
}