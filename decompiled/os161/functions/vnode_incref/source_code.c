vnode_incref(struct vnode *vn)
{
	KASSERT(vn != NULL);

	spinlock_acquire(&vn->vn_countlock);
	vn->vn_refcount++;
	spinlock_release(&vn->vn_countlock);
}