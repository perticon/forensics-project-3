void vnode_incref(int32_t * vn) {
    if (vn == NULL) {
        // 0x8001a77c
        badassert("vn != NULL", "../../vfs/vnode.c", 82, "vnode_incref");
    }
    // 0x8001a79c
    spinlock_acquire((int32_t *)"= NULL");
    *vn = (int32_t)"= NULL" + 1;
    spinlock_release((int32_t *)"= NULL");
}