fakethread_destroy(struct thread *t)
{
	KASSERT(t->t_stack == FAKE_MAGIC);
	threadlistnode_cleanup(&t->t_listnode);
	kfree(t->t_name);
	kfree(t);
}