void fakethread_destroy(int32_t * t) {
    // 0x80014254
    if (*(int32_t *)((int32_t)t + 72) != 0xbaabaa) {
        // 0x80014274
        badassert("t->t_stack == FAKE_MAGIC", "../../test/threadlisttest.c", 85, "fakethread_destroy");
    }
    // 0x80014294
    threadlistnode_cleanup((int32_t *)"Aldaran");
    kfree("Aldaran");
    kfree((char *)t);
}