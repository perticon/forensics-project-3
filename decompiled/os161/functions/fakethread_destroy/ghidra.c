void fakethread_destroy(thread *t)

{
  char *pcVar1;
  
  pcVar1 = (char *)t;
  if (t->t_stack != (void *)0xbaabaa) {
    pcVar1 = "t->t_stack == FAKE_MAGIC";
    badassert("t->t_stack == FAKE_MAGIC","../../test/threadlisttest.c",0x55,"fakethread_destroy");
  }
  threadlistnode_cleanup((threadlistnode *)((int)pcVar1 + 0x3c));
  kfree(t->t_name);
  kfree(t);
  return;
}