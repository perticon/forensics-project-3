tryattach_rtclock_to_ltimer(int devunit, struct ltimer_softc *bus, int busunit)
{
	struct rtclock_softc *dev;
	int result;

	dev = attach_rtclock_to_ltimer(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("rtclock%d at ltimer%d", devunit, busunit);
	result = config_rtclock(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_rtclock = devunit+1;
	autoconf_rtclock(dev, devunit);
	return 0;
}