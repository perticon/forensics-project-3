int32_t tryattach_rtclock_to_ltimer(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_rtclock_to_ltimer(devunit, bus); // 0x800013d0
    if (v1 == NULL) {
        // 0x80001448
        return -1;
    }
    // 0x800013dc
    kprintf("rtclock%d at ltimer%d", devunit, busunit);
    int32_t v2 = config_rtclock(v1, devunit); // 0x800013f8
    int32_t result; // 0x800013b4
    if (v2 == 0) {
        // 0x80001424
        kprintf("\n");
        nextunit_rtclock = devunit + 1;
        result = 0;
    } else {
        // 0x80001404
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x80001448
    return result;
}