int tryattach_rtclock_to_ltimer(int devunit,ltimer_softc *bus,int busunit)

{
  rtclock_softc *rtc;
  int errcode;
  char *pcVar1;
  
  rtc = attach_rtclock_to_ltimer(devunit,bus);
  if (rtc == (rtclock_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("rtclock%d at ltimer%d",devunit,busunit);
    errcode = config_rtclock(rtc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_rtclock = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}