int32_t sfs_sync_freemap(int32_t * sfs) {
    char * v1 = (char *)((int32_t)sfs + 536); // 0x8000807c
    if (*v1 == 0) {
        // 0x800080a8
        return 0;
    }
    int32_t v2 = sfs_freemapio(sfs, 1); // 0x80008090
    int32_t result = v2; // 0x80008098
    if (v2 == 0) {
        // 0x8000809c
        *v1 = 0;
        result = 0;
    }
    // 0x800080a8
    return result;
}