int sfs_sync_freemap(sfs_fs *sfs)

{
  int iVar1;
  
  if (sfs->sfs_freemapdirty == false) {
    iVar1 = 0;
  }
  else {
    iVar1 = sfs_freemapio(sfs,UIO_WRITE);
    if (iVar1 == 0) {
      sfs->sfs_freemapdirty = false;
    }
  }
  return iVar1;
}