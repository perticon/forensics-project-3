sfs_sync_freemap(struct sfs_fs *sfs)
{
	int result;

	if (sfs->sfs_freemapdirty) {
		result = sfs_freemapio(sfs, UIO_WRITE);
		if (result) {
			return result;
		}
		sfs->sfs_freemapdirty = false;
	}

	return 0;
}