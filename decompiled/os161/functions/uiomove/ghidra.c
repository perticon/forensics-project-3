int uiomove(void *ptr,size_t n,uio *uio)

{
  addrspace *paVar1;
  uint uVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  uio *puVar6;
  addrspace *paVar7;
  iovec *piVar8;
  
  if (UIO_WRITE < uio->uio_rw) {
                    /* WARNING: Subroutine does not return */
    panic("uiomove: Invalid uio_rw %d\n");
  }
  puVar6 = uio;
  if (uio->uio_segflg == UIO_SYSSPACE) {
    if (uio->uio_space == (addrspace *)0x0) goto LAB_8000b9dc;
    puVar6 = (uio *)0x34;
    badassert("uio->uio_space == NULL","../../lib/uio.c",0x34,"uiomove");
  }
  paVar7 = puVar6->uio_space;
  paVar1 = proc_getas();
  if (paVar7 == paVar1) goto LAB_8000b9dc;
  badassert("uio->uio_space == proc_getas()","../../lib/uio.c",0x37,"uiomove");
  do {
    piVar8 = uio->uio_iov;
    uVar2 = piVar8->iov_len;
    if (n < piVar8->iov_len) {
      uVar2 = n;
    }
    if (uVar2 == 0) {
      uio->uio_iov = piVar8 + 1;
      uVar2 = uio->uio_iovcnt - 1;
      uio->uio_iovcnt = uVar2;
      if (uVar2 == 0) {
                    /* WARNING: Subroutine does not return */
        panic("uiomove: ran out of buffers\n");
      }
    }
    else {
      if (uio->uio_segflg < UIO_SYSSPACE) {
        if (uio->uio_rw == UIO_READ) {
          iVar3 = copyout(ptr,(userptr_t)piVar8->field_0,uVar2);
        }
        else {
          iVar3 = copyin((const_userptr_t)piVar8->field_0,ptr,uVar2);
        }
        if (iVar3 != 0) {
          return iVar3;
        }
        piVar8->field_0 = piVar8->field_0 + uVar2;
      }
      else {
        if (uio->uio_segflg != UIO_SYSSPACE) {
                    /* WARNING: Subroutine does not return */
          panic("uiomove: Invalid uio_segflg %d\n");
        }
        if (uio->uio_rw == UIO_READ) {
          memmove((void *)piVar8->field_0,ptr,uVar2);
        }
        else {
          memmove(ptr,(void *)piVar8->field_0,uVar2);
        }
        piVar8->field_0 = piVar8->field_0 + uVar2;
      }
      piVar8->iov_len = piVar8->iov_len - uVar2;
      uio->uio_resid = uio->uio_resid - uVar2;
      uVar5 = *(uint *)((int)&uio->uio_offset + 4);
      uVar4 = uVar5 + uVar2;
      *(uint *)&uio->uio_offset = (uint)(uVar4 < uVar5) + *(int *)&uio->uio_offset;
      *(uint *)((int)&uio->uio_offset + 4) = uVar4;
      ptr = (void *)((int)ptr + uVar2);
      n = n - uVar2;
    }
LAB_8000b9dc:
    if (n == 0) {
      return 0;
    }
  } while (uio->uio_resid != 0);
  return 0;
}