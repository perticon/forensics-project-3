uiomove(void *ptr, size_t n, struct uio *uio)
{
	struct iovec *iov;
	size_t size;
	int result;

	if (uio->uio_rw != UIO_READ && uio->uio_rw != UIO_WRITE) {
		panic("uiomove: Invalid uio_rw %d\n", (int) uio->uio_rw);
	}
	if (uio->uio_segflg==UIO_SYSSPACE) {
		KASSERT(uio->uio_space == NULL);
	}
	else {
		KASSERT(uio->uio_space == proc_getas());
	}

	while (n > 0 && uio->uio_resid > 0) {
		/* get the first iovec */
		iov = uio->uio_iov;
		size = iov->iov_len;

		if (size > n) {
			size = n;
		}

		if (size == 0) {
			/* move to the next iovec and try again */
			uio->uio_iov++;
			uio->uio_iovcnt--;
			if (uio->uio_iovcnt == 0) {
				/*
				 * This should only happen if you set
				 * uio_resid incorrectly (to more than
				 * the total length of buffers the uio
				 * points to).
				 */
				panic("uiomove: ran out of buffers\n");
			}
			continue;
		}

		switch (uio->uio_segflg) {
		    case UIO_SYSSPACE:
			    if (uio->uio_rw == UIO_READ) {
				    memmove(iov->iov_kbase, ptr, size);
			    }
			    else {
				    memmove(ptr, iov->iov_kbase, size);
			    }
			    iov->iov_kbase = ((char *)iov->iov_kbase+size);
			    break;
		    case UIO_USERSPACE:
		    case UIO_USERISPACE:
			    if (uio->uio_rw == UIO_READ) {
				    result = copyout(ptr, iov->iov_ubase,size);
			    }
			    else {
				    result = copyin(iov->iov_ubase, ptr, size);
			    }
			    if (result) {
				    return result;
			    }
			    iov->iov_ubase += size;
			    break;
		    default:
			    panic("uiomove: Invalid uio_segflg %d\n",
				  (int)uio->uio_segflg);
		}

		iov->iov_len -= size;
		uio->uio_resid -= size;
		uio->uio_offset += size;
		ptr = ((char *)ptr + size);
		n -= size;
	}

	return 0;
}