int32_t uiomove(char * ptr, int32_t n, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t * v2 = (int32_t *)(v1 + 24); // 0x8000b7f4
    uint32_t v3 = *v2; // 0x8000b7f4
    int32_t v4 = (int32_t)ptr; // 0x8000b804
    if (v3 >= 2) {
        // 0x8000b808
        panic("uiomove: Invalid uio_rw %d\n", v3);
        v4 = (int32_t)"uiomove: Invalid uio_rw %d\n";
    }
    int32_t * v5 = (int32_t *)(v1 + 20); // 0x8000b814
    int32_t v6 = v1; // 0x8000b820
    int32_t v7; // 0x8000b7d0
    int32_t v8; // 0x8000b7d0
    int32_t v9; // 0x8000b7d0
    if (*v5 == 2) {
        // 0x8000b824
        v7 = v1;
        v8 = n;
        v9 = v4;
        if (*(int32_t *)(v1 + 28) == 0) {
            goto lab_0x8000b9dc;
        } else {
            // 0x8000b834
            badassert("uio->uio_space == NULL", "../../lib/uio.c", 52, "uiomove");
            v6 = 52;
            goto lab_0x8000b854;
        }
    } else {
        goto lab_0x8000b854;
    }
  lab_0x8000b854:
    // 0x8000b854
    v7 = v6;
    v8 = n;
    v9 = v4;
    int32_t v10; // 0x8000b7d0
    int32_t v11; // 0x8000b7d0
    int32_t v12; // 0x8000b7d0
    if (*(int32_t *)(v6 + 28) == (int32_t)proc_getas()) {
        goto lab_0x8000b9dc;
    } else {
        // 0x8000b868
        badassert("uio->uio_space == proc_getas()", "../../lib/uio.c", 55, "uiomove");
        v10 = 55;
        v11 = n;
        v12 = v4;
        goto lab_0x8000b888;
    }
  lab_0x8000b9dc:
    // 0x8000b9dc
    if (v8 == 0) {
        // 0x8000b9f4
        return 0;
    }
    // 0x8000b9e4
    v10 = v7;
    v11 = v8;
    v12 = v9;
    if (*(int32_t *)(v1 + 16) == 0) {
        // 0x8000b9f4
        return 0;
    }
    goto lab_0x8000b888;
  lab_0x8000b888:;
    int32_t v13 = v12;
    int32_t v14 = v11;
    int32_t v15 = v10;
    uint32_t v16 = *(int32_t *)(v15 + 4); // 0x8000b890
    int32_t v17 = v14 >= v16 ? v16 : v14;
    int32_t v18 = v15; // 0x8000b8ac
    if (v17 == 0) {
        int32_t v19 = v15 + 8; // 0x8000b8b0
        *uio = v19;
        int32_t * v20 = (int32_t *)(v1 + 4); // 0x8000b8b8
        int32_t v21 = *v20 - 1; // 0x8000b8c0
        *v20 = v21;
        v7 = v15;
        v8 = v14;
        v9 = v13;
        if (v21 == 0) {
            // 0x8000b8cc
            panic("uiomove: ran out of buffers\n");
            v18 = v19;
            goto lab_0x8000b8d8;
        } else {
            goto lab_0x8000b9dc;
        }
    } else {
        goto lab_0x8000b8d8;
    }
  lab_0x8000b8d8:;
    int32_t v22 = v18;
    uint32_t v23 = *v5; // 0x8000b8d8
    int32_t v24; // 0x8000b7d0
    if (v23 < 2) {
        int32_t * v25 = (int32_t *)v22;
        int32_t v26 = *v25;
        int32_t v27; // 0x8000b7d0
        if (*v2 == 0) {
            // 0x8000b94c
            v27 = copyout((int32_t *)v13, (int32_t *)v26, v17);
        } else {
            // 0x8000b964
            v27 = copyin((int32_t *)v26, (char *)v13, v17);
        }
        int32_t result = v27; // 0x8000b974
        if (v27 != 0) {
            // 0x8000b9f4
            return result;
        }
        // 0x8000b978
        *v25 = *v25 + v17;
        v24 = v17;
    } else {
        if (v23 == 2) {
            int32_t * v28 = (int32_t *)v22;
            int32_t v29 = *v28;
            if (*v2 == 0) {
                // 0x8000b904
                memmove((char *)v29, (int32_t *)v13, v17);
            } else {
                // 0x8000b91c
                memmove((char *)v13, (int32_t *)v29, v17);
            }
            // 0x8000b928
            *v28 = *v28 + v17;
            v24 = v17;
        } else {
            // 0x8000b98c
            panic("uiomove: Invalid uio_segflg %d\n", v23);
            v24 = v15;
        }
    }
    int32_t * v30 = (int32_t *)(v22 + 4); // 0x8000b994
    *v30 = *v30 - v17;
    int32_t * v31 = (int32_t *)(v1 + 16); // 0x8000b9a4
    *v31 = *v31 - v17;
    int32_t * v32 = (int32_t *)(v1 + 8); // 0x8000b9b4
    int32_t * v33 = (int32_t *)(v1 + 12); // 0x8000b9b8
    uint32_t v34 = *v33; // 0x8000b9b8
    uint32_t v35 = v34 + v17; // 0x8000b9c0
    *v32 = *v32 + (int32_t)(v35 < v34);
    *v33 = v35;
    v7 = v24;
    v8 = v14 - v17;
    v9 = v17 + v13;
    goto lab_0x8000b9dc;
}