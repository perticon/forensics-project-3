lser_softc * attach_lser_to_lamebus(int lserno,lamebus_softc *sc)

{
  uint32_t slot;
  lser_softc *devdata;
  
  slot = lamebus_probe(sc,1,4,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (lser_softc *)0x0;
  }
  else {
    devdata = (lser_softc *)kmalloc(0x20);
    if (devdata == (lser_softc *)0x0) {
      devdata = (lser_softc *)0x0;
    }
    else {
      devdata->ls_busdata = sc;
      devdata->ls_buspos = slot;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,lser_irq);
    }
  }
  return devdata;
}