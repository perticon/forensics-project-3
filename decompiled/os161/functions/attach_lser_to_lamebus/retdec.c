int32_t * attach_lser_to_lamebus(int32_t lserno, int32_t * sc) {
    int32_t v1 = lamebus_probe(sc, 1, 4, 1, NULL); // 0x80004b5c
    if (v1 < 0) {
        // 0x80004bb8
        return NULL;
    }
    char * v2 = kmalloc(32); // 0x80004b6c
    int32_t * result = NULL; // 0x80004b74
    if (v2 != NULL) {
        int32_t v3 = (int32_t)v2; // 0x80004b6c
        *(int32_t *)(v3 + 12) = (int32_t)sc;
        *(int32_t *)(v3 + 16) = v1;
        lamebus_mark(sc, v1);
        lamebus_attach_interrupt(sc, v1, v2, (void (*)(char *))-0x7fffb3cc);
        result = (int32_t *)v2;
    }
    // 0x80004bb8
    return result;
}