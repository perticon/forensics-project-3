int32_t uiomovezeros(int32_t n, int32_t * uio) {
    int32_t v1 = n; // 0x8000ba3c
    int32_t v2; // 0x8000ba18
    if (*(int32_t *)((int32_t)uio + 24) == 0) {
        goto lab_0x8000ba98;
    } else {
        // 0x8000ba40
        badassert("uio->uio_rw == UIO_READ", "../../lib/uio.c", 130, "uiomovezeros");
        v2 = &g41;
        goto lab_0x8000ba60;
    }
  lab_0x8000ba98:
    // 0x8000ba98
    v2 = v1 < 16;
    int32_t v3 = v1; // 0x8000ba9c
    if (v1 == 0) {
        // 0x8000baa4
        return 0;
    }
    goto lab_0x8000ba60;
  lab_0x8000ba60:;
    int32_t v4 = v2 == 0 ? 16 : v3;
    int32_t v5 = uiomove((char *)&g27, v4, uio); // 0x8000ba78
    v1 = v3 - v4;
    int32_t result = v5; // 0x8000ba80
    if (v5 != 0) {
        // 0x8000baa4
        return result;
    }
    goto lab_0x8000ba98;
}