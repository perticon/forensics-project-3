uiomovezeros(size_t n, struct uio *uio)
{
	/* static, so initialized as zero */
	static char zeros[16];
	size_t amt;
	int result;

	/* This only makes sense when reading */
	KASSERT(uio->uio_rw == UIO_READ);

	while (n > 0) {
		amt = sizeof(zeros);
		if (amt > n) {
			amt = n;
		}
		result = uiomove(zeros, amt, uio);
		if (result) {
			return result;
		}
		n -= amt;
	}

	return 0;
}