int uiomovezeros(size_t n,uio *uio)

{
  uio_rw uVar1;
  int iVar2;
  uint unaff_s0;
  size_t n_00;
  char *unaff_s3;
  
  uVar1 = uio->uio_rw;
  if (uVar1 == UIO_READ) {
    unaff_s3 = uiomovezeros::zeros;
    goto LAB_8000ba98;
  }
  badassert("uio->uio_rw == UIO_READ","../../lib/uio.c",0x82,"uiomovezeros");
  n = unaff_s0;
  while( true ) {
    n_00 = 0x10;
    if (uVar1 != UIO_READ) {
      n_00 = n;
    }
    iVar2 = uiomove(unaff_s3,n_00,uio);
    n = n - n_00;
    if (iVar2 != 0) break;
LAB_8000ba98:
    uVar1 = (uio_rw)(n < 0x10);
    if (n == 0) {
      return 0;
    }
  }
  return iVar2;
}