semaphore * makesem(uint count)

{
  semaphore *psVar1;
  
  psVar1 = sem_create("some-silly-name",count);
  if (psVar1 == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("semunit: whoops: sem_create failed\n");
  }
  return psVar1;
}