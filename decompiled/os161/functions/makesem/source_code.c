makesem(unsigned count)
{
	struct semaphore *sem;

	sem = sem_create(NAMESTRING, count);
	if (sem == NULL) {
		panic("semunit: whoops: sem_create failed\n");
	}
	return sem;
}