int32_t * makesem(int32_t count) {
    int32_t * v1 = sem_create("some-silly-name", count); // 0x800114e0
    int32_t v2 = (int32_t)v1; // 0x800114e8
    if (v1 == NULL) {
        // 0x800114ec
        panic("semunit: whoops: sem_create failed\n");
        v2 = &g41;
    }
    // 0x800114f4
    return (int32_t *)v2;
}