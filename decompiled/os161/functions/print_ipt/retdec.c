void print_ipt(void) {
    // 0x8001cb30
    if (ipt_active == 0) {
        // 0x8001cb5c
        badassert("ipt_active", "../../vm/pt.c", 33, "print_ipt");
    }
    // 0x8001cb78
    int32_t v1; // 0x8001cb30
    kprintf("PID: %d\n", *(int32_t *)(*(int32_t *)(v1 + 84) + 28));
    kprintf("<< IPT >>\n");
    if (nRamFrames <= 0) {
        // 0x8001cbf8
        return;
    }
    int32_t v2 = 0;
    int32_t v3 = 8 * v2 + (int32_t)ipt; // 0x8001cbbc
    int32_t v4 = *(int32_t *)v3; // 0x8001cbc0
    int32_t v5 = nRamFrames; // 0x8001cbcc
    if (v4 != -1) {
        // 0x8001cbd0
        kprintf("%d -   %d   - %d\n", v2, v4, *(int32_t *)(v3 + 4) / 0x1000);
        v5 = nRamFrames;
    }
    int32_t v6 = v2 + 1; // 0x8001cbe0
    while (v6 < v5) {
        // 0x8001cbb0
        v2 = v6;
        v3 = 8 * v2 + (int32_t)ipt;
        v4 = *(int32_t *)v3;
        if (v4 != -1) {
            // 0x8001cbd0
            kprintf("%d -   %d   - %d\n", v2, v4, *(int32_t *)(v3 + 4) / 0x1000);
            v5 = nRamFrames;
        }
        // 0x8001cbe0
        v6 = v2 + 1;
    }
}