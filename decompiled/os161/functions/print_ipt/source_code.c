void print_ipt(void)
{
    int i;
    //  spinlock_acquire(&ipt_lock);
    KASSERT(ipt_active);
    kprintf("PID: %d\n", curproc->p_pid);
    kprintf("<< IPT >>\n");

    for (i = 0; i < nRamFrames; i++)
    {
        if (ipt[i].pid != -1)
        {
            kprintf("%d -   %d   - %d\n", i, ipt[i].pid, ipt[i].vaddr / PAGE_SIZE);
        }
    }
    //   spinlock_release(&ipt_lock);
}