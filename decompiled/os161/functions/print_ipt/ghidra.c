void print_ipt(void)

{
  int iVar1;
  int iVar2;
  int unaff_s7;
  
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x21,"print_ipt");
  }
  kprintf("PID: %d\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x1c));
  kprintf("<< IPT >>\n");
  for (iVar2 = 0; iVar2 < nRamFrames; iVar2 = iVar2 + 1) {
    iVar1 = ipt[iVar2].pid;
    if (iVar1 != -1) {
      kprintf("%d -   %d   - %d\n",iVar2,iVar1,ipt[iVar2].vaddr >> 0xc);
    }
  }
  return;
}