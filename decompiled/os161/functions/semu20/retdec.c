int32_t semu20(int32_t nargs, char ** args) {
    // 0x800128bc
    kprintf("This should assert that we aren't in an interrupt\n");
    int32_t * v1 = makesem(0); // 0x800128d4
    int32_t v2; // 0x800128bc
    *(char *)(v2 + 88) = 1;
    P(v1);
    panic("semu20: P tolerated being in an interrupt handler\n");
    return &g41;
}