int semu20(int nargs,char **args)

{
  semaphore *sem;
  int unaff_s7;
  
  kprintf("This should assert that we aren\'t in an interrupt\n");
  sem = makesem(0);
  *(undefined *)(unaff_s7 + 0x58) = 1;
  P(sem);
                    /* WARNING: Subroutine does not return */
  panic("semu20: P tolerated being in an interrupt handler\n");
}