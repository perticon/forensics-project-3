getfreeppages(unsigned long npages)
{
    paddr_t addr;
    long i, first, found, np = (long)npages;
    int result;

    if (!isTableActive())
        return 0;
    spinlock_acquire(&freemem_lock);

    for (i = 0, first = found = -1; i < nRamFrames; i++)
    {
        result = TestBit(freeRamFrames, i);
        if (result)
        {
            result = TestBit(freeRamFrames, i - 1);
            if (i == 0 || !result)
                first = i; /* set first free in an interval */
            if (i - first + 1 >= np)
            {
                found = first;
                break;
            }
        }
    }

    if (found >= 0)
    {
        for (i = found; i < found + np; i++)
        {
            ClearBit(freeRamFrames, i);
        }
        allocSize[found] = np;
        addr = (paddr_t)found * PAGE_SIZE;
    }
    else
    {
        addr = 0;
    }

    spinlock_release(&freemem_lock);

    return addr;
}