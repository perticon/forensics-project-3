paddr_t getfreeppages(ulong npages)

{
  int *A;
  int iVar1;
  paddr_t pVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  
  iVar1 = isTableActive();
  pVar2 = 0;
  if (iVar1 != 0) {
    spinlock_acquire(&freemem_lock);
    iVar1 = -1;
    for (iVar5 = 0; A = freeRamFrames, iVar5 < nRamFrames; iVar5 = iVar5 + 1) {
      iVar3 = TestBit(freeRamFrames,iVar5);
      if (iVar3 != 0) {
        iVar3 = TestBit(A,iVar5 + -1);
        if ((iVar5 == 0) || (iVar4 = iVar5 - iVar1, iVar3 == 0)) {
          iVar4 = 0;
          iVar1 = iVar5;
        }
        if ((int)npages <= iVar4 + 1) goto LAB_8001b37c;
      }
    }
    iVar1 = -1;
LAB_8001b37c:
    if (iVar1 < 0) {
      pVar2 = 0;
    }
    else {
      for (iVar5 = iVar1; iVar5 < (int)(iVar1 + npages); iVar5 = iVar5 + 1) {
        ClearBit(freeRamFrames,iVar5);
      }
      allocSize[iVar1] = npages;
      pVar2 = iVar1 << 0xc;
    }
    spinlock_release(&freemem_lock);
  }
  return pVar2;
}