int32_t getfreeppages(int32_t npages) {
    // 0x8001b2ac
    if (isTableActive() == 0) {
        // 0x8001b3dc
        return 0;
    }
    // 0x8001b2e0
    spinlock_acquire(&freemem_lock);
    int32_t v1 = 0; // 0x8001b374
    int32_t v2 = -1; // 0x8001b374
    if (g20 <= 0) {
        // 0x8001b3cc
        spinlock_release(&freemem_lock);
        // 0x8001b3dc
        return 0;
    }
    int32_t v3; // 0x8001b2ac
    while (true) {
        int32_t v4 = v2;
        int32_t v5 = v1;
        int32_t v6; // 0x8001b2ac
        if (TestBit(freeRamFrames, v5) == 0) {
            // 0x8001b304
            v6 = v5 + 1;
            v2 = v4;
        } else {
            // 0x8001b320
            v3 = v5 == 0 | TestBit(freeRamFrames, v5 - 1) == 0 ? v5 : v4;
            int32_t v7 = v5 + 1;
            v6 = v7;
            v2 = v3;
            if (v7 - v3 >= npages) {
                // break -> 0x8001b37c
                break;
            }
        }
        // 0x8001b360
        v1 = v6;
        if (v1 >= g20) {
            goto lab_0x8001b3cc;
        }
    }
    // 0x8001b37c
    if (v3 <= 0xffffffff) {
      lab_0x8001b3cc:
        // 0x8001b3cc
        spinlock_release(&freemem_lock);
        // 0x8001b3dc
        return 0;
    }
    int32_t v8 = v3 + npages; // 0x8001b3a0
    if (v3 < v8) {
        ClearBit(freeRamFrames, v3);
        int32_t v9 = v3 + 1; // 0x8001b39c
        int32_t v10 = v9; // 0x8001b3b0
        while (v9 < v8) {
            // 0x8001b38c
            ClearBit(freeRamFrames, v10);
            v9 = v10 + 1;
            v10 = v9;
        }
    }
    // 0x8001b3b4
    *(int32_t *)(4 * v3 + (int32_t)allocSize) = npages;
    // 0x8001b3cc
    spinlock_release(&freemem_lock);
    // 0x8001b3dc
    return 0x1000 * v3;
}