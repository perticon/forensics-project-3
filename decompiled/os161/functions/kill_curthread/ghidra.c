void kill_curthread(vaddr_t epc,uint code,vaddr_t vaddr)

{
  uint uVar1;
  char *pcVar2;
  
  uVar1 = (uint)(code < 0xd);
  if (uVar1 == 0) {
    pcVar2 = "../../arch/mips/locore/trap.c";
    badassert("code < NTRAPCODES","../../arch/mips/locore/trap.c",0x51,"kill_curthread");
    code = (uint)pcVar2;
  }
  if (uVar1 != 0) {
                    /* WARNING: Could not recover jumptable at 0x8001e7ec. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    (*(code *)(&PTR_LAB_800220c0)[code])();
    return;
  }
  kprintf("Fatal user mode trap %u sig %d (%s, epc 0x%x, vaddr 0x%x)\n");
                    /* WARNING: Subroutine does not return */
  panic("I don\'t know how to handle this\n");
}