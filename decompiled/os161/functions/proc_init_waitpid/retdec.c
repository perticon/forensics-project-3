void proc_init_waitpid(int32_t * proc, char * name) {
    int32_t v1 = (int32_t)proc;
    spinlock_acquire(&g29);
    int32_t v2 = g28 + 1; // 0x8000ca98
    int32_t * v3 = (int32_t *)(v1 + 28); // 0x8000caa4
    *v3 = 0;
    int32_t v4 = v2 < 100 ? v2 : 1;
    int32_t v5 = g28; // 0x8000cb04
    if (v5 != v4) {
        int32_t v6 = v4;
        int32_t * v7 = (int32_t *)(4 * v6 + (int32_t)&processTable + 4); // 0x8000cab8
        while (*v7 != 0) {
            int32_t v8 = v6 + 1; // 0x8000cac4
            int32_t v9 = v8 < 100 ? v8 : 1;
            if (v5 == v9) {
                goto lab_0x8000cb14;
            }
            v6 = v9;
            v7 = (int32_t *)(4 * v6 + (int32_t)&processTable + 4);
        }
        // 0x8000cac8
        *v7 = v1;
        g28 = v6;
        *v3 = v6;
    }
  lab_0x8000cb14:
    // 0x8000cb14
    spinlock_release(&g29);
    if (*v3 == 0) {
        // 0x8000cb30
        panic("too many processes. proc table is full\n");
    }
    // 0x8000cb38
    *(int32_t *)(v1 + 24) = 0;
    *(int32_t *)(v1 + 32) = (int32_t)cv_create(name);
    *(int32_t *)(v1 + 36) = (int32_t)lock_create(name);
}