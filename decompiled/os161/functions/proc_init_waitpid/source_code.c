proc_init_waitpid(struct proc *proc, const char *name)
{
#if OPT_WAITPID
	/* search a free index in table using a circular strategy */
	int i;
	spinlock_acquire(&processTable.lk);
	i = processTable.last_i + 1;
	proc->p_pid = 0;
	if (i >= MAX_PROC)
		i = 1;
	while (i != processTable.last_i)
	{
		if (processTable.proc[i] == NULL)
		{
			processTable.proc[i] = proc;
			processTable.last_i = i;
			proc->p_pid = i;
			break;
		}
		i++;
		if (i >= MAX_PROC)
			i = 1;
	}
	spinlock_release(&processTable.lk);
	if (proc->p_pid == 0)
	{
		panic("too many processes. proc table is full\n");
	}
	proc->p_status = 0;

	proc->p_cv = cv_create(name);
	proc->p_wlock = lock_create(name);

#else
	(void)proc;
	(void)name;
#endif
}