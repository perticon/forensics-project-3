void proc_init_waitpid(proc *proc,char *name)

{
  int iVar1;
  int iVar2;
  cv *pcVar3;
  lock *plVar4;
  
  spinlock_acquire(&processTable.lk);
  iVar1 = processTable.last_i + 1;
  proc->p_pid = 0;
  if (99 < iVar1) {
    iVar1 = 1;
  }
  do {
    if (processTable.last_i == iVar1) {
LAB_8000cb14:
      spinlock_release(&processTable.lk);
      if (proc->p_pid != 0) {
        proc->p_status = 0;
        pcVar3 = cv_create(name);
        proc->p_cv = pcVar3;
        plVar4 = lock_create(name);
        proc->p_wlock = plVar4;
        return;
      }
                    /* WARNING: Subroutine does not return */
      panic("too many processes. proc table is full\n");
    }
    iVar2 = iVar1 + 1;
    if (processTable.proc[iVar1] == (proc *)0x0) {
      processTable.proc[iVar1] = proc;
      processTable.last_i = iVar1;
      proc->p_pid = iVar1;
      goto LAB_8000cb14;
    }
    iVar1 = iVar2;
    if (99 < iVar2) {
      iVar1 = 1;
    }
  } while( true );
}