static link get_free_link(void)
{
    link tmp;

    tmp = free_list_head->next;
    if (tmp == free_list_tail)
    {
        panic("no free link in ipt_hash");
    }
    free_list_head->next = tmp->next;

    return tmp;
}