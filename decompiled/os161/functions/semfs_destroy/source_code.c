semfs_destroy(struct semfs *semfs)
{
	struct semfs_sem *sem;
	struct semfs_direntry *dent;
	unsigned i, num;

	num = semfs_semarray_num(semfs->semfs_sems);
	for (i=0; i<num; i++) {
		sem = semfs_semarray_get(semfs->semfs_sems, i);
		semfs_sem_destroy(sem);
	}
	semfs_semarray_setsize(semfs->semfs_sems, 0);

	num = semfs_direntryarray_num(semfs->semfs_dents);
	for (i=0; i<num; i++) {
		dent = semfs_direntryarray_get(semfs->semfs_dents, i);
		semfs_direntry_destroy(dent);
	}
	semfs_direntryarray_setsize(semfs->semfs_dents, 0);

	semfs_direntryarray_destroy(semfs->semfs_dents);
	lock_destroy(semfs->semfs_dirlock);
	semfs_semarray_destroy(semfs->semfs_sems);
	vnodearray_destroy(semfs->semfs_vnodes);
	lock_destroy(semfs->semfs_tablelock);
	kfree(semfs);
}