void semfs_destroy(semfs *semfs)

{
  semfs_semarray *psVar1;
  semfs_direntryarray *psVar2;
  uint uVar3;
  array *paVar4;
  uint uVar5;
  
  uVar5 = (semfs->semfs_sems->arr).num;
  for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
    psVar1 = semfs->semfs_sems;
    if ((psVar1->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    semfs_sem_destroy((semfs_sem *)(psVar1->arr).v[uVar3]);
  }
  array_setsize((array *)semfs->semfs_sems,0);
  uVar5 = (semfs->semfs_dents->arr).num;
  for (uVar3 = 0; uVar3 < uVar5; uVar3 = uVar3 + 1) {
    psVar2 = semfs->semfs_dents;
    if ((psVar2->arr).num <= uVar3) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    semfs_direntry_destroy((semfs_direntry *)(psVar2->arr).v[uVar3]);
  }
  array_setsize((array *)semfs->semfs_dents,0);
  paVar4 = (array *)semfs->semfs_dents;
  array_cleanup(paVar4);
  kfree(paVar4);
  lock_destroy(semfs->semfs_dirlock);
  paVar4 = (array *)semfs->semfs_sems;
  array_cleanup(paVar4);
  kfree(paVar4);
  paVar4 = (array *)semfs->semfs_vnodes;
  array_cleanup(paVar4);
  kfree(paVar4);
  lock_destroy(semfs->semfs_tablelock);
  kfree(semfs);
  return;
}