void semfs_destroy(int32_t * semfs) {
    int32_t v1 = (int32_t)semfs;
    int32_t * v2 = (int32_t *)(v1 + 16); // 0x800054cc
    int32_t v3 = *v2;
    uint32_t v4 = *(int32_t *)(v3 + 4); // 0x800054d4
    int32_t v5 = v3; // 0x80005538
    if (v4 != 0) {
        uint32_t v6 = 0;
        int32_t v7 = v3; // 0x800054f8
        if (v6 >= *(int32_t *)(v3 + 4)) {
            // 0x800054fc
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v7 = &g41;
        }
        int32_t v8 = *(int32_t *)v7; // 0x80005518
        int32_t v9 = v6 + 1; // 0x8000552c
        semfs_sem_destroy((int32_t *)*(int32_t *)(v8 + 4 * v6));
        int32_t v10 = *v2;
        v5 = v10;
        while (v9 < v4) {
            // 0x800054e0
            v6 = v9;
            v7 = v10;
            if (v6 >= *(int32_t *)(v10 + 4)) {
                // 0x800054fc
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v7 = &g41;
            }
            // 0x80005518
            v8 = *(int32_t *)v7;
            v9 = v6 + 1;
            semfs_sem_destroy((int32_t *)*(int32_t *)(v8 + 4 * v6));
            v10 = *v2;
            v5 = v10;
        }
    }
    // 0x8000553c
    array_setsize((int32_t *)v5, 0);
    int32_t * v11 = (int32_t *)(v1 + 24); // 0x80005548
    int32_t v12 = *v11;
    uint32_t v13 = *(int32_t *)(v12 + 4); // 0x80005550
    int32_t v14 = v12; // 0x800055b4
    if (v13 != 0) {
        uint32_t v15 = 0;
        int32_t v16 = v12; // 0x80005574
        if (v15 >= *(int32_t *)(v12 + 4)) {
            // 0x80005578
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v16 = &g41;
        }
        int32_t v17 = *(int32_t *)v16; // 0x80005594
        int32_t v18 = v15 + 1; // 0x800055a8
        semfs_direntry_destroy((int32_t *)*(int32_t *)(v17 + 4 * v15));
        int32_t v19 = *v11;
        v14 = v19;
        while (v18 < v13) {
            // 0x8000555c
            v15 = v18;
            v16 = v19;
            if (v15 >= *(int32_t *)(v19 + 4)) {
                // 0x80005578
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v16 = &g41;
            }
            // 0x80005594
            v17 = *(int32_t *)v16;
            v18 = v15 + 1;
            semfs_direntry_destroy((int32_t *)*(int32_t *)(v17 + 4 * v15));
            v19 = *v11;
            v14 = v19;
        }
    }
    // 0x800055b8
    array_setsize((int32_t *)v14, 0);
    int32_t v20 = *v11; // 0x800055c4
    array_cleanup((int32_t *)v20);
    kfree((char *)v20);
    lock_destroy((int32_t *)*(int32_t *)(v1 + 20));
    int32_t v21 = *v2; // 0x800055e4
    array_cleanup((int32_t *)v21);
    kfree((char *)v21);
    int32_t v22 = *(int32_t *)(v1 + 12); // 0x800055f8
    array_cleanup((int32_t *)v22);
    kfree((char *)v22);
    lock_destroy((int32_t *)*(int32_t *)(v1 + 8));
    kfree((char *)semfs);
}