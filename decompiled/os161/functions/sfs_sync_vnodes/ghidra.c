int sfs_sync_vnodes(sfs_fs *sfs)

{
  vnodearray *pvVar1;
  uint uVar2;
  vnode *v;
  uint uVar3;
  
  uVar3 = (sfs->sfs_vnodes->arr).num;
  for (uVar2 = 0; uVar2 < uVar3; uVar2 = uVar2 + 1) {
    pvVar1 = sfs->sfs_vnodes;
    if ((pvVar1->arr).num <= uVar2) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    v = (vnode *)(pvVar1->arr).v[uVar2];
    vnode_check(v,"fsync");
    (*v->vn_ops->vop_fsync)(v);
  }
  return 0;
}