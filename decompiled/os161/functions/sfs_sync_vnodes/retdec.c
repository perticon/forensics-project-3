int32_t sfs_sync_vnodes(int32_t * sfs) {
    int32_t * v1 = (int32_t *)((int32_t)sfs + 528); // 0x80007e14
    int32_t v2 = *v1; // 0x80007e14
    uint32_t v3 = *(int32_t *)(v2 + 4); // 0x80007e1c
    if (v3 == 0) {
        // 0x80007eb0
        return 0;
    }
    int32_t v4 = 0; // 0x80007eac
    int32_t v5 = v2; // 0x80007e48
    if (v4 >= *(int32_t *)(v2 + 4)) {
        // 0x80007e4c
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v5 = &g41;
    }
    int32_t v6 = *(int32_t *)v5; // 0x80007e68
    vnode_check((int32_t *)*(int32_t *)(v6 + 4 * v4), "fsync");
    v4++;
    while (v4 < v3) {
        int32_t v7 = *v1; // 0x80007df4
        v5 = v7;
        if (v4 >= *(int32_t *)(v7 + 4)) {
            // 0x80007e4c
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v5 = &g41;
        }
        // 0x80007e68
        v6 = *(int32_t *)v5;
        vnode_check((int32_t *)*(int32_t *)(v6 + 4 * v4), "fsync");
        v4++;
    }
    // 0x80007eb0
    return 0;
}