void proc_end_waitpid(int32_t * proc) {
    int32_t v1 = (int32_t)proc;
    spinlock_acquire(&g29);
    int32_t v2 = *(int32_t *)(v1 + 28); // 0x8000c9ec
    int32_t v3 = 4 * v2; // 0x8000ca00
    if (v2 >= 101) {
        // 0x8000ca04
        badassert("i > 0 && i <= MAX_PROC", "../../proc/proc.c", 88, "proc_end_waitpid");
        v3 = &g41;
    }
    // 0x8000ca24
    *(int32_t *)(v3 + (int32_t)&processTable + 4) = 0;
    spinlock_release(&g29);
    cv_destroy((int32_t *)*(int32_t *)(v1 + 32));
    lock_destroy((int32_t *)*(int32_t *)(v1 + 36));
}