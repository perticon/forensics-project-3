proc_end_waitpid(struct proc *proc)
{
#if OPT_WAITPID
	/* remove the process from the table */
	int i;
	spinlock_acquire(&processTable.lk);
	i = proc->p_pid;
	KASSERT(i > 0 && i <= MAX_PROC);
	processTable.proc[i] = NULL;
	spinlock_release(&processTable.lk);

#if USE_SEMAPHORE_FOR_WAITPID
	sem_destroy(proc->p_sem);
#else
	cv_destroy(proc->p_cv);
	lock_destroy(proc->p_wlock);
#endif
#else
	(void)proc;
#endif
}