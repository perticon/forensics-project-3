void proc_end_waitpid(proc *proc)

{
  int iVar1;
  
  spinlock_acquire(&processTable.lk);
  iVar1 = proc->p_pid << 2;
  if (99 < proc->p_pid - 1U) {
    badassert("i > 0 && i <= MAX_PROC","../../proc/proc.c",0x58,"proc_end_waitpid");
  }
  *(undefined4 *)((int)processTable.proc + iVar1) = 0;
  spinlock_release(&processTable.lk);
  cv_destroy(proc->p_cv);
  lock_destroy(proc->p_wlock);
  return;
}