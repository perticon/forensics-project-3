int sfs_itrunc(sfs_vnode *sv,off_t len)

{
  bool bVar1;
  bool bVar2;
  uint uVar3;
  uint uVar4;
  int in_a2;
  int iVar5;
  uint in_a3;
  sfs_fs *sfs;
  daddr_t dVar6;
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  uVar3 = in_a3 + 0x1ff;
  iVar5 = (uint)(uVar3 < in_a3) + in_a2;
  uVar4 = uVar3;
  if (iVar5 < 0) {
    uVar4 = in_a3 + 0x3fe;
    iVar5 = (uint)(uVar4 < uVar3) + iVar5;
  }
  uVar3 = iVar5 << 0x17 | uVar4 >> 9;
  vfs_biglock_acquire();
  for (uVar4 = 0; uVar4 < 0xf; uVar4 = uVar4 + 1) {
    dVar6 = (sv->sv_i).sfi_direct[uVar4];
    if ((uVar3 <= uVar4) && (dVar6 != 0)) {
      sfs_bfree(sfs,dVar6);
      (sv->sv_i).sfi_direct[uVar4] = 0;
      sv->sv_dirty = true;
    }
  }
  dVar6 = (sv->sv_i).sfi_indirect;
  if ((uVar3 < 0x8e) && (dVar6 != 0)) {
    iVar5 = sfs_readblock(sfs,dVar6,sfs_itrunc::idbuf,0x200);
    if (iVar5 != 0) {
      vfs_biglock_release();
      return iVar5;
    }
    bVar2 = false;
    bVar1 = false;
    for (uVar4 = 0; uVar4 < 0x80; uVar4 = uVar4 + 1) {
      iVar5 = uVar4 * 4;
      if (uVar3 < uVar4 + 0xf) {
        iVar5 = uVar4 << 2;
        if (sfs_itrunc::idbuf[uVar4] != 0) {
          sfs_bfree(sfs,sfs_itrunc::idbuf[uVar4]);
          sfs_itrunc::idbuf[uVar4] = 0;
          bVar2 = true;
          iVar5 = uVar4 << 2;
        }
      }
      if (*(int *)((int)sfs_itrunc::idbuf + iVar5) != 0) {
        bVar1 = true;
      }
    }
    if (bVar1) {
      if ((bVar2) && (iVar5 = sfs_writeblock(sfs,dVar6,sfs_itrunc::idbuf,0x200), iVar5 != 0)) {
        vfs_biglock_release();
        return iVar5;
      }
    }
    else {
      sfs_bfree(sfs,dVar6);
      (sv->sv_i).sfi_indirect = 0;
      sv->sv_dirty = true;
    }
  }
  (sv->sv_i).sfi_size = in_a3;
  sv->sv_dirty = true;
  vfs_biglock_release();
  return 0;
}