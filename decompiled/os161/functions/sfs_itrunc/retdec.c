int32_t sfs_itrunc(int32_t * sv, int64_t len) {
    // 0x80007760
    int32_t v1; // 0x80007760
    uint32_t v2 = v1;
    int32_t v3 = (int32_t)sv;
    int32_t v4 = v2 + 511; // 0x8000779c
    int32_t v5 = (int32_t)(v2 > 0xfffffe00) + (int32_t)len; // 0x800077a4
    int32_t v6 = v5; // 0x800077b0
    int32_t v7 = v4; // 0x800077b0
    if (v5 <= 0xffffffff) {
        // 0x800077b4
        v7 = v2 + 1022;
        v6 = v5 + (int32_t)(v7 < v4);
    }
    uint32_t v8 = v7 / 512 | 0x800000 * v6; // 0x800077cc
    vfs_biglock_acquire();
    int32_t * v9 = (int32_t *)*(int32_t *)*(int32_t *)(v3 + 12);
    char * v10 = (char *)(v3 + 540);
    for (int32_t i = 0; i < 15; i++) {
        int32_t * v11 = (int32_t *)(v3 + 32 + 4 * i); // 0x800077e4
        int32_t v12 = *v11; // 0x800077e4
        if (i >= v8 && v12 != 0) {
            // 0x800077fc
            sfs_bfree(v9, v12);
            *v11 = 0;
            *v10 = 1;
        }
    }
    int32_t * v13 = (int32_t *)(v3 + 92); // 0x80007828
    int32_t v14 = *v13; // 0x80007828
    int32_t v15; // 0x80007760
    int32_t v16; // 0x80007760
    int32_t v17; // 0x80007760
    int32_t v18; // 0x80007760
    int32_t v19; // 0x80007760
    int32_t v20; // 0x80007760
    int32_t v21; // 0x80007760
    int32_t v22; // 0x80007760
    int32_t v23; // 0x80007760
    if (v8 < 142 && v14 != 0) {
        int32_t result = sfs_readblock(v9, v14, (char *)&g23, 512); // 0x80007850
        v18 = 0;
        v17 = 0;
        v23 = 0;
        if (result != 0) {
            // 0x8000785c
            vfs_biglock_release();
            // 0x8000794c
            return result;
        }
        while (true) {
          lab_0x8000786c:;
            int32_t v24 = v23;
            v19 = v18;
            int32_t * v25 = (int32_t *)(4 * v17 + (int32_t)&g23);
            int32_t v26 = *v25;
            if (v8 >= v17 + 15) {
                // 0x800078a8
                v22 = v24;
                v20 = v24;
                v15 = 1;
                if (v26 == 0) {
                    goto lab_0x800078a8_2;
                } else {
                    goto lab_dec_label_pc_unknown;
                }
            } else {
                // 0x80007878
                v22 = v24;
                if (v26 != 0) {
                    // 0x8000788c
                    sfs_bfree(v9, v26);
                    *v25 = 0;
                    v22 = 1;
                }
                goto lab_0x800078a8_2;
            }
        }
      lab_0x800078e8:
        if (v16 == 0) {
            // 0x800078f0
            sfs_bfree(v9, v14);
            *v13 = 0;
            *v10 = 1;
        } else {
            if (v21 != 0) {
                int32_t result2 = sfs_writeblock(v9, v14, (char *)&g23, 512); // 0x8000791c
                if (result2 != 0) {
                    // 0x80007928
                    vfs_biglock_release();
                    // 0x8000794c
                    return result2;
                }
            }
        }
    }
    // 0x80007938
    *v10 = 1;
    vfs_biglock_release();
    // 0x8000794c
    return 0;
  lab_0x800078a8_2:
    // 0x800078a8
    v20 = v22;
    v15 = v19;
    goto lab_dec_label_pc_unknown;
  lab_dec_label_pc_unknown:
    v16 = v15;
    v21 = v20;
    int32_t v27 = v17 + 1; // 0x800078c4
    v18 = v16;
    v17 = v27;
    v23 = v21;
    if (v27 >= 128) {
        // break -> 0x800078e8
        goto lab_0x800078e8;
    }
    goto lab_0x8000786c;
}