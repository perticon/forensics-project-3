fstest_write(const char *fs, const char *namesuffix,
	     int stridesize, int stridepos)
{
	struct vnode *vn;
	int err;
	int i;
	size_t shouldbytes=0;
	size_t bytes=0;
	off_t pos=0;
	char name[32];
	char buf[32];
	struct iovec iov;
	struct uio ku;
	int flags;

	KASSERT(sizeof(buf) > strlen(SLOGAN));

	MAKENAME();

	flags = O_WRONLY|O_CREAT;
	if (stridesize == 1) {
		flags |= O_TRUNC;
	}

	/* vfs_open destroys the string it's passed */
	strcpy(buf, name);
	err = vfs_open(buf, flags, 0664, &vn);
	if (err) {
		kprintf("Could not open %s for write: %s\n",
			name, strerror(err));
		return -1;
	}

	for (i=0; i<NCHUNKS; i++) {
		if (i % stridesize != stridepos) {
			pos += strlen(SLOGAN);
			continue;
		}
		strcpy(buf, SLOGAN);
		rotate(buf, i);
		uio_kinit(&iov, &ku, buf, strlen(SLOGAN), pos, UIO_WRITE);
		err = VOP_WRITE(vn, &ku);
		if (err) {
			kprintf("%s: Write error: %s\n", name, strerror(err));
			vfs_close(vn);
			vfs_remove(name);
			return -1;
		}

		if (ku.uio_resid > 0) {
			kprintf("%s: Short write: %lu bytes left over\n",
				name, (unsigned long) ku.uio_resid);
			vfs_close(vn);
			vfs_remove(name);
			return -1;
		}

		bytes += (ku.uio_offset - pos);
		shouldbytes += strlen(SLOGAN);
		pos = ku.uio_offset;
	}

	vfs_close(vn);

	if (bytes != shouldbytes) {
		kprintf("%s: %lu bytes written, should have been %lu!\n",
			name, (unsigned long) bytes,
			(unsigned long) (NCHUNKS*strlen(SLOGAN)));
		vfs_remove(name);
		return -1;
	}
	kprintf("%s: %lu bytes written\n", name, (unsigned long) bytes);

	return 0;
}