int fstest_write(char *fs,char *namesuffix,int stridesize,int stridepos)

{
  bool bVar1;
  size_t sVar2;
  int iVar3;
  char *pcVar4;
  int iVar5;
  uint uVar6;
  int iVar7;
  int iVar8;
  vnode *vn;
  char name [32];
  char buf [32];
  iovec iov;
  uio ku;
  
  sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
  pcVar4 = name;
  if (0x1f < sVar2) {
    pcVar4 = "sizeof(buf) > strlen(SLOGAN)";
    badassert("sizeof(buf) > strlen(SLOGAN)","../../test/fstest.c",0x96,"fstest_write");
  }
  fstest_makename(pcVar4,0x20,fs,namesuffix);
  iVar5 = 5;
  if (stridesize == 1) {
    iVar5 = 0x15;
  }
  strcpy(buf,name);
  iVar3 = vfs_open(buf,iVar5,0x1b4,&vn);
  iVar5 = 0;
  if (iVar3 == 0) {
    uVar6 = 0;
    iVar7 = 0;
    iVar8 = 0;
    for (iVar3 = 0; iVar3 < 0x2d0; iVar3 = iVar3 + 1) {
      if (stridesize == 0) {
        trap(0x1c00);
      }
      if (iVar3 % stridesize == stridepos) {
        strcpy(buf,"HODIE MIHI - CRAS TIBI\n");
        rotate(buf,iVar3);
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        uio_kinit(&iov,&ku,buf,sVar2,CONCAT44(iVar5,uVar6),UIO_WRITE);
        vnode_check(vn,s_console_write_80022690 + 8);
        iVar5 = (*vn->vn_ops->vop_write)(vn,&ku);
        if (iVar5 != 0) {
          pcVar4 = strerror(iVar5);
          kprintf("%s: Write error: %s\n",name,pcVar4);
          vfs_close(vn);
          vfs_remove(name);
          return -1;
        }
        if (ku.uio_resid != 0) {
          kprintf("%s: Short write: %lu bytes left over\n",name);
          vfs_close(vn);
          vfs_remove(name);
          return -1;
        }
        iVar7 = iVar7 + (ku.uio_offset._4_4_ - uVar6);
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        iVar8 = iVar8 + sVar2;
        uVar6 = ku.uio_offset._4_4_;
        iVar5 = ku.uio_offset._0_4_;
      }
      else {
        sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
        bVar1 = uVar6 + sVar2 < uVar6;
        uVar6 = uVar6 + sVar2;
        iVar5 = (uint)bVar1 + iVar5;
      }
    }
    vfs_close(vn);
    if (iVar7 == iVar8) {
      kprintf("%s: %lu bytes written\n",name,iVar7);
      iVar5 = 0;
    }
    else {
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      kprintf("%s: %lu bytes written, should have been %lu!\n",name,iVar7,sVar2 * 0x2d0);
      vfs_remove(name);
      iVar5 = -1;
    }
  }
  else {
    pcVar4 = strerror(iVar3);
    kprintf("Could not open %s for write: %s\n",name,pcVar4);
    iVar5 = -1;
  }
  return iVar5;
}