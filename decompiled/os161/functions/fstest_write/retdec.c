int32_t fstest_write(char * fs, char * namesuffix, int32_t stridesize, int32_t stridepos) {
    char buf[32]; // bp-124, 0x8000f470
    char name[32]; // bp-156, 0x8000f470
    uint32_t v1 = strlen("HODIE MIHI - CRAS TIBI\n"); // 0x8000f4b0
    int32_t v2 = (int32_t)&name; // 0x8000f4bc
    if (v1 >= 32) {
        // 0x8000f4c0
        badassert("sizeof(buf) > strlen(SLOGAN)", "../../test/fstest.c", 150, "fstest_write");
        v2 = (int32_t)"sizeof(buf) > strlen(SLOGAN)";
    }
    // 0x8000f4e0
    fstest_makename((char *)v2, 32, fs, namesuffix);
    strcpy(buf, name);
    int32_t * vn; // bp-160, 0x8000f470
    int32_t v3 = vfs_open(buf, stridesize == 1 ? 21 : 5, 436, &vn); // 0x8000f51c
    if (v3 != 0) {
        char * v4 = strerror(v3); // 0x8000f52c
        kprintf("Could not open %s for write: %s\n", name, v4);
        // 0x8000f734
        return -1;
    }
    int32_t v5 = 0; // 0x8000f694
    int32_t v6 = 0;
    int32_t v7 = 0;
    int32_t v8 = 0; // 0x8000f470
    int32_t v9 = 0; // 0x8000f56c
    int32_t result; // 0x8000f470
    int32_t v10; // 0x8000f5e4
    while (true) {
        uint32_t v11 = v9;
        int32_t v12 = v8;
        int32_t v13 = v7;
        int32_t v14 = v6;
        int32_t v15 = v5;
        int32_t v16; // 0x8000f470
        int32_t v17; // 0x8000f470
        if (v15 % stridesize == stridepos) {
            // 0x8000f580
            strcpy(buf, "HODIE MIHI - CRAS TIBI\n");
            rotate(buf, v15);
            int32_t v18 = strlen("HODIE MIHI - CRAS TIBI\n"); // 0x8000f59c
            int32_t iov; // bp-92, 0x8000f470
            int32_t ku; // bp-80, 0x8000f470
            uio_kinit(&iov, &ku, buf, v18, (int64_t)v12, 1);
            vnode_check(vn, "write");
            v10 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 24);
            if (v10 != 0) {
                // break -> 0x8000f5fc
                break;
            }
            int32_t v19; // 0x8000f470
            if (v19 != 0) {
                // 0x8000f644
                kprintf("%s: Short write: %lu bytes left over\n", name, v19);
                vfs_close(vn);
                vfs_remove(name);
                result = -1;
                return result;
            }
            // 0x8000f66c
            int64_t v20; // 0x8000f470
            v8 = v20;
            v17 = v13 + v19 - v11;
            v16 = strlen("HODIE MIHI - CRAS TIBI\n") + v14;
        } else {
            // 0x8000f564
            v9 = strlen("HODIE MIHI - CRAS TIBI\n") + v11;
            v8 = v12 + (int32_t)(v9 < v11);
            v17 = v13;
            v16 = v14;
        }
        // 0x8000f690
        v6 = v16;
        v7 = v17;
        v5 = v15 + 1;
        if (v5 >= 720) {
            // 0x8000f6c8
            vfs_close(vn);
            if (v7 == v6) {
                // 0x8000f720
                kprintf("%s: %lu bytes written\n", name, v7);
                result = 0;
                return result;
            } else {
                int32_t v21 = strlen("HODIE MIHI - CRAS TIBI\n"); // 0x8000f6e4
                kprintf("%s: %lu bytes written, should have been %lu!\n", name, v7, 720 * v21);
                vfs_remove(name);
                result = -1;
                return result;
            }
        }
    }
    char * v22 = strerror(v10); // 0x8000f600
    kprintf("%s: Write error: %s\n", name, v22);
    vfs_close(vn);
    vfs_remove(name);
    result = -1;
  lab_0x8000f734:
    // 0x8000f734
    return result;
}