void lamebus_unmask_interrupt(lamebus_softc *lamebus,int slot)

{
  uint32_t uVar1;
  char *pcVar2;
  
  if (0x1f < (uint)slot) {
    pcVar2 = "slot >= 0 && slot < LB_NSLOTS";
    badassert("slot >= 0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x1a3,
              "lamebus_unmask_interrupt");
    lamebus = (lamebus_softc *)pcVar2;
  }
  spinlock_acquire(&lamebus->ls_lock);
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7e0c);
  lamebus_write_register(lamebus,0x1f,0x7e0c,1 << (slot & 0x1fU) | uVar1);
  spinlock_release(&lamebus->ls_lock);
  return;
}