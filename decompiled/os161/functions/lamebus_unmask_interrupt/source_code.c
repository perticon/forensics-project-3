lamebus_unmask_interrupt(struct lamebus_softc *lamebus, int slot)
{
	uint32_t bits, mask = ((uint32_t)1) << slot;
	KASSERT(slot >= 0 && slot < LB_NSLOTS);

	spinlock_acquire(&lamebus->ls_lock);
	bits = read_ctl_register(lamebus, CTLREG_IRQE);
	bits |= mask;
	write_ctl_register(lamebus, CTLREG_IRQE, bits);
	spinlock_release(&lamebus->ls_lock);
}