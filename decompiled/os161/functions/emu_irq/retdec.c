void emu_irq(char * dev) {
    int32_t v1 = (int32_t)dev;
    int32_t * v2 = (int32_t *)(v1 + 4); // 0x8000391c
    *(int32_t *)(v1 + 24) = lamebus_read_register((int32_t *)dev, *v2, 16);
    lamebus_write_register((int32_t *)dev, *v2, 16, 0);
    V((int32_t *)*(int32_t *)(v1 + 16));
}