int32_t * pid_getas(int32_t pid) {
    int32_t v1 = pid; // 0x8000cc20
    int32_t v2 = pid - 1; // 0x8000cc20
    int32_t v3; // 0x8000cc04
    if (*(int32_t *)(*(int32_t *)(v3 + 84) + 28) != pid) {
        // 0x8000cc24
        badassert("pid == curproc->p_pid", "../../proc/proc.c", 151, "pid_getas");
        v1 = (int32_t)"pid == curproc->p_pid";
        v2 = &g41;
    }
    int32_t v4 = 4 * v1; // 0x8000cc4c
    if (v2 >= 100) {
        // 0x8000cc50
        badassert("pid > 0 && pid < MAX_PROC + 1", "../../proc/proc.c", 152, "pid_getas");
        v4 = (int32_t)"pid > 0 && pid < MAX_PROC + 1";
    }
    int32_t v5 = *(int32_t *)(v4 + (int32_t)&processTable + 4); // 0x8000cc7c
    return (int32_t *)*(int32_t *)(v5 + 16);
}