addrspace * pid_getas(pid_t pid)

{
  uint uVar1;
  char *pcVar2;
  int unaff_s7;
  
  uVar1 = pid - 1;
  if (*(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c) != pid) {
    pcVar2 = "pid == curproc->p_pid";
    badassert("pid == curproc->p_pid","../../proc/proc.c",0x97,"pid_getas");
    pid = (pid_t)pcVar2;
  }
  pcVar2 = (char *)(pid << 2);
  if (99 < uVar1) {
    pcVar2 = "pid > 0 && pid < MAX_PROC + 1";
    badassert("pid > 0 && pid < MAX_PROC + 1","../../proc/proc.c",0x98,"pid_getas");
  }
  return *(addrspace **)(*(int *)((int)processTable.proc + (int)pcVar2) + 0x10);
}