struct addrspace *pid_getas(pid_t pid)
{
	KASSERT( pid == curproc->p_pid);
	KASSERT(pid > 0 && pid < MAX_PROC + 1);
	return processTable.proc[pid]->p_addrspace;
}