int32_t semfs_read(int32_t * vn, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = *(int32_t *)((int32_t)vn + 16); // 0x80006544
    int32_t * v3 = semfs_getsem((int32_t *)v2); // 0x8000654c
    lock_acquire((int32_t *)*v3);
    int32_t * v4 = (int32_t *)(v1 + 16); // 0x80006644
    int32_t v5 = *v4; // 0x80006644
    if (v5 == 0) {
        // 0x80006654
        lock_release((int32_t *)*v3);
        return 0;
    }
    int32_t v6 = (int32_t)v3; // 0x8000654c
    int32_t * v7 = (int32_t *)(v6 + 8); // 0x80006574
    int32_t * v8 = (int32_t *)(v2 + 28);
    int32_t * v9 = (int32_t *)(v1 + 8);
    int32_t * v10 = (int32_t *)(v1 + 12);
    int32_t v11 = v5; // 0x8000651c
    int32_t v12; // 0x8000651c
    while (true) {
      lab_0x80006574:;
        uint32_t v13 = *v7;
        if (v13 == 0) {
            goto lab_0x80006614;
        } else {
            uint32_t v14 = v11;
            int32_t v15 = v13 >= v14 ? v14 : v13;
            int32_t v16 = v13; // 0x800065a0
            if ((dbflags & 256) != 0) {
                // 0x800065a4
                kprintf("semfs: sem%u: P, count %u -> %u\n", *v8, v13, v13 - v15);
                v16 = *v7;
            }
            // 0x800065f4
            *v7 = v16 - v15;
            uint32_t v17 = *v10; // 0x800065c8
            uint32_t v18 = v17 + v15; // 0x800065d0
            *v9 = *v9 + (int32_t)(v18 < v17);
            *v10 = v18;
            int32_t v19 = *v4 - v15; // 0x800065ec
            *v4 = v19;
            if (v19 == 0) {
                // break -> 0x80006654
                break;
            }
            // 0x80006604
            v12 = v19;
            if (*v7 == 0) {
                goto lab_0x80006614;
            } else {
                goto lab_0x80006644;
            }
        }
    }
  lab_0x80006654:
    // 0x80006654
    lock_release((int32_t *)*v3);
    return 0;
  lab_0x80006614:
    // 0x80006614
    if ((dbflags & 256) != 0) {
        // 0x80006628
        kprintf("semfs: sem%u: blocking\n", *v8);
    }
    // 0x80006634
    cv_wait((int32_t *)*(int32_t *)(v6 + 4), (int32_t *)*v3);
    v12 = *v4;
    goto lab_0x80006644;
  lab_0x80006644:
    // 0x80006644
    v11 = v12;
    if (v12 == 0) {
        // break -> 0x80006654
        goto lab_0x80006654;
    }
    goto lab_0x80006574;
}