int semfs_read(vnode *vn,uio *uio)

{
  semfs_sem *psVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar1 = semfs_getsem(semv);
  lock_acquire(psVar1->sems_lock);
  while (uVar4 = uio->uio_resid, uVar4 != 0) {
    uVar3 = psVar1->sems_count;
    if (uVar3 != 0) {
      if (uVar3 < uVar4) {
        uVar4 = uVar3;
      }
      if ((dbflags & 0x100) != 0) {
        kprintf("semfs: sem%u: P, count %u -> %u\n",semv->semv_semnum,uVar3,uVar3 - uVar4);
      }
      psVar1->sems_count = psVar1->sems_count - uVar4;
      uVar2 = *(uint *)((int)&uio->uio_offset + 4);
      uVar3 = uVar2 + uVar4;
      *(uint *)&uio->uio_offset = (uint)(uVar3 < uVar2) + *(int *)&uio->uio_offset;
      *(uint *)((int)&uio->uio_offset + 4) = uVar3;
      uio->uio_resid = uio->uio_resid - uVar4;
    }
    if (uio->uio_resid == 0) break;
    if (psVar1->sems_count == 0) {
      if ((dbflags & 0x100) != 0) {
        kprintf("semfs: sem%u: blocking\n",semv->semv_semnum);
      }
      cv_wait(psVar1->sems_cv,psVar1->sems_lock);
    }
  }
  lock_release(psVar1->sems_lock);
  return 0;
}