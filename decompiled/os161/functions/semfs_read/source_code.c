semfs_read(struct vnode *vn, struct uio *uio)
{
	struct semfs_vnode *semv = vn->vn_data;
	struct semfs_sem *sem;
	size_t consume;

	sem = semfs_getsem(semv);

	lock_acquire(sem->sems_lock);
	while (uio->uio_resid > 0) {
		if (sem->sems_count > 0) {
			consume = uio->uio_resid;
			if (consume > sem->sems_count) {
				consume = sem->sems_count;
			}
			DEBUG(DB_SEMFS, "semfs: sem%u: P, count %u -> %u\n",
			      semv->semv_semnum, sem->sems_count,
			      sem->sems_count - consume);
			sem->sems_count -= consume;
			/* don't bother advancing the uio data pointers */
			uio->uio_offset += consume;
			uio->uio_resid -= consume;
		}
		if (uio->uio_resid == 0) {
			break;
		}
		if (sem->sems_count == 0) {
			DEBUG(DB_SEMFS, "semfs: sem%u: blocking\n",
			      semv->semv_semnum);
			cv_wait(sem->sems_cv, sem->sems_lock);
		}
	}
	lock_release(sem->sems_lock);
	return 0;
}