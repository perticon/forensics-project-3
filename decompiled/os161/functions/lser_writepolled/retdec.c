void lser_writepolled(char * vsc, int32_t ch) {
    int32_t v1 = (int32_t)vsc;
    spinlock_acquire((int32_t *)vsc);
    int32_t * v2; // 0x80004dc4
    int32_t * v3; // 0x80004dc4
    bool v4; // 0x80004dc4
    if (*(char *)(v1 + 8) == 0) {
        int32_t * v5 = (int32_t *)(v1 + 12);
        int32_t * v6 = (int32_t *)(v1 + 16);
        lamebus_write_register((int32_t *)*v5, *v6, 4, 0);
        v3 = v6;
        v2 = v5;
        v4 = true;
    } else {
        // 0x80004df8
        lser_poll_until_write((int32_t *)vsc);
        int32_t * v7 = (int32_t *)(v1 + 12);
        int32_t * v8 = (int32_t *)(v1 + 16);
        lamebus_write_register((int32_t *)*v7, *v8, 4, 5);
        v3 = v8;
        v2 = v7;
        v4 = false;
    }
    // 0x80004e30
    lamebus_write_register((int32_t *)*v2, *v3, 0, ch);
    lser_poll_until_write((int32_t *)vsc);
    if (v4) {
        // 0x80004e54
        lamebus_write_register((int32_t *)*v2, *v3, 4, 1);
    }
    // 0x80004e64
    spinlock_release((int32_t *)vsc);
}