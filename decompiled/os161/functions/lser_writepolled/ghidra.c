void lser_writepolled(void *vsc,int ch)

{
  bool bVar1;
  
  spinlock_acquire((spinlock *)vsc);
  bVar1 = *(char *)((int)vsc + 8) == '\0';
  if (bVar1) {
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,0);
  }
  else {
    lser_poll_until_write((lser_softc *)vsc);
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,5);
  }
  lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),0,ch);
  lser_poll_until_write((lser_softc *)vsc);
  if (bVar1) {
    lamebus_write_register(*(lamebus_softc **)((int)vsc + 0xc),*(int *)((int)vsc + 0x10),4,1);
  }
  spinlock_release((spinlock *)vsc);
  return;
}