lser_writepolled(void *vsc, int ch)
{
	struct lser_softc *sc = vsc;
	bool irqpending;

	spinlock_acquire(&sc->ls_lock);

	if (sc->ls_wbusy) {
		irqpending = true;
		lser_poll_until_write(sc);
		/* Clear the ready condition, but leave the IRQ asserted */
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSER_REG_WIRQ,
				   LSER_IRQ_FORCE|LSER_IRQ_ENABLE);
	}
	else {
		irqpending = false;
		/* Clear the interrupt enable bit */
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSER_REG_WIRQ, 0);
	}

	/* Send the character. */
	bus_write_register(sc->ls_busdata, sc->ls_buspos, LSER_REG_CHAR, ch);

	/* Wait until it's done. */
	lser_poll_until_write(sc);

	/*
	 * If there wasn't already an IRQ pending, clear the ready
	 * condition and turn interruption back on. But if there was,
	 * leave the register alone, with the ready condition set (and
	 * the force bit still on); in due course we'll get to the
	 * interrupt handler and they'll be cleared.
	 */
	if (!irqpending) {
		bus_write_register(sc->ls_busdata, sc->ls_buspos,
				   LSER_REG_WIRQ, LSER_IRQ_ENABLE);
	}

	spinlock_release(&sc->ls_lock);
}