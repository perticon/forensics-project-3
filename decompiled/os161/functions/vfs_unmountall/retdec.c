int32_t vfs_unmountall(void) {
    // 0x800198b4
    vfs_biglock_acquire();
    int32_t v1 = 0; // 0x80019ab0
    if (bootfs_vnode == NULL) {
        // 0x80019ab4
        vfs_biglock_release();
        return 0;
    }
    int32_t v2; // 0x800198b4
    int32_t * v3; // 0x80019970
    int32_t * v4; // 0x80019990
    while (true) {
      lab_0x80019914:;
        int32_t v5 = (int32_t)knowndevs; // 0x8001992c
        if (v1 >= (int32_t)bootfs_vnode) {
            // 0x80019930
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v5 = &g41;
        }
        int32_t v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v1); // 0x80019958
        if (*(int32_t *)(v6 + 4) == 0) {
            goto lab_0x80019aa8;
        } else {
            // 0x80019970
            v3 = (int32_t *)(v6 + 16);
            switch (*v3) {
                case 0: {
                    goto lab_0x80019aa8;
                }
                case -1: {
                    // 0x80019988
                    *v3 = 0;
                    goto lab_0x80019aa8;
                }
                default: {
                    // 0x80019990
                    v4 = (int32_t *)v6;
                    kprintf("vfs: Unmounting %s:\n", (char *)*v4);
                    int32_t v7 = *(int32_t *)(*v3 + 4); // 0x800199a4
                    int32_t v8 = *(int32_t *)v7; // 0x800199ac
                    v2 = v7;
                    if (v8 == 0) {
                        goto lab_0x80019a34;
                    } else {
                        // 0x800199c4
                        kprintf("vfs: Warning: sync failed for %s: %s, trying again\n", (char *)*v4, strerror(v8));
                        int32_t v9 = *(int32_t *)(*v3 + 4); // 0x800199ec
                        int32_t v10 = *(int32_t *)v9; // 0x800199f4
                        v2 = v9;
                        if (v10 == 0) {
                            goto lab_0x80019a34;
                        } else {
                            int32_t v11 = *v4; // 0x80019a0c
                            kprintf("vfs: Warning: sync failed second time for %s: %s, giving up...\n", (char *)v11, strerror(v10));
                            goto lab_0x80019aa8;
                        }
                    }
                }
            }
        }
    }
  lab_0x80019ab4:
    // 0x80019ab4
    vfs_biglock_release();
    return 0;
  lab_0x80019aa8:
    // 0x80019aa8
    v1++;
    if (v1 >= (int32_t)bootfs_vnode) {
        // break -> 0x80019ab4
        goto lab_0x80019ab4;
    }
    // 0x80019aa8
    goto lab_0x80019914;
  lab_0x80019a34:;
    int32_t v12 = *(int32_t *)(v2 + 12); // 0x80019a44
    switch (v12) {
        case 27: {
            // 0x80019a60
            kprintf("vfs: Cannot unmount %s: (busy)\n", (char *)*v4);
            // break -> 0x80019aa8
            break;
        }
        case 0: {
            // 0x80019aa0
            *v3 = 0;
            // break -> 0x80019aa8
            break;
        }
        default: {
            int32_t v13 = *v4; // 0x80019a7c
            kprintf("vfs: Warning: unmount failed for %s: %s, already synced, dropping...\n", (char *)v13, strerror(v12));
            // break -> 0x80019aa8
            break;
        }
    }
    goto lab_0x80019aa8;
}