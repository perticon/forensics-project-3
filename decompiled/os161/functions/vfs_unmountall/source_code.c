vfs_unmountall(void)
{
	struct knowndev *dev;
	unsigned i, num;
	int result;

	vfs_biglock_acquire();

	num = knowndevarray_num(knowndevs);
	for (i=0; i<num; i++) {
		dev = knowndevarray_get(knowndevs, i);
		if (dev->kd_rawname == NULL) {
			/* not mountable/unmountable */
			continue;
		}
		if (dev->kd_fs == NULL) {
			/* not mounted */
			continue;
		}
		if (dev->kd_fs == SWAP_FS) {
			/* just drop it */
			dev->kd_fs = NULL;
			continue;
		}

		kprintf("vfs: Unmounting %s:\n", dev->kd_name);

		result = FSOP_SYNC(dev->kd_fs);
		if (result) {
			kprintf("vfs: Warning: sync failed for %s: %s, trying "
				"again\n", dev->kd_name, strerror(result));

			result = FSOP_SYNC(dev->kd_fs);
			if (result) {
				kprintf("vfs: Warning: sync failed second time"
					" for %s: %s, giving up...\n",
					dev->kd_name, strerror(result));
				/*
				 * Do not attempt to complete the
				 * unmount as it will likely explode.
				 */
				continue;
			}
		}

		result = FSOP_UNMOUNT(dev->kd_fs);
		if (result == EBUSY) {
			kprintf("vfs: Cannot unmount %s: (busy)\n",
				dev->kd_name);
			continue;
		}
		if (result) {
			kprintf("vfs: Warning: unmount failed for %s:"
				" %s, already synced, dropping...\n",
				dev->kd_name, strerror(result));
			continue;
		}

		/* now drop the filesystem */
		dev->kd_fs = NULL;
	}

	vfs_biglock_release();

	return 0;
}