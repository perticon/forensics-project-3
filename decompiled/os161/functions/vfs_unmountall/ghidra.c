int vfs_unmountall(void)

{
  knowndevarray *pkVar1;
  int iVar2;
  char *pcVar3;
  undefined4 *puVar4;
  uint uVar5;
  uint uVar6;
  undefined4 uVar7;
  
  vfs_biglock_acquire();
  uVar6 = (knowndevs->arr).num;
  uVar5 = 0;
LAB_80019aa8:
  do {
    while( true ) {
      if (uVar6 <= uVar5) {
        vfs_biglock_release();
        return 0;
      }
      pkVar1 = knowndevs;
      if ((knowndevs->arr).num <= uVar5) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      puVar4 = (undefined4 *)(pkVar1->arr).v[uVar5];
      if ((puVar4[1] != 0) && (puVar4[4] != 0)) break;
LAB_80019aa4:
      uVar5 = uVar5 + 1;
    }
    if (puVar4[4] == -1) {
      puVar4[4] = 0;
      goto LAB_80019aa4;
    }
    kprintf("vfs: Unmounting %s:\n",*puVar4);
    iVar2 = (***(code ***)(puVar4[4] + 4))();
    if (iVar2 != 0) {
      uVar7 = *puVar4;
      pcVar3 = strerror(iVar2);
      kprintf("vfs: Warning: sync failed for %s: %s, trying again\n",uVar7,pcVar3);
      iVar2 = (***(code ***)(puVar4[4] + 4))();
      if (iVar2 != 0) {
        uVar7 = *puVar4;
        pcVar3 = strerror(iVar2);
        kprintf("vfs: Warning: sync failed second time for %s: %s, giving up...\n",uVar7,pcVar3);
        uVar5 = uVar5 + 1;
        goto LAB_80019aa8;
      }
    }
    iVar2 = (**(code **)(*(int *)(puVar4[4] + 4) + 0xc))();
    if (iVar2 == 0x1b) {
      kprintf("vfs: Cannot unmount %s: (busy)\n",*puVar4);
      uVar5 = uVar5 + 1;
    }
    else {
      if (iVar2 == 0) {
        puVar4[4] = 0;
        goto LAB_80019aa4;
      }
      uVar7 = *puVar4;
      pcVar3 = strerror(iVar2);
      kprintf("vfs: Warning: unmount failed for %s: %s, already synced, dropping...\n",uVar7,pcVar3)
      ;
      uVar5 = uVar5 + 1;
    }
  } while( true );
}