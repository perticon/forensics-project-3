call_enter_forked_process(void *tfv, unsigned long dummy)
{
  struct trapframe *tf = (struct trapframe *)tfv;
  (void)dummy;
  enter_forked_process(tf);

  panic("enter_forked_process returned (should not happen)\n");
}