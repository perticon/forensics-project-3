void call_enter_forked_process(char * tfv, int32_t dummy) {
    // 0x8000dc90
    enter_forked_process((int32_t *)tfv);
    panic("enter_forked_process returned (should not happen)\n");
}