void proc_remthread(thread *t)

{
  uint uVar1;
  int spl;
  proc *ppVar2;
  
  ppVar2 = t->t_proc;
  if (ppVar2 == (proc *)0x0) {
    badassert("proc != NULL","../../proc/proc.c",0x18b,"proc_remthread");
  }
  spinlock_acquire(&ppVar2->p_lock);
  uVar1 = ppVar2->p_numthreads - 1;
  if (ppVar2->p_numthreads == 0) {
    badassert("proc->p_numthreads > 0","../../proc/proc.c",0x18e,"proc_remthread");
  }
  ppVar2->p_numthreads = uVar1;
  spinlock_release(&ppVar2->p_lock);
  spl = splx(1);
  t->t_proc = (proc *)0x0;
  splx(spl);
  return;
}