void proc_remthread(int32_t * t) {
    int32_t * v1 = (int32_t *)((int32_t)t + 84); // 0x8000ce20
    int32_t v2 = *v1; // 0x8000ce20
    if (v2 == 0) {
        // 0x8000ce30
        badassert("proc != NULL", "../../proc/proc.c", 395, "proc_remthread");
    }
    int32_t * v3 = (int32_t *)(v2 + 8); // 0x8000ce58
    spinlock_acquire(v3);
    int32_t * v4 = (int32_t *)(v2 + 4); // 0x8000ce5c
    int32_t v5 = *v4; // 0x8000ce5c
    int32_t v6 = v5 - 1; // 0x8000ce68
    if (v5 == 0) {
        // 0x8000ce6c
        badassert("proc->p_numthreads > 0", "../../proc/proc.c", 398, "proc_remthread");
        v6 = &g41;
    }
    // 0x8000ce8c
    *v4 = v6;
    spinlock_release(v3);
    int32_t v7 = splx(1); // 0x8000ce9c
    *v1 = 0;
    splx(v7);
}