vopfail_string_notdir(struct vnode *vn, const char *name)
{
	(void)vn;
	(void)name;
	return ENOTDIR;
}