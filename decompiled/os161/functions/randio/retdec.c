int32_t randio(int32_t * dev, int32_t * uio) {
    // 0x800021f8
    if (*(int32_t *)((int32_t)uio + 24) == 0) {
        // 0x8000220c
        return *(int32_t *)(*(int32_t *)((int32_t)dev + 16) + 12);
    }
    // 0x8000222c
    return 32;
}