randio(struct device *dev, struct uio *uio)
{
	struct random_softc *rs = dev->d_data;

	if (uio->uio_rw != UIO_READ) {
		return EIO;
	}

	return rs->rs_read(rs->rs_devdata, uio);
}