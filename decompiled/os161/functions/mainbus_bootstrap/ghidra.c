void mainbus_bootstrap(void)

{
  int unaff_s7;
  
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    badassert("curthread->t_curspl > 0","../../arch/sys161/dev/lamebus_machdep.c",0x5a,
              "mainbus_bootstrap");
  }
  lamebus = lamebus_init();
  lamebus_find_cpus(lamebus);
  kprintf("lamebus0 (system main bus)\n");
  splx(0);
  autoconf_lamebus(lamebus,0);
  mips_timer_set(250000);
  return;
}