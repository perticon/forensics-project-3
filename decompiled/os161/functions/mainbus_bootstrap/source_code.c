mainbus_bootstrap(void)
{
	/* Interrupts should be off (and have been off since startup) */
	KASSERT(curthread->t_curspl > 0);

	/* Initialize the system LAMEbus data */
	lamebus = lamebus_init();

	/* Probe CPUs (should these be done as device attachments instead?) */
	lamebus_find_cpus(lamebus);

	/*
	 * Print the device name for the main bus.
	 */
	kprintf("lamebus0 (system main bus)\n");

	/*
	 * Now we can take interrupts without croaking, so turn them on.
	 * Some device probes might require being able to get interrupts.
	 */

	spl0();

	/*
	 * Now probe all the devices attached to the bus.
	 * (This amounts to all devices.)
	 */
	autoconf_lamebus(lamebus, 0);

	/*
	 * Configure the MIPS on-chip timer to interrupt HZ times a second.
	 */
	mips_timer_set(CPU_FREQUENCY / HZ);
}