void mainbus_bootstrap(void) {
    // 0x8002077c
    int32_t v1; // 0x8002077c
    if (*(int32_t *)(v1 + 92) <= 0) {
        // 0x80020798
        badassert("curthread->t_curspl > 0", "../../arch/sys161/dev/lamebus_machdep.c", 90, "mainbus_bootstrap");
    }
    int32_t * v2 = lamebus_init(); // 0x800207b8
    *(int32_t *)&lamebus = (int32_t)v2;
    lamebus_find_cpus(v2);
    kprintf("lamebus0 (system main bus)\n");
    splx(0);
    autoconf_lamebus(lamebus, 0);
    mips_timer_set(0x3d090);
}