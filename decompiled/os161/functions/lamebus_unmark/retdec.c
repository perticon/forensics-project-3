void lamebus_unmark(int32_t * sc, uint32_t slot) {
    int32_t v1 = (int32_t)sc; // 0x80003e38
    if (slot >= 32) {
        // 0x80003e3c
        badassert("slot>=0 && slot < LB_NSLOTS", "../../dev/lamebus/lamebus.c", 333, "lamebus_unmark");
        v1 = (int32_t)"slot>=0 && slot < LB_NSLOTS";
    }
    int32_t v2 = 1 << slot; // 0x80003e38
    int32_t * v3 = (int32_t *)v1; // 0x80003e60
    spinlock_acquire(v3);
    int32_t v4 = 0x73202626; // 0x80003e74
    if ((v2 & 0x73202626) == 0) {
        // 0x80003e78
        panic("lamebus_mark: slot %d not marked in use\n", slot);
        v4 = &g41;
    }
    // 0x80003e84
    *(int32_t *)"&& slot < LB_NSLOTS" = v4 & -1 - v2;
    spinlock_release(v3);
}