lamebus_unmark(struct lamebus_softc *sc, int slot)
{
	uint32_t mask = ((uint32_t)1) << slot;
	KASSERT(slot>=0 && slot < LB_NSLOTS);

	spinlock_acquire(&sc->ls_lock);

	if ((sc->ls_slotsinuse & mask)==0) {
		panic("lamebus_mark: slot %d not marked in use\n", slot);
	}

	sc->ls_slotsinuse &= ~mask;

	spinlock_release(&sc->ls_lock);
}