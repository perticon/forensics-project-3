bool dev_isseekable(vnode *v)

{
  int iVar1;
  
  iVar1 = *(int *)((int)v->vn_data + 4);
  if (iVar1 != 0) {
    iVar1 = 1;
  }
  return SUB41(iVar1,0);
}