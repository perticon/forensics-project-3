bool dev_isseekable(int32_t * v2) {
    // 0x80017b6c
    return *(int32_t *)(*(int32_t *)((int32_t)v2 + 16) + 4) != 0;
}