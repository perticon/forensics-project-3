dev_isseekable(struct vnode *v)
{
	struct device *d = v->vn_data;

	if (d->d_blocks == 0) {
		return false;
	}
	return true;
}