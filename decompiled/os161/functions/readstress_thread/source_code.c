readstress_thread(void *fs, unsigned long num)
{
	const char *filesys = fs;
	if (fstest_read(filesys, "")) {
		kprintf("*** Thread %lu: failed\n", num);
	}
	V(threadsem);
}