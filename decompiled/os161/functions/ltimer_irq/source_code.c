ltimer_irq(void *vlt)
{
	struct ltimer_softc *lt = vlt;
	uint32_t val;

	val = bus_read_register(lt->lt_bus, lt->lt_buspos, LT_REG_IRQ);
	if (val) {
		/*
		 * Only call hardclock if we're responsible for hardclock.
		 * (Any additional timer devices are unused.)
		 */
		if (lt->lt_hardclock) {
			hardclock();
		}
		/*
		 * Likewise for timerclock.
		 */
		if (lt->lt_timerclock) {
			timerclock();
		}
	}
}