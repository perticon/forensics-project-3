void ltimer_irq(char * vlt) {
    int32_t v1 = (int32_t)vlt;
    int32_t v2 = *(int32_t *)(v1 + 8); // 0x80005008
    if (lamebus_read_register((int32_t *)v2, *(int32_t *)(v1 + 12), 12) == 0) {
        // 0x80005050
        return;
    }
    if (v2 != 0) {
        // 0x80005030
        hardclock();
    }
    // 0x80005038
    if (*(int32_t *)(v1 + 4) != 0) {
        // 0x80005048
        timerclock();
    }
}