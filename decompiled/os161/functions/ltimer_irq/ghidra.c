void ltimer_irq(void *vlt)

{
  uint32_t uVar1;
  
  uVar1 = lamebus_read_register(*(lamebus_softc **)((int)vlt + 8),*(int *)((int)vlt + 0xc),0xc);
  if (uVar1 != 0) {
                    /* WARNING: Load size is inaccurate */
    if (*vlt != 0) {
      hardclock();
    }
    if (*(int *)((int)vlt + 4) != 0) {
      timerclock();
    }
  }
  return;
}