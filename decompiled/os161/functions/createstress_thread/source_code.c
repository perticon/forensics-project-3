createstress_thread(void *fs, unsigned long num)
{
	const char *filesys = fs;
	int i, err;
	char namesuffix[16];
	char name[32];
	char buf[32];
	struct vnode *vn;
	struct iovec iov;
	struct uio ku;
	size_t bytes;
	unsigned numwritten = 0, numread = 0, numremoved = 0;

	for (i=0; i<NCREATE; i++) {
		snprintf(namesuffix, sizeof(namesuffix), "%lu-%d", num, i);
		MAKENAME();

		/* vfs_open destroys the string it's passed */
		strcpy(buf, name);
		err = vfs_open(buf, O_WRONLY|O_CREAT|O_TRUNC, 0664, &vn);
		if (err) {
			kprintf("Could not open %s for write: %s\n",
				name, strerror(err));
			continue;
		}

		strcpy(buf, SLOGAN);
		rotate(buf, i);

		uio_kinit(&iov, &ku, buf, strlen(SLOGAN), 0, UIO_WRITE);
		err = VOP_WRITE(vn, &ku);
		vfs_close(vn);
		if (err) {
			kprintf("%s: Write error: %s\n", name, strerror(err));
			continue;
		}
		if (ku.uio_resid > 0) {
			kprintf("%s: Short write: %lu bytes left over\n",
				name, (unsigned long) ku.uio_resid);
			continue;
		}

		bytes = ku.uio_offset;
		if (bytes != strlen(SLOGAN)) {
			kprintf("%s: %lu bytes written, expected %lu!\n",
				name, (unsigned long) bytes,
				(unsigned long) strlen(SLOGAN));
			continue;
		}
		numwritten++;
	}
	kprintf("Thread %lu: %u files written\n", num, numwritten);

	for (i=0; i<NCREATE; i++) {
		snprintf(namesuffix, sizeof(namesuffix), "%lu-%d", num, i);
		MAKENAME();

		/* vfs_open destroys the string it's passed */
		strcpy(buf, name);
		err = vfs_open(buf, O_RDONLY, 0664, &vn);
		if (err) {
			kprintf("Could not open %s for read: %s\n",
				name, strerror(err));
			continue;
		}

		uio_kinit(&iov, &ku, buf, strlen(SLOGAN), 0, UIO_READ);
		err = VOP_READ(vn, &ku);
		vfs_close(vn);
		if (err) {
			kprintf("%s: Read error: %s\n", name, strerror(err));
			continue;
		}
		if (ku.uio_resid > 0) {
			kprintf("%s: Short read: %lu bytes left over\n",
				name, (unsigned long) ku.uio_resid);
			continue;
		}

		buf[strlen(SLOGAN)] = 0;
		rotate(buf, -i);

		if (strcmp(buf, SLOGAN)) {
			kprintf("%s: Test failed: file mismatched: %s\n",
				name, buf);
			continue;
		}

		bytes = ku.uio_offset;
		if (bytes != strlen(SLOGAN)) {
			kprintf("%s: %lu bytes read, expected %lu!\n",
				name, (unsigned long) bytes,
				(unsigned long) strlen(SLOGAN));
			continue;
		}

		numread++;
	}
	kprintf("Thread %lu: %u files read\n", num, numread);

	for (i=0; i<NCREATE; i++) {
		snprintf(namesuffix, sizeof(namesuffix), "%lu-%d", num, i);
		if (fstest_remove(filesys, namesuffix)) {
			continue;
		}
		numremoved++;
	}
	kprintf("Thread %lu: %u files removed\n", num, numremoved);

	V(threadsem);
}