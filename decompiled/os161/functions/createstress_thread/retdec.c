void createstress_thread(char * fs, int32_t num) {
    char buf[32]; // bp-120, 0x8000fde8
    char name[32]; // bp-152, 0x8000fde8
    char namesuffix[16]; // bp-168, 0x8000fde8
    // 0x8000fde8
    int32_t v1; // bp-200, 0x8000fde8
    int32_t v2 = &v1; // 0x8000fde8
    int32_t v3 = 0;
    int32_t v4 = 0;
    int32_t v5; // 0x8000fde8
    int32_t iov; // bp-84, 0x8000fde8
    int32_t ku; // bp-72, 0x8000fde8
    int32_t v6; // 0x8000fde8
    int32_t v7; // 0x8000ff14
    char * v8; // 0x8000fe90
    char * v9; // 0x8000ff3c
    int32_t v10; // 0x8000ff98
    int32_t v11; // 0x8000fde8
    int32_t v12; // 0x8000fe80
    int32_t v13; // 0x8000fec8
    int32_t * vn; // bp-88, 0x8000fde8
    while (true) {
        // 0x8000fe3c
        v6 = v4;
        snprintf(namesuffix, 16, "%lu-%d", num, v6);
        fstest_makename(name, 32, fs, namesuffix);
        strcpy(buf, name);
        v12 = vfs_open(buf, 21, 436, &vn);
        if (v12 == 0) {
            // 0x8000feb0
            strcpy(buf, "HODIE MIHI - CRAS TIBI\n");
            rotate(buf, v6);
            v13 = strlen("HODIE MIHI - CRAS TIBI\n");
            uio_kinit(&iov, &ku, buf, v13, 0, 1);
            vnode_check(vn, "write");
            v7 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 24);
            vfs_close(vn);
            if (v7 == 0) {
                if (v11 == 0) {
                    // 0x8000ff80
                    if (strlen("HODIE MIHI - CRAS TIBI\n") == v11) {
                        // break -> 0x8000ffbc
                        break;
                    }
                    // 0x8000ff94
                    v10 = strlen("HODIE MIHI - CRAS TIBI\n");
                    kprintf("%s: %lu bytes written, expected %lu!\n", name, v11, v10);
                } else {
                    // 0x8000ff6c
                    kprintf("%s: Short write: %lu bytes left over\n", name, v11);
                }
            } else {
                // 0x8000ff38
                v9 = strerror(v7);
                kprintf("%s: Write error: %s\n", name, v9);
            }
        } else {
            // 0x8000fe8c
            v8 = strerror(v12);
            kprintf("Could not open %s for write: %s\n", name, v8);
        }
        // 0x8000ffc4
        v4 = v6 + 1;
        v5 = v3;
        if (v4 >= 24) {
            // break (via goto) -> 0x8000ffd0
            goto lab_0x8000ffd0;
        }
    }
    int32_t v14 = v3 + 1; // 0x8000ffbc
    int32_t v15 = v6 + 1; // 0x8000ffc0
    v5 = v14;
    while (v15 < 24) {
        // 0x8000fe3c
        v3 = v14;
        v4 = v15;
        while (true) {
            // 0x8000fe3c
            v6 = v4;
            snprintf(namesuffix, 16, "%lu-%d", num, v6);
            fstest_makename(name, 32, fs, namesuffix);
            strcpy(buf, name);
            v12 = vfs_open(buf, 21, 436, &vn);
            if (v12 == 0) {
                // 0x8000feb0
                strcpy(buf, "HODIE MIHI - CRAS TIBI\n");
                rotate(buf, v6);
                v13 = strlen("HODIE MIHI - CRAS TIBI\n");
                uio_kinit(&iov, &ku, buf, v13, 0, 1);
                vnode_check(vn, "write");
                v7 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 24);
                vfs_close(vn);
                if (v7 == 0) {
                    if (v11 == 0) {
                        // 0x8000ff80
                        if (strlen("HODIE MIHI - CRAS TIBI\n") == v11) {
                            // break -> 0x8000ffbc
                            break;
                        }
                        // 0x8000ff94
                        v10 = strlen("HODIE MIHI - CRAS TIBI\n");
                        kprintf("%s: %lu bytes written, expected %lu!\n", name, v11, v10);
                    } else {
                        // 0x8000ff6c
                        kprintf("%s: Short write: %lu bytes left over\n", name, v11);
                    }
                } else {
                    // 0x8000ff38
                    v9 = strerror(v7);
                    kprintf("%s: Write error: %s\n", name, v9);
                }
            } else {
                // 0x8000fe8c
                v8 = strerror(v12);
                kprintf("Could not open %s for write: %s\n", name, v8);
            }
            // 0x8000ffc4
            v4 = v6 + 1;
            v5 = v3;
            if (v4 >= 24) {
                // break (via goto) -> 0x8000ffd0
                goto lab_0x8000ffd0;
            }
        }
        // 0x8000ffbc
        v14 = v3 + 1;
        v15 = v6 + 1;
        v5 = v14;
    }
  lab_0x8000ffd0:
    // 0x8000ffd0
    kprintf("Thread %lu: %u files written\n", num, v5);
    int32_t v16 = 0;
    int32_t v17 = 0;
    int32_t v18; // 0x8000fde8
    int32_t v19; // 0x8000fde8
    int32_t v20; // 0x8001004c
    int32_t v21; // 0x80010080
    int32_t v22; // 0x800100c8
    char * v23; // 0x8001005c
    char * v24; // 0x800100f0
    int32_t v25; // 0x80010194
    while (true) {
        // 0x80010008
        v19 = v17;
        snprintf(namesuffix, 16, "%lu-%d", num, v19);
        fstest_makename(name, 32, fs, namesuffix);
        strcpy(buf, name);
        v20 = vfs_open(buf, 0, 436, &vn);
        if (v20 == 0) {
            // 0x8001007c
            v21 = strlen("HODIE MIHI - CRAS TIBI\n");
            uio_kinit(&iov, &ku, buf, v21, 0, 0);
            vnode_check(vn, "read");
            v22 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 12);
            vfs_close(vn);
            if (v22 == 0) {
                if (v11 == 0) {
                    // 0x80010134
                    *(char *)(v2 + 80 + strlen("HODIE MIHI - CRAS TIBI\n")) = 0;
                    rotate(buf, -v19);
                    if (strcmp(buf, "HODIE MIHI - CRAS TIBI\n") == 0) {
                        // 0x8001017c
                        if (strlen("HODIE MIHI - CRAS TIBI\n") == v11) {
                            // break -> 0x800101b8
                            break;
                        }
                        // 0x80010190
                        v25 = strlen("HODIE MIHI - CRAS TIBI\n");
                        kprintf("%s: %lu bytes read, expected %lu!\n", name, v11, v25);
                    } else {
                        // 0x80010164
                        kprintf("%s: Test failed: file mismatched: %s\n", name, buf);
                    }
                } else {
                    // 0x80010120
                    kprintf("%s: Short read: %lu bytes left over\n", name, v11);
                }
            } else {
                // 0x800100ec
                v24 = strerror(v22);
                kprintf("%s: Read error: %s\n", name, v24);
            }
        } else {
            // 0x80010058
            v23 = strerror(v20);
            kprintf("Could not open %s for read: %s\n", name, v23);
        }
        // 0x800101c0
        v17 = v19 + 1;
        v18 = v16;
        if (v17 >= 24) {
            // break (via goto) -> 0x800101cc
            goto lab_0x800101cc;
        }
    }
    int32_t v26 = v16 + 1; // 0x800101b8
    int32_t v27 = v19 + 1; // 0x800101bc
    v18 = v26;
    while (v27 < 24) {
        // 0x80010008
        v16 = v26;
        v17 = v27;
        while (true) {
            // 0x80010008
            v19 = v17;
            snprintf(namesuffix, 16, "%lu-%d", num, v19);
            fstest_makename(name, 32, fs, namesuffix);
            strcpy(buf, name);
            v20 = vfs_open(buf, 0, 436, &vn);
            if (v20 == 0) {
                // 0x8001007c
                v21 = strlen("HODIE MIHI - CRAS TIBI\n");
                uio_kinit(&iov, &ku, buf, v21, 0, 0);
                vnode_check(vn, "read");
                v22 = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 12);
                vfs_close(vn);
                if (v22 == 0) {
                    if (v11 == 0) {
                        // 0x80010134
                        *(char *)(v2 + 80 + strlen("HODIE MIHI - CRAS TIBI\n")) = 0;
                        rotate(buf, -v19);
                        if (strcmp(buf, "HODIE MIHI - CRAS TIBI\n") == 0) {
                            // 0x8001017c
                            if (strlen("HODIE MIHI - CRAS TIBI\n") == v11) {
                                // break -> 0x800101b8
                                break;
                            }
                            // 0x80010190
                            v25 = strlen("HODIE MIHI - CRAS TIBI\n");
                            kprintf("%s: %lu bytes read, expected %lu!\n", name, v11, v25);
                        } else {
                            // 0x80010164
                            kprintf("%s: Test failed: file mismatched: %s\n", name, buf);
                        }
                    } else {
                        // 0x80010120
                        kprintf("%s: Short read: %lu bytes left over\n", name, v11);
                    }
                } else {
                    // 0x800100ec
                    v24 = strerror(v22);
                    kprintf("%s: Read error: %s\n", name, v24);
                }
            } else {
                // 0x80010058
                v23 = strerror(v20);
                kprintf("Could not open %s for read: %s\n", name, v23);
            }
            // 0x800101c0
            v17 = v19 + 1;
            v18 = v16;
            if (v17 >= 24) {
                // break (via goto) -> 0x800101cc
                goto lab_0x800101cc;
            }
        }
        // 0x800101b8
        v26 = v16 + 1;
        v27 = v19 + 1;
        v18 = v26;
    }
  lab_0x800101cc:
    // 0x800101cc
    kprintf("Thread %lu: %u files read\n", num, v18);
    int32_t v28 = 0; // 0x8000fde8
    snprintf(namesuffix, 16, "%lu-%d", num, v28);
    int32_t v29 = fstest_remove(fs, namesuffix) == 0;
    int32_t v30 = v28 + 1; // 0x80010220
    int32_t v31 = v29; // 0x8001022c
    v28 = v30;
    while (v30 < 24) {
        // 0x800101f4
        snprintf(namesuffix, 16, "%lu-%d", num, v28);
        v29 = v31 + (int32_t)(fstest_remove(fs, namesuffix) == 0);
        v30 = v28 + 1;
        v31 = v29;
        v28 = v30;
    }
    // 0x80010230
    kprintf("Thread %lu: %u files removed\n", num, v29);
    V(threadsem);
}