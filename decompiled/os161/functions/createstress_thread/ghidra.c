void createstress_thread(void *fs,ulong num)

{
  char *pcVar1;
  size_t sVar2;
  size_t sVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  char namesuffix [16];
  char name [32];
  char buf [32];
  vnode *vn;
  iovec iov;
  uio ku;
  
  iVar6 = 0;
  iVar5 = 0;
  while (iVar5 < 0x18) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    fstest_makename(name,0x20,(char *)fs,namesuffix);
    strcpy(buf,name);
    iVar4 = vfs_open(buf,0x15,0x1b4,&vn);
    if (iVar4 == 0) {
      strcpy(buf,"HODIE MIHI - CRAS TIBI\n");
      rotate(buf,iVar5);
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar2,0,UIO_WRITE);
      vnode_check(vn,s_console_write_80022690 + 8);
      iVar4 = (*vn->vn_ops->vop_write)(vn,&ku);
      vfs_close(vn);
      sVar2 = ku.uio_offset._4_4_;
      if (iVar4 == 0) {
        if (ku.uio_resid == 0) {
          sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
          if (sVar3 == sVar2) {
            iVar6 = iVar6 + 1;
            iVar5 = iVar5 + 1;
          }
          else {
            sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
            kprintf("%s: %lu bytes written, expected %lu!\n",name,sVar2,sVar3);
            iVar5 = iVar5 + 1;
          }
        }
        else {
          kprintf("%s: Short write: %lu bytes left over\n",name);
          iVar5 = iVar5 + 1;
        }
      }
      else {
        pcVar1 = strerror(iVar4);
        kprintf("%s: Write error: %s\n",name,pcVar1);
        iVar5 = iVar5 + 1;
      }
    }
    else {
      pcVar1 = strerror(iVar4);
      kprintf("Could not open %s for write: %s\n",name,pcVar1);
      iVar5 = iVar5 + 1;
    }
  }
  kprintf("Thread %lu: %u files written\n",num,iVar6);
  iVar6 = 0;
  iVar5 = 0;
  while (iVar5 < 0x18) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    fstest_makename(name,0x20,(char *)fs,namesuffix);
    strcpy(buf,name);
    iVar4 = vfs_open(buf,0,0x1b4,&vn);
    if (iVar4 == 0) {
      sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
      uio_kinit(&iov,&ku,buf,sVar2,0,UIO_READ);
      vnode_check(vn,"read");
      iVar4 = (*vn->vn_ops->vop_read)(vn,&ku);
      vfs_close(vn);
      if (iVar4 == 0) {
        if (ku.uio_resid == 0) {
          sVar2 = strlen("HODIE MIHI - CRAS TIBI\n");
          buf[sVar2] = '\0';
          rotate(buf,-iVar5);
          iVar4 = strcmp(buf,"HODIE MIHI - CRAS TIBI\n");
          sVar2 = ku.uio_offset._4_4_;
          if (iVar4 == 0) {
            sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
            if (sVar3 == sVar2) {
              iVar6 = iVar6 + 1;
              iVar5 = iVar5 + 1;
            }
            else {
              sVar3 = strlen("HODIE MIHI - CRAS TIBI\n");
              kprintf("%s: %lu bytes read, expected %lu!\n",name,sVar2,sVar3);
              iVar5 = iVar5 + 1;
            }
          }
          else {
            kprintf("%s: Test failed: file mismatched: %s\n",name,buf);
            iVar5 = iVar5 + 1;
          }
        }
        else {
          kprintf("%s: Short read: %lu bytes left over\n",name);
          iVar5 = iVar5 + 1;
        }
      }
      else {
        pcVar1 = strerror(iVar4);
        kprintf("%s: Read error: %s\n",name,pcVar1);
        iVar5 = iVar5 + 1;
      }
    }
    else {
      pcVar1 = strerror(iVar4);
      kprintf("Could not open %s for read: %s\n",name,pcVar1);
      iVar5 = iVar5 + 1;
    }
  }
  kprintf("Thread %lu: %u files read\n",num,iVar6);
  iVar6 = 0;
  for (iVar5 = 0; iVar5 < 0x18; iVar5 = iVar5 + 1) {
    snprintf(namesuffix,0x10,"%lu-%d",num,iVar5);
    iVar4 = fstest_remove((char *)fs,namesuffix);
    if (iVar4 == 0) {
      iVar6 = iVar6 + 1;
    }
  }
  kprintf("Thread %lu: %u files removed\n",num,iVar6);
  V(threadsem);
  return;
}