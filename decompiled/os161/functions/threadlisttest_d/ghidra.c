void threadlisttest_d(void)

{
  thread *ptVar1;
  threadlist tl;
  
  threadlist_init(&tl);
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xce,"threadlisttest_d");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remhead(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xd3,"threadlisttest_d");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xd5,"threadlisttest_d");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xd6,"threadlisttest_d");
  }
  threadlist_addhead(&tl,fakethreads[0]);
  threadlist_addtail(&tl,fakethreads[1]);
  if (tl.tl_count != 2) {
    badassert("tl.tl_count == 2","../../test/threadlisttest.c",0xda,"threadlisttest_d");
  }
  check_order(&tl,false);
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[1] != ptVar1) {
    badassert("t == fakethreads[1]","../../test/threadlisttest.c",0xdf,"threadlisttest_d");
  }
  ptVar1 = threadlist_remtail(&tl);
  if (fakethreads[0] != ptVar1) {
    badassert("t == fakethreads[0]","../../test/threadlisttest.c",0xe1,"threadlisttest_d");
  }
  if (tl.tl_count != 0) {
    badassert("tl.tl_count == 0","../../test/threadlisttest.c",0xe2,"threadlisttest_d");
  }
  threadlist_cleanup(&tl);
  return;
}