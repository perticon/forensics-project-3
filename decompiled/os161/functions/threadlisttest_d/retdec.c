void threadlisttest_d(void) {
    // 0x80013d28
    int32_t tl; // bp-40, 0x80013d28
    threadlist_init(&tl);
    threadlist_addhead(&tl, (int32_t *)*(int32_t *)&fakethreads);
    threadlist_addtail(&tl, (int32_t *)g30);
    int32_t v1; // 0x80013d28
    if (v1 != 2) {
        // 0x80013d70
        badassert("tl.tl_count == 2", "../../test/threadlisttest.c", 206, "threadlisttest_d");
    }
    // 0x80013d8c
    check_order(&tl, false);
    int32_t * v2 = threadlist_remhead(&tl); // 0x80013d9c
    if (*(int32_t *)&fakethreads != (int32_t)v2) {
        // 0x80013db4
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 211, "threadlisttest_d");
    }
    int32_t * v3 = threadlist_remtail(&tl); // 0x80013dd4
    if (g30 != (int32_t)v3) {
        // 0x80013dec
        badassert("t == fakethreads[1]", "../../test/threadlisttest.c", 213, "threadlisttest_d");
    }
    if (v1 != 0) {
        // 0x80013e18
        badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 214, "threadlisttest_d");
    }
    // 0x80013e38
    threadlist_addhead(&tl, (int32_t *)*(int32_t *)&fakethreads);
    threadlist_addtail(&tl, (int32_t *)g30);
    if (v1 != 2) {
        // 0x80013e68
        badassert("tl.tl_count == 2", "../../test/threadlisttest.c", 218, "threadlisttest_d");
    }
    // 0x80013e84
    check_order(&tl, false);
    int32_t * v4 = threadlist_remtail(&tl); // 0x80013e94
    if (g30 != (int32_t)v4) {
        // 0x80013eac
        badassert("t == fakethreads[1]", "../../test/threadlisttest.c", 223, "threadlisttest_d");
    }
    int32_t * v5 = threadlist_remtail(&tl); // 0x80013ecc
    if (*(int32_t *)&fakethreads != (int32_t)v5) {
        // 0x80013ee4
        badassert("t == fakethreads[0]", "../../test/threadlisttest.c", 225, "threadlisttest_d");
    }
    if (v1 != 0) {
        // 0x80013f10
        badassert("tl.tl_count == 0", "../../test/threadlisttest.c", 226, "threadlisttest_d");
    }
    // 0x80013f2c
    threadlist_cleanup(&tl);
}