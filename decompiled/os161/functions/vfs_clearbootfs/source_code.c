vfs_clearbootfs(void)
{
	vfs_biglock_acquire();
	change_bootfs(NULL);
	vfs_biglock_release();
}