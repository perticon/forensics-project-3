void vfs_clearbootfs(void)

{
  vfs_biglock_acquire();
  change_bootfs((vnode *)0x0);
  vfs_biglock_release();
  return;
}