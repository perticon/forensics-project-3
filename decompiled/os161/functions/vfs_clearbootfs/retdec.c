void vfs_clearbootfs(void) {
    // 0x80019e44
    vfs_biglock_acquire();
    change_bootfs(NULL);
    vfs_biglock_release();
}