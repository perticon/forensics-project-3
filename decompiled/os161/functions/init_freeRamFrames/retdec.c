int32_t init_freeRamFrames(int32_t ramFrames) {
    // 0x8001b4b8
    g20 = ramFrames;
    char * v1 = kmalloc(ramFrames + 4 & -4); // 0x8001b4e0
    *(int32_t *)&freeRamFrames = (int32_t)v1;
    if (v1 == NULL) {
        // 0x8001b530
        return 1;
    }
    // 0x8001b510
    if (g20 <= 0) {
        // 0x8001b530
        return 0;
    }
    // 0x8001b500
    ClearBit((int32_t *)v1, 0);
    for (int32_t i = 1; i < g20; i++) {
        // 0x8001b500
        ClearBit(freeRamFrames, i);
    }
    // 0x8001b530
    return 0;
}