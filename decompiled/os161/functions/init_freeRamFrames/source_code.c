int init_freeRamFrames(int ramFrames)
{
    int i;
    nRamFrames = ramFrames;
    freeRamFrames = kmalloc(sizeof(int) * (nRamFrames/sizeof(int)+1));
    if (freeRamFrames == NULL)
        return 1;

    for (i = 0; i < nRamFrames; i++)
    {
        ClearBit(freeRamFrames, i);
    }

    return 0;
}