int init_freeRamFrames(int ramFrames)

{
  uint uVar1;
  int k;
  
  nRamFrames = ramFrames;
  freeRamFrames = (int *)kmalloc((((uint)ramFrames >> 2) + 1) * 4);
  if (freeRamFrames == (int *)0x0) {
    uVar1 = 1;
  }
  else {
    for (k = 0; uVar1 = (uint)(k < nRamFrames), uVar1 != 0; k = k + 1) {
      ClearBit(freeRamFrames,k);
    }
  }
  return uVar1;
}