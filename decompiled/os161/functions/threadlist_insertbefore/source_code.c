threadlist_insertbefore(struct threadlist *tl,
			struct thread *addee, struct thread *onlist)
{
	threadlist_insertbeforenode(addee, &onlist->t_listnode);
	tl->tl_count++;
}