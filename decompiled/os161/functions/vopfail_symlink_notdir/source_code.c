vopfail_symlink_notdir(struct vnode *vn, const char *contents,
		       const char *name)
{
	(void)vn;
	(void)contents;
	(void)name;
	return ENOTDIR;
}