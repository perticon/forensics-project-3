void vm_bootstrap(void)
{
  int result;
  paddr_t firstpaddr, addr;
  int occupiedpages, freepages;
  // int result;
  nRamFrames = ((int)ram_getsize()) / PAGE_SIZE;
  kprintf("ram_getsize(): %d\n", (int)ram_getsize());
  kprintf("RamFrames: %d\n", nRamFrames);
  /* alloc inverted page table */
  if (create_ipt() == -1)
    return;
  /* alloc freeRamFrame and allocSize */
  result = init_freeRamFrames(nRamFrames);
  if (result)
  {
    return;
  }
  result = init_allocSize(nRamFrames);
  if (result)
  {
    /* reset to disable this vm management */
    destroy_freeRamFrames();
    return;
  }
  spinlock_acquire(&freemem_lock);
  allocTableActive = 1;
  spinlock_release(&freemem_lock);
  /*allocation and deallocation of all ram to avoid using ram_stealmem*/
  firstpaddr = ram_getfirstfreeafterbootstrap(); /* get address of first free page */
  occupiedpages = ((int)firstpaddr) / PAGE_SIZE; /* calculate occupied pages by kernel */
  freepages = nRamFrames - occupiedpages;        /* calculate free pages remaining*/
  addr = alloc_kpages(freepages);                /*allocate all pages available*/
  free_kpages(addr);                             /* deallocate all pages previously allocated */
  init_instrumentation();
}