void vm_bootstrap(void)

{
  paddr_t pVar1;
  int iVar2;
  vaddr_t addr;
  
  pVar1 = ram_getsize();
  if ((int)pVar1 < 0) {
    pVar1 = pVar1 + 0xfff;
  }
  nRamFrames = (int)pVar1 >> 0xc;
  pVar1 = ram_getsize();
  kprintf("ram_getsize(): %d\n",pVar1);
  kprintf("RamFrames: %d\n",nRamFrames);
  iVar2 = create_ipt();
  if ((iVar2 != -1) && (iVar2 = init_freeRamFrames(nRamFrames), iVar2 == 0)) {
    iVar2 = init_allocSize(nRamFrames);
    if (iVar2 == 0) {
      spinlock_acquire(&freemem_lock);
      allocTableActive = 1;
      spinlock_release(&freemem_lock);
      pVar1 = ram_getfirstfreeafterbootstrap();
      if ((int)pVar1 < 0) {
        pVar1 = pVar1 + 0xfff;
      }
      addr = alloc_kpages(nRamFrames - ((int)pVar1 >> 0xc));
      free_kpages(addr);
      init_instrumentation();
    }
    else {
      destroy_freeRamFrames();
    }
  }
  return;
}