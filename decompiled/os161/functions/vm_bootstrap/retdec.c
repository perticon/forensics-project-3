void vm_bootstrap(void) {
    int32_t v1 = ram_getsize(); // 0x8001ac08
    g19 = (v1 > -1 ? v1 : v1 + 4095) >> 12;
    kprintf("ram_getsize(): %d\n", ram_getsize());
    kprintf("RamFrames: %d\n", g19);
    if (create_ipt() == -1 || init_freeRamFrames(g19) != 0) {
        // 0x8001ace8
        return;
    }
    // 0x8001ac70
    if (init_allocSize(g19) == 0) {
        // 0x8001ac94
        spinlock_acquire(&g18);
        allocTableActive = 1;
        spinlock_release(&g18);
        int32_t v2 = ram_getfirstfreeafterbootstrap(); // 0x8001acb4
        free_kpages(alloc_kpages(g19 - ((v2 > -1 ? v2 : v2 + 4095) >> 12)));
        init_instrumentation();
    } else {
        // 0x8001ac84
        destroy_freeRamFrames();
    }
}