void free_swap_table(int32_t pid) {
    // 0x8001dd8c
    spinlock_acquire(&swap_lock);
    int32_t v1 = 0; // 0x8001ddac
    int32_t v2 = -1; // 0x8001ddac
    int32_t v3 = &swap_table; // 0x8001ddac
    int32_t v4; // 0x8001dd8c
    int32_t v5; // 0x8001dd8c
    int32_t v6; // 0x8001dd8c
    if (pid > -1) {
        goto lab_0x8001de00;
    } else {
        // 0x8001ddb0
        badassert("pid >= 0", "../../vm/swapfile.c", 630, "free_swap_table");
        v6 = &g41;
        v4 = (int32_t)"pid >= 0";
        v5 = (int32_t)"../../vm/swapfile.c";
        goto lab_0x8001ddd0;
    }
  lab_0x8001de00:
    // 0x8001de00
    v6 = v1;
    int32_t v7 = 8 * v1; // 0x8001de08
    v4 = v2;
    v5 = v3;
    if (v1 >= 2304) {
        // 0x8001de0c
        spinlock_release(&swap_lock);
        return;
    }
    goto lab_0x8001ddd0;
  lab_0x8001ddd0:
    // 0x8001ddd0
    v3 = v5;
    v2 = v4;
    int32_t v8 = v6;
    if (*(int32_t *)(v3 + v7) == pid) {
        // 0x8001dde4
        *(int32_t *)(v3 + 8 * v8) = v2;
    }
    // 0x8001ddec
    v1 = v8 + 1;
    goto lab_0x8001de00;
}