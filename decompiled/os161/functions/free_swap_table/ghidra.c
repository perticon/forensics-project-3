void free_swap_table(pid_t pid)

{
  int iVar1;
  int in_v1;
  char *pcVar2;
  char *pcVar3;
  swap_entry *psVar4;
  
  spinlock_acquire(&swap_lock);
  iVar1 = 0;
  if (pid < 0) {
    pcVar2 = "pid >= 0";
    pcVar3 = s_______vm_swapfile_c_800281fc;
    badassert("pid >= 0",s_______vm_swapfile_c_800281fc,0x276,"free_swap_table");
    psVar4 = (swap_entry *)pcVar3;
    do {
      if (*(int *)((int)&psVar4->pid + in_v1) == pid) {
        psVar4[iVar1].pid = (pid_t)pcVar2;
      }
      iVar1 = iVar1 + 1;
LAB_8001de00:
      in_v1 = iVar1 << 3;
    } while (iVar1 < 0x900);
    spinlock_release(&swap_lock);
    return;
  }
  psVar4 = swap_table;
  pcVar2 = (char *)0xffffffff;
  goto LAB_8001de00;
}