void free_swap_table(pid_t pid)
{
    spinlock_acquire(&swap_lock);

    KASSERT(pid >= 0);

    for (int i = 0; i < ENTRIES; i++)
    {
        if (swap_table[i].pid == pid)
        {
            swap_table[i].pid = -1;
        }
    }

    spinlock_release(&swap_lock);
}