int32_t cmd_deadlock(int32_t nargs, char ** args) {
    int32_t * v1 = lock_create("deadlock1"); // 0x8000c31c
    int32_t dl = (int32_t)v1; // bp-16, 0x8000c324
    if (v1 == NULL) {
        // 0x8000c328
        kprintf("lock_create failed\n");
        // 0x8000c420
        return 3;
    }
    int32_t * v2 = lock_create("deadlock2"); // 0x8000c344
    if (v2 == NULL) {
        // 0x8000c350
        lock_destroy(v1);
        kprintf("lock_create failed\n");
        // 0x8000c420
        return 3;
    }
    int32_t result = thread_fork((char *)args, NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 0x3bd0), (char *)&dl, 0); // 0x8000c388
    if (result == 0) {
        while (true) {
            // 0x8000c3e0
            lock_acquire((int32_t *)dl);
            lock_acquire(v2);
            kprintf(".");
            lock_release(v2);
            lock_release((int32_t *)dl);
        }
    }
    // 0x8000c394
    kprintf("thread_fork failed: %s\n", strerror(result));
    lock_release((int32_t *)dl);
    lock_destroy(v2);
    lock_destroy((int32_t *)dl);
    // 0x8000c420
    return result;
}