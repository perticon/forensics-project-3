cmd_deadlock(int nargs, char **args)
{
	struct deadlock dl;
	int result;

	(void)nargs;
	(void)args;

	dl.lock1 = lock_create("deadlock1");
	if (dl.lock1 == NULL)
	{
		kprintf("lock_create failed\n");
		return ENOMEM;
	}
	dl.lock2 = lock_create("deadlock2");
	if (dl.lock2 == NULL)
	{
		lock_destroy(dl.lock1);
		kprintf("lock_create failed\n");
		return ENOMEM;
	}

	result = thread_fork(args[0] /* thread name */,
						 NULL /* kernel thread */,
						 cmd_deadlockthread /* thread function */,
						 &dl /* thread arg */, 0 /* thread arg */);
	if (result)
	{
		kprintf("thread_fork failed: %s\n", strerror(result));
		lock_release(dl.lock1);
		lock_destroy(dl.lock2);
		lock_destroy(dl.lock1);
		return result;
	}

	/* If it doesn't wedge right away, keep trying... */
	while (1)
	{
		lock_acquire(dl.lock1);
		lock_acquire(dl.lock2);
		kprintf(".");
		lock_release(dl.lock2);
		lock_release(dl.lock1);
	}
	/* NOTREACHED */
	return 0;
}