int config_rtclock(rtclock_softc *rtc,int unit)

{
  char *pcVar1;
  
  if (unit == 0) {
    if (the_clock != (rtclock_softc *)0x0) {
      pcVar1 = "the_clock==NULL";
      badassert("the_clock==NULL","../../dev/generic/rtclock.c",0x3c,"config_rtclock");
      rtc = (rtclock_softc *)pcVar1;
    }
    the_clock = rtc;
    return 0;
  }
  return 0x19;
}