int32_t config_rtclock(int32_t * rtc, int32_t unit) {
    if (unit != 0) {
        // 0x800023c0
        return 25;
    }
    int32_t v1 = (int32_t)rtc; // 0x80002388
    if (the_clock != NULL) {
        // 0x8000238c
        badassert("the_clock==NULL", "../../dev/generic/rtclock.c", 60, "config_rtclock");
        v1 = (int32_t)"the_clock==NULL";
    }
    // 0x800023b0
    *(int32_t *)&the_clock = v1;
    while (true) {
        // continue -> 0x800023b0
    }
}