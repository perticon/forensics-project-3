config_rtclock(struct rtclock_softc *rtc, int unit)
{
	/* We use only the first clock device. */
	if (unit!=0) {
		return ENODEV;
	}

	KASSERT(the_clock==NULL);
	the_clock = rtc;
	return 0;
}