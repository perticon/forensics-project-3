vopfail_mkdir_nosys(struct vnode *vn, const char *name, mode_t mode)
{
	(void)vn;
	(void)name;
	(void)mode;
	return ENOSYS;
}