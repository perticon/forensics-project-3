sfs_getvolname(struct fs *fs)
{
	struct sfs_fs *sfs = fs->fs_data;
	const char *ret;

	vfs_biglock_acquire();
	ret = sfs->sfs_sb.sb_volname;
	vfs_biglock_release();

	return ret;
}