void spinlock_init(int32_t * splk) {
    // 0x80015118
    spinlock_data_set(splk, 0);
    *(int32_t *)((int32_t)splk + 4) = 0;
}