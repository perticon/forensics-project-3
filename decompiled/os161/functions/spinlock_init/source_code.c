spinlock_init(struct spinlock *splk)
{
	spinlock_data_set(&splk->splk_lock, 0);
	splk->splk_holder = NULL;
	HANGMAN_LOCKABLEINIT(&splk->splk_hangman, "spinlock");
}