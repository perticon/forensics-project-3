void spinlock_init(spinlock *splk)

{
  spinlock_data_set(&splk->splk_lock,0);
  splk->splk_holder = (cpu *)0x0;
  return;
}