int32_t tryattach_beep_to_ltimer(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_beep_to_ltimer(devunit, bus); // 0x80001324
    if (v1 == NULL) {
        // 0x8000139c
        return -1;
    }
    // 0x80001330
    kprintf("beep%d at ltimer%d", devunit, busunit);
    int32_t v2 = config_beep(v1, devunit); // 0x8000134c
    int32_t result; // 0x80001308
    if (v2 == 0) {
        // 0x80001378
        kprintf("\n");
        nextunit_beep = devunit + 1;
        result = 0;
    } else {
        // 0x80001358
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x8000139c
    return result;
}