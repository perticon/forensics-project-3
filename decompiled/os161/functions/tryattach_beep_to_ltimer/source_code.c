tryattach_beep_to_ltimer(int devunit, struct ltimer_softc *bus, int busunit)
{
	struct beep_softc *dev;
	int result;

	dev = attach_beep_to_ltimer(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("beep%d at ltimer%d", devunit, busunit);
	result = config_beep(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_beep = devunit+1;
	autoconf_beep(dev, devunit);
	return 0;
}