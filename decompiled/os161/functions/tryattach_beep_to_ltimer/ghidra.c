int tryattach_beep_to_ltimer(int devunit,ltimer_softc *bus,int busunit)

{
  beep_softc *bs;
  int errcode;
  char *pcVar1;
  
  bs = attach_beep_to_ltimer(devunit,bus);
  if (bs == (beep_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("beep%d at ltimer%d",devunit,busunit);
    errcode = config_beep(bs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_beep = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}