lamebus_poweroff(struct lamebus_softc *lamebus)
{
	/*
	 * Write 0 to the power register to shut the system off.
	 */

	cpu_irqoff();
	write_ctl_register(lamebus, CTLREG_PWR, 0);

	/* The power doesn't go off instantly... so halt the cpu. */
	cpu_halt();
}