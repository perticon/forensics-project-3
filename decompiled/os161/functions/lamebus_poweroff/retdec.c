void lamebus_poweroff(int32_t * lamebus2) {
    // 0x800043c8
    cpu_irqoff();
    lamebus_write_register(lamebus2, 31, 0x7e08, 0);
    cpu_halt();
}