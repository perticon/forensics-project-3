void lamebus_poweroff(lamebus_softc *lamebus)

{
  cpu_irqoff();
  lamebus_write_register(lamebus,0x1f,0x7e08,0);
  cpu_halt();
  return;
}