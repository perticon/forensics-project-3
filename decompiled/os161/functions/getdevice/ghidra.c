int getdevice(char *path,char **subpath,vnode **startvn)

{
  char cVar1;
  undefined3 extraout_var;
  bool bVar4;
  int iVar2;
  int iVar3;
  int iVar5;
  fs *pfVar6;
  int iVar7;
  vnode *vn;
  
  bVar4 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslookup.c",0x88,"getdevice");
  }
  iVar7 = (int)*path;
  if (iVar7 == 0) {
    iVar7 = 8;
  }
  else {
    for (iVar2 = 0; cVar1 = path[iVar2], cVar1 != '\0'; iVar2 = iVar2 + 1) {
      if (cVar1 == ':') {
        iVar5 = -1;
        iVar3 = iVar2;
        goto LAB_80019bc8;
      }
      if (cVar1 == '/') {
        iVar3 = -1;
        iVar5 = iVar2;
        goto LAB_80019bc8;
      }
    }
    iVar3 = -1;
    iVar5 = -1;
LAB_80019bc8:
    if ((iVar3 < 0) && (iVar5 != 0)) {
      *subpath = path;
      iVar7 = vfs_getcurdir(startvn);
    }
    else if (iVar3 < 1) {
      iVar2 = 0x2f;
      if ((iVar3 != 0) && (iVar5 != 0)) {
        iVar7 = 0xc3;
        badassert("colon==0 || slash==0","../../vfs/vfslookup.c",0xc3,"getdevice");
      }
      if (iVar7 == iVar2) {
        if (bootfs_vnode == (vnode *)0x0) {
          return 0x13;
        }
        vnode_incref(bootfs_vnode);
        *startvn = bootfs_vnode;
      }
      else {
        if (iVar7 != 0x3a) {
          badassert("path[0]==\':\'","../../vfs/vfslookup.c",0xcd,"getdevice");
        }
        iVar7 = vfs_getcurdir(&vn);
        if (iVar7 != 0) {
          return iVar7;
        }
        pfVar6 = vn->vn_fs;
        if (pfVar6 == (fs *)0x0) {
          pfVar6 = (fs *)&DAT_800273d8;
          badassert("vn->vn_fs!=NULL","../../vfs/vfslookup.c",0xd8,"getdevice");
        }
        iVar7 = (*pfVar6->fs_ops->fsop_getroot)(pfVar6,startvn);
        vnode_decref(vn);
        if (iVar7 != 0) {
          return iVar7;
        }
      }
      for (; iVar7 = 0, path[1] == '/'; path = path + 1) {
      }
      *subpath = path + 1;
    }
    else {
      path[iVar3] = '\0';
      do {
        iVar7 = iVar3 + 1;
        iVar3 = iVar3 + 1;
      } while (path[iVar7] == '/');
      *subpath = path + iVar7;
      iVar7 = vfs_getroot(path,startvn);
    }
  }
  return iVar7;
}