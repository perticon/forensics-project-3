int32_t getdevice(char * path, char ** subpath, int32_t * startvn) {
    // 0x80019b24
    if (!vfs_biglock_do_i_hold()) {
        // 0x80019b50
        badassert("vfs_biglock_do_i_hold()", "../../vfs/vfslookup.c", 136, "getdevice");
    }
    // 0x80019b6c
    int32_t v1; // 0x80019b24
    char v2 = v1;
    if (v2 == 0) {
        // 0x80019d58
        return 8;
    }
    int32_t v3 = (int32_t)path;
    char v4 = *path; // 0x80019ba0
    char v5 = v4; // 0x80019bac
    int32_t v6 = 0; // 0x80019bac
    int32_t v7 = -1; // 0x80019bac
    int32_t v8 = -1; // 0x80019bac
    if (v4 != 0) {
        int32_t v9; // 0x80019b24
        while (true) {
          lab_0x80019b88:
            // 0x80019b88
            v9 = v6;
            v7 = -1;
            v8 = v6;
            switch (v5) {
                case 58: {
                    goto lab_0x80019bc8;
                }
                case 47: {
                    goto lab_0x80019bc8_2;
                }
                default: {
                    // 0x80019b98
                    v6++;
                    v5 = *(char *)(v6 + v3);
                    if (v5 == 0) {
                        // break -> 0x80019bc8
                        break;
                    }
                    goto lab_0x80019b88;
                }
            }
        }
      lab_0x80019bc8:
        // 0x80019bc8
        v7 = v9;
        v8 = -1;
    }
  lab_0x80019bc8_2:
    // 0x80019bc8
    if (v7 <= -1 && v8 != 0) {
        // 0x80019bd8
        *(int32_t *)subpath = v3;
        // 0x80019d58
        return vfs_getcurdir((int32_t **)startvn);
    }
    if (v7 >= 1) {
        // 0x80019bf4
        *(char *)(v7 + v3) = 0;
        int32_t v10 = v7; // 0x80019bf8
        v10++;
        int32_t v11 = v10 + v3; // 0x80019c00
        while (*(char *)v11 == 47) {
            // 0x80019bfc
            v10++;
            v11 = v10 + v3;
        }
        // 0x80019c14
        *(int32_t *)subpath = v11;
        // 0x80019d58
        return vfs_getroot(path, startvn);
    }
    int32_t v12 = 47; // 0x80019c34
    int32_t v13 = v2; // 0x80019c34
    if (v7 != 0 && v8 != 0) {
        // 0x80019c40
        badassert("colon==0 || slash==0", "../../vfs/vfslookup.c", 195, "getdevice");
        v12 = &g41;
        v13 = 195;
    }
    // 0x80019c5c
    if (v13 == v12) {
        // 0x80019c64
        if (bootfs_vnode == NULL) {
            // 0x80019d58
            return 19;
        }
        // 0x80019c78
        vnode_incref(bootfs_vnode);
        *startvn = (int32_t)bootfs_vnode;
    } else {
        if (v13 != 58) {
            // 0x80019c98
            badassert("path[0]==':'", "../../vfs/vfslookup.c", 205, "getdevice");
        }
        // 0x80019cb4
        int32_t * vn; // bp-24, 0x80019b24
        int32_t result = vfs_getcurdir(&vn); // 0x80019cb8
        if (result != 0) {
            // 0x80019d58
            return result;
        }
        int32_t v14 = *(int32_t *)((int32_t)vn + 12); // 0x80019ccc
        int32_t * v15 = vn; // 0x80019cd8
        int32_t v16 = v14; // 0x80019cd8
        if (v14 == 0) {
            // 0x80019cdc
            badassert("vn->vn_fs!=NULL", "../../vfs/vfslookup.c", 216, "getdevice");
            v15 = vn;
            v16 = (int32_t)"vn->vn_fs!=NULL";
        }
        int32_t result2 = *(int32_t *)(*(int32_t *)(v16 + 4) + 8); // 0x80019d00
        vnode_decref(v15);
        if (result2 != 0) {
            // 0x80019d58
            return result2;
        }
    }
    int32_t v17 = v3 + 1; // 0x80019d38
    int32_t v18 = v17; // 0x80019d44
    while (*(char *)v17 == 47) {
        // 0x80019d38
        v17 = v18 + 1;
        v18 = v17;
    }
    // 0x80019d48
    *(int32_t *)subpath = v17;
    // 0x80019d58
    return 0;
}