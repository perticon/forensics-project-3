int32_t array_preallocate(int32_t * a, uint32_t num) {
    int32_t v1 = (int32_t)a;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x8000a990
    uint32_t v3 = *v2; // 0x8000a990
    if (v3 >= num) {
        // 0x8000aa08
        return 0;
    }
    int32_t v4 = v3; // 0x8000a9a0
    while (v4 < num) {
        // 0x8000a9ac
        v4 *= 2;
    }
    char * v5 = kmalloc(4 * v4); // 0x8000a9c8
    int32_t result = 3; // 0x8000a9d0
    if (v5 != NULL) {
        // 0x8000a9d4
        memcpy(v5, (int32_t *)v5, 4 * *(int32_t *)(v1 + 4));
        kfree(v5);
        *a = (int32_t)v5;
        *v2 = v4;
        result = 0;
    }
    // 0x8000aa08
    return result;
}