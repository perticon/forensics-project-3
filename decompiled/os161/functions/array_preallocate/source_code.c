array_preallocate(struct array *a, unsigned num)
{
	void **newptr;
	unsigned newmax;

	if (num > a->max) {
		/* Don't touch A until the allocation succeeds. */
		newmax = a->max;
		while (num > newmax) {
			newmax = newmax ? newmax*2 : 4;
		}

		/*
		 * We don't have krealloc, and it wouldn't be
		 * worthwhile to implement just for this. So just
		 * allocate a new block and copy. (Exercise: what
		 * about this and/or kmalloc makes it not worthwhile?)
		 */

		newptr = kmalloc(newmax*sizeof(*a->v));
		if (newptr == NULL) {
			return ENOMEM;
		}
		memcpy(newptr, a->v, a->num*sizeof(*a->v));
		kfree(a->v);
		a->v = newptr;
		a->max = newmax;
	}
	return 0;
}