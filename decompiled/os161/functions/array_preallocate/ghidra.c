int array_preallocate(array *a,uint num)

{
  bool bVar1;
  int iVar2;
  void **dst;
  uint uVar3;
  
  uVar3 = a->max;
  if (uVar3 < num) {
    while (uVar3 < num) {
      bVar1 = uVar3 == 0;
      uVar3 = uVar3 << 1;
      if (bVar1) {
        uVar3 = 4;
      }
    }
    dst = (void **)kmalloc(uVar3 << 2);
    if (dst == (void **)0x0) {
      iVar2 = 3;
    }
    else {
      memcpy(dst,a->v,a->num << 2);
      kfree(a->v);
      a->v = dst;
      a->max = uVar3;
      iVar2 = 0;
    }
  }
  else {
    iVar2 = 0;
  }
  return iVar2;
}