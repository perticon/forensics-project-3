void lamebus_mask_interrupt(int32_t * lamebus2, uint32_t slot) {
    int32_t v1 = (int32_t)lamebus2; // 0x800040ec
    if (slot >= 32) {
        // 0x800040f0
        badassert("slot >= 0 && slot < LB_NSLOTS", "../../dev/lamebus/lamebus.c", 406, "lamebus_mask_interrupt");
        v1 = (int32_t)"slot >= 0 && slot < LB_NSLOTS";
    }
    int32_t * v2 = (int32_t *)v1; // 0x80004114
    spinlock_acquire(v2);
    lamebus_write_register(v2, 31, 0x7e0c, lamebus_read_register(v2, 31, 0x7e0c) & (-1 << slot) - 1);
    spinlock_release(v2);
}