int32_t mainbus_ramsize(void) {
    uint32_t v1 = lamebus_ramsize(); // 0x80020964
    return v1 < 0x1fc00000 ? v1 : 0x1fc00000;
}