mainbus_ramsize(void)
{
	uint32_t ramsize;

	ramsize = lamebus_ramsize();

	/*
	 * This is the same as the last physical address, as long as
	 * we have less than 508 megabytes of memory. The LAMEbus I/O
	 * area occupies the space between 508 megabytes and 512
	 * megabytes, so if we had more RAM than this it would have to
	 * be discontiguous. This is not a case we are going to worry
	 * about.
	 */
	if (ramsize > 508*1024*1024) {
		ramsize = 508*1024*1024;
	}

	return ramsize;
}