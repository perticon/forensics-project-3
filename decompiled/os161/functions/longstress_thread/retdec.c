void longstress_thread(char * fs, int32_t num) {
    char numstr[16]; // bp-40, 0x8000fca8
    uint32_t v1 = 0;
    while (v1 < 32) {
        // 0x8000fcd8
        snprintf(numstr, 16, "%lu-%d", num, v1);
        if (fstest_write(fs, numstr, 1, 0) != 0 || fstest_read(fs, numstr) != 0) {
            // 0x8000fd08
            kprintf("*** Thread %lu: file %d: failed\n", num, v1);
            V(threadsem);
            return;
        }
        // 0x8000fd70
        if (fstest_remove(fs, numstr) != 0) {
            // 0x8000fd80
            kprintf("*** Thread %lu: file %d: failed\n", num, v1);
            V(threadsem);
            return;
        }
        v1++;
    }
    // 0x8000fdbc
    V(threadsem);
}