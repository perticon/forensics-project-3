void longstress_thread(void *fs,ulong num)

{
  int iVar1;
  int iVar2;
  int iVar3;
  char numstr [16];
  
  iVar1 = 0;
  do {
    iVar3 = iVar1;
    if (0x1f < iVar3) {
      V(threadsem);
      return;
    }
    snprintf(numstr,0x10,"%lu-%d",num,iVar3);
    iVar1 = fstest_write((char *)fs,numstr,1,0);
    if (iVar1 != 0) {
      kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
      V(threadsem);
      return;
    }
    iVar1 = fstest_read((char *)fs,numstr);
    if (iVar1 != 0) {
      kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
      V(threadsem);
      return;
    }
    iVar2 = fstest_remove((char *)fs,numstr);
    iVar1 = iVar3 + 1;
  } while (iVar2 == 0);
  kprintf("*** Thread %lu: file %d: failed\n",num,iVar3);
  V(threadsem);
  return;
}