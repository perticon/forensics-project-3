longstress_thread(void *fs, unsigned long num)
{
	const char *filesys = fs;
	int i;
	char numstr[16];

	for (i=0; i<NLONG; i++) {

		snprintf(numstr, sizeof(numstr), "%lu-%d", num, i);

		if (fstest_write(filesys, numstr, 1, 0)) {
			kprintf("*** Thread %lu: file %d: failed\n", num, i);
			V(threadsem);
			return;
		}

		if (fstest_read(filesys, numstr)) {
			kprintf("*** Thread %lu: file %d: failed\n", num, i);
			V(threadsem);
			return;
		}

		if (fstest_remove(filesys, numstr)) {
			kprintf("*** Thread %lu: file %d: failed\n", num, i);
			V(threadsem);
			return;
		}

	}

	V(threadsem);
}