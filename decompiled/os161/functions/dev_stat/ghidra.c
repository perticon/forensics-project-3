int dev_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  if (*(int *)((int)pvVar2 + 4) == 0) {
    *(undefined4 *)((int)&statbuf->st_size + 4) = 0;
    *(undefined4 *)&statbuf->st_size = 0;
  }
  else {
    *(int *)((int)&statbuf->st_size + 4) = *(int *)((int)pvVar2 + 4) * *(int *)((int)pvVar2 + 8);
    *(undefined4 *)&statbuf->st_size = 0;
    statbuf->st_blksize = *(blksize_t *)((int)pvVar2 + 8);
  }
  vnode_check(v,"gettype");
  iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
  if (iVar1 == 0) {
    statbuf->st_mode = statbuf->st_mode | 0x180;
    statbuf->st_nlink = 1;
    statbuf->st_blocks = *(blkcnt_t *)((int)pvVar2 + 4);
    statbuf->st_dev = 0;
    statbuf->st_rdev = *(dev_t *)((int)pvVar2 + 0xc);
    iVar1 = 0;
  }
  return iVar1;
}