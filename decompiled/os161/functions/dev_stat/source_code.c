dev_stat(struct vnode *v, struct stat *statbuf)
{
	struct device *d = v->vn_data;
	int result;

	bzero(statbuf, sizeof(struct stat));

	if (d->d_blocks > 0) {
		statbuf->st_size = d->d_blocks * d->d_blocksize;
		statbuf->st_blksize = d->d_blocksize;
	}
	else {
		statbuf->st_size = 0;
	}

	result = VOP_GETTYPE(v, &statbuf->st_mode);
	if (result) {
		return result;
	}
	/* Make up some plausible default permissions. */
	statbuf->st_mode |= 0600;

	statbuf->st_nlink = 1;
	statbuf->st_blocks = d->d_blocks;

	/* The device number this device sits on (in OS/161, it doesn't) */
	statbuf->st_dev = 0;

	/* The device number this device *is* */
	statbuf->st_rdev = d->d_devnumber;

	return 0;
}