int32_t dev_stat(int32_t * v2, int32_t * statbuf) {
    int32_t v1 = (int32_t)statbuf;
    int32_t v2_ = (int32_t)v2;
    int32_t v3 = *(int32_t *)(v2_ + 16); // 0x80017c60
    bzero((char *)statbuf, 88);
    int32_t * v4 = (int32_t *)(v3 + 4); // 0x80017c70
    int32_t v5 = *v4; // 0x80017c70
    if (v5 == 0) {
        // 0x80017ca4
        *(int32_t *)(v1 + 4) = 0;
        *statbuf = 0;
    } else {
        int32_t * v6 = (int32_t *)(v3 + 8); // 0x80017c80
        *(int32_t *)(v1 + 4) = *v6 * v5;
        *statbuf = 0;
        *(int32_t *)(v1 + 80) = *v6;
    }
    // 0x80017cb0
    vnode_check(v2, "gettype");
    int32_t result = *(int32_t *)(*(int32_t *)(v2_ + 20) + 36); // 0x80017cc8
    if (result == 0) {
        int32_t * v7 = (int32_t *)(v1 + 8); // 0x80017ce0
        *v7 = *v7 | 384;
        *(int16_t *)(v1 + 12) = 1;
        *(int32_t *)(v1 + 16) = *v4;
        *(int32_t *)(v1 + 20) = 0;
        *(int32_t *)(v1 + 28) = *(int32_t *)(v3 + 12);
    }
    // 0x80017d18
    return result;
}