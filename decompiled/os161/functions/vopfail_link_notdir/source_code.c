vopfail_link_notdir(struct vnode *dir, const char *name, struct vnode *file)
{
	(void)dir;
	(void)name;
	(void)file;
	return ENOTDIR;
}