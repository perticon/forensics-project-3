uint32_t translate_err(emu_softc *sc,uint32_t code)

{
  uint32_t uVar1;
  
  switch(code) {
  default:
    kprintf("emu%d: Unknown result code %d\n",sc->e_unit);
    uVar1 = 4;
    break;
  case 1:
    uVar1 = 0;
    break;
  case 2:
  case 3:
  case 5:
                    /* WARNING: Subroutine does not return */
    panic("emu%d: got fatal result code %d\n",sc->e_unit,code);
  case 4:
    uVar1 = 0x13;
    break;
  case 6:
    uVar1 = 0x16;
    break;
  case 7:
    uVar1 = 0x12;
    break;
  case 8:
    uVar1 = 0x20;
    break;
  case 9:
    uVar1 = 0x1d;
    break;
  case 10:
    uVar1 = 0x24;
    break;
  case 0xb:
    uVar1 = 0x11;
    break;
  case 0xc:
    uVar1 = 0x20;
    break;
  case 0xd:
    uVar1 = 1;
  }
  return uVar1;
}