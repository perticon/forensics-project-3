translate_err(struct emu_softc *sc, uint32_t code)
{
	switch (code) {
	    case EMU_RES_SUCCESS: return 0;
	    case EMU_RES_BADHANDLE:
	    case EMU_RES_BADOP:
	    case EMU_RES_BADSIZE:
		panic("emu%d: got fatal result code %d\n", sc->e_unit, code);
	    case EMU_RES_BADPATH: return ENOENT;
	    case EMU_RES_EXISTS: return EEXIST;
	    case EMU_RES_ISDIR: return EISDIR;
	    case EMU_RES_MEDIA: return EIO;
	    case EMU_RES_NOHANDLES: return ENFILE;
	    case EMU_RES_NOSPACE: return ENOSPC;
	    case EMU_RES_NOTDIR: return ENOTDIR;
	    case EMU_RES_UNKNOWN: return EIO;
	    case EMU_RES_UNSUPP: return ENOSYS;
	}
	kprintf("emu%d: Unknown result code %d\n", sc->e_unit, code);
	return EAGAIN;
}