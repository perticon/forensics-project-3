int32_t translate_err(int32_t * sc, int32_t code) {
    int32_t v1 = (int32_t)sc;
    g38 = code;
    int32_t result = 0; // 0x800027e4
    switch (code) {
        case 2: {
        }
        case 3: {
        }
        case 5: {
            // 0x800027f0
            panic("emu%d: got fatal result code %d\n", *(int32_t *)(v1 + 8), code);
        }
        case 6: {
            // 0x80002800
            result = 22;
        }
        case 1: {
            // 0x8000285c
            return result;
        }
        case 4: {
            // 0x80002858
            result = 19;
            return result;
        }
        case 7: {
            // 0x80002808
            result = 18;
            return result;
        }
        case 8: {
            // 0x80002810
            result = 32;
            return result;
        }
        case 9: {
            // 0x80002818
            result = 29;
            return result;
        }
        case 10: {
            // 0x80002820
            result = 36;
            return result;
        }
        case 11: {
            // 0x80002828
            result = 17;
            return result;
        }
        case 12: {
            // 0x80002830
            result = 32;
            return result;
        }
        case 13: {
            // 0x80002838
            result = 1;
            return result;
        }
        default: {
            // 0x80002840
            kprintf("emu%d: Unknown result code %d\n", *(int32_t *)(v1 + 8), code);
            result = 4;
            return result;
        }
    }
}