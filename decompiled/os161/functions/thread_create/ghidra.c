thread * thread_create(char *name)

{
  thread *t;
  char *pcVar1;
  
  t = (thread *)kmalloc(100);
  if (t == (thread *)0x0) {
    t = (thread *)0x0;
  }
  else {
    pcVar1 = kstrdup(name);
    t->t_name = pcVar1;
    if (pcVar1 == (char *)0x0) {
      kfree(t);
      t = (thread *)0x0;
    }
    else {
      t->t_wchan_name = "NEW";
      t->t_state = S_READY;
      thread_machdep_init(&t->t_machdep);
      threadlistnode_init(&t->t_listnode,t);
      t->t_stack = (void *)0x0;
      t->t_context = (switchframe *)0x0;
      t->t_cpu = (cpu *)0x0;
      t->t_proc = (proc *)0x0;
      t->t_in_interrupt = false;
      t->t_curspl = 1;
      t->t_iplhigh_count = 1;
    }
  }
  return t;
}