int32_t * thread_create(char * name) {
    char * v1 = kmalloc(100); // 0x80015f60
    if (v1 == NULL) {
        // 0x80015fdc
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x80015f70
    *(int32_t *)v1 = (int32_t)v2;
    int32_t * result; // 0x80015f48
    if (v2 == NULL) {
        // 0x80015f7c
        kfree(v1);
        result = NULL;
    } else {
        int32_t v3 = (int32_t)v1; // 0x80015f60
        *(int32_t *)(v3 + 4) = (int32_t)"NEW";
        *(int32_t *)(v3 + 8) = 1;
        thread_machdep_init((int32_t *)(v3 + 12));
        threadlistnode_init((int32_t *)(v3 + 60), (int32_t *)v1);
        *(int32_t *)(v3 + 72) = 0;
        *(int32_t *)(v3 + 76) = 0;
        *(int32_t *)(v3 + 80) = 0;
        *(int32_t *)(v3 + 84) = 0;
        *(char *)(v3 + 88) = 0;
        *(int32_t *)(v3 + 92) = 1;
        *(int32_t *)(v3 + 96) = 1;
        result = (int32_t *)v1;
    }
    // 0x80015fdc
    return result;
}