char * vfs_getdevname(int32_t * fs) {
    if (fs == NULL) {
        // 0x80019264
        badassert("fs != NULL", "../../vfs/vfslist.c", 282, "vfs_getdevname");
    }
    // 0x80019284
    if (!vfs_biglock_do_i_hold()) {
        // 0x80019294
        badassert("vfs_biglock_do_i_hold()", "../../vfs/vfslist.c", 284, "vfs_getdevname");
    }
    int32_t v1 = 0; // 0x800192c4
    int32_t v2 = (int32_t)bootfs_vnode; // 0x800192c4
    char * result = NULL; // 0x80019338
    while (v1 < v2) {
        int32_t v3 = v1; // 0x800192d8
        int32_t v4 = 4 * v1; // 0x800192d8
        if (v1 >= (int32_t)bootfs_vnode) {
            // 0x800192dc
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v3 = &g41;
            v4 = (int32_t)"index < a->num";
            v2 = (int32_t)"array_get";
        }
        int32_t v5 = *(int32_t *)(v4 + (int32_t)knowndevs); // 0x80019308
        v1 = v3 + 1;
        if (*(int32_t *)(v5 + 16) == (int32_t)fs) {
            // 0x80019320
            result = (char *)*(int32_t *)v5;
            return result;
        }
        result = NULL;
    }
  lab_0x80019340:
    // 0x80019340
    return result;
}