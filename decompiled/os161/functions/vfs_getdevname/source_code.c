vfs_getdevname(struct fs *fs)
{
	struct knowndev *kd;
	unsigned i, num;

	KASSERT(fs != NULL);

	KASSERT(vfs_biglock_do_i_hold());

	num = knowndevarray_num(knowndevs);
	for (i=0; i<num; i++) {
		kd = knowndevarray_get(knowndevs, i);

		if (kd->kd_fs == fs) {
			/*
			 * This is not a race condition: as long as the
			 * guy calling us holds a reference to the fs,
			 * the fs cannot go away, and the device can't
			 * go away until the fs goes away.
			 */
			return kd->kd_name;
		}
	}

	return NULL;
}