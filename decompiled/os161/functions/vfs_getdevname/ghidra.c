char * vfs_getdevname(fs *fs)

{
  undefined3 extraout_var;
  bool bVar2;
  char *pcVar1;
  knowndevarray *pkVar3;
  char *pcVar4;
  char *pcVar5;
  
  if (fs == (fs *)0x0) {
    badassert("fs != NULL","../../vfs/vfslist.c",0x11a,"vfs_getdevname");
  }
  bVar2 = vfs_biglock_do_i_hold();
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x11c,"vfs_getdevname");
  }
  pcVar5 = (char *)(knowndevs->arr).num;
  pcVar1 = (char *)0x0;
  pkVar3 = knowndevs;
  do {
    if (pcVar5 <= pcVar1) {
      return (char *)0x0;
    }
    pcVar4 = (char *)((int)pcVar1 << 2);
    if ((char *)(pkVar3->arr).num <= pcVar1) {
      pcVar4 = "index < a->num";
      pcVar5 = "array_get";
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar1 = pcVar1 + 1;
  } while ((fs *)(*(char ***)(pcVar4 + (int)(pkVar3->arr).v))[4] != fs);
  return **(char ***)(pcVar4 + (int)(pkVar3->arr).v);
}