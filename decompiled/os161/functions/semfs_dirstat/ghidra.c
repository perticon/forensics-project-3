int semfs_dirstat(vnode *vn,stat *buf)

{
  int iVar1;
  
  iVar1 = *(int *)((int)vn->vn_data + 0x18);
  bzero(buf,0x58);
  lock_acquire(*(lock **)(iVar1 + 0x14));
  *(undefined4 *)((int)&buf->st_size + 4) = *(undefined4 *)(*(int *)(iVar1 + 0x18) + 4);
  *(undefined4 *)&buf->st_size = 0;
  lock_release(*(lock **)(iVar1 + 0x14));
  buf->st_mode = 0x26f1;
  buf->st_nlink = 2;
  buf->st_blocks = 0;
  buf->st_dev = 0;
  buf->st_ino = 0xffffffff;
  return 0;
}