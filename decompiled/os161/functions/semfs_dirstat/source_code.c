semfs_dirstat(struct vnode *vn, struct stat *buf)
{
	struct semfs_vnode *semv = vn->vn_data;
	struct semfs *semfs = semv->semv_semfs;

	bzero(buf, sizeof(*buf));

	lock_acquire(semfs->semfs_dirlock);
	buf->st_size = semfs_direntryarray_num(semfs->semfs_dents);
	lock_release(semfs->semfs_dirlock);

	buf->st_mode = S_IFDIR | 1777;
	buf->st_nlink = 2;
	buf->st_blocks = 0;
	buf->st_dev = 0;
	buf->st_ino = SEMFS_ROOTDIR;

	return 0;
}