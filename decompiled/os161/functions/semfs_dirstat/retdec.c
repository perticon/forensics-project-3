int32_t semfs_dirstat(int32_t * vn, int32_t * buf) {
    int32_t v1 = (int32_t)buf;
    int32_t v2 = *(int32_t *)(*(int32_t *)((int32_t)vn + 16) + 24); // 0x8000638c
    bzero((char *)buf, 88);
    int32_t * v3 = (int32_t *)(v2 + 20); // 0x8000639c
    lock_acquire((int32_t *)*v3);
    *(int32_t *)(v1 + 4) = *(int32_t *)(*(int32_t *)(v2 + 24) + 4);
    *buf = 0;
    lock_release((int32_t *)*v3);
    *(int32_t *)(v1 + 8) = 0x26f1;
    *(int16_t *)(v1 + 12) = 2;
    *(int32_t *)(v1 + 16) = 0;
    *(int32_t *)(v1 + 20) = 0;
    *(int32_t *)(v1 + 24) = -1;
    return 0;
}