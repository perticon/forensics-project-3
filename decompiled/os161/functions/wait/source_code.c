wait(void)
{
	/*
	 * The WAIT instruction goes into powersave mode until an
	 * interrupt is trying to occur.
	 *
	 * Then switch interrupts on and off again, so we actually
	 * take the interrupt.
	 *
	 * Note that the precise behavior of this instruction in the
	 * System/161 simulator is partly guesswork. This code may not
	 * work on a real mips.
	 */
	__asm volatile(
		".set push;"		/* save assembler mode */
		".set mips32;"		/* allow MIPS32 instructions */
		".set volatile;"	/* avoid unwanted optimization */
		"wait;"			/* suspend until interrupted */
		".set pop"		/* restore assembler mode */
	      );
}