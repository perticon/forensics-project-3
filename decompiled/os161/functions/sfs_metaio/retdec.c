int32_t sfs_metaio(int32_t * sv, int64_t actualpos, char * data, int32_t len, uint32_t rw) {
    int32_t v1 = (int32_t)sv;
    if (!vfs_biglock_do_i_hold()) {
        // 0x80009340
        badassert("vfs_biglock_do_i_hold()", "../../fs/sfs/sfs_io.c", 427, "sfs_metaio");
    }
    uint32_t v2 = (int32_t)data;
    int32_t v3 = actualpos;
    int32_t v4; // 0x800092f8
    int32_t v5; // 0x800092f8
    if (v3 > -1) {
        // 0x80009374
        v4 = v2 / 512 | 0x800000 * v3;
        v5 = v2 % 512;
    } else {
        uint32_t v6 = v2 + 511;
        v4 = 0x800000 * ((int32_t)(data > (char *)-512) + v3) | v6 / 512;
        v5 = (v6 | -512) + 1;
    }
    // 0x80009398
    int32_t diskblock; // bp-40, 0x800092f8
    int32_t v7; // 0x800092f8
    int32_t result = sfs_bmap(sv, v4, v7 == 1, &diskblock); // 0x800093ac
    if (result != 0) {
        // 0x800094bc
        return result;
    }
    // 0x800093b8
    if (diskblock == 0) {
        if (v7 != 0) {
            // 0x800093d0
            badassert("rw == UIO_READ", "../../fs/sfs/sfs_io.c", 442, "sfs_metaio");
        }
        // 0x800093ec
        bzero((char *)len, rw);
        // 0x800094bc
        return 0;
    }
    int32_t * v8 = (int32_t *)*(int32_t *)*(int32_t *)(v1 + 12); // 0x80009410
    int32_t result2 = sfs_readblock(v8, diskblock, (char *)&g25, 512); // 0x80009410
    if (result2 != 0) {
        // 0x800094bc
        return result2;
    }
    int32_t v9 = v5 + (int32_t)&g25;
    if (v7 == 0) {
        // 0x80009424
        memcpy((char *)len, (int32_t *)v9, rw);
        // 0x800094bc
        return 0;
    }
    // 0x80009444
    memcpy((char *)v9, (int32_t *)len, rw);
    int32_t result3 = sfs_writeblock(v8, diskblock, (char *)&g25, 512); // 0x80009468
    if (result3 != 0) {
        // 0x800094bc
        return result3;
    }
    uint32_t v10 = v2 + rw; // 0x8000947c
    int32_t v11 = (int32_t)(v10 < rw) + v3; // 0x80009484
    int32_t * v12 = (int32_t *)(v1 + 24);
    if (v11 > 0) {
        // 0x800094a4
        *v12 = v10;
        *(char *)(v1 + 540) = 1;
        // 0x800094bc
        return 0;
    }
    // 0x80009494
    if (v11 != 0 | *v12 >= v10) {
        // 0x800094bc
        return 0;
    }
    // 0x800094a4
    *v12 = v10;
    *(char *)(v1 + 540) = 1;
    // 0x800094bc
    return 0;
}