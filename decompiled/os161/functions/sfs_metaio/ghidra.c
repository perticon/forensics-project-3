int sfs_metaio(sfs_vnode *sv,off_t actualpos,void *data,size_t len,uio_rw rw)

{
  undefined3 extraout_var;
  bool bVar4;
  uio_rw uVar1;
  uint uVar2;
  int iVar3;
  size_t sVar5;
  sfs_fs *sfs;
  int in_stack_00000018;
  daddr_t local_28 [3];
  
  sfs = (sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data;
  bVar4 = vfs_biglock_do_i_hold();
  sVar5 = len;
  if (CONCAT31(extraout_var,bVar4) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../fs/sfs/sfs_io.c",0x1ab,"sfs_metaio");
  }
  uVar1 = rw;
  if ((int)len < 0) {
    uVar1 = rw + 0x1ff;
    sVar5 = (uVar1 < rw) + len;
  }
  uVar2 = rw & 0x1ff;
  if ((int)len < 0) {
    uVar2 = (uVar2 - 1 | 0xfffffe00) + 1;
  }
  iVar3 = sfs_bmap(sv,sVar5 << 0x17 | uVar1 >> 9,in_stack_00000018 == 1,local_28);
  if (iVar3 == 0) {
    if (local_28[0] == 0) {
      if (in_stack_00000018 != 0) {
        badassert("rw == UIO_READ","../../fs/sfs/sfs_io.c",0x1ba,"sfs_metaio");
      }
      bzero(actualpos._0_4_,actualpos._4_4_);
      iVar3 = 0;
    }
    else {
      iVar3 = sfs_readblock(sfs,local_28[0],sfs_metaio::metaiobuf,0x200);
      if (iVar3 == 0) {
        if (in_stack_00000018 == 0) {
          memcpy(actualpos._0_4_,sfs_metaio::metaiobuf + uVar2,actualpos._4_4_);
          iVar3 = 0;
        }
        else {
          memcpy(sfs_metaio::metaiobuf + uVar2,actualpos._0_4_,actualpos._4_4_);
          iVar3 = sfs_writeblock(sfs,local_28[0],sfs_metaio::metaiobuf,0x200);
          if (iVar3 == 0) {
            uVar2 = actualpos._4_4_ + rw;
            iVar3 = (uVar2 < actualpos._4_4_) + len;
            if ((iVar3 < 1) && ((iVar3 != 0 || (uVar2 <= (sv->sv_i).sfi_size)))) {
              iVar3 = 0;
            }
            else {
              (sv->sv_i).sfi_size = uVar2;
              sv->sv_dirty = true;
              iVar3 = 0;
            }
          }
        }
      }
    }
  }
  return iVar3;
}