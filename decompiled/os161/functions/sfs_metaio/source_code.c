sfs_metaio(struct sfs_vnode *sv, off_t actualpos, void *data, size_t len,
	   enum uio_rw rw)
{
	struct sfs_fs *sfs = sv->sv_absvn.vn_fs->fs_data;
	off_t endpos;
	uint32_t vnblock;
	uint32_t blockoffset;
	daddr_t diskblock;
	bool doalloc;
	int result;

	/*
	 * I/O buffer for metadata ops.
	 *
	 * Note: in real life (and when you've done the fs assignment) you
	 * would get space from the disk buffer cache for this, not use a
	 * static area.
	 */
	static char metaiobuf[SFS_BLOCKSIZE];

	/* We're using a global static buffer; it had better be locked */
	KASSERT(vfs_biglock_do_i_hold());

	/* Figure out which block of the vnode (directory, whatever) this is */
	vnblock = actualpos / SFS_BLOCKSIZE;
	blockoffset = actualpos % SFS_BLOCKSIZE;

	/* Get the disk block number */
	doalloc = (rw == UIO_WRITE);
	result = sfs_bmap(sv, vnblock, doalloc, &diskblock);
	if (result) {
		return result;
	}

	if (diskblock == 0) {
		/* Should only get block 0 back if doalloc is false */
		KASSERT(rw == UIO_READ);

		/* Sparse file, read as zeros. */
		bzero(data, len);
		return 0;
	}

	/* Read the block */
	result = sfs_readblock(sfs, diskblock, metaiobuf, sizeof(metaiobuf));
	if (result) {
		return result;
	}

	if (rw == UIO_READ) {
		/* Copy out the selected region */
		memcpy(data, metaiobuf + blockoffset, len);
	}
	else {
		/* Update the selected region */
		memcpy(metaiobuf + blockoffset, data, len);

		/* Write the block back */
		result = sfs_writeblock(sfs, diskblock,
					metaiobuf, sizeof(metaiobuf));
		if (result) {
			return result;
		}

		/* Update the vnode size if needed */
		endpos = actualpos + len;
		if (endpos > (off_t)sv->sv_i.sfi_size) {
			sv->sv_i.sfi_size = endpos;
			sv->sv_dirty = true;
		}
	}

	/* Done */
	return 0;
}