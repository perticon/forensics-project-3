int32_t ITEMcheckvoid(int32_t * data) {
    // 0x80009fb0
    int32_t k1; // bp-24, 0x80009fb0
    KEYget(&k1);
    int32_t v1; // 0x80009fb0
    return KEYcompare(k1, v1) == 0;
}