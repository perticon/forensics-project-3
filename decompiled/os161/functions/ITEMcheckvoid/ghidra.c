int ITEMcheckvoid(Item data)

{
  int iVar1;
  Key k1;
  Key k2;
  
  KEYget((Item)&k1);
  iVar1 = KEYcompare(k1,(Key)0xffffffffffffffff);
  return (uint)(iVar1 == 0);
}