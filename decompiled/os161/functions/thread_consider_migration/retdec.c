void thread_consider_migration(void) {
    uint32_t v1 = g34; // 0x80017014
    int32_t v2 = 0; // 0x800170b4
    int32_t v3 = 0; // 0x800170b4
    int32_t v4; // 0x80016fe8
    if (v1 != 0) {
        int32_t v5 = 0;
        int32_t v6 = 0; // 0x80017098
        int32_t v7 = 0; // 0x800170a8
        int32_t v8 = 4 * v7; // 0x8001703c
        if (v7 >= v1) {
            // 0x80017040
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v8 = &g41;
        }
        int32_t v9 = *(int32_t *)(allcpus + v8); // 0x8001706c
        int32_t * v10 = (int32_t *)(v9 + 84); // 0x8001707c
        spinlock_acquire(v10);
        int32_t v11 = *(int32_t *)(v9 + 80); // 0x80017080
        v6 += v11;
        v5 = *(int32_t *)*(int32_t *)(v4 + 80) == v9 ? v11 : v5;
        spinlock_release(v10);
        v7++;
        v2 = v6;
        v3 = v5;
        while (v7 < v1) {
            // 0x80017060
            v8 = 4 * v7;
            if (v7 >= g34) {
                // 0x80017040
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v8 = &g41;
            }
            // 0x80017060
            v9 = *(int32_t *)(allcpus + v8);
            v10 = (int32_t *)(v9 + 84);
            spinlock_acquire(v10);
            v11 = *(int32_t *)(v9 + 80);
            v6 += v11;
            v5 = *(int32_t *)*(int32_t *)(v4 + 80) == v9 ? v11 : v5;
            spinlock_release(v10);
            v7++;
            v2 = v6;
            v3 = v5;
        }
    }
    uint32_t v12 = (v1 - 1 + v2) / v1; // 0x800170c4
    if (v3 < v12) {
        // 0x80017304
        return;
    }
    int32_t v13 = v3 - v12; // 0x800170dc
    int32_t victims; // bp-80, 0x80016fe8
    threadlist_init(&victims);
    int32_t * v14 = (int32_t *)(v4 + 80); // 0x800170e8
    spinlock_acquire((int32_t *)(*v14 + 84));
    int32_t v15 = *v14;
    int32_t v16 = 0; // 0x80017120
    int32_t v17 = v15; // 0x80017120
    if (v13 != 0) {
        int32_t * v18 = threadlist_remtail((int32_t *)(v15 + 56)); // 0x80017104
        threadlist_addhead(&victims, v18);
        v16++;
        int32_t v19 = *v14;
        v17 = v19;
        while (v16 < v13) {
            // 0x800170fc
            v18 = threadlist_remtail((int32_t *)(v19 + 56));
            threadlist_addhead(&victims, v18);
            v16++;
            v19 = *v14;
            v17 = v19;
        }
    }
    // 0x80017124
    spinlock_release((int32_t *)(v17 + 84));
    int32_t v20 = 0; // 0x80017270
    if (v1 != 0 && v13 != 0) {
        int32_t v21 = 4 * v20; // 0x8001715c
        if (v20 >= g34) {
            // 0x80017160
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v21 = &g41;
        }
        int32_t v22 = *(int32_t *)(allcpus + v21); // 0x8001718c
        int32_t v23 = v13; // 0x800171a4
        int32_t v24; // 0x80016fe8
        int32_t v25; // 0x80016fe8
        int32_t v26; // 0x80016fe8
        int32_t v27; // 0x80016fe8
        int32_t * v28; // 0x800171b0
        int32_t * v29; // 0x8001723c
        int32_t * v30; // 0x80016fe8
        int32_t * v31; // 0x80016fe8
        char * v32; // 0x80016fe8
        int32_t * v33; // 0x80016fe8
        int32_t * v34; // 0x800171c0
        int32_t v35; // 0x800171c0
        int32_t v36; // 0x8001722c
        int32_t v37; // 0x80017210
        if (*(int32_t *)*v14 != v22) {
            // 0x800171a8
            v28 = (int32_t *)(v22 + 84);
            spinlock_acquire(v28);
            v29 = (int32_t *)(v22 + 80);
            v27 = v13;
            if (*v29 < v12) {
                // 0x800171bc
                v30 = (int32_t *)(v22 + 56);
                v31 = (int32_t *)(v22 + 4);
                v32 = (char *)(v22 + 52);
                v33 = (int32_t *)v22;
                v34 = threadlist_remhead(&victims);
                v35 = (int32_t)v34;
                if (v4 == v35) {
                    // 0x800171cc
                    threadlist_addtail(&victims, v34);
                    v25 = v13 - 1;
                } else {
                    // 0x800171e0
                    *(int32_t *)(v35 + 80) = v22;
                    threadlist_addtail(v30, v34);
                    if ((dbflags & 16) != 0) {
                        // 0x80017204
                        v37 = *(int32_t *)(*v14 + 4);
                        kprintf("Migrated thread %s: cpu %u -> %u", (char *)*v34, v37, *v31);
                    }
                    // 0x80017220
                    v36 = v13 - 1;
                    v25 = v36;
                    if (*v32 != 0) {
                        // 0x80017230
                        ipi_send(v33, 2);
                        v25 = v36;
                    }
                }
                // 0x8001723c
                v26 = v25;
                v27 = v26;
                while (!((v26 == 0 | *v29 >= v12))) {
                    // 0x800171bc
                    v24 = v26;
                    v34 = threadlist_remhead(&victims);
                    v35 = (int32_t)v34;
                    if (v4 == v35) {
                        // 0x800171cc
                        threadlist_addtail(&victims, v34);
                        v25 = v24 - 1;
                    } else {
                        // 0x800171e0
                        *(int32_t *)(v35 + 80) = v22;
                        threadlist_addtail(v30, v34);
                        if ((dbflags & 16) != 0) {
                            // 0x80017204
                            v37 = *(int32_t *)(*v14 + 4);
                            kprintf("Migrated thread %s: cpu %u -> %u", (char *)*v34, v37, *v31);
                        }
                        // 0x80017220
                        v36 = v24 - 1;
                        v25 = v36;
                        if (*v32 != 0) {
                            // 0x80017230
                            ipi_send(v33, 2);
                            v25 = v36;
                        }
                    }
                    // 0x8001723c
                    v26 = v25;
                    v27 = v26;
                }
            }
            // 0x80017258
            spinlock_release(v28);
            v23 = v27;
        }
        int32_t v38 = v23;
        v20++;
        while (v20 < v1 && v38 != 0) {
            int32_t v39 = v38;
            v21 = 4 * v20;
            if (v20 >= g34) {
                // 0x80017160
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v21 = &g41;
            }
            // 0x80017180
            v22 = *(int32_t *)(allcpus + v21);
            v23 = v39;
            if (*(int32_t *)*v14 != v22) {
                // 0x800171a8
                v28 = (int32_t *)(v22 + 84);
                spinlock_acquire(v28);
                v29 = (int32_t *)(v22 + 80);
                v27 = v39;
                if (*v29 < v12) {
                    // 0x800171bc
                    v30 = (int32_t *)(v22 + 56);
                    v31 = (int32_t *)(v22 + 4);
                    v32 = (char *)(v22 + 52);
                    v33 = (int32_t *)v22;
                    v24 = v39;
                    v34 = threadlist_remhead(&victims);
                    v35 = (int32_t)v34;
                    if (v4 == v35) {
                        // 0x800171cc
                        threadlist_addtail(&victims, v34);
                        v25 = v24 - 1;
                    } else {
                        // 0x800171e0
                        *(int32_t *)(v35 + 80) = v22;
                        threadlist_addtail(v30, v34);
                        if ((dbflags & 16) != 0) {
                            // 0x80017204
                            v37 = *(int32_t *)(*v14 + 4);
                            kprintf("Migrated thread %s: cpu %u -> %u", (char *)*v34, v37, *v31);
                        }
                        // 0x80017220
                        v36 = v24 - 1;
                        v25 = v36;
                        if (*v32 != 0) {
                            // 0x80017230
                            ipi_send(v33, 2);
                            v25 = v36;
                        }
                    }
                    // 0x8001723c
                    v26 = v25;
                    v27 = v26;
                    while (!((v26 == 0 | *v29 >= v12))) {
                        // 0x800171bc
                        v24 = v26;
                        v34 = threadlist_remhead(&victims);
                        v35 = (int32_t)v34;
                        if (v4 == v35) {
                            // 0x800171cc
                            threadlist_addtail(&victims, v34);
                            v25 = v24 - 1;
                        } else {
                            // 0x800171e0
                            *(int32_t *)(v35 + 80) = v22;
                            threadlist_addtail(v30, v34);
                            if ((dbflags & 16) != 0) {
                                // 0x80017204
                                v37 = *(int32_t *)(*v14 + 4);
                                kprintf("Migrated thread %s: cpu %u -> %u", (char *)*v34, v37, *v31);
                            }
                            // 0x80017220
                            v36 = v24 - 1;
                            v25 = v36;
                            if (*v32 != 0) {
                                // 0x80017230
                                ipi_send(v33, 2);
                                v25 = v36;
                            }
                        }
                        // 0x8001723c
                        v26 = v25;
                        v27 = v26;
                    }
                }
                // 0x80017258
                spinlock_release(v28);
                v23 = v27;
            }
            // 0x80017264
            v38 = v23;
            v20++;
        }
    }
    // 0x8001727c
    if (!threadlist_isempty(&victims)) {
        // 0x8001728c
        spinlock_acquire((int32_t *)(*v14 + 84));
        int32_t * v40 = threadlist_remhead(&victims); // 0x800172b8
        int32_t v41 = *v14;
        int32_t v42 = v41; // 0x800172c0
        if (v40 != NULL) {
            threadlist_addtail((int32_t *)(v41 + 56), v40);
            int32_t * v43 = threadlist_remhead(&victims); // 0x800172b8
            int32_t v44 = *v14;
            int32_t v45 = v44; // 0x800172c0
            int32_t * v46 = v43; // 0x800172c0
            v42 = v44;
            while (v43 != NULL) {
                // 0x800172a0
                threadlist_addtail((int32_t *)(v45 + 56), v46);
                v43 = threadlist_remhead(&victims);
                v44 = *v14;
                v45 = v44;
                v46 = v43;
                v42 = v44;
            }
        }
        // 0x800172c4
        spinlock_release((int32_t *)(v42 + 84));
    }
    // 0x800172d0
    if (!threadlist_isempty(&victims)) {
        // 0x800172e0
        badassert("threadlist_isempty(&victims)", "../../thread/thread.c", 959, "thread_consider_migration");
    }
    // 0x800172fc
    threadlist_cleanup(&victims);
}