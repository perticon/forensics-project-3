void thread_consider_migration(void)

{
  uint uVar1;
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  int iVar2;
  bool bVar4;
  thread *ptVar3;
  uint uVar5;
  cpu *pcVar6;
  uint uVar7;
  int iVar8;
  uint uVar9;
  thread *unaff_s7;
  threadlist victims;
  
  uVar1 = allcpus.arr.num;
  iVar8 = 0;
  uVar9 = 0;
  for (uVar5 = 0; uVar5 < uVar1; uVar5 = uVar5 + 1) {
    iVar2 = uVar5 << 2;
    if (allcpus.arr.num <= uVar5) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    pcVar6 = *(cpu **)((int)allcpus.arr.v + iVar2);
    spinlock_acquire(&pcVar6->c_runqueue_lock);
    uVar7 = (pcVar6->c_runqueue).tl_count;
    iVar8 = iVar8 + uVar7;
    if (unaff_s7->t_cpu->c_self == pcVar6) {
      uVar9 = uVar7;
    }
    spinlock_release(&pcVar6->c_runqueue_lock);
  }
  uVar5 = ((iVar8 + uVar1) - 1) / uVar1;
  if (uVar1 == 0) {
    trap(0x1c00);
  }
  uVar7 = uVar9 - uVar5;
  if (uVar5 <= uVar9) {
    threadlist_init(&victims);
    spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
    for (uVar9 = 0; uVar9 < uVar7; uVar9 = uVar9 + 1) {
      ptVar3 = threadlist_remtail(&unaff_s7->t_cpu->c_runqueue);
      threadlist_addhead(&victims,ptVar3);
    }
    spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
    uVar9 = 0;
    while ((uVar9 < uVar1 && (uVar7 != 0))) {
      iVar8 = uVar9 << 2;
      if (allcpus.arr.num <= uVar9) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      pcVar6 = *(cpu **)((int)allcpus.arr.v + iVar8);
      if (unaff_s7->t_cpu->c_self != pcVar6) {
        spinlock_acquire(&pcVar6->c_runqueue_lock);
        while (((pcVar6->c_runqueue).tl_count < uVar5 && (uVar7 != 0))) {
          ptVar3 = threadlist_remhead(&victims);
          if (ptVar3 == unaff_s7) {
            threadlist_addtail(&victims,ptVar3);
            uVar7 = uVar7 - 1;
          }
          else {
            ptVar3->t_cpu = pcVar6;
            threadlist_addtail(&pcVar6->c_runqueue,ptVar3);
            if ((dbflags & 0x10) != 0) {
              kprintf("Migrated thread %s: cpu %u -> %u",ptVar3->t_name,unaff_s7->t_cpu->c_number,
                      pcVar6->c_number);
            }
            uVar7 = uVar7 - 1;
            if (pcVar6->c_isidle != false) {
              ipi_send(pcVar6,2);
            }
          }
        }
        spinlock_release(&pcVar6->c_runqueue_lock);
      }
      uVar9 = uVar9 + 1;
    }
    bVar4 = threadlist_isempty(&victims);
    if (CONCAT31(extraout_var,bVar4) == 0) {
      spinlock_acquire(&unaff_s7->t_cpu->c_runqueue_lock);
      while (ptVar3 = threadlist_remhead(&victims), ptVar3 != (thread *)0x0) {
        threadlist_addtail(&unaff_s7->t_cpu->c_runqueue,ptVar3);
      }
      spinlock_release(&unaff_s7->t_cpu->c_runqueue_lock);
    }
    bVar4 = threadlist_isempty(&victims);
    if (CONCAT31(extraout_var_00,bVar4) == 0) {
      badassert("threadlist_isempty(&victims)","../../thread/thread.c",0x3bf,
                "thread_consider_migration");
    }
    threadlist_cleanup(&victims);
  }
  return;
}