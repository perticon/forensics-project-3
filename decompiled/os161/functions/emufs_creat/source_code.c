emufs_creat(struct vnode *dir, const char *name, bool excl, mode_t mode,
	    struct vnode **ret)
{
	struct emufs_vnode *ev = dir->vn_data;
	struct emufs_fs *ef = dir->vn_fs->fs_data;
	struct emufs_vnode *newguy;
	uint32_t handle;
	int result;
	int isdir;

	result = emu_open(ev->ev_emu, ev->ev_handle, name, true, excl, mode,
			  &handle, &isdir);
	if (result) {
		return result;
	}

	result = emufs_loadvnode(ef, handle, isdir, &newguy);
	if (result) {
		emu_close(ev->ev_emu, handle);
		return result;
	}

	*ret = &newguy->ev_v;
	return 0;
}