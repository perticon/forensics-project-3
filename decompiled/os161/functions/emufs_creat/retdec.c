int32_t emufs_creat(int32_t * dir, char * name, bool excl, int32_t mode, int32_t * ret) {
    int32_t v1 = (int32_t)dir;
    int32_t v2 = *(int32_t *)(v1 + 16); // 0x8000386c
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80003878
    int32_t * v4 = (int32_t *)(v2 + 24); // 0x8000387c
    int32_t v5 = *(int32_t *)(v2 + 28); // 0x80003880
    int32_t handle; // bp-28, 0x80003854
    int32_t isdir; // bp-24, 0x80003854
    int32_t result = emu_open((int32_t *)*v4, v5, name, true, excl, mode, &handle, &isdir); // 0x800038a4
    if (result != 0) {
        // 0x800038f4
        return result;
    }
    // 0x800038b0
    int32_t * newguy; // bp-32, 0x80003854
    int32_t v6 = emufs_loadvnode((int32_t *)v3, handle, isdir, &newguy); // 0x800038bc
    int32_t result2; // 0x80003854
    if (v6 == 0) {
        // 0x800038e0
        *ret = (int32_t)newguy;
        result2 = 0;
    } else {
        // 0x800038c8
        emu_close((int32_t *)*v4, handle);
        result2 = v6;
    }
    // 0x800038f4
    return result2;
}