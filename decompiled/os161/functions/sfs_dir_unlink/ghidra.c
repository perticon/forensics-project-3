int sfs_dir_unlink(sfs_vnode *sv,int slot)

{
  int iVar1;
  sfs_direntry sd;
  
  bzero(&sd,0x40);
  sd.sfd_ino = 0;
  iVar1 = sfs_writedir(sv,slot,&sd);
  return iVar1;
}