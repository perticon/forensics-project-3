sfs_dir_unlink(struct sfs_vnode *sv, int slot)
{
	struct sfs_direntry sd;

	/* Initialize a suitable directory entry... */
	bzero(&sd, sizeof(sd));
	sd.sfd_ino = SFS_NOINO;

	/* ... and write it */
	return sfs_writedir(sv, slot, &sd);
}