int32_t sfs_dir_unlink(int32_t * sv, int32_t slot) {
    // 0x80007cb8
    int32_t sd; // bp-80, 0x80007cb8
    bzero((char *)&sd, 64);
    sd = 0;
    return sfs_writedir(sv, slot, &sd);
}