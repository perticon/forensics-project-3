semfs_getsembynum(struct semfs *semfs, unsigned semnum)
{
	struct semfs_sem *sem;

	lock_acquire(semfs->semfs_tablelock);
	sem = semfs_semarray_get(semfs->semfs_sems, semnum);
	lock_release(semfs->semfs_tablelock);

	return sem;
}