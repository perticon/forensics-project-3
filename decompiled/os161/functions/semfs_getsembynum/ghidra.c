semfs_sem * semfs_getsembynum(semfs *semfs,uint semnum)

{
  semfs_semarray *psVar1;
  semfs_sem *psVar2;
  
  lock_acquire(semfs->semfs_tablelock);
  psVar1 = semfs->semfs_sems;
  if ((psVar1->arr).num <= semnum) {
    badassert("index < a->num","../../include/array.h",100,"array_get");
  }
  psVar2 = (semfs_sem *)(psVar1->arr).v[semnum];
  lock_release(semfs->semfs_tablelock);
  return psVar2;
}