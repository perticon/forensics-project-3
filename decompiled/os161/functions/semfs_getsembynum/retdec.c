int32_t * semfs_getsembynum(int32_t * semfs, uint32_t semnum) {
    int32_t v1 = (int32_t)semfs;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x800060a0
    lock_acquire((int32_t *)*v2);
    int32_t v3 = *(int32_t *)(v1 + 16); // 0x800060ac
    int32_t v4 = v3; // 0x800060c4
    if (*(int32_t *)(v3 + 4) <= semnum) {
        // 0x800060c8
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v4 = &g41;
    }
    // 0x800060e4
    lock_release((int32_t *)*v2);
    return (int32_t *)*(int32_t *)(*(int32_t *)v4 + 4 * semnum);
}