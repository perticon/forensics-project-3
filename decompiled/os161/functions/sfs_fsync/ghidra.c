int sfs_fsync(vnode *v)

{
  int iVar1;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  iVar1 = sfs_sync_inode(sv);
  vfs_biglock_release();
  return iVar1;
}