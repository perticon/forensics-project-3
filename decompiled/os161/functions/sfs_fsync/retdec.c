int32_t sfs_fsync(int32_t * v2) {
    // 0x8000954c
    vfs_biglock_acquire();
    int32_t result = sfs_sync_inode((int32_t *)*(int32_t *)((int32_t)v2 + 16)); // 0x80009568
    vfs_biglock_release();
    return result;
}