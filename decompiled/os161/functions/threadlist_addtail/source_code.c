threadlist_addtail(struct threadlist *tl, struct thread *t)
{
	DEBUGASSERT(tl != NULL);
	DEBUGASSERT(t != NULL);

	threadlist_insertbeforenode(t, &tl->tl_tail);
	tl->tl_count++;
}