void console_send(char * junk, char * data, uint32_t len) {
    // 0x8000b2e0
    if (len == 0) {
        // 0x8000b320
        return;
    }
    int32_t v1 = 0;
    int32_t v2 = v1 + 1; // 0x8000b310
    putch((int32_t)*(char *)(v1 + (int32_t)data));
    while (v2 < len) {
        // 0x8000b304
        v1 = v2;
        v2 = v1 + 1;
        putch((int32_t)*(char *)(v1 + (int32_t)data));
    }
}