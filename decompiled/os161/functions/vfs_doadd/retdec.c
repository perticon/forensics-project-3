int32_t vfs_doadd(char * dname, int32_t mountable, int32_t * dev, int32_t * fs) {
    // 0x80018b7c
    vfs_biglock_acquire();
    char * v1 = kstrdup(dname); // 0x80018bbc
    if (v1 == NULL) {
        // 0x80018d3c
        vfs_biglock_release();
        // 0x80018d48
        return 3;
    }
    int32_t v2 = 0; // 0x80018bcc
    int32_t v3; // 0x80018b7c
    int32_t v4; // 0x80018b7c
    int32_t v5; // 0x80018b7c
    int32_t result; // 0x80018b7c
    if (mountable == 0) {
        goto lab_0x80018bec;
    } else {
        char * v6 = mkrawname(v1); // 0x80018bd4
        v2 = (int32_t)v6;
        v3 = 0;
        v4 = v2;
        v5 = 0;
        result = 3;
        if (v6 == NULL) {
            goto lab_0x80018d0c;
        } else {
            goto lab_0x80018bec;
        }
    }
  lab_0x80018bec:;
    int32_t * v7 = dev_create_vnode(dev); // 0x80018bf0
    int32_t v8 = (int32_t)v7; // 0x80018bf0
    v3 = 0;
    v4 = v2;
    v5 = v8;
    result = 3;
    if (v7 != NULL) {
        char * v9 = kmalloc(20); // 0x80018c00
        int32_t v10 = (int32_t)v9; // 0x80018c00
        v3 = v10;
        v4 = v2;
        v5 = v8;
        result = 3;
        if (v9 != NULL) {
            int32_t v11 = (int32_t)fs;
            int32_t v12 = (int32_t)dev;
            *(int32_t *)v9 = (int32_t)v1;
            *(int32_t *)(v10 + 4) = v2;
            *(int32_t *)(v10 + 8) = v12;
            *(int32_t *)(v10 + 12) = v8;
            *(int32_t *)(v10 + 16) = v11;
            char * v13 = NULL; // 0x80018c20
            if (fs != NULL) {
                // 0x80018c24
                v13 = (char *)*(int32_t *)(*(int32_t *)(v11 + 4) + 4);
            }
            int32_t v14 = badnames(v1, (char *)v2, v13); // 0x80018c50
            v3 = v10;
            v4 = v2;
            v5 = v8;
            result = 22;
            if (v14 == 0) {
                int32_t v15 = array_setsize(knowndevs, (int32_t)&g17); // 0x80018c74
                v3 = v10;
                v4 = v2;
                v5 = v8;
                result = v15;
                if (v15 == 0) {
                    int32_t v16 = (int32_t)bootfs_vnode; // 0x80018c68
                    *(int32_t *)(4 * v16 + (int32_t)knowndevs) = v10;
                    if (dev != NULL) {
                        // 0x80018cb0
                        *(int32_t *)(v12 + 12) = v16 + 1;
                    }
                    // 0x80018cb4
                    vfs_biglock_release();
                    // 0x80018d48
                    return 0;
                }
            }
        }
    }
    goto lab_0x80018d0c;
  lab_0x80018d0c:
    // 0x80018d0c
    kfree(v1);
    if (v4 != 0) {
        // 0x80018d14
        kfree((char *)v4);
    }
    if (v5 != 0) {
        // 0x80018d24
        dev_uncreate_vnode((int32_t *)v5);
    }
    // 0x80018d2c
    if (v3 != 0) {
        // 0x80018d34
        kfree((char *)v3);
    }
    // 0x80018d3c
    vfs_biglock_release();
    // 0x80018d48
    return result;
}