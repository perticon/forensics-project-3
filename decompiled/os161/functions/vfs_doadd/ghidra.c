int vfs_doadd(char *dname,int mountable,device *dev,fs *fs)

{
  knowndevarray *pkVar1;
  char *name;
  char *n2;
  char *n3;
  char **ptr;
  vnode *vn;
  int iVar2;
  uint uVar3;
  
  vfs_biglock_acquire();
  name = kstrdup(dname);
  if (name == (char *)0x0) {
    iVar2 = 3;
    vn = (vnode *)0x0;
    ptr = (char **)0x0;
    n2 = (char *)0x0;
  }
  else {
    if (mountable == 0) {
      n2 = (char *)0x0;
    }
    else {
      n2 = mkrawname(name);
      if (n2 == (char *)0x0) {
        iVar2 = 3;
        vn = (vnode *)0x0;
        ptr = (char **)0x0;
        goto fail;
      }
    }
    vn = dev_create_vnode(dev);
    if (vn == (vnode *)0x0) {
      iVar2 = 3;
      ptr = (char **)0x0;
    }
    else {
      ptr = (char **)kmalloc(0x14);
      if (ptr == (char **)0x0) {
        iVar2 = 3;
      }
      else {
        *ptr = name;
        ptr[1] = n2;
        ptr[2] = (char *)dev;
        ptr[3] = (char *)vn;
        ptr[4] = (char *)fs;
        if (fs == (fs *)0x0) {
          n3 = (char *)0x0;
        }
        else {
          n3 = (*fs->fs_ops->fsop_getvolname)(fs);
        }
        iVar2 = badnames(name,n2,n3);
        pkVar1 = knowndevs;
        if (iVar2 == 0) {
          uVar3 = (knowndevs->arr).num;
          iVar2 = array_setsize(&knowndevs->arr,uVar3 + 1);
          if (iVar2 == 0) {
            (pkVar1->arr).v[uVar3] = ptr;
            iVar2 = 0;
          }
          else {
            uVar3 = 0;
          }
          if (iVar2 == 0) {
            if (dev != (device *)0x0) {
              dev->d_devnumber = uVar3 + 1;
            }
            vfs_biglock_release();
            return 0;
          }
        }
        else {
          iVar2 = 0x16;
        }
      }
    }
  }
fail:
  if (name != (char *)0x0) {
    kfree(name);
  }
  if (n2 != (char *)0x0) {
    kfree(n2);
  }
  if (vn != (vnode *)0x0) {
    dev_uncreate_vnode(vn);
  }
  if (ptr != (char **)0x0) {
    kfree(ptr);
  }
  vfs_biglock_release();
  return iVar2;
}