vfs_doadd(const char *dname, int mountable, struct device *dev, struct fs *fs)
{
	char *name=NULL, *rawname=NULL;
	struct knowndev *kd=NULL;
	struct vnode *vnode=NULL;
	const char *volname=NULL;
	unsigned index;
	int result;

	/* Silence warning with gcc 4.8 -Og (but not -O2) */
	index = 0;

	vfs_biglock_acquire();

	name = kstrdup(dname);
	if (name==NULL) {
		result = ENOMEM;
		goto fail;
	}
	if (mountable) {
		rawname = mkrawname(name);
		if (rawname==NULL) {
			result = ENOMEM;
			goto fail;
		}
	}

	vnode = dev_create_vnode(dev);
	if (vnode==NULL) {
		result = ENOMEM;
		goto fail;
	}

	kd = kmalloc(sizeof(struct knowndev));
	if (kd==NULL) {
		result = ENOMEM;
		goto fail;
	}

	kd->kd_name = name;
	kd->kd_rawname = rawname;
	kd->kd_device = dev;
	kd->kd_vnode = vnode;
	kd->kd_fs = fs;

	if (fs!=NULL) {
		volname = FSOP_GETVOLNAME(fs);
	}

	if (badnames(name, rawname, volname)) {
		result = EEXIST;
		goto fail;
	}

	result = knowndevarray_add(knowndevs, kd, &index);
	if (result) {
		goto fail;
	}

	if (dev != NULL) {
		/* use index+1 as the device number, so 0 is reserved */
		dev->d_devnumber = index+1;
	}

	vfs_biglock_release();
	return 0;

 fail:
	if (name) {
		kfree(name);
	}
	if (rawname) {
		kfree(rawname);
	}
	if (vnode) {
		dev_uncreate_vnode(vnode);
	}
	if (kd) {
		kfree(kd);
	}

	vfs_biglock_release();
	return result;
}