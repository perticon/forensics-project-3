int32_t * semfs_vnode_create(int32_t * semfs, int32_t semnum) {
    char * v1 = kmalloc(32); // 0x80006184
    if (v1 == NULL) {
        // 0x800061dc
        return NULL;
    }
    int32_t v2 = (int32_t)v1; // 0x80006184
    int32_t v3 = semnum == -1 ? (int32_t)&semfs_dirops : (int32_t)&semfs_semops;
    *(int32_t *)(v2 + 24) = (int32_t)semfs;
    *(int32_t *)(v2 + 28) = semnum;
    int32_t v4 = vnode_init((int32_t *)v1, (int32_t *)v3, semfs, v1); // 0x800061a8
    int32_t * result = (int32_t *)v1; // 0x800061b0
    if (v4 != 0) {
        // 0x800061b4
        badassert("result == 0", "../../fs/semfs/semfs_vnops.c", 743, "semfs_vnode_create");
        result = NULL;
    }
    // 0x800061dc
    return result;
}