semfs_vnode_create(struct semfs *semfs, unsigned semnum)
{
	const struct vnode_ops *optable;
	struct semfs_vnode *semv;
	int result;

	if (semnum == SEMFS_ROOTDIR) {
		optable = &semfs_dirops;
	}
	else {
		optable = &semfs_semops;
	}

	semv = kmalloc(sizeof(*semv));
	if (semv == NULL) {
		return NULL;
	}

	semv->semv_semfs = semfs;
	semv->semv_semnum = semnum;

	result = vnode_init(&semv->semv_absvn, optable,
			    &semfs->semfs_absfs, semv);
	/* vnode_init doesn't actually fail */
	KASSERT(result == 0);

	return semv;
}