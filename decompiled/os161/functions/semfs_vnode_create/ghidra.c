semfs_vnode * semfs_vnode_create(semfs *semfs,uint semnum)

{
  semfs_vnode *vn;
  int iVar1;
  vnode_ops *ops;
  
  if (semnum == 0xffffffff) {
    ops = &semfs_dirops;
  }
  else {
    ops = &semfs_semops;
  }
  vn = (semfs_vnode *)kmalloc(0x20);
  if (vn != (semfs_vnode *)0x0) {
    vn->semv_semfs = semfs;
    vn->semv_semnum = semnum;
    iVar1 = vnode_init((vnode *)vn,ops,&semfs->semfs_absfs,vn);
    if (iVar1 == 0) {
      return vn;
    }
    badassert("result == 0","../../fs/semfs/semfs_vnops.c",0x2e7,"semfs_vnode_create");
  }
  return (semfs_vnode *)0x0;
}