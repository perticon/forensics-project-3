paddr_t ram_stealmem(ulong npages)

{
  paddr_t pVar1;
  uint uVar2;
  
  pVar1 = firstpaddr;
  uVar2 = firstpaddr + npages * 0x1000;
  if (uVar2 <= lastpaddr) {
    firstpaddr = uVar2;
    return pVar1;
  }
  return 0;
}