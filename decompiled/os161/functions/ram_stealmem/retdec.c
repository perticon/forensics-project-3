int32_t ram_stealmem(int32_t npages) {
    int32_t result = firstpaddr; // 0x8001f550
    int32_t v1 = result + 0x1000 * npages; // 0x8001f558
    if (lastpaddr < v1) {
        // 0x8001f57c
        return 0;
    }
    // 0x8001f574
    firstpaddr = v1;
    return result;
}