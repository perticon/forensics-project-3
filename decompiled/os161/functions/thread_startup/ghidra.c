void thread_startup(anon_subr_void_void_ptr_ulong *entrypoint,void *data1,ulong data2)

{
  int unaff_s7;
  
  *(undefined4 *)(unaff_s7 + 4) = 0;
  *(undefined4 *)(unaff_s7 + 8) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
  as_activate();
  exorcise();
  splx(0);
  (*entrypoint)(data1,data2);
                    /* WARNING: Subroutine does not return */
  thread_exit();
}