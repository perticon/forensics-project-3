void thread_startup(void (*entrypoint)(char *, int32_t), char * data1, int32_t data2) {
    // 0x80016d90
    int32_t v1; // 0x80016d90
    *(int32_t *)(v1 + 4) = 0;
    *(int32_t *)(v1 + 8) = 0;
    spinlock_release((int32_t *)(*(int32_t *)(v1 + 80) + 84));
    as_activate();
    exorcise();
    splx(0);
    thread_exit();
}