mips_timer_set(uint32_t count)
{
	/*
	 * $11 == c0_compare; we can't use the symbolic name inside
	 * the asm string.
	 */
	__asm volatile(
		".set push;"		/* save assembler mode */
		".set mips32;"		/* allow MIPS32 registers */
		"mtc0 %0, $11;"		/* do it */
		".set pop"		/* restore assembler mode */
		:: "r" (count));
}