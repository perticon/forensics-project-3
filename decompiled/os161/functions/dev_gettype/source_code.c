dev_gettype(struct vnode *v, mode_t *ret)
{
	struct device *d = v->vn_data;
	if (d->d_blocks > 0) {
		*ret = S_IFBLK;
	}
	else {
		*ret = S_IFCHR;
	}
	return 0;
}