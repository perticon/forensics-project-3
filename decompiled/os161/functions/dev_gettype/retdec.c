int32_t dev_gettype(int32_t * v2, int32_t * ret) {
    int32_t v1 = *(int32_t *)(*(int32_t *)((int32_t)v2 + 16) + 4); // 0x80017b40
    *ret = v1 == 0 ? 0x6000 : 0x7000;
    return 0;
}