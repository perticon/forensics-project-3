int dev_gettype(vnode *v,mode_t *ret)

{
  if (*(int *)((int)v->vn_data + 4) == 0) {
    *ret = 0x6000;
  }
  else {
    *ret = 0x7000;
  }
  return 0;
}