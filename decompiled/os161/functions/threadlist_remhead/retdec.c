int32_t * threadlist_remhead(int32_t * tl) {
    int32_t v1 = (int32_t)tl;
    int32_t v2 = *(int32_t *)(v1 + 4); // 0x8001788c
    int32_t * result = NULL; // 0x800178a0
    if (*(int32_t *)(v2 + 4) != 0) {
        // 0x800178a4
        threadlist_removenode((int32_t *)v2);
        int32_t * v3 = (int32_t *)(v1 + 24); // 0x800178ac
        *v3 = *v3 - 1;
        result = (int32_t *)*(int32_t *)(v2 + 8);
    }
    // 0x800178cc
    return result;
}