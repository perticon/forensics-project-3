thread * threadlist_remhead(threadlist *tl)

{
  thread *ptVar1;
  threadlistnode *tln;
  
  tln = (tl->tl_head).tln_next;
  if (tln->tln_next == (threadlistnode *)0x0) {
    ptVar1 = (thread *)0x0;
  }
  else {
    threadlist_removenode(tln);
    tl->tl_count = tl->tl_count - 1;
    ptVar1 = tln->tln_self;
  }
  return ptVar1;
}