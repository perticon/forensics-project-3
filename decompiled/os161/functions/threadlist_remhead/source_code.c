threadlist_remhead(struct threadlist *tl)
{
	struct threadlistnode *tln;

	DEBUGASSERT(tl != NULL);

	tln = tl->tl_head.tln_next;
	if (tln->tln_next == NULL) {
		/* list was empty  */
		return NULL;
	}
	threadlist_removenode(tln);
	DEBUGASSERT(tl->tl_count > 0);
	tl->tl_count--;
	return tln->tln_self;
}