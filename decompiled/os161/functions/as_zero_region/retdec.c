void as_zero_region(int32_t paddr, int32_t npages) {
    // 0x8001b404
    bzero((char *)-paddr, 0x1000 * npages);
}