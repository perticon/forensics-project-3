bswap16(uint16_t val)
{
	return    ((val & 0x00ff) << 8)
		| ((val & 0xff00) >> 8);
}