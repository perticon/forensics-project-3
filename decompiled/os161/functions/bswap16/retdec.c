int16_t bswap16(int16_t val) {
    // 0x8000afe0
    return llvm_bswap_i16(val);
}