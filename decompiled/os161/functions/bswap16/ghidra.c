uint16_t bswap16(uint16_t val)

{
  return val << 8 | (ushort)((uint)val >> 8);
}