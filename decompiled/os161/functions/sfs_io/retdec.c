int32_t sfs_io(int32_t * sv, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    int32_t v2 = (int32_t)sv;
    int32_t * v3 = (int32_t *)(v1 + 16); // 0x800090b0
    int32_t v4 = *v3; // 0x800090b0
    int32_t * v5 = (int32_t *)(v1 + 24); // 0x800090b4
    int32_t * v6; // 0x8000908c
    int32_t * v7; // 0x8000908c
    int32_t v8; // 0x8000908c
    int32_t v9; // 0x8000908c
    int32_t * v10; // 0x8000908c
    uint32_t v11; // 0x800090d4
    uint32_t v12; // 0x800090c4
    int32_t * v13; // 0x8000908c
    if (*v5 == 0) {
        // 0x800090c4
        v12 = *(int32_t *)(v2 + 24);
        v13 = (int32_t *)(v1 + 8);
        uint32_t v14 = *v13; // 0x800090c8
        v10 = (int32_t *)(v1 + 12);
        uint32_t v15 = *v10; // 0x800090cc
        if (v14 >= 0) {
            // 0x800090ec
            if (v14 != 0 || v15 >= v12) {
                // 0x800092d4
                return 0;
            }
        }
        // 0x800090fc
        v11 = v15 + v4;
        int32_t v16 = v14 + (int32_t)(v11 < v15); // 0x800090dc
        if (v16 > 0) {
            goto lab_0x80009118;
        } else {
            // 0x80009104
            v7 = v10;
            v6 = v13;
            v8 = v4;
            v9 = 0;
            if (v11 <= v12 || v16 != 0) {
                goto lab_0x8000915c;
            } else {
                goto lab_0x80009118;
            }
        }
    } else {
        // 0x8000908c
        v7 = (int32_t *)(v1 + 12);
        v6 = (int32_t *)(v1 + 8);
        v8 = v4;
        v9 = 0;
        goto lab_0x8000915c;
    }
  lab_0x80009228:;
    uint32_t v17 = *v3; // 0x80009228
    int32_t v18; // 0x8000908c
    int32_t v19; // 0x8000908c
    int32_t result; // 0x8000908c
    if (v17 < 512) {
        // 0x80009258
        v18 = v17;
        result = v19;
        if (v17 == 0) {
            goto lab_0x8000926c;
        } else {
            goto lab_0x80009260;
        }
    } else {
        // 0x80009258
        badassert("uio->uio_resid < SFS_BLOCKSIZE", "../../fs/sfs/sfs_io.c", 365, "sfs_io");
        v18 = (int32_t)"sfs_io";
        goto lab_0x80009260;
    }
  lab_0x80009118:;
    int32_t v20 = v11 - v12; // 0x80009118
    if (v4 <= v20) {
        // 0x80009128
        badassert("uio->uio_resid > extraresid", "../../fs/sfs/sfs_io.c", 317, "sfs_io");
    }
    int32_t v21 = v4 - v20; // 0x80009144
    *v3 = v21;
    v7 = v10;
    v6 = v13;
    v8 = v21;
    v9 = v20;
    goto lab_0x8000915c;
  lab_0x8000915c:;
    int32_t v22 = v8; // 0x8000918c
    uint32_t v23 = *v7; // 0x80009160
    int32_t v24 = v23 % 512; // 0x80009168
    if (*v6 <= 0xffffffff) {
        // 0x8000916c
        v24 = (v23 + 511 | -512) + 1;
    }
    int32_t v25 = v22; // 0x80009180
    if (v24 == 0) {
        goto lab_0x800091c0;
    } else {
        uint32_t v26 = 512 - v24; // 0x80009188
        int32_t v27 = sfs_partialio(sv, uio, v24, v22 < v26 ? v22 : v26); // 0x800091a8
        result = v27;
        if (v27 == 0) {
            // 0x80009184
            v25 = *v3;
            goto lab_0x800091c0;
        } else {
            goto lab_0x8000926c;
        }
    }
  lab_0x800091c0:
    // 0x800091c0
    result = 0;
    if (v25 == 0) {
        goto lab_0x8000926c;
    } else {
        int32_t v28 = 0; // 0x800091e0
        if (*v7 % 512 != 0) {
            // 0x800091e4
            badassert("uio->uio_offset % SFS_BLOCKSIZE == 0", "../../fs/sfs/sfs_io.c", 353, "sfs_io");
            v28 = &g41;
        }
        int32_t v29 = 0; // 0x80009224
        v19 = v28;
        if (v25 < 512) {
            goto lab_0x80009228;
        } else {
            int32_t v30 = sfs_blockio(sv, uio); // 0x80009210
            result = v30;
            while (v30 == 0) {
                int32_t v31 = v29 + 1; // 0x80009218
                v29 = v31;
                v19 = 0;
                if (v31 >= v25 / 512) {
                    goto lab_0x80009228;
                }
                v30 = sfs_blockio(sv, uio);
                result = v30;
            }
            goto lab_0x8000926c;
        }
    }
  lab_0x8000926c:;
    int32_t v32 = *v3; // 0x8000926c
    if (v32 == v4) {
        // 0x800092bc
        *v3 = v4 + v9;
        // 0x800092d4
        return result;
    }
    // 0x8000927c
    if (*v5 != 1) {
        // 0x800092bc
        *v3 = v32 + v9;
        // 0x800092d4
        return result;
    }
    uint32_t v33 = *v6; // 0x8000928c
    uint32_t v34 = *v7; // 0x80009290
    int32_t * v35 = (int32_t *)(v2 + 24);
    if (v33 <= 0) {
        // 0x800092a0
        if (v33 != 0 | *v35 >= v34) {
            // 0x800092bc
            *v3 = v32 + v9;
            // 0x800092d4
            return result;
        }
    }
    // 0x800092b0
    *v35 = v34;
    *(char *)(v2 + 540) = 1;
    // 0x800092bc
    *v3 = *v3 + v9;
    // 0x800092d4
    return result;
  lab_0x80009260:
    // 0x80009260
    result = sfs_partialio(sv, uio, 0, v18);
    goto lab_0x8000926c;
}