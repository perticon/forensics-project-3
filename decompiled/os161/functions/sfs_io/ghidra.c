int sfs_io(sfs_vnode *sv,uio *uio)

{
  int iVar1;
  uint32_t skipstart;
  uint uVar2;
  int iVar3;
  uint uVar4;
  char *len;
  uint uVar5;
  uint uVar6;
  
  uVar5 = uio->uio_resid;
  if (uio->uio_rw == UIO_READ) {
    uVar6 = (sv->sv_i).sfi_size;
    iVar1 = *(int *)&uio->uio_offset;
    uVar2 = *(uint *)((int)&uio->uio_offset + 4);
    uVar4 = uVar2 + uVar5;
    iVar3 = (uint)(uVar4 < uVar2) + iVar1;
    if (-1 < iVar1) {
      if (iVar1 != 0) {
        return 0;
      }
      if (uVar6 <= uVar2) {
        return 0;
      }
    }
    if ((iVar3 < 1) && ((iVar3 != 0 || (uVar4 <= uVar6)))) {
      uVar4 = 0;
    }
    else {
      uVar4 = uVar4 - uVar6;
      if (uVar5 <= uVar4) {
        badassert("uio->uio_resid > extraresid","../../fs/sfs/sfs_io.c",0x13d,"sfs_io");
      }
      uio->uio_resid = uVar5 - uVar4;
    }
  }
  else {
    uVar4 = 0;
  }
  skipstart = *(uint *)((int)&uio->uio_offset + 4) & 0x1ff;
  if (*(int *)&uio->uio_offset < 0) {
    skipstart = (skipstart - 1 | 0xfffffe00) + 1;
  }
  if (skipstart == 0) {
    iVar1 = 0;
  }
  else {
    uVar2 = uio->uio_resid;
    if (0x200 - skipstart <= uio->uio_resid) {
      uVar2 = 0x200 - skipstart;
    }
    iVar1 = sfs_partialio(sv,uio,skipstart,uVar2);
    if (iVar1 != 0) goto out;
  }
  uVar2 = uio->uio_resid;
  if (uVar2 != 0) {
    if ((*(uint *)((int)&uio->uio_offset + 4) & 0x1ff) != 0) {
      badassert("uio->uio_offset % SFS_BLOCKSIZE == 0","../../fs/sfs/sfs_io.c",0x161,"sfs_io");
    }
    uVar6 = 0;
    do {
      if (uVar2 >> 9 <= uVar6) {
        len = (char *)uio->uio_resid;
        if ((char *)0x1ff < len) {
          len = "sfs_io";
          badassert("uio->uio_resid < SFS_BLOCKSIZE","../../fs/sfs/sfs_io.c",0x16d,"sfs_io");
        }
        if (len != (char *)0x0) {
          iVar1 = sfs_partialio(sv,uio,0,(uint32_t)len);
        }
        break;
      }
      iVar1 = sfs_blockio(sv,uio);
      uVar6 = uVar6 + 1;
    } while (iVar1 == 0);
  }
out:
  if ((uio->uio_resid != uVar5) && (uio->uio_rw == UIO_WRITE)) {
    uVar5 = *(uint *)((int)&uio->uio_offset + 4);
    if ((0 < *(int *)&uio->uio_offset) ||
       ((*(int *)&uio->uio_offset == 0 && ((sv->sv_i).sfi_size < uVar5)))) {
      (sv->sv_i).sfi_size = uVar5;
      sv->sv_dirty = true;
    }
  }
  uio->uio_resid = uio->uio_resid + uVar4;
  return iVar1;
}