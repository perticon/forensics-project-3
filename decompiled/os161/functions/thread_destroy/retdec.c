void thread_destroy(int32_t * thread) {
    int32_t v1 = (int32_t)thread;
    int32_t v2 = v1; // 0x80016000
    int32_t v3; // 0x80015ff0
    if (v3 == v1) {
        // 0x80016004
        badassert("thread != curthread", "../../thread/thread.c", 267, "thread_destroy");
        v2 = (int32_t)"thread != curthread";
    }
    int32_t v4 = v2; // 0x8001605c
    if (*(int32_t *)(v2 + 84) != 0) {
        // 0x80016060
        badassert("thread->t_proc == NULL", (char *)-0x7ffd94d8, 276, "thread_destroy");
        v4 = (int32_t)"thread->t_proc == NULL";
    }
    int32_t v5 = *(int32_t *)(v4 + 72); // 0x80016078
    if (v5 != 0) {
        // 0x80016088
        kfree((char *)v5);
    }
    // 0x80016090
    threadlistnode_cleanup((int32_t *)(v1 + 60));
    int32_t v6 = v1 + 12; // 0x8001609c
    thread_machdep_cleanup((int32_t *)v6);
    *(int32_t *)(v1 + 4) = (int32_t)"DESTROYED";
    kfree((char *)v6);
    kfree((char *)thread);
}