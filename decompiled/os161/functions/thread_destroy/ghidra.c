void thread_destroy(thread *thread)

{
  char *pcVar1;
  char *pcVar2;
  thread *unaff_s7;
  
  pcVar1 = (char *)thread;
  if (thread == unaff_s7) {
    pcVar1 = "thread != curthread";
    badassert("thread != curthread","../../thread/thread.c",0x10b,"thread_destroy");
  }
  pcVar2 = (char *)0x80020000;
  if (*(threadstate_t *)((int)pcVar1 + 8) == S_RUN) {
    pcVar1 = "thread->t_state != S_RUN";
    pcVar2 = "../../thread/thread.c";
    badassert("thread->t_state != S_RUN","../../thread/thread.c",0x10c,"thread_destroy");
  }
  if (*(proc **)((int)pcVar1 + 0x54) != (proc *)0x0) {
    pcVar1 = "thread->t_proc == NULL";
    badassert("thread->t_proc == NULL",pcVar2 + 0x6b28,0x114,"thread_destroy");
  }
  if (*(void **)((int)pcVar1 + 0x48) != (void *)0x0) {
    kfree(*(void **)((int)pcVar1 + 0x48));
  }
  threadlistnode_cleanup(&thread->t_listnode);
  thread_machdep_cleanup(&thread->t_machdep);
  thread->t_wchan_name = "DESTROYED";
  kfree(thread->t_name);
  kfree(thread);
  return;
}