semfs_getvnode(struct semfs *semfs, unsigned semnum, struct vnode **ret)
{
	struct vnode *vn;
	struct semfs_vnode *semv;
	struct semfs_sem *sem;
	unsigned i, num;
	int result;

	/* Lock the vnode table */
	lock_acquire(semfs->semfs_tablelock);

	/* Look for it */
	num = vnodearray_num(semfs->semfs_vnodes);
	for (i=0; i<num; i++) {
		vn = vnodearray_get(semfs->semfs_vnodes, i);
		semv = vn->vn_data;
		if (semv->semv_semnum == semnum) {
			VOP_INCREF(vn);
			lock_release(semfs->semfs_tablelock);
			*ret = vn;
			return 0;
		}
	}

	/* Make it */
	semv = semfs_vnode_create(semfs, semnum);
	if (semv == NULL) {
		lock_release(semfs->semfs_tablelock);
		return ENOMEM;
	}
	result = vnodearray_add(semfs->semfs_vnodes, &semv->semv_absvn, NULL);
	if (result) {
		semfs_vnode_destroy(semv);
		lock_release(semfs->semfs_tablelock);
		return ENOMEM;
	}
	if (semnum != SEMFS_ROOTDIR) {
		sem = semfs_semarray_get(semfs->semfs_sems, semnum);
		KASSERT(sem != NULL);
		KASSERT(sem->sems_hasvnode == false);
		sem->sems_hasvnode = true;
	}
	lock_release(semfs->semfs_tablelock);

	*ret = &semv->semv_absvn;
	return 0;
}