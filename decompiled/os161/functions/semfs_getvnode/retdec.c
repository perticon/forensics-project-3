int32_t semfs_getvnode(int32_t * semfs, uint32_t semnum, int32_t * ret) {
    int32_t v1 = (int32_t)semfs;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x80006cd4
    lock_acquire((int32_t *)*v2);
    int32_t * v3 = (int32_t *)(v1 + 12); // 0x80006ce0
    int32_t v4 = *v3; // 0x80006ce0
    int32_t v5 = 0; // 0x80006cf0
    int32_t v6; // 0x80006d00
    while (v5 < *(int32_t *)(v4 + 4)) {
        // 0x80006cf4
        v6 = *(int32_t *)(*(int32_t *)v4 + 4 * v5);
        v5++;
        if (*(int32_t *)(*(int32_t *)(v6 + 16) + 28) == semnum) {
            // 0x80006d20
            vnode_incref((int32_t *)v6);
            lock_release((int32_t *)*v2);
            goto lab_0x80006e94;
        }
    }
    int32_t * v7 = semfs_vnode_create(semfs, semnum); // 0x80006d54
    if (v7 == NULL) {
        // 0x80006d60
        lock_release((int32_t *)*v2);
        // 0x80006e98
        return 3;
    }
    int32_t v8 = *v3; // 0x80006d74
    int32_t v9 = *(int32_t *)(v8 + 4); // 0x80006d7c
    int32_t * v10 = (int32_t *)v8; // 0x80006d88
    if (array_setsize(v10, v9 + 1) != 0) {
        // 0x80006db8
        semfs_vnode_destroy(v7);
        lock_release((int32_t *)*v2);
        // 0x80006e98
        return 3;
    }
    int32_t v11 = (int32_t)v7; // 0x80006d54
    *(int32_t *)(*v10 + 4 * v9) = v11;
    if (semnum != -1) {
        int32_t v12 = *(int32_t *)(v1 + 16); // 0x80006de0
        int32_t v13 = v12; // 0x80006df8
        if (*(int32_t *)(v12 + 4) <= semnum) {
            // 0x80006dfc
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v13 = &g41;
        }
        int32_t v14 = *(int32_t *)(*(int32_t *)v13 + 4 * semnum); // 0x80006e24
        int32_t v15 = v14; // 0x80006e30
        if (v14 == 0) {
            // 0x80006e34
            badassert("sem != NULL", "../../fs/semfs/semfs_vnops.c", 791, "semfs_getvnode");
            v15 = &g41;
        }
        int32_t v16 = v15; // 0x80006e5c
        if (*(char *)(v15 + 12) != 0) {
            // 0x80006e60
            badassert("sem->sems_hasvnode == false", "../../fs/semfs/semfs_vnops.c", 792, "semfs_getvnode");
            v16 = &g41;
        }
        // 0x80006e80
        *(char *)(v16 + 12) = 1;
    }
    // 0x80006e84
    lock_release((int32_t *)*v2);
    v6 = v11;
  lab_0x80006e94:
    // 0x80006e94
    *ret = v6;
    // 0x80006e98
    return 0;
}