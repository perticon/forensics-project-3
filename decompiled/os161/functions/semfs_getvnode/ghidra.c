int semfs_getvnode(semfs *semfs,uint semnum,vnode **ret)

{
  uint uVar1;
  semfs_vnode *semv;
  int iVar2;
  semfs_semarray *psVar3;
  void *pvVar4;
  undefined uVar5;
  vnode *vn;
  array *a;
  
  lock_acquire(semfs->semfs_tablelock);
  uVar1 = 0;
  while (uVar1 < (semfs->semfs_vnodes->arr).num) {
    vn = (vnode *)(semfs->semfs_vnodes->arr).v[uVar1];
    uVar1 = uVar1 + 1;
    if (*(uint *)((int)vn->vn_data + 0x1c) == semnum) {
      vnode_incref(vn);
      lock_release(semfs->semfs_tablelock);
      *ret = vn;
      return 0;
    }
  }
  semv = semfs_vnode_create(semfs,semnum);
  if (semv == (semfs_vnode *)0x0) {
    lock_release(semfs->semfs_tablelock);
    return 3;
  }
  a = (array *)semfs->semfs_vnodes;
  uVar1 = a->num;
  iVar2 = array_setsize(a,uVar1 + 1);
  if (iVar2 == 0) {
    a->v[uVar1] = semv;
    iVar2 = 0;
  }
  if (iVar2 == 0) {
    if (semnum != 0xffffffff) {
      psVar3 = semfs->semfs_sems;
      if ((psVar3->arr).num <= semnum) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      pvVar4 = (psVar3->arr).v[semnum];
      if (pvVar4 == (void *)0x0) {
        badassert("sem != NULL","../../fs/semfs/semfs_vnops.c",0x317,"semfs_getvnode");
      }
      uVar5 = 1;
      if (*(char *)((int)pvVar4 + 0xc) != '\0') {
        uVar5 = 1;
        badassert("sem->sems_hasvnode == false","../../fs/semfs/semfs_vnops.c",0x318,
                  "semfs_getvnode");
      }
      *(undefined *)((int)pvVar4 + 0xc) = uVar5;
    }
    lock_release(semfs->semfs_tablelock);
    *ret = &semv->semv_absvn;
    return 0;
  }
  semfs_vnode_destroy(semv);
  lock_release(semfs->semfs_tablelock);
  return 3;
}