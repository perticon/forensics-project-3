int shutdown(int __fd,int __how)

{
  int iVar1;
  
  kprintf("Shutting down.\n");
  vm_shutdown();
  vfs_clearbootfs();
  vfs_clearcurdir();
  vfs_unmountall();
  thread_shutdown();
  iVar1 = splx(1);
  return iVar1;
}