shutdown(void)
{

	kprintf("Shutting down.\n");
	#if OPT_PAGING
	vm_shutdown();
	#endif
	vfs_clearbootfs();
	vfs_clearcurdir();
	vfs_unmountall();

	thread_shutdown();

	splhigh();
}