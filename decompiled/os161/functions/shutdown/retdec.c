void shutdown(void) {
    // 0x8000bb10
    kprintf("Shutting down.\n");
    vm_shutdown();
    vfs_clearbootfs();
    vfs_clearcurdir();
    vfs_unmountall();
    thread_shutdown();
    splx(1);
}