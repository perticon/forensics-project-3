int32_t emufs_lookparent(int32_t * dir, char * pathname, int32_t * ret, char * buf, uint32_t len) {
    char * v1 = strrchr(pathname, 47); // 0x80003798
    int32_t result; // 0x80003764
    if (v1 == NULL) {
        // 0x800037a4
        result = 7;
        if (strlen(pathname) + 1 <= len) {
            // 0x800037c4
            vnode_incref(dir);
            *ret = (int32_t)dir;
            strcpy(buf, pathname);
            result = 0;
        }
    } else {
        // 0x800037e4
        *v1 = 0;
        char * v2 = (char *)((int32_t)v1 + 1); // 0x800037ec
        result = 7;
        if (strlen(v2) + 1 <= len) {
            // 0x80003808
            strcpy(buf, v2);
            result = emufs_lookup(dir, pathname, ret);
        }
    }
    // 0x80003834
    return result;
}