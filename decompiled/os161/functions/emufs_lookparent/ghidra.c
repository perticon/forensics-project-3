int emufs_lookparent(vnode *dir,char *pathname,vnode **ret,char *buf,size_t len)

{
  char *pcVar1;
  size_t sVar2;
  int iVar3;
  
  pcVar1 = strrchr(pathname,0x2f);
  if (pcVar1 == (char *)0x0) {
    sVar2 = strlen(pathname);
    if (len < sVar2 + 1) {
      iVar3 = 7;
    }
    else {
      vnode_incref(dir);
      *ret = dir;
      strcpy(buf,pathname);
      iVar3 = 0;
    }
  }
  else {
    *pcVar1 = '\0';
    sVar2 = strlen(pcVar1 + 1);
    if (len < sVar2 + 1) {
      iVar3 = 7;
    }
    else {
      strcpy(buf,pcVar1 + 1);
      iVar3 = emufs_lookup(dir,pathname,ret);
    }
  }
  return iVar3;
}