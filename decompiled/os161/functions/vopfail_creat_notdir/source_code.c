vopfail_creat_notdir(struct vnode *vn, const char *name, bool excl,
		     mode_t mode, struct vnode **result)
{
	(void)vn;
	(void)name;
	(void)excl;
	(void)mode;
	(void)result;
	return ENOTDIR;
}