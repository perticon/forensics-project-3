mainbus_poweroff(void)
{
	/*
	 *
	 * Note that lamebus_write_register() doesn't actually access
	 * the bus argument, so this will still work if we get here
	 * before the bus is initialized.
	 */
	lamebus_poweroff(lamebus);
}