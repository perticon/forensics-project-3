kheap_nextgeneration(void)
{
#ifdef LABELS
	spinlock_acquire(&kmalloc_spinlock);
	mallocgeneration++;
	spinlock_release(&kmalloc_spinlock);
#endif
}