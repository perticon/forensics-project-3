lser_write(void *vls, int ch)
{
	struct lser_softc *ls = vls;

	spinlock_acquire(&ls->ls_lock);

	if (ls->ls_wbusy) {
		/*
		 * We're not clear to write.
		 *
		 * This should not happen. It's the job of the driver
		 * attached to us to not write until we call
		 * ls->ls_start.
		 *
		 * (Note: if we're the console, the panic will go to
		 * lser_writepolled for printing, because we hold a
		 * spinlock and interrupts are off; it won't recurse.)
		 */
		panic("lser: Not clear to write\n");
	}
	ls->ls_wbusy = true;

	bus_write_register(ls->ls_busdata, ls->ls_buspos, LSER_REG_CHAR, ch);

	spinlock_release(&ls->ls_lock);
}