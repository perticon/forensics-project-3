void lser_write(char * vls, int32_t ch) {
    int32_t v1 = (int32_t)vls;
    spinlock_acquire((int32_t *)vls);
    char * v2 = (char *)(v1 + 8); // 0x80004d70
    char v3 = 1; // 0x80004d80
    if (*v2 != 0) {
        // 0x80004d84
        panic("lser: Not clear to write\n");
        v3 = &g41;
    }
    // 0x80004d90
    *v2 = v3;
    int32_t v4 = *(int32_t *)(v1 + 16); // 0x80004d98
    lamebus_write_register((int32_t *)*(int32_t *)(v1 + 12), v4, 0, ch);
    spinlock_release((int32_t *)vls);
}