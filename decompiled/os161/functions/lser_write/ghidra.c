void lser_write(void *vls,int ch)

{
  spinlock_acquire((spinlock *)vls);
  if (*(char *)((int)vls + 8) != '\0') {
                    /* WARNING: Subroutine does not return */
    panic("lser: Not clear to write\n");
  }
  *(undefined *)((int)vls + 8) = 1;
  lamebus_write_register(*(lamebus_softc **)((int)vls + 0xc),*(int *)((int)vls + 0x10),0,ch);
  spinlock_release((spinlock *)vls);
  return;
}