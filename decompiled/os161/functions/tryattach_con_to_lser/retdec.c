int32_t tryattach_con_to_lser(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_con_to_lser(devunit, bus); // 0x800017ec
    if (v1 == NULL) {
        // 0x80001864
        return -1;
    }
    // 0x800017f8
    kprintf("con%d at lser%d", devunit, busunit);
    int32_t v2 = config_con(v1, devunit); // 0x80001814
    int32_t result; // 0x800017d0
    if (v2 == 0) {
        // 0x80001840
        kprintf("\n");
        nextunit_con = devunit + 1;
        result = 0;
    } else {
        // 0x80001820
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x80001864
    return result;
}