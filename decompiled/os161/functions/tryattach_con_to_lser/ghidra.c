int tryattach_con_to_lser(int devunit,lser_softc *bus,int busunit)

{
  con_softc *cs;
  int errcode;
  char *pcVar1;
  
  cs = attach_con_to_lser(devunit,bus);
  if (cs == (con_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("con%d at lser%d",devunit,busunit);
    errcode = config_con(cs,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_con = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}