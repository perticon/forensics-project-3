tryattach_con_to_lser(int devunit, struct lser_softc *bus, int busunit)
{
	struct con_softc *dev;
	int result;

	dev = attach_con_to_lser(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("con%d at lser%d", devunit, busunit);
	result = config_con(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_con = devunit+1;
	autoconf_con(dev, devunit);
	return 0;
}