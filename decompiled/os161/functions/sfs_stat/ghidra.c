int sfs_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  vnode_check(v,"gettype");
  iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
  if (iVar1 == 0) {
    *(undefined4 *)((int)&statbuf->st_size + 4) = *(undefined4 *)((int)pvVar2 + 0x18);
    *(undefined4 *)&statbuf->st_size = 0;
    statbuf->st_nlink = *(nlink_t *)((int)pvVar2 + 0x1e);
    statbuf->st_blocks = 0;
    iVar1 = 0;
  }
  return iVar1;
}