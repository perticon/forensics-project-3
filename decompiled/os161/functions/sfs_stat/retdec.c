int32_t sfs_stat(int32_t * v2, int32_t * statbuf) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(v1 + 16); // 0x8000963c
    bzero((char *)statbuf, 88);
    vnode_check(v2, "gettype");
    int32_t result = *(int32_t *)(*(int32_t *)(v1 + 20) + 36); // 0x80009664
    if (result == 0) {
        int32_t v3 = (int32_t)statbuf;
        *(int32_t *)(v3 + 4) = *(int32_t *)(v2_ + 24);
        *statbuf = 0;
        *(int16_t *)(v3 + 12) = *(int16_t *)(v2_ + 30);
        *(int32_t *)(v3 + 16) = 0;
    }
    // 0x800096a0
    return result;
}