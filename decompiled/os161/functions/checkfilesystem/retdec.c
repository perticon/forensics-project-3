int32_t checkfilesystem(int32_t nargs, char ** args) {
    if (nargs != 2) {
        // 0x8000f2c4
        kprintf("Usage: fs[123456] filesystem:\n");
        // 0x8000f314
        return 8;
    }
    int32_t v1 = *(int32_t *)((int32_t)args + 4); // 0x8000f2d8
    char * v2 = (char *)v1; // 0x8000f2e0
    int32_t v3 = strlen(v2); // 0x8000f2e0
    int32_t v4 = v1 - 1;
    if (*(char *)(v4 + v3) == 58) {
        // 0x8000f2f8
        *(char *)(strlen(v2) + v4) = 0;
    }
    // 0x8000f314
    return 0;
}