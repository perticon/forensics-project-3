checkfilesystem(int nargs, char **args)
{
	char *device;

	if (nargs != 2) {
		kprintf("Usage: fs[123456] filesystem:\n");
		return EINVAL;
	}

	device = args[1];

	/* Allow (but do not require) colon after device name */
	if (device[strlen(device)-1]==':') {
		device[strlen(device)-1] = 0;
	}

	return 0;
}