int checkfilesystem(int nargs,char **args)

{
  int iVar1;
  size_t sVar2;
  char *str;
  
  if (nargs == 2) {
    str = args[1];
    sVar2 = strlen(str);
    if (str[sVar2 - 1] == ':') {
      sVar2 = strlen(str);
      str[sVar2 - 1] = '\0';
      iVar1 = 0;
    }
    else {
      iVar1 = 0;
    }
  }
  else {
    kprintf("Usage: fs[123456] filesystem:\n");
    iVar1 = 8;
  }
  return iVar1;
}