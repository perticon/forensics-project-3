int32_t sfs_read(int32_t * v2, int32_t * uio) {
    // 0x80009774
    if (*(int32_t *)((int32_t)uio + 24) != 0) {
        // 0x80009798
        badassert("uio->uio_rw==UIO_READ", "../../fs/sfs/sfs_vnops.c", 103, "sfs_read");
    }
    // 0x800097b8
    vfs_biglock_acquire();
    int32_t result = sfs_io((int32_t *)*(int32_t *)((int32_t)v2 + 16), uio); // 0x800097c8
    vfs_biglock_release();
    return result;
}