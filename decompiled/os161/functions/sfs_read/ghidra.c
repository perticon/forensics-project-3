int sfs_read(vnode *v,uio *uio)

{
  int iVar1;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)v->vn_data;
  if (uio->uio_rw != UIO_READ) {
    badassert("uio->uio_rw==UIO_READ","../../fs/sfs/sfs_vnops.c",0x67,"sfs_read");
  }
  vfs_biglock_acquire();
  iVar1 = sfs_io(sv,uio);
  vfs_biglock_release();
  return iVar1;
}