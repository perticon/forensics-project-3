int32_t emu_getsize(int32_t * sc, int32_t handle, int64_t * retval) {
    int32_t v1 = (int32_t)sc;
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x800029a8
    int32_t * v3 = (int32_t *)*v2; // 0x800029b0
    lock_acquire(v3);
    int32_t * v4 = (int32_t *)(v1 + 4); // 0x800029b8
    lamebus_write_register(v3, *v4, 0, handle);
    lamebus_write_register(v3, *v4, 12, 8);
    int32_t result = emu_waitdone(sc); // 0x800029e0
    if (result == 0) {
        // 0x800029ec
        *(int32_t *)((int32_t)retval + 4) = lamebus_read_register(sc, *v4, 8);
        *(int32_t *)retval = 0;
    }
    // 0x80002a04
    lock_release((int32_t *)*v2);
    return result;
}