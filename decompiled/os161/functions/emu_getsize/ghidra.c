int emu_getsize(emu_softc *sc,uint32_t handle,off_t *retval)

{
  int iVar1;
  uint32_t uVar2;
  
  lock_acquire(sc->e_lock);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
  lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,8);
  iVar1 = emu_waitdone(sc);
  if (iVar1 == 0) {
    uVar2 = lamebus_read_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8);
    *(uint32_t *)((int)retval + 4) = uVar2;
    *(undefined4 *)retval = 0;
  }
  lock_release(sc->e_lock);
  return iVar1;
}