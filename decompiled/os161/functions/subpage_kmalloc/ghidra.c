void * subpage_kmalloc(size_t sz)

{
  char *pcVar1;
  freelist *addr;
  char **ppcVar2;
  uint uVar3;
  pageref *ppVar4;
  char *pcVar5;
  uint16_t uVar6;
  uint uVar7;
  uint uVar8;
  freelist *fl;
  int i;
  
  uVar3 = 0;
  do {
    uVar7 = uVar3;
    if (7 < uVar7) {
                    /* WARNING: Subroutine does not return */
      panic("Subpage allocator cannot handle allocation of size %zu\n");
    }
    uVar3 = uVar7 + 1;
  } while (sizes[uVar7] < sz);
  spinlock_acquire(&kmalloc_spinlock);
  for (ppVar4 = sizebases[uVar7]; ppVar4 != (pageref *)0x0; ppVar4 = ppVar4->next_samesize) {
    if ((ppVar4->pageaddr_and_blocktype & 0xfff) != uVar7) {
      badassert("PR_BLOCKTYPE(pr) == blktype","../../vm/kmalloc.c",0x395,"subpage_kmalloc");
    }
    if (ppVar4->nfree != 0) goto doalloc;
  }
  spinlock_release(&kmalloc_spinlock);
  addr = (freelist *)alloc_kpages(1);
  if (addr == (freelist *)0x0) {
    kprintf("kmalloc: Subpage allocator couldn\'t get a page\n");
    ppcVar2 = (char **)0x0;
  }
  else {
    if (((uint)addr & 0xfff) != 0) {
      badassert("prpage % PAGE_SIZE == 0","../../vm/kmalloc.c",0x3cd,"subpage_kmalloc");
    }
    spinlock_acquire(&kmalloc_spinlock);
    ppVar4 = allocpageref();
    if (ppVar4 == (pageref *)0x0) {
      spinlock_release(&kmalloc_spinlock);
      free_kpages((vaddr_t)addr);
      kprintf("kmalloc: Subpage allocator couldn\'t get pageref\n");
      ppcVar2 = (char **)0x0;
    }
    else {
      ppVar4->pageaddr_and_blocktype = (uint)addr & 0xfffff000 | uVar7 & 0xfff;
      uVar3 = sizes[uVar7];
      if (uVar3 == 0) {
        trap(0x1c00);
      }
      ppVar4->nfree = (uint16_t)(0x1000 / uVar3);
      addr->next = (freelist *)0x0;
      fl = addr;
      for (i = 1; i < (int)(uint)ppVar4->nfree; i = i + 1) {
        fl = (freelist *)((int)&addr->next + uVar3 * i);
        *fl = (freelist)((int)&addr->next + uVar3 * (i + -1));
        if (*(freelist **)fl == fl) {
          badassert("fl != fl->next","../../vm/kmalloc.c",0x3ec,"subpage_kmalloc");
        }
      }
      uVar8 = (int)fl - (int)addr & 0xffff;
      ppVar4->freelist_offset = (uint16_t)uVar8;
      if (uVar8 != uVar3 * (ppVar4->nfree - 1)) {
        badassert("pr->freelist_offset == (pr->nfree-1)*sizes[blktype]","../../vm/kmalloc.c",0x3f0,
                  "subpage_kmalloc");
      }
      ppVar4->next_samesize = sizebases[uVar7];
      sizebases[uVar7] = ppVar4;
      ppVar4->next_all = allbase;
      allbase = ppVar4;
doalloc:
      uVar3 = (uint)ppVar4->freelist_offset;
      pcVar5 = (char *)0xfffff000;
      if (0xfff < uVar3) {
        pcVar5 = "pr->freelist_offset < PAGE_SIZE";
        badassert("pr->freelist_offset < PAGE_SIZE","../../vm/kmalloc.c",0x39c,"subpage_kmalloc");
      }
      pcVar5 = (char *)(ppVar4->pageaddr_and_blocktype & (uint)pcVar5);
      ppcVar2 = (char **)(pcVar5 + uVar3);
      pcVar1 = *ppcVar2;
      uVar6 = ppVar4->nfree - 1;
      ppVar4->nfree = uVar6;
      if (pcVar1 == (char *)0x0) {
        if (uVar6 != 0) {
          badassert("pr->nfree == 0","../../vm/kmalloc.c",0x3ac,"subpage_kmalloc");
        }
        ppVar4->freelist_offset = 0xffff;
      }
      else {
        if (uVar6 == 0) {
          pcVar5 = "pr->nfree > 0";
          badassert("pr->nfree > 0","../../vm/kmalloc.c",0x3a6,"subpage_kmalloc");
        }
        if (0xfff < (uint)((int)pcVar1 - (int)pcVar5)) {
          pcVar5 = "fla - prpage < PAGE_SIZE";
          pcVar1 = "../../vm/kmalloc.c";
          badassert("fla - prpage < PAGE_SIZE","../../vm/kmalloc.c",0x3a8,"subpage_kmalloc");
        }
        ppVar4->freelist_offset = (short)pcVar1 - (short)pcVar5;
      }
      spinlock_release(&kmalloc_spinlock);
    }
  }
  return ppcVar2;
}