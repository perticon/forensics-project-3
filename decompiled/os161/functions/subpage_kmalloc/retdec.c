char * subpage_kmalloc(uint32_t sz) {
    int32_t v1 = 0;
    int32_t v2; // 0x8001c34c
    int32_t v3; // 0x8001c34c
    while (v1 < 8) {
        int32_t v4 = 4 * v1; // 0x8001c3c0
        if (*(int32_t *)(v4 + (int32_t)&sizes) >= sz) {
            // 0x8001c388
            spinlock_acquire(&kmalloc_spinlock);
            v2 = v1;
            v3 = v4 + (int32_t)&sizebases;
            goto lab_0x8001c540;
        }
        v1++;
    }
    // 0x8001c3c4
    int32_t v5; // 0x8001c34c
    panic("Subpage allocator cannot handle allocation of size %zu\n", (int64_t)v5);
    int32_t v6 = (int32_t)&sizes; // 0x8001c3cc
    goto lab_0x8001c3d0;
  lab_0x8001c3d0:;
    int32_t v7 = v6;
    int32_t v8; // 0x8001c34c
    if (*(int32_t *)(v7 + 8) % 0x1000 != v8) {
        // 0x8001c3e4
        badassert("PR_BLOCKTYPE(pr) == blktype", "../../vm/kmalloc.c", 917, "subpage_kmalloc");
    }
    // 0x8001c400
    v2 = v8;
    v3 = v7;
    int16_t * v9; // 0x8001c34c
    int32_t v10; // 0x8001c34c
    if (*(int16_t *)(v7 + 14) == 0) {
        goto lab_0x8001c540;
    } else {
        // 0x8001c400
        v9 = (int16_t *)(v7 + 12);
        v10 = v7;
        goto lab_0x8001c410;
    }
  lab_0x8001c540:;
    int32_t v11 = v2;
    v6 = *(int32_t *)v3;
    v8 = v11;
    char * result; // 0x8001c34c
    uint32_t v12; // 0x8001c554
    if (v6 == 0) {
        // 0x8001c548
        spinlock_release(&kmalloc_spinlock);
        v12 = alloc_kpages(1);
        if (v12 == 0) {
            // 0x8001c560
            kprintf("kmalloc: Subpage allocator couldn't get a page\n");
            result = NULL;
            goto lab_0x8001c754;
        } else {
            if (v12 % 0x1000 == 0) {
                goto lab_0x8001c59c;
            } else {
                // 0x8001c580
                badassert("prpage % PAGE_SIZE == 0", "../../vm/kmalloc.c", 973, "subpage_kmalloc");
                goto lab_0x8001c59c;
            }
        }
    } else {
        goto lab_0x8001c3d0;
    }
  lab_0x8001c410:;
    uint16_t v13 = *v9; // 0x8001c410
    int32_t v14 = v13; // 0x8001c420
    int32_t v15 = -0x1000; // 0x8001c420
    if (v13 >= 0x1000) {
        // 0x8001c424
        badassert("pr->freelist_offset < PAGE_SIZE", "../../vm/kmalloc.c", 924, "subpage_kmalloc");
        v14 = &g41;
        v15 = (int32_t)"pr->freelist_offset < PAGE_SIZE";
    }
    int32_t v16 = *(int32_t *)(v10 + 8) & v15; // 0x8001c44c
    int32_t v17 = v16 + v14; // 0x8001c450
    int32_t v18 = *(int32_t *)v17; // 0x8001c464
    int16_t * v19 = (int16_t *)(v10 + 14); // 0x8001c470
    int16_t v20 = *v19 - 1;
    *v19 = v20;
    int16_t v21; // 0x8001c34c
    if (v18 == 0) {
        // 0x8001c4f8
        v21 = -1;
        if (v20 != 0) {
            // 0x8001c500
            badassert("pr->nfree == 0", "../../vm/kmalloc.c", 940, "subpage_kmalloc");
            v21 = -1;
        }
    } else {
        int32_t v22 = v16; // 0x8001c498
        if (v20 == 0) {
            // 0x8001c49c
            badassert("pr->nfree > 0", "../../vm/kmalloc.c", 934, "subpage_kmalloc");
            v22 = (int32_t)"pr->nfree > 0";
        }
        int32_t v23 = v22; // 0x8001c4cc
        int32_t v24 = v18; // 0x8001c4cc
        if (v18 - v22 >= 0x1000) {
            // 0x8001c4d0
            badassert("fla - prpage < PAGE_SIZE", "../../vm/kmalloc.c", 936, "subpage_kmalloc");
            v23 = (int32_t)"fla - prpage < PAGE_SIZE";
            v24 = (int32_t)"../../vm/kmalloc.c";
        }
        // 0x8001c4ec
        v21 = v24 - v23;
    }
    // 0x8001c524
    *v9 = v21;
    spinlock_release(&kmalloc_spinlock);
    result = (char *)v17;
  lab_0x8001c754:
    // 0x8001c754
    return result;
  lab_0x8001c59c:
    // 0x8001c59c
    spinlock_acquire(&kmalloc_spinlock);
    int32_t * v25 = allocpageref(); // 0x8001c5ac
    int32_t v26; // 0x8001c34c
    int32_t * v27; // 0x8001c34c
    int32_t v28; // 0x8001c34c
    int32_t v29; // 0x8001c34c
    int32_t v30; // 0x8001c34c
    int32_t v31; // 0x8001c5ac
    int32_t v32; // 0x8001c5f4
    int16_t * v33; // 0x8001c61c
    if (v25 == NULL) {
        // 0x8001c5b8
        spinlock_release(&kmalloc_spinlock);
        free_kpages(v12);
        kprintf("kmalloc: Subpage allocator couldn't get pageref\n");
        result = NULL;
        goto lab_0x8001c754;
    } else {
        // 0x8001c5e0
        v31 = (int32_t)v25;
        *(int32_t *)(v31 + 8) = v12 & -0x1000 | v11 % 0x1000;
        v32 = 4 * v11;
        int32_t v34 = *(int32_t *)(v32 + (int32_t)&sizes); // 0x8001c604
        v33 = (int16_t *)(v31 + 14);
        *v33 = (int16_t)(0x1000 / v34);
        int32_t * v35 = (int32_t *)v12; // 0x8001c620
        *v35 = 0;
        uint16_t v36 = *v33; // 0x8001c6c0
        v28 = 1;
        v29 = v34;
        v27 = v35;
        v30 = v34;
        v26 = v36;
        if (v36 > 1) {
            goto lab_0x8001c640;
        } else {
            goto lab_0x8001c6d8;
        }
    }
  lab_0x8001c640:;
    int32_t v37 = v29 * v28 + v12; // 0x8001c650
    int32_t * v38 = (int32_t *)v37; // 0x8001c654
    int32_t v39 = (v28 - 1) * v29 + v12; // 0x8001c670
    *v38 = v39;
    int32_t v40 = v29; // 0x8001c690
    if (v39 == v37) {
        // 0x8001c694
        badassert("fl != fl->next", "../../vm/kmalloc.c", 1004, "subpage_kmalloc");
        v40 = &g41;
        goto lab_0x8001c6b0;
    } else {
        goto lab_0x8001c6b0;
    }
  lab_0x8001c6d8:;
    uint32_t v41 = (int32_t)v27 - v12; // 0x8001c6e0
    int16_t * v42 = (int16_t *)(v31 + 12);
    *v42 = (int16_t)v41;
    if (v41 % 0x10000 == (v26 - 1) * v30) {
        goto lab_0x8001c71c;
    } else {
        // 0x8001c6fc
        badassert("pr->freelist_offset == (pr->nfree-1)*sizes[blktype]", "../../vm/kmalloc.c", 1008, "subpage_kmalloc");
        goto lab_0x8001c71c;
    }
  lab_0x8001c6b0:;
    int32_t v43 = v28 + 1; // 0x8001c6b8
    int32_t v44 = (int32_t)*v33; // 0x8001c6c0
    v28 = v43;
    v29 = v40;
    v27 = v38;
    v30 = v40;
    v26 = v44;
    if (v43 < v44) {
        goto lab_0x8001c640;
    } else {
        goto lab_0x8001c6d8;
    }
  lab_0x8001c71c:;
    int32_t * v45 = (int32_t *)(v32 + (int32_t)&sizebases); // 0x8001c72c
    *v25 = *v45;
    *v45 = v31;
    *(int32_t *)(v31 + 4) = (int32_t)allbase;
    *(int32_t *)&allbase = v31;
    v9 = v42;
    v10 = v31;
    goto lab_0x8001c410;
}