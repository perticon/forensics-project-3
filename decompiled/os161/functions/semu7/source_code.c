semu7(int nargs, char **args)
{
	struct semaphore *sem;
	struct spinlock lk;

	(void)nargs; (void)args;

	sem = makesem(0);

	/*
	 * Check for blocking by taking a spinlock; if we block while
	 * holding a spinlock, wchan_sleep will assert.
	 */
	spinlock_init(&lk);
	spinlock_acquire(&lk);

	/* try with count 0, count 1, and count 2, just for completeness */
	V(sem);
	V(sem);
	V(sem);

	/* Clean up. */
	ok();
	spinlock_release(&lk);
	spinlock_cleanup(&lk);
	sem_destroy(sem);
	return 0;
}