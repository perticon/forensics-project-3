int semu7(int nargs,char **args)

{
  semaphore *sem;
  spinlock lk;
  
  sem = makesem(0);
  spinlock_init(&lk);
  spinlock_acquire(&lk);
  V(sem);
  V(sem);
  V(sem);
  ok();
  spinlock_release(&lk);
  spinlock_cleanup(&lk);
  sem_destroy(sem);
  return 0;
}