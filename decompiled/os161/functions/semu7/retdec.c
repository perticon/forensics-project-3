int32_t semu7(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(0); // 0x800121b8
    int32_t lk; // bp-16, 0x800121a8
    spinlock_init(&lk);
    spinlock_acquire(&lk);
    V(v1);
    V(v1);
    V(v1);
    ok();
    spinlock_release(&lk);
    spinlock_cleanup(&lk);
    sem_destroy(v1);
    return 0;
}