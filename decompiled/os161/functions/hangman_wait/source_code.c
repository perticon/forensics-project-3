hangman_wait(struct hangman_actor *a,
	     struct hangman_lockable *l)
{
	if (l == &hangman_lock.splk_hangman) {
		/* don't recurse */
		return;
	}

	spinlock_acquire(&hangman_lock);

	if (a->a_waiting != NULL) {
		spinlock_release(&hangman_lock);
		panic("hangman_wait: already waiting for something?\n");
	}

	hangman_check(l, a);
	a->a_waiting = l;

	spinlock_release(&hangman_lock);
}