arraytest(int nargs, char **args)
{
	struct array *a;

	(void)nargs;
	(void)args;

	kprintf("Beginning array test...\n");
	a = array_create();
	KASSERT(a != NULL);

	testa(a);

	array_setsize(a, 0);

	testa(a);

	array_setsize(a, 0);
	array_destroy(a);

	kprintf("Array test complete\n");
	return 0;
}