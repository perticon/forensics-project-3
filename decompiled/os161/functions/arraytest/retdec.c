int32_t arraytest(int32_t nargs, char ** args) {
    // 0x8000e8d8
    kprintf("Beginning array test...\n");
    int32_t * v1 = array_create(); // 0x8000e8f4
    int32_t v2 = (int32_t)v1; // 0x8000e8fc
    if (v1 == NULL) {
        // 0x8000e900
        badassert("a != NULL", "../../test/arraytest.c", 133, "arraytest");
        v2 = &g41;
    }
    // 0x8000e920
    testa((int32_t *)v2);
    array_setsize(v1, 0);
    testa(v1);
    array_setsize(v1, 0);
    array_destroy(v1);
    kprintf("Array test complete\n");
    return 0;
}