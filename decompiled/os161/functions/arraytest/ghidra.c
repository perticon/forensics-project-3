int arraytest(int nargs,char **args)

{
  array *a;
  array *a_00;
  
  kprintf("Beginning array test...\n");
  a = array_create();
  a_00 = a;
  if (a == (array *)0x0) {
    badassert("a != NULL","../../test/arraytest.c",0x85,"arraytest");
  }
  testa(a_00);
  array_setsize(a,0);
  testa(a);
  array_setsize(a,0);
  array_destroy(a);
  kprintf("Array test complete\n");
  return 0;
}