dev_ioctl(struct vnode *v, int op, userptr_t data)
{
	struct device *d = v->vn_data;
	return DEVOP_IOCTL(d, op, data);
}