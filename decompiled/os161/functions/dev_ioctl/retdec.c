int32_t dev_ioctl(int32_t * v2, int32_t op, int32_t * data) {
    int32_t v1 = *(int32_t *)*(int32_t *)((int32_t)v2 + 16); // 0x80017b10
    return *(int32_t *)(v1 + 8);
}