void remove_lists(pageref *pr,int blktype)

{
  pageref *ppVar1;
  pageref *ppVar2;
  pageref **pppVar3;
  char *pcVar4;
  char *pcVar5;
  
  pcVar5 = (char *)(blktype << 2);
  if (7 < (uint)blktype) {
    pcVar4 = "blktype>=0 && blktype<NSIZES";
    pcVar5 = "../../vm/kmalloc.c";
    badassert("blktype>=0 && blktype<NSIZES","../../vm/kmalloc.c",0x33c,"remove_lists");
    pr = (pageref *)pcVar4;
  }
  ppVar1 = (pageref *)((int)sizebases + (int)pcVar5);
  do {
    ppVar2 = ppVar1;
    ppVar1 = ppVar2->next_samesize;
    if (ppVar1 == (pageref *)0x0) goto LAB_8001bf84;
  } while (ppVar1 != pr);
  ppVar2->next_samesize = pr->next_samesize;
LAB_8001bf84:
  pppVar3 = &allbase;
  while( true ) {
    ppVar1 = *pppVar3;
    if (ppVar1 == (pageref *)0x0) {
      return;
    }
    if (pr == ppVar1) break;
    pppVar3 = &ppVar1->next_all;
  }
  *pppVar3 = pr->next_all;
  return;
}