void threadlisttest_a(void) {
    // 0x800135c8
    int32_t tl; // bp-40, 0x800135c8
    threadlist_init(&tl);
    if (!threadlist_isempty(&tl)) {
        // 0x800135e8
        badassert("threadlist_isempty(&tl)", "../../test/threadlisttest.c", 127, "threadlisttest_a");
    }
    // 0x80013604
    threadlist_cleanup(&tl);
}