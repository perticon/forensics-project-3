void threadlisttest_a(void)

{
  undefined3 extraout_var;
  bool bVar1;
  threadlist tl;
  
  threadlist_init(&tl);
  bVar1 = threadlist_isempty(&tl);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("threadlist_isempty(&tl)","../../test/threadlisttest.c",0x7f,"threadlisttest_a");
  }
  threadlist_cleanup(&tl);
  return;
}