threadlisttest_a(void)
{
	struct threadlist tl;

	threadlist_init(&tl);
	KASSERT(threadlist_isempty(&tl));
	threadlist_cleanup(&tl);
}