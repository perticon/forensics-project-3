int32_t vfs_swapon(char * devname, int32_t * ret) {
    int32_t v1 = (int32_t)devname;
    int32_t v2 = strlen(devname); // 0x8001957c
    int32_t v3 = v1; // 0x80019584
    int32_t v4 = 0; // 0x80019584
    if (v2 != 0) {
        int32_t v5 = v2 - 1; // 0x80019588
        v3 = v1;
        v4 = 0;
        if (*(char *)(v5 + v1) == 58) {
            char * v6 = kstrdup(devname); // 0x800195a4
            if (v6 == NULL) {
                // 0x800196b4
                return 3;
            }
            int32_t v7 = (int32_t)v6; // 0x800195a4
            *(char *)(v5 + v7) = 0;
            v3 = v7;
            v4 = v7;
        }
    }
    // 0x800195c4
    vfs_biglock_acquire();
    int32_t * kd; // bp-24, 0x80019560
    int32_t v8 = findmount((char *)v3, &kd); // 0x800195d4
    int32_t result = v8; // 0x800195dc
    if (v8 == 0) {
        int32_t v9 = (int32_t)kd; // 0x800195e0
        result = 27;
        if (*(int32_t *)(v9 + 16) == 0) {
            int32_t v10 = v9; // 0x80019604
            if (*(int32_t *)(v9 + 4) == 0) {
                // 0x80019608
                badassert("kd->kd_rawname != NULL", "../../vfs/vfslist.c", 615, "vfs_swapon");
                v10 = &g41;
            }
            int32_t v11 = v10; // 0x80019630
            if (*(int32_t *)(v10 + 8) == 0) {
                // 0x80019634
                badassert("kd->kd_device != NULL", "../../vfs/vfslist.c", 616, "vfs_swapon");
                v11 = &g41;
            }
            // 0x80019650
            kprintf("vfs: Swap attached to %s\n", (char *)*(int32_t *)v11);
            int32_t v12 = (int32_t)kd; // 0x80019660
            *(int32_t *)(v12 + 16) = -1;
            vnode_incref((int32_t *)*(int32_t *)(v12 + 12));
            *ret = *(int32_t *)((int32_t)kd + 12);
            result = 0;
        }
    }
    // 0x80019690
    vfs_biglock_release();
    if (v4 != 0) {
        // 0x800196a0
        kfree((char *)v4);
    }
    // 0x800196b4
    return result;
}