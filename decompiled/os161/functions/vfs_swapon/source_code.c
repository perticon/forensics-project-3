vfs_swapon(const char *devname, struct vnode **ret)
{
	char *myname = NULL;
	size_t len;
	struct knowndev *kd;
	int result;

	len = strlen(devname);
	if (len > 0 && devname[len - 1] == ':') {
		/* tolerate trailing :, e.g. lhd0: rather than lhd0 */
		myname = kstrdup(devname);
		if (myname == NULL) {
			return ENOMEM;
		}
		myname[len - 1] = 0;
		devname = myname;
	}

	vfs_biglock_acquire();

	result = findmount(devname, &kd);
	if (result) {
		goto out;
	}

	if (kd->kd_fs != NULL) {
		result = EBUSY;
		goto out;
	}
	KASSERT(kd->kd_rawname != NULL);
	KASSERT(kd->kd_device != NULL);

	kprintf("vfs: Swap attached to %s\n", kd->kd_name);

	kd->kd_fs = SWAP_FS;
	VOP_INCREF(kd->kd_vnode);
	*ret = kd->kd_vnode;

 out:
	vfs_biglock_release();
	if (myname != NULL) {
		kfree(myname);
	}

	return result;
}