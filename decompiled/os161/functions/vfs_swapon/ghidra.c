int vfs_swapon(char *devname,vnode **ret)

{
  size_t sVar1;
  int iVar2;
  knowndev *pkVar3;
  char *ptr;
  knowndev *kd;
  
  sVar1 = strlen(devname);
  ptr = (char *)0x0;
  if (sVar1 != 0) {
    if (devname[sVar1 - 1] == ':') {
      devname = kstrdup(devname);
      if (devname == (char *)0x0) {
        return 3;
      }
      devname[sVar1 - 1] = '\0';
      ptr = devname;
    }
    else {
      ptr = (char *)0x0;
    }
  }
  vfs_biglock_acquire();
  iVar2 = findmount(devname,&kd);
  if (iVar2 == 0) {
    if (kd->kd_fs == (fs *)0x0) {
      pkVar3 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x267,"vfs_swapon");
      }
      if (pkVar3->kd_device == (device *)0x0) {
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0x268,"vfs_swapon");
      }
      kprintf("vfs: Swap attached to %s\n",pkVar3->kd_name);
      kd->kd_fs = (fs *)0xffffffff;
      vnode_incref(kd->kd_vnode);
      *ret = kd->kd_vnode;
    }
    else {
      iVar2 = 0x1b;
    }
  }
  vfs_biglock_release();
  if (ptr != (char *)0x0) {
    kfree(ptr);
  }
  return iVar2;
}