int badnames(char *n1,char *n2,char *n3)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar5;
  int iVar2;
  knowndevarray *pkVar3;
  char *pcVar4;
  uint uVar6;
  uint uVar7;
  char **ppcVar8;
  
  bVar5 = vfs_biglock_do_i_hold();
  iVar2 = -0x7ffd0000;
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("vfs_biglock_do_i_hold()","../../vfs/vfslist.c",0x169,"badnames");
  }
  uVar7 = *(uint *)(*(int *)(iVar2 + -0x7374) + 4);
  uVar6 = 0;
  while( true ) {
    if (uVar7 <= uVar6) {
      return 0;
    }
    pkVar3 = knowndevs;
    if ((knowndevs->arr).num <= uVar6) {
      badassert("index < a->num","../../include/array.h",100,"array_get");
    }
    ppcVar8 = (char **)(pkVar3->arr).v[uVar6];
    if (ppcVar8[4] + -1 < (char *)0xfffffffe) {
      pcVar4 = (char *)(**(code **)(*(int *)(ppcVar8[4] + 4) + 4))();
      if (pcVar4 == (char *)0x0) {
        bVar1 = false;
      }
      else if (n1 == (char *)0x0) {
        bVar1 = false;
      }
      else {
        iVar2 = strcmp(pcVar4,n1);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n2);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
        else {
          if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
            iVar2 = strcmp(pcVar4,n3);
            bVar1 = iVar2 == 0;
          }
          if (bVar1) {
            bVar1 = true;
          }
        }
      }
      if (bVar1) {
        return 1;
      }
    }
    pcVar4 = ppcVar8[1];
    bVar1 = false;
    if ((pcVar4 != (char *)0x0) && (n1 != (char *)0x0)) {
      iVar2 = strcmp(pcVar4,n1);
      bVar1 = iVar2 == 0;
    }
    if (bVar1) {
      bVar1 = true;
    }
    else {
      if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
        iVar2 = strcmp(pcVar4,n2);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n3);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
      }
    }
    if (bVar1) break;
    pcVar4 = *ppcVar8;
    if (pcVar4 == (char *)0x0) {
      bVar1 = false;
    }
    else if (n1 == (char *)0x0) {
      bVar1 = false;
    }
    else {
      iVar2 = strcmp(pcVar4,n1);
      bVar1 = iVar2 == 0;
    }
    if (bVar1) {
      bVar1 = true;
    }
    else {
      if ((pcVar4 != (char *)0x0) && (n2 != (char *)0x0)) {
        iVar2 = strcmp(pcVar4,n2);
        bVar1 = iVar2 == 0;
      }
      if (bVar1) {
        bVar1 = true;
      }
      else {
        if ((pcVar4 != (char *)0x0) && (n3 != (char *)0x0)) {
          iVar2 = strcmp(pcVar4,n3);
          bVar1 = iVar2 == 0;
        }
        if (bVar1) {
          bVar1 = true;
        }
      }
    }
    uVar6 = uVar6 + 1;
    if (bVar1) {
      return 1;
    }
  }
  return 1;
}