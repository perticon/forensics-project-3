int32_t badnames(char * n1, char * n2, char * n3) {
    bool v1 = vfs_biglock_do_i_hold(); // 0x8001888c
    int32_t v2 = (int32_t)&g35; // 0x80018894
    if (!v1) {
        // 0x80018898
        badassert("vfs_biglock_do_i_hold()", "../../vfs/vfslist.c", 361, "badnames");
        v2 = &g41;
    }
    uint32_t v3 = *(int32_t *)(*(int32_t *)(v2 - 0x7374) + 4); // 0x800188c0
    if (v3 == 0) {
        // 0x80018b50
        return 0;
    }
    int32_t v4 = 0; // 0x80018b2c
    int32_t v5 = (int32_t)knowndevs; // 0x800188e8
    if (v4 >= (int32_t)bootfs_vnode) {
        // 0x800188ec
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v5 = &g41;
    }
    int32_t v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v4); // 0x80018914
    int32_t v7 = *(int32_t *)(v6 + 16); // 0x8001891c
    int32_t v8; // 0x8001893c
    if (v7 < 0xffffffff) {
        // 0x80018934
        v8 = *(int32_t *)(*(int32_t *)(v7 + 4) + 4);
        if (n1 != NULL && v8 != 0) {
            // 0x80018978
            if (strcmp((char *)v8, n1) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n2 != NULL && v8 != 0) {
            // 0x8001899c
            if (strcmp((char *)v8, n2) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n3 != NULL && v8 != 0) {
            // 0x800189c0
            if (strcmp((char *)v8, n3) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
    }
    int32_t v9 = *(int32_t *)(v6 + 4); // 0x800189ec
    if (n1 != NULL && v9 != 0) {
        // 0x80018a14
        if (strcmp((char *)v9, n1) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    if (n2 != NULL && v9 != 0) {
        // 0x80018a38
        if (strcmp((char *)v9, n2) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    if (n3 != NULL && v9 != 0) {
        // 0x80018a5c
        if (strcmp((char *)v9, n3) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    int32_t v10 = *(int32_t *)v6; // 0x80018a88
    if (n1 != NULL && v10 != 0) {
        // 0x80018abc
        if (strcmp((char *)v10, n1) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    if (n2 != NULL && v10 != 0) {
        // 0x80018ae0
        if (strcmp((char *)v10, n2) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    if (n3 != NULL && v10 != 0) {
        // 0x80018b04
        if (strcmp((char *)v10, n3) == 0) {
            // break -> 0x80018b50
            break;
        }
    }
    // 0x80018b28
    v4++;
    int32_t result = 0; // 0x80018b38
    while (v4 < v3) {
        // 0x800188d0
        v5 = (int32_t)knowndevs;
        if (v4 >= (int32_t)bootfs_vnode) {
            // 0x800188ec
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v5 = &g41;
        }
        // 0x80018908
        v6 = *(int32_t *)(*(int32_t *)v5 + 4 * v4);
        v7 = *(int32_t *)(v6 + 16);
        if (v7 < 0xffffffff) {
            // 0x80018934
            v8 = *(int32_t *)(*(int32_t *)(v7 + 4) + 4);
            if (n1 != NULL && v8 != 0) {
                // 0x80018978
                result = 1;
                if (strcmp((char *)v8, n1) == 0) {
                    // break -> 0x80018b50
                    break;
                }
            }
            if (n2 != NULL && v8 != 0) {
                // 0x8001899c
                result = 1;
                if (strcmp((char *)v8, n2) == 0) {
                    // break -> 0x80018b50
                    break;
                }
            }
            if (n3 != NULL && v8 != 0) {
                // 0x800189c0
                result = 1;
                if (strcmp((char *)v8, n3) == 0) {
                    // break -> 0x80018b50
                    break;
                }
            }
        }
        // 0x800189ec
        v9 = *(int32_t *)(v6 + 4);
        if (n1 != NULL && v9 != 0) {
            // 0x80018a14
            result = 1;
            if (strcmp((char *)v9, n1) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n2 != NULL && v9 != 0) {
            // 0x80018a38
            result = 1;
            if (strcmp((char *)v9, n2) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n3 != NULL && v9 != 0) {
            // 0x80018a5c
            result = 1;
            if (strcmp((char *)v9, n3) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        // 0x80018a88
        v10 = *(int32_t *)v6;
        if (n1 != NULL && v10 != 0) {
            // 0x80018abc
            result = 1;
            if (strcmp((char *)v10, n1) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n2 != NULL && v10 != 0) {
            // 0x80018ae0
            result = 1;
            if (strcmp((char *)v10, n2) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        if (n3 != NULL && v10 != 0) {
            // 0x80018b04
            result = 1;
            if (strcmp((char *)v10, n3) == 0) {
                // break -> 0x80018b50
                break;
            }
        }
        // 0x80018b28
        v4++;
        result = 0;
    }
    // 0x80018b50
    return result;
}