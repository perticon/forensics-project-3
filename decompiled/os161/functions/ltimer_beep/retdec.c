void ltimer_beep(char * vlt) {
    int32_t v1 = (int32_t)vlt;
    int32_t v2 = *(int32_t *)(v1 + 12); // 0x80005070
    lamebus_write_register((int32_t *)*(int32_t *)(v1 + 8), v2, 20, 440);
}