cpu_irqoff(void)
{
        uint32_t x;

        GET_STATUS(x);
        x &= ~(uint32_t)CST_IEc;
        SET_STATUS(x);
}