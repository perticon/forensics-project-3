void cpu_irqoff(void) {
    // 0x8001f314
    int32_t v1; // 0x8001f314
    __asm_mfc0(v1, v1, 0);
    __asm_mtc0(v1 & -2, v1, 0);
}