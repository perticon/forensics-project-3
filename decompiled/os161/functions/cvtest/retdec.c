int32_t cvtest(int32_t nargs, char ** args) {
    // 0x80013298
    inititems();
    kprintf("Starting CV test...\n");
    kprintf("Threads should print out in reverse order.\n");
    testval1 = 31;
    int32_t v1 = 0;
    int32_t v2 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1); // 0x800132fc
    int32_t v3 = v2; // 0x80013304
    int32_t v4; // 0x800132fc
    if (v2 != 0) {
        panic("cvtest: thread_fork failed: %s\n", strerror(v3));
        v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x80013308
            panic("cvtest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x80013304
    while (v5 < 32) {
        // 0x800132ec
        v1 = v5;
        v2 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("cvtest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80013308
                panic("cvtest: thread_fork failed: %s\n", strerror(v3));
                v4 = thread_fork("synchtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2e1c), NULL, v1);
                v3 = v4;
            }
        }
        // 0x80013324
        v5 = v1 + 1;
    }
    int32_t v6 = 1; // 0x80013344
    P((int32_t *)g16);
    int32_t v7 = v6; // 0x80013350
    while (v6 < 32) {
        // 0x8001333c
        v6 = v7 + 1;
        P((int32_t *)g16);
        v7 = v6;
    }
    // 0x80013354
    kprintf("CV test done\n");
    return 0;
}