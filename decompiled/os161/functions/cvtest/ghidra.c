int cvtest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting CV test...\n");
  kprintf("Threads should print out in reverse order.\n");
  testval1 = 0x1f;
  data2 = 0;
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        P(donesem);
      }
      kprintf("CV test done\n");
      return 0;
    }
    iVar2 = thread_fork("synchtest",(proc *)0x0,cvtestthread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("cvtest: thread_fork failed: %s\n",pcVar1);
}