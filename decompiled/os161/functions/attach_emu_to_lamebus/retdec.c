int32_t * attach_emu_to_lamebus(int32_t emuno, int32_t * sc) {
    int32_t v1 = lamebus_probe(sc, 1, 7, 1, NULL); // 0x80002524
    if (v1 < 0) {
        // 0x80002584
        return NULL;
    }
    char * v2 = kmalloc(28); // 0x80002534
    int32_t * result = NULL; // 0x8000253c
    if (v2 != NULL) {
        int32_t v3 = (int32_t)v2; // 0x80002534
        *(int32_t *)v2 = (int32_t)sc;
        *(int32_t *)(v3 + 4) = v1;
        *(int32_t *)(v3 + 8) = emuno;
        lamebus_mark(sc, v1);
        lamebus_attach_interrupt(sc, v1, v2, (void (*)(char *))-0x7fffc6f8);
        result = (int32_t *)v2;
    }
    // 0x80002584
    return result;
}