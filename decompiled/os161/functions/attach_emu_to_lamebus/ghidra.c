emu_softc * attach_emu_to_lamebus(int emuno,lamebus_softc *sc)

{
  uint32_t slot;
  emu_softc *devdata;
  
  slot = lamebus_probe(sc,1,7,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (emu_softc *)0x0;
  }
  else {
    devdata = (emu_softc *)kmalloc(0x1c);
    if (devdata == (emu_softc *)0x0) {
      devdata = (emu_softc *)0x0;
    }
    else {
      devdata->e_busdata = sc;
      devdata->e_buspos = slot;
      devdata->e_unit = emuno;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,emu_irq);
    }
  }
  return devdata;
}