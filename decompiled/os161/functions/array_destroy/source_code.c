array_destroy(struct array *a)
{
	array_cleanup(a);
	kfree(a);
}