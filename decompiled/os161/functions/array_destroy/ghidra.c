void array_destroy(array *a)

{
  array_cleanup(a);
  kfree(a);
  return;
}