void vfs_close(vnode *vn)

{
  vnode_decref(vn);
  return;
}