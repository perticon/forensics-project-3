int vnode_init(vnode *vn,vnode_ops *ops,fs *fs,void *fsdata)

{
  char *pcVar1;
  char *pcVar2;
  char *pcVar3;
  
  pcVar1 = (char *)vn;
  pcVar2 = (char *)ops;
  if (vn == (vnode *)0x0) {
    pcVar1 = s_vn____NULL_80027408;
    pcVar2 = "../../vfs/vnode.c";
    fs = (fs *)0x2f;
    pcVar3 = "vnode_init";
    badassert(s_vn____NULL_80027408,"../../vfs/vnode.c",0x2f,"vnode_init");
    fsdata = pcVar3;
  }
  if ((vnode_ops *)pcVar2 == (vnode_ops *)0x0) {
    pcVar1 = s_ops____NULL_80027488;
    pcVar2 = "../../vfs/vnode.c";
    badassert(s_ops____NULL_80027488,"../../vfs/vnode.c",0x30,"vnode_init");
  }
  *(char **)((int)pcVar1 + 0x14) = pcVar2;
  *(int *)pcVar1 = 1;
  spinlock_init((spinlock *)((int)pcVar1 + 4));
  vn->vn_fs = fs;
  vn->vn_data = fsdata;
  return 0;
}