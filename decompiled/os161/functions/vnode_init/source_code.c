vnode_init(struct vnode *vn, const struct vnode_ops *ops,
	   struct fs *fs, void *fsdata)
{
	KASSERT(vn != NULL);
	KASSERT(ops != NULL);

	vn->vn_ops = ops;
	vn->vn_refcount = 1;
	spinlock_init(&vn->vn_countlock);
	vn->vn_fs = fs;
	vn->vn_data = fsdata;
	return 0;
}