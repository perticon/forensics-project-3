int32_t vnode_init(int32_t * vn, int32_t * ops, int32_t * fs, char * fsdata) {
    int32_t v1 = (int32_t)vn;
    int32_t v2; // 0x8001a660
    int32_t v3; // 0x8001a660
    int32_t v4; // 0x8001a660
    int32_t v5; // 0x8001a660
    if (vn == NULL) {
        // 0x8001a69c
        badassert("vn != NULL", "../../vfs/vnode.c", 47, "vnode_init");
        v5 = (int32_t)"vnode_init";
        v4 = 47;
        v2 = (int32_t)"vn != NULL";
        v3 = (int32_t)"../../vfs/vnode.c";
    } else {
        int32_t v6 = (int32_t)fsdata;
        int32_t v7 = (int32_t)fs;
        v5 = v6;
        v4 = v7;
        v2 = v1;
        v3 = (int32_t)ops;
        if (ops == NULL) {
            // 0x8001a6a8
            badassert("ops != NULL", "../../vfs/vnode.c", 48, "vnode_init");
            v5 = v6;
            v4 = v7;
            v2 = (int32_t)"ops != NULL";
            v3 = (int32_t)"../../vfs/vnode.c";
        }
    }
    // 0x8001a6c8
    *(int32_t *)(v2 + 20) = v3;
    *(int32_t *)v2 = 1;
    spinlock_init((int32_t *)(v2 + 4));
    *(int32_t *)(v1 + 12) = v4;
    *(int32_t *)(v1 + 16) = v5;
    return 0;
}