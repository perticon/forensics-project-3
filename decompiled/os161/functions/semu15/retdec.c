int32_t semu15(int32_t nargs, char ** args) {
    // 0x80012368
    kprintf("This should assert that the semaphore isn't null.\n");
    V(NULL);
    panic("semu15: V tolerated null semaphore\n");
    return &g41;
}