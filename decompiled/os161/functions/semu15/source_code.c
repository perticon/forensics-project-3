semu15(int nargs, char **args)
{
	(void)nargs; (void)args;

	kprintf("This should assert that the semaphore isn't null.\n");
	V(NULL);
	panic("semu15: V tolerated null semaphore\n");
	return 0;
}