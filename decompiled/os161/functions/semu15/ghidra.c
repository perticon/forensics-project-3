int semu15(int nargs,char **args)

{
  kprintf("This should assert that the semaphore isn\'t null.\n");
  V((semaphore *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("semu15: V tolerated null semaphore\n");
}