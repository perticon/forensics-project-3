showmenu(const char *name, const char *x[])
{
	int ct, half, i;

	kprintf("\n");
	kprintf("%s\n", name);

	for (i = ct = 0; x[i]; i++)
	{
		ct++;
	}
	half = (ct + 1) / 2;

	for (i = 0; i < half; i++)
	{
		kprintf("    %-36s", x[i]);
		if (i + half < ct)
		{
			kprintf("%s", x[i + half]);
		}
		kprintf("\n");
	}

	kprintf("\n");
}