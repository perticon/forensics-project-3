vfs_symlink(const char *contents, char *path)
{
	struct vnode *newdir;
	char newname[NAME_MAX+1];
	int result;

	result = vfs_lookparent(path, &newdir, newname, sizeof(newname));
	if (result) {
		return result;
	}

	result = VOP_SYMLINK(newdir, newname, contents);
	VOP_DECREF(newdir);

	return result;
}