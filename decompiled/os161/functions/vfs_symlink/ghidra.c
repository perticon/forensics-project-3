int vfs_symlink(char *contents,char *path)

{
  int iVar1;
  vnode *newdir;
  char newname [256];
  
  iVar1 = vfs_lookparent(path,&newdir,newname,0x100);
  if (iVar1 == 0) {
    vnode_check(newdir,"symlink");
    iVar1 = (*newdir->vn_ops->vop_symlink)(newdir,newname,contents);
    vnode_decref(newdir);
  }
  return iVar1;
}