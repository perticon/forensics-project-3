int32_t vfs_symlink(char * contents, char * path) {
    char newname[256]; // bp-268, 0x8001a498
    // 0x8001a498
    int32_t * newdir; // bp-272, 0x8001a498
    int32_t v1 = vfs_lookparent(path, (int32_t *)&newdir, newname, 256); // 0x8001a4b8
    int32_t result = v1; // 0x8001a4c0
    if (v1 == 0) {
        // 0x8001a4c4
        vnode_check(newdir, "symlink");
        result = *(int32_t *)(*(int32_t *)((int32_t)newdir + 20) + 64);
        vnode_decref(newdir);
    }
    // 0x8001a500
    return result;
}