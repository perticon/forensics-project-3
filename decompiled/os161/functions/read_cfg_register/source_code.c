read_cfg_register(struct lamebus_softc *lb, int slot, uint32_t offset)
{
	/* Note that lb might be NULL on some platforms in some contexts. */
	offset += LB_CONFIG_SIZE*slot;
	return lamebus_read_register(lb, LB_CONTROLLER_SLOT, offset);
}