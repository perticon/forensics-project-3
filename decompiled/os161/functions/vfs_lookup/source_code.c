vfs_lookup(char *path, struct vnode **retval)
{
	struct vnode *startvn;
	int result;

	vfs_biglock_acquire();

	result = getdevice(path, &path, &startvn);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	if (strlen(path)==0) {
		*retval = startvn;
		vfs_biglock_release();
		return 0;
	}

	result = VOP_LOOKUP(startvn, path, retval);

	VOP_DECREF(startvn);
	vfs_biglock_release();
	return result;
}