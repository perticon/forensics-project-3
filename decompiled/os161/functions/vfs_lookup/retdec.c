int32_t vfs_lookup(char * path, int32_t * retval) {
    char * v1 = path; // 0x80019f60
    vfs_biglock_acquire();
    int32_t * startvn; // bp-24, 0x80019f50
    int32_t result = getdevice(path, &v1, (int32_t *)&startvn); // 0x80019f78
    if (result != 0) {
        // 0x80019f84
        vfs_biglock_release();
        // 0x8001a000
        return result;
    }
    // 0x80019f94
    int32_t result2; // 0x80019f50
    if (strlen(v1) == 0) {
        // 0x80019fa8
        *retval = (int32_t)startvn;
        vfs_biglock_release();
        result2 = 0;
    } else {
        // 0x80019fbc
        vnode_check(startvn, "lookup");
        vnode_decref(startvn);
        vfs_biglock_release();
        result2 = *(int32_t *)(*(int32_t *)((int32_t)startvn + 20) + 88);
    }
    // 0x8001a000
    return result2;
}