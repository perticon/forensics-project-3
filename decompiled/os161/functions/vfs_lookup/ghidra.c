int vfs_lookup(char *path,vnode **retval)

{
  int iVar1;
  size_t sVar2;
  char *local_res0;
  vnode *startvn;
  
  local_res0 = path;
  vfs_biglock_acquire();
  iVar1 = getdevice(local_res0,(char **)register0x00000074,&startvn);
  if (iVar1 == 0) {
    sVar2 = strlen(local_res0);
    if (sVar2 == 0) {
      *retval = startvn;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vnode_check(startvn,"lookup");
      iVar1 = (*startvn->vn_ops->vop_lookup)(startvn,local_res0,retval);
      vnode_decref(startvn);
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}