ltrace_softc * attach_ltrace_to_lamebus(int ltraceno,lamebus_softc *sc)

{
  uint32_t slot;
  ltrace_softc *plVar1;
  uint32_t drl;
  
  slot = lamebus_probe(sc,1,8,1,&drl);
  if ((int)slot < 0) {
    plVar1 = (ltrace_softc *)0x0;
  }
  else {
    plVar1 = (ltrace_softc *)kmalloc(0xc);
    if (plVar1 == (ltrace_softc *)0x0) {
      plVar1 = (ltrace_softc *)0x0;
    }
    else {
      plVar1->lt_busdata = sc;
      plVar1->lt_buspos = slot;
      plVar1->lt_canstop = 1 < drl;
      plVar1->lt_canprof = 2 < drl;
      lamebus_mark(sc,slot);
    }
  }
  return plVar1;
}