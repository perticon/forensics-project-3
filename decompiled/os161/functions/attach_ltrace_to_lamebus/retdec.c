int32_t * attach_ltrace_to_lamebus(int32_t ltraceno, int32_t * sc) {
    // 0x80005150
    int32_t drl; // bp-24, 0x80005150
    int32_t v1 = lamebus_probe(sc, 1, 8, 1, &drl); // 0x80005180
    if (v1 < 0) {
        // 0x800051e4
        return NULL;
    }
    char * v2 = kmalloc(12); // 0x80005190
    int32_t * result = NULL; // 0x80005198
    if (v2 != NULL) {
        int32_t v3 = (int32_t)v2; // 0x80005190
        *(int32_t *)v2 = (int32_t)sc;
        *(int32_t *)(v3 + 4) = v1;
        *(char *)(v3 + 8) = (char)(drl > 1);
        *(char *)(v3 + 9) = (char)(drl > 2);
        lamebus_mark(sc, v1);
        result = (int32_t *)v2;
    }
    // 0x800051e4
    return result;
}