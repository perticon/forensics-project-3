int32_t file_write(uint32_t fd, int32_t * buf_ptr, int32_t size) {
    // 0x8000d240
    if (fd >= 129) {
        // 0x8000d314
        return -1;
    }
    // 0x8000d24c
    int32_t v1; // 0x8000d240
    int32_t v2 = *(int32_t *)(4 * fd + 96 + *(int32_t *)(v1 + 84)); // 0x8000d26c
    if (v2 == 0) {
        // 0x8000d314
        return -1;
    }
    int32_t v3 = *(int32_t *)v2; // 0x8000d27c
    if (v3 == 0) {
        // 0x8000d314
        return -1;
    }
    int32_t * v4 = (int32_t *)(v2 + 12); // 0x8000d2a4
    int32_t * v5 = (int32_t *)(v2 + 8); // 0x8000d2a8
    vnode_check((int32_t *)v3, "write");
    int32_t v6 = *(int32_t *)(*(int32_t *)(v3 + 20) + 24); // 0x8000d2e0
    int32_t result = v6; // 0x8000d2f4
    if (v6 == 0) {
        // 0x8000d2f8
        result = 0;
    }
    // 0x8000d314
    return result;
}