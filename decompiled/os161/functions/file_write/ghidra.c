int file_write(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  vnode *v;
  vnode **ppvVar2;
  int unaff_s7;
  iovec iov;
  uio u;
  
  if ((uint)fd < 0x81) {
    ppvVar2 = *(vnode ***)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
    if (ppvVar2 == (vnode **)0x0) {
      iVar1 = -1;
    }
    else {
      v = *ppvVar2;
      u.uio_iov = &iov;
      if (v == (vnode *)0x0) {
        iVar1 = -1;
      }
      else {
        u.uio_iovcnt = 1;
        u.uio_offset._4_4_ = ppvVar2[3];
        u.uio_offset._0_4_ = ppvVar2[2];
        u.uio_segflg = UIO_USERISPACE;
        u.uio_rw = UIO_WRITE;
        u.uio_space = *(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10);
        iov.field_0 = buf_ptr;
        iov.iov_len = size;
        u.uio_resid = size;
        vnode_check(v,s_console_write_80022690 + 8);
        iVar1 = (*v->vn_ops->vop_write)(v,&u);
        if (iVar1 == 0) {
          ppvVar2[3] = u.uio_offset._4_4_;
          ppvVar2[2] = u.uio_offset._0_4_;
          iVar1 = size - u.uio_resid;
        }
      }
    }
    return iVar1;
  }
  return -1;
}