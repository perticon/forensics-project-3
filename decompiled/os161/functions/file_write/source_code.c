file_write(int fd, userptr_t buf_ptr, size_t size)
{
  struct iovec iov;
  struct uio u;
  int result, nwrite;
  struct vnode *vn;
  struct openfile *of;

  if (fd < 0 || fd > OPEN_MAX)
    return -1;
  of = curproc->fileTable[fd];
  if (of == NULL)
    return -1;
  vn = of->vn;
  if (vn == NULL)
    return -1;

  iov.iov_ubase = buf_ptr;
  iov.iov_len = size;

  u.uio_iov = &iov;
  u.uio_iovcnt = 1;
  u.uio_resid = size; // amount to read from the file
  u.uio_offset = of->offset;
  u.uio_segflg = UIO_USERISPACE;
  u.uio_rw = UIO_WRITE;
  u.uio_space = curproc->p_addrspace;

  result = VOP_WRITE(vn, &u);
  if (result)
  {
    return result;
  }
  of->offset = u.uio_offset;
  nwrite = size - u.uio_resid;
  return (nwrite);
}