int isTableActive(void)

{
  int iVar1;
  
  spinlock_acquire(&freemem_lock);
  iVar1 = allocTableActive;
  spinlock_release(&freemem_lock);
  return iVar1;
}