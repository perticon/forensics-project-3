split64to32(uint64_t x, uint32_t *y1, uint32_t *y2)
{
#if _BYTE_ORDER == _BIG_ENDIAN
	*y1 = x >> 32;
	*y2 = x & 0xffffffff;
#elif _BYTE_ORDER == _LITTLE_ENDIAN
	*y1 = x & 0xffffffff;
	*y2 = x >> 32;
#else
#error "Eh?"
#endif
}