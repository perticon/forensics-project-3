int file_read_paddr(vnode *vn,paddr_t buf_ptr,size_t size,off_t offset)

{
  size_t sVar1;
  iovec iov;
  uio u;
  
  iov.field_0 = buf_ptr + 0x80000000;
  u.uio_iov = &iov;
  u.uio_iovcnt = 1;
  u.uio_segflg = UIO_SYSSPACE;
  u.uio_rw = UIO_READ;
  u.uio_space = (addrspace *)0x0;
  iov.iov_len = size;
  u.uio_resid = size;
  u.uio_offset = offset;
  vnode_check(vn,"read");
  sVar1 = (*vn->vn_ops->vop_read)(vn,&u);
  if ((sVar1 == 0) && (sVar1 = size, u.uio_resid != 0)) {
    kprintf("SWAPPING IN: short read on page - problems reading?\n");
    sVar1 = 6;
  }
  return sVar1;
}