int32_t file_read_paddr(int32_t * vn, int32_t buf_ptr, int32_t size, int64_t offset) {
    // 0x8001d8b8
    vnode_check(vn, "read");
    int32_t result = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 12); // 0x8001d928
    if (result != 0) {
        // 0x8001d964
        return result;
    }
    int32_t result2 = 0; // 0x8001d94c
    if (size != 0) {
        // 0x8001d950
        kprintf("SWAPPING IN: short read on page - problems reading?\n");
        result2 = 6;
    }
    // 0x8001d964
    return result2;
}