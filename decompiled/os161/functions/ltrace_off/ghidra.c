void ltrace_off(uint32_t code)

{
  if (the_trace != (ltrace_softc *)0x0) {
    lamebus_write_register((lamebus_softc *)the_trace->lt_busdata,the_trace->lt_buspos,4,code);
  }
  return;
}