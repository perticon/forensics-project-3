lhd_ioctl(struct device *d, int op, userptr_t data)
{
	/*
	 * We don't support any ioctls.
	 */
	(void)d;
	(void)op;
	(void)data;
	return EIOCTL;
}