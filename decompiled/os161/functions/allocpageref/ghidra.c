pageref * allocpageref(void)

{
  uint uVar1;
  pagerefpage *ppVar2;
  char *pcVar3;
  char *pcVar4;
  kheap_root *pkVar5;
  char *pcVar6;
  int iVar7;
  kheap_root *root;
  uint uVar8;
  
  uVar1 = 0;
  pkVar5 = kheaproots;
  pcVar6 = (char *)0xffffffff;
  do {
    if (0xf < uVar1) {
      return (pageref *)0x0;
    }
    root = pkVar5 + uVar1;
    pcVar3 = (char *)root->numinuse;
    if (pcVar3 < (char *)0x100) {
      uVar8 = 0;
      while (uVar8 < 8) {
        pcVar4 = (char *)root->pagerefs_inuse[uVar8];
        iVar7 = 0;
        if (pcVar4 == pcVar6) {
          uVar8 = uVar8 + 1;
        }
        else {
          for (uVar1 = 1; uVar1 != 0; uVar1 = uVar1 << 1) {
            if ((uVar1 & (uint)pcVar4) == 0) {
              root->pagerefs_inuse[uVar8] = uVar1 | (uint)pcVar4;
              root->numinuse = (uint)(pcVar3 + 1);
              ppVar2 = root->page;
              if (ppVar2 == (pagerefpage *)0x0) {
                allocpagerefpage(root);
                ppVar2 = root->page;
              }
              if (ppVar2 != (pagerefpage *)0x0) {
                return ppVar2->refs + uVar8 * 0x20 + iVar7;
              }
              return (pageref *)0x0;
            }
            iVar7 = iVar7 + 1;
          }
          pcVar3 = "0";
          pkVar5 = (kheap_root *)0x124;
          pcVar6 = "allocpageref";
          badassert("0","../../vm/kmalloc.c",0x124,"allocpageref");
        }
      }
    }
    uVar1 = uVar1 + 1;
  } while( true );
}