allocpageref(void)
{
	unsigned i,j;
	uint32_t k;
	unsigned whichroot;
	struct kheap_root *root;

	for (whichroot=0; whichroot < NUM_PAGEREFPAGES; whichroot++) {
		root = &kheaproots[whichroot];
		if (root->numinuse >= NPAGEREFS_PER_PAGE) {
			continue;
		}

		/*
		 * This should probably not be a linear search.
		 */
		for (i=0; i<INUSE_WORDS; i++) {
			if (root->pagerefs_inuse[i]==0xffffffff) {
				/* full */
				continue;
			}
			for (k=1,j=0; k!=0; k<<=1,j++) {
				if ((root->pagerefs_inuse[i] & k)==0) {
					root->pagerefs_inuse[i] |= k;
					root->numinuse++;
					if (root->page == NULL) {
						allocpagerefpage(root);
					}
					if (root->page == NULL) {
						return NULL;
					}
					return &root->page->refs[i*32 + j];
				}
			}
			KASSERT(0);
		}
	}

	/* ran out */
	return NULL;
}