int emufs_getroot(fs *fs,vnode **ret)

{
  char *pcVar1;
  void *pvVar2;
  
  if (fs == (fs *)0x0) {
    fs = (fs *)0x800270fc;
    pcVar1 = s_______dev_lamebus_emu_c_80022750;
    badassert("fs != NULL",s_______dev_lamebus_emu_c_80022750,0x4e1,"emufs_getroot");
    ret = (vnode **)pcVar1;
  }
  pvVar2 = fs->fs_data;
  if (pvVar2 == (void *)0x0) {
    badassert("ef != NULL",s_______dev_lamebus_emu_c_80022750,0x4e5,"emufs_getroot");
  }
  pcVar1 = *(char **)((int)pvVar2 + 0xc);
  if ((vnode *)pcVar1 == (vnode *)0x0) {
    pcVar1 = "ef->ef_root != NULL";
    badassert("ef->ef_root != NULL",s_______dev_lamebus_emu_c_80022750,0x4e6,"emufs_getroot");
  }
  vnode_incref((vnode *)pcVar1);
  *ret = *(vnode **)((int)pvVar2 + 0xc);
  return 0;
}