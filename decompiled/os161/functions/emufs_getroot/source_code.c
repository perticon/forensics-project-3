emufs_getroot(struct fs *fs, struct vnode **ret)
{
	struct emufs_fs *ef;

	KASSERT(fs != NULL);

	ef = fs->fs_data;

	KASSERT(ef != NULL);
	KASSERT(ef->ef_root != NULL);

	VOP_INCREF(&ef->ef_root->ev_v);
	*ret = &ef->ef_root->ev_v;
	return 0;
}