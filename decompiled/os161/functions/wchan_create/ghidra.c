wchan * wchan_create(char *name)

{
  wchan *pwVar1;
  
  pwVar1 = (wchan *)kmalloc(0x20);
  if (pwVar1 == (wchan *)0x0) {
    pwVar1 = (wchan *)0x0;
  }
  else {
    threadlist_init(&pwVar1->wc_threads);
    pwVar1->wc_name = name;
  }
  return pwVar1;
}