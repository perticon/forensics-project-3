setup(void)
{
	char tmp[16];
	int i;

	if (wakersem == NULL) {
		wakersem = sem_create("wakersem", 1);
		donesem = sem_create("donesem", 0);
		for (i=0; i<NWAITCHANS; i++) {
			spinlock_init(&spinlocks[i]);
			snprintf(tmp, sizeof(tmp), "wc%d", i);
			waitchans[i] = wchan_create(kstrdup(tmp));
		}
	}
	wakerdone = 0;
}