void setup(void) {
    char tmp2[16]; // bp-40, 0x80014630
    // 0x80014630
    if (wakersem != NULL) {
        // 0x80014718
        wakerdone = 0;
        return;
    }
    int32_t * v1 = sem_create("wakersem", 1); // 0x80014668
    *(int32_t *)&wakersem = (int32_t)v1;
    int32_t * v2 = sem_create("donesem", 0); // 0x80014680
    *(int32_t *)&donesem = (int32_t)v2;
    for (int32_t i = 0; i < 12; i++) {
        // 0x800146ac
        spinlock_init((int32_t *)(8 * i + (int32_t)&spinlocks));
        snprintf(tmp2, 16, "wc%d", i);
        char * v3 = kstrdup(tmp2); // 0x800146d0
        int32_t * v4 = wchan_create(v3); // 0x800146d8
        *(int32_t *)(4 * i + (int32_t)&waitchans) = (int32_t)v4;
    }
    // 0x800146f8
    wakerdone = 0;
}