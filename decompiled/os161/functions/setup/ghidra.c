void setup(void)

{
  char *name;
  wchan *pwVar1;
  int iVar2;
  char tmp [16];
  
  if (wakersem == (semaphore *)0x0) {
    wakersem = sem_create("wakersem",1);
    donesem = sem_create("donesem",0);
    for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
      spinlock_init(spinlocks + iVar2);
      snprintf(tmp,0x10,"wc%d",iVar2);
      name = kstrdup(tmp);
      pwVar1 = wchan_create(name);
      waitchans[iVar2] = pwVar1;
    }
    wakerdone = 0;
    return;
  }
  wakerdone = 0;
  return;
}