void mips_trap(int32_t * tf) {
    int32_t v1 = (int32_t)tf;
    uint32_t v2 = *(int32_t *)(v1 + 8) / 4; // 0x8001e894
    uint32_t v3 = v2 % 16; // 0x8001e898
    int32_t v4 = v1; // 0x8001e8b0
    if (v3 >= 13) {
        // 0x8001e8b4
        badassert("code < NTRAPCODES", "../../arch/mips/locore/trap.c", 147, "mips_trap");
        v4 = (int32_t)"code < NTRAPCODES";
    }
    int32_t v5; // 0x8001e878
    if (v5 != 0) {
        uint32_t v6 = *(int32_t *)(v5 + 72); // 0x8001e8dc
        if (v6 != 0) {
            int32_t v7 = v4; // 0x8001e8f0
            if (v6 >= v4) {
                // 0x8001e8f4
                badassert("(vaddr_t)tf > (vaddr_t)curthread->t_stack", "../../arch/mips/locore/trap.c", 151, "mips_trap");
                v7 = (int32_t)"(vaddr_t)tf > (vaddr_t)curthread->t_stack";
            }
            // 0x8001e914
            if (v7 >= v6 + 0x1000) {
                // 0x8001e920
                badassert("(vaddr_t)tf < (vaddr_t)(curthread->t_stack + STACK_SIZE)", "../../arch/mips/locore/trap.c", 153, "mips_trap");
            }
        }
    }
    if (v3 == 0) {
        char * v8 = (char *)(v5 + 88); // 0x8001e944
        *v8 = 1;
        int32_t * v9 = (int32_t *)(v5 + 92); // 0x8001e94c
        if (*v9 == 0) {
            int32_t * v10 = (int32_t *)(v5 + 96); // 0x8001e980
            int32_t v11 = 1; // 0x8001e98c
            if (*v10 != 0) {
                // 0x8001e990
                badassert("curthread->t_iplhigh_count == 0", "../../arch/mips/locore/trap.c", 183, "mips_trap");
                v11 = (int32_t)&g41 + 1;
            }
            // 0x8001e9b0
            *v9 = 1;
            *v10 = v11;
            mainbus_interrupt(tf);
            if (*v9 != 1) {
                // 0x8001e9e8
                badassert("curthread->t_curspl == IPL_HIGH", "../../arch/mips/locore/trap.c", 195, "mips_trap");
            }
            int32_t v12 = 0; // 0x8001ea14
            if (*v10 != 1) {
                // 0x8001ea18
                badassert("curthread->t_iplhigh_count == 1", "../../arch/mips/locore/trap.c", 196, "mips_trap");
                v12 = &g41;
            }
            // 0x8001ea38
            *v10 = v12;
            *v9 = 0;
        } else {
            // 0x8001e9c8
            mainbus_interrupt(tf);
        }
        // 0x8001ea40
        goto lab_0x8001ec10;
    } else {
        int32_t v13 = splx(1); // 0x8001ea4c
        splx(v13);
        if (v3 == 8) {
            // 0x8001ea64
            if (*(int32_t *)(v5 + 92) != 0) {
                // 0x8001ea74
                badassert("curthread->t_curspl == 0", "../../arch/mips/locore/trap.c", 222, "mips_trap");
            }
            int32_t v14 = (int32_t)&g35; // 0x8001ea9c
            if (*(int32_t *)(v5 + 96) != 0) {
                // 0x8001eaa0
                badassert("curthread->t_iplhigh_count == 0", "../../arch/mips/locore/trap.c", 223, "mips_trap");
                v14 = &g41;
            }
            // 0x8001eac0
            if ((*(int32_t *)(v14 - 0x64c0) & 2) != 0) {
                int32_t v15 = *(int32_t *)(v1 + 36); // 0x8001ead8
                int32_t v16 = *(int32_t *)(v1 + 40); // 0x8001eadc
                int32_t v17 = *(int32_t *)(v1 + 44); // 0x8001eae0
                int32_t v18 = *(int32_t *)(v1 + 48); // 0x8001eae4
                kprintf("syscall: #%d, args %x %x %x %x\n", *(int32_t *)(v1 + 28), v15, v16, v17, v18);
            }
            // 0x8001eaf8
            syscall(tf);
            // 0x8001ec08
            cpu_irqoff();
            goto lab_0x8001ec10;
        } else {
            // 0x8001eb08
            g39 = v3;
            switch ((int4_t)v2) {
                case 1: {
                    // 0x8001eb2c
                    if (vm_fault(2, v13) == 0) {
                        // 0x8001ec08
                        cpu_irqoff();
                        goto lab_0x8001ec10;
                    } else {
                        goto lab_0x8001eb90;
                    }
                }
                case 2: {
                    // 0x8001eb48
                    if (vm_fault(0, v13) == 0) {
                        // 0x8001ec08
                        cpu_irqoff();
                        goto lab_0x8001ec10;
                    } else {
                        goto lab_0x8001eb90;
                    }
                }
                case 3: {
                    // 0x8001eb64
                    if (vm_fault(1, v13) == 0) {
                        // 0x8001ec08
                        cpu_irqoff();
                        goto lab_0x8001ec10;
                    } else {
                        goto lab_0x8001eb90;
                    }
                }
                case 6: {
                    // 0x8001eb80
                    panic("Bus error exception, PC=0x%x\n", *(int32_t *)(v1 + 136));
                    goto lab_0x8001eb90;
                }
                case 7: {
                    // 0x8001eb80
                    panic("Bus error exception, PC=0x%x\n", *(int32_t *)(v1 + 136));
                    goto lab_0x8001eb90;
                }
                default: {
                    goto lab_0x8001eb90;
                }
            }
        }
    }
  lab_0x8001ec10:;
    int32_t * v19 = (int32_t *)(v5 + 72); // 0x8001ec10
    if (*v19 == 0) {
        // 0x8001ecc4
        return;
    }
    int32_t * v20 = (int32_t *)(v5 + 80); // 0x8001ec20
    int32_t v21 = *(int32_t *)(*v20 + 4); // 0x8001ec4c
    *(int32_t *)(4 * v21 + (int32_t)&cpustacks) = *v19 + 0x1000;
    int32_t v22 = *(int32_t *)(*v20 + 4); // 0x8001ec78
    if ((*(int32_t *)(4 * v22 + (int32_t)&cpustacks) - 1 ^ v1) >= 0x1000) {
        // 0x8001eca8
        badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)", "../../arch/mips/locore/trap.c", 352, "mips_trap");
    }
  lab_0x8001eb90:
    if ((*(int32_t *)(v1 + 4) & 8) != 0) {
        int32_t v23 = *(int32_t *)(v1 + 136); // 0x8001eb98
        kill_curthread(v23, v3, v23);
    }
    if (v5 == 0) {
        goto lab_0x8001ebc8;
    } else {
        int32_t v24 = *(int32_t *)(v5 + 12); // 0x8001ebb0
        if (v24 == 0) {
            goto lab_0x8001ebc8;
        } else {
            // 0x8001ebc0
            *(int32_t *)(v1 + 136) = v24;
            // 0x8001ec08
            cpu_irqoff();
            goto lab_0x8001ec10;
        }
    }
  lab_0x8001ebc8:;
    int32_t v25 = *(int32_t *)(4 * v3 + (int32_t)&trapcodenames); // 0x8001ebdc
    kprintf("panic: Fatal exception %u (%s) in kernel mode\n", v3, (char *)v25);
    int32_t v26 = *(int32_t *)(v1 + 136); // 0x8001ebec
    kprintf("panic: EPC 0x%x, exception vaddr 0x%x\n", v26, (int32_t)&g35);
    panic("I can't handle this... I think I'll just die now...\n");
    // 0x8001ec08
    cpu_irqoff();
    goto lab_0x8001ec10;
}