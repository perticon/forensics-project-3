void mips_trap(trapframe *tf)

{
  undefined uVar1;
  bool bVar2;
  uint uVar3;
  trapframe *ptVar4;
  int iVar5;
  int iVar6;
  trapframe *ptVar7;
  undefined4 uVar8;
  char *pcVar9;
  uint uVar10;
  uint code;
  vaddr_t unaff_s7;
  
  uVar10 = tf->tf_cause & 0x3c;
  code = uVar10 >> 2;
  uVar3 = tf->tf_status;
  pcVar9 = (char *)tf;
  if (0xc < code) {
    pcVar9 = "code < NTRAPCODES";
    badassert("code < NTRAPCODES","../../arch/mips/locore/trap.c",0x93,"mips_trap");
  }
  if (unaff_s7 != 0) {
    ptVar4 = *(trapframe **)(unaff_s7 + 0x48);
    if (ptVar4 != (trapframe *)0x0) {
      ptVar7 = (trapframe *)&ptVar4[0x1d].tf_a0;
      if (pcVar9 <= ptVar4) {
        pcVar9 = "(vaddr_t)tf > (vaddr_t)curthread->t_stack";
        badassert("(vaddr_t)tf > (vaddr_t)curthread->t_stack","../../arch/mips/locore/trap.c",0x97,
                  "mips_trap");
      }
      if (ptVar7 <= pcVar9) {
        badassert("(vaddr_t)tf < (vaddr_t)(curthread->t_stack + STACK_SIZE)",
                  "../../arch/mips/locore/trap.c",0x99,"mips_trap");
      }
    }
  }
  if (code == 0) {
    uVar1 = *(undefined *)(unaff_s7 + 0x58);
    *(undefined *)(unaff_s7 + 0x58) = 1;
    if (*(int *)(unaff_s7 + 0x5c) == 0) {
      iVar5 = *(int *)(unaff_s7 + 0x60);
      uVar8 = 1;
      if (iVar5 != 0) {
        badassert("curthread->t_iplhigh_count == 0","../../arch/mips/locore/trap.c",0xb7,"mips_trap"
                 );
      }
      *(undefined4 *)(unaff_s7 + 0x5c) = uVar8;
      *(int *)(unaff_s7 + 0x60) = iVar5 + 1;
      bVar2 = true;
    }
    else {
      bVar2 = false;
    }
    mainbus_interrupt(tf);
    if (bVar2) {
      iVar5 = 1;
      if (*(int *)(unaff_s7 + 0x5c) != 1) {
        badassert("curthread->t_curspl == IPL_HIGH","../../arch/mips/locore/trap.c",0xc3,"mips_trap"
                 );
      }
      iVar6 = *(int *)(unaff_s7 + 0x60) + -1;
      if (*(int *)(unaff_s7 + 0x60) != iVar5) {
        badassert("curthread->t_iplhigh_count == 1","../../arch/mips/locore/trap.c",0xc4,"mips_trap"
                 );
      }
      *(int *)(unaff_s7 + 0x60) = iVar6;
      *(undefined4 *)(unaff_s7 + 0x5c) = 0;
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar1;
    goto done2;
  }
  iVar5 = splx(1);
  splx(iVar5);
  switch(uVar10) {
  case 0:
  case 0x10:
  case 0x14:
    goto switchD_8001eb24_caseD_0;
  case 4:
    iVar5 = vm_fault(2,tf->tf_vaddr);
    break;
  case 8:
    iVar5 = vm_fault(0,tf->tf_vaddr);
    break;
  case 0xc:
    iVar5 = vm_fault(1,tf->tf_vaddr);
    break;
  case 0x18:
  case 0x1c:
                    /* WARNING: Subroutine does not return */
    panic("Bus error exception, PC=0x%x\n",tf->tf_epc);
  default:
    if (*(int *)(unaff_s7 + 0x5c) != 0) {
      badassert("curthread->t_curspl == 0","../../arch/mips/locore/trap.c",0xde,"mips_trap");
    }
    iVar5 = -0x7ffd0000;
    if (*(int *)(unaff_s7 + 0x60) != 0) {
      badassert("curthread->t_iplhigh_count == 0","../../arch/mips/locore/trap.c",0xdf,"mips_trap");
    }
    if ((*(uint *)(iVar5 + -0x64c0) & 2) != 0) {
      kprintf("syscall: #%d, args %x %x %x %x\n",tf->tf_v0,tf->tf_a0,tf->tf_a1,tf->tf_a2,tf->tf_a3);
    }
    syscall(tf);
    goto done;
  }
  if (iVar5 != 0) {
switchD_8001eb24_caseD_0:
    if ((uVar3 & 8) != 0) {
      kill_curthread(tf->tf_epc,code,tf->tf_vaddr);
    }
    if ((unaff_s7 == 0) || (*(uint32_t *)(unaff_s7 + 0xc) == 0)) {
      kprintf("panic: Fatal exception %u (%s) in kernel mode\n",code,
              *(undefined4 *)((int)trapcodenames + uVar10));
      kprintf("panic: EPC 0x%x, exception vaddr 0x%x\n",tf->tf_epc,tf->tf_vaddr);
                    /* WARNING: Subroutine does not return */
      panic("I can\'t handle this... I think I\'ll just die now...\n");
    }
    tf->tf_epc = *(uint32_t *)(unaff_s7 + 0xc);
  }
done:
  cpu_irqoff();
done2:
  if (*(int *)(unaff_s7 + 0x48) != 0) {
    cputhreads[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = unaff_s7;
    cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = *(int *)(unaff_s7 + 0x48) + 0x1000;
    if (((cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] - 1 ^ (uint)tf) & 0xfffff000) != 0) {
      badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)",
                "../../arch/mips/locore/trap.c",0x160,"mips_trap");
    }
  }
  return;
}