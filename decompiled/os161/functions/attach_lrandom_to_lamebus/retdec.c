int32_t * attach_lrandom_to_lamebus(int32_t lrandomno, int32_t * sc) {
    int32_t v1 = lamebus_probe(sc, 1, 9, 1, NULL); // 0x80004a1c
    if (v1 < 0) {
        // 0x80004a60
        return NULL;
    }
    char * v2 = kmalloc(8); // 0x80004a2c
    int32_t * result = NULL; // 0x80004a34
    if (v2 != NULL) {
        // 0x80004a38
        *(int32_t *)v2 = (int32_t)sc;
        *(int32_t *)((int32_t)v2 + 4) = v1;
        lamebus_mark(sc, v1);
        result = (int32_t *)v2;
    }
    // 0x80004a60
    return result;
}