lrandom_softc * attach_lrandom_to_lamebus(int lrandomno,lamebus_softc *sc)

{
  uint32_t slot;
  lrandom_softc *plVar1;
  
  slot = lamebus_probe(sc,1,9,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    plVar1 = (lrandom_softc *)0x0;
  }
  else {
    plVar1 = (lrandom_softc *)kmalloc(8);
    if (plVar1 == (lrandom_softc *)0x0) {
      plVar1 = (lrandom_softc *)0x0;
    }
    else {
      plVar1->lr_bus = sc;
      plVar1->lr_buspos = slot;
      lamebus_mark(sc,slot);
    }
  }
  return plVar1;
}