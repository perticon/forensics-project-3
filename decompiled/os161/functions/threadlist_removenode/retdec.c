void threadlist_removenode(int32_t * tln) {
    int32_t v1 = (int32_t)tln;
    int32_t * v2 = (int32_t *)(v1 + 4); // 0x8001766c
    *(int32_t *)*v2 = v1;
    *tln = 0;
    *v2 = 0;
}