void threadlist_removenode(threadlistnode *tln)

{
  tln->tln_prev->tln_next = tln->tln_next;
  tln->tln_next->tln_prev = tln->tln_prev;
  tln->tln_prev = (threadlistnode *)0x0;
  tln->tln_next = (threadlistnode *)0x0;
  return;
}