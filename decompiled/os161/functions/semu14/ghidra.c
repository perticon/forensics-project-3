int semu14(int nargs,char **args)

{
  semaphore *sem;
  
  kprintf("This should assert that sem_count is > 0.\n");
  sem = makesem(0);
  sem->sem_count = sem->sem_count - 1;
  V(sem);
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x23d,"semu14");
  }
                    /* WARNING: Subroutine does not return */
  panic("semu14: V tolerated count wraparound\n");
}