semu14(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	kprintf("This should assert that sem_count is > 0.\n");
	sem = makesem(0);

	/*
	 * The maximum value is (unsigned)-1. Get this by decrementing
	 * from 0.
	 */
	sem->sem_count--;
	V(sem);
	KASSERT(sem->sem_count == 0);
	panic("semu14: V tolerated count wraparound\n");
	return 0;
}