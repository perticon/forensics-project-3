int32_t semu14(int32_t nargs, char ** args) {
    // 0x800122f4
    kprintf("This should assert that sem_count is > 0.\n");
    int32_t * v1 = makesem(0); // 0x80012310
    int32_t * v2 = (int32_t *)((int32_t)v1 + 16); // 0x80012318
    *v2 = *v2 - 1;
    V(v1);
    if (*v2 != 0) {
        // 0x80012340
        badassert("sem->sem_count == 0", "../../test/semunit.c", 573, "semu14");
    }
    // 0x8001235c
    panic("semu14: V tolerated count wraparound\n");
    return &g41;
}