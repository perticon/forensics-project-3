int32_t emufs_eachopendir(int32_t * v2, uint32_t openflags) {
    int32_t result = 18; // 0x800025b0
    if (openflags % 4 == 0) {
        // 0x800025b4
        result = 0;
        if ((openflags & 32) != 0) {
            // 0x800025c0
            return 18;
        }
    }
    // 0x800025c0
    return result;
}