int emu_waitdone(emu_softc *sc)

{
  uint32_t uVar1;
  
  P(sc->e_sem);
  uVar1 = translate_err(sc,sc->e_result);
  return uVar1;
}