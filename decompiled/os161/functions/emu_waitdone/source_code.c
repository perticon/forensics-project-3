emu_waitdone(struct emu_softc *sc)
{
	P(sc->e_sem);
	return translate_err(sc, sc->e_result);
}