int32_t emu_waitdone(int32_t * sc) {
    int32_t v1 = (int32_t)sc;
    P((int32_t *)*(int32_t *)(v1 + 16));
    return translate_err(sc, *(int32_t *)(v1 + 24));
}