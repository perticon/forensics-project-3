int32_t vfs_open(char * path, uint32_t openflags, int32_t mode, int32_t ** ret) {
    uint32_t v1 = openflags % 4; // 0x8001a048
    int32_t * vn = NULL; // bp-288, 0x8001a050
    int32_t v2 = 0; // 0x8001a050
    if (v1 != 0) {
        // 0x8001a05c
        v2 = 1;
        if (v1 == 3) {
            // 0x8001a1ec
            return 8;
        }
    }
    // 0x8001a070
    int32_t result2; // 0x8001a020
    if ((openflags & 4) == 0) {
        // 0x8001a0e4
        result2 = vfs_lookup(path, (int32_t *)&vn);
    } else {
        // 0x8001a07c
        int32_t v3; // bp-28, 0x8001a020
        int32_t v4; // bp-284, 0x8001a020
        int32_t result = vfs_lookparent(path, &v3, (char *)&v4, 256); // 0x8001a08c
        if (result != 0) {
            // 0x8001a1ec
            return result;
        }
        // 0x8001a098
        vnode_check((int32_t *)v3, "creat");
        vnode_decref((int32_t *)v3);
        result2 = *(int32_t *)(*(int32_t *)(v3 + 20) + 60);
    }
    // 0x8001a0f0
    if (result2 != 0) {
        // 0x8001a1ec
        return result2;
    }
    int32_t v5 = (int32_t)vn; // 0x8001a104
    if (vn == NULL) {
        // 0x8001a108
        badassert("vn != NULL", "../../vfs/vfspath.c", 88, "vfs_open");
        v5 = (int32_t)"vn != NULL";
    }
    // 0x8001a124
    vnode_check((int32_t *)v5, "eachopen");
    int32_t v6 = (int32_t)vn;
    int32_t result3 = *(int32_t *)(*(int32_t *)(v6 + 20) + 4); // 0x8001a140
    if (result3 != 0) {
        // 0x8001a158
        vnode_decref(vn);
        // 0x8001a1ec
        return result3;
    }
    // 0x8001a16c
    if ((openflags & 16) == 0) {
        // 0x8001a1d4
        *(int32_t *)ret = v6;
        // 0x8001a1ec
        return 0;
    }
    int32_t * v7 = vn; // 0x8001a17c
    int32_t result4 = 8; // 0x8001a17c
    if (v2 != 0) {
        // 0x8001a1b8
        vnode_check(vn, "truncate");
        v7 = vn;
        int32_t v8 = (int32_t)v7;
        result4 = *(int32_t *)(*(int32_t *)(v8 + 20) + 52);
        if (result4 == 0) {
            // 0x8001a1d4
            *(int32_t *)ret = v8;
            // 0x8001a1ec
            return 0;
        }
    }
    // 0x8001a1c0
    vnode_decref(v7);
    // 0x8001a1ec
    return result4;
}