int vfs_open(char *path,int openflags,mode_t mode,vnode **ret)

{
  bool bVar1;
  int iVar2;
  char *v;
  vnode **in_stack_fffffed8;
  undefined4 in_stack_fffffedc;
  vnode *vn;
  char name [256];
  vnode *dir;
  
  vn = (vnode *)0x0;
  if ((openflags & 3U) == 0) {
    bVar1 = false;
  }
  else {
    bVar1 = true;
    if (2 < (openflags & 3U)) {
      return 8;
    }
  }
  if ((openflags & 4U) == 0) {
    iVar2 = vfs_lookup(path,&vn);
  }
  else {
    iVar2 = vfs_lookparent(path,&dir,name,0x100);
    if (iVar2 != 0) {
      return iVar2;
    }
    vnode_check(dir,"creat");
    in_stack_fffffed8 = &vn;
    iVar2 = (*dir->vn_ops->vop_creat)(dir,name,(openflags & 8U) != 0,mode,in_stack_fffffed8);
    vnode_decref(dir);
  }
  if (iVar2 == 0) {
    v = (char *)vn;
    if (vn == (vnode *)0x0) {
      v = s_vn____NULL_80027408;
      badassert(s_vn____NULL_80027408,"../../vfs/vfspath.c",0x58,"vfs_open");
    }
    vnode_check((vnode *)v,"eachopen");
    iVar2 = (*vn->vn_ops->vop_eachopen)(vn,openflags);
    if (iVar2 == 0) {
      if ((openflags & 0x10U) != 0) {
        if (bVar1) {
          vnode_check(vn,"truncate");
          iVar2 = (*vn->vn_ops->vop_truncate)(vn,CONCAT44(in_stack_fffffed8,in_stack_fffffedc));
        }
        else {
          iVar2 = 8;
        }
        if (iVar2 != 0) {
          vnode_decref(vn);
          return iVar2;
        }
      }
      *ret = vn;
      iVar2 = 0;
    }
    else {
      vnode_decref(vn);
    }
  }
  return iVar2;
}