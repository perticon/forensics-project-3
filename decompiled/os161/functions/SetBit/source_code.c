static void SetBit(int *A, int k)
{

    A[k / 32] |= 1 << (k % 32); // Set the bit at the k-th position in A[i]
}