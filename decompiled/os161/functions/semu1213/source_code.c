semu1213(bool interrupthandler)
{
	struct semaphore *sem;
	struct wchan *wchan;
	const char *name;

	sem = makesem(0);
	makewaiter(sem);
	makewaiter(sem);

	/* check preconditions */
	name = sem->sem_name;
	wchan = sem->sem_wchan;
	KASSERT(!strcmp(name, NAMESTRING));
	wchan = sem->sem_wchan;
	KASSERT(spinlock_not_held(&sem->sem_lock));
	spinlock_acquire(&waiters_lock);
	KASSERT(waiters_running == 2);
	spinlock_release(&waiters_lock);

	/* see above */
	if (interrupthandler) {
		KASSERT(curthread->t_in_interrupt == false);
		curthread->t_in_interrupt = true;
	}

	V(sem);

	if (interrupthandler) {
		KASSERT(curthread->t_in_interrupt == true);
		curthread->t_in_interrupt = false;
	}

	/* give the waiter time to exit */
	clocksleep(1);

	/* check postconditions */
	KASSERT(name == sem->sem_name);
	KASSERT(!strcmp(name, NAMESTRING));
	KASSERT(wchan == sem->sem_wchan);
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 0);
	spinlock_acquire(&waiters_lock);
	KASSERT(waiters_running == 1);
	spinlock_release(&waiters_lock);

	/* clean up */
	ok();
	V(sem);
	clocksleep(1);
	spinlock_acquire(&waiters_lock);
	KASSERT(waiters_running == 0);
	spinlock_release(&waiters_lock);
	sem_destroy(sem);
}