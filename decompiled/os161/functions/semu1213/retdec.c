void semu1213(bool interrupthandler) {
    int32_t * v1 = makesem(0); // 0x80011ba0
    makewaiter(v1);
    makewaiter(v1);
    int32_t v2 = *v1; // 0x80011bb8
    if (strcmp((char *)v2, "some-silly-name") != 0) {
        // 0x80011bd8
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 492, "semu1213");
    }
    int32_t v3 = (int32_t)v1; // 0x80011ba0
    int32_t * v4 = (int32_t *)(v3 + 4); // 0x80011bf8
    int32_t * v5 = (int32_t *)(v3 + 8); // 0x80011c00
    if (!spinlock_not_held(v5)) {
        // 0x80011c0c
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 494, "semu1213");
    }
    // 0x80011c28
    spinlock_acquire(&waiters_lock);
    if (waiters_running != 2) {
        // 0x80011c48
        badassert("waiters_running == 2", "../../test/semunit.c", 496, "semu1213");
    }
    // 0x80011c64
    spinlock_release(&waiters_lock);
    if (!interrupthandler) {
        // 0x80011cac
        V(v1);
    } else {
        // 0x80011c78
        int32_t v6; // 0x80011b7c
        char * v7 = (char *)(v6 + 88);
        char v8 = 1; // 0x80011c84
        if (*v7 != 0) {
            // 0x80011c88
            badassert("curthread->t_in_interrupt == false", "../../test/semunit.c", 501, "semu1213");
            v8 = &g41;
        }
        // 0x80011cbc
        *v7 = v8;
        V(v1);
        if (*v7 == 0) {
            // 0x80011ccc
            badassert("curthread->t_in_interrupt == true", "../../test/semunit.c", 508, "semu1213");
        }
        // 0x80011ce8
        *v7 = 0;
    }
    // 0x80011cec
    clocksleep(1);
    int32_t v9 = v2; // 0x80011d00
    if (*v1 != v2) {
        // 0x80011d04
        badassert("name == sem->sem_name", "../../test/semunit.c", 516, "semu1213");
        v9 = (int32_t)"name == sem->sem_name";
    }
    // 0x80011d24
    if (strcmp((char *)v9, "some-silly-name") != 0) {
        // 0x80011d38
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 517, "semu1213");
    }
    // 0x80011d54
    if (*v4 != *v4) {
        // 0x80011d64
        badassert("wchan == sem->sem_wchan", "../../test/semunit.c", 518, "semu1213");
    }
    // 0x80011d80
    if (!spinlock_not_held(v5)) {
        // 0x80011d90
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 519, "semu1213");
    }
    // 0x80011dac
    if (*(int32_t *)(v3 + 16) != 0) {
        // 0x80011dbc
        badassert("sem->sem_count == 0", "../../test/semunit.c", 520, "semu1213");
    }
    // 0x80011dd8
    spinlock_acquire(&waiters_lock);
    if (waiters_running != 1) {
        // 0x80011df8
        badassert("waiters_running == 1", "../../test/semunit.c", 522, "semu1213");
    }
    // 0x80011e18
    spinlock_release(&waiters_lock);
    ok();
    V(v1);
    clocksleep(1);
    spinlock_acquire(&waiters_lock);
    if (waiters_running != 0) {
        // 0x80011e54
        badassert("waiters_running == 0", "../../test/semunit.c", 530, "semu1213");
    }
    // 0x80011e70
    spinlock_release(&waiters_lock);
    sem_destroy(v1);
}