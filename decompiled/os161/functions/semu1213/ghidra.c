void semu1213(bool interrupthandler)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  undefined uVar3;
  undefined3 in_register_00000010;
  char *a;
  wchan *pwVar4;
  int unaff_s7;
  
  sem = makesem(0);
  makewaiter(sem);
  makewaiter(sem);
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x1ec,"semu1213");
  }
  pwVar4 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x1ee,"semu1213");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 2) {
    badassert("waiters_running == 2","../../test/semunit.c",0x1f0,"semu1213");
  }
  spinlock_release(&waiters_lock);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    uVar3 = 1;
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      uVar3 = 1;
      badassert("curthread->t_in_interrupt == false","../../test/semunit.c",0x1f5,"semu1213");
    }
    *(undefined *)(unaff_s7 + 0x58) = uVar3;
  }
  V(sem);
  if (CONCAT31(in_register_00000010,interrupthandler) != 0) {
    if (*(char *)(unaff_s7 + 0x58) == '\0') {
      badassert("curthread->t_in_interrupt == true","../../test/semunit.c",0x1fc,"semu1213");
    }
    *(undefined *)(unaff_s7 + 0x58) = 0;
  }
  clocksleep(1);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",0x204,"semu1213");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x205,"semu1213");
  }
  if (sem->sem_wchan != pwVar4) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x206,"semu1213");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x207,"semu1213");
  }
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x208,"semu1213");
  }
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 1) {
    badassert("waiters_running == 1","../../test/semunit.c",0x20a,"semu1213");
  }
  spinlock_release(&waiters_lock);
  ok();
  V(sem);
  clocksleep(1);
  spinlock_acquire(&waiters_lock);
  if (waiters_running != 0) {
    badassert("waiters_running == 0","../../test/semunit.c",0x212,"semu1213");
  }
  spinlock_release(&waiters_lock);
  sem_destroy(sem);
  return;
}