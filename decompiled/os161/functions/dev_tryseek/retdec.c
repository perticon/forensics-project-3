int32_t dev_tryseek(int32_t * d, int64_t pos) {
    // 0x80017a58
    int32_t v1; // 0x80017a58
    uint32_t v2 = v1;
    uint32_t v3 = *(int32_t *)((int32_t)d + 4); // 0x80017a70
    if (v3 == 0) {
        // 0x80017ae4
        return 0;
    }
    int64_t v4 = 0x100000000 * pos >> 32; // 0x80017a9c
    if ((v2 | (int32_t)__moddi3(v4, 0)) != 0) {
        // 0x80017ae4
        return 8;
    }
    int32_t v5 = __divdi3(v4, 0); // 0x80017abc
    if (v5 < 0) {
        // 0x80017ae4
        return 0;
    }
    // 0x80017ac8
    return v2 >= v3 | v5 != 0 ? 8 : 0;
}