int dev_tryseek(device *d,off_t pos)

{
  int iVar1;
  undefined4 unaff_s0;
  undefined4 unaff_s1;
  undefined4 unaff_s2;
  uint uVar2;
  longlong lVar3;
  undefined4 in_stack_ffffffe8;
  
  uVar2 = d->d_blocks;
  iVar1 = 0;
  if (uVar2 != 0) {
    lVar3 = __moddi3(CONCAT44(in_stack_ffffffe8,unaff_s0),CONCAT44(unaff_s1,unaff_s2));
    iVar1 = 8;
    if (lVar3 == 0) {
      lVar3 = __divdi3(CONCAT44(in_stack_ffffffe8,unaff_s0),CONCAT44(unaff_s1,unaff_s2));
      if ((lVar3 < 0) || (((int)((ulonglong)lVar3 >> 0x20) == 0 && ((uint)lVar3 < uVar2)))) {
        iVar1 = 0;
      }
      else {
        iVar1 = 8;
      }
    }
  }
  return iVar1;
}