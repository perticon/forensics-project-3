dev_tryseek(struct device *d, off_t pos)
{
	if (d->d_blocks > 0) {
		if ((pos % d->d_blocksize)!=0) {
			/* not block-aligned */
			return EINVAL;
		}
		if (pos / d->d_blocksize >= d->d_blocks) {
			/* off the end */
			return EINVAL;
		}
	}
	else {
		//return ESPIPE;
	}
	return 0;
}