int32_t * cv_create(char * name) {
    char * v1 = kmalloc(16); // 0x80015af0
    if (v1 == NULL) {
        // 0x80015b5c
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x80015b00
    *(int32_t *)v1 = (int32_t)v2;
    if (v2 == NULL) {
        // 0x80015b0c
        kfree(v1);
        // 0x80015b5c
        return NULL;
    }
    int32_t v3 = (int32_t)v1; // 0x80015af0
    int32_t * v4 = wchan_create(v2); // 0x80015b20
    *(int32_t *)(v3 + 4) = (int32_t)v4;
    int32_t * result; // 0x80015ad8
    if (v4 == NULL) {
        // 0x80015b2c
        kfree((char *)*(int32_t *)v1);
        kfree(v1);
        result = NULL;
    } else {
        // 0x80015b48
        spinlock_init((int32_t *)(v3 + 8));
        result = (int32_t *)v1;
    }
    // 0x80015b5c
    return result;
}