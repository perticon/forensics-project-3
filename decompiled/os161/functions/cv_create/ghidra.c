cv * cv_create(char *name)

{
  cv *ptr;
  char *name_00;
  wchan *pwVar1;
  
  ptr = (cv *)kmalloc(0x10);
  if (ptr == (cv *)0x0) {
    ptr = (cv *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->cv_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (cv *)0x0;
    }
    else {
      pwVar1 = wchan_create(name_00);
      ptr->cv_wchan = pwVar1;
      if (pwVar1 == (wchan *)0x0) {
        kfree(ptr->cv_name);
        kfree(ptr);
        ptr = (cv *)0x0;
      }
      else {
        spinlock_init(&ptr->cv_lock);
      }
    }
  }
  return ptr;
}