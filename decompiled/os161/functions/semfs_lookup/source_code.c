semfs_lookup(struct vnode *dirvn, char *path, struct vnode **resultvn)
{
	struct semfs_vnode *dirsemv = dirvn->vn_data;
	struct semfs *semfs = dirsemv->semv_semfs;
	struct semfs_direntry *dent;
	unsigned i, num;
	int result;

	if (!strcmp(path, ".") || !strcmp(path, "..")) {
		VOP_INCREF(dirvn);
		*resultvn = dirvn;
		return 0;
	}

	lock_acquire(semfs->semfs_dirlock);
	num = semfs_direntryarray_num(semfs->semfs_dents);
	for (i=0; i<num; i++) {
		dent = semfs_direntryarray_get(semfs->semfs_dents, i);
		if (dent == NULL) {
			continue;
		}
		if (!strcmp(path, dent->semd_name)) {
			result = semfs_getvnode(semfs, dent->semd_semnum,
						resultvn);
			lock_release(semfs->semfs_dirlock);
			return result;
		}
	}
	lock_release(semfs->semfs_dirlock);
	return ENOENT;
}