int semfs_lookup(vnode *dirvn,char *path,vnode **resultvn)

{
  bool bVar1;
  int iVar2;
  semfs_direntryarray *psVar3;
  uint uVar4;
  char **ppcVar5;
  semfs *semfs;
  uint uVar6;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar2 = strcmp(path,".");
  if ((iVar2 == 0) || (iVar2 = strcmp(path,".."), iVar2 == 0)) {
    vnode_incref(dirvn);
    *resultvn = dirvn;
    iVar2 = 0;
  }
  else {
    uVar4 = 0;
    lock_acquire(semfs->semfs_dirlock);
    uVar6 = (semfs->semfs_dents->arr).num;
    bVar1 = uVar6 != 0;
    while (bVar1) {
      psVar3 = semfs->semfs_dents;
      if ((psVar3->arr).num <= uVar4) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      ppcVar5 = (char **)(psVar3->arr).v[uVar4];
      if ((ppcVar5 != (char **)0x0) && (iVar2 = strcmp(path,*ppcVar5), iVar2 == 0)) {
        iVar2 = semfs_getvnode(semfs,(uint)ppcVar5[1],resultvn);
        lock_release(semfs->semfs_dirlock);
        return iVar2;
      }
      uVar4 = uVar4 + 1;
      bVar1 = uVar4 < uVar6;
    }
    lock_release(semfs->semfs_dirlock);
    iVar2 = 0x13;
  }
  return iVar2;
}