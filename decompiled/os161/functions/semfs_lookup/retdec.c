int32_t semfs_lookup(int32_t * dirvn, char * path, int32_t * resultvn) {
    int32_t v1 = (int32_t)dirvn;
    int32_t v2 = *(int32_t *)(*(int32_t *)(v1 + 16) + 24); // 0x80006ef4
    if (strcmp(path, ".") == 0 || strcmp(path, "..") == 0) {
        // 0x80006f24
        vnode_incref(dirvn);
        *resultvn = v1;
        // 0x80007008
        return 0;
    }
    int32_t * v3 = (int32_t *)(v2 + 20); // 0x80006f38
    lock_acquire((int32_t *)*v3);
    int32_t * v4 = (int32_t *)(v2 + 24); // 0x80006f44
    int32_t v5 = *v4; // 0x80006f44
    uint32_t v6 = *(int32_t *)(v5 + 4); // 0x80006f4c
    int32_t v7 = 0; // 0x80006ff4
    int32_t result; // 0x80006ec0
    if (v6 == 0) {
        // 0x80006ff8
        lock_release((int32_t *)*v3);
        result = 19;
      lab_0x80007008:
        // 0x80007008
        return result;
    }
    int32_t v8 = v5; // 0x80006f70
    if (v7 >= *(int32_t *)(v5 + 4)) {
        // 0x80006f74
        badassert("index < a->num", "../../include/array.h", 100, "array_get");
        v8 = &g41;
    }
    int32_t v9 = *(int32_t *)(*(int32_t *)v8 + 4 * v7); // 0x80006f9c
    int32_t v10; // 0x80006fd0
    if (v9 != 0) {
        // 0x80006fac
        if (strcmp(path, (char *)*(int32_t *)v9) == 0) {
            // 0x80006fc0
            v10 = semfs_getvnode((int32_t *)v2, *(int32_t *)(v9 + 4), resultvn);
            lock_release((int32_t *)*v3);
            result = v10;
            return result;
        }
    }
    // 0x80006fec
    v7++;
    while (v7 < v6) {
        int32_t v11 = *v4; // 0x80006ec0
        v8 = v11;
        if (v7 >= *(int32_t *)(v11 + 4)) {
            // 0x80006f74
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v8 = &g41;
        }
        // 0x80006f90
        v9 = *(int32_t *)(*(int32_t *)v8 + 4 * v7);
        if (v9 != 0) {
            // 0x80006fac
            if (strcmp(path, (char *)*(int32_t *)v9) == 0) {
                // 0x80006fc0
                v10 = semfs_getvnode((int32_t *)v2, *(int32_t *)(v9 + 4), resultvn);
                lock_release((int32_t *)*v3);
                result = v10;
                return result;
            }
        }
        // 0x80006fec
        v7++;
    }
    // 0x80006ff8
    lock_release((int32_t *)*v3);
    // 0x80007008
    return 19;
}