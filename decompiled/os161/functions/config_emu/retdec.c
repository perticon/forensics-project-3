int32_t config_emu(int32_t * sc, int32_t emuno) {
    char name[32]; // bp-48, 0x8000395c
    int32_t v1 = (int32_t)sc;
    int32_t * v2 = lock_create("emufs-lock"); // 0x8000397c
    int32_t * v3 = (int32_t *)(v1 + 12); // 0x80003984
    *v3 = (int32_t)v2;
    if (v2 == NULL) {
        // 0x800039fc
        return 3;
    }
    int32_t * v4 = sem_create("emufs-sem", 0); // 0x80003994
    *(int32_t *)(v1 + 16) = (int32_t)v4;
    int32_t result; // 0x8000395c
    if (v4 == NULL) {
        // 0x800039a0
        lock_destroy((int32_t *)*v3);
        *v3 = 0;
        result = 3;
    } else {
        char * v5 = lamebus_map_area((int32_t *)"emufs-sem", *(int32_t *)(v1 + 4), 0x8000); // 0x800039c4
        *(int32_t *)(v1 + 20) = (int32_t)v5;
        snprintf(name, 32, "emu%d", emuno);
        result = emufs_addtovfs(sc, name);
    }
    // 0x800039fc
    return result;
}