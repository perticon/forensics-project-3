int config_emu(emu_softc *sc,int emuno)

{
  lock *plVar1;
  semaphore *psVar2;
  int iVar3;
  void *pvVar4;
  char name [32];
  
  plVar1 = lock_create("emufs-lock");
  sc->e_lock = plVar1;
  if (plVar1 == (lock *)0x0) {
    iVar3 = 3;
  }
  else {
    psVar2 = sem_create("emufs-sem",0);
    sc->e_sem = psVar2;
    if (psVar2 == (semaphore *)0x0) {
      lock_destroy(sc->e_lock);
      sc->e_lock = (lock *)0x0;
      iVar3 = 3;
    }
    else {
      pvVar4 = lamebus_map_area((lamebus_softc *)sc->e_busdata,sc->e_buspos,0x8000);
      sc->e_iobuf = pvVar4;
      snprintf(name,0x20,"emu%d",emuno);
      iVar3 = emufs_addtovfs(sc,name);
    }
  }
  return iVar3;
}