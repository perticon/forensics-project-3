write_cfg_register(struct lamebus_softc *lb, int slot, uint32_t offset,
		   uint32_t val)
{
	offset += LB_CONFIG_SIZE*slot;
	lamebus_write_register(lb, LB_CONTROLLER_SLOT, offset, val);
}