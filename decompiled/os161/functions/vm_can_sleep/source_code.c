static void vm_can_sleep(void)
{
  if (CURCPU_EXISTS())
  {
    /* must not hold spinlocks */
    KASSERT(curcpu->c_spinlocks == 0);

    /* must not be in an interrupt handler */
    KASSERT(curthread->t_in_interrupt == 0);
  }
}