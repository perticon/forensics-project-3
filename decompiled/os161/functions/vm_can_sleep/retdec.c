void vm_can_sleep(void) {
    // 0x8001aa10
    int32_t v1; // 0x8001aa10
    if (v1 == 0) {
        // 0x8001aa7c
        return;
    }
    // 0x8001aa1c
    if (*(int32_t *)(*(int32_t *)(v1 + 80) + 48) != 0) {
        // 0x8001aa34
        badassert("curcpu->c_spinlocks == 0", "../../vm/addrspace.c", 109, "vm_can_sleep");
    }
    // 0x8001aa50
    if (*(char *)(v1 + 88) != 0) {
        // 0x8001aa60
        badassert("curthread->t_in_interrupt == 0", "../../vm/addrspace.c", 112, "vm_can_sleep");
    }
}