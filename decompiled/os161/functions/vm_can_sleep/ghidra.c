void vm_can_sleep(void)

{
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    if (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) != 0) {
      badassert("curcpu->c_spinlocks == 0",s_______vm_addrspace_c_80027688,0x6d,"vm_can_sleep");
    }
    if (*(char *)(unaff_s7 + 0x58) != '\0') {
      badassert("curthread->t_in_interrupt == 0",s_______vm_addrspace_c_80027688,0x70,"vm_can_sleep"
               );
    }
  }
  return;
}