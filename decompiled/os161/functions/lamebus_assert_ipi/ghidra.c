void lamebus_assert_ipi(lamebus_softc *lamebus,cpu *target)

{
  if (lamebus->ls_uniprocessor == 0) {
    lamebus_write_register(lamebus,0x1f,(target->c_hardware_number + 0x20) * 0x400 + 4,1);
  }
  return;
}