void lamebus_assert_ipi(int32_t * lamebus2, int32_t * target) {
    // 0x80004430
    if (*(int32_t *)((int32_t)lamebus2 + 268) == 0) {
        int32_t v1 = *(int32_t *)((int32_t)target + 8); // 0x80004448
        lamebus_write_register(lamebus2, 31, 1024 * v1 + 0x8004, 1);
    }
}