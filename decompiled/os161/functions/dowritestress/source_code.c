dowritestress(const char *filesys)
{
	int i, err;

	init_threadsem();

	kprintf("*** Starting fs write stress test on %s:\n", filesys);

	for (i=0; i<NTHREADS; i++) {
		err = thread_fork("writestress", NULL,
				  writestress_thread, (char *)filesys, i);
		if (err) {
			panic("thread_fork failed %s\n", strerror(err));
		}
	}

	for (i=0; i<NTHREADS; i++) {
		P(threadsem);
	}

	kprintf("*** fs write stress test done\n");
}