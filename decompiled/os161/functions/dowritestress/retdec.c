void dowritestress(char * filesys) {
    // 0x80010400
    init_threadsem();
    kprintf("*** Starting fs write stress test on %s:\n", filesys);
    int32_t v1 = 0;
    int32_t v2 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1); // 0x80010458
    int32_t v3 = v2; // 0x80010460
    int32_t v4; // 0x80010458
    if (v2 != 0) {
        panic("thread_fork failed %s\n", strerror(v3));
        v4 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x80010464
            panic("thread_fork failed %s\n", strerror(v3));
            v4 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x80010460
    while (v5 < 12) {
        // 0x80010448
        v1 = v5;
        v2 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("thread_fork failed %s\n", strerror(v3));
            v4 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80010464
                panic("thread_fork failed %s\n", strerror(v3));
                v4 = thread_fork("writestress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 1224), filesys, v1);
                v3 = v4;
            }
        }
        // 0x80010480
        v5 = v1 + 1;
    }
    int32_t v6 = 1; // 0x800104a0
    P(threadsem);
    int32_t v7 = v6; // 0x800104ac
    while (v6 < 12) {
        // 0x80010498
        v6 = v7 + 1;
        P(threadsem);
        v7 = v6;
    }
    // 0x800104b0
    kprintf("*** fs write stress test done\n");
}