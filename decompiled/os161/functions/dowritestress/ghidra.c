void dowritestress(char *filesys)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  init_threadsem();
  kprintf("*** Starting fs write stress test on %s:\n",filesys);
  data2 = 0;
  do {
    if (0xb < (int)data2) {
      for (iVar2 = 0; iVar2 < 0xc; iVar2 = iVar2 + 1) {
        P(threadsem);
      }
      kprintf("*** fs write stress test done\n");
      return;
    }
    iVar2 = thread_fork("writestress",(proc *)0x0,writestress_thread,filesys,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("thread_fork failed %s\n",pcVar1);
}