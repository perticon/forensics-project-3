hangman_acquire(struct hangman_actor *a,
		struct hangman_lockable *l)
{
	if (l == &hangman_lock.splk_hangman) {
		/* don't recurse */
		return;
	}

	spinlock_acquire(&hangman_lock);

	if (a->a_waiting != l) {
		spinlock_release(&hangman_lock);
		panic("hangman_acquire: not waiting for lock %s (%p)\n",
		      l->l_name, l);
	}
	if (l->l_holding != NULL) {
		spinlock_release(&hangman_lock);
		panic("hangman_acquire: lock %s (%p) still held by %s (%p)\n",
		      l->l_name, l, a->a_name, a);
	}

	l->l_holding = a;
	a->a_waiting = NULL;

	spinlock_release(&hangman_lock);
}