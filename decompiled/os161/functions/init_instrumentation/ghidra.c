void init_instrumentation(void)

{
  swap_in_pages = 0;
  swap_out_pages = 0;
  faults_with_elf_load = 0;
  faults_with_load = 0;
  new_pages_zeroed = 0;
  tlb_reloads = 0;
  tlb_invalidations = 0;
  tlb_misses_full = 0;
  tlb_misses_free = 0;
  tlb_misses = 0;
  return;
}