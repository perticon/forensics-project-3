void kmalloctest4thread(char * sm, int32_t num) {
    char * ptrs[5]; // bp-64, 0x80010cf8
    int32_t v1 = (int32_t)&ptrs;
    int32_t v2 = 0; // 0x80010d3c
    *(int32_t *)(4 * v2 + v1) = 0;
    v2++;
    int32_t v3 = 0; // 0x80010d48
    int32_t v4 = 0; // 0x80010d48
    int32_t v5 = 2; // 0x80010d48
    while (v2 < 5) {
        // 0x80010d30
        *(int32_t *)(4 * v2 + v1) = 0;
        v2++;
        v3 = 0;
        v4 = 0;
        v5 = 2;
    }
    int32_t * v6 = (int32_t *)(4 * v5 + v1); // 0x80010d70
    int32_t v7 = *v6; // 0x80010d70
    if (v7 != 0) {
        // 0x80010d80
        kfree((char *)v7);
        *v6 = 0;
    }
    int32_t v8 = 4 * v3; // 0x80010d7c
    int32_t v9 = *(int32_t *)(v8 + (int32_t)&g5); // 0x80010d9c
    char * v10 = kmalloc(0x1000 * v9); // 0x80010da4
    *(int32_t *)(v8 + v1) = (int32_t)v10;
    if (v10 == NULL) {
        // 0x80010db8
        panic("kmalloctest4: thread %lu: allocating %u pages failed\n", num, v9);
    }
    int32_t v11 = v4 + 1; // 0x80010df8
    v3 = (v3 + 1) % 5;
    v4 = v11;
    v5 = (v5 + 1) % 5;
    int32_t v12 = 0; // 0x80010e04
    while (v11 < 1200) {
        // 0x80010d68
        v6 = (int32_t *)(4 * v5 + v1);
        v7 = *v6;
        if (v7 != 0) {
            // 0x80010d80
            kfree((char *)v7);
            *v6 = 0;
        }
        // 0x80010d98
        v8 = 4 * v3;
        v9 = *(int32_t *)(v8 + (int32_t)&g5);
        v10 = kmalloc(0x1000 * v9);
        *(int32_t *)(v8 + v1) = (int32_t)v10;
        if (v10 == NULL) {
            // 0x80010db8
            panic("kmalloctest4: thread %lu: allocating %u pages failed\n", num, v9);
        }
        // 0x80010dcc
        v11 = v4 + 1;
        v3 = (v3 + 1) % 5;
        v4 = v11;
        v5 = (v5 + 1) % 5;
        v12 = 0;
    }
    int32_t v13 = *(int32_t *)(4 * v12 + v1); // 0x80010e18
    if (v13 != 0) {
        // 0x80010e28
        kfree((char *)v13);
    }
    int32_t v14 = v12 + 1; // 0x80010e30
    v12 = v14;
    while (v14 < 5) {
        // 0x80010e10
        v13 = *(int32_t *)(4 * v12 + v1);
        if (v13 != 0) {
            // 0x80010e28
            kfree((char *)v13);
        }
        // 0x80010e30
        v14 = v12 + 1;
        v12 = v14;
    }
    // 0x80010e40
    V((int32_t *)sm);
}