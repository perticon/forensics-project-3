void kmalloctest4thread(void *sm,ulong num)

{
  uint uVar1;
  void *pvVar2;
  uint uVar3;
  uint uVar4;
  uint uVar5;
  void *ptrs [5];
  
  for (uVar1 = 0; uVar1 < 5; uVar1 = uVar1 + 1) {
    ptrs[uVar1] = (void *)0x0;
  }
  uVar3 = 0;
  uVar1 = 2;
  uVar4 = 0;
  while( true ) {
    if (0x4af < uVar3) {
      for (uVar1 = 0; uVar1 < 5; uVar1 = uVar1 + 1) {
        if (ptrs[uVar1] != (void *)0x0) {
          kfree(ptrs[uVar1]);
        }
      }
      V((semaphore *)sm);
      return;
    }
    if (ptrs[uVar1] != (void *)0x0) {
      kfree(ptrs[uVar1]);
      ptrs[uVar1] = (void *)0x0;
    }
    uVar5 = kmalloctest4thread::sizes[uVar4];
    pvVar2 = kmalloc(uVar5 << 0xc);
    ptrs[uVar4] = pvVar2;
    if (pvVar2 == (void *)0x0) break;
    uVar4 = (uVar4 + 1) % 5;
    uVar1 = (uVar1 + 1) % 5;
    uVar3 = uVar3 + 1;
  }
                    /* WARNING: Subroutine does not return */
  panic("kmalloctest4: thread %lu: allocating %u pages failed\n",num,uVar5);
}