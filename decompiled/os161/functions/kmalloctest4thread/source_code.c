kmalloctest4thread(void *sm, unsigned long num)
{
#define NUM_KM4_SIZES 5
	static const unsigned sizes[NUM_KM4_SIZES] = { 1, 3, 5, 2, 4 };

	struct semaphore *sem = sm;
	void *ptrs[NUM_KM4_SIZES];
	unsigned p, q;
	unsigned i;

	for (i=0; i<NUM_KM4_SIZES; i++) {
		ptrs[i] = NULL;
	}
	p = 0;
	q = NUM_KM4_SIZES / 2;

	for (i=0; i<NTRIES; i++) {
		if (ptrs[q] != NULL) {
			kfree(ptrs[q]);
			ptrs[q] = NULL;
		}
		ptrs[p] = kmalloc(sizes[p] * PAGE_SIZE);
		if (ptrs[p] == NULL) {
			panic("kmalloctest4: thread %lu: "
			      "allocating %u pages failed\n",
			      num, sizes[p]);
		}
		p = (p + 1) % NUM_KM4_SIZES;
		q = (q + 1) % NUM_KM4_SIZES;
	}

	for (i=0; i<NUM_KM4_SIZES; i++) {
		if (ptrs[i] != NULL) {
			kfree(ptrs[i]);
		}
	}

	V(sem);
}