int copystr(char *dest,char *src,size_t maxlen,size_t stoplen,size_t *gotlen)

{
  uint uVar1;
  uint uVar2;
  
  uVar1 = 0;
  while ((uVar2 = uVar1, uVar2 < maxlen && (uVar2 < stoplen))) {
    dest[uVar2] = src[uVar2];
    uVar1 = uVar2 + 1;
    if (src[uVar2] == '\0') {
      if (gotlen != (size_t *)0x0) {
        *gotlen = uVar2 + 1;
        return 0;
      }
      return 0;
    }
  }
  if (stoplen < maxlen) {
    return 6;
  }
  return 7;
}