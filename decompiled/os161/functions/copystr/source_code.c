copystr(char *dest, const char *src, size_t maxlen, size_t stoplen,
	size_t *gotlen)
{
	size_t i;

	for (i=0; i<maxlen && i<stoplen; i++) {
		dest[i] = src[i];
		if (src[i] == 0) {
			if (gotlen != NULL) {
				*gotlen = i+1;
			}
			return 0;
		}
	}
	if (stoplen < maxlen) {
		/* ran into user-kernel boundary */
		return EFAULT;
	}
	/* otherwise just ran out of space */
	return ENAMETOOLONG;
}