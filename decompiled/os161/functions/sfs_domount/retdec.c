int32_t sfs_domount(char * options, int32_t * dev, int32_t ** ret) {
    int32_t v1 = (int32_t)dev;
    vfs_biglock_acquire();
    if (*(int32_t *)(v1 + 8) != 512) {
        // 0x80008210
        vfs_biglock_release();
        kprintf("sfs: Cannot mount on device with blocksize %zu\n", (int64_t)(int32_t)ret);
        // 0x80008368
        return 26;
    }
    int32_t * v2 = sfs_fs_create(); // 0x80008234
    if (v2 == NULL) {
        // 0x80008240
        vfs_biglock_release();
        // 0x80008368
        return 3;
    }
    int32_t v3 = (int32_t)v2; // 0x80008234
    int32_t * v4 = (int32_t *)(v3 + 524); // 0x80008250
    *v4 = v1;
    int32_t v5 = v3 + 8; // 0x8000825c
    int32_t result = sfs_readblock(v2, 0, (char *)v5, 512); // 0x80008264
    if (result != 0) {
        // 0x80008270
        *v4 = 0;
        sfs_fs_destroy(v2);
        vfs_biglock_release();
        // 0x80008368
        return result;
    }
    int32_t v6 = *(int32_t *)v5; // 0x8000828c
    if (v6 != -0x54520fff) {
        // 0x800082a0
        kprintf("sfs: Wrong magic number in superblock (0x%x, should be 0x%x)\n", v6, -0x54520fff);
        *v4 = 0;
        sfs_fs_destroy(v2);
        vfs_biglock_release();
        // 0x80008368
        return 8;
    }
    int32_t * v7 = (int32_t *)(v3 + 12); // 0x800082c8
    uint32_t v8 = *v7; // 0x800082c8
    uint32_t v9 = *(int32_t *)(v1 + 4); // 0x800082cc
    if (v9 < v8) {
        // 0x800082e0
        kprintf("sfs: warning - fs has %u blocks, device has %u\n", v8, v9);
    }
    // 0x800082ec
    *(char *)(v3 + 47) = 0;
    int32_t * v10 = bitmap_create(*v7 + 4095 & -0x1000); // 0x80008304
    *(int32_t *)(v3 + 532) = (int32_t)v10;
    if (v10 == NULL) {
        // 0x80008310
        *v4 = 0;
        sfs_fs_destroy(v2);
        vfs_biglock_release();
        // 0x80008368
        return 3;
    }
    int32_t v11 = sfs_freemapio(v2, 0); // 0x80008334
    int32_t result2; // 0x800081dc
    if (v11 == 0) {
        // 0x8000835c
        *(int32_t *)ret = v3;
        vfs_biglock_release();
        result2 = 0;
    } else {
        // 0x80008340
        *v4 = 0;
        sfs_fs_destroy(v2);
        vfs_biglock_release();
        result2 = v11;
    }
    // 0x80008368
    return result2;
}