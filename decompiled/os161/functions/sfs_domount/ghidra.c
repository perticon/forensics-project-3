int sfs_domount(void *options,device *dev,fs **ret)

{
  int iVar1;
  sfs_fs *sfs;
  bitmap *pbVar2;
  uint32_t uVar3;
  
  vfs_biglock_acquire();
  if (dev->d_blocksize == 0x200) {
    sfs = sfs_fs_create();
    if (sfs == (sfs_fs *)0x0) {
      vfs_biglock_release();
      iVar1 = 3;
    }
    else {
      sfs->sfs_device = dev;
      iVar1 = sfs_readblock(sfs,0,&sfs->sfs_sb,0x200);
      if (iVar1 == 0) {
        uVar3 = (sfs->sfs_sb).sb_magic;
        if (uVar3 == 0xabadf001) {
          if (dev->d_blocks < (sfs->sfs_sb).sb_nblocks) {
            kprintf("sfs: warning - fs has %u blocks, device has %u\n");
          }
          (sfs->sfs_sb).sb_volname[0x1f] = '\0';
          pbVar2 = bitmap_create((sfs->sfs_sb).sb_nblocks + 0xfff & 0xfffff000);
          sfs->sfs_freemap = pbVar2;
          if (pbVar2 == (bitmap *)0x0) {
            sfs->sfs_device = (device *)0x0;
            sfs_fs_destroy(sfs);
            vfs_biglock_release();
            iVar1 = 3;
          }
          else {
            iVar1 = sfs_freemapio(sfs,UIO_READ);
            if (iVar1 == 0) {
              *ret = &sfs->sfs_absfs;
              vfs_biglock_release();
              iVar1 = 0;
            }
            else {
              sfs->sfs_device = (device *)0x0;
              sfs_fs_destroy(sfs);
              vfs_biglock_release();
            }
          }
        }
        else {
          kprintf("sfs: Wrong magic number in superblock (0x%x, should be 0x%x)\n",uVar3,0xabadf001)
          ;
          sfs->sfs_device = (device *)0x0;
          sfs_fs_destroy(sfs);
          vfs_biglock_release();
          iVar1 = 8;
        }
      }
      else {
        sfs->sfs_device = (device *)0x0;
        sfs_fs_destroy(sfs);
        vfs_biglock_release();
      }
    }
  }
  else {
    vfs_biglock_release();
    kprintf("sfs: Cannot mount on device with blocksize %zu\n",dev->d_blocksize);
    iVar1 = 0x1a;
  }
  return iVar1;
}