sfs_domount(void *options, struct device *dev, struct fs **ret)
{
	int result;
	struct sfs_fs *sfs;

	vfs_biglock_acquire();

	/* We don't pass any options through mount */
	(void)options;

	/*
	 * We can't mount on devices with the wrong sector size.
	 *
	 * (Note: for all intents and purposes here, "sector" and
	 * "block" are interchangeable terms. Technically a filesystem
	 * block may be composed of several hardware sectors, but we
	 * don't do that in sfs.)
	 */
	if (dev->d_blocksize != SFS_BLOCKSIZE) {
		vfs_biglock_release();
		kprintf("sfs: Cannot mount on device with blocksize %zu\n",
			dev->d_blocksize);
		return ENXIO;
	}

	sfs = sfs_fs_create();
	if (sfs == NULL) {
		vfs_biglock_release();
		return ENOMEM;
	}

	/* Set the device so we can use sfs_readblock() */
	sfs->sfs_device = dev;

	/* Load superblock */
	result = sfs_readblock(sfs, SFS_SUPER_BLOCK, &sfs->sfs_sb,
			       sizeof(sfs->sfs_sb));
	if (result) {
		sfs->sfs_device = NULL;
		sfs_fs_destroy(sfs);
		vfs_biglock_release();
		return result;
	}

	/* Make some simple sanity checks */

	if (sfs->sfs_sb.sb_magic != SFS_MAGIC) {
		kprintf("sfs: Wrong magic number in superblock "
			"(0x%x, should be 0x%x)\n",
			sfs->sfs_sb.sb_magic,
			SFS_MAGIC);
		sfs->sfs_device = NULL;
		sfs_fs_destroy(sfs);
		vfs_biglock_release();
		return EINVAL;
	}

	if (sfs->sfs_sb.sb_nblocks > dev->d_blocks) {
		kprintf("sfs: warning - fs has %u blocks, device has %u\n",
			sfs->sfs_sb.sb_nblocks, dev->d_blocks);
	}

	/* Ensure null termination of the volume name */
	sfs->sfs_sb.sb_volname[sizeof(sfs->sfs_sb.sb_volname)-1] = 0;

	/* Load free block bitmap */
	sfs->sfs_freemap = bitmap_create(SFS_FS_FREEMAPBITS(sfs));
	if (sfs->sfs_freemap == NULL) {
		sfs->sfs_device = NULL;
		sfs_fs_destroy(sfs);
		vfs_biglock_release();
		return ENOMEM;
	}
	result = sfs_freemapio(sfs, UIO_READ);
	if (result) {
		sfs->sfs_device = NULL;
		sfs_fs_destroy(sfs);
		vfs_biglock_release();
		return result;
	}

	/* Hand back the abstract fs */
	*ret = &sfs->sfs_absfs;

	vfs_biglock_release();
	return 0;
}