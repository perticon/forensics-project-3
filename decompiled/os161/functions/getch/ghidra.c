int getch(void)

{
  int iVar1;
  char *cs;
  int unaff_s7;
  
  cs = (char *)the_console;
  if (the_console == (con_softc *)0x0) {
    cs = "cs != NULL";
    badassert("cs != NULL","../../dev/generic/console.c",0xde,"getch");
  }
  if ((*(char *)(unaff_s7 + 0x58) != '\0') || (*(int *)(unaff_s7 + 0x60) != 0)) {
    cs = "!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0";
    badassert("!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0",
              "../../dev/generic/console.c",0xdf,"getch");
  }
  iVar1 = getch_intr((con_softc *)cs);
  return iVar1;
}