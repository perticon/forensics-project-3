int32_t getch(void) {
    int32_t v1 = (int32_t)the_console; // 0x80001e6c
    if (the_console == NULL) {
        // 0x80001e70
        badassert("cs != NULL", "../../dev/generic/console.c", 222, "getch");
        v1 = (int32_t)"cs != NULL";
    }
    // 0x80001e8c
    int32_t v2; // 0x80001e54
    if (*(char *)(v2 + 88) != 0) {
        // 0x80001eac
        badassert("!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0", "../../dev/generic/console.c", 223, "getch");
        // 0x80001ecc
        return getch_intr((int32_t *)(int32_t)"!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0");
    }
    // 0x80001e9c
    if (*(int32_t *)(v2 + 96) == 0) {
        // 0x80001ecc
        return getch_intr((int32_t *)v1);
    }
    // 0x80001eac
    badassert("!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0", "../../dev/generic/console.c", 223, "getch");
    // 0x80001ecc
    return getch_intr((int32_t *)(int32_t)"!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0");
}