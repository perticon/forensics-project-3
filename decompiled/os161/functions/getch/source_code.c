getch(void)
{
	struct con_softc *cs = the_console;
	KASSERT(cs != NULL);
	KASSERT(!curthread->t_in_interrupt && curthread->t_iplhigh_count == 0);

	return getch_intr(cs);
}