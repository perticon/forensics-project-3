void array_init(int32_t * a) {
    int32_t v1 = (int32_t)a;
    *(int32_t *)(v1 + 8) = 0;
    *(int32_t *)(v1 + 4) = 0;
    *a = 0;
}