int32_t * sem_create(char * name, int32_t initial_count) {
    char * v1 = kmalloc(20); // 0x80015550
    if (v1 == NULL) {
        // 0x800155c0
        return NULL;
    }
    char * v2 = kstrdup(name); // 0x80015560
    *(int32_t *)v1 = (int32_t)v2;
    if (v2 == NULL) {
        // 0x8001556c
        kfree(v1);
        // 0x800155c0
        return NULL;
    }
    int32_t v3 = (int32_t)v1; // 0x80015550
    int32_t * v4 = wchan_create(v2); // 0x80015580
    *(int32_t *)(v3 + 4) = (int32_t)v4;
    int32_t * result; // 0x80015530
    if (v4 == NULL) {
        // 0x8001558c
        kfree((char *)*(int32_t *)v1);
        kfree(v1);
        result = NULL;
    } else {
        // 0x800155a8
        spinlock_init((int32_t *)(v3 + 8));
        *(int32_t *)(v3 + 16) = initial_count;
        result = (int32_t *)v1;
    }
    // 0x800155c0
    return result;
}