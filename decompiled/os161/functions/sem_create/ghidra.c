semaphore * sem_create(char *name,uint initial_count)

{
  semaphore *ptr;
  char *name_00;
  wchan *pwVar1;
  
  ptr = (semaphore *)kmalloc(0x14);
  if (ptr == (semaphore *)0x0) {
    ptr = (semaphore *)0x0;
  }
  else {
    name_00 = kstrdup(name);
    ptr->sem_name = name_00;
    if (name_00 == (char *)0x0) {
      kfree(ptr);
      ptr = (semaphore *)0x0;
    }
    else {
      pwVar1 = wchan_create(name_00);
      ptr->sem_wchan = pwVar1;
      if (pwVar1 == (wchan *)0x0) {
        kfree(ptr->sem_name);
        kfree(ptr);
        ptr = (semaphore *)0x0;
      }
      else {
        spinlock_init(&ptr->sem_lock);
        ptr->sem_count = initial_count;
      }
    }
  }
  return ptr;
}