void fill_deadbeef(char * vptr, uint32_t len) {
    // 0x8001bca0
    for (int32_t i = 0; i < len / 4; i++) {
        // 0x8001bcb4
        *(int32_t *)(4 * i + (int32_t)vptr) = -0x21524111;
    }
}