void fill_deadbeef(void *vptr,size_t len)

{
  uint uVar1;
  
  for (uVar1 = 0; uVar1 < len >> 2; uVar1 = uVar1 + 1) {
    *(undefined4 *)((int)vptr + uVar1 * 4) = 0xdeadbeef;
  }
  return;
}