threadlist_addhead(struct threadlist *tl, struct thread *t)
{
	DEBUGASSERT(tl != NULL);
	DEBUGASSERT(t != NULL);

	threadlist_insertafternode(&tl->tl_head, t);
	tl->tl_count++;
}