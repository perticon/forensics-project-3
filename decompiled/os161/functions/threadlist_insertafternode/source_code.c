threadlist_insertafternode(struct threadlistnode *onlist, struct thread *t)
{
	struct threadlistnode *addee;

	addee = &t->t_listnode;

	DEBUGASSERT(addee->tln_prev == NULL);
	DEBUGASSERT(addee->tln_next == NULL);

	addee->tln_prev = onlist;
	addee->tln_next = onlist->tln_next;
	addee->tln_prev->tln_next = addee;
	addee->tln_next->tln_prev = addee;
}