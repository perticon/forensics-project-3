void threadlist_insertafternode(threadlistnode *onlist,thread *t)

{
  (t->t_listnode).tln_prev = onlist;
  (t->t_listnode).tln_next = onlist->tln_next;
  onlist->tln_next = &t->t_listnode;
  ((t->t_listnode).tln_next)->tln_prev = &t->t_listnode;
  return;
}