void threadlist_insertafternode(int32_t * onlist, int32_t * t) {
    int32_t v1 = (int32_t)t;
    int32_t v2 = (int32_t)onlist;
    int32_t v3 = v1 + 60; // 0x80017620
    *(int32_t *)v3 = v2;
    int32_t * v4 = (int32_t *)(v2 + 4); // 0x80017628
    int32_t * v5 = (int32_t *)(v1 + 64); // 0x80017630
    *v5 = *v4;
    *v4 = v3;
    *(int32_t *)*v5 = v3;
}