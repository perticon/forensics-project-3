void kfree(void *ptr)

{
  int iVar1;
  
  if (ptr != (void *)0x0) {
    iVar1 = subpage_kfree(ptr);
    if (iVar1 != 0) {
      if (((uint)ptr & 0xfff) != 0) {
        badassert("(vaddr_t)ptr%PAGE_SIZE==0","../../vm/kmalloc.c",0x4c4,"kfree");
      }
      free_kpages((vaddr_t)ptr);
    }
  }
  return;
}