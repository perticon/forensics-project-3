void kfree(char * ptr) {
    if (ptr == NULL || subpage_kfree(ptr) == 0) {
        // 0x8001cb18
        return;
    }
    uint32_t v1 = (int32_t)ptr;
    if (v1 % 0x1000 != 0) {
        // 0x8001caf4
        badassert("(vaddr_t)ptr%PAGE_SIZE==0", "../../vm/kmalloc.c", 1220, "kfree");
    }
    // 0x8001cb10
    free_kpages(v1);
}