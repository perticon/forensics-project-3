sfs_sync_inode(struct sfs_vnode *sv)
{
	struct sfs_fs *sfs = sv->sv_absvn.vn_fs->fs_data;
	int result;

	if (sv->sv_dirty) {
		result = sfs_writeblock(sfs, sv->sv_ino, &sv->sv_i,
					sizeof(sv->sv_i));
		if (result) {
			return result;
		}
		sv->sv_dirty = false;
	}
	return 0;
}