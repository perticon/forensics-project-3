int sfs_sync_inode(sfs_vnode *sv)

{
  int iVar1;
  
  iVar1 = 0;
  if ((sv->sv_dirty != false) &&
     (iVar1 = sfs_writeblock((sfs_fs *)((sv->sv_absvn).vn_fs)->fs_data,sv->sv_ino,&sv->sv_i,0x200),
     iVar1 == 0)) {
    sv->sv_dirty = false;
  }
  return iVar1;
}