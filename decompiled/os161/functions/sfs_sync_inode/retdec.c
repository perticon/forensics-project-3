int32_t sfs_sync_inode(int32_t * sv) {
    int32_t v1 = (int32_t)sv;
    char * v2 = (char *)(v1 + 540); // 0x8000848c
    if (*v2 == 0) {
        // 0x800084b8
        return 0;
    }
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x80008488
    int32_t v4 = *(int32_t *)(v1 + 536); // 0x8000849c
    int32_t v5 = sfs_writeblock((int32_t *)v3, v4, (char *)(v1 + 24), 512); // 0x800084a8
    int32_t result = v5; // 0x800084b0
    if (v5 == 0) {
        // 0x800084b4
        *v2 = 0;
        result = 0;
    }
    // 0x800084b8
    return result;
}