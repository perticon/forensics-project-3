void vfs_bootstrap(void)

{
  array *a;
  
  a = (array *)kmalloc(0xc);
  if (a == (array *)0x0) {
    a = (array *)0x0;
  }
  else {
    array_init(a);
  }
  knowndevs = (knowndevarray *)a;
  if (a == (array *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vfs: Could not create knowndevs array\n");
  }
  vfs_biglock = lock_create("vfs_biglock");
  if (vfs_biglock == (lock *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("vfs: Could not create vfs big lock\n");
  }
  vfs_biglock_depth = 0;
  devnull_create();
  semfs_bootstrap();
  return;
}