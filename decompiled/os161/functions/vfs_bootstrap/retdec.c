void vfs_bootstrap(void) {
    char * v1 = kmalloc(12); // 0x80018698
    if (v1 == NULL) {
        // 0x800186bc
        *(int32_t *)&knowndevs = 0;
        panic("vfs: Could not create knowndevs array\n");
    } else {
        // 0x800186bc
        array_init((int32_t *)v1);
        *(int32_t *)&knowndevs = (int32_t)v1;
    }
    int32_t * v2 = lock_create("vfs_biglock"); // 0x800186d8
    *(int32_t *)&vfs_biglock = (int32_t)v2;
    if (v2 == NULL) {
        // 0x800186e8
        panic("vfs: Could not create vfs big lock\n");
    }
    // 0x800186f4
    vfs_biglock_depth = 0;
    devnull_create();
    semfs_bootstrap();
}