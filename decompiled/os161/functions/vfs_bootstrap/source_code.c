vfs_bootstrap(void)
{
	knowndevs = knowndevarray_create();
	if (knowndevs==NULL) {
		panic("vfs: Could not create knowndevs array\n");
	}

	vfs_biglock = lock_create("vfs_biglock");
	if (vfs_biglock==NULL) {
		panic("vfs: Could not create vfs big lock\n");
	}
	vfs_biglock_depth = 0;

	devnull_create();
	semfs_bootstrap();
}