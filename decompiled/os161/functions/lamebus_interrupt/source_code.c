lamebus_interrupt(struct lamebus_softc *lamebus)
{
	/*
	 * Note that despite the fact that "spl" stands for "set
	 * priority level", we don't actually support interrupt
	 * priorities. When an interrupt happens, we look through the
	 * slots to find the first interrupting device and call its
	 * interrupt routine, no matter what that device is.
	 *
	 * Note that the entire LAMEbus uses only one on-cpu interrupt line.
	 * Thus, we do not use any on-cpu interrupt priority system either.
	 */

	int slot;
	uint32_t mask;
	uint32_t irqs;
	void (*handler)(void *);
	void *data;

	/* For keeping track of how many bogus things happen in a row. */
	static int duds = 0;
	int duds_this_time = 0;

	/* and we better have a valid bus instance. */
	KASSERT(lamebus != NULL);

	/* Lock the softc */
	spinlock_acquire(&lamebus->ls_lock);

	/*
	 * Read the LAMEbus controller register that tells us which
	 * slots are asserting an interrupt condition.
	 */
	irqs = read_ctl_register(lamebus, CTLREG_IRQS);

	if (irqs == 0) {
		/*
		 * Huh? None of them? Must be a glitch.
		 */
		kprintf("lamebus: stray interrupt on cpu %u\n",
			curcpu->c_number);
		duds++;
		duds_this_time++;

		/*
		 * We could just return now, but instead we'll
		 * continue ahead. Because irqs == 0, nothing in the
		 * loop will execute, and passing through it gets us
		 * to the code that checks how many duds we've
		 * seen. This is important, because we just might get
		 * a stray interrupt that latches itself on. If that
		 * happens, we're pretty much toast, but it's better
		 * to panic and hopefully reset the system than to
		 * loop forever printing "stray interrupt".
		 */
	}

	/*
	 * Go through the bits in the value we got back to see which
	 * ones are set.
	 */

	for (mask=1, slot=0; slot<LB_NSLOTS; mask<<=1, slot++) {
		if ((irqs & mask) == 0) {
			/* Nope. */
			continue;
		}

		/*
		 * This slot is signalling an interrupt.
		 */

		if ((lamebus->ls_slotsinuse & mask)==0) {
			/*
			 * No device driver is using this slot.
			 */
			duds++;
			duds_this_time++;
			continue;
		}

		if (lamebus->ls_irqfuncs[slot]==NULL) {
			/*
			 * The device driver hasn't installed an interrupt
			 * handler.
			 */
			duds++;
			duds_this_time++;
			continue;
		}

		/*
		 * Call the interrupt handler. Release the spinlock
		 * while we do so, in case other CPUs are handling
		 * interrupts on other devices.
		 */
		handler = lamebus->ls_irqfuncs[slot];
		data = lamebus->ls_devdata[slot];
		spinlock_release(&lamebus->ls_lock);

		handler(data);

		spinlock_acquire(&lamebus->ls_lock);

		/*
		 * Reload the mask of pending IRQs - if we just called
		 * hardclock, we might not have come back to this
		 * context for some time, and it might have changed.
		 */

		irqs = read_ctl_register(lamebus, CTLREG_IRQS);
	}


	/*
	 * If we get interrupts for a slot with no driver or no
	 * interrupt handler, it's fairly serious. Because LAMEbus
	 * uses level-triggered interrupts, if we don't shut off the
	 * condition, we'll keep getting interrupted continuously and
	 * the system will make no progress. But we don't know how to
	 * do that if there's no driver or no interrupt handler.
	 *
	 * So, if we get too many dud interrupts, panic, since it's
	 * better to panic and reset than to hang.
	 *
	 * If we get through here without seeing any duds this time,
	 * the condition, whatever it was, has gone away. It might be
	 * some stupid device we don't have a driver for, or it might
	 * have been an electrical transient. In any case, warn and
	 * clear the dud count.
	 */

	if (duds_this_time == 0 && duds > 0) {
		kprintf("lamebus: %d dud interrupts\n", duds);
		duds = 0;
	}

	if (duds > 10000) {
		panic("lamebus: too many (%d) dud interrupts\n", duds);
	}

	/* Unlock the softc */
	spinlock_release(&lamebus->ls_lock);
}