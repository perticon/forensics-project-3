void lamebus_interrupt(int32_t * lamebus2) {
    int32_t v1 = (int32_t)lamebus2;
    int32_t v2 = v1; // 0x80004210
    if (lamebus2 == NULL) {
        // 0x80004214
        badassert("lamebus != NULL", "../../dev/lamebus/lamebus.c", 457, "lamebus_interrupt");
        v2 = (int32_t)"lamebus != NULL";
    }
    int32_t * v3 = (int32_t *)v2; // 0x80004238
    spinlock_acquire(v3);
    int32_t v4 = lamebus_read_register(lamebus2, 31, 0x7e04); // 0x80004248
    int32_t v5 = 0; // 0x80004250
    if (v4 == 0) {
        // 0x80004254
        int32_t v6; // 0x800041e4
        kprintf("lamebus: stray interrupt on cpu %u\n", *(int32_t *)(*(int32_t *)(v6 + 80) + 4));
        g13++;
        v5 = 1;
    }
    int32_t v7 = 1;
    int32_t v8 = 0;
    int32_t v9 = v4; // 0x8000429c
    int32_t v10 = v5; // 0x8000429c
    if ((v7 & v4) != 0) {
        // 0x800042a0
        if ((*(int32_t *)(v1 + 8) & v7) == 0) {
            // 0x800042b4
            g13++;
            v9 = v4;
            v10 = v5 + 1;
        } else {
            // 0x800042cc
            if (*(int32_t *)(v1 + 140 + 4 * v8) == 0) {
                // 0x800042e4
                g13++;
                v9 = v4;
                v10 = v5 + 1;
            } else {
                // 0x800042fc
                spinlock_release(v3);
                spinlock_acquire(v3);
                v9 = lamebus_read_register(lamebus2, 31, 0x7e04);
                v10 = v5;
            }
        }
    }
    int32_t v11 = v10;
    int32_t v12 = v8 + 1; // 0x80004338
    int32_t v13 = 2 * v7; // 0x80004344
    while (v12 < 32) {
        // 0x80004298
        v7 = v13;
        int32_t v14 = v9;
        int32_t v15 = v11;
        v8 = v12;
        v9 = v14;
        v10 = v15;
        if ((v7 & v14) != 0) {
            // 0x800042a0
            if ((*(int32_t *)(v1 + 8) & v7) == 0) {
                // 0x800042b4
                g13++;
                v9 = v14;
                v10 = v15 + 1;
            } else {
                // 0x800042cc
                if (*(int32_t *)(v1 + 140 + 4 * v8) == 0) {
                    // 0x800042e4
                    g13++;
                    v9 = v14;
                    v10 = v15 + 1;
                } else {
                    // 0x800042fc
                    spinlock_release(v3);
                    spinlock_acquire(v3);
                    v9 = lamebus_read_register(lamebus2, 31, 0x7e04);
                    v10 = v15;
                }
            }
        }
        // 0x80004334
        v11 = v10;
        v12 = v8 + 1;
        v13 = 2 * v7;
    }
    int32_t v16 = g13;
    if (v11 == 0) {
        if (v16 >= 1) {
            // 0x80004384
            kprintf("lamebus: %d dud interrupts\n", v16);
            g13 = 0;
        }
    } else {
        if (v16 >= 0x2711) {
            // 0x8000438c
            panic("lamebus: too many (%d) dud interrupts\n", v16);
        }
    }
    // 0x80004394
    spinlock_release(lamebus2);
}