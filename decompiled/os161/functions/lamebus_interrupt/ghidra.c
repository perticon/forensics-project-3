void lamebus_interrupt(lamebus_softc *lamebus)

{
  uint32_t uVar1;
  char *splk;
  uint uVar2;
  int iVar3;
  void *pvVar4;
  uint uVar5;
  int unaff_s7;
  lb_irqfunc *plVar6;
  
  splk = (char *)lamebus;
  if (lamebus == (lamebus_softc *)0x0) {
    splk = "lamebus != NULL";
    badassert("lamebus != NULL","../../dev/lamebus/lamebus.c",0x1c9,"lamebus_interrupt");
  }
  spinlock_acquire((spinlock *)splk);
  uVar1 = lamebus_read_register(lamebus,0x1f,0x7e04);
  if (uVar1 == 0) {
    kprintf("lamebus: stray interrupt on cpu %u\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
  }
  uVar5 = (uint)(uVar1 == 0);
  uVar2 = 1;
  for (iVar3 = 0; iVar3 < 0x20; iVar3 = iVar3 + 1) {
    if ((uVar1 & uVar2) != 0) {
      if ((lamebus->ls_slotsinuse & uVar2) == 0) {
        lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
        uVar5 = uVar5 + 1;
      }
      else {
        plVar6 = lamebus->ls_irqfuncs[iVar3];
        if (plVar6 == (lb_irqfunc *)0x0) {
          lamebus_interrupt::duds = lamebus_interrupt::duds + 1;
          uVar5 = uVar5 + 1;
        }
        else {
          pvVar4 = lamebus->ls_devdata[iVar3];
          spinlock_release((spinlock *)splk);
          (*plVar6)(pvVar4);
          spinlock_acquire((spinlock *)splk);
          uVar1 = lamebus_read_register(lamebus,0x1f,0x7e04);
        }
      }
    }
    uVar2 = uVar2 << 1;
  }
  if ((uVar5 == 0) && (0 < lamebus_interrupt::duds)) {
    kprintf("lamebus: %d dud interrupts\n");
    lamebus_interrupt::duds = 0;
  }
  if (lamebus_interrupt::duds < 0x2711) {
    spinlock_release(&lamebus->ls_lock);
    return;
  }
                    /* WARNING: Subroutine does not return */
  panic("lamebus: too many (%d) dud interrupts\n");
}