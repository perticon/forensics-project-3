void lamebus_start_cpus(int32_t * lamebus2) {
    // 0x80003b84
    if (*(int32_t *)((int32_t)lamebus2 + 268) != 0) {
        // 0x80003c50
        return;
    }
    int32_t v1 = lamebus_read_register(lamebus2, 31, 0x7e10); // 0x80003bc0
    int32_t v2 = lamebus_read_register(lamebus2, 31, 0x7e18); // 0x80003bd4
    uint32_t v3 = 0;
    int32_t v4 = 1;
    int32_t v5 = 1;
    int32_t v6 = v4; // 0x80003bfc
    char * v7; // 0x80003c18
    if ((v5 & v1) != 0 == (v5 & v2) == 0) {
        // 0x80003c08
        v7 = lamebus_map_area(lamebus2, 31, 1024 * v3 + 0x8300);
        *(int32_t *)v7 = -0x7ffdf45c;
        *(int32_t *)((int32_t)v7 + 4) = v4;
        v6 = v4 + 1;
    }
    int32_t v8 = v3 + 1; // 0x80003c28
    int32_t v9 = 2 << v3; // 0x80003c34
    while (v8 < 32) {
        // 0x80003bf4
        v3 = v8;
        v4 = v6;
        v5 = v9;
        v6 = v4;
        if ((v5 & v1) != 0 == (v5 & v2) == 0) {
            // 0x80003c08
            v7 = lamebus_map_area(lamebus2, 31, 1024 * v3 + 0x8300);
            *(int32_t *)v7 = -0x7ffdf45c;
            *(int32_t *)((int32_t)v7 + 4) = v4;
            v6 = v4 + 1;
        }
        // 0x80003c28
        v8 = v3 + 1;
        v9 = 2 << v3;
    }
    // 0x80003c38
    __asm_sync();
    lamebus_write_register(lamebus2, 31, 0x7e14, v1);
}