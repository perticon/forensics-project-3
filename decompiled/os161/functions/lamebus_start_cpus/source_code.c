lamebus_start_cpus(struct lamebus_softc *lamebus)
{
	uint32_t cpumask, self, bit;
	uint32_t ctlcpuoffset;
	uint32_t *cram;
	unsigned i;
	unsigned cpunum;

	if (lamebus->ls_uniprocessor) {
		return;
	}

	cpumask = read_ctl_register(lamebus, CTLREG_CPUS);
	self = read_ctl_register(lamebus, CTLREG_SELF);

	/* Poke in the startup address. */
	cpunum = 1;
	for (i=0; i<32; i++) {
		bit = (uint32_t)1 << i;
		if ((cpumask & bit) != 0) {
			if (self & bit) {
				continue;
			}
			ctlcpuoffset = LB_CTLCPU_OFFSET + i * LB_CTLCPU_SIZE;
			cram = lamebus_map_area(lamebus,
						LB_CONTROLLER_SLOT,
						ctlcpuoffset + CTLCPU_CRAM);
			cram[0] = (uint32_t)cpu_start_secondary;
			cram[1] = cpunum++;
		}
	}
	/* Ensure all the above writes get flushed. */
	membar_store_store();

	/* Now, enable them all. */
	write_ctl_register(lamebus, CTLREG_CPUE, cpumask);
}