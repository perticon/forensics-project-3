void lamebus_start_cpus(lamebus_softc *lamebus)

{
  uint32_t val;
  uint32_t uVar1;
  code **ppcVar2;
  uint uVar3;
  uint uVar4;
  code *pcVar5;
  
  if (lamebus->ls_uniprocessor == 0) {
    val = lamebus_read_register(lamebus,0x1f,0x7e10);
    uVar1 = lamebus_read_register(lamebus,0x1f,0x7e18);
    pcVar5 = (code *)0x1;
    for (uVar4 = 0; uVar3 = 1 << (uVar4 & 0x1f), uVar4 < 0x20; uVar4 = uVar4 + 1) {
      if (((uVar3 & val) != 0) && ((uVar3 & uVar1) == 0)) {
        ppcVar2 = (code **)lamebus_map_area(lamebus,0x1f,(uVar4 + 0x20) * 0x400 + 0x300);
        *ppcVar2 = cpu_start_secondary;
        ppcVar2[1] = pcVar5;
        pcVar5 = pcVar5 + 1;
      }
    }
    SYNC(0);
    lamebus_write_register(lamebus,0x1f,0x7e14,val);
  }
  return;
}