vopfail_symlink_nosys(struct vnode *vn, const char *contents,
		      const char *name)
{
	(void)vn;
	(void)contents;
	(void)name;
	return ENOSYS;
}