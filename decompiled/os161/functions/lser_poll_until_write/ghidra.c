void lser_poll_until_write(lser_softc *sc)

{
  undefined3 extraout_var;
  bool bVar2;
  uint32_t uVar1;
  
  bVar2 = spinlock_do_i_hold(&sc->ls_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_do_i_hold(&sc->ls_lock)","../../dev/lamebus/lser.c",0x79,
              "lser_poll_until_write");
  }
  do {
    uVar1 = lamebus_read_register((lamebus_softc *)sc->ls_busdata,sc->ls_buspos,4);
  } while ((uVar1 & 2) == 0);
  return;
}