lser_poll_until_write(struct lser_softc *sc)
{
	uint32_t val;

	KASSERT(spinlock_do_i_hold(&sc->ls_lock));

	do {
		val = bus_read_register(sc->ls_busdata, sc->ls_buspos,
					LSER_REG_WIRQ);
	}
	while ((val & LSER_IRQ_ACTIVE) == 0);
}