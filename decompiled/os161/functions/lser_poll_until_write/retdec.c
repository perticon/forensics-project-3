void lser_poll_until_write(int32_t * sc) {
    // 0x80004bd0
    if (!spinlock_do_i_hold(sc)) {
        // 0x80004bec
        badassert("spinlock_do_i_hold(&sc->ls_lock)", "../../dev/lamebus/lser.c", 121, "lser_poll_until_write");
    }
    int32_t v1 = (int32_t)sc;
    int32_t v2 = *(int32_t *)(v1 + 16); // 0x80004c0c
    while ((lamebus_read_register((int32_t *)*(int32_t *)(v1 + 12), v2, 4) & 2) == 0) {
        // 0x80004c08
        v2 = *(int32_t *)(v1 + 16);
    }
}