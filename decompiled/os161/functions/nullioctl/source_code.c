nullioctl(struct device *dev, int op, userptr_t data)
{
	/*
	 * No ioctls.
	 */

	(void)dev;
	(void)op;
	(void)data;

	return EINVAL;
}