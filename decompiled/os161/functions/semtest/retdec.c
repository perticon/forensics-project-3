int32_t semtest(int32_t nargs, char ** args) {
    // 0x800130ac
    inititems();
    kprintf("Starting semaphore test...\n");
    kprintf("If this hangs, it's broken: ");
    P(testsem);
    P(testsem);
    kprintf("ok\n");
    int32_t v1 = 0;
    int32_t v2 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1); // 0x80013124
    int32_t v3 = v2; // 0x8001312c
    int32_t v4; // 0x80013124
    if (v2 != 0) {
        panic("semtest: thread_fork failed: %s\n", strerror(v3));
        v4 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x80013130
            panic("semtest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x8001312c
    while (v5 < 32) {
        // 0x80013114
        v1 = v5;
        v2 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("semtest: thread_fork failed: %s\n", strerror(v3));
            v4 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80013130
                panic("semtest: thread_fork failed: %s\n", strerror(v3));
                v4 = thread_fork("semtest", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x2960), NULL, v1);
                v3 = v4;
            }
        }
        // 0x8001314c
        v5 = v1 + 1;
    }
    int32_t v6 = 0; // 0x80013170
    v6++;
    V(testsem);
    P((int32_t *)g16);
    int32_t * v7 = testsem;
    while (v6 < 32) {
        // 0x80013168
        v6++;
        V(v7);
        P((int32_t *)g16);
        v7 = testsem;
    }
    // 0x8001318c
    V(v7);
    V(testsem);
    kprintf("Semaphore test done.\n");
    return 0;
}