int semtest(int nargs,char **args)

{
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  inititems();
  kprintf("Starting semaphore test...\n");
  kprintf("If this hangs, it\'s broken: ");
  P(testsem);
  data2 = 0;
  P(testsem);
  kprintf("ok\n");
  do {
    if (0x1f < (int)data2) {
      for (iVar2 = 0; iVar2 < 0x20; iVar2 = iVar2 + 1) {
        V(testsem);
        P(donesem);
      }
      V(testsem);
      V(testsem);
      kprintf("Semaphore test done.\n");
      return 0;
    }
    iVar2 = thread_fork("semtest",(proc *)0x0,semtestthread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("semtest: thread_fork failed: %s\n",pcVar1);
}