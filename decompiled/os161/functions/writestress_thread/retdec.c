void writestress_thread(char * fs, int32_t num) {
    char numstr[8]; // bp-24, 0x8000fb38
    // 0x8000fb38
    snprintf(numstr, 8, "%lu", num);
    if (fstest_write(fs, numstr, 1, 0) != 0 || fstest_read(fs, numstr) != 0) {
        // 0x8000fb84
        kprintf("*** Thread %lu: failed\n", num);
        V(threadsem);
        // 0x8000fc24
        return;
    }
    // 0x8000fbe4
    if (fstest_remove(fs, numstr) != 0) {
        // 0x8000fbf4
        kprintf("*** Thread %lu: failed\n", num);
    }
    // 0x8000fc08
    kprintf("*** Thread %lu: done\n", num);
    V(threadsem);
}