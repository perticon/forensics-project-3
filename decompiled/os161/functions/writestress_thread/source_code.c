writestress_thread(void *fs, unsigned long num)
{
	const char *filesys = fs;
	char numstr[8];
	snprintf(numstr, sizeof(numstr), "%lu", num);

	if (fstest_write(filesys, numstr, 1, 0)) {
		kprintf("*** Thread %lu: failed\n", num);
		V(threadsem);
		return;
	}

	if (fstest_read(filesys, numstr)) {
		kprintf("*** Thread %lu: failed\n", num);
		V(threadsem);
		return;
	}

	if (fstest_remove(filesys, numstr)) {
		kprintf("*** Thread %lu: failed\n", num);
	}

	kprintf("*** Thread %lu: done\n", num);

	V(threadsem);
}