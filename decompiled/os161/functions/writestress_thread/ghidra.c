void writestress_thread(void *fs,ulong num)

{
  int iVar1;
  char numstr [8];
  
  snprintf(numstr,8,"%lu",num);
  iVar1 = fstest_write((char *)fs,numstr,1,0);
  if (iVar1 == 0) {
    iVar1 = fstest_read((char *)fs,numstr);
    if (iVar1 == 0) {
      iVar1 = fstest_remove((char *)fs,numstr);
      if (iVar1 != 0) {
        kprintf("*** Thread %lu: failed\n",num);
      }
      kprintf("*** Thread %lu: done\n",num);
      V(threadsem);
    }
    else {
      kprintf("*** Thread %lu: failed\n",num);
      V(threadsem);
    }
  }
  else {
    kprintf("*** Thread %lu: failed\n",num);
    V(threadsem);
  }
  return;
}