void cpu_machdep_init(int32_t * c) {
    int32_t v1 = (int32_t)c;
    uint32_t v2 = *(int32_t *)(v1 + 4); // 0x8001f1f4
    int32_t v3 = v2; // 0x8001f204
    int32_t v4 = v1; // 0x8001f204
    if (v2 >= 32) {
        // 0x8001f208
        badassert("c->c_number < MAXCPUS", "../../arch/mips/thread/cpu.c", 80, "cpu_machdep_init");
        v3 = &g41;
        v4 = (int32_t)"c->c_number < MAXCPUS";
    }
    int32_t v5 = v3; // 0x8001f26c
    int32_t v6 = v4; // 0x8001f260
    while (true) {
        int32_t v7 = v6;
        int32_t v8 = v5;
        int32_t * v9 = (int32_t *)(v7 + 12); // 0x8001f22c
        int32_t v10 = *(int32_t *)(*v9 + 72); // 0x8001f234
        v5 = v8;
        v6 = v7;
        if (v10 != 0) {
            // 0x8001f244
            *(int32_t *)(4 * v8 + (int32_t)&cpustacks) = v10 + 0x1000;
            v6 = 4 * *(int32_t *)(v7 + 4);
            v5 = v6 + (int32_t)&cputhreads;
            *(int32_t *)v5 = *v9;
        }
    }
}