void cpu_machdep_init(cpu *c)

{
  uint uVar1;
  void *pvVar2;
  char *pcVar3;
  
  uVar1 = c->c_number;
  if (0x1f < uVar1) {
    pcVar3 = "c->c_number < MAXCPUS";
    badassert("c->c_number < MAXCPUS","../../arch/mips/thread/cpu.c",0x50,"cpu_machdep_init");
    c = (cpu *)pcVar3;
  }
  pvVar2 = c->c_curthread->t_stack;
  if (pvVar2 != (void *)0x0) {
    cpustacks[uVar1] = (int)pvVar2 + 0x1000;
    cputhreads[c->c_number] = (vaddr_t)c->c_curthread;
  }
  return;
}