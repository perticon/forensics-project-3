cpu_machdep_init(struct cpu *c)
{
	vaddr_t stackpointer;

	KASSERT(c->c_number < MAXCPUS);

	if (c->c_curthread->t_stack == NULL) {
		/* boot cpu; don't need to do anything here */
	}
	else {
		/*
		 * Stick the stack in cpustacks[], and thread pointer
		 * in cputhreads[].
		 */

		/* stack base address */
		stackpointer = (vaddr_t) c->c_curthread->t_stack;
		/* since stacks grow down, get the top */
		stackpointer += STACK_SIZE;

		cpustacks[c->c_number] = stackpointer;
		cputhreads[c->c_number] = (vaddr_t)c->c_curthread;
	}
}