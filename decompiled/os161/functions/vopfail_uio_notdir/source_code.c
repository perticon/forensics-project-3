vopfail_uio_notdir(struct vnode *vn, struct uio *uio)
{
	(void)vn;
	(void)uio;
	return ENOTDIR;
}