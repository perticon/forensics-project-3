int freeppages(paddr_t addr,long first_page)

{
  ulong uVar1;
  uint k;
  int iVar2;
  ulong uVar3;
  
  uVar3 = allocSize[first_page];
  uVar1 = isTableActive();
  if (uVar1 != 0) {
    k = addr >> 0xc;
    if (allocSize == (ulong *)0x0) {
      badassert("allocSize != NULL","../../vm/coremap.c",0xeb,"freeppages");
    }
    if (nRamFrames <= (int)k) {
      badassert("nRamFrames > first","../../vm/coremap.c",0xec,"freeppages");
    }
    spinlock_acquire(&freemem_lock);
    iVar2 = k + uVar3;
    for (; (int)k < iVar2; k = k + 1) {
      SetBit(freeRamFrames,k);
    }
    spinlock_release(&freemem_lock);
    uVar1 = uVar3;
  }
  return uVar1;
}