int freeppages(paddr_t addr, long first_page)
{
    long i, first, np = (long)allocSize[first_page];

    if (!isTableActive())
        return 0;
    first = addr / PAGE_SIZE;
    KASSERT(allocSize != NULL);
    KASSERT(nRamFrames > first);

    spinlock_acquire(&freemem_lock);
    for (i = first; i < first + np; i++)
    {
        SetBit(freeRamFrames, i);
    }

    spinlock_release(&freemem_lock);

    return (int)np;
}