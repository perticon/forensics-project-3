int32_t freeppages(uint32_t addr, int32_t first_page) {
    int32_t result = *(int32_t *)(4 * first_page + (int32_t)allocSize); // 0x8001b7dc
    if (isTableActive() == 0) {
        // 0x8001b89c
        return 0;
    }
    // 0x8001b7f0
    if (allocSize == NULL) {
        // 0x8001b804
        badassert("allocSize != NULL", "../../vm/coremap.c", 235, "freeppages");
    }
    int32_t v1 = addr / 0x1000; // 0x8001b800
    if (v1 >= g20) {
        // 0x8001b83c
        badassert("nRamFrames > first", "../../vm/coremap.c", 236, "freeppages");
    }
    // 0x8001b858
    spinlock_acquire(&freemem_lock);
    int32_t v2 = result + v1; // 0x8001b868
    int32_t v3 = v1; // 0x8001b88c
    if (v1 >= v2) {
        // 0x8001b890
        spinlock_release(&freemem_lock);
        // 0x8001b89c
        return result;
    }
    SetBit(freeRamFrames, v3);
    v3++;
    while (v3 < v2) {
        // 0x8001b874
        SetBit(freeRamFrames, v3);
        v3++;
    }
    // 0x8001b890
    spinlock_release(&freemem_lock);
    // 0x8001b89c
    return result;
}