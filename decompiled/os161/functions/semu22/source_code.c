semu22(int nargs, char **args)
{
	(void)nargs; (void)args;

	kprintf("This should assert that the semaphore isn't null.\n");
	P(NULL);
	panic("semu22: P tolerated null semaphore\n");
	return 0;
}