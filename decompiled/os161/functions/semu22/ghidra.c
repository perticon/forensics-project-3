int semu22(int nargs,char **args)

{
  kprintf("This should assert that the semaphore isn\'t null.\n");
  P((semaphore *)0x0);
                    /* WARNING: Subroutine does not return */
  panic("semu22: P tolerated null semaphore\n");
}