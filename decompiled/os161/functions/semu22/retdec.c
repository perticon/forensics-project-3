int32_t semu22(int32_t nargs, char ** args) {
    // 0x8001292c
    kprintf("This should assert that the semaphore isn't null.\n");
    P(NULL);
    panic("semu22: P tolerated null semaphore\n");
    return &g41;
}