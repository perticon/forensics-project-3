int32_t threadlisttest(int32_t nargs, char ** args) {
    // 0x800142c0
    kprintf("Testing threadlists...\n");
    int32_t v1 = 0; // 0x800142c0
    int32_t v2 = 0; // 0x80014310
    int32_t * v3 = fakethread_create((char *)*(int32_t *)(v1 + (int32_t)&names)); // 0x80014308
    *(int32_t *)(v1 + (int32_t)&fakethreads) = (int32_t)v3;
    v2++;
    v1 = 4 * v2;
    while (v2 < 7) {
        // 0x800142fc
        v3 = fakethread_create((char *)*(int32_t *)(v1 + (int32_t)&names));
        *(int32_t *)(v1 + (int32_t)&fakethreads) = (int32_t)v3;
        v2++;
        v1 = 4 * v2;
    }
    // 0x80014320
    threadlisttest_a();
    threadlisttest_b();
    threadlisttest_c();
    threadlisttest_d();
    threadlisttest_e();
    threadlisttest_f();
    int32_t v4 = 0; // 0x80014364
    int32_t * v5 = (int32_t *)(4 * v4 + (int32_t)&fakethreads); // 0x8001435c
    v4++;
    fakethread_destroy((int32_t *)*v5);
    *v5 = 0;
    while (v4 < 7) {
        // 0x80014358
        v5 = (int32_t *)(4 * v4 + (int32_t)&fakethreads);
        v4++;
        fakethread_destroy((int32_t *)*v5);
        *v5 = 0;
    }
    // 0x80014378
    kprintf("Done.\n");
    return 0;
}