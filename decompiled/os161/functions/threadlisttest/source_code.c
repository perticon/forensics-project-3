threadlisttest(int nargs, char **args)
{
	unsigned i;

	(void)nargs;
	(void)args;

	kprintf("Testing threadlists...\n");

	for (i=0; i<NUMNAMES; i++) {
		fakethreads[i] = fakethread_create(names[i]);
	}

	threadlisttest_a();
	threadlisttest_b();
	threadlisttest_c();
	threadlisttest_d();
	threadlisttest_e();
	threadlisttest_f();

	for (i=0; i<NUMNAMES; i++) {
		fakethread_destroy(fakethreads[i]);
		fakethreads[i] = NULL;
	}

	kprintf("Done.\n");
	return 0;
}