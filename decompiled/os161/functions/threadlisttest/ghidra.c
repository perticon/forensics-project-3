int threadlisttest(int nargs,char **args)

{
  bool bVar1;
  thread *ptVar2;
  uint uVar3;
  thread **pptVar4;
  
  kprintf("Testing threadlists...\n");
  for (uVar3 = 0; uVar3 < 7; uVar3 = uVar3 + 1) {
    ptVar2 = fakethread_create(names[uVar3]);
    fakethreads[uVar3] = ptVar2;
  }
  uVar3 = 0;
  threadlisttest_a();
  threadlisttest_b();
  threadlisttest_c();
  threadlisttest_d();
  threadlisttest_e();
  threadlisttest_f();
  bVar1 = true;
  while (bVar1) {
    pptVar4 = fakethreads + uVar3;
    uVar3 = uVar3 + 1;
    fakethread_destroy(*pptVar4);
    *pptVar4 = (thread *)0x0;
    bVar1 = uVar3 < 7;
  }
  kprintf("Done.\n");
  return 0;
}