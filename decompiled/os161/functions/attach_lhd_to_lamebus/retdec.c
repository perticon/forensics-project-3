int32_t * attach_lhd_to_lamebus(int32_t lhdno, int32_t * sc) {
    int32_t v1 = lamebus_probe(sc, 1, 3, 2, NULL); // 0x80004584
    if (v1 < 0) {
        // 0x800045e4
        return NULL;
    }
    char * v2 = kmalloc(48); // 0x80004594
    int32_t * result = NULL; // 0x8000459c
    if (v2 != NULL) {
        int32_t v3 = (int32_t)v2; // 0x80004594
        *(int32_t *)v2 = (int32_t)sc;
        *(int32_t *)(v3 + 4) = v1;
        *(int32_t *)(v3 + 8) = lhdno;
        lamebus_mark(sc, v1);
        lamebus_attach_interrupt(sc, v1, v2, (void (*)(char *))-0x7fffb774);
        result = (int32_t *)v2;
    }
    // 0x800045e4
    return result;
}