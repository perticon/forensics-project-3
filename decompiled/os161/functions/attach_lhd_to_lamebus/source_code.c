attach_lhd_to_lamebus(int lhdno, struct lamebus_softc *sc)
{
	struct lhd_softc *lh;
	int slot = lamebus_probe(sc, LB_VENDOR_CS161, LBCS161_DISK,
				 LOW_VERSION, NULL);
	if (slot < 0) {
		/* None found */
		return NULL;
	}

	lh = kmalloc(sizeof(struct lhd_softc));
	if (lh==NULL) {
		/* Out of memory */
		return NULL;
	}

	/* Record what the lhd is attached to */
	lh->lh_busdata = sc;
	lh->lh_buspos = slot;
	lh->lh_unit = lhdno;

	/* Mark the slot in use and collect interrupts */
	lamebus_mark(sc, slot);
	lamebus_attach_interrupt(sc, slot, lh, lhd_irq);

	return lh;
}