lhd_softc * attach_lhd_to_lamebus(int lhdno,lamebus_softc *sc)

{
  uint32_t slot;
  lhd_softc *devdata;
  
  slot = lamebus_probe(sc,1,3,2,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (lhd_softc *)0x0;
  }
  else {
    devdata = (lhd_softc *)kmalloc(0x30);
    if (devdata == (lhd_softc *)0x0) {
      devdata = (lhd_softc *)0x0;
    }
    else {
      devdata->lh_busdata = sc;
      devdata->lh_buspos = slot;
      devdata->lh_unit = lhdno;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,lhd_irq);
    }
  }
  return devdata;
}