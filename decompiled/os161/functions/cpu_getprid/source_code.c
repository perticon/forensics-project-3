cpu_getprid(void)
{
	uint32_t prid;

	__asm volatile("mfc0 %0,$15" : "=r" (prid));
	return prid;
}