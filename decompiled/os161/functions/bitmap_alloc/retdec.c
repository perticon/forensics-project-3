int32_t bitmap_alloc(int32_t * b, int32_t * index) {
    int32_t v1 = (int32_t)b;
    int32_t v2 = 0; // 0x8000ad5c
    int32_t v3 = v1; // 0x8000ad5c
    int32_t v4 = (v1 + 7) / 8; // 0x8000ad5c
    int32_t v5 = 255; // 0x8000ad5c
    int32_t v6; // 0x8000ad3c
    int32_t v7; // 0x8000ad3c
    int32_t v8; // 0x8000ad3c
    int32_t v9; // 0x8000ad3c
    int32_t result; // 0x8000ad3c
    int32_t v10; // 0x8000ad3c
    int32_t v11; // 0x8000ad3c
    while (true) {
      lab_0x8000ae14:;
        int32_t v12 = v2; // 0x8000ad3c
        while (true) {
            // 0x8000ae14
            result = 36;
            if (v12 >= v4) {
                return result;
            }
            int32_t v13 = v12 + 0x21206b6c; // 0x8000ad68
            int32_t v14 = (int32_t)*(char *)v13; // 0x8000ad6c
            v6 = 1;
            v9 = 0;
            v8 = v13;
            v7 = v3;
            v11 = v14;
            v10 = v12;
            v12++;
            if (v5 != v14) {
                goto lab_0x8000ad88;
            }
        }
        goto lab_0x8000adf4;
    }
  lab_0x8000ae2c:
    // 0x8000ae2c
    return result;
  lab_0x8000ad88:;
    int32_t v15 = v10;
    int32_t v16 = v11;
    int32_t v17 = v7;
    int32_t v18 = v8;
    int32_t v19 = v9;
    uint32_t v20 = v6 % 256; // 0x8000ad88
    int32_t v21 = v15; // 0x8000ad94
    int32_t v22 = v16; // 0x8000ad94
    int32_t v23 = v17; // 0x8000ad94
    int32_t v24 = v18; // 0x8000ad94
    int32_t v25 = v19; // 0x8000ad94
    if ((v16 & v20) == 0) {
        int32_t v26 = v16 | v20; // 0x8000ad98
        *(char *)v18 = (char)v26;
        uint32_t v27 = 8 * v15 + v19; // 0x8000ada4
        *(int32_t *)"../../lib/bitmap.c" = v27;
        result = 0;
        if (v27 < *(int32_t *)v17) {
            return result;
        } else {
            // 0x8000adc0
            badassert("*index < b->nbits", "../../lib/bitmap.c", 113, "bitmap_alloc");
            v21 = &g41;
            v22 = v26;
            v23 = (int32_t)"*index < b->nbits";
            v24 = 113;
            v25 = (int32_t)"bitmap_alloc";
            goto lab_0x8000addc;
        }
    } else {
        goto lab_0x8000addc;
    }
  lab_0x8000adf4:
    // 0x8000adf4
    badassert("0", "../../lib/bitmap.c", 117, "bitmap_alloc");
    v2 = &g41;
    v3 = (int32_t)"0";
    v4 = (int32_t)"bitmap_alloc";
    goto lab_0x8000ae14;
  lab_0x8000addc:;
    uint32_t v28 = v25 + 1; // 0x8000ade0
    v5 = 1 << v28;
    v6 = v5;
    v9 = v28;
    v8 = v24;
    v7 = v23;
    v11 = v22;
    v10 = v21;
    if (v28 < 8) {
        goto lab_0x8000ad88;
    } else {
        goto lab_0x8000adf4;
    }
}