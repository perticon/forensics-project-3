timespec_sub(const struct timespec *ts1,
	     const struct timespec *ts2,
	     struct timespec *ret)
{
	/* in case ret and ts1 or ts2 are the same */
	struct timespec r;

	r = *ts1;
	if (r.tv_nsec < ts2->tv_nsec) {
		r.tv_nsec += 1000000000;
		r.tv_sec--;
	}

	r.tv_nsec -= ts2->tv_nsec;
	r.tv_sec -= ts2->tv_sec;
	*ret = r;
}