void timespec_sub(int32_t * ts1, int32_t * ts2, int32_t * ret) {
    int32_t v1 = (int32_t)ts2;
    int32_t v2 = (int32_t)ts1;
    int32_t v3 = *(int32_t *)(v2 + 4); // 0x8000b724
    int32_t v4 = *(int32_t *)(v2 + 8); // 0x8000b728
    int32_t v5 = *(int32_t *)(v1 + 8); // 0x8000b740
    int32_t v6 = v3; // 0x8000b750
    int32_t v7 = v4; // 0x8000b750
    int32_t v8 = v2; // 0x8000b750
    if (v4 < v5) {
        // 0x8000b754
        v6 = v3 - 1;
        v7 = v4 + 0x3b9aca00;
        v8 = v2 - 1 + (int32_t)(v3 != 0);
    }
    int32_t v9 = (int32_t)ret;
    uint32_t v10 = *(int32_t *)(v1 + 4); // 0x8000b788
    *ret = v8 - v1 + (int32_t)(v6 < v10);
    *(int32_t *)(v9 + 4) = v6 - v10;
    *(int32_t *)(v9 + 8) = v7 - v5;
    *(int32_t *)(v9 + 12) = *(int32_t *)(v2 + 12);
}