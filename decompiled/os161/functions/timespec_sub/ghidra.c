void timespec_sub(timespec *ts1,timespec *ts2,timespec *ret)

{
  undefined4 uVar1;
  int iVar2;
  uint uVar3;
  timespec r;
  
  r.tv_sec._0_4_ = *(int *)&ts1->tv_sec;
  uVar3 = *(uint *)((int)&ts1->tv_sec + 4);
  r.tv_nsec = ts1->tv_nsec;
  uVar1 = *(undefined4 *)&ts1->field_0xc;
  iVar2 = ts2->tv_nsec;
  r.tv_sec._4_4_ = uVar3;
  if (r.tv_nsec < iVar2) {
    r.tv_nsec = r.tv_nsec + 1000000000;
    r.tv_sec._4_4_ = uVar3 - 1;
    r.tv_sec._0_4_ = (uint)(r.tv_sec._4_4_ < uVar3) + r.tv_sec._0_4_ + -1;
  }
  uVar3 = r.tv_sec._4_4_ - *(int *)((int)&ts2->tv_sec + 4);
  *(uint *)&ret->tv_sec = (r.tv_sec._0_4_ - *(int *)&ts2->tv_sec) - (uint)(r.tv_sec._4_4_ < uVar3);
  *(uint *)((int)&ret->tv_sec + 4) = uVar3;
  ret->tv_nsec = r.tv_nsec - iVar2;
  *(undefined4 *)&ret->field_0xc = uVar1;
  return;
}