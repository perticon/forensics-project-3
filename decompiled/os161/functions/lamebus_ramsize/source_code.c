lamebus_ramsize(void)
{
	/*
	 * Note that this has to work before bus initialization.
	 * On machines where lamebus_read_register doesn't work
	 * before bus initialization, this function can't be used
	 * for initial RAM size lookup.
	 */

	return read_ctl_register(NULL, CTLREG_RAMSZ);
}