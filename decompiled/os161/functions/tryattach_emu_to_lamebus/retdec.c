int32_t tryattach_emu_to_lamebus(int32_t devunit, int32_t * bus, int32_t busunit) {
    int32_t * v1 = attach_emu_to_lamebus(devunit, bus); // 0x800011cc
    if (v1 == NULL) {
        // 0x80001244
        return -1;
    }
    // 0x800011d8
    kprintf("emu%d at lamebus%d", devunit, busunit);
    int32_t v2 = config_emu(v1, devunit); // 0x800011f4
    int32_t result; // 0x800011b0
    if (v2 == 0) {
        // 0x80001220
        kprintf("\n");
        nextunit_emu = devunit + 1;
        result = 0;
    } else {
        // 0x80001200
        kprintf(": %s\n", strerror(v2));
        result = v2;
    }
    // 0x80001244
    return result;
}