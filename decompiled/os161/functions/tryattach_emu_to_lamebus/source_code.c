tryattach_emu_to_lamebus(int devunit, struct lamebus_softc *bus, int busunit)
{
	struct emu_softc *dev;
	int result;

	dev = attach_emu_to_lamebus(devunit, bus);
	if (dev==NULL) {
		return -1;
	}
	kprintf("emu%d at lamebus%d", devunit, busunit);
	result = config_emu(dev, devunit);
	if (result != 0) {
		kprintf(": %s\n", strerror(result));
		/* should really clean up dev */
		return result;
	}
	kprintf("\n");
	nextunit_emu = devunit+1;
	autoconf_emu(dev, devunit);
	return 0;
}