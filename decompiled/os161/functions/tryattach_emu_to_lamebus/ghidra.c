int tryattach_emu_to_lamebus(int devunit,lamebus_softc *bus,int busunit)

{
  emu_softc *sc;
  int errcode;
  char *pcVar1;
  
  sc = attach_emu_to_lamebus(devunit,bus);
  if (sc == (emu_softc *)0x0) {
    errcode = -1;
  }
  else {
    kprintf("emu%d at lamebus%d",devunit,busunit);
    errcode = config_emu(sc,devunit);
    if (errcode == 0) {
      kprintf("\n");
      nextunit_emu = devunit + 1;
      errcode = 0;
    }
    else {
      pcVar1 = strerror(errcode);
      kprintf(": %s\n",pcVar1);
    }
  }
  return errcode;
}