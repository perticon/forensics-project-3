void lhd_wreg(struct lhd_softc *lh, uint32_t reg, uint32_t val)
{
	bus_write_register(lh->lh_busdata, lh->lh_buspos, reg, val);
}