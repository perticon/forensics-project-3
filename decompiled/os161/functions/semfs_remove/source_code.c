semfs_remove(struct vnode *dirvn, const char *name)
{
	struct semfs_vnode *dirsemv = dirvn->vn_data;
	struct semfs *semfs = dirsemv->semv_semfs;
	struct semfs_direntry *dent;
	struct semfs_sem *sem;
	unsigned i, num;
	int result;

	if (!strcmp(name, ".") || !strcmp(name, "..")) {
		return EINVAL;
	}

	lock_acquire(semfs->semfs_dirlock);
	num = semfs_direntryarray_num(semfs->semfs_dents);
	for (i=0; i<num; i++) {
		dent = semfs_direntryarray_get(semfs->semfs_dents, i);
		if (dent == NULL) {
			continue;
		}
		if (!strcmp(name, dent->semd_name)) {
			/* found */
			sem = semfs_getsembynum(semfs, dent->semd_semnum);
			lock_acquire(sem->sems_lock);
			KASSERT(sem->sems_linked);
			sem->sems_linked = false;
			if (sem->sems_hasvnode == false) {
				lock_acquire(semfs->semfs_tablelock);
				semfs_semarray_set(semfs->semfs_sems,
						   dent->semd_semnum, NULL);
				lock_release(semfs->semfs_tablelock);
				lock_release(sem->sems_lock);
				semfs_sem_destroy(sem);
			}
			else {
				lock_release(sem->sems_lock);
			}
			semfs_direntryarray_set(semfs->semfs_dents, i, NULL);
			semfs_direntry_destroy(dent);
			result = 0;
			goto out;
		}
	}
	result = ENOENT;
 out:
	lock_release(semfs->semfs_dirlock);
	return result;
}