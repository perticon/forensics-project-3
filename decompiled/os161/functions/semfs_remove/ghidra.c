int semfs_remove(vnode *dirvn,char *name)

{
  bool bVar1;
  int iVar2;
  int iVar3;
  semfs_direntryarray *psVar4;
  semfs_sem *sem;
  semfs_semarray *psVar5;
  uint uVar6;
  semfs *semfs;
  semfs_direntry *dent;
  uint uVar7;
  
  semfs = *(semfs **)((int)dirvn->vn_data + 0x18);
  iVar2 = strcmp(name,".");
  iVar3 = 8;
  if (iVar2 != 0) {
    iVar2 = strcmp(name,"..");
    if (iVar2 == 0) {
      iVar3 = 8;
    }
    else {
      uVar6 = 0;
      lock_acquire(semfs->semfs_dirlock);
      uVar7 = (semfs->semfs_dents->arr).num;
      bVar1 = uVar7 != 0;
      while (bVar1) {
        psVar4 = semfs->semfs_dents;
        if ((psVar4->arr).num <= uVar6) {
          badassert("index < a->num","../../include/array.h",100,"array_get");
        }
        dent = (semfs_direntry *)(psVar4->arr).v[uVar6];
        if ((dent != (semfs_direntry *)0x0) && (iVar2 = strcmp(name,dent->semd_name), iVar2 == 0)) {
          sem = semfs_getsembynum(semfs,dent->semd_semnum);
          lock_acquire(sem->sems_lock);
          if (sem->sems_linked == false) {
            badassert("sem->sems_linked","../../fs/semfs/semfs_vnops.c",0x1f0,"semfs_remove");
          }
          sem->sems_linked = false;
          if (sem->sems_hasvnode == false) {
            lock_acquire(semfs->semfs_tablelock);
            psVar5 = semfs->semfs_sems;
            iVar2 = dent->semd_semnum << 2;
            if ((psVar5->arr).num <= dent->semd_semnum) {
              badassert("index < a->num","../../include/array.h",0x6b,"array_set");
            }
            *(undefined4 *)((int)(psVar5->arr).v + iVar2) = 0;
            lock_release(semfs->semfs_tablelock);
            lock_release(sem->sems_lock);
            semfs_sem_destroy(sem);
          }
          else {
            lock_release(sem->sems_lock);
          }
          psVar4 = semfs->semfs_dents;
          if ((psVar4->arr).num <= uVar6) {
            badassert("index < a->num","../../include/array.h",0x6b,"array_set");
          }
          (psVar4->arr).v[uVar6] = (void *)0x0;
          semfs_direntry_destroy(dent);
          iVar3 = 0;
          goto out;
        }
        uVar6 = uVar6 + 1;
        bVar1 = uVar6 < uVar7;
      }
      iVar3 = 0x13;
out:
      lock_release(semfs->semfs_dirlock);
    }
  }
  return iVar3;
}