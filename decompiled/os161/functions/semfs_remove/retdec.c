int32_t semfs_remove(int32_t * dirvn, char * name) {
    int32_t v1 = *(int32_t *)(*(int32_t *)((int32_t)dirvn + 16) + 24); // 0x8000673c
    if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) {
        // 0x80006968
        return 8;
    }
    int32_t * v2 = (int32_t *)(v1 + 20); // 0x80006770
    lock_acquire((int32_t *)*v2);
    int32_t * v3 = (int32_t *)(v1 + 24); // 0x8000677c
    int32_t v4 = *v3; // 0x8000677c
    uint32_t v5 = *(int32_t *)(v4 + 4); // 0x80006784
    int32_t result = 19; // 0x80006948
    uint32_t v6; // 0x80006710
    int32_t v7; // 0x800067a8
    int32_t * v8; // 0x800067e8
    int32_t * v9; // 0x80006800
    int32_t * v10; // 0x80006808
    int32_t v11; // 0x80006808
    char * v12; // 0x80006818
    if (v5 != 0) {
        v6 = 0;
        int32_t v13 = v4; // 0x800067a8
        if (v6 >= *(int32_t *)(v4 + 4)) {
            // 0x800067ac
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v13 = &g41;
        }
        // 0x800067cc
        v7 = 4 * v6;
        int32_t v14 = *(int32_t *)(*(int32_t *)v13 + v7); // 0x800067d8
        if (v14 != 0) {
            // 0x800067e8
            v8 = (int32_t *)v14;
            if (strcmp(name, (char *)*v8) == 0) {
                // 0x800067fc
                v9 = (int32_t *)(v14 + 4);
                v10 = semfs_getsembynum((int32_t *)v1, *v9);
                v11 = (int32_t)v10;
                lock_acquire((int32_t *)*v10);
                v12 = (char *)(v11 + 13);
                if (*v12 == 0) {
                    // 0x80006828
                    badassert("sem->sems_linked", "../../fs/semfs/semfs_vnops.c", 496, "semfs_remove");
                    goto lab_0x80006844;
                } else {
                    goto lab_0x80006844;
                }
            }
        }
        int32_t v15 = v6 + 1;
        result = 19;
        while (v15 < v5) {
            int32_t v16 = *v3; // 0x80006710
            v6 = v15;
            v13 = v16;
            if (v6 >= *(int32_t *)(v16 + 4)) {
                // 0x800067ac
                badassert("index < a->num", "../../include/array.h", 100, "array_get");
                v13 = &g41;
            }
            // 0x800067cc
            v7 = 4 * v6;
            v14 = *(int32_t *)(*(int32_t *)v13 + v7);
            if (v14 != 0) {
                // 0x800067e8
                v8 = (int32_t *)v14;
                if (strcmp(name, (char *)*v8) == 0) {
                    // 0x800067fc
                    v9 = (int32_t *)(v14 + 4);
                    v10 = semfs_getsembynum((int32_t *)v1, *v9);
                    v11 = (int32_t)v10;
                    lock_acquire((int32_t *)*v10);
                    v12 = (char *)(v11 + 13);
                    if (*v12 == 0) {
                        // 0x80006828
                        badassert("sem->sems_linked", "../../fs/semfs/semfs_vnops.c", 496, "semfs_remove");
                        goto lab_0x80006844;
                    } else {
                        goto lab_0x80006844;
                    }
                }
            }
            // 0x80006940
            v15 = v6 + 1;
            result = 19;
        }
    }
    goto lab_0x80006950;
  lab_0x80006950:
    // 0x80006950
    lock_release((int32_t *)*v2);
    // 0x80006968
    return result;
  lab_0x80006844:
    // 0x80006844
    *v12 = 0;
    int32_t v17; // 0x80006710
    int32_t * v18; // 0x80006858
    int32_t v19; // 0x80006864
    if (*(char *)(v11 + 12) == 0) {
        // 0x80006858
        v18 = (int32_t *)(v1 + 8);
        lock_acquire((int32_t *)*v18);
        v19 = *(int32_t *)(v1 + 16);
        uint32_t v20 = *v9; // 0x80006868
        v17 = 4 * v20;
        if (v20 < *(int32_t *)(v19 + 4)) {
            goto lab_0x800068a0;
        } else {
            // 0x80006880
            badassert("index < a->num", "../../include/array.h", 107, "array_set");
            v17 = &g41;
            goto lab_0x800068a0;
        }
    } else {
        // 0x800068d8
        lock_release((int32_t *)*v10);
        goto lab_0x800068e4;
    }
  lab_0x800068a0:
    // 0x800068a0
    *(int32_t *)(*(int32_t *)v19 + v17) = 0;
    lock_release((int32_t *)*v18);
    lock_release((int32_t *)*v10);
    semfs_sem_destroy(v10);
    goto lab_0x800068e4;
  lab_0x800068e4:;
    int32_t v21 = *v3; // 0x800068e4
    int32_t v22 = v21; // 0x800068fc
    if (v6 < *(int32_t *)(v21 + 4)) {
        goto lab_0x8000691c;
    } else {
        // 0x80006900
        badassert("index < a->num", "../../include/array.h", 107, "array_set");
        v22 = &g41;
        goto lab_0x8000691c;
    }
  lab_0x8000691c:
    // 0x8000691c
    *(int32_t *)(*(int32_t *)v22 + v7) = 0;
    semfs_direntry_destroy(v8);
    result = 0;
    goto lab_0x80006950;
}