static void add_swap_list(struct swap_entry *entry)
{

    entry->next = swap_list->next;
    entry->previous = swap_list;
    swap_list->next->previous = entry;
    swap_list->next = entry;
}