addrspace * as_create(void)

{
  addrspace *paVar1;
  
  paVar1 = (addrspace *)kmalloc(0x10);
  if (paVar1 == (addrspace *)0x0) {
    paVar1 = (addrspace *)0x0;
  }
  else {
    paVar1->as_vbase1 = 0;
    paVar1->as_vbase2 = 0;
  }
  return paVar1;
}