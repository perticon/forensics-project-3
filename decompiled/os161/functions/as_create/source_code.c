struct addrspace *as_create(void)
{
  struct addrspace *as;

  as = kmalloc(sizeof(struct addrspace));
  if (as == NULL)
  {
    return NULL;
  }

  as->as_vbase1 = 0;
  as->as_vbase2 = 0;

  return as;
}