int32_t cmd_chdir(int32_t nargs, char ** args) {
    int32_t result; // 0x8000c554
    if (nargs == 2) {
        // 0x8000c578
        result = vfs_chdir((char *)*(int32_t *)((int32_t)args + 4));
    } else {
        // 0x8000c564
        kprintf("Usage: cd directory\n");
        result = 8;
    }
    // 0x8000c584
    return result;
}