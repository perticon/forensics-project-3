int cmd_chdir(int nargs,char **args)

{
  int iVar1;
  
  if (nargs == 2) {
    iVar1 = vfs_chdir(args[1]);
  }
  else {
    kprintf("Usage: cd directory\n");
    iVar1 = 8;
  }
  return iVar1;
}