cmd_chdir(int nargs, char **args)
{
	if (nargs != 2)
	{
		kprintf("Usage: cd directory\n");
		return EINVAL;
	}

	return vfs_chdir(args[1]);
}