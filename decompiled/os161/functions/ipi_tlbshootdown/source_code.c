ipi_tlbshootdown(struct cpu *target, const struct tlbshootdown *mapping)
{
	unsigned n;

	spinlock_acquire(&target->c_ipi_lock);

	n = target->c_numshootdown;
	if (n == TLBSHOOTDOWN_MAX) {
		/*
		 * If you have problems with this panic going off,
		 * consider: (1) increasing the maximum, (2) putting
		 * logic here to sleep until space appears (may
		 * interact awkwardly with VM system locking), (3)
		 * putting logic here to coalesce requests together,
		 * and/or (4) improving VM system state tracking to
		 * reduce the number of unnecessary shootdowns.
		 */
		panic("ipi_tlbshootdown: Too many shootdowns queued\n");
	}
	else {
		target->c_shootdown[n] = *mapping;
		target->c_numshootdown = n+1;
	}

	target->c_ipi_pending |= (uint32_t)1 << IPI_TLBSHOOTDOWN;
	mainbus_send_ipi(target);

	spinlock_release(&target->c_ipi_lock);
}