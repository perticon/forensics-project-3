void ipi_tlbshootdown(int32_t * target, int32_t * mapping) {
    int32_t v1 = (int32_t)target;
    int32_t * v2 = (int32_t *)(v1 + 164); // 0x80017480
    spinlock_acquire(v2);
    int32_t * v3 = (int32_t *)(v1 + 160); // 0x80017484
    int32_t v4 = *v3; // 0x80017484
    int32_t v5 = v4; // 0x80017490
    if (v4 == 16) {
        // 0x80017494
        panic("ipi_tlbshootdown: Too many shootdowns queued\n");
        v5 = &g41;
    }
    // 0x800174a0
    *(int32_t *)(v1 + 96 + 4 * v4) = (int32_t)mapping;
    *v3 = v5 + 1;
    int32_t * v6 = (int32_t *)(v1 + 92); // 0x800174bc
    *v6 = *v6 | 8;
    mainbus_send_ipi(target);
    spinlock_release(v2);
}