void ipi_tlbshootdown(cpu *target,tlbshootdown *mapping)

{
  uint uVar1;
  
  spinlock_acquire(&target->c_ipi_lock);
  uVar1 = target->c_numshootdown;
  if (uVar1 == 0x10) {
                    /* WARNING: Subroutine does not return */
    panic("ipi_tlbshootdown: Too many shootdowns queued\n");
  }
  target->c_shootdown[uVar1].ts_placeholder = mapping->ts_placeholder;
  target->c_numshootdown = uVar1 + 1;
  target->c_ipi_pending = target->c_ipi_pending | 8;
  mainbus_send_ipi(target);
  spinlock_release(&target->c_ipi_lock);
  return;
}