writestress2_thread(void *fs, unsigned long num)
{
	const char *filesys = fs;

	if (fstest_write(filesys, "", NTHREADS, num)) {
		kprintf("*** Thread %lu: failed\n", num);
		V(threadsem);
		return;
	}

	V(threadsem);
}