void writestress2_thread(void *fs,ulong num)

{
  int iVar1;
  
  iVar1 = fstest_write((char *)fs,"",0xc,num);
  if (iVar1 == 0) {
    V(threadsem);
  }
  else {
    kprintf("*** Thread %lu: failed\n",num);
    V(threadsem);
  }
  return;
}