void writestress2_thread(char * fs, int32_t num) {
    // 0x8000fc38
    if (fstest_write(fs, (char *)&g10, 12, num) == 0) {
        // 0x8000fc8c
        V(threadsem);
    } else {
        // 0x8000fc64
        kprintf("*** Thread %lu: failed\n", num);
        V(threadsem);
    }
}