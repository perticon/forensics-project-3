void init_sem(void) {
    // 0x800143b0
    if (tsem != NULL) {
        // 0x800143f8
        return;
    }
    int32_t * v1 = sem_create("tsem", 0); // 0x800143d4
    *(int32_t *)&tsem = (int32_t)v1;
    if (v1 == NULL) {
        // 0x800143e4
        panic("threadtest: sem_create failed\n");
    }
}