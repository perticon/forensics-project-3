void lhd_irq(char * vlh) {
    int32_t * v1 = (int32_t *)((int32_t)vlh + 4); // 0x800048a4
    int32_t v2 = lamebus_read_register((int32_t *)vlh, *v1, 4); // 0x800048ac
    switch ((int5_t)v2 & -3) {
        case 12: {
        }
        case -12: {
        }
        case 4: {
            // 0x800048d4
            lamebus_write_register((int32_t *)vlh, *v1, 4, 0);
            lhd_iodone((int32_t *)vlh, lhd_code_to_errno((int32_t *)vlh, v2));
            // break -> 0x80004900
            break;
        }
    }
}