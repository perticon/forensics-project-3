lhd_irq(void *vlh)
{
	struct lhd_softc *lh = vlh;
	uint32_t val;

	val = lhd_rdreg(lh, LHD_REG_STAT);

	switch (val & LHD_STATEMASK) {
	    case LHD_IDLE:
	    case LHD_WORKING:
		break;
	    case LHD_OK:
	    case LHD_INVSECT:
	    case LHD_MEDIA:
		lhd_wreg(lh, LHD_REG_STAT, 0);
		lhd_iodone(lh, lhd_code_to_errno(lh, val));
		break;
	}
}