void lhd_irq(void *vlh)

{
  uint32_t code;
  int err;
  uint uVar1;
  
                    /* WARNING: Load size is inaccurate */
  code = lamebus_read_register(*vlh,*(int *)((int)vlh + 4),4);
  uVar1 = code & 0x1d;
  if (((uVar1 == 0xc) || (uVar1 == 0x14)) || (uVar1 == 4)) {
                    /* WARNING: Load size is inaccurate */
    lamebus_write_register(*vlh,*(int *)((int)vlh + 4),4,0);
    err = lhd_code_to_errno((lhd_softc *)vlh,code);
    lhd_iodone((lhd_softc *)vlh,err);
  }
  return;
}