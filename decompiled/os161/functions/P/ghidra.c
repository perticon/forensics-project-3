void P(semaphore *sem)

{
  char *pcVar1;
  spinlock *splk;
  int unaff_s7;
  
  pcVar1 = (char *)sem;
  if (sem == (semaphore *)0x0) {
    pcVar1 = "sem != NULL";
    badassert("sem != NULL","../../thread/synch.c",0x5d,"P");
  }
  if (*(char *)(unaff_s7 + 0x58) != '\0') {
    pcVar1 = "curthread->t_in_interrupt == false";
    badassert("curthread->t_in_interrupt == false","../../thread/synch.c",0x65,"P");
  }
  splk = (spinlock *)((int)pcVar1 + 8);
  spinlock_acquire(splk);
  while (sem->sem_count == 0) {
    wchan_sleep(sem->sem_wchan,splk);
  }
  if (sem->sem_count == 0) {
    badassert("sem->sem_count > 0","../../thread/synch.c",0x78,"P");
  }
  sem->sem_count = sem->sem_count - 1;
  spinlock_release(splk);
  return;
}