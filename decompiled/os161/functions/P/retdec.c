void P(int32_t * sem) {
    int32_t v1 = (int32_t)sem;
    int32_t v2 = v1; // 0x80015658
    if (sem == NULL) {
        // 0x8001565c
        badassert("sem != NULL", "../../thread/synch.c", 93, "P");
        v2 = (int32_t)"sem != NULL";
    }
    int32_t v3 = v2; // 0x80015688
    int32_t v4; // 0x80015644
    if (*(char *)(v4 + 88) != 0) {
        // 0x8001568c
        badassert("curthread->t_in_interrupt == false", "../../thread/synch.c", 101, "P");
        v3 = (int32_t)"curthread->t_in_interrupt == false";
    }
    int32_t * v5 = (int32_t *)(v3 + 8); // 0x800156b0
    spinlock_acquire(v5);
    int32_t * v6 = (int32_t *)(v1 + 16); // 0x800156c8
    int32_t v7 = *v6; // 0x800156c8
    if (v7 != 0) {
        // 0x80015704
        *v6 = v7 - 1;
        spinlock_release(v5);
        return;
    }
    wchan_sleep((int32_t *)*(int32_t *)(v1 + 4), v5);
    int32_t v8 = *v6; // 0x800156c8
    // 0x800156bc
    while (v8 == 0) {
        // 0x800156bc
        wchan_sleep((int32_t *)*(int32_t *)(v1 + 4), v5);
        v8 = *v6;
    }
    // 0x80015704
    *v6 = v8 - 1;
    spinlock_release(v5);
}