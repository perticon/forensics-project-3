int32_t * bitmap_create(uint32_t nbits) {
    char * v1 = kmalloc(8); // 0x8000ac34
    if (v1 == NULL) {
        // 0x8000ad18
        return NULL;
    }
    uint32_t v2 = (nbits + 7) / 8; // 0x8000ac2c
    char * v3 = kmalloc(v2); // 0x8000ac44
    int32_t * v4 = (int32_t *)((int32_t)v1 + 4); // 0x8000ac4c
    *v4 = (int32_t)v3;
    if (v3 == NULL) {
        // 0x8000ac50
        kfree(v1);
        // 0x8000ad18
        return NULL;
    }
    // 0x8000ac60
    bzero(v3, v2);
    uint32_t v5 = nbits / 8; // 0x8000ac6c
    *(int32_t *)v1 = nbits;
    if (v5 >= v2) {
        // 0x8000ad18
        return (int32_t *)v1;
    }
    int32_t v6 = v2 - 1; // 0x8000ac7c
    int32_t v7 = v5; // 0x8000ac88
    if (v5 != v6) {
        // 0x8000ac8c
        badassert("nbits / BITS_PER_WORD == words-1", "../../lib/bitmap.c", 81, "bitmap_create");
        v7 = (int32_t)"../../lib/bitmap.c";
    }
    int32_t v8 = nbits - 8 * v6; // 0x8000ac88
    int32_t v9 = v8; // 0x8000acb8
    int32_t v10 = 1; // 0x8000acb8
    int32_t v11 = v7; // 0x8000acb8
    int32_t v12; // 0x8000ac10
    int32_t v13; // 0x8000ac10
    char v14; // 0x8000ac10
    int32_t v15; // 0x8000ac10
    if (v8 < 8) {
        goto lab_0x8000ad00;
    } else {
        // 0x8000acbc
        badassert("overbits > 0 && overbits < BITS_PER_WORD", "../../lib/bitmap.c", 82, "bitmap_create");
        v15 = v8;
        v12 = (int32_t)"overbits > 0 && overbits < BITS_PER_WORD";
        v13 = (int32_t)"../../lib/bitmap.c";
        v14 = (char)"bitmap_create";
        goto lab_0x8000acd8;
    }
  lab_0x8000ad00:
    // 0x8000ad00
    v15 = v9;
    v12 = v10;
    v13 = v11;
    v14 = v10 << v9;
    if (v9 >= 8) {
        // 0x8000ad18
        return (int32_t *)v1;
    }
    goto lab_0x8000acd8;
  lab_0x8000acd8:
    // 0x8000acd8
    v11 = v13;
    char * v16 = (char *)(*v4 + v11); // 0x8000ace4
    *v16 = *v16 | v14;
    v9 = v15 + 1;
    v10 = v12;
    goto lab_0x8000ad00;
}