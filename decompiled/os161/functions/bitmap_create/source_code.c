bitmap_create(unsigned nbits)
{
        struct bitmap *b;
        unsigned words;

        words = DIVROUNDUP(nbits, BITS_PER_WORD);
        b = kmalloc(sizeof(struct bitmap));
        if (b == NULL) {
                return NULL;
        }
        b->v = kmalloc(words*sizeof(WORD_TYPE));
        if (b->v == NULL) {
                kfree(b);
                return NULL;
        }

        bzero(b->v, words*sizeof(WORD_TYPE));
        b->nbits = nbits;

        /* Mark any leftover bits at the end in use */
        if (words > nbits / BITS_PER_WORD) {
                unsigned j, ix = words-1;
                unsigned overbits = nbits - ix*BITS_PER_WORD;

                KASSERT(nbits / BITS_PER_WORD == words-1);
                KASSERT(overbits > 0 && overbits < BITS_PER_WORD);

                for (j=overbits; j<BITS_PER_WORD; j++) {
                        b->v[ix] |= ((WORD_TYPE)1 << j);
                }
        }

        return b;
}