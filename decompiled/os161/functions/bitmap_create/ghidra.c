bitmap * bitmap_create(uint nbits)

{
  bitmap *ptr;
  uchar *vblock;
  uint uVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar4 = (char *)(nbits + 7 >> 3);
  ptr = (bitmap *)kmalloc(8);
  if (ptr == (bitmap *)0x0) {
    ptr = (bitmap *)0x0;
  }
  else {
    vblock = (uchar *)kmalloc((size_t)pcVar4);
    ptr->v = vblock;
    if (vblock == (uchar *)0x0) {
      kfree(ptr);
      ptr = (bitmap *)0x0;
    }
    else {
      bzero(vblock,(size_t)pcVar4);
      ptr->nbits = nbits;
      if ((char *)(nbits >> 3) < pcVar4) {
        pcVar4 = pcVar4 + -1;
        uVar1 = nbits + (int)pcVar4 * -8;
        if ((char *)(nbits >> 3) != pcVar4) {
          pcVar4 = s_______lib_bitmap_c_800235b4;
          badassert("nbits / BITS_PER_WORD == words-1",s_______lib_bitmap_c_800235b4,0x51,
                    "bitmap_create");
        }
        if (uVar1 - 1 < 7) {
          pcVar2 = (char *)0x1;
          goto LAB_8000ad00;
        }
        pcVar2 = "overbits > 0 && overbits < BITS_PER_WORD";
        pcVar4 = s_______lib_bitmap_c_800235b4;
        pcVar3 = "bitmap_create";
        badassert("overbits > 0 && overbits < BITS_PER_WORD",s_______lib_bitmap_c_800235b4,0x52,
                  "bitmap_create");
        do {
          ptr->v[(int)pcVar4] = (byte)pcVar3 | ptr->v[(int)pcVar4];
          uVar1 = uVar1 + 1;
LAB_8000ad00:
          pcVar3 = (char *)((int)pcVar2 << (uVar1 & 0x1f));
        } while (uVar1 < 8);
      }
    }
  }
  return ptr;
}