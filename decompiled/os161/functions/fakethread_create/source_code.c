fakethread_create(const char *name)
{
	struct thread *t;

	t = kmalloc(sizeof(*t));
	if (t == NULL) {
		panic("threadlisttest: Out of memory\n");
	}
	/* ignore most of the fields, zero everything for tidiness */
	bzero(t, sizeof(*t));
	t->t_name = kstrdup(name);
	if (t->t_name == NULL) {
		panic("threadlisttest: Out of memory\n");
	}
	t->t_stack = FAKE_MAGIC;
	threadlistnode_init(&t->t_listnode, t);
	return t;
}