int32_t * fakethread_create(char * name) {
    char * v1 = kmalloc(100); // 0x80013558
    int32_t v2 = (int32_t)v1; // 0x80013558
    int32_t v3 = v2; // 0x80013560
    if (v1 == NULL) {
        // 0x80013564
        panic("threadlisttest: Out of memory\n");
        v3 = &g41;
    }
    // 0x80013570
    bzero((char *)v3, 100);
    char * v4 = kstrdup(name); // 0x80013580
    *(int32_t *)v1 = (int32_t)v4;
    if (v4 == NULL) {
        // 0x8001358c
        panic("threadlisttest: Out of memory\n");
    }
    // 0x80013598
    *(int32_t *)(v2 + 72) = 0xbaabaa;
    threadlistnode_init((int32_t *)(v2 + 60), (int32_t *)v1);
    return (int32_t *)v1;
}