thread * fakethread_create(char *name)

{
  thread *t;
  char *pcVar1;
  
  t = (thread *)kmalloc(100);
  if (t == (thread *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("threadlisttest: Out of memory\n");
  }
  bzero(t,100);
  pcVar1 = kstrdup(name);
  t->t_name = pcVar1;
  if (pcVar1 == (char *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("threadlisttest: Out of memory\n");
  }
  t->t_stack = (void *)0xbaabaa;
  threadlistnode_init(&t->t_listnode,t);
  return t;
}