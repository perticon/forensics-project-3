read_ctl_register(struct lamebus_softc *lb, uint32_t offset)
{
	/* Note that lb might be NULL on some platforms in some contexts. */
	return read_cfg_register(lb, LB_CONTROLLER_SLOT, offset);
}