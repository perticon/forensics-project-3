void timespec_add(int32_t * ts1, int32_t * ts2, int32_t * ret) {
    int32_t v1 = (int32_t)ret;
    int32_t v2 = (int32_t)ts2;
    int32_t v3 = (int32_t)ts1;
    uint32_t v4 = *(int32_t *)(v2 + 8) + *(int32_t *)(v3 + 8); // 0x8000b6ac
    int32_t * v5 = (int32_t *)(v1 + 8); // 0x8000b6b0
    *v5 = v4;
    uint32_t v6 = *(int32_t *)(v3 + 4); // 0x8000b6b8
    uint32_t v7 = *(int32_t *)(v2 + 4) + v6; // 0x8000b6c8
    int32_t v8 = v2 + v3 + (int32_t)(v7 < v6); // 0x8000b6d4
    *ret = v8;
    int32_t * v9 = (int32_t *)(v1 + 4); // 0x8000b6ec
    *v9 = v7;
    if (v4 >= 0x3b9aca00) {
        // 0x8000b6f0
        *v5 = v4 - 0x3b9aca00;
        *ret = v8 + (int32_t)(v7 == -1);
        *v9 = v7 + 1;
    }
}