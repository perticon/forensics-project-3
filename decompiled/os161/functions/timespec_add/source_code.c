timespec_add(const struct timespec *ts1,
	     const struct timespec *ts2,
	     struct timespec *ret)
{
	ret->tv_nsec = ts1->tv_nsec + ts2->tv_nsec;
	ret->tv_sec = ts1->tv_sec + ts2->tv_sec;
	if (ret->tv_nsec >= 1000000000) {
		ret->tv_nsec -= 1000000000;
		ret->tv_sec += 1;
	}
}