void timespec_add(timespec *ts1,timespec *ts2,timespec *ret)

{
  uint uVar1;
  int iVar2;
  uint uVar3;
  int iVar4;
  
  iVar4 = ts1->tv_nsec + ts2->tv_nsec;
  ret->tv_nsec = iVar4;
  uVar1 = *(uint *)((int)&ts1->tv_sec + 4);
  uVar3 = uVar1 + *(int *)((int)&ts2->tv_sec + 4);
  iVar2 = (uint)(uVar3 < uVar1) + *(int *)&ts1->tv_sec + *(int *)&ts2->tv_sec;
  *(int *)&ret->tv_sec = iVar2;
  *(uint *)((int)&ret->tv_sec + 4) = uVar3;
  if (999999999 < iVar4) {
    ret->tv_nsec = iVar4 + -1000000000;
    *(uint *)&ret->tv_sec = (uint)(uVar3 + 1 < uVar3) + iVar2;
    *(uint *)((int)&ret->tv_sec + 4) = uVar3 + 1;
  }
  return;
}