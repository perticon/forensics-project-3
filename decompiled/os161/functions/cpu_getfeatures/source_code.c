cpu_getfeatures(void)
{
	uint32_t features;

	__asm volatile(".set push;"		/* save assembler mode */
		       ".set mips32;"		/* allow mips32 instructions */
		       "mfc0 %0,$15,1;"		/* get cop0 reg 15 sel 1 */
		       ".set pop"		/* restore assembler mode */
		       : "=r" (features));
	return features;
}