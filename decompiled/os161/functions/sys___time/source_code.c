sys___time(userptr_t user_seconds_ptr, userptr_t user_nanoseconds_ptr)
{
	struct timespec ts;
	int result;

	gettime(&ts);

	result = copyout(&ts.tv_sec, user_seconds_ptr, sizeof(ts.tv_sec));
	if (result) {
		return result;
	}

	result = copyout(&ts.tv_nsec, user_nanoseconds_ptr,
			 sizeof(ts.tv_nsec));
	if (result) {
		return result;
	}

	return 0;
}