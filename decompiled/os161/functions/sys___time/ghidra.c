int sys___time(userptr_t user_seconds_ptr,userptr_t user_nanoseconds_ptr)

{
  int iVar1;
  timespec ts;
  
  gettime(&ts);
  iVar1 = copyout(&ts,user_seconds_ptr,8);
  if (iVar1 == 0) {
    iVar1 = copyout(&ts.tv_nsec,user_nanoseconds_ptr,4);
  }
  return iVar1;
}