int32_t sys___time(int32_t * user_seconds_ptr, int32_t * user_nanoseconds_ptr) {
    // 0x8000e070
    int32_t ts; // bp-32, 0x8000e070
    gettime(&ts);
    int32_t v1 = copyout(&ts, user_seconds_ptr, 8); // 0x8000e09c
    int32_t result = v1; // 0x8000e0a4
    if (v1 == 0) {
        // 0x8000e0a8
        int32_t v2; // bp-24, 0x8000e070
        result = copyout(&v2, user_nanoseconds_ptr, 4);
    }
    // 0x8000e0b4
    return result;
}