vopfail_lookup_notdir(struct vnode *vn, char *path, struct vnode **result)
{
	(void)vn;
	(void)path;
	(void)result;
	return ENOTDIR;
}