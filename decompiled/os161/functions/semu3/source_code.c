semu3(int nargs, char **args)
{
	(void)nargs; (void)args;

	kprintf("This should assert that sem != NULL\n");
	sem_destroy(NULL);
	panic("semu3: sem_destroy accepted a null semaphore\n");
}