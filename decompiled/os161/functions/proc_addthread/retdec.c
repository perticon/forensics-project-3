int32_t proc_addthread(int32_t * proc, int32_t * t) {
    int32_t * v1 = (int32_t *)((int32_t)t + 84); // 0x8000cd84
    int32_t v2 = (int32_t)proc; // 0x8000cd90
    if (*v1 != 0) {
        // 0x8000cd94
        badassert("t->t_proc == NULL", "../../proc/proc.c", 367, "proc_addthread");
        v2 = (int32_t)"t->t_proc == NULL";
    }
    // 0x8000cdb4
    spinlock_acquire((int32_t *)"c == NULL");
    *(int32_t *)"_proc == NULL" = (int32_t)"proc == NULL";
    spinlock_release((int32_t *)"c == NULL");
    int32_t v3 = splx(1); // 0x8000cde0
    *v1 = v2;
    splx(v3);
    return 0;
}