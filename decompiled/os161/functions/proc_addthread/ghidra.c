int proc_addthread(proc *proc,thread *t)

{
  int spl;
  char *pcVar1;
  
  if (t->t_proc != (proc *)0x0) {
    pcVar1 = "t->t_proc == NULL";
    badassert("t->t_proc == NULL","../../proc/proc.c",0x16f,"proc_addthread");
    proc = (proc *)pcVar1;
  }
  spinlock_acquire(&proc->p_lock);
  proc->p_numthreads = proc->p_numthreads + 1;
  spinlock_release(&proc->p_lock);
  spl = splx(1);
  t->t_proc = proc;
  splx(spl);
  return 0;
}