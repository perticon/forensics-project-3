menu_execute(char *line, int isargs)
{
	char *command;
	char *context;
	int result;

	for (command = strtok_r(line, ";", &context);
		 command != NULL;
		 command = strtok_r(NULL, ";", &context))
	{

		if (isargs)
		{
			kprintf("OS/161 kernel: %s\n", command);
		}

		result = cmd_dispatch(command);
		if (result)
		{
			kprintf("Menu command failed: %s\n", strerror(result));
			if (isargs)
			{
				panic("Failure processing kernel arguments\n");
			}
		}
	}
}