void menu_execute(char * line, int32_t isargs) {
    // 0x8000c134
    char * context; // bp-32, 0x8000c134
    char * v1 = strtok_r(line, ";", &context); // 0x8000c160
    if (v1 == NULL) {
        // 0x8000c1e4
        return;
    }
    // 0x8000c184
    if (isargs != 0) {
        // 0x8000c18c
        kprintf("OS/161 kernel: %s\n", v1);
    }
    int32_t v2 = cmd_dispatch(v1); // 0x8000c198
    if (v2 != 0) {
        // 0x8000c1a4
        kprintf("Menu command failed: %s\n", strerror(v2));
        if (isargs != 0) {
            // 0x8000c1c0
            panic("Failure processing kernel arguments\n");
        }
    }
    char * v3 = strtok_r(NULL, ";", &context); // 0x8000c1d4
    while (v3 != NULL) {
        char * v4 = v3;
        if (isargs != 0) {
            // 0x8000c18c
            kprintf("OS/161 kernel: %s\n", v4);
        }
        // 0x8000c194
        v2 = cmd_dispatch(v4);
        if (v2 != 0) {
            // 0x8000c1a4
            kprintf("Menu command failed: %s\n", strerror(v2));
            if (isargs != 0) {
                // 0x8000c1c0
                panic("Failure processing kernel arguments\n");
            }
        }
        // 0x8000c1cc
        v3 = strtok_r(NULL, ";", &context);
    }
}