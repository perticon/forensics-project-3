void menu_execute(char *line,int isargs)

{
  char *pcVar1;
  int errcode;
  char *context;
  
  pcVar1 = strtok_r(line,";",&context);
  do {
    if (pcVar1 == (char *)0x0) {
      return;
    }
    if (isargs != 0) {
      kprintf("OS/161 kernel: %s\n",pcVar1);
    }
    errcode = cmd_dispatch(pcVar1);
    if (errcode != 0) {
      pcVar1 = strerror(errcode);
      kprintf("Menu command failed: %s\n",pcVar1);
      if (isargs != 0) {
                    /* WARNING: Subroutine does not return */
        panic("Failure processing kernel arguments\n");
      }
    }
    pcVar1 = strtok_r((char *)0x0,";",&context);
  } while( true );
}