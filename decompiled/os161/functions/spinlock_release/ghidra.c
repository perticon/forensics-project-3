void spinlock_release(spinlock *splk)

{
  cpu **ppcVar1;
  cpu *pcVar2;
  int unaff_s7;
  
  if (unaff_s7 != 0) {
    ppcVar1 = *(cpu ***)(unaff_s7 + 0x50);
    if (splk->splk_holder != *ppcVar1) {
      badassert("splk->splk_holder == curcpu->c_self","../../thread/spinlock.c",0x84,
                "spinlock_release");
    }
    pcVar2 = (cpu *)((int)&ppcVar1[0xc][-1].c_ipi_lock.splk_holder + 3);
    if (ppcVar1[0xc] == (cpu *)0x0) {
      badassert("curcpu->c_spinlocks > 0","../../thread/spinlock.c",0x85,"spinlock_release");
    }
    ppcVar1[0xc] = pcVar2;
  }
  splk->splk_holder = (cpu *)0x0;
  membar_any_store();
  spinlock_data_set(&splk->splk_lock,0);
  spllower(1,0);
  return;
}