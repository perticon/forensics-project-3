spinlock_release(struct spinlock *splk)
{
	/* this must work before curcpu initialization */
	if (CURCPU_EXISTS()) {
		KASSERT(splk->splk_holder == curcpu->c_self);
		KASSERT(curcpu->c_spinlocks > 0);
		curcpu->c_spinlocks--;
		HANGMAN_RELEASE(&curcpu->c_hangman, &splk->splk_hangman);
	}

	splk->splk_holder = NULL;
	membar_any_store();
	spinlock_data_set(&splk->splk_lock, 0);
	spllower(IPL_HIGH, IPL_NONE);
}