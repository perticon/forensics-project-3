void spinlock_release(int32_t * splk) {
    int32_t * v1 = (int32_t *)((int32_t)splk + 4);
    int32_t v2; // 0x80015254
    if (v2 == 0) {
        // 0x800152d4
        *v1 = 0;
        membar_any_store();
        spinlock_data_set(splk, 0);
        spllower(1, 0);
        return;
    }
    int32_t v3 = *(int32_t *)(v2 + 80); // 0x8001526c
    int32_t v4 = v3; // 0x80015280
    if (*v1 != *(int32_t *)v3) {
        // 0x80015284
        badassert("splk->splk_holder == curcpu->c_self", "../../thread/spinlock.c", 132, "spinlock_release");
        v4 = &g41;
    }
    int32_t v5 = *(int32_t *)(v4 + 48); // 0x800152a0
    int32_t v6 = v4; // 0x800152ac
    if (v5 == 0) {
        // 0x800152b0
        badassert("curcpu->c_spinlocks > 0", "../../thread/spinlock.c", 133, "spinlock_release");
        v6 = &g41;
    }
    // 0x800152d0
    *(int32_t *)(v6 + 48) = v5 - 1;
    // 0x800152d4
    *v1 = 0;
    membar_any_store();
    spinlock_data_set(splk, 0);
    spllower(1, 0);
}