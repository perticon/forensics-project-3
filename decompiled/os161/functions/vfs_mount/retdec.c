int32_t vfs_mount(char * devname, char * data, int32_t (*mountfunc)(char *, int32_t *, int32_t **)) {
    // 0x800193a4
    vfs_biglock_acquire();
    int32_t * kd; // bp-24, 0x800193a4
    int32_t result = findmount(devname, &kd); // 0x800193d0
    if (result != 0) {
        // 0x800193dc
        vfs_biglock_release();
        // 0x80019548
        return result;
    }
    int32_t v1 = (int32_t)kd; // 0x800193ec
    if (*(int32_t *)(v1 + 16) != 0) {
        // 0x80019404
        vfs_biglock_release();
        // 0x80019548
        return 27;
    }
    int32_t result2 = v1; // 0x80019420
    if (*(int32_t *)(v1 + 4) == 0) {
        // 0x80019424
        badassert("kd->kd_rawname != NULL", "../../vfs/vfslist.c", 558, "vfs_mount");
        result2 = &g41;
    }
    // 0x80019440
    if (*(int32_t *)(result2 + 8) == 0) {
        // 0x80019470
        badassert("kd->kd_device != NULL", "../../vfs/vfslist.c", 559, "vfs_mount");
        // 0x80019480
        vfs_biglock_release();
        // 0x80019548
        return &g41;
    }
    // 0x80019470
    if (result2 != 0) {
        // 0x80019480
        vfs_biglock_release();
        // 0x80019548
        return result2;
    }
    int32_t v2 = -1; // 0x8001949c
    int32_t * v3; // 0x800193a4
    int32_t v4 = (int32_t)v3; // 0x8001949c
    if (v3 == NULL) {
        // 0x800194a0
        badassert("fs != NULL", "../../vfs/vfslist.c", 567, "vfs_mount");
        v2 = &g41;
        v4 = (int32_t)"fs != NULL";
    }
    int32_t v5 = v4; // 0x800194c4
    if (v4 == v2) {
        // 0x800194c8
        badassert("fs != SWAP_FS", "../../vfs/vfslist.c", 568, "vfs_mount");
        v5 = (int32_t)"fs != SWAP_FS";
    }
    // 0x800194e4
    *(int32_t *)((int32_t)kd + 16) = v5;
    int32_t v6 = *(int32_t *)(*(int32_t *)(v5 + 4) + 4); // 0x800194f8
    int32_t v7 = *kd;
    int32_t v8 = v6 == 0 ? v7 : v6;
    kprintf("vfs: Mounted %s: on %s\n", (char *)v8, (char *)v7);
    vfs_biglock_release();
    // 0x80019548
    return 0;
}