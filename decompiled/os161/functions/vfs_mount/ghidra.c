int vfs_mount(char *devname,void *data,anon_subr_int_void_ptr_device_ptr_fs_ptr_ptr *mountfunc)

{
  int iVar1;
  knowndev *pkVar2;
  fs *pfVar3;
  char *pcVar4;
  char *pcVar5;
  knowndev *kd;
  fs *fs;
  
  vfs_biglock_acquire();
  iVar1 = findmount(devname,&kd);
  if (iVar1 == 0) {
    if (kd->kd_fs == (fs *)0x0) {
      pkVar2 = kd;
      if (kd->kd_rawname == (char *)0x0) {
        badassert("kd->kd_rawname != NULL","../../vfs/vfslist.c",0x22e,"vfs_mount");
      }
      pcVar5 = (char *)pkVar2->kd_device;
      if ((device *)pcVar5 == (device *)0x0) {
        pcVar4 = "kd->kd_device != NULL";
        pcVar5 = "../../vfs/vfslist.c";
        badassert("kd->kd_device != NULL","../../vfs/vfslist.c",0x22f,"vfs_mount");
        data = pcVar4;
      }
      iVar1 = (*mountfunc)(data,(device *)pcVar5,&fs);
      if (iVar1 == 0) {
        pfVar3 = (fs *)0xffffffff;
        pcVar5 = (char *)fs;
        if (fs == (fs *)0x0) {
          pcVar5 = "fs != NULL";
          badassert("fs != NULL","../../vfs/vfslist.c",0x237,"vfs_mount");
        }
        if ((fs *)pcVar5 == pfVar3) {
          pcVar5 = "fs != SWAP_FS";
          badassert("fs != SWAP_FS","../../vfs/vfslist.c",0x238,"vfs_mount");
        }
        kd->kd_fs = (fs *)pcVar5;
        pcVar5 = (*(*(fs_ops **)((int)pcVar5 + 4))->fsop_getvolname)((fs *)pcVar5);
        if (pcVar5 == (char *)0x0) {
          pcVar5 = kd->kd_name;
        }
        kprintf("vfs: Mounted %s: on %s\n",pcVar5,kd->kd_name);
        vfs_biglock_release();
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
    else {
      vfs_biglock_release();
      iVar1 = 0x1b;
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}