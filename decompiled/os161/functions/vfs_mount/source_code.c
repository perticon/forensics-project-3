vfs_mount(const char *devname, void *data,
	  int (*mountfunc)(void *data, struct device *, struct fs **ret))
{
	const char *volname;
	struct knowndev *kd;
	struct fs *fs;
	int result;

	vfs_biglock_acquire();

	result = findmount(devname, &kd);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	if (kd->kd_fs != NULL) {
		vfs_biglock_release();
		return EBUSY;
	}
	KASSERT(kd->kd_rawname != NULL);
	KASSERT(kd->kd_device != NULL);

	result = mountfunc(data, kd->kd_device, &fs);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	KASSERT(fs != NULL);
	KASSERT(fs != SWAP_FS); 

	kd->kd_fs = fs;

	volname = FSOP_GETVOLNAME(fs);
	kprintf("vfs: Mounted %s: on %s\n",
		volname ? volname : kd->kd_name, kd->kd_name);

	vfs_biglock_release();
	return 0;
}