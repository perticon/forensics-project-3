randmax(void)
{
	if (the_random==NULL) {
		panic("No random device\n");
	}
	return the_random->rs_randmax(the_random->rs_devdata);
}