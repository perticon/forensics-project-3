void ram_bootstrap(void) {
    uint32_t v1 = mainbus_ramsize(); // 0x8001f4ec
    int32_t v2 = v1 < 0x20000000 ? v1 : 0x20000000;
    lastpaddr = v2;
    firstpaddr = -firstfree;
    kprintf("%uk physical memory available\n", (v2 + firstfree) / 1024);
}