void ram_bootstrap(void)

{
  lastpaddr = mainbus_ramsize();
  if (0x20000000 < lastpaddr) {
    lastpaddr = 0x20000000;
  }
  firstpaddr = firstfree + 0x80000000;
  kprintf("%uk physical memory available\n",lastpaddr - firstpaddr >> 10);
  return;
}