void dowritestress2(char * filesys) {
    char name[32]; // bp-64, 0x800104d4
    // 0x800104d4
    init_threadsem();
    kprintf("*** Starting fs write stress test 2 on %s:\n", filesys);
    fstest_makename(name, 32, filesys, (char *)&g10);
    int32_t * vn; // bp-32, 0x800104d4
    int32_t v1 = vfs_open(name, 21, 436, &vn); // 0x8001052c
    if (v1 != 0) {
        // 0x80010538
        kprintf("Could not create test file: %s\n", strerror(v1));
        kprintf("*** Test failed\n");
        // 0x80010640
        return;
    }
    // 0x80010564
    vfs_close(vn);
    int32_t v2 = 0;
    int32_t v3 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2); // 0x80010594
    int32_t v4 = v3; // 0x8001059c
    int32_t v5; // 0x80010594
    if (v3 != 0) {
        panic("writestress2: thread_fork failed: %s\n", strerror(v4));
        v5 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2);
        v4 = v5;
        while (v5 != 0) {
            // 0x800105a0
            panic("writestress2: thread_fork failed: %s\n", strerror(v4));
            v5 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2);
            v4 = v5;
        }
    }
    int32_t v6 = v2 + 1; // 0x8001059c
    while (v6 < 12) {
        // 0x80010584
        v2 = v6;
        v3 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2);
        v4 = v3;
        if (v3 != 0) {
            panic("writestress2: thread_fork failed: %s\n", strerror(v4));
            v5 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2);
            v4 = v5;
            while (v5 != 0) {
                // 0x800105a0
                panic("writestress2: thread_fork failed: %s\n", strerror(v4));
                v5 = thread_fork("writestress2", NULL, (void (*)(char *, int32_t))((int32_t)&g2 - 968), filesys, v2);
                v4 = v5;
            }
        }
        // 0x800105bc
        v6 = v2 + 1;
    }
    int32_t v7 = 1; // 0x800105dc
    P(threadsem);
    int32_t v8 = v7; // 0x800105e8
    while (v7 < 12) {
        // 0x800105d4
        v7 = v8 + 1;
        P(threadsem);
        v8 = v7;
    }
    // 0x800105ec
    if (fstest_read(filesys, (char *)&g10) != 0) {
        // 0x80010600
        kprintf("*** Test failed\n");
        // 0x80010640
        return;
    }
    // 0x80010614
    if (fstest_remove(filesys, (char *)&g10) != 0) {
        // 0x80010628
        kprintf("*** Test failed\n");
    }
    // 0x80010634
    kprintf("*** fs write stress test 2 done\n");
}