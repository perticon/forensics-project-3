dowritestress2(const char *filesys)
{
	int i, err;
	char name[32];
	struct vnode *vn;

	init_threadsem();

	kprintf("*** Starting fs write stress test 2 on %s:\n", filesys);

	/* Create and truncate test file */
	fstest_makename(name, sizeof(name), filesys, "");
	err = vfs_open(name, O_WRONLY|O_CREAT|O_TRUNC, 0664, &vn);
	if (err) {
		kprintf("Could not create test file: %s\n", strerror(err));
		kprintf("*** Test failed\n");
		return;
	}
	vfs_close(vn);

	for (i=0; i<NTHREADS; i++) {
		err = thread_fork("writestress2", NULL,
				  writestress2_thread, (char *)filesys, i);
		if (err) {
			panic("writestress2: thread_fork failed: %s\n",
			      strerror(err));
		}
	}

	for (i=0; i<NTHREADS; i++) {
		P(threadsem);
	}

	if (fstest_read(filesys, "")) {
		kprintf("*** Test failed\n");
		return;
	}

	if (fstest_remove(filesys, "")) {
		kprintf("*** Test failed\n");
	}


	kprintf("*** fs write stress test 2 done\n");
}