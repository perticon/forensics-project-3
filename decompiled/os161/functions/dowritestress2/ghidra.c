void dowritestress2(char *filesys)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  char name [32];
  vnode *vn;
  
  init_threadsem();
  kprintf("*** Starting fs write stress test 2 on %s:\n",filesys);
  fstest_makename(name,0x20,filesys,"");
  iVar1 = vfs_open(name,0x15,0x1b4,&vn);
  if (iVar1 == 0) {
    data2 = 0;
    vfs_close(vn);
    while ((int)data2 < 0xc) {
      iVar1 = thread_fork("writestress2",(proc *)0x0,writestress2_thread,filesys,data2);
      data2 = data2 + 1;
      if (iVar1 != 0) {
        pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
        panic("writestress2: thread_fork failed: %s\n",pcVar2);
      }
    }
    for (iVar1 = 0; iVar1 < 0xc; iVar1 = iVar1 + 1) {
      P(threadsem);
    }
    iVar1 = fstest_read(filesys,"");
    if (iVar1 == 0) {
      iVar1 = fstest_remove(filesys,"");
      if (iVar1 != 0) {
        kprintf("*** Test failed\n");
      }
      kprintf("*** fs write stress test 2 done\n");
    }
    else {
      kprintf("*** Test failed\n");
    }
  }
  else {
    pcVar2 = strerror(iVar1);
    kprintf("Could not create test file: %s\n",pcVar2);
    kprintf("*** Test failed\n");
  }
  return;
}