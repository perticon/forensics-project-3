void enter_forked_process(trapframe *tf)

{
  trapframe *ptVar1;
  trapframe *ptVar2;
  trapframe *ptVar3;
  trapframe *ptVar4;
  uint32_t uVar5;
  uint32_t uVar6;
  uint32_t uVar7;
  trapframe forkedTf;
  
  ptVar3 = tf;
  ptVar1 = &forkedTf;
  do {
    ptVar4 = ptVar1;
    ptVar2 = ptVar3;
    uVar7 = ptVar2->tf_status;
    uVar6 = ptVar2->tf_cause;
    uVar5 = ptVar2->tf_lo;
    ptVar4->tf_vaddr = ptVar2->tf_vaddr;
    ptVar4->tf_status = uVar7;
    ptVar4->tf_cause = uVar6;
    ptVar4->tf_lo = uVar5;
    ptVar3 = (trapframe *)&ptVar2->tf_hi;
    ptVar1 = (trapframe *)&ptVar4->tf_hi;
  } while (ptVar3 != (trapframe *)&tf->tf_sp);
  uVar6 = ptVar2->tf_ra;
  uVar5 = ptVar2->tf_at;
  ptVar4->tf_hi = *(uint32_t *)ptVar3;
  ptVar4->tf_ra = uVar6;
  ptVar4->tf_at = uVar5;
  forkedTf.tf_v0 = 0;
  forkedTf.tf_a3 = 0;
  forkedTf.tf_epc = forkedTf.tf_epc + 4;
  as_activate();
  mips_usermode(&forkedTf);
  setCopReg(0,Status,Status | 1,0);
  setCopReg(0,Status,Status & 0xfffffffe,0);
  return;
}