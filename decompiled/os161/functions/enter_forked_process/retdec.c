void enter_forked_process(int32_t * tf) {
    int32_t v1 = (int32_t)tf;
    int32_t forkedTf; // bp-152, 0x8001f13c
    int32_t v2 = &forkedTf;
    int32_t v3 = v1;
    *(int32_t *)v2 = *(int32_t *)v3;
    *(int32_t *)(v2 + 4) = *(int32_t *)(v3 + 4);
    *(int32_t *)(v2 + 8) = *(int32_t *)(v3 + 8);
    *(int32_t *)(v2 + 12) = *(int32_t *)(v3 + 12);
    int32_t v4 = v3 + 16; // 0x8001f170
    int32_t v5 = v2 + 16; // 0x8001f178
    while (v3 != v1 + 112) {
        // 0x8001f150
        v2 = v5;
        v3 = v4;
        *(int32_t *)v2 = *(int32_t *)v3;
        *(int32_t *)(v2 + 4) = *(int32_t *)(v3 + 4);
        *(int32_t *)(v2 + 8) = *(int32_t *)(v3 + 8);
        *(int32_t *)(v2 + 12) = *(int32_t *)(v3 + 12);
        v4 = v3 + 16;
        v5 = v2 + 16;
    }
    // 0x8001f17c
    *(int32_t *)v5 = *(int32_t *)v4;
    *(int32_t *)(v2 + 20) = *(int32_t *)(v1 + 132);
    *(int32_t *)(v2 + 24) = *(int32_t *)(v1 + 136);
    as_activate();
    mips_usermode(&forkedTf);
}