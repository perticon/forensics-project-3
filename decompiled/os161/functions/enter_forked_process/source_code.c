enter_forked_process(struct trapframe *tf)
{
#if OPT_FORK
	// Duplicate frame so it's on stack
	struct trapframe forkedTf = *tf; // copy trap frame onto kernel stack

	forkedTf.tf_v0 = 0; // return value is 0
        forkedTf.tf_a3 = 0; // return with success

	forkedTf.tf_epc += 4; // return to next instruction
	
	as_activate();


	mips_usermode(&forkedTf);
	
#else
	(void)tf;
#endif

}