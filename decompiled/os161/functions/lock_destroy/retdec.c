void lock_destroy(int32_t * lock) {
    if (lock == NULL) {
        // 0x8001588c
        badassert("lock != NULL", "../../thread/synch.c", 182, "lock_destroy");
    }
    // 0x800158ac
    spinlock_cleanup((int32_t *)"NULL");
    int32_t v1 = *(int32_t *)((int32_t)lock + 4); // 0x800158b4
    sem_destroy((int32_t *)v1);
    kfree((char *)v1);
    kfree((char *)lock);
}