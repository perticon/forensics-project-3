lock_destroy(struct lock *lock)
{
        KASSERT(lock != NULL);

        // add stuff here as needed
#if OPT_SYNCH
	spinlock_cleanup(&lock->lk_lock);
#if USE_SEMAPHORE_FOR_LOCK
        sem_destroy(lock->lk_sem);
#else
	wchan_destroy(lock->lk_wchan);
#endif
#endif
        kfree(lock->lk_name);
        kfree(lock);
}