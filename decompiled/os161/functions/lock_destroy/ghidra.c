void lock_destroy(lock *lock)

{
  char *pcVar1;
  
  pcVar1 = (char *)lock;
  if (lock == (lock *)0x0) {
    pcVar1 = "lock != NULL";
    badassert("lock != NULL","../../thread/synch.c",0xb6,"lock_destroy");
  }
  spinlock_cleanup((spinlock *)((int)pcVar1 + 8));
  sem_destroy(lock->lk_sem);
  kfree(lock->lk_name);
  kfree(lock);
  return;
}