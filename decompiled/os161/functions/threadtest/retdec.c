int32_t threadtest(int32_t nargs, char ** args) {
    // 0x800145a4
    init_sem();
    kprintf("Starting thread test...\n");
    runthreads(1);
    kprintf("\nThread test done.\n");
    return 0;
}