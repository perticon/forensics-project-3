char * mkrawname(char * name) {
    char * result = kmalloc(strlen(name) + 4); // 0x8001840c
    if (result != NULL) {
        // 0x80018418
        strcpy(result, name);
        strcat(result, "raw");
    }
    // 0x80018440
    return result;
}