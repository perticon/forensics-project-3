mkrawname(const char *name)
{
	char *s = kmalloc(strlen(name)+3+1);
	if (!s) {
		return NULL;
	}
	strcpy(s, name);
	strcat(s, "raw");
	return s;
}