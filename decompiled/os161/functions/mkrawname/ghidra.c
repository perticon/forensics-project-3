char * mkrawname(char *name)

{
  size_t sVar1;
  char *dest;
  
  sVar1 = strlen(name);
  dest = (char *)kmalloc(sVar1 + 4);
  if (dest == (char *)0x0) {
    dest = (char *)0x0;
  }
  else {
    strcpy(dest,name);
    strcat(dest,"raw");
  }
  return dest;
}