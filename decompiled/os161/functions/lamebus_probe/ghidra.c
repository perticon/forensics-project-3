int lamebus_probe(lamebus_softc *sc,uint32_t vendorid,uint32_t deviceid,uint32_t lowver,
                 uint32_t *version_ret)

{
  uint32_t uVar1;
  uint uVar2;
  uint32_t uVar3;
  
  spinlock_acquire(&sc->ls_lock);
  uVar2 = 0;
  while( true ) {
    if (0x1f < (int)uVar2) {
      spinlock_release(&sc->ls_lock);
      return -1;
    }
    uVar3 = uVar2 * 0x400;
    if (((((sc->ls_slotsinuse & 1 << (uVar2 & 0x1f)) == 0) &&
         (uVar1 = lamebus_read_register(sc,0x1f,uVar3), vendorid == uVar1)) &&
        (uVar1 = lamebus_read_register(sc,0x1f,uVar3 + 4), deviceid == uVar1)) &&
       (uVar3 = lamebus_read_register(sc,0x1f,uVar3 + 8), lowver <= uVar3)) break;
    uVar2 = uVar2 + 1;
  }
  if (version_ret != (uint32_t *)0x0) {
    *version_ret = uVar3;
  }
  spinlock_release(&sc->ls_lock);
  return uVar2;
}