int32_t lamebus_probe(int32_t * sc, int32_t vendorid, int32_t deviceid, uint32_t lowver, int32_t * version_ret) {
    // 0x80003c78
    spinlock_acquire(sc);
    int32_t v1 = 0;
    int32_t result; // 0x80003c78
    int32_t v2; // 0x80003ccc
    uint32_t v3; // 0x80003d04
    if ((*(int32_t *)((int32_t)sc + 8) & 1 << v1) == 0) {
        // 0x80003cd0
        v2 = 1024 * v1;
        if (lamebus_read_register(sc, 31, v2) == vendorid) {
            // 0x80003ce8
            if (lamebus_read_register(sc, 31, v2 || 4) == deviceid) {
                // 0x80003cfc
                v3 = lamebus_read_register(sc, 31, v2 | 8);
                if (v3 >= lowver) {
                    if (version_ret == NULL) {
                        // 0x80003d28
                        spinlock_release(sc);
                        result = v1;
                        return result;
                    } else {
                        // 0x80003d24
                        *version_ret = v3;
                        // 0x80003d28
                        spinlock_release(sc);
                        result = v1;
                        return result;
                    }
                }
            }
        }
    }
    int32_t v4 = v1 + 1; // 0x80003d38
    while (v4 < 32) {
        // 0x80003cbc
        v1 = v4;
        if ((*(int32_t *)((int32_t)sc + 8) & 1 << v1) == 0) {
            // 0x80003cd0
            v2 = 1024 * v1;
            if (lamebus_read_register(sc, 31, v2) == vendorid) {
                // 0x80003ce8
                if (lamebus_read_register(sc, 31, v2 || 4) == deviceid) {
                    // 0x80003cfc
                    v3 = lamebus_read_register(sc, 31, v2 | 8);
                    if (v3 >= lowver) {
                        if (version_ret == NULL) {
                            // 0x80003d28
                            spinlock_release(sc);
                            result = v1;
                            return result;
                        } else {
                            // 0x80003d24
                            *version_ret = v3;
                            // 0x80003d28
                            spinlock_release(sc);
                            result = v1;
                            return result;
                        }
                    }
                }
            }
        }
        // 0x80003d38
        v4 = v1 + 1;
    }
    // 0x80003d48
    spinlock_release(sc);
    result = -1;
  lab_0x80003d54:
    // 0x80003d54
    return result;
}