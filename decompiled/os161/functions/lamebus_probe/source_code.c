lamebus_probe(struct lamebus_softc *sc,
	      uint32_t vendorid, uint32_t deviceid,
	      uint32_t lowver, uint32_t *version_ret)
{
	int slot;
	uint32_t val;

	/*
	 * Because the slot information in sc is used when dispatching
	 * interrupts, disable interrupts while working with it.
	 */

	spinlock_acquire(&sc->ls_lock);

	for (slot=0; slot<LB_NSLOTS; slot++) {
		if (sc->ls_slotsinuse & (1<<slot)) {
			/* Slot already in use; skip */
			continue;
		}

		val = read_cfg_register(sc, slot, CFGREG_VID);
		if (val!=vendorid) {
			/* Wrong vendor id */
			continue;
		}

		val = read_cfg_register(sc, slot, CFGREG_DID);
		if (val != deviceid) {
			/* Wrong device id */
			continue;
		}

		val = read_cfg_register(sc, slot, CFGREG_DRL);
		if (val < lowver) {
			/* Unsupported device revision */
			continue;
		}
		if (version_ret != NULL) {
			*version_ret = val;
		}

		/* Found something */

		spinlock_release(&sc->ls_lock);
		return slot;
	}

	/* Found nothing */

	spinlock_release(&sc->ls_lock);
	return -1;
}