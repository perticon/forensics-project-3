int sys_read(int fd,userptr_t buf_ptr,size_t size)

{
  int iVar1;
  int iVar2;
  
  if (fd == 0) {
    for (iVar2 = 0; iVar2 < (int)size; iVar2 = iVar2 + 1) {
      iVar1 = getch();
      buf_ptr[iVar2]._dummy = (char)iVar1;
      if ((char)iVar1 < '\0') {
        return iVar2;
      }
    }
  }
  else {
    size = file_read(fd,buf_ptr,size);
  }
  return size;
}