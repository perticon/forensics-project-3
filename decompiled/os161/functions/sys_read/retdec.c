int32_t sys_read(int32_t fd, int32_t * buf_ptr, uint32_t size) {
    if (fd != 0) {
        // 0x8000d9d4
        return file_read(fd, buf_ptr, size);
    }
    // 0x8000d9bc
    if (size <= 0) {
        // 0x8000d9d4
        return size;
    }
    int32_t v1 = 0; // 0x8000d9c4
    int32_t v2 = getch(); // 0x8000d99c
    *(char *)(v1 + (int32_t)buf_ptr) = (char)v2;
    int32_t result = v1; // 0x8000d9ac
    while ((v2 & 128) == 0) {
        // 0x8000d9b0
        v1++;
        result = size;
        if (v1 >= size) {
            // break -> 0x8000d9d4
            break;
        }
        v2 = getch();
        *(char *)(v1 + (int32_t)buf_ptr) = (char)v2;
        result = v1;
    }
    // 0x8000d9d4
    return result;
}