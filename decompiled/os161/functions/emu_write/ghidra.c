int emu_write(emu_softc *sc,uint32_t handle,uint32_t len,uio *uio)

{
  int iVar1;
  char *pcVar2;
  char *pcVar3;
  char *pcVar4;
  
  pcVar4 = (char *)uio;
  if (uio->uio_rw != UIO_WRITE) {
    pcVar2 = "uio->uio_rw == UIO_WRITE";
    pcVar3 = s_______dev_lamebus_emu_c_80022750;
    len = 0x14e;
    pcVar4 = "emu_write";
    badassert("uio->uio_rw == UIO_WRITE",s_______dev_lamebus_emu_c_80022750,0x14e,"emu_write");
    sc = (emu_softc *)pcVar2;
    handle = (uint32_t)pcVar3;
  }
  if (*(int *)((int)pcVar4 + 8) < 1) {
    lock_acquire(sc->e_lock);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,8,len);
    lamebus_write_register
              ((lamebus_softc *)sc->e_busdata,sc->e_buspos,4,
               *(uint32_t *)((int)&uio->uio_offset + 4));
    iVar1 = uiomove(sc->e_iobuf,len,uio);
    SYNC(0);
    if (iVar1 == 0) {
      lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,7);
      iVar1 = emu_waitdone(sc);
    }
    lock_release(sc->e_lock);
  }
  else {
    iVar1 = 0x26;
  }
  return iVar1;
}