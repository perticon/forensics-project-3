void subpage_stats(pageref *pr)

{
  undefined3 extraout_var;
  bool bVar4;
  uint uVar1;
  int iVar2;
  undefined4 *puVar3;
  int in_v1;
  uint uVar5;
  char *pcVar6;
  char *pcVar7;
  undefined4 uVar8;
  uint uVar9;
  uint32_t freemap [8];
  
  bVar4 = spinlock_do_i_hold(&kmalloc_spinlock);
  uVar1 = CONCAT31(extraout_var,bVar4);
  if (uVar1 == 0) {
    badassert("spinlock_do_i_hold(&kmalloc_spinlock)","../../vm/kmalloc.c",0x2f4,"subpage_stats");
    do {
      *(undefined4 *)((int)freemap + in_v1) = 0;
      uVar1 = uVar1 + 1;
LAB_8001bd38:
      in_v1 = uVar1 << 2;
    } while (uVar1 < 8);
    pcVar7 = (char *)(pr->pageaddr_and_blocktype & 0xfffff000);
    uVar1 = pr->pageaddr_and_blocktype & 0xfff;
    iVar2 = uVar1 << 2;
    if (7 < uVar1) {
      pcVar7 = "../../vm/kmalloc.c";
      badassert("blktype >= 0 && blktype < NSIZES","../../vm/kmalloc.c",0x2fd,"subpage_stats");
    }
    uVar1 = *(uint *)((int)sizes + iVar2);
    uVar9 = 0x1000 / uVar1;
    if (uVar1 == 0) {
      trap(0x1c00);
    }
    uVar5 = 0xffff;
    if (0x100 < uVar9) {
      pcVar7 = "../../vm/kmalloc.c";
      uVar1 = 0x301;
      badassert("n <= 32 * ARRAYCOUNT(freemap)","../../vm/kmalloc.c",0x301,"subpage_stats");
    }
    puVar3 = (undefined4 *)(pcVar7 + pr->freelist_offset);
    if (pr->freelist_offset != uVar5) {
      iVar2 = 1;
      for (; puVar3 != (undefined4 *)0x0; puVar3 = (undefined4 *)*puVar3) {
        uVar5 = (uint)((int)puVar3 - (int)pcVar7) / uVar1;
        if (uVar1 == 0) {
          trap(0x1c00);
        }
        pcVar6 = (char *)freemap;
        if (uVar9 <= uVar5) {
          pcVar6 = s_index_n_80027c58;
          pcVar7 = "../../vm/kmalloc.c";
          uVar1 = 0x30a;
          badassert(s_index_n_80027c58,"../../vm/kmalloc.c",0x30a,"subpage_stats");
        }
        *(uint *)((int)pcVar6 + (uVar5 >> 5) * 4) =
             *(uint *)((int)pcVar6 + (uVar5 >> 5) * 4) | iVar2 << (uVar5 & 0x1f);
      }
    }
    kprintf("at 0x%08lx: size %-4lu  %u/%u free\n");
    kprintf("   ");
    for (uVar1 = 0; uVar1 < uVar9; uVar1 = uVar1 + 1) {
      uVar8 = 0x2e;
      if ((freemap[uVar1 >> 5] & 1 << (uVar1 & 0x1f)) == 0) {
        uVar8 = 0x2a;
      }
      kprintf("%c",uVar8);
      if (((uVar1 & 0x3f) == 0x3f) && (uVar1 < uVar9 - 1)) {
        kprintf("\n   ");
      }
    }
    kprintf("\n");
    return;
  }
  uVar1 = 0;
  goto LAB_8001bd38;
}