void subpage_stats(int32_t * pr) {
    int32_t freemap[8]; // bp-56, 0x8001bcd4
    bool v1 = spinlock_do_i_hold(&kmalloc_spinlock); // 0x8001bcf8
    int32_t v2 = 0; // 0x8001bd00
    int32_t v3; // 0x8001bcd4
    if (v1) {
        goto lab_0x8001bd38;
    } else {
        // 0x8001bd04
        badassert("spinlock_do_i_hold(&kmalloc_spinlock)", "../../vm/kmalloc.c", 756, "subpage_stats");
        v3 = &g41;
        goto lab_0x8001bd20;
    }
  lab_0x8001bd38:
    // 0x8001bd38
    v3 = v2;
    int32_t v4 = 4 * v2; // 0x8001bd40
    if (v2 >= 8) {
        int32_t v5 = (int32_t)pr;
        uint32_t v6 = *(int32_t *)(v5 + 8); // 0x8001bd44
        uint32_t v7 = v6 % 0x1000; // 0x8001bd50
        int32_t v8 = 4 * v7; // 0x8001bd5c
        int32_t v9 = v6 & -0x1000; // 0x8001bd5c
        if (v7 >= 8) {
            // 0x8001bd60
            badassert("blktype >= 0 && blktype < NSIZES", "../../vm/kmalloc.c", 765, "subpage_stats");
            v8 = &g41;
            v9 = (int32_t)"../../vm/kmalloc.c";
        }
        uint32_t v10 = *(int32_t *)(v8 + (int32_t)&sizes); // 0x8001bd8c
        int32_t v11 = v9; // 0x8001bdb0
        int32_t v12 = v10; // 0x8001bdb0
        if (v10 <= 15) {
            // 0x8001bdb4
            badassert("n <= 32 * ARRAYCOUNT(freemap)", "../../vm/kmalloc.c", 769, "subpage_stats");
            v11 = (int32_t)"../../vm/kmalloc.c";
            v12 = 769;
        }
        uint32_t v13 = 0x1000 / v10; // 0x8001bd98
        uint16_t v14 = *(int16_t *)(v5 + 12); // 0x8001bdd4
        int32_t v15 = v11; // 0x8001bde0
        int32_t v16 = v12; // 0x8001bde0
        if (v14 != -1) {
            int32_t v17 = v11 + (int32_t)v14; // 0x8001bde0
            v15 = v11;
            v16 = v12;
            if (v17 != 0) {
                uint32_t v18 = (v17 - v11) / v12; // 0x8001bdf0
                int32_t v19 = v17; // 0x8001be04
                int32_t v20 = (int32_t)&freemap; // 0x8001be04
                int32_t v21 = v11; // 0x8001be04
                int32_t v22 = v12; // 0x8001be04
                if (v18 >= v13) {
                    // 0x8001be08
                    badassert("index<n", "../../vm/kmalloc.c", 778, "subpage_stats");
                    v19 = &g41;
                    v20 = (int32_t)"index<n";
                    v21 = (int32_t)"../../vm/kmalloc.c";
                    v22 = 778;
                }
                int32_t v23 = v22;
                int32_t v24 = v21;
                int32_t * v25 = (int32_t *)(v20 + 4 * v18 / 32); // 0x8001be38
                *v25 = *v25 | 1 << v18;
                int32_t v26 = *(int32_t *)v19; // 0x8001be48
                v15 = v24;
                v16 = v23;
                while (v26 != 0) {
                    // 0x8001bdec
                    v18 = (v26 - v24) / v23;
                    v19 = v26;
                    v20 = (int32_t)&freemap;
                    v21 = v24;
                    v22 = v23;
                    if (v18 >= v13) {
                        // 0x8001be08
                        badassert("index<n", "../../vm/kmalloc.c", 778, "subpage_stats");
                        v19 = &g41;
                        v20 = (int32_t)"index<n";
                        v21 = (int32_t)"../../vm/kmalloc.c";
                        v22 = 778;
                    }
                    // 0x8001be28
                    v23 = v22;
                    v24 = v21;
                    v25 = (int32_t *)(v20 + 4 * v18 / 32);
                    *v25 = *v25 | 1 << v18;
                    v26 = *(int32_t *)v19;
                    v15 = v24;
                    v16 = v23;
                }
            }
        }
        uint16_t v27 = *(int16_t *)(v5 + 14); // 0x8001be58
        kprintf("at 0x%08lx: size %-4lu  %u/%u free\n", v15, v16, (int32_t)v27, v13);
        kprintf("   ");
        if (v10 >= 0x1001) {
            // 0x8001bef0
            kprintf("\n");
            return;
        }
        uint32_t v28 = 0;
        int32_t v29; // bp-80, 0x8001bcd4
        int32_t v30 = *(int32_t *)((int32_t)&v29 + 24 + 4 * v28 / 32); // 0x8001be98
        kprintf("%c", (v30 & 1 << v28) == 0 ? 42 : 46);
        if (v28 % 64 == 63 && v28 < v13 - 1) {
            // 0x8001bed8
            kprintf("\n   ");
        }
        int32_t v31 = v28 + 1; // 0x8001bee0
        while (v31 < v13) {
            // 0x8001be90
            v28 = v31;
            v30 = *(int32_t *)((int32_t)&v29 + 24 + 4 * v28 / 32);
            kprintf("%c", (v30 & 1 << v28) == 0 ? 42 : 46);
            if (v28 % 64 == 63 && v28 < v13 - 1) {
                // 0x8001bed8
                kprintf("\n   ");
            }
            // 0x8001bee0
            v31 = v28 + 1;
        }
        // 0x8001bef0
        kprintf("\n");
        return;
    }
    goto lab_0x8001bd20;
  lab_0x8001bd20:
    // 0x8001bd20
    *(int32_t *)(v4 + (int32_t)&freemap) = 0;
    v2 = v3 + 1;
    goto lab_0x8001bd38;
}