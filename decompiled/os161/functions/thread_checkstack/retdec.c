void thread_checkstack(int32_t * thread) {
    int32_t v1 = *(int32_t *)((int32_t)thread + 72); // 0x80015e6c
    if (v1 == 0) {
        // 0x80015f38
        return;
    }
    int32_t v2 = v1; // 0x80015e88
    if (*(int32_t *)v1 != -0x45520ff3) {
        // 0x80015e8c
        badassert("((uint32_t*)thread->t_stack)[0] == THREAD_STACK_MAGIC", "../../thread/thread.c", 104, "thread_checkstack");
        v2 = &g41;
    }
    int32_t v3 = v2; // 0x80015eb8
    if (*(int32_t *)(v2 + 4) != -0x45520ff3) {
        // 0x80015ebc
        badassert("((uint32_t*)thread->t_stack)[1] == THREAD_STACK_MAGIC", "../../thread/thread.c", 105, "thread_checkstack");
        v3 = &g41;
    }
    int32_t v4 = v3; // 0x80015ee8
    if (*(int32_t *)(v3 + 8) != -0x45520ff3) {
        // 0x80015eec
        badassert("((uint32_t*)thread->t_stack)[2] == THREAD_STACK_MAGIC", "../../thread/thread.c", 106, "thread_checkstack");
        v4 = &g41;
    }
    // 0x80015f08
    if (*(int32_t *)(v4 + 12) != -0x45520ff3) {
        // 0x80015f1c
        badassert("((uint32_t*)thread->t_stack)[3] == THREAD_STACK_MAGIC", "../../thread/thread.c", 107, "thread_checkstack");
    }
}