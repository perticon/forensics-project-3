void thread_checkstack(thread *thread)

{
  int *piVar1;
  uint uVar2;
  uint uVar3;
  
  piVar1 = (int *)thread->t_stack;
  if (piVar1 != (int *)0x0) {
    uVar2 = 0xbaad0000;
    if (*piVar1 != -0x45520ff3) {
      badassert("((uint32_t*)thread->t_stack)[0] == THREAD_STACK_MAGIC","../../thread/thread.c",0x68
                ,"thread_checkstack");
    }
    uVar3 = 0xbaad0000;
    if (piVar1[1] != (uVar2 | 0xf00d)) {
      badassert("((uint32_t*)thread->t_stack)[1] == THREAD_STACK_MAGIC","../../thread/thread.c",0x69
                ,"thread_checkstack");
    }
    if (piVar1[2] != (uVar3 | 0xf00d)) {
      badassert("((uint32_t*)thread->t_stack)[2] == THREAD_STACK_MAGIC","../../thread/thread.c",0x6a
                ,"thread_checkstack");
    }
    if (piVar1[3] != -0x45520ff3) {
      badassert("((uint32_t*)thread->t_stack)[3] == THREAD_STACK_MAGIC","../../thread/thread.c",0x6b
                ,"thread_checkstack");
    }
  }
  return;
}