static void add_free_entry(struct swap_entry *fentry)
{
    fentry->next = free_list_head->next;
    free_list_head->next = fentry;
}