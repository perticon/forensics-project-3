void devnull_create(void)

{
  device *dev;
  int errcode;
  char *pcVar1;
  
  dev = (device *)kmalloc(0x14);
  if (dev == (device *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Could not add null device: out of memory\n");
  }
  dev->d_ops = &null_devops;
  dev->d_blocks = 0;
  dev->d_blocksize = 1;
  dev->d_devnumber = 0;
  dev->d_data = (void *)0x0;
  errcode = vfs_adddev("null",dev,0);
  if (errcode != 0) {
    pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
    panic("Could not add null device: %s\n",pcVar1);
  }
  return;
}