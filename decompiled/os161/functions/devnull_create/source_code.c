devnull_create(void)
{
	int result;
	struct device *dev;

	dev = kmalloc(sizeof(*dev));
	if (dev==NULL) {
		panic("Could not add null device: out of memory\n");
	}

	dev->d_ops = &null_devops;

	dev->d_blocks = 0;
	dev->d_blocksize = 1;

	dev->d_devnumber = 0; /* assigned by vfs_adddev */

	dev->d_data = NULL;

	result = vfs_adddev("null", dev, 0);
	if (result) {
		panic("Could not add null device: %s\n", strerror(result));
	}
}