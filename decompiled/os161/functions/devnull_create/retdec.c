void devnull_create(void) {
    char * v1 = kmalloc(20); // 0x80017f68
    int32_t v2 = (int32_t)v1; // 0x80017f70
    if (v1 == NULL) {
        // 0x80017f74
        panic("Could not add null device: out of memory\n");
        v2 = &g41;
    }
    int32_t * v3 = (int32_t *)v2; // 0x80017f84
    *v3 = (int32_t)&null_devops;
    *(int32_t *)(v2 + 4) = 0;
    *(int32_t *)(v2 + 8) = 1;
    *(int32_t *)(v2 + 12) = 0;
    *(int32_t *)(v2 + 16) = 0;
    int32_t v4 = vfs_adddev("null", v3, 0); // 0x80017fac
    if (v4 != 0) {
        // 0x80017fb8
        panic("Could not add null device: %s\n", strerror(v4));
    }
}