int sys_fork(trapframe *ctf,pid_t *retval)

{
  char **ppcVar1;
  proc *proc;
  int iVar2;
  void *dst;
  undefined4 *puVar3;
  undefined4 uVar4;
  char *pcVar5;
  undefined4 uVar6;
  Elf_Ehdr *pEVar7;
  undefined4 uVar8;
  char **unaff_s7;
  
  ppcVar1 = (char **)unaff_s7[0x15];
  if (ppcVar1 == (char **)0x0) {
    pcVar5 = s_______syscall_proc_syscalls_c_80024d30;
    badassert("curproc != NULL",s_______syscall_proc_syscalls_c_80024d30,0x7c,"sys_fork");
    retval = (pid_t *)pcVar5;
  }
  proc = proc_create_runprogram(*ppcVar1);
  if (proc == (proc *)0x0) {
    iVar2 = 3;
  }
  else {
    pcVar5 = unaff_s7[0x15];
    puVar3 = (undefined4 *)(pcVar5 + 0x2c);
    pEVar7 = &proc->p_eh;
    do {
      uVar8 = puVar3[1];
      uVar6 = puVar3[2];
      uVar4 = puVar3[3];
      *(undefined4 *)pEVar7->e_ident = *puVar3;
      *(undefined4 *)(pEVar7->e_ident + 4) = uVar8;
      *(undefined4 *)(pEVar7->e_ident + 8) = uVar6;
      *(undefined4 *)(pEVar7->e_ident + 0xc) = uVar4;
      puVar3 = puVar3 + 4;
      pEVar7 = (Elf_Ehdr *)&pEVar7->e_type;
    } while (puVar3 != (undefined4 *)(pcVar5 + 0x5c));
    *(undefined4 *)pEVar7 = *puVar3;
    as_copy(*(addrspace **)(unaff_s7[0x15] + 0x10),&proc->p_addrspace,
            *(pid_t *)(unaff_s7[0x15] + 0x1c),proc->p_pid);
    if (proc->p_addrspace == (addrspace *)0x0) {
      proc_destroy(proc);
      iVar2 = 3;
    }
    else {
      dst = kmalloc(0x8c);
      if (dst == (void *)0x0) {
        proc_destroy(proc);
        iVar2 = 3;
      }
      else {
        memcpy(dst,ctf,0x8c);
        iVar2 = thread_fork(*unaff_s7,proc,call_enter_forked_process,dst,0);
        if (iVar2 == 0) {
          *retval = proc->p_pid;
          iVar2 = 0;
        }
        else {
          proc_destroy(proc);
          kfree(dst);
          iVar2 = 3;
        }
      }
    }
  }
  return iVar2;
}