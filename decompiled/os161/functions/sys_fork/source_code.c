int sys_fork(struct trapframe *ctf, pid_t *retval)
{
  struct trapframe *tf_child;
  struct proc *newp;
  int result;

  KASSERT(curproc != NULL);

  newp = proc_create_runprogram(curproc->p_name);
  if (newp == NULL)
  {
    return ENOMEM;
  }

  newp->p_eh = curproc->p_eh;

  /* done here as we need to duplicate the address space 
     of thbe current process */
  as_copy(curproc->p_addrspace, &(newp->p_addrspace), curproc->p_pid, newp->p_pid);
  if (newp->p_addrspace == NULL)
  {
    proc_destroy(newp);
    return ENOMEM;
  }

  /* we need a copy of the parent's trapframe */
  tf_child = kmalloc(sizeof(struct trapframe));
  if (tf_child == NULL)
  {
    proc_destroy(newp);
    return ENOMEM;
  }
  memcpy(tf_child, ctf, sizeof(struct trapframe));

  /* TO BE DONE: linking parent/child, so that child terminated 
     on parent exit */

  result = thread_fork(
      curthread->t_name, newp,
      call_enter_forked_process,
      (void *)tf_child, (unsigned long)0 /*unused*/);

  if (result)
  {
    proc_destroy(newp);
    kfree(tf_child);
    return ENOMEM;
  }

  *retval = newp->p_pid;

  return 0;
}