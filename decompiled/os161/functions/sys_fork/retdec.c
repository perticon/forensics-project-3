int32_t sys_fork(int32_t * ctf, int32_t * retval) {
    // 0x8000dde0
    int32_t v1; // 0x8000dde0
    int32_t * v2 = (int32_t *)(v1 + 84); // 0x8000ddf8
    int32_t v3 = *v2; // 0x8000ddf8
    int32_t v4 = (int32_t)retval; // 0x8000de04
    int32_t v5 = v3; // 0x8000de04
    if (v3 == 0) {
        // 0x8000de08
        badassert("curproc != NULL", "../../syscall/proc_syscalls.c", 124, "sys_fork");
        v4 = (int32_t)"../../syscall/proc_syscalls.c";
        v5 = &g41;
    }
    int32_t * v6 = proc_create_runprogram((char *)*(int32_t *)v5); // 0x8000de30
    if (v6 == NULL) {
        // 0x8000df48
        return 3;
    }
    int32_t v7 = (int32_t)v6; // 0x8000de30
    int32_t v8 = *v2; // 0x8000de3c
    int32_t v9 = v7 + 44; // 0x8000de4c
    int32_t v10 = v8 + 44;
    *(int32_t *)v9 = *(int32_t *)v10;
    *(int32_t *)(v9 + 4) = *(int32_t *)(v10 + 4);
    *(int32_t *)(v9 + 8) = *(int32_t *)(v10 + 8);
    *(int32_t *)(v9 + 12) = *(int32_t *)(v10 + 12);
    int32_t v11 = v10 + 16; // 0x8000de70
    v9 += 16;
    while (v10 != v8 + 76) {
        // 0x8000de50
        v10 = v11;
        *(int32_t *)v9 = *(int32_t *)v10;
        *(int32_t *)(v9 + 4) = *(int32_t *)(v10 + 4);
        *(int32_t *)(v9 + 8) = *(int32_t *)(v10 + 8);
        *(int32_t *)(v9 + 12) = *(int32_t *)(v10 + 12);
        v11 = v10 + 16;
        v9 += 16;
    }
    // 0x8000de7c
    *(int32_t *)v9 = *(int32_t *)v11;
    int32_t v12 = *v2; // 0x8000de88
    int32_t v13 = *(int32_t *)(v12 + 28); // 0x8000de94
    int32_t * v14 = (int32_t *)(v7 + 28); // 0x8000de98
    int32_t v15 = v7 + 16; // 0x8000dea0
    as_copy((int32_t *)*(int32_t *)(v12 + 16), (int32_t **)v15, v13, *v14);
    if (*(int32_t *)v15 == 0) {
        // 0x8000deb4
        proc_destroy(v6);
        // 0x8000df48
        return 3;
    }
    char * v16 = kmalloc(140); // 0x8000dec8
    if (v16 == NULL) {
        // 0x8000ded4
        proc_destroy(v6);
        // 0x8000df48
        return 3;
    }
    // 0x8000dee4
    memcpy(v16, ctf, 140);
    int32_t v17 = thread_fork((char *)v1, v6, (void (*)(char *, int32_t))((int32_t)&g2 - 0x2370), v16, 0); // 0x8000df0c
    int32_t result; // 0x8000dde0
    if (v17 == 0) {
        // 0x8000df30
        *(int32_t *)v4 = *v14;
        result = 0;
    } else {
        // 0x8000df18
        proc_destroy(v6);
        kfree(v16);
        result = 3;
    }
    // 0x8000df48
    return result;
}