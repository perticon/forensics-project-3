int32_t lrandom_random(char * devdata) {
    int32_t v1 = *(int32_t *)((int32_t)devdata + 4); // 0x80004a98
    return lamebus_read_register((int32_t *)devdata, v1, 0);
}