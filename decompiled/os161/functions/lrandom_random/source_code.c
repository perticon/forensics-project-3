lrandom_random(void *devdata)
{
	struct lrandom_softc *lr = devdata;
	return bus_read_register(lr->lr_bus, lr->lr_buspos, LR_REG_RAND);
}