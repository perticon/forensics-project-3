kstrdup(const char *s)
{
	char *z;

	z = kmalloc(strlen(s)+1);
	if (z == NULL) {
		return NULL;
        }
	strcpy(z, s);
	return z;
}