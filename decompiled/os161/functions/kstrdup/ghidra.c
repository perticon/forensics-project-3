char * kstrdup(char *s)

{
  size_t sVar1;
  char *dest;
  
  sVar1 = strlen(s);
  dest = (char *)kmalloc(sVar1 + 1);
  if (dest == (char *)0x0) {
    dest = (char *)0x0;
  }
  else {
    strcpy(dest,s);
  }
  return dest;
}