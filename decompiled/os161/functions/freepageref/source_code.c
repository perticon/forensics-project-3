freepageref(struct pageref *p)
{
	size_t i, j;
	uint32_t k;
	unsigned whichroot;
	struct kheap_root *root;
	struct pagerefpage *page;

	for (whichroot=0; whichroot < NUM_PAGEREFPAGES; whichroot++) {
		root = &kheaproots[whichroot];

		page = root->page;
		if (page == NULL) {
			KASSERT(root->numinuse == 0);
			continue;
		}

		j = p-page->refs;
		/* note: j is unsigned, don't test < 0 */
		if (j < NPAGEREFS_PER_PAGE) {
			/* on this page */
			i = j/32;
			k = ((uint32_t)1) << (j%32);
			KASSERT((root->pagerefs_inuse[i] & k) != 0);
			root->pagerefs_inuse[i] &= ~k;
			KASSERT(root->numinuse > 0);
			root->numinuse--;
			return;
		}
	}
	/* pageref wasn't on any of the pages */
	KASSERT(0);
}