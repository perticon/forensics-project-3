void freepageref(pageref *p)

{
  uint uVar1;
  uint uVar2;
  uint uVar3;
  char *pcVar4;
  char *pcVar5;
  char *pcVar6;
  uint uVar7;
  
  uVar1 = 0;
  pcVar4 = (char *)p;
  pcVar6 = (char *)kheaproots;
LAB_8001c0c4:
  do {
    if (0xf < uVar1) {
      badassert("0","../../vm/kmalloc.c",0x150,"freepageref");
      return;
    }
    pcVar5 = (char *)((int)pcVar6 + uVar1 * 0x28);
    uVar2 = (int)pcVar4 - (int)*(pagerefpage **)pcVar5;
    if (*(pagerefpage **)pcVar5 == (pagerefpage *)0x0) {
      uVar2 = *(uint *)((int)pcVar5 + 0x24);
      uVar1 = uVar1 + 1;
      if (uVar2 == 0) goto LAB_8001c0c4;
      pcVar4 = "root->numinuse == 0";
      pcVar5 = "../../vm/kmalloc.c";
      pcVar6 = "freepageref";
      badassert("root->numinuse == 0","../../vm/kmalloc.c",0x13e,"freepageref");
    }
    uVar2 = (int)uVar2 >> 4;
    uVar1 = uVar1 + 1;
    if (uVar2 < 0x100) {
      uVar7 = uVar2 >> 5;
      uVar2 = 1 << (uVar2 & 0x1f);
      uVar1 = ((uint32_t *)((int)pcVar5 + 4))[uVar7];
      uVar3 = ~uVar2;
      if ((uVar1 & uVar2) == 0) {
        pcVar5 = "../../vm/kmalloc.c";
        badassert("(root->pagerefs_inuse[i] & k) != 0","../../vm/kmalloc.c",0x148,"freepageref");
      }
      ((uint32_t *)((int)pcVar5 + 4))[uVar7] = uVar1 & uVar3;
      uVar1 = *(uint *)((int)pcVar5 + 0x24) - 1;
      if (*(uint *)((int)pcVar5 + 0x24) == 0) {
        pcVar5 = "../../vm/kmalloc.c";
        badassert("root->numinuse > 0","../../vm/kmalloc.c",0x14a,"freepageref");
      }
      *(uint *)((int)pcVar5 + 0x24) = uVar1;
      return;
    }
  } while( true );
}