void freepageref(int32_t * p) {
    int32_t v1 = 0; // 0x8001bfd4
    int32_t v2 = (int32_t)p; // 0x8001bfd4
    int32_t v3 = &kheaproots; // 0x8001bfd4
    int32_t v4; // 0x8001bfc0
    int32_t v5; // 0x8001bfc0
    int32_t v6; // 0x8001bfc0
    while (true) {
      lab_0x8001c0c4:;
        int32_t v7 = v3;
        int32_t v8 = v2;
        int32_t v9 = v1;
        if (v9 >= 16) {
            // break (via goto) -> 0x8001c0d0
            goto lab_0x8001c0d0;
        }
        int32_t v10 = 40 * v9 + v7; // 0x8001bfe0
        int32_t v11 = *(int32_t *)v10; // 0x8001bfe4
        while (v11 == 0) {
            int32_t v12 = *(int32_t *)(v10 + 36); // 0x8001bff4
            int32_t v13 = v9 + 1; // 0x8001c000
            if (v12 != 0) {
                // 0x8001c004
                badassert("root->numinuse == 0", "../../vm/kmalloc.c", 318, "freepageref");
                v5 = &g41;
                v6 = v12;
                v2 = (int32_t)"root->numinuse == 0";
                v4 = (int32_t)"../../vm/kmalloc.c";
                v3 = (int32_t)"freepageref";
                goto lab_0x8001c0f0;
            }
            v9 = v13;
            if (v9 >= 16) {
                // break (via goto) -> 0x8001c0d0
                goto lab_0x8001c0d0;
            }
            // 0x8001bfd8
            v10 = 40 * v9 + v7;
            v11 = *(int32_t *)v10;
        }
        // 0x8001c024
        v5 = v9;
        v6 = v8 - v11;
        v2 = v8;
        v4 = v10;
        v3 = v7;
        goto lab_0x8001c0f0;
    }
  lab_0x8001c0d0:
    // 0x8001c0d0
    badassert("0", "../../vm/kmalloc.c", 336, "freepageref");
  lab_0x8001c0f0:;
    uint32_t v14 = v6 >> 4; // 0x8001c024
    v1 = v5 + 1;
    int32_t v15; // 0x8001bfc0
    int32_t v16; // 0x8001bfc0
    int32_t v17; // 0x8001c03c
    int32_t v18; // 0x8001bfc0
    if (v14 < 256) {
        // 0x8001c034
        v17 = 1 << v14;
        v18 = 4 * v14 / 32 + 4;
        int32_t v19 = *(int32_t *)(v18 + v4); // 0x8001c048
        v16 = v19;
        v15 = v4;
        if ((v19 & v17) == 0) {
            // 0x8001c05c
            badassert("(root->pagerefs_inuse[i] & k) != 0", "../../vm/kmalloc.c", 328, "freepageref");
            v16 = &g41;
            v15 = (int32_t)"../../vm/kmalloc.c";
            goto lab_0x8001c07c;
        } else {
            goto lab_0x8001c07c;
        }
    }
    goto lab_0x8001c0c4;
  lab_0x8001c07c:
    // 0x8001c07c
    *(int32_t *)(v15 + v18) = v16 & -1 - v17;
    int32_t v20 = *(int32_t *)(v15 + 36); // 0x8001c08c
    int32_t v21 = v20 - 1; // 0x8001c098
    int32_t v22 = v15; // 0x8001c098
    if (v20 == 0) {
        // 0x8001c09c
        badassert("root->numinuse > 0", "../../vm/kmalloc.c", 330, "freepageref");
        v21 = &g41;
        v22 = (int32_t)"../../vm/kmalloc.c";
        goto lab_0x8001c0bc;
    } else {
        goto lab_0x8001c0bc;
    }
  lab_0x8001c0bc:
    // 0x8001c0bc
    *(int32_t *)(v22 + 36) = v21;
    goto lab_0x8001c0f0;
}