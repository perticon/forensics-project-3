int sfs_gettype(vnode *v,uint32_t *ret)

{
  short sVar1;
  void *pvVar2;
  void *pvVar3;
  
  pvVar2 = v->vn_data;
  pvVar3 = v->vn_fs->fs_data;
  vfs_biglock_acquire();
  sVar1 = *(short *)((int)pvVar2 + 0x1c);
  if (sVar1 == 1) {
    *ret = 0x1000;
    vfs_biglock_release();
  }
  else {
    if (sVar1 != 2) {
                    /* WARNING: Subroutine does not return */
      panic("sfs: %s: gettype: Invalid inode type (inode %u, type %u)\n",(int)pvVar3 + 0x10,
            *(undefined4 *)((int)pvVar2 + 0x218));
    }
    *ret = 0x2000;
    vfs_biglock_release();
  }
  return 0;
}