int32_t sfs_gettype(int32_t * v2, int32_t * ret) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(v1 + 16); // 0x8000959c
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x800095a8
    vfs_biglock_acquire();
    uint16_t v4 = *(int16_t *)(v2_ + 28); // 0x800095b4
    int32_t result; // 0x80009588
    switch (v4) {
        case 1: {
            // 0x800095d8
            *ret = 0x1000;
            vfs_biglock_release();
            result = 0;
            // break -> 0x80009608
            break;
        }
        case 2: {
            // 0x800095e8
            *ret = 0x2000;
            vfs_biglock_release();
            result = 0;
            // break -> 0x80009608
            break;
        }
        default: {
            int32_t v5 = *(int32_t *)(v2_ + 536); // 0x800095fc
            panic("sfs: %s: gettype: Invalid inode type (inode %u, type %u)\n", (char *)(v3 + 16), v5, (int32_t)v4);
            result = &g41;
            // break -> 0x80009608
            break;
        }
    }
    // 0x80009608
    return result;
}