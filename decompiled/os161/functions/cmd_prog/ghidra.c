int cmd_prog(int nargs,char **args)

{
  int iVar1;
  
  if (nargs < 2) {
    kprintf("Usage: p program [arguments]\n");
    iVar1 = 8;
  }
  else {
    iVar1 = common_prog(nargs + -1,args + 1);
  }
  return iVar1;
}