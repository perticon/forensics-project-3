cmd_prog(int nargs, char **args)
{
	if (nargs < 2)
	{
		kprintf("Usage: p program [arguments]\n");
		return EINVAL;
	}

	/* drop the leading "p" */
	args++;
	nargs--;

	return common_prog(nargs, args);
}