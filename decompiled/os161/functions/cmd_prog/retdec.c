int32_t cmd_prog(int32_t nargs, char ** args) {
    int32_t result; // 0x8000c82c
    if (nargs >= 2) {
        // 0x8000c850
        result = common_prog(nargs - 1, (char **)((int32_t)args + 4));
    } else {
        // 0x8000c83c
        kprintf("Usage: p program [arguments]\n");
        result = 8;
    }
    // 0x8000c85c
    return result;
}