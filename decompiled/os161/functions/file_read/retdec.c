int32_t file_read(uint32_t fd, int32_t * buf_ptr, int32_t size) {
    // 0x8000d340
    if (fd >= 129) {
        // 0x8000d414
        return -1;
    }
    // 0x8000d34c
    int32_t v1; // 0x8000d340
    int32_t v2 = *(int32_t *)(4 * fd + 96 + *(int32_t *)(v1 + 84)); // 0x8000d36c
    if (v2 == 0) {
        // 0x8000d414
        return -1;
    }
    int32_t v3 = *(int32_t *)v2; // 0x8000d37c
    if (v3 == 0) {
        // 0x8000d414
        return -1;
    }
    int32_t * v4 = (int32_t *)(v2 + 12); // 0x8000d3a4
    int32_t * v5 = (int32_t *)(v2 + 8); // 0x8000d3a8
    vnode_check((int32_t *)v3, "read");
    int32_t v6 = *(int32_t *)(*(int32_t *)(v3 + 20) + 12); // 0x8000d3e0
    int32_t result = v6; // 0x8000d3f4
    if (v6 == 0) {
        // 0x8000d3f8
        result = 0;
    }
    // 0x8000d414
    return result;
}