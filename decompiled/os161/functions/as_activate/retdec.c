void as_activate(void) {
    // 0x8001ad74
    if (proc_getas() == NULL) {
        // 0x8001addc
        return;
    }
    int32_t v1 = splx(1); // 0x8001ad98
    int32_t v2 = 0;
    tlb_write(-((0x1000 * v2)), 0, v2);
    int32_t v3 = v2 + 1; // 0x8001adbc
    while (v3 < 64) {
        // 0x8001adac
        v2 = v3;
        tlb_write(-((0x1000 * v2)), 0, v2);
        v3 = v2 + 1;
    }
    // 0x8001adcc
    splx(v1);
    increase(3);
}