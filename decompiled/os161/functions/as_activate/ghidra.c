void as_activate(void)

{
  addrspace *paVar1;
  int spl;
  int iVar2;
  
  paVar1 = proc_getas();
  if (paVar1 != (addrspace *)0x0) {
    spl = splx(1);
    for (iVar2 = 0; iVar2 < 0x40; iVar2 = iVar2 + 1) {
      tlb_write((iVar2 + 0x80000) * 0x1000,0,iVar2);
    }
    splx(spl);
    increase(3);
  }
  return;
}