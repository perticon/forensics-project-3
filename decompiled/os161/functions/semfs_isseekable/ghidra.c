bool semfs_isseekable(vnode *vn)

{
  if (*(int *)((int)vn->vn_data + 0x1c) != -1) {
    return false;
  }
  return true;
}