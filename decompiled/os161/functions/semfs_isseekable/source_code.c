semfs_isseekable(struct vnode *vn)
{
	struct semfs_vnode *semv = vn->vn_data;

	if (semv->semv_semnum != SEMFS_ROOTDIR) {
		/* seeking a semaphore doesn't mean anything */
		return false;
	}
	return true;
}