bool semfs_isseekable(int32_t * vn) {
    // 0x80006054
    return *(int32_t *)(*(int32_t *)((int32_t)vn + 16) + 28) == -1;
}