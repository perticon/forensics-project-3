void vfs_biglock_acquire(void)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = lock_do_i_hold(vfs_biglock);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    lock_acquire(vfs_biglock);
  }
  else if (vfs_biglock_depth == 0) {
    lock_acquire(vfs_biglock);
  }
  vfs_biglock_depth = vfs_biglock_depth + 1;
  return;
}