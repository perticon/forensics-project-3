void vfs_biglock_acquire(void) {
    // 0x80018718
    if (!lock_do_i_hold(vfs_biglock)) {
        // 0x80018738
        lock_acquire(vfs_biglock);
        // 0x80018774
        vfs_biglock_depth++;
        return;
    }
    // 0x8001874c
    if (vfs_biglock_depth == 0) {
        // 0x80018760
        lock_acquire(vfs_biglock);
    }
    // 0x80018774
    vfs_biglock_depth++;
}