checkguardband(vaddr_t blockaddr, size_t smallerblocksize, size_t blocksize)
{
	/*
	 * The first two bytes of the block are the lower guard band.
	 * The next two bytes are the real size (the size of the
	 * client data). The last four bytes of the block duplicate
	 * this info. In between the real data and the upper guard
	 * band should be filled with GUARD_FILLBYTE.
	 *
	 * If the guard values are wrong, or the low and high sizes
	 * don't match, or the size is out of range, by far the most
	 * likely explanation is that something ran past the bounds of
	 * its memory block.
	 */
	vaddr_t lowguard, lowsize, data, enddata, highguard, highsize, i;
	unsigned clientsize;

	lowguard = blockaddr;
	lowsize = lowguard + 2;
	data = lowsize + 2;
	highguard = blockaddr + blocksize - 4;
	highsize = highguard + 2;

	KASSERT(*(uint16_t *)lowguard == GUARD_HALFWORD);
	KASSERT(*(uint16_t *)highguard == GUARD_HALFWORD);
	clientsize = *(uint16_t *)lowsize;
	KASSERT(clientsize == *(uint16_t *)highsize);
	KASSERT(clientsize + GUARD_OVERHEAD > smallerblocksize);
	KASSERT(clientsize + GUARD_OVERHEAD <= blocksize);
	enddata = data + clientsize;
	for (i=enddata; i<highguard; i++) {
		KASSERT(*(uint8_t *)i == GUARD_FILLBYTE);
	}
}