void make_sleepalots(int howmany)

{
  int iVar1;
  char *pcVar2;
  ulong data2;
  char name [16];
  
  data2 = 0;
  while ((int)data2 < howmany) {
    snprintf(name,0x10,"sleepalot%d",data2);
    iVar1 = thread_fork(name,(proc *)0x0,sleepalot_thread,(void *)0x0,data2);
    data2 = data2 + 1;
    if (iVar1 != 0) {
      pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
      panic("thread_fork failed: %s\n",pcVar2);
    }
  }
  iVar1 = thread_fork("waker",(proc *)0x0,waker_thread,(void *)0x0,0);
  if (iVar1 != 0) {
    pcVar2 = strerror(iVar1);
                    /* WARNING: Subroutine does not return */
    panic("thread_fork failed: %s\n",pcVar2);
  }
  return;
}