void make_sleepalots(uint32_t howmany) {
    char name[16]; // bp-40, 0x80014724
    if ((int32_t)howmany > 0) {
        int32_t v1 = 0;
        snprintf(name, 16, "sleepalot%d", v1);
        int32_t v2 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1); // 0x8001477c
        int32_t v3 = v2; // 0x80014784
        int32_t v4; // 0x8001477c
        if (v2 != 0) {
            panic("thread_fork failed: %s\n", strerror(v3));
            snprintf(name, 16, "sleepalot%d", v1);
            v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80014788
                panic("thread_fork failed: %s\n", strerror(v3));
                snprintf(name, 16, "sleepalot%d", v1);
                v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1);
                v3 = v4;
            }
        }
        int32_t v5 = v1 + 1; // 0x80014784
        while (v5 < howmany) {
            // 0x80014758
            v1 = v5;
            snprintf(name, 16, "sleepalot%d", v1);
            v2 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1);
            v3 = v2;
            if (v2 != 0) {
                panic("thread_fork failed: %s\n", strerror(v3));
                snprintf(name, 16, "sleepalot%d", v1);
                v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1);
                v3 = v4;
                while (v4 != 0) {
                    // 0x80014788
                    panic("thread_fork failed: %s\n", strerror(v3));
                    snprintf(name, 16, "sleepalot%d", v1);
                    v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4ab8), NULL, v1);
                    v3 = v4;
                }
            }
            // 0x800147a4
            v5 = v1 + 1;
        }
    }
    int32_t v6 = thread_fork("waker", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x49a8), NULL, 0); // 0x800147cc
    if (v6 != 0) {
        // 0x800147d8
        panic("thread_fork failed: %s\n", strerror(v6));
    }
}