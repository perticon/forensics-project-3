make_sleepalots(int howmany)
{
	char name[16];
	int i, result;

	for (i=0; i<howmany; i++) {
		snprintf(name, sizeof(name), "sleepalot%d", i);
		result = thread_fork(name, NULL, sleepalot_thread, NULL, i);
		if (result) {
			panic("thread_fork failed: %s\n", strerror(result));
		}
	}
	result = thread_fork("waker", NULL, waker_thread, NULL, 0);
	if (result) {
		panic("thread_fork failed: %s\n", strerror(result));
	}
}