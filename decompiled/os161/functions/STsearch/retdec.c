int32_t STsearch(int32_t * st, int32_t pid, int32_t addr) {
    int32_t v1 = *(int32_t *)(4 * hashU(pid, addr) + pid); // 0x8000a4c8
    int32_t * v2 = searchST((int32_t *)v1, pid, (int32_t *)addr); // 0x8000a4d8
    int32_t result = -1; // 0x8000a4e0
    if (v2 != NULL) {
        // 0x8000a4e4
        result = *(int32_t *)((int32_t)v2 + 8);
    }
    // 0x8000a4f4
    return result;
}