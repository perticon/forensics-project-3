int STsearch(ST st,pid_t pid,vaddr_t addr)

{
  int iVar1;
  Item piVar2;
  Key k;
  
  iVar1 = hashU((Key)CONCAT44(pid,addr),pid);
  piVar2 = searchST(st->heads[iVar1],(Key)CONCAT44(pid,addr),(link)pid);
  if (piVar2 == (Item)0x0) {
    iVar1 = -1;
  }
  else {
    iVar1 = piVar2->index;
  }
  return iVar1;
}