void spllower(int oldspl,int newspl)

{
  int iVar1;
  char *pcVar2;
  int unaff_s7;
  
  if (oldspl != 1) {
    pcVar2 = "../../thread/spl.c";
    badassert("oldspl == IPL_HIGH","../../thread/spl.c",0x70,"spllower");
    newspl = (int)pcVar2;
  }
  if ((char *)newspl != (char *)0x0) {
    badassert("newspl == IPL_NONE","../../thread/spl.c",0x71,"spllower");
  }
  if ((unaff_s7 != 0) &&
     (iVar1 = *(int *)(unaff_s7 + 0x60) + -1, *(int *)(unaff_s7 + 0x60) = iVar1, iVar1 == 0)) {
    cpu_irqon();
  }
  return;
}