void spllower(int32_t oldspl, int32_t newspl) {
    // 0x800153dc
    if (oldspl == 1) {
        if (newspl == 0) {
            goto lab_0x80015430;
        } else {
            // 0x80015414
            badassert("newspl == IPL_NONE", "../../thread/spl.c", 113, "spllower");
            goto lab_0x80015430;
        }
    } else {
        // 0x8001540c
        badassert("oldspl == IPL_HIGH", "../../thread/spl.c", 112, "spllower");
        // 0x80015414
        badassert("newspl == IPL_NONE", "../../thread/spl.c", 113, "spllower");
        goto lab_0x80015430;
    }
  lab_0x80015430:;
    int32_t v1; // 0x800153dc
    if (v1 == 0) {
        // 0x80015454
        return;
    }
    int32_t * v2 = (int32_t *)(v1 + 96); // 0x80015438
    int32_t v3 = *v2 - 1; // 0x80015440
    *v2 = v3;
    if (v3 == 0) {
        // 0x8001544c
        cpu_irqon();
    }
}