spllower(int oldspl, int newspl)
{
	struct thread *cur = curthread;

	/* only one priority level, only one valid args configuration */
	KASSERT(oldspl == IPL_HIGH);
	KASSERT(newspl == IPL_NONE);

	if (!CURCPU_EXISTS()) {
		/* before curcpu initialization; interrupts are off anyway */
		return;
	}

	cur->t_iplhigh_count--;
	if (cur->t_iplhigh_count == 0) {
		cpu_irqon();
	}
}