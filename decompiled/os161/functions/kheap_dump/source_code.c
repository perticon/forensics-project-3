kheap_dump(void)
{
#ifdef LABELS
	/* print the whole thing with interrupts off */
	spinlock_acquire(&kmalloc_spinlock);
	dump_subpages(mallocgeneration);
	spinlock_release(&kmalloc_spinlock);
#else
	kprintf("Enable LABELS in kmalloc.c to use this functionality.\n");
#endif
}