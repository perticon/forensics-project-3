int32_t sfs_write(int32_t * v2, int32_t * uio) {
    // 0x800096fc
    if (*(int32_t *)((int32_t)uio + 24) != 1) {
        // 0x80009720
        badassert("uio->uio_rw==UIO_WRITE", "../../fs/sfs/sfs_vnops.c", 122, "sfs_write");
    }
    // 0x80009740
    vfs_biglock_acquire();
    int32_t result = sfs_io((int32_t *)*(int32_t *)((int32_t)v2 + 16), uio); // 0x80009750
    vfs_biglock_release();
    return result;
}