int subpage_kfree(void *ptr)

{
  uint uVar1;
  void *pvVar2;
  char *pcVar3;
  uint uVar4;
  pageref *pr;
  uint blktype;
  void *addr;
  uint uVar5;
  char *pcVar6;
  
  spinlock_acquire(&kmalloc_spinlock);
  addr = (void *)0x0;
  blktype = 0;
  uVar4 = 0xfffff000;
  for (pr = allbase; pr != (pageref *)0x0; pr = pr->next_all) {
    addr = (void *)(pr->pageaddr_and_blocktype & uVar4);
    blktype = pr->pageaddr_and_blocktype & 0xfff;
    uVar5 = (uint)(blktype < 8);
    if (uVar5 == 0) {
      badassert("blktype >= 0 && blktype < NSIZES","../../vm/kmalloc.c",0x433,"subpage_kfree");
    }
    uVar1 = (uint)(ptr < addr);
    if (uVar5 == 0) {
      badassert("blktype>=0 && blktype<NSIZES","../../vm/kmalloc.c",0x436,"subpage_kfree");
    }
    if ((uVar1 == 0) && (ptr < (void *)((int)addr + 0x1000U))) break;
  }
  uVar4 = (int)ptr - (int)addr;
  if (pr == (pageref *)0x0) {
    spinlock_release(&kmalloc_spinlock);
    return -1;
  }
  if (uVar4 < 0x1000) {
    uVar5 = sizes[blktype];
    if (uVar5 == 0) {
      trap(0x1c00);
    }
    if (uVar4 % uVar5 == 0) {
      fill_deadbeef(ptr,uVar5);
      pvVar2 = (void *)((uint)pr->freelist_offset + (int)addr);
      if (pr->freelist_offset == 0xffff) {
        *(undefined4 *)ptr = 0;
      }
      else {
        *(void **)ptr = pvVar2;
        if (ptr == pvVar2) {
          badassert("fl != fl->next","../../vm/kmalloc.c",0x46e,"subpage_kfree");
        }
      }
      pr->freelist_offset = (uint16_t)uVar4;
      pcVar3 = (char *)(pr->nfree + 1 & 0xffff);
      pcVar6 = (char *)(0x1000 / uVar5);
      if (uVar5 == 0) {
        trap(0x1c00);
      }
      pr->nfree = (uint16_t)pcVar3;
      if (pcVar6 < pcVar3) {
        pcVar6 = "pr->nfree <= PAGE_SIZE / sizes[blktype]";
        badassert("pr->nfree <= PAGE_SIZE / sizes[blktype]","../../vm/kmalloc.c",0x474,
                  "subpage_kfree");
      }
      if (pcVar3 == pcVar6) {
        remove_lists(pr,blktype);
        freepageref(pr);
        spinlock_release(&kmalloc_spinlock);
        free_kpages((vaddr_t)addr);
        return 0;
      }
      spinlock_release(&kmalloc_spinlock);
      return 0;
    }
  }
                    /* WARNING: Subroutine does not return */
  panic("kfree: subpage free of invalid addr %p\n",ptr);
}