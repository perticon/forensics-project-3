int32_t subpage_kfree(char * ptr) {
    // 0x8001c768
    spinlock_acquire(&kmalloc_spinlock);
    if (allbase == NULL) {
        // 0x8001c840
        spinlock_release(&kmalloc_spinlock);
        // 0x8001c984
        return -1;
    }
    int32_t v1 = (int32_t)ptr;
    int32_t v2 = (int32_t)allbase;
    uint32_t v3 = *(int32_t *)(v2 + 8); // 0x8001c7b4
    uint32_t v4 = v3 % 0x1000; // 0x8001c7c0
    if (v4 >= 8) {
        // 0x8001c7d0
        badassert("blktype >= 0 && blktype < NSIZES", "../../vm/kmalloc.c", 1075, "subpage_kfree");
    }
    uint32_t v5 = v3 & -0x1000; // 0x8001c7bc
    while (v5 <= v1 != v5 + 0x1000 > v1) {
        int32_t v6 = *(int32_t *)(v2 + 4); // 0x8001c828
        if (v6 == 0) {
            // 0x8001c840
            spinlock_release(&kmalloc_spinlock);
            // 0x8001c984
            return -1;
        }
        v2 = v6;
        v3 = *(int32_t *)(v2 + 8);
        v4 = v3 % 0x1000;
        if (v4 >= 8) {
            // 0x8001c7d0
            badassert("blktype >= 0 && blktype < NSIZES", "../../vm/kmalloc.c", 1075, "subpage_kfree");
        }
        // 0x8001c814
        v5 = v3 & -0x1000;
    }
    uint32_t v7 = v1 - v5; // 0x8001c83c
    int32_t v8 = v1; // 0x8001c85c
    int32_t v9; // 0x8001c768
    if (v7 >= 0x1000) {
        goto lab_0x8001c88c;
    } else {
        // 0x8001c860
        v8 = *(int32_t *)(4 * v4 + (int32_t)&sizes);
        v9 = v1;
        if (v7 % v8 == 0) {
            goto lab_0x8001c89c;
        } else {
            goto lab_0x8001c88c;
        }
    }
  lab_0x8001c88c:
    // 0x8001c88c
    panic("kfree: subpage free of invalid addr %p\n", ptr);
    v9 = (int32_t)"kfree: subpage free of invalid addr %p\n";
    goto lab_0x8001c89c;
  lab_0x8001c89c:
    // 0x8001c89c
    fill_deadbeef((char *)v9, v8);
    int16_t * v10 = (int16_t *)(v2 + 12); // 0x8001c8a4
    uint16_t v11 = *v10; // 0x8001c8a4
    if (v11 == -1) {
        // 0x8001c8b4
        *(int32_t *)ptr = 0;
    } else {
        int32_t v12 = v5 + (int32_t)v11; // 0x8001c8b0
        *(int32_t *)ptr = v12;
        if (v12 == v1) {
            // 0x8001c8c4
            badassert("fl != fl->next", "../../vm/kmalloc.c", 1134, "subpage_kfree");
        }
    }
    // 0x8001c8e4
    *v10 = (int16_t)v7;
    int16_t * v13 = (int16_t *)(v2 + 14); // 0x8001c8e8
    uint16_t v14 = *v13 + 1;
    int32_t v15 = v14; // 0x8001c8f4
    uint32_t v16 = 0x1000 / v8; // 0x8001c900
    *v13 = v14;
    int32_t v17 = v15; // 0x8001c918
    int32_t v18 = v16; // 0x8001c918
    if (v16 < v15) {
        // 0x8001c91c
        badassert("pr->nfree <= PAGE_SIZE / sizes[blktype]", "../../vm/kmalloc.c", 1140, "subpage_kfree");
        v17 = &g41;
        v18 = (int32_t)"pr->nfree <= PAGE_SIZE / sizes[blktype]";
    }
    // 0x8001c93c
    if (v17 == v18) {
        int32_t * v19 = (int32_t *)v2; // 0x8001c94c
        remove_lists(v19, v4);
        freepageref(v19);
        spinlock_release(&kmalloc_spinlock);
        free_kpages(v5);
    } else {
        // 0x8001c974
        spinlock_release(&kmalloc_spinlock);
    }
    // 0x8001c984
    return 0;
}