int sfs_lookup(vnode *v,char *path,vnode **ret)

{
  int iVar1;
  sfs_vnode *sv;
  sfs_vnode *final;
  
  sv = (sfs_vnode *)v->vn_data;
  vfs_biglock_acquire();
  if ((sv->sv_i).sfi_type == 2) {
    iVar1 = sfs_lookonce(sv,path,&final,(int *)0x0);
    if (iVar1 == 0) {
      *ret = &final->sv_absvn;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x11;
  }
  return iVar1;
}