int32_t sfs_lookup(int32_t * v2, char * path, int32_t * ret) {
    int32_t v1 = *(int32_t *)((int32_t)v2 + 16); // 0x800098b8
    vfs_biglock_acquire();
    if (*(int16_t *)(v1 + 28) != 2) {
        // 0x800098d4
        vfs_biglock_release();
        // 0x8000991c
        return 17;
    }
    // 0x800098e4
    int32_t * final; // bp-24, 0x800098a0
    int32_t v2_ = sfs_lookonce((int32_t *)v1, path, &final, NULL); // 0x800098f0
    int32_t result; // 0x800098a0
    if (v2_ == 0) {
        // 0x8000990c
        *ret = (int32_t)final;
        vfs_biglock_release();
        result = 0;
    } else {
        // 0x800098fc
        vfs_biglock_release();
        result = v2_;
    }
    // 0x8000991c
    return result;
}