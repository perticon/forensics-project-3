autoconf_lamebus(struct lamebus_softc *bus, int busunit)
{
	(void)bus; (void)busunit;
	{
		int result, devunit=nextunit_emu;
		do {
			result = tryattach_emu_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
	{
		int result, devunit=nextunit_ltrace;
		do {
			result = tryattach_ltrace_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
	{
		int result, devunit=nextunit_ltimer;
		do {
			result = tryattach_ltimer_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
	{
		int result, devunit=nextunit_lrandom;
		do {
			result = tryattach_lrandom_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
	{
		int result, devunit=nextunit_lhd;
		do {
			result = tryattach_lhd_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
	{
		int result, devunit=nextunit_lser;
		do {
			result = tryattach_lser_to_lamebus(devunit, bus, busunit);
			devunit++;
		} while (result==0);
	}
}