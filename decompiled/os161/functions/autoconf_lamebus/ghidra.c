void autoconf_lamebus(lamebus_softc *bus,int busunit)

{
  int iVar1;
  int iVar2;
  int iVar3;
  
  iVar2 = nextunit_emu;
  do {
    iVar1 = tryattach_emu_to_lamebus(iVar2,bus,busunit);
    iVar2 = iVar2 + 1;
    iVar3 = nextunit_ltrace;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_ltrace_to_lamebus(iVar3,bus,busunit);
    iVar2 = nextunit_ltimer;
    iVar3 = iVar3 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_ltimer_to_lamebus(iVar2,bus,busunit);
    iVar3 = nextunit_lrandom;
    iVar2 = iVar2 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_lrandom_to_lamebus(iVar3,bus,busunit);
    iVar2 = nextunit_lhd;
    iVar3 = iVar3 + 1;
  } while (iVar1 == 0);
  do {
    iVar1 = tryattach_lhd_to_lamebus(iVar2,bus,busunit);
    iVar3 = nextunit_lser;
    iVar2 = iVar2 + 1;
  } while (iVar1 == 0);
  do {
    iVar2 = tryattach_lser_to_lamebus(iVar3,bus,busunit);
    iVar3 = iVar3 + 1;
  } while (iVar2 == 0);
  return;
}