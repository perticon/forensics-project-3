void autoconf_lamebus(int32_t * bus, int32_t busunit) {
    int32_t v1 = nextunit_emu; // 0x80001994
    int32_t v2 = tryattach_emu_to_lamebus(v1, bus, busunit); // 0x800019a4
    v1++;
    while (v2 == 0) {
        // 0x80001998
        v2 = tryattach_emu_to_lamebus(v1, bus, busunit);
        v1++;
    }
    int32_t v3 = nextunit_ltrace; // 0x800019b8
    int32_t v4 = tryattach_ltrace_to_lamebus(v3, bus, busunit); // 0x800019c8
    v3++;
    while (v4 == 0) {
        // 0x800019bc
        v4 = tryattach_ltrace_to_lamebus(v3, bus, busunit);
        v3++;
    }
    int32_t v5 = nextunit_ltimer; // 0x800019dc
    int32_t v6 = tryattach_ltimer_to_lamebus(v5, bus, busunit); // 0x800019ec
    v5++;
    while (v6 == 0) {
        // 0x800019e0
        v6 = tryattach_ltimer_to_lamebus(v5, bus, busunit);
        v5++;
    }
    int32_t v7 = nextunit_lrandom; // 0x80001a00
    int32_t v8 = tryattach_lrandom_to_lamebus(v7, bus, busunit); // 0x80001a10
    v7++;
    while (v8 == 0) {
        // 0x80001a04
        v8 = tryattach_lrandom_to_lamebus(v7, bus, busunit);
        v7++;
    }
    int32_t v9 = nextunit_lhd; // 0x80001a24
    int32_t v10 = tryattach_lhd_to_lamebus(v9, bus, busunit); // 0x80001a34
    v9++;
    while (v10 == 0) {
        // 0x80001a28
        v10 = tryattach_lhd_to_lamebus(v9, bus, busunit);
        v9++;
    }
    int32_t v11 = nextunit_lser; // 0x80001a48
    int32_t v12 = tryattach_lser_to_lamebus(v11, bus, busunit); // 0x80001a58
    v11++;
    while (v12 == 0) {
        // 0x80001a4c
        v12 = tryattach_lser_to_lamebus(v11, bus, busunit);
        v11++;
    }
}