void kgets(char *buf,size_t maxlen)

{
  int ch;
  uint uVar1;
  
  uVar1 = 0;
  while ((ch = getch(), ch != 10 && (ch != 0xd))) {
    if ((ch - 0x20U < 0x5f) && (uVar1 < maxlen - 1)) {
      putch(ch);
      buf[uVar1] = (char)ch;
      uVar1 = uVar1 + 1;
    }
    else if (((ch == 8) || (ch == 0x7f)) && (uVar1 != 0)) {
      uVar1 = uVar1 - 1;
      backsp();
    }
    else {
      if (ch == 3) {
        putch(0x5e);
        putch(0x43);
        putch(10);
        uVar1 = 0;
        goto LAB_8000b2b0;
      }
      if (ch == 0x12) {
        buf[uVar1] = '\0';
        kprintf("^R\n%s",buf);
      }
      else if (ch == 0x15) {
        for (; uVar1 != 0; uVar1 = uVar1 - 1) {
          backsp();
        }
      }
      else if (ch == 0x17) {
        while ((uVar1 != 0 && (buf[uVar1 - 1] == ' '))) {
          backsp();
          uVar1 = uVar1 - 1;
        }
        while ((uVar1 != 0 && (buf[uVar1 - 1] != ' '))) {
          backsp();
          uVar1 = uVar1 - 1;
        }
      }
      else {
        beep();
      }
    }
  }
  putch(10);
LAB_8000b2b0:
  buf[uVar1] = '\0';
  return;
}