void duplicate_swap_pages(pid_t old_pid, pid_t new_pid)
{

    int result, i, j;
    paddr_t paddr;

    paddr = as_prepare_load(1);

    spinlock_acquire(&swap_lock);
    /* page must be in swap file */
    for (i = 0; i < ENTRIES; i++)
    {
        if (swap_table[i].pid == old_pid)
        {
            for (j = 0; j < ENTRIES; j++)
            {
                if (swap_table[j].pid == -1)
                {
                    swap_table[j].pid = new_pid;
                    swap_table[j].page = swap_table[i].page;
                    print_swap_internal();
                    spinlock_release(&swap_lock);

                    result = file_read_paddr(v, paddr, PAGE_SIZE, i * PAGE_SIZE);
                    if (result != PAGE_SIZE)
                    {
                        panic("Unable to read page from swap file");
                    }
                    result = file_write_paddr(v, paddr, PAGE_SIZE, j * PAGE_SIZE);
                    if (result != PAGE_SIZE)
                    {
                        panic("Unable to swap page out for fork");
                    }
                    spinlock_acquire(&swap_lock);

                    KASSERT(result >= 0);
                    break;
                }
            }
            if (j == ENTRIES)
            {
                panic("Swap file is full");
            }
        }
    }
    spinlock_release(&swap_lock);
    freeppages(paddr, paddr / PAGE_SIZE);
}