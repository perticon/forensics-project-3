void duplicate_swap_pages(int32_t old_pid, int32_t new_pid) {
    uint32_t v1 = as_prepare_load(1); // 0x8001def0
    spinlock_acquire(&swap_lock);
    int32_t v2 = 0; // 0x8001debc
    int32_t v3; // 0x8001debc
    while (true) {
      lab_0x8001df28:
        // 0x8001df28
        if (*(int32_t *)(8 * v2 + (int32_t)&swap_table) == old_pid) {
            int32_t v4 = 0; // 0x8001e028
            int32_t v5; // 0x8001debc
            int32_t v6; // 0x8001dfd8
            int32_t * v7; // 0x8001debc
            int32_t v8; // 0x8001e034
            int32_t * v9; // 0x8001df48
            if (*(int32_t *)(8 * v4 + (int32_t)&swap_table) == -1) {
                // 0x8001df58
                *v9 = new_pid;
                *(int32_t *)(v8 + (int32_t)&swap_table + 4) = *v7;
                print_swap_internal();
                spinlock_release(&swap_lock);
                if (file_read_paddr(v, v1, 0x1000, (int64_t)&g41) != 0x1000) {
                    // 0x8001dfb4
                    panic("Unable to read page from swap file");
                }
                // 0x8001dfc0
                v6 = file_write_paddr(v, v1, 0x1000, (int64_t)&g41);
                if (v6 == 0x1000) {
                    // 0x8001dff4
                    spinlock_acquire(&swap_lock);
                    v3 = v5;
                    goto lab_0x8001e03c;
                }
                // 0x8001dff4
                panic("Unable to swap page out for fork");
                spinlock_acquire(&swap_lock);
                if (v6 > -1) {
                    // break -> 0x8001e03c
                    break;
                }
                // 0x8001e008
                badassert("result >= 0", "../../vm/swapfile.c", 693, "duplicate_swap_pages");
            }
            // 0x8001e028
            v4++;
            v3 = v4;
            while (v4 < 2304) {
                // 0x8001df44
                if (*(int32_t *)(8 * v4 + (int32_t)&swap_table) == -1) {
                    // 0x8001df58
                    *v9 = new_pid;
                    *(int32_t *)(v8 + (int32_t)&swap_table + 4) = *v7;
                    print_swap_internal();
                    spinlock_release(&swap_lock);
                    if (file_read_paddr(v, v1, 0x1000, (int64_t)&g41) != 0x1000) {
                        // 0x8001dfb4
                        panic("Unable to read page from swap file");
                    }
                    // 0x8001dfc0
                    v6 = file_write_paddr(v, v1, 0x1000, (int64_t)&g41);
                    if (v6 == 0x1000) {
                        // 0x8001dff4
                        spinlock_acquire(&swap_lock);
                        v3 = v5;
                        goto lab_0x8001e03c;
                    }
                    // 0x8001dff4
                    panic("Unable to swap page out for fork");
                    spinlock_acquire(&swap_lock);
                    v3 = v5;
                    if (v6 > -1) {
                        // break -> 0x8001e03c
                        break;
                    }
                    // 0x8001e008
                    badassert("result >= 0", "../../vm/swapfile.c", 693, "duplicate_swap_pages");
                }
                // 0x8001e028
                v4++;
                v3 = v4;
            }
            goto lab_0x8001e03c;
        } else {
            goto lab_0x8001e058;
        }
    }
  lab_0x8001e064:
    // 0x8001e064
    spinlock_release(&swap_lock);
    freeppages(v1, v1 / 0x1000);
  lab_0x8001e058:;
    int32_t v10 = v2 + 1;
    v2 = v10;
    if (v10 >= 2304) {
        // break -> 0x8001e064
        goto lab_0x8001e064;
    }
    goto lab_0x8001df28;
  lab_0x8001e03c:
    // 0x8001e03c
    if (v3 == 2304) {
        // 0x8001e044
        panic("Swap file is full");
    }
    goto lab_0x8001e058;
}