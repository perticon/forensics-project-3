void duplicate_swap_pages(pid_t old_pid,pid_t new_pid)

{
  bool bVar1;
  paddr_t buf_ptr;
  int iVar2;
  int iVar3;
  int iVar4;
  
  buf_ptr = as_prepare_load(1);
  spinlock_acquire(&swap_lock);
  iVar4 = 0;
  do {
    while( true ) {
      if (0x8ff < iVar4) {
        spinlock_release(&swap_lock);
        freeppages(buf_ptr,buf_ptr >> 0xc);
        return;
      }
      iVar3 = 0;
      if (swap_table[iVar4].pid == old_pid) break;
      iVar4 = iVar4 + 1;
    }
    bVar1 = true;
    while (bVar1) {
      if (swap_table[iVar3].pid == -1) {
        swap_table[iVar3].pid = new_pid;
        swap_table[iVar3].page = swap_table[iVar4].page;
        print_swap_internal();
        spinlock_release(&swap_lock);
        iVar2 = file_read_paddr(v,buf_ptr,0x1000,(longlong)(iVar4 << 0xc));
        if (iVar2 != 0x1000) {
                    /* WARNING: Subroutine does not return */
          panic("Unable to read page from swap file");
        }
        iVar2 = file_write_paddr(v,buf_ptr,0x1000,(longlong)(iVar3 << 0xc));
        if (iVar2 != 0x1000) {
                    /* WARNING: Subroutine does not return */
          panic("Unable to swap page out for fork");
        }
        spinlock_acquire(&swap_lock);
        break;
      }
      iVar3 = iVar3 + 1;
      bVar1 = iVar3 < 0x900;
    }
    iVar4 = iVar4 + 1;
    if (iVar3 == 0x900) {
                    /* WARNING: Subroutine does not return */
      panic("Swap file is full");
    }
  } while( true );
}