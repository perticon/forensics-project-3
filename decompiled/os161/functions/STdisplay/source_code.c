void STdisplay(ST st)
{
    int i;

    for (i = 0; i < st->M; i++)
    {
        kprintf("st->heads[%d]: %d", i, st->heads[i]->item.key.kaddr);
        visitR(st->heads[i], st->z);
        kprintf("\n");
    }

    return;
}