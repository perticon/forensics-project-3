int32_t kmallocstress(int32_t nargs, char ** args) {
    int32_t * v1 = sem_create("kmallocstress", 0); // 0x80010ed8
    if (v1 == NULL) {
        // 0x80010ee4
        panic("kmallocstress: sem_create failed\n");
    }
    // 0x80010ef0
    kprintf("Starting kmalloc stress test...\n");
    int32_t v2 = 0;
    int32_t v3 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2); // 0x80010f24
    int32_t v4 = v3; // 0x80010f2c
    int32_t v5; // 0x80010f24
    if (v3 != 0) {
        panic("kmallocstress: thread_fork failed: %s\n", strerror(v4));
        v5 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2);
        v4 = v5;
        while (v5 != 0) {
            // 0x80010f30
            panic("kmallocstress: thread_fork failed: %s\n", strerror(v4));
            v5 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2);
            v4 = v5;
        }
    }
    int32_t v6 = v2 + 1; // 0x80010f2c
    int32_t v7 = 0; // 0x80010f54
    while (v6 < 8) {
        // 0x80010f14
        v2 = v6;
        v3 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2);
        v4 = v3;
        if (v3 != 0) {
            panic("kmallocstress: thread_fork failed: %s\n", strerror(v4));
            v5 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2);
            v4 = v5;
            while (v5 != 0) {
                // 0x80010f30
                panic("kmallocstress: thread_fork failed: %s\n", strerror(v4));
                v5 = thread_fork("kmallocstress", NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 3072), (char *)v1, v2);
                v4 = v5;
            }
        }
        // 0x80010f4c
        v6 = v2 + 1;
        v7 = 0;
    }
    P(v1);
    int32_t v8 = v7 + 1; // 0x80010f68
    v7 = v8;
    while (v8 < 8) {
        // 0x80010f60
        P(v1);
        v8 = v7 + 1;
        v7 = v8;
    }
    // 0x80010f78
    sem_destroy(v1);
    kprintf("kmalloc stress test done\n");
    return 0;
}