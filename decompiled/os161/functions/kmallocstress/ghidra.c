int kmallocstress(int nargs,char **args)

{
  semaphore *sem;
  char *pcVar1;
  ulong data2;
  int iVar2;
  
  sem = sem_create("kmallocstress",0);
  if (sem == (semaphore *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("kmallocstress: sem_create failed\n");
  }
  kprintf("Starting kmalloc stress test...\n");
  data2 = 0;
  do {
    if (7 < (int)data2) {
      for (iVar2 = 0; iVar2 < 8; iVar2 = iVar2 + 1) {
        P(sem);
      }
      sem_destroy(sem);
      kprintf("kmalloc stress test done\n");
      return 0;
    }
    iVar2 = thread_fork("kmallocstress",(proc *)0x0,kmallocthread,sem,data2);
    data2 = data2 + 1;
  } while (iVar2 == 0);
  pcVar1 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
  panic("kmallocstress: thread_fork failed: %s\n",pcVar1);
}