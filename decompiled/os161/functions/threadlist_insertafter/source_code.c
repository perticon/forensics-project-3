threadlist_insertafter(struct threadlist *tl,
		       struct thread *onlist, struct thread *addee)
{
	threadlist_insertafternode(&onlist->t_listnode, addee);
	tl->tl_count++;
}