config_lscreen(struct lscreen_softc *sc, int lscreenno)
{
	uint32_t val;

	(void)lscreenno;

	spinlock_init(&sc->ls_lock);

	/*
	 * Enable interrupting.
	 */

	bus_write_register(sc->ls_busdata, sc->ls_buspos,
			   LSCR_REG_RIRQ, LSCR_IRQ_ENABLE);

	/*
	 * Get screen size.
	 */
	val = bus_read_register(sc->ls_busdata, sc->ls_buspos,
				LSCR_REG_SIZE);
	splitxy(val, &sc->ls_width, &sc->ls_height);

	/*
	 * Get cursor position.
	 */
	val = bus_read_register(sc->ls_busdata, sc->ls_buspos,
				LSCR_REG_POSN);
	splitxy(val, &sc->ls_cx, &sc->ls_cy);

	/*
	 * Get a pointer to the memory-mapped screen area.
	 */
	sc->ls_screen = bus_map_area(sc->ls_busdata, sc->ls_buspos,
				     LSCR_SCREEN);

	return 0;
}