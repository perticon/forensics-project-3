int32_t vfs_readlink(char * path, int32_t * uio) {
    // 0x8001a510
    int32_t * vn; // bp-16, 0x8001a510
    int32_t v1 = vfs_lookup(path, (int32_t *)&vn); // 0x8001a524
    int32_t result = v1; // 0x8001a52c
    if (v1 == 0) {
        // 0x8001a530
        vnode_check(vn, "readlink");
        result = *(int32_t *)(*(int32_t *)((int32_t)vn + 20) + 16);
        vnode_decref(vn);
    }
    // 0x8001a56c
    return result;
}