int vfs_readlink(char *path,uio *uio)

{
  int iVar1;
  vnode *vn;
  
  iVar1 = vfs_lookup(path,&vn);
  if (iVar1 == 0) {
    vnode_check(vn,"readlink");
    iVar1 = (*vn->vn_ops->vop_readlink)(vn,uio);
    vnode_decref(vn);
  }
  return iVar1;
}