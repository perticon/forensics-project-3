void thread_checkstack_init(thread *thread)

{
  *(undefined4 *)thread->t_stack = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 4) = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 8) = 0xbaadf00d;
  *(undefined4 *)((int)thread->t_stack + 0xc) = 0xbaadf00d;
  return;
}