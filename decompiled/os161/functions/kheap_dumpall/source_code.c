kheap_dumpall(void)
{
#ifdef LABELS
	unsigned i;

	/* print the whole thing with interrupts off */
	spinlock_acquire(&kmalloc_spinlock);
	for (i=0; i<=mallocgeneration; i++) {
		dump_subpages(i);
	}
	spinlock_release(&kmalloc_spinlock);
#else
	kprintf("Enable LABELS in kmalloc.c to use this functionality.\n");
#endif
}