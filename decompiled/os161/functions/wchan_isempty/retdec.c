bool wchan_isempty(int32_t * wc, int32_t * lk) {
    // 0x800167bc
    if (!spinlock_do_i_hold(lk)) {
        // 0x800167dc
        badassert("spinlock_do_i_hold(lk)", "../../thread/thread.c", 1097, "wchan_isempty");
    }
    // 0x800167f8
    return threadlist_isempty((int32_t *)((int32_t)wc + 4));
}