bool wchan_isempty(wchan *wc,spinlock *lk)

{
  undefined3 extraout_var;
  bool bVar1;
  
  bVar1 = spinlock_do_i_hold(lk);
  if (CONCAT31(extraout_var,bVar1) == 0) {
    badassert("spinlock_do_i_hold(lk)","../../thread/thread.c",0x449,"wchan_isempty");
  }
  bVar1 = threadlist_isempty(&wc->wc_threads);
  return bVar1;
}