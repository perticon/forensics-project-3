semfs_create(void)
{
	struct semfs *semfs;

	semfs = kmalloc(sizeof(*semfs));
	if (semfs == NULL) {
		goto fail_total;
	}

	semfs->semfs_tablelock = lock_create("semfs_table");
	if (semfs->semfs_tablelock == NULL) {
		goto fail_semfs;
	}
	semfs->semfs_vnodes = vnodearray_create();
	if (semfs->semfs_vnodes == NULL) {
		goto fail_tablelock;
	}
	semfs->semfs_sems = semfs_semarray_create();
	if (semfs->semfs_sems == NULL) {
		goto fail_vnodes;
	}

	semfs->semfs_dirlock = lock_create("semfs_dir");
	if (semfs->semfs_dirlock == NULL) {
		goto fail_sems;
	}
	semfs->semfs_dents = semfs_direntryarray_create();
	if (semfs->semfs_dents == NULL) {
		goto fail_dirlock;
	}

	semfs->semfs_absfs.fs_data = semfs;
	semfs->semfs_absfs.fs_ops = &semfs_fsops;
	return semfs;

 fail_dirlock:
	lock_destroy(semfs->semfs_dirlock);
 fail_sems:
	semfs_semarray_destroy(semfs->semfs_sems);
 fail_vnodes:
	vnodearray_destroy(semfs->semfs_vnodes);
 fail_tablelock:
	lock_destroy(semfs->semfs_tablelock);
 fail_semfs:
	kfree(semfs);
 fail_total:
	return NULL;
}