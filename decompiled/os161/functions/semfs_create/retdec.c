int32_t * semfs_create(void) {
    char * v1 = kmalloc(28); // 0x80005730
    if (v1 == NULL) {
        // 0x80005860
        return NULL;
    }
    int32_t v2 = (int32_t)v1; // 0x80005730
    int32_t * v3 = lock_create("semfs_table"); // 0x80005744
    int32_t * v4 = (int32_t *)(v2 + 8); // 0x8000574c
    *v4 = (int32_t)v3;
    if (v3 == NULL) {
        // 0x8000584c
        kfree(v1);
        // 0x80005860
        return NULL;
    }
    char * v5 = kmalloc(12); // 0x80005754
    if (v5 == NULL) {
        // 0x80005770
        *(int32_t *)(v2 + 12) = 0;
    } else {
        // 0x80005760
        array_init((int32_t *)v5);
        int32_t * v6 = (int32_t *)(v2 + 12); // 0x8000576c
        *v6 = (int32_t)v5;
        char * v7 = kmalloc(12); // 0x80005784
        if (v7 == NULL) {
            // 0x800057a0
            *(int32_t *)(v2 + 16) = 0;
        } else {
            // 0x80005790
            array_init((int32_t *)v7);
            int32_t * v8 = (int32_t *)(v2 + 16); // 0x8000579c
            *v8 = (int32_t)v7;
            int32_t * v9 = lock_create("semfs_dir"); // 0x800057b8
            int32_t * v10 = (int32_t *)(v2 + 20); // 0x800057c0
            *v10 = (int32_t)v9;
            if (v9 != NULL) {
                char * v11 = kmalloc(12); // 0x800057c8
                if (v11 != NULL) {
                    // 0x800057d4
                    array_init((int32_t *)v11);
                    *(int32_t *)(v2 + 24) = (int32_t)v11;
                    *(int32_t *)v1 = v2;
                    *(int32_t *)(v2 + 4) = (int32_t)&semfs_fsops;
                    // 0x80005860
                    return (int32_t *)v1;
                }
                // 0x800057e4
                *(int32_t *)(v2 + 24) = 0;
                lock_destroy((int32_t *)*v10);
            }
            int32_t v12 = *v8; // 0x80005818
            array_cleanup((int32_t *)v12);
            kfree((char *)v12);
        }
        int32_t v13 = *v6; // 0x8000582c
        array_cleanup((int32_t *)v13);
        kfree((char *)v13);
    }
    // 0x80005840
    lock_destroy((int32_t *)*v4);
    // 0x8000584c
    kfree(v1);
    // 0x80005860
    return NULL;
}