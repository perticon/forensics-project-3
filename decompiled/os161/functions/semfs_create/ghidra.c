semfs * semfs_create(void)

{
  semfs *ptr;
  lock *plVar1;
  array *paVar2;
  
  ptr = (semfs *)kmalloc(0x1c);
  if (ptr != (semfs *)0x0) {
    plVar1 = lock_create("semfs_table");
    ptr->semfs_tablelock = plVar1;
    if (plVar1 != (lock *)0x0) {
      paVar2 = (array *)kmalloc(0xc);
      if (paVar2 == (array *)0x0) {
        paVar2 = (array *)0x0;
        ptr->semfs_vnodes = (vnodearray *)0x0;
      }
      else {
        array_init(paVar2);
        ptr->semfs_vnodes = (vnodearray *)paVar2;
      }
      if (paVar2 != (array *)0x0) {
        paVar2 = (array *)kmalloc(0xc);
        if (paVar2 == (array *)0x0) {
          paVar2 = (array *)0x0;
          ptr->semfs_sems = (semfs_semarray *)0x0;
        }
        else {
          array_init(paVar2);
          ptr->semfs_sems = (semfs_semarray *)paVar2;
        }
        if (paVar2 != (array *)0x0) {
          plVar1 = lock_create("semfs_dir");
          ptr->semfs_dirlock = plVar1;
          if (plVar1 != (lock *)0x0) {
            paVar2 = (array *)kmalloc(0xc);
            if (paVar2 == (array *)0x0) {
              paVar2 = (array *)0x0;
              ptr->semfs_dents = (semfs_direntryarray *)0x0;
            }
            else {
              array_init(paVar2);
              ptr->semfs_dents = (semfs_direntryarray *)paVar2;
            }
            if (paVar2 != (array *)0x0) {
              (ptr->semfs_absfs).fs_data = ptr;
              (ptr->semfs_absfs).fs_ops = &semfs_fsops;
              return ptr;
            }
            lock_destroy(ptr->semfs_dirlock);
          }
          paVar2 = (array *)ptr->semfs_sems;
          array_cleanup(paVar2);
          kfree(paVar2);
        }
        paVar2 = (array *)ptr->semfs_vnodes;
        array_cleanup(paVar2);
        kfree(paVar2);
      }
      lock_destroy(ptr->semfs_tablelock);
    }
    kfree(ptr);
  }
  return (semfs *)0x0;
}