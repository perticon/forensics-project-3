vopfail_string_nosys(struct vnode *vn, const char *name)
{
	(void)vn;
	(void)name;
	return ENOSYS;
}