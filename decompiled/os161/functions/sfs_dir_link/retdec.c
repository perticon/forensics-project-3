int32_t sfs_dir_link(int32_t * sv, char * name, int32_t ino, int32_t * slot) {
    int32_t emptyslot = -1; // bp-96, 0x80007bfc
    int32_t result = sfs_dir_findname(sv, name, NULL, NULL, &emptyslot); // 0x80007c10
    switch (result) {
        case 0: {
            // 0x80007c9c
            return 22;
        }
        case 19: {
            // 0x80007c2c
            if (strlen(name) >= 60) {
                // 0x80007c9c
                return 7;
            }
            // break -> 0x80007c44
            break;
        }
        default: {
            // 0x80007c9c
            return result;
        }
    }
    // 0x80007c44
    if (emptyslot <= 0xffffffff) {
        // 0x80007c54
        emptyslot = sfs_dir_nentries(sv);
    }
    // 0x80007c64
    int32_t sd; // bp-92, 0x80007bd0
    bzero((char *)&sd, 64);
    sd = ino;
    int32_t v1; // bp-88, 0x80007bd0
    strcpy((char *)&v1, name);
    if (slot != NULL) {
        // 0x80007c84
        *slot = emptyslot;
    }
    // 0x80007c9c
    return sfs_writedir(sv, emptyslot, &sd);
}