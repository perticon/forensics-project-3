int sfs_dir_link(sfs_vnode *sv,char *name,uint32_t ino,int *slot)

{
  bool bVar1;
  int iVar2;
  size_t sVar3;
  int emptyslot;
  sfs_direntry sd;
  
  emptyslot = -1;
  iVar2 = sfs_dir_findname(sv,name,(uint32_t *)0x0,(int *)0x0,&emptyslot);
  if (((iVar2 == 0) || (iVar2 == 0x13)) && (bVar1 = iVar2 != 0, iVar2 = 0x16, bVar1)) {
    sVar3 = strlen(name);
    iVar2 = 7;
    if (sVar3 + 1 < 0x3d) {
      if (emptyslot < 0) {
        emptyslot = sfs_dir_nentries(sv);
      }
      bzero(&sd,0x40);
      sd.sfd_ino = ino;
      strcpy(sd.sfd_name,name);
      if (slot != (int *)0x0) {
        *slot = emptyslot;
      }
      iVar2 = sfs_writedir(sv,emptyslot,&sd);
    }
  }
  return iVar2;
}