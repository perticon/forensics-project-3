void putch(int ch)

{
  int unaff_s7;
  
  if (the_console == (con_softc *)0x0) {
    putch_delayed(ch);
  }
  else if (((*(char *)(unaff_s7 + 0x58) == '\0') && (*(int *)(unaff_s7 + 0x5c) < 1)) &&
          (*(int *)(*(int *)(unaff_s7 + 0x50) + 0x30) == 0)) {
    putch_intr(the_console,ch);
  }
  else {
    putch_polled(the_console,ch);
  }
  return;
}