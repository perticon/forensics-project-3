void putch(int32_t ch) {
    // 0x80001d5c
    if (the_console == NULL) {
        // 0x80001d7c
        putch_delayed(ch);
        // 0x80001ddc
        return;
    }
    // 0x80001d8c
    int32_t v1; // 0x80001d5c
    if (*(char *)(v1 + 88) != 0 || *(int32_t *)(v1 + 92) > 0 || *(int32_t *)(*(int32_t *)(v1 + 80) + 48) != 0) {
        // 0x80001dc4
        putch_polled(the_console, ch);
        // 0x80001ddc
        return;
    }
    // 0x80001dd4
    putch_intr(the_console, ch);
}