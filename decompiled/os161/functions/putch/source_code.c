putch(int ch)
{
	struct con_softc *cs = the_console;

	if (cs==NULL) {
		putch_delayed(ch);
	}
	else if (curthread->t_in_interrupt ||
		 curthread->t_curspl > 0 ||
		 curcpu->c_spinlocks > 0) {
		putch_polled(cs, ch);
	}
	else {
		putch_intr(cs, ch);
	}
}