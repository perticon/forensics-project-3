void mainbus_interrupt(int32_t * tf) {
    // 0x800209dc
    int32_t v1; // 0x800209dc
    if (*(int32_t *)(v1 + 92) <= 0) {
        // 0x800209f8
        badassert("curthread->t_curspl > 0", "../../arch/sys161/dev/lamebus_machdep.c", 304, "mainbus_interrupt");
    }
    // 0x80020a14
    lamebus_interrupt(lamebus);
    interprocessor_interrupt();
    lamebus_clear_ipi(lamebus, (int32_t *)*(int32_t *)(v1 + 80));
}