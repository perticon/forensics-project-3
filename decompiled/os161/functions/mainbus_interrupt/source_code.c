mainbus_interrupt(struct trapframe *tf)
{
	uint32_t cause;
	bool seen = false;

	/* interrupts should be off */
	KASSERT(curthread->t_curspl > 0);

	cause = tf->tf_cause;
	if (cause & LAMEBUS_IRQ_BIT) {
		lamebus_interrupt(lamebus);
		seen = true;
	}
	if (cause & LAMEBUS_IPI_BIT) {
		interprocessor_interrupt();
		lamebus_clear_ipi(lamebus, curcpu);
		seen = true;
	}
	if (cause & MIPS_TIMER_BIT) {
		/* Reset the timer (this clears the interrupt) */
		mips_timer_set(CPU_FREQUENCY / HZ);
		/* and call hardclock */
		hardclock();
		seen = true;
	}

	if (!seen) {
		if ((cause & CCA_IRQS) == 0) {
			/*
			 * Don't panic here; this can happen if an
			 * interrupt line asserts (very) briefly and
			 * turns off again before we get as far as
			 * reading the cause register.  This was
			 * actually seen... once.
			 */
		}
		else {
			/*
			 * But if we get an interrupt on an interrupt
			 * line that's not supposed to be wired up,
			 * complain.
			 */
			panic("Unknown interrupt; cause register is %08x\n",
			      cause);
		}
	}
}