void mainbus_interrupt(trapframe *tf)

{
  bool bVar1;
  bool bVar2;
  bool bVar3;
  char *pcVar4;
  uint uVar5;
  int unaff_s7;
  
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    pcVar4 = "curthread->t_curspl > 0";
    badassert("curthread->t_curspl > 0","../../arch/sys161/dev/lamebus_machdep.c",0x130,
              "mainbus_interrupt");
    tf = (trapframe *)pcVar4;
  }
  uVar5 = tf->tf_cause;
  bVar1 = (uVar5 & 0x400) == 0;
  if (!bVar1) {
    lamebus_interrupt(lamebus);
  }
  bVar2 = (uVar5 & 0x800) != 0;
  if (bVar2) {
    interprocessor_interrupt();
    lamebus_clear_ipi(lamebus,*(cpu **)(unaff_s7 + 0x50));
  }
  bVar3 = (uVar5 & 0x8000) != 0;
  if (bVar3) {
    mips_timer_set(250000);
    hardclock();
  }
  if ((!bVar3 && (!bVar2 && bVar1)) && ((uVar5 & 0xff00) != 0)) {
                    /* WARNING: Subroutine does not return */
    panic("Unknown interrupt; cause register is %08x\n",uVar5);
  }
  return;
}