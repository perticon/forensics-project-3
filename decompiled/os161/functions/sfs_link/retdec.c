int32_t sfs_link(int32_t * dir, char * name, int32_t * file) {
    int32_t v1 = (int32_t)file;
    int32_t v2 = (int32_t)dir;
    int32_t v3 = *(int32_t *)(v2 + 16); // 0x80009948
    int32_t v4 = *(int32_t *)(v1 + 16); // 0x8000994c
    int32_t v5 = (int32_t)name; // 0x80009960
    if (*(int32_t *)(v1 + 12) != *(int32_t *)(v2 + 12)) {
        // 0x80009964
        badassert("file->vn_fs == dir->vn_fs", "../../fs/sfs/sfs_vnops.c", 362, "sfs_link");
        v5 = (int32_t)"../../fs/sfs/sfs_vnops.c";
    }
    // 0x80009980
    vfs_biglock_acquire();
    if (*(int16_t *)(v4 + 28) == 2) {
        // 0x80009998
        vfs_biglock_release();
        // 0x800099f0
        return 8;
    }
    int32_t v6 = *(int32_t *)(v4 + 536); // 0x800099ac
    int32_t v7 = sfs_dir_link((int32_t *)v3, (char *)v5, v6, NULL); // 0x800099b4
    int32_t result; // 0x80009934
    if (v7 == 0) {
        int16_t * v8 = (int16_t *)(v4 + 30); // 0x800099d0
        *v8 = *v8 + 1;
        *(char *)(v4 + 540) = 1;
        vfs_biglock_release();
        result = 0;
    } else {
        // 0x800099c0
        vfs_biglock_release();
        result = v7;
    }
    // 0x800099f0
    return result;
}