int sfs_link(vnode *dir,char *name,vnode *file)

{
  int iVar1;
  void *pvVar2;
  sfs_vnode *sv;
  
  sv = (sfs_vnode *)dir->vn_data;
  pvVar2 = file->vn_data;
  if (file->vn_fs != dir->vn_fs) {
    name = "../../fs/sfs/sfs_vnops.c";
    badassert("file->vn_fs == dir->vn_fs","../../fs/sfs/sfs_vnops.c",0x16a,"sfs_link");
  }
  vfs_biglock_acquire();
  if (*(short *)((int)pvVar2 + 0x1c) == 2) {
    vfs_biglock_release();
    iVar1 = 8;
  }
  else {
    iVar1 = sfs_dir_link(sv,name,*(uint32_t *)((int)pvVar2 + 0x218),(int *)0x0);
    if (iVar1 == 0) {
      *(short *)((int)pvVar2 + 0x1e) = *(short *)((int)pvVar2 + 0x1e) + 1;
      *(undefined *)((int)pvVar2 + 0x21c) = 1;
      vfs_biglock_release();
      iVar1 = 0;
    }
    else {
      vfs_biglock_release();
    }
  }
  return iVar1;
}