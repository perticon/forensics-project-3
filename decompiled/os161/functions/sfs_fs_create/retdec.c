int32_t * sfs_fs_create(void) {
    char * v1 = kmalloc(540); // 0x80007f34
    if (v1 == NULL) {
        // 0x80007fac
        return NULL;
    }
    int32_t v2 = (int32_t)v1; // 0x80007f34
    *(int32_t *)v1 = v2;
    *(int32_t *)(v2 + 4) = (int32_t)&sfs_fsops;
    *(char *)(v2 + 520) = 0;
    *(int32_t *)(v2 + 524) = 0;
    char * v3 = kmalloc(12); // 0x80007f5c
    int32_t * result; // 0x80007f20
    if (v3 == NULL) {
        // 0x80007f78
        *(int32_t *)(v2 + 528) = 0;
        kfree(v1);
        result = NULL;
    } else {
        // 0x80007f68
        array_init((int32_t *)v3);
        *(int32_t *)(v2 + 528) = (int32_t)v3;
        *(int32_t *)(v2 + 532) = 0;
        *(char *)(v2 + 536) = 0;
        result = (int32_t *)v1;
    }
    // 0x80007fac
    return result;
}