sfs_fs * sfs_fs_create(void)

{
  sfs_fs *ptr;
  array *a;
  
  ptr = (sfs_fs *)kmalloc(0x21c);
  if (ptr == (sfs_fs *)0x0) {
    ptr = (sfs_fs *)0x0;
  }
  else {
    (ptr->sfs_absfs).fs_data = ptr;
    (ptr->sfs_absfs).fs_ops = &sfs_fsops;
    ptr->sfs_superdirty = false;
    ptr->sfs_device = (device *)0x0;
    a = (array *)kmalloc(0xc);
    if (a == (array *)0x0) {
      a = (array *)0x0;
      ptr->sfs_vnodes = (vnodearray *)0x0;
    }
    else {
      array_init(a);
      ptr->sfs_vnodes = (vnodearray *)a;
    }
    if (a == (array *)0x0) {
      kfree(ptr);
      ptr = (sfs_fs *)0x0;
    }
    else {
      ptr->sfs_freemap = (bitmap *)0x0;
      ptr->sfs_freemapdirty = false;
    }
  }
  return ptr;
}