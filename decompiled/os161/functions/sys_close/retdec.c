int32_t sys_close(uint32_t fd) {
    // 0x8000d82c
    if (fd >= 129) {
        // 0x8000d8a0
        return -1;
    }
    // 0x8000d838
    int32_t v1; // 0x8000d82c
    int32_t * v2 = (int32_t *)(4 * fd + 96 + *(int32_t *)(v1 + 84)); // 0x8000d848
    int32_t v3 = *v2; // 0x8000d848
    if (v3 == 0) {
        // 0x8000d8a0
        return -1;
    }
    // 0x8000d858
    *v2 = 0;
    int32_t * v4 = (int32_t *)(v3 + 16); // 0x8000d864
    int32_t v5 = *v4 - 1; // 0x8000d86c
    *v4 = v5;
    if (v5 != 0) {
        // 0x8000d8a0
        return 0;
    }
    int32_t * v6 = (int32_t *)v3; // 0x8000d878
    int32_t v7 = *v6; // 0x8000d878
    *v6 = 0;
    if (v7 == 0) {
        // 0x8000d8a0
        return -1;
    }
    // 0x8000d888
    vfs_close((int32_t *)v7);
    return 0;
}