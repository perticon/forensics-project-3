int sys_close(int fd)
{
  struct openfile *of = NULL;
  struct vnode *vn;

  if (fd < 0 || fd > OPEN_MAX)
    return -1;
  of = curproc->fileTable[fd];
  if (of == NULL)
    return -1;
  curproc->fileTable[fd] = NULL;

  if (--of->countRef > 0)
    return 0; // just decrement ref cnt
  vn = of->vn;
  of->vn = NULL;
  if (vn == NULL)
    return -1;

  vfs_close(vn);
  return 0;
}