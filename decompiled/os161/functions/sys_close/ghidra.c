int sys_close(int fd)

{
  vnode **ppvVar1;
  vnode *pvVar2;
  int unaff_s7;
  
  if (0x80 < (uint)fd) {
    return -1;
  }
  ppvVar1 = *(vnode ***)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4);
  if (ppvVar1 == (vnode **)0x0) {
    return -1;
  }
  *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + (fd + 0x18) * 4) = 0;
  pvVar2 = (vnode *)((int)&ppvVar1[4][-1].vn_ops + 3);
  ppvVar1[4] = pvVar2;
  if (pvVar2 != (vnode *)0x0) {
    return 0;
  }
  pvVar2 = *ppvVar1;
  *ppvVar1 = (vnode *)0x0;
  if (pvVar2 != (vnode *)0x0) {
    vfs_close(pvVar2);
    return 0;
  }
  return -1;
}