sfs_unmount(struct fs *fs)
{
	struct sfs_fs *sfs = fs->fs_data;

	vfs_biglock_acquire();

	/* Do we have any files open? If so, can't unmount. */
	if (vnodearray_num(sfs->sfs_vnodes) > 0) {
		vfs_biglock_release();
		return EBUSY;
	}

	/* We should have just had sfs_sync called. */
	KASSERT(sfs->sfs_superdirty == false);
	KASSERT(sfs->sfs_freemapdirty == false);

	/* The vfs layer takes care of the device for us */
	sfs->sfs_device = NULL;

	/* Destroy the fs object; once we start nuking stuff we can't fail. */
	sfs_fs_destroy(sfs);

	/* nothing else to do */
	vfs_biglock_release();
	return 0;
}