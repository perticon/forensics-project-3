int sfs_unmount(fs *fs)

{
  int iVar1;
  sfs_fs *sfs;
  
  sfs = (sfs_fs *)fs->fs_data;
  vfs_biglock_acquire();
  if ((sfs->sfs_vnodes->arr).num == 0) {
    if (sfs->sfs_superdirty != false) {
      badassert("sfs->sfs_superdirty == false","../../fs/sfs/sfs_fsops.c",0x122,"sfs_unmount");
    }
    if (sfs->sfs_freemapdirty != false) {
      badassert("sfs->sfs_freemapdirty == false","../../fs/sfs/sfs_fsops.c",0x123,"sfs_unmount");
    }
    sfs->sfs_device = (device *)0x0;
    sfs_fs_destroy(sfs);
    vfs_biglock_release();
    iVar1 = 0;
  }
  else {
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}