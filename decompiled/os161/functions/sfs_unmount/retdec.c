int32_t sfs_unmount(int32_t * fs) {
    int32_t v1 = (int32_t)fs;
    vfs_biglock_acquire();
    if (*(int32_t *)(*(int32_t *)(v1 + 528) + 4) != 0) {
        // 0x800083b4
        vfs_biglock_release();
        // 0x80008434
        return 27;
    }
    // 0x800083c4
    if (*(char *)(v1 + 520) != 0) {
        // 0x800083d4
        badassert("sfs->sfs_superdirty == false", "../../fs/sfs/sfs_fsops.c", 290, "sfs_unmount");
    }
    // 0x800083f0
    if (*(char *)(v1 + 536) != 0) {
        // 0x80008400
        badassert("sfs->sfs_freemapdirty == false", "../../fs/sfs/sfs_fsops.c", 291, "sfs_unmount");
    }
    // 0x8000841c
    *(int32_t *)(v1 + 524) = 0;
    sfs_fs_destroy(fs);
    vfs_biglock_release();
    // 0x80008434
    return 0;
}