ltimer_softc * attach_ltimer_to_lamebus(int ltimerno,lamebus_softc *sc)

{
  uint32_t slot;
  ltimer_softc *devdata;
  
  slot = lamebus_probe(sc,1,2,1,(uint32_t *)0x0);
  if ((int)slot < 0) {
    devdata = (ltimer_softc *)0x0;
  }
  else {
    devdata = (ltimer_softc *)kmalloc(0x10);
    if (devdata == (ltimer_softc *)0x0) {
      devdata = (ltimer_softc *)0x0;
    }
    else {
      devdata->lt_bus = sc;
      devdata->lt_buspos = slot;
      lamebus_mark(sc,slot);
      lamebus_attach_interrupt(sc,slot,devdata,ltimer_irq);
    }
  }
  return devdata;
}