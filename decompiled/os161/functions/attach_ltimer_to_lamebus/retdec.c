int32_t * attach_ltimer_to_lamebus(int32_t ltimerno, int32_t * sc) {
    int32_t v1 = lamebus_probe(sc, 1, 2, 1, NULL); // 0x80004f0c
    if (v1 < 0) {
        // 0x80004f68
        return NULL;
    }
    char * v2 = kmalloc(16); // 0x80004f1c
    int32_t * result = NULL; // 0x80004f24
    if (v2 != NULL) {
        int32_t v3 = (int32_t)v2; // 0x80004f1c
        *(int32_t *)(v3 + 8) = (int32_t)sc;
        *(int32_t *)(v3 + 12) = v1;
        lamebus_mark(sc, v1);
        lamebus_attach_interrupt(sc, v1, v2, (void (*)(char *))-0x7fffb008);
        result = (int32_t *)v2;
    }
    // 0x80004f68
    return result;
}