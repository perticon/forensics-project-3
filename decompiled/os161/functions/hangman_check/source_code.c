hangman_check(const struct hangman_lockable *start,
	      const struct hangman_actor *target)
{
	const struct hangman_actor *cur;

	cur = start->l_holding;
	while (cur != NULL) {
		if (cur == target) {
			goto found;
		}
		if (cur->a_waiting == NULL) {
			break;
		}
		cur = cur->a_waiting->l_holding;
	}
	return;

 found:
	/*
	 * None of this can change while we print it (that's the point
	 * of it being a deadlock) so drop hangman_lock while
	 * printing; otherwise we can come back via kprintf_spinlock
	 * and that makes a mess. But force splhigh() explicitly so
	 * the console prints in polled mode and to discourage other
	 * things from running in the middle of the printout.
	 */
	splhigh();
	spinlock_release(&hangman_lock);

	kprintf("hangman: Detected lock cycle!\n");
	kprintf("hangman: in %s (%p);\n", target->a_name, target);
	kprintf("hangman: waiting for %s (%p), but:\n", start->l_name, start);
	kprintf("   lockable %s (%p)\n", start->l_name, start);
	cur = start->l_holding;
	while (cur != target) {
		kprintf("   held by actor %s (%p)\n", cur->a_name, cur);
		kprintf("   waiting for lockable %s (%p)\n",
			cur->a_waiting->l_name, cur->a_waiting);
		cur = cur->a_waiting->l_holding;
	}
	kprintf("   held by actor %s (%p)\n", cur->a_name, cur);
	panic("Deadlock.\n");
}