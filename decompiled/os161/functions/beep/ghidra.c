void beep(void)

{
  if (the_beep == (beep_softc *)0x0) {
    kprintf("beep: Warning: no beep device\n");
  }
  else {
    (*the_beep->bs_beep)(the_beep->bs_devdata);
  }
  return;
}