beep(void)
{
	if (the_beep!=NULL) {
		the_beep->bs_beep(the_beep->bs_devdata);
	}
	else {
		kprintf("beep: Warning: no beep device\n");
	}
}