int emufs_addtovfs(emu_softc *sc,char *devname)

{
  emufs_fs *ef;
  array *a;
  int iVar1;
  
  ef = (emufs_fs *)kmalloc(0x14);
  if (ef == (emufs_fs *)0x0) {
    iVar1 = 3;
  }
  else {
    (ef->ef_fs).fs_data = ef;
    (ef->ef_fs).fs_ops = &emufs_fsops;
    ef->ef_emu = sc;
    ef->ef_root = (emufs_vnode *)0x0;
    a = (array *)kmalloc(0xc);
    if (a == (array *)0x0) {
      a = (array *)0x0;
      ef->ef_vnodes = (vnodearray *)0x0;
    }
    else {
      array_init(a);
      ef->ef_vnodes = (vnodearray *)a;
    }
    if (a == (array *)0x0) {
      kfree(ef);
      iVar1 = 3;
    }
    else {
      iVar1 = emufs_loadvnode(ef,0,1,&ef->ef_root);
      if (iVar1 == 0) {
        if (ef->ef_root == (emufs_vnode *)0x0) {
          devname = "ef->ef_root!=NULL";
          badassert("ef->ef_root!=NULL",s_______dev_lamebus_emu_c_80022750,0x527,"emufs_addtovfs");
        }
        iVar1 = vfs_addfs(devname,(fs *)ef);
        if (iVar1 != 0) {
          vnode_decref(&ef->ef_root->ev_v);
          kfree(ef);
        }
      }
      else {
        kfree(ef);
      }
    }
  }
  return iVar1;
}