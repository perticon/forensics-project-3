int32_t emufs_addtovfs(int32_t * sc, char * devname) {
    char * v1 = kmalloc(20); // 0x800035a8
    if (v1 == NULL) {
        // 0x80003694
        return 3;
    }
    int32_t v2 = (int32_t)v1; // 0x800035a8
    *(int32_t *)v1 = v2;
    *(int32_t *)(v2 + 4) = (int32_t)&emufs_fsops;
    *(int32_t *)(v2 + 8) = (int32_t)sc;
    int32_t v3 = v2 + 12; // 0x800035c8
    int32_t * v4 = (int32_t *)v3; // 0x800035c8
    *v4 = 0;
    char * v5 = kmalloc(12); // 0x800035d0
    if (v5 == NULL) {
        // 0x800035ec
        *(int32_t *)(v2 + 16) = 0;
        kfree(v1);
        // 0x80003694
        return 3;
    }
    // 0x800035dc
    array_init((int32_t *)v5);
    *(int32_t *)(v2 + 16) = (int32_t)v5;
    int32_t result = emufs_loadvnode((int32_t *)v1, 0, 1, (int32_t **)v3); // 0x80003618
    if (result != 0) {
        // 0x80003624
        kfree(v1);
        // 0x80003694
        return result;
    }
    int32_t v6 = (int32_t)devname; // 0x80003640
    if (*v4 == 0) {
        // 0x80003644
        badassert("ef->ef_root!=NULL", "../../dev/lamebus/emu.c", 1319, "emufs_addtovfs");
        v6 = (int32_t)"ef->ef_root!=NULL";
    }
    // 0x80003664
    vfs_addfs((char *)v6, (int32_t *)v1);
    int32_t result2 = 0; // 0x80003670
    if (result2 != 0) {
        // 0x80003674
        vnode_decref((int32_t *)*v4);
        kfree(v1);
    }
    // 0x80003694
    return result2;
}