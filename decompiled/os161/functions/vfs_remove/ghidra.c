int vfs_remove(char *path)

{
  int iVar1;
  vnode *dir;
  char name [256];
  
  iVar1 = vfs_lookparent(path,&dir,name,0x100);
  if (iVar1 == 0) {
    vnode_check(dir,"remove");
    iVar1 = (*dir->vn_ops->vop_remove)(dir,name);
    vnode_decref(dir);
  }
  return iVar1;
}