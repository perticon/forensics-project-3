int32_t vfs_remove(char * path) {
    char name[256]; // bp-268, 0x8001a22c
    // 0x8001a22c
    int32_t * dir; // bp-272, 0x8001a22c
    int32_t v1 = vfs_lookparent(path, (int32_t *)&dir, name, 256); // 0x8001a244
    int32_t result = v1; // 0x8001a24c
    if (v1 == 0) {
        // 0x8001a250
        vnode_check(dir, "remove");
        result = *(int32_t *)(*(int32_t *)((int32_t)dir + 20) + 76);
        vnode_decref(dir);
    }
    // 0x8001a28c
    return result;
}