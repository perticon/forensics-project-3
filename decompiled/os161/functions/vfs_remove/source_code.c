vfs_remove(char *path)
{
	struct vnode *dir;
	char name[NAME_MAX+1];
	int result;

	result = vfs_lookparent(path, &dir, name, sizeof(name));
	if (result) {
		return result;
	}

	result = VOP_REMOVE(dir, name);
	VOP_DECREF(dir);

	return result;
}