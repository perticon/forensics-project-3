copyin(const_userptr_t usersrc, void *dest, size_t len)
{
	int result;
	size_t stoplen;

	result = copycheck(usersrc, len, &stoplen);
	if (result) {
		return result;
	}
	if (stoplen != len) {
		/* Single block, can't legally truncate it. */
		return EFAULT;
	}

	curthread->t_machdep.tm_badfaultfunc = copyfail;

	result = setjmp(curthread->t_machdep.tm_copyjmp);
	if (result) {
		curthread->t_machdep.tm_badfaultfunc = NULL;
		return EFAULT;
	}

	memcpy(dest, (const void *)usersrc, len);

	curthread->t_machdep.tm_badfaultfunc = NULL;
	return 0;
}