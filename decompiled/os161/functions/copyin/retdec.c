int32_t copyin(int32_t * usersrc, char * dest, int32_t len) {
    // 0x80020248
    int32_t stoplen; // bp-16, 0x80020248
    int32_t result = copycheck(usersrc, len, &stoplen); // 0x80020264
    if (result != 0) {
        // 0x800202c8
        return result;
    }
    // 0x80020270
    if (stoplen != len) {
        // 0x800202c8
        return 6;
    }
    // 0x80020284
    int32_t v1; // 0x80020248
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x8002028c
    *v2 = -0x7ffdfdcc;
    int32_t result2; // 0x80020248
    if (setjmp(v1 + 16) == 0) {
        // 0x800202ac
        memcpy(dest, usersrc, len);
        *v2 = 0;
        result2 = 0;
    } else {
        // 0x800202a0
        *v2 = 0;
        result2 = 6;
    }
    // 0x800202c8
    return result2;
}