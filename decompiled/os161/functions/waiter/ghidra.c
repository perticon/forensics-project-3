void waiter(void *vsem,ulong junk)

{
  uint uVar1;
  
  P((semaphore *)vsem);
  spinlock_acquire(&waiters_lock);
  uVar1 = waiters_running - 1;
  if (waiters_running == 0) {
    badassert("waiters_running > 0","../../test/semunit.c",100,"waiter");
  }
  waiters_running = uVar1;
  spinlock_release(&waiters_lock);
  return;
}