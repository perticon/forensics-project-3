void waiter(char * vsem, int32_t junk) {
    // 0x800115c0
    P((int32_t *)vsem);
    spinlock_acquire(&waiters_lock);
    int32_t v1 = waiters_running - 1; // 0x800115ec
    if (waiters_running == 0) {
        // 0x800115f0
        badassert("waiters_running > 0", "../../test/semunit.c", 100, "waiter");
        v1 = &g41;
    }
    // 0x80011610
    waiters_running = v1;
    spinlock_release(&waiters_lock);
}