waiter(void *vsem, unsigned long junk)
{
	struct semaphore *sem = vsem;
	(void)junk;

	P(sem);

	spinlock_acquire(&waiters_lock);
	KASSERT(waiters_running > 0);
	waiters_running--;
	spinlock_release(&waiters_lock);
}