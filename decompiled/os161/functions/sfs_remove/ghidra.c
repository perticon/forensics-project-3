int sfs_remove(vnode *dir,char *name)

{
  uint16_t uVar1;
  int iVar2;
  uint16_t uVar3;
  sfs_vnode *psVar4;
  sfs_vnode *victim;
  int slot;
  
  psVar4 = (sfs_vnode *)dir->vn_data;
  vfs_biglock_acquire();
  iVar2 = sfs_lookonce(psVar4,name,&victim,&slot);
  if (iVar2 == 0) {
    iVar2 = sfs_dir_unlink(psVar4,slot);
    if (iVar2 == 0) {
      uVar1 = (victim->sv_i).sfi_linkcount;
      uVar3 = uVar1 - 1;
      psVar4 = victim;
      if (uVar1 == 0) {
        badassert("victim->sv_i.sfi_linkcount > 0","../../fs/sfs/sfs_vnops.c",0x19c,"sfs_remove");
      }
      (psVar4->sv_i).sfi_linkcount = uVar3;
      psVar4->sv_dirty = true;
    }
    vnode_decref(&victim->sv_absvn);
    vfs_biglock_release();
  }
  else {
    vfs_biglock_release();
  }
  return iVar2;
}