int32_t sfs_remove(int32_t * dir, char * name) {
    // 0x80009a08
    vfs_biglock_acquire();
    int32_t * v1 = (int32_t *)*(int32_t *)((int32_t)dir + 16); // 0x80009a34
    int32_t slot; // bp-20, 0x80009a08
    int32_t * victim; // bp-24, 0x80009a08
    int32_t result = sfs_lookonce(v1, name, &victim, &slot); // 0x80009a34
    if (result != 0) {
        // 0x80009a40
        vfs_biglock_release();
        // 0x80009ac0
        return result;
    }
    int32_t result2 = sfs_dir_unlink(v1, slot); // 0x80009a58
    if (result2 == 0) {
        int32_t v2 = (int32_t)victim; // 0x80009a64
        int16_t v3 = *(int16_t *)(v2 + 30); // 0x80009a6c
        int32_t v4 = v2; // 0x80009a78
        if (v3 == 0) {
            // 0x80009a7c
            badassert("victim->sv_i.sfi_linkcount > 0", "../../fs/sfs/sfs_vnops.c", 412, "sfs_remove");
            v4 = &g41;
        }
        // 0x80009a9c
        *(int16_t *)(v4 + 30) = v3 - 1;
        *(char *)(v4 + 540) = 1;
    }
    // 0x80009aa8
    vnode_decref(victim);
    vfs_biglock_release();
    // 0x80009ac0
    return result2;
}