int emufs_truncate(vnode *v,off_t len)

{
  int iVar1;
  undefined4 unaff_retaddr;
  undefined4 in_stack_fffffff8;
  
  iVar1 = emu_trunc(*(emu_softc **)((int)v->vn_data + 0x18),*(uint32_t *)((int)v->vn_data + 0x1c),
                    CONCAT44(in_stack_fffffff8,unaff_retaddr));
  return iVar1;
}