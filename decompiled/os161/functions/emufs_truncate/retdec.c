int32_t emufs_truncate(int32_t * v2, int64_t len) {
    int32_t v1 = *(int32_t *)((int32_t)v2 + 16); // 0x80002964
    int32_t v2_ = *(int32_t *)(v1 + 28); // 0x80002970
    return emu_trunc((int32_t *)*(int32_t *)(v1 + 24), v2_, 0x100000000 * len >> 32);
}