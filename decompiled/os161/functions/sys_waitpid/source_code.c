int sys_waitpid(pid_t pid, userptr_t statusp, int options)
{
#if OPT_WAITPID
  struct proc *p = proc_search_pid(pid);
  KASSERT(p != NULL);
  int s;
  (void)options; /* not handled */
  if (p == NULL)
    return -1;
  s = proc_wait(p);
  if (statusp != NULL)
    *(int *)statusp = s;
  return pid;
#else
  (void)options; /* not handled */
  (void)pid;
  (void)statusp;
  return -1;
#endif
}