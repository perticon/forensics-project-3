int sys_waitpid(pid_t pid,userptr_t statusp,int options)

{
  proc *proc;
  int iVar1;
  
  proc = proc_search_pid(pid);
  if (proc == (proc *)0x0) {
    badassert("p != NULL",s_______syscall_proc_syscalls_c_80024d30,0x50,"sys_waitpid");
  }
  if (proc == (proc *)0x0) {
    pid = -1;
  }
  else {
    iVar1 = proc_wait(proc);
    if (statusp != (userptr_t)0x0) {
      *(int *)statusp = iVar1;
    }
  }
  return pid;
}