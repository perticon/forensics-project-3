int32_t sys_waitpid(int32_t pid, int32_t * statusp, int32_t options) {
    int32_t * v1 = proc_search_pid(pid); // 0x8000dd3c
    int32_t v2 = (int32_t)v1; // 0x8000dd44
    if (v1 == NULL) {
        // 0x8000dd64
        badassert("p != NULL", "../../syscall/proc_syscalls.c", 80, "sys_waitpid");
        v2 = &g41;
    }
    int32_t v3 = proc_wait((int32_t *)v2); // 0x8000dd70
    if (statusp != NULL) {
        // 0x8000dd7c
        *statusp = v3;
    }
    // 0x8000dd8c
    return pid;
}