void interprocessor_interrupt(void) {
    // 0x800174f4
    int32_t v1; // 0x800174f4
    int32_t * v2 = (int32_t *)(v1 + 80); // 0x80017500
    spinlock_acquire((int32_t *)(*v2 + 164));
    int32_t v3 = *v2; // 0x8001750c
    uint32_t v4 = *(int32_t *)(v3 + 92); // 0x80017514
    if (v4 % 2 != 0) {
        // 0x80017528
        spinlock_release((int32_t *)(v3 + 164));
        cpu_halt();
    }
    if ((v4 & 2) != 0) {
        // 0x80017544
        spinlock_release((int32_t *)(*v2 + 164));
        spinlock_acquire((int32_t *)(*v2 + 84));
        int32_t v5 = *v2; // 0x8001755c
        int32_t v6 = v5; // 0x80017570
        if (*(char *)(v5 + 52) == 0) {
            // 0x80017574
            kprintf("cpu%d: offline: warning: not idle\n", *(int32_t *)(v5 + 4));
            v6 = *v2;
        }
        // 0x80017584
        spinlock_release((int32_t *)(v6 + 84));
        kprintf("cpu%d: offline.\n", *(int32_t *)(*v2 + 4));
        cpu_halt();
    }
    if ((v4 & 8) == 0) {
        // 0x800175f4
        *(int32_t *)(*v2 + 92) = 0;
        spinlock_release((int32_t *)(*v2 + 164));
        return;
    }
    int32_t v7 = *v2; // 0x800175d4
    int32_t * v8 = (int32_t *)(v7 + 160); // 0x800175dc
    int32_t v9 = 0; // 0x800175ec
    int32_t * v10 = v8; // 0x800175ec
    if (*v8 != 0) {
        vm_tlbshootdown((int32_t *)(v7 + 96 + 4 * v9));
        v9++;
        int32_t v11 = *v2; // 0x800175d4
        int32_t * v12 = (int32_t *)(v11 + 160); // 0x800175dc
        v10 = v12;
        while (v9 < *v12) {
            // 0x800175c0
            vm_tlbshootdown((int32_t *)(v11 + 96 + 4 * v9));
            v9++;
            v11 = *v2;
            v12 = (int32_t *)(v11 + 160);
            v10 = v12;
        }
    }
    // 0x800175f0
    *v10 = 0;
    // 0x800175f4
    *(int32_t *)(*v2 + 92) = 0;
    spinlock_release((int32_t *)(*v2 + 164));
}