void interprocessor_interrupt(void)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
  uVar2 = *(uint *)(*(int *)(unaff_s7 + 0x50) + 0x5c);
  if ((uVar2 & 1) != 0) {
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
    cpu_halt();
  }
  if ((uVar2 & 2) != 0) {
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
    spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
    if (*(char *)(*(int *)(unaff_s7 + 0x50) + 0x34) == '\0') {
      kprintf("cpu%d: offline: warning: not idle\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    }
    spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0x54));
    kprintf("cpu%d: offline.\n",*(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 4));
    cpu_halt();
  }
  uVar3 = 0;
  if ((uVar2 & 8) != 0) {
    for (; iVar1 = *(int *)(unaff_s7 + 0x50), uVar3 < *(uint *)(iVar1 + 0xa0); uVar3 = uVar3 + 1) {
      vm_tlbshootdown((tlbshootdown *)(iVar1 + (uVar3 + 0x18) * 4));
    }
    *(undefined4 *)(iVar1 + 0xa0) = 0;
  }
  *(undefined4 *)(*(int *)(unaff_s7 + 0x50) + 0x5c) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x50) + 0xa4));
  return;
}