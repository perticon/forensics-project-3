int32_t sfs_eachopendir(int32_t * v2, uint32_t openflags) {
    int32_t result = 18; // 0x800094f0
    if (openflags % 4 == 0) {
        // 0x800094f4
        result = 0;
        if ((openflags & 32) != 0) {
            // 0x80009500
            return 18;
        }
    }
    // 0x80009500
    return result;
}