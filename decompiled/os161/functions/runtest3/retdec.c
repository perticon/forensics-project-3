void runtest3(int32_t nsleeps, int32_t ncomputes) {
    // 0x80014940
    setup();
    kprintf("Starting thread test 3 (%d [sleepalots], %d {computes}, 1 waker)\n", nsleeps, ncomputes);
    make_sleepalots(nsleeps);
    make_computes(ncomputes);
    finish(ncomputes + nsleeps);
    kprintf("\nThread test 3 done\n");
}