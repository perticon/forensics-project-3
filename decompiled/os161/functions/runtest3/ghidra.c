void runtest3(int nsleeps,int ncomputes)

{
  setup();
  kprintf("Starting thread test 3 (%d [sleepalots], %d {computes}, 1 waker)\n",nsleeps,ncomputes);
  make_sleepalots(nsleeps);
  make_computes(ncomputes);
  finish(nsleeps + ncomputes);
  kprintf("\nThread test 3 done\n");
  return;
}