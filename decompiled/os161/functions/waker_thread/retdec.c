void waker_thread(char * junk1, int32_t junk2) {
    // 0x800149a8
    P(wakersem);
    V(wakersem);
    if (wakerdone != 0) {
        // 0x80014a7c
        V(donesem);
        return;
    }
    int32_t v1 = 0; // 0x80014a1c
    v1++;
    uint32_t v2 = random() % 12; // 0x80014a24
    int32_t v3 = *(int32_t *)(4 * v2 + (int32_t)&waitchans); // 0x80014a40
    int32_t * v4 = (int32_t *)(8 * v2 + (int32_t)&spinlocks); // 0x80014a48
    spinlock_acquire(v4);
    wchan_wakeall((int32_t *)v3, v4);
    spinlock_release(v4);
    thread_yield();
    while (v1 < 100) {
        // 0x80014a18
        v1++;
        v2 = random() % 12;
        v3 = *(int32_t *)(4 * v2 + (int32_t)&waitchans);
        v4 = (int32_t *)(8 * v2 + (int32_t)&spinlocks);
        spinlock_acquire(v4);
        wchan_wakeall((int32_t *)v3, v4);
        spinlock_release(v4);
        thread_yield();
    }
    // 0x800149ec
    P(wakersem);
    V(wakersem);
    while (wakerdone == 0) {
        // 0x80014a6c
        v1 = 0;
        v1++;
        v2 = random() % 12;
        v3 = *(int32_t *)(4 * v2 + (int32_t)&waitchans);
        v4 = (int32_t *)(8 * v2 + (int32_t)&spinlocks);
        spinlock_acquire(v4);
        wchan_wakeall((int32_t *)v3, v4);
        spinlock_release(v4);
        thread_yield();
        while (v1 < 100) {
            // 0x80014a18
            v1++;
            v2 = random() % 12;
            v3 = *(int32_t *)(4 * v2 + (int32_t)&waitchans);
            v4 = (int32_t *)(8 * v2 + (int32_t)&spinlocks);
            spinlock_acquire(v4);
            wchan_wakeall((int32_t *)v3, v4);
            spinlock_release(v4);
            thread_yield();
        }
        // 0x800149ec
        P(wakersem);
        V(wakersem);
    }
    // 0x80014a7c
    V(donesem);
}