void waker_thread(void *junk1,ulong junk2)

{
  bool bVar1;
  int iVar2;
  uint uVar3;
  spinlock *splk;
  int iVar4;
  wchan *wc;
  
  while( true ) {
    P(wakersem);
    iVar2 = wakerdone;
    V(wakersem);
    iVar4 = 0;
    if (iVar2 != 0) break;
    bVar1 = true;
    while (bVar1) {
      iVar4 = iVar4 + 1;
      uVar3 = random();
      splk = spinlocks + uVar3 % 0xc;
      wc = waitchans[uVar3 % 0xc];
      spinlock_acquire(splk);
      wchan_wakeall(wc,splk);
      spinlock_release(splk);
      thread_yield();
      bVar1 = iVar4 < 100;
    }
  }
  V(donesem);
  return;
}