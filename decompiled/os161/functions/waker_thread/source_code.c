waker_thread(void *junk1, unsigned long junk2)
{
	int i, done;

	(void)junk1;
	(void)junk2;

	while (1) {
		P(wakersem);
		done = wakerdone;
		V(wakersem);
		if (done) {
			break;
		}

		for (i=0; i<WAKER_WAKES; i++) {
			unsigned n;
			struct spinlock *lk;
			struct wchan *wc;

			n = random() % NWAITCHANS;
			lk = &spinlocks[n];
			wc = waitchans[n];
			spinlock_acquire(lk);
			wchan_wakeall(wc, lk);
			spinlock_release(lk);

			thread_yield();
		}
	}
	V(donesem);
}