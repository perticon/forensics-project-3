putch_delayed(int ch)
{
	/*
	 * No synchronization needed: called only during system startup
	 * by main thread.
	 */

	KASSERT(delayed_outbuf_pos < sizeof(delayed_outbuf));
	delayed_outbuf[delayed_outbuf_pos++] = ch;
}