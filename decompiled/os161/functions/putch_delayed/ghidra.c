void putch_delayed(int ch)

{
  char *pcVar1;
  char cVar2;
  char *pcVar3;
  
  cVar2 = (char)ch;
  pcVar3 = (char *)(delayed_outbuf_pos + 1);
  if (0x3ff < delayed_outbuf_pos) {
    cVar2 = -0x4c;
    pcVar3 = "../../dev/generic/console.c";
    badassert("delayed_outbuf_pos < sizeof(delayed_outbuf)","../../dev/generic/console.c",0x5f,
              "putch_delayed");
  }
  pcVar1 = delayed_outbuf + delayed_outbuf_pos;
  delayed_outbuf_pos = (size_t)pcVar3;
  *pcVar1 = cVar2;
  return;
}