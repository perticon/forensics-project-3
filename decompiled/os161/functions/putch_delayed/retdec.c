void putch_delayed(int32_t ch) {
    int32_t v1 = delayed_outbuf_pos; // 0x80001b8c
    char v2 = ch; // 0x80001b8c
    int32_t v3 = delayed_outbuf_pos + 1; // 0x80001b8c
    if (delayed_outbuf_pos >= 1024) {
        // 0x80001b90
        badassert("delayed_outbuf_pos < sizeof(delayed_outbuf)", "../../dev/generic/console.c", 95, "putch_delayed");
        v1 = &g41;
        v2 = (char)"delayed_outbuf_pos < sizeof(delayed_outbuf)";
        v3 = (int32_t)"../../dev/generic/console.c";
    }
    int32_t v4 = v1; // 0x80001bc8
    while (true) {
        // 0x80001bb8
        delayed_outbuf_pos = v3;
        v4 += (int32_t)&delayed_outbuf;
        *(char *)v4 = v2;
    }
}