void fstest_makename(char * buf, uint32_t buflen, char * fs, char * namesuffix) {
    // 0x8000f3f0
    snprintf(buf, buflen, "%s:%s%s", fs, "fstest.tmp", namesuffix);
    if ((uint32_t)strlen(buf) >= buflen) {
        // 0x8000f440
        badassert("strlen(buf) < buflen", "../../test/fstest.c", 108, "fstest_makename");
    }
}