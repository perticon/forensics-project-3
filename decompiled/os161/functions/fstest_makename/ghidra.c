void fstest_makename(char *buf,size_t buflen,char *fs,char *namesuffix)

{
  size_t sVar1;
  
  snprintf(buf,buflen,"%s:%s%s",fs,"fstest.tmp",namesuffix);
  sVar1 = strlen(buf);
  if (buflen <= sVar1) {
    badassert("strlen(buf) < buflen","../../test/fstest.c",0x6c,"fstest_makename");
  }
  return;
}