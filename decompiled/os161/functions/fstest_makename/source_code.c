fstest_makename(char *buf, size_t buflen,
		const char *fs, const char *namesuffix)
{
	snprintf(buf, buflen, "%s:%s%s", fs, FILENAME, namesuffix);
	KASSERT(strlen(buf) < buflen);
}