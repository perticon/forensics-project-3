semfs_semstat(struct vnode *vn, struct stat *buf)
{
	struct semfs_vnode *semv = vn->vn_data;
	struct semfs_sem *sem;

	sem = semfs_getsem(semv);

	bzero(buf, sizeof(*buf));

	lock_acquire(sem->sems_lock);
	buf->st_size = sem->sems_count;
	buf->st_nlink = sem->sems_linked ? 1 : 0;
	lock_release(sem->sems_lock);

	buf->st_mode = S_IFREG | 0666;
	buf->st_blocks = 0;
	buf->st_dev = 0;
	buf->st_ino = semv->semv_semnum;

	return 0;
}