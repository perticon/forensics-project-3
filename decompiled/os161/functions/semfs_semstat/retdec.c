int32_t semfs_semstat(int32_t * vn, int32_t * buf) {
    int32_t v1 = (int32_t)buf;
    int32_t v2 = *(int32_t *)((int32_t)vn + 16); // 0x800062e8
    int32_t * v3 = semfs_getsem((int32_t *)v2); // 0x800062f0
    int32_t v4 = (int32_t)v3; // 0x800062f0
    bzero((char *)buf, 88);
    lock_acquire((int32_t *)*v3);
    *(int32_t *)(v1 + 4) = *(int32_t *)(v4 + 8);
    *buf = 0;
    *(int16_t *)(v1 + 12) = (int16_t)*(char *)(v4 + 13);
    lock_release((int32_t *)*v3);
    *(int32_t *)(v1 + 8) = 0x11b6;
    *(int32_t *)(v1 + 16) = 0;
    *(int32_t *)(v1 + 20) = 0;
    *(int32_t *)(v1 + 24) = *(int32_t *)(v2 + 28);
    return 0;
}