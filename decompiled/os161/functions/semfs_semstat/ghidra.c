int semfs_semstat(vnode *vn,stat *buf)

{
  semfs_sem *psVar1;
  semfs_vnode *semv;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar1 = semfs_getsem(semv);
  bzero(buf,0x58);
  lock_acquire(psVar1->sems_lock);
  *(uint *)((int)&buf->st_size + 4) = psVar1->sems_count;
  *(undefined4 *)&buf->st_size = 0;
  buf->st_nlink = (ushort)psVar1->sems_linked;
  lock_release(psVar1->sems_lock);
  buf->st_mode = 0x11b6;
  buf->st_blocks = 0;
  buf->st_dev = 0;
  buf->st_ino = semv->semv_semnum;
  return 0;
}