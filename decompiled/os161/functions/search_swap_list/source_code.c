static struct swap_entry *search_swap_list(pid_t pid, vaddr_t vaddr)
{
    struct swap_entry *tmp;

    tmp = swap_list->next;

    for (int i = 0; i < ENTRIES; i++)
    {
        if (tmp == free_list_tail)
        {
            return NULL;
        }

        if (tmp->pid == pid && tmp->page == vaddr)
        {
            return tmp;
        }

        tmp = tmp->next;
    }

    panic("Should not get here while searching in swap list\n");
}