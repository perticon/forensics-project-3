int hashU(Key v, int M)
{
    unsigned x = 10;
    unsigned sum;
    /* concatenate pid and vaddr */
    while (v.kaddr >= x)
        x *= 10;
    sum = (unsigned)v.kpid * x + v.kaddr;

    int h = sum % M;
    return h;
}