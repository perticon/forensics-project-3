int32_t hashU(int32_t v2, uint32_t M) {
    // 0x8000a328
    int32_t v1; // 0x8000a328
    uint32_t v2_ = v1;
    if (M < 10) {
        // 0x8000a358
        return (10 * v2 + M) % v2_;
    }
    int32_t v3 = 10; // 0x8000a354
    v3 *= 10;
    while (v3 <= M) {
        // 0x8000a338
        v3 *= 10;
    }
    // 0x8000a358
    return (v3 * v2 + M) % v2_;
}