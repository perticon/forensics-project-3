int sfs_sync(fs *fs)

{
  int iVar1;
  sfs_fs *sfs;
  
  vfs_biglock_acquire();
  sfs = (sfs_fs *)fs->fs_data;
  iVar1 = sfs_sync_vnodes(sfs);
  if (iVar1 == 0) {
    iVar1 = sfs_sync_freemap(sfs);
    if (iVar1 == 0) {
      iVar1 = sfs_sync_superblock(sfs);
      if (iVar1 == 0) {
        vfs_biglock_release();
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
    else {
      vfs_biglock_release();
    }
  }
  else {
    vfs_biglock_release();
  }
  return iVar1;
}