sfs_sync(struct fs *fs)
{
	struct sfs_fs *sfs;
	int result;

	vfs_biglock_acquire();

	/*
	 * Get the sfs_fs from the generic abstract fs.
	 *
	 * Note that the abstract struct fs, which is all the VFS
	 * layer knows about, is actually a member of struct sfs_fs.
	 * The pointer in the struct fs points back to the top of the
	 * struct sfs_fs - essentially the same object. This can be a
	 * little confusing at first.
	 *
	 * The following diagram may help:
	 *
	 *     struct sfs_fs        <-------------\
         *           :                            |
         *           :   sfs_absfs (struct fs)    |   <------\
         *           :      :                     |          |
         *           :      :  various members    |          |
         *           :      :                     |          |
         *           :      :  fs_data  ----------/          |
         *           :      :                             ...|...
         *           :                                   .  VFS  .
         *           :                                   . layer .
         *           :   other members                    .......
         *           :
         *           :
	 *
	 * This construct is repeated with vnodes and devices and other
	 * similar things all over the place in OS/161, so taking the
	 * time to straighten it out in your mind is worthwhile.
	 */

	sfs = fs->fs_data;

	/* If any vnodes need to be written, write them. */
	result = sfs_sync_vnodes(sfs);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	/* If the free block map needs to be written, write it. */
	result = sfs_sync_freemap(sfs);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	/* If the superblock needs to be written, write it. */
	result = sfs_sync_superblock(sfs);
	if (result) {
		vfs_biglock_release();
		return result;
	}

	vfs_biglock_release();
	return 0;
}