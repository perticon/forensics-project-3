int32_t sfs_sync(int32_t * fs) {
    // 0x800080b8
    vfs_biglock_acquire();
    int32_t result = sfs_sync_vnodes(fs); // 0x800080d8
    if (result != 0) {
        // 0x800080e4
        vfs_biglock_release();
        // 0x80008140
        return result;
    }
    int32_t v1 = sfs_sync_freemap(fs); // 0x800080f8
    int32_t result2; // 0x800080b8
    if (v1 == 0) {
        int32_t v2 = sfs_sync_superblock(fs); // 0x80008118
        vfs_biglock_release();
        result2 = v2;
    } else {
        // 0x80008104
        vfs_biglock_release();
        result2 = v1;
    }
    // 0x80008140
    return result2;
}