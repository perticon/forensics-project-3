void threadlistnode_cleanup(threadlistnode *tln)

{
  char *pcVar1;
  char *pcVar2;
  
  pcVar2 = (char *)0x80020000;
  pcVar1 = (char *)tln;
  if (tln->tln_next != (threadlistnode *)0x0) {
    pcVar1 = "tln->tln_next == NULL";
    pcVar2 = "../../thread/threadlist.c";
    badassert("tln->tln_next == NULL","../../thread/threadlist.c",0x37,"threadlistnode_cleanup");
  }
  if (*(threadlistnode **)pcVar1 != (threadlistnode *)0x0) {
    pcVar1 = "tln->tln_prev == NULL";
    badassert("tln->tln_prev == NULL",pcVar2 + 0x6f94,0x38,"threadlistnode_cleanup");
  }
  if (*(thread **)((int)pcVar1 + 8) == (thread *)0x0) {
    badassert("tln->tln_self != NULL","../../thread/threadlist.c",0x39,"threadlistnode_cleanup");
  }
  return;
}