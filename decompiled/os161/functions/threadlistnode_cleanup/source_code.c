threadlistnode_cleanup(struct threadlistnode *tln)
{
	DEBUGASSERT(tln != NULL);

	KASSERT(tln->tln_next == NULL);
	KASSERT(tln->tln_prev == NULL);
	KASSERT(tln->tln_self != NULL);
}