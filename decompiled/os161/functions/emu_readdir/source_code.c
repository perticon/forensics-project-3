emu_readdir(struct emu_softc *sc, uint32_t handle, uint32_t len,
	    struct uio *uio)
{
	return emu_doread(sc, handle, len, EMU_OP_READDIR, uio);
}