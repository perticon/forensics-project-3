static struct swap_entry *search_swap_list_pid(pid_t pid, struct swap_entry **tmp)
{
    spinlock_acquire(&swap_lock);
    
    if (*tmp == NULL)
    {
        *tmp = swap_list->next;
    }

    for (int i = 0; i < ENTRIES; i++)
    {
        if (*tmp == free_list_tail)
        {
            return NULL;
            spinlock_release(&swap_lock);

        }

        if ((*tmp)->pid == pid)
        {
            spinlock_release(&swap_lock);
            return *tmp;
        }

        *tmp = (*tmp)->next;
    }

    panic("Should not get here while searching in swap list\n");
}