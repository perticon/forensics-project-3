void kprintf_bootstrap(void) {
    // 0x8000b598
    if (kprintf_lock != NULL) {
        // 0x8000b5b4
        badassert("kprintf_lock == NULL", "../../lib/kprintf.c", 66, "kprintf_bootstrap");
    }
    int32_t * v1 = lock_create("kprintf_lock"); // 0x8000b5d8
    *(int32_t *)&kprintf_lock = (int32_t)v1;
    if (v1 == NULL) {
        // 0x8000b5e8
        panic("Could not create kprintf_lock\n");
    }
    // 0x8000b5f4
    spinlock_init(&kprintf_spinlock);
}