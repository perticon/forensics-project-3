kprintf_bootstrap(void)
{
	KASSERT(kprintf_lock == NULL);

	kprintf_lock = lock_create("kprintf_lock");
	if (kprintf_lock == NULL) {
		panic("Could not create kprintf_lock\n");
	}
	spinlock_init(&kprintf_spinlock);
}