void kprintf_bootstrap(void)

{
  if (kprintf_lock != (lock *)0x0) {
    badassert("kprintf_lock == NULL","../../lib/kprintf.c",0x42,"kprintf_bootstrap");
  }
  kprintf_lock = lock_create("kprintf_lock");
  if (kprintf_lock == (lock *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("Could not create kprintf_lock\n");
  }
  spinlock_init(&kprintf_spinlock);
  return;
}