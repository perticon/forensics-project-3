semu2(int nargs, char **args)
{
	struct semaphore *sem;

	(void)nargs; (void)args;

	kprintf("This should crash with a kernel null dereference\n");
	sem = sem_create(NULL, 44);
	(void)sem;
	panic("semu2: sem_create accepted a null name\n");
	return 0;
}