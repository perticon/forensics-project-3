int semu2(int nargs,char **args)

{
  kprintf("This should crash with a kernel null dereference\n");
  sem_create((char *)0x0,0x2c);
                    /* WARNING: Subroutine does not return */
  panic("semu2: sem_create accepted a null name\n");
}