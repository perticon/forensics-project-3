int32_t semu2(int32_t nargs, char ** args) {
    // 0x8001201c
    kprintf("This should crash with a kernel null dereference\n");
    sem_create(NULL, 44);
    panic("semu2: sem_create accepted a null name\n");
    return &g41;
}