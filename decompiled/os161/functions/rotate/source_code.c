rotate(char *str, int amt)
{
	int i, ch;

	amt = (amt+2600)%26;
	KASSERT(amt>=0);

	for (i=0; str[i]; i++) {
		ch = str[i];
		if (ch>='A' && ch<='Z') {
			ch = ch - 'A';
			ch += amt;
			ch %= 26;
			ch = ch + 'A';
			KASSERT(ch>='A' && ch<='Z');
		}
		str[i] = ch;
	}
}