void rotate(char * str, int32_t amt) {
    int32_t v1 = (amt + 2600) % 26; // 0x8000f334
    int32_t v2; // 0x8000f324
    int32_t v3; // 0x8000f324
    int32_t v4; // 0x8000f324
    int32_t v5; // 0x8000f324
    int32_t v6; // 0x8000f324
    int32_t v7; // 0x8000f324
    int32_t v8; // 0x8000f324
    if (v1 > -1) {
        // 0x8000f3c4
        v3 = (int32_t)str;
        v5 = 0;
        v7 = 26;
        goto lab_0x8000f3cc;
    } else {
        // 0x8000f348
        badassert("amt>=0", "../../test/fstest.c", 85, "rotate");
        v8 = &g41;
        v2 = (int32_t)"amt>=0";
        v4 = (int32_t)"../../test/fstest.c";
        v6 = (int32_t)"rotate";
        goto lab_0x8000f368;
    }
  lab_0x8000f3cc:;
    int32_t v9 = v5 + v3; // 0x8000f3cc
    char v10 = *(char *)v9; // 0x8000f3d0
    int32_t v11 = v10; // 0x8000f3d0
    v8 = v11;
    int32_t v12 = v9; // 0x8000f3dc
    v2 = v3;
    v4 = v5;
    v6 = v11 - 65;
    int32_t v13 = v7; // 0x8000f3dc
    if (v10 == 0) {
        // 0x8000f3e0
        return;
    }
    goto lab_0x8000f368;
  lab_0x8000f368:;
    int32_t v14 = v13;
    int32_t v15 = v4;
    int32_t v16 = v2;
    int32_t v17 = v8;
    int32_t v18 = v16; // 0x8000f370
    int32_t v19 = v15; // 0x8000f370
    char v20 = v17; // 0x8000f370
    if (v6 < 26) {
        int32_t v21 = (v1 - 65 + v17) % v14; // 0x8000f380
        v18 = v16;
        v19 = v15;
        v20 = (char)v21 + 65;
        if (v21 >= 26) {
            // 0x8000f39c
            badassert("ch>='A' && ch<='Z'", "../../test/fstest.c", 94, "rotate");
            v18 = (int32_t)"ch>='A' && ch<='Z'";
            v19 = (int32_t)"../../test/fstest.c";
            v20 = 94;
        }
    }
    // 0x8000f3b8
    *(char *)v12 = v20;
    v3 = v18;
    v5 = v19 + 1;
    v7 = v14;
    goto lab_0x8000f3cc;
}