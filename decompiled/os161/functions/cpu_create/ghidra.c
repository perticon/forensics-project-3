cpu * cpu_create(uint hardware_number)

{
  uint uVar1;
  cpu *c;
  int iVar2;
  char *pcVar3;
  thread *ptVar4;
  void *pvVar5;
  int unaff_s7;
  char namebuf [16];
  
  c = (cpu *)kmalloc(0xac);
  if (c == (cpu *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: Out of memory\n");
  }
  c->c_self = c;
  c->c_hardware_number = hardware_number;
  c->c_curthread = (thread *)0x0;
  threadlist_init(&c->c_zombies);
  c->c_hardclocks = 0;
  c->c_spinlocks = 0;
  c->c_isidle = false;
  threadlist_init(&c->c_runqueue);
  spinlock_init(&c->c_runqueue_lock);
  c->c_ipi_pending = 0;
  c->c_numshootdown = 0;
  spinlock_init(&c->c_ipi_lock);
  uVar1 = allcpus.arr.num;
  iVar2 = array_setsize(&allcpus.arr,allcpus.arr.num + 1);
  if (iVar2 == 0) {
    allcpus.arr.v[uVar1] = c;
    if (c == (cpu *)0xfffffffc) {
      iVar2 = 0;
    }
    else {
      c->c_number = uVar1;
      iVar2 = 0;
    }
  }
  if (iVar2 != 0) {
    pcVar3 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: array_add: %s\n",pcVar3);
  }
  snprintf(namebuf,0x10,"<boot #%d>",c->c_number);
  ptVar4 = thread_create(namebuf);
  c->c_curthread = ptVar4;
  if (ptVar4 == (thread *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: thread_create failed\n");
  }
  ptVar4->t_cpu = c;
  if (c->c_number != 0) {
    ptVar4 = c->c_curthread;
    pvVar5 = kmalloc(0x1000);
    ptVar4->t_stack = pvVar5;
    if (c->c_curthread->t_stack == (void *)0x0) {
                    /* WARNING: Subroutine does not return */
      panic("cpu_create: couldn\'t allocate stack");
    }
    thread_checkstack_init(c->c_curthread);
  }
  if (unaff_s7 == 0) {
    ptVar4 = c->c_curthread;
    ptVar4->t_cpu = c;
    c->c_curthread = ptVar4;
  }
  iVar2 = proc_addthread(kproc,c->c_curthread);
  if (iVar2 != 0) {
    pcVar3 = strerror(iVar2);
                    /* WARNING: Subroutine does not return */
    panic("cpu_create: proc_addthread:: %s\n",pcVar3);
  }
  cpu_machdep_init(c);
  return c;
}