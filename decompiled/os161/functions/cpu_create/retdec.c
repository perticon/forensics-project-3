int32_t * cpu_create(int32_t hardware_number) {
    char namebuf[16]; // bp-32, 0x80016394
    char * v1 = kmalloc(172); // 0x800163b0
    int32_t v2 = (int32_t)v1; // 0x800163b0
    int32_t v3 = v2; // 0x800163b8
    if (v1 == NULL) {
        // 0x800163bc
        panic("cpu_create: Out of memory\n");
        v3 = &g41;
    }
    // 0x800163c8
    *(int32_t *)v1 = v3;
    *(int32_t *)(v3 + 8) = hardware_number;
    *(int32_t *)(v3 + 12) = 0;
    threadlist_init((int32_t *)(v3 + 16));
    *(int32_t *)(v2 + 44) = 0;
    *(int32_t *)(v2 + 48) = 0;
    *(char *)(v2 + 52) = 0;
    threadlist_init((int32_t *)(v2 + 56));
    spinlock_init((int32_t *)(v2 + 84));
    *(int32_t *)(v2 + 92) = 0;
    *(int32_t *)(v2 + 160) = 0;
    spinlock_init((int32_t *)(v2 + 164));
    int32_t v4 = array_setsize(&allcpus, g34 + 1); // 0x8001641c
    int32_t v5; // 0x80016394
    if (v4 == 0) {
        // 0x80016428
        *(int32_t *)(allcpus + 4 * g34) = v2;
        v5 = 16;
        if (v1 != (char *)-4) {
            // 0x80016440
            *(int32_t *)(v2 + 4) = g34;
            v5 = 16;
        }
    } else {
        char * v6 = strerror(v4); // 0x8001645c
        panic("cpu_create: array_add: %s\n", v6);
        v5 = (int32_t)v6;
    }
    int32_t * v7 = (int32_t *)(v2 + 4); // 0x80016478
    snprintf(namebuf, v5, "<boot #%d>", *v7);
    int32_t * v8 = thread_create(namebuf); // 0x80016488
    int32_t v9 = (int32_t)v8; // 0x80016488
    int32_t * v10 = (int32_t *)(v2 + 12); // 0x80016490
    *v10 = v9;
    int32_t v11 = v9; // 0x80016490
    if (v8 == NULL) {
        // 0x80016494
        panic("cpu_create: thread_create failed\n");
        v11 = &g41;
    }
    // 0x800164a0
    *(int32_t *)(v11 + 80) = v2;
    if (*v7 != 0) {
        // 0x800164b4
        *(int32_t *)(*v10 + 72) = (int32_t)kmalloc(0x1000);
        int32_t v12 = *v10; // 0x800164c4
        int32_t v13 = v12; // 0x800164d8
        if (*(int32_t *)(v12 + 72) == 0) {
            // 0x800164dc
            panic("cpu_create: couldn't allocate stack");
            v13 = (int32_t)"cpu_create: couldn't allocate stack";
        }
        // 0x800164e8
        thread_checkstack_init((int32_t *)v13);
    }
    int32_t v14 = *v10;
    int32_t v15; // 0x80016394
    if (v15 == 0) {
        // 0x800164f8
        *(int32_t *)(v14 + 80) = v2;
        *v10 = v14;
    }
    int32_t v16 = proc_addthread(kproc, (int32_t *)v14); // 0x8001651c
    if (v16 != 0) {
        // 0x80016528
        panic("cpu_create: proc_addthread:: %s\n", strerror(v16));
    }
    // 0x80016540
    cpu_machdep_init((int32_t *)v1);
    return (int32_t *)v1;
}