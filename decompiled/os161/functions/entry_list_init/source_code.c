static void entry_list_init(int maxN)
{

    int i;
    struct swap_entry *tmp;

    free_list_head = kmalloc(sizeof(struct swap_entry));
    free_list_tail = kmalloc(sizeof(struct swap_entry));
    free_list_head->next = free_list_tail;

    swap_list = kmalloc(sizeof(struct swap_entry));
    swap_list->next = free_list_tail;
    swap_list->previous = NULL;

    for (i = 0; i < maxN; i++)
    {
        tmp = kmalloc(sizeof(struct swap_entry));
        tmp->pid = -1;
        tmp->file_offset = i * PAGE_SIZE;
        add_free_entry(tmp);
    }
}