void lamebus_mark(lamebus_softc *sc,int slot)

{
  char *pcVar1;
  uint uVar2;
  
  uVar2 = 1 << (slot & 0x1fU);
  if (0x1f < (uint)slot) {
    pcVar1 = "slot>=0 && slot < LB_NSLOTS";
    badassert("slot>=0 && slot < LB_NSLOTS","../../dev/lamebus/lamebus.c",0x139,"lamebus_mark");
    sc = (lamebus_softc *)pcVar1;
  }
  spinlock_acquire(&sc->ls_lock);
  if ((sc->ls_slotsinuse & uVar2) != 0) {
                    /* WARNING: Subroutine does not return */
    panic("lamebus_mark: slot %d already in use\n",slot);
  }
  sc->ls_slotsinuse = uVar2 | sc->ls_slotsinuse;
  spinlock_release(&sc->ls_lock);
  return;
}