void lamebus_mark(int32_t * sc, uint32_t slot) {
    int32_t v1 = (int32_t)sc; // 0x80003da0
    if (slot >= 32) {
        // 0x80003da4
        badassert("slot>=0 && slot < LB_NSLOTS", "../../dev/lamebus/lamebus.c", 313, "lamebus_mark");
        v1 = (int32_t)"slot>=0 && slot < LB_NSLOTS";
    }
    int32_t v2 = 1 << slot; // 0x80003da0
    int32_t * v3 = (int32_t *)v1; // 0x80003dc8
    spinlock_acquire(v3);
    int32_t v4 = 0x73202626; // 0x80003ddc
    if ((v2 & 0x73202626) != 0) {
        // 0x80003de0
        panic("lamebus_mark: slot %d already in use\n", slot);
        v4 = &g41;
    }
    // 0x80003dec
    *(int32_t *)"&& slot < LB_NSLOTS" = v4 | v2;
    spinlock_release(v3);
}