int32_t do_semu1011(bool interrupthandler) {
    int32_t * v1 = makesem(0); // 0x800118d4
    int32_t v2 = (int32_t)v1; // 0x800118d4
    makewaiter(v1);
    int32_t v3 = *v1; // 0x800118e4
    int32_t * v4 = (int32_t *)(v2 + 4); // 0x800118e8
    if (strcmp((char *)v3, "some-silly-name") != 0) {
        // 0x80011904
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 409, "do_semu1011");
    }
    int32_t * v5 = (int32_t *)(v2 + 8); // 0x80011928
    if (!spinlock_not_held(v5)) {
        // 0x80011934
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 410, "do_semu1011");
    }
    // 0x80011950
    spinlock_acquire(&waiters_lock);
    if (waiters_running != 1) {
        // 0x80011970
        badassert("waiters_running == 1", "../../test/semunit.c", 412, "do_semu1011");
    }
    // 0x8001198c
    spinlock_release(&waiters_lock);
    if (!interrupthandler) {
        // 0x800119d4
        V(v1);
    } else {
        // 0x800119a0
        int32_t v6; // 0x800118b0
        char * v7 = (char *)(v6 + 88);
        char v8 = 1; // 0x800119ac
        if (*v7 != 0) {
            // 0x800119b0
            badassert("curthread->t_in_interrupt == false", "../../test/semunit.c", 417, "do_semu1011");
            v8 = &g41;
        }
        // 0x800119e4
        *v7 = v8;
        V(v1);
        if (*v7 == 0) {
            // 0x800119f4
            badassert("curthread->t_in_interrupt == true", "../../test/semunit.c", 424, "do_semu1011");
        }
        // 0x80011a10
        *v7 = 0;
    }
    // 0x80011a14
    clocksleep(1);
    int32_t v9 = v3; // 0x80011a28
    if (*v1 != v3) {
        // 0x80011a2c
        badassert("name == sem->sem_name", "../../test/semunit.c", 432, "do_semu1011");
        v9 = (int32_t)"name == sem->sem_name";
    }
    // 0x80011a4c
    if (strcmp((char *)v9, "some-silly-name") != 0) {
        // 0x80011a60
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 433, "do_semu1011");
    }
    // 0x80011a7c
    if (*v4 != *v4) {
        // 0x80011a8c
        badassert("wchan == sem->sem_wchan", "../../test/semunit.c", 434, "do_semu1011");
    }
    // 0x80011aa8
    if (!spinlock_not_held(v5)) {
        // 0x80011ab8
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 435, "do_semu1011");
    }
    // 0x80011ad4
    if (*(int32_t *)(v2 + 16) != 0) {
        // 0x80011ae4
        badassert("sem->sem_count == 0", "../../test/semunit.c", 436, "do_semu1011");
    }
    // 0x80011b00
    spinlock_acquire(&waiters_lock);
    if (waiters_running != 0) {
        // 0x80011b20
        badassert("waiters_running == 0", "../../test/semunit.c", 438, "do_semu1011");
    }
    // 0x80011b3c
    spinlock_release(&waiters_lock);
    ok();
    sem_destroy(v1);
    return 0;
}