sfs_readdir(struct sfs_vnode *sv, int slot, struct sfs_direntry *sd)
{
	off_t actualpos;

	/* Compute the actual position in the directory to read. */
	actualpos = slot * sizeof(struct sfs_direntry);

	return sfs_metaio(sv, actualpos, sd, sizeof(*sd), UIO_READ);
}