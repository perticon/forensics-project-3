paddr_t ipt_lookup(pid_t pid, vaddr_t vaddr)
{
    int index;
    int paddr;
    KASSERT(pid >= 0);
    KASSERT(vaddr != 0);
    spinlock_acquire(&ipt_lock);
    KASSERT(ipt_active);

    index = STsearch(ipt_hash, pid, vaddr);

    if (index == -1)
    {
        paddr = 0;
    }
    else
    {
        paddr = index * PAGE_SIZE;
    }

    spinlock_release(&ipt_lock);

    /* return 0 in case the frame is not in memory */
    return paddr;
}