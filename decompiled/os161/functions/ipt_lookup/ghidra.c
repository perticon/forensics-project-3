paddr_t ipt_lookup(pid_t pid,vaddr_t vaddr)

{
  int iVar1;
  char *pcVar2;
  paddr_t pVar3;
  
  if (pid < 0) {
    pcVar2 = "../../vm/pt.c";
    badassert("pid >= 0","../../vm/pt.c",0x8c,"ipt_lookup");
    vaddr = (vaddr_t)pcVar2;
  }
  if ((char *)vaddr == (char *)0x0) {
    badassert("vaddr != 0","../../vm/pt.c",0x8d,"ipt_lookup");
  }
  spinlock_acquire(&ipt_lock);
  iVar1 = -0x7ffd0000;
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x8f,"ipt_lookup");
  }
  iVar1 = STsearch(*(ST *)(iVar1 + -0x7304),pid,vaddr);
  pVar3 = iVar1 << 0xc;
  if (iVar1 == -1) {
    pVar3 = 0;
  }
  spinlock_release(&ipt_lock);
  return pVar3;
}