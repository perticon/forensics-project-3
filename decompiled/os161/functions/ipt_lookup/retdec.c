int32_t ipt_lookup(int32_t pid, int32_t vaddr) {
    int32_t v1; // 0x8001cd20
    if (pid > -1) {
        // 0x8001cd58
        v1 = vaddr;
        if (vaddr == 0) {
            // 0x8001cd60
            badassert("vaddr != 0", "../../vm/pt.c", 141, "ipt_lookup");
            v1 = 0;
        }
    } else {
        // 0x8001cd58
        badassert("pid >= 0", "../../vm/pt.c", 140, "ipt_lookup");
        v1 = (int32_t)"../../vm/pt.c";
    }
    // 0x8001cd80
    spinlock_acquire(&ipt_lock);
    int32_t v2 = (int32_t)&g35; // 0x8001cd9c
    if (ipt_active == 0) {
        // 0x8001cda0
        badassert("ipt_active", "../../vm/pt.c", 143, "ipt_lookup");
        v2 = &g41;
    }
    int32_t v3 = STsearch((int32_t *)*(int32_t *)(v2 - 0x7304), pid, v1); // 0x8001cdcc
    spinlock_release(&ipt_lock);
    return v3 == -1 ? 0 : 0x1000 * v3;
}