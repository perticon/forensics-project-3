void cpu_irqonoff(void) {
    // 0x8001f1c0
    int32_t v1; // 0x8001f1c0
    __asm_mfc0(v1, v1, 0);
    __asm_mtc0(v1 | 1, v1, 0);
    __asm_mtc0(v1 & -2, v1, 0);
}