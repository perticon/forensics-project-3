cpu_irqonoff(void)
{
        uint32_t x, xon, xoff;

        GET_STATUS(x);
        xon = x | CST_IEc;
        xoff = x & ~(uint32_t)CST_IEc;
        SET_STATUS(xon);
	__asm volatile("nop; nop; nop; nop");
        SET_STATUS(xoff);
}