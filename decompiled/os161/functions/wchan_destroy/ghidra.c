void wchan_destroy(wchan *wc)

{
  threadlist_cleanup(&wc->wc_threads);
  kfree(wc);
  return;
}