wchan_destroy(struct wchan *wc)
{
	threadlist_cleanup(&wc->wc_threads);
	kfree(wc);
}