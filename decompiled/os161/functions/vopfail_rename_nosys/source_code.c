vopfail_rename_nosys(struct vnode *fromdir, const char *fromname,
		     struct vnode *todir, const char *toname)
{
	(void)fromdir;
	(void)fromname;
	(void)todir;
	(void)toname;
	return ENOSYS;
}