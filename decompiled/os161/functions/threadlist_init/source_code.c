threadlist_init(struct threadlist *tl)
{
	DEBUGASSERT(tl != NULL);

	tl->tl_head.tln_next = &tl->tl_tail;
	tl->tl_head.tln_prev = NULL;
	tl->tl_tail.tln_next = NULL;
	tl->tl_tail.tln_prev = &tl->tl_head;
	tl->tl_head.tln_self = NULL;
	tl->tl_tail.tln_self = NULL;
	tl->tl_count = 0;
}