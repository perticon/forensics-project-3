void threadlist_init(int32_t * tl) {
    int32_t v1 = (int32_t)tl;
    int32_t v2 = v1 + 12; // 0x80017768
    *(int32_t *)(v1 + 4) = v2;
    *tl = 0;
    *(int32_t *)(v1 + 16) = 0;
    *(int32_t *)v2 = v1;
    *(int32_t *)(v1 + 8) = 0;
    *(int32_t *)(v1 + 20) = 0;
    *(int32_t *)(v1 + 24) = 0;
}