void threadlist_init(threadlist *tl)

{
  (tl->tl_head).tln_next = &tl->tl_tail;
  (tl->tl_head).tln_prev = (threadlistnode *)0x0;
  (tl->tl_tail).tln_next = (threadlistnode *)0x0;
  (tl->tl_tail).tln_prev = &tl->tl_head;
  (tl->tl_head).tln_self = (thread *)0x0;
  (tl->tl_tail).tln_self = (thread *)0x0;
  tl->tl_count = 0;
  return;
}