lock_do_i_hold(struct lock *lock)
{
        // Write this
#if OPT_SYNCH
        bool res;
	/*  G.Cabodi - 2019: this could possibly work without spinlock for mutual 
	    exclusion, which could simplify the semaphore-based solution, by 
	    removing the spinlock. 
	    Whenever the current thread owns the lock, the test is safe without 
	    guaranteeing mutual exclusion. 
	    If NOT the owner, a wrong verdict could happen (very low chance!!!)
            by wrongly reading a pointer == curthread. However, using the spinlock 
	    is good practice for shared data. */
	spinlock_acquire(&lock->lk_lock);
	res = lock->lk_owner == curthread;
	spinlock_release(&lock->lk_lock);
	return res;
#endif

        (void)lock;  // suppress warning until code gets written

        return true; // dummy until code gets written
}