bool lock_do_i_hold(int32_t * lock) {
    int32_t v1 = (int32_t)lock;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x80015900
    spinlock_acquire(v2);
    spinlock_release(v2);
    int32_t v3; // 0x800158e4
    return *(int32_t *)(v1 + 16) == v3;
}