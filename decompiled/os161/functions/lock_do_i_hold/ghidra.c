bool lock_do_i_hold(lock *lock)

{
  thread *ptVar1;
  thread *unaff_s7;
  
  spinlock_acquire(&lock->lk_lock);
  ptVar1 = lock->lk_owner;
  spinlock_release(&lock->lk_lock);
  return ptVar1 == unaff_s7;
}