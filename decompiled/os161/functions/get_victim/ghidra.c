paddr_t get_victim(vaddr_t *vaddr,pid_t *pid)

{
  pid_t *ppVar1;
  int iVar2;
  pid_t pid_00;
  int iVar3;
  int iVar4;
  int unaff_s7;
  
  spinlock_acquire(&ipt_lock);
  if (ipt_active == 0) {
    badassert("ipt_active","../../vm/pt.c",0x37,"get_victim");
  }
  iVar3 = *(int *)(*(int *)(unaff_s7 + 0x54) + 0x260);
  iVar2 = 0;
  while( true ) {
    if (nRamFrames <= iVar2) {
      spinlock_release(&ipt_lock);
      return 0;
    }
    iVar4 = iVar3 << 3;
    if (iVar3 == nRamFrames) {
      *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x260) = 0xffffffff;
      iVar3 = 0;
      iVar4 = 0;
    }
    ppVar1 = (pid_t *)((int)&ipt->pid + iVar4);
    iVar2 = iVar2 + 1;
    if (*ppVar1 == *(int *)(*(int *)(unaff_s7 + 0x54) + 0x1c)) break;
    iVar3 = iVar3 + 1;
  }
  *(int *)(*(int *)(unaff_s7 + 0x54) + 0x260) = iVar3 + 1;
  *vaddr = ppVar1[1];
  pid_00 = *ppVar1;
  *pid = pid_00;
  hash_delete(pid_00,*vaddr);
  *(undefined4 *)((int)&ipt->pid + iVar4) = 0xfffffffe;
  iVar2 = splx(1);
  iVar4 = tlb_probe(*(undefined4 *)((int)&ipt->vaddr + iVar4),0);
  if (-1 < iVar4) {
    tlb_write((iVar4 + 0x80000) * 0x1000,0,iVar4);
  }
  splx(iVar2);
  spinlock_release(&ipt_lock);
  return iVar3 << 0xc;
}