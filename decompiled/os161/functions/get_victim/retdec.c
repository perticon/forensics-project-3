int32_t get_victim(int32_t * vaddr, int32_t * pid) {
    // 0x8001d1ec
    spinlock_acquire(&ipt_lock);
    if (ipt_active == 0) {
        // 0x8001d22c
        badassert("ipt_active", "../../vm/pt.c", 55, "get_victim");
    }
    // 0x8001d248
    if (nRamFrames <= 0) {
        // 0x8001d360
        spinlock_release(&ipt_lock);
        // 0x8001d36c
        return 0;
    }
    // 0x8001d268
    int32_t v1; // 0x8001d1ec
    int32_t * v2 = (int32_t *)(v1 + 84); // 0x8001d248
    int32_t v3 = *v2; // 0x8001d248
    int32_t v4 = *(int32_t *)(v3 + 608);
    int32_t v5 = 0; // 0x8001d2a8
    int32_t v6 = v3; // 0x8001d26c
    int32_t v7 = v4; // 0x8001d26c
    int32_t v8 = 8 * v4; // 0x8001d26c
    if (v4 == nRamFrames) {
        // 0x8001d270
        *(int32_t *)(v3 + 608) = -1;
        v6 = *v2;
        v7 = 0;
        v8 = 0;
    }
    int32_t v9 = v8;
    int32_t v10 = v7;
    int32_t v11 = v6; // 0x8001d294
    int32_t v12 = v9 + (int32_t)ipt; // 0x8001d28c
    int32_t * v13 = (int32_t *)v12; // 0x8001d290
    v4 = v10 + 1;
    while (*v13 != *(int32_t *)(v11 + 28)) {
        // 0x8001d34c
        v5++;
        if (v5 >= nRamFrames) {
            // 0x8001d360
            spinlock_release(&ipt_lock);
            // 0x8001d36c
            return 0;
        }
        int32_t v14 = v11;
        v6 = v14;
        v7 = v4;
        v8 = 8 * v4;
        if (v4 == nRamFrames) {
            // 0x8001d270
            *(int32_t *)(v14 + 608) = -1;
            v6 = *v2;
            v7 = 0;
            v8 = 0;
        }
        // 0x8001d284
        v9 = v8;
        v10 = v7;
        v11 = v6;
        v12 = v9 + (int32_t)ipt;
        v13 = (int32_t *)v12;
        v4 = v10 + 1;
    }
    // 0x8001d2ac
    *(int32_t *)(v11 + 608) = v4;
    *vaddr = *(int32_t *)(v12 + 4);
    int32_t v15 = *v13; // 0x8001d2c0
    *pid = v15;
    hash_delete(v15, v15);
    *(int32_t *)(v9 + (int32_t)ipt) = -2;
    int32_t v16 = splx(1); // 0x8001d2f0
    uint32_t v17 = tlb_probe(*(int32_t *)((v9 | 4) + (int32_t)ipt), 0); // 0x8001d30c
    if (v17 >= 0) {
        // 0x8001d318
        tlb_write(-((0x1000 * v17)), 0, v17);
    }
    // 0x8001d32c
    splx(v16);
    spinlock_release(&ipt_lock);
    // 0x8001d36c
    return 0x1000 * v10;
}