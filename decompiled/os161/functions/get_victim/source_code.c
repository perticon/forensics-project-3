paddr_t get_victim(vaddr_t *vaddr, pid_t *pid)
{
    int tlb_entry, spl;

    spinlock_acquire(&ipt_lock);
    KASSERT(ipt_active);

    /* for each ram frames */
    for (int i = curproc->last_victim, j = 0; j < nRamFrames; j++, i++)
    {
        if (i == nRamFrames)
        {
            curproc->last_victim = -1;
            i = 0;
        }
        /* 
         *Till a ram frame is free (pid == -1) and its position is greater 
         *than the previous victim .
         *This guarantees that pages allocated to kernel are not touched and implements a 
         *round robin policy.
         */
        if ((ipt[i].pid == curproc->p_pid))
        {
            /* update last victim: if last frame is selected, start again from the beginning */
            curproc->last_victim = i + 1;
            *vaddr = ipt[i].vaddr;
            *pid = ipt[i].pid;
            /* delete entry from hash */
            hash_delete(*pid, *vaddr);

            /* free ipt entry: set it as kernel page so that no one can select it as a free while loading it*/
            ipt[i].pid = -2;
            /* free tlb entry */
            spl = splhigh();

            tlb_entry = tlb_probe(ipt[i].vaddr, 0);
            /* if victim page is in the tlb, invalidate the entry */
            if (tlb_entry >= 0)
            {
               tlb_write(TLBHI_INVALID(tlb_entry), TLBLO_INVALID(), tlb_entry);
            }
            splx(spl);

            /* return paddr of victim */
            spinlock_release(&ipt_lock);

            return i * PAGE_SIZE;
        }
    }
    /* error */
    spinlock_release(&ipt_lock);
    return 0;
}