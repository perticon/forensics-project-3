int32_t load_page(uint32_t page_offset_from_segbase, int32_t vaddr, int32_t segment, int32_t paddr) {
    // 0x8001d51c
    int32_t v1; // 0x8001d51c
    int32_t * v2_ = (int32_t *)(v1 + 84); // 0x8001d548
    int32_t v3 = *v2_; // 0x8001d548
    int32_t eh; // bp-112, 0x8001d51c
    int32_t v4 = &eh; // 0x8001d558
    int32_t v5 = v3 + 44;
    *(int32_t *)v4 = *(int32_t *)v5;
    *(int32_t *)(v4 + 4) = *(int32_t *)(v5 + 4);
    *(int32_t *)(v4 + 8) = *(int32_t *)(v5 + 8);
    *(int32_t *)(v4 + 12) = *(int32_t *)(v5 + 12);
    int32_t v6 = v5 + 16; // 0x8001d57c
    v4 += 16;
    while (v5 != v3 + 76) {
        // 0x8001d55c
        v5 = v6;
        *(int32_t *)v4 = *(int32_t *)v5;
        *(int32_t *)(v4 + 4) = *(int32_t *)(v5 + 4);
        *(int32_t *)(v4 + 8) = *(int32_t *)(v5 + 8);
        *(int32_t *)(v4 + 12) = *(int32_t *)(v5 + 12);
        v6 = v5 + 16;
        v4 += 16;
    }
    // 0x8001d588
    *(int32_t *)v4 = *(int32_t *)v6;
    int32_t * v2; // bp-160, 0x8001d51c
    int32_t result = vfs_open((char *)*(int32_t *)v3, 0, 0, &v2); // 0x8001d5a4
    if (result != 0) {
        // 0x8001d7ec
        return result;
    }
    // 0x8001d5b0
    int32_t iov; // bp-156, 0x8001d51c
    int32_t ku; // bp-144, 0x8001d51c
    int32_t ph; // bp-60, 0x8001d51c
    uio_kinit(&iov, &ku, (char *)&ph, 32, 0, 0);
    vnode_check(v2, "read");
    int32_t result2 = *(int32_t *)(*(int32_t *)((int32_t)v2 + 20) + 12); // 0x8001d608
    if (result2 != 0) {
        // 0x8001d7ec
        return result2;
    }
    if (v1 != 0) {
        // 0x8001d630
        kprintf("ELF: short read on phdr - file truncated?\n");
        // 0x8001d7ec
        return 13;
    }
    // 0x8001d640
    int32_t v7; // 0x8001d688
    switch (ph) {
        case 1: {
            // 0x8001d680
            v7 = v1 & -0x1000;
            if (v7 + page_offset_from_segbase != vaddr) {
                // 0x8001d698
                badassert("vaddr == ((ph.p_vaddr & PAGE_FRAME) + page_offset_from_segbase)", "../../vm/segments.c", 199, "load_page");
            }
            // break -> 0x8001d6b8
            break;
        }
        case 0: {
        }
        case 6: {
        }
        case 0x70000000: {
            // 0x8001d7ec
            return 0;
        }
        default: {
            // 0x8001d670
            kprintf("loadelf: unknown segment type %d\n", ph);
            // 0x8001d7ec
            return 13;
        }
    }
    uint32_t v8 = v1 % 0x1000 + v1; // 0x8001d6c0
    if (v8 > page_offset_from_segbase) {
        // 0x8001d6e0
        increase(5);
        increase(6);
        int32_t v9; // 0x8001d51c
        int32_t v10; // 0x8001d51c
        if (page_offset_from_segbase == 0) {
            if (2 * v1 >= v7 + 0x1000) {
                // 0x8001d71c
                v9 = 0x1000 - v1 + v7;
            }
        } else {
            // 0x8001d724
            v9 = 0x1000;
            v10 = vaddr;
            if (v8 - page_offset_from_segbase < 0x1000) {
                // 0x8001d740
                v9 = v1 - page_offset_from_segbase - v7 + v8;
                v10 = vaddr;
            }
        }
        int32_t v11 = *(int32_t *)(*v2_ + 16); // 0x8001d77c
        int32_t result3 = load_segment((int32_t *)v11, v2, 0, v10, 0x1000, v9, v1 % 2, paddr); // 0x8001d7b4
        if (result3 != 0) {
            // 0x8001d7ec
            return result3;
        }
    } else {
        // 0x8001d7bc
        increase(9);
    }
    // 0x8001d7ec
    return as_complete_load((int32_t *)*(int32_t *)(*v2_ + 16));
}