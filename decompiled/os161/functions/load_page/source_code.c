int load_page(vaddr_t page_offset_from_segbase, vaddr_t vaddr, int segment, paddr_t paddr)
{

	int bytes_toread_from_file;
	struct vnode *v;
	int result;
	struct iovec iov;
	struct uio ku;
	Elf_Ehdr eh = curproc->p_eh;
	Elf_Phdr ph;

	/* Open the file. */
	result = vfs_open(curproc->p_name, O_RDONLY, 0, &v);
	if (result)
	{
		return result;
	}

	/* read the program header for the needed segment */
	off_t offset = eh.e_phoff + segment * eh.e_phentsize;
	uio_kinit(&iov, &ku, &ph, sizeof(ph), offset, UIO_READ);

	result = VOP_READ(v, &ku);
	if (result)
	{
		return result;
	}

	if (ku.uio_resid != 0)
	{
		/* short read; problem with executable? */
		kprintf("ELF: short read on phdr - file truncated?\n");
		return ENOEXEC;
	}

	switch (ph.p_type)
	{
	case PT_NULL: /* skip */
		return 0;
	case PT_PHDR: /* skip */
		return 0;
	case PT_MIPS_REGINFO: /* skip */
		return 0;
	case PT_LOAD:
		break;
	default:
		kprintf("loadelf: unknown segment type %d\n",
				ph.p_type);
		return ENOEXEC;
	}

	/*
	 * IMPORTANT!
	 * It is very much worth noticing the following.
	 * When we need to load a page, three cases can occur:
	 * 
	 * 1)We must load the first page of the segment
	 * 2)We must load an intermediate page of the segment
	 * 3)We must load the last page of the segment
	 * 
	 * Depending on the case, the behaviour must be different.
	 * In the case (1), it is IMPORTANT to understand that we have to load the the first page
	 * starting from the vaddr of the segment. With an example, when we load a page, it could happen
	 * that the starting address is not page align: (page boundery)00444 (where 0 means not used).
	 * It is clear that the segment does not start at a multiple of a page. This MUST be taken into
	 * account when loading the first page, as well as all the others.
	 * When we load the first page, the amount of byte to read from the file could be not multiple of 
	 * a page: this implies that less than PAGE_SIZE must be read (precisily, we have to read from
	 * the starting address to the following page multiple).
	 * 
	 * When we are in case (2), any page we want to read is page aligned (properly, the first used address of 
	 * that page is aligned). This simplifies a bit things. Unfortunately, we have to keep into account that 
	 * the offset inside the file is calculated starting from the first address used in the first page, and not from
	 * the start of the page-aligned segment (they could be the same, if starting address is a multiple of a page).
	 * This means that if a segment first address is not multiple of a page, we have a segment like the following 00444 4444
	 * but on file it will be stored as 444 4444. When calculating the true offset inside the file to load the right page,
	 * we have to considering the fact that the initial padding is missing in the file. 
	 * Indeed, page_offset_from_segbase is the offset from the page-aligned segment and not from the first address.
	 * This means that we have to add the initial padding to any calculation we will perform, if we want things to work.
	 * 
	 * In the last case, (3), things are easy and similar to (2). The only true difference is that the amount of bytes
	 * that we read could be less than PAGE_SIZE. For example: 0444 4444 4400. In this case, when reading the last page, we 
	 * have to read just 44 and not the whole page (which is not present in the file). This can be easily prevented.
	 */

	/* 
	 * vaddr is the address of the page where the fault occured, that must be equal to the page-aligned
	 * starting segment address + the offset from it of the page we want to read.
	 */
	KASSERT(vaddr == ((ph.p_vaddr & PAGE_FRAME) + page_offset_from_segbase));

	/* bytes_to_align_first stores the padding bytes (if any) of the first page (example: 00044, it will contain 3) */

	int bytes_to_align_first = ph.p_vaddr - (ph.p_vaddr & PAGE_FRAME);

	/* if the page we want is greater than the segment size, it means that we have to just allocate an empty page */
	/* 1111 1111 1111  filesz: 3 memsz: 300 */

	if (page_offset_from_segbase >= ph.p_filesz + bytes_to_align_first)
	{
		increase(NEW_PAGE_ZEROED);
		result = 0;
	}
	else /* else, we have to read from file */
	{
		increase(FAULT_WITH_LOAD);
		increase(FAULT_WITH_ELF_LOAD);

		/* 
		 * load the needed page .
		 */

		/* if we want to read the first page */
		if (page_offset_from_segbase == 0)
		{
			vaddr = ph.p_vaddr;
			/* amount of bytes to read */

				// 00110 
			if (ph.p_vaddr + ph.p_filesz < (ph.p_vaddr & PAGE_FRAME) + PAGE_SIZE)
			{
				bytes_toread_from_file = ph.p_filesz;
			}
			else
			{  //00111 11111
				bytes_toread_from_file = ((ph.p_vaddr & PAGE_FRAME) + PAGE_SIZE) - ph.p_vaddr;
			}
		}

		/*
		 * Last page reading
		 * if  ph.p_filesz-page (segment size minus offset of page) is less than PAGE_SIZE, 
		 * read the remaning part (less than PAGE_SIZE)
		 */
		else if (bytes_to_align_first + ph.p_filesz - page_offset_from_segbase < PAGE_SIZE)
		{
			/* 
			 * we have to compesate for the possible padding present in the first page 
			 * We take offset from the last page we remove the first page and we add only the portion of the 
			 * first page that is truly present in the file.
			 * Ex.  0111 1111 1100 -> we have to remove the first 0
			 */
			page_offset_from_segbase = page_offset_from_segbase - PAGE_SIZE + (((ph.p_vaddr & PAGE_FRAME) + PAGE_SIZE) - ph.p_vaddr);
			bytes_toread_from_file = bytes_to_align_first + ph.p_filesz - page_offset_from_segbase;
		}
		/* middle page read PAGE_SIZE */
		else
		{
			page_offset_from_segbase = page_offset_from_segbase - PAGE_SIZE + ((ph.p_vaddr & PAGE_FRAME) + PAGE_SIZE) - ph.p_vaddr;
			bytes_toread_from_file = PAGE_SIZE;
		}

		result = load_segment(curproc->p_addrspace, v, ph.p_offset + page_offset_from_segbase, vaddr,
							  PAGE_SIZE, bytes_toread_from_file,
							  ph.p_flags & PF_X, paddr);
	}

	if (result)
	{
		return result;
	}

	result = as_complete_load(curproc->p_addrspace);
	if (result)
	{
		return result;
	}

	return 0;
}