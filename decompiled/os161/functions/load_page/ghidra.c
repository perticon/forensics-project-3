int load_page(vaddr_t page_offset_from_segbase,vaddr_t vaddr,int segment,paddr_t paddr)

{
  char **ppcVar1;
  int iVar2;
  Elf_Ehdr *pEVar3;
  char *pcVar4;
  char *pcVar5;
  char *pcVar6;
  char **ppcVar7;
  uint uVar8;
  int unaff_s7;
  vnode *v;
  iovec iov;
  uio ku;
  Elf_Ehdr eh;
  Elf_Phdr ph;
  
  ppcVar7 = *(char ***)(unaff_s7 + 0x54);
  ppcVar1 = ppcVar7 + 0xb;
  pEVar3 = &eh;
  do {
    pcVar6 = ppcVar1[1];
    pcVar5 = ppcVar1[2];
    pcVar4 = ppcVar1[3];
    *(char **)pEVar3->e_ident = *ppcVar1;
    *(char **)(pEVar3->e_ident + 4) = pcVar6;
    *(char **)(pEVar3->e_ident + 8) = pcVar5;
    *(char **)(pEVar3->e_ident + 0xc) = pcVar4;
    ppcVar1 = ppcVar1 + 4;
    pEVar3 = (Elf_Ehdr *)&pEVar3->e_type;
  } while (ppcVar1 != ppcVar7 + 0x17);
  *(char **)pEVar3 = *ppcVar1;
  iVar2 = vfs_open(*ppcVar7,0,0,&v);
  if (iVar2 == 0) {
    uio_kinit(&iov,&ku,&ph,0x20,(ulonglong)(eh.e_phoff + (uint)eh.e_phentsize * segment),UIO_READ);
    vnode_check(v,"read");
    iVar2 = (*v->vn_ops->vop_read)(v,&ku);
    if (iVar2 == 0) {
      if (ku.uio_resid == 0) {
        if (ph.p_type == 1) {
          uVar8 = ph.p_vaddr & 0xfff;
          if ((ph.p_vaddr & 0xfffff000) + page_offset_from_segbase != vaddr) {
            badassert("vaddr == ((ph.p_vaddr & PAGE_FRAME) + page_offset_from_segbase)",
                      "../../vm/segments.c",199,"load_page");
          }
          if (page_offset_from_segbase < ph.p_filesz + uVar8) {
            increase(5);
            increase(6);
            if (page_offset_from_segbase == 0) {
              if ((ph.p_vaddr & 0xfffff000) + 0x1000 <= ph.p_vaddr + ph.p_filesz) {
                ph.p_filesz = ((ph.p_vaddr & 0xfffff000) - ph.p_vaddr) + 0x1000;
              }
            }
            else {
              iVar2 = uVar8 + ph.p_filesz;
              if (iVar2 - page_offset_from_segbase < 0x1000) {
                page_offset_from_segbase =
                     page_offset_from_segbase + ((ph.p_vaddr & 0xfffff000) - ph.p_vaddr);
                ph.p_filesz = iVar2 - page_offset_from_segbase;
                ph.p_vaddr = vaddr;
              }
              else {
                page_offset_from_segbase =
                     ((ph.p_vaddr & 0xfffff000) + page_offset_from_segbase) - ph.p_vaddr;
                ph.p_filesz = 0x1000;
                ph.p_vaddr = vaddr;
              }
            }
            iVar2 = load_segment(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10),v,
                                 CONCAT44(ph.p_vaddr,0x1000),0,
                                 ph.p_offset + page_offset_from_segbase,ph.p_filesz,ph.p_flags & 1,
                                 paddr);
          }
          else {
            increase(9);
          }
          if (iVar2 == 0) {
            iVar2 = as_complete_load(*(addrspace **)(*(int *)(unaff_s7 + 0x54) + 0x10));
          }
        }
        else {
          iVar2 = 0;
          if ((ph.p_type != 0) && (iVar2 = 0, ph.p_type != 6)) {
            if (ph.p_type == 0x70000000) {
              iVar2 = 0;
            }
            else {
              kprintf("loadelf: unknown segment type %d\n");
              iVar2 = 0xd;
            }
          }
        }
      }
      else {
        kprintf("ELF: short read on phdr - file truncated?\n");
        iVar2 = 0xd;
      }
    }
  }
  return iVar2;
}