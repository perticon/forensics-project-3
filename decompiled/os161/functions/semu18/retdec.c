int32_t semu18(int32_t nargs, char ** args) {
    int32_t * v1 = makesem(1); // 0x80012510
    int32_t v2 = *v1; // 0x80012518
    if (strcmp((char *)v2, "some-silly-name") != 0) {
        // 0x80012538
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 691, "semu18");
    }
    int32_t v3 = (int32_t)v1; // 0x80012510
    int32_t * v4 = (int32_t *)(v3 + 4); // 0x80012558
    int32_t * v5 = (int32_t *)(v3 + 8); // 0x80012560
    if (!spinlock_not_held(v5)) {
        // 0x8001256c
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 693, "semu18");
    }
    int32_t * v6 = (int32_t *)(v3 + 16); // 0x80012588
    if (*v6 != 1) {
        // 0x80012598
        badassert("sem->sem_count == 1", "../../test/semunit.c", 694, "semu18");
    }
    // 0x800125b4
    P(v1);
    int32_t v7 = v2; // 0x800125c8
    if (*v1 != v2) {
        // 0x800125cc
        badassert("name == sem->sem_name", "../../test/semunit.c", 699, "semu18");
        v7 = (int32_t)"name == sem->sem_name";
    }
    // 0x800125ec
    if (strcmp((char *)v7, "some-silly-name") != 0) {
        // 0x80012600
        badassert("!strcmp(name, NAMESTRING)", "../../test/semunit.c", 700, "semu18");
    }
    // 0x8001261c
    if (*v4 != *v4) {
        // 0x8001262c
        badassert("wchan == sem->sem_wchan", "../../test/semunit.c", 701, "semu18");
    }
    // 0x80012648
    if (!spinlock_not_held(v5)) {
        // 0x80012658
        badassert("spinlock_not_held(&sem->sem_lock)", "../../test/semunit.c", 702, "semu18");
    }
    int32_t result = 0; // 0x80012680
    if (*v6 != 0) {
        // 0x80012684
        badassert("sem->sem_count == 0", "../../test/semunit.c", 703, "semu18");
        result = &g41;
    }
    // 0x800126a4
    return result;
}