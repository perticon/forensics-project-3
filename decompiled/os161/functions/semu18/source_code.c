semu18(int nargs, char **args)
{
	struct semaphore *sem;
	struct wchan *wchan;
	const char *name;

	(void)nargs; (void)args;

	sem = makesem(1);

	/* preconditions */
	name = sem->sem_name;
	KASSERT(!strcmp(name, NAMESTRING));
	wchan = sem->sem_wchan;
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 1);

	P(sem);
	
	/* postconditions */
	KASSERT(name == sem->sem_name);
	KASSERT(!strcmp(name, NAMESTRING));
	KASSERT(wchan == sem->sem_wchan);
	KASSERT(spinlock_not_held(&sem->sem_lock));
	KASSERT(sem->sem_count == 0);

	return 0;
}