int semu18(int nargs,char **args)

{
  undefined3 extraout_var;
  undefined3 extraout_var_00;
  semaphore *sem;
  int iVar1;
  bool bVar2;
  char *a;
  wchan *pwVar3;
  
  sem = makesem(1);
  a = sem->sem_name;
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",0x2b3,"semu18");
  }
  pwVar3 = sem->sem_wchan;
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2b5,"semu18");
  }
  if (sem->sem_count != 1) {
    badassert("sem->sem_count == 1","../../test/semunit.c",0x2b6,"semu18");
  }
  P(sem);
  if (sem->sem_name != a) {
    a = "name == sem->sem_name";
    badassert("name == sem->sem_name","../../test/semunit.c",699,"semu18");
  }
  iVar1 = strcmp(a,"some-silly-name");
  if (iVar1 != 0) {
    badassert("!strcmp(name, NAMESTRING)","../../test/semunit.c",700,"semu18");
  }
  if (sem->sem_wchan != pwVar3) {
    badassert("wchan == sem->sem_wchan","../../test/semunit.c",0x2bd,"semu18");
  }
  bVar2 = spinlock_not_held(&sem->sem_lock);
  if (CONCAT31(extraout_var_00,bVar2) == 0) {
    badassert("spinlock_not_held(&sem->sem_lock)","../../test/semunit.c",0x2be,"semu18");
  }
  iVar1 = 0;
  if (sem->sem_count != 0) {
    badassert("sem->sem_count == 0","../../test/semunit.c",0x2bf,"semu18");
  }
  return iVar1;
}