mergexy(unsigned x, unsigned y)
{
	uint32_t val = x;

	return (val << 16) | y;
}