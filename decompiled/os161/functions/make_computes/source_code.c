make_computes(int howmany)
{
	char name[16];
	int i, result;

	for (i=0; i<howmany; i++) {
		snprintf(name, sizeof(name), "compute%d", i);
		result = thread_fork(name, NULL, compute_thread, NULL, i);
		if (result) {
			panic("thread_fork failed: %s\n", strerror(result));
		}
	}
}