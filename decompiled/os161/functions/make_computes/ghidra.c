void make_computes(int howmany)

{
  int errcode;
  char *pcVar1;
  ulong data2;
  char name [16];
  
  data2 = 0;
  do {
    if (howmany <= (int)data2) {
      return;
    }
    snprintf(name,0x10,"compute%d",data2);
    errcode = thread_fork(name,(proc *)0x0,compute_thread,(void *)0x0,data2);
    data2 = data2 + 1;
  } while (errcode == 0);
  pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
  panic("thread_fork failed: %s\n",pcVar1);
}