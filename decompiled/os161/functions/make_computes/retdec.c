void make_computes(uint32_t howmany) {
    char name[16]; // bp-40, 0x8001480c
    if (howmany <= 0) {
        // 0x80014898
        return;
    }
    int32_t v1 = 0;
    snprintf(name, 16, "compute%d", v1);
    int32_t v2 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1); // 0x80014864
    int32_t v3 = v2; // 0x8001486c
    int32_t v4; // 0x80014864
    if (v2 != 0) {
        panic("thread_fork failed: %s\n", strerror(v3));
        snprintf(name, 16, "compute%d", v1);
        v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1);
        v3 = v4;
        while (v4 != 0) {
            // 0x80014870
            panic("thread_fork failed: %s\n", strerror(v3));
            snprintf(name, 16, "compute%d", v1);
            v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1);
            v3 = v4;
        }
    }
    int32_t v5 = v1 + 1; // 0x8001486c
    while (v5 < howmany) {
        // 0x80014840
        v1 = v5;
        snprintf(name, 16, "compute%d", v1);
        v2 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1);
        v3 = v2;
        if (v2 != 0) {
            panic("thread_fork failed: %s\n", strerror(v3));
            snprintf(name, 16, "compute%d", v1);
            v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1);
            v3 = v4;
            while (v4 != 0) {
                // 0x80014870
                panic("thread_fork failed: %s\n", strerror(v3));
                snprintf(name, 16, "compute%d", v1);
                v4 = thread_fork(name, NULL, (void (*)(char *, int32_t))((int32_t)&g2 + 0x4bc0), NULL, v1);
                v3 = v4;
            }
        }
        // 0x8001488c
        v5 = v1 + 1;
    }
}