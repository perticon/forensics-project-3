int32_t * STinit(int32_t maxN) {
    char * v1 = kmalloc(12); // 0x8000a1f0
    if (v1 == NULL) {
        // 0x8000a1fc
        badassert("st != NULL", "../../hash/st.c", 318, "STinit");
    }
    int32_t v2 = first_prime(maxN); // 0x8000a220
    kprintf("Hash Table dimension: %d\n", v2);
    char * v3 = kmalloc(16 * v2); // 0x8000a23c
    *(int32_t *)&link_list = (int32_t)v3;
    item_init();
    int32_t v4 = 0; // 0x8000a278
    if (v2 > 0) {
        *(int32_t *)((16 * v4 | 8) + (int32_t)link_list) = -1;
        v4++;
        while (v4 < v2) {
            // 0x8000a25c
            *(int32_t *)((16 * v4 | 8) + (int32_t)link_list) = -1;
            v4++;
        }
    }
    int32_t v5 = (int32_t)v1; // 0x8000a1f0
    free_link = 0;
    n_entries = v2;
    int32_t * v6 = (int32_t *)(v5 + 4); // 0x8000a28c
    *v6 = v2;
    char * v7 = kmalloc(4 * v2); // 0x8000a294
    *(int32_t *)v1 = (int32_t)v7;
    if (v7 == NULL) {
        // 0x8000a2a0
        badassert("st->heads != NULL", "../../hash/st.c", 332, "STinit");
    }
    int32_t * v8 = ITEMsetvoid(); // 0x8000a2c4
    int32_t v9 = (int32_t)NEW(v8, NULL); // 0x8000a2d0
    int32_t * v10 = (int32_t *)(v5 + 8); // 0x8000a2d4
    *v10 = v9;
    if (*v6 <= 0) {
        // 0x8000a310
        return (int32_t *)v1;
    }
    // 0x8000a2e0
    *(int32_t *)*(int32_t *)v1 = v9;
    for (int32_t i = 1; i < *v6; i++) {
        // 0x8000a2e0
        *(int32_t *)(*(int32_t *)v1 + 4 * i) = *v10;
    }
    // 0x8000a310
    return (int32_t *)v1;
}