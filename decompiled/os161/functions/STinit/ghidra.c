ST STinit(int maxN)

{
  ST psVar1;
  int iVar2;
  link *ppSVar3;
  Item item;
  link pSVar4;
  int iVar5;
  
  psVar1 = (ST)kmalloc(0xc);
  if (psVar1 == (ST)0x0) {
    badassert("st != NULL","../../hash/st.c",0x13e,"STinit");
  }
  iVar2 = first_prime(maxN);
  kprintf("Hash Table dimension: %d\n",iVar2);
  link_list = (link)kmalloc(iVar2 << 4);
  item_init();
  for (iVar5 = 0; iVar5 < iVar2; iVar5 = iVar5 + 1) {
    link_list[iVar5].item.index = -1;
  }
  free_link = 0;
  n_entries = iVar2;
  psVar1->M = iVar2;
  ppSVar3 = (link *)kmalloc(iVar2 << 2);
  psVar1->heads = ppSVar3;
  if (ppSVar3 == (link *)0x0) {
    badassert("st->heads != NULL","../../hash/st.c",0x14c,"STinit");
  }
  item = ITEMsetvoid();
  pSVar4 = NEW(item,(link)0x0);
  psVar1->z = pSVar4;
  for (iVar2 = 0; iVar2 < psVar1->M; iVar2 = iVar2 + 1) {
    psVar1->heads[iVar2] = psVar1->z;
  }
  return psVar1;
}