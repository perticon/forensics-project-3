ST STinit(int maxN)
{
    int i;
    ST st = kmalloc(sizeof *st);
    KASSERT(st != NULL);
    /* need to have a dimension that is a prime number */
    maxN=first_prime(maxN);
    kprintf("Hash Table dimension: %d\n", maxN);
    link_list = kmalloc(sizeof(struct STnode) * maxN);
    item_init();
    for (i = 0; i < maxN; i++)
    {
        link_list[i].item.index = -1;
    }
    free_link = 0;
    n_entries = maxN;
    st->M = maxN;
    st->heads = kmalloc(st->M * sizeof(link));
    KASSERT(st->heads != NULL);
    st->z = NEW(ITEMsetvoid(), NULL);

    for (i = 0; i < st->M; i++)
        st->heads[i] = st->z;

    return st;
}