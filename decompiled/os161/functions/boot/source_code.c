boot(void)
{
	/*
	 * The order of these is important!
	 * Don't go changing it without thinking about the consequences.
	 *
	 * Among other things, be aware that console output gets
	 * buffered up at first and does not actually appear until
	 * mainbus_bootstrap() attaches the console device. This can
	 * be remarkably confusing if a bug occurs at this point. So
	 * don't put new code before mainbus_bootstrap if you don't
	 * absolutely have to.
	 *
	 * Also note that the buffer for this is only 1k. If you
	 * overflow it, the system will crash without printing
	 * anything at all. You can make it larger though (it's in
	 * dev/generic/console.c).
	 */

	kprintf("\n");
	kprintf("OS/161 base system version %s\n", BASE_VERSION);
	kprintf("%s", harvard_copyright);
	kprintf("\n");

	kprintf("Celada-Macori-Perticone %s (%s #%d)\n",
		GROUP_VERSION, buildconfig, buildversion);
	kprintf("\n");

	/* Early initialization. */
	ram_bootstrap();
	vm_bootstrap();
	
	proc_bootstrap();
	thread_bootstrap();
	hardclock_bootstrap();
	vfs_bootstrap();
	kheap_nextgeneration();

	/* Probe and initialize devices. Interrupts should come on. */
	kprintf("Device probe...\n");
	KASSERT(curthread->t_curspl > 0);
	mainbus_bootstrap();
	KASSERT(curthread->t_curspl == 0);
	/* Now do pseudo-devices. */
	pseudoconfig();
	kprintf("\n");
	kheap_nextgeneration();

	/* Late phase of initialization. */
	kprintf_bootstrap();
	thread_start_cpus();

	/* Default bootfs - but ignore failure, in case emu0 doesn't exist */
	vfs_setbootfs("emu0");

	kheap_nextgeneration();

	/* init swapfile */
	#if OPT_PAGING
	init_swapfile();
	#endif


	/*
	 * Make sure various things aren't screwed up.
	 */
	COMPILE_ASSERT(sizeof(userptr_t) == sizeof(char *));
	COMPILE_ASSERT(sizeof(*(userptr_t)0) == sizeof(char));
}