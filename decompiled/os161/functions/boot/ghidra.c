void boot(void)

{
  int unaff_s7;
  
  kprintf("\n");
  kprintf("OS/161 base system version %s\n",&UNK_80023dbc);
  kprintf("%s",
          "Copyright (c) 2000, 2001-2005, 2008-2011, 2013, 2014\n   President and Fellows of Harvard College.  All rights reserved.\n"
         );
  kprintf("\n");
  kprintf("Celada-Macori-Perticone %s (%s #%d)\n","0","PAGING",buildversion);
  kprintf("\n");
  ram_bootstrap();
  vm_bootstrap();
  proc_bootstrap();
  thread_bootstrap();
  hardclock_bootstrap();
  vfs_bootstrap();
  kheap_nextgeneration();
  kprintf("Device probe...\n");
  if (*(int *)(unaff_s7 + 0x5c) < 1) {
    badassert("curthread->t_curspl > 0","../../main/main.c",0x7b,"boot");
  }
  mainbus_bootstrap();
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../main/main.c",0x7d,"boot");
  }
  pseudoconfig();
  kprintf("\n");
  kheap_nextgeneration();
  kprintf_bootstrap();
  thread_start_cpus();
  vfs_setbootfs((char *)&DAT_80023e48);
  kheap_nextgeneration();
  init_swapfile();
  return;
}