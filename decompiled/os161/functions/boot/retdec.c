void boot(void) {
    // 0x8000bb64
    kprintf("\n");
    kprintf("OS/161 base system version %s\n", "2.0.3");
    kprintf("%s", "Copyright (c) 2000, 2001-2005, 2008-2011, 2013, 2014\n   President and Fellows of Harvard College.  All rights reserved.\n");
    kprintf("\n");
    kprintf("Celada-Macori-Perticone %s (%s #%d)\n", "0", "PAGING", 3);
    kprintf("\n");
    ram_bootstrap();
    vm_bootstrap();
    proc_bootstrap();
    thread_bootstrap();
    hardclock_bootstrap();
    vfs_bootstrap();
    kheap_nextgeneration();
    kprintf("Device probe...\n");
    int32_t v1; // 0x8000bb64
    int32_t * v2 = (int32_t *)(v1 + 92); // 0x8000bc1c
    if (*v2 <= 0) {
        // 0x8000bc2c
        badassert("curthread->t_curspl > 0", "../../main/main.c", 123, "boot");
    }
    // 0x8000bc48
    mainbus_bootstrap();
    if (*v2 != 0) {
        // 0x8000bc60
        badassert("curthread->t_curspl == 0", "../../main/main.c", 125, "boot");
    }
    // 0x8000bc7c
    pseudoconfig();
    kprintf("\n");
    kheap_nextgeneration();
    kprintf_bootstrap();
    thread_start_cpus();
    vfs_setbootfs("emu0");
    kheap_nextgeneration();
    init_swapfile();
}