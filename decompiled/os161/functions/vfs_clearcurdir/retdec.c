int32_t vfs_clearcurdir(void) {
    // 0x80018110
    int32_t v1; // 0x80018110
    int32_t * v2 = (int32_t *)(v1 + 84); // 0x8001811c
    spinlock_acquire((int32_t *)(*v2 + 8));
    int32_t * v3 = (int32_t *)(*v2 + 20); // 0x80018130
    int32_t v4 = *v3; // 0x80018130
    *v3 = 0;
    spinlock_release((int32_t *)(*v2 + 8));
    if (v4 != 0) {
        // 0x8001814c
        vnode_decref((int32_t *)v4);
    }
    // 0x80018158
    return 0;
}