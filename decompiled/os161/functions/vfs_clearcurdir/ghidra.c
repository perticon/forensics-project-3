int vfs_clearcurdir(void)

{
  vnode *vn;
  int unaff_s7;
  
  spinlock_acquire((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  vn = *(vnode **)(*(int *)(unaff_s7 + 0x54) + 0x14);
  *(undefined4 *)(*(int *)(unaff_s7 + 0x54) + 0x14) = 0;
  spinlock_release((spinlock *)(*(int *)(unaff_s7 + 0x54) + 8));
  if (vn != (vnode *)0x0) {
    vnode_decref(vn);
  }
  return 0;
}