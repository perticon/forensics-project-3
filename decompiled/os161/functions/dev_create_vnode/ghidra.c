vnode * dev_create_vnode(device *dev)

{
  vnode *vn;
  int errcode;
  char *pcVar1;
  
  vn = (vnode *)kmalloc(0x18);
  if (vn == (vnode *)0x0) {
    vn = (vnode *)0x0;
  }
  else {
    errcode = vnode_init(vn,&dev_vnode_ops,(fs *)0x0,dev);
    if (errcode != 0) {
      pcVar1 = strerror(errcode);
                    /* WARNING: Subroutine does not return */
      panic("While creating vnode for device: vnode_init: %s\n",pcVar1);
    }
  }
  return vn;
}