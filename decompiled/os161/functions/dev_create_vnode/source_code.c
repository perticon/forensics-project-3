dev_create_vnode(struct device *dev)
{
	int result;
	struct vnode *v;

	v = kmalloc(sizeof(struct vnode));
	if (v==NULL) {
		return NULL;
	}

	result = vnode_init(v, &dev_vnode_ops, NULL, dev);
	if (result != 0) {
		panic("While creating vnode for device: vnode_init: %s\n",
		      strerror(result));
	}

	return v;
}