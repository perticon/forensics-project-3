int32_t * dev_create_vnode(int32_t * dev) {
    char * v1 = kmalloc(24); // 0x80017e60
    if (v1 == NULL) {
        // 0x80017eb0
        return NULL;
    }
    int32_t v2 = vnode_init((int32_t *)v1, &dev_vnode_ops, NULL, (char *)dev); // 0x80017e80
    int32_t * result = (int32_t *)v1; // 0x80017e88
    if (v2 != 0) {
        // 0x80017e8c
        panic("While creating vnode for device: vnode_init: %s\n", strerror(v2));
        result = NULL;
    }
    // 0x80017eb0
    return result;
}