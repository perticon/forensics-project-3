int32_t threadtest2(int32_t nargs, char ** args) {
    // 0x800145e8
    init_sem();
    kprintf("Starting thread test 2...\n");
    runthreads(0);
    kprintf("\nThread test 2 done.\n");
    return 0;
}