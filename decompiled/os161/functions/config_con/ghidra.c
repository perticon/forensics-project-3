int config_con(con_softc *cs,int unit)

{
  semaphore *sem;
  semaphore *sem_00;
  int iVar1;
  lock *lock;
  lock *plVar2;
  char *pcVar3;
  
  if (0 < unit) {
    if (the_console != (con_softc *)0x0) {
      return 0x19;
    }
    pcVar3 = "the_console!=NULL";
    badassert("the_console!=NULL","../../dev/generic/console.c",0x165,"config_con");
    cs = (con_softc *)pcVar3;
  }
  if (the_console != (con_softc *)0x0) {
    badassert("the_console==NULL","../../dev/generic/console.c",0x168,"config_con");
  }
  sem = sem_create("console read",0);
  if (sem == (semaphore *)0x0) {
    iVar1 = 3;
  }
  else {
    sem_00 = sem_create(s_console_write_80022690,1);
    if (sem_00 == (semaphore *)0x0) {
      sem_destroy(sem);
      iVar1 = 3;
    }
    else {
      lock = lock_create("console-lock-read");
      if (lock == (lock *)0x0) {
        sem_destroy(sem);
        sem_destroy(sem_00);
        iVar1 = 3;
      }
      else {
        plVar2 = lock_create("console-lock-write");
        if (plVar2 == (lock *)0x0) {
          lock_destroy(lock);
          sem_destroy(sem);
          sem_destroy(sem_00);
          iVar1 = 3;
        }
        else {
          cs->cs_rsem = sem;
          cs->cs_wsem = sem_00;
          cs->cs_gotchars_head = 0;
          cs->cs_gotchars_tail = 0;
          con_userlock_write = plVar2;
          con_userlock_read = lock;
          the_console = cs;
          flush_delay_buf();
          iVar1 = attach_console_to_vfs(cs);
        }
      }
    }
  }
  return iVar1;
}