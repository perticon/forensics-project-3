int32_t config_con(int32_t * cs, uint32_t unit) {
    int32_t v1 = (int32_t)cs; // 0x80002070
    if (unit >= 1) {
        // 0x80002074
        if (the_console != NULL) {
            // 0x800021b8
            return 25;
        }
        // 0x80002088
        badassert("the_console!=NULL", "../../dev/generic/console.c", 357, "config_con");
        v1 = (int32_t)"the_console!=NULL";
    }
    // 0x800020a8
    if (the_console != NULL) {
        // 0x800020bc
        badassert("the_console==NULL", "../../dev/generic/console.c", 360, "config_con");
    }
    int32_t * v2 = sem_create("console read", 0); // 0x800020e8
    if (v2 == NULL) {
        // 0x800021b8
        return 3;
    }
    int32_t * v3 = sem_create("console write", 1); // 0x80002100
    if (v3 == NULL) {
        // 0x8000210c
        sem_destroy(v2);
        // 0x800021b8
        return 3;
    }
    int32_t * v4 = lock_create("console-lock-read"); // 0x80002124
    if (v4 == NULL) {
        // 0x80002130
        sem_destroy(v2);
        sem_destroy(v3);
        // 0x800021b8
        return 3;
    }
    int32_t * v5 = lock_create("console-lock-write"); // 0x80002150
    int32_t result; // 0x80002058
    if (v5 == NULL) {
        // 0x8000215c
        lock_destroy(v4);
        sem_destroy(v2);
        sem_destroy(v3);
        result = 3;
    } else {
        // 0x8000217c
        *(int32_t *)"=NULL" = (int32_t)v2;
        g7 = (int32_t)v3;
        g8 = 0;
        *(int32_t *)"console write" = 0;
        *(int32_t *)&the_console = v1;
        *(int32_t *)&con_userlock_read = (int32_t)v4;
        *(int32_t *)&con_userlock_write = (int32_t)v5;
        flush_delay_buf();
        result = attach_console_to_vfs((int32_t *)v1);
    }
    // 0x800021b8
    return result;
}