config_con(struct con_softc *cs, int unit)
{
	struct semaphore *rsem, *wsem;
	struct lock *rlk, *wlk;

	/*
	 * Only allow one system console.
	 * Further devices that could be the system console are ignored.
	 *
	 * Do not hardwire the console to be "con1" instead of "con0",
	 * or these asserts will go off.
	 */
	if (unit>0) {
		KASSERT(the_console!=NULL);
		return ENODEV;
	}
	KASSERT(the_console==NULL);

	rsem = sem_create("console read", 0);
	if (rsem == NULL) {
		return ENOMEM;
	}
	wsem = sem_create("console write", 1);
	if (wsem == NULL) {
		sem_destroy(rsem);
		return ENOMEM;
	}
	rlk = lock_create("console-lock-read");
	if (rlk == NULL) {
		sem_destroy(rsem);
		sem_destroy(wsem);
		return ENOMEM;
	}
	wlk = lock_create("console-lock-write");
	if (wlk == NULL) {
		lock_destroy(rlk);
		sem_destroy(rsem);
		sem_destroy(wsem);
		return ENOMEM;
	}

	cs->cs_rsem = rsem;
	cs->cs_wsem = wsem;
	cs->cs_gotchars_head = 0;
	cs->cs_gotchars_tail = 0;

	the_console = cs;
	con_userlock_read = rlk;
	con_userlock_write = wlk;

	flush_delay_buf();

	return attach_console_to_vfs(cs);
}