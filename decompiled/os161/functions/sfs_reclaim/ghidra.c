int sfs_reclaim(vnode *v)

{
  int iVar1;
  int iVar2;
  uint uVar3;
  array *a;
  uint uVar4;
  uint index;
  undefined4 unaff_s0;
  sfs_vnode *sv;
  sfs_fs *sfs;
  spinlock *splk;
  undefined4 in_stack_ffffffe8;
  
  sv = (sfs_vnode *)v->vn_data;
  sfs = (sfs_fs *)v->vn_fs->fs_data;
  vfs_biglock_acquire();
  splk = &v->vn_countlock;
  spinlock_acquire(splk);
  iVar1 = v->vn_refcount;
  if (iVar1 == 1) {
    spinlock_release(splk);
    if (((sv->sv_i).sfi_linkcount == 0) &&
       (iVar1 = sfs_itrunc(sv,CONCAT44(in_stack_ffffffe8,unaff_s0)), iVar1 != 0)) {
      vfs_biglock_release();
    }
    else {
      iVar1 = sfs_sync_inode(sv);
      if (iVar1 == 0) {
        if ((sv->sv_i).sfi_linkcount == 0) {
          sfs_bfree(sfs,sv->sv_ino);
        }
        a = (array *)sfs->sfs_vnodes;
        uVar3 = a->num;
        uVar4 = 0;
        while ((index = uVar3, uVar4 < uVar3 &&
               (index = uVar4, *(sfs_vnode **)((int)a->v[uVar4] + 0x10) != sv))) {
          uVar4 = uVar4 + 1;
        }
        if (index == uVar3) {
                    /* WARNING: Subroutine does not return */
          panic("sfs: %s: reclaim vnode %u not in vnode pool\n",(sfs->sfs_sb).sb_volname,sv->sv_ino)
          ;
        }
        array_remove(a,index);
        vnode_cleanup((vnode *)sv);
        vfs_biglock_release();
        kfree(sv);
        iVar1 = 0;
      }
      else {
        vfs_biglock_release();
      }
    }
  }
  else {
    iVar2 = iVar1 + -1;
    if (iVar1 < 2) {
      badassert("v->vn_refcount>1","../../fs/sfs/sfs_inode.c",0x57,"sfs_reclaim");
      iVar2 = iVar1;
    }
    v->vn_refcount = iVar2;
    spinlock_release(splk);
    vfs_biglock_release();
    iVar1 = 0x1b;
  }
  return iVar1;
}