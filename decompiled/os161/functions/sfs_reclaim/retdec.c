int32_t sfs_reclaim(int32_t * v2) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(v1 + 16); // 0x800084e0
    int32_t v3 = *(int32_t *)*(int32_t *)(v1 + 12); // 0x800084ec
    vfs_biglock_acquire();
    int32_t v4 = v1 + 4; // 0x800084f8
    int32_t * v5 = (int32_t *)v4; // 0x80008500
    spinlock_acquire(v5);
    if (v4 != 1) {
        int32_t v6 = v1 + 3; // 0x80008518
        if (v4 < 2) {
            // 0x8000851c
            badassert("v->vn_refcount>1", "../../fs/sfs/sfs_inode.c", 87, "sfs_reclaim");
            v6 = &g41;
        }
        // 0x80008540
        *v2 = v6;
        spinlock_release(v5);
        vfs_biglock_release();
        // 0x80008660
        return 27;
    }
    // 0x8000855c
    spinlock_release(v5);
    int16_t * v7 = (int16_t *)(v2_ + 30); // 0x80008564
    int32_t * v8 = (int32_t *)v2_;
    if (*v7 == 0) {
        int32_t result = sfs_itrunc(v8, 0); // 0x8000857c
        if (result != 0) {
            // 0x80008588
            vfs_biglock_release();
            // 0x80008660
            return result;
        }
    }
    int32_t result2 = sfs_sync_inode(v8); // 0x8000859c
    if (result2 != 0) {
        // 0x800085a8
        vfs_biglock_release();
        // 0x80008660
        return result2;
    }
    // 0x800085b8
    if (*v7 == 0) {
        // 0x800085c8
        sfs_bfree((int32_t *)v3, *(int32_t *)(v2_ + 536));
    }
    int32_t v9 = *(int32_t *)(v3 + 528); // 0x800085d4
    uint32_t v10 = *(int32_t *)(v9 + 4); // 0x800085dc
    int32_t v11; // 0x800084c8
    int32_t v12; // 0x800084c8
    if (v10 == 0) {
        goto lab_0x80008628;
    } else {
        int32_t v13 = 0;
        int32_t v14 = *(int32_t *)(4 * v13 + *(int32_t *)v9); // 0x800085f4
        while (*(int32_t *)(v14 + 16) != v2_) {
            int32_t v15 = v13 + 1; // 0x8000860c
            if (v15 >= v10) {
                goto lab_0x80008628;
            }
            v13 = v15;
            v14 = *(int32_t *)(4 * v13 + *(int32_t *)v9);
        }
        // 0x80008620
        v11 = v9;
        v12 = v13;
        if (v13 == v10) {
            goto lab_0x80008628;
        } else {
            goto lab_0x8000863c;
        }
    }
  lab_0x80008628:;
    int32_t v16 = *(int32_t *)(v2_ + 536); // 0x80008630
    int32_t v17 = v3 + 16; // 0x80008638
    panic("sfs: %s: reclaim vnode %u not in vnode pool\n", (char *)v17, v16);
    v11 = (int32_t)"sfs: %s: reclaim vnode %u not in vnode pool\n";
    v12 = v17;
    goto lab_0x8000863c;
  lab_0x8000863c:
    // 0x8000863c
    array_remove((int32_t *)v11, v12);
    vnode_cleanup(v8);
    vfs_biglock_release();
    kfree((char *)v2_);
    // 0x80008660
    return 0;
}