long random(void)

{
  uint32_t uVar1;
  
  if (the_random == (random_softc *)0x0) {
                    /* WARNING: Subroutine does not return */
    panic("No random device\n");
  }
  uVar1 = (*the_random->rs_random)(the_random->rs_devdata);
  return uVar1;
}