mips_usermode(struct trapframe *tf)
{

	/*
	 * Interrupts should be off within the kernel while entering
	 * user mode. However, while in user mode, interrupts should
	 * be on. To interact properly with the spl-handling logic
	 * above, we explicitly call spl0() and then call cpu_irqoff().
	 */
	spl0();
	cpu_irqoff();

	cputhreads[curcpu->c_number] = (vaddr_t)curthread;
	cpustacks[curcpu->c_number] = (vaddr_t)curthread->t_stack + STACK_SIZE;

	/*
	 * This assertion will fail if either
	 *   (1) cpustacks[] is corrupted, or
	 *   (2) the trap frame is not on our own kernel stack, or
	 *   (3) the boot thread tries to enter user mode.
	 *
	 * If cpustacks[] is corrupted, the next trap back to the
	 * kernel will (most likely) hang the system, so it's better
	 * to find out now.
	 *
	 * It's necessary for the trap frame used here to be on the
	 * current thread's own stack. It cannot correctly be on
	 * either another thread's stack or in the kernel heap.
	 * (Exercise: why?)
	 */
	KASSERT(SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf));

	/*
	 * This actually does it. See exception-*.S.
	 */
	asm_usermode(tf);
}