void mips_usermode(trapframe *tf)

{
  char *tf_00;
  char *ctf;
  char *in_a1;
  char *whence;
  uint32_t in_a2;
  char *in_a3;
  uint32_t uVar1;
  vaddr_t unaff_s7;
  uint32_t uStack232;
  uint32_t auStack228 [2];
  char *pcStack220;
  uint32_t uStack216;
  undefined4 uStack212;
  trapframe tStack192;
  trapframe *ptStack44;
  
  splx(0);
  cpu_irqoff();
  cputhreads[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = unaff_s7;
  cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] = *(int *)(unaff_s7 + 0x48) + 0x1000;
  if (((cpustacks[*(int *)(*(int *)(unaff_s7 + 0x50) + 4)] - 1 ^ (uint)tf) & 0xfffff000) != 0) {
    in_a1 = "../../arch/mips/locore/trap.c";
    in_a2 = 0x193;
    in_a3 = "mips_usermode";
    badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)",
              "../../arch/mips/locore/trap.c",0x193,"mips_usermode");
  }
  tStack192.tf_a0 = (uint32_t)tf;
  asm_usermode();
  ptStack44 = tf;
  bzero(&tStack192,0x8c);
  tStack192.tf_status = 0xff0c;
  uStack212 = 0x8001ee08;
  tf_00 = (char *)&tStack192;
  tStack192.tf_a1 = (uint32_t)in_a1;
  tStack192.tf_a2 = in_a2;
  tStack192.tf_sp = (uint32_t)in_a3;
  mips_usermode((trapframe *)tf_00);
  pcStack220 = in_a3;
  uStack216 = in_a2;
  if (unaff_s7 == 0) {
    tf_00 = "curthread != NULL";
    badassert("curthread != NULL","../../arch/mips/syscall/syscall.c",0x59,"syscall");
  }
  ctf = tf_00;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    ctf = "curthread->t_curspl == 0";
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0x5a,"syscall");
  }
  whence = (char *)0x80030000;
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    ctf = "curthread->t_iplhigh_count == 0";
    whence = "../../arch/mips/syscall/syscall.c";
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0x5b,"syscall");
  }
  uVar1 = *(uint32_t *)((int)ctf + 0x1c);
  uStack232 = 0;
  switch(uVar1) {
  case 0:
    auStack228[0] = sys_fork((trapframe *)ctf,(pid_t *)&uStack232);
    break;
  default:
    kprintf("Unknown syscall %d\n",uVar1);
    if (uVar1 == 3) {
      kprintf("Unknown syscall %d\n",3);
    }
    auStack228[0] = 1;
    break;
  case 3:
    sys__exit(*(uint32_t *)((int)ctf + 0x24));
    auStack228[0] = 0;
    break;
  case 4:
    uStack232 = sys_waitpid(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                            *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 5:
    uStack232 = sys_getpid();
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x2d:
    uStack232 = sys_open(*(userptr_t *)((int)ctf + 0x24),*(uint32_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c),(int *)auStack228);
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x31:
    uStack232 = sys_close(*(uint32_t *)((int)ctf + 0x24));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x32:
    uStack232 = sys_read(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                         *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x37:
    uStack232 = sys_write(*(uint32_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28),
                          *(uint32_t *)((int)tf_00 + 0x2c));
    if ((int)uStack232 < 0) {
      auStack228[0] = 1;
    }
    else {
      auStack228[0] = 0;
    }
    break;
  case 0x3b:
    auStack228[0] =
         sys_lseek(*(uint32_t *)((int)ctf + 0x24),
                   CONCAT44(*(uint32_t *)((int)tf_00 + 0x2c),&uStack232),(int)whence,(int *)0x0);
    break;
  case 0x44:
    break;
  case 0x71:
    auStack228[0] = sys___time(*(userptr_t *)((int)ctf + 0x24),*(userptr_t *)((int)tf_00 + 0x28));
    break;
  case 0x77:
    auStack228[0] = sys_reboot(*(uint32_t *)((int)ctf + 0x24));
  }
  if (auStack228[0] == 0) {
    *(uint32_t *)((int)tf_00 + 0x1c) = uStack232;
    *(uint32_t *)((int)tf_00 + 0x30) = 0;
  }
  else {
    *(uint32_t *)((int)tf_00 + 0x1c) = auStack228[0];
    *(uint32_t *)((int)tf_00 + 0x30) = 1;
  }
  *(uint32_t *)((int)tf_00 + 0x88) = *(uint32_t *)((int)tf_00 + 0x88) + 4;
  if (*(int *)(unaff_s7 + 0x5c) != 0) {
    badassert("curthread->t_curspl == 0","../../arch/mips/syscall/syscall.c",0xed,"syscall");
  }
  if (*(int *)(unaff_s7 + 0x60) != 0) {
    badassert("curthread->t_iplhigh_count == 0","../../arch/mips/syscall/syscall.c",0xef,"syscall");
  }
  return;
}