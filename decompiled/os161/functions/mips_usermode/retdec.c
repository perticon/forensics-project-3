void mips_usermode(int32_t * tf) {
    int32_t v1 = (int32_t)tf;
    splx(0);
    cpu_irqoff();
    int32_t v2; // 0x8001ecdc
    int32_t * v3 = (int32_t *)(v2 + 80); // 0x8001ecfc
    int32_t v4 = *(int32_t *)(*v3 + 4); // 0x8001ed28
    int32_t v5 = *(int32_t *)(v2 + 72); // 0x8001ed2c
    *(int32_t *)(4 * v4 + (int32_t)&cpustacks) = v5 + 0x1000;
    int32_t v6 = *(int32_t *)(*v3 + 4); // 0x8001ed54
    if ((*(int32_t *)(4 * v6 + (int32_t)&cpustacks) - 1 ^ v1) >= 0x1000) {
        // 0x8001ed84
        badassert("SAME_STACK(cpustacks[curcpu->c_number]-1, (vaddr_t)tf)", "../../arch/mips/locore/trap.c", 403, "mips_usermode");
    }
    // 0x8001eda0
    asm_usermode(v1);
}