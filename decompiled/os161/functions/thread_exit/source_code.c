thread_exit(void)
{
	struct thread *cur;

	cur = curthread;

	/*
	 * Detach from our process. You might need to move this action
	 * around, depending on how your wait/exit works.
	 *
	 *Check if the thread has already been detached from the process.
	 *In this case, do no detach it again.
	 */

#if !OPT_WAITPID
	proc_remthread(cur);
#endif

#if OPT_WAITPID
	if(cur->t_proc != NULL){
		proc_remthread(cur);
	}
#endif
	/* Make sure we *are* detached (move this only if you're sure!) */
	KASSERT(cur->t_proc == NULL);

	/* Check the stack guard band. */
	thread_checkstack(cur);
	

	/* Interrupts off on this processor */
        splhigh();
	thread_switch(S_ZOMBIE, NULL, NULL);
	panic("braaaaaaaiiiiiiiiiiinssssss\n");
}