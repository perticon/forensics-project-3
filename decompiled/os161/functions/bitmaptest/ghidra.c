int bitmaptest(int nargs,char **args)

{
  long lVar1;
  bitmap *b;
  uint32_t uVar2;
  char *pcVar3;
  char *pcVar4;
  int iVar5;
  uint unaff_s1;
  uint uVar6;
  uint uVar7;
  char data [533];
  uint32_t x;
  
  kprintf("Starting bitmap test...\n");
  for (iVar5 = 0; iVar5 < 0x215; iVar5 = iVar5 + 1) {
    lVar1 = random();
    data[iVar5] = (byte)lVar1 & 1;
  }
  b = bitmap_create(0x215);
  if (b != (bitmap *)0x0) {
    unaff_s1 = 0;
    goto LAB_8000efa4;
  }
  pcVar4 = "b != NULL";
  badassert("b != NULL","../../test/bitmaptest.c",0x37,"bitmaptest");
  do {
    iVar5 = bitmap_isset((bitmap *)pcVar4,unaff_s1);
    if (iVar5 != 0) {
      badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x3a,"bitmaptest");
    }
    unaff_s1 = unaff_s1 + 1;
LAB_8000efa4:
    pcVar4 = (char *)b;
  } while ((int)unaff_s1 < 0x215);
  for (uVar6 = 0; (int)uVar6 < 0x215; uVar6 = uVar6 + 1) {
    if (data[uVar6] != '\0') {
      bitmap_mark(b,uVar6);
    }
  }
  uVar6 = 0;
LAB_8000f068:
  uVar7 = uVar6;
  if ((int)uVar7 < 0x215) {
    pcVar4 = (char *)b;
    if (data[uVar7] != '\0') goto code_r0x8000f000;
    goto LAB_8000f034;
  }
  uVar6 = 0;
  while ((int)uVar6 < 0x215) {
    if (data[uVar6] == '\0') {
      bitmap_mark(b,uVar6);
      uVar6 = uVar6 + 1;
    }
    else {
      bitmap_unmark(b,uVar6);
      uVar6 = uVar6 + 1;
    }
  }
  uVar6 = 0;
LAB_8000f13c:
  uVar7 = uVar6;
  if (0x214 < (int)uVar7) {
    while (iVar5 = bitmap_alloc(b,&x), iVar5 == 0) {
      pcVar4 = (char *)x;
      if (0x214 < x) {
        pcVar4 = "../../test/bitmaptest.c";
        badassert("x < TESTSIZE","../../test/bitmaptest.c",0x5d,"bitmaptest");
      }
      iVar5 = bitmap_isset(b,(uint)pcVar4);
      pcVar4 = data;
      if (iVar5 == 0) {
        pcVar4 = "bitmap_isset(b, x)";
        badassert("bitmap_isset(b, x)","../../test/bitmaptest.c",0x5e,"bitmaptest");
      }
      pcVar3 = data;
      uVar2 = x;
      if (pcVar4[x] != '\x01') {
        badassert("data[x]==1","../../test/bitmaptest.c",0x5f,"bitmaptest");
      }
      pcVar3[uVar2] = '\0';
    }
    uVar6 = 0;
    while (uVar7 = uVar6, (int)uVar7 < 0x215) {
      iVar5 = bitmap_isset(b,uVar7);
      pcVar4 = data;
      if (iVar5 == 0) {
        pcVar4 = "bitmap_isset(b, i)";
        badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",100,"bitmaptest");
      }
      uVar6 = uVar7 + 1;
      if (pcVar4[uVar7] != '\0') {
        badassert("data[i]==0","../../test/bitmaptest.c",0x65,"bitmaptest");
        uVar6 = uVar7;
      }
    }
    kprintf("Bitmap test complete\n");
    return 0;
  }
  pcVar4 = (char *)b;
  if (data[uVar7] != '\0') goto code_r0x8000f0d4;
  goto LAB_8000f108;
code_r0x8000f000:
  iVar5 = bitmap_isset(b,uVar7);
  uVar6 = uVar7 + 1;
  if (iVar5 == 0) {
    pcVar4 = "bitmap_isset(b, i)";
    badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",0x44,"bitmaptest");
LAB_8000f034:
    iVar5 = bitmap_isset((bitmap *)pcVar4,uVar7);
    uVar6 = uVar7 + 1;
    if (iVar5 != 0) {
      badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x47,"bitmaptest");
      uVar6 = uVar7;
    }
  }
  goto LAB_8000f068;
code_r0x8000f0d4:
  iVar5 = bitmap_isset(b,uVar7);
  uVar6 = uVar7 + 1;
  if (iVar5 != 0) {
    pcVar4 = "bitmap_isset(b, i)==0";
    badassert("bitmap_isset(b, i)==0","../../test/bitmaptest.c",0x55,"bitmaptest");
LAB_8000f108:
    iVar5 = bitmap_isset((bitmap *)pcVar4,uVar7);
    uVar6 = uVar7 + 1;
    if (iVar5 == 0) {
      badassert("bitmap_isset(b, i)","../../test/bitmaptest.c",0x58,"bitmaptest");
      uVar6 = uVar7;
    }
  }
  goto LAB_8000f13c;
}