int32_t bitmaptest(int32_t nargs, char ** args) {
    char data[533]; // bp-560, 0x8000eef0
    // 0x8000eef0
    kprintf("Starting bitmap test...\n");
    int32_t v1 = (int32_t)&data;
    for (int32_t i = 0; i < 533; i++) {
        // 0x8000ef14
        *(char *)(i + v1) = (char)random() % 2;
    }
    int32_t * v2 = bitmap_create(533); // 0x8000ef40
    int32_t v3 = 0; // 0x8000ef48
    int32_t v4; // 0x8000eef0
    if (v2 == NULL) {
        // 0x8000ef4c
        badassert("b != NULL", "../../test/bitmaptest.c", 55, "bitmaptest");
        v4 = (int32_t)"b != NULL";
        goto lab_0x8000ef6c;
    } else {
        goto lab_0x8000efa4;
    }
  lab_0x8000efa4:;
    int32_t v5 = (int32_t)v2; // 0x8000ef40
    v4 = v5;
    int32_t v6 = v3; // 0x8000efac
    if (v3 >= 533) {
        int32_t v7 = 0;
        if (*(char *)(v7 + v1) != 0) {
            // 0x8000efcc
            bitmap_mark(v2, v7);
        }
        int32_t v8 = v7 + 1; // 0x8000efd4
        while (v8 < 533) {
            // 0x8000efb8
            v7 = v8;
            if (*(char *)(v7 + v1) != 0) {
                // 0x8000efcc
                bitmap_mark(v2, v7);
            }
            // 0x8000efd4
            v8 = v7 + 1;
        }
        int32_t v9 = 0;
        int32_t v10 = v5; // 0x8000effc
        if (*(char *)(v9 + v1) != 0) {
            // 0x8000f000
            if (bitmap_isset(v2, v9) != 0) {
                // break -> 0x8000f068
                break;
            }
            // 0x8000f010
            badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 68, "bitmaptest");
            v10 = (int32_t)"bitmap_isset(b, i)";
        }
        while (bitmap_isset((int32_t *)v10, v9) != 0) {
            // 0x8000f044
            badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 71, "bitmaptest");
            v10 = v5;
            if (*(char *)(v9 + v1) != 0) {
                // 0x8000f000
                if (bitmap_isset(v2, v9) != 0) {
                    // break -> 0x8000f068
                    break;
                }
                // 0x8000f010
                badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 68, "bitmaptest");
                v10 = (int32_t)"bitmap_isset(b, i)";
            }
        }
        int32_t v11 = v9 + 1;
        int32_t v12 = 0; // 0x8000f070
        while (v11 < 533) {
            // 0x8000efec
            v9 = v11;
            v10 = v5;
            if (*(char *)(v9 + v1) != 0) {
                // 0x8000f000
                if (bitmap_isset(v2, v9) != 0) {
                    // break -> 0x8000f068
                    break;
                }
                // 0x8000f010
                badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 68, "bitmaptest");
                v10 = (int32_t)"bitmap_isset(b, i)";
            }
            while (bitmap_isset((int32_t *)v10, v9) != 0) {
                // 0x8000f044
                badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 71, "bitmaptest");
                v10 = v5;
                if (*(char *)(v9 + v1) != 0) {
                    // 0x8000f000
                    if (bitmap_isset(v2, v9) != 0) {
                        // break -> 0x8000f068
                        break;
                    }
                    // 0x8000f010
                    badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 68, "bitmaptest");
                    v10 = (int32_t)"bitmap_isset(b, i)";
                }
            }
            // 0x8000f068
            v11 = v9 + 1;
            v12 = 0;
        }
        if (*(char *)(v12 + v1) == 0) {
            // 0x8000f0a0
            bitmap_mark(v2, v12);
        } else {
            // 0x8000f090
            bitmap_unmark(v2, v12);
        }
        int32_t v13 = v12 + 1;
        while (v13 < 533) {
            int32_t v14 = v13;
            if (*(char *)(v14 + v1) == 0) {
                // 0x8000f0a0
                bitmap_mark(v2, v14);
            } else {
                // 0x8000f090
                bitmap_unmark(v2, v14);
            }
            // 0x8000f0ac
            v13 = v14 + 1;
        }
        int32_t v15 = 0;
        int32_t v16 = v5; // 0x8000f0d0
        if (*(char *)(v15 + v1) != 0) {
            // 0x8000f0d4
            if (bitmap_isset(v2, v15) == 0) {
                // break -> 0x8000f13c
                break;
            }
            // 0x8000f0e4
            badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 85, "bitmaptest");
            v16 = (int32_t)"bitmap_isset(b, i)==0";
        }
        while (bitmap_isset((int32_t *)v16, v15) == 0) {
            // 0x8000f118
            badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 88, "bitmaptest");
            v16 = v5;
            if (*(char *)(v15 + v1) != 0) {
                // 0x8000f0d4
                if (bitmap_isset(v2, v15) == 0) {
                    // break -> 0x8000f13c
                    break;
                }
                // 0x8000f0e4
                badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 85, "bitmaptest");
                v16 = (int32_t)"bitmap_isset(b, i)==0";
            }
        }
        int32_t v17 = v15 + 1;
        while (v17 < 533) {
            // 0x8000f0c0
            v15 = v17;
            v16 = v5;
            if (*(char *)(v15 + v1) != 0) {
                // 0x8000f0d4
                if (bitmap_isset(v2, v15) == 0) {
                    // break -> 0x8000f13c
                    break;
                }
                // 0x8000f0e4
                badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 85, "bitmaptest");
                v16 = (int32_t)"bitmap_isset(b, i)==0";
            }
            while (bitmap_isset((int32_t *)v16, v15) == 0) {
                // 0x8000f118
                badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 88, "bitmaptest");
                v16 = v5;
                if (*(char *)(v15 + v1) != 0) {
                    // 0x8000f0d4
                    if (bitmap_isset(v2, v15) == 0) {
                        // break -> 0x8000f13c
                        break;
                    }
                    // 0x8000f0e4
                    badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 85, "bitmaptest");
                    v16 = (int32_t)"bitmap_isset(b, i)==0";
                }
            }
            // 0x8000f13c
            v17 = v15 + 1;
        }
        // 0x8000f1f4
        int32_t x; // bp-24, 0x8000eef0
        if (bitmap_alloc(v2, &x) == 0) {
            int32_t v18 = x; // 0x8000f160
            if (x >= 533) {
                // 0x8000f164
                badassert("x < TESTSIZE", "../../test/bitmaptest.c", 93, "bitmaptest");
                v18 = (int32_t)"../../test/bitmaptest.c";
            }
            int32_t v19 = v1; // 0x8000f18c
            if (bitmap_isset(v2, v18) == 0) {
                // 0x8000f190
                badassert("bitmap_isset(b, x)", "../../test/bitmaptest.c", 94, "bitmaptest");
                v19 = (int32_t)"bitmap_isset(b, x)";
            }
            int32_t v20 = x; // 0x8000f1c8
            if (*(char *)(x + v19) != 1) {
                // 0x8000f1cc
                badassert("data[x]==1", "../../test/bitmaptest.c", 95, "bitmaptest");
                v20 = &g41;
            }
            // 0x8000f1ec
            *(char *)(v20 + v1) = 0;
            while (bitmap_alloc(v2, &x) == 0) {
                // 0x8000f150
                v18 = x;
                if (x >= 533) {
                    // 0x8000f164
                    badassert("x < TESTSIZE", "../../test/bitmaptest.c", 93, "bitmaptest");
                    v18 = (int32_t)"../../test/bitmaptest.c";
                }
                // 0x8000f180
                v19 = v1;
                if (bitmap_isset(v2, v18) == 0) {
                    // 0x8000f190
                    badassert("bitmap_isset(b, x)", "../../test/bitmaptest.c", 94, "bitmaptest");
                    v19 = (int32_t)"bitmap_isset(b, x)";
                }
                // 0x8000f1b0
                v20 = x;
                if (*(char *)(x + v19) != 1) {
                    // 0x8000f1cc
                    badassert("data[x]==1", "../../test/bitmaptest.c", 95, "bitmaptest");
                    v20 = &g41;
                }
                // 0x8000f1ec
                *(char *)(v20 + v1) = 0;
            }
        }
        for (int32_t i = 0; i < 533; i++) {
            int32_t v21 = v1; // 0x8000f220
            if (bitmap_isset(v2, i) == 0) {
                // 0x8000f224
                badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 100, "bitmaptest");
                v21 = (int32_t)"bitmap_isset(b, i)";
            }
            // 0x8000f210
            while (*(char *)(v21 + i) != 0) {
                // 0x8000f258
                badassert("data[i]==0", "../../test/bitmaptest.c", 101, "bitmaptest");
                v21 = v1;
                if (bitmap_isset(v2, i) == 0) {
                    // 0x8000f224
                    badassert("bitmap_isset(b, i)", "../../test/bitmaptest.c", 100, "bitmaptest");
                    v21 = (int32_t)"bitmap_isset(b, i)";
                }
            }
        }
        // 0x8000f288
        kprintf("Bitmap test complete\n");
        return 0;
    }
    goto lab_0x8000ef6c;
  lab_0x8000ef6c:
    // 0x8000ef6c
    if (bitmap_isset((int32_t *)v4, v6) != 0) {
        // 0x8000ef7c
        badassert("bitmap_isset(b, i)==0", "../../test/bitmaptest.c", 58, "bitmaptest");
    }
    // 0x8000ef98
    v3 = v6 + 1;
    goto lab_0x8000efa4;
}