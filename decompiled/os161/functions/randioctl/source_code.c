randioctl(struct device *dev, int op, userptr_t data)
{
	/*
	 * We don't support any ioctls.
	 */
	(void)dev;
	(void)op;
	(void)data;
	return EIOCTL;
}