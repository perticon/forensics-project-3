int32_t lamebus_read_register(int32_t * bus, int32_t slot, int32_t offset) {
    // 0x80020870
    lamebus_map_area(bus, slot, offset);
    return *(int32_t *)__asm_sync();
}