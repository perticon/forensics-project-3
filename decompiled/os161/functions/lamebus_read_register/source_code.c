lamebus_read_register(struct lamebus_softc *bus, int slot, uint32_t offset)
{
	uint32_t *ptr;

	ptr = lamebus_map_area(bus, slot, offset);

	/*
	 * Make sure the load happens after anything the device has
	 * been doing.
	 */
	membar_load_load();

	return *ptr;
}