uint32_t lamebus_read_register(lamebus_softc *bus,int slot,uint32_t offset)

{
  uint32_t *puVar1;
  
  puVar1 = (uint32_t *)lamebus_map_area(bus,slot,offset);
  SYNC(0);
  return *puVar1;
}