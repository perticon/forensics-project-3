cmd_progthread(void *ptr, unsigned long nargs)
{
	char **args = ptr;
	char progname[128];
	int result;

	KASSERT(nargs >= 1);

	if (nargs > 2)
	{
		kprintf("Warning: argument passing from menu not supported\n");
	}

	/* Hope we fit. */
	KASSERT(strlen(args[0]) < sizeof(progname));

	strcpy(progname, args[0]);

	result = runprogram(progname);
	if (result)
	{
		kprintf("Running program %s failed: %s\n", args[0],
				strerror(result));
		#if OPT_PAGING
			sys__exit(-1);
		#endif
		
		return;
	}

	/* NOTREACHED: runprogram only returns on error. */
}