void cmd_progthread(void *ptr,ulong nargs)

{
  size_t sVar1;
  int errcode;
  char *pcVar2;
  char *pcVar3;
  char progname [128];
  
  if (nargs == 0) {
    pcVar2 = "nargs >= 1";
    pcVar3 = "../../main/menu.c";
    badassert("nargs >= 1","../../main/menu.c",0x53,"cmd_progthread");
    ptr = pcVar2;
    nargs = (ulong)pcVar3;
  }
  if ((char *)0x2 < nargs) {
    kprintf("Warning: argument passing from menu not supported\n");
  }
                    /* WARNING: Load size is inaccurate */
  sVar1 = strlen(*ptr);
  if (0x7f < sVar1) {
    badassert("strlen(args[0]) < sizeof(progname)","../../main/menu.c",0x5b,"cmd_progthread");
  }
                    /* WARNING: Load size is inaccurate */
  strcpy(progname,*ptr);
  errcode = runprogram(progname);
  if (errcode != 0) {
                    /* WARNING: Load size is inaccurate */
    pcVar3 = *ptr;
    pcVar2 = strerror(errcode);
    kprintf("Running program %s failed: %s\n",pcVar3,pcVar2);
    sys__exit(-1);
  }
  return;
}