void cmd_progthread(char * ptr, int32_t nargs) {
    char progname[128]; // bp-136, 0x8000c8b0
    int32_t v1 = nargs; // 0x8000c8bc
    if (nargs == 0) {
        // 0x8000c8c0
        badassert("nargs >= 1", "../../main/menu.c", 83, "cmd_progthread");
        v1 = (int32_t)"../../main/menu.c";
    }
    // 0x8000c8e0
    if (v1 >= 3) {
        // 0x8000c8ec
        kprintf("Warning: argument passing from menu not supported\n");
    }
    // 0x8000c8f8
    if (strlen((char *)0x6772616e) >= 128) {
        // 0x8000c910
        badassert("strlen(args[0]) < sizeof(progname)", "../../main/menu.c", 91, "cmd_progthread");
    }
    // 0x8000c92c
    strcpy(progname, (char *)0x6772616e);
    int32_t v2 = runprogram(progname); // 0x8000c93c
    if (v2 != 0) {
        // 0x8000c948
        kprintf("Running program %s failed: %s\n", (char *)0x6772616e, strerror(v2));
        sys__exit(-1);
    }
}