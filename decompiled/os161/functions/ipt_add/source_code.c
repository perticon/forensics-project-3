int ipt_add(pid_t pid, paddr_t paddr, vaddr_t vaddr)
{
    int frame_index;
    Item item;
    KASSERT(pid >= 0);
    KASSERT(paddr != 0);
    KASSERT(vaddr != 0);

    frame_index = paddr / PAGE_SIZE;
    KASSERT(frame_index < nRamFrames);

    spinlock_acquire(&ipt_lock);
    if (ipt_active)
    {

        item = ITEMscan(pid, vaddr, frame_index);
        KASSERT(ipt_active);
        ipt[frame_index].pid = pid;
        ipt[frame_index].vaddr = vaddr;

        /*Add entry to hash table*/
        STinsert(ipt_hash, item);
        // STdisplay(ipt_hash);
    }
    spinlock_release(&ipt_lock);

    return 0;
}