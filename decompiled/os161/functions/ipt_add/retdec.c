int32_t ipt_add(int32_t pid, uint32_t paddr, int32_t vaddr) {
    int32_t v1; // 0x8001ce04
    int32_t v2; // 0x8001ce04
    if (pid > -1) {
        if (paddr == 0) {
            // 0x8001ce48
            badassert("paddr != 0", "../../vm/pt.c", 171, "ipt_add");
            v1 = (int32_t)((uint32_t)(int32_t)"../../vm/pt.c" / 0x1000);
            v2 = vaddr;
        } else {
            int32_t v3 = (uint32_t)(paddr / 0x1000); // 0x8001ce6c
            v1 = v3;
            v2 = vaddr;
            if (vaddr == 0) {
                // 0x8001ce70
                badassert("vaddr != 0", "../../vm/pt.c", 172, "ipt_add");
                v1 = v3;
                v2 = 0;
            }
        }
    } else {
        // 0x8001ce40
        badassert("pid >= 0", "../../vm/pt.c", 170, "ipt_add");
        v1 = (int32_t)((uint32_t)(int32_t)"../../vm/pt.c" / 0x1000);
        v2 = 170;
    }
    // 0x8001ce90
    if (v1 >= nRamFrames) {
        // 0x8001cea8
        badassert("frame_index < nRamFrames", "../../vm/pt.c", 175, "ipt_add");
    }
    // 0x8001cec4
    spinlock_acquire(&ipt_lock);
    if (ipt_active == 0) {
        // 0x8001cf4c
        spinlock_release(&ipt_lock);
        return 0;
    }
    int32_t v4 = (int32_t)ITEMscan(pid, v2, v1); // 0x8001cf00
    if (ipt_active == 0) {
        // 0x8001cf04
        badassert("ipt_active", "../../vm/pt.c", 182, "ipt_add");
        v4 = &g41;
    }
    int32_t v5 = 8 * v1 + (int32_t)ipt; // 0x8001cf30
    *(int32_t *)v5 = pid;
    *(int32_t *)(v5 + 4) = v2;
    STinsert(ipt_hash, (int32_t *)v4);
    // 0x8001cf4c
    spinlock_release(&ipt_lock);
    return 0;
}