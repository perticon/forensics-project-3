int ipt_add(pid_t pid,paddr_t paddr,vaddr_t vaddr)

{
  Item item;
  int iVar1;
  char *pcVar2;
  vaddr_t vVar3;
  uint index;
  pid_t *ppVar4;
  
  pcVar2 = (char *)paddr;
  if (pid < 0) {
    pcVar2 = "../../vm/pt.c";
    vaddr = 0xaa;
    badassert("pid >= 0","../../vm/pt.c",0xaa,"ipt_add");
  }
  vVar3 = vaddr;
  if (pcVar2 == (char *)0x0) {
    pcVar2 = "../../vm/pt.c";
    vVar3 = 0xab;
    badassert("paddr != 0","../../vm/pt.c",0xab,"ipt_add");
  }
  index = (uint)pcVar2 >> 0xc;
  if (vVar3 == 0) {
    badassert("vaddr != 0","../../vm/pt.c",0xac,"ipt_add");
  }
  if (nRamFrames <= (int)index) {
    badassert("frame_index < nRamFrames","../../vm/pt.c",0xaf,"ipt_add");
  }
  spinlock_acquire(&ipt_lock);
  if (ipt_active != 0) {
    item = ITEMscan(pid,vaddr,index);
    iVar1 = -0x7ffd0000;
    if (ipt_active == 0) {
      badassert("ipt_active","../../vm/pt.c",0xb6,"ipt_add");
    }
    ppVar4 = (pid_t *)(*(int *)(iVar1 + -0x72f0) + index * 8);
    *ppVar4 = pid;
    ppVar4[1] = vaddr;
    STinsert(ipt_hash,item);
  }
  spinlock_release(&ipt_lock);
  return 0;
}