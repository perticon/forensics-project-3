void ipi_send(cpu *target,int code)

{
  char *pcVar1;
  
  if (0x1f < (uint)code) {
    pcVar1 = "code >= 0 && code < 32";
    badassert("code >= 0 && code < 32","../../thread/thread.c",0x45b,"ipi_send");
    target = (cpu *)pcVar1;
  }
  spinlock_acquire(&target->c_ipi_lock);
  target->c_ipi_pending = target->c_ipi_pending | 1 << (code & 0x1fU);
  mainbus_send_ipi(target);
  spinlock_release(&target->c_ipi_lock);
  return;
}