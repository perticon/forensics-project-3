void ipi_send(int32_t * target, uint32_t code) {
    int32_t v1 = (int32_t)target; // 0x8001682c
    if (code >= 32) {
        // 0x80016830
        badassert("code >= 0 && code < 32", "../../thread/thread.c", 1115, "ipi_send");
        v1 = (int32_t)"code >= 0 && code < 32";
    }
    // 0x80016850
    spinlock_acquire((int32_t *)"c_number == software_number");
    *(int32_t *)"ead_switch\n" = 1 << code | 0x5f646165;
    mainbus_send_ipi((int32_t *)v1);
    spinlock_release((int32_t *)"c_number == software_number");
}