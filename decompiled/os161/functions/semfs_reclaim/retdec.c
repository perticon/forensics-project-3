int32_t semfs_reclaim(int32_t * vn) {
    int32_t v1 = (int32_t)vn;
    int32_t v2 = *(int32_t *)(v1 + 16); // 0x80006ad4
    int32_t v3 = *(int32_t *)(v2 + 24); // 0x80006adc
    int32_t * v4 = (int32_t *)(v3 + 8); // 0x80006ae4
    uint32_t v5 = v1 + 4; // 0x80006aec
    lock_acquire((int32_t *)*v4);
    int32_t * v6 = (int32_t *)v5; // 0x80006af4
    spinlock_acquire(v6);
    if (v5 >= 2) {
        // 0x80006b0c
        *vn = v1 + 3;
        spinlock_release(v6);
        lock_release((int32_t *)*v4);
        // 0x80006c8c
        return 27;
    }
    // 0x80006b30
    spinlock_release(v6);
    int32_t v7 = *(int32_t *)(v3 + 12); // 0x80006b38
    int32_t * v8 = (int32_t *)v7;
    uint32_t v9 = 0;
    while (v9 < *(int32_t *)(v7 + 4)) {
        // 0x80006b4c
        if (*(int32_t *)(*v8 + 4 * v9) == v1) {
            // 0x80006b68
            array_remove(v8, v9);
            goto lab_0x80006b84;
        }
        v9++;
    }
    goto lab_0x80006b84;
  lab_0x80006b84:;
    int32_t * v10 = (int32_t *)(v2 + 28); // 0x80006b84
    uint32_t v11 = *v10; // 0x80006b84
    if (v11 != -1) {
        int32_t * v12 = (int32_t *)(v3 + 16); // 0x80006b94
        int32_t v13 = *v12; // 0x80006b94
        int32_t v14 = 4 * v11; // 0x80006bac
        if (v11 >= *(int32_t *)(v13 + 4)) {
            // 0x80006bb0
            badassert("index < a->num", "../../include/array.h", 100, "array_get");
            v14 = &g41;
        }
        int32_t v15 = *(int32_t *)(*(int32_t *)v13 + v14); // 0x80006bdc
        int32_t v16 = v15; // 0x80006bf0
        if (*(char *)(v15 + 12) == 0) {
            // 0x80006bf4
            badassert("sem->sems_hasvnode", "../../fs/semfs/semfs_vnops.c", 631, "semfs_reclaim");
            v16 = (int32_t)"sem->sems_hasvnode";
        }
        // 0x80006c10
        *(char *)(v16 + 12) = 0;
        if (*(char *)(v16 + 13) == 0) {
            int32_t v17 = *v12; // 0x80006c24
            uint32_t v18 = *v10; // 0x80006c28
            int32_t v19 = 4 * v18; // 0x80006c3c
            int32_t v20 = v16; // 0x80006c3c
            if (v18 >= *(int32_t *)(v17 + 4)) {
                // 0x80006c40
                badassert("index < a->num", "../../include/array.h", 107, "array_set");
                v19 = &g41;
                v20 = (int32_t)"index < a->num";
            }
            // 0x80006c60
            *(int32_t *)(*(int32_t *)v17 + v19) = 0;
            semfs_sem_destroy((int32_t *)v20);
        }
    }
    // 0x80006c74
    lock_release((int32_t *)*v4);
    semfs_vnode_destroy((int32_t *)v2);
    // 0x80006c8c
    return 0;
}