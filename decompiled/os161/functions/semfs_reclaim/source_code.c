semfs_reclaim(struct vnode *vn)
{
	struct semfs_vnode *semv = vn->vn_data;
	struct semfs *semfs = semv->semv_semfs;
	struct vnode *vn2;
	struct semfs_sem *sem;
	unsigned i, num;

	lock_acquire(semfs->semfs_tablelock);

	/* vnode refcount is protected by the vnode's ->vn_countlock */
	spinlock_acquire(&vn->vn_countlock);
	if (vn->vn_refcount > 1) {
		/* consume the reference VOP_DECREF passed us */
		vn->vn_refcount--;

		spinlock_release(&vn->vn_countlock);
		lock_release(semfs->semfs_tablelock);
		return EBUSY;
	}

	spinlock_release(&vn->vn_countlock);

	/* remove from the table */
	num = vnodearray_num(semfs->semfs_vnodes);
	for (i=0; i<num; i++) {
		vn2 = vnodearray_get(semfs->semfs_vnodes, i);
		if (vn2 == vn) {
			vnodearray_remove(semfs->semfs_vnodes, i);
			break;
		}
	}

	if (semv->semv_semnum != SEMFS_ROOTDIR) {
		sem = semfs_semarray_get(semfs->semfs_sems, semv->semv_semnum);
		KASSERT(sem->sems_hasvnode);
		sem->sems_hasvnode = false;
		if (sem->sems_linked == false) {
			semfs_semarray_set(semfs->semfs_sems,
					   semv->semv_semnum, NULL);
			semfs_sem_destroy(sem);
		}
	}

	/* done with the table */
	lock_release(semfs->semfs_tablelock);

	/* destroy it */
	semfs_vnode_destroy(semv);
	return 0;
}