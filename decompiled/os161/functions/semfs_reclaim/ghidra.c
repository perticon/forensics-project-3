int semfs_reclaim(vnode *vn)

{
  int iVar1;
  uint uVar2;
  semfs_semarray *psVar3;
  array *a;
  char *sem;
  uint index;
  semfs *psVar4;
  semfs_vnode *semv;
  spinlock *splk;
  
  semv = (semfs_vnode *)vn->vn_data;
  psVar4 = semv->semv_semfs;
  splk = &vn->vn_countlock;
  lock_acquire(psVar4->semfs_tablelock);
  spinlock_acquire(splk);
  if (vn->vn_refcount < 2) {
    spinlock_release(splk);
    a = (array *)psVar4->semfs_vnodes;
    uVar2 = 0;
    do {
      index = uVar2;
      if (a->num <= index) goto LAB_80006b84;
      uVar2 = index + 1;
    } while (vn != (vnode *)a->v[index]);
    array_remove(a,index);
LAB_80006b84:
    uVar2 = semv->semv_semnum;
    if (uVar2 != 0xffffffff) {
      psVar3 = psVar4->semfs_sems;
      iVar1 = uVar2 << 2;
      if ((psVar3->arr).num <= uVar2) {
        badassert("index < a->num","../../include/array.h",100,"array_get");
      }
      sem = *(char **)((int)(psVar3->arr).v + iVar1);
      if (*(bool *)((int)sem + 0xc) == false) {
        sem = "sem->sems_hasvnode";
        badassert("sem->sems_hasvnode","../../fs/semfs/semfs_vnops.c",0x277,"semfs_reclaim");
      }
      *(bool *)((int)sem + 0xc) = false;
      if (*(bool *)((int)sem + 0xd) == false) {
        psVar3 = psVar4->semfs_sems;
        iVar1 = semv->semv_semnum << 2;
        if ((psVar3->arr).num <= semv->semv_semnum) {
          sem = "index < a->num";
          badassert("index < a->num","../../include/array.h",0x6b,"array_set");
        }
        *(undefined4 *)((int)(psVar3->arr).v + iVar1) = 0;
        semfs_sem_destroy((semfs_sem *)sem);
      }
    }
    lock_release(psVar4->semfs_tablelock);
    semfs_vnode_destroy(semv);
    iVar1 = 0;
  }
  else {
    vn->vn_refcount = vn->vn_refcount + -1;
    spinlock_release(splk);
    lock_release(psVar4->semfs_tablelock);
    iVar1 = 0x1b;
  }
  return iVar1;
}