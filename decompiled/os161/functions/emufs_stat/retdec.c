int32_t emufs_stat(int32_t * v2, int32_t * statbuf) {
    int32_t v1 = (int32_t)v2;
    int32_t v2_ = *(int32_t *)(v1 + 16); // 0x80002a48
    bzero((char *)statbuf, 88);
    int32_t v3 = *(int32_t *)(v2_ + 28); // 0x80002a5c
    int32_t result = emu_getsize((int32_t *)*(int32_t *)(v2_ + 24), v3, (int64_t *)statbuf); // 0x80002a64
    if (result != 0) {
        // 0x80002abc
        return result;
    }
    // 0x80002a70
    vnode_check(v2, "gettype");
    int32_t v4 = *(int32_t *)(*(int32_t *)(v1 + 20) + 36); // 0x80002a84
    int32_t result2 = v4; // 0x80002a98
    if (v4 == 0) {
        int32_t v5 = (int32_t)statbuf;
        int32_t * v6 = (int32_t *)(v5 + 8); // 0x80002a9c
        *v6 = *v6 | 420;
        *(int16_t *)(v5 + 12) = 1;
        *(int32_t *)(v5 + 16) = 0;
        result2 = 0;
    }
    // 0x80002abc
    return result2;
}