int emufs_stat(vnode *v,stat *statbuf)

{
  int iVar1;
  void *pvVar2;
  
  pvVar2 = v->vn_data;
  bzero(statbuf,0x58);
  iVar1 = emu_getsize(*(emu_softc **)((int)pvVar2 + 0x18),*(uint32_t *)((int)pvVar2 + 0x1c),
                      &statbuf->st_size);
  if (iVar1 == 0) {
    vnode_check(v,"gettype");
    iVar1 = (*v->vn_ops->vop_gettype)(v,&statbuf->st_mode);
    if (iVar1 == 0) {
      statbuf->st_mode = statbuf->st_mode | 0x1a4;
      statbuf->st_nlink = 1;
      statbuf->st_blocks = 0;
      iVar1 = 0;
    }
  }
  return iVar1;
}