int semfs_sem_insert(semfs *semfs,semfs_sem *sem,uint *ret)

{
  bool bVar1;
  undefined3 extraout_var;
  bool bVar5;
  uint uVar2;
  semfs_sem *psVar3;
  int iVar4;
  uint index;
  semfs_semarray *a;
  
  bVar5 = lock_do_i_hold(semfs->semfs_tablelock);
  if (CONCAT31(extraout_var,bVar5) == 0) {
    badassert("lock_do_i_hold(semfs->semfs_tablelock)","../../fs/semfs/semfs_obj.c",0x61,
              "semfs_sem_insert");
  }
  a = semfs->semfs_sems;
  uVar2 = semfs_semarray_num(a);
  if (uVar2 == 0xffffffff) {
    iVar4 = 0x24;
  }
  else {
    bVar1 = uVar2 != 0;
    index = 0;
    while (bVar1) {
      psVar3 = semfs_semarray_get(a,index);
      if (psVar3 == (semfs_sem *)0x0) {
        semfs_semarray_set(a,index,sem);
        *ret = index;
        return 0;
      }
      bVar1 = index + 1 < uVar2;
      index = index + 1;
    }
    iVar4 = semfs_semarray_add(a,sem,ret);
  }
  return iVar4;
}