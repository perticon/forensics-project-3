semfs_sem_insert(struct semfs *semfs, struct semfs_sem *sem, unsigned *ret)
{
	unsigned i, num;

	KASSERT(lock_do_i_hold(semfs->semfs_tablelock));
	num = semfs_semarray_num(semfs->semfs_sems);
	if (num == SEMFS_ROOTDIR) {
		/* Too many */
		return ENOSPC;
	}
	for (i=0; i<num; i++) {
		if (semfs_semarray_get(semfs->semfs_sems, i) == NULL) {
			semfs_semarray_set(semfs->semfs_sems, i, sem);
			*ret = i;
			return 0;
		}
	}
	return semfs_semarray_add(semfs->semfs_sems, sem, ret);
}