int32_t semfs_sem_insert(int32_t * semfs, int32_t * sem, int32_t * ret) {
    int32_t v1 = (int32_t)semfs;
    if (!lock_do_i_hold((int32_t *)*(int32_t *)(v1 + 8))) {
        // 0x80005e74
        badassert("lock_do_i_hold(semfs->semfs_tablelock)", "../../fs/semfs/semfs_obj.c", 97, "semfs_sem_insert");
    }
    int32_t * v2 = (int32_t *)*(int32_t *)(v1 + 16); // 0x80005e98
    uint32_t v3 = semfs_semarray_num(v2); // 0x80005e98
    if (v3 == -1) {
        // 0x80005f08
        return 36;
    }
    // 0x80005eac
    if (v3 == 0) {
        // 0x80005f08
        return semfs_semarray_add(v2, sem, ret);
    }
    int32_t v4 = 0;
    while (semfs_semarray_get(v2, v4) != NULL) {
        int32_t v5 = v4 + 1; // 0x80005ec0
        if (v5 >= v3) {
            // 0x80005f08
            return semfs_semarray_add(v2, sem, ret);
        }
        v4 = v5;
    }
    // 0x80005ec4
    semfs_semarray_set(v2, v4, sem);
    *ret = v4;
    // 0x80005f08
    return 0;
}