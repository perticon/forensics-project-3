int32_t emu_close(int32_t * sc, int32_t handle) {
    int32_t v1 = (int32_t)sc;
    int32_t * v2 = (int32_t *)(v1 + 12); // 0x80002f74
    int32_t v3 = *v2; // 0x80002f74
    bool v4 = lock_do_i_hold((int32_t *)v3); // 0x80002f7c
    int32_t v5 = v3; // 0x80002f84
    if (!v4) {
        // 0x80002f88
        v5 = *v2;
        lock_acquire((int32_t *)v5);
    }
    int32_t * v6 = (int32_t *)(v1 + 4); // 0x80002fa8
    int32_t v7 = 0; // 0x80002fa0
    int32_t * v8 = (int32_t *)v5; // 0x80002fb4
    lamebus_write_register(v8, *v6, 0, handle);
    lamebus_write_register(v8, *v6, 12, 4);
    int32_t result = emu_waitdone(sc); // 0x80002fd0
    while (v7 < 10 && result == 32) {
        // 0x80002fe8
        kprintf("emu%d: I/O error on close, retrying\n", *(int32_t *)(v1 + 8));
        v7++;
        v8 = (int32_t *)(int32_t)"emu%d: I/O error on close, retrying\n";
        lamebus_write_register(v8, *v6, 0, handle);
        lamebus_write_register(v8, *v6, 12, 4);
        result = emu_waitdone(sc);
    }
    if (!v4) {
        // 0x80003004
        lock_release((int32_t *)*v2);
    }
    // 0x80003014
    return result;
}