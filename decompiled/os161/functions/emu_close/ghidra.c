int emu_close(emu_softc *sc,uint32_t handle)

{
  undefined3 extraout_var;
  bool bVar2;
  int iVar1;
  int iVar3;
  
  bVar2 = lock_do_i_hold(sc->e_lock);
  if (CONCAT31(extraout_var,bVar2) == 0) {
    lock_acquire(sc->e_lock);
  }
  iVar3 = 0;
  while( true ) {
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0,handle);
    lamebus_write_register((lamebus_softc *)sc->e_busdata,sc->e_buspos,0xc,4);
    iVar1 = emu_waitdone(sc);
    if ((iVar1 != 0x20) || (9 < iVar3)) break;
    kprintf("emu%d: I/O error on close, retrying\n",sc->e_unit);
    iVar3 = iVar3 + 1;
  }
  if (CONCAT31(extraout_var,bVar2) == 0) {
    lock_release(sc->e_lock);
  }
  return iVar1;
}