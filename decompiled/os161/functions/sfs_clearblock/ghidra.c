int sfs_clearblock(sfs_fs *sfs,daddr_t block)

{
  int iVar1;
  
  iVar1 = sfs_writeblock(sfs,block,sfs_clearblock::zeros,0x200);
  return iVar1;
}