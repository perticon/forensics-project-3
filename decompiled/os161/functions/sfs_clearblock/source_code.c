sfs_clearblock(struct sfs_fs *sfs, daddr_t block)
{
	/* static -> automatically initialized to zero */
	static char zeros[SFS_BLOCKSIZE];

	return sfs_writeblock(sfs, block, zeros, SFS_BLOCKSIZE);
}