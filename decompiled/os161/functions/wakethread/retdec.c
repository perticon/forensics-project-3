void wakethread(char * junk1, int32_t junk2) {
    for (int32_t i = 0; i < 40; i++) {
        int32_t v1 = 0; // 0x80013034
        int32_t v2 = 4 * v1; // 0x80013014
        P(gatesem);
        int32_t * v3 = (int32_t *)(v2 + (int32_t)&testlocks); // 0x8001301c
        lock_acquire((int32_t *)*v3);
        int32_t v4 = *(int32_t *)(v2 + (int32_t)&testcvs); // 0x80013028
        v1++;
        cv_signal((int32_t *)v4, (int32_t *)*v3);
        lock_release((int32_t *)*v3);
        while (v1 < 250) {
            // 0x8001300c
            v2 = 4 * v1;
            P(gatesem);
            v3 = (int32_t *)(v2 + (int32_t)&testlocks);
            lock_acquire((int32_t *)*v3);
            v4 = *(int32_t *)(v2 + (int32_t)&testcvs);
            v1++;
            cv_signal((int32_t *)v4, (int32_t *)*v3);
            lock_release((int32_t *)*v3);
        }
        // 0x80013058
        kprintf("wakethread: %u\n", i);
    }
    // 0x80013070
    V(exitsem);
}