int32_t nullio(int32_t * dev, int32_t * uio) {
    int32_t v1 = (int32_t)uio;
    if (*(int32_t *)(v1 + 24) == 1) {
        // 0x80017f48
        *(int32_t *)(v1 + 16) = 0;
    }
    // 0x80017f4c
    return 0;
}