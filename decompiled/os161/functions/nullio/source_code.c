nullio(struct device *dev, struct uio *uio)
{
	/*
	 * On write, discard everything without looking at it.
	 * (Notice that you can write to the null device from invalid
	 * buffer pointers and it will still succeed. This behavior is
	 * traditional.)
	 *
	 * On read, do nothing, generating an immediate EOF.
	 */

	(void)dev; // unused

	if (uio->uio_rw == UIO_WRITE) {
		uio->uio_resid = 0;
	}

	return 0;
}