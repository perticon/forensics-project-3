int nullio(device *dev,uio *uio)

{
  if (uio->uio_rw == UIO_WRITE) {
    uio->uio_resid = 0;
  }
  return 0;
}