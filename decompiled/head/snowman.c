
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[5] pad8;
    struct s0* f8;
    signed char[15] pad24;
    struct s0* f18;
    signed char[8175] pad8200;
    struct s0* f2008;
    signed char[7] pad8208;
    void* f2010;
    struct s0* f2018;
};

struct s1 {
    signed char f0;
    signed char f1;
};

struct s0* fun_2450();

struct s0* string_to_integer(signed char dil, struct s1* rsi) {
    if (!dil) {
        fun_2450();
    } else {
        fun_2450();
    }
}

struct s0* g28;

struct s0* safe_read(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

unsigned char line_end = 0;

/* xwrite_stdout.part.0 */
void xwrite_stdout_part_0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

void fun_2480();

struct s0* fun_24d0(int64_t rdi, ...);

/* elseek.part.0 */
void elseek_part_0();

struct s0* quotearg_style(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

int32_t* fun_23c0();

void fun_2630();

struct s0* fun_2640(struct s0* rdi, int64_t rsi, struct s0* rdx);

int32_t copy_fd(int32_t edi, struct s0* rsi);

void diagnose_copy_fd_failure(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

int32_t fun_26e0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

uint32_t head_lines(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rsi2;
    void* rsp6;
    struct s0* rax7;
    struct s0* v8;
    uint32_t eax9;
    struct s0* r14_10;
    struct s0* r13_11;
    struct s0* rbx12;
    struct s0* r12_13;
    void* r15_14;
    struct s0* rax15;
    uint32_t eax16;
    struct s0* rbp17;
    void* rdx18;
    struct s0* v19;
    void* rbx20;
    int32_t r13d21;
    struct s0* v22;
    int64_t rdi23;
    struct s0* rax24;
    struct s0* v25;
    void* rax26;
    void* rdx27;
    void* rax28;
    void* rax29;
    struct s0* rax30;
    struct s0* rdx31;
    struct s0* rbp32;
    struct s0* rax33;
    void* rsp34;
    struct s0* r15_35;
    struct s0* rdx36;
    struct s0* rdi37;
    struct s0* rax38;
    struct s0* r8_39;
    int32_t r12d40;
    void* r14_41;
    void* rax42;
    int64_t rsi43;
    struct s0* rax44;
    int64_t rdi45;
    struct s0* rax46;
    struct s0* rdi47;
    struct s0* rax48;
    void* rdx49;
    int64_t v50;
    int64_t rdi51;
    struct s0* rax52;
    int32_t eax53;
    struct s0* rdi54;
    struct s0* rsi55;
    int64_t rdi56;
    struct s0* rax57;
    struct s0* rax58;
    int32_t* rax59;
    struct s0* rdx60;
    int64_t rdi61;
    struct s0* rax62;
    struct s0* rsp63;
    int64_t rdi64;
    int32_t eax65;
    uint32_t v66;
    int64_t rdi67;
    struct s0* rax68;

    *reinterpret_cast<int32_t*>(&rsi2) = esi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax7 = g28;
    v8 = rax7;
    if (!rdx) {
        addr_3e55_2:
        eax9 = 1;
    } else {
        r14_10 = rdi;
        *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
        rbx12 = rdx;
        r12_13 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp6) + 0x90);
        r15_14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) + 0x8f);
        while (*reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax15 = safe_read(rdi, r12_13, 0x2000, rcx, r8), rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8), rsi2 = rax15, rax15 != 0xffffffffffffffff) {
            if (!rax15) 
                goto addr_3e55_2;
            eax16 = line_end;
            *reinterpret_cast<int32_t*>(&rbp17) = 0;
            *reinterpret_cast<int32_t*>(&rbp17 + 4) = 0;
            do {
                rbp17 = reinterpret_cast<struct s0*>(&rbp17->f1);
                if (*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r15_14) + reinterpret_cast<unsigned char>(rbp17)) != *reinterpret_cast<signed char*>(&eax16)) 
                    continue;
                rbx12 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx12) - 1);
                if (!rbx12) 
                    goto addr_3d5d_9;
            } while (rsi2 != rbp17);
            xwrite_stdout_part_0(r12_13, rsi2, 0x2000, rcx, r8);
            rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8);
        }
        goto addr_3e12_12;
    }
    addr_3d88_13:
    rdx18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx18) {
        return eax9;
    }
    fun_2480();
    v19 = rdi;
    rbx20 = rdx18;
    r13d21 = *reinterpret_cast<int32_t*>(&rsi2);
    v22 = rcx;
    *reinterpret_cast<int32_t*>(&rdi23) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
    rax24 = g28;
    v25 = rax24;
    rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax26) >> 63) >> 51);
    rax28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax26) + reinterpret_cast<uint64_t>(rdx27));
    *reinterpret_cast<uint32_t*>(&rax29) = *reinterpret_cast<uint32_t*>(&rax28) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
    rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax29) - reinterpret_cast<uint64_t>(rdx27));
    *reinterpret_cast<int32_t*>(&rdx31) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
    if (!rax30) 
        goto addr_3edd_17;
    rdx31 = rax30;
    addr_3edd_17:
    rbp32 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx31));
    rax33 = fun_24d0(rdi23, rdi23);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax33) < reinterpret_cast<signed char>(0)) {
        addr_4060_19:
        elseek_part_0();
    } else {
        r15_35 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp34) + 32);
        rdx36 = rdx31;
        *reinterpret_cast<int32_t*>(&rdi37) = r13d21;
        *reinterpret_cast<int32_t*>(&rdi37 + 4) = 0;
        rax38 = safe_read(rdi37, r15_35, rdx36, rcx, r8);
        r8_39 = rax38;
        if (rax38 == 0xffffffffffffffff) {
            addr_40cc_21:
            quotearg_style(4, v19, rdx36, rcx, r8_39);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d40 = reinterpret_cast<signed char>(line_end);
            r14_41 = rbx20;
            if (rbx20 && (r8_39 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 + reinterpret_cast<unsigned char>(r8_39) + 31) != *reinterpret_cast<signed char*>(&r12d40))) {
                r14_41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx20) + 0xffffffffffffffff);
                if (r8_39) 
                    goto addr_3fb5_24;
                goto addr_3f48_26;
            }
            while (1) {
                if (!r8_39) 
                    goto addr_3f48_26;
                addr_3fb5_24:
                if (!rbx20) {
                    r8_39 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_39) - 1);
                    rax42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_41) + 0xffffffffffffffff);
                    if (!r14_41) 
                        goto addr_3fe0_29;
                } else {
                    rdx36 = r8_39;
                    *reinterpret_cast<int32_t*>(&rsi43) = r12d40;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi43) + 4) = 0;
                    rax44 = fun_2640(r15_35, rsi43, rdx36);
                    r8_39 = rax44;
                    if (!rax44) {
                        addr_3f48_26:
                        if (rbp32 == v22) 
                            goto addr_4142_31; else 
                            goto addr_3f53_32;
                    } else {
                        r8_39 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_39) - reinterpret_cast<unsigned char>(r15_35));
                        rax42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_41) + 0xffffffffffffffff);
                        if (!r14_41) 
                            goto addr_3fe0_29;
                    }
                }
                r14_41 = rax42;
                continue;
                addr_3f53_32:
                rbp32 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp32) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi45) = r13d21;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi45) + 4) = 0;
                rax46 = fun_24d0(rdi45, rdi45);
                if (reinterpret_cast<signed char>(rax46) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_19;
                *reinterpret_cast<int32_t*>(&rdx36) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi47) = r13d21;
                *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0;
                rax48 = safe_read(rdi47, r15_35, 0x2000, rcx, r8_39);
                r8_39 = rax48;
                if (rax48 == 0xffffffffffffffff) 
                    goto addr_40cc_21;
                if (!r8_39) 
                    goto addr_4142_31;
                r12d40 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_38:
    rdx49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(g28));
    if (rdx49) {
        fun_2480();
    } else {
        goto v50;
    }
    addr_4142_31:
    goto addr_4071_38;
    addr_3fe0_29:
    if (reinterpret_cast<signed char>(rbp32) <= reinterpret_cast<signed char>(v22)) 
        goto addr_4029_42;
    *reinterpret_cast<int32_t*>(&rdx36) = 0;
    *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi51) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi51) + 4) = 0;
    rax52 = fun_24d0(rdi51);
    if (reinterpret_cast<signed char>(rax52) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_38;
    } else {
        eax53 = copy_fd(r13d21, reinterpret_cast<unsigned char>(rbp32) - reinterpret_cast<unsigned char>(v22));
        r8_39 = r8_39;
        if (eax53) {
            *reinterpret_cast<int32_t*>(&rdi54) = eax53;
            *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
            diagnose_copy_fd_failure(rdi54, v19, 0, rcx, r8_39);
            goto addr_4071_38;
        } else {
            addr_4029_42:
            rsi55 = reinterpret_cast<struct s0*>(&r8_39->f1);
            if (rsi55) {
                xwrite_stdout_part_0(r15_35, rsi55, rdx36, rcx, r8_39);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi56) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi56) + 4) = 0;
    rax57 = fun_24d0(rdi56, rdi56);
    if (reinterpret_cast<signed char>(rax57) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_38;
    addr_3e12_12:
    rax58 = quotearg_style(4, r14_10, 0x2000, rcx, r8);
    fun_2450();
    rax59 = fun_23c0();
    rcx = rax58;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax59;
    fun_2630();
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax9 = 0;
    goto addr_3d88_13;
    addr_3d5d_9:
    *reinterpret_cast<int32_t*>(&rdx60) = 1;
    *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi61) = *reinterpret_cast<int32_t*>(&r13_11);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
    rax62 = fun_24d0(rdi61, rdi61);
    rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8);
    if (reinterpret_cast<signed char>(rax62) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi64) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi64) + 4) = 0, eax65 = fun_26e0(rdi64, rsp63, 1, rcx, r8), rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8), !!eax65) || reinterpret_cast<int1_t>((v66 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx60) = 1, *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi67) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0, rax68 = fun_24d0(rdi67, rdi67), rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8), reinterpret_cast<signed char>(rax68) < reinterpret_cast<signed char>(0)))) {
        rdx60 = r14_10;
        elseek_part_0();
        rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8);
    }
    rsi2 = rbp17;
    rdi = r12_13;
    xwrite_stdout_part_0(rdi, rsi2, rdx60, rcx, r8);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8);
    eax9 = 1;
    goto addr_3d88_13;
}

struct s0* stdout = reinterpret_cast<struct s0*>(0);

struct s0* fun_25f0();

void fun_2410(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

/* xwrite_stdout.part.0 */
void xwrite_stdout_part_0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rcx6;
    int64_t v7;
    int64_t rbx8;
    struct s0* rax9;
    struct s0* rdi10;
    struct s0* rax11;
    int32_t* rax12;
    struct s0* rcx13;
    struct s0* rdi14;
    struct s0* rsi15;
    struct s0* rsp16;
    struct s0* rax17;
    struct s0* v18;
    struct s0* rdx19;
    struct s0* rsp20;
    struct s0* rax21;
    struct s0* v22;
    struct s0* rdx23;
    int64_t v24;
    void* rsp25;
    struct s0* rax26;
    struct s0* v27;
    void* rdx28;
    int64_t v29;
    struct s0* v30;
    void* rbx31;
    int32_t r13d32;
    struct s0* v33;
    int64_t rdi34;
    struct s0* rax35;
    struct s0* v36;
    void* rax37;
    void* rdx38;
    void* rax39;
    void* rax40;
    struct s0* rax41;
    struct s0* rdx42;
    struct s0* rbp43;
    struct s0* rax44;
    void* rsp45;
    struct s0* r15_46;
    struct s0* rdx47;
    struct s0* rdi48;
    struct s0* rax49;
    struct s0* r8_50;
    int32_t r12d51;
    void* r14_52;
    void* rax53;
    int64_t rsi54;
    struct s0* rax55;
    int64_t rdi56;
    struct s0* rax57;
    struct s0* rdi58;
    struct s0* rax59;
    void* rdx60;
    int64_t v61;
    int64_t rdi62;
    struct s0* rax63;
    int32_t eax64;
    struct s0* rdi65;
    struct s0* rsi66;
    int64_t rdi67;
    struct s0* rax68;
    struct s0* r14_69;
    struct s0* r13_70;
    struct s0* rbx71;
    struct s0* r12_72;
    struct s0* r15_73;
    struct s0* rax74;
    uint32_t eax75;
    struct s0* rbp76;
    struct s0* rax77;
    int32_t* rax78;
    struct s0* rdx79;
    int64_t rdi80;
    struct s0* rax81;
    struct s0* rsp82;
    int64_t rdi83;
    int32_t eax84;
    uint32_t v85;
    int64_t rdi86;
    struct s0* rax87;
    struct s0* rax88;
    struct s0* rax89;
    int32_t* rax90;
    struct s0* rdx91;
    struct s0* rax92;

    rcx6 = stdout;
    v7 = rbx8;
    rax9 = fun_25f0();
    if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(rax9)) {
        return;
    }
    rdi10 = stdout;
    fun_2410(rdi10, 1, rsi, rcx6);
    rax11 = quotearg_style(4, "standard output", rsi, rcx6, r8);
    fun_2450();
    rax12 = fun_23c0();
    rcx13 = rax11;
    *reinterpret_cast<int32_t*>(&rdi14) = 1;
    *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi15) = *rax12;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    fun_2630();
    rsp16 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 16);
    rax17 = g28;
    v18 = rax17;
    if (rsi15) 
        goto addr_3b18_5;
    addr_3b88_6:
    addr_3b66_7:
    rdx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
    if (!rdx19) {
        goto v7;
    }
    fun_2480();
    rsp20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp16) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax21 = g28;
    v22 = rax21;
    if (rdx19) 
        goto addr_3be2_11;
    addr_3c78_12:
    addr_3c7d_13:
    rdx23 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v22) - reinterpret_cast<unsigned char>(g28));
    if (!rdx23) {
        goto v24;
    }
    fun_2480();
    rsp25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp20) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax26 = g28;
    v27 = rax26;
    if (rdx23) 
        goto addr_3cf5_17;
    addr_3e55_18:
    addr_3d88_19:
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
    if (!rdx28) {
        goto v29;
    }
    fun_2480();
    v30 = rdi14;
    rbx31 = rdx28;
    r13d32 = *reinterpret_cast<int32_t*>(&rsi15);
    v33 = rcx13;
    *reinterpret_cast<int32_t*>(&rdi34) = r13d32;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
    rax35 = g28;
    v36 = rax35;
    rax37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx13));
    rdx38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax37) >> 63) >> 51);
    rax39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax37) + reinterpret_cast<uint64_t>(rdx38));
    *reinterpret_cast<uint32_t*>(&rax40) = *reinterpret_cast<uint32_t*>(&rax39) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax40) + 4) = 0;
    rax41 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax40) - reinterpret_cast<uint64_t>(rdx38));
    *reinterpret_cast<int32_t*>(&rdx42) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx42 + 4) = 0;
    if (!rax41) 
        goto addr_3edd_23;
    rdx42 = rax41;
    addr_3edd_23:
    rbp43 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx42));
    rax44 = fun_24d0(rdi34, rdi34);
    rsp45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp25) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) {
        addr_4060_25:
        elseek_part_0();
    } else {
        r15_46 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp45) + 32);
        rdx47 = rdx42;
        *reinterpret_cast<int32_t*>(&rdi48) = r13d32;
        *reinterpret_cast<int32_t*>(&rdi48 + 4) = 0;
        rax49 = safe_read(rdi48, r15_46, rdx47, rcx13, r8);
        r8_50 = rax49;
        if (rax49 == 0xffffffffffffffff) {
            addr_40cc_27:
            quotearg_style(4, v30, rdx47, rcx13, r8_50);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d51 = reinterpret_cast<signed char>(line_end);
            r14_52 = rbx31;
            if (rbx31 && (r8_50 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp45) - 8 + 8 + reinterpret_cast<unsigned char>(r8_50) + 31) != *reinterpret_cast<signed char*>(&r12d51))) {
                r14_52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx31) + 0xffffffffffffffff);
                if (r8_50) 
                    goto addr_3fb5_30;
                goto addr_3f48_32;
            }
            while (1) {
                if (!r8_50) 
                    goto addr_3f48_32;
                addr_3fb5_30:
                if (!rbx31) {
                    r8_50 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_50) - 1);
                    rax53 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_52) + 0xffffffffffffffff);
                    if (!r14_52) 
                        goto addr_3fe0_35;
                } else {
                    rdx47 = r8_50;
                    *reinterpret_cast<int32_t*>(&rsi54) = r12d51;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0;
                    rax55 = fun_2640(r15_46, rsi54, rdx47);
                    r8_50 = rax55;
                    if (!rax55) {
                        addr_3f48_32:
                        if (rbp43 == v33) 
                            goto addr_4142_37; else 
                            goto addr_3f53_38;
                    } else {
                        r8_50 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_50) - reinterpret_cast<unsigned char>(r15_46));
                        rax53 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_52) + 0xffffffffffffffff);
                        if (!r14_52) 
                            goto addr_3fe0_35;
                    }
                }
                r14_52 = rax53;
                continue;
                addr_3f53_38:
                rbp43 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp43) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi56) = r13d32;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi56) + 4) = 0;
                rax57 = fun_24d0(rdi56, rdi56);
                if (reinterpret_cast<signed char>(rax57) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_25;
                *reinterpret_cast<int32_t*>(&rdx47) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi58) = r13d32;
                *reinterpret_cast<int32_t*>(&rdi58 + 4) = 0;
                rax59 = safe_read(rdi58, r15_46, 0x2000, rcx13, r8_50);
                r8_50 = rax59;
                if (rax59 == 0xffffffffffffffff) 
                    goto addr_40cc_27;
                if (!r8_50) 
                    goto addr_4142_37;
                r12d51 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_44:
    rdx60 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v36) - reinterpret_cast<unsigned char>(g28));
    if (rdx60) {
        fun_2480();
    } else {
        goto v61;
    }
    addr_4142_37:
    goto addr_4071_44;
    addr_3fe0_35:
    if (reinterpret_cast<signed char>(rbp43) <= reinterpret_cast<signed char>(v33)) 
        goto addr_4029_48;
    *reinterpret_cast<int32_t*>(&rdx47) = 0;
    *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi62) = r13d32;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
    rax63 = fun_24d0(rdi62);
    if (reinterpret_cast<signed char>(rax63) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_44;
    } else {
        eax64 = copy_fd(r13d32, reinterpret_cast<unsigned char>(rbp43) - reinterpret_cast<unsigned char>(v33));
        r8_50 = r8_50;
        if (eax64) {
            *reinterpret_cast<int32_t*>(&rdi65) = eax64;
            *reinterpret_cast<int32_t*>(&rdi65 + 4) = 0;
            diagnose_copy_fd_failure(rdi65, v30, 0, rcx13, r8_50);
            goto addr_4071_44;
        } else {
            addr_4029_48:
            rsi66 = reinterpret_cast<struct s0*>(&r8_50->f1);
            if (rsi66) {
                xwrite_stdout_part_0(r15_46, rsi66, rdx47, rcx13, r8_50);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi67) = r13d32;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
    rax68 = fun_24d0(rdi67, rdi67);
    if (reinterpret_cast<signed char>(rax68) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_44;
    addr_3cf5_17:
    r14_69 = rdi14;
    *reinterpret_cast<int32_t*>(&r13_70) = *reinterpret_cast<int32_t*>(&rsi15);
    rbx71 = rdx23;
    r12_72 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp25) + 0x90);
    r15_73 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp25) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi14) = *reinterpret_cast<int32_t*>(&r13_70), *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0, rax74 = safe_read(rdi14, r12_72, 0x2000, rcx13, r8), rsp25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp25) - 8 + 8), rsi15 = rax74, rax74 != 0xffffffffffffffff) {
        if (!rax74) 
            goto addr_3e55_18;
        eax75 = line_end;
        *reinterpret_cast<int32_t*>(&rbp76) = 0;
        *reinterpret_cast<int32_t*>(&rbp76 + 4) = 0;
        do {
            rbp76 = reinterpret_cast<struct s0*>(&rbp76->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_73) + reinterpret_cast<unsigned char>(rbp76)) != *reinterpret_cast<signed char*>(&eax75)) 
                continue;
            rbx71 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx71) - 1);
            if (!rbx71) 
                goto addr_3d5d_62;
        } while (rsi15 != rbp76);
        xwrite_stdout_part_0(r12_72, rsi15, 0x2000, rcx13, r8);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp25) - 8 + 8);
    }
    rax77 = quotearg_style(4, r14_69, 0x2000, rcx13, r8);
    fun_2450();
    rax78 = fun_23c0();
    rcx13 = rax77;
    *reinterpret_cast<int32_t*>(&rdi14) = 0;
    *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi15) = *rax78;
    fun_2630();
    rsp25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp25) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_19;
    addr_3d5d_62:
    *reinterpret_cast<int32_t*>(&rdx79) = 1;
    *reinterpret_cast<int32_t*>(&rdx79 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi80) = *reinterpret_cast<int32_t*>(&r13_70);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi80) + 4) = 0;
    rax81 = fun_24d0(rdi80, rdi80);
    rsp82 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp25) - 8 + 8);
    if (reinterpret_cast<signed char>(rax81) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi83) = *reinterpret_cast<int32_t*>(&r13_70), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi83) + 4) = 0, eax84 = fun_26e0(rdi83, rsp82, 1, rcx13, r8), rsp82 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp82) - 8 + 8), !!eax84) || reinterpret_cast<int1_t>((v85 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx79) = 1, *reinterpret_cast<int32_t*>(&rdx79 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi86) = *reinterpret_cast<int32_t*>(&r13_70), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi86) + 4) = 0, rax87 = fun_24d0(rdi86, rdi86), rsp82 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp82) - 8 + 8), reinterpret_cast<signed char>(rax87) < reinterpret_cast<signed char>(0)))) {
        rdx79 = r14_69;
        elseek_part_0();
        rsp82 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp82) - 8 + 8);
    }
    rsi15 = rbp76;
    rdi14 = r12_72;
    xwrite_stdout_part_0(rdi14, rsi15, rdx79, rcx13, r8);
    rsp25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp82) - 8 + 8);
    goto addr_3d88_19;
    addr_3be2_11:
    r14_69 = rdi14;
    *reinterpret_cast<int32_t*>(&r13_70) = *reinterpret_cast<int32_t*>(&rsi15);
    rbp76 = rdx19;
    *reinterpret_cast<int32_t*>(&r15_73) = 0x2000;
    *reinterpret_cast<int32_t*>(&r15_73 + 4) = 0;
    r12_72 = rsp20;
    do {
        rsi15 = r12_72;
        *reinterpret_cast<int32_t*>(&rdi14) = *reinterpret_cast<int32_t*>(&r13_70);
        *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r15_73) > reinterpret_cast<unsigned char>(rbp76)) {
            r15_73 = rbp76;
        }
        rax88 = safe_read(rdi14, rsi15, r15_73, rcx13, r8);
        rsp20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp20) - 8 + 8);
        if (reinterpret_cast<int1_t>(rax88 == 0xffffffffffffffff)) 
            break;
    } while (rax88 && (rsi15 = rax88, rdi14 = r12_72, xwrite_stdout_part_0(rdi14, rsi15, r15_73, rcx13, r8), rsp20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp20) - 8 + 8), rbp76 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp76) - reinterpret_cast<unsigned char>(rax88)), !!rbp76));
    goto addr_3c78_12;
    rax89 = quotearg_style(4, r14_69, r15_73, rcx13, r8);
    fun_2450();
    rax90 = fun_23c0();
    rcx13 = rax89;
    *reinterpret_cast<int32_t*>(&rdi14) = 0;
    *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi15) = *rax90;
    fun_2630();
    rsp20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3c7d_13;
    addr_3b18_5:
    rbx71 = rsi15;
    r12_72 = rsp16;
    do {
        *reinterpret_cast<int32_t*>(&rdx91) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx91 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi14) = 1;
        *reinterpret_cast<int32_t*>(&rdi14 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx71) <= reinterpret_cast<unsigned char>(0x2000)) {
            rdx91 = rbx71;
        }
        rax92 = safe_read(1, r12_72, rdx91, rcx13, r8);
        rsp16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp16) - 8 + 8);
        rsi15 = rax92;
        if (reinterpret_cast<int1_t>(rax92 == 0xffffffffffffffff)) 
            break;
        rbx71 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx71) - reinterpret_cast<unsigned char>(rax92));
        if (rax92) 
            continue;
        if (rbx71) 
            goto addr_3b90_78;
        rdi14 = r12_72;
        xwrite_stdout_part_0(rdi14, rsi15, rdx91, rcx13, r8);
        rsp16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp16) - 8 + 8);
    } while (rbx71);
    goto addr_3b88_6;
    goto addr_3b66_7;
    addr_3b90_78:
    goto addr_3b66_7;
}

struct s0* offtostr();

int64_t quotearg_n_style_colon();

/* elseek.part.0 */
void elseek_part_0() {
    struct s0* rax1;
    struct s0* rax2;
    int32_t esi3;
    int32_t* rax4;
    struct s0* r8_5;
    struct s0* rsi6;
    void* rax7;
    struct s0* rcx8;
    struct s0* v9;
    struct s0* rax10;
    struct s0* rdi11;
    struct s0* rax12;
    int32_t* rax13;
    struct s0* rcx14;
    struct s0* rdi15;
    struct s0* rsi16;
    struct s0* rsp17;
    struct s0* rax18;
    struct s0* v19;
    struct s0* rdx20;
    struct s0* rsp21;
    struct s0* rax22;
    struct s0* v23;
    struct s0* rdx24;
    int64_t v25;
    void* rsp26;
    struct s0* rax27;
    struct s0* v28;
    void* rdx29;
    int64_t v30;
    struct s0* v31;
    void* rbx32;
    int32_t r13d33;
    struct s0* v34;
    int64_t rdi35;
    struct s0* rax36;
    struct s0* v37;
    void* rax38;
    void* rdx39;
    void* rax40;
    void* rax41;
    struct s0* rax42;
    struct s0* rdx43;
    struct s0* rbp44;
    struct s0* rax45;
    void* rsp46;
    struct s0* r15_47;
    struct s0* rdx48;
    struct s0* rdi49;
    struct s0* rax50;
    struct s0* r8_51;
    int32_t r12d52;
    void* r14_53;
    void* rax54;
    int64_t rsi55;
    struct s0* rax56;
    int64_t rdi57;
    struct s0* rax58;
    struct s0* rdi59;
    struct s0* rax60;
    void* rdx61;
    int64_t v62;
    int64_t rdi63;
    struct s0* rax64;
    int32_t eax65;
    struct s0* rdi66;
    struct s0* rsi67;
    int64_t rdi68;
    struct s0* rax69;
    struct s0* r14_70;
    struct s0* r13_71;
    struct s0* rbx72;
    struct s0* r12_73;
    struct s0* r15_74;
    struct s0* rax75;
    uint32_t eax76;
    struct s0* rbp77;
    struct s0* rax78;
    int32_t* rax79;
    struct s0* rdx80;
    int64_t rdi81;
    struct s0* rax82;
    struct s0* rsp83;
    int64_t rdi84;
    int32_t eax85;
    uint32_t v86;
    int64_t rdi87;
    struct s0* rax88;
    struct s0* rax89;
    struct s0* rax90;
    int32_t* rax91;
    struct s0* rdx92;
    struct s0* rax93;
    int64_t v94;

    rax1 = g28;
    rax2 = offtostr();
    quotearg_n_style_colon();
    if (esi3) {
    }
    fun_2450();
    rax4 = fun_23c0();
    r8_5 = rax2;
    *reinterpret_cast<int32_t*>(&rsi6) = *rax4;
    *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
    fun_2630();
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (!rax7) {
        return;
    }
    fun_2480();
    rcx8 = stdout;
    v9 = rax2;
    rax10 = fun_25f0();
    if (reinterpret_cast<unsigned char>(rsi6) <= reinterpret_cast<unsigned char>(rax10)) 
        goto addr_3a81_7;
    rdi11 = stdout;
    fun_2410(rdi11, 1, rsi6, rcx8);
    rax12 = quotearg_style(4, "standard output", rsi6, rcx8, r8_5);
    fun_2450();
    rax13 = fun_23c0();
    rcx14 = rax12;
    *reinterpret_cast<int32_t*>(&rdi15) = 1;
    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi16) = *rax13;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    fun_2630();
    rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 32 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 16);
    rax18 = g28;
    v19 = rax18;
    if (rsi16) 
        goto addr_3b18_10;
    addr_3b88_11:
    addr_3b66_12:
    rdx20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        goto v9;
    }
    fun_2480();
    rsp21 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax22 = g28;
    v23 = rax22;
    if (rdx20) 
        goto addr_3be2_16;
    addr_3c78_17:
    addr_3c7d_18:
    rdx24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v23) - reinterpret_cast<unsigned char>(g28));
    if (!rdx24) {
        goto v25;
    }
    fun_2480();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp21) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax27 = g28;
    v28 = rax27;
    if (rdx24) 
        goto addr_3cf5_22;
    addr_3e55_23:
    addr_3d88_24:
    rdx29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (!rdx29) {
        goto v30;
    }
    fun_2480();
    v31 = rdi15;
    rbx32 = rdx29;
    r13d33 = *reinterpret_cast<int32_t*>(&rsi16);
    v34 = rcx14;
    *reinterpret_cast<int32_t*>(&rdi35) = r13d33;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi35) + 4) = 0;
    rax36 = g28;
    v37 = rax36;
    rax38 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_5) - reinterpret_cast<unsigned char>(rcx14));
    rdx39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax38) >> 63) >> 51);
    rax40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax38) + reinterpret_cast<uint64_t>(rdx39));
    *reinterpret_cast<uint32_t*>(&rax41) = *reinterpret_cast<uint32_t*>(&rax40) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax41) + 4) = 0;
    rax42 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax41) - reinterpret_cast<uint64_t>(rdx39));
    *reinterpret_cast<int32_t*>(&rdx43) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx43 + 4) = 0;
    if (!rax42) 
        goto addr_3edd_28;
    rdx43 = rax42;
    addr_3edd_28:
    rbp44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_5) - reinterpret_cast<unsigned char>(rdx43));
    rax45 = fun_24d0(rdi35, rdi35);
    rsp46 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp26) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax45) < reinterpret_cast<signed char>(0)) {
        addr_4060_30:
        elseek_part_0();
    } else {
        r15_47 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp46) + 32);
        rdx48 = rdx43;
        *reinterpret_cast<int32_t*>(&rdi49) = r13d33;
        *reinterpret_cast<int32_t*>(&rdi49 + 4) = 0;
        rax50 = safe_read(rdi49, r15_47, rdx48, rcx14, r8_5);
        r8_51 = rax50;
        if (rax50 == 0xffffffffffffffff) {
            addr_40cc_32:
            quotearg_style(4, v31, rdx48, rcx14, r8_51);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d52 = reinterpret_cast<signed char>(line_end);
            r14_53 = rbx32;
            if (rbx32 && (r8_51 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp46) - 8 + 8 + reinterpret_cast<unsigned char>(r8_51) + 31) != *reinterpret_cast<signed char*>(&r12d52))) {
                r14_53 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx32) + 0xffffffffffffffff);
                if (r8_51) 
                    goto addr_3fb5_35;
                goto addr_3f48_37;
            }
            while (1) {
                if (!r8_51) 
                    goto addr_3f48_37;
                addr_3fb5_35:
                if (!rbx32) {
                    r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - 1);
                    rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                    if (!r14_53) 
                        goto addr_3fe0_40;
                } else {
                    rdx48 = r8_51;
                    *reinterpret_cast<int32_t*>(&rsi55) = r12d52;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi55) + 4) = 0;
                    rax56 = fun_2640(r15_47, rsi55, rdx48);
                    r8_51 = rax56;
                    if (!rax56) {
                        addr_3f48_37:
                        if (rbp44 == v34) 
                            goto addr_4142_42; else 
                            goto addr_3f53_43;
                    } else {
                        r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - reinterpret_cast<unsigned char>(r15_47));
                        rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                        if (!r14_53) 
                            goto addr_3fe0_40;
                    }
                }
                r14_53 = rax54;
                continue;
                addr_3f53_43:
                rbp44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp44) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi57) = r13d33;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
                rax58 = fun_24d0(rdi57, rdi57);
                if (reinterpret_cast<signed char>(rax58) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_30;
                *reinterpret_cast<int32_t*>(&rdx48) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx48 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi59) = r13d33;
                *reinterpret_cast<int32_t*>(&rdi59 + 4) = 0;
                rax60 = safe_read(rdi59, r15_47, 0x2000, rcx14, r8_51);
                r8_51 = rax60;
                if (rax60 == 0xffffffffffffffff) 
                    goto addr_40cc_32;
                if (!r8_51) 
                    goto addr_4142_42;
                r12d52 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_49:
    rdx61 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v37) - reinterpret_cast<unsigned char>(g28));
    if (rdx61) {
        fun_2480();
    } else {
        goto v62;
    }
    addr_4142_42:
    goto addr_4071_49;
    addr_3fe0_40:
    if (reinterpret_cast<signed char>(rbp44) <= reinterpret_cast<signed char>(v34)) 
        goto addr_4029_53;
    *reinterpret_cast<int32_t*>(&rdx48) = 0;
    *reinterpret_cast<int32_t*>(&rdx48 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi63) = r13d33;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
    rax64 = fun_24d0(rdi63);
    if (reinterpret_cast<signed char>(rax64) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_49;
    } else {
        eax65 = copy_fd(r13d33, reinterpret_cast<unsigned char>(rbp44) - reinterpret_cast<unsigned char>(v34));
        r8_51 = r8_51;
        if (eax65) {
            *reinterpret_cast<int32_t*>(&rdi66) = eax65;
            *reinterpret_cast<int32_t*>(&rdi66 + 4) = 0;
            diagnose_copy_fd_failure(rdi66, v31, 0, rcx14, r8_51);
            goto addr_4071_49;
        } else {
            addr_4029_53:
            rsi67 = reinterpret_cast<struct s0*>(&r8_51->f1);
            if (rsi67) {
                xwrite_stdout_part_0(r15_47, rsi67, rdx48, rcx14, r8_51);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi68) = r13d33;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi68) + 4) = 0;
    rax69 = fun_24d0(rdi68, rdi68);
    if (reinterpret_cast<signed char>(rax69) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_49;
    addr_3cf5_22:
    r14_70 = rdi15;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi16);
    rbx72 = rdx24;
    r12_73 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp26) + 0x90);
    r15_74 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp26) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi15) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0, rax75 = safe_read(rdi15, r12_73, 0x2000, rcx14, r8_5), rsp26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp26) - 8 + 8), rsi16 = rax75, rax75 != 0xffffffffffffffff) {
        if (!rax75) 
            goto addr_3e55_23;
        eax76 = line_end;
        *reinterpret_cast<int32_t*>(&rbp77) = 0;
        *reinterpret_cast<int32_t*>(&rbp77 + 4) = 0;
        do {
            rbp77 = reinterpret_cast<struct s0*>(&rbp77->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_74) + reinterpret_cast<unsigned char>(rbp77)) != *reinterpret_cast<signed char*>(&eax76)) 
                continue;
            rbx72 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx72) - 1);
            if (!rbx72) 
                goto addr_3d5d_67;
        } while (rsi16 != rbp77);
        xwrite_stdout_part_0(r12_73, rsi16, 0x2000, rcx14, r8_5);
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp26) - 8 + 8);
    }
    rax78 = quotearg_style(4, r14_70, 0x2000, rcx14, r8_5);
    fun_2450();
    rax79 = fun_23c0();
    rcx14 = rax78;
    *reinterpret_cast<int32_t*>(&rdi15) = 0;
    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi16) = *rax79;
    fun_2630();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp26) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_24;
    addr_3d5d_67:
    *reinterpret_cast<int32_t*>(&rdx80) = 1;
    *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi81) = *reinterpret_cast<int32_t*>(&r13_71);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi81) + 4) = 0;
    rax82 = fun_24d0(rdi81, rdi81);
    rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp26) - 8 + 8);
    if (reinterpret_cast<signed char>(rax82) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi84) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi84) + 4) = 0, eax85 = fun_26e0(rdi84, rsp83, 1, rcx14, r8_5), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), !!eax85) || reinterpret_cast<int1_t>((v86 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx80) = 1, *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi87) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi87) + 4) = 0, rax88 = fun_24d0(rdi87, rdi87), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), reinterpret_cast<signed char>(rax88) < reinterpret_cast<signed char>(0)))) {
        rdx80 = r14_70;
        elseek_part_0();
        rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    }
    rsi16 = rbp77;
    rdi15 = r12_73;
    xwrite_stdout_part_0(rdi15, rsi16, rdx80, rcx14, r8_5);
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    goto addr_3d88_24;
    addr_3be2_16:
    r14_70 = rdi15;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi16);
    rbp77 = rdx20;
    *reinterpret_cast<int32_t*>(&r15_74) = 0x2000;
    *reinterpret_cast<int32_t*>(&r15_74 + 4) = 0;
    r12_73 = rsp21;
    do {
        rsi16 = r12_73;
        *reinterpret_cast<int32_t*>(&rdi15) = *reinterpret_cast<int32_t*>(&r13_71);
        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r15_74) > reinterpret_cast<unsigned char>(rbp77)) {
            r15_74 = rbp77;
        }
        rax89 = safe_read(rdi15, rsi16, r15_74, rcx14, r8_5);
        rsp21 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp21) - 8 + 8);
        if (reinterpret_cast<int1_t>(rax89 == 0xffffffffffffffff)) 
            break;
    } while (rax89 && (rsi16 = rax89, rdi15 = r12_73, xwrite_stdout_part_0(rdi15, rsi16, r15_74, rcx14, r8_5), rsp21 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp21) - 8 + 8), rbp77 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp77) - reinterpret_cast<unsigned char>(rax89)), !!rbp77));
    goto addr_3c78_17;
    rax90 = quotearg_style(4, r14_70, r15_74, rcx14, r8_5);
    fun_2450();
    rax91 = fun_23c0();
    rcx14 = rax90;
    *reinterpret_cast<int32_t*>(&rdi15) = 0;
    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi16) = *rax91;
    fun_2630();
    rsp21 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp21) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3c7d_18;
    addr_3b18_10:
    rbx72 = rsi16;
    r12_73 = rsp17;
    do {
        *reinterpret_cast<int32_t*>(&rdx92) = 0x2000;
        *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi15) = 1;
        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx72) <= reinterpret_cast<unsigned char>(0x2000)) {
            rdx92 = rbx72;
        }
        rax93 = safe_read(1, r12_73, rdx92, rcx14, r8_5);
        rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8);
        rsi16 = rax93;
        if (reinterpret_cast<int1_t>(rax93 == 0xffffffffffffffff)) 
            break;
        rbx72 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx72) - reinterpret_cast<unsigned char>(rax93));
        if (rax93) 
            continue;
        if (rbx72) 
            goto addr_3b90_83;
        rdi15 = r12_73;
        xwrite_stdout_part_0(rdi15, rsi16, rdx92, rcx14, r8_5);
        rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8);
    } while (rbx72);
    goto addr_3b88_11;
    goto addr_3b66_12;
    addr_3b90_83:
    goto addr_3b66_12;
    addr_3a81_7:
    goto v94;
}

int32_t copy_fd(int32_t edi, struct s0* rsi) {
    struct s0* rdi1;
    struct s0* rsp3;
    struct s0* rax4;
    struct s0* v5;
    int32_t eax6;
    struct s0* rbp7;
    struct s0* rbx8;
    struct s0* r12_9;
    struct s0* rdx10;
    struct s0* rcx11;
    struct s0* r8_12;
    struct s0* rax13;
    struct s0* rcx14;
    struct s0* r8_15;
    struct s0* rdx16;
    struct s0* rsp17;
    struct s0* rax18;
    struct s0* v19;
    struct s0* rdx20;
    int64_t v21;
    void* rsp22;
    struct s0* rax23;
    struct s0* v24;
    void* rdx25;
    int64_t v26;
    struct s0* v27;
    void* rbx28;
    int32_t r13d29;
    struct s0* v30;
    struct s0* rcx31;
    int64_t rdi32;
    struct s0* rax33;
    struct s0* v34;
    uint64_t rax35;
    int64_t r8_36;
    uint64_t rdx37;
    uint64_t rax38;
    void* rax39;
    struct s0* rax40;
    struct s0* rdx41;
    struct s0* rbp42;
    void* r8_43;
    struct s0* rax44;
    void* rsp45;
    struct s0* r15_46;
    struct s0* rdx47;
    struct s0* rdi48;
    struct s0* r8_49;
    struct s0* rax50;
    struct s0* r8_51;
    int32_t r12d52;
    void* r14_53;
    void* rax54;
    int64_t rsi55;
    struct s0* rax56;
    int64_t rdi57;
    struct s0* rax58;
    struct s0* rdi59;
    struct s0* rax60;
    void* rdx61;
    int64_t v62;
    int64_t rdi63;
    struct s0* rax64;
    int32_t eax65;
    struct s0* rdi66;
    struct s0* rsi67;
    int64_t rdi68;
    struct s0* rax69;
    struct s0* r14_70;
    struct s0* r13_71;
    struct s0* r15_72;
    struct s0* r8_73;
    struct s0* rax74;
    uint32_t eax75;
    struct s0* r8_76;
    struct s0* r8_77;
    struct s0* rax78;
    int32_t* rax79;
    struct s0* rdx80;
    int64_t rdi81;
    struct s0* rax82;
    struct s0* rsp83;
    int64_t rdi84;
    struct s0* r8_85;
    int32_t eax86;
    uint32_t v87;
    int64_t rdi88;
    struct s0* rax89;
    struct s0* r8_90;
    struct s0* rcx91;
    struct s0* r8_92;
    struct s0* rax93;
    struct s0* rcx94;
    struct s0* r8_95;
    struct s0* rcx96;
    struct s0* r8_97;
    struct s0* rax98;
    int32_t* rax99;

    *reinterpret_cast<int32_t*>(&rdi1) = edi;
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x1000 - 0x1000 - 16);
    rax4 = g28;
    v5 = rax4;
    if (!rsi) {
        addr_3b88_2:
        eax6 = 0;
    } else {
        *reinterpret_cast<int32_t*>(&rbp7) = *reinterpret_cast<int32_t*>(&rdi1);
        rbx8 = rsi;
        r12_9 = rsp3;
        do {
            *reinterpret_cast<int32_t*>(&rdx10) = 0x2000;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&rbp7);
            *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
            if (reinterpret_cast<unsigned char>(rbx8) <= reinterpret_cast<unsigned char>(0x2000)) {
                rdx10 = rbx8;
            }
            rax13 = safe_read(rdi1, r12_9, rdx10, rcx11, r8_12);
            rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8);
            rsi = rax13;
            if (reinterpret_cast<int1_t>(rax13 == 0xffffffffffffffff)) 
                goto addr_3b61_7;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx8) - reinterpret_cast<unsigned char>(rax13));
            if (rax13) 
                continue;
            if (rbx8) 
                goto addr_3b90_10;
            rdi1 = r12_9;
            xwrite_stdout_part_0(rdi1, rsi, rdx10, rcx14, r8_15);
            rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8);
        } while (rbx8);
        goto addr_3b88_2;
    }
    addr_3b66_12:
    rdx16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (!rdx16) {
        return eax6;
    }
    fun_2480();
    rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax18 = g28;
    v19 = rax18;
    if (rdx16) 
        goto addr_3be2_16;
    addr_3c78_17:
    addr_3c7d_18:
    rdx20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        goto v21;
    }
    fun_2480();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax23 = g28;
    v24 = rax23;
    if (rdx20) 
        goto addr_3cf5_22;
    addr_3e55_23:
    addr_3d88_24:
    rdx25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        goto v26;
    }
    fun_2480();
    v27 = rdi1;
    rbx28 = rdx25;
    r13d29 = *reinterpret_cast<int32_t*>(&rsi);
    v30 = rcx31;
    *reinterpret_cast<int32_t*>(&rdi32) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
    rax33 = g28;
    v34 = rax33;
    rax35 = reinterpret_cast<uint64_t>(r8_36 - reinterpret_cast<unsigned char>(rcx31));
    rdx37 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax35) >> 63) >> 51;
    rax38 = rax35 + rdx37;
    *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
    rax40 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax39) - rdx37);
    *reinterpret_cast<int32_t*>(&rdx41) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
    if (!rax40) 
        goto addr_3edd_28;
    rdx41 = rax40;
    addr_3edd_28:
    rbp42 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(r8_43) - reinterpret_cast<unsigned char>(rdx41));
    rax44 = fun_24d0(rdi32, rdi32);
    rsp45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) {
        addr_4060_30:
        elseek_part_0();
    } else {
        r15_46 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp45) + 32);
        rdx47 = rdx41;
        *reinterpret_cast<int32_t*>(&rdi48) = r13d29;
        *reinterpret_cast<int32_t*>(&rdi48 + 4) = 0;
        rax50 = safe_read(rdi48, r15_46, rdx47, rcx31, r8_49);
        r8_51 = rax50;
        if (rax50 == 0xffffffffffffffff) {
            addr_40cc_32:
            quotearg_style(4, v27, rdx47, rcx31, r8_51);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d52 = reinterpret_cast<signed char>(line_end);
            r14_53 = rbx28;
            if (rbx28 && (r8_51 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp45) - 8 + 8 + reinterpret_cast<unsigned char>(r8_51) + 31) != *reinterpret_cast<signed char*>(&r12d52))) {
                r14_53 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx28) + 0xffffffffffffffff);
                if (r8_51) 
                    goto addr_3fb5_35;
                goto addr_3f48_37;
            }
            while (1) {
                if (!r8_51) 
                    goto addr_3f48_37;
                addr_3fb5_35:
                if (!rbx28) {
                    r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - 1);
                    rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                    if (!r14_53) 
                        goto addr_3fe0_40;
                } else {
                    rdx47 = r8_51;
                    *reinterpret_cast<int32_t*>(&rsi55) = r12d52;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi55) + 4) = 0;
                    rax56 = fun_2640(r15_46, rsi55, rdx47);
                    r8_51 = rax56;
                    if (!rax56) {
                        addr_3f48_37:
                        if (rbp42 == v30) 
                            goto addr_4142_42; else 
                            goto addr_3f53_43;
                    } else {
                        r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - reinterpret_cast<unsigned char>(r15_46));
                        rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                        if (!r14_53) 
                            goto addr_3fe0_40;
                    }
                }
                r14_53 = rax54;
                continue;
                addr_3f53_43:
                rbp42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp42) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi57) = r13d29;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
                rax58 = fun_24d0(rdi57, rdi57);
                if (reinterpret_cast<signed char>(rax58) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_30;
                *reinterpret_cast<int32_t*>(&rdx47) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi59) = r13d29;
                *reinterpret_cast<int32_t*>(&rdi59 + 4) = 0;
                rax60 = safe_read(rdi59, r15_46, 0x2000, rcx31, r8_51);
                r8_51 = rax60;
                if (rax60 == 0xffffffffffffffff) 
                    goto addr_40cc_32;
                if (!r8_51) 
                    goto addr_4142_42;
                r12d52 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_49:
    rdx61 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
    if (rdx61) {
        fun_2480();
    } else {
        goto v62;
    }
    addr_4142_42:
    goto addr_4071_49;
    addr_3fe0_40:
    if (reinterpret_cast<signed char>(rbp42) <= reinterpret_cast<signed char>(v30)) 
        goto addr_4029_53;
    *reinterpret_cast<int32_t*>(&rdx47) = 0;
    *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi63) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
    rax64 = fun_24d0(rdi63);
    if (reinterpret_cast<signed char>(rax64) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_49;
    } else {
        eax65 = copy_fd(r13d29, reinterpret_cast<unsigned char>(rbp42) - reinterpret_cast<unsigned char>(v30));
        r8_51 = r8_51;
        if (eax65) {
            *reinterpret_cast<int32_t*>(&rdi66) = eax65;
            *reinterpret_cast<int32_t*>(&rdi66 + 4) = 0;
            diagnose_copy_fd_failure(rdi66, v27, 0, rcx31, r8_51);
            goto addr_4071_49;
        } else {
            addr_4029_53:
            rsi67 = reinterpret_cast<struct s0*>(&r8_51->f1);
            if (rsi67) {
                xwrite_stdout_part_0(r15_46, rsi67, rdx47, rcx31, r8_51);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi68) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi68) + 4) = 0;
    rax69 = fun_24d0(rdi68, rdi68);
    if (reinterpret_cast<signed char>(rax69) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_49;
    addr_3cf5_22:
    r14_70 = rdi1;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi);
    rbx8 = rdx20;
    r12_9 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) + 0x90);
    r15_72 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0, rax74 = safe_read(rdi1, r12_9, 0x2000, rcx31, r8_73), rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8), rsi = rax74, rax74 != 0xffffffffffffffff) {
        if (!rax74) 
            goto addr_3e55_23;
        eax75 = line_end;
        *reinterpret_cast<int32_t*>(&rbp7) = 0;
        *reinterpret_cast<int32_t*>(&rbp7 + 4) = 0;
        do {
            rbp7 = reinterpret_cast<struct s0*>(&rbp7->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_72) + reinterpret_cast<unsigned char>(rbp7)) != *reinterpret_cast<signed char*>(&eax75)) 
                continue;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx8) - 1);
            if (!rbx8) 
                goto addr_3d5d_67;
        } while (rsi != rbp7);
        xwrite_stdout_part_0(r12_9, rsi, 0x2000, rcx31, r8_76);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
    }
    rax78 = quotearg_style(4, r14_70, 0x2000, rcx31, r8_77);
    fun_2450();
    rax79 = fun_23c0();
    rcx31 = rax78;
    *reinterpret_cast<int32_t*>(&rdi1) = 0;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi) = *rax79;
    fun_2630();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_24;
    addr_3d5d_67:
    *reinterpret_cast<int32_t*>(&rdx80) = 1;
    *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi81) = *reinterpret_cast<int32_t*>(&r13_71);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi81) + 4) = 0;
    rax82 = fun_24d0(rdi81, rdi81);
    rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
    if (reinterpret_cast<signed char>(rax82) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi84) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi84) + 4) = 0, eax86 = fun_26e0(rdi84, rsp83, 1, rcx31, r8_85), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), !!eax86) || reinterpret_cast<int1_t>((v87 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx80) = 1, *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi88) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi88) + 4) = 0, rax89 = fun_24d0(rdi88, rdi88), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), reinterpret_cast<signed char>(rax89) < reinterpret_cast<signed char>(0)))) {
        rdx80 = r14_70;
        elseek_part_0();
        rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    }
    rsi = rbp7;
    rdi1 = r12_9;
    xwrite_stdout_part_0(rdi1, rsi, rdx80, rcx31, r8_90);
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    goto addr_3d88_24;
    addr_3be2_16:
    r14_70 = rdi1;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi);
    rbp7 = rdx16;
    *reinterpret_cast<int32_t*>(&r15_72) = 0x2000;
    *reinterpret_cast<int32_t*>(&r15_72 + 4) = 0;
    r12_9 = rsp17;
    do {
        rsi = r12_9;
        *reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&r13_71);
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r15_72) > reinterpret_cast<unsigned char>(rbp7)) {
            r15_72 = rbp7;
        }
        rax93 = safe_read(rdi1, rsi, r15_72, rcx91, r8_92);
        rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8);
        if (reinterpret_cast<int1_t>(rax93 == 0xffffffffffffffff)) 
            break;
    } while (rax93 && (rsi = rax93, rdi1 = r12_9, xwrite_stdout_part_0(rdi1, rsi, r15_72, rcx94, r8_95), rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8), rbp7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp7) - reinterpret_cast<unsigned char>(rax93)), !!rbp7));
    goto addr_3c78_17;
    rax98 = quotearg_style(4, r14_70, r15_72, rcx96, r8_97);
    fun_2450();
    rax99 = fun_23c0();
    rcx31 = rax98;
    *reinterpret_cast<int32_t*>(&rdi1) = 0;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi) = *rax99;
    fun_2630();
    rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3c7d_18;
    addr_3b61_7:
    eax6 = 1;
    goto addr_3b66_12;
    addr_3b90_10:
    eax6 = 2;
    goto addr_3b66_12;
}

int64_t fun_2460();

int64_t fun_23b0(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2460();
    if (r8d > 10) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x99e0 + rax11 * 4) + 0x99e0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* slotvec = reinterpret_cast<struct s0*>(0x90);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24f0();

struct s3 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_23a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    struct s0* r8_15;
    struct s3* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5daf;
    rax8 = fun_23c0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7141]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5e3b;
            fun_24f0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xd120) {
                fun_23a0(r14_19, rsi24, rsi, rdx, r8_15, r14_19, rsi24, rsi, rdx, r8_15);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5eca);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2480();
        } else {
            return r14_19;
        }
    }
}

uint32_t head_bytes(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rsi2;
    struct s0* rsp6;
    struct s0* rax7;
    struct s0* v8;
    uint32_t eax9;
    struct s0* r14_10;
    struct s0* r13_11;
    struct s0* rbp12;
    struct s0* r15_13;
    struct s0* r12_14;
    struct s0* rax15;
    struct s0* rdx16;
    void* rsp17;
    struct s0* rax18;
    struct s0* v19;
    void* rdx20;
    int64_t v21;
    struct s0* v22;
    void* rbx23;
    int32_t r13d24;
    struct s0* v25;
    int64_t rdi26;
    struct s0* rax27;
    struct s0* v28;
    void* rax29;
    void* rdx30;
    void* rax31;
    void* rax32;
    struct s0* rax33;
    struct s0* rdx34;
    struct s0* rbp35;
    struct s0* rax36;
    void* rsp37;
    struct s0* r15_38;
    struct s0* rdx39;
    struct s0* rdi40;
    struct s0* rax41;
    struct s0* r8_42;
    int32_t r12d43;
    void* r14_44;
    void* rax45;
    int64_t rsi46;
    struct s0* rax47;
    int64_t rdi48;
    struct s0* rax49;
    struct s0* rdi50;
    struct s0* rax51;
    void* rdx52;
    int64_t v53;
    int64_t rdi54;
    struct s0* rax55;
    int32_t eax56;
    struct s0* rdi57;
    struct s0* rsi58;
    int64_t rdi59;
    struct s0* rax60;
    struct s0* rbx61;
    struct s0* rax62;
    uint32_t eax63;
    struct s0* rax64;
    int32_t* rax65;
    struct s0* rdx66;
    int64_t rdi67;
    struct s0* rax68;
    struct s0* rsp69;
    int64_t rdi70;
    int32_t eax71;
    uint32_t v72;
    int64_t rdi73;
    struct s0* rax74;
    struct s0* rax75;
    int32_t* rax76;

    *reinterpret_cast<int32_t*>(&rsi2) = esi;
    rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax7 = g28;
    v8 = rax7;
    if (!rdx) {
        addr_3c78_2:
        eax9 = 1;
    } else {
        r14_10 = rdi;
        *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
        rbp12 = rdx;
        *reinterpret_cast<int32_t*>(&r15_13) = 0x2000;
        *reinterpret_cast<int32_t*>(&r15_13 + 4) = 0;
        r12_14 = rsp6;
        do {
            rsi2 = r12_14;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            if (reinterpret_cast<unsigned char>(r15_13) > reinterpret_cast<unsigned char>(rbp12)) {
                r15_13 = rbp12;
            }
            rax15 = safe_read(rdi, rsi2, r15_13, rcx, r8);
            rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8);
            if (reinterpret_cast<int1_t>(rax15 == 0xffffffffffffffff)) 
                goto addr_3c33_7;
        } while (rax15 && (rsi2 = rax15, rdi = r12_14, xwrite_stdout_part_0(rdi, rsi2, r15_13, rcx, r8), rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8), rbp12 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp12) - reinterpret_cast<unsigned char>(rax15)), !!rbp12));
        goto addr_3c78_2;
    }
    addr_3c7d_9:
    rdx16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx16) {
        return eax9;
    }
    fun_2480();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax18 = g28;
    v19 = rax18;
    if (rdx16) 
        goto addr_3cf5_13;
    addr_3e55_14:
    addr_3d88_15:
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        goto v21;
    }
    fun_2480();
    v22 = rdi;
    rbx23 = rdx20;
    r13d24 = *reinterpret_cast<int32_t*>(&rsi2);
    v25 = rcx;
    *reinterpret_cast<int32_t*>(&rdi26) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rax27 = g28;
    v28 = rax27;
    rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax29) >> 63) >> 51);
    rax31 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax29) + reinterpret_cast<uint64_t>(rdx30));
    *reinterpret_cast<uint32_t*>(&rax32) = *reinterpret_cast<uint32_t*>(&rax31) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
    rax33 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax32) - reinterpret_cast<uint64_t>(rdx30));
    *reinterpret_cast<int32_t*>(&rdx34) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0;
    if (!rax33) 
        goto addr_3edd_19;
    rdx34 = rax33;
    addr_3edd_19:
    rbp35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx34));
    rax36 = fun_24d0(rdi26, rdi26);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax36) < reinterpret_cast<signed char>(0)) {
        addr_4060_21:
        elseek_part_0();
    } else {
        r15_38 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp37) + 32);
        rdx39 = rdx34;
        *reinterpret_cast<int32_t*>(&rdi40) = r13d24;
        *reinterpret_cast<int32_t*>(&rdi40 + 4) = 0;
        rax41 = safe_read(rdi40, r15_38, rdx39, rcx, r8);
        r8_42 = rax41;
        if (rax41 == 0xffffffffffffffff) {
            addr_40cc_23:
            quotearg_style(4, v22, rdx39, rcx, r8_42);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d43 = reinterpret_cast<signed char>(line_end);
            r14_44 = rbx23;
            if (rbx23 && (r8_42 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp37) - 8 + 8 + reinterpret_cast<unsigned char>(r8_42) + 31) != *reinterpret_cast<signed char*>(&r12d43))) {
                r14_44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx23) + 0xffffffffffffffff);
                if (r8_42) 
                    goto addr_3fb5_26;
                goto addr_3f48_28;
            }
            while (1) {
                if (!r8_42) 
                    goto addr_3f48_28;
                addr_3fb5_26:
                if (!rbx23) {
                    r8_42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_42) - 1);
                    rax45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_44) + 0xffffffffffffffff);
                    if (!r14_44) 
                        goto addr_3fe0_31;
                } else {
                    rdx39 = r8_42;
                    *reinterpret_cast<int32_t*>(&rsi46) = r12d43;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    rax47 = fun_2640(r15_38, rsi46, rdx39);
                    r8_42 = rax47;
                    if (!rax47) {
                        addr_3f48_28:
                        if (rbp35 == v25) 
                            goto addr_4142_33; else 
                            goto addr_3f53_34;
                    } else {
                        r8_42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_42) - reinterpret_cast<unsigned char>(r15_38));
                        rax45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_44) + 0xffffffffffffffff);
                        if (!r14_44) 
                            goto addr_3fe0_31;
                    }
                }
                r14_44 = rax45;
                continue;
                addr_3f53_34:
                rbp35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp35) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi48) = r13d24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi48) + 4) = 0;
                rax49 = fun_24d0(rdi48, rdi48);
                if (reinterpret_cast<signed char>(rax49) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_21;
                *reinterpret_cast<int32_t*>(&rdx39) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi50) = r13d24;
                *reinterpret_cast<int32_t*>(&rdi50 + 4) = 0;
                rax51 = safe_read(rdi50, r15_38, 0x2000, rcx, r8_42);
                r8_42 = rax51;
                if (rax51 == 0xffffffffffffffff) 
                    goto addr_40cc_23;
                if (!r8_42) 
                    goto addr_4142_33;
                r12d43 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_40:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (rdx52) {
        fun_2480();
    } else {
        goto v53;
    }
    addr_4142_33:
    goto addr_4071_40;
    addr_3fe0_31:
    if (reinterpret_cast<signed char>(rbp35) <= reinterpret_cast<signed char>(v25)) 
        goto addr_4029_44;
    *reinterpret_cast<int32_t*>(&rdx39) = 0;
    *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi54) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
    rax55 = fun_24d0(rdi54);
    if (reinterpret_cast<signed char>(rax55) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_40;
    } else {
        eax56 = copy_fd(r13d24, reinterpret_cast<unsigned char>(rbp35) - reinterpret_cast<unsigned char>(v25));
        r8_42 = r8_42;
        if (eax56) {
            *reinterpret_cast<int32_t*>(&rdi57) = eax56;
            *reinterpret_cast<int32_t*>(&rdi57 + 4) = 0;
            diagnose_copy_fd_failure(rdi57, v22, 0, rcx, r8_42);
            goto addr_4071_40;
        } else {
            addr_4029_44:
            rsi58 = reinterpret_cast<struct s0*>(&r8_42->f1);
            if (rsi58) {
                xwrite_stdout_part_0(r15_38, rsi58, rdx39, rcx, r8_42);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi59) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi59) + 4) = 0;
    rax60 = fun_24d0(rdi59, rdi59);
    if (reinterpret_cast<signed char>(rax60) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_40;
    addr_3cf5_13:
    r14_10 = rdi;
    *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
    rbx61 = rdx16;
    r12_14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) + 0x90);
    r15_13 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax62 = safe_read(rdi, r12_14, 0x2000, rcx, r8), rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8), rsi2 = rax62, rax62 != 0xffffffffffffffff) {
        if (!rax62) 
            goto addr_3e55_14;
        eax63 = line_end;
        *reinterpret_cast<int32_t*>(&rbp12) = 0;
        *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
        do {
            rbp12 = reinterpret_cast<struct s0*>(&rbp12->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_13) + reinterpret_cast<unsigned char>(rbp12)) != *reinterpret_cast<signed char*>(&eax63)) 
                continue;
            rbx61 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx61) - 1);
            if (!rbx61) 
                goto addr_3d5d_58;
        } while (rsi2 != rbp12);
        xwrite_stdout_part_0(r12_14, rsi2, 0x2000, rcx, r8);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8);
    }
    rax64 = quotearg_style(4, r14_10, 0x2000, rcx, r8);
    fun_2450();
    rax65 = fun_23c0();
    rcx = rax64;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax65;
    fun_2630();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_15;
    addr_3d5d_58:
    *reinterpret_cast<int32_t*>(&rdx66) = 1;
    *reinterpret_cast<int32_t*>(&rdx66 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi67) = *reinterpret_cast<int32_t*>(&r13_11);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
    rax68 = fun_24d0(rdi67, rdi67);
    rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8);
    if (reinterpret_cast<signed char>(rax68) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi70) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0, eax71 = fun_26e0(rdi70, rsp69, 1, rcx, r8), rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8), !!eax71) || reinterpret_cast<int1_t>((v72 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx66) = 1, *reinterpret_cast<int32_t*>(&rdx66 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi73) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0, rax74 = fun_24d0(rdi73, rdi73), rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8), reinterpret_cast<signed char>(rax74) < reinterpret_cast<signed char>(0)))) {
        rdx66 = r14_10;
        elseek_part_0();
        rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8);
    }
    rsi2 = rbp12;
    rdi = r12_14;
    xwrite_stdout_part_0(rdi, rsi2, rdx66, rcx, r8);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8);
    goto addr_3d88_15;
    addr_3c33_7:
    rax75 = quotearg_style(4, r14_10, r15_13, rcx, r8);
    fun_2450();
    rax76 = fun_23c0();
    rcx = rax75;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax76;
    fun_2630();
    rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax9 = 0;
    goto addr_3c7d_9;
}

unsigned char elide_tail_lines_seekable(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* v6;
    struct s0* rbx7;
    int32_t r13d8;
    struct s0* v9;
    int64_t rdi10;
    struct s0* rax11;
    struct s0* v12;
    void* rax13;
    void* rdx14;
    void* rax15;
    void* rax16;
    struct s0* rax17;
    struct s0* rdx18;
    struct s0* rbp19;
    struct s0* rax20;
    void* rsp21;
    uint64_t rax22;
    struct s0* r15_23;
    struct s0* rdx24;
    struct s0* rdi25;
    struct s0* rax26;
    struct s0* r8_27;
    int32_t r12d28;
    struct s0* r14_29;
    struct s0* rax30;
    int64_t rsi31;
    struct s0* rax32;
    int64_t rdi33;
    struct s0* rax34;
    struct s0* rdi35;
    struct s0* rax36;
    void* rdx37;
    int64_t rdi38;
    struct s0* rax39;
    int32_t eax40;
    struct s0* rdi41;
    struct s0* rsi42;
    int64_t rdi43;
    struct s0* rax44;

    v6 = rdi;
    rbx7 = rdx;
    r13d8 = esi;
    v9 = rcx;
    *reinterpret_cast<int32_t*>(&rdi10) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax13) >> 63) >> 51);
    rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax13) + reinterpret_cast<uint64_t>(rdx14));
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax15) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax16) - reinterpret_cast<uint64_t>(rdx14));
    *reinterpret_cast<int32_t*>(&rdx18) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
    if (rax17) {
        rdx18 = rax17;
    }
    rbp19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx18));
    rax20 = fun_24d0(rdi10, rdi10);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax20) < reinterpret_cast<signed char>(0)) {
        addr_4060_4:
        elseek_part_0();
        *reinterpret_cast<int32_t*>(&rax22) = 0;
    } else {
        r15_23 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp21) + 32);
        rdx24 = rdx18;
        *reinterpret_cast<int32_t*>(&rdi25) = r13d8;
        *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0;
        rax26 = safe_read(rdi25, r15_23, rdx24, rcx, r8);
        r8_27 = rax26;
        if (rax26 == 0xffffffffffffffff) {
            addr_40cc_6:
            quotearg_style(4, v6, rdx24, rcx, r8_27);
            fun_2450();
            fun_23c0();
            fun_2630();
            *reinterpret_cast<int32_t*>(&rax22) = 0;
        } else {
            r12d28 = reinterpret_cast<signed char>(line_end);
            r14_29 = rbx7;
            if (rbx7 && (r8_27 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 + reinterpret_cast<unsigned char>(r8_27)) + 31) != *reinterpret_cast<signed char*>(&r12d28))) {
                r14_29 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx7) + 0xffffffffffffffff);
                if (r8_27) 
                    goto addr_3fb5_9;
                goto addr_3f48_11;
            }
            while (1) {
                if (!r8_27) 
                    goto addr_3f48_11;
                addr_3fb5_9:
                if (!rbx7) {
                    r8_27 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_27) - 1);
                    rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_29) + 0xffffffffffffffff);
                    if (!r14_29) 
                        goto addr_3fe0_14;
                } else {
                    rdx24 = r8_27;
                    *reinterpret_cast<int32_t*>(&rsi31) = r12d28;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi31) + 4) = 0;
                    rax32 = fun_2640(r15_23, rsi31, rdx24);
                    r8_27 = rax32;
                    if (!rax32) {
                        addr_3f48_11:
                        if (rbp19 == v9) 
                            goto addr_4142_16; else 
                            goto addr_3f53_17;
                    } else {
                        r8_27 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_27) - reinterpret_cast<unsigned char>(r15_23));
                        rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_29) + 0xffffffffffffffff);
                        if (!r14_29) 
                            goto addr_3fe0_14;
                    }
                }
                r14_29 = rax30;
                continue;
                addr_3f53_17:
                rbp19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp19) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi33) = r13d8;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
                rax34 = fun_24d0(rdi33, rdi33);
                if (reinterpret_cast<signed char>(rax34) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_4;
                *reinterpret_cast<int32_t*>(&rdx24) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx24 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi35) = r13d8;
                *reinterpret_cast<int32_t*>(&rdi35 + 4) = 0;
                rax36 = safe_read(rdi35, r15_23, 0x2000, rcx, r8_27);
                r8_27 = rax36;
                if (rax36 == 0xffffffffffffffff) 
                    goto addr_40cc_6;
                if (!r8_27) 
                    goto addr_4142_16;
                r12d28 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_23:
    rdx37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rdx37) {
        fun_2480();
    } else {
        return *reinterpret_cast<unsigned char*>(&rax22);
    }
    addr_4142_16:
    *reinterpret_cast<int32_t*>(&rax22) = 1;
    goto addr_4071_23;
    addr_3fe0_14:
    if (reinterpret_cast<signed char>(rbp19) <= reinterpret_cast<signed char>(v9)) 
        goto addr_4029_27;
    *reinterpret_cast<int32_t*>(&rdx24) = 0;
    *reinterpret_cast<int32_t*>(&rdx24 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi38) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi38) + 4) = 0;
    rax39 = fun_24d0(rdi38);
    if (reinterpret_cast<signed char>(rax39) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        *reinterpret_cast<int32_t*>(&rax22) = 0;
        goto addr_4071_23;
    } else {
        eax40 = copy_fd(r13d8, reinterpret_cast<unsigned char>(rbp19) - reinterpret_cast<unsigned char>(v9));
        r8_27 = r8_27;
        if (eax40) {
            *reinterpret_cast<int32_t*>(&rdi41) = eax40;
            *reinterpret_cast<int32_t*>(&rdi41 + 4) = 0;
            diagnose_copy_fd_failure(rdi41, v6, 0, rcx, r8_27);
            *reinterpret_cast<int32_t*>(&rax22) = 0;
            goto addr_4071_23;
        } else {
            addr_4029_27:
            rsi42 = reinterpret_cast<struct s0*>(&r8_27->f1);
            if (rsi42) {
                xwrite_stdout_part_0(r15_23, rsi42, rdx24, rcx, r8_27);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi43) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi43) + 4) = 0;
    rax44 = fun_24d0(rdi43, rdi43);
    if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        rax44 = rax44;
    }
    rax22 = reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rax44)) >> 63;
    goto addr_4071_23;
}

void diagnose_copy_fd_failure(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    if (*reinterpret_cast<int32_t*>(&rdi) != 1) {
        if (*reinterpret_cast<int32_t*>(&rdi) != 2) {
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
            fun_23b0(rdi);
        } else {
            quotearg_n_style_colon();
        }
    } else {
        quotearg_style(4, rsi, rdx, rcx, r8);
    }
    fun_2450();
    fun_23c0();
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x997b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x9974);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x997f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x9970);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gce18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gce18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2393() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_23a3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_23b3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_23c3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23d3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23e3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23f3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_2403() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20a0;

void fun_2413() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20b0;

void fun_2423() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2433() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2443() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_2453() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_2463() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_2473() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_2483() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2120;

void fun_2493() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2130;

void fun_24a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2140;

void fun_24b3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2150;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2170;

void fun_24e3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2180;

void fun_24f3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2190;

void fun_2503() {
    __asm__("cli ");
    goto close;
}

int64_t memchr = 0x21a0;

void fun_2513() {
    __asm__("cli ");
    goto memchr;
}

int64_t read = 0x21b0;

void fun_2523() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x21c0;

void fun_2533() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_2543() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x21e0;

void fun_2553() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x21f0;

void fun_2563() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2573() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2583() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2593() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_25a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_25b3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25c3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25d3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25e3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2280;

void fun_25f3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2290;

void fun_2603() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_2613() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_2623() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22c0;

void fun_2633() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x22d0;

void fun_2643() {
    __asm__("cli ");
    goto memrchr;
}

int64_t open = 0x22e0;

void fun_2653() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x22f0;

void fun_2663() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2300;

void fun_2673() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2310;

void fun_2683() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2320;

void fun_2693() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2330;

void fun_26a3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2340;

void fun_26b3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2350;

void fun_26c3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26d3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2370;

void fun_26e3() {
    __asm__("cli ");
    goto fstat;
}

int64_t __ctype_b_loc = 0x2380;

void fun_26f3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

struct s6 {
    signed char f0;
    signed char f1;
};

struct s5 {
    struct s0* f0;
    signed char[7] pad8;
    struct s6* f8;
};

void set_program_name(struct s0* rdi);

struct s0* fun_2610(int64_t rdi, ...);

void fun_2440(int64_t rdi, int64_t rsi);

void fun_2420(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

signed char have_read_stdin = 0;

signed char print_headers = 0;

int32_t fun_2490(int64_t rdi, struct s5* rsi, void* rdx, int64_t rcx);

struct s0* Version = reinterpret_cast<struct s0*>(16);

void version_etc(struct s0* rdi, int64_t rsi, int64_t rdx);

void fun_2690();

struct s6* usage();

int32_t optind = 0;

int64_t umaxtostr(struct s0* rdi, void* rsi, void* rdx, struct s0* rcx, struct s0* r8);

int64_t quote(int64_t rdi, ...);

int32_t fun_2500();

int32_t fun_2650(struct s0* rdi);

/* first_file.0 */
signed char first_file_0 = 1;

void fun_2620(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

unsigned char presume_input_pipe = 0;

struct s0* xmalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* xnmalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* full_read(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s7 {
    signed char[8192] pad8192;
    struct s0* f2000;
};

struct s0* xreallocarray(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s8 {
    signed char f0;
    struct s0* f1;
};

struct s8* fun_2550(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

void fun_2590(signed char* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* fun_2510();

struct s9 {
    signed char[8192] pad8192;
    struct s0* f2000;
};

int64_t fun_2753(int32_t edi, struct s5* rsi) {
    int64_t rbp3;
    struct s5* rbx4;
    struct s0* rdi5;
    struct s0* rax6;
    struct s0* v7;
    void* rsp8;
    struct s6* rax9;
    struct s0* v10;
    struct s0* r12_11;
    struct s0* r8_12;
    void* rdx13;
    int64_t rdi14;
    int32_t eax15;
    void* rsp16;
    struct s0* rcx17;
    struct s0* rdi18;
    int64_t r9_19;
    struct s1* r8_20;
    struct s1* rcx21;
    int32_t eax22;
    struct s0* rax23;
    int1_t less_or_equal24;
    int64_t rax25;
    struct s0** rax26;
    struct s0* r15_27;
    unsigned char v28;
    void* v29;
    struct s0* rdi30;
    uint64_t rdi31;
    struct s0* rdx32;
    uint64_t v33;
    struct s0* v34;
    struct s0* rdx35;
    struct s0* rbx36;
    struct s0* v37;
    struct s0* v38;
    struct s0* v39;
    int64_t rax40;
    int64_t rax41;
    int32_t eax42;
    int64_t rax43;
    struct s0* rax44;
    struct s0* rax45;
    uint32_t v46;
    int32_t eax47;
    struct s0* rax48;
    void* rsp49;
    struct s0* r13_50;
    uint32_t eax51;
    uint32_t eax52;
    int32_t eax53;
    void* rsp54;
    struct s0* rax55;
    struct s0* rax56;
    int1_t zf57;
    int32_t eax58;
    struct s0* rax59;
    int1_t zf60;
    int1_t zf61;
    struct s0* rsi62;
    int64_t rdi63;
    int32_t eax64;
    uint32_t v65;
    struct s0* v66;
    struct s0* rax67;
    struct s0* r13_68;
    struct s0* v69;
    struct s0* rax70;
    void* rsp71;
    struct s0* v72;
    struct s0* r12_73;
    int32_t r14d74;
    uint32_t r13d75;
    struct s0* rbp76;
    struct s0* rax77;
    void* rsp78;
    struct s0* v79;
    struct s0* r15_80;
    struct s0* v81;
    struct s0* r14_82;
    int64_t rax83;
    int64_t rdi84;
    struct s0* rbp85;
    struct s0* rsi86;
    struct s0* rax87;
    uint32_t eax88;
    int64_t rax89;
    struct s0* rdi90;
    int32_t* rax91;
    struct s0* rax92;
    struct s0* rbp93;
    struct s0* r14_94;
    struct s0* v95;
    struct s0* v96;
    struct s0* r15_97;
    unsigned char* v98;
    struct s0* v99;
    struct s0* rax100;
    struct s0* r14_101;
    struct s0* rsi102;
    struct s0* rax103;
    struct s0* v104;
    struct s0* r14_105;
    struct s0* r13_106;
    struct s0** r15_107;
    struct s0* rax108;
    int64_t rdi109;
    struct s0* rax110;
    struct s0* r15_111;
    int32_t* rax112;
    int64_t rdi113;
    struct s0* rax114;
    struct s0* rdi115;
    int64_t rdi116;
    struct s0* rax117;
    struct s0* rdi118;
    struct s0* rax119;
    struct s0* rsi120;
    int32_t ebp121;
    struct s8* rbx122;
    struct s0* rdi123;
    struct s0* rsi124;
    struct s8* rax125;
    struct s0* rdx126;
    struct s0* rbx127;
    struct s0* rdi128;
    struct s0* rax129;
    struct s0* rsi130;
    struct s0* rbx131;
    struct s0* rax132;
    struct s0* rax133;
    int32_t* rax134;
    int64_t rdi135;
    struct s0* rax136;
    uint32_t ebx137;
    struct s0* r14_138;
    struct s0* rbx139;
    struct s0* rbp140;
    int32_t v141;
    struct s0* rdi142;
    struct s0* rax143;
    struct s0* v144;
    struct s0* rdi145;
    void* rbp146;
    struct s0* r13_147;
    int32_t r14d148;
    void* rbx149;
    struct s0* rax150;
    struct s0* rdi151;
    int64_t rdi152;
    struct s0* rax153;
    struct s0* rax154;
    struct s0* rax155;
    uint32_t ebp156;
    int64_t rax157;
    struct s0* rdi158;
    struct s0* rdi159;
    struct s0* rdi160;
    struct s0* rdi161;
    struct s0** rax162;
    struct s0* rdi163;
    struct s0* r13_164;
    struct s0* r14_165;
    struct s0* rdi166;
    struct s0* rax167;
    struct s0* rax168;
    struct s0* rax169;
    struct s0* v170;
    struct s0* v171;
    struct s0* rsi172;
    int32_t eax173;
    struct s0* rdi174;
    uint32_t ebx175;
    unsigned char al176;
    uint32_t eax177;
    uint32_t eax178;
    uint32_t eax179;
    int64_t rax180;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    rbx4 = rsi;
    rdi5 = rsi->f0;
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_2610(6, 6);
    fun_2440("coreutils", "/usr/local/share/locale");
    fun_2420("coreutils", "/usr/local/share/locale");
    atexit(0x45a0, "/usr/local/share/locale");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    have_read_stdin = 0;
    print_headers = 0;
    line_end = 10;
    if (*reinterpret_cast<int32_t*>(&rbp3) <= 1) 
        goto addr_27e7_2;
    rax9 = rbx4->f8;
    if (rax9->f0 == 45) 
        goto addr_2949_4;
    while (1) {
        addr_27e7_2:
        v10 = reinterpret_cast<struct s0*>(10);
        while (1) {
            r12_11 = reinterpret_cast<struct s0*>(0x9834);
            *reinterpret_cast<int32_t*>(&r8_12) = 0;
            *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
            rdx13 = reinterpret_cast<void*>("c:n:qvz0123456789");
            *reinterpret_cast<int32_t*>(&rdi14) = *reinterpret_cast<int32_t*>(&rbp3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
            eax15 = fun_2490(rdi14, rbx4, "c:n:qvz0123456789", 0xca80);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rcx17) = eax15;
            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
            if (eax15 == -1) 
                goto addr_2a47_7;
            if (*reinterpret_cast<int32_t*>(&rcx17) > 0x80) 
                goto addr_32f0_9;
            if (*reinterpret_cast<int32_t*>(&rcx17) > 98) 
                goto addr_2845_11;
            if (*reinterpret_cast<int32_t*>(&rcx17) == 0xffffff7d) {
                rdi18 = stdout;
                rcx17 = Version;
                r9_19 = reinterpret_cast<int64_t>("Jim Meyering");
                version_etc(rdi18, "head", "GNU coreutils");
                fun_2690();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 - 8 - 8 + 8 - 8 + 8);
            }
            if (*reinterpret_cast<int32_t*>(&rcx17) != 0xffffff7e) 
                goto addr_32f0_9;
            rax9 = usage();
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
            addr_2949_4:
            if (rax9->f1 - 48 > 9) 
                break;
            r8_20 = reinterpret_cast<struct s1*>(&rax9->f1);
            rcx21 = r8_20;
            do {
                eax22 = rcx21->f1;
                rcx21 = reinterpret_cast<struct s1*>(&rcx21->f1);
                *reinterpret_cast<int32_t*>(&r12_11) = eax22;
                *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
            } while (eax22 - 48 <= 9);
            if (*reinterpret_cast<signed char*>(&r12_11)) 
                goto addr_297c_19;
            rbx4 = reinterpret_cast<struct s5*>(&rbx4->f8);
            *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
            rax23 = string_to_integer(1, r8_20);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            v10 = rax23;
            rbx4->f0 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbx4) - 8);
        }
    }
    addr_2a47_7:
    if (!0 && (less_or_equal24 = static_cast<int32_t>(rbp3 - 1) <= optind, !less_or_equal24)) {
        print_headers = 1;
    }
    if (1 || (1 || reinterpret_cast<signed char>(v10) >= reinterpret_cast<signed char>(0))) {
        rax25 = optind;
        if (*reinterpret_cast<int32_t*>(&rax25) >= *reinterpret_cast<int32_t*>(&rbp3)) {
            rax26 = reinterpret_cast<struct s0**>(0xca60);
            r15_27 = reinterpret_cast<struct s0*>("-");
        } else {
            rax26 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rbx4) + rax25 * 8);
            r15_27 = *rax26;
            if (!r15_27) {
                v28 = 1;
                goto addr_2e56_28;
            }
        }
        v28 = 1;
        v29 = reinterpret_cast<void*>(rax26 + 8);
        rdi30 = v10;
        *reinterpret_cast<uint32_t*>(&rdi31) = *reinterpret_cast<uint32_t*>(&rdi30) & 0x1fff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi31) + 4) = 0;
        rdx32 = reinterpret_cast<struct s0*>(0x2000 - rdi31);
        v33 = rdi31;
        v34 = rdx32;
        rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(rdx32)) >> 13);
        rbx36 = reinterpret_cast<struct s0*>(&rdx35->f1);
        v37 = rdx35;
        v38 = rbx36;
        v39 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx36) >> 1);
        goto addr_2b7f_30;
    }
    addr_379e_31:
    rax40 = umaxtostr(v10, reinterpret_cast<int64_t>(rsp16) + 0x120, rdx13, rcx17, r8_12);
    quote(rax40, rax40);
    fun_2450();
    fun_2630();
    addr_32f0_9:
    if (static_cast<uint32_t>(reinterpret_cast<unsigned char>(rcx17) + 0xffffffffffffffd0) > 9) 
        goto addr_32fc_32;
    fun_2450();
    while (1) {
        fun_2630();
        addr_32fc_32:
        usage();
        addr_3306_35:
        if (*reinterpret_cast<signed char*>(&r9_19)) {
            line_end = 0;
        }
        fun_2450();
    }
    addr_2845_11:
    *reinterpret_cast<uint32_t*>(&rax41) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rcx17) + 0xffffffffffffff9d);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax41) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax41) <= 29) {
        goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(0x9834) + reinterpret_cast<uint64_t>(rax41 * 4)))) + reinterpret_cast<unsigned char>(0x9834);
    }
    addr_297c_19:
    *reinterpret_cast<int32_t*>(&r9_19) = 0;
    eax42 = static_cast<int32_t>(reinterpret_cast<unsigned char>(r12_11) + 0xffffffffffffff9e);
    if (*reinterpret_cast<unsigned char*>(&eax42) > 24) 
        goto addr_3306_35;
    *reinterpret_cast<uint32_t*>(&rax43) = *reinterpret_cast<unsigned char*>(&eax42);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x98ac + rax43 * 4) + 0x98ac;
    while (1) {
        addr_3211_41:
        *reinterpret_cast<uint32_t*>(&rbx4) = 0;
        rax44 = quotearg_style(4, r15_27, rdx35, rcx17, r8_12);
        rax45 = fun_2450();
        fun_23c0();
        rcx17 = rax44;
        rdx35 = rax45;
        fun_2630();
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        while (1) {
            while (1) {
                if (v46 && (eax47 = fun_2500(), rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8), !!eax47)) {
                    rax48 = quotearg_style(4, r15_27, rdx35, rcx17, r8_12);
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    r13_50 = rax48;
                    goto addr_2bc3_44;
                }
                while (v29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v29) + 8), v28 = reinterpret_cast<unsigned char>(v28 & *reinterpret_cast<unsigned char*>(&rbx4)), r15_27 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(v29) - 8), !!r15_27) {
                    addr_2b7f_30:
                    eax51 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r15_27->f0)) - 45;
                    v46 = eax51;
                    if (eax51) 
                        goto addr_2b90_46;
                    eax52 = r15_27->f1;
                    v46 = eax52;
                    if (!eax52) 
                        goto addr_2b02_48;
                    addr_2b90_46:
                    eax53 = fun_2650(r15_27);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    *reinterpret_cast<int32_t*>(&r12_11) = eax53;
                    if (eax53 >= 0) 
                        goto addr_2b22_49;
                    rax55 = quotearg_style(4, r15_27, rdx35, rcx17, r8_12);
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                    r13_50 = rax55;
                    addr_2bc3_44:
                    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                    rax56 = fun_2450();
                    r12_11 = rax56;
                    fun_23c0();
                    rcx17 = r13_50;
                    rdx35 = r12_11;
                    fun_2630();
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                addr_2e56_28:
                zf57 = have_read_stdin == 0;
                if (zf57) 
                    goto addr_2e63_51;
                eax58 = fun_2500();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                if (eax58 >= 0) 
                    goto addr_2e63_51;
                fun_23c0();
                fun_2630();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
                goto addr_327d_54;
                addr_2b02_48:
                *reinterpret_cast<int32_t*>(&rdx35) = 5;
                *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r12_11) = 0;
                have_read_stdin = 1;
                rax59 = fun_2450();
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                r15_27 = rax59;
                addr_2b22_49:
                zf60 = print_headers == 0;
                if (!zf60) {
                    zf61 = first_file_0 == 0;
                    rcx17 = r15_27;
                    rdx35 = reinterpret_cast<struct s0*>(0x9da1);
                    if (zf61) {
                        rdx35 = reinterpret_cast<struct s0*>("\n");
                    }
                    fun_2620(1, "%s==> %s <==\n", rdx35, rcx17, r8_12);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                    first_file_0 = 0;
                }
                if (!0) 
                    break;
                rsi62 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp54) + 0x90);
                *reinterpret_cast<int32_t*>(&rdi63) = *reinterpret_cast<int32_t*>(&r12_11);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
                eax64 = fun_26e0(rdi63, rsi62, rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                if (eax64) 
                    goto addr_3211_41;
                *reinterpret_cast<uint32_t*>(&rbx4) = presume_input_pipe;
                if (*reinterpret_cast<unsigned char*>(&rbx4) || (v65 & 0xd000) != rpl_mbrtowc) {
                    v66 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
                    if (1) {
                        rax67 = xmalloc(0x2020, rsi62, rdx35, rcx17, r8_12);
                        rax67->f2010 = reinterpret_cast<void*>(0);
                        r13_68 = rax67;
                        rax67->f2008 = reinterpret_cast<struct s0*>(0);
                        rax67->f2018 = reinterpret_cast<struct s0*>(0);
                        v69 = rax67;
                        rax70 = xmalloc(0x2020, rsi62, rdx35, rcx17, r8_12);
                        rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
                        v72 = reinterpret_cast<struct s0*>(0);
                        r12_73 = rax70;
                        r14d74 = *reinterpret_cast<int32_t*>(&r12_11);
                    } else {
                        if (reinterpret_cast<unsigned char>(v10) <= reinterpret_cast<unsigned char>(0x100000)) {
                            r13d75 = 0;
                            rbp76 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v10) + 0x2000);
                            rax77 = xnmalloc(2, rbp76, rdx35, rcx17, r8_12);
                            rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                            *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                            v79 = r15_27;
                            r15_80 = v10;
                            v81 = rax77;
                            r14_82 = rbp76;
                            while (1) {
                                *reinterpret_cast<uint32_t*>(&rax83) = *reinterpret_cast<unsigned char*>(&r13d75);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax83) + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdi84) = *reinterpret_cast<int32_t*>(&r12_11);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi84) + 4) = 0;
                                rbp85 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp78) + rax83 * 8 + 0x80);
                                rsi86 = rbp85;
                                rax87 = full_read(rdi84, rsi86, r14_82, rcx17, r8_12);
                                rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                                rdx35 = rax87;
                                if (reinterpret_cast<unsigned char>(r14_82) <= reinterpret_cast<unsigned char>(rax87)) {
                                    if (1) {
                                        addr_3556_67:
                                        if (r15_80 && (rsi86 = r15_80, eax88 = r13d75 ^ 1, *reinterpret_cast<uint32_t*>(&rax89) = *reinterpret_cast<unsigned char*>(&eax88), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0, rdi90 = reinterpret_cast<struct s0*>(&(*reinterpret_cast<struct s7**>(reinterpret_cast<int64_t>(rsp78) + rax89 * 8 + 0x80))->f2000), xwrite_stdout_part_0(rdi90, rsi86, rdx35, rcx17, r8_12), rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8), rdx35 = rdx35, reinterpret_cast<unsigned char>(r15_80) >= reinterpret_cast<unsigned char>(rdx35)) || (rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx35) - reinterpret_cast<unsigned char>(r15_80)), rsi86 = rdx35, rdx35 == 0)) {
                                            addr_358f_68:
                                            r13d75 = r13d75 ^ 1;
                                            if (!1) 
                                                goto addr_3597_69;
                                        } else {
                                            addr_35b1_70:
                                            xwrite_stdout_part_0(rbp85, rsi86, rdx35, rcx17, r8_12);
                                            rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                                            goto addr_358f_68;
                                        }
                                    } else {
                                        if (reinterpret_cast<unsigned char>(r15_80) >= reinterpret_cast<unsigned char>(rax87) || (rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx35) - reinterpret_cast<unsigned char>(r15_80)), rsi86 = rdx35, !rdx35)) {
                                            r13d75 = r13d75 ^ 1;
                                        } else {
                                            *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                                            goto addr_35b1_70;
                                        }
                                    }
                                    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                                    continue;
                                }
                                rax91 = fun_23c0();
                                rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                                rdx35 = rax87;
                                if (*rax91) 
                                    goto addr_36ec_76;
                                if (reinterpret_cast<unsigned char>(r15_80) >= reinterpret_cast<unsigned char>(rdx35)) 
                                    goto addr_3500_78;
                                if (!0) 
                                    goto addr_3551_80;
                                rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx35) - reinterpret_cast<unsigned char>(r15_80));
                                rsi86 = rdx35;
                                if (rdx35) 
                                    goto addr_35b1_70; else 
                                    goto addr_3604_82;
                                addr_3551_80:
                                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                                goto addr_3556_67;
                            }
                        } else {
                            *reinterpret_cast<int32_t*>(&rax92) = 16;
                            *reinterpret_cast<int32_t*>(&rax92 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rbp93) = 0;
                            *reinterpret_cast<int32_t*>(&rbp93 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&r14_94) = 0;
                            *reinterpret_cast<int32_t*>(&r14_94 + 4) = 0;
                            v95 = r15_27;
                            v96 = reinterpret_cast<struct s0*>(1);
                            r15_97 = reinterpret_cast<struct s0*>(0);
                            v98 = reinterpret_cast<unsigned char*>(0);
                            if (reinterpret_cast<unsigned char>(v38) <= reinterpret_cast<unsigned char>(16)) {
                                rax92 = v38;
                            }
                            v99 = rax92;
                            while (1) {
                                if (r14_94) {
                                    rax100 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_94) + reinterpret_cast<unsigned char>(r14_94));
                                    if (reinterpret_cast<unsigned char>(v39) < reinterpret_cast<unsigned char>(r14_94)) {
                                        rax100 = v38;
                                    }
                                    r14_101 = rax100;
                                } else {
                                    r14_101 = v99;
                                }
                                rsi102 = r14_101;
                                *reinterpret_cast<int32_t*>(&rdx35) = 8;
                                *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                                rax103 = xreallocarray(rbp93, rsi102, 8, rcx17, r8_12);
                                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                                v104 = r14_101;
                                rbp93 = rax103;
                                r14_105 = r15_97;
                                r13_106 = v96;
                                while (1) {
                                    r15_107 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<unsigned char>(r14_105) * 8);
                                    if (1) {
                                        rax108 = xmalloc(0x2000, rsi102, rdx35, rcx17, r8_12);
                                        *reinterpret_cast<int32_t*>(&rdi109) = *reinterpret_cast<int32_t*>(&r12_11);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0;
                                        *r15_107 = rax108;
                                        rsi102 = rax108;
                                        v98 = &r14_105->f1;
                                        rax110 = full_read(rdi109, rsi102, 0x2000, rcx17, r8_12);
                                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
                                        r15_111 = rax110;
                                        if (reinterpret_cast<unsigned char>(rax110) <= reinterpret_cast<unsigned char>(0x1fff)) {
                                            rax112 = fun_23c0();
                                            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                                            if (*rax112) 
                                                goto addr_3636_96;
                                        }
                                        if (v37 != r14_105) 
                                            goto addr_2d57_99;
                                    } else {
                                        rsi102 = *r15_107;
                                        *reinterpret_cast<int32_t*>(&rdi113) = *reinterpret_cast<int32_t*>(&r12_11);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi113) + 4) = 0;
                                        rax114 = full_read(rdi113, rsi102, 0x2000, rcx17, r8_12);
                                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                                        r15_111 = rax114;
                                        if (reinterpret_cast<unsigned char>(rax114) > reinterpret_cast<unsigned char>(0x1fff)) {
                                            goto addr_2e1d_103;
                                        } else {
                                            rax112 = fun_23c0();
                                            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                                            if (*rax112) 
                                                goto addr_3636_96;
                                        }
                                    }
                                    if (r15_111) {
                                        addr_2e1d_103:
                                        rdi115 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<unsigned char>(r13_106) * 8);
                                        rsi102 = r15_111;
                                        xwrite_stdout_part_0(rdi115, rsi102, 0x2000, rcx17, r8_12);
                                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                                        goto addr_2d57_99;
                                    } else {
                                        addr_2d57_99:
                                        rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(&r13_106->f1) % reinterpret_cast<unsigned char>(v38));
                                        if (0) 
                                            goto addr_33ae_107;
                                    }
                                    if (v104 == r13_106) 
                                        break;
                                    r14_105 = r13_106;
                                    r13_106 = rdx35;
                                }
                                r14_94 = v104;
                                r15_97 = r13_106;
                                v96 = rdx35;
                            }
                        }
                    }
                } else {
                    addr_327d_54:
                    *reinterpret_cast<int32_t*>(&rdi116) = *reinterpret_cast<int32_t*>(&r12_11);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi116) + 4) = 0;
                    rax117 = fun_24d0(rdi116, rdi116);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    v66 = rax117;
                    if (reinterpret_cast<signed char>(rax117) < reinterpret_cast<signed char>(0)) {
                        rdx35 = r15_27;
                        elseek_part_0();
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        continue;
                    }
                }
                while (*reinterpret_cast<int32_t*>(&rdi118) = r14d74, *reinterpret_cast<int32_t*>(&rdi118 + 4) = 0, rax119 = safe_read(rdi118, r12_73, 0x2000, rcx17, r8_12), rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8), rsi120 = rax119, reinterpret_cast<unsigned char>(rax119) + 0xffffffffffffffff <= 0xfffffffffffffffd) {
                    if (!v10) {
                        xwrite_stdout_part_0(r12_73, rsi120, 0x2000, rcx17, r8_12);
                        rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                        continue;
                    }
                    ebp121 = reinterpret_cast<signed char>(line_end);
                    rbx122 = reinterpret_cast<struct s8*>(reinterpret_cast<unsigned char>(r12_73) + reinterpret_cast<unsigned char>(rsi120));
                    r12_73->f2008 = rsi120;
                    rdi123 = r12_73;
                    r12_73->f2010 = reinterpret_cast<void*>(0);
                    r12_73->f2018 = reinterpret_cast<struct s0*>(0);
                    rbx122->f0 = *reinterpret_cast<signed char*>(&ebp121);
                    while (*reinterpret_cast<int32_t*>(&rsi124) = ebp121, *reinterpret_cast<int32_t*>(&rsi124 + 4) = 0, rax125 = fun_2550(rdi123, rsi124, 0x2000, rcx17, r8_12), rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8), reinterpret_cast<uint64_t>(rbx122) > reinterpret_cast<uint64_t>(rax125)) {
                        r12_73->f2010 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_73->f2010) + 1);
                        rdi123 = reinterpret_cast<struct s0*>(&rax125->f1);
                    }
                    rdx126 = r12_73->f2008;
                    rbx127 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v72) + reinterpret_cast<uint64_t>(r12_73->f2010));
                    rdi128 = v69->f2008;
                    v72 = rbx127;
                    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx126) + reinterpret_cast<unsigned char>(rdi128)) > 0x1fff) 
                        goto addr_2fcb_119;
                    fun_2590(reinterpret_cast<unsigned char>(rdi128) + reinterpret_cast<unsigned char>(v69), r12_73, rdx126, rcx17, r8_12);
                    rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                    v69->f2008 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v69->f2008) + reinterpret_cast<unsigned char>(r12_73->f2008));
                    v69->f2010 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v69->f2010) + reinterpret_cast<uint64_t>(r12_73->f2010));
                    continue;
                    addr_2fcb_119:
                    v69->f2018 = r12_73;
                    if (reinterpret_cast<unsigned char>(v10) >= reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx127) - reinterpret_cast<uint64_t>(r13_68->f2010))) {
                        rax129 = xmalloc(0x2020, rsi124, rdx126, rcx17, r8_12);
                        rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                        v69 = r12_73;
                        r12_73 = rax129;
                    } else {
                        rsi130 = r13_68->f2008;
                        if (rsi130) {
                            xwrite_stdout_part_0(r13_68, rsi130, rdx126, rcx17, r8_12);
                            rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                        }
                        rbx131 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx127) - reinterpret_cast<uint64_t>(r13_68->f2010));
                        v69 = r12_73;
                        r12_73 = r13_68;
                        v72 = rbx131;
                        r13_68 = r13_68->f2018;
                    }
                }
                *reinterpret_cast<int32_t*>(&r12_11) = r14d74;
                fun_23a0(r12_73, rsi120, 0x2000, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
                if (!&rsi120->f1) {
                    rax132 = quotearg_style(4, r15_27, 0x2000, rcx17, r8_12);
                    rax133 = fun_2450();
                    rax134 = fun_23c0();
                    rdx35 = rax133;
                    rcx17 = rax132;
                    *reinterpret_cast<int32_t*>(&rsi120) = *rax134;
                    *reinterpret_cast<int32_t*>(&rsi120 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                    fun_2630();
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    if (!r13_68) {
                        addr_31c4_127:
                        if (v66 == 0xffffffffffffffff) 
                            continue;
                        *reinterpret_cast<int32_t*>(&rdx35) = 0;
                        *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi135) = *reinterpret_cast<int32_t*>(&r12_11);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi135) + 4) = 0;
                        rax136 = fun_24d0(rdi135, rdi135);
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        if (reinterpret_cast<signed char>(rax136) >= reinterpret_cast<signed char>(0)) 
                            continue;
                    } else {
                        goto addr_31b0_130;
                    }
                } else {
                    rdx35 = v69;
                    if (rdx35->f2008 && (ebx137 = line_end, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx35) + reinterpret_cast<unsigned char>(rdx35->f2008) + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&ebx137))) {
                        rdx35->f2010 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx35->f2010) + 1);
                        v72 = reinterpret_cast<struct s0*>(&v72->f1);
                    }
                    r14_138 = r13_68;
                    if (reinterpret_cast<unsigned char>(v10) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v72) - reinterpret_cast<uint64_t>(r13_68->f2010))) {
                        rbx139 = v10;
                        rbp140 = v72;
                        v141 = *reinterpret_cast<int32_t*>(&r12_11);
                        do {
                            rsi120 = r14_138->f2008;
                            if (rsi120) {
                                xwrite_stdout_part_0(r14_138, rsi120, rdx35, rcx17, r8_12);
                                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                            }
                            rbp140 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp140) - reinterpret_cast<uint64_t>(r14_138->f2010));
                            r14_138 = r14_138->f2018;
                            rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp140) - reinterpret_cast<uint64_t>(r14_138->f2010));
                        } while (reinterpret_cast<unsigned char>(rbx139) < reinterpret_cast<unsigned char>(rdx35));
                        v72 = rbp140;
                        *reinterpret_cast<int32_t*>(&r12_11) = v141;
                    }
                    if (reinterpret_cast<unsigned char>(v10) >= reinterpret_cast<unsigned char>(v72)) 
                        goto addr_31a8_140; else 
                        goto addr_3132_141;
                }
                rdx35 = r15_27;
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                elseek_part_0();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                continue;
                do {
                    addr_31b0_130:
                    rdi142 = r13_68;
                    r13_68 = r13_68->f2018;
                    fun_23a0(rdi142, rsi120, rdx35, rcx17, r8_12);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                } while (r13_68);
                goto addr_31c4_127;
                addr_31a8_140:
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                goto addr_31b0_130;
                addr_3132_141:
                rax143 = r14_138->f2008;
                v144 = r13_68;
                rdi145 = r14_138;
                rbp146 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v72) - reinterpret_cast<unsigned char>(v10));
                r13_147 = r14_138;
                r14d148 = *reinterpret_cast<int32_t*>(&r12_11);
                rbx149 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax143) + reinterpret_cast<unsigned char>(r14_138));
                do {
                    rdx35 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx149) - reinterpret_cast<unsigned char>(rdi145));
                    rax150 = fun_2510();
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    rdi145 = rax150;
                    if (!rax150) 
                        break;
                    r13_147->f2010 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_147->f2010) + 1);
                    rdi145 = reinterpret_cast<struct s0*>(&rdi145->f1);
                    rbp146 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp146) - 1);
                } while (rbp146);
                *reinterpret_cast<int32_t*>(&r12_11) = r14d148;
                r13_68 = v144;
                rdi151 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi145) - reinterpret_cast<unsigned char>(r13_147));
                rsi120 = rdi151;
                if (!rdi151) 
                    goto addr_31a8_140;
                xwrite_stdout_part_0(r13_147, rsi120, rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                goto addr_31a8_140;
                addr_3597_69:
                r15_27 = v79;
                addr_3512_147:
                fun_23a0(v81, rsi86, rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                addr_3443_148:
                if (v66 == 0xffffffffffffffff) 
                    continue;
                *reinterpret_cast<int32_t*>(&rdx35) = 0;
                *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi152) = *reinterpret_cast<int32_t*>(&r12_11);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi152) + 4) = 0;
                rax153 = fun_24d0(rdi152, rdi152);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                if (reinterpret_cast<signed char>(rax153) >= reinterpret_cast<signed char>(0)) 
                    continue;
                rdx35 = r15_27;
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                elseek_part_0();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                continue;
                addr_36ec_76:
                r15_27 = v79;
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                rax154 = quotearg_style(4, r15_27, rdx35, rcx17, r8_12);
                rax155 = fun_2450();
                rcx17 = rax154;
                rdx35 = rax155;
                *reinterpret_cast<int32_t*>(&rsi86) = *rax91;
                *reinterpret_cast<int32_t*>(&rsi86 + 4) = 0;
                fun_2630();
                rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_3512_147;
                addr_3500_78:
                r15_27 = v79;
                if (!1 || !rdx35) {
                    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                    goto addr_3512_147;
                } else {
                    rsi86 = rdx35;
                    ebp156 = r13d75 ^ 1;
                    *reinterpret_cast<uint32_t*>(&rax157) = *reinterpret_cast<unsigned char*>(&ebp156);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax157) + 4) = 0;
                    rdi158 = reinterpret_cast<struct s0*>(&(*reinterpret_cast<struct s9**>(reinterpret_cast<int64_t>(rsp78) + rax157 * 8 + 0x80))->f2000);
                    xwrite_stdout_part_0(rdi158, rsi86, rdx35, rcx17, r8_12);
                    rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                    goto addr_3512_147;
                }
                addr_3604_82:
                goto addr_3597_69;
                addr_33ae_107:
                rcx17 = r15_111;
                r15_27 = v95;
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                if (1) {
                    if (v37 == r13_106 && (rsi102 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx17) - v33), !!rsi102)) {
                        rdi159 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<unsigned char>(rdx35) * 8);
                        xwrite_stdout_part_0(rdi159, rsi102, rdx35, rcx17, r8_12);
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    }
                } else {
                    rdi160 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<unsigned char>(r13_106) * 8);
                    rsi102 = reinterpret_cast<struct s0*>(0x2000 - reinterpret_cast<unsigned char>(rcx17));
                    rdi161 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi160) + reinterpret_cast<unsigned char>(rcx17));
                    if (reinterpret_cast<unsigned char>(v34) < reinterpret_cast<unsigned char>(rsi102)) {
                        rsi102 = v34;
                        *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                        xwrite_stdout_part_0(rdi161, rsi102, rdx35, rcx17, r8_12);
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    } else {
                        rcx17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx17) - v33);
                        rax162 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<unsigned char>(rdx35) * 8);
                        if (!rsi102 || (*reinterpret_cast<uint32_t*>(&rbx4) = 0, xwrite_stdout_part_0(rdi161, rsi102, rdx35, rcx17, r8_12), rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8), rax162 = rax162, !!rcx17)) {
                            rdi163 = *rax162;
                            rsi102 = rcx17;
                            *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                            xwrite_stdout_part_0(rdi163, rsi102, rdx35, rcx17, r8_12);
                            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        }
                    }
                }
                addr_341a_159:
                r13_164 = rbp93;
                r14_165 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp93) + reinterpret_cast<uint64_t>(v98) * 8);
                while (r13_164 != r14_165) {
                    rdi166 = *reinterpret_cast<struct s0**>(&r13_164->f0);
                    r13_164 = reinterpret_cast<struct s0*>(&r13_164->f8);
                    fun_23a0(rdi166, rsi102, rdx35, rcx17, r8_12);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                }
                fun_23a0(rbp93, rsi102, rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                goto addr_3443_148;
                addr_3636_96:
                r15_27 = v95;
                rax167 = quotearg_style(4, r15_27, 0x2000, rcx17, r8_12);
                rax168 = fun_2450();
                *reinterpret_cast<int32_t*>(&rsi102) = *rax112;
                *reinterpret_cast<int32_t*>(&rsi102 + 4) = 0;
                rcx17 = rax167;
                rdx35 = rax168;
                *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                fun_2630();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_341a_159;
                rax169 = v170;
                rsi62 = v171;
                rcx17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax169) + 0xffffffffffffffff);
                *reinterpret_cast<int32_t*>(&rdx35) = 0x200;
                *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                if (reinterpret_cast<unsigned char>(rcx17) > reinterpret_cast<unsigned char>(0x1fffffffffffffff)) {
                    rax169 = reinterpret_cast<struct s0*>(0x200);
                }
                if (!0) 
                    goto addr_32cf_166;
                if (reinterpret_cast<signed char>(rsi62) > reinterpret_cast<signed char>(rax169)) {
                    rsi172 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsi62) - reinterpret_cast<unsigned char>(v66));
                    if (reinterpret_cast<signed char>(rsi172) < reinterpret_cast<signed char>(0)) {
                        rsi172 = reinterpret_cast<struct s0*>(0);
                    }
                    if (reinterpret_cast<unsigned char>(v10) >= reinterpret_cast<unsigned char>(rsi172) || (eax173 = copy_fd(*reinterpret_cast<int32_t*>(&r12_11), reinterpret_cast<unsigned char>(rsi172) - reinterpret_cast<unsigned char>(v10)), rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8), !eax173)) {
                        *reinterpret_cast<uint32_t*>(&rbx4) = 0;
                        continue;
                    } else {
                        *reinterpret_cast<int32_t*>(&rdi174) = eax173;
                        *reinterpret_cast<int32_t*>(&rdi174 + 4) = 0;
                        diagnose_copy_fd_failure(rdi174, r15_27, 0x200, rcx17, r8_12);
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        continue;
                    }
                }
                addr_32cf_166:
                if (reinterpret_cast<signed char>(rax169) < reinterpret_cast<signed char>(rsi62)) {
                    ebx175 = 1;
                    if (reinterpret_cast<signed char>(v66) < reinterpret_cast<signed char>(rsi62)) {
                        rcx17 = v66;
                        rdx35 = v10;
                        r8_12 = rsi62;
                        al176 = elide_tail_lines_seekable(r15_27, *reinterpret_cast<int32_t*>(&r12_11), rdx35, rcx17, r8_12);
                        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        ebx175 = al176;
                    }
                    *reinterpret_cast<uint32_t*>(&rbx4) = ebx175 & 1;
                }
            }
            rdx35 = v10;
            if (0) {
                eax177 = head_bytes(r15_27, *reinterpret_cast<int32_t*>(&r12_11), rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rbx4) = eax177;
            } else {
                eax178 = head_lines(r15_27, *reinterpret_cast<int32_t*>(&r12_11), rdx35, rcx17, r8_12);
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rbx4) = eax178;
            }
        }
    }
    addr_2e63_51:
    eax179 = static_cast<uint32_t>(v28) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax180) = *reinterpret_cast<unsigned char*>(&eax179);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax180) + 4) = 0;
    rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rdx13) {
        fun_2480();
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        goto addr_379e_31;
    } else {
        return rax180;
    }
}

int64_t __libc_start_main = 0;

void fun_37f3() {
    __asm__("cli ");
    __libc_start_main(0x2750, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_2390(int64_t rdi);

int64_t fun_3893() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2390(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_38d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2540(struct s0* rdi, struct s0* rsi, int64_t rdx, struct s0* rcx);

int32_t fun_2570(int64_t rdi);

int32_t fun_23d0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_26b0(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_4163(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    struct s0* r8_7;
    struct s0* rax8;
    struct s0* r8_9;
    struct s0* r12_10;
    struct s0* rax11;
    struct s0* r12_12;
    struct s0* rax13;
    struct s0* rax14;
    struct s0* r8_15;
    struct s0* r12_16;
    struct s0* rax17;
    struct s0* r12_18;
    struct s0* rax19;
    struct s0* r12_20;
    struct s0* rax21;
    struct s0* r12_22;
    struct s0* rax23;
    struct s0* r12_24;
    struct s0* rax25;
    int32_t eax26;
    struct s0* r13_27;
    struct s0* rax28;
    struct s0* r8_29;
    struct s0* rax30;
    int32_t eax31;
    struct s0* rax32;
    struct s0* r8_33;
    struct s0* rax34;
    struct s0* r8_35;
    struct s0* rax36;
    int32_t eax37;
    struct s0* rax38;
    struct s0* r8_39;
    struct s0* r15_40;
    struct s0* rax41;
    struct s0* rax42;
    struct s0* r8_43;
    struct s0* rax44;
    struct s0* rdi45;
    struct s0* r8_46;
    struct s0* r9_47;
    int64_t v48;
    int64_t v49;
    int64_t v50;
    int64_t v51;
    int64_t v52;
    int64_t v53;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2450();
            fun_2620(1, rax5, r12_2, rcx6, r8_7);
            rax8 = fun_2450();
            fun_2620(1, rax8, 10, rcx6, r8_9);
            r12_10 = stdout;
            rax11 = fun_2450();
            fun_2540(rax11, r12_10, 5, rcx6);
            r12_12 = stdout;
            rax13 = fun_2450();
            fun_2540(rax13, r12_12, 5, rcx6);
            rax14 = fun_2450();
            fun_2620(1, rax14, 10, rcx6, r8_15);
            r12_16 = stdout;
            rax17 = fun_2450();
            fun_2540(rax17, r12_16, 5, rcx6);
            r12_18 = stdout;
            rax19 = fun_2450();
            fun_2540(rax19, r12_18, 5, rcx6);
            r12_20 = stdout;
            rax21 = fun_2450();
            fun_2540(rax21, r12_20, 5, rcx6);
            r12_22 = stdout;
            rax23 = fun_2450();
            fun_2540(rax23, r12_22, 5, rcx6);
            r12_24 = stdout;
            rax25 = fun_2450();
            fun_2540(rax25, r12_24, 5, rcx6);
            do {
                if (1) 
                    break;
                eax26 = fun_2570("head");
            } while (eax26);
            r13_27 = v4;
            if (!r13_27) {
                rax28 = fun_2450();
                fun_2620(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_29);
                rax30 = fun_2610(5);
                if (!rax30 || (eax31 = fun_23d0(rax30, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax31)) {
                    rax32 = fun_2450();
                    r13_27 = reinterpret_cast<struct s0*>("head");
                    fun_2620(1, rax32, "https://www.gnu.org/software/coreutils/", "head", r8_33);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_27 = reinterpret_cast<struct s0*>("head");
                    goto addr_4550_9;
                }
            } else {
                rax34 = fun_2450();
                fun_2620(1, rax34, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_35);
                rax36 = fun_2610(5);
                if (!rax36 || (eax37 = fun_23d0(rax36, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax37)) {
                    addr_4456_11:
                    rax38 = fun_2450();
                    fun_2620(1, rax38, "https://www.gnu.org/software/coreutils/", "head", r8_39);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_27 == "head")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x9da1);
                    }
                } else {
                    addr_4550_9:
                    r15_40 = stdout;
                    rax41 = fun_2450();
                    fun_2540(rax41, r15_40, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_4456_11;
                }
            }
            rax42 = fun_2450();
            rcx6 = r12_2;
            fun_2620(1, rax42, r13_27, rcx6, r8_43);
            addr_41be_14:
            fun_2690();
        }
    } else {
        rax44 = fun_2450();
        rdi45 = stderr;
        rcx6 = r12_2;
        fun_26b0(rdi45, 1, rax44, rcx6, r8_46, r9_47, v48, v49, v50, v51, v52, v53);
        goto addr_41be_14;
    }
}

int64_t file_name = 0;

void fun_4583(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4593(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23e0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_45a3() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23c0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2450();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4633_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2630();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23e0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4633_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2630();
    }
}

uint64_t fun_4653(int32_t edi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    uint64_t r13_6;
    int32_t r12d7;
    struct s0* rbp8;
    struct s0* rbx9;
    struct s0* rdi10;
    struct s0* rax11;
    int32_t* rax12;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_6) + 4) = 0;
    } else {
        r12d7 = edi;
        rbp8 = rsi;
        rbx9 = rdx;
        *reinterpret_cast<int32_t*>(&r13_6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_6) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi10) = r12d7;
            *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
            rax11 = safe_read(rdi10, rbp8, rbx9, rcx, r8);
            if (reinterpret_cast<int1_t>(rax11 == 0xffffffffffffffff)) 
                break;
            if (!rax11) 
                goto addr_46b0_6;
            r13_6 = r13_6 + reinterpret_cast<unsigned char>(rax11);
            rbp8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax11));
            rbx9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx9) - reinterpret_cast<unsigned char>(rax11));
        } while (rbx9);
    }
    return r13_6;
    addr_46b0_6:
    rax12 = fun_23c0();
    *rax12 = 0;
    return r13_6;
}

struct s10 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_46d3(uint64_t rdi, struct s10* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

struct s11 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_4773(uint64_t rdi, struct s11* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_26a0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s12 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s12* fun_24c0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_47d3(struct s0* rdi) {
    struct s0* rcx2;
    struct s0* rbx3;
    struct s12* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26a0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23b0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23d0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5f73(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23c0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5fb3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5fd3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd220);
    }
    *rdi = esi;
    return 0xd220;
}

int64_t fun_5ff3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s13 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6033(struct s13* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s13*>(0xd220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s14 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s14* fun_6053(struct s14* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s14*>(0xd220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x270f;
    if (!rdx) 
        goto 0x270f;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd220;
}

struct s15 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_6093(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s15* r8) {
    struct s15* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s15*>(0xd220);
    }
    rax7 = fun_23c0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x60c6);
    *rax7 = r15d8;
    return rax13;
}

struct s16 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_6113(struct s0* rdi, struct s0* rsi, struct s0** rdx, struct s16* rcx) {
    struct s16* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s16*>(0xd220);
    }
    rax6 = fun_23c0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6141);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x619c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6203() {
    __asm__("cli ");
}

struct s0* gd098 = reinterpret_cast<struct s0*>(32);

int64_t slotvec0 = 0x100;

void fun_6213() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdx8;
    struct s0* rcx9;
    struct s0* r8_10;
    struct s0* rdi11;
    struct s0* rsi12;
    struct s0* rdx13;
    struct s0* rcx14;
    struct s0* r8_15;
    struct s0* rsi16;
    struct s0* rdx17;
    struct s0* rcx18;
    struct s0* r8_19;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23a0(rdi6, rsi7, rdx8, rcx9, r8_10);
        } while (rbx4 != rbp5);
    }
    rdi11 = r12_2->f8;
    if (rdi11 != 0xd120) {
        fun_23a0(rdi11, rsi12, rdx13, rcx14, r8_15);
        gd098 = reinterpret_cast<struct s0*>(0xd120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd090) {
        fun_23a0(r12_2, rsi16, rdx17, rcx18, r8_19);
        slotvec = reinterpret_cast<struct s0*>(0xd090);
    }
    nslots = 1;
    return;
}

void fun_62b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_62d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_62e3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6303(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_6323(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2715;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_63b3(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x271a;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s0* fun_6443(int32_t edi, struct s0* rsi) {
    struct s0* rax3;
    struct s2* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x271f;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2480();
    } else {
        return rax5;
    }
}

struct s0* fun_64d3(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    struct s2* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2724;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_6563(struct s0* rdi, struct s0* rsi, uint32_t edx) {
    struct s2* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6cb0]");
    __asm__("movdqa xmm1, [rip+0x6cb8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6ca1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2480();
    } else {
        return rax10;
    }
}

struct s0* fun_6603(struct s0* rdi, uint32_t esi) {
    struct s2* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6c10]");
    __asm__("movdqa xmm1, [rip+0x6c18]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6c01]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s0* fun_66a3(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b70]");
    __asm__("movdqa xmm1, [rip+0x6b78]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6b59]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2480();
    } else {
        return rax3;
    }
}

struct s0* fun_6733(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6ae0]");
    __asm__("movdqa xmm1, [rip+0x6ae8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6ad6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2480();
    } else {
        return rax4;
    }
}

struct s0* fun_67c3(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2729;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_6863(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x69aa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x69a2]");
    __asm__("movdqa xmm2, [rip+0x69aa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x272e;
    if (!rdx) 
        goto 0x272e;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s0* fun_6903(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rcx6;
    struct s2* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x690a]");
    __asm__("movdqa xmm1, [rip+0x6912]");
    __asm__("movdqa xmm2, [rip+0x691a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2733;
    if (!rdx) 
        goto 0x2733;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s0* fun_69b3(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s0* rdx4;
    struct s2* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x685a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6852]");
    __asm__("movdqa xmm2, [rip+0x685a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2738;
    if (!rsi) 
        goto 0x2738;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_6a53(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s2* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x67ba]");
    __asm__("movdqa xmm1, [rip+0x67c2]");
    __asm__("movdqa xmm2, [rip+0x67ca]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x273d;
    if (!rsi) 
        goto 0x273d;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

void fun_6af3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b03(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b43(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_2520(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_6b63(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    int32_t* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2520(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_23c0();
        if (*rax9 == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (*rax9 != 22) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

struct s17 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2580(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_6bd3(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s17* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26b0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26b0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2450();
    fun_26b0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2580(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2450();
    fun_26b0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2580(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2450();
        fun_26b0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xa048 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xa048;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7043() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s18 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_7063(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s18* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s18* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2480();
    } else {
        return;
    }
}

void fun_7103(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_71a6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_71b0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2480();
    } else {
        return;
    }
    addr_71a6_5:
    goto addr_71b0_7;
}

void fun_71e3() {
    struct s0* rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* r8_8;
    struct s0* rax9;
    struct s0* r8_10;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2580(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2450();
    fun_2620(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8);
    rax9 = fun_2450();
    fun_2620(1, rax9, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_10);
    fun_2450();
    goto fun_2620;
}

int64_t fun_2400();

void xalloc_die();

void fun_7283(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_25b0(unsigned char* rdi);

void fun_72c3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72e3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7303(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2600();

void fun_7323(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2600();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7353() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2600();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7383(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_73c3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7403(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7433(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7483(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2400();
            if (rax5) 
                break;
            addr_74cd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_74cd_5;
        rax8 = fun_2400();
        if (rax8) 
            goto addr_74b6_9;
        if (rbx4) 
            goto addr_74cd_5;
        addr_74b6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7513(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2400();
            if (rax8) 
                break;
            addr_755a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_755a_5;
        rax11 = fun_2400();
        if (rax11) 
            goto addr_7542_9;
        if (!rbx6) 
            goto addr_7542_9;
        if (r12_4) 
            goto addr_755a_5;
        addr_7542_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_75a3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_764d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7660_10:
                *r12_8 = 0;
            }
            addr_7600_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7626_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7674_14;
            if (rcx10 <= rsi9) 
                goto addr_761d_16;
            if (rsi9 >= 0) 
                goto addr_7674_14;
            addr_761d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7674_14;
            addr_7626_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2600();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7674_14;
            if (!rbp13) 
                break;
            addr_7674_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_764d_9;
        } else {
            if (!r13_6) 
                goto addr_7660_10;
            goto addr_7600_11;
        }
    }
}

int64_t fun_2560();

void fun_76a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_76d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7703() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7723() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7743(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_7783(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

struct s19 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_77c3(int64_t rdi, struct s19* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2590;
    }
}

struct s0* fun_2470(struct s0* rdi, ...);

void fun_7803(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2470(rdi);
    rax3 = fun_25b0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_7843() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2450();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2630();
    fun_23b0(rdi1);
}

int32_t xstrtoumax();

uint64_t fun_7883(int64_t rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    struct s0* rax5;
    int32_t eax6;
    int32_t* rax7;
    int32_t* r12_8;
    uint64_t v9;
    void* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax();
    if (eax6) {
        rax7 = fun_23c0();
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_7920_3:
            *r12_8 = 75;
        } else {
            if (eax6 == 3) {
                *rax7 = 0;
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2480();
            } else {
                return v9;
            }
        }
        rax11 = fun_23c0();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_7920_3;
        *rax11 = 34;
    }
    quote(rdi);
    if (*r12_8 != 22) 
        goto addr_793c_13;
    while (1) {
        addr_793c_13:
        if (!1) {
        }
        fun_2630();
    }
}

void xnumtoumax();

void fun_7993() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void fun_24e0(int64_t rdi);

void** fun_26f0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_2670(struct s0* rdi);

int64_t fun_24b0(struct s0* rdi);

int64_t fun_79c3(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* v6;
    struct s0* rax7;
    struct s0* v8;
    void* rcx9;
    struct s0* rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s0* rax13;
    struct s0* r15_14;
    void* rax15;
    int64_t rax16;
    struct s0* rbp17;
    struct s0* r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void** rax21;
    struct s0* rax22;
    int64_t rdx23;
    struct s0* rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<struct s0*>("lib/xstrtol.c");
        fun_24e0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2480();
            while (1) {
                rbx10 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_7d34_6;
                    rbx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_7d34_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<struct s0**>(&r15_14->f0) = rax13;
            if (*reinterpret_cast<struct s0**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_7a7d_12:
            *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
            addr_7a85_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_23c0();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0));
    rax21 = fun_26f0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s0*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_7abb_21;
    rsi = r15_14;
    rax24 = fun_2670(rbp17);
    r8 = *reinterpret_cast<struct s0**>(&r15_14->f0);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_24b0(r13_18), r8 = r8, rax26 == 0))) {
            addr_7abb_21:
            r12d11 = 4;
            goto addr_7a85_13;
        } else {
            addr_7af9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_24b0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xa0f8 + rbp33 * 4) + 0xa0f8;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_7abb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_7a7d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_7a7d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_24b0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_7af9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
    goto addr_7a85_13;
}

int64_t fun_23f0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7df3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23f0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_7e4e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23c0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_7e4e_3;
            rax6 = fun_23c0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s20 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    struct s0* f90;
};

int32_t fun_25a0(struct s20* rdi);

int32_t fun_25e0(struct s20* rdi);

int32_t rpl_fflush(struct s20* rdi);

int64_t fun_2430(struct s20* rdi);

int64_t fun_7e63(struct s20* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    struct s0* rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25e0(rdi);
        if (!(eax3 && (eax4 = fun_25a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5, rdi5), reinterpret_cast<int1_t>(rax6 == 0xffffffffffffffff)) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23c0();
            r12d9 = *rax8;
            rax10 = fun_2430(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2430;
}

void rpl_fseeko(struct s20* rdi);

void fun_7ef3(struct s20* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25e0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7f43(struct s20* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25d0(int64_t rdi);

signed char* fun_7fc3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25d0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_8003(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2480();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_8093() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax3;
    }
}

int64_t fun_8113(int64_t rdi, signed char* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rax6;
    int32_t r13d7;
    struct s0* rax8;
    int64_t rax9;

    __asm__("cli ");
    rax6 = fun_2610(rdi);
    if (!rax6) {
        r13d7 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax8 = fun_2470(rax6);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax8)) {
            fun_2590(rsi, rax6, &rax8->f1, rcx, r8);
            return 0;
        } else {
            r13d7 = 34;
            if (rdx) {
                fun_2590(rsi, rax6, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff, rcx, r8);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax9) = r13d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

void fun_81c3() {
    __asm__("cli ");
    goto fun_2610;
}

void fun_81d3() {
    __asm__("cli ");
}

void fun_81e7() {
    __asm__("cli ");
    return;
}

void fun_2860() {
    presume_input_pipe = 1;
    goto 0x2812;
}

struct s1* optarg = reinterpret_cast<struct s1*>(0);

void fun_28b8() {
    struct s1* rsi1;
    int1_t zf2;

    rsi1 = optarg;
    zf2 = rsi1->f0 == 45;
    if (zf2) {
        rsi1 = reinterpret_cast<struct s1*>(&rsi1->f1);
        optarg = rsi1;
    }
    string_to_integer(0, rsi1);
    goto 0x2812;
}

void fun_29b2() {
    goto 0x2812;
}

struct s21 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s22 {
    signed char[1] pad1;
    signed char f1;
};

void fun_2a20(signed char dil) {
    uint32_t r12d2;
    struct s21* rdx3;
    signed char* rcx4;
    struct s22* rcx5;

    r12d2 = rdx3->f1;
    if (*reinterpret_cast<signed char*>(&r12d2)) 
        goto 0x2998;
    if (!0) {
        line_end = 0;
    }
    *rcx4 = dil;
    if (!dil) 
        goto 0x29fb;
    rcx5->f1 = 0;
}

uint32_t fun_2530(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_26d0(int64_t rdi, struct s0* rsi);

uint32_t fun_26c0(struct s0* rdi, struct s0* rsi);

void fun_4a05() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2450();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2450();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2470(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4d03_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4d03_22; else 
                            goto addr_50fd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_51bd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5510_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4d00_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4d00_30; else 
                                goto addr_5529_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2470(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5510_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2530(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5510_28; else 
                            goto addr_4bac_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5670_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_54f0_40:
                        if (r11_27 == 1) {
                            addr_507d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5638_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4cb7_44;
                            }
                        } else {
                            goto addr_5500_46;
                        }
                    } else {
                        addr_567f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_507d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4d03_22:
                                if (v47 != 1) {
                                    addr_5259_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2470(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_52a4_54;
                                    }
                                } else {
                                    goto addr_4d10_56;
                                }
                            } else {
                                addr_4cb5_57:
                                ebp36 = 0;
                                goto addr_4cb7_44;
                            }
                        } else {
                            addr_54e4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_567f_47; else 
                                goto addr_54ee_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_507d_41;
                        if (v47 == 1) 
                            goto addr_4d10_56; else 
                            goto addr_5259_52;
                    }
                }
                addr_4d71_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4c08_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_4c2d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4f30_65;
                    } else {
                        addr_4d99_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_55e8_67;
                    }
                } else {
                    goto addr_4d90_69;
                }
                addr_4c41_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_4c8c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_55e8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4c8c_81;
                }
                addr_4d90_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_4c2d_64; else 
                    goto addr_4d99_66;
                addr_4cb7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_4d6f_91;
                if (v22) 
                    goto addr_4ccf_93;
                addr_4d6f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4d71_62;
                addr_52a4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_5a2b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_5a9b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_589f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26d0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26c0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_539e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_4d5c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_53a8_112;
                    }
                } else {
                    addr_53a8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5479_114;
                }
                addr_4d68_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4d6f_91;
                while (1) {
                    addr_5479_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_5987_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_53e6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_5995_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5467_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5467_128;
                        }
                    }
                    addr_5415_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5467_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_53e6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5415_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4c8c_81;
                addr_5995_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_55e8_67;
                addr_5a2b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_539e_109;
                addr_5a9b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_539e_109;
                addr_4d10_56:
                rax93 = fun_26f0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_4d5c_110;
                addr_54ee_59:
                goto addr_54f0_40;
                addr_51bd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4d03_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4d68_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4cb5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4d03_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5202_160;
                if (!v22) 
                    goto addr_55d7_162; else 
                    goto addr_57e3_163;
                addr_5202_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_55d7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_55e8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_50ab_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4f13_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4c41_70; else 
                    goto addr_4f27_169;
                addr_50ab_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4c08_63;
                goto addr_4d90_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_54e4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_561f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4d00_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4bf8_178; else 
                        goto addr_55a2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_54e4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4d03_22;
                }
                addr_561f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4d00_30:
                    r8d42 = 0;
                    goto addr_4d03_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4d71_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5638_42;
                    }
                }
                addr_4bf8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4c08_63;
                addr_55a2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5500_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4d71_62;
                } else {
                    addr_55b2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4d03_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5d62_188;
                if (v28) 
                    goto addr_55d7_162;
                addr_5d62_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4f13_168;
                addr_4bac_37:
                if (v22) 
                    goto addr_5ba3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4bc3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5670_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_56fb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4d03_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4bf8_178; else 
                        goto addr_56d7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_54e4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4d03_22;
                }
                addr_56fb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4d03_22;
                }
                addr_56d7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5500_46;
                goto addr_55b2_186;
                addr_4bc3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4d03_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4d03_22; else 
                    goto addr_4bd4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_5cae_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5b34_210;
            if (1) 
                goto addr_5b32_212;
            if (!v29) 
                goto addr_576e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_5ca1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4f30_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_4ceb_219; else 
            goto addr_4f4a_220;
        addr_4ccf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4ce3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4f4a_220; else 
            goto addr_4ceb_219;
        addr_589f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4f4a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_58bd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_5d30_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5796_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_5987_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4ce3_221;
        addr_57e3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4ce3_221;
        addr_4f27_169:
        goto addr_4f30_65;
        addr_5cae_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4f4a_220;
        goto addr_58bd_222;
        addr_5b34_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_5b8e_236;
        fun_2480();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5d30_225;
        addr_5b32_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5b34_210;
        addr_576e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5b34_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_5796_226;
        }
        addr_5ca1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_50fd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9b0c + rax113 * 4) + 0x9b0c;
    addr_5529_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9c0c + rax114 * 4) + 0x9c0c;
    addr_5ba3_190:
    addr_4ceb_219:
    goto 0x49d0;
    addr_4bd4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9a0c + rax115 * 4) + 0x9a0c;
    addr_5b8e_236:
    goto v116;
}

void fun_4bf0() {
}

void fun_4da8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4aa2;
}

void fun_4e01() {
    goto 0x4aa2;
}

void fun_4eee() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4d71;
    }
    if (v2) 
        goto 0x57e3;
    if (!r10_3) 
        goto addr_594e_5;
    if (!v4) 
        goto addr_581e_7;
    addr_594e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_581e_7:
    goto 0x4c24;
}

void fun_4f0c() {
}

void fun_4fb7() {
    signed char v1;

    if (v1) {
        goto 0x4f3f;
    } else {
        goto 0x4c7a;
    }
}

void fun_4fd1() {
    signed char v1;

    if (!v1) 
        goto 0x4fca; else 
        goto "???";
}

void fun_4ff8() {
    goto 0x4f13;
}

void fun_5078() {
}

void fun_5090() {
}

void fun_50bf() {
    goto 0x4f13;
}

void fun_5111() {
    goto 0x50a0;
}

void fun_5140() {
    goto 0x50a0;
}

void fun_5173() {
    goto 0x50a0;
}

void fun_5540() {
    goto 0x4bf8;
}

void fun_583e() {
    signed char v1;

    if (v1) 
        goto 0x57e3;
    goto 0x4c24;
}

void fun_58e5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4c24;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4c08;
        goto 0x4c24;
    }
}

void fun_5d02() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4f70;
    } else {
        goto 0x4aa2;
    }
}

void fun_6ca8() {
    fun_2450();
}

void fun_7b6c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x7b7b;
}

void fun_7c3c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x7c49;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x7b7b;
}

void fun_7c60() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_7c8c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x7b7b;
}

void fun_7cad() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x7b7b;
}

void fun_7cd1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x7b7b;
}

void fun_7cf5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7c84;
}

void fun_7d19() {
}

void fun_7d39() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7c84;
}

void fun_7d55() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7c84;
}

void fun_2870() {
    line_end = 0;
    goto 0x2812;
}

void fun_29bd() {
    goto 0x2812;
}

void fun_2a28() {
    goto 0x29d0;
}

void fun_4e2e() {
    goto 0x4aa2;
}

void fun_5004() {
    goto 0x4fbc;
}

void fun_50cb() {
    goto 0x4bf8;
}

void fun_511d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x50a0;
    goto 0x4ccf;
}

void fun_514f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x50ab;
        goto 0x4ad0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4f4a;
        goto 0x4ceb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x58e8;
    if (r10_8 > r15_9) 
        goto addr_5035_9;
    addr_503a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x58f3;
    goto 0x4c24;
    addr_5035_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_503a_10;
}

void fun_5182() {
    goto 0x4cb7;
}

void fun_5550() {
    goto 0x4cb7;
}

void fun_5cef() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4e0c;
    } else {
        goto 0x4f70;
    }
}

void fun_6d60() {
}

void fun_7c0f() {
    if (__intrinsic()) 
        goto 0x7c49; else 
        goto "???";
}

void fun_2880() {
    struct s1* rsi1;
    int1_t zf2;

    rsi1 = optarg;
    zf2 = rsi1->f0 == 45;
    if (zf2) {
        rsi1 = reinterpret_cast<struct s1*>(&rsi1->f1);
        optarg = rsi1;
    }
    string_to_integer(1, rsi1);
    goto 0x2812;
}

void fun_29c8() {
}

void fun_2a30() {
    goto 0x29d0;
}

void fun_518c() {
    goto 0x5127;
}

void fun_555a() {
    goto 0x507d;
}

void fun_6dc0() {
    fun_2450();
    goto fun_26b0;
}

void fun_2a37() {
    goto 0x29d0;
}

void fun_4e5d() {
    goto 0x4aa2;
}

void fun_5198() {
    goto 0x5127;
}

void fun_5567() {
    goto 0x50ce;
}

void fun_6e00() {
    fun_2450();
    goto fun_26b0;
}

void fun_2a3f() {
    goto 0x29d0;
}

void fun_4e8a() {
    goto 0x4aa2;
}

void fun_51a4() {
    goto 0x50a0;
}

void fun_6e40() {
    fun_2450();
    goto fun_26b0;
}

void fun_4eac() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5840;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4d71;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4d71;
    }
    if (v11) 
        goto 0x5ba3;
    if (r10_12 > r15_13) 
        goto addr_5bf3_8;
    addr_5bf8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5931;
    addr_5bf3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5bf8_9;
}

struct s23 {
    signed char[24] pad24;
    int64_t f18;
};

struct s24 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s25 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_6e90() {
    int64_t r15_1;
    struct s23* rbx2;
    struct s0* r14_3;
    struct s24* rbx4;
    struct s0* r13_5;
    struct s25* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2450();
    fun_26b0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6eb2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6ee8() {
    fun_2450();
    goto 0x6eb9;
}

struct s26 {
    signed char[32] pad32;
    int64_t f20;
};

struct s27 {
    signed char[24] pad24;
    int64_t f18;
};

struct s28 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s29 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s30 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6f20() {
    int64_t rcx1;
    struct s26* rbx2;
    int64_t r15_3;
    struct s27* rbx4;
    struct s0* r14_5;
    struct s28* rbx6;
    struct s0* r13_7;
    struct s29* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s30* rbx12;
    struct s0* rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2450();
    fun_26b0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6f54, __return_address(), rcx1);
    goto v15;
}

void fun_6f98() {
    fun_2450();
    goto 0x6f5b;
}
