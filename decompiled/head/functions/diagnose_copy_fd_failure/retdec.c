int64_t diagnose_copy_fd_failure(int64_t a1, int64_t a2) {
    int32_t v1 = a1; // 0x3948
    if (v1 == 1) {
        // 0x394d
        quotearg_style();
        // 0x3966
        function_2450();
        function_23c0();
        return function_2630();
    }
    if (v1 != 2) {
        // 0x39b5
        return diagnose_copy_fd_failure_cold();
    }
    // 0x3995
    quotearg_n_style_colon();
    // 0x3966
    function_2450();
    function_23c0();
    return function_2630();
}