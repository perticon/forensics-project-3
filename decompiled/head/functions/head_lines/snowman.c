uint32_t head_lines(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rsi2;
    void* rsp6;
    struct s0* rax7;
    struct s0* v8;
    uint32_t eax9;
    struct s0* r14_10;
    struct s0* r13_11;
    struct s0* rbx12;
    struct s0* r12_13;
    void* r15_14;
    struct s0* rax15;
    uint32_t eax16;
    struct s0* rbp17;
    void* rdx18;
    struct s0* v19;
    void* rbx20;
    int32_t r13d21;
    struct s0* v22;
    int64_t rdi23;
    struct s0* rax24;
    struct s0* v25;
    void* rax26;
    void* rdx27;
    void* rax28;
    void* rax29;
    struct s0* rax30;
    struct s0* rdx31;
    struct s0* rbp32;
    struct s0* rax33;
    void* rsp34;
    struct s0* r15_35;
    struct s0* rdx36;
    struct s0* rdi37;
    struct s0* rax38;
    struct s0* r8_39;
    int32_t r12d40;
    void* r14_41;
    void* rax42;
    int64_t rsi43;
    struct s0* rax44;
    int64_t rdi45;
    struct s0* rax46;
    struct s0* rdi47;
    struct s0* rax48;
    void* rdx49;
    int64_t v50;
    int64_t rdi51;
    struct s0* rax52;
    int32_t eax53;
    struct s0* rdi54;
    struct s0* rsi55;
    int64_t rdi56;
    struct s0* rax57;
    struct s0* rax58;
    int32_t* rax59;
    struct s0* rdx60;
    int64_t rdi61;
    struct s0* rax62;
    struct s0* rsp63;
    int64_t rdi64;
    int32_t eax65;
    uint32_t v66;
    int64_t rdi67;
    struct s0* rax68;

    *reinterpret_cast<int32_t*>(&rsi2) = esi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax7 = g28;
    v8 = rax7;
    if (!rdx) {
        addr_3e55_2:
        eax9 = 1;
    } else {
        r14_10 = rdi;
        *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
        rbx12 = rdx;
        r12_13 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp6) + 0x90);
        r15_14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) + 0x8f);
        while (*reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax15 = safe_read(rdi, r12_13, 0x2000, rcx, r8), rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8), rsi2 = rax15, rax15 != 0xffffffffffffffff) {
            if (!rax15) 
                goto addr_3e55_2;
            eax16 = line_end;
            *reinterpret_cast<int32_t*>(&rbp17) = 0;
            *reinterpret_cast<int32_t*>(&rbp17 + 4) = 0;
            do {
                rbp17 = reinterpret_cast<struct s0*>(&rbp17->f1);
                if (*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r15_14) + reinterpret_cast<unsigned char>(rbp17)) != *reinterpret_cast<signed char*>(&eax16)) 
                    continue;
                rbx12 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx12) - 1);
                if (!rbx12) 
                    goto addr_3d5d_9;
            } while (rsi2 != rbp17);
            xwrite_stdout_part_0(r12_13, rsi2, 0x2000, rcx, r8);
            rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8);
        }
        goto addr_3e12_12;
    }
    addr_3d88_13:
    rdx18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx18) {
        return eax9;
    }
    fun_2480();
    v19 = rdi;
    rbx20 = rdx18;
    r13d21 = *reinterpret_cast<int32_t*>(&rsi2);
    v22 = rcx;
    *reinterpret_cast<int32_t*>(&rdi23) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
    rax24 = g28;
    v25 = rax24;
    rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax26) >> 63) >> 51);
    rax28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax26) + reinterpret_cast<uint64_t>(rdx27));
    *reinterpret_cast<uint32_t*>(&rax29) = *reinterpret_cast<uint32_t*>(&rax28) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
    rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax29) - reinterpret_cast<uint64_t>(rdx27));
    *reinterpret_cast<int32_t*>(&rdx31) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
    if (!rax30) 
        goto addr_3edd_17;
    rdx31 = rax30;
    addr_3edd_17:
    rbp32 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx31));
    rax33 = fun_24d0(rdi23, rdi23);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax33) < reinterpret_cast<signed char>(0)) {
        addr_4060_19:
        elseek_part_0();
    } else {
        r15_35 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp34) + 32);
        rdx36 = rdx31;
        *reinterpret_cast<int32_t*>(&rdi37) = r13d21;
        *reinterpret_cast<int32_t*>(&rdi37 + 4) = 0;
        rax38 = safe_read(rdi37, r15_35, rdx36, rcx, r8);
        r8_39 = rax38;
        if (rax38 == 0xffffffffffffffff) {
            addr_40cc_21:
            quotearg_style(4, v19, rdx36, rcx, r8_39);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d40 = reinterpret_cast<signed char>(line_end);
            r14_41 = rbx20;
            if (rbx20 && (r8_39 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 + reinterpret_cast<unsigned char>(r8_39) + 31) != *reinterpret_cast<signed char*>(&r12d40))) {
                r14_41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx20) + 0xffffffffffffffff);
                if (r8_39) 
                    goto addr_3fb5_24;
                goto addr_3f48_26;
            }
            while (1) {
                if (!r8_39) 
                    goto addr_3f48_26;
                addr_3fb5_24:
                if (!rbx20) {
                    r8_39 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_39) - 1);
                    rax42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_41) + 0xffffffffffffffff);
                    if (!r14_41) 
                        goto addr_3fe0_29;
                } else {
                    rdx36 = r8_39;
                    *reinterpret_cast<int32_t*>(&rsi43) = r12d40;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi43) + 4) = 0;
                    rax44 = fun_2640(r15_35, rsi43, rdx36);
                    r8_39 = rax44;
                    if (!rax44) {
                        addr_3f48_26:
                        if (rbp32 == v22) 
                            goto addr_4142_31; else 
                            goto addr_3f53_32;
                    } else {
                        r8_39 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_39) - reinterpret_cast<unsigned char>(r15_35));
                        rax42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_41) + 0xffffffffffffffff);
                        if (!r14_41) 
                            goto addr_3fe0_29;
                    }
                }
                r14_41 = rax42;
                continue;
                addr_3f53_32:
                rbp32 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp32) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi45) = r13d21;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi45) + 4) = 0;
                rax46 = fun_24d0(rdi45, rdi45);
                if (reinterpret_cast<signed char>(rax46) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_19;
                *reinterpret_cast<int32_t*>(&rdx36) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi47) = r13d21;
                *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0;
                rax48 = safe_read(rdi47, r15_35, 0x2000, rcx, r8_39);
                r8_39 = rax48;
                if (rax48 == 0xffffffffffffffff) 
                    goto addr_40cc_21;
                if (!r8_39) 
                    goto addr_4142_31;
                r12d40 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_38:
    rdx49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(g28));
    if (rdx49) {
        fun_2480();
    } else {
        goto v50;
    }
    addr_4142_31:
    goto addr_4071_38;
    addr_3fe0_29:
    if (reinterpret_cast<signed char>(rbp32) <= reinterpret_cast<signed char>(v22)) 
        goto addr_4029_42;
    *reinterpret_cast<int32_t*>(&rdx36) = 0;
    *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi51) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi51) + 4) = 0;
    rax52 = fun_24d0(rdi51);
    if (reinterpret_cast<signed char>(rax52) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_38;
    } else {
        eax53 = copy_fd(r13d21, reinterpret_cast<unsigned char>(rbp32) - reinterpret_cast<unsigned char>(v22));
        r8_39 = r8_39;
        if (eax53) {
            *reinterpret_cast<int32_t*>(&rdi54) = eax53;
            *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
            diagnose_copy_fd_failure(rdi54, v19, 0, rcx, r8_39);
            goto addr_4071_38;
        } else {
            addr_4029_42:
            rsi55 = reinterpret_cast<struct s0*>(&r8_39->f1);
            if (rsi55) {
                xwrite_stdout_part_0(r15_35, rsi55, rdx36, rcx, r8_39);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi56) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi56) + 4) = 0;
    rax57 = fun_24d0(rdi56, rdi56);
    if (reinterpret_cast<signed char>(rax57) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_38;
    addr_3e12_12:
    rax58 = quotearg_style(4, r14_10, 0x2000, rcx, r8);
    fun_2450();
    rax59 = fun_23c0();
    rcx = rax58;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax59;
    fun_2630();
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax9 = 0;
    goto addr_3d88_13;
    addr_3d5d_9:
    *reinterpret_cast<int32_t*>(&rdx60) = 1;
    *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi61) = *reinterpret_cast<int32_t*>(&r13_11);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
    rax62 = fun_24d0(rdi61, rdi61);
    rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp6) - 8 + 8);
    if (reinterpret_cast<signed char>(rax62) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi64) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi64) + 4) = 0, eax65 = fun_26e0(rdi64, rsp63, 1, rcx, r8), rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8), !!eax65) || reinterpret_cast<int1_t>((v66 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx60) = 1, *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi67) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0, rax68 = fun_24d0(rdi67, rdi67), rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8), reinterpret_cast<signed char>(rax68) < reinterpret_cast<signed char>(0)))) {
        rdx60 = r14_10;
        elseek_part_0();
        rsp63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8);
    }
    rsi2 = rbp17;
    rdi = r12_13;
    xwrite_stdout_part_0(rdi, rsi2, rdx60, rcx, r8);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp63) - 8 + 8);
    eax9 = 1;
    goto addr_3d88_13;
}