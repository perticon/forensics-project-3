bool head_lines(char * filename, int32_t fd, int64_t lines_to_write) {
    int64_t v1 = __readfsqword(40); // 0x3cd9
    int64_t v2 = 1; // 0x3cef
    int64_t v3; // 0x3d4d
    int64_t v4; // 0x3cb0
    int64_t v5; // 0x3d1b
    if (lines_to_write != 0) {
        // 0x3cf5
        int64_t v6; // bp-8264, 0x3cb0
        v4 = &v6;
        int64_t v7 = lines_to_write; // 0x3d0e
        while (true) {
          lab_0x3d10:
            // 0x3d10
            v5 = safe_read(fd, (char *)&v6, 0x2000);
            v2 = 1;
            switch (v5) {
                case -1: {
                    goto lab_0x3e12;
                }
                case 0: {
                    goto lab_0x3d88;
                }
                default: {
                    int64_t v8 = v7;
                    v3 = 1;
                    int64_t v9 = v8; // 0x3d55
                    int64_t v10; // bp-8265, 0x3cb0
                    int32_t v11; // 0x3cb0
                    if (*(char *)(v3 + (int64_t)&v10) == line_end) {
                        // 0x3d57
                        v9 = v8 - 1;
                        if (v9 == 0) {
                            // 0x3d5d
                            if (function_24d0() < 0) {
                                // 0x3dc5
                                if ((int32_t)function_26e0() != 0) {
                                    goto lab_0x3de4;
                                } else {
                                    if ((v11 & 0xf000) != 0x8000) {
                                        // 0x3d78
                                        xwrite_stdout_part_0(v4, v3);
                                        v2 = 1;
                                        goto lab_0x3d88;
                                    } else {
                                        goto lab_0x3de4;
                                    }
                                }
                            } else {
                                // 0x3d78
                                xwrite_stdout_part_0(v4, v3);
                                v2 = 1;
                                goto lab_0x3d88;
                            }
                        }
                    }
                    int64_t v12 = v9;
                    int64_t v13 = v3; // 0x3d4b
                    while (v3 != v5) {
                        // 0x3d4d
                        v8 = v12;
                        v3 = v13 + 1;
                        v9 = v8;
                        if (*(char *)(v3 + (int64_t)&v10) == line_end) {
                            // 0x3d57
                            v9 = v8 - 1;
                            if (v9 == 0) {
                                // 0x3d5d
                                if (function_24d0() < 0) {
                                    // 0x3dc5
                                    if ((int32_t)function_26e0() != 0) {
                                        goto lab_0x3de4;
                                    } else {
                                        if ((v11 & 0xf000) != 0x8000) {
                                            // 0x3d78
                                            xwrite_stdout_part_0(v4, v3);
                                            v2 = 1;
                                            goto lab_0x3d88;
                                        } else {
                                            goto lab_0x3de4;
                                        }
                                    }
                                } else {
                                    // 0x3d78
                                    xwrite_stdout_part_0(v4, v3);
                                    v2 = 1;
                                    goto lab_0x3d88;
                                }
                            }
                        }
                        // 0x3d48
                        v12 = v9;
                        v13 = v3;
                    }
                    // 0x3db8
                    xwrite_stdout_part_0(v4, v5);
                    v7 = v12;
                    goto lab_0x3d10;
                }
            }
        }
      lab_0x3e12:
        // 0x3e12
        quotearg_style();
        function_2450();
        function_23c0();
        function_2630();
        v2 = 0;
    }
    goto lab_0x3d88;
  lab_0x3d88:
    // 0x3d88
    if (v1 != __readfsqword(40)) {
        // 0x3e5f
        return function_2480() % 2 != 0;
    }
    // 0x3d9f
    return v2 != 0;
  lab_0x3de4:
    // 0x3de4
    if (function_24d0() >= 0) {
        // 0x3d78
        xwrite_stdout_part_0(v4, v3);
        v2 = 1;
        goto lab_0x3d88;
    } else {
        // 0x3dfd
        elseek_part_0(v3 - v5, 1, (int64_t)filename);
        // 0x3d78
        xwrite_stdout_part_0(v4, v3);
        v2 = 1;
        goto lab_0x3d88;
    }
}