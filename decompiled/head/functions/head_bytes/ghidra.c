undefined8 head_bytes(undefined8 param_1,undefined4 param_2,ulong param_3)

{
  long lVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  ulong uVar5;
  long in_FS_OFFSET;
  undefined auStack8264 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_3 != 0) {
    uVar5 = 0x2000;
    do {
      if (param_3 < uVar5) {
        uVar5 = param_3;
      }
      lVar1 = safe_read(param_2,auStack8264,uVar5);
      if (lVar1 == -1) {
        uVar2 = quotearg_style(4,param_1);
        uVar3 = dcgettext(0,"error reading %s",5);
        piVar4 = __errno_location();
        error(0,*piVar4,uVar3,uVar2);
        uVar2 = 0;
        goto LAB_00103c7d;
      }
      if (lVar1 == 0) break;
      xwrite_stdout_part_0(auStack8264,lVar1);
      param_3 = param_3 - lVar1;
    } while (param_3 != 0);
  }
  uVar2 = 1;
LAB_00103c7d:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}