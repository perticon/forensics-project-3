bool head_bytes(char * filename, int32_t fd, int64_t bytes_to_write) {
    int64_t v1 = __readfsqword(40); // 0x3bc6
    int64_t v2 = 1; // 0x3bdc
    if (bytes_to_write != 0) {
        uint64_t v3 = 0x2000;
        uint64_t v4 = bytes_to_write;
        int64_t v5 = v3 > v4 ? v4 : v3; // 0x3c1e
        int64_t v6; // bp-8264, 0x3ba0
        int64_t v7 = safe_read(fd, (char *)&v6, v5); // 0x3c25
        while (v7 != -1) {
            // 0x3c00
            v2 = 1;
            if (v7 == 0) {
                goto lab_0x3c7d_2;
            }
            // 0x3c05
            xwrite_stdout_part_0((int64_t)&v6, v7);
            int64_t v8 = v4 - v7; // 0x3c10
            v2 = 1;
            if (v8 == 0) {
                goto lab_0x3c7d_2;
            }
            v3 = v5;
            v4 = v8;
            v5 = v3 > v4 ? v4 : v3;
            v7 = safe_read(fd, (char *)&v6, v5);
        }
        // 0x3c33
        quotearg_style();
        function_2450();
        function_23c0();
        function_2630();
        v2 = 0;
    }
  lab_0x3c7d_2:
    // 0x3c7d
    if (v1 != __readfsqword(40)) {
        // 0x3ca2
        return function_2480() % 2 != 0;
    }
    // 0x3c90
    return v2 != 0;
}