uint32_t head_bytes(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rsi2;
    struct s0* rsp6;
    struct s0* rax7;
    struct s0* v8;
    uint32_t eax9;
    struct s0* r14_10;
    struct s0* r13_11;
    struct s0* rbp12;
    struct s0* r15_13;
    struct s0* r12_14;
    struct s0* rax15;
    struct s0* rdx16;
    void* rsp17;
    struct s0* rax18;
    struct s0* v19;
    void* rdx20;
    int64_t v21;
    struct s0* v22;
    void* rbx23;
    int32_t r13d24;
    struct s0* v25;
    int64_t rdi26;
    struct s0* rax27;
    struct s0* v28;
    void* rax29;
    void* rdx30;
    void* rax31;
    void* rax32;
    struct s0* rax33;
    struct s0* rdx34;
    struct s0* rbp35;
    struct s0* rax36;
    void* rsp37;
    struct s0* r15_38;
    struct s0* rdx39;
    struct s0* rdi40;
    struct s0* rax41;
    struct s0* r8_42;
    int32_t r12d43;
    void* r14_44;
    void* rax45;
    int64_t rsi46;
    struct s0* rax47;
    int64_t rdi48;
    struct s0* rax49;
    struct s0* rdi50;
    struct s0* rax51;
    void* rdx52;
    int64_t v53;
    int64_t rdi54;
    struct s0* rax55;
    int32_t eax56;
    struct s0* rdi57;
    struct s0* rsi58;
    int64_t rdi59;
    struct s0* rax60;
    struct s0* rbx61;
    struct s0* rax62;
    uint32_t eax63;
    struct s0* rax64;
    int32_t* rax65;
    struct s0* rdx66;
    int64_t rdi67;
    struct s0* rax68;
    struct s0* rsp69;
    int64_t rdi70;
    int32_t eax71;
    uint32_t v72;
    int64_t rdi73;
    struct s0* rax74;
    struct s0* rax75;
    int32_t* rax76;

    *reinterpret_cast<int32_t*>(&rsi2) = esi;
    rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax7 = g28;
    v8 = rax7;
    if (!rdx) {
        addr_3c78_2:
        eax9 = 1;
    } else {
        r14_10 = rdi;
        *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
        rbp12 = rdx;
        *reinterpret_cast<int32_t*>(&r15_13) = 0x2000;
        *reinterpret_cast<int32_t*>(&r15_13 + 4) = 0;
        r12_14 = rsp6;
        do {
            rsi2 = r12_14;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            if (reinterpret_cast<unsigned char>(r15_13) > reinterpret_cast<unsigned char>(rbp12)) {
                r15_13 = rbp12;
            }
            rax15 = safe_read(rdi, rsi2, r15_13, rcx, r8);
            rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8);
            if (reinterpret_cast<int1_t>(rax15 == 0xffffffffffffffff)) 
                goto addr_3c33_7;
        } while (rax15 && (rsi2 = rax15, rdi = r12_14, xwrite_stdout_part_0(rdi, rsi2, r15_13, rcx, r8), rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8), rbp12 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp12) - reinterpret_cast<unsigned char>(rax15)), !!rbp12));
        goto addr_3c78_2;
    }
    addr_3c7d_9:
    rdx16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx16) {
        return eax9;
    }
    fun_2480();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax18 = g28;
    v19 = rax18;
    if (rdx16) 
        goto addr_3cf5_13;
    addr_3e55_14:
    addr_3d88_15:
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        goto v21;
    }
    fun_2480();
    v22 = rdi;
    rbx23 = rdx20;
    r13d24 = *reinterpret_cast<int32_t*>(&rsi2);
    v25 = rcx;
    *reinterpret_cast<int32_t*>(&rdi26) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rax27 = g28;
    v28 = rax27;
    rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax29) >> 63) >> 51);
    rax31 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax29) + reinterpret_cast<uint64_t>(rdx30));
    *reinterpret_cast<uint32_t*>(&rax32) = *reinterpret_cast<uint32_t*>(&rax31) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
    rax33 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax32) - reinterpret_cast<uint64_t>(rdx30));
    *reinterpret_cast<int32_t*>(&rdx34) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0;
    if (!rax33) 
        goto addr_3edd_19;
    rdx34 = rax33;
    addr_3edd_19:
    rbp35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx34));
    rax36 = fun_24d0(rdi26, rdi26);
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax36) < reinterpret_cast<signed char>(0)) {
        addr_4060_21:
        elseek_part_0();
    } else {
        r15_38 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp37) + 32);
        rdx39 = rdx34;
        *reinterpret_cast<int32_t*>(&rdi40) = r13d24;
        *reinterpret_cast<int32_t*>(&rdi40 + 4) = 0;
        rax41 = safe_read(rdi40, r15_38, rdx39, rcx, r8);
        r8_42 = rax41;
        if (rax41 == 0xffffffffffffffff) {
            addr_40cc_23:
            quotearg_style(4, v22, rdx39, rcx, r8_42);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d43 = reinterpret_cast<signed char>(line_end);
            r14_44 = rbx23;
            if (rbx23 && (r8_42 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp37) - 8 + 8 + reinterpret_cast<unsigned char>(r8_42) + 31) != *reinterpret_cast<signed char*>(&r12d43))) {
                r14_44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx23) + 0xffffffffffffffff);
                if (r8_42) 
                    goto addr_3fb5_26;
                goto addr_3f48_28;
            }
            while (1) {
                if (!r8_42) 
                    goto addr_3f48_28;
                addr_3fb5_26:
                if (!rbx23) {
                    r8_42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_42) - 1);
                    rax45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_44) + 0xffffffffffffffff);
                    if (!r14_44) 
                        goto addr_3fe0_31;
                } else {
                    rdx39 = r8_42;
                    *reinterpret_cast<int32_t*>(&rsi46) = r12d43;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    rax47 = fun_2640(r15_38, rsi46, rdx39);
                    r8_42 = rax47;
                    if (!rax47) {
                        addr_3f48_28:
                        if (rbp35 == v25) 
                            goto addr_4142_33; else 
                            goto addr_3f53_34;
                    } else {
                        r8_42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_42) - reinterpret_cast<unsigned char>(r15_38));
                        rax45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_44) + 0xffffffffffffffff);
                        if (!r14_44) 
                            goto addr_3fe0_31;
                    }
                }
                r14_44 = rax45;
                continue;
                addr_3f53_34:
                rbp35 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp35) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi48) = r13d24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi48) + 4) = 0;
                rax49 = fun_24d0(rdi48, rdi48);
                if (reinterpret_cast<signed char>(rax49) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_21;
                *reinterpret_cast<int32_t*>(&rdx39) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi50) = r13d24;
                *reinterpret_cast<int32_t*>(&rdi50 + 4) = 0;
                rax51 = safe_read(rdi50, r15_38, 0x2000, rcx, r8_42);
                r8_42 = rax51;
                if (rax51 == 0xffffffffffffffff) 
                    goto addr_40cc_23;
                if (!r8_42) 
                    goto addr_4142_33;
                r12d43 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_40:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (rdx52) {
        fun_2480();
    } else {
        goto v53;
    }
    addr_4142_33:
    goto addr_4071_40;
    addr_3fe0_31:
    if (reinterpret_cast<signed char>(rbp35) <= reinterpret_cast<signed char>(v25)) 
        goto addr_4029_44;
    *reinterpret_cast<int32_t*>(&rdx39) = 0;
    *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi54) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
    rax55 = fun_24d0(rdi54);
    if (reinterpret_cast<signed char>(rax55) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_40;
    } else {
        eax56 = copy_fd(r13d24, reinterpret_cast<unsigned char>(rbp35) - reinterpret_cast<unsigned char>(v25));
        r8_42 = r8_42;
        if (eax56) {
            *reinterpret_cast<int32_t*>(&rdi57) = eax56;
            *reinterpret_cast<int32_t*>(&rdi57 + 4) = 0;
            diagnose_copy_fd_failure(rdi57, v22, 0, rcx, r8_42);
            goto addr_4071_40;
        } else {
            addr_4029_44:
            rsi58 = reinterpret_cast<struct s0*>(&r8_42->f1);
            if (rsi58) {
                xwrite_stdout_part_0(r15_38, rsi58, rdx39, rcx, r8_42);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi59) = r13d24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi59) + 4) = 0;
    rax60 = fun_24d0(rdi59, rdi59);
    if (reinterpret_cast<signed char>(rax60) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_40;
    addr_3cf5_13:
    r14_10 = rdi;
    *reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&rsi2);
    rbx61 = rdx16;
    r12_14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) + 0x90);
    r15_13 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax62 = safe_read(rdi, r12_14, 0x2000, rcx, r8), rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8), rsi2 = rax62, rax62 != 0xffffffffffffffff) {
        if (!rax62) 
            goto addr_3e55_14;
        eax63 = line_end;
        *reinterpret_cast<int32_t*>(&rbp12) = 0;
        *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
        do {
            rbp12 = reinterpret_cast<struct s0*>(&rbp12->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_13) + reinterpret_cast<unsigned char>(rbp12)) != *reinterpret_cast<signed char*>(&eax63)) 
                continue;
            rbx61 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx61) - 1);
            if (!rbx61) 
                goto addr_3d5d_58;
        } while (rsi2 != rbp12);
        xwrite_stdout_part_0(r12_14, rsi2, 0x2000, rcx, r8);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8);
    }
    rax64 = quotearg_style(4, r14_10, 0x2000, rcx, r8);
    fun_2450();
    rax65 = fun_23c0();
    rcx = rax64;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax65;
    fun_2630();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_15;
    addr_3d5d_58:
    *reinterpret_cast<int32_t*>(&rdx66) = 1;
    *reinterpret_cast<int32_t*>(&rdx66 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi67) = *reinterpret_cast<int32_t*>(&r13_11);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
    rax68 = fun_24d0(rdi67, rdi67);
    rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp17) - 8 + 8);
    if (reinterpret_cast<signed char>(rax68) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi70) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0, eax71 = fun_26e0(rdi70, rsp69, 1, rcx, r8), rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8), !!eax71) || reinterpret_cast<int1_t>((v72 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx66) = 1, *reinterpret_cast<int32_t*>(&rdx66 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi73) = *reinterpret_cast<int32_t*>(&r13_11), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0, rax74 = fun_24d0(rdi73, rdi73), rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8), reinterpret_cast<signed char>(rax74) < reinterpret_cast<signed char>(0)))) {
        rdx66 = r14_10;
        elseek_part_0();
        rsp69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8);
    }
    rsi2 = rbp12;
    rdi = r12_14;
    xwrite_stdout_part_0(rdi, rsi2, rdx66, rcx, r8);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp69) - 8 + 8);
    goto addr_3d88_15;
    addr_3c33_7:
    rax75 = quotearg_style(4, r14_10, r15_13, rcx, r8);
    fun_2450();
    rax76 = fun_23c0();
    rcx = rax75;
    *reinterpret_cast<int32_t*>(&rdi) = 0;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi2) = *rax76;
    fun_2630();
    rsp6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    eax9 = 0;
    goto addr_3c7d_9;
}