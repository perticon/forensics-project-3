elide_tail_lines_file (char const *filename, int fd, uintmax_t n_elide,
                       struct stat const *st, off_t current_pos)
{
  off_t size = st->st_size;
  if (presume_input_pipe || current_pos < 0 || size <= ST_BLKSIZE (*st))
    return elide_tail_lines_pipe (filename, fd, n_elide, current_pos);
  else
    {
      /* Find the offset, OFF, of the Nth newline from the end,
         but not counting the last byte of the file.
         If found, write from current position to OFF, inclusive.
         Otherwise, just return true.  */

      return (size <= current_pos
              || elide_tail_lines_seekable (filename, fd, n_elide,
                                            current_pos, size));
    }
}