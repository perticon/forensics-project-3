elide_tail_bytes_file (char const *filename, int fd, uintmax_t n_elide,
                       struct stat const *st, off_t current_pos)
{
  off_t size = st->st_size;
  if (presume_input_pipe || current_pos < 0 || size <= ST_BLKSIZE (*st))
    return elide_tail_bytes_pipe (filename, fd, n_elide, current_pos);
  else
    {
      /* Be careful here.  The current position may actually be
         beyond the end of the file.  */
      off_t diff = size - current_pos;
      off_t bytes_remaining = diff < 0 ? 0 : diff;

      if (bytes_remaining <= n_elide)
        return true;

      enum Copy_fd_status err = copy_fd (fd, bytes_remaining - n_elide);
      if (err == COPY_FD_OK)
        return true;

      diagnose_copy_fd_failure (err, filename);
      return false;
    }
}