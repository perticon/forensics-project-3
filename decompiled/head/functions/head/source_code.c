head (char const *filename, int fd, uintmax_t n_units, bool count_lines,
      bool elide_from_end)
{
  if (print_headers)
    write_header (filename);

  if (elide_from_end)
    {
      off_t current_pos = -1;
      struct stat st;
      if (fstat (fd, &st) != 0)
        {
          error (0, errno, _("cannot fstat %s"),
                 quoteaf (filename));
          return false;
        }
      if (! presume_input_pipe && usable_st_size (&st))
        {
          current_pos = elseek (fd, 0, SEEK_CUR, filename);
          if (current_pos < 0)
            return false;
        }
      if (count_lines)
        return elide_tail_lines_file (filename, fd, n_units, &st, current_pos);
      else
        return elide_tail_bytes_file (filename, fd, n_units, &st, current_pos);
    }
  if (count_lines)
    return head_lines (filename, fd, n_units);
  else
    return head_bytes (filename, fd, n_units);
}