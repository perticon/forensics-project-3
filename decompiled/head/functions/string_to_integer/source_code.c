string_to_integer (bool count_lines, char const *n_string)
{
  return xdectoumax (n_string, 0, UINTMAX_MAX, "bkKmMGTPEZY0",
                     count_lines ? _("invalid number of lines")
                                 : _("invalid number of bytes"), 0);
}