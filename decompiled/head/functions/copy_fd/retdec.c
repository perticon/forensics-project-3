int32_t copy_fd(int32_t src_fd, int64_t n_bytes) {
    int64_t v1 = __readfsqword(40); // 0x3b00
    int32_t result = 0; // 0x3b16
    if (n_bytes != 0) {
        uint64_t v2 = n_bytes;
        int64_t v3; // bp-8232, 0x3ae0
        int64_t v4 = safe_read(src_fd, (char *)&v3, v2 < 0x2000 ? v2 : 0x2000); // 0x3b53
        result = 1;
        while (v4 != -1) {
            int64_t v5 = v2 - v4; // 0x3b28
            result = 2;
            if (v4 == 0 == (v5 != 0)) {
                // break -> 0x3b66
                break;
            }
            // 0x3b35
            xwrite_stdout_part_0((int64_t)&v3, v4);
            result = 0;
            if (v5 == 0) {
                // break -> 0x3b66
                break;
            }
            v2 = v5;
            v4 = safe_read(src_fd, (char *)&v3, v2 < 0x2000 ? v2 : 0x2000);
            result = 1;
        }
    }
    // 0x3b66
    if (v1 != __readfsqword(40)) {
        // 0x3b97
        return function_2480();
    }
    // 0x3b79
    return result;
}