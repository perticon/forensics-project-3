int32_t copy_fd(int32_t edi, struct s0* rsi) {
    struct s0* rdi1;
    struct s0* rsp3;
    struct s0* rax4;
    struct s0* v5;
    int32_t eax6;
    struct s0* rbp7;
    struct s0* rbx8;
    struct s0* r12_9;
    struct s0* rdx10;
    struct s0* rcx11;
    struct s0* r8_12;
    struct s0* rax13;
    struct s0* rcx14;
    struct s0* r8_15;
    struct s0* rdx16;
    struct s0* rsp17;
    struct s0* rax18;
    struct s0* v19;
    struct s0* rdx20;
    int64_t v21;
    void* rsp22;
    struct s0* rax23;
    struct s0* v24;
    void* rdx25;
    int64_t v26;
    struct s0* v27;
    void* rbx28;
    int32_t r13d29;
    struct s0* v30;
    struct s0* rcx31;
    int64_t rdi32;
    struct s0* rax33;
    struct s0* v34;
    uint64_t rax35;
    int64_t r8_36;
    uint64_t rdx37;
    uint64_t rax38;
    void* rax39;
    struct s0* rax40;
    struct s0* rdx41;
    struct s0* rbp42;
    void* r8_43;
    struct s0* rax44;
    void* rsp45;
    struct s0* r15_46;
    struct s0* rdx47;
    struct s0* rdi48;
    struct s0* r8_49;
    struct s0* rax50;
    struct s0* r8_51;
    int32_t r12d52;
    void* r14_53;
    void* rax54;
    int64_t rsi55;
    struct s0* rax56;
    int64_t rdi57;
    struct s0* rax58;
    struct s0* rdi59;
    struct s0* rax60;
    void* rdx61;
    int64_t v62;
    int64_t rdi63;
    struct s0* rax64;
    int32_t eax65;
    struct s0* rdi66;
    struct s0* rsi67;
    int64_t rdi68;
    struct s0* rax69;
    struct s0* r14_70;
    struct s0* r13_71;
    struct s0* r15_72;
    struct s0* r8_73;
    struct s0* rax74;
    uint32_t eax75;
    struct s0* r8_76;
    struct s0* r8_77;
    struct s0* rax78;
    int32_t* rax79;
    struct s0* rdx80;
    int64_t rdi81;
    struct s0* rax82;
    struct s0* rsp83;
    int64_t rdi84;
    struct s0* r8_85;
    int32_t eax86;
    uint32_t v87;
    int64_t rdi88;
    struct s0* rax89;
    struct s0* r8_90;
    struct s0* rcx91;
    struct s0* r8_92;
    struct s0* rax93;
    struct s0* rcx94;
    struct s0* r8_95;
    struct s0* rcx96;
    struct s0* r8_97;
    struct s0* rax98;
    int32_t* rax99;

    *reinterpret_cast<int32_t*>(&rdi1) = edi;
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x1000 - 0x1000 - 16);
    rax4 = g28;
    v5 = rax4;
    if (!rsi) {
        addr_3b88_2:
        eax6 = 0;
    } else {
        *reinterpret_cast<int32_t*>(&rbp7) = *reinterpret_cast<int32_t*>(&rdi1);
        rbx8 = rsi;
        r12_9 = rsp3;
        do {
            *reinterpret_cast<int32_t*>(&rdx10) = 0x2000;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&rbp7);
            *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
            if (reinterpret_cast<unsigned char>(rbx8) <= reinterpret_cast<unsigned char>(0x2000)) {
                rdx10 = rbx8;
            }
            rax13 = safe_read(rdi1, r12_9, rdx10, rcx11, r8_12);
            rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8);
            rsi = rax13;
            if (reinterpret_cast<int1_t>(rax13 == 0xffffffffffffffff)) 
                goto addr_3b61_7;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx8) - reinterpret_cast<unsigned char>(rax13));
            if (rax13) 
                continue;
            if (rbx8) 
                goto addr_3b90_10;
            rdi1 = r12_9;
            xwrite_stdout_part_0(rdi1, rsi, rdx10, rcx14, r8_15);
            rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8);
        } while (rbx8);
        goto addr_3b88_2;
    }
    addr_3b66_12:
    rdx16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (!rdx16) {
        return eax6;
    }
    fun_2480();
    rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp3) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 24);
    rax18 = g28;
    v19 = rax18;
    if (rdx16) 
        goto addr_3be2_16;
    addr_3c78_17:
    addr_3c7d_18:
    rdx20 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v19) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        goto v21;
    }
    fun_2480();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax23 = g28;
    v24 = rax23;
    if (rdx20) 
        goto addr_3cf5_22;
    addr_3e55_23:
    addr_3d88_24:
    rdx25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        goto v26;
    }
    fun_2480();
    v27 = rdi1;
    rbx28 = rdx25;
    r13d29 = *reinterpret_cast<int32_t*>(&rsi);
    v30 = rcx31;
    *reinterpret_cast<int32_t*>(&rdi32) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
    rax33 = g28;
    v34 = rax33;
    rax35 = reinterpret_cast<uint64_t>(r8_36 - reinterpret_cast<unsigned char>(rcx31));
    rdx37 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax35) >> 63) >> 51;
    rax38 = rax35 + rdx37;
    *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
    rax40 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax39) - rdx37);
    *reinterpret_cast<int32_t*>(&rdx41) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
    if (!rax40) 
        goto addr_3edd_28;
    rdx41 = rax40;
    addr_3edd_28:
    rbp42 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(r8_43) - reinterpret_cast<unsigned char>(rdx41));
    rax44 = fun_24d0(rdi32, rdi32);
    rsp45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) {
        addr_4060_30:
        elseek_part_0();
    } else {
        r15_46 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp45) + 32);
        rdx47 = rdx41;
        *reinterpret_cast<int32_t*>(&rdi48) = r13d29;
        *reinterpret_cast<int32_t*>(&rdi48 + 4) = 0;
        rax50 = safe_read(rdi48, r15_46, rdx47, rcx31, r8_49);
        r8_51 = rax50;
        if (rax50 == 0xffffffffffffffff) {
            addr_40cc_32:
            quotearg_style(4, v27, rdx47, rcx31, r8_51);
            fun_2450();
            fun_23c0();
            fun_2630();
        } else {
            r12d52 = reinterpret_cast<signed char>(line_end);
            r14_53 = rbx28;
            if (rbx28 && (r8_51 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsp45) - 8 + 8 + reinterpret_cast<unsigned char>(r8_51) + 31) != *reinterpret_cast<signed char*>(&r12d52))) {
                r14_53 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx28) + 0xffffffffffffffff);
                if (r8_51) 
                    goto addr_3fb5_35;
                goto addr_3f48_37;
            }
            while (1) {
                if (!r8_51) 
                    goto addr_3f48_37;
                addr_3fb5_35:
                if (!rbx28) {
                    r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - 1);
                    rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                    if (!r14_53) 
                        goto addr_3fe0_40;
                } else {
                    rdx47 = r8_51;
                    *reinterpret_cast<int32_t*>(&rsi55) = r12d52;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi55) + 4) = 0;
                    rax56 = fun_2640(r15_46, rsi55, rdx47);
                    r8_51 = rax56;
                    if (!rax56) {
                        addr_3f48_37:
                        if (rbp42 == v30) 
                            goto addr_4142_42; else 
                            goto addr_3f53_43;
                    } else {
                        r8_51 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_51) - reinterpret_cast<unsigned char>(r15_46));
                        rax54 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_53) + 0xffffffffffffffff);
                        if (!r14_53) 
                            goto addr_3fe0_40;
                    }
                }
                r14_53 = rax54;
                continue;
                addr_3f53_43:
                rbp42 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp42) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi57) = r13d29;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
                rax58 = fun_24d0(rdi57, rdi57);
                if (reinterpret_cast<signed char>(rax58) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_30;
                *reinterpret_cast<int32_t*>(&rdx47) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi59) = r13d29;
                *reinterpret_cast<int32_t*>(&rdi59 + 4) = 0;
                rax60 = safe_read(rdi59, r15_46, 0x2000, rcx31, r8_51);
                r8_51 = rax60;
                if (rax60 == 0xffffffffffffffff) 
                    goto addr_40cc_32;
                if (!r8_51) 
                    goto addr_4142_42;
                r12d52 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_49:
    rdx61 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
    if (rdx61) {
        fun_2480();
    } else {
        goto v62;
    }
    addr_4142_42:
    goto addr_4071_49;
    addr_3fe0_40:
    if (reinterpret_cast<signed char>(rbp42) <= reinterpret_cast<signed char>(v30)) 
        goto addr_4029_53;
    *reinterpret_cast<int32_t*>(&rdx47) = 0;
    *reinterpret_cast<int32_t*>(&rdx47 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi63) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
    rax64 = fun_24d0(rdi63);
    if (reinterpret_cast<signed char>(rax64) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        goto addr_4071_49;
    } else {
        eax65 = copy_fd(r13d29, reinterpret_cast<unsigned char>(rbp42) - reinterpret_cast<unsigned char>(v30));
        r8_51 = r8_51;
        if (eax65) {
            *reinterpret_cast<int32_t*>(&rdi66) = eax65;
            *reinterpret_cast<int32_t*>(&rdi66 + 4) = 0;
            diagnose_copy_fd_failure(rdi66, v27, 0, rcx31, r8_51);
            goto addr_4071_49;
        } else {
            addr_4029_53:
            rsi67 = reinterpret_cast<struct s0*>(&r8_51->f1);
            if (rsi67) {
                xwrite_stdout_part_0(r15_46, rsi67, rdx47, rcx31, r8_51);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi68) = r13d29;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi68) + 4) = 0;
    rax69 = fun_24d0(rdi68, rdi68);
    if (reinterpret_cast<signed char>(rax69) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
    }
    goto addr_4071_49;
    addr_3cf5_22:
    r14_70 = rdi1;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi);
    rbx8 = rdx20;
    r12_9 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) + 0x90);
    r15_72 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) + 0x8f);
    while (*reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0, rax74 = safe_read(rdi1, r12_9, 0x2000, rcx31, r8_73), rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8), rsi = rax74, rax74 != 0xffffffffffffffff) {
        if (!rax74) 
            goto addr_3e55_23;
        eax75 = line_end;
        *reinterpret_cast<int32_t*>(&rbp7) = 0;
        *reinterpret_cast<int32_t*>(&rbp7 + 4) = 0;
        do {
            rbp7 = reinterpret_cast<struct s0*>(&rbp7->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_72) + reinterpret_cast<unsigned char>(rbp7)) != *reinterpret_cast<signed char*>(&eax75)) 
                continue;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx8) - 1);
            if (!rbx8) 
                goto addr_3d5d_67;
        } while (rsi != rbp7);
        xwrite_stdout_part_0(r12_9, rsi, 0x2000, rcx31, r8_76);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
    }
    rax78 = quotearg_style(4, r14_70, 0x2000, rcx31, r8_77);
    fun_2450();
    rax79 = fun_23c0();
    rcx31 = rax78;
    *reinterpret_cast<int32_t*>(&rdi1) = 0;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi) = *rax79;
    fun_2630();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d88_24;
    addr_3d5d_67:
    *reinterpret_cast<int32_t*>(&rdx80) = 1;
    *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi81) = *reinterpret_cast<int32_t*>(&r13_71);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi81) + 4) = 0;
    rax82 = fun_24d0(rdi81, rdi81);
    rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
    if (reinterpret_cast<signed char>(rax82) < reinterpret_cast<signed char>(0) && (((*reinterpret_cast<int32_t*>(&rdi84) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi84) + 4) = 0, eax86 = fun_26e0(rdi84, rsp83, 1, rcx31, r8_85), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), !!eax86) || reinterpret_cast<int1_t>((v87 & 0xf000) == rpl_mbrtowc)) && (*reinterpret_cast<int32_t*>(&rdx80) = 1, *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi88) = *reinterpret_cast<int32_t*>(&r13_71), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi88) + 4) = 0, rax89 = fun_24d0(rdi88, rdi88), rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8), reinterpret_cast<signed char>(rax89) < reinterpret_cast<signed char>(0)))) {
        rdx80 = r14_70;
        elseek_part_0();
        rsp83 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    }
    rsi = rbp7;
    rdi1 = r12_9;
    xwrite_stdout_part_0(rdi1, rsi, rdx80, rcx31, r8_90);
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp83) - 8 + 8);
    goto addr_3d88_24;
    addr_3be2_16:
    r14_70 = rdi1;
    *reinterpret_cast<int32_t*>(&r13_71) = *reinterpret_cast<int32_t*>(&rsi);
    rbp7 = rdx16;
    *reinterpret_cast<int32_t*>(&r15_72) = 0x2000;
    *reinterpret_cast<int32_t*>(&r15_72 + 4) = 0;
    r12_9 = rsp17;
    do {
        rsi = r12_9;
        *reinterpret_cast<int32_t*>(&rdi1) = *reinterpret_cast<int32_t*>(&r13_71);
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r15_72) > reinterpret_cast<unsigned char>(rbp7)) {
            r15_72 = rbp7;
        }
        rax93 = safe_read(rdi1, rsi, r15_72, rcx91, r8_92);
        rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8);
        if (reinterpret_cast<int1_t>(rax93 == 0xffffffffffffffff)) 
            break;
    } while (rax93 && (rsi = rax93, rdi1 = r12_9, xwrite_stdout_part_0(rdi1, rsi, r15_72, rcx94, r8_95), rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8), rbp7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp7) - reinterpret_cast<unsigned char>(rax93)), !!rbp7));
    goto addr_3c78_17;
    rax98 = quotearg_style(4, r14_70, r15_72, rcx96, r8_97);
    fun_2450();
    rax99 = fun_23c0();
    rcx31 = rax98;
    *reinterpret_cast<int32_t*>(&rdi1) = 0;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi) = *rax99;
    fun_2630();
    rsp17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3c7d_18;
    addr_3b61_7:
    eax6 = 1;
    goto addr_3b66_12;
    addr_3b90_10:
    eax6 = 2;
    goto addr_3b66_12;
}