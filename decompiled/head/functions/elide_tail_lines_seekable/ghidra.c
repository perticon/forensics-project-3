ulong elide_tail_lines_seekable
                (undefined8 param_1,int param_2,long param_3,long param_4,long param_5)

{
  size_t sVar1;
  long lVar2;
  long lVar3;
  __off_t _Var4;
  size_t sVar5;
  void *pvVar6;
  ulong uVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  int *piVar10;
  int iVar11;
  long in_FS_OFFSET;
  undefined8 local_2050;
  undefined local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = param_5 - param_4;
  lVar3 = (ulong)((int)lVar2 + ((uint)(lVar2 >> 0x5f) >> 0x13) & 0x1fff) -
          ((ulong)(lVar2 >> 0x3f) >> 0x33);
  lVar2 = 0x2000;
  if (lVar3 != 0) {
    lVar2 = lVar3;
  }
  param_5 = param_5 - lVar2;
  _Var4 = lseek(param_2,param_5,0);
  if (_Var4 < 0) {
LAB_00104060:
    elseek_part_0(param_5,0,param_1);
    uVar7 = 0;
  }
  else {
    sVar5 = safe_read(param_2,local_2048,lVar2);
    if (sVar5 != 0xffffffffffffffff) {
      iVar11 = (int)line_end;
      lVar2 = param_3;
      if (((param_3 != 0) && (sVar5 != 0)) && (local_2048[sVar5 - 1] != line_end)) {
        lVar2 = param_3 + -1;
      }
joined_r0x00103f41:
      do {
        if (sVar5 == 0) {
LAB_00103f48:
          if (param_5 != param_4) {
            param_5 = param_5 + -0x2000;
            _Var4 = lseek(param_2,param_5,0);
            if (_Var4 < 0) goto LAB_00104060;
            sVar5 = safe_read(param_2,local_2048,0x2000);
            if (sVar5 == 0xffffffffffffffff) break;
            if (sVar5 != 0) {
              iVar11 = (int)line_end;
              goto joined_r0x00103f41;
            }
          }
          uVar7 = 1;
          goto LAB_00104071;
        }
        if (param_3 == 0) {
          sVar5 = sVar5 - 1;
          sVar1 = local_2050;
        }
        else {
          pvVar6 = memrchr(local_2048,iVar11,sVar5);
          if (pvVar6 == (void *)0x0) goto LAB_00103f48;
          sVar5 = (long)pvVar6 - (long)local_2048;
          sVar1 = local_2050;
        }
        local_2050 = sVar5;
        if (lVar2 == 0) goto LAB_00103fe0;
        lVar2 = lVar2 + -1;
        sVar5 = local_2050;
        local_2050 = sVar1;
      } while( true );
    }
    uVar8 = quotearg_style(4,param_1);
    uVar9 = dcgettext(0,"error reading %s",5);
    piVar10 = __errno_location();
    error(0,*piVar10,uVar9,uVar8);
    uVar7 = 0;
  }
  goto LAB_00104071;
LAB_00103fe0:
  sVar5 = local_2050;
  if (param_4 < param_5) {
    _Var4 = lseek(param_2,param_4,0);
    if (_Var4 < 0) {
      elseek_part_0(param_4,0,param_1);
      uVar7 = 0;
      goto LAB_00104071;
    }
    iVar11 = copy_fd(param_2,param_5 - param_4);
    sVar5 = local_2050;
    sVar1 = local_2050;
    if (iVar11 != 0) {
      diagnose_copy_fd_failure(iVar11,param_1);
      uVar7 = 0;
      goto LAB_00104071;
    }
  }
  local_2050 = sVar1;
  if (sVar5 != 0xffffffffffffffff) {
    xwrite_stdout_part_0();
  }
  lVar2 = param_5 + 1 + sVar5;
  uVar7 = lseek(param_2,lVar2,0);
  if ((long)uVar7 < 0) {
    elseek_part_0(lVar2,0,param_1);
  }
  uVar7 = ~uVar7 >> 0x3f;
LAB_00104071:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}