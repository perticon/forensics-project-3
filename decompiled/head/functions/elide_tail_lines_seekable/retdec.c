bool elide_tail_lines_seekable(char * pretty_filename, int32_t fd, int64_t n_lines, int64_t start_pos, int64_t size) {
    char buffer[8192]; // bp-8272, 0x3e70
    char v1[8192]; // 0x3fef
    char v2[8192]; // 0x4012
    int64_t v3 = __readfsqword(40); // 0x3eac
    int64_t v4 = size - start_pos; // 0x3ec0
    uint64_t v5 = (v4 >> 63) / 0x8000000000000; // 0x3ec5
    int64_t v6 = (v5 + v4) % 0x2000 - v5; // 0x3ed1
    int64_t v7 = v6 != 0 ? v6 : 0x2000; // 0x3ed9
    int64_t v8 = size - v7; // 0x3edd
    int64_t v9 = v8; // 0x3ef0
    int64_t v10; // 0x3e70
    int64_t v11; // 0x3e70
    int64_t v12; // 0x3e70
    int64_t v13; // 0x3e70
    int64_t v14; // 0x3e70
    int64_t v15; // 0x3e70
    int64_t v16; // bp-8264, 0x3e70
    if (function_24d0() < 0) {
        goto lab_0x4060;
    } else {
        int64_t v17 = safe_read(fd, (char *)&v16, v7); // 0x3f04
        if (v17 == -1) {
            goto lab_0x40cc;
        } else {
            // 0x3f16
            v14 = v8;
            v12 = v17;
            v10 = n_lines;
            if (n_lines == 0 || v17 == 0) {
                goto lab_0x3fb0;
            } else {
                // 0x3f33
                v14 = v8;
                v12 = v17;
                v10 = n_lines;
                int64_t v18; // bp-8296, 0x3e70
                if (*(char *)((int64_t)&v18 + 31 + v17) == line_end) {
                    goto lab_0x3fb0;
                } else {
                    // 0x3f3a
                    v15 = v8;
                    v13 = v17;
                    v11 = n_lines - 1;
                    goto lab_0x3fb5;
                }
            }
        }
    }
  lab_0x4060:
    // 0x4060
    elseek_part_0(v9, 0, (int64_t)pretty_filename);
    int64_t v19 = 0; // 0x406f
    goto lab_0x4071;
  lab_0x4071:
    // 0x4071
    if (v3 != __readfsqword(40)) {
        // 0x414c
        return function_2480() % 2 != 0;
    }
    // 0x4088
    return v19 != 0;
  lab_0x40cc:
    // 0x40cc
    quotearg_style();
    function_2450();
    function_23c0();
    function_2630();
    v19 = 0;
    goto lab_0x4071;
  lab_0x3fb0:;
    int64_t v20 = v14; // 0x3fb3
    int64_t v21 = v10; // 0x3fb3
    v15 = v14;
    v13 = v12;
    v11 = v10;
    if (v12 == 0) {
        goto lab_0x3f48;
    } else {
        goto lab_0x3fb5;
    }
  lab_0x3f48:
    // 0x3f48
    v19 = 1;
    if (v20 == start_pos) {
        goto lab_0x4071;
    } else {
        int64_t v22 = v20 - 0x2000; // 0x3f53
        v9 = v22;
        if (function_24d0() < 0) {
            goto lab_0x4060;
        } else {
            int64_t v23 = safe_read(fd, (char *)&v16, 0x2000); // 0x3f7b
            v14 = v22;
            v12 = v23;
            v10 = v21;
            v19 = 1;
            switch (v23) {
                case -1: {
                    goto lab_0x40cc;
                }
                case 0: {
                    goto lab_0x4071;
                }
                default: {
                    goto lab_0x3fb0;
                }
            }
        }
    }
  lab_0x3fb5:;
    int64_t v24 = &v16; // 0x3ef6
    int64_t v25 = v11;
    int64_t v26 = v15;
    int64_t v27; // 0x3e70
    int64_t v28; // 0x3fd4
    if (n_lines == 0) {
        int64_t v29 = v13 - 1; // 0x3fa0
        v28 = v29;
        v27 = v29;
        if (v25 == 0) {
            goto lab_0x3fe0;
        } else {
            goto lab_0x3fad;
        }
    } else {
        int64_t v30 = function_2640(); // 0x3fc3
        v20 = v26;
        v21 = v25;
        if (v30 == 0) {
            goto lab_0x3f48;
        } else {
            // 0x3fd4
            v28 = v30 - v24;
            v27 = v28;
            if (v25 != 0) {
                goto lab_0x3fad;
            } else {
                goto lab_0x3fe0;
            }
        }
    }
  lab_0x3fe0:;
    int64_t v31 = v27; // 0x3fe8
    if (v26 > start_pos) {
        // 0x3fea
        v1[0] = v27;
        buffer = v1;
        if (function_24d0() < 0) {
            // 0x40b7
            elseek_part_0(start_pos, 0, (int64_t)pretty_filename);
            v19 = 0;
            goto lab_0x4071;
        } else {
            // 0x4007
            v2[0] = (char)*(int64_t *)&buffer;
            buffer = v2;
            uint32_t v32 = copy_fd(fd, v26 - start_pos); // 0x4017
            v31 = *(int64_t *)&buffer;
            if (v32 != 0) {
                // 0x412f
                diagnose_copy_fd_failure((int64_t)v32, (int64_t)pretty_filename);
                v19 = 0;
                goto lab_0x4071;
            } else {
                goto lab_0x4029;
            }
        }
    } else {
        goto lab_0x4029;
    }
  lab_0x3fad:
    // 0x3fad
    v14 = v26;
    v12 = v28;
    v10 = v25 - 1;
    goto lab_0x3fb0;
  lab_0x4029:;
    int64_t v33 = v31 + 1;
    if (v33 != 0) {
        // 0x40a0
        xwrite_stdout_part_0(v24, v33);
    }
    int64_t v34 = function_24d0(); // 0x403f
    if (v34 < 0) {
        // 0x4111
        elseek_part_0(v33 + v26, 0, (int64_t)pretty_filename);
    }
    // 0x404d
    v19 = v34 >> 63 ^ 1;
    goto lab_0x4071;
}