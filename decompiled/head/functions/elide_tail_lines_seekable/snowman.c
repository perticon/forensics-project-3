unsigned char elide_tail_lines_seekable(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* v6;
    struct s0* rbx7;
    int32_t r13d8;
    struct s0* v9;
    int64_t rdi10;
    struct s0* rax11;
    struct s0* v12;
    void* rax13;
    void* rdx14;
    void* rax15;
    void* rax16;
    struct s0* rax17;
    struct s0* rdx18;
    struct s0* rbp19;
    struct s0* rax20;
    void* rsp21;
    uint64_t rax22;
    struct s0* r15_23;
    struct s0* rdx24;
    struct s0* rdi25;
    struct s0* rax26;
    struct s0* r8_27;
    int32_t r12d28;
    struct s0* r14_29;
    struct s0* rax30;
    int64_t rsi31;
    struct s0* rax32;
    int64_t rdi33;
    struct s0* rax34;
    struct s0* rdi35;
    struct s0* rax36;
    void* rdx37;
    int64_t rdi38;
    struct s0* rax39;
    int32_t eax40;
    struct s0* rdi41;
    struct s0* rsi42;
    int64_t rdi43;
    struct s0* rax44;

    v6 = rdi;
    rbx7 = rdx;
    r13d8 = esi;
    v9 = rcx;
    *reinterpret_cast<int32_t*>(&rdi10) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rcx));
    rdx14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax13) >> 63) >> 51);
    rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax13) + reinterpret_cast<uint64_t>(rdx14));
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax15) & 0x1fff;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax16) - reinterpret_cast<uint64_t>(rdx14));
    *reinterpret_cast<int32_t*>(&rdx18) = 0x2000;
    *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
    if (rax17) {
        rdx18 = rax17;
    }
    rbp19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rdx18));
    rax20 = fun_24d0(rdi10, rdi10);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 56 - 8 + 8);
    if (reinterpret_cast<signed char>(rax20) < reinterpret_cast<signed char>(0)) {
        addr_4060_4:
        elseek_part_0();
        *reinterpret_cast<int32_t*>(&rax22) = 0;
    } else {
        r15_23 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp21) + 32);
        rdx24 = rdx18;
        *reinterpret_cast<int32_t*>(&rdi25) = r13d8;
        *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0;
        rax26 = safe_read(rdi25, r15_23, rdx24, rcx, r8);
        r8_27 = rax26;
        if (rax26 == 0xffffffffffffffff) {
            addr_40cc_6:
            quotearg_style(4, v6, rdx24, rcx, r8_27);
            fun_2450();
            fun_23c0();
            fun_2630();
            *reinterpret_cast<int32_t*>(&rax22) = 0;
        } else {
            r12d28 = reinterpret_cast<signed char>(line_end);
            r14_29 = rbx7;
            if (rbx7 && (r8_27 && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 + reinterpret_cast<unsigned char>(r8_27)) + 31) != *reinterpret_cast<signed char*>(&r12d28))) {
                r14_29 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx7) + 0xffffffffffffffff);
                if (r8_27) 
                    goto addr_3fb5_9;
                goto addr_3f48_11;
            }
            while (1) {
                if (!r8_27) 
                    goto addr_3f48_11;
                addr_3fb5_9:
                if (!rbx7) {
                    r8_27 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_27) - 1);
                    rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_29) + 0xffffffffffffffff);
                    if (!r14_29) 
                        goto addr_3fe0_14;
                } else {
                    rdx24 = r8_27;
                    *reinterpret_cast<int32_t*>(&rsi31) = r12d28;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi31) + 4) = 0;
                    rax32 = fun_2640(r15_23, rsi31, rdx24);
                    r8_27 = rax32;
                    if (!rax32) {
                        addr_3f48_11:
                        if (rbp19 == v9) 
                            goto addr_4142_16; else 
                            goto addr_3f53_17;
                    } else {
                        r8_27 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8_27) - reinterpret_cast<unsigned char>(r15_23));
                        rax30 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_29) + 0xffffffffffffffff);
                        if (!r14_29) 
                            goto addr_3fe0_14;
                    }
                }
                r14_29 = rax30;
                continue;
                addr_3f53_17:
                rbp19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp19) - 0x2000);
                *reinterpret_cast<int32_t*>(&rdi33) = r13d8;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
                rax34 = fun_24d0(rdi33, rdi33);
                if (reinterpret_cast<signed char>(rax34) < reinterpret_cast<signed char>(0)) 
                    goto addr_4060_4;
                *reinterpret_cast<int32_t*>(&rdx24) = 0x2000;
                *reinterpret_cast<int32_t*>(&rdx24 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi35) = r13d8;
                *reinterpret_cast<int32_t*>(&rdi35 + 4) = 0;
                rax36 = safe_read(rdi35, r15_23, 0x2000, rcx, r8_27);
                r8_27 = rax36;
                if (rax36 == 0xffffffffffffffff) 
                    goto addr_40cc_6;
                if (!r8_27) 
                    goto addr_4142_16;
                r12d28 = reinterpret_cast<signed char>(line_end);
            }
        }
    }
    addr_4071_23:
    rdx37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rdx37) {
        fun_2480();
    } else {
        return *reinterpret_cast<unsigned char*>(&rax22);
    }
    addr_4142_16:
    *reinterpret_cast<int32_t*>(&rax22) = 1;
    goto addr_4071_23;
    addr_3fe0_14:
    if (reinterpret_cast<signed char>(rbp19) <= reinterpret_cast<signed char>(v9)) 
        goto addr_4029_27;
    *reinterpret_cast<int32_t*>(&rdx24) = 0;
    *reinterpret_cast<int32_t*>(&rdx24 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi38) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi38) + 4) = 0;
    rax39 = fun_24d0(rdi38);
    if (reinterpret_cast<signed char>(rax39) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        *reinterpret_cast<int32_t*>(&rax22) = 0;
        goto addr_4071_23;
    } else {
        eax40 = copy_fd(r13d8, reinterpret_cast<unsigned char>(rbp19) - reinterpret_cast<unsigned char>(v9));
        r8_27 = r8_27;
        if (eax40) {
            *reinterpret_cast<int32_t*>(&rdi41) = eax40;
            *reinterpret_cast<int32_t*>(&rdi41 + 4) = 0;
            diagnose_copy_fd_failure(rdi41, v6, 0, rcx, r8_27);
            *reinterpret_cast<int32_t*>(&rax22) = 0;
            goto addr_4071_23;
        } else {
            addr_4029_27:
            rsi42 = reinterpret_cast<struct s0*>(&r8_27->f1);
            if (rsi42) {
                xwrite_stdout_part_0(r15_23, rsi42, rdx24, rcx, r8_27);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi43) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi43) + 4) = 0;
    rax44 = fun_24d0(rdi43, rdi43);
    if (reinterpret_cast<signed char>(rax44) < reinterpret_cast<signed char>(0)) {
        elseek_part_0();
        rax44 = rax44;
    }
    rax22 = reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rax44)) >> 63;
    goto addr_4071_23;
}