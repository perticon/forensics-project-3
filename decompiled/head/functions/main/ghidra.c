byte main(int param_1,undefined8 *param_2)

{
  byte *pbVar1;
  word *pwVar2;
  void **ppvVar3;
  Elf64_Ehdr **ppEVar4;
  ulong uVar5;
  byte bVar6;
  uint uVar7;
  int iVar8;
  char *pcVar9;
  int *piVar10;
  void **__ptr;
  ulong uVar11;
  void *pvVar12;
  Elf64_Ehdr *pEVar13;
  Elf64_Ehdr *pEVar14;
  undefined *puVar15;
  __off_t _Var16;
  undefined1 *puVar17;
  long lVar18;
  Elf64_Ehdr *pEVar19;
  Elf64_Ehdr *pEVar20;
  undefined8 uVar21;
  char *pcVar22;
  char *pcVar23;
  char *pcVar24;
  undefined8 *puVar25;
  Elf64_Ehdr *pEVar26;
  long lVar27;
  uint *puVar28;
  undefined8 in_R9;
  char cVar29;
  uint *puVar30;
  Elf64_Ehdr *pEVar31;
  void **ppvVar32;
  long in_FS_OFFSET;
  undefined auVar33 [16];
  undefined8 uVar34;
  undefined8 uStack376;
  Elf64_Ehdr *local_170;
  Elf64_Ehdr **local_168;
  Elf64_Ehdr *local_160;
  uint local_158;
  byte local_153;
  char local_152;
  byte local_151;
  Elf64_Ehdr *local_150;
  Elf64_Ehdr *local_148;
  Elf64_Ehdr *local_140;
  Elf64_Ehdr *local_138;
  Elf64_Ehdr *local_130;
  char *local_128;
  Elf64_Ehdr *local_120;
  Elf64_Ehdr *local_118;
  ulong local_110;
  ulong local_108;
  Elf64_Ehdr *local_100;
  char *local_f8 [2];
  stat local_e8;
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  have_read_stdin = '\0';
  print_headers = '\0';
  line_end = '\n';
  if ((1 < param_1) && (pcVar9 = (char *)param_2[1], puVar25 = param_2, *pcVar9 == '-'))
  goto LAB_00102949;
LAB_001027e7:
  local_152 = '\x01';
  iVar8 = 0;
  local_170 = (Elf64_Ehdr *)0xa;
  do {
    local_153 = 0;
    puVar30 = &switchD_00102858::switchdataD_00109834;
    while( true ) {
      uVar34 = 0x102825;
      uVar7 = getopt_long(param_1,param_2,"c:n:qvz0123456789",long_options,0);
      if (uVar7 == 0xffffffff) {
        if ((iVar8 == 1) || ((iVar8 == 0 && (optind < param_1 + -1)))) {
          print_headers = '\x01';
        }
        if (((local_152 == '\0') && (local_153 != 0)) && ((long)local_170 < 0)) {
          uVar34 = umaxtostr(local_170,local_58);
          uVar34 = quote(uVar34);
          uVar21 = dcgettext(0,"invalid number of bytes",5);
          auVar33 = error(1,0x4b,"%s: %s",uVar21,uVar34);
          uVar34 = uStack376;
          uStack376 = SUB168(auVar33,0);
          (*(code *)PTR___libc_start_main_0010cfd0)
                    (main,uVar34,&local_170,0,0,SUB168(auVar33 >> 0x40,0),&uStack376);
          do {
                    /* WARNING: Do nothing block with infinite loop */
          } while( true );
        }
        if (optind < param_1) {
          puVar17 = (undefined1 *)(param_2 + optind);
          pEVar19 = *(Elf64_Ehdr **)puVar17;
          if (pEVar19 == (Elf64_Ehdr *)0x0) {
            local_151 = 1;
            goto LAB_00102e56;
          }
        }
        else {
          puVar17 = default_file_list_1;
          pEVar19 = (Elf64_Ehdr *)&DAT_00109155;
        }
        local_151 = 1;
        local_108 = (ulong)((uint)local_170 & 0x1fff);
        local_110 = 0x2000 - local_108;
        local_130 = (Elf64_Ehdr *)((ulong)((long)local_170 + local_110) >> 0xd);
        local_148 = (Elf64_Ehdr *)local_130->e_ident_magic_str;
        local_120 = (Elf64_Ehdr *)((ulong)local_148 >> 1);
        local_168 = (Elf64_Ehdr **)((long)puVar17 + 8);
        goto LAB_00102b7f;
      }
      if (0x80 < (int)uVar7) goto switchD_00102858_caseD_64;
      if ((int)uVar7 < 99) break;
      switch(uVar7) {
      case 99:
        local_153 = *optarg == '-';
        if ((bool)local_153) {
          optarg = optarg + 1;
        }
        local_170 = (Elf64_Ehdr *)string_to_integer();
        local_152 = '\0';
        break;
      default:
        goto switchD_00102858_caseD_64;
      case 0x6e:
        local_153 = *optarg == '-';
        if ((bool)local_153) {
          optarg = optarg + 1;
        }
        local_170 = (Elf64_Ehdr *)string_to_integer();
        local_152 = '\x01';
        break;
      case 0x71:
        iVar8 = 2;
        break;
      case 0x76:
        iVar8 = 1;
        break;
      case 0x7a:
        line_end = '\0';
        break;
      case 0x80:
        presume_input_pipe = 1;
      }
    }
    if (uVar7 == 0xffffff7d) {
      version_etc(stdout,&DAT_001090ad,"GNU coreutils",Version,"David MacKenzie","Jim Meyering",0,
                  uVar34);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (uVar7 != 0xffffff7e) {
switchD_00102858_caseD_64:
      if (9 < uVar7 - 0x30) goto LAB_001032fc;
      local_170 = (Elf64_Ehdr *)((ulong)local_170 & 0xffffffff00000000 | (ulong)uVar7);
      uVar34 = dcgettext(0,"invalid trailing option -- %c",5);
      do {
        error(0,0,uVar34,(int)local_170);
LAB_001032fc:
        usage(1);
switchD_001029af_caseD_2:
        if ((char)in_R9 != '\0') {
          line_end = '\0';
        }
        uVar34 = dcgettext(0,"invalid trailing option -- %c",5);
        local_170._0_4_ = (int)(char)puVar30;
      } while( true );
    }
    pcVar9 = (char *)usage();
    puVar25 = param_2;
LAB_00102949:
    param_2 = puVar25;
    if (9 < (int)pcVar9[1] - 0x30U) goto LAB_001027e7;
    pcVar23 = pcVar9 + 1;
    do {
      pcVar22 = pcVar23;
      cVar29 = pcVar22[1];
      pcVar23 = pcVar22 + 1;
      puVar30 = (uint *)(ulong)(uint)(int)cVar29;
    } while ((int)cVar29 - 0x30U < 10);
    if (cVar29 == '\0') {
      local_152 = '\x01';
      iVar8 = 0;
    }
    else {
      in_R9 = 0;
      puVar28 = (uint *)0x0;
      cVar29 = '\x01';
      iVar8 = 0;
      pcVar24 = pcVar23;
      do {
        switch((int)puVar30 - 0x62U & 0xff) {
        case 0:
        case 9:
        case 0xb:
          cVar29 = '\0';
          puVar28 = puVar30;
          break;
        case 1:
          puVar28 = (uint *)0x0;
          cVar29 = '\0';
          break;
        default:
          goto switchD_001029af_caseD_2;
        case 10:
          cVar29 = '\x01';
          break;
        case 0xf:
          iVar8 = 2;
          break;
        case 0x14:
          iVar8 = 1;
          break;
        case 0x18:
          in_R9 = 1;
        }
        pbVar1 = (byte *)(pcVar24 + 1);
        puVar30 = (uint *)(ulong)*pbVar1;
        pcVar24 = pcVar24 + 1;
      } while (*pbVar1 != 0);
      if ((char)in_R9 != '\0') {
        line_end = '\0';
      }
      *pcVar23 = (char)puVar28;
      local_152 = cVar29;
      if ((char)puVar28 != '\0') {
        pcVar22[2] = '\0';
      }
    }
    param_2 = puVar25 + 1;
    param_1 = param_1 + -1;
    local_170 = (Elf64_Ehdr *)string_to_integer(local_152,pcVar9 + 1);
    *param_2 = *puVar25;
  } while( true );
LAB_00102b57:
  while( true ) {
    if ((local_158 != 0) && (iVar8 = close((int)puVar30), iVar8 != 0)) {
      uVar34 = quotearg_style(4,pEVar19);
      pcVar9 = "failed to close %s";
LAB_00102bc3:
      param_2 = (undefined8 *)0x0;
      puVar30 = (uint *)dcgettext(0,pcVar9,5);
      piVar10 = __errno_location();
      error(0,*piVar10,puVar30,uVar34);
    }
    ppEVar4 = local_168 + 1;
    local_151 = local_151 & (byte)param_2;
    pEVar19 = *local_168;
    local_168 = ppEVar4;
    if (pEVar19 == (Elf64_Ehdr *)0x0) break;
LAB_00102b7f:
    local_158 = pEVar19->e_ident_magic_num - 0x2d;
    if ((local_158 == 0) && (local_158 = (uint)(byte)pEVar19->e_ident_magic_str[0], local_158 == 0))
    {
      puVar30 = (uint *)0x0;
      have_read_stdin = '\x01';
      pEVar19 = (Elf64_Ehdr *)dcgettext(0,"standard input",5);
    }
    else {
      uVar7 = open((char *)pEVar19,0);
      puVar30 = (uint *)(ulong)uVar7;
      if ((int)uVar7 < 0) {
        uVar34 = quotearg_style(4,pEVar19);
        pcVar9 = "cannot open %s for reading";
        goto LAB_00102bc3;
      }
    }
    if (print_headers != '\0') {
      pcVar9 = "";
      if (first_file_0 == '\0') {
        pcVar9 = "\n";
      }
      __printf_chk(1,"%s==> %s <==\n",pcVar9,pEVar19);
      first_file_0 = '\0';
    }
    if (local_153 == 0) {
      if (local_152 == '\0') {
        uVar7 = head_bytes(pEVar19,puVar30,local_170);
        param_2 = (undefined8 *)(ulong)uVar7;
      }
      else {
        uVar7 = head_lines();
        param_2 = (undefined8 *)(ulong)uVar7;
      }
    }
    else {
      iVar8 = fstat((int)puVar30,&local_e8);
      if (iVar8 == 0) {
        param_2 = (undefined8 *)(ulong)presume_input_pipe;
        if ((presume_input_pipe == 0) && ((local_e8.st_mode & 0xd000) == 0x8000)) goto LAB_0010327d;
        local_138 = (Elf64_Ehdr *)0xffffffffffffffff;
        if (local_152 != '\0') goto LAB_00102eb6;
        goto LAB_00102c7e;
      }
      param_2 = (undefined8 *)0x0;
      uVar34 = quotearg_style(4,pEVar19);
      uVar21 = dcgettext(0,"cannot fstat %s",5);
      piVar10 = __errno_location();
      error(0,*piVar10,uVar21,uVar34);
    }
  }
LAB_00102e56:
  if ((have_read_stdin == '\0') || (iVar8 = close(0), -1 < iVar8)) {
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return local_151 ^ 1;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  piVar10 = __errno_location();
  error(1,*piVar10,&DAT_00109155);
LAB_0010327d:
  local_138 = (Elf64_Ehdr *)lseek((int)puVar30,0,1);
  if ((long)local_138 < 0) {
    elseek_part_0(0,1,pEVar19);
    goto LAB_00102b57;
  }
  lVar18 = local_e8.st_blksize;
  if (0x1fffffffffffffff < local_e8.st_blksize - 1U) {
    lVar18 = 0x200;
  }
  if (local_152 == '\0') {
    if (local_e8.st_size <= lVar18) {
LAB_00102c7e:
      pEVar13 = local_170;
      local_150 = local_138;
      if (&Elf64_Ehdr_00100000 < local_170) {
        local_118 = (Elf64_Ehdr *)0x10;
        pEVar20 = (Elf64_Ehdr *)0x0;
        uVar7 = 0;
        __ptr = (void **)0x0;
        local_160 = (Elf64_Ehdr *)0x1;
        local_128 = (char *)0x0;
        pEVar14 = (Elf64_Ehdr *)0x0;
        pEVar13 = local_160;
        local_100 = pEVar19;
        if (local_148 < (Elf64_Ehdr *)0x11) {
          local_118 = local_148;
        }
        do {
          local_160 = pEVar13;
          pEVar19 = local_118;
          if ((pEVar14 != (Elf64_Ehdr *)0x0) &&
             (pEVar19 = (Elf64_Ehdr *)((long)pEVar14 * 2), local_120 < pEVar14)) {
            pEVar19 = local_148;
          }
          __ptr = (void **)xreallocarray(__ptr,pEVar19,8);
          pEVar13 = local_160;
          pEVar26 = pEVar20;
          local_160 = pEVar19;
          do {
            pEVar20 = pEVar13;
            if ((char)uVar7 != '\0') {
              uVar11 = full_read((ulong)puVar30 & 0xffffffff,__ptr[(long)pEVar26],0x2000);
              if (0x1fff < uVar11) {
                local_150 = (Elf64_Ehdr *)(local_150->e_ident_magic_str + (uVar11 - 1));
                bVar6 = 0;
LAB_00102e1d:
                local_140 = (Elf64_Ehdr *)((ulong)local_140 & 0xffffffffffffff00 | (ulong)bVar6);
                xwrite_stdout_part_0();
                uVar7 = (uint)local_153;
                param_2 = (undefined8 *)((ulong)local_140 & 0xff);
                goto LAB_00102d57;
              }
              piVar10 = __errno_location();
              if (*piVar10 == 0) {
                param_2 = (undefined8 *)(ulong)uVar7;
                goto LAB_00102d44;
              }
LAB_00103636:
              pEVar19 = local_100;
              uVar34 = quotearg_style(4,local_100);
              uVar21 = dcgettext(0,"error reading %s",5);
              param_2 = (undefined8 *)0x0;
              error(0,*piVar10,uVar21,uVar34);
LAB_0010341a:
              ppvVar3 = __ptr + (long)local_128;
              for (ppvVar32 = __ptr; ppvVar32 != ppvVar3; ppvVar32 = ppvVar32 + 1) {
                free(*ppvVar32);
              }
              free(__ptr);
              goto LAB_00103443;
            }
            pvVar12 = (void *)xmalloc(0x2000);
            __ptr[(long)pEVar26] = pvVar12;
            local_128 = pEVar26->e_ident_magic_str;
            uVar11 = full_read((ulong)puVar30 & 0xffffffff,pvVar12,0x2000);
            if (uVar11 < 0x2000) {
              piVar10 = __errno_location();
              if (*piVar10 != 0) goto LAB_00103636;
              param_2 = (undefined8 *)(ulong)local_153;
            }
            else {
              param_2 = (undefined8 *)0x0;
            }
            if (local_130 == pEVar26) {
LAB_00102d44:
              bVar6 = (byte)param_2;
              local_150 = (Elf64_Ehdr *)(local_150->e_ident_magic_str + (uVar11 - 1));
              uVar7 = (uint)local_153;
              if (uVar11 != 0) goto LAB_00102e1d;
            }
LAB_00102d57:
            pEVar19 = local_100;
            uVar5 = local_108;
            pEVar13 = (Elf64_Ehdr *)((ulong)pEVar20->e_ident_magic_str % (ulong)local_148);
            if ((char)param_2 != '\0') {
              puVar25 = (undefined8 *)(ulong)uVar7;
              if ((char)uVar7 == '\0') {
                if ((local_130 == pEVar20) &&
                   (local_150 = (Elf64_Ehdr *)((long)local_150 + (uVar11 - local_108)),
                   uVar11 - local_108 != 0)) {
                  xwrite_stdout_part_0(__ptr[(long)pEVar13]);
                }
              }
              else {
                local_150 = (Elf64_Ehdr *)(local_150->e_ident_magic_str + (local_110 - 1));
                param_2 = puVar25;
                if (local_110 < -uVar11 + 0x2000) {
                  xwrite_stdout_part_0((long)__ptr[(long)pEVar20] + uVar11);
                }
                else {
                  pEVar14 = (Elf64_Ehdr *)(__ptr + (long)pEVar13);
                  if ((-uVar11 == -0x2000) ||
                     (local_160 = (Elf64_Ehdr *)(__ptr + (long)pEVar13),
                     xwrite_stdout_part_0((long)__ptr[(long)pEVar20] + uVar11), pEVar14 = local_160,
                     uVar11 != uVar5)) {
                    xwrite_stdout_part_0(*(void **)pEVar14);
                  }
                }
              }
              goto LAB_0010341a;
            }
            pEVar14 = local_160;
            pEVar26 = pEVar20;
          } while (local_160 != pEVar20);
        } while( true );
      }
      uVar11 = 0;
      pEVar14 = local_170 + 0x80;
      local_128 = (char *)xnmalloc(2,pEVar14);
      param_2 = (undefined8 *)(ulong)local_153;
      local_f8[0] = local_128;
      local_f8[1] = local_128 + (long)pEVar14;
      local_140 = pEVar19;
LAB_001034c1:
      pEVar19 = (Elf64_Ehdr *)full_read((ulong)puVar30 & 0xffffffff,local_f8[uVar11],pEVar14);
      cVar29 = (char)param_2;
      if (pEVar19 < pEVar14) {
        local_160 = pEVar19;
        pEVar20 = (Elf64_Ehdr *)__errno_location();
        pEVar19 = local_140;
        if (*(int *)pEVar20 != 0) {
          param_2 = (undefined8 *)0x0;
          local_160 = pEVar20;
          uVar34 = quotearg_style(4,local_140);
          uVar21 = dcgettext(0,"error reading %s",5);
          error(0,*(undefined4 *)local_160,uVar21,uVar34);
          goto LAB_00103512;
        }
        if (local_160 <= pEVar13) {
          if ((cVar29 == '\0') &&
             (local_150 = (Elf64_Ehdr *)
                          (local_160->e_ident_magic_str + (long)(local_150->e_ident_magic_str + -2))
             , local_160 != (Elf64_Ehdr *)0x0)) {
            xwrite_stdout_part_0((long)local_f8[(uint)uVar11 ^ 1] + 0x2000);
            param_2 = (undefined8 *)(ulong)local_153;
          }
          else {
            param_2 = (undefined8 *)(ulong)local_153;
          }
LAB_00103512:
          free(local_128);
LAB_00103443:
          if ((local_138 != (Elf64_Ehdr *)0xffffffffffffffff) &&
             (_Var16 = lseek((int)puVar30,(__off_t)local_150,0), _Var16 < 0)) {
            param_2 = (undefined8 *)0x0;
            elseek_part_0(local_150,0,pEVar19);
          }
          goto LAB_00102b57;
        }
        if (cVar29 != '\0') {
          local_150 = (Elf64_Ehdr *)((long)local_150 + ((long)local_160 - (long)pEVar13));
          if ((long)local_160 - (long)pEVar13 != 0) goto LAB_001035b1;
          goto LAB_00103512;
        }
        param_2 = (undefined8 *)(ulong)local_153;
        pEVar19 = local_160;
LAB_00103556:
        local_150 = (Elf64_Ehdr *)
                    (pEVar13->e_ident_magic_str + (long)(local_150->e_ident_magic_str + -2));
        if (((pEVar13 == (Elf64_Ehdr *)0x0) ||
            (local_160 = pEVar19, xwrite_stdout_part_0(), pEVar19 = local_160, pEVar13 < local_160))
           && (local_150 = (Elf64_Ehdr *)((long)local_150 + ((long)pEVar19 - (long)pEVar13)),
              (long)pEVar19 - (long)pEVar13 != 0)) {
LAB_001035b1:
          xwrite_stdout_part_0();
        }
        pEVar19 = local_140;
        if ((char)param_2 != '\0') goto LAB_00103512;
      }
      else {
        if (cVar29 == '\0') goto LAB_00103556;
        if ((pEVar13 < pEVar19) &&
           (local_150 = (Elf64_Ehdr *)((long)local_150 + ((long)pEVar19 - (long)pEVar13)),
           (long)pEVar19 - (long)pEVar13 != 0)) {
          param_2 = (undefined8 *)0x0;
          goto LAB_001035b1;
        }
      }
      uVar11 = (ulong)((uint)uVar11 ^ 1);
      param_2 = (undefined8 *)0x0;
      goto LAB_001034c1;
    }
    pEVar13 = (Elf64_Ehdr *)(local_e8.st_size - (long)local_138);
    if ((long)pEVar13 < 0) {
      pEVar13 = (Elf64_Ehdr *)0x0;
    }
    if ((local_170 < pEVar13) && (iVar8 = copy_fd(), iVar8 != 0)) {
      diagnose_copy_fd_failure();
    }
    else {
      param_2 = (undefined8 *)(ulong)local_153;
    }
    goto LAB_00102b57;
  }
  if (lVar18 < local_e8.st_size) {
    uVar7 = 1;
    if ((long)local_138 < local_e8.st_size) {
      bVar6 = elide_tail_lines_seekable
                        (pEVar19,(ulong)puVar30 & 0xffffffff,local_170,local_138,local_e8.st_size);
      uVar7 = (uint)bVar6;
    }
    param_2 = (undefined8 *)(ulong)(uVar7 & 1);
  }
  else {
LAB_00102eb6:
    local_140 = local_138;
    pEVar13 = (Elf64_Ehdr *)xmalloc(0x2020);
    *(undefined8 *)&pEVar13[0x80].e_type = 0;
    *(undefined8 *)&pEVar13[0x80].e_ident_abiversion = 0;
    pEVar13[0x80].e_entry = 0;
    local_150 = pEVar13;
    pEVar14 = (Elf64_Ehdr *)xmalloc(0x2020);
    local_160 = (Elf64_Ehdr *)0x0;
    puVar30 = (uint *)((ulong)puVar30 & 0xffffffff);
    while (pEVar20 = pEVar14, lVar18 = safe_read(puVar30,pEVar20,0x2000), cVar29 = line_end,
          lVar18 - 1U < 0xfffffffffffffffe) {
      pEVar14 = pEVar20;
      if (local_170 == (Elf64_Ehdr *)0x0) {
        local_140 = (Elf64_Ehdr *)(local_140->e_ident_magic_str + lVar18 + -1);
        xwrite_stdout_part_0();
      }
      else {
        iVar8 = (int)line_end;
        *(long *)&pEVar20[0x80].e_ident_abiversion = lVar18;
        *(undefined8 *)&pEVar20[0x80].e_type = 0;
        pEVar20[0x80].e_entry = 0;
        pEVar20->e_ident_magic_str[lVar18 + -1] = cVar29;
        pEVar26 = pEVar20;
        while (puVar15 = (undefined *)rawmemchr(pEVar26,iVar8), pEVar26 = local_150,
              puVar15 < pEVar20->e_ident_magic_str + lVar18 + -1) {
          *(long *)&pEVar20[0x80].e_type = *(long *)&pEVar20[0x80].e_type + 1;
          pEVar26 = (Elf64_Ehdr *)(puVar15 + 1);
        }
        pEVar31 = (Elf64_Ehdr *)(local_160->e_ident_magic_str + *(long *)&pEVar20[0x80].e_type + -1)
        ;
        local_160 = pEVar31;
        if (*(size_t *)&pEVar20[0x80].e_ident_abiversion +
            *(long *)&local_150[0x80].e_ident_abiversion < 0x2000) {
          memcpy(local_150->e_ident_magic_str + *(long *)&local_150[0x80].e_ident_abiversion + -1,
                 pEVar20,*(size_t *)&pEVar20[0x80].e_ident_abiversion);
          pbVar1 = &pEVar26[0x80].e_ident_abiversion;
          *(long *)pbVar1 = *(long *)pbVar1 + *(long *)&pEVar20[0x80].e_ident_abiversion;
          pwVar2 = &pEVar26[0x80].e_type;
          *(long *)pwVar2 = *(long *)pwVar2 + *(long *)&pEVar20[0x80].e_type;
        }
        else {
          local_150[0x80].e_entry = (qword)pEVar20;
          if (local_170 < (Elf64_Ehdr *)((long)pEVar31 - *(long *)&pEVar13[0x80].e_type)) {
            local_140 = (Elf64_Ehdr *)
                        (local_140->e_ident_magic_str +
                        *(long *)&pEVar13[0x80].e_ident_abiversion + -1);
            if (*(long *)&pEVar13[0x80].e_ident_abiversion != 0) {
              xwrite_stdout_part_0();
            }
            local_160 = (Elf64_Ehdr *)((long)pEVar31 - *(long *)&pEVar13[0x80].e_type);
            pEVar14 = pEVar13;
            pEVar13 = (Elf64_Ehdr *)pEVar13[0x80].e_entry;
            local_150 = pEVar20;
          }
          else {
            pEVar14 = (Elf64_Ehdr *)xmalloc();
            local_150 = pEVar20;
          }
        }
      }
    }
    free(pEVar20);
    pEVar14 = local_170;
    if (lVar18 == -1) {
      uVar34 = quotearg_style(4,pEVar19);
      uVar21 = dcgettext(0,"error reading %s",5);
      piVar10 = __errno_location();
      param_2 = (undefined8 *)0x0;
      error(0,*piVar10,uVar21,uVar34);
      goto joined_r0x001036e1;
    }
    if ((*(long *)&local_150[0x80].e_ident_abiversion != 0) &&
       (*(char *)((long)&local_150[-1].e_shstrndx + *(long *)&local_150[0x80].e_ident_abiversion + 1
                 ) != line_end)) {
      *(long *)&local_150[0x80].e_type = *(long *)&local_150[0x80].e_type + 1;
      local_160 = (Elf64_Ehdr *)local_160->e_ident_magic_str;
    }
    pEVar20 = pEVar13;
    if (local_170 < (Elf64_Ehdr *)((long)local_160 - *(long *)&pEVar13[0x80].e_type)) {
      local_150 = (Elf64_Ehdr *)((ulong)local_150 & 0xffffffff00000000 | (ulong)puVar30);
      pEVar26 = local_160;
      pEVar31 = local_140;
      do {
        pEVar31 = (Elf64_Ehdr *)
                  (pEVar31->e_ident_magic_str + *(long *)&pEVar20[0x80].e_ident_abiversion + -1);
        if (*(long *)&pEVar20[0x80].e_ident_abiversion != 0) {
          xwrite_stdout_part_0(pEVar20);
        }
        pEVar26 = (Elf64_Ehdr *)((long)pEVar26 - *(long *)&pEVar20[0x80].e_type);
        pEVar20 = (Elf64_Ehdr *)pEVar20[0x80].e_entry;
      } while (pEVar14 < (Elf64_Ehdr *)((long)pEVar26 - *(long *)&pEVar20[0x80].e_type));
      puVar30 = (uint *)((ulong)local_150 & 0xffffffff);
      local_160 = pEVar26;
      local_140 = pEVar31;
    }
    if (local_170 < local_160) {
      lVar18 = *(long *)&pEVar20[0x80].e_ident_abiversion;
      lVar27 = (long)local_160 - (long)local_170;
      iVar8 = (int)line_end;
      pEVar14 = pEVar20;
      local_160 = pEVar13;
      do {
        pEVar14 = (Elf64_Ehdr *)
                  memchr(pEVar14,iVar8,(size_t)((long)pEVar20 + (lVar18 - (long)pEVar14)));
        pEVar13 = local_160;
        if (pEVar14 == (Elf64_Ehdr *)0x0) break;
        *(long *)&pEVar20[0x80].e_type = *(long *)&pEVar20[0x80].e_type + 1;
        pEVar14 = (Elf64_Ehdr *)((long)pEVar14 + 1);
        lVar27 = lVar27 + -1;
      } while (lVar27 != 0);
      local_140 = (Elf64_Ehdr *)((long)local_140 + ((long)pEVar14 - (long)pEVar20));
      if ((long)pEVar14 - (long)pEVar20 != 0) {
        xwrite_stdout_part_0(pEVar20);
      }
    }
    param_2 = (undefined8 *)(ulong)local_153;
    do {
      pEVar14 = (Elf64_Ehdr *)pEVar13[0x80].e_entry;
      free(pEVar13);
      pEVar13 = pEVar14;
joined_r0x001036e1:
    } while (pEVar13 != (Elf64_Ehdr *)0x0);
    if ((local_138 != (Elf64_Ehdr *)0xffffffffffffffff) &&
       (_Var16 = lseek((int)puVar30,(__off_t)local_140,0), _Var16 < 0)) {
      param_2 = (undefined8 *)0x0;
      elseek_part_0(local_140,0,pEVar19);
    }
  }
  goto LAB_00102b57;
}