
struct s0 {
    uint16_t f0;
    signed char[42] pad44;
    signed char f2c;
    signed char[295] pad340;
    int32_t f154;
};

void** g28;

uint32_t* fun_27d0(int64_t rdi, int32_t* rsi);

void* fun_26f0();

int32_t* fun_2710(int32_t* rdi, int32_t* rsi, uint32_t* rdx);

void c_strtod();

int64_t rpl_fclose(uint32_t* rdi, int32_t* rsi, uint32_t* rdx);

void** fun_25a0();

void*** fun_24d0();

void fun_27b0();

int64_t fun_24b0(int32_t* rdi, int32_t* rsi);

void fun_2780(void** rdi, void** rsi, void** rdx, ...);

void** stdout = reinterpret_cast<void**>(0);

void fprintftime(void** rdi, void** rsi, int64_t rdx);

void** fun_27e0();

int32_t fun_2580();

uint64_t localtime_r;

int32_t ga0a7 = 0x6c20202c;

int32_t ga0ab = 0x2064616f;

void** ga0bd = reinterpret_cast<void**>(44);

void fun_2610();

void fun_25d0();

int32_t read_utmp();

int64_t quotearg_n_style_colon();

void fun_2810();

void print_uptime(int64_t rdi, struct s0* rsi, void* rdx, int64_t rcx, int32_t* r8) {
    void** rax6;
    void** v7;
    struct s0* rbx8;
    int32_t* rsi9;
    uint64_t rbp10;
    uint32_t* rax11;
    void* rsp12;
    int32_t* r14_13;
    void* rsp14;
    int32_t* r15_15;
    int32_t* rax16;
    void* rsp17;
    int32_t* v18;
    int32_t* v19;
    uint64_t r13_20;
    void** r12_21;
    int32_t v22;
    int32_t* v23;
    uint32_t eax24;
    int1_t cf25;
    int1_t cf26;
    void* rax27;
    void* rsp28;
    void** rdx29;
    int64_t rdx30;
    int64_t rax31;
    void* rsp32;
    void** rax33;
    void* rsp34;
    void** rax35;
    void** rdi36;
    void** rax37;
    void* rsp38;
    void** rax39;
    void** rax40;
    void** rdx41;
    void** rax42;
    void** rdx43;
    void* rsp44;
    void** rdi45;
    void** rsi46;
    int32_t eax47;
    void** rax48;
    void** rax49;
    void* rax50;
    int64_t rcx51;
    int32_t eax52;
    int64_t v53;

    rax6 = g28;
    v7 = rax6;
    rbx8 = rsi;
    rsi9 = reinterpret_cast<int32_t*>("r");
    rbp10 = reinterpret_cast<uint64_t>(rdi - 1);
    rax11 = fun_27d0("/proc/uptime", "r");
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 88 - 8 + 8);
    if (!rax11) {
        addr_2b47_2:
        *reinterpret_cast<int32_t*>(&r14_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_13) + 4) = 0;
        if (!rdi) {
            fun_26f0();
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            goto addr_2f1a_4;
        }
    } else {
        r15_15 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp12) + 64);
        *reinterpret_cast<int32_t*>(&rsi9) = 0x2000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax16 = fun_2710(r15_15, 0x2000, rax11);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        r14_13 = rax16;
        if (rax16 != r15_15 || (r8 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp17) + 24), rsi9 = r8, c_strtod(), rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8), v18 == r14_13)) {
            rpl_fclose(rax11, rsi9, rax11);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            goto addr_2b47_2;
        } else {
            __asm__("comisd xmm0, [rip+0x760c]");
            if (reinterpret_cast<uint64_t>(v19) < reinterpret_cast<uint64_t>(r14_13)) {
                addr_2ed7_8:
                r14_13 = reinterpret_cast<int32_t*>(0xffffffffffffffff);
                rpl_fclose(rax11, rsi9, rax11);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                r8 = r8;
                if (rdi) {
                    addr_2b53_9:
                    *reinterpret_cast<int32_t*>(&r13_20) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_20) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r12_21) = 0;
                    *reinterpret_cast<int32_t*>(&r12_21 + 4) = 0;
                } else {
                    fun_26f0();
                    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                    r8 = r8;
                    goto addr_2ecf_11;
                }
            } else {
                *rax16 = v22;
                ++rsi9;
                __asm__("comisd xmm1, xmm0");
                if (reinterpret_cast<uint64_t>(v23) <= reinterpret_cast<uint64_t>(r14_13)) 
                    goto addr_2ed7_8; else 
                    goto addr_2e99_16;
            }
        }
    }
    while (1) {
        eax24 = rbx8->f0;
        if (!rbx8->f2c || *reinterpret_cast<int16_t*>(&eax24) != 7) {
            if (*reinterpret_cast<int16_t*>(&eax24) == 2) {
                r13_20 = reinterpret_cast<uint64_t>(static_cast<int64_t>(rbx8->f154));
            }
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx8) + 0x180);
            cf25 = rbp10 < 1;
            --rbp10;
            if (cf25) 
                break;
        } else {
            ++r12_21;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx8) + 0x180);
            cf26 = rbp10 < 1;
            --rbp10;
            if (cf26) 
                goto addr_2b9a_22;
        }
    }
    addr_2ba0_23:
    rax27 = fun_26f0();
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    if (r14_13 || (r14_13 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rax27) - r13_20), !!r13_20)) {
        r8 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp14) + 24);
    } else {
        addr_2f1a_4:
        fun_25a0();
        fun_24d0();
        fun_27b0();
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_2f46_25;
    }
    addr_2bc5_26:
    rdx29 = reinterpret_cast<void**>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r14_13) >> 63));
    rdx30 = (__intrinsic() >> 10) - (reinterpret_cast<int64_t>(r14_13 + reinterpret_cast<unsigned char>(rdx29) * 0xffffffffffffaba0) >> 63);
    rax31 = fun_24b0(r8, rsi9);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    if (!rax31) {
        rax33 = fun_25a0();
        fun_2780(1, rax33, 5);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
    } else {
        rax35 = fun_25a0();
        rdi36 = stdout;
        fprintftime(rdi36, rax35, rax31);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
    }
    if (r14_13 == 0xffffffffffffffff) {
        rax37 = fun_25a0();
        fun_2780(1, rax37, 5);
        rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
    } else {
        if (reinterpret_cast<int64_t>(r14_13) > reinterpret_cast<int64_t>(0x1517f)) {
            rax39 = fun_27e0();
            fun_2780(1, rax39, rdx29, 1, rax39, rdx29);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
        } else {
            rax40 = fun_25a0();
            *reinterpret_cast<int32_t*>(&rdx41) = *reinterpret_cast<int32_t*>(&rdx30);
            *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
            fun_2780(1, rax40, rdx41, 1, rax40, rdx41);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
        }
    }
    *reinterpret_cast<int32_t*>(&r8) = 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8) + 4) = 0;
    rax42 = fun_27e0();
    rdx43 = r12_21;
    fun_2780(1, rax42, rdx43, 1, rax42, rdx43);
    rsp44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8 - 8 + 8);
    rdi45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp44) + 32);
    *reinterpret_cast<int32_t*>(&rsi46) = 3;
    *reinterpret_cast<int32_t*>(&rsi46 + 4) = 0;
    eax47 = fun_2580();
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
    if (eax47 == -1) 
        goto addr_2dee_35;
    if (!(reinterpret_cast<uint1_t>(eax47 < 0) | reinterpret_cast<uint1_t>(eax47 == 0))) {
        *reinterpret_cast<int32_t*>(&rdx43) = 5;
        *reinterpret_cast<int32_t*>(&rdx43 + 4) = 0;
        *reinterpret_cast<int32_t*>(&localtime_r) = ga0a7;
        rax48 = fun_25a0();
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&localtime_r) + 4) = ga0ab;
        *reinterpret_cast<int32_t*>(&rdi45) = 1;
        *reinterpret_cast<int32_t*>(&rdi45 + 4) = 0;
        rsi46 = rax48;
        fun_2780(1, rsi46, 5, 1, rsi46, 5);
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
    }
    if (eax47 <= 1) 
        goto addr_2d48_48;
    *reinterpret_cast<void***>(rdi45) = *reinterpret_cast<void***>(rsi46);
    rsi46 = reinterpret_cast<void**>(", %.2f");
    fun_2780(1, ", %.2f", rdx43, 1, ", %.2f", rdx43);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
    if (eax47 == 2) 
        goto addr_2dee_35;
    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&localtime_r) + 1) = ga0bd;
    rsi46 = reinterpret_cast<void**>(", %.2f");
    fun_2780(1, ", %.2f", rdx43, 1, ", %.2f", rdx43);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
    goto addr_2dee_35;
    addr_2d48_48:
    if (eax47 == 1) {
        addr_2dee_35:
        rdi45 = stdout;
        rax49 = *reinterpret_cast<void***>(rdi45 + 40);
        if (reinterpret_cast<unsigned char>(rax49) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi45 + 48))) {
            addr_2f46_25:
            *reinterpret_cast<int32_t*>(&rsi46) = 10;
            fun_2610();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            goto addr_2d4e_57;
        } else {
            *reinterpret_cast<void***>(rdi45 + 40) = rax49 + 1;
            *reinterpret_cast<void***>(rax49) = reinterpret_cast<void**>(10);
            goto addr_2d4e_57;
        }
    } else {
        addr_2d4e_57:
        rax50 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (!rax50) {
            return;
        }
    }
    fun_25d0();
    *reinterpret_cast<int32_t*>(&rcx51) = *reinterpret_cast<int32_t*>(&rsi46);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx51) + 4) = 0;
    eax52 = read_utmp();
    if (!eax52) 
        goto addr_2f97_62;
    addr_2fad_63:
    quotearg_n_style_colon();
    fun_24d0();
    fun_27b0();
    addr_2f97_62:
    print_uptime(v53, 0, reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 - 32 + 16, rcx51, r8);
    fun_2810();
    goto addr_2fad_63;
    addr_2b9a_22:
    goto addr_2ba0_23;
    addr_2ecf_11:
    *reinterpret_cast<int32_t*>(&r12_21) = 0;
    *reinterpret_cast<int32_t*>(&r12_21 + 4) = 0;
    goto addr_2bc5_26;
    addr_2e99_16:
    __asm__("cvttsd2si r14, xmm0");
    rpl_fclose(rax11, rsi9, rax11);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
    r8 = r8;
    if (rdi) 
        goto addr_2b53_9;
    fun_26f0();
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    r8 = r8;
    if (!r14_13) 
        goto addr_2f1a_4; else 
        goto addr_2ecf_11;
}

void fun_2650(void** rdi, void** rsi, ...);

struct s1 {
    signed char[1] pad1;
    void** f1;
};

void fun_2820(void** rdi, void** rsi, int64_t rdx, void** rcx);

void**** fun_2470(void** rdi, void** rsi, ...);

/* __strftime_internal.isra.0 */
void* __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, unsigned char cl, void** r8d, int32_t r9d, int64_t a7, int64_t a8, int64_t a9) {
    void** r14_10;
    void** r13_11;
    void** r12_12;
    void** rbx13;
    unsigned char v14;
    void** rax15;
    void** v16;
    void*** rax17;
    void* rsp18;
    void** rdi19;
    void** ecx20;
    void*** v21;
    void** eax22;
    void** v23;
    uint32_t eax24;
    void* r15_25;
    void* rax26;
    int32_t ebp27;
    void** rax28;
    signed char* rbp29;
    signed char* r13_30;
    uint32_t eax31;
    void** v32;
    int64_t rdx33;
    int32_t ecx34;
    uint64_t rax35;
    struct s1* rax36;
    struct s1* v37;
    void** rbp38;
    void** v39;
    void** rax40;
    void* r13_41;
    void**** rax42;
    void**** r13_43;
    void** rbp44;
    int64_t rdx45;
    void** v46;
    void* rbx47;
    void* rbx48;
    void* rbp49;
    void** v50;
    void**** rax51;
    unsigned char* rdx52;
    int32_t v53;
    unsigned char* r12_54;
    unsigned char* rbx55;
    void**** rbp56;
    void** r13_57;
    int32_t r14d58;
    int64_t rsi59;
    unsigned char v60;
    void* r12_61;
    int64_t rax62;

    r14_10 = rdi;
    r13_11 = reinterpret_cast<void**>(static_cast<int64_t>(r9d));
    r12_12 = rsi;
    rbx13 = rdx;
    v14 = cl;
    rax15 = g28;
    v16 = rax15;
    rax17 = fun_24d0();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    rdi19 = *reinterpret_cast<void***>(rbx13 + 48);
    ecx20 = *reinterpret_cast<void***>(rbx13 + 8);
    v21 = rax17;
    eax22 = *rax17;
    v23 = eax22;
    if (rdi19) {
    }
    if (reinterpret_cast<signed char>(ecx20) <= reinterpret_cast<signed char>(12)) {
        rsi = ecx20;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        if (rsi) {
        }
    }
    eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_12));
    *reinterpret_cast<int32_t*>(&r15_25) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_25) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax24)) 
        goto addr_3635_9;
    while (1) {
        addr_3692_10:
        rsi = v23;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *v21 = rsi;
        while (rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28)), !!rax26) {
            fun_25d0();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            if (!r8d) {
                r8d = reinterpret_cast<void**>(48);
            }
            if (reinterpret_cast<unsigned char>(~reinterpret_cast<uint64_t>(r15_25)) <= reinterpret_cast<unsigned char>(0)) 
                goto addr_36a8_17;
            r15_25 = r15_25;
            if (!r14_10) {
                addr_3e0c_19:
                if (r8d == 45 || (ebp27 = *reinterpret_cast<int32_t*>(&r13_11) - *reinterpret_cast<int32_t*>(&r12_12), ebp27 < 0)) {
                    addr_369f_20:
                    if (!reinterpret_cast<int1_t>(r15_25 == 0xffffffffffffffff)) {
                        addr_367f_21:
                        while (eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1)), r12_12 = rbx13 + 1, r13_11 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax24)) {
                            addr_3635_9:
                            if (*reinterpret_cast<signed char*>(&eax24) != 37) {
                                *reinterpret_cast<int32_t*>(&rax28) = 0;
                                *reinterpret_cast<int32_t*>(&rax28 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rbx13) = 1;
                                *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                                if (*reinterpret_cast<int32_t*>(&r13_11) >= 0) {
                                    rax28 = r13_11;
                                }
                                if (rax28) {
                                    rbx13 = rax28;
                                }
                                if (reinterpret_cast<unsigned char>(rbx13) >= reinterpret_cast<unsigned char>(~reinterpret_cast<uint64_t>(r15_25))) 
                                    goto addr_36a8_17;
                                if (r14_10) {
                                    if (*reinterpret_cast<int32_t*>(&r13_11) > 1) {
                                        rbp29 = reinterpret_cast<signed char*>(rax28 + 0xffffffffffffffff);
                                        *reinterpret_cast<int32_t*>(&r13_30) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_30) + 4) = 0;
                                        do {
                                            ++r13_30;
                                            fun_2650(32, r14_10, 32, r14_10);
                                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        } while (reinterpret_cast<uint64_t>(rbp29) > reinterpret_cast<uint64_t>(r13_30));
                                    }
                                    rdi19 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_12))));
                                    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                    rsi = r14_10;
                                    fun_2650(rdi19, rsi, rdi19, rsi);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                }
                                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = r12_12;
                                continue;
                            }
                            eax31 = v14;
                            rbx13 = r12_12;
                            r8d = reinterpret_cast<void**>(0);
                            *reinterpret_cast<signed char*>(&v32) = *reinterpret_cast<signed char*>(&eax31);
                            while (*reinterpret_cast<void***>(&rdx33) = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx13 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx33) + 4) = 0, ++rbx13, ecx34 = static_cast<int32_t>(rdx33 - 35), rsi = *reinterpret_cast<void***>(&rdx33), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rdi19 = *reinterpret_cast<void***>(&rdx33), *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0, *reinterpret_cast<unsigned char*>(&ecx34) <= 60) {
                                rax35 = 1 << *reinterpret_cast<unsigned char*>(&ecx34);
                                if (rax35 & 0x1000000000002500) {
                                    r8d = *reinterpret_cast<void***>(&rdx33);
                                } else {
                                    if (*reinterpret_cast<unsigned char*>(&ecx34) == 59) {
                                        *reinterpret_cast<signed char*>(&v32) = 1;
                                    } else {
                                        if (!(*reinterpret_cast<uint32_t*>(&rax35) & 1)) 
                                            break;
                                    }
                                }
                            }
                            if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rdx33) - 48) > 9) 
                                goto addr_378e_43;
                            *reinterpret_cast<int32_t*>(&r13_11) = 0;
                            do {
                                if (__intrinsic() || (*reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&r13_11) * 10 + reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rbx13) - 48), __intrinsic())) {
                                    *reinterpret_cast<int32_t*>(&r13_11) = 0x7fffffff;
                                }
                                rdi19 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx13 + 1))));
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                ++rbx13;
                                rsi = rdi19;
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            } while (static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdi19 + 0xffffffffffffffd0)) <= 9);
                            addr_378e_43:
                            if (*reinterpret_cast<unsigned char*>(&rsi) == 69 || *reinterpret_cast<unsigned char*>(&rsi) == 79) {
                                rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1))));
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                ++rbx13;
                            } else {
                                rdi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                            }
                            if (*reinterpret_cast<unsigned char*>(&rsi) <= 0x7a) 
                                goto addr_37aa_51;
                            rax36 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(r12_12));
                            v37 = rax36;
                            rbp38 = reinterpret_cast<void**>(&rax36->f1);
                            if (r8d == 45 || *reinterpret_cast<int32_t*>(&r13_11) < 0) {
                                v39 = rbp38;
                                *reinterpret_cast<int32_t*>(&r13_11) = 0;
                                *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
                            } else {
                                r13_11 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_11)));
                                rax40 = r13_11;
                                if (reinterpret_cast<unsigned char>(rbp38) >= reinterpret_cast<unsigned char>(r13_11)) {
                                    rax40 = rbp38;
                                }
                                v39 = rax40;
                            }
                            if (reinterpret_cast<unsigned char>(~reinterpret_cast<uint64_t>(r15_25)) <= reinterpret_cast<unsigned char>(v39)) 
                                goto addr_36a8_17;
                            if (!r14_10) {
                                addr_38ad_59:
                                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<unsigned char>(v39));
                                continue;
                            } else {
                                if (reinterpret_cast<unsigned char>(rbp38) >= reinterpret_cast<unsigned char>(r13_11)) 
                                    goto addr_386b_61;
                                r13_41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_11) - reinterpret_cast<unsigned char>(rbp38));
                                if (r8d == 48) 
                                    goto addr_4a59_63;
                                if (r8d != 43) 
                                    goto addr_383e_65;
                            }
                            addr_4a59_63:
                            if (!r13_41) {
                                addr_386b_61:
                                if (!*reinterpret_cast<signed char*>(&v32)) {
                                    rsi = rbp38;
                                    rdi19 = r12_12;
                                    fun_2820(rdi19, rsi, 1, r14_10);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                    goto addr_38ad_59;
                                } else {
                                    if (rbp38) {
                                        rax42 = fun_2470(rdi19, rsi);
                                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        r13_43 = rax42;
                                        rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_12) + reinterpret_cast<uint64_t>(v37) + 1);
                                        do {
                                            *reinterpret_cast<uint32_t*>(&rdx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_12));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx45) + 4) = 0;
                                            rsi = r14_10;
                                            ++r12_12;
                                            rdi19 = (*r13_43)[rdx45 * 4];
                                            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                            fun_2650(rdi19, rsi);
                                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        } while (r12_12 != rbp44);
                                        goto addr_38ad_59;
                                    }
                                }
                            } else {
                                v46 = rbx13;
                                rbx47 = reinterpret_cast<void*>(0);
                                do {
                                    rsi = r14_10;
                                    rdi19 = reinterpret_cast<void**>(48);
                                    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                    rbx47 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx47) + 1);
                                    fun_2650(48, rsi);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                } while (r13_41 != rbx47);
                            }
                            addr_3866_73:
                            rbx13 = v46;
                            goto addr_386b_61;
                            addr_383e_65:
                            if (!r13_41) 
                                goto addr_386b_61;
                            v46 = rbx13;
                            rbx48 = reinterpret_cast<void*>(0);
                            do {
                                rsi = r14_10;
                                rdi19 = reinterpret_cast<void**>(32);
                                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                                rbx48 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx48) + 1);
                                fun_2650(32, rsi);
                                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                            } while (r13_41 != rbx48);
                            goto addr_3866_73;
                        }
                        goto addr_3692_10;
                    } else {
                        goto addr_36a8_17;
                    }
                } else {
                    rbp49 = reinterpret_cast<void*>(static_cast<int64_t>(ebp27));
                    if (reinterpret_cast<uint64_t>(rbp49) >= ~reinterpret_cast<uint64_t>(r15_25)) {
                        addr_36a8_17:
                        *v21 = reinterpret_cast<void**>(34);
                    } else {
                        if (!r14_10) {
                            r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<uint64_t>(rbp49));
                            goto addr_367f_21;
                        }
                    }
                }
            } else {
                if (!*reinterpret_cast<signed char*>(&v32)) {
                    rsi = reinterpret_cast<void**>(0);
                    v32 = r8d;
                    rdi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 0xb0);
                    fun_2820(rdi19, 0, 1, r14_10);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    r8d = v32;
                    goto addr_3e0c_19;
                } else {
                    if (1) {
                        if (r8d == 45) 
                            goto addr_369f_20;
                        rbp49 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_11) - *reinterpret_cast<int32_t*>(&r12_12)));
                        if (~reinterpret_cast<uint64_t>(r15_25) <= reinterpret_cast<uint64_t>(rbp49)) {
                            goto addr_36a8_17;
                        }
                    } else {
                        v50 = r8d;
                        rax51 = fun_2470(rdi19, rsi, rdi19, rsi);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        rdx52 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp18) + 0xb0);
                        v32 = rbx13;
                        v53 = *reinterpret_cast<int32_t*>(&r12_12);
                        r12_54 = rdx52;
                        rbx55 = rdx52;
                        rbp56 = rax51;
                        r13_57 = r14_10;
                        r14d58 = *reinterpret_cast<int32_t*>(&r13_11);
                        do {
                            *reinterpret_cast<uint32_t*>(&rsi59) = v60;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                            ++r12_54;
                            rdi19 = (*rbp56)[rsi59 * 4];
                            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                            rsi = r13_57;
                            fun_2650(rdi19, rsi);
                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        } while (rbx55 != r12_54);
                        rbx13 = v32;
                        *reinterpret_cast<int32_t*>(&r12_12) = v53;
                        r14_10 = r13_57;
                        r8d = v50;
                        *reinterpret_cast<int32_t*>(&r13_11) = r14d58;
                        goto addr_3e0c_19;
                    }
                }
            }
            *reinterpret_cast<int32_t*>(&r15_25) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_25) + 4) = 0;
            continue;
            if (rbp49) {
                if (r8d == 48 || (*reinterpret_cast<int32_t*>(&r12_61) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_61) + 4) = 0, r8d == 43)) {
                    *reinterpret_cast<int32_t*>(&r12_61) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_61) + 4) = 0;
                    do {
                        rsi = r14_10;
                        rdi19 = reinterpret_cast<void**>(48);
                        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                        r12_61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_61) + 1);
                        fun_2650(48, rsi, 48, rsi);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    } while (r12_61 != rbp49);
                } else {
                    do {
                        rsi = r14_10;
                        rdi19 = reinterpret_cast<void**>(32);
                        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                        r12_61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_61) + 1);
                        fun_2650(32, rsi, 32, rsi);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    } while (r12_61 != rbp49);
                }
                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<uint64_t>(r12_61));
                goto addr_367f_21;
            }
        }
        break;
    }
    return r15_25;
    addr_37aa_51:
    *reinterpret_cast<uint32_t*>(&rax62) = *reinterpret_cast<unsigned char*>(&rsi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa4e4 + rax62 * 4) + 0xa4e4;
}

int64_t fun_25b0();

int64_t fun_24c0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_25b0();
    if (r8d > 10) {
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa7a0 + rax11 * 4) + 0xa7a0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2640();

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_24a0(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    int64_t r8_15;
    struct s3* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    int64_t r9_23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x6a1f;
    rax8 = fun_24d0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
        fun_24c0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x74b1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x6aab;
            fun_2640();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx16->f0 = rsi25;
            if (r14_19 != 0xe100) {
                fun_24a0(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x6b3a);
        }
        *rax8 = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_25d0();
        } else {
            return r14_19;
        }
    }
}

void** fun_2490(int64_t rdi);

int32_t fun_2690(void** rdi, void** rsi, void** rdx);

void** tzalloc(void** rdi, void** rsi);

int32_t fun_27f0(int64_t rdi, void** rsi);

int32_t fun_2530(int64_t rdi, void** rsi, int64_t rdx);

void fun_26d0(int64_t rdi, void** rsi, int64_t rdx);

void** set_tz(void** rdi) {
    void** rax2;
    void** rsi3;
    void** rdx4;
    int32_t eax5;
    void** rax6;
    void** r12_7;
    int32_t eax8;
    int32_t eax9;
    void*** rax10;
    void** ebx11;
    void*** rbp12;
    void** rdi13;
    int64_t rcx14;
    int64_t r8_15;
    int64_t r9_16;

    rax2 = fun_2490("TZ");
    if (!rax2) {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            return 1;
        }
    } else {
        if (*reinterpret_cast<void***>(rdi + 8) && (rsi3 = rax2, eax5 = fun_2690(rdi + 9, rsi3, rdx4), !eax5)) {
            return 1;
        }
    }
    rax6 = tzalloc(rax2, rsi3);
    r12_7 = rax6;
    if (!rax6) {
        addr_7c71_7:
        return r12_7;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            eax8 = fun_27f0("TZ", rsi3);
            if (eax8) 
                goto addr_7cdd_10; else 
                goto addr_7c6c_11;
        }
        rsi3 = rdi + 9;
        eax9 = fun_2530("TZ", rsi3, 1);
        if (!eax9) {
            addr_7c6c_11:
            fun_26d0("TZ", rsi3, 1);
            goto addr_7c71_7;
        } else {
            addr_7cdd_10:
            rax10 = fun_24d0();
            ebx11 = *rax10;
            rbp12 = rax10;
            if (r12_7 != 1) {
                do {
                    rdi13 = r12_7;
                    r12_7 = *reinterpret_cast<void***>(r12_7);
                    fun_24a0(rdi13, rsi3, 1, rcx14, r8_15, r9_16);
                } while (r12_7);
            }
        }
    }
    *rbp12 = ebx11;
    return 0;
}

void** fun_25c0(void** rdi, ...);

void fun_26c0(void** rdi, void** rsi, void** rdx);

signed char save_abbr(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** r13_5;
    void** rdx6;
    int32_t eax7;
    void** rbx8;
    void** rsi9;
    int32_t eax10;
    void** rax11;
    int32_t eax12;
    void** rax13;
    void** rdx14;
    void** rax15;

    r12_3 = *reinterpret_cast<void***>(rsi + 48);
    if (!r12_3) {
        return 1;
    }
    rbp4 = rdi;
    r13_5 = rsi;
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(r12_3) || (rdx6 = rsi + 56, eax7 = 1, reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rdx6))) {
        rbx8 = rbp4 + 9;
        if (!*reinterpret_cast<void***>(r12_3)) {
            rbx8 = reinterpret_cast<void**>(0xab64);
        } else {
            while (rsi9 = r12_3, eax10 = fun_2690(rbx8, rsi9, rdx6), !!eax10) {
                do {
                    if (*reinterpret_cast<void***>(rbx8)) 
                        goto addr_7b53_9;
                    if (rbx8 != rbp4 + 9) 
                        goto addr_7bc0_11;
                    if (!*reinterpret_cast<void***>(rbp4 + 8)) 
                        goto addr_7bc0_11;
                    addr_7b53_9:
                    rax11 = fun_25c0(rbx8, rbx8);
                    rbx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax11) + 1);
                    if (*reinterpret_cast<void***>(rbx8)) 
                        break;
                    if (!*reinterpret_cast<void***>(rbp4)) 
                        break;
                    rbx8 = *reinterpret_cast<void***>(rbp4) + 9;
                    rsi9 = r12_3;
                    rbp4 = *reinterpret_cast<void***>(rbp4);
                    eax12 = fun_2690(rbx8, rsi9, rdx6);
                } while (eax12);
                goto addr_7b84_15;
            }
        }
    } else {
        addr_7b91_16:
        return *reinterpret_cast<signed char*>(&eax7);
    }
    addr_7b88_17:
    *reinterpret_cast<void***>(r13_5 + 48) = rbx8;
    eax7 = 1;
    goto addr_7b91_16;
    addr_7bc0_11:
    rax13 = fun_25c0(r12_3, r12_3);
    rdx14 = rax13 + 1;
    if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbp4 + 0x80) - reinterpret_cast<unsigned char>(rbx8)) <= reinterpret_cast<signed char>(rdx14)) {
        rax15 = tzalloc(r12_3, rsi9);
        *reinterpret_cast<void***>(rbp4) = rax15;
        if (!rax15) {
            eax7 = 0;
            goto addr_7b91_16;
        } else {
            *reinterpret_cast<void***>(rax15 + 8) = reinterpret_cast<void**>(0);
            rbx8 = rax15 + 9;
            goto addr_7b88_17;
        }
    } else {
        fun_26c0(rbx8, r12_3, rdx14);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax13) + 1) = 0;
        goto addr_7b88_17;
    }
    addr_7b84_15:
    goto addr_7b88_17;
}

/* revert_tz.part.0 */
signed char revert_tz_part_0(void** rdi, void** rsi) {
    void** rbx3;
    void*** rax4;
    void** r12d5;
    void*** rbp6;
    int32_t eax7;
    int32_t r13d8;
    void** rdi9;
    int64_t rcx10;
    int64_t r8_11;
    int64_t r9_12;
    int32_t eax13;
    int32_t eax14;

    rbx3 = rdi;
    rax4 = fun_24d0();
    r12d5 = *rax4;
    rbp6 = rax4;
    if (*reinterpret_cast<void***>(rbx3 + 8)) {
        rsi = rbx3 + 9;
        eax7 = fun_2530("TZ", rsi, 1);
        if (eax7) {
            addr_79ee_3:
            r12d5 = *rbp6;
            r13d8 = 0;
        } else {
            addr_7a39_4:
            fun_26d0("TZ", rsi, 1);
            r13d8 = 1;
        }
        do {
            rdi9 = rbx3;
            rbx3 = *reinterpret_cast<void***>(rbx3);
            fun_24a0(rdi9, rsi, 1, rcx10, r8_11, r9_12);
        } while (rbx3);
        *rbp6 = r12d5;
        eax13 = r13d8;
        return *reinterpret_cast<signed char*>(&eax13);
    } else {
        eax14 = fun_27f0("TZ", rsi);
        if (!eax14) 
            goto addr_7a39_4; else 
            goto addr_79ee_3;
    }
}

uint64_t ydhms_diff(int64_t rdi, int64_t rsi, int32_t edx, uint32_t ecx, int32_t r8d, int32_t r9d, int32_t a7, int32_t a8, int32_t a9, int32_t a10) {
    int64_t rcx11;
    uint64_t rcx12;
    int64_t rax13;
    int64_t rbx14;
    int64_t rdx15;
    int64_t rax16;
    int64_t r9_17;
    int64_t r9_18;
    int64_t rbp19;
    int64_t rdi20;
    int32_t ecx21;
    int64_t rdx22;
    uint32_t edx23;
    int64_t rbp24;
    int32_t r12d25;
    int64_t rdx26;
    int64_t rcx27;
    uint32_t ecx28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t rax31;
    int64_t rax32;
    int64_t rax33;

    rcx11 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx11) & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
    rax13 = rdi >> 2;
    rbx14 = r9d;
    rdx15 = rbx14;
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax13) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax13) < 0x1db - reinterpret_cast<uint1_t>(rcx12 < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    r9_17 = rbx14 >> 2;
    *reinterpret_cast<uint32_t*>(&r9_18) = *reinterpret_cast<uint32_t*>(&r9_17) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_17) < 0x1db - reinterpret_cast<uint1_t>((*reinterpret_cast<uint32_t*>(&rdx15) & 3) < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_18) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbp19) = *reinterpret_cast<uint32_t*>(&rax16) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    rdi20 = rdi - rbx14;
    ecx21 = static_cast<int32_t>(rbp19 + rax16);
    rdx22 = ecx21 * 0x51eb851f >> 35;
    edx23 = *reinterpret_cast<int32_t*>(&rdx22) - (ecx21 >> 31) - *reinterpret_cast<uint32_t*>(&rbp19);
    *reinterpret_cast<uint32_t*>(&rbp24) = *reinterpret_cast<uint32_t*>(&r9_18) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    r12d25 = static_cast<int32_t>(rbp24 + r9_18);
    rdx26 = static_cast<int64_t>(reinterpret_cast<int32_t>(edx23)) >> 2;
    rcx27 = r12d25 * 0x51eb851f >> 35;
    ecx28 = *reinterpret_cast<int32_t*>(&rcx27) - (r12d25 >> 31) - *reinterpret_cast<uint32_t*>(&rbp24);
    rcx29 = static_cast<int64_t>(reinterpret_cast<int32_t>(ecx28)) >> 2;
    rdx30 = rdi20 + (rdi20 + rdi20 * 8) * 8;
    rax31 = reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax16) - *reinterpret_cast<uint32_t*>(&r9_18) - (edx23 - ecx28) + (*reinterpret_cast<int32_t*>(&rdx26) - *reinterpret_cast<uint32_t*>(&rcx29))) + (rdx30 + rdx30 * 4 + rsi - a7);
    rax32 = edx + (rax31 + rax31 * 2) * 8 - a8;
    rax33 = reinterpret_cast<int32_t>(ecx) + ((rax32 << 4) - rax32) * 4 - a9;
    return r8d + ((rax33 << 4) - rax33) * 4 - reinterpret_cast<uint64_t>(static_cast<int64_t>(a10));
}

struct s4 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
    int64_t f30;
};

struct s4* ranged_convert(int64_t rdi, uint64_t* rsi, struct s4* rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rbx7;
    uint64_t r12_8;
    uint64_t* v9;
    void* rbp10;
    void** rax11;
    void** v12;
    struct s4* rax13;
    struct s4* v14;
    void*** rax15;
    int1_t zf16;
    void*** v17;
    uint64_t rcx18;
    int64_t rcx19;
    uint64_t r13_20;
    void* rax21;
    int32_t v22;
    uint64_t r14_23;
    uint64_t r12_24;
    uint64_t r13_25;
    struct s4* r15_26;
    int64_t rax27;
    int32_t v28;
    int32_t v29;
    int32_t v30;
    int32_t v31;
    int32_t v32;
    int32_t v33;
    int32_t v34;
    int32_t v35;
    int64_t v36;
    int64_t v37;
    uint64_t rax38;
    uint64_t rax39;

    rbx7 = rdi;
    r12_8 = *rsi;
    v9 = rsi;
    rbp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 + 80);
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<struct s4*>(rbx7(rbp10, rdx));
    v14 = rax13;
    if (!rax13) {
        rax15 = fun_24d0();
        zf16 = reinterpret_cast<int1_t>(*rax15 == 75);
        v17 = rax15;
        if (!zf16 || ((rcx18 = r12_8, *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<uint32_t*>(&rcx18) & 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0, r13_20 = rcx19 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_8) >> 1), r12_8 == r13_20) || !r13_20)) {
            addr_8d7e_3:
            rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
            if (rax21) {
                fun_25d0();
            } else {
                return v14;
            }
        } else {
            v22 = -1;
            r14_23 = r12_8;
            r12_24 = r13_20;
            r13_25 = 0;
            r15_26 = rdx;
            do {
                rax27 = reinterpret_cast<int64_t>(rbx7(rbp10, r15_26));
                if (rax27) {
                    r13_25 = r12_24;
                    v22 = r15_26->f0;
                    v28 = r15_26->f4;
                    v29 = r15_26->f8;
                    v30 = r15_26->fc;
                    v31 = r15_26->f10;
                    v32 = r15_26->f14;
                    v33 = r15_26->f18;
                    v34 = r15_26->f1c;
                    v35 = r15_26->f20;
                    v36 = r15_26->f28;
                    v37 = r15_26->f30;
                } else {
                    if (!reinterpret_cast<int1_t>(*v17 == 75)) 
                        goto addr_8d7e_3;
                    r14_23 = r12_24;
                }
                rax38 = r13_25 | r14_23;
                *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
                r12_24 = (reinterpret_cast<int64_t>(r13_25) >> 1) + (reinterpret_cast<int64_t>(r14_23) >> 1) + rax39;
            } while (r12_24 != r13_25 && r12_24 != r14_23);
        }
        if (v22 >= 0) {
            v14 = r15_26;
            *v9 = r13_25;
            r15_26->f0 = v22;
            r15_26->f4 = v28;
            r15_26->f8 = v29;
            r15_26->fc = v30;
            r15_26->f10 = v31;
            r15_26->f14 = v32;
            r15_26->f18 = v33;
            r15_26->f1c = v34;
            r15_26->f20 = v35;
            r15_26->f28 = v36;
            r15_26->f30 = v37;
            goto addr_8d7e_3;
        }
    } else {
        *rsi = r12_8;
        goto addr_8d7e_3;
    }
}

int64_t uptime(int64_t rdi, int32_t esi, void** rdx, int64_t rcx, int32_t* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rcx11;
    int32_t eax12;
    int64_t v13;

    *reinterpret_cast<int32_t*>(&rcx11) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
    eax12 = read_utmp();
    if (!eax12) {
        print_uptime(v13, 0, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32 + 16, rcx11, r8);
        fun_2810();
    }
    quotearg_n_style_colon();
    fun_24d0();
    fun_27b0();
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s5 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s5* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s5* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xa72b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xa724);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xa72f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xa720);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gddb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gddb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t localtime_r = 0;

void fun_2443() {
    __asm__("cli ");
    goto localtime_r;
}

int64_t gmtime_r = 0;

void fun_2453() {
    __asm__("cli ");
    goto gmtime_r;
}

int64_t __cxa_finalize = 0;

void fun_2463() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_2473() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t utmpxname = 0x2040;

void fun_2483() {
    __asm__("cli ");
    goto utmpxname;
}

int64_t getenv = 0x2050;

void fun_2493() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2060;

void fun_24a3() {
    __asm__("cli ");
    goto free;
}

int64_t localtime = 0x2070;

void fun_24b3() {
    __asm__("cli ");
    goto localtime;
}

int64_t abort = 0x2080;

void fun_24c3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2090;

void fun_24d3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncpy = 0x20a0;

void fun_24e3() {
    __asm__("cli ");
    goto strncpy;
}

int64_t strncmp = 0x20b0;

void fun_24f3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20c0;

void fun_2503() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20d0;

void fun_2513() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20e0;

void fun_2523() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t setenv = 0x20f0;

void fun_2533() {
    __asm__("cli ");
    goto setenv;
}

int64_t textdomain = 0x2100;

void fun_2543() {
    __asm__("cli ");
    goto textdomain;
}

int64_t endutxent = 0x2110;

void fun_2553() {
    __asm__("cli ");
    goto endutxent;
}

int64_t strtod_l = 0x2120;

void fun_2563() {
    __asm__("cli ");
    goto strtod_l;
}

int64_t fclose = 0x2130;

void fun_2573() {
    __asm__("cli ");
    goto fclose;
}

int64_t getloadavg = 0x2140;

void fun_2583() {
    __asm__("cli ");
    goto getloadavg;
}

int64_t bindtextdomain = 0x2150;

void fun_2593() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2160;

void fun_25a3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2170;

void fun_25b3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2180;

void fun_25c3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2190;

void fun_25d3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x21a0;

void fun_25e3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21b0;

void fun_25f3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t newlocale = 0x21c0;

void fun_2603() {
    __asm__("cli ");
    goto newlocale;
}

int64_t __overflow = 0x21d0;

void fun_2613() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21e0;

void fun_2623() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21f0;

void fun_2633() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2200;

void fun_2643() {
    __asm__("cli ");
    goto memset;
}

int64_t fputc = 0x2210;

void fun_2653() {
    __asm__("cli ");
    goto fputc;
}

int64_t memcmp = 0x2220;

void fun_2663() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2230;

void fun_2673() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2240;

void fun_2683() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2250;

void fun_2693() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2260;

void fun_26a3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t setutxent = 0x2270;

void fun_26b3() {
    __asm__("cli ");
    goto setutxent;
}

int64_t memcpy = 0x2280;

void fun_26c3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t tzset = 0x2290;

void fun_26d3() {
    __asm__("cli ");
    goto tzset;
}

int64_t kill = 0x22a0;

void fun_26e3() {
    __asm__("cli ");
    goto kill;
}

int64_t time = 0x22b0;

void fun_26f3() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x22c0;

void fun_2703() {
    __asm__("cli ");
    goto fileno;
}

int64_t fgets_unlocked = 0x22d0;

void fun_2713() {
    __asm__("cli ");
    goto fgets_unlocked;
}

int64_t malloc = 0x22e0;

void fun_2723() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22f0;

void fun_2733() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2300;

void fun_2743() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2310;

void fun_2753() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2320;

void fun_2763() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2330;

void fun_2773() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2340;

void fun_2783() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strftime = 0x2350;

void fun_2793() {
    __asm__("cli ");
    goto strftime;
}

int64_t getutxent = 0x2360;

void fun_27a3() {
    __asm__("cli ");
    goto getutxent;
}

int64_t error = 0x2370;

void fun_27b3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2380;

void fun_27c3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2390;

void fun_27d3() {
    __asm__("cli ");
    goto fopen;
}

int64_t dcngettext = 0x23a0;

void fun_27e3() {
    __asm__("cli ");
    goto dcngettext;
}

int64_t unsetenv = 0x23b0;

void fun_27f3() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t __cxa_atexit = 0x23c0;

void fun_2803() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23d0;

void fun_2813() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23e0;

void fun_2823() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23f0;

void fun_2833() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2400;

void fun_2843() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2410;

void fun_2853() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_tolower_loc = 0x2420;

void fun_2863() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x2430;

void fun_2873() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(int64_t rdi);

void** fun_2770(int64_t rdi, ...);

void fun_2590(int64_t rdi, int64_t rsi);

void fun_2540(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t* Version = reinterpret_cast<int32_t*>(0xa4a0);

void parse_gnu_standard_options_only(int64_t rdi, int64_t* rsi, void** rdx, int64_t rcx, int32_t* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16);

void usage(int64_t rdi);

int32_t optind = 0;

int64_t quote(int64_t rdi, int64_t* rsi, void** rdx, int64_t rcx, int32_t* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10);

void fun_28c3(int32_t edi, int64_t* rsi) {
    int64_t rdi3;
    int64_t rdi4;
    int32_t* r8_5;
    int64_t rcx6;
    void** rdx7;
    int64_t rbx8;
    int64_t rbp9;
    int64_t r12_10;
    int64_t rax11;
    int32_t ebx12;
    int64_t rdi13;
    int64_t rdi14;
    int64_t rax15;
    void** rax16;

    __asm__("cli ");
    rdi3 = *rsi;
    set_program_name(rdi3);
    fun_2770(6, 6);
    fun_2590("coreutils", "/usr/local/share/locale");
    fun_2540("coreutils", "/usr/local/share/locale");
    atexit(0x3410, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&rdi4) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    r8_5 = Version;
    rcx6 = reinterpret_cast<int64_t>("GNU coreutils");
    rdx7 = reinterpret_cast<void**>("uptime");
    parse_gnu_standard_options_only(rdi4, rsi, "uptime", "GNU coreutils", r8_5, 1, usage, "Joseph Arceneaux", "David MacKenzie", "Kaveh Ghazi", 0, 0x2910, rbx8, rbp9, r12_10, __return_address());
    rax11 = optind;
    ebx12 = edi - *reinterpret_cast<int32_t*>(&rax11);
    if (ebx12) {
        if (ebx12 == 1) {
            addr_29bd_3:
            rdi13 = rsi[rax11];
            uptime(rdi13, 0, rdx7, rcx6, r8_5, 1, rbx8, rbp9, r12_10, __return_address());
        } else {
            rdi14 = (rsi + rax11)[1];
            rax15 = quote(rdi14, rsi, "uptime", "GNU coreutils", r8_5, 1, rbx8, rbp9, r12_10, __return_address());
            rax16 = fun_25a0();
            rcx6 = rax15;
            rdx7 = rax16;
            fun_27b0();
            usage(1);
        }
    }
    rax11 = uptime("/var/run/utmp", 1, rdx7, rcx6, r8_5, 1, rbx8, rbp9, r12_10, __return_address());
    goto addr_29bd_3;
}

int64_t __libc_start_main = 0;

void fun_29d3() {
    __asm__("cli ");
    __libc_start_main(0x28c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_2460(int64_t rdi);

int64_t fun_2a73() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2460(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2ab3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2670(void** rdi, void** rsi, void** rdx, int64_t rcx);

int32_t fun_24f0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2830(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2fe3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rax6;
    void** rax7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    int32_t eax13;
    void** r13_14;
    void** rax15;
    void** rax16;
    int32_t eax17;
    void** rax18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    void** rax22;
    void** r15_23;
    void** rax24;
    void** rax25;
    void** rax26;
    void** rdi27;
    void** r8_28;
    void** r9_29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_25a0();
            fun_2780(1, rax5, r12_2, 1, rax5, r12_2);
            rax6 = fun_25a0();
            fun_2780(1, rax6, 5, 1, rax6, 5);
            rax7 = fun_25a0();
            fun_2780(1, rax7, 5, 1, rax7, 5);
            rax8 = fun_25a0();
            fun_2780(1, rax8, "/var/run/utmp", 1, rax8, "/var/run/utmp");
            r12_9 = stdout;
            rax10 = fun_25a0();
            fun_2670(rax10, r12_9, 5, "/var/log/wtmp");
            r12_11 = stdout;
            rax12 = fun_25a0();
            fun_2670(rax12, r12_11, 5, "/var/log/wtmp");
            do {
                if (1) 
                    break;
                eax13 = fun_2690("uptime", 0, 5);
            } while (eax13);
            r13_14 = v4;
            if (!r13_14) {
                rax15 = fun_25a0();
                fun_2780(1, rax15, "GNU coreutils", 1, rax15, "GNU coreutils");
                rax16 = fun_2770(5);
                if (!rax16 || (eax17 = fun_24f0(rax16, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax17)) {
                    rax18 = fun_25a0();
                    r13_14 = reinterpret_cast<void**>("uptime");
                    fun_2780(1, rax18, "https://www.gnu.org/software/coreutils/", 1, rax18, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_14 = reinterpret_cast<void**>("uptime");
                    goto addr_3340_9;
                }
            } else {
                rax19 = fun_25a0();
                fun_2780(1, rax19, "GNU coreutils", 1, rax19, "GNU coreutils");
                rax20 = fun_2770(5);
                if (!rax20 || (eax21 = fun_24f0(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    addr_3246_11:
                    rax22 = fun_25a0();
                    fun_2780(1, rax22, "https://www.gnu.org/software/coreutils/", 1, rax22, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_14 == "uptime")) {
                        r12_2 = reinterpret_cast<void**>(0xab64);
                    }
                } else {
                    addr_3340_9:
                    r15_23 = stdout;
                    rax24 = fun_25a0();
                    fun_2670(rax24, r15_23, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3246_11;
                }
            }
            rax25 = fun_25a0();
            fun_2780(1, rax25, r13_14, 1, rax25, r13_14);
            addr_303e_14:
            fun_2810();
        }
    } else {
        rax26 = fun_25a0();
        rdi27 = stderr;
        fun_2830(rdi27, 1, rax26, r12_2, r8_28, r9_29, v30, v31, v32, v33, v34, v35);
        goto addr_303e_14;
    }
}

int64_t c_locale_cache = 0;

int64_t fun_2600(int64_t rdi, int64_t rsi);

void fun_3373(int64_t rdi, int64_t* rsi) {
    int64_t rax3;
    int64_t rax4;
    int64_t rdx5;

    __asm__("cli ");
    rax3 = c_locale_cache;
    if (!rax3) {
        rax4 = fun_2600(0x1fbf, "C");
        c_locale_cache = rax4;
    }
    rdx5 = c_locale_cache;
    if (!rdx5) {
        if (rsi) {
            *rsi = rdi;
        }
        __asm__("pxor xmm0, xmm0");
        return;
    }
}

int64_t file_name = 0;

void fun_33f3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3403(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2500(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3413() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_24d0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_25a0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_34a3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_27b0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2500(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_34a3_5:
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_27b0();
    }
}

struct s6 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2700(struct s6* rdi);

int32_t fun_2750(struct s6* rdi);

int64_t fun_2630(int64_t rdi, ...);

int32_t rpl_fflush(struct s6* rdi);

int64_t fun_2570(struct s6* rdi);

int64_t fun_34c3(struct s6* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2700(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2750(rdi);
        if (!(eax3 && (eax4 = fun_2700(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2630(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_24d0();
            r12d9 = *rax8;
            rax10 = fun_2570(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2570;
}

void rpl_fseeko(struct s6* rdi);

void fun_3553(struct s6* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2750(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void* fun_5123(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8) {
    int64_t v6;
    void* rax7;

    __asm__("cli ");
    rax7 = __strftime_internal_isra_0(rdi, rsi, rdx, 0, 0, -1, rcx, r8, v6);
    return rax7;
}

int64_t fun_5143(struct s6* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2700(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2630(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t opterr = 0;

int32_t fun_25e0();

void version_etc_va(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, void* r8);

void fun_51c3(int32_t edi) {
    signed char al2;
    void** rax3;
    int32_t r14d4;
    int32_t eax5;
    void* rax6;
    void** rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    r14d4 = opterr;
    opterr = 0;
    if (edi != 2) 
        goto addr_5240_4;
    eax5 = fun_25e0();
    if (eax5 == -1) 
        goto addr_5240_4;
    if (eax5 != 0x68) {
        if (eax5 != 0x76) {
            addr_5240_4:
            opterr = r14d4;
            optind = 0;
            rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
            if (rax6) {
                fun_25d0();
            } else {
                return;
            }
        } else {
            rdi7 = stdout;
            version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
            fun_2810();
        }
    }
    r9_11();
    goto addr_5240_4;
}

void fun_5303() {
    signed char al1;
    void** rax2;
    signed char r9b3;
    int32_t ebx4;
    int32_t eax5;
    int64_t v6;
    void** rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t rdi11;
    void* rax12;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    if (!r9b3) {
    }
    ebx4 = opterr;
    opterr = 1;
    eax5 = fun_25e0();
    if (eax5 != -1) {
        if (eax5 == 0x68) {
            addr_5430_7:
            v6();
        } else {
            if (eax5 == 0x76) {
                rdi7 = stdout;
                version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
                fun_2810();
                goto addr_5430_7;
            } else {
                *reinterpret_cast<int32_t*>(&rdi11) = exit_failure;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                v6(rdi11);
            }
        }
    }
    opterr = ebx4;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_25d0();
    } else {
        return;
    }
}

struct s7 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s7* fun_2620();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_5443(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s7* rax4;
    void** r12_5;
    int64_t rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2820("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_24c0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2620();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_24f0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6be3(int64_t rdi) {
    int64_t rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_24d0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xe200;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_6c23(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_6c43(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe200);
    }
    *rdi = esi;
    return 0xe200;
}

int64_t fun_6c63(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6ca3(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xe200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_6cc3(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xe200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x288a;
    if (!rdx) 
        goto 0x288a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe200;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6d03(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xe200);
    }
    rax7 = fun_24d0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x6d36);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6d83(int64_t rdi, int64_t rsi, void*** rdx, struct s11* rcx) {
    struct s11* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xe200);
    }
    rax6 = fun_24d0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6db1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x6e0c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6e73() {
    __asm__("cli ");
}

void** ge078 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_6e83() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;
    void** rdi12;
    void** rsi13;
    int64_t rdx14;
    int64_t rcx15;
    int64_t r8_16;
    int64_t r9_17;
    void** rsi18;
    int64_t rdx19;
    int64_t rcx20;
    int64_t r8_21;
    int64_t r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_24a0(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi12 != 0xe100) {
        fun_24a0(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        ge078 = reinterpret_cast<void**>(0xe100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        fun_24a0(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<void**>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_6f23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f53(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6f73(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6f93(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2890;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_7023(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2895;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void** fun_70b3(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s2* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x289a;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_25d0();
    } else {
        return rax5;
    }
}

void** fun_7143(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x289f;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_71d3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7020]");
    __asm__("movdqa xmm1, [rip+0x7028]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x7011]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_25d0();
    } else {
        return rax10;
    }
}

void** fun_7273(int64_t rdi, uint32_t esi) {
    struct s2* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6f80]");
    __asm__("movdqa xmm1, [rip+0x6f88]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6f71]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_25d0();
    } else {
        return rax9;
    }
}

void** fun_7313(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6ee0]");
    __asm__("movdqa xmm1, [rip+0x6ee8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6ec9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_25d0();
    } else {
        return rax3;
    }
}

void** fun_73a3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6e50]");
    __asm__("movdqa xmm1, [rip+0x6e58]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6e46]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_25d0();
    } else {
        return rax4;
    }
}

void** fun_7433(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28a4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_74d3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6d1a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6d12]");
    __asm__("movdqa xmm2, [rip+0x6d1a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28a9;
    if (!rdx) 
        goto 0x28a9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void** fun_7573(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s2* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6c7a]");
    __asm__("movdqa xmm1, [rip+0x6c82]");
    __asm__("movdqa xmm2, [rip+0x6c8a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28ae;
    if (!rdx) 
        goto 0x28ae;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_25d0();
    } else {
        return rax9;
    }
}

void** fun_7623(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6bca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6bc2]");
    __asm__("movdqa xmm2, [rip+0x6bca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28b3;
    if (!rsi) 
        goto 0x28b3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_76c3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b2a]");
    __asm__("movdqa xmm1, [rip+0x6b32]");
    __asm__("movdqa xmm2, [rip+0x6b3a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28b8;
    if (!rsi) 
        goto 0x28b8;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void fun_7763() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7773(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7793() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_77b3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** xmalloc(int64_t rdi);

void fun_24e0(void** rdi, int64_t rsi, int64_t rdx);

void** fun_77d3(int64_t rdi) {
    void** rax2;
    void** r12_3;
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax2 = xmalloc(33);
    r12_3 = rax2;
    fun_24e0(rax2, rdi + 44, 32);
    *reinterpret_cast<signed char*>(r12_3 + 32) = 0;
    rax4 = fun_25c0(r12_3, r12_3);
    rax5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(r12_3));
    if (reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rax5)) {
        addr_7818_2:
        return r12_3;
    } else {
        do {
            if (*reinterpret_cast<signed char*>(rax5 + 0xffffffffffffffff) != 32) 
                goto addr_7818_2;
            --rax5;
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
        } while (r12_3 != rax5);
    }
    goto addr_7818_2;
}

void fun_2480();

void fun_26b0();

struct s12 {
    void** f0;
    signed char[3] pad4;
    int32_t f4;
    signed char[36] pad44;
    signed char f2c;
    signed char[331] pad376;
    int64_t f178;
};

struct s12* fun_27a0();

int32_t fun_26e0();

struct s13 {
    void** f0;
    signed char[375] pad376;
    int64_t f178;
};

void fun_2550();

int64_t fun_7843() {
    void** r15_1;
    int64_t* r14_2;
    int64_t* rsi3;
    uint32_t r12d4;
    uint32_t ecx5;
    uint32_t ebp6;
    int64_t rbx7;
    void*** v8;
    void*** rdx9;
    void** rax10;
    void** v11;
    struct s12* rax12;
    struct s12* r13_13;
    int32_t eax14;
    void*** rax15;
    void** rax16;
    int64_t rax17;
    struct s13* rax18;
    int64_t* rdi19;
    void* rax20;
    int64_t* rsi21;
    int64_t rcx22;
    struct s12* rax23;
    void* rax24;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_1) = 0;
    *reinterpret_cast<int32_t*>(&r15_1 + 4) = 0;
    r14_2 = rsi3;
    r12d4 = ecx5;
    ebp6 = r12d4 & 2;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    v8 = rdx9;
    rax10 = g28;
    v11 = rax10;
    fun_2480();
    fun_26b0();
    while (rax12 = fun_27a0(), r13_13 = rax12, !!rax12) {
        do {
            if (!r13_13->f2c || r13_13->f0 != 7) {
                if (ebp6) 
                    break;
            } else {
                if (*reinterpret_cast<unsigned char*>(&r12d4) & 1 && (!(reinterpret_cast<uint1_t>(r13_13->f4 < 0) | reinterpret_cast<uint1_t>(r13_13->f4 == 0)) && (eax14 = fun_26e0(), eax14 < 0))) {
                    rax15 = fun_24d0();
                    if (*rax15 == 3) 
                        break;
                    if (rbx7) 
                        goto addr_78be_8;
                    goto addr_7988_10;
                }
            }
            if (!rbx7) {
                addr_7988_10:
                rax16 = xpalloc();
                r15_1 = rax16;
                goto addr_78be_8;
            } else {
                addr_78be_8:
                rax17 = rbx7 + rbx7 * 2;
                ++rbx7;
                rax18 = reinterpret_cast<struct s13*>((rax17 << 7) + reinterpret_cast<unsigned char>(r15_1));
                rax18->f0 = r13_13->f0;
                rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rax18) + 8 & 0xfffffffffffffff8);
                rax18->f178 = r13_13->f178;
                rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax18) - reinterpret_cast<uint64_t>(rdi19));
                rsi21 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r13_13) - reinterpret_cast<uint64_t>(rax20));
                *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax20) + 0x180) >> 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
            }
            while (rcx22) {
                --rcx22;
                *rdi19 = *rsi21;
                ++rdi19;
                ++rsi21;
            }
            rax23 = fun_27a0();
            r13_13 = rax23;
        } while (rax23);
        goto addr_790d_15;
    }
    addr_7910_16:
    fun_2550();
    *r14_2 = rbx7;
    *v8 = r15_1;
    rax24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax24) {
        fun_25d0();
    } else {
        return 0;
    }
    addr_790d_15:
    goto addr_7910_16;
}

struct s14 {
    int64_t f0;
    int16_t f8;
    void** f9;
};

struct s14* fun_2720(void** rdi);

struct s14* fun_7a53(void** rdi) {
    struct s14* rax2;
    struct s14* r12_3;
    void** rax4;
    void** rbx5;
    void** rax6;
    struct s14* rax7;

    __asm__("cli ");
    if (!rdi) {
        rax2 = fun_2720(0x80);
        r12_3 = rax2;
        if (rax2) {
            r12_3->f0 = 0;
            r12_3->f8 = 0;
            return r12_3;
        }
    } else {
        rax4 = fun_25c0(rdi);
        rbx5 = rax4 + 1;
        *reinterpret_cast<int32_t*>(&rax6) = 0x76;
        *reinterpret_cast<int32_t*>(&rax6 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx5) >= reinterpret_cast<unsigned char>(0x76)) {
            rax6 = rbx5;
        }
        rax7 = fun_2720(reinterpret_cast<uint64_t>(rax6 + 17) & 0xfffffffffffffff8);
        r12_3 = rax7;
        if (rax7) {
            rax7->f0 = 0;
            r12_3->f8 = 1;
            fun_26c0(reinterpret_cast<int64_t>(r12_3) + 9, rdi, rbx5);
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_3) + reinterpret_cast<unsigned char>(rbx5) + 9) = 0;
        }
    }
    return r12_3;
}

void fun_7d23(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    void** rbx7;
    void** rdi8;

    __asm__("cli ");
    if (rdi == 1) {
        return;
    } else {
        rbx7 = rdi;
        if (rdi) {
            do {
                rdi8 = rbx7;
                rbx7 = *reinterpret_cast<void***>(rbx7);
                fun_24a0(rdi8, rsi, rdx, rcx, r8, r9);
            } while (rbx7);
        }
        return;
    }
}

int64_t fun_2440(int64_t rdi, void** rsi);

void** fun_7d63(void** rdi, int64_t rsi, void** rdx) {
    void** rax4;
    void** rsi5;
    int64_t rax6;
    signed char al7;
    signed char al8;

    __asm__("cli ");
    if (rdi) {
        rax4 = set_tz(rdi);
        if (rax4) {
            rsi5 = rdx;
            rax6 = fun_2440(rsi, rsi5);
            if (!rax6 || (rsi5 = rdx, al7 = save_abbr(rdi, rsi5), al7 == 0)) {
                if (rax4 != 1) {
                    revert_tz_part_0(rax4, rsi5);
                }
            } else {
                if (reinterpret_cast<int1_t>(rax4 == 1) || (al8 = revert_tz_part_0(rax4, rsi5), !!al8)) {
                    return rdx;
                }
            }
        }
        return 0;
    }
}

int64_t rpl_mktime(void** rdi);

int64_t fun_7e13(void** rdi, void** rsi) {
    void** rbp3;
    void** rax4;
    void* rax5;
    void** rax6;
    int64_t r13_7;
    void** r15_8;
    int64_t rax9;
    signed char al10;
    signed char al11;
    void** v12;
    void* rax13;

    __asm__("cli ");
    rbp3 = rsi;
    rax4 = g28;
    if (!rdi) {
        rax5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
        if (rax5) {
            fun_25d0();
        }
    }
    rax6 = set_tz(rdi);
    if (!rax6) {
        addr_7ea0_7:
        r13_7 = -1;
    } else {
        r15_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 64) - 8 + 8);
        rax9 = rpl_mktime(r15_8);
        r13_7 = rax9;
        if (!0 || (rsi = r15_8, al10 = save_abbr(rdi, rsi), al10 == 0)) {
            if (rax6 != 1) {
                revert_tz_part_0(rax6, rsi);
                goto addr_7ea0_7;
            }
        } else {
            if (reinterpret_cast<int1_t>(rax6 == 1) || (al11 = revert_tz_part_0(rax6, rsi), !!al11)) {
                __asm__("movdqa xmm0, [rsp]");
                __asm__("movdqa xmm1, [rsp+0x10]");
                __asm__("movdqa xmm2, [rsp+0x20]");
                __asm__("movups [rbp+0x0], xmm0");
                *reinterpret_cast<void***>(rbp3 + 48) = v12;
                __asm__("movups [rbp+0x10], xmm1");
                __asm__("movups [rbp+0x20], xmm2");
            } else {
                goto addr_7ea0_7;
            }
        }
    }
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rax13) {
        return r13_7;
    }
}

struct s15 {
    signed char[32] pad32;
    int32_t f20;
};

void fun_7f63(struct s15* rdi) {
    __asm__("cli ");
    rdi->f20 = 0;
    goto 0x8f30;
}

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_26a0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_7f83(void** rdi, void** rsi, void** rdx, void** rcx, struct s16* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2830(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2830(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_25a0();
    fun_2830(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_26a0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_25a0();
    fun_2830(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_26a0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_25a0();
        fun_2830(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xae08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xae08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_83f3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s17 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_8413(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s17* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s17* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_25d0();
    } else {
        return;
    }
}

void fun_84b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_8556_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_8560_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_25d0();
    } else {
        return;
    }
    addr_8556_5:
    goto addr_8560_7;
}

void fun_8593() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rsi1 = stdout;
    fun_26a0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_25a0();
    fun_2780(1, rax6, "bug-coreutils@gnu.org");
    rax7 = fun_25a0();
    fun_2780(1, rax7, "GNU coreutils", 1, rax7, "GNU coreutils");
    fun_25a0();
    goto fun_2780;
}

int64_t fun_2520();

void xalloc_die();

void fun_8633(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_8673(void** rdi) {
    struct s14* rax2;

    __asm__("cli ");
    rax2 = fun_2720(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8693(void** rdi) {
    struct s14* rax2;

    __asm__("cli ");
    rax2 = fun_2720(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_86b3(void** rdi) {
    struct s14* rax2;

    __asm__("cli ");
    rax2 = fun_2720(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2760();

void fun_86d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2760();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_8703() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2760();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8733(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_8773() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_87b3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_87e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8833(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2520();
            if (rax5) 
                break;
            addr_887d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_887d_5;
        rax8 = fun_2520();
        if (rax8) 
            goto addr_8866_9;
        if (rbx4) 
            goto addr_887d_5;
        addr_8866_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_88c3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2520();
            if (rax8) 
                break;
            addr_890a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_890a_5;
        rax11 = fun_2520();
        if (rax11) 
            goto addr_88f2_9;
        if (!rbx6) 
            goto addr_88f2_9;
        if (r12_4) 
            goto addr_890a_5;
        addr_88f2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_8953(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_89fd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_8a10_10:
                *r12_8 = 0;
            }
            addr_89b0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_89d6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_8a24_14;
            if (rcx10 <= rsi9) 
                goto addr_89cd_16;
            if (rsi9 >= 0) 
                goto addr_8a24_14;
            addr_89cd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_8a24_14;
            addr_89d6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2760();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_8a24_14;
            if (!rbp13) 
                break;
            addr_8a24_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_89fd_9;
        } else {
            if (!r13_6) 
                goto addr_8a10_10;
            goto addr_89b0_11;
        }
    }
}

int64_t fun_2680();

void fun_8a53() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8a83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8ab3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8ad3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8af3(int64_t rdi, void** rsi) {
    struct s14* rax3;

    __asm__("cli ");
    rax3 = fun_2720(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

void fun_8b33(int64_t rdi, void** rsi) {
    struct s14* rax3;

    __asm__("cli ");
    rax3 = fun_2720(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

struct s18 {
    signed char[1] pad1;
    void** f1;
};

void fun_8b73(int64_t rdi, struct s18* rsi) {
    struct s14* rax3;

    __asm__("cli ");
    rax3 = fun_2720(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_26c0;
    }
}

void fun_8bb3(void** rdi) {
    void** rax2;
    struct s14* rax3;

    __asm__("cli ");
    rax2 = fun_25c0(rdi);
    rax3 = fun_2720(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

void fun_8bf3() {
    void** rdi1;

    __asm__("cli ");
    fun_25a0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_27b0();
    fun_24c0(rdi1);
}

struct s19 {
    int32_t f0;
    uint32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    signed char[8] pad32;
    uint32_t f20;
    signed char[12] pad48;
    int64_t f30;
};

uint64_t fun_8f33(struct s19* rdi, int64_t rsi, uint64_t* rdx) {
    int64_t rcx4;
    struct s19* v5;
    int64_t v6;
    int64_t rsi7;
    uint64_t* v8;
    void** rax9;
    void** v10;
    int32_t v11;
    uint32_t v12;
    int64_t rdi13;
    int32_t v14;
    int64_t rcx15;
    uint32_t v16;
    int64_t rcx17;
    uint32_t edi18;
    int64_t r15_19;
    int64_t rdx20;
    int64_t rdx21;
    int64_t rdx22;
    uint64_t r12_23;
    int64_t r9_24;
    int64_t rsi25;
    uint64_t rax26;
    int64_t v27;
    int64_t r8_28;
    uint64_t v29;
    int64_t rax30;
    int32_t v31;
    int64_t v32;
    int64_t rcx33;
    uint64_t rax34;
    void* rsp35;
    uint64_t v36;
    uint64_t rbp37;
    struct s4* r13_38;
    uint64_t* r14_39;
    uint64_t v40;
    uint64_t v41;
    int32_t v42;
    uint32_t v43;
    struct s4* rax44;
    int64_t rbx45;
    int32_t v46;
    int64_t v47;
    int64_t rax48;
    int32_t v49;
    int64_t v50;
    int64_t rax51;
    int32_t v52;
    int64_t v53;
    int64_t rax54;
    int32_t v55;
    int64_t v56;
    int32_t v57;
    uint64_t rax58;
    uint64_t r10_59;
    int32_t v60;
    unsigned char dl61;
    uint32_t eax62;
    int32_t v63;
    unsigned char v64;
    uint32_t edi65;
    unsigned char v66;
    int32_t v67;
    int64_t rcx68;
    int32_t v69;
    uint64_t rbp70;
    int64_t r12_71;
    struct s4* v72;
    int64_t r14_73;
    uint64_t* v74;
    void* v75;
    int64_t v76;
    struct s4* v77;
    uint64_t* rax78;
    uint64_t rdx79;
    int64_t rax80;
    int64_t v81;
    void* rax82;
    uint64_t rax83;
    uint64_t v84;
    int32_t v85;
    struct s4* rax86;
    int32_t v87;
    int64_t rax88;
    int32_t v89;
    int64_t v90;
    int64_t rax91;
    int32_t v92;
    int64_t v93;
    int64_t rax94;
    int32_t v95;
    int64_t v96;
    int64_t rax97;
    int32_t v98;
    int64_t v99;
    int32_t v100;
    int64_t rdx101;
    int64_t rax102;
    void*** rax103;
    int32_t ebx104;
    int32_t r15d105;
    int32_t r13d106;
    int64_t rax107;
    void*** rax108;
    int32_t v109;

    __asm__("cli ");
    rcx4 = rdi->f10;
    v5 = rdi;
    v6 = rsi;
    rsi7 = rdi->fc;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    v11 = rdi->f0;
    v12 = rdi->f4;
    rdi13 = rcx4;
    v14 = rdi->f8;
    rcx15 = rcx4 * 0x2aaaaaab >> 33;
    v16 = rdi->f20;
    *reinterpret_cast<int32_t*>(&rcx17) = *reinterpret_cast<int32_t*>(&rcx15) - (*reinterpret_cast<int32_t*>(&rdi13) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    edi18 = *reinterpret_cast<int32_t*>(&rdi13) - (static_cast<uint32_t>(rcx17 + rcx17 * 2) << 2);
    r15_19 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rcx17) - (edi18 >> 31)) + static_cast<int64_t>(rdi->f14);
    *reinterpret_cast<uint32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&r15_19) & 3) && (*reinterpret_cast<uint32_t*>(&rdx20) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0, reinterpret_cast<uint64_t>(0x8f5c28f5c28f5c29 * r15_19 + 0x51eb851eb851eb8) <= 0x28f5c28f5c28f5c)) {
        rdx21 = (__intrinsic() + r15_19 >> 6) - (r15_19 >> 63);
        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<uint1_t>(rdx22 == 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r12_23) = 59;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_23) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_24) = 70;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
    rsi25 = rsi7 + reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(0xae80 + ((reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edi18) >> 31) & 12) + edi18 + (rdx20 + (rdx20 + rdx20 * 2) * 4)) * 2) - 1);
    rax26 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v11));
    v27 = rsi25;
    if (*reinterpret_cast<int32_t*>(&rax26) <= 59) {
        r12_23 = rax26;
    }
    if (*reinterpret_cast<int32_t*>(&r12_23) < 0) {
        r12_23 = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
    v29 = *v8;
    *reinterpret_cast<int32_t*>(&rax30) = -*reinterpret_cast<int32_t*>(&v29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    v31 = *reinterpret_cast<int32_t*>(&rax30);
    v32 = rax30;
    *reinterpret_cast<uint32_t*>(&rcx33) = v12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
    rax34 = ydhms_diff(r15_19, rsi25, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), 70, 0, 0, 0, *reinterpret_cast<int32_t*>(&v32));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
    v36 = rax34;
    rbp37 = rax34;
    r13_38 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rsp35) + 0xa0);
    r14_39 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x88);
    v40 = rax34;
    v41 = rax34;
    v42 = 6;
    v43 = 0;
    while (rax44 = ranged_convert(v6, r14_39, r13_38, rcx33, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax44) {
        *reinterpret_cast<int32_t*>(&rbx45) = v46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx45) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
        v47 = rbx45;
        *reinterpret_cast<int32_t*>(&rax48) = v49;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        v50 = rax48;
        *reinterpret_cast<int32_t*>(&rax51) = v52;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        v53 = rax51;
        *reinterpret_cast<int32_t*>(&rax54) = v55;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        v56 = rax54;
        *reinterpret_cast<int32_t*>(&r9_24) = v57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx33) = v12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
        rax58 = ydhms_diff(r15_19, v27, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v56), *reinterpret_cast<int32_t*>(&v53), *reinterpret_cast<int32_t*>(&v50), *reinterpret_cast<int32_t*>(&v47));
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        r10_59 = v40;
        if (!rax58) 
            goto addr_92d0_10;
        if (rbp37 == r10_59 || v41 != r10_59) {
            addr_9130_12:
            --v42;
            if (!v42) 
                goto addr_9240_13;
        } else {
            if (v60 < 0) 
                goto addr_9190_15;
            *reinterpret_cast<uint32_t*>(&rcx33) = v16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            dl61 = reinterpret_cast<uint1_t>(!!v60);
            if (*reinterpret_cast<int32_t*>(&rcx33) < reinterpret_cast<int32_t>(0)) 
                goto addr_92b8_17; else 
                goto addr_9182_18;
        }
        v41 = rbp37;
        rbp37 = r10_59;
        v40 = rax58 + r10_59;
        eax62 = 0;
        *reinterpret_cast<unsigned char*>(&eax62) = reinterpret_cast<uint1_t>(!!v63);
        v43 = eax62;
        continue;
        addr_92b8_17:
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(dl61)) < reinterpret_cast<int32_t>(v43)) 
            goto addr_9130_12; else 
            goto addr_92c5_20;
        addr_9182_18:
        *reinterpret_cast<unsigned char*>(&rcx33) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rcx33));
        if (*reinterpret_cast<unsigned char*>(&rcx33) == dl61) 
            goto addr_9130_12; else 
            goto addr_9189_21;
    }
    goto addr_924b_22;
    addr_92d0_10:
    v64 = reinterpret_cast<uint1_t>(v16 == 0);
    edi65 = v64;
    v66 = reinterpret_cast<uint1_t>(v67 == 0);
    *reinterpret_cast<uint32_t*>(&rcx68) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx68) != *reinterpret_cast<signed char*>(&edi65) && !__intrinsic()) {
        v69 = *reinterpret_cast<int32_t*>(&r12_23);
        rbp70 = r10_59;
        r12_71 = v6;
        v72 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rsp35) + 0xe0);
        *reinterpret_cast<int32_t*>(&r14_73) = 0x92c70;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
        v74 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x90);
        v75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) + 0x98);
        v76 = r15_19;
        v77 = r13_38;
        goto addr_9364_24;
    }
    addr_9190_15:
    rax78 = v8;
    *rax78 = r10_59 - (v31 + v36);
    if (v11 == *reinterpret_cast<int32_t*>(&rbx45)) 
        goto addr_91ff_25;
    *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx45) == 60);
    *reinterpret_cast<int32_t*>(&rdx79) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx79) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx79) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v11 < 0) | reinterpret_cast<uint1_t>(v11 == 0));
    if (!__intrinsic()) {
        rax80 = reinterpret_cast<int64_t>(v6(reinterpret_cast<int64_t>(rsp35) + 0xe0, r13_38));
        r10_59 = v11 + ((rdx79 & reinterpret_cast<uint64_t>(rax78)) - r12_23) + r10_59;
        if (!rax80) {
            addr_924b_22:
            r10_59 = 0xffffffffffffffff;
        } else {
            addr_91ff_25:
            __asm__("movdqa xmm0, [rsp+0xa0]");
            __asm__("movdqa xmm1, [rsp+0xb0]");
            __asm__("movdqa xmm2, [rsp+0xc0]");
            __asm__("movups [rcx], xmm0");
            v5->f30 = v81;
            __asm__("movups [rcx+0x10], xmm1");
            __asm__("movups [rcx+0x20], xmm2");
        }
        rax82 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
        if (rax82) {
            fun_25d0();
        } else {
            return r10_59;
        }
    }
    addr_92c5_20:
    goto addr_9190_15;
    addr_9189_21:
    goto addr_9190_15;
    addr_94c2_31:
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    r13_38 = v77;
    r10_59 = rax83 + v84;
    *reinterpret_cast<int32_t*>(&rbx45) = v85;
    goto addr_9190_15;
    addr_945b_32:
    goto addr_924b_22;
    while (rax86 = ranged_convert(r12_71, v74, v72, rcx68, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax86) {
        if (v64 == static_cast<unsigned char>(reinterpret_cast<uint1_t>(v87 == 0)) || v87 < 0) {
            *reinterpret_cast<int32_t*>(&rax88) = v89;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
            v90 = rax88;
            *reinterpret_cast<int32_t*>(&rax91) = v92;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
            v93 = rax91;
            *reinterpret_cast<int32_t*>(&rax94) = v95;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            v96 = rax94;
            *reinterpret_cast<int32_t*>(&rax97) = v98;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            v99 = rax97;
            *reinterpret_cast<int32_t*>(&r9_24) = v100;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_28) = v69;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx68) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx101) = v14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
            rax83 = ydhms_diff(v76, v27, *reinterpret_cast<int32_t*>(&rdx101), *reinterpret_cast<uint32_t*>(&rcx68), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v99), *reinterpret_cast<int32_t*>(&v96), *reinterpret_cast<int32_t*>(&v93), *reinterpret_cast<int32_t*>(&v90));
            rax102 = reinterpret_cast<int64_t>(r12_71(v75, v77, rdx101, rcx68, r8_28, r9_24));
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
            if (rax102) 
                goto addr_94c2_31;
            rax103 = fun_24d0();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            if (*rax103 != 75) 
                goto addr_945b_32;
        }
        while (1) {
            do {
                ebx104 = ebx104 + r15d105;
                if (r13d106 == 1) 
                    break;
                r13d106 = 1;
                v84 = ebx104 + rbp70;
            } while (__intrinsic());
            break;
            *reinterpret_cast<int32_t*>(&r14_73) = *reinterpret_cast<int32_t*>(&r14_73) + 0x92c70;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r14_73) == 0xdb04f20) 
                goto addr_9460_40;
            addr_9364_24:
            r15d105 = static_cast<int32_t>(r14_73 + r14_73);
            r13d106 = 2;
            ebx104 = -*reinterpret_cast<int32_t*>(&r14_73);
            v84 = ebx104 + rbp70;
            if (!__intrinsic()) 
                break;
        }
    }
    goto addr_924b_22;
    addr_9460_40:
    r13_38 = v77;
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    rax107 = reinterpret_cast<int64_t>(v6(v72, r13_38));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax107) {
        addr_9240_13:
        rax108 = fun_24d0();
        *rax108 = reinterpret_cast<void**>(75);
        goto addr_924b_22;
    } else {
        *reinterpret_cast<int32_t*>(&rbx45) = v109;
        r10_59 = rbp70 + static_cast<int64_t>(reinterpret_cast<int32_t>((v64 - v66) * reinterpret_cast<uint32_t>("dutxent")));
        goto addr_9190_15;
    }
}

void fun_94e3(int64_t rdi, void** rsi, int64_t rdx) {
    __asm__("cli ");
    fun_26d0(rdi, rsi, rdx);
    goto 0x8f30;
}

int64_t fun_2510();

int64_t fun_9513(uint32_t* rdi, int32_t* rsi, uint32_t* rdx) {
    int64_t rax4;
    uint32_t ebx5;
    int64_t rax6;
    void*** rax7;
    void*** rax8;

    __asm__("cli ");
    rax4 = fun_2510();
    ebx5 = *rdi & 32;
    rax6 = rpl_fclose(rdi, rsi, rdx);
    if (ebx5) {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            addr_956e_3:
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            rax7 = fun_24d0();
            *rax7 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            if (rax4) 
                goto addr_956e_3;
            rax8 = fun_24d0();
            *reinterpret_cast<int32_t*>(&rax6) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax8 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    }
    return rax6;
}

signed char* fun_2740(int64_t rdi);

signed char* fun_9583() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2740(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25f0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_95c3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25f0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_25d0();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_9653() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax3;
    }
}

int64_t fun_96d3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2770(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_25c0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_26c0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_26c0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_9783() {
    __asm__("cli ");
    goto fun_2770;
}

void fun_9793() {
    __asm__("cli ");
}

void fun_97a7() {
    __asm__("cli ");
    return;
}

void fun_37bf() {
}

void* fun_2790(void** rdi, void** rsi, void* rdx, int64_t rcx);

int32_t** fun_2860(void** rdi, void** rsi, void* rdx, int64_t rcx);

void fun_3c80(int32_t edi, signed char sil) {
    uint32_t eax3;
    unsigned char v4;
    signed char bpl5;
    uint32_t ebp6;
    signed char v7;
    int64_t rcx8;
    int64_t v9;
    void** r12_10;
    void* rdx11;
    void** rsi12;
    void** rdi13;
    void* rax14;
    void* rsp15;
    void* rbp16;
    void** rax17;
    void** v18;
    int32_t r8d19;
    int32_t r13d20;
    void** r13_21;
    void** v22;
    int32_t r13d23;
    int64_t r15_24;
    int64_t r14_25;
    void* r13_26;
    int32_t** rax27;
    unsigned char* rbp28;
    unsigned char* r13_29;
    int32_t** r12_30;
    int64_t rdx31;
    unsigned char v32;
    void** rdi33;
    void** r14_34;
    void** r14_35;
    void**** rax36;
    unsigned char* rbp37;
    unsigned char* r13_38;
    void**** r12_39;
    int64_t rdx40;
    unsigned char v41;
    void** rdi42;
    void** r14_43;
    void* rbx44;
    void** r14_45;
    void* rbx46;
    void** r14_47;

    eax3 = v4;
    if (bpl5) {
        eax3 = ebp6;
    }
    v7 = *reinterpret_cast<signed char*>(&eax3);
    if (edi == 69) {
        goto 0x37c8;
    }
    if (!edi) 
        goto addr_395b_7;
    addr_3963_9:
    rcx8 = v9;
    r12_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xb0);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xab);
    *reinterpret_cast<int32_t*>(&rsi12) = 0x400;
    *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
    rdi13 = r12_10;
    rax14 = fun_2790(rdi13, 0x400, rdx11, rcx8);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8);
    rbp16 = rax14;
    if (!rax14) 
        goto 0x367f;
    rax17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax14) + 0xffffffffffffffff);
    v18 = rax17;
    if (r8d19 == 45 || r13d20 < 0) {
        *reinterpret_cast<int32_t*>(&r13_21) = 0;
        *reinterpret_cast<int32_t*>(&r13_21 + 4) = 0;
        v22 = v18;
    } else {
        r13_21 = reinterpret_cast<void**>(static_cast<int64_t>(r13d23));
        if (reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(r13_21)) {
            rax17 = r13_21;
        }
        v22 = rax17;
    }
    if (reinterpret_cast<unsigned char>(~r15_24) <= reinterpret_cast<unsigned char>(v22)) 
        goto 0x36a8;
    if (!r14_25) {
        addr_3a90_17:
        goto 0x367f;
    } else {
        if (reinterpret_cast<unsigned char>(v18) >= reinterpret_cast<unsigned char>(r13_21)) 
            goto addr_3a3b_19;
        r13_26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_21) - reinterpret_cast<unsigned char>(v18));
        if (r8d19 == 48) 
            goto addr_4b09_21;
        if (r8d19 != 43) 
            goto addr_3a07_23;
    }
    addr_4b09_21:
    if (!r13_26) {
        addr_3a3b_19:
        if (0) {
            if (v18) {
                rax27 = fun_2860(rdi13, rsi12, rdx11, rcx8);
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                rbp28 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp16) + reinterpret_cast<unsigned char>(r12_10));
                r13_29 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp15) + 0xb1);
                r12_30 = rax27;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx31) = v32;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx31) + 4) = 0;
                    ++r13_29;
                    *reinterpret_cast<int32_t*>(&rdi33) = (*r12_30)[rdx31];
                    *reinterpret_cast<int32_t*>(&rdi33 + 4) = 0;
                    fun_2650(rdi33, r14_34, rdi33, r14_34);
                } while (r13_29 != rbp28);
                goto addr_3a90_17;
            }
        } else {
            if (!v7) {
                fun_2820(reinterpret_cast<int64_t>(rsp15) + 0xb1, v18, 1, r14_35);
                goto addr_3a90_17;
            } else {
                if (v18) {
                    rax36 = fun_2470(rdi13, rsi12, rdi13, rsi12);
                    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                    rbp37 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp16) + reinterpret_cast<unsigned char>(r12_10));
                    r13_38 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp15) + 0xb1);
                    r12_39 = rax36;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx40) = v41;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx40) + 4) = 0;
                        ++r13_38;
                        rdi42 = (*r12_39)[rdx40 * 4];
                        *reinterpret_cast<int32_t*>(&rdi42 + 4) = 0;
                        fun_2650(rdi42, r14_43, rdi42, r14_43);
                    } while (r13_38 != rbp37);
                    goto addr_3a90_17;
                }
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx11) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        rbx44 = reinterpret_cast<void*>(0);
        do {
            rsi12 = r14_45;
            *reinterpret_cast<int32_t*>(&rdi13) = 48;
            *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
            rbx44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx44) + 1);
            fun_2650(48, rsi12, 48, rsi12);
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        } while (r13_26 != rbx44);
    }
    addr_3a36_37:
    goto addr_3a3b_19;
    addr_3a07_23:
    *reinterpret_cast<int32_t*>(&rdx11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
    if (!r13_26) 
        goto addr_3a3b_19;
    rbx46 = reinterpret_cast<void*>(0);
    do {
        rsi12 = r14_47;
        *reinterpret_cast<int32_t*>(&rdi13) = 32;
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        rbx46 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx46) + 1);
        fun_2650(32, rsi12, 32, rsi12);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    } while (r13_26 != rbx46);
    goto addr_3a36_37;
    addr_395b_7:
    goto addr_3963_9;
}

void fun_3c9d(int32_t edi) {
    void* rsp2;
    int32_t edx3;
    int32_t v4;
    int64_t r12_5;
    int32_t r13d6;
    int32_t r13d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    int64_t rsi13;
    signed char* rcx14;
    int64_t rdi15;
    signed char* rsi16;
    int64_t rax17;
    int64_t rax18;
    int32_t edi19;
    int32_t edx20;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0x37c8;
    edx3 = v4;
    *reinterpret_cast<int32_t*>(&r12_5) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    if (reinterpret_cast<uint1_t>(r13d6 < 0) | reinterpret_cast<uint1_t>(r13d7 == 0)) {
    }
    while (1) {
        rax8 = edx3;
        if (*reinterpret_cast<int32_t*>(&r12_5) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx3 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&r12_5) == 1) 
                break;
            if (edx3 - (esi11 + esi11)) 
                goto addr_3d11_8;
        }
        *reinterpret_cast<int32_t*>(&r12_5) = *reinterpret_cast<int32_t*>(&r12_5) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx3 = *reinterpret_cast<int32_t*>(&rax12) - (edx3 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    addr_3d20_11:
    rcx14 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + rsi13 + 0xb0);
    *reinterpret_cast<int32_t*>(&rdi15) = static_cast<int32_t>(r12_5 - 1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
    rsi16 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + rsi13 + 0xaf - rdi15);
    while (1) {
        --rcx14;
        rax17 = rax8 * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax18) = *reinterpret_cast<int32_t*>(&rax17) - (edx3 >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        edi19 = static_cast<int32_t>(rax18 + rax18 * 4);
        edx20 = edx3 - (edi19 + edi19) + 48;
        *rcx14 = *reinterpret_cast<signed char*>(&edx20);
        edx3 = *reinterpret_cast<int32_t*>(&rax18);
        if (rcx14 == rsi16) 
            goto "???";
        rax8 = *reinterpret_cast<int32_t*>(&rax18);
    }
    addr_3d11_8:
    rsi13 = *reinterpret_cast<int32_t*>(&r12_5);
    if (!*reinterpret_cast<int32_t*>(&r12_5)) 
        goto 0x50ae; else 
        goto addr_3d20_11;
}

void fun_3e80(int32_t edi) {
    uint32_t edx2;
    uint32_t* v3;
    uint32_t r10d4;
    uint32_t eax5;
    void** rdi6;
    void** r9_7;
    void** rsi8;
    int64_t rax9;
    uint64_t rax10;
    int32_t r8d11;
    int32_t eax12;
    int32_t r8d13;
    int32_t r13d14;
    int64_t r13_15;
    void** rdi16;
    void** r12_17;
    signed char v18;
    int32_t ecx19;
    int32_t ebp20;
    uint64_t v21;
    int64_t r14_22;
    void** v23;
    int64_t rbx24;
    int32_t v25;
    int64_t rbp26;
    void** r14_27;
    uint64_t r15_28;
    int64_t r15_29;
    int64_t r14_30;
    void** r14_31;
    void** rbp32;
    uint64_t r15_33;
    void** rax34;
    void** r12_35;
    int64_t r14_36;
    void* rax37;
    void** v38;
    void* rbx39;
    void* r13_40;
    void** r14_41;
    void* rbx42;
    void* r13_43;
    void** r14_44;
    signed char v45;
    void** r14_46;
    void**** rax47;
    void**** rbx48;
    void** r13_49;
    void** rbp50;
    int64_t rdx51;
    void** v52;
    void** rdi53;
    void** r14_54;

    if (edi == 69) 
        goto 0x37c8;
    edx2 = *v3;
    r10d4 = edx2 >> 31;
    eax5 = ~edx2 >> 31;
    if (edi == 79 && *reinterpret_cast<signed char*>(&eax5)) {
    }
    if (*reinterpret_cast<signed char*>(&r10d4)) {
        edx2 = -edx2;
    }
    rdi6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xc7);
    r9_7 = rdi6;
    while (1) {
        rsi8 = r9_7;
        if (!1) {
            --rsi8;
        }
        *reinterpret_cast<uint32_t*>(&rax9) = edx2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        r9_7 = rsi8 + 0xffffffffffffffff;
        rax10 = reinterpret_cast<uint64_t>(rax9 * 0xcccccccd) >> 35;
        if (edx2 > 9) 
            goto addr_42b8_13;
        if (1) 
            break;
        addr_42b8_13:
        edx2 = *reinterpret_cast<uint32_t*>(&rax10);
    }
    if (!r8d11) {
        eax12 = 1;
    } else {
        *reinterpret_cast<unsigned char*>(&eax12) = reinterpret_cast<uint1_t>(r8d13 != 45);
    }
    if (r13d14 < 0) {
        *reinterpret_cast<int32_t*>(&r13_15) = 2;
    }
    rdi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi6) - reinterpret_cast<unsigned char>(r9_7));
    r12_17 = rdi16;
    if (!*reinterpret_cast<signed char*>(&r10d4)) 
        goto addr_4088_21;
    v18 = 45;
    addr_4995_23:
    ecx19 = 1 - *reinterpret_cast<int32_t*>(&r12_17);
    ebp20 = ecx19;
    if (reinterpret_cast<uint1_t>(ecx19 < 0) | reinterpret_cast<uint1_t>(ecx19 == 0) || !*reinterpret_cast<unsigned char*>(&eax12)) {
        ebp20 = 0;
    }
    if (0) {
        v21 = reinterpret_cast<uint64_t>(static_cast<int64_t>(ebp20));
        if (r14_22 && ebp20) {
            v23 = r9_7;
            rbx24 = ebp20;
            v25 = ebp20;
            rbp26 = 0;
            do {
                rsi8 = r14_27;
                *reinterpret_cast<int32_t*>(&rdi16) = 32;
                *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
                ++rbp26;
                fun_2650(32, rsi8);
            } while (rbp26 != rbx24);
            r9_7 = v23;
            ebp20 = v25;
        }
        r15_28 = r15_29 + v21;
        *reinterpret_cast<int32_t*>(&r13_15) = 2 - ebp20;
    }
    if (r15_28 > 0xfffffffffffffffd) 
        goto 0x36a8;
    if (r14_30) {
        *reinterpret_cast<int32_t*>(&rdi16) = v18;
        *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
        rsi8 = r14_31;
        fun_2650(rdi16, rsi8);
        r9_7 = r9_7;
    }
    rbp32 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r12_17)));
    r15_33 = r15_28 + 1;
    *reinterpret_cast<int32_t*>(&rax34) = 0;
    *reinterpret_cast<int32_t*>(&rax34 + 4) = 0;
    r12_35 = rbp32;
    if (*reinterpret_cast<int32_t*>(&r13_15) = *reinterpret_cast<int32_t*>(&r13_15) - 1, *reinterpret_cast<int32_t*>(&r13_15) < 0) {
        addr_40b4_35:
        if (reinterpret_cast<unsigned char>(~r15_33) <= reinterpret_cast<unsigned char>(r12_35)) 
            goto 0x36a8;
    } else {
        goto addr_40a7_37;
    }
    if (!r14_36) 
        goto 0x4170;
    if (reinterpret_cast<unsigned char>(rax34) > reinterpret_cast<unsigned char>(rbp32)) {
        rax37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax34) - reinterpret_cast<unsigned char>(rbp32));
        if (1) {
            v38 = r9_7;
            rbx39 = reinterpret_cast<void*>(0);
            r13_40 = rax37;
            do {
                rsi8 = r14_41;
                *reinterpret_cast<int32_t*>(&rdi16) = 48;
                *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
                rbx39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx39) + 1);
                fun_2650(48, rsi8);
            } while (r13_40 != rbx39);
        } else {
            v38 = r9_7;
            rbx42 = reinterpret_cast<void*>(0);
            r13_43 = rax37;
            do {
                rsi8 = r14_44;
                *reinterpret_cast<int32_t*>(&rdi16) = 32;
                *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
                rbx42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx42) + 1);
                fun_2650(32, rsi8);
            } while (r13_43 != rbx42);
        }
        r9_7 = v38;
    }
    if (!v45) {
        fun_2820(r9_7, rbp32, 1, r14_46);
        goto 0x4170;
    } else {
        if (!rbp32) 
            goto 0x4170;
        rax47 = fun_2470(rdi16, rsi8);
        rbx48 = rax47;
        r13_49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<unsigned char>(r9_7));
        rbp50 = r9_7;
        do {
            *reinterpret_cast<uint32_t*>(&rdx51) = reinterpret_cast<unsigned char>(v52);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx51) + 4) = 0;
            ++rbp50;
            rdi53 = (*rbx48)[rdx51 * 4];
            *reinterpret_cast<int32_t*>(&rdi53 + 4) = 0;
            fun_2650(rdi53, r14_54);
        } while (rbp50 != r13_49);
    }
    addr_40a7_37:
    rax34 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_15)));
    r12_35 = rbp32;
    if (reinterpret_cast<unsigned char>(rax34) >= reinterpret_cast<unsigned char>(rbp32)) {
        r12_35 = rax34;
        goto addr_40b4_35;
    }
    addr_4088_21:
    if (0) {
        v18 = 43;
        goto addr_4995_23;
    } else {
        rbp32 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdi16)));
        if ((2 <= *reinterpret_cast<int32_t*>(&rdi16) || !*reinterpret_cast<unsigned char*>(&eax12)) && (r12_35 = rbp32, *reinterpret_cast<int32_t*>(&rax34) = 0, *reinterpret_cast<int32_t*>(&rax34 + 4) = 0, !1)) {
            goto addr_40b4_35;
        }
    }
}

void fun_3ebc(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

void fun_3ed3(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

void fun_3eee(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

void fun_3f09(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

int64_t mktime_z(int64_t rdi, void* rsi);

void fun_3f73() {
    int64_t v1;
    int64_t rax2;
    int64_t rsi3;
    int32_t* v4;
    int64_t rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0x36b3;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = (__intrinsic() >> 2) - (rcx5 >> 63);
            if (rsi3 < 0) {
            }
        } while (rcx5);
    }
}

void fun_418d() {
    int32_t r8d1;
    int32_t r13d2;
    uint64_t r15_3;
    int64_t r14_4;
    uint64_t r12_5;
    int32_t r13d6;
    uint64_t rbp7;
    int64_t r15_8;
    int64_t r14_9;
    int32_t r13d10;
    uint64_t r12_11;
    int32_t r8d12;
    uint64_t r13_13;
    int32_t r8d14;
    uint64_t r13_15;
    void** r14_16;
    void** r14_17;
    void** r14_18;

    if (r8d1 == 45 || r13d2 < 0) {
        if (r15_3 > 0xfffffffffffffffd) 
            goto 0x36a8;
        if (!r14_4) 
            goto addr_4c07_4;
    } else {
        r12_5 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d6));
        *reinterpret_cast<int32_t*>(&rbp7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp7) + 4) = 0;
        if (r12_5) {
            rbp7 = r12_5;
        }
        if (rbp7 >= reinterpret_cast<uint64_t>(~r15_8)) 
            goto 0x36a8;
        if (!r14_9) 
            goto 0x45ba;
        if (r13d10 > 1) {
            r12_11 = r12_5 - 1;
            if (r8d12 == 48 || (*reinterpret_cast<int32_t*>(&r13_13) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0, r8d14 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_15) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0;
                do {
                    ++r13_15;
                    fun_2650(48, r14_16);
                } while (r12_11 != r13_15);
            } else {
                do {
                    ++r13_13;
                    fun_2650(32, r14_17);
                } while (r12_11 != r13_13);
            }
        }
    }
    fun_2650(9, r14_18);
    goto 0x45ba;
    addr_4c07_4:
    goto 0x45ba;
}

void fun_4210(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

void fun_422e(int32_t edi) {
    if (edi == 79) 
        goto 0x3c21;
}

void fun_42bc() {
    goto 0x3ea0;
}

struct s20 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_431e(int32_t edi) {
    int64_t rcx2;
    struct s20* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r8d7;
    int32_t v8;
    int32_t r8d9;

    if (edi == 69) 
        goto 0x3c21;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (!r8d7) {
        if (v8 == 43) 
            goto addr_4db4_4; else 
            goto addr_4383_5;
    }
    if (r8d9 != 43) {
        addr_4383_5:
    } else {
        addr_4db4_4:
        goto addr_4d90_7;
    }
    addr_3c0d_8:
    addr_4d90_7:
    if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) {
        goto addr_3c0d_8;
    } else {
        goto addr_3c0d_8;
    }
}

void fun_4395(int32_t edi) {
    int64_t v2;
    int64_t rax3;
    int32_t v4;
    uint32_t r12d5;
    unsigned char v6;
    uint32_t ecx7;
    void** v8;
    void** r8d9;
    int64_t v10;
    void* rax11;
    void** r8d12;
    void** r8d13;
    int32_t r13d14;
    void* v15;
    void* r13_16;
    int32_t r13d17;
    void* rsi18;
    int64_t r15_19;
    int64_t r14_20;
    void* rax21;
    void** v22;
    void* rbx23;
    void* rbp24;
    void** r14_25;
    void* rbx26;
    void* rbp27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    uint32_t ecx31;
    void** r14_32;
    void** v33;
    int64_t v34;

    v2 = reinterpret_cast<int64_t>(__return_address());
    if (edi) 
        goto 0x37c8;
    *reinterpret_cast<int32_t*>(&rax3) = v4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    r12d5 = v6;
    ecx7 = r12d5;
    rax11 = __strftime_internal_isra_0(0, "%m/%d/%y", v8, *reinterpret_cast<unsigned char*>(&ecx7), r8d9, -1, v10, rax3, v2);
    r8d12 = r8d13;
    if (r8d12 == 45 || r13d14 < 0) {
        v15 = rax11;
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_16) + 4) = 0;
    } else {
        r13_16 = reinterpret_cast<void*>(static_cast<int64_t>(r13d17));
        rsi18 = r13_16;
        if (reinterpret_cast<uint64_t>(rax11) >= reinterpret_cast<uint64_t>(r13_16)) {
            rsi18 = rax11;
        }
        v15 = rsi18;
    }
    if (reinterpret_cast<uint64_t>(~r15_19) <= reinterpret_cast<uint64_t>(v15)) 
        goto 0x36a8;
    if (r14_20) {
        if (reinterpret_cast<uint64_t>(r13_16) > reinterpret_cast<uint64_t>(rax11)) {
            rax21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_16) - reinterpret_cast<uint64_t>(rax11));
            if (r8d12 == 48 || r8d12 == 43) {
                v22 = r8d12;
                rbx23 = rax21;
                rbp24 = reinterpret_cast<void*>(0);
                do {
                    rbp24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp24) + 1);
                    fun_2650(48, r14_25, 48, r14_25);
                } while (rbx23 != rbp24);
            } else {
                v22 = r8d12;
                rbx26 = rax21;
                rbp27 = reinterpret_cast<void*>(0);
                do {
                    rbp27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp27) + 1);
                    fun_2650(32, r14_28, 32, r14_28);
                } while (rbx26 != rbp27);
            }
            r8d12 = v22;
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        ecx31 = r12d5;
        __strftime_internal_isra_0(r14_32, "%m/%d/%y", v33, *reinterpret_cast<unsigned char*>(&ecx31), r8d12, -1, v34, rax29, v2);
    }
    goto 0x367f;
}

void fun_44e7(int32_t edi) {
    int32_t r8d2;

    if (edi == 69) 
        goto 0x37c8;
    if (!r8d2) {
    }
    goto 0x3ea0;
}

void fun_450d(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3c10;
}

void fun_4540() {
    int32_t r8d1;
    int32_t r13d2;
    uint64_t r15_3;
    int64_t r14_4;
    uint64_t r12_5;
    int32_t r13d6;
    uint64_t rbp7;
    int64_t r15_8;
    int64_t r14_9;
    int32_t r13d10;
    uint64_t r12_11;
    int32_t r8d12;
    uint64_t r13_13;
    int32_t r8d14;
    uint64_t r13_15;
    void** r14_16;
    void** r14_17;
    void** r14_18;

    if (r8d1 == 45 || r13d2 < 0) {
        if (r15_3 > 0xfffffffffffffffd) 
            goto 0x36a8;
        if (!r14_4) 
            goto addr_4bea_4;
    } else {
        r12_5 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d6));
        *reinterpret_cast<int32_t*>(&rbp7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp7) + 4) = 0;
        if (r12_5) {
            rbp7 = r12_5;
        }
        if (rbp7 >= reinterpret_cast<uint64_t>(~r15_8)) 
            goto 0x36a8;
        if (!r14_9) 
            goto 0x45ba;
        if (r13d10 > 1) {
            r12_11 = r12_5 - 1;
            if (r8d12 == 48 || (*reinterpret_cast<int32_t*>(&r13_13) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0, r8d14 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_15) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0;
                do {
                    ++r13_15;
                    fun_2650(48, r14_16);
                } while (r12_11 != r13_15);
            } else {
                do {
                    ++r13_13;
                    fun_2650(32, r14_17);
                } while (r12_11 != r13_13);
            }
        }
    }
    fun_2650(10, r14_18);
    addr_4bea_4:
    goto 0x45ba;
}

void fun_45de(int32_t edi) {
    int32_t r8d2;
    int32_t r8d3;
    int32_t v4;

    if (edi == 69) 
        goto 0x3c21;
    if (edi == 79) 
        goto 0x37c8;
    if (r8d2) {
        if (r8d3 == 43) 
            goto "???";
        goto 0x3c0d;
    } else {
        if (v4 == 43) {
            goto 0x4d90;
        }
    }
}

void fun_463f() {
    void** rsi1;
    unsigned char v2;
    void** rdi3;
    void** v4;
    signed char bpl5;
    signed char v6;
    void** rax7;
    void** v8;
    int32_t r13d9;
    int32_t r8d10;
    void** r12_11;
    void** v12;
    int32_t r13d13;
    int64_t r15_14;
    int64_t r14_15;
    void* r12_16;
    void* r13_17;
    void** r14_18;
    void* r13_19;
    void** r14_20;
    signed char bpl21;
    void* rdx22;
    int64_t rcx23;
    int32_t** rax24;
    unsigned char* rbp25;
    unsigned char* v26;
    int32_t** r12_27;
    unsigned char* r13_28;
    int64_t rdx29;
    void** rdi30;
    void** r14_31;
    void** v32;
    void** r14_33;
    void**** rax34;
    unsigned char* rbp35;
    unsigned char* v36;
    void**** r12_37;
    unsigned char* r13_38;
    int64_t rdx39;
    void** rdi40;
    void** r14_41;

    *reinterpret_cast<uint32_t*>(&rsi1) = v2;
    *reinterpret_cast<int32_t*>(&rsi1 + 4) = 0;
    rdi3 = v4;
    if (bpl5) {
        *reinterpret_cast<uint32_t*>(&rsi1) = 0;
        *reinterpret_cast<int32_t*>(&rsi1 + 4) = 0;
    }
    v6 = *reinterpret_cast<signed char*>(&rsi1);
    rax7 = fun_25c0(rdi3);
    v8 = rax7;
    if (r13d9 < 0 || r8d10 == 45) {
        *reinterpret_cast<int32_t*>(&r12_11) = 0;
        *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
        v12 = v8;
    } else {
        r12_11 = reinterpret_cast<void**>(static_cast<int64_t>(r13d13));
        if (reinterpret_cast<unsigned char>(rax7) < reinterpret_cast<unsigned char>(r12_11)) {
            rax7 = r12_11;
        }
        v12 = rax7;
    }
    if (reinterpret_cast<unsigned char>(~r15_14) <= reinterpret_cast<unsigned char>(v12)) 
        goto 0x36a8;
    if (!r14_15) 
        goto 0x3a90;
    if (reinterpret_cast<unsigned char>(v8) < reinterpret_cast<unsigned char>(r12_11)) {
        r12_16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_11) - reinterpret_cast<unsigned char>(v8));
        if (r8d10 == 48 || r8d10 == 43) {
            if (r12_16) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_17) + 4) = 0;
                do {
                    rsi1 = r14_18;
                    *reinterpret_cast<int32_t*>(&rdi3) = 48;
                    *reinterpret_cast<int32_t*>(&rdi3 + 4) = 0;
                    r13_17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_17) + 1);
                    fun_2650(48, rsi1);
                } while (r12_16 != r13_17);
            }
        } else {
            *reinterpret_cast<int32_t*>(&r13_19) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_19) + 4) = 0;
            if (r12_16) {
                do {
                    rsi1 = r14_20;
                    *reinterpret_cast<int32_t*>(&rdi3) = 32;
                    *reinterpret_cast<int32_t*>(&rdi3 + 4) = 0;
                    r13_19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_19) + 1);
                    fun_2650(32, rsi1);
                } while (r12_16 != r13_19);
            }
        }
    }
    if (bpl21) {
        if (!v8) 
            goto 0x3a90;
        rax24 = fun_2860(rdi3, rsi1, rdx22, rcx23);
        rbp25 = v26;
        r12_27 = rax24;
        r13_28 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(v8) + reinterpret_cast<uint64_t>(rbp25));
        do {
            *reinterpret_cast<uint32_t*>(&rdx29) = *rbp25;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
            ++rbp25;
            *reinterpret_cast<int32_t*>(&rdi30) = (*r12_27)[rdx29];
            *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
            fun_2650(rdi30, r14_31);
        } while (rbp25 != r13_28);
        goto 0x3a90;
    } else {
        if (!v6) {
            fun_2820(v32, v8, 1, r14_33);
            goto 0x3a90;
        } else {
            if (!v8) 
                goto 0x3a90;
            rax34 = fun_2470(rdi3, rsi1);
            rbp35 = v36;
            r12_37 = rax34;
            r13_38 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(v8) + reinterpret_cast<uint64_t>(rbp35));
            do {
                *reinterpret_cast<uint32_t*>(&rdx39) = *rbp35;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx39) + 4) = 0;
                ++rbp35;
                rdi40 = (*r12_37)[rdx39 * 4];
                *reinterpret_cast<int32_t*>(&rdi40 + 4) = 0;
                fun_2650(rdi40, r14_41);
            } while (rbp35 != r13_38);
            goto 0x3a90;
        }
    }
}

struct s21 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_473a(int32_t edi) {
    int64_t rdx2;
    struct s21* v3;
    int64_t rcx4;
    int64_t rdx5;
    int32_t r8d6;
    int32_t r8d7;
    int32_t v8;

    if (edi == 69) 
        goto 0x3c21;
    rdx2 = v3->f14;
    rcx4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rcx4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rcx4) >> 31)) * 100 < 0) {
        if (*reinterpret_cast<int32_t*>(&rcx4) <= 0xfffff893) {
        }
    }
    if (r8d6) {
        if (r8d7 != 43) {
            addr_3bf8_8:
        } else {
            addr_4d44_9:
        }
    } else {
        if (v8 == 43) 
            goto addr_4d44_9; else 
            goto addr_3bf8_8;
    }
}

struct s22 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s23 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_4783() {
    uint32_t eax1;
    struct s22* rbx2;
    uint64_t rcx3;
    int64_t rbx4;
    struct s23* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r11_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&rcx3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++rcx3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + rcx3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0x37c8;
    if (v5->f20 < 0) 
        goto 0x367f;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r11_9 = eax8 * 0xffffffff88888889 >> 32;
    if (rcx3 == 2) {
        addr_4ecf_10:
        goto 0x3c10;
    } else {
        if (rcx3 > 2) {
            if (rcx3 != 3) 
                goto 0x37c8;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_4ecf_10;
        } else {
            if (!rcx3) {
                goto 0x3c10;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r11_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0x3c10;
    }
    goto 0x3c10;
}

void fun_4877(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3ea0;
}

void fun_4895(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3c10;
}

void fun_48c8(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x44f4;
}

void fun_48de(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x44f4;
}

void fun_48f4() {
    goto 0x47a3;
}

void fun_4940() {
    int1_t zf1;
    signed char bpl2;

    zf1 = bpl2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0x3946;
}

uint32_t fun_2660(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2850(int64_t rdi, void** rsi);

uint32_t fun_2840(void** rdi, void** rsi);

void** fun_2870(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_5675() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_25a0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_25a0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_25c0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_5973_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_5973_22; else 
                            goto addr_5d6d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_5e2d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_6180_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_5970_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_5970_30; else 
                                goto addr_6199_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_25c0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_6180_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2660(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_6180_28; else 
                            goto addr_581c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_62e0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_6160_40:
                        if (r11_27 == 1) {
                            addr_5ced_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_62a8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_5927_44;
                            }
                        } else {
                            goto addr_6170_46;
                        }
                    } else {
                        addr_62ef_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_5ced_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_5973_22:
                                if (v47 != 1) {
                                    addr_5ec9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_25c0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_5f14_54;
                                    }
                                } else {
                                    goto addr_5980_56;
                                }
                            } else {
                                addr_5925_57:
                                ebp36 = 0;
                                goto addr_5927_44;
                            }
                        } else {
                            addr_6154_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_62ef_47; else 
                                goto addr_615e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_5ced_41;
                        if (v47 == 1) 
                            goto addr_5980_56; else 
                            goto addr_5ec9_52;
                    }
                }
                addr_59e1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_5878_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_589d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_5ba0_65;
                    } else {
                        addr_5a09_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_6258_67;
                    }
                } else {
                    goto addr_5a00_69;
                }
                addr_58b1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_58fc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_6258_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_58fc_81;
                }
                addr_5a00_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_589d_64; else 
                    goto addr_5a09_66;
                addr_5927_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_59df_91;
                if (v22) 
                    goto addr_593f_93;
                addr_59df_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_59e1_62;
                addr_5f14_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_669b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_670b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_650f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2850(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2840(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_600e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_59cc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_6018_112;
                    }
                } else {
                    addr_6018_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_60e9_114;
                }
                addr_59d8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_59df_91;
                while (1) {
                    addr_60e9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_65f7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_6056_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_6605_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_60d7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_60d7_128;
                        }
                    }
                    addr_6085_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_60d7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_6056_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6085_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_58fc_81;
                addr_6605_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6258_67;
                addr_669b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_600e_109;
                addr_670b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_600e_109;
                addr_5980_56:
                rax93 = fun_2870(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_59cc_110;
                addr_615e_59:
                goto addr_6160_40;
                addr_5e2d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_5973_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_59d8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5925_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_5973_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5e72_160;
                if (!v22) 
                    goto addr_6247_162; else 
                    goto addr_6453_163;
                addr_5e72_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_6247_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_6258_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_5d1b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_5b83_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_58b1_70; else 
                    goto addr_5b97_169;
                addr_5d1b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_5878_63;
                goto addr_5a00_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_6154_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_628f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5970_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5868_178; else 
                        goto addr_6212_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6154_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5973_22;
                }
                addr_628f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_5970_30:
                    r8d42 = 0;
                    goto addr_5973_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_59e1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_62a8_42;
                    }
                }
                addr_5868_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5878_63;
                addr_6212_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_6170_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_59e1_62;
                } else {
                    addr_6222_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_5973_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_69d2_188;
                if (v28) 
                    goto addr_6247_162;
                addr_69d2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_5b83_168;
                addr_581c_37:
                if (v22) 
                    goto addr_6813_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_5833_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_62e0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_636b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5973_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5868_178; else 
                        goto addr_6347_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6154_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5973_22;
                }
                addr_636b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_5973_22;
                }
                addr_6347_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6170_46;
                goto addr_6222_186;
                addr_5833_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_5973_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_5973_22; else 
                    goto addr_5844_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_691e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_67a4_210;
            if (1) 
                goto addr_67a2_212;
            if (!v29) 
                goto addr_63de_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_25b0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_6911_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_5ba0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_595b_219; else 
            goto addr_5bba_220;
        addr_593f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_5953_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_5bba_220; else 
            goto addr_595b_219;
        addr_650f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_5bba_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_25b0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_652d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_25b0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_69a0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_6406_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_65f7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5953_221;
        addr_6453_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5953_221;
        addr_5b97_169:
        goto addr_5ba0_65;
        addr_691e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_5bba_220;
        goto addr_652d_222;
        addr_67a4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_67fe_236;
        fun_25d0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_69a0_225;
        addr_67a2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_67a4_210;
        addr_63de_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_67a4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_6406_226;
        }
        addr_6911_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_5d6d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa8cc + rax113 * 4) + 0xa8cc;
    addr_6199_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa9cc + rax114 * 4) + 0xa9cc;
    addr_6813_190:
    addr_595b_219:
    goto 0x5640;
    addr_5844_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa7cc + rax115 * 4) + 0xa7cc;
    addr_67fe_236:
    goto v116;
}

void fun_5860() {
}

void fun_5a18() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x5712;
}

void fun_5a71() {
    goto 0x5712;
}

void fun_5b5e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x59e1;
    }
    if (v2) 
        goto 0x6453;
    if (!r10_3) 
        goto addr_65be_5;
    if (!v4) 
        goto addr_648e_7;
    addr_65be_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_648e_7:
    goto 0x5894;
}

void fun_5b7c() {
}

void fun_5c27() {
    signed char v1;

    if (v1) {
        goto 0x5baf;
    } else {
        goto 0x58ea;
    }
}

void fun_5c41() {
    signed char v1;

    if (!v1) 
        goto 0x5c3a; else 
        goto "???";
}

void fun_5c68() {
    goto 0x5b83;
}

void fun_5ce8() {
}

void fun_5d00() {
}

void fun_5d2f() {
    goto 0x5b83;
}

void fun_5d81() {
    goto 0x5d10;
}

void fun_5db0() {
    goto 0x5d10;
}

void fun_5de3() {
    goto 0x5d10;
}

void fun_61b0() {
    goto 0x5868;
}

void fun_64ae() {
    signed char v1;

    if (v1) 
        goto 0x6453;
    goto 0x5894;
}

void fun_6555() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x5894;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x5878;
        goto 0x5894;
    }
}

void fun_6972() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5be0;
    } else {
        goto 0x5712;
    }
}

void fun_8058() {
    fun_25a0();
}

void fun_3c4a(int32_t edi) {
    signed char bpl2;

    if (edi) 
        goto 0x37c8;
    if (bpl2) {
    }
    goto 0x3963;
}

struct s24 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_3a9a(int32_t edi, signed char sil) {
    int64_t r9_3;
    struct s24* v4;
    uint32_t ecx5;
    int64_t rax6;
    int32_t eax7;
    uint64_t rdx8;
    int64_t rdx9;
    int64_t rbp10;
    int64_t rdx11;
    int32_t eax12;
    int64_t rax13;
    uint32_t edx14;
    int32_t edx15;
    uint32_t edx16;
    uint32_t r10d17;
    uint32_t edx18;
    int64_t r10_19;
    uint64_t rax20;
    int64_t rax21;
    int64_t r11_22;
    int32_t eax23;
    int32_t r8d24;
    int32_t v25;
    int64_t rdx26;
    int32_t ecx27;
    int64_t rdx28;
    int32_t r8d29;
    int32_t v30;
    int32_t r8d31;

    if (edi == 69) 
        goto 0x37c8;
    *reinterpret_cast<int32_t*>(&r9_3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_3) + 4) = 0;
    ecx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&r9_3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    eax7 = static_cast<int32_t>(r9_3 + rax6 - 100);
    rdx8 = reinterpret_cast<int32_t>(ecx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rdx9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rdx8) + ecx5) >> 2) - (reinterpret_cast<int32_t>(ecx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp10) = static_cast<int32_t>(rdx9 * 8) - *reinterpret_cast<int32_t*>(&rdx9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx11) = v4->f1c - ecx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
    if (static_cast<int32_t>(rdx11 + rbp10 + 3) < 0) {
        eax12 = eax7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 3) && reinterpret_cast<uint32_t>(eax12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        edx14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&eax7) & 3) && (edx14 = 0x16e, reinterpret_cast<uint32_t>(eax7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            edx15 = eax7 % 0x190;
            edx16 = reinterpret_cast<uint32_t>(-edx15);
            edx14 = edx16 - (edx16 + reinterpret_cast<uint1_t>(edx16 < edx16 + reinterpret_cast<uint1_t>(!!edx15))) + 0x16e;
        }
        r10d17 = v4->f1c - edx14;
        edx18 = r10d17 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r10_19) = r10d17 - edx18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_19) + 4) = 0;
        rax20 = reinterpret_cast<int32_t>(edx18) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax21) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax20) + edx18) >> 2) - (reinterpret_cast<int32_t>(edx18) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r11_22) = static_cast<int32_t>(rax21 * 8) - *reinterpret_cast<int32_t*>(&rax21);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_22) + 4) = 0;
        eax23 = static_cast<int32_t>(r10_19 + r11_22 + 3);
        if (eax23 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax23 >> 31) + 1;
    }
    if (sil == 71) {
        if (r8d24) 
            goto 0x4d79;
        if (v25 == 43) 
            goto 0x5005;
        goto 0x3c0d;
    }
    if (sil != 0x67) {
        goto 0x3ea0;
    }
    rdx26 = *reinterpret_cast<int32_t*>(&r9_3) * 0x51eb851f >> 37;
    ecx27 = *reinterpret_cast<int32_t*>(&r9_3) - (*reinterpret_cast<int32_t*>(&rdx26) - (*reinterpret_cast<int32_t*>(&r9_3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
    rdx28 = ecx27 * 0x51eb851f >> 37;
    if (ecx27 - (*reinterpret_cast<int32_t*>(&rdx28) - (ecx27 >> 31)) * 100 >= 0) 
        goto "???";
    if (*reinterpret_cast<int32_t*>(&r9_3) < 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
        goto addr_4d2f_19;
    if (r8d29) 
        goto 0x4d3a;
    if (v30 == 43) 
        goto 0x4d44;
    goto 0x3bf8;
    addr_4d2f_19:
    if (!r8d31) 
        goto 0x3be9; else 
        goto "???";
}

void fun_45c2(int32_t edi) {
    if (edi == 69) 
        goto 0x37c8;
    goto 0x3f4e;
}

void fun_4178() {
    int64_t rbx1;
    int64_t r12_2;
    int32_t r8d3;
    int32_t r13d4;
    uint64_t r15_5;
    int64_t r14_6;
    uint64_t r12_7;
    int32_t r13d8;
    uint64_t rbp9;
    int64_t r15_10;
    int64_t r14_11;
    int32_t r13d12;
    uint64_t r12_13;
    int32_t r8d14;
    uint64_t r13_15;
    int32_t r8d16;
    uint64_t r13_17;
    void** r14_18;
    void** r14_19;
    void** rdi20;
    signed char* rbx21;
    void** r14_22;

    if (rbx1 - 1 != r12_2) {
        goto 0x37c8;
    }
    if (r8d3 == 45 || r13d4 < 0) {
        if (r15_5 > 0xfffffffffffffffd) 
            goto 0x36a8;
        if (!r14_6) 
            goto addr_4e5c_6;
    } else {
        r12_7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d8));
        *reinterpret_cast<int32_t*>(&rbp9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp9) + 4) = 0;
        if (r12_7) {
            rbp9 = r12_7;
        }
        if (rbp9 >= reinterpret_cast<uint64_t>(~r15_10)) 
            goto 0x36a8;
        if (!r14_11) 
            goto 0x45ba;
        if (r13d12 > 1) {
            r12_13 = r12_7 - 1;
            if (r8d14 == 48 || (*reinterpret_cast<int32_t*>(&r13_15) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0, r8d16 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_17) + 4) = 0;
                do {
                    ++r13_17;
                    fun_2650(48, r14_18);
                } while (r12_13 != r13_17);
            } else {
                do {
                    ++r13_15;
                    fun_2650(32, r14_19);
                } while (r12_13 != r13_15);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi20) = *rbx21;
    *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
    fun_2650(rdi20, r14_22);
    goto 0x45ba;
    addr_4e5c_6:
    goto 0x45ba;
}

void fun_42fc(int32_t edi) {
    signed char bpl2;

    if (edi == 69) 
        goto 0x37c8;
    if (bpl2) {
    }
    goto 0x3946;
}

void fun_44b5(int32_t edi) {
    int32_t r13d2;
    int32_t r8d3;
    int64_t r13_4;
    int64_t rax5;
    int32_t v6;
    uint32_t ecx7;
    unsigned char v8;
    void** v9;
    int64_t v10;

    if (edi) 
        goto 0x37c8;
    if (r13d2 >= 0 || r8d3) {
        if (static_cast<int32_t>(r13_4 - 6) < 0) {
        }
        goto 0x43ae;
    } else {
        *reinterpret_cast<int32_t*>(&rax5) = v6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        ecx7 = v8;
        __strftime_internal_isra_0(0, "%Y-%m-%d", v9, *reinterpret_cast<unsigned char*>(&ecx7), 43, 4, v10, rax5, __return_address());
        goto 0x4408;
    }
}

void fun_48fb() {
    goto 0x43ae;
}

void fun_4947() {
    goto 0x43ae;
}

void fun_5a9e() {
    goto 0x5712;
}

void fun_5c74() {
    goto 0x5c2c;
}

void fun_5d3b() {
    goto 0x5868;
}

void fun_5d8d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x5d10;
    goto 0x593f;
}

void fun_5dbf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x5d1b;
        goto 0x5740;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x5bba;
        goto 0x595b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x6558;
    if (r10_8 > r15_9) 
        goto addr_5ca5_9;
    addr_5caa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x6563;
    goto 0x5894;
    addr_5ca5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_5caa_10;
}

void fun_5df2() {
    goto 0x5927;
}

void fun_61c0() {
    goto 0x5927;
}

void fun_695f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x5a7c;
    } else {
        goto 0x5be0;
    }
}

void fun_8110() {
}

void fun_3938(int32_t edi) {
    if (edi == 79) 
        goto 0x37c8; else 
        goto "???";
}

void fun_4911() {
}

void fun_5dfc() {
    goto 0x5d97;
}

void fun_61ca() {
    goto 0x5ced;
}

void fun_8170() {
    fun_25a0();
    goto fun_2830;
}

void fun_5acd() {
    goto 0x5712;
}

void fun_5e08() {
    goto 0x5d97;
}

void fun_61d7() {
    goto 0x5d3e;
}

void fun_81b0() {
    fun_25a0();
    goto fun_2830;
}

void fun_5afa() {
    goto 0x5712;
}

void fun_5e14() {
    goto 0x5d10;
}

void fun_81f0() {
    fun_25a0();
    goto fun_2830;
}

void fun_5b1c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x64b0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x59e1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x59e1;
    }
    if (v11) 
        goto 0x6813;
    if (r10_12 > r15_13) 
        goto addr_6863_8;
    addr_6868_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x65a1;
    addr_6863_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_6868_9;
}

struct s25 {
    signed char[24] pad24;
    int64_t f18;
};

struct s26 {
    signed char[16] pad16;
    void** f10;
};

struct s27 {
    signed char[8] pad8;
    void** f8;
};

void fun_8240() {
    int64_t r15_1;
    struct s25* rbx2;
    void** r14_3;
    struct s26* rbx4;
    void** r13_5;
    struct s27* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_25a0();
    fun_2830(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x8262, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_8298() {
    fun_25a0();
    goto 0x8269;
}

struct s28 {
    signed char[32] pad32;
    int64_t f20;
};

struct s29 {
    signed char[24] pad24;
    int64_t f18;
};

struct s30 {
    signed char[16] pad16;
    void** f10;
};

struct s31 {
    signed char[8] pad8;
    void** f8;
};

struct s32 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_82d0() {
    int64_t rcx1;
    struct s28* rbx2;
    int64_t r15_3;
    struct s29* rbx4;
    void** r14_5;
    struct s30* rbx6;
    void** r13_7;
    struct s31* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s32* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_25a0();
    fun_2830(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x8304, __return_address(), rcx1);
    goto v15;
}

void fun_8348() {
    fun_25a0();
    goto 0x830b;
}
