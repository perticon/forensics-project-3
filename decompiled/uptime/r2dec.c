#include <stdint.h>

/* /tmp/tmpdb8sk3u8 @ 0x29d0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpdb8sk3u8 @ 0x54f0 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000a72f;
        rdx = 0x0000a720;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000a727;
        rdx = 0x0000a729;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000a72b;
    rdx = 0x0000a724;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x9580 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x2740 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpdb8sk3u8 @ 0x55d0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2880)() ();
    }
    rdx = 0x0000a7a0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xa7a0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000a733;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000a729;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000a7cc;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xa7cc */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000a727;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000a729;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000a727;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000a729;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000a8cc;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa8cc */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000a9cc;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa9cc */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000a729;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000a727;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000a727;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000a729;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpdb8sk3u8 @ 0x2880 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 28450 named .text */
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x69f0 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2885)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2885 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x288a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x24c0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2890 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2895 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x289a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x289f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28a4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28a9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28ae */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28b3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28b8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2ac0 */
 
int64_t dbg_print_uptime (int64_t arg1, int64_t arg2) {
    char * end_ptr;
    double[3] avg;
    char[8192] buf;
    int64_t var_2048h;
    rdi = arg1;
    rsi = arg2;
    /* void print_uptime(size_t n,STRUCT_UTMP const * this); */
    rax = *(fs:0x28);
    *((rsp + 0x2048)) = rax;
    eax = 0;
    r13 = rdi;
    rbx = rsi;
    rbp = r13 - 1;
    rax = fopen ("/proc/uptime", 0x0000a0a5);
    if (rax != 0) {
        r15 = rsp + 0x40;
        rdx = rax;
        esi = _init;
        r12 = rax;
        rdi = r15;
        rax = fgets_unlocked ();
        r14 = rax;
        if (rax == r15) {
            goto label_9;
        }
label_5:
        ax = rpl_fclose (r12);
    }
    r14d = 0;
    if (r13 == 0) {
        goto label_10;
    }
label_6:
    r13d = 0;
    r12d = 0;
    while (*((rbx + 0x2c)) == 0) {
label_0:
        if (ax == 2) {
            r13 = *((rbx + 0x154));
        }
        rbx += 0x180;
        rbp--;
        if (rbp < 0) {
            goto label_11;
        }
label_1:
        eax = *(rbx);
    }
    if (ax != 7) {
        goto label_0;
    }
    r12++;
    rbx += 0x180;
    rbp--;
    if (rbp >= 0) {
        goto label_1;
    }
label_11:
    rax = time (0);
    *((rsp + 0x18)) = rax;
    if (r14 == 0) {
        rax -= r13;
        r14 = rax;
        if (r13 == 0) {
            goto label_12;
        }
    }
    r8 = rsp + 0x18;
label_7:
    rdx = 0x1845c8a0ce512957;
    rax = r14;
    rdx:rax = rax * rdx;
    rax = r14;
    rax >>= 0x3f;
    rdx >>= 0xd;
    rdx -= rax;
    rcx = rdx * 0xfffffffffffeae80;
    r13 = rdx;
    rdx = 0x48d159e26af37c05;
    rcx += r14;
    rax = rcx;
    rdx:rax = rax * rdx;
    rax = rcx;
    rax >>= 0x3f;
    rdx >>= 0xa;
    rdx -= rax;
    r15d = edx;
    edx *= 0xe10;
    rdx = (int64_t) edx;
    rcx -= rdx;
    rdx = 0x8888888888888889;
    rax = rcx;
    rdx:rax = rax * rdx;
    rbx = rdx + rcx;
    rcx >>= 0x3f;
    rbx >>= 5;
    rbx -= rcx;
    rax = localtime (r8);
    edx = 5;
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_13;
    }
    rax = dcgettext (0, " %H:%M:%S  ");
    rdx = *((rsp + 8));
    r8d = 0;
    ecx = 0;
    fprintftime (*(obj.stdout), rax);
label_3:
    if (r14 == -1) {
        goto label_14;
    }
    if (r14 > 0x1517f) {
        goto label_15;
    }
    edx = 5;
    rax = dcgettext (0, "up  %2d:%02d,  ");
    ecx = ebp;
    edx = r15d;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    do {
label_4:
        r8d = 5;
        rcx = r12;
        rdx = "%lu users";
        edi = 0;
        rsi = "%lu user";
        rax = dcngettext ();
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        rdi = rsp + 0x20;
        esi = 3;
        eax = getloadavg ();
        ebx = eax;
        if (eax == 0xffffffff) {
            goto label_16;
        }
        if (eax > 0) {
            xmm0 = *((rsp + 0x20));
            edx = 5;
            *((rsp + 8)) = xmm0;
            rax = dcgettext (0, ",  load average: %.2f");
            xmm0 = *((rsp + 8));
            edi = 1;
            rsi = rax;
            eax = 1;
            printf_chk ();
        }
        if (ebx > 1) {
            goto label_17;
        }
        if (ebx == 1) {
            goto label_16;
        }
label_2:
        rax = *((rsp + 0x2048));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_18;
        }
        return rax;
label_15:
        rcx = r13;
        edi = 0;
        r8d = 5;
        rdx = "up %ld days %2d:%02d,  ";
        rsi = "up %ld day %2d:%02d,  ";
        rax = dcngettext ();
        r8d = ebx;
        ecx = r15d;
        rdx = r13;
        rsi = rax;
        edi = 1;
        eax = 0;
        printf_chk ();
    } while (1);
label_17:
    xmm0 = *((rsp + 0x28));
    edi = 1;
    eax = 1;
    rbp = ", %.2f";
    rsi = rbp;
    printf_chk ();
    if (ebx != 2) {
        xmm0 = *((rsp + 0x30));
        rsi = rbp;
        edi = 1;
        eax = 1;
        printf_chk ();
    }
label_16:
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_19;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0xa;
    goto label_2;
label_13:
    rax = dcgettext (0, " ??:????  ");
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    goto label_3;
label_14:
    edx = 5;
    rax = dcgettext (0, "up ???? days ??:??,  ");
    edi = 1;
    rsi = rax;
    eax = 0;
    rax = printf_chk ();
    goto label_4;
label_9:
    r8 = rsp + 0x18;
    rsi = r8;
    *((rsp + 8)) = r8;
    c_strtod (rax, rsi);
    if (*((rsp + 0x18)) == r14) {
        goto label_5;
    }
    __asm ("comisd xmm0, xmmword [0x0000a490]");
    r8 = *((rsp + 8));
    if (*((rsp + 0x18)) < r14) {
        goto label_20;
    }
    xmm1 = *(0x0000a498);
    __asm ("comisd xmm1, xmm0");
    if (*((rsp + 0x18)) <= r14) {
        goto label_20;
    }
    __asm ("cvttsd2si r14, xmm0");
    rpl_fclose (r12);
    r8 = *((rsp + 8));
    if (r13 != 0) {
        goto label_6;
    }
    *((rsp + 8)) = r8;
    rax = time (0);
    r8 = *((rsp + 8));
    *((rsp + 0x18)) = rax;
    if (r14 == 0) {
        goto label_12;
    }
label_8:
    r12d = 0;
    goto label_7;
label_20:
    *((rsp + 8)) = r8;
    r14 |= 0xffffffffffffffff;
    rpl_fclose (r12);
    r8 = *((rsp + 8));
    if (r13 != 0) {
        goto label_6;
    }
    *((rsp + 8)) = r8;
    rax = time (0);
    r8 = *((rsp + 8));
    *((rsp + 0x18)) = rax;
    goto label_8;
label_10:
    rax = time (0);
    *((rsp + 0x18)) = rax;
label_12:
    edx = 5;
    rax = dcgettext (0, "couldn't get boot time");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_19:
    esi = 0xa;
    overflow ();
    goto label_2;
label_18:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2f60 */
 
int64_t dbg_uptime (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1, int64_t arg2) {
    size_t n_users;
    STRUCT_UTMP * utmp_buf;
    int64_t var_78h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* void uptime(char const * filename,int options); */
    ecx = esi;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    *((rsp + 0x10)) = 0;
    eax = read_utmp (rdi, rsp + 8, rsp + 0x10);
    if (eax == 0) {
        print_uptime (*((rsp + 8)), *((rsp + 0x10)));
        exit (0);
    }
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    rax = error (1, *(rax), 0x0000a4bf);
}

/* /tmp/tmpdb8sk3u8 @ 0x7840 */
 
int64_t dbg_read_utmp (int64_t arg2, int64_t arg3, int64_t arg4) {
    idx_t n_alloc;
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_18h;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int read_utmp(char const * file,size_t * n_entries,STRUCT_UTMP ** utmp_buf,int options); */
    r15d = 0;
    r14 = rsi;
    r12d = ecx;
    ebp &= 2;
    ebx = 0;
    *((rsp + 8)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    *((rsp + 0x10)) = 0;
    utmpxname ();
    setutxent ();
    do {
label_2:
        rax = getutxent ();
        r13 = rax;
        if (rax == 0) {
            goto label_4;
        }
label_0:
        edi = *((r13 + 4));
        if (*((r13 + 0x2c)) != 0) {
            if (*(r13) == 7) {
                goto label_5;
            }
        }
    } while (ebp != 0);
label_1:
    if (*((rsp + 0x10)) == rbx) {
        goto label_6;
    }
label_3:
    rax = rbx * 3;
    rdx = *(r13);
    rsi = r13;
    rbx++;
    rax <<= 7;
    rax += r15;
    *(rax) = rdx;
    rdi = rax + 8;
    rdx = *((r13 + 0x178));
    rdi &= 0xfffffffffffffff8;
    *((rax + 0x178)) = rdx;
    rax -= rdi;
    rsi -= rax;
    eax += 0x180;
    eax >>= 3;
    ecx = eax;
    *(rdi) = *(rsi);
    rcx--;
    rsi += 8;
    rdi += 8;
    rax = getutxent ();
    r13 = rax;
    if (rax != 0) {
        goto label_0;
    }
label_4:
    endutxent ();
    rax = *((rsp + 8));
    *(r14) = rbx;
    *(rax) = r15;
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_7;
    }
    eax = 0;
    return rax;
label_5:
    if ((r12b & 1) == 0) {
        goto label_1;
    }
    if (edi <= 0) {
        goto label_1;
    }
    eax = kill (rdi, 0);
    if (eax >= 0) {
        goto label_1;
    }
    rax = errno_location ();
    if (*(rax) == 3) {
        goto label_2;
    }
    if (*((rsp + 0x10)) != rbx) {
        goto label_3;
    }
label_6:
    r8d = 0x180;
    rax = xpalloc (r15, rsp + 0x10, 1, 0xffffffffffffffff);
    r15 = rax;
    goto label_3;
label_7:
    return stack_chk_fail ();
    do {
    } while (rcx != 0);
}

/* /tmp/tmpdb8sk3u8 @ 0x2810 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpdb8sk3u8 @ 0x7430 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x28a4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x25d0 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpdb8sk3u8 @ 0x24d0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27b0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpdb8sk3u8 @ 0x25a0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2830 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2780 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2670 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2690 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2770 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpdb8sk3u8 @ 0x24f0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2a00 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x2a30 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x2a70 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002460 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpdb8sk3u8 @ 0x2460 */
 
void fcn_00002460 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2ab0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpdb8sk3u8 @ 0x35a0 */
 
uint32_t rotate_right32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t strftime_internal_isra_0 (int64_t arg_500h, int64_t arg_508h, int64_t arg1, tm * arg2, tm * arg3, uint32_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_17h;
    tm * timeptr;
    tm * var_20h;
    int64_t var_28h;
    uint32_t var_2ch;
    char * var_30h;
    uint32_t var_38h;
    tm * size;
    tm * var_50h;
    tm * var_58h;
    tm * var_60h;
    tm * var_68h;
    tm * var_6ch;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_8ch;
    int64_t var_90h;
    int64_t var_a0h;
    char * format;
    int64_t var_adh;
    int64_t var_aeh;
    int64_t var_afh;
    char * s;
    void * ptr;
    int64_t var_c7h;
    int64_t var_4b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
label_22:
    r14 = rdi;
    r13 = (int64_t) r9d;
    r12 = rsi;
    rbx = rdx;
    rax = *((rsp + 0x500));
    *((rsp + 0x17)) = cl;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x2c)) = r8d;
    *((rsp + 0x20)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0x4b8)) = rax;
    eax = 0;
    rax = errno_location ();
    rdi = *((rbx + 0x30));
    ecx = *((rbx + 8));
    *((rsp + 8)) = rax;
    eax = *(rax);
    *((rsp + 0x10)) = ecx;
    *((rsp + 0x28)) = eax;
    rax = 0x0000ab64;
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 0x30)) = rax;
    if (ecx <= 0xc) {
        goto label_68;
    }
    ecx -= 0xc;
    *((rsp + 0x10)) = ecx;
label_1:
    eax = *(r12);
    r15d = 0;
    if (al == 0) {
        goto label_69;
    }
    do {
        if (al == 0x25) {
            goto label_70;
        }
        eax = 0;
        ebx = 1;
        rdx = r15;
        __asm ("cmovns rax, r13");
        rdx = ~rdx;
        if (rax != 0) {
            rbx = rax;
        }
        if (rbx >= rdx) {
            goto label_3;
        }
        if (r14 != 0) {
            if (r13d > 1) {
                goto label_71;
            }
label_2:
            edi = *(r12);
            fputc (rdi, r14);
        }
        r15 += rbx;
        rbx = r12;
label_0:
        eax = *((rbx + 1));
        r12 = rbx + 1;
        r13 = 0xffffffffffffffff;
    } while (al != 0);
label_69:
    rax = *((rsp + 8));
    esi = *((rsp + 0x28));
    *(rax) = esi;
    goto label_72;
label_14:
    if (r15 != -1) {
        goto label_0;
    }
label_3:
    rax = *((rsp + 8));
    *(rax) = 0x22;
label_67:
    r15d = 0;
label_72:
    rax = *((rsp + 0x4b8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_73;
    }
    rax = r15;
    return rax;
label_68:
    esi = *((rsp + 0x10));
    eax = 0xc;
    if (esi != 0) {
        eax = esi;
    }
    *((rsp + 0x10)) = eax;
    goto label_1;
label_71:
    rbp = rax - 1;
    r13d = 0;
    do {
        r13++;
        fputc (0x20, r14);
    } while (rbp > r13);
    goto label_2;
label_70:
    eax = *((rsp + 0x17));
    rbx = r12;
    ebp = 0;
    r8d = 0;
    *((rsp + 0x38)) = al;
label_8:
    edx = *((rbx + 1));
    rbx++;
    ecx = rdx - 0x23;
    esi = edx;
    edi = edx;
    if (cl <= 0x3c) {
        r10 = 0x1000000000002500;
        eax = 1;
        rax <<= cl;
        if ((rax & r10) != 0) {
            goto label_74;
        }
        if (cl == 0x3b) {
            goto label_75;
        }
        eax &= 1;
        if (eax != 0) {
            goto label_76;
        }
    }
    edx -= 0x30;
    if (edx <= 9) {
        goto label_77;
    }
label_5:
    if (sil == 0x45) {
        goto label_78;
    }
    if (sil == 0x4f) {
        goto label_78;
    }
    edi = 0;
label_4:
    if (sil <= 0x7a) {
        rdx = 0x0000a4e4;
        eax = (int32_t) sil;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (123 cases) at 0xa4e4 */
        void (*rax)() ();
        rbx--;
    }
label_10:
    rax = rbx;
    rax -= r12;
    *((rsp + 0x50)) = rax;
    rbp = rax + 1;
    if (r8d != 0x2d) {
        if (r13d >= 0) {
            r13 = (int64_t) r13d;
            rax = r13;
            if (rbp >= r13) {
                rax = rbp;
            }
            *((rsp + 0x40)) = rax;
        }
    } else {
        *((rsp + 0x40)) = rbp;
        r13d = 0;
    }
    rax = r15;
    rax = ~rax;
    if (rax <= *((rsp + 0x40))) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_9;
    }
    if (rbp >= r13) {
        goto label_38;
    }
    r13 -= rbp;
    if (r8d == 0x30) {
        goto label_79;
    }
    if (r8d == 0x2b) {
        goto label_79;
    }
    edx = 0;
    if (r13 == 0) {
        goto label_38;
    }
    *((rsp + 0x58)) = rbx;
    rbx = rdx;
    do {
        rbx++;
        fputc (0x20, r14);
    } while (r13 != rbx);
label_39:
    rbx = *((rsp + 0x58));
label_38:
    if (*((rsp + 0x38)) == 0) {
        goto label_80;
    }
    if (rbp == 0) {
        goto label_9;
    }
    rax = ctype_toupper_loc ();
    r13 = rax;
    rax = *((rsp + 0x50));
    rbp = r12 + rax + 1;
    do {
        edx = *(r12);
        rax = *(r13);
        r12++;
        fputc (*((rax + rdx*4)), r14);
    } while (r12 != rbp);
label_9:
    r15 += *((rsp + 0x40));
    goto label_0;
label_78:
    esi = *((rbx + 1));
    rbx++;
    goto label_4;
label_77:
    r13d = 0;
    goto label_81;
label_6:
    eax = *(rbx);
    eax -= 0x30;
    r13d += eax;
    if (r13d overflow 0) {
        goto label_82;
    }
label_7:
    edi = *((rbx + 1));
    rbx++;
    eax = rdi - 0x30;
    esi = edi;
    if (eax > 9) {
        goto label_5;
    }
label_81:
    r13d *= 0xa;
    if (eax !overflow 9) {
        goto label_6;
    }
label_82:
    r13d = 0x7fffffff;
    goto label_7;
label_74:
    r8d = edx;
    goto label_8;
label_80:
    fwrite (r12, rbp, 1, r14);
    goto label_9;
    if (edi == 0x4f) {
        goto label_10;
    }
label_12:
    *((rsp + 0x58)) = 0;
label_20:
    *((rsp + 0xab)) = bp;
    if (edi != 0) {
        goto label_83;
    }
    rax = rsp + 0xad;
label_11:
    *(rax) = sil;
    r12 = rsp + 0xb0;
    *((rax + 1)) = 0;
    *((rsp + 0x50)) = r8d;
    rax = strftime (r12, section..dynsym, rsp + 0xab, *((rsp + 0x18)));
    if (rax == 0) {
        goto label_0;
    }
    r8d = *((rsp + 0x50));
    rax = rax - 1;
    *((rsp + 0x40)) = rax;
    if (r8d == 0x2d) {
        goto label_84;
    }
    if (r13d < 0) {
        goto label_84;
    }
    r13 = (int64_t) r13d;
    if (rax < r13) {
        rax = r13;
    }
    *((rsp + 0x50)) = rax;
label_32:
    rax = r15;
    rax = ~rax;
    if (rax <= *((rsp + 0x50))) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_26;
    }
    if (*((rsp + 0x40)) >= r13) {
        goto label_42;
    }
    r13 -= *((rsp + 0x40));
    if (r8d == 0x30) {
        goto label_85;
    }
    if (r8d == 0x2b) {
        goto label_85;
    }
    edx = 0;
    if (r13 == 0) {
        goto label_42;
    }
    *((rsp + 0x60)) = rbx;
    rbx = rdx;
    do {
        rbx++;
        fputc (0x20, r14);
    } while (r13 != rbx);
label_43:
    rbx = *((rsp + 0x60));
label_42:
    if (*((rsp + 0x58)) != 0) {
        goto label_86;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_87;
    }
    if (*((rsp + 0x40)) == 0) {
        goto label_26;
    }
    rax = ctype_toupper_loc ();
    rbp += r12;
    r13 = rsp + 0xb1;
    r12 = rax;
    do {
        edx = *(r13);
        rax = *(r12);
        r13++;
        rax = fputc (*((rax + rdx*4)), r14);
    } while (r13 != rbp);
label_26:
    r15 += *((rsp + 0x50));
    goto label_0;
    if (edi == 0x45) {
        goto label_10;
    }
    rcx = *((rsp + 0x18));
    r10d = *((rcx + 0x1c));
    r11d = *((rcx + 0x18));
    r9d = *((rcx + 0x14));
    ecx = r10d;
    ecx -= r11d;
    eax = r9d;
    ecx += 0x17e;
    eax >>= 0x1f;
    rdx = (int64_t) ecx;
    eax &= 0x190;
    rdx *= 0xffffffff92492493;
    ebp >>= 0x1f;
    eax = r9 + rax - 0x64;
    rdx >>= 0x20;
    edx += ecx;
    edx >>= 2;
    edx -= ebp;
    ebp = rdx*8;
    ebp -= edx;
    edx = r10d;
    edx -= ecx;
    ecx = rdx + rbp + 3;
    if (ecx < 0) {
        goto label_88;
    }
    edx = 0x16d;
    if ((al & 3) == 0) {
        ebp = eax * 0xc28f5c29;
        edx = 0x16e;
        ebp += 0x51eb850;
        ebp = rotate_right32 (ebp, 2);
        if (ebp > 0x28f5c28) {
            goto label_89;
        }
        edx:eax = (int64_t) eax;
        eax = edx:eax / ebp;
        edx = edx:eax % ebp;
        edx = -edx;
        edx -= edx;
        edx += 0x16e;
    }
label_89:
    r10d -= edx;
    edx = r10d;
    edx -= r11d;
    edx += 0x17e;
    rax = (int64_t) edx;
    r11d = edx;
    r10d -= edx;
    rax *= 0xffffffff92492493;
    r11d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r11d;
    r11d = rax*8;
    r11d -= eax;
    eax = r10 + r11 + 3;
    __asm ("cmovns ecx, eax");
    eax >>= 0x1f;
    eax++;
label_58:
    if (sil == 0x47) {
        goto label_90;
    }
    if (sil != 0x67) {
        goto label_91;
    }
    rdx = (int64_t) r9d;
    ecx = r9d;
    rdx *= 0x51eb851f;
    ecx >>= 0x1f;
    rdx >>= 0x25;
    edx -= ecx;
    ecx = r9d;
    edx *= 0x64;
    ecx -= edx;
    ecx += eax;
    rdx = (int64_t) ecx;
    r10d = ecx;
    rdx *= 0x51eb851f;
    r10d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r10d;
    r10d = edx * 0x64;
    ecx -= r10d;
    edx = ecx;
    if (ecx < 0) {
        goto label_92;
    }
label_27:
    if (r8d != 0) {
        goto label_64;
    }
label_53:
    r8d = *((rsp + 0x2c));
    if (r8d == 0x2b) {
        goto label_65;
    }
label_54:
    *((rsp + 0x50)) = 0;
    eax = 1;
    r10d = 0;
    *((rsp + 0x40)) = 2;
label_21:
    r11d = 0;
label_15:
    if (edi != 0x4f) {
        goto label_93;
    }
    if (al == 0) {
        goto label_93;
    }
label_17:
    r9d = imp.reallocarray;
    *((rsp + 0x58)) = 0;
    *((rsp + 0xab)) = r9w;
label_83:
    *((rsp + 0xad)) = dil;
    rax = rsp + 0xae;
    goto label_11;
    if (edi != 0) {
        goto label_10;
    }
    eax = *((rsp + 0x38));
    edi = imp.reallocarray;
    *((rsp + 0x58)) = 0;
    *((rsp + 0xab)) = di;
    if (bpl != 0) {
        eax = ebp;
    }
    *((rsp + 0x38)) = al;
    rax = rsp + 0xad;
    goto label_11;
    eax = *((rsp + 0x38));
    if (bpl != 0) {
        eax = ebp;
    }
    *((rsp + 0x38)) = al;
    if (edi != 0x45) {
        goto label_12;
    }
    goto label_10;
    if (edi == 0x45) {
        goto label_10;
    }
    eax = 9;
    edx = *((rsp + 0x508));
    r12d = 9;
    if (r13d <= 0) {
        r13d = eax;
    }
    while (r12d > r13d) {
label_13:
        rax *= 0x66666667;
        ecx = edx;
        r12d--;
        ecx >>= 0x1f;
        rax >>= 0x22;
        eax -= ecx;
        edx = eax;
        rax = (int64_t) edx;
    }
    rcx = rax * 0x66666667;
    esi = edx;
    esi >>= 0x1f;
    rcx >>= 0x22;
    ecx -= esi;
    esi = rcx * 5;
    ecx = edx;
    esi += esi;
    ecx -= esi;
    if (r12d == 1) {
        goto label_94;
    }
    if (ecx == 0) {
        goto label_13;
    }
    rbp = (int64_t) r12d;
    rsi = rbp;
    if (r12d == 0) {
        goto label_95;
    }
label_55:
    rcx = rsp + rsi + 0xb0;
    edi = r12 - 1;
    rsi = rsp + rsi + 0xaf;
    rsi -= rdi;
    while (rcx != rsi) {
        rax = (int64_t) eax;
        rax *= 0x66666667;
        edi = edx;
        rcx--;
        edi >>= 0x1f;
        rax >>= 0x22;
        eax -= edi;
        edi = rax * 5;
        edi += edi;
        edx -= edi;
        edx += 0x30;
        *(rcx) = dl;
        edx = eax;
    }
label_63:
    eax = 0x30;
    if (r8d == 0) {
        r8d = eax;
    }
    rax = r15;
    rax = ~rax;
    if (rax <= rbp) {
        goto label_3;
    }
    r15 += rbp;
    if (r14 == 0) {
        goto label_61;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_96;
    }
    if (rbp == 0) {
        goto label_97;
    }
    *((rsp + 0x50)) = r8d;
    rax = ctype_toupper_loc ();
    rdx = rsp + 0xb0;
    *((rsp + 0x38)) = rbx;
    rcx = rax;
    rax = rdx + rbp;
    *((rsp + 0x40)) = r12d;
    r12 = rdx;
    rbx = rax;
    eax = r13d;
    r13 = r14;
    r14d = eax;
    do {
        esi = *(r12);
        rax = *(rbp);
        r12++;
        fputc (*((rax + rsi*4)), r13);
    } while (rbx != r12);
    eax = r14d;
    rbx = *((rsp + 0x38));
    r12d = *((rsp + 0x40));
    r14 = r13;
    r8d = *((rsp + 0x50));
    r13d = eax;
label_61:
    if (r8d == 0x2d) {
        goto label_14;
    }
    ebp -= r12d;
    if (ebp < 0) {
        goto label_14;
    }
    rax = r15;
    rbp = (int64_t) ebp;
    rax = ~rax;
    if (rbp >= rax) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_49;
    }
label_66:
    if (rbp == 0) {
        goto label_0;
    }
    if (r8d == 0x30) {
        goto label_98;
    }
    r12d = 0;
    if (r8d == 0x2b) {
        goto label_98;
    }
    do {
        r12++;
        fputc (0x20, r14);
    } while (r12 != rbp);
    goto label_37;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 2;
    edx = *(rax);
label_16:
    eax = edx;
    *((rsp + 0x50)) = 0;
    r11d = 0;
    eax >>= 0x1f;
    r10d = eax;
    eax = edx;
    eax = ~eax;
    eax >>= 0x1f;
    goto label_15;
    if (edi == 0x45) {
        goto label_10;
    }
    *((rsp + 0x40)) = 2;
    edx = *((rsp + 0x10));
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 2;
    edx = *((rax + 4));
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 2;
    edx = *((rax + 8));
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    r11 = *((rsp + 0x18));
    eax = *((r11 + 0x18));
    r9d = *((r11 + 0x1c));
    edx = rax + 6;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    eax = ecx;
    eax -= edx;
    eax = rax + r9 + 7;
label_24:
    rdx = (int64_t) eax;
    *((rsp + 0x40)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += eax;
    eax >>= 0x1f;
    edx >>= 2;
    edx -= eax;
    goto label_16;
    rax = *((rsp + 0x18));
    *((rsp + 0x50)) = r8d;
    __asm ("movdqu xmm4, xmmword [rax + 0x20]");
    __asm ("movdqu xmm0, xmmword [rax]");
    __asm ("movdqu xmm2, xmmword [rax + 0x10]");
    rax = *((rax + 0x30));
    *((rsp + 0x70)) = xmm0;
    *((rsp + 0x80)) = xmm2;
    *((rsp + 0xa0)) = rax;
    *((rsp + 0x8c)) = 0xffffffff;
    *((rsp + 0x40)) = xmm4;
    *((rsp + 0x90)) = xmm4;
    rax = mktime_z (*((rsp + 0x20)), rsp + 0x70, rdx, rcx, r8, r9);
    r11d = *((rsp + 0x8c));
    r8d = *((rsp + 0x50));
    rsi = rax;
    if (r11d < 0) {
        goto label_99;
    }
    rax >>= 0x3f;
    rcx = rsi;
    rdi = rsp + 0xc7;
    r10 = rax;
    r9 = rdi;
    r11d = 0x30;
    do {
        rax = rcx;
        rdx:rax = rax * rbp;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        r12 = rdx * 5;
        rax = rdx;
        r12 += r12;
        rcx -= r12;
        rdx = rcx;
        rcx = rax;
        eax = r11d;
        eax -= edx;
        edx += 0x30;
        __asm ("cmovs edx, eax");
        r9--;
        *(r9) = dl;
    } while (rcx != 0);
    *((rsp + 0x50)) = 0;
    *((rsp + 0x40)) = 1;
label_18:
    if (r8d == 0) {
        goto label_100;
    }
    al = (r8d != 0x2d) ? 1 : 0;
label_35:
    __asm ("cmovs r13d, dword [rsp + 0x40]");
    rdi -= r9;
    r12 = rdi;
    if (r10b != 0) {
        goto label_101;
    }
    if (*((rsp + 0x50)) != 0) {
        goto label_102;
    }
    rbp = (int64_t) edi;
    if (r13d <= edi) {
        goto label_103;
    }
    if (al == 0) {
        goto label_103;
    }
label_34:
    rax = (int64_t) r13d;
    r12 = rbp;
    if (rax >= rbp) {
        r12 = rax;
    }
label_33:
    rdx = r15;
    rdx = ~rdx;
    if (rdx <= r12) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_37;
    }
    if (rax <= rbp) {
        goto label_104;
    }
    rax -= rbp;
    if (r8d == 0x30) {
        goto label_105;
    }
    r13d = 0;
    if (r8d == 0x2b) {
        goto label_105;
    }
    *((rsp + 0x50)) = r9;
    *((rsp + 0x40)) = rbx;
    rbx = r13;
    r13 = rax;
    do {
        rbx++;
        fputc (0x20, r14);
    } while (r13 != rbx);
label_44:
    rbx = *((rsp + 0x40));
    r9 = *((rsp + 0x50));
label_104:
    if (*((rsp + 0x38)) == 0) {
        goto label_106;
    }
    *((rsp + 0x38)) = r9;
    if (rbp == 0) {
        goto label_37;
    }
    rax = ctype_toupper_loc ();
    r9 = *((rsp + 0x38));
    *((rsp + 0x38)) = rbx;
    rbx = rax;
    rbp += r9;
    r13 = rbp;
    do {
        edx = *(rbp);
        rax = *(rbx);
        rbp++;
        fputc (*((rax + rdx*4)), r14);
    } while (rbp != r13);
    rbx = *((rsp + 0x38));
label_37:
    r15 += r12;
    goto label_0;
    rax = rbx - 1;
    if (rax == r12) {
        goto label_107;
    }
    rbx = rax;
    goto label_10;
    if (r8d == 0x2d) {
        goto label_108;
    }
    if (r13d < 0) {
        goto label_108;
    }
    r12 = (int64_t) r13d;
    rax = r15;
    rax = ~rax;
    if (r12 != 0) {
    }
    if (rbp >= rax) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_49;
    }
    if (r13d <= 1) {
        goto label_50;
    }
    r12--;
    if (r8d == 0x30) {
        goto label_109;
    }
    r13d = 0;
    if (r8d == 0x2b) {
        goto label_109;
    }
    do {
        r13++;
        fputc (0x20, r14);
    } while (r12 != r13);
label_50:
    fputc (9, r14);
    goto label_49;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 1;
    edx = *((rax + 0x18));
    goto label_16;
    if (edi == 0x4f) {
        goto label_17;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x50)) = 0;
    r10d = 0;
    r11d = 0;
    *((rsp + 0x40)) = 1;
    eax = *((rax + 0x10));
    edx = rax * 5;
    edx = rax + rdx*2;
    edx >>= 5;
    edx++;
label_25:
    rdi = rsp + 0xc7;
    r12d = 0xcccccccd;
    r9 = rdi;
label_19:
    rsi = r9;
    if ((r11b & 1) != 0) {
        *((r9 - 1)) = 0x3a;
        rsi--;
    }
    eax = edx;
    ecx = edx;
    r11d >>= 1;
    r9 = rsi - 1;
    rax *= r12;
    rax >>= 0x23;
    ebp = rax * 5;
    ebp += ebp;
    ecx -= ebp;
    ecx += 0x30;
    *((rsi - 1)) = cl;
    if (edx > 9) {
        goto label_110;
    }
    if (r11d == 0) {
        goto label_18;
    }
label_110:
    edx = eax;
    goto label_19;
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 1;
    eax = *((rax + 0x18));
    edx = rax + 6;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    edx -= ecx;
    edx++;
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    eax = *((rsp + 0x38));
    *((rsp + 0x58)) = 0;
    if (bpl != 0) {
        eax = ebp;
    }
    *((rsp + 0x38)) = al;
    goto label_20;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x18));
    ecx = *((rax + 0x14));
    eax = rcx + 0x76c;
    r10b = (ecx < 0xfffff894) ? 1 : 0;
    eax -= eax;
    eax &= 0xffffff9d;
    eax += ecx;
    rdx = (int64_t) eax;
    eax >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x25;
    edx -= eax;
    edx += 0x13;
    al = (ecx >= 0xfffff894) ? 1 : 0;
    if (r8d != 0) {
        goto label_111;
    }
    r8d = *((rsp + 0x2c));
    if (*((rsp + 0x2c)) == 0x2b) {
        goto label_112;
    }
label_56:
    *((rsp + 0x50)) = 0;
    *((rsp + 0x40)) = 2;
    goto label_21;
    if (edi != 0) {
        goto label_10;
    }
    rax = "%m/%d/%y";
    *((rsp + 0x40)) = rax;
label_23:
    eax = *((rsp + 0x508));
    r12d = *((rsp + 0x38));
    r9d = ebp;
    edi = 0;
    ecx = r12d;
    rdx = *((rsp + 0x28));
    rsi = *((rsp + 0x50));
    *((rsp + 0x48)) = r8d;
    rax = _strftime_internal_isra_0 ();
    goto label_22;
    r8d = *((rsp + 0x38));
    if (r8d == 0x2d) {
        goto label_113;
    }
    if (r13d < 0) {
        goto label_113;
    }
    r13 = (int64_t) r13d;
    rsi = r13;
    if (rax >= r13) {
        rsi = rax;
    }
    *((rsp + 0x38)) = rsi;
label_40:
    rdx = r15;
    rdx = ~rdx;
    if (rdx <= *((rsp + 0x38))) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_114;
    }
    if (r13 <= rax) {
        goto label_115;
    }
    r13 -= rax;
    rax = r13;
    if (r8d == 0x30) {
        goto label_116;
    }
    r13d = 0;
    if (r8d == 0x2b) {
        goto label_116;
    }
    *((rsp + 0x58)) = r8d;
    *((rsp + 0x50)) = rbx;
    rbx = rax;
    rax = r13;
    r13d = ebp;
    do {
        rbp++;
        fputc (0x20, r14);
    } while (rbx != rbp);
label_51:
    rbx = *((rsp + 0x50));
    r8d = *((rsp + 0x58));
label_115:
    eax = *((rsp + 0x508));
    ecx = r12d;
    r9d = ebp;
    rdi = r14;
    rdx = *((rsp + 0x28));
    rsi = *((rsp + 0x50));
    eax = _strftime_internal_isra_0 ();
    goto label_22;
label_114:
    r15 += *((rsp + 0x38));
    goto label_0;
    if (edi != 0) {
        goto label_10;
    }
    if (r13d < 0) {
        if (r8d == 0) {
            goto label_117;
        }
    }
    ebp = r13 - 6;
    eax = 0;
    __asm ("cmovs ebp, eax");
    rax = "%Y-%m-%d";
    *((rsp + 0x40)) = rax;
    goto label_23;
    if (edi == 0x45) {
        goto label_10;
    }
    edx = *((rsp + 0x10));
label_28:
    eax = 0x5f;
    *((rsp + 0x40)) = 2;
    if (r8d == 0) {
        r8d = eax;
    }
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x50)) = 0;
    *((rsp + 0x40)) = 2;
    eax = *((rax + 0x10));
    edx = rax + 1;
    r10b = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r11d = 0;
    goto label_15;
    if (r8d == 0x2d) {
        goto label_118;
    }
    if (r13d < 0) {
        goto label_118;
    }
    r12 = (int64_t) r13d;
    rax = r15;
    rax = ~rax;
    if (r12 != 0) {
    }
    if (rbp >= rax) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_49;
    }
    if (r13d <= 1) {
        goto label_48;
    }
    r12--;
    if (r8d == 0x30) {
        goto label_119;
    }
    r13d = 0;
    if (r8d == 0x2b) {
        goto label_119;
    }
    do {
        r13++;
        fputc (0x20, r14);
    } while (r12 != r13);
label_48:
    fputc (0xa, r14);
label_49:
    r15 += rbp;
    goto label_0;
    if (edi == 0x45) {
        goto label_10;
    }
    rcx = *((rsp + 0x18));
    eax = *((rcx + 0x1c));
    eax -= *((rcx + 0x18));
    eax += 7;
    goto label_24;
    if (edi == 0x45) {
        goto label_17;
    }
    if (edi == 0x4f) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    edx = *((rax + 0x14));
    r10b = (edx < 0xfffff894) ? 1 : 0;
    edx += 0x76c;
    if (r8d != 0) {
        goto label_60;
    }
    r8d = *((rsp + 0x2c));
    if (r8d == 0x2b) {
        goto label_120;
    }
    *((rsp + 0x50)) = 0;
    r11d = 0;
    *((rsp + 0x40)) = 4;
label_93:
    eax = edx;
    eax = -eax;
    if (r10b != 0) {
        edx = eax;
    }
    goto label_25;
    esi = *((rsp + 0x38));
    eax = 0;
    rdi = *((rsp + 0x30));
    *((rsp + 0x50)) = r8d;
    if (bpl != 0) {
        esi = eax;
    }
    *((rsp + 0x38)) = sil;
    rax = strlen (rdi);
    r8d = *((rsp + 0x50));
    *((rsp + 0x40)) = rax;
    if (r13d < 0) {
        goto label_121;
    }
    if (r8d == 0x2d) {
        goto label_121;
    }
    r12 = (int64_t) r13d;
    if (rax < r12) {
        rax = r12;
    }
    *((rsp + 0x50)) = rax;
label_47:
    rax = r15;
    rax = ~rax;
    if (rax <= *((rsp + 0x50))) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_26;
    }
    if (*((rsp + 0x40)) >= r12) {
        goto label_62;
    }
    r12 -= *((rsp + 0x40));
    if (r8d == 0x30) {
        goto label_122;
    }
    if (r8d == 0x2b) {
        goto label_122;
    }
    r13d = 0;
    if (r12 == 0) {
        goto label_62;
    }
    do {
        r13++;
        fputc (0x20, r14);
    } while (r12 != r13);
label_62:
    if (bpl != 0) {
        goto label_123;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_124;
    }
    rbp = *((rsp + 0x40));
    if (rbp == 0) {
        goto label_26;
    }
    rax = ctype_toupper_loc ();
    r13 = rbp;
    rbp = *((rsp + 0x30));
    r12 = rax;
    r13 += rbp;
    do {
        edx = *(rbp);
        rax = *(r12);
        rbp++;
        fputc (*((rax + rdx*4)), r14);
    } while (rbp != r13);
    goto label_26;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x18));
    rdx = *((rax + 0x14));
    rcx = rdx;
    rdx *= 0x51eb851f;
    eax = ecx;
    eax >>= 0x1f;
    rdx >>= 0x25;
    edx -= eax;
    eax = edx * 0x64;
    edx = ecx;
    edx -= eax;
    if (edx >= 0) {
        goto label_27;
    }
    eax = edx;
    edx += 0x64;
    eax = -eax;
    if (ecx <= 0xfffff893) {
        edx = eax;
    }
    goto label_27;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    ecx = 1;
    if (al == 0x3a) {
        goto label_31;
    }
label_30:
    if (al != 0x7a) {
        goto label_10;
    }
    rbx = rdx;
label_29:
    rax = *((rsp + 0x18));
    r10d = *((rax + 0x20));
    if (r10d < 0) {
        goto label_0;
    }
    rdx = *((rax + 0x28));
    r10d = 1;
    if (edx >= 0) {
        r10d = 0;
        if (edx != 0) {
            goto label_125;
        }
        rax = *((rsp + 0x30));
        r10b = (*(rax) == 0x2d) ? 1 : 0;
    }
label_125:
    rax = (int64_t) edx;
    r9d = edx;
    r11 = rax * 0xffffffff91a2b3c5;
    r9d >>= 0x1f;
    rax *= 0xffffffff88888889;
    r11 >>= 0x20;
    rax >>= 0x20;
    r11d += edx;
    eax += edx;
    r11d >>= 0xb;
    eax >>= 5;
    r11d -= r9d;
    eax -= r9d;
    r11 = (int64_t) eax;
    r9d = eax;
    r11 *= 0xffffffff88888889;
    r9d >>= 0x1f;
    r11 >>= 0x20;
    r11d += eax;
    r11d >>= 5;
    r11d -= r9d;
    r9d = r11d * 0x3c;
    r11d = eax;
    eax *= 0x3c;
    r11d -= r9d;
    edx -= eax;
    if (rcx == 2) {
        goto label_126;
    }
    if (rcx > 2) {
        goto label_127;
    }
    if (rcx == 0) {
        goto label_128;
    }
label_46:
    edx = ebp * 0x64;
    eax = r10d;
    *((rsp + 0x50)) = 1;
    *((rsp + 0x40)) = 6;
    eax ^= 1;
    edx += r11d;
    r11d = 4;
    goto label_15;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x40)) = 2;
    edx = *((rax + 0xc));
    goto label_16;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    *((rsp + 0x50)) = 0;
    *((rsp + 0x40)) = 3;
    eax = *((rax + 0x1c));
    edx = rax + 1;
    r10b = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r11d = 0;
    goto label_15;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    edx = *((rax + 8));
    goto label_28;
    if (edi == 0x45) {
        goto label_10;
    }
    rax = *((rsp + 0x18));
    edx = *((rax + 0xc));
    goto label_28;
    ecx = 0;
    goto label_29;
    rax = "%H:%M";
    *((rsp + 0x40)) = rax;
    goto label_23;
    *((rsp + 0x58)) = 0;
    do {
        eax = *((rsp + 0x58));
        ecx = *((rsp + 0x38));
        esi = 0x70;
        if (bpl != 0) {
            eax = ebp;
        }
        *((rsp + 0x58)) = al;
        eax = 0;
        if (bpl != 0) {
            ecx = eax;
        }
        *((rsp + 0x38)) = cl;
        goto label_20;
        *((rsp + 0x58)) = 1;
    } while (1);
    rax = "%H:%M:%S";
    *((rsp + 0x40)) = rax;
    goto label_23;
label_31:
    rcx++;
    eax = *((rbx + rcx));
    rdx = rbx + rcx;
    if (al != 0x3a) {
        goto label_30;
    }
    goto label_31;
label_84:
    rax = *((rsp + 0x40));
    r13d = 0;
    *((rsp + 0x50)) = rax;
    goto label_32;
label_101:
    *((rsp + 0x40)) = 0x2d;
label_41:
    ecx = r13 - 1;
    ecx -= r12d;
    if (ecx <= 0) {
        goto label_129;
    }
    if (al == 0) {
        goto label_129;
    }
label_36:
    if (r8d == 0x5f) {
        goto label_130;
    }
label_45:
    if (r15 > 0xfffffffffffffffd) {
        goto label_3;
    }
    if (r14 != 0) {
        edi = *((rsp + 0x40));
        *((rsp + 0x58)) = r8d;
        *((rsp + 0x50)) = r9;
        eax = fputc (rdi, r14);
        r8d = *((rsp + 0x58));
        r9 = *((rsp + 0x50));
    }
    rbp = (int64_t) r12d;
    r15++;
    eax = 0;
    r12 = rbp;
    if (r8d == 0x2d) {
        goto label_33;
    }
    r13d--;
    if (r13d < 0) {
        goto label_33;
    }
    goto label_34;
label_100:
    eax = 1;
    r8d = 0x30;
    goto label_35;
label_129:
    ebp = 0;
    goto label_36;
label_103:
    r12 = rbp;
    eax = 0;
    if (r8d != 0x2d) {
        goto label_34;
    }
    goto label_33;
label_106:
    eax = fwrite (r9, rbp, 1, r14);
    goto label_37;
label_75:
    *((rsp + 0x38)) = 1;
    goto label_8;
label_76:
    goto label_8;
label_79:
    if (r13 == 0) {
        goto label_38;
    }
    edx = 0;
    *((rsp + 0x58)) = rbx;
    rbx = rdx;
    do {
        rbx++;
        fputc (0x30, r14);
    } while (r13 != rbx);
    goto label_39;
label_87:
    fwrite (rsp + 0xb1, *((rsp + 0x40)), 1, r14);
    goto label_26;
label_86:
    if (*((rsp + 0x40)) == 0) {
        goto label_26;
    }
    rax = ctype_tolower_loc ();
    rbp += r12;
    r13 = rsp + 0xb1;
    r12 = rax;
    do {
        edx = *(r13);
        rax = *(r12);
        r13++;
        rax = fputc (*((rax + rdx*4)), r14);
    } while (r13 != rbp);
    goto label_26;
label_113:
    *((rsp + 0x38)) = rax;
    r13d = 0;
    goto label_40;
label_102:
    *((rsp + 0x40)) = 0x2b;
    goto label_41;
label_85:
    if (r13 == 0) {
        goto label_42;
    }
    edx = 0;
    *((rsp + 0x60)) = rbx;
    rbx = rdx;
    do {
        rbx++;
        rax = fputc (0x30, r14);
    } while (r13 != rbx);
    goto label_43;
label_105:
    *((rsp + 0x50)) = r9;
    r13d = 0;
    *((rsp + 0x40)) = rbx;
    rbx = r13;
    r13 = rax;
    do {
        rbx++;
        fputc (0x30, r14);
    } while (r13 != rbx);
    goto label_44;
label_130:
    rax = (int64_t) ebp;
    *((rsp + 0x50)) = rax;
    if (r14 != 0) {
        goto label_131;
    }
label_52:
    r15 += *((rsp + 0x50));
    r13d -= ebp;
    goto label_45;
label_127:
    if (rcx != 3) {
        goto label_10;
    }
    if (edx != 0) {
        goto label_126;
    }
    if (r11d != 0) {
        goto label_46;
    }
    eax = r10d;
    *((rsp + 0x50)) = 1;
    edx = ebp;
    *((rsp + 0x40)) = 3;
    eax ^= 1;
    goto label_15;
label_121:
    rax = *((rsp + 0x40));
    r12d = 0;
    *((rsp + 0x50)) = rax;
    goto label_47;
label_118:
    if (r15 > 0xfffffffffffffffd) {
        goto label_3;
    }
    if (r14 != 0) {
        goto label_48;
    }
    goto label_49;
label_108:
    if (r15 > 0xfffffffffffffffd) {
        goto label_3;
    }
    if (r14 != 0) {
        goto label_50;
    }
    goto label_49;
label_116:
    r13d = 0;
    *((rsp + 0x58)) = r8d;
    *((rsp + 0x50)) = rbx;
    rbx = rax;
    rax = r13;
    r13d = ebp;
    do {
        rbp++;
        fputc (0x30, r14);
    } while (rbx != rbp);
    goto label_51;
label_131:
    if (ebp == 0) {
        goto label_52;
    }
    *((rsp + 0x60)) = r9;
    r10d = 0;
    *((rsp + 0x68)) = r8d;
    *((rsp + 0x58)) = rbx;
    rbx = (int64_t) ebp;
    *((rsp + 0x6c)) = ebp;
    do {
        rbp++;
        fputc (0x20, r14);
    } while (rbp != rbx);
    rbx = *((rsp + 0x58));
    r9 = *((rsp + 0x60));
    r8d = *((rsp + 0x68));
    ebp = *((rsp + 0x6c));
    goto label_52;
label_107:
    if (r8d == 0x2d) {
        goto label_132;
    }
    if (r13d < 0) {
        goto label_132;
    }
    r12 = (int64_t) r13d;
    rax = r15;
    rax = ~rax;
    if (r12 != 0) {
    }
    if (rbp >= rax) {
        goto label_3;
    }
    if (r14 == 0) {
        goto label_49;
    }
    r13d--;
    if (r13d <= 0) {
        goto label_59;
    }
    r12--;
    if (r8d == 0x30) {
        goto label_133;
    }
    r13d = 0;
    if (r8d == 0x2b) {
        goto label_133;
    }
    do {
        r13++;
        fputc (0x20, r14);
    } while (r12 != r13);
label_59:
    edi = *(rbx);
    eax = fputc (rdi, r14);
    goto label_49;
label_92:
    ecx = 0xfffff894;
    ecx -= eax;
    if (r9d >= ecx) {
        goto label_134;
    }
    edx = -edx;
    if (r8d == 0) {
        goto label_53;
    }
label_64:
    if (r8d != 0x2b) {
        goto label_54;
    }
label_65:
    *((rsp + 0x40)) = 2;
    r10d = 0;
    do {
        eax = r10d;
        r8d = 0x2b;
        rsp + 0x50 = (r13d > *((rsp + 0x40))) ? 1 : 0;
        eax ^= 1;
        goto label_21;
label_94:
        esi = 1;
        goto label_55;
label_60:
        *((rsp + 0x40)) = 4;
        eax = 0x270f;
        if (r8d != 0x2b) {
            goto label_135;
        }
label_57:
    } while (eax >= edx);
    eax = r10d;
    *((rsp + 0x50)) = 1;
    r8d = 0x2b;
    eax ^= 1;
    goto label_21;
label_111:
    if (r8d != 0x2b) {
        goto label_56;
    }
label_112:
    *((rsp + 0x40)) = 2;
    eax = 0x63;
    goto label_57;
label_88:
    eax--;
    edx = 0x16d;
    if ((al & 3) == 0) {
        ecx = eax * 0xc28f5c29;
        edx = 0x16e;
        ecx += 0x51eb850;
        ecx = rotate_right32 (ecx, 2);
        if (ecx > 0x28f5c28) {
            goto label_136;
        }
        edx:eax = (int64_t) eax;
        ecx = 0x190;
        eax = edx:eax / ecx;
        edx = edx:eax % ecx;
        edx = -edx;
        edx -= edx;
        edx += 0x16e;
    }
label_136:
    edx += r10d;
    ecx = edx;
    ecx -= r11d;
    ecx += 0x17e;
    rax = (int64_t) ecx;
    r10d = ecx;
    edx -= ecx;
    rax *= 0xffffffff92492493;
    r10d >>= 0x1f;
    rax >>= 0x20;
    eax += ecx;
    eax >>= 2;
    eax -= r10d;
    r10d = rax*8;
    r10d -= eax;
    eax = 0xffffffff;
    ecx = rdx + r10 + 3;
    goto label_58;
label_132:
    if (r15 > 0xfffffffffffffffd) {
        goto label_3;
    }
    if (r14 != 0) {
        goto label_59;
    }
    goto label_49;
label_91:
    rdx = (int64_t) ecx;
    *((rsp + 0x40)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += ecx;
    ecx >>= 0x1f;
    edx >>= 2;
    edx -= ecx;
    edx++;
    goto label_16;
label_90:
    ecx = 0xfffff894;
    edx = r9 + rax + 0x76c;
    ecx -= eax;
    r10b = (r9d < ecx) ? 1 : 0;
    if (r8d != 0) {
        goto label_60;
    }
    r8d = *((rsp + 0x2c));
    if (r8d == 0x2b) {
        goto label_120;
    }
    *((rsp + 0x50)) = 0;
    *((rsp + 0x40)) = 4;
    al = (r9d >= ecx) ? 1 : 0;
    goto label_21;
label_126:
    r11d *= 0x64;
    *((rsp + 0x50)) = 1;
    eax = ebp * sym.imp.fgets_unlocked;
    *((rsp + 0x40)) = 9;
    eax += r11d;
    r11d = 0x14;
    edx += eax;
    eax = r10d;
    eax ^= 1;
    goto label_15;
label_124:
    fwrite (*((rsp + 0x30)), *((rsp + 0x40)), 1, r14);
    goto label_26;
label_123:
    rbp = *((rsp + 0x40));
    if (rbp == 0) {
        goto label_26;
    }
    rax = ctype_tolower_loc ();
    r13 = rbp;
    rbp = *((rsp + 0x30));
    r12 = rax;
    r13 += rbp;
    do {
        edx = *(rbp);
        rax = *(r12);
        rbp++;
        fputc (*((rax + rdx*4)), r14);
    } while (rbp != r13);
    goto label_26;
label_96:
    *((rsp + 0x38)) = r8d;
    fwrite (rsp + 0xb0, rbp, 1, r14);
    r8d = *((rsp + 0x38));
    goto label_61;
label_109:
    r13d = 0;
    do {
        r13++;
        fputc (0x30, r14);
    } while (r12 != r13);
    goto label_50;
label_119:
    r13d = 0;
    do {
        r13++;
        fputc (0x30, r14);
    } while (r12 != r13);
    goto label_48;
label_122:
    if (r12 == 0) {
        goto label_62;
    }
    r13d = 0;
    do {
        r13++;
        fputc (0x30, r14);
    } while (r12 != r13);
    goto label_62;
label_128:
    edx = ebp * 0x64;
    eax = r10d;
    *((rsp + 0x50)) = 1;
    *((rsp + 0x40)) = 5;
    eax ^= 1;
    edx += r11d;
    r11d = 0;
    goto label_15;
label_120:
    *((rsp + 0x40)) = 4;
    eax = 0x270f;
    goto label_57;
label_98:
    r12d = 0;
    do {
        r12++;
        fputc (0x30, r14);
    } while (r12 != rbp);
    goto label_37;
label_117:
    eax = *((rsp + 0x508));
    r12d = *((rsp + 0x38));
    edi = 0;
    r13 = "%Y-%m-%d";
    r8d = 0x2b;
    rsi = r13;
    r9d = 4;
    ecx = r12d;
    rdx = *((rsp + 0x28));
    rax = _strftime_internal_isra_0 ();
    goto label_22;
    r8d = 0x2b;
    *((rsp + 0x40)) = r13;
    r13d = 0;
    *((rsp + 0x38)) = rax;
    goto label_40;
label_133:
    r13d = 0;
    do {
        r13++;
        fputc (0x30, r14);
    } while (r12 != r13);
    goto label_59;
label_73:
    stack_chk_fail ();
label_95:
    ebp = 0;
    goto label_63;
label_134:
    edx += 0x64;
    if (r8d != 0) {
        goto label_64;
    }
    eax = *((rsp + 0x2c));
    if (eax == 0x2b) {
        goto label_65;
    }
    r8d = eax;
    goto label_54;
label_135:
    eax = r10d;
    *((rsp + 0x50)) = 0;
    eax ^= 1;
    goto label_21;
label_97:
    if (r8d == 0x2d) {
        goto label_14;
    }
    rax = r15;
    ebp -= r12d;
    rax = ~rax;
    rbp = (int64_t) ebp;
    if (rax > rbp) {
        goto label_66;
    }
    goto label_3;
label_99:
    rax = *((rsp + 8));
    *(rax) = 0x4b;
    goto label_67;
}

/* /tmp/tmpdb8sk3u8 @ 0x79c0 */
 
uint64_t revert_tz_part_0 (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    rax = errno_location ();
    r12d = *(rax);
    if (*((rbx + 8)) != 0) {
        goto label_2;
    }
    rdi = 0x0000ab08;
    eax = unsetenv ();
    if (eax == 0) {
        goto label_3;
    }
label_0:
    r12d = *(rbp);
    r13d = 0;
    do {
label_1:
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
    *(rbp) = r12d;
    eax = r13d;
    return rax;
label_2:
    eax = setenv (0x0000ab08, rbx + 9, 1);
    if (eax != 0) {
        goto label_0;
    }
label_3:
    tzset ();
    r13d = 1;
    goto label_1;
}

/* /tmp/tmpdb8sk3u8 @ 0x7af0 */
 
uint64_t dbg_save_abbr (uint32_t arg_8h, int64_t arg_9h, int64_t arg_80h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool save_abbr(timezone_t tz,tm * tm); */
    r12 = *((rsi + 0x30));
    if (r12 == 0) {
        goto label_4;
    }
    r13 = rsi;
    if (rsi <= r12) {
        rdx = rsi + 0x38;
        eax = 1;
        if (r12 < rdx) {
            goto label_3;
        }
    }
    rbx = rbp + 9;
    if (*(r12) == 0) {
        goto label_5;
    }
    do {
label_0:
        eax = strcmp (rbx, r12);
        if (eax == 0) {
            goto label_2;
        }
label_1:
        if (*(rbx) == 0) {
            rax = rbp + 9;
            if (rbx != rax) {
                goto label_6;
            }
            if (*((rbp + 8)) == 0) {
                goto label_6;
            }
        }
        strlen (rbx);
        rbx = rbx + rax + 1;
    } while (*(rbx) != 0);
    rax = *(rbp);
    if (rax == 0) {
        goto label_0;
    }
    rbx = rax + 9;
    eax = strcmp (rbx, r12);
    if (eax != 0) {
        goto label_1;
    }
    do {
label_2:
        *((r13 + 0x30)) = rbx;
        eax = 1;
label_3:
        return rax;
label_5:
        rbx = 0x0000ab64;
    } while (1);
label_4:
    eax = 1;
    return rax;
label_6:
    rax = strlen (r12);
    r14 = rax;
    rdx = rax + 1;
    rax = rbp + 0x80;
    rax -= rbx;
    if (rax > rdx) {
        memcpy (rbx, r12, rdx);
        *((rbx + r14 + 1)) = 0;
        goto label_2;
    }
    rax = tzalloc (r12);
    *(rbp) = rax;
    if (rax != 0) {
        *((rax + 8)) = 0;
        rbx = rax + 9;
        goto label_2;
    }
    eax = 0;
    goto label_3;
}

/* /tmp/tmpdb8sk3u8 @ 0x7c20 */
 
uint64_t dbg_set_tz (int64_t arg1) {
    rdi = arg1;
    /* timezone_t set_tz(timezone_t tz); */
    r13 = 0x0000ab08;
    rbx = rdi;
    rax = getenv (r13);
    if (rax == 0) {
        goto label_2;
    }
    while (eax != 0) {
label_0:
        rax = tzalloc (rbp);
        r12 = rax;
        if (rax != 0) {
            if (*((rbx + 8)) != 0) {
                goto label_3;
            }
            rdi = r13;
            eax = unsetenv ();
            if (eax != 0) {
                goto label_4;
            }
label_1:
            tzset ();
        }
        rax = r12;
        return rax;
        r12d = 1;
        eax = strcmp (rbx + 9, rax);
    }
    rax = r12;
    return rax;
label_2:
    r12d = 1;
    if (*((rbx + 8)) != 0) {
        goto label_0;
    }
    rax = r12;
    return rax;
label_3:
    eax = setenv (r13, rbx + 9, 1);
    if (eax == 0) {
        goto label_1;
    }
label_4:
    rax = errno_location ();
    ebx = *(rax);
    if (r12 == 1) {
        goto label_5;
    }
    do {
        rdi = r12;
        r12 = *(r12);
        free (rdi);
    } while (r12 != 0);
label_5:
    *(rbp) = ebx;
    r12d = 0;
    rax = r12;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x8c30 */
 
int64_t dbg_ydhms_diff (int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg3, int64_t arg4, int64_t arg6, int32_t sec1, long_int yday1) {
    int32_t yday0;
    int32_t hour0;
    int32_t min0;
    int32_t sec0;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    r8 = sec1;
    rsi = yday1;
    /* long_int ydhms_diff(long_int year1,long_int yday1,int hour1,int min1,int sec1,int year0,int yday0,int hour0,int min0,int sec0); */
    r11 = (int64_t) ecx;
    rcx = rdi;
    rax = rdi;
    ecx &= 3;
    r10 = (int64_t) edx;
    rax >>= 2;
    rbx = (int64_t) r9d;
    r8 = (int64_t) r8d;
    rdx = rbx;
    eax -= 0xfffffe25;
    r9 = rbx;
    edx &= 3;
    r9 >>= 2;
    r9d -= 0xfffffe25;
    ebp >>= 0x1f;
    rdi -= rbx;
    ecx = rbp + rax;
    eax -= r9d;
    rdx = (int64_t) ecx;
    ecx >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x23;
    edx -= ecx;
    edx -= ebp;
    ebp >>= 0x1f;
    r12d = rbp + r9;
    r9d = edx;
    rdx = (int64_t) edx;
    rcx = (int64_t) r12d;
    r12d >>= 0x1f;
    rcx *= 0x51eb851f;
    rdx >>= 2;
    rcx >>= 0x23;
    ecx -= r12d;
    ecx -= ebp;
    r9d -= ecx;
    rcx = (int64_t) ecx;
    rcx >>= 2;
    eax -= r9d;
    edx -= ecx;
    rcx = *((rsp + 0x20));
    eax += edx;
    rdx = rdi * 9;
    rdx = rdi + rdx*8;
    rax = (int64_t) eax;
    rdx *= 5;
    rdx += rsi;
    rdx -= rcx;
    rax += rdx;
    rdx = *((rsp + 0x28));
    rax *= 3;
    rax = r10 + rax*8;
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r11 + rdx*4;
    rdx = *((rsp + 0x30));
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r8 + rdx*4;
    rdx = *((rsp + 0x38));
    r12 = rbx;
    rax -= rdx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x8d30 */
 
int64_t dbg_ranged_convert (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    time_t x;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t canary;
    int64_t var_38h;
    int64_t var_40h;
    signed int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* tm * ranged_convert(tm * (*)() convert,long_int * t,tm * tp); */
    r15 = rsi;
    r14 = rdx;
    rbx = rdi;
    r12 = *(rsi);
    rbp = rsp + 0x50;
    rsi = rdx;
    rdi = rbp;
    rax = *(fs:0x28);
    eax = 0;
    rax = void (*rbx)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    *((rsp + 0x38)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    *(r15) = r12;
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = *((rsp + 0x38));
        return rax;
label_2:
        rax = errno_location ();
        *((rsp + 0x40)) = rax;
    } while (*(rax) != 0x4b);
    rcx = r12;
    rax = r12;
    ecx &= 1;
    rax >>= 1;
    r13 = rcx + rax;
    if (r12 == r13) {
        goto label_0;
    }
    if (r13 == 0) {
        goto label_0;
    }
    r15d = 0;
    rax = r14;
    *((rsp + 4)) = 0xffffffff;
    r14 = r12;
    r12 = r13;
    r13 = r15;
    r15 = rax;
    while (rax != 0) {
        eax = *(r15);
        r13 = r12;
        *((rsp + 4)) = eax;
        eax = *((r15 + 4));
        *((rsp + 0x2c)) = eax;
        eax = *((r15 + 8));
        *((rsp + 0x28)) = eax;
        eax = *((r15 + 0xc));
        *((rsp + 0x24)) = eax;
        eax = *((r15 + 0x10));
        *((rsp + 0x20)) = eax;
        eax = *((r15 + 0x14));
        *((rsp + 0xc)) = eax;
        eax = *((r15 + 0x18));
        *((rsp + 0x18)) = eax;
        eax = *((r15 + 0x1c));
        *((rsp + 0x1c)) = eax;
        eax = *((r15 + 0x20));
        *((rsp + 8)) = eax;
        rax = *((r15 + 0x28));
        *((rsp + 0x10)) = rax;
        rax = *((r15 + 0x30));
        *((rsp + 0x30)) = rax;
label_1:
        rdx = r13;
        rax = r14;
        rax >>= 1;
        rdx >>= 1;
        rdx += rax;
        rax = r13;
        rax |= r14;
        eax &= 1;
        r12 = rdx + rax;
        if (r12 == r13) {
            goto label_4;
        }
        if (r12 == r14) {
            goto label_4;
        }
        rsi = r15;
        rdi = rbp;
        rax = void (*rbx)(uint64_t) (r12);
    }
    rax = *((rsp + 0x40));
    if (*(rax) != 0x4b) {
        goto label_0;
    }
    r14 = r12;
    goto label_1;
label_4:
    eax = *((rsp + 4));
    r14 = r15;
    if (eax < 0) {
        goto label_0;
    }
    rcx = *((rsp + 0x48));
    *((rsp + 0x38)) = r14;
    *(rcx) = r13;
    *(r14) = eax;
    eax = *((rsp + 0x2c));
    *((r14 + 4)) = eax;
    eax = *((rsp + 0x28));
    *((r14 + 8)) = eax;
    eax = *((rsp + 0x24));
    *((r14 + 0xc)) = eax;
    eax = *((rsp + 0x20));
    *((r14 + 0x10)) = eax;
    eax = *((rsp + 0xc));
    *((r14 + 0x14)) = eax;
    eax = *((rsp + 0x18));
    *((r14 + 0x18)) = eax;
    eax = *((rsp + 0x1c));
    *((r14 + 0x1c)) = eax;
    eax = *((rsp + 8));
    *((r14 + 0x20)) = eax;
    rax = *((rsp + 0x10));
    *((r14 + 0x28)) = rax;
    rax = *((rsp + 0x30));
    *((r14 + 0x30)) = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x9790 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6f40 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7270 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x84b0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6d80 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x86b0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2720 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x8bf0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000a4bf);
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2760 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2520 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpdb8sk3u8 @ 0x7d20 */
 
void tzfree (uint32_t arg1) {
    rdi = arg1;
    if (rdi == 1) {
        goto label_0;
    }
    rbx = rdi;
    if (rdi == 0) {
        goto label_1;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
label_1:
    return;
label_0:
}

/* /tmp/tmpdb8sk3u8 @ 0x6ca0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x95c0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6f90 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2890)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6d00 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x5440 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2620 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2820 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpdb8sk3u8 @ 0x74d0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x28a9)() ();
    }
    if (rdx == 0) {
        void (*0x28a9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7570 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28ae)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x28ae)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x70b0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x289a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x51c0 */
 
int64_t dbg_parse_long_options (int64_t arg_100h, uint32_t arg1, int64_t arg10, int64_t arg11, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list authors;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* void parse_long_options(int argc,char ** argv,char const * command_name,char const * package,char const * version,void (*)() usage_func,va_args ...); */
    r13 = rcx;
    r12 = r8;
    rbx = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    r14d = opterr;
    *(obj.opterr) = 0;
    while (eax == 0xffffffff) {
label_0:
        *(obj.opterr) = r14d;
        *(obj.optind) = 0;
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        return rax;
        r8d = 0;
        rcx = obj_long_options;
        rdx = 0x0000a6d0;
        eax = getopt_long ();
    }
    if (eax == 0x68) {
        goto label_2;
    }
    if (eax != 0x76) {
        goto label_0;
    }
    rdi = stdout;
    r8 = rsp;
    rcx = r12;
    rdx = r13;
    rax = rsp + 0x100;
    rsi = rbp;
    *(rsp) = 0x30;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    version_etc_va ();
    exit (0);
label_2:
    edi = 0;
    void (*rbx)() ();
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x97a4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpdb8sk3u8 @ 0x8730 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8830 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x86d0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x3550 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2730)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8670 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6f70 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8bb0 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x25c0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpdb8sk3u8 @ 0x8690 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x83f0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x7f80)() ();
}

/* /tmp/tmpdb8sk3u8 @ 0x3410 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a4bf);
    } while (1);
}

/* /tmp/tmpdb8sk3u8 @ 0x96d0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpdb8sk3u8 @ 0x8630 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x9650 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7620 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28b3)() ();
    }
    if (rax == 0) {
        void (*0x28b3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x73a0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8a80 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2680 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x6c40 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x7760 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6be0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x8af0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x5120 */
 
void dbg_fprintftime (int64_t arg4, int64_t arg5) {
    rcx = arg4;
    r8 = arg5;
    /* size_t fprintftime(FILE * s,char const * format,tm const * tp,timezone_t tz,int ns); */
    r9d = 0xffffffff;
    r8d = 0;
    ecx = 0;
    _strftime_internal_isra_0 ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6e80 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x77d0 */
 
uint64_t dbg_extract_trimmed_name (int64_t arg1) {
    rdi = arg1;
    /* char * extract_trimmed_name(STRUCT_UTMP const * ut); */
    rbx = rdi;
    rax = xmalloc (0x21);
    r12 = rax;
    strncpy (rax, rbx + 0x2c, 0x20);
    *((r12 + 0x20)) = 0;
    rax = strlen (r12);
    rax += r12;
    while (r12 != rax) {
        if (*((rax - 1)) != 0x20) {
label_0:
            rax = r12;
            return rax;
        }
        rax--;
        *(rax) = 0;
    }
    goto label_0;
}

/* /tmp/tmpdb8sk3u8 @ 0x3400 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpdb8sk3u8 @ 0x8700 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x88c0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x34c0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2570)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpdb8sk3u8 @ 0x87e0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7770 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x76c0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28b8)() ();
    }
    if (rax == 0) {
        void (*0x28b8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8b30 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7a50 */
 
int64_t dbg_tzalloc (int64_t arg1) {
    rdi = arg1;
    /* timezone_t tzalloc(char const * name); */
    if (rdi == 0) {
        goto label_0;
    }
    strlen (rdi);
    rbx = rax + 1;
    eax = 0x76;
    if (rbx >= rax) {
        rax = rbx;
    }
    rdi += 0x11;
    rdi &= 0xfffffffffffffff8;
    rax = malloc (rax);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rax) = 0;
    eax = 1;
    *((r12 + 8)) = ax;
    memcpy (r12 + 9, rbp, rbx);
    *((r12 + rbx + 9)) = 0;
    do {
label_1:
        rax = r12;
        return rax;
label_0:
        rax = malloc (0x80);
        r12 = rax;
    } while (rax == 0);
    edx = 0;
    *(r12) = 0;
    rax = r12;
    *((r12 + 8)) = dx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x26c0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpdb8sk3u8 @ 0x6e70 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x6d80)() ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7d60 */
 
uint64_t dbg_localtime_rz (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
label_1:
    __asm ("bnd jmp qword [reloc.gmtime_r]");
    /* tm * localtime_rz(timezone_t tz,time_t const * t,tm * tm); */
    r14 = rsi;
    if (rdi == 0) {
        goto label_2;
    }
    r12 = rdi;
    rax = set_tz (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rsi = rbp;
    rdi = r14;
    rax = fcn_00002440 ();
    if (rax == 0) {
        goto label_3;
    }
    al = save_abbr (r12, rbp, rdx, rcx, r8);
    if (al == 0) {
        goto label_3;
    }
    while (al != 0) {
        rax = rbp;
        return rax;
label_3:
        if (r13 != 1) {
            rdi = r13;
            eax = revert_tz_part_0 ();
        }
label_0:
        eax = 0;
        return rax;
        rdi = r13;
        al = revert_tz_part_0 ();
    }
    goto label_0;
label_2:
    rdi = r14;
    rsi = rdx;
    goto label_1;
}

/* /tmp/tmpdb8sk3u8 @ 0x94e0 */
 
void dbg_rpl_mktime (int64_t arg1) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    /* time_t rpl_mktime(tm * tp); */
    tzset ();
    rsi = *(reloc.localtime_r);
    rdi = rbp;
    rdx = 0x0000e240;
    return void (*0x8f30)() ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6f50 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x28c0 */
 
uint64_t dbg_main (int32_t argc, char ** argv) {
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r12 = 0x0000a178;
    ebx = edi;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000ab64);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rax = "Kaveh Ghazi";
    rax = "David MacKenzie";
    rax = "Joseph Arceneaux";
    rax = dbg_usage;
    eax = 0;
    parse_gnu_standard_options_only (ebx, rbp, 0x0000a00a, "GNU coreutils", *(obj.Version), 1);
    rax = *(obj.optind);
    ebx -= eax;
    if (ebx != 0) {
        if (ebx == 1) {
            goto label_0;
        }
        rax = quote (*((rbp + rax*8 + 8)), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "extra operand %s");
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        usage (1, rsi, rdx, rcx, r8, r9);
    }
    uptime ("/var/run/utmp", 1, rdx, rcx, r8, r9);
label_0:
    return uptime (*((rbp + rax*8)), 0, rdx, rcx, r8, r9);
}

/* /tmp/tmpdb8sk3u8 @ 0x2590 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2540 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpdb8sk3u8 @ 0x5300 */
 
int64_t dbg_parse_gnu_standard_options_only (int64_t arg_100h, int64_t arg_108h, int64_t arg10, int64_t arg11, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    void (*)() usage_func;
    va_list authors;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    xmm3 = arg10;
    xmm4 = arg11;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* void parse_gnu_standard_options_only(int argc,char ** argv,char const * command_name,char const * package,char const * version,_Bool scan_all,void (*)() usage_func,va_args ...); */
    r13 = r8;
    r12 = rcx;
    r14 = *((rsp + 0x100));
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rdx = 0x0000ab64;
    rax = 0x0000a6d0;
    if (r9b == 0) {
        rdx = rax;
    }
    rcx = obj_long_options;
    r8d = 0;
    ebx = opterr;
    *(obj.opterr) = 1;
    eax = getopt_long ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    if (eax == 0x68) {
        goto label_1;
    }
    if (eax == 0x76) {
        goto label_2;
    }
    edi = *(obj.exit_failure);
    void (*r14)() ();
    do {
label_0:
        *(obj.opterr) = ebx;
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        return rax;
label_2:
        rdi = stdout;
        r8 = rsp;
        rcx = r13;
        rdx = r12;
        rax = rsp + 0x108;
        rsi = rbp;
        *(rsp) = 0x30;
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 4)) = 0x30;
        *((rsp + 0x10)) = rax;
        version_etc_va ();
        exit (0);
label_1:
        edi = 0;
        void (*r14)() ();
    } while (1);
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x77b0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2fe0 */
 
int64_t dbg_usage (char * arg_8h, int64_t arg_10h, char * arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, char * arg_60h, int64_t arg_68h, int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "Print the current time, the length of time the system has been up,\nthe number of users on the system, and the average number of jobs\nin the run queue over the last 1, 5 and 15 minutes.");
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "  Processes in\nan uninterruptible sleep state also contribute to the load average.\n");
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "If FILE is not specified, use %s.  %s as FILE is common.\n\n");
    rcx = "/var/log/wtmp";
    edi = 1;
    rdx = "/var/run/utmp";
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    r12 = 0x0000a00a;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000a0fe;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000a178;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000a182, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000ab64;
    r12 = 0x0000a11a;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000a182, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = 0x0000a00a;
        printf_chk ();
        r12 = 0x0000a11a;
    }
label_5:
    r13 = 0x0000a00a;
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpdb8sk3u8 @ 0x8f30 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_mktime_internal (int64_t arg2, int64_t arg3, int32_t mon) {
    long_int t;
    long_int ot;
    tm tm;
    time_t x;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    signed int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    uint32_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rsi = arg2;
    rdx = arg3;
    rdi = mon;
    /* time_t mktime_internal(tm * tp,tm * (*)() convert,mktime_offset_t * offset); */
    rbx = rdi;
    rcx = *((rdi + 0x10));
    *((rsp + 0x58)) = rdi;
    *((rsp + 0x20)) = rsi;
    rsi = *((rdi + 0xc));
    *((rsp + 0x40)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = *(rdi);
    *((rsp + 0x48)) = eax;
    eax = *((rdi + 4));
    *((rsp + 0x10)) = eax;
    eax = *((rdi + 8));
    rdi = rcx;
    rcx *= 0x2aaaaaab;
    *((rsp + 0x14)) = eax;
    eax = *((rbx + 0x20));
    rcx >>= 0x21;
    *((rsp + 0x38)) = eax;
    eax = edi;
    eax >>= 0x1f;
    ecx -= eax;
    edx = rcx * 3;
    eax = ecx;
    edx <<= 2;
    edi -= edx;
    edx = edi;
    ecx = edi;
    edx >>= 0x1f;
    eax -= edx;
    rdx = *((rbx + 0x14));
    rax = (int64_t) eax;
    r15 = rax + rdx;
    edx = 0;
    if ((r15b & 3) == 0) {
        rax = 0x8f5c28f5c28f5c29;
        rdx = 0x51eb851eb851eb8;
        rdi = 0x28f5c28f5c28f5c;
        rax *= r15;
        rax += rdx;
        edx = 1;
        rax = rotate_right64 (rax, 2);
        if (rax <= rdi) {
            goto label_6;
        }
    }
label_1:
    eax = ecx;
    r12d = 0x3b;
    r9d = 0x46;
    rdi = r15;
    eax >>= 0x1f;
    eax &= 0xc;
    eax += ecx;
    rcx = rdx * 3;
    rdx = rdx + rcx*4;
    rax = (int64_t) eax;
    rax += rdx;
    rdx = obj___mon_yday;
    eax = *((rdx + rax*2));
    eax--;
    rax = (int64_t) eax;
    rsi += rax;
    rax = *((rsp + 0x48));
    *((rsp + 0x18)) = rsi;
    if (eax <= r12d) {
        r12 = rax;
    }
    eax = 0;
    __asm ("cmovs r12, rax");
    rax = *((rsp + 0x40));
    rax = *(rax);
    *((rsp + 8)) = rax;
    eax = *((rsp + 8));
    eax = -eax;
    *((rsp + 0x4c)) = eax;
    rax = ydhms_diff (rdi, rsi, *((rsp + 0x34)), *((rsp + 0x30)), r12d, r9);
    *((rsp + 0x50)) = rax;
    r13 = rsp + 0xa0;
    r14 = rsp + 0x88;
    *((rsp + 0x88)) = rax;
    *((rsp + 8)) = rax;
    *((rsp + 0x28)) = 6;
    *((rsp + 0x30)) = 0;
    do {
        rax = ranged_convert (*((rsp + 0x20)), r14, r13);
        if (rax == 0) {
            goto label_4;
        }
        ebx = *((rsp + 0xa0));
        eax = *((rsp + 0xac));
        eax = *((rsp + 0xb8));
        eax = *((rsp + 0xd4));
        rax = ydhms_diff (r15, *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), r12d, *((rsp + 0xd4)));
        r10 = *((rsp + 0x88));
        if (rax == 0) {
            goto label_7;
        }
        if (rbp != r10) {
            if (*((rsp + 8)) == r10) {
                goto label_8;
            }
        }
label_0:
        if (*((rsp + 8)) == r10) {
            goto label_5;
        }
        edx = *((rsp + 0xc0));
        rax += r10;
        *((rsp + 8)) = rbp;
        *((rsp + 0x88)) = rax;
        eax = 0;
        al = (edx != 0) ? 1 : 0;
        *((rsp + 0x30)) = eax;
    } while (1);
label_8:
    esi = *((rsp + 0xc0));
    if (esi < 0) {
        goto label_2;
    }
    ecx = *((rsp + 0x38));
    dl = (esi != 0) ? 1 : 0;
    if (ecx < 0) {
        goto label_9;
    }
    cl = (ecx != 0) ? 1 : 0;
    if (cl == dl) {
        goto label_0;
    }
label_2:
    rax = *((rsp + 0x4c));
    rdx = r10;
    rax += *((rsp + 0x50));
    rdx -= rax;
    edi = *((rsp + 0x48));
    rax = *((rsp + 0x40));
    *(rax) = rdx;
    if (edi != ebx) {
        al = (ebx == 0x3c) ? 1 : 0;
        edx = 0;
        dl = (edi <= 0) ? 1 : 0;
        rdx &= rax;
        rax = (int64_t) edi;
        rdx -= r12;
        rax += rdx;
        rax += r10;
        *((rsp + 0x88)) = rax;
        if (rax overflow 0) {
            goto label_5;
        }
        rdi = rsp + 0xe0;
        rsi = r13;
        rax = *((rsp + 0x20));
        rax = void (*rax)(uint64_t, uint64_t) (rax, rax);
        r10 = *((rsp + 8));
        if (rax == 0) {
            goto label_4;
        }
    }
    rcx = *((rsp + 0x58));
    __asm ("movdqa xmm0, xmmword [rsp + 0xa0]");
    __asm ("movdqa xmm1, xmmword [rsp + 0xb0]");
    rax = *((rsp + 0xd0));
    __asm ("movdqa xmm2, xmmword [rsp + 0xc0]");
    __asm ("movups xmmword [rcx], xmm0");
    *((rcx + 0x30)) = rax;
    __asm ("movups xmmword [rcx + 0x10], xmm1");
    __asm ("movups xmmword [rcx + 0x20], xmm2");
    goto label_10;
label_5:
    errno_location ();
    *(rax) = 0x4b;
label_4:
    r10 = 0xffffffffffffffff;
label_10:
    rax = *((rsp + 0x118));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_11;
    }
    rax = r10;
    return rax;
label_6:
    rdx = 0xa3d70a3d70a3d70b;
    rax = r15;
    rdx:rax = rax * rdx;
    rax = r15;
    rax >>= 0x3f;
    rdx += r15;
    rdx >>= 6;
    rdx -= rax;
    edx &= 3;
    dl = (rdx == 1) ? 1 : 0;
    edx = (int32_t) dl;
    goto label_1;
label_9:
    edx = (int32_t) dl;
    if (edx < *((rsp + 0x30))) {
        goto label_0;
    }
    goto label_2;
label_7:
    esi = *((rsp + 0x38));
    eax = *((rsp + 0xc0));
    rsp + 8 = (esi == 0) ? 1 : 0;
    edi = *((rsp + 8));
    rsp + 0x7f = (eax == 0) ? 1 : 0;
    ecx = *((rsp + 0x7f));
    if (cl == dil) {
        goto label_2;
    }
    eax |= esi;
    if (eax < 0) {
        goto label_2;
    }
    rax = rsp + 0xe0;
    *((rsp + 0x78)) = r12d;
    r12 = *((rsp + 0x20));
    *((rsp + 0x28)) = rax;
    rax = rsp + 0x90;
    r14d = 0x92c70;
    *((rsp + 0x30)) = rax;
    rax = rsp + 0x98;
    *((rsp + 0x68)) = rax;
    *((rsp + 0x70)) = r15;
    *((rsp + 0x60)) = r13;
    while (r13d == 1) {
        r14d += 0x92c70;
        if (r14d == 0xdb04f20) {
            goto label_12;
        }
        ebx = r14d;
        r15d = r14 + r14;
        r13d = 2;
        ebx = -ebx;
        rax = (int64_t) ebx;
        rax += rbp;
        *((rsp + 0x90)) = rax;
        if (rax !overflow 0) {
            goto label_13;
        }
label_3:
        ebx += r15d;
    }
    rax = (int64_t) ebx;
    r13d = 1;
    rax += rbp;
    *((rsp + 0x90)) = rax;
    if (rax overflow 0) {
        goto label_3;
    }
label_13:
    rax = ranged_convert (r12, *((rsp + 0x30)), *((rsp + 0x28)));
    if (rax == 0) {
        goto label_4;
    }
    eax = *((rsp + 0x100));
    dl = (eax == 0) ? 1 : 0;
    if (*((rsp + 8)) == dl) {
        goto label_14;
    }
    if (eax >= 0) {
        goto label_3;
    }
label_14:
    eax = *((rsp + 0xe0));
    eax = *((rsp + 0xec));
    eax = *((rsp + 0xf8));
    eax = *((rsp + 0x114));
    rax = ydhms_diff (*((rsp + 0x90)), *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), *((rsp + 0x98)), *((rsp + 0x114)));
    rax += *((rsp + 0xb0));
    rsi = *((rsp + 0x60));
    rdi = *((rsp + 0x68));
    rax = void (*r12)(uint64_t) (rax);
    rdx = *((rsp + 0x38));
    if (rax != 0) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) == 0x4b) {
        goto label_3;
    }
    goto label_4;
label_12:
    eax = *((rsp + 8));
    edx = *((rsp + 0x7f));
    r10 = rbp;
    r13 = *((rsp + 0x60));
    r12 = *((rsp + 0x78));
    eax -= edx;
    rdi = *((rsp + 0x28));
    eax *= 0xe10;
    rsi = r13;
    rax = (int64_t) eax;
    r10 += rax;
    rax = *((rsp + 0x20));
    rax = void (*rax)(uint64_t, uint64_t) (r10, r10);
    if (rax == 0) {
        goto label_5;
    }
    ebx = *((rsp + 0xa0));
    r10 = *((rsp + 8));
    goto label_2;
label_15:
    r12 = *((rsp + 0x78));
    r13 = *((rsp + 0x60));
    r10 = rdx;
    ebx = *((rsp + 0xa0));
    goto label_2;
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6c20 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x7310 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7140 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x289f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7020 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2895)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6cc0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x288a)() ();
    }
    if (rdx == 0) {
        void (*0x288a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x71d0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e210]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000e220]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8a50 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8ad0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x6f20 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x3370 */
 
int64_t dbg_c_strtod (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* double c_strtod(char const * nptr,char ** endptr); */
    r12 = rdi;
    rax = c_locale_cache;
    while (1) {
        rdx = c_locale_cache;
        if (rdx != 0) {
            rsi = rbp;
            rdi = r12;
            void (*0x2560)() ();
        }
        if (rbp != 0) {
            *(rbp) = r12;
        }
        xmm0 = 0;
        return rax;
        edx = 0;
        rsi = 0x0000a4ad;
        edi = 0x1fbf;
        rax = newlocale ();
        *(obj.c_locale_cache) = rax;
    }
}

/* /tmp/tmpdb8sk3u8 @ 0x8770 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7790 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpdb8sk3u8 @ 0x5140 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x27c0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpdb8sk3u8 @ 0x6c60 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x8950 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpdb8sk3u8 @ 0x7e10 */
 
int64_t dbg_mktime_z (int64_t arg_8h, int64_t arg_10h, int64_t arg_20h, int64_t arg_30h, tm * arg1, int64_t arg2) {
    tm tm_1;
    int64_t var_114h_2;
    int64_t var_8h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_118h;
    int64_t var_30h_2;
    int64_t var_38h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h_2;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    rdi = arg1;
    rsi = arg2;
    /* time_t mktime_z(timezone_t tz,tm * tm); */
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_2;
    }
    r14 = rdi;
    rax = set_tz (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = *(rbp);
    r15 = rsp;
    *((rsp + 0x1c)) = 0xffffffff;
    *(rsp) = rax;
    rax = *((rbp + 8));
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x10));
    *((rsp + 0x10)) = rax;
    eax = *((rbp + 0x20));
    *((rsp + 0x20)) = eax;
    rax = rpl_mktime (r15);
    r13 = rax;
    eax = *((rsp + 0x1c));
    while (al == 0) {
        if (r12 != 1) {
            rdi = r12;
            revert_tz_part_0 ();
        }
label_1:
        r13 = 0xffffffffffffffff;
label_0:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r13;
        return rax;
        al = save_abbr (r14, r15, rdx, rcx, r8);
    }
    while (al != 0) {
        __asm ("movdqa xmm0, xmmword [rsp]");
        __asm ("movdqa xmm1, xmmword [rsp + 0x10]");
        __asm ("movdqa xmm2, xmmword [rsp + 0x20]");
        rax = *((rsp + 0x30));
        __asm ("movups xmmword [rbp], xmm0");
        *((rbp + 0x30)) = rax;
        __asm ("movups xmmword [rbp + 0x10], xmm1");
        __asm ("movups xmmword [rbp + 0x20], xmm2");
        goto label_0;
        rdi = r12;
        al = revert_tz_part_0 ();
    }
    goto label_1;
label_2:
    rax = *((rsp + 0x38));
    rax -= *(fs:0x28);
    if (rax == 0) {
        rdi = rsi;
        void (*0x7f60)() ();
    }
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8590 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpdb8sk3u8 @ 0x26a0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpdb8sk3u8 @ 0x33f0 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpdb8sk3u8 @ 0x9510 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpdb8sk3u8 @ 0x2510 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpdb8sk3u8 @ 0x7f80 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000ab0b;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000ab1e);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000ae08;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xae08 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2830)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2830)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2830)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpdb8sk3u8 @ 0x8410 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpdb8sk3u8 @ 0x8ab0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x9780 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpdb8sk3u8 @ 0x87b0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x7f60 */
 
void dbg_rpl_timegm (int64_t arg1) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    /* time_t rpl_timegm(tm * tmp); */
    *((rdi + 0x20)) = 0;
    rsi = *(reloc.gmtime_r);
    rdx = 0x0000e238;
    return void (*0x8f30)() ();
}

/* /tmp/tmpdb8sk3u8 @ 0x8b70 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpdb8sk3u8 @ 0x2470 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 1040 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2480 */
 
void utmpxname (void) {
    __asm ("bnd jmp qword [reloc.utmpxname]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2490 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmpdb8sk3u8 @ 0x24a0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpdb8sk3u8 @ 0x24b0 */
 
void localtime (void) {
    __asm ("bnd jmp qword [reloc.localtime]");
}

/* /tmp/tmpdb8sk3u8 @ 0x0 */
 
int64_t libc_start_main (func init, char ** ubp_av) {
    rcx = init;
    rdx = ubp_av;
    bh &= *(rdi);
    *((rdi + riz + 0x5c)) ^= bl;
    *((rcx + rsi)) ^= esi;
    *(rsi) += bh;
    *(rcx) += al;
    *(rax) += al;
    al += dl;
    *(rax) -= eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rcx - 0x78)) += dh;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) |= eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax |= 0x28004000;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmpdb8sk3u8 @ 0x24e0 */
 
void strncpy (void) {
    __asm ("bnd jmp qword [reloc.strncpy]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2500 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2530 */
 
void setenv (void) {
    __asm ("bnd jmp qword [reloc.setenv]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2550 */
 
void endutxent (void) {
    __asm ("bnd jmp qword [reloc.endutxent]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2560 */
 
void strtod_l (void) {
    __asm ("bnd jmp qword [reloc.strtod_l]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2570 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2580 */
 
void getloadavg (void) {
    __asm ("bnd jmp qword [reloc.getloadavg]");
}

/* /tmp/tmpdb8sk3u8 @ 0x25b0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpdb8sk3u8 @ 0x25e0 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpdb8sk3u8 @ 0x25f0 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2600 */
 
void newlocale (void) {
    __asm ("bnd jmp qword [reloc.newlocale]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2610 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2630 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2640 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2650 */
 
void fputc (void) {
    __asm ("bnd jmp qword [reloc.fputc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2660 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpdb8sk3u8 @ 0x26b0 */
 
void setutxent (void) {
    __asm ("bnd jmp qword [reloc.setutxent]");
}

/* /tmp/tmpdb8sk3u8 @ 0x26d0 */
 
void tzset (void) {
    __asm ("bnd jmp qword [reloc.tzset]");
}

/* /tmp/tmpdb8sk3u8 @ 0x26e0 */
 
void kill (void) {
    __asm ("bnd jmp qword [reloc.kill]");
}

/* /tmp/tmpdb8sk3u8 @ 0x26f0 */
 
void time (void) {
    __asm ("bnd jmp qword [reloc.time]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2700 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2710 */
 
void fgets_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fgets_unlocked]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2730 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2750 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2790 */
 
void strftime (void) {
    __asm ("bnd jmp qword [reloc.strftime]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27a0 */
 
void getutxent (void) {
    __asm ("bnd jmp qword [reloc.getutxent]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27c0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27d0 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27e0 */
 
void dcngettext (void) {
    __asm ("bnd jmp qword [reloc.dcngettext]");
}

/* /tmp/tmpdb8sk3u8 @ 0x27f0 */
 
void unsetenv (void) {
    __asm ("bnd jmp qword [reloc.unsetenv]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2800 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2840 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2850 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2860 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2870 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2440 */
 
void fcn_00002440 (void) {
    /* [14] -r-x section size 48 named .plt.got */
    __asm ("bnd jmp qword [reloc.localtime_r]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1056 named .plt */
    __asm ("bnd jmp qword [0x0000ddb8]");
}

/* /tmp/tmpdb8sk3u8 @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpdb8sk3u8 @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}
