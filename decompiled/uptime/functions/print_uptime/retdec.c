void print_uptime(int64_t n, struct utmpx * this) {
    float64_t avg[3]; // bp-8304, 0x2ac0
    float64_t v1[3]; // 0x2f15
    float64_t v2[3]; // 0x2f07
    float64_t v3[3]; // 0x2ba7
    float64_t v4[3]; // 0x2ec8
    int64_t v5 = __readfsqword(40); // 0x2ae6
    int64_t v6 = function_27d0(); // 0x2b11
    int64_t v7; // 0x2ac0
    int64_t v8; // 0x2ac0
    if (v6 == 0) {
        goto lab_0x2b47;
    } else {
        // 0x2b1b
        int64_t v9; // bp-8264, 0x2ac0
        uint64_t v10 = (int64_t)&v9; // 0x2b1b
        if (function_2710() == v10) {
            float64_t v11 = c_strtod((char *)&v9, (char **)&avg); // 0x2e6c
            uint64_t v12 = *(int64_t *)&avg; // 0x2e71
            if (v12 == v10) {
                // 0x2b3f
                rpl_fclose((struct _IO_FILE *)v6);
                goto lab_0x2b47;
            } else {
                int128_t v13 = (float32_t)v11; // 0x2e6c
                __asm_comisd(v13, g2);
                if (v12 < v10) {
                    goto lab_0x2ed7;
                } else {
                    // 0x2e8b
                    __asm_comisd(__asm_movsd(0x43e0000000000000), v13);
                    if (v12 > v10) {
                        int64_t v14 = __asm_cvttsd2si(v13); // 0x2e9c
                        rpl_fclose((struct _IO_FILE *)v6);
                        v7 = v14;
                        if (n != 0) {
                            goto lab_0x2b53;
                        } else {
                            int64_t v15 = function_26f0(); // 0x2ebb
                            v4[0] = v15;
                            avg = v4;
                            v8 = v14;
                            if (v14 == 0) {
                                goto lab_0x2f1a;
                            } else {
                                goto lab_0x2bc5;
                            }
                        }
                    } else {
                        goto lab_0x2ed7;
                    }
                }
            }
        } else {
            // 0x2b3f
            rpl_fclose((struct _IO_FILE *)v6);
            goto lab_0x2b47;
        }
    }
  lab_0x2b47:
    // 0x2b47
    v7 = 0;
    if (n == 0) {
        int64_t v16 = function_26f0(); // 0x2f10
        v1[0] = v16;
        avg = v1;
        goto lab_0x2f1a;
    } else {
        goto lab_0x2b53;
    }
  lab_0x2b53:;
    int64_t v17 = 0;
    int64_t v18 = (int64_t)this;
    int64_t v19 = n - 1;
    int16_t v20 = *(int16_t *)v18; // 0x2b7e
    int64_t v21; // 0x2ac0
    int64_t v22; // 0x2ac0
    while (*(char *)(v18 + 44) == 0 || v20 != 7) {
        // 0x2b60
        v21 = v17;
        if (v20 == 2) {
            // 0x2b66
            v21 = (int64_t)*(int32_t *)(v18 + 340);
        }
        // 0x2b6d
        v22 = v21;
        if (v19 == 0) {
            // break (via goto) -> 0x2ba0
            goto lab_0x2ba0;
        }
        v17 = v21;
        v18 += 384;
        v19--;
        v20 = *(int16_t *)v18;
    }
    int64_t v23 = v17; // 0x2b98
    v22 = v17;
    while (v19 != 0) {
        // 0x2b7a
        v17 = v23;
        v18 += 384;
        v19--;
        v20 = *(int16_t *)v18;
        while (*(char *)(v18 + 44) == 0 || v20 != 7) {
            // 0x2b60
            v21 = v17;
            if (v20 == 2) {
                // 0x2b66
                v21 = (int64_t)*(int32_t *)(v18 + 340);
            }
            // 0x2b6d
            v22 = v21;
            if (v19 == 0) {
                // break (via goto) -> 0x2ba0
                goto lab_0x2ba0;
            }
            v17 = v21;
            v18 += 384;
            v19--;
            v20 = *(int16_t *)v18;
        }
        // 0x2b89
        v23 = v17;
        v22 = v17;
    }
  lab_0x2ba0:;
    int64_t v24 = function_26f0(); // 0x2ba2
    v3[0] = v24;
    avg = v3;
    v8 = v7;
    if (v7 != 0) {
        goto lab_0x2bc5;
    } else {
        // 0x2bb1
        v8 = v24 - v22;
        if (v22 == 0) {
            goto lab_0x2f1a;
        } else {
            goto lab_0x2bc5;
        }
    }
  lab_0x2f1a:
    // 0x2f1a
    function_25a0();
    function_24d0();
    function_27b0();
    // 0x2f46
    function_2610();
    goto lab_0x2d4e;
  lab_0x2bc5:;
    int64_t v25 = function_24b0(); // 0x2c3f
    int64_t v26 = function_25a0();
    if (v25 == 0) {
        // 0x2e13
        function_2780();
    } else {
        // 0x2c59
        fprintftime(g21, (char *)v26, (struct tm *)v25, NULL, 0);
    }
    if (v8 == -1) {
        // 0x2e35
        function_25a0();
        function_2780();
    } else {
        if (v8 > 0x1517f) {
            // 0x2d77
            function_27e0();
            function_2780();
        } else {
            // 0x2c97
            function_25a0();
            function_2780();
        }
    }
    // 0x2cbe
    function_27e0();
    function_2780();
    int32_t v27 = function_2580(); // 0x2cff
    if (v27 == -1) {
        goto lab_0x2dee;
    } else {
        if (v27 < 1) {
            goto lab_0x2d4e;
        } else {
            // 0x2d43
            int64_t v28; // 0x2ac0
            int64_t v29 = __asm_movsd_1(__asm_movsd(v28)); // 0x2d20
            function_25a0();
            __asm_movsd(v29);
            function_2780();
            if (v27 != 1) {
                // 0x2db2
                __asm_movsd(v28);
                function_2780();
                if (v27 != 2) {
                    // 0x2dd6
                    __asm_movsd(v28);
                    function_2780();
                }
            }
            goto lab_0x2dee;
        }
    }
  lab_0x2dee:;
    int64_t v30 = (int64_t)g21; // 0x2dee
    int64_t * v31 = (int64_t *)(v30 + 40); // 0x2df5
    uint64_t v32 = *v31; // 0x2df5
    if (v32 >= *(int64_t *)(v30 + 48)) {
        // 0x2f46
        function_2610();
        goto lab_0x2d4e;
    } else {
        // 0x2e03
        *v31 = v32 + 1;
        *(char *)v32 = 10;
        goto lab_0x2d4e;
    }
  lab_0x2ed7:
    // 0x2ed7
    rpl_fclose((struct _IO_FILE *)v6);
    v7 = -1;
    if (n != 0) {
        goto lab_0x2b53;
    } else {
        int64_t v33 = function_26f0(); // 0x2efd
        v2[0] = v33;
        avg = v2;
        v8 = -1;
        goto lab_0x2bc5;
    }
  lab_0x2d4e:
    // 0x2d4e
    if (v5 == __readfsqword(40)) {
        // 0x2d65
        return;
    }
    // 0x2f55
    function_25d0();
}