void print_uptime(int64_t rdi, struct s0* rsi, void* rdx, int64_t rcx, int32_t* r8) {
    void** rax6;
    void** v7;
    struct s0* rbx8;
    int32_t* rsi9;
    uint64_t rbp10;
    uint32_t* rax11;
    void* rsp12;
    int32_t* r14_13;
    void* rsp14;
    int32_t* r15_15;
    int32_t* rax16;
    void* rsp17;
    int32_t* v18;
    int32_t* v19;
    uint64_t r13_20;
    void** r12_21;
    int32_t v22;
    int32_t* v23;
    uint32_t eax24;
    int1_t cf25;
    int1_t cf26;
    void* rax27;
    void* rsp28;
    void** rdx29;
    int64_t rdx30;
    int64_t rax31;
    void* rsp32;
    void** rax33;
    void* rsp34;
    void** rax35;
    void** rdi36;
    void** rax37;
    void* rsp38;
    void** rax39;
    void** rax40;
    void** rdx41;
    void** rax42;
    void** rdx43;
    void* rsp44;
    void** rdi45;
    void** rsi46;
    int32_t eax47;
    void** rax48;
    void** rax49;
    void* rax50;
    int64_t rcx51;
    int32_t eax52;
    int64_t v53;

    rax6 = g28;
    v7 = rax6;
    rbx8 = rsi;
    rsi9 = reinterpret_cast<int32_t*>("r");
    rbp10 = reinterpret_cast<uint64_t>(rdi - 1);
    rax11 = fun_27d0("/proc/uptime", "r");
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 88 - 8 + 8);
    if (!rax11) {
        addr_2b47_2:
        *reinterpret_cast<int32_t*>(&r14_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_13) + 4) = 0;
        if (!rdi) {
            fun_26f0();
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            goto addr_2f1a_4;
        }
    } else {
        r15_15 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp12) + 64);
        *reinterpret_cast<int32_t*>(&rsi9) = 0x2000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax16 = fun_2710(r15_15, 0x2000, rax11);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        r14_13 = rax16;
        if (rax16 != r15_15 || (r8 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp17) + 24), rsi9 = r8, c_strtod(), rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8), v18 == r14_13)) {
            rpl_fclose(rax11, rsi9, rax11);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            goto addr_2b47_2;
        } else {
            __asm__("comisd xmm0, [rip+0x760c]");
            if (reinterpret_cast<uint64_t>(v19) < reinterpret_cast<uint64_t>(r14_13)) {
                addr_2ed7_8:
                r14_13 = reinterpret_cast<int32_t*>(0xffffffffffffffff);
                rpl_fclose(rax11, rsi9, rax11);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                r8 = r8;
                if (rdi) {
                    addr_2b53_9:
                    *reinterpret_cast<int32_t*>(&r13_20) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_20) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r12_21) = 0;
                    *reinterpret_cast<int32_t*>(&r12_21 + 4) = 0;
                } else {
                    fun_26f0();
                    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                    r8 = r8;
                    goto addr_2ecf_11;
                }
            } else {
                *rax16 = v22;
                ++rsi9;
                __asm__("comisd xmm1, xmm0");
                if (reinterpret_cast<uint64_t>(v23) <= reinterpret_cast<uint64_t>(r14_13)) 
                    goto addr_2ed7_8; else 
                    goto addr_2e99_16;
            }
        }
    }
    while (1) {
        eax24 = rbx8->f0;
        if (!rbx8->f2c || *reinterpret_cast<int16_t*>(&eax24) != 7) {
            if (*reinterpret_cast<int16_t*>(&eax24) == 2) {
                r13_20 = reinterpret_cast<uint64_t>(static_cast<int64_t>(rbx8->f154));
            }
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx8) + 0x180);
            cf25 = rbp10 < 1;
            --rbp10;
            if (cf25) 
                break;
        } else {
            ++r12_21;
            rbx8 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx8) + 0x180);
            cf26 = rbp10 < 1;
            --rbp10;
            if (cf26) 
                goto addr_2b9a_22;
        }
    }
    addr_2ba0_23:
    rax27 = fun_26f0();
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    if (r14_13 || (r14_13 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rax27) - r13_20), !!r13_20)) {
        r8 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp14) + 24);
    } else {
        addr_2f1a_4:
        fun_25a0();
        fun_24d0();
        fun_27b0();
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_2f46_25;
    }
    addr_2bc5_26:
    rdx29 = reinterpret_cast<void**>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r14_13) >> 63));
    rdx30 = (__intrinsic() >> 10) - (reinterpret_cast<int64_t>(r14_13 + reinterpret_cast<unsigned char>(rdx29) * 0xffffffffffffaba0) >> 63);
    rax31 = fun_24b0(r8, rsi9);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    if (!rax31) {
        rax33 = fun_25a0();
        fun_2780(1, rax33, 5);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
    } else {
        rax35 = fun_25a0();
        rdi36 = stdout;
        fprintftime(rdi36, rax35, rax31);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
    }
    if (r14_13 == 0xffffffffffffffff) {
        rax37 = fun_25a0();
        fun_2780(1, rax37, 5);
        rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
    } else {
        if (reinterpret_cast<int64_t>(r14_13) > reinterpret_cast<int64_t>(0x1517f)) {
            rax39 = fun_27e0();
            fun_2780(1, rax39, rdx29, 1, rax39, rdx29);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
        } else {
            rax40 = fun_25a0();
            *reinterpret_cast<int32_t*>(&rdx41) = *reinterpret_cast<int32_t*>(&rdx30);
            *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
            fun_2780(1, rax40, rdx41, 1, rax40, rdx41);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
        }
    }
    *reinterpret_cast<int32_t*>(&r8) = 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8) + 4) = 0;
    rax42 = fun_27e0();
    rdx43 = r12_21;
    fun_2780(1, rax42, rdx43, 1, rax42, rdx43);
    rsp44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8 - 8 + 8);
    rdi45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp44) + 32);
    *reinterpret_cast<int32_t*>(&rsi46) = 3;
    *reinterpret_cast<int32_t*>(&rsi46 + 4) = 0;
    eax47 = fun_2580();
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
    if (eax47 == -1) 
        goto addr_2dee_35;
    if (!(reinterpret_cast<uint1_t>(eax47 < 0) | reinterpret_cast<uint1_t>(eax47 == 0))) {
        *reinterpret_cast<int32_t*>(&rdx43) = 5;
        *reinterpret_cast<int32_t*>(&rdx43 + 4) = 0;
        *reinterpret_cast<int32_t*>(&localtime_r) = ga0a7;
        rax48 = fun_25a0();
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&localtime_r) + 4) = ga0ab;
        *reinterpret_cast<int32_t*>(&rdi45) = 1;
        *reinterpret_cast<int32_t*>(&rdi45 + 4) = 0;
        rsi46 = rax48;
        fun_2780(1, rsi46, 5, 1, rsi46, 5);
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
    }
    if (eax47 <= 1) 
        goto addr_2d48_48;
    *reinterpret_cast<void***>(rdi45) = *reinterpret_cast<void***>(rsi46);
    rsi46 = reinterpret_cast<void**>(", %.2f");
    fun_2780(1, ", %.2f", rdx43, 1, ", %.2f", rdx43);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
    if (eax47 == 2) 
        goto addr_2dee_35;
    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&localtime_r) + 1) = ga0bd;
    rsi46 = reinterpret_cast<void**>(", %.2f");
    fun_2780(1, ", %.2f", rdx43, 1, ", %.2f", rdx43);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
    goto addr_2dee_35;
    addr_2d48_48:
    if (eax47 == 1) {
        addr_2dee_35:
        rdi45 = stdout;
        rax49 = *reinterpret_cast<void***>(rdi45 + 40);
        if (reinterpret_cast<unsigned char>(rax49) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi45 + 48))) {
            addr_2f46_25:
            *reinterpret_cast<int32_t*>(&rsi46) = 10;
            fun_2610();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            goto addr_2d4e_57;
        } else {
            *reinterpret_cast<void***>(rdi45 + 40) = rax49 + 1;
            *reinterpret_cast<void***>(rax49) = reinterpret_cast<void**>(10);
            goto addr_2d4e_57;
        }
    } else {
        addr_2d4e_57:
        rax50 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (!rax50) {
            return;
        }
    }
    fun_25d0();
    *reinterpret_cast<int32_t*>(&rcx51) = *reinterpret_cast<int32_t*>(&rsi46);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx51) + 4) = 0;
    eax52 = read_utmp();
    if (!eax52) 
        goto addr_2f97_62;
    addr_2fad_63:
    quotearg_n_style_colon();
    fun_24d0();
    fun_27b0();
    addr_2f97_62:
    print_uptime(v53, 0, reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 - 32 + 16, rcx51, r8);
    fun_2810();
    goto addr_2fad_63;
    addr_2b9a_22:
    goto addr_2ba0_23;
    addr_2ecf_11:
    *reinterpret_cast<int32_t*>(&r12_21) = 0;
    *reinterpret_cast<int32_t*>(&r12_21 + 4) = 0;
    goto addr_2bc5_26;
    addr_2e99_16:
    __asm__("cvttsd2si r14, xmm0");
    rpl_fclose(rax11, rsi9, rax11);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
    r8 = r8;
    if (rdi) 
        goto addr_2b53_9;
    fun_26f0();
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    r8 = r8;
    if (!r14_13) 
        goto addr_2f1a_4; else 
        goto addr_2ecf_11;
}