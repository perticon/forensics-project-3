void print_uptime(long param_1,short *param_2)

{
  undefined4 uVar1;
  int iVar2;
  FILE *__stream;
  char *pcVar3;
  long lVar4;
  tm *ptVar5;
  undefined8 uVar6;
  int *piVar7;
  long lVar8;
  _IO_FILE *p_Var9;
  long lVar10;
  long lVar11;
  long in_FS_OFFSET;
  bool bVar12;
  double dVar13;
  char *local_2070;
  double local_2068;
  undefined8 local_2060;
  undefined8 local_2058;
  char local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar8 = param_1 + -1;
  __stream = fopen("/proc/uptime","r");
  if (__stream == (FILE *)0x0) {
LAB_00102b47:
    lVar4 = 0;
    if (param_1 == 0) {
      local_2070 = (char *)time((time_t *)0x0);
      goto LAB_00102f1a;
    }
LAB_00102b53:
    lVar11 = 0;
    lVar10 = 0;
    do {
      while ((*(char *)(param_2 + 0x16) == '\0' || (*param_2 != 7))) {
        if (*param_2 == 2) {
          lVar11 = (long)*(int *)(param_2 + 0xaa);
        }
        param_2 = param_2 + 0xc0;
        bVar12 = lVar8 == 0;
        lVar8 = lVar8 + -1;
        if (bVar12) goto LAB_00102ba0;
      }
      lVar10 = lVar10 + 1;
      param_2 = param_2 + 0xc0;
      bVar12 = lVar8 != 0;
      lVar8 = lVar8 + -1;
    } while (bVar12);
LAB_00102ba0:
    local_2070 = (char *)time((time_t *)0x0);
    if ((lVar4 == 0) && (lVar4 = (long)local_2070 - lVar11, lVar11 == 0)) goto LAB_00102f1a;
LAB_00102bc5:
    iVar2 = (int)((lVar4 % 0x15180) / 0xe10);
    uVar1 = (undefined4)((lVar4 % 0x15180 - (long)(iVar2 * 0xe10)) / 0x3c);
    ptVar5 = localtime((time_t *)&local_2070);
    if (ptVar5 == (tm *)0x0) {
      uVar6 = dcgettext(0," ??:????  ",5);
      __printf_chk(1,uVar6);
    }
    else {
      uVar6 = dcgettext(0," %H:%M:%S  ",5);
      fprintftime(stdout,uVar6,ptVar5,0,0);
    }
    if (lVar4 == -1) {
      uVar6 = dcgettext(0,"up ???? days ??:??,  ",5);
      __printf_chk(1,uVar6);
    }
    else if (lVar4 < 0x15180) {
      uVar6 = dcgettext(0,"up  %2d:%02d,  ",5);
      __printf_chk(1,uVar6,iVar2,uVar1);
    }
    else {
      uVar6 = dcngettext(0,"up %ld day %2d:%02d,  ","up %ld days %2d:%02d,  ",lVar4 / 0x15180,5);
      __printf_chk(1,uVar6,lVar4 / 0x15180,iVar2,uVar1);
    }
    uVar6 = dcngettext(0,"%lu user","%lu users",lVar10,5);
    __printf_chk(1,uVar6,lVar10);
    iVar2 = getloadavg(&local_2068,3);
    if (iVar2 != -1) {
      if (0 < iVar2) {
        uVar6 = dcgettext(0,",  load average: %.2f",5);
        __printf_chk(local_2068,1,uVar6);
      }
      if (iVar2 < 2) {
        if (iVar2 != 1) goto LAB_00102d4e;
      }
      else {
        __printf_chk(local_2060,1,", %.2f");
        if (iVar2 != 2) {
          __printf_chk(local_2058,1,", %.2f");
        }
      }
    }
    pcVar3 = stdout->_IO_write_ptr;
    p_Var9 = stdout;
    if (pcVar3 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar3 + 1;
      *pcVar3 = '\n';
      goto LAB_00102d4e;
    }
  }
  else {
    pcVar3 = fgets_unlocked(local_2048,0x2000,__stream);
    if ((pcVar3 != local_2048) ||
       (dVar13 = (double)c_strtod(pcVar3,&local_2070), local_2070 == pcVar3)) {
      rpl_fclose();
      goto LAB_00102b47;
    }
    if ((dVar13 < _DAT_0010a490) || (DAT_0010a498 <= dVar13)) {
      lVar4 = -1;
      rpl_fclose();
      if (param_1 != 0) goto LAB_00102b53;
      local_2070 = (char *)time((time_t *)0x0);
LAB_00102ecf:
      lVar10 = 0;
      goto LAB_00102bc5;
    }
    lVar4 = (long)dVar13;
    rpl_fclose();
    if (param_1 != 0) goto LAB_00102b53;
    local_2070 = (char *)time((time_t *)0x0);
    if (lVar4 != 0) goto LAB_00102ecf;
LAB_00102f1a:
    uVar6 = dcgettext(0,"couldn\'t get boot time",5);
    piVar7 = __errno_location();
    p_Var9 = (_IO_FILE *)0x1;
    error(1,*piVar7,uVar6);
  }
  __overflow(p_Var9,10);
LAB_00102d4e:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}