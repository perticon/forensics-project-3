void usage(int param_1)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  char *pcVar6;
  char *pcVar7;
  undefined *puVar8;
  long in_FS_OFFSET;
  undefined *local_b8;
  char *local_b0;
  char *local_a8 [5];
  char *local_80;
  char *local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  undefined8 local_58;
  undefined8 local_50;
  undefined8 local_40;
  
  uVar5 = program_name;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (param_1 != 0) {
    uVar4 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar4,uVar5);
    goto LAB_0010303e;
  }
  uVar4 = dcgettext(0,"Usage: %s [OPTION]... [FILE]\n",5);
  __printf_chk(1,uVar4,uVar5);
  uVar5 = dcgettext(0,
                    "Print the current time, the length of time the system has been up,\nthe number of users on the system, and the average number of jobs\nin the run queue over the last 1, 5 and 15 minutes."
                    ,5);
  __printf_chk(1,uVar5);
  uVar5 = dcgettext(0,
                    "  Processes in\nan uninterruptible sleep state also contribute to the load average.\n"
                    ,5);
  __printf_chk(1,uVar5);
  uVar5 = dcgettext(0,"If FILE is not specified, use %s.  %s as FILE is common.\n\n",5);
  __printf_chk(1,uVar5,"/var/run/utmp","/var/log/wtmp");
  pFVar1 = stdout;
  pcVar6 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar6,pFVar1);
  pFVar1 = stdout;
  pcVar6 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar6,pFVar1);
  local_58 = 0;
  local_b8 = &DAT_0010a0fe;
  local_b0 = "test invocation";
  local_a8[0] = "coreutils";
  local_a8[1] = "Multi-call invocation";
  local_a8[4] = "sha256sum";
  local_a8[2] = "sha224sum";
  local_78 = "sha384sum";
  local_a8[3] = "sha2 utilities";
  local_80 = "sha2 utilities";
  local_70 = "sha2 utilities";
  local_68 = "sha512sum";
  local_60 = "sha2 utilities";
  local_50 = 0;
  ppuVar2 = &local_b8;
  do {
    puVar8 = (undefined *)ppuVar2;
    if (*(char **)(puVar8 + 0x10) == (char *)0x0) break;
    iVar3 = strcmp("uptime",*(char **)(puVar8 + 0x10));
    ppuVar2 = (undefined **)(puVar8 + 0x10);
  } while (iVar3 != 0);
  pcVar6 = *(char **)(puVar8 + 0x18);
  if (pcVar6 == (char *)0x0) {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar6 = setlocale(5,(char *)0x0);
    if (pcVar6 != (char *)0x0) {
      iVar3 = strncmp(pcVar6,"en_",3);
      if (iVar3 != 0) {
        pcVar6 = "uptime";
        goto LAB_00103340;
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    pcVar6 = "uptime";
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","uptime");
    pcVar7 = " invocation";
  }
  else {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar7 = setlocale(5,(char *)0x0);
    if (pcVar7 != (char *)0x0) {
      iVar3 = strncmp(pcVar7,"en_",3);
      if (iVar3 != 0) {
LAB_00103340:
        pFVar1 = stdout;
        pcVar7 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar7,pFVar1);
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","uptime");
    pcVar7 = " invocation";
    if (pcVar6 != "uptime") {
      pcVar7 = "";
    }
  }
  uVar5 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar5,pcVar6,pcVar7);
LAB_0010303e:
                    /* WARNING: Subroutine does not return */
  exit(param_1);
}