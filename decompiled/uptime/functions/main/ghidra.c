void main(int param_1,undefined8 *param_2)

{
  undefined8 uVar1;
  undefined8 uVar2;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar2 = 0x102910;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"uptime","GNU coreutils",Version,1,usage,"Joseph Arceneaux",
             "David MacKenzie","Kaveh Ghazi",0,uVar2);
  if (param_1 != optind) {
    if (param_1 - optind == 1) {
                    /* WARNING: Subroutine does not return */
      uptime(param_2[optind],0);
    }
    uVar2 = quote(param_2[(long)optind + 1]);
    uVar1 = dcgettext(0,"extra operand %s",5);
    error(0,0,uVar1,uVar2);
    usage(1);
  }
                    /* WARNING: Subroutine does not return */
  uptime("/var/run/utmp",1);
}