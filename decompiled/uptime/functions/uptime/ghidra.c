void uptime(undefined8 param_1,undefined4 param_2)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  int iVar3;
  int iVar4;
  undefined8 uVar5;
  int *piVar6;
  undefined8 uVar7;
  char *pcVar8;
  char *pcVar9;
  undefined *puVar10;
  long in_FS_OFFSET;
  undefined *puStack224;
  char *pcStack216;
  char *apcStack208 [5];
  char *pcStack168;
  char *pcStack160;
  char *pcStack152;
  char *pcStack144;
  char *pcStack136;
  undefined8 uStack128;
  undefined8 uStack120;
  undefined8 uStack104;
  undefined8 local_20;
  undefined8 local_18;
  undefined8 local_10;
  
  local_10 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_18 = 0;
  iVar3 = read_utmp(param_1,&local_20,&local_18,param_2);
  if (iVar3 == 0) {
    print_uptime(local_20,local_18);
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  uVar5 = quotearg_n_style_colon(0,3,param_1);
  piVar6 = __errno_location();
  iVar3 = 1;
  error(1,*piVar6,"%s",uVar5);
  uVar5 = program_name;
  uStack104 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (iVar3 != 0) {
    uVar7 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar7,uVar5);
    goto LAB_0010303e;
  }
  uVar7 = dcgettext(0,"Usage: %s [OPTION]... [FILE]\n",5);
  __printf_chk(1,uVar7,uVar5);
  uVar5 = dcgettext(0,
                    "Print the current time, the length of time the system has been up,\nthe number of users on the system, and the average number of jobs\nin the run queue over the last 1, 5 and 15 minutes."
                    ,5);
  __printf_chk(1,uVar5);
  uVar5 = dcgettext(0,
                    "  Processes in\nan uninterruptible sleep state also contribute to the load average.\n"
                    ,5);
  __printf_chk(1,uVar5);
  uVar5 = dcgettext(0,"If FILE is not specified, use %s.  %s as FILE is common.\n\n",5);
  __printf_chk(1,uVar5,"/var/run/utmp","/var/log/wtmp");
  pFVar1 = stdout;
  pcVar8 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar8,pFVar1);
  pFVar1 = stdout;
  pcVar8 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar8,pFVar1);
  uStack128 = 0;
  puStack224 = &DAT_0010a0fe;
  pcStack216 = "test invocation";
  apcStack208[0] = "coreutils";
  apcStack208[1] = "Multi-call invocation";
  apcStack208[4] = "sha256sum";
  apcStack208[2] = "sha224sum";
  pcStack160 = "sha384sum";
  apcStack208[3] = "sha2 utilities";
  pcStack168 = "sha2 utilities";
  pcStack152 = "sha2 utilities";
  pcStack144 = "sha512sum";
  pcStack136 = "sha2 utilities";
  uStack120 = 0;
  ppuVar2 = &puStack224;
  do {
    puVar10 = (undefined *)ppuVar2;
    if (*(char **)(puVar10 + 0x10) == (char *)0x0) break;
    iVar4 = strcmp("uptime",*(char **)(puVar10 + 0x10));
    ppuVar2 = (undefined **)(puVar10 + 0x10);
  } while (iVar4 != 0);
  pcVar8 = *(char **)(puVar10 + 0x18);
  if (pcVar8 == (char *)0x0) {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar8 = setlocale(5,(char *)0x0);
    if (pcVar8 != (char *)0x0) {
      iVar4 = strncmp(pcVar8,"en_",3);
      if (iVar4 != 0) {
        pcVar8 = "uptime";
        goto LAB_00103340;
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    pcVar8 = "uptime";
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","uptime");
    pcVar9 = " invocation";
  }
  else {
    uVar5 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar5,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar9 = setlocale(5,(char *)0x0);
    if (pcVar9 != (char *)0x0) {
      iVar4 = strncmp(pcVar9,"en_",3);
      if (iVar4 != 0) {
LAB_00103340:
        pFVar1 = stdout;
        pcVar9 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar9,pFVar1);
      }
    }
    uVar5 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar5,"https://www.gnu.org/software/coreutils/","uptime");
    pcVar9 = " invocation";
    if (pcVar8 != "uptime") {
      pcVar9 = "";
    }
  }
  uVar5 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar5,pcVar8,pcVar9);
LAB_0010303e:
                    /* WARNING: Subroutine does not return */
  exit(iVar3);
}