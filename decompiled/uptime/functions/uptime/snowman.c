int64_t uptime(int64_t rdi, int32_t esi, void** rdx, int64_t rcx, int32_t* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rcx11;
    int32_t eax12;
    int64_t v13;

    *reinterpret_cast<int32_t*>(&rcx11) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
    eax12 = read_utmp();
    if (!eax12) {
        print_uptime(v13, 0, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32 + 16, rcx11, r8);
        fun_2810();
    }
    quotearg_n_style_colon();
    fun_24d0();
    fun_27b0();
}