void uptime(char * filename, int32_t options) {
    // 0x2f60
    __readfsqword(40);
    struct utmpx * v1 = NULL; // bp-24, 0x2f85
    int32_t * utmp_buf; // bp-32, 0x2f60
    if (read_utmp(filename, (int64_t *)&utmp_buf, &v1, options) == 0) {
        // 0x2f97
        print_uptime((int64_t)utmp_buf, v1);
        function_2810();
    }
    // 0x2fad
    quotearg_n_style_colon();
    function_24d0();
    function_27b0();
}