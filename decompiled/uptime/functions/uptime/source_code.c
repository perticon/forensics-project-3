uptime (char const *filename, int options)
{
  size_t n_users;
  STRUCT_UTMP *utmp_buf = NULL;

#if HAVE_UTMPX_H || HAVE_UTMP_H
  if (read_utmp (filename, &n_users, &utmp_buf, options) != 0)
    die (EXIT_FAILURE, errno, "%s", quotef (filename));
#endif

  print_uptime (n_users, utmp_buf);

  exit (EXIT_SUCCESS);
}