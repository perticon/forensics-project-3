undefined8 main(int param_1,undefined8 *param_2)

{
  ulong uVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar3 = 0x102630;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"hostid","GNU coreutils",Version,1,usage,"Jim Meyering",0,uVar3);
  if (param_1 <= optind) {
    uVar1 = gethostid();
    __printf_chk(1,"%08x\n",uVar1 & 0xffffffff);
    return 0;
  }
  uVar3 = quote(param_2[optind]);
  uVar2 = dcgettext(0,"extra operand %s",5);
  error(0,0,uVar2,uVar3);
                    /* WARNING: Subroutine does not return */
  usage(1);
}