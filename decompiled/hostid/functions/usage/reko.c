void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002560(fn00000000000023A0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000282E;
	}
	fn0000000000002500(fn00000000000023A0(0x05, "Usage: %s [OPTION]\nPrint the numeric identifier (in hexadecimal) for the current host.\n\n", null), 0x01);
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_534 * rbx_126 = fp - 0xB8 + 16;
	do
	{
		char * rsi_128 = rbx_126->qw0000;
		++rbx_126;
	} while (rsi_128 != null && fn0000000000002460(rsi_128, "hostid") != 0x00);
	ptr64 r13_141 = rbx_126->qw0008;
	if (r13_141 != 0x00)
	{
		fn0000000000002500(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_29 rax_228 = fn00000000000024F0(null, 0x05);
		if (rax_228 == 0x00 || fn0000000000002330(0x03, "en_", rax_228) == 0x00)
			goto l00000000000029BE;
	}
	else
	{
		fn0000000000002500(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_29 rax_170 = fn00000000000024F0(null, 0x05);
		if (rax_170 == 0x00 || fn0000000000002330(0x03, "en_", rax_170) == 0x00)
		{
			fn0000000000002500(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000029FB:
			fn0000000000002500(fn00000000000023A0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000282E:
			fn0000000000002540(edi);
		}
		r13_141 = 0x7004;
	}
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000029BE:
	fn0000000000002500(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000029FB;
}