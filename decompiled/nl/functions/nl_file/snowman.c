uint32_t nl_file(void** rdi, void** rsi, void** rdx, void** rcx) {
    uint32_t eax5;
    void** v6;
    uint32_t eax7;
    uint32_t ebx8;
    int32_t* rax9;
    int32_t* v10;
    void** rax11;
    void** rbp12;
    uint32_t eax13;
    void** rsi14;
    void** rdi15;
    void** r8_16;
    int64_t rax17;
    void** r15_18;
    void** r14_19;
    void** r12_20;
    void** rax21;
    void** r13_22;
    uint32_t eax23;
    unsigned char* rax24;
    uint32_t eax25;
    int1_t zf26;
    uint32_t eax27;
    int1_t zf28;
    uint32_t eax29;
    void** rax30;
    void** rsi31;
    void** rdi32;
    int64_t rax33;
    uint32_t eax34;
    void** rdi35;
    int64_t rax36;
    unsigned char* rax37;
    int1_t zf38;
    void** rdi39;
    void** rax40;
    void** rax41;
    unsigned char* rax42;
    int1_t zf43;
    unsigned char* rax44;
    int1_t zf45;
    int32_t r13d46;
    int64_t rax47;

    eax5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    v6 = rdi;
    eax7 = eax5 - 45;
    ebx8 = eax7;
    if (!eax7) {
        ebx8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    }
    rax9 = fun_24c0(rdi);
    v10 = rax9;
    if (ebx8) {
        rax11 = fun_27d0(v6, "r", rdx, rcx);
        rbp12 = rax11;
        if (!rax11) {
            quotearg_n_style_colon();
            fun_27b0();
            eax13 = 0;
            goto addr_331f_6;
        }
    } else {
        have_read_stdin = 1;
        rbp12 = stdin;
    }
    fadvise(rbp12, 2, rdx, rcx);
    while (rsi14 = rbp12, rdi15 = reinterpret_cast<void**>(0x1c160), rax17 = readlinebuffer(0x1c160, rsi14, rdx, rcx, r8_16), !!rax17) {
        do {
            r15_18 = g1c168;
            r14_19 = r15_18 + 0xffffffffffffffff;
            if (reinterpret_cast<unsigned char>(r14_19) <= reinterpret_cast<unsigned char>(1) || ((r12_20 = footer_del_len, reinterpret_cast<unsigned char>(r12_20) <= reinterpret_cast<unsigned char>(1)) || (rax21 = section_del, r13_22 = g1c170, eax23 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax21)), *reinterpret_cast<void***>(r13_22) != *reinterpret_cast<void***>(&eax23)))) {
                addr_3250_11:
                rax24 = current_type;
                eax25 = *rax24;
                if (*reinterpret_cast<signed char*>(&eax25) == 0x6e) 
                    goto addr_3348_12;
            } else {
                zf26 = r14_19 == header_del_len;
                if (!zf26) 
                    goto addr_321f_14;
                rsi14 = header_del;
                rdx = r14_19;
                rdi15 = r13_22;
                eax27 = fun_2660(rdi15, rsi14, rdx, rcx, rdi15, rsi14, rdx, rcx);
                if (!eax27) 
                    break;
                addr_321f_14:
                zf28 = r14_19 == body_del_len;
                if (!zf28) 
                    goto addr_322c_16;
                rsi14 = body_del;
                rdx = r14_19;
                rdi15 = r13_22;
                eax29 = fun_2660(rdi15, rsi14, rdx, rcx, rdi15, rsi14, rdx, rcx);
                if (!eax29) 
                    goto addr_344a_18;
                addr_322c_16:
                if (r14_19 != r12_20) 
                    goto addr_3250_11; else 
                    goto addr_3231_19;
            }
            if (*reinterpret_cast<signed char*>(&eax25) <= 0x6e) {
                if (*reinterpret_cast<signed char*>(&eax25) == 97) {
                    rax30 = blank_join;
                    if (reinterpret_cast<signed char>(rax30) <= reinterpret_cast<signed char>(1)) {
                        addr_32a2_23:
                        print_lineno(rdi15, rsi14, rdx, rcx, r8_16);
                    } else {
                        if (reinterpret_cast<signed char>(r15_18) > reinterpret_cast<signed char>(1) || (rcx = blank_lines_0, rdx = rcx + 1, blank_lines_0 = rdx, rax30 == rdx)) {
                            print_lineno(rdi15, rsi14, rdx, rcx, r8_16);
                            blank_lines_0 = reinterpret_cast<void**>(0);
                        } else {
                            addr_3348_12:
                            rsi31 = stdout;
                            rdi32 = print_no_line_fmt;
                            fun_2680(rdi32, rsi31, rdx, rcx, r8_16);
                        }
                    }
                    r15_18 = g1c168;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax25) != 0x70) {
                    if (*reinterpret_cast<signed char*>(&eax25) == 0x74) {
                        if (reinterpret_cast<signed char>(r15_18) <= reinterpret_cast<signed char>(1)) {
                            goto addr_3348_12;
                        }
                    }
                } else {
                    rsi14 = g1c170;
                    *reinterpret_cast<int32_t*>(&rcx) = 0;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    r8_16 = r14_19;
                    rdi15 = current_regex;
                    rdx = r14_19;
                    rax33 = rpl_re_search(rdi15, rsi14, rdx);
                    if (rax33 == -2) 
                        goto addr_3563_32;
                    if (rax33 == -1) 
                        goto addr_3348_12; else 
                        goto addr_32a2_23;
                }
            }
            addr_3231_19:
            rsi14 = footer_del;
            rdx = r14_19;
            rdi15 = r13_22;
            eax34 = fun_2660(rdi15, rsi14, rdx, rcx, rdi15, rsi14, rdx, rcx);
            if (!eax34) 
                goto addr_3478_34;
            goto addr_3250_11;
            rdi35 = g1c170;
            rcx = stdout;
            rdx = r15_18;
            fun_2750(rdi35, 1, rdx, rcx, r8_16);
            rsi14 = rbp12;
            rdi15 = reinterpret_cast<void**>(0x1c160);
            rax36 = readlinebuffer(0x1c160, rsi14, rdx, rcx, r8_16);
        } while (rax36);
        goto addr_32dd_37;
        rax37 = header_type;
        zf38 = reset_numbers == 0;
        current_type = rax37;
        current_regex = reinterpret_cast<void**>(0x1c500);
        if (zf38) {
            addr_3404_39:
            rdi39 = stdout;
            rax40 = *reinterpret_cast<void***>(rdi39 + 40);
            if (reinterpret_cast<unsigned char>(rax40) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi39 + 48))) {
                fun_2600();
                continue;
            } else {
                rdx = rax40 + 1;
                *reinterpret_cast<void***>(rdi39 + 40) = rdx;
                *reinterpret_cast<void***>(rax40) = reinterpret_cast<void**>(10);
                continue;
            }
        } else {
            addr_33ef_42:
            rax41 = starting_line_number;
            line_no_overflow = 0;
            line_no = rax41;
            goto addr_3404_39;
        }
        addr_344a_18:
        rax42 = body_type;
        zf43 = reset_numbers == 0;
        current_type = rax42;
        current_regex = reinterpret_cast<void**>(0x1c540);
        if (!zf43) 
            goto addr_33ef_42;
        goto addr_3404_39;
        addr_3478_34:
        rax44 = footer_type;
        zf45 = reset_numbers == 0;
        current_type = rax44;
        current_regex = reinterpret_cast<void**>(0x1c4c0);
        if (!zf45) 
            goto addr_33ef_42;
        goto addr_3404_39;
    }
    addr_32e0_45:
    r13d46 = *v10;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp12)) & 32)) {
        r13d46 = 0;
    }
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v6) == 45) || *reinterpret_cast<void***>(v6 + 1)) {
        rax47 = rpl_fclose(rbp12, rsi14, rdx, rcx, rbp12, rsi14, rdx, rcx);
        if (*reinterpret_cast<int32_t*>(&rax47)) {
            if (r13d46) 
                goto addr_34df_50;
            r13d46 = *v10;
        }
    } else {
        fun_2520(rbp12, rsi14, rdx, rcx, r8_16);
    }
    eax13 = 1;
    if (r13d46) {
        addr_34df_50:
        quotearg_n_style_colon();
        fun_27b0();
        return 0;
    } else {
        addr_331f_6:
        return eax13;
    }
    addr_32dd_37:
    goto addr_32e0_45;
    addr_3563_32:
    fun_2580();
    fun_27b0();
}