undefined8 nl_file(byte *param_1)

{
  ulong __n;
  char cVar1;
  char *pcVar2;
  short *__s1;
  ulong uVar3;
  uint uVar4;
  int iVar5;
  int iVar6;
  int *piVar7;
  long lVar8;
  undefined8 uVar9;
  FILE *__stream;
  size_t __n_00;
  
  uVar4 = *param_1 - 0x2d;
  if (uVar4 == 0) {
    uVar4 = (uint)param_1[1];
  }
  piVar7 = __errno_location();
  if (uVar4 == 0) {
    have_read_stdin = 1;
    __stream = stdin;
  }
  else {
    __stream = fopen((char *)param_1,"r");
    if (__stream == (FILE *)0x0) {
      uVar9 = quotearg_n_style_colon(0,3,param_1);
                    /* WARNING: Subroutine does not return */
      error(0,*piVar7,"%s",uVar9);
    }
  }
  fadvise(__stream,2);
LAB_001031c8:
  lVar8 = readlinebuffer(line_buf,__stream);
  if (lVar8 != 0) {
    do {
      uVar3 = footer_del_len;
      __s1 = line_buf._16_8_;
      __n_00 = line_buf._8_8_;
      __n = line_buf._8_8_ - 1;
      if (((1 < __n) && (1 < footer_del_len)) && (*line_buf._16_8_ == *(short *)section_del)) {
        if ((__n == header_del_len) && (iVar5 = memcmp(line_buf._16_8_,header_del,__n), iVar5 == 0))
        {
          current_regex = header_regex;
          current_type = header_type;
          goto joined_r0x001033ed;
        }
        if ((__n == body_del_len) && (iVar5 = memcmp(__s1,body_del,__n), iVar5 == 0)) {
          current_regex = body_regex;
          current_type = body_type;
          goto joined_r0x001033ed;
        }
        if ((__n == uVar3) && (iVar5 = memcmp(__s1,footer_del,__n), iVar5 == 0)) goto LAB_00103478;
      }
      cVar1 = *current_type;
      if (cVar1 == 'n') goto LAB_00103348;
      if (cVar1 < 'o') {
        if (cVar1 == 'a') {
          if (blank_join < 2) goto LAB_001032a2;
          if (((long)__n_00 < 2) && (blank_lines_0 = blank_lines_0 + 1, blank_join != blank_lines_0)
             ) goto LAB_00103348;
          print_lineno();
          blank_lines_0 = 0;
          __n_00 = line_buf._8_8_;
        }
      }
      else if (cVar1 == 'p') {
        lVar8 = rpl_re_search(current_regex,line_buf._16_8_,__n,0,__n,0);
        if (lVar8 == -2) {
          uVar9 = dcgettext(0,"error in regular expression search",5);
                    /* WARNING: Subroutine does not return */
          error(1,*piVar7,uVar9);
        }
        if (lVar8 == -1) {
LAB_00103348:
          fputs_unlocked(print_no_line_fmt,stdout);
          __n_00 = line_buf._8_8_;
        }
        else {
LAB_001032a2:
          print_lineno();
          __n_00 = line_buf._8_8_;
        }
      }
      else if (cVar1 == 't') {
        if (1 < (long)__n_00) goto LAB_001032a2;
        goto LAB_00103348;
      }
      fwrite_unlocked(line_buf._16_8_,1,__n_00,stdout);
      lVar8 = readlinebuffer(line_buf,__stream);
      if (lVar8 == 0) break;
    } while( true );
  }
  iVar5 = *piVar7;
  if ((*(byte *)&__stream->_flags & 0x20) == 0) {
    iVar5 = 0;
  }
  if ((*param_1 == 0x2d) && (param_1[1] == 0)) {
    clearerr_unlocked(__stream);
  }
  else {
    iVar6 = rpl_fclose();
    if (iVar6 != 0) {
      if (iVar5 != 0) goto LAB_001034df;
      iVar5 = *piVar7;
    }
  }
  if (iVar5 == 0) {
    return 1;
  }
LAB_001034df:
  uVar9 = quotearg_n_style_colon(0,3,param_1);
                    /* WARNING: Subroutine does not return */
  error(0,iVar5,"%s",uVar9);
LAB_00103478:
  current_regex = footer_regex;
  current_type = footer_type;
joined_r0x001033ed:
  if (reset_numbers != '\0') {
    line_no_overflow = 0;
    line_no = starting_line_number;
  }
  pcVar2 = stdout->_IO_write_ptr;
  if (pcVar2 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar2 + 1;
    *pcVar2 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  goto LAB_001031c8;
}