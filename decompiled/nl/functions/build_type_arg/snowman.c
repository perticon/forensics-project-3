signed char build_type_arg(void*** rdi, struct s99* rsi, int64_t rdx) {
    void** rcx4;
    uint32_t eax5;
    int32_t r8d6;
    int32_t eax7;
    void** rbp8;
    void** rax9;
    void** rax10;
    void** rcx11;
    int1_t zf12;
    void** rax13;
    void** rdx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t ebx17;
    int32_t* rax18;
    int32_t* v19;
    void** rax20;
    void** rbp21;
    void** rsi22;
    void** rdi23;
    void** r8_24;
    int64_t rax25;
    void** r15_26;
    void** r14_27;
    void** r12_28;
    void** rax29;
    void** r13_30;
    uint32_t eax31;
    unsigned char* rax32;
    uint32_t eax33;
    int1_t zf34;
    uint32_t eax35;
    int1_t zf36;
    uint32_t eax37;
    void** rax38;
    void** rsi39;
    void** rdi40;
    int64_t rax41;
    uint32_t eax42;
    void** rdi43;
    int64_t rax44;
    unsigned char* rax45;
    int1_t zf46;
    void** rdi47;
    void** rax48;
    void** rax49;
    unsigned char* rax50;
    int1_t zf51;
    unsigned char* rax52;
    int1_t zf53;
    int32_t r13d54;
    int1_t zf55;
    int1_t zf56;
    int64_t rax57;
    void** r8_58;
    void** rcx59;
    void** rdx60;
    void** rsi61;
    void** rax62;
    void** tmp64_63;
    int64_t v64;

    rcx4 = optarg;
    eax5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx4));
    if (*reinterpret_cast<signed char*>(&eax5) != 0x70) {
        if (*reinterpret_cast<signed char*>(&eax5) > 0x70) {
            if (*reinterpret_cast<signed char*>(&eax5) != 0x74) {
                return 0;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&eax5) == 97 || (r8d6 = 0, *reinterpret_cast<signed char*>(&eax5) == 0x6e)) {
                *rdi = rcx4;
                r8d6 = 1;
                goto addr_3044_7;
            } else {
                addr_3044_7:
                eax7 = r8d6;
                return *reinterpret_cast<signed char*>(&eax7);
            }
        }
    }
    optarg = rcx4 + 1;
    *rdi = rcx4;
    rbp8 = optarg;
    rsi->f0 = 0;
    rsi->f20 = rdx;
    rsi->f8 = 0;
    rsi->f28 = 0;
    rpl_re_syntax_options = reinterpret_cast<void**>(0x2c6);
    rax9 = fun_25a0(rbp8);
    rax10 = rpl_re_compile_pattern(rbp8, rax9, rsi);
    if (!rax10) {
        return 1;
    }
    rcx11 = rax10;
    fun_27b0();
    zf12 = line_no_overflow == 0;
    if (zf12) 
        goto addr_30fd_12;
    rax13 = fun_2580();
    rdx14 = rax13;
    fun_27b0();
    eax15 = reinterpret_cast<unsigned char>((&__cxa_finalize)[1]);
    eax16 = eax15 - 45;
    ebx17 = eax16;
    if (!eax16) 
        goto addr_318c_15;
    addr_3190_16:
    rax18 = fun_24c0(1);
    v19 = rax18;
    if (ebx17) {
        rax20 = fun_27d0(1, "r", rdx14, rcx11);
        rbp21 = rax20;
        if (!rax20) {
            quotearg_n_style_colon();
            fun_27b0();
            goto addr_331f_19;
        }
    } else {
        have_read_stdin = 1;
        rbp21 = stdin;
    }
    fadvise(rbp21, 2, rdx14, rcx11);
    while (rsi22 = rbp21, rdi23 = reinterpret_cast<void**>(0x1c160), rax25 = readlinebuffer(0x1c160, rsi22, rdx14, rcx11, r8_24), !!rax25) {
        do {
            r15_26 = g1c168;
            r14_27 = r15_26 + 0xffffffffffffffff;
            if (reinterpret_cast<unsigned char>(r14_27) <= reinterpret_cast<unsigned char>(1) || ((r12_28 = footer_del_len, reinterpret_cast<unsigned char>(r12_28) <= reinterpret_cast<unsigned char>(1)) || (rax29 = section_del, r13_30 = g1c170, eax31 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax29)), *reinterpret_cast<void***>(r13_30) != *reinterpret_cast<void***>(&eax31)))) {
                addr_3250_24:
                rax32 = current_type;
                eax33 = *rax32;
                if (*reinterpret_cast<signed char*>(&eax33) == 0x6e) 
                    goto addr_3348_25;
            } else {
                zf34 = r14_27 == header_del_len;
                if (!zf34) 
                    goto addr_321f_27;
                rsi22 = header_del;
                rdx14 = r14_27;
                rdi23 = r13_30;
                eax35 = fun_2660(rdi23, rsi22, rdx14, rcx11, rdi23, rsi22, rdx14, rcx11);
                if (!eax35) 
                    break;
                addr_321f_27:
                zf36 = r14_27 == body_del_len;
                if (!zf36) 
                    goto addr_322c_29;
                rsi22 = body_del;
                rdx14 = r14_27;
                rdi23 = r13_30;
                eax37 = fun_2660(rdi23, rsi22, rdx14, rcx11, rdi23, rsi22, rdx14, rcx11);
                if (!eax37) 
                    goto addr_344a_31;
                addr_322c_29:
                if (r14_27 != r12_28) 
                    goto addr_3250_24; else 
                    goto addr_3231_32;
            }
            if (*reinterpret_cast<signed char*>(&eax33) <= 0x6e) {
                if (*reinterpret_cast<signed char*>(&eax33) == 97) {
                    rax38 = blank_join;
                    if (reinterpret_cast<signed char>(rax38) <= reinterpret_cast<signed char>(1)) {
                        addr_32a2_36:
                        print_lineno(rdi23, rsi22, rdx14, rcx11, r8_24);
                    } else {
                        if (reinterpret_cast<signed char>(r15_26) > reinterpret_cast<signed char>(1) || (rcx11 = blank_lines_0, rdx14 = rcx11 + 1, blank_lines_0 = rdx14, rax38 == rdx14)) {
                            print_lineno(rdi23, rsi22, rdx14, rcx11, r8_24);
                            blank_lines_0 = reinterpret_cast<void**>(0);
                        } else {
                            addr_3348_25:
                            rsi39 = stdout;
                            rdi40 = print_no_line_fmt;
                            fun_2680(rdi40, rsi39, rdx14, rcx11, r8_24);
                        }
                    }
                    r15_26 = g1c168;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax33) != 0x70) {
                    if (*reinterpret_cast<signed char*>(&eax33) == 0x74) {
                        if (reinterpret_cast<signed char>(r15_26) <= reinterpret_cast<signed char>(1)) {
                            goto addr_3348_25;
                        }
                    }
                } else {
                    rsi22 = g1c170;
                    *reinterpret_cast<int32_t*>(&rcx11) = 0;
                    *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                    r8_24 = r14_27;
                    rdi23 = current_regex;
                    rdx14 = r14_27;
                    rax41 = rpl_re_search(rdi23, rsi22, rdx14);
                    if (rax41 == -2) 
                        goto addr_3563_45;
                    if (rax41 == -1) 
                        goto addr_3348_25; else 
                        goto addr_32a2_36;
                }
            }
            addr_3231_32:
            rsi22 = footer_del;
            rdx14 = r14_27;
            rdi23 = r13_30;
            eax42 = fun_2660(rdi23, rsi22, rdx14, rcx11, rdi23, rsi22, rdx14, rcx11);
            if (!eax42) 
                goto addr_3478_47;
            goto addr_3250_24;
            rdi43 = g1c170;
            rcx11 = stdout;
            rdx14 = r15_26;
            fun_2750(rdi43, 1, rdx14, rcx11, r8_24);
            rsi22 = rbp21;
            rdi23 = reinterpret_cast<void**>(0x1c160);
            rax44 = readlinebuffer(0x1c160, rsi22, rdx14, rcx11, r8_24);
        } while (rax44);
        goto addr_32dd_50;
        rax45 = header_type;
        zf46 = reset_numbers == 0;
        current_type = rax45;
        current_regex = reinterpret_cast<void**>(0x1c500);
        if (zf46) {
            addr_3404_52:
            rdi47 = stdout;
            rax48 = *reinterpret_cast<void***>(rdi47 + 40);
            if (reinterpret_cast<unsigned char>(rax48) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi47 + 48))) {
                fun_2600();
                continue;
            } else {
                rdx14 = rax48 + 1;
                *reinterpret_cast<void***>(rdi47 + 40) = rdx14;
                *reinterpret_cast<void***>(rax48) = reinterpret_cast<void**>(10);
                continue;
            }
        } else {
            addr_33ef_55:
            rax49 = starting_line_number;
            line_no_overflow = 0;
            line_no = rax49;
            goto addr_3404_52;
        }
        addr_344a_31:
        rax50 = body_type;
        zf51 = reset_numbers == 0;
        current_type = rax50;
        current_regex = reinterpret_cast<void**>(0x1c540);
        if (!zf51) 
            goto addr_33ef_55;
        goto addr_3404_52;
        addr_3478_47:
        rax52 = footer_type;
        zf53 = reset_numbers == 0;
        current_type = rax52;
        current_regex = reinterpret_cast<void**>(0x1c4c0);
        if (!zf53) 
            goto addr_33ef_55;
        goto addr_3404_52;
    }
    addr_32e0_58:
    r13d54 = *v19;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp21)) & 32)) {
        r13d54 = 0;
    }
    zf55 = reinterpret_cast<int1_t>((&__cxa_finalize)[1] == 45);
    if (!zf55 || (zf56 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 2) == 0, !zf56)) {
        rax57 = rpl_fclose(rbp21, rsi22, rdx14, rcx11, rbp21, rsi22, rdx14, rcx11);
        if (*reinterpret_cast<int32_t*>(&rax57)) {
            if (r13d54) 
                goto addr_34df_63;
            r13d54 = *v19;
        }
    } else {
        fun_2520(rbp21, rsi22, rdx14, rcx11, r8_24);
    }
    if (r13d54) {
        addr_34df_63:
        quotearg_n_style_colon();
        fun_27b0();
        goto 0x30e5;
    } else {
        addr_331f_19:
        goto 0x30e5;
    }
    addr_32dd_50:
    goto addr_32e0_58;
    addr_3563_45:
    fun_2580();
    fun_27b0();
    addr_318c_15:
    ebx17 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 2);
    goto addr_3190_16;
    addr_30fd_12:
    r8_58 = separator_str;
    rcx59 = line_no;
    *reinterpret_cast<int32_t*>(&rdx60) = lineno_width;
    *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
    rsi61 = lineno_format;
    fun_2780(1, rsi61, rdx60, rcx59, r8_58);
    rax62 = page_incr;
    tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax62) + reinterpret_cast<unsigned char>(line_no));
    line_no = tmp64_63;
    if (__intrinsic()) {
        line_no_overflow = 1;
    }
    goto v64;
}