undefined8 build_type_arg(char **param_1,undefined8 *param_2,undefined8 param_3)

{
  char cVar1;
  char *pcVar2;
  size_t sVar3;
  long lVar4;
  
  cVar1 = *optarg;
  if (cVar1 == 'p') {
    pcVar2 = optarg + 1;
    *param_1 = optarg;
    optarg = pcVar2;
    pcVar2 = optarg;
    *param_2 = 0;
    param_2[4] = param_3;
    param_2[1] = 0;
    param_2[5] = 0;
    rpl_re_syntax_options = 0x2c6;
    sVar3 = strlen(pcVar2);
    lVar4 = rpl_re_compile_pattern(pcVar2,sVar3,param_2);
    if (lVar4 != 0) {
                    /* WARNING: Subroutine does not return */
      error(1,0,"%s",lVar4);
    }
    return 1;
  }
  if (cVar1 < 'q') {
    if ((cVar1 != 'a') && (cVar1 != 'n')) {
      return 0;
    }
  }
  else if (cVar1 != 't') {
    return 0;
  }
  *param_1 = optarg;
  return 1;
}