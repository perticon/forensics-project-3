byte main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  undefined *__src;
  char cVar2;
  byte bVar3;
  byte bVar4;
  int iVar5;
  undefined8 uVar6;
  size_t sVar7;
  char *pcVar8;
  void *pvVar9;
  int *piVar10;
  char *pcVar11;
  long lVar12;
  undefined8 uVar13;
  
  bVar4 = 1;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  have_read_stdin = '\0';
  pcVar8 = section_del;
LAB_00102970:
  do {
    while( true ) {
      section_del = pcVar8;
      uVar13 = 0x102984;
      iVar5 = getopt_long(param_1,param_2,"h:b:f:v:i:pl:s:w:n:d:",longopts,0);
      pcVar11 = optarg;
      if (iVar5 == -1) {
        if (bVar4 == 0) {
                    /* WARNING: Subroutine does not return */
          usage(1);
        }
        sVar7 = strlen(section_del);
        header_del_len = sVar7 * 3;
        header_del = (char *)xmalloc(header_del_len + 1);
        __src = section_del;
        pcVar11 = header_del + sVar7;
        pcVar8 = stpcpy(header_del,section_del);
        pcVar8 = stpcpy(pcVar8,__src);
        strcpy(pcVar8,__src);
        body_del_len = sVar7 * 2;
        footer_del = pcVar11 + sVar7;
        footer_del_len = sVar7;
        body_del = pcVar11;
        initbuffer(line_buf);
        sVar7 = strlen(separator_str);
        print_no_line_fmt = (void *)xmalloc(sVar7 + 1 + (long)lineno_width);
        lVar12 = (long)lineno_width;
        pvVar9 = memset(print_no_line_fmt,0x20,lVar12 + sVar7);
        *(undefined *)((long)pvVar9 + lVar12 + sVar7) = 0;
        line_no = starting_line_number;
        current_type = body_type;
        current_regex = body_regex;
        if (optind == param_1) {
          bVar4 = nl_file(&DAT_0011614c);
        }
        else {
          for (; optind < param_1; optind = optind + 1) {
            bVar3 = nl_file(param_2[optind]);
            bVar4 = bVar4 & bVar3;
          }
        }
        if ((have_read_stdin != '\0') && (iVar5 = rpl_fclose(stdin), iVar5 == -1)) {
          piVar10 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar10,&DAT_0011614c);
        }
        return bVar4 ^ 1;
      }
      pcVar8 = section_del;
      if (iVar5 < 0x78) break;
switchD_001029ab_caseD_63:
      bVar4 = 0;
    }
    if (iVar5 < 0x62) {
      if (iVar5 == -0x83) {
        version_etc(stdout,&DAT_00116019,"GNU coreutils",Version,"Scott Bartram","David MacKenzie",0
                    ,uVar13);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      bVar4 = 0;
      if (iVar5 == -0x82) {
                    /* WARNING: Subroutine does not return */
        usage(0);
      }
      goto LAB_00102970;
    }
    switch(iVar5) {
    case 0x62:
      cVar2 = build_type_arg(&body_type,body_regex,body_fastmap);
      pcVar8 = section_del;
      if (cVar2 == '\0') {
        uVar13 = quote(optarg);
        pcVar8 = "invalid body numbering style: %s";
        goto LAB_00102c85;
      }
      break;
    default:
      goto switchD_001029ab_caseD_63;
    case 100:
      sVar7 = strlen(optarg);
      pcVar8 = pcVar11;
      if (sVar7 - 1 < 2) {
        cVar2 = *pcVar11;
        pcVar1 = section_del;
        while (pcVar8 = section_del, cVar2 != '\0') {
          optarg = pcVar11 + 1;
          *pcVar1 = *pcVar11;
          pcVar1 = pcVar1 + 1;
          pcVar11 = optarg;
          cVar2 = *optarg;
        }
      }
      break;
    case 0x66:
      cVar2 = build_type_arg(&footer_type,footer_regex,footer_fastmap);
      pcVar8 = section_del;
      if (cVar2 == '\0') {
        uVar13 = quote(optarg);
        pcVar8 = "invalid footer numbering style: %s";
        goto LAB_00102c85;
      }
      break;
    case 0x68:
      cVar2 = build_type_arg(&header_type,header_regex,header_fastmap);
      pcVar8 = section_del;
      if (cVar2 == '\0') {
        uVar13 = quote(optarg);
        pcVar8 = "invalid header numbering style: %s";
LAB_00102c85:
        uVar6 = dcgettext(0,pcVar8,5);
                    /* WARNING: Subroutine does not return */
        error(0,0,uVar6,uVar13);
      }
      break;
    case 0x69:
      uVar13 = dcgettext(0,"invalid line number increment",5);
      page_incr = xdectoimax(optarg,0x8000000000000000,0x7fffffffffffffff,&DAT_0011620c,uVar13,0);
      pcVar8 = section_del;
      break;
    case 0x6c:
      uVar13 = dcgettext(0,"invalid line number of blank lines",5);
      blank_join = xdectoimax(optarg,1,0x7fffffffffffffff,&DAT_0011620c,uVar13,0);
      pcVar8 = section_del;
      break;
    case 0x6e:
      if (((*optarg == 'l') && (optarg[1] == 'n')) && (optarg[2] == '\0')) {
        lineno_format = FORMAT_LEFT;
      }
      else {
        iVar5 = strcmp(optarg,"rn");
        if (iVar5 == 0) {
          lineno_format = FORMAT_RIGHT_NOLZ;
          pcVar8 = section_del;
        }
        else {
          iVar5 = strcmp(pcVar11,"rz");
          if (iVar5 != 0) {
            uVar13 = quote();
            pcVar8 = "invalid line numbering format: %s";
            goto LAB_00102c85;
          }
          lineno_format = FORMAT_RIGHT_LZ;
          pcVar8 = section_del;
        }
      }
      break;
    case 0x70:
      reset_numbers = 0;
      break;
    case 0x73:
      separator_str = optarg;
      break;
    case 0x76:
      uVar13 = dcgettext(0,"invalid starting line number",5);
      starting_line_number =
           xdectoimax(optarg,0x8000000000000000,0x7fffffffffffffff,&DAT_0011620c,uVar13,0);
      pcVar8 = section_del;
      break;
    case 0x77:
      uVar13 = dcgettext(0,"invalid line number field width",5);
      lineno_width = xdectoimax(optarg,1,0x7fffffff,&DAT_0011620c,uVar13,0);
      pcVar8 = section_del;
    }
  } while( true );
}