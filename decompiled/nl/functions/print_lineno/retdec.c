int64_t print_lineno(int64_t a1) {
    // 0x30f0
    if (*(char *)&line_no_overflow != 0) {
        // 0x3147
        function_2580();
        return function_27b0();
    }
    // 0x30fd
    function_2780();
    int64_t v1 = line_no; // 0x312b
    int64_t result = v1 + page_incr; // 0x312b
    line_no = result;
    if (((result ^ page_incr) & (result ^ v1)) < 0) {
        // 0x313b
        *(char *)&line_no_overflow = 1;
    }
    // 0x3142
    return result;
}