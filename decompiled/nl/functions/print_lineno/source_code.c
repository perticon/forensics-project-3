print_lineno (void)
{
  if (line_no_overflow)
    die (EXIT_FAILURE, 0, _("line number overflow"));

  printf (lineno_format, lineno_width, line_no, separator_str);

  if (INT_ADD_WRAPV (line_no, page_incr, &line_no))
    line_no_overflow = true;
}