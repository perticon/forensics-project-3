void print_lineno(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int1_t zf6;
    void** r8_7;
    void** rcx8;
    void** rdx9;
    void** rsi10;
    void** rax11;
    void** tmp64_12;
    void** rax13;
    void** rdx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t ebx17;
    int32_t* rax18;
    int32_t* v19;
    void** rax20;
    void** rbp21;
    void** rsi22;
    void** rdi23;
    int64_t rax24;
    void** r15_25;
    void** r14_26;
    void** r12_27;
    void** rax28;
    void** r13_29;
    uint32_t eax30;
    unsigned char* rax31;
    uint32_t eax32;
    int1_t zf33;
    uint32_t eax34;
    int1_t zf35;
    uint32_t eax36;
    void** rax37;
    void** rsi38;
    void** rdi39;
    int64_t rax40;
    uint32_t eax41;
    void** rdi42;
    int64_t rax43;
    unsigned char* rax44;
    int1_t zf45;
    void** rdi46;
    void** rax47;
    void** rax48;
    unsigned char* rax49;
    int1_t zf50;
    unsigned char* rax51;
    int1_t zf52;
    int32_t r13d53;
    int1_t zf54;
    int1_t zf55;
    int64_t rax56;
    int64_t v57;
    int64_t v58;

    zf6 = line_no_overflow == 0;
    if (zf6) {
        r8_7 = separator_str;
        rcx8 = line_no;
        *reinterpret_cast<int32_t*>(&rdx9) = lineno_width;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        rsi10 = lineno_format;
        fun_2780(1, rsi10, rdx9, rcx8, r8_7);
        rax11 = page_incr;
        tmp64_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) + reinterpret_cast<unsigned char>(line_no));
        line_no = tmp64_12;
        if (__intrinsic()) {
            line_no_overflow = 1;
        }
        return;
    }
    rax13 = fun_2580();
    rdx14 = rax13;
    fun_27b0();
    eax15 = reinterpret_cast<unsigned char>((&__cxa_finalize)[1]);
    eax16 = eax15 - 45;
    ebx17 = eax16;
    if (!eax16) 
        goto addr_318c_7;
    addr_3190_8:
    rax18 = fun_24c0(1);
    v19 = rax18;
    if (ebx17) {
        rax20 = fun_27d0(1, "r", rdx14, rcx);
        rbp21 = rax20;
        if (!rax20) {
            quotearg_n_style_colon();
            fun_27b0();
            goto addr_331f_11;
        }
    } else {
        have_read_stdin = 1;
        rbp21 = stdin;
    }
    fadvise(rbp21, 2, rdx14, rcx);
    while (rsi22 = rbp21, rdi23 = reinterpret_cast<void**>(0x1c160), rax24 = readlinebuffer(0x1c160, rsi22, rdx14, rcx, r8), !!rax24) {
        do {
            r15_25 = g1c168;
            r14_26 = r15_25 + 0xffffffffffffffff;
            if (reinterpret_cast<unsigned char>(r14_26) <= reinterpret_cast<unsigned char>(1) || ((r12_27 = footer_del_len, reinterpret_cast<unsigned char>(r12_27) <= reinterpret_cast<unsigned char>(1)) || (rax28 = section_del, r13_29 = g1c170, eax30 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax28)), *reinterpret_cast<void***>(r13_29) != *reinterpret_cast<void***>(&eax30)))) {
                addr_3250_16:
                rax31 = current_type;
                eax32 = *rax31;
                if (*reinterpret_cast<signed char*>(&eax32) == 0x6e) 
                    goto addr_3348_17;
            } else {
                zf33 = r14_26 == header_del_len;
                if (!zf33) 
                    goto addr_321f_19;
                rsi22 = header_del;
                rdx14 = r14_26;
                rdi23 = r13_29;
                eax34 = fun_2660(rdi23, rsi22, rdx14, rcx, rdi23, rsi22, rdx14, rcx);
                if (!eax34) 
                    break;
                addr_321f_19:
                zf35 = r14_26 == body_del_len;
                if (!zf35) 
                    goto addr_322c_21;
                rsi22 = body_del;
                rdx14 = r14_26;
                rdi23 = r13_29;
                eax36 = fun_2660(rdi23, rsi22, rdx14, rcx, rdi23, rsi22, rdx14, rcx);
                if (!eax36) 
                    goto addr_344a_23;
                addr_322c_21:
                if (r14_26 != r12_27) 
                    goto addr_3250_16; else 
                    goto addr_3231_24;
            }
            if (*reinterpret_cast<signed char*>(&eax32) <= 0x6e) {
                if (*reinterpret_cast<signed char*>(&eax32) == 97) {
                    rax37 = blank_join;
                    if (reinterpret_cast<signed char>(rax37) <= reinterpret_cast<signed char>(1)) {
                        addr_32a2_28:
                        print_lineno(rdi23, rsi22, rdx14, rcx, r8);
                    } else {
                        if (reinterpret_cast<signed char>(r15_25) > reinterpret_cast<signed char>(1) || (rcx = blank_lines_0, rdx14 = rcx + 1, blank_lines_0 = rdx14, rax37 == rdx14)) {
                            print_lineno(rdi23, rsi22, rdx14, rcx, r8);
                            blank_lines_0 = reinterpret_cast<void**>(0);
                        } else {
                            addr_3348_17:
                            rsi38 = stdout;
                            rdi39 = print_no_line_fmt;
                            fun_2680(rdi39, rsi38, rdx14, rcx, r8);
                        }
                    }
                    r15_25 = g1c168;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax32) != 0x70) {
                    if (*reinterpret_cast<signed char*>(&eax32) == 0x74) {
                        if (reinterpret_cast<signed char>(r15_25) <= reinterpret_cast<signed char>(1)) {
                            goto addr_3348_17;
                        }
                    }
                } else {
                    rsi22 = g1c170;
                    *reinterpret_cast<int32_t*>(&rcx) = 0;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    r8 = r14_26;
                    rdi23 = current_regex;
                    rdx14 = r14_26;
                    rax40 = rpl_re_search(rdi23, rsi22, rdx14);
                    if (rax40 == -2) 
                        goto addr_3563_37;
                    if (rax40 == -1) 
                        goto addr_3348_17; else 
                        goto addr_32a2_28;
                }
            }
            addr_3231_24:
            rsi22 = footer_del;
            rdx14 = r14_26;
            rdi23 = r13_29;
            eax41 = fun_2660(rdi23, rsi22, rdx14, rcx, rdi23, rsi22, rdx14, rcx);
            if (!eax41) 
                goto addr_3478_39;
            goto addr_3250_16;
            rdi42 = g1c170;
            rcx = stdout;
            rdx14 = r15_25;
            fun_2750(rdi42, 1, rdx14, rcx, r8);
            rsi22 = rbp21;
            rdi23 = reinterpret_cast<void**>(0x1c160);
            rax43 = readlinebuffer(0x1c160, rsi22, rdx14, rcx, r8);
        } while (rax43);
        goto addr_32dd_42;
        rax44 = header_type;
        zf45 = reset_numbers == 0;
        current_type = rax44;
        current_regex = reinterpret_cast<void**>(0x1c500);
        if (zf45) {
            addr_3404_44:
            rdi46 = stdout;
            rax47 = *reinterpret_cast<void***>(rdi46 + 40);
            if (reinterpret_cast<unsigned char>(rax47) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi46 + 48))) {
                fun_2600();
                continue;
            } else {
                rdx14 = rax47 + 1;
                *reinterpret_cast<void***>(rdi46 + 40) = rdx14;
                *reinterpret_cast<void***>(rax47) = reinterpret_cast<void**>(10);
                continue;
            }
        } else {
            addr_33ef_47:
            rax48 = starting_line_number;
            line_no_overflow = 0;
            line_no = rax48;
            goto addr_3404_44;
        }
        addr_344a_23:
        rax49 = body_type;
        zf50 = reset_numbers == 0;
        current_type = rax49;
        current_regex = reinterpret_cast<void**>(0x1c540);
        if (!zf50) 
            goto addr_33ef_47;
        goto addr_3404_44;
        addr_3478_39:
        rax51 = footer_type;
        zf52 = reset_numbers == 0;
        current_type = rax51;
        current_regex = reinterpret_cast<void**>(0x1c4c0);
        if (!zf52) 
            goto addr_33ef_47;
        goto addr_3404_44;
    }
    addr_32e0_50:
    r13d53 = *v19;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp21)) & 32)) {
        r13d53 = 0;
    }
    zf54 = reinterpret_cast<int1_t>((&__cxa_finalize)[1] == 45);
    if (!zf54 || (zf55 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 2) == 0, !zf55)) {
        rax56 = rpl_fclose(rbp21, rsi22, rdx14, rcx, rbp21, rsi22, rdx14, rcx);
        if (*reinterpret_cast<int32_t*>(&rax56)) {
            if (r13d53) 
                goto addr_34df_55;
            r13d53 = *v19;
        }
    } else {
        fun_2520(rbp21, rsi22, rdx14, rcx, r8);
    }
    if (r13d53) {
        addr_34df_55:
        quotearg_n_style_colon();
        fun_27b0();
        goto v57;
    } else {
        addr_331f_11:
        goto v58;
    }
    addr_32dd_42:
    goto addr_32e0_50;
    addr_3563_37:
    fun_2580();
    fun_27b0();
    addr_318c_7:
    ebx17 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 2);
    goto addr_3190_8;
}