byte main(uint param_1,FILE *param_2)

{
  char cVar1;
  byte bVar2;
  int iVar3;
  int iVar4;
  long lVar5;
  byte *pbVar6;
  long lVar7;
  __int32_t **pp_Var8;
  FILE *pFVar9;
  undefined8 uVar10;
  ushort **ppuVar11;
  int *piVar12;
  ulong uVar13;
  size_t sVar14;
  ulong extraout_RDX;
  undefined *puVar15;
  uint uVar16;
  undefined1 *puVar17;
  char cVar18;
  FILE *pFVar19;
  byte *pbVar20;
  char *pcVar21;
  byte *pbVar22;
  FILE **__lineptr;
  ulong uVar23;
  long in_FS_OFFSET;
  bool bVar24;
  undefined auVar25 [16];
  undefined8 uVar26;
  undefined8 uStack392;
  undefined8 local_180;
  char *local_178;
  FILE *local_170;
  char local_161;
  char **local_160;
  FILE *local_158;
  char *local_150;
  size_t local_148;
  ulong local_140;
  long local_138;
  undefined *local_130;
  char **local_128;
  ulong local_120;
  ulong local_118;
  int local_110;
  char local_10c;
  char local_10b;
  byte local_10a;
  byte local_109;
  undefined *local_108;
  uint local_100;
  int local_fc;
  FILE *local_f8;
  size_t local_f0;
  byte local_e8;
  undefined7 uStack231;
  long local_e0;
  undefined local_d8 [80];
  undefined local_88 [72];
  long local_40;
  
  __lineptr = (FILE **)(ulong)param_1;
  pcVar21 = "a:l:bctwz";
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*(undefined8 *)param_2);
  setlocale(6,"");
  bindtextdomain("coreutils");
  textdomain("coreutils");
  atexit(close_stdout);
  setvbuf(stdout,(char *)0x0,1,0);
  local_178 = "";
  local_180 = (size_t *)CONCAT71(local_180._1_7_,1);
  local_161 = '\0';
LAB_00102970:
  do {
    uVar26 = 0x102984;
    iVar3 = getopt_long(__lineptr,param_2,pcVar21,long_options,0);
    if (iVar3 == -1) {
      uVar23 = (ulong)cksum_algorithm;
      min_digest_line_length = 3;
      if (digest_length == 0) goto LAB_00102f81;
      if (cksum_algorithm == 9) {
        if (digest_length < 0x201) {
          digest_hex_bytes = digest_length >> 2;
          goto LAB_00102c14;
        }
        uVar26 = quote(local_178);
        uVar10 = dcgettext(0,"invalid length: %s",5);
        error(0,0,uVar10,uVar26);
        uVar26 = quote(*(undefined8 *)(algorithm_tags + (ulong)cksum_algorithm * 8));
        uVar10 = dcgettext(0,"maximum digest length for %s is %d bits",5);
        error(1,0,uVar10,uVar26,0x200);
      }
      uVar26 = dcgettext(0,"--length is only supported with --algorithm=blake2b",5);
      error(1,0,uVar26);
      goto LAB_00103c4b;
    }
    if (0x86 < iVar3) {
switchD_001029b5_caseD_62:
      do {
        usage(1);
LAB_00103030:
        do {
          if ((strict != 0) &&
             (pcVar21 = "the --strict option is meaningful only when verifying checksums",
             local_161 == '\0')) break;
LAB_00102c5e:
          do {
            iVar3 = (int)__lineptr;
            local_128 = (char **)(&param_2->_flags + (long)iVar3 * 2);
            lVar5 = (long)optind;
            if (optind == iVar3) {
              *local_128 = "-";
              local_128 = local_128 + 1;
            }
            local_160 = (char **)(&param_2->_flags + lVar5 * 2);
            if (local_128 <= local_160) {
              local_10a = 1;
              goto LAB_0010343d;
            }
            local_108 = local_d8;
            local_10a = 1;
            local_100 = (uint)(byte)local_180;
            local_fc = iVar3;
LAB_00102cd0:
            puVar15 = local_108;
            pcVar21 = *local_160;
            local_150 = pcVar21;
            if (local_161 == '\0') {
              bVar2 = digest_file_constprop_0(pcVar21,local_108,&local_e8,&local_e0);
              if (bVar2 == 0) goto LAB_00103427;
              (**(code **)(cksum_output_fns + (ulong)cksum_algorithm * 8))
                        (pcVar21,1,puVar15,local_100,digest_delim,optind != local_fc,local_e0,
                         local_fc);
              bVar2 = local_10a;
              goto LAB_00103427;
            }
            local_130 = local_88;
            local_110 = strcmp(pcVar21,"-");
            if (local_110 != 0) {
              pFVar9 = (FILE *)fopen_safer(local_150,&DAT_0011a008);
              if (pFVar9 != (FILE *)0x0) goto LAB_00102d33;
              uVar26 = quotearg_n_style_colon(0,3,local_150);
              piVar12 = __errno_location();
              error(0,*piVar12,&DAT_0011f104,uVar26);
              bVar2 = 0;
              goto LAB_001032e4;
            }
            have_read_stdin = '\x01';
            local_150 = (char *)dcgettext(0,"standard input",5);
            pFVar9 = stdin;
LAB_00102d33:
            local_180 = &local_f0;
            local_109 = 0;
            __lineptr = &local_f8;
            local_f8 = (FILE *)0x0;
            local_f0 = 0;
            local_10b = '\0';
            local_118 = 0;
            local_120 = 0;
            local_138 = 0;
            local_178 = (char *)0x1;
            do {
              param_2 = (FILE *)__getdelim((char **)__lineptr,local_180,10,pFVar9);
              if ((long)param_2 < 1) goto LAB_0010322c;
              if (*(byte *)&local_f8->_flags == 0x23) {
LAB_00102f30:
                bVar2 = *(byte *)&pFVar9->_flags;
              }
              else {
                lVar5 = (long)param_2 -
                        (ulong)((local_f8[-1]._unused2 + 0x13)[(long)param_2] == '\n');
                local_170 = (FILE *)(lVar5 - (ulong)(*(byte *)((long)local_f8 +
                                                              (lVar5 - (ulong)(0 < lVar5))) == 0xd))
                ;
                param_2 = local_170;
                if (local_170 == (FILE *)0x0) goto LAB_00102f30;
                *(byte *)((long)local_f8 + (long)local_170) = 0;
                lVar5 = 0;
                bVar2 = *(byte *)&local_f8->_flags;
                if ((bVar2 == 9) || (bVar2 == 0x20)) {
                  do {
                    do {
                      lVar5 = lVar5 + 1;
                      bVar2 = *(byte *)((long)&local_f8->_flags + lVar5);
                    } while (bVar2 == 0x20);
                  } while (bVar2 == 9);
                }
                local_10c = '\0';
                if (bVar2 == 0x5c) {
                  lVar5 = lVar5 + 1;
                  local_10c = local_161;
                }
                pbVar20 = (byte *)((long)&local_f8->_flags + lVar5);
                param_2 = local_f8;
                if (algorithm_specified == '\0') {
                  if (max_tag_len_0 == 0) {
                    local_158 = local_f8;
                    pcVar21 = "BSD";
                    puVar17 = algorithm_tags;
                    do {
                      uVar23 = max_tag_len_0;
                      sVar14 = strlen(pcVar21);
                      puVar17 = (undefined1 *)((long)puVar17 + 8);
                      pcVar21 = *(char **)puVar17;
                      max_tag_len_0 = uVar23;
                      if (uVar23 < sVar14) {
                        max_tag_len_0 = sVar14;
                      }
                      param_2 = local_158;
                    } while (pcVar21 != (char *)0x0);
                  }
                  uVar23 = 0;
                  do {
                    bVar2 = pbVar20[uVar23];
                    if ((bVar2 < 0x2e) && ((0xffffdefefffffdfeU >> ((ulong)bVar2 & 0x3f) & 1) == 0))
                    {
                      *(byte *)&((FILE *)(pbVar20 + uVar23))->_flags = 0;
                      local_158 = (FILE *)(pbVar20 + uVar23);
                      lVar7 = argmatch_exact(pbVar20,algorithm_tags);
                      *(byte *)&local_158->_flags = bVar2;
                      if (2 < lVar7) {
                        cksum_algorithm = (uint)lVar7;
                        goto LAB_00102e48;
                      }
                      break;
                    }
                    uVar23 = uVar23 + 1;
                  } while (uVar23 <= max_tag_len_0);
LAB_00102f18:
                  local_138 = local_138 + 1;
                  if (warn != '\0') {
                    param_2 = *(FILE **)(algorithm_tags + (ulong)cksum_algorithm * 8);
                    uVar26 = quotearg_n_style_colon(0,3,local_150);
                    uVar10 = dcgettext(0,"%s: %lu: improperly formatted %s checksum line",5);
                    error(0,0,uVar10,uVar26);
                  }
                  goto LAB_00102f30;
                }
LAB_00102e48:
                local_140 = (ulong)cksum_algorithm;
                pcVar21 = *(char **)(algorithm_tags + local_140 * 8);
                local_158 = (FILE *)((ulong)local_158 & 0xffffffff00000000 | (ulong)cksum_algorithm)
                ;
                local_148 = strlen(pcVar21);
                iVar3 = strncmp((char *)pbVar20,pcVar21,local_148);
                uVar23 = digest_hex_bytes;
                if (iVar3 == 0) {
                  local_148 = local_148 + lVar5;
                  pbVar22 = (byte *)((long)&param_2->_flags + local_148);
                  bVar2 = *pbVar22;
                  *pbVar22 = 0;
                  local_158 = (FILE *)((ulong)local_158 & 0xffffffffffffff00 | (ulong)bVar2);
                  iVar3 = strcmp((char *)pbVar20,pcVar21);
                  if (iVar3 == 0) {
                    digest_length = (ulong)*(int *)(algorithm_bits + local_140 * 4);
                    if ((char)local_158 == '(') {
                      *pbVar22 = 0x28;
                      sVar14 = local_148;
                    }
                    else {
                      sVar14 = local_148 + 1;
                      if ((char)local_158 == '-') {
                        iVar3 = xstrtoumax((byte *)((long)&param_2->_flags + local_148 + 1),
                                           &local_e0,0,&local_e8);
                        if ((((iVar3 != 0) || (uVar23 = CONCAT71(uStack231,local_e8), uVar23 == 0))
                            || (digest_length < uVar23)) || ((local_e8 & 7) != 0))
                        goto LAB_00102f18;
                        sVar14 = local_e0 - (long)param_2;
                        digest_length = uVar23;
                      }
                    }
                    uVar23 = digest_length >> 2;
                    if (*(byte *)((long)&param_2->_flags + sVar14) == 0x20) {
                      sVar14 = sVar14 + 1;
                    }
                    digest_hex_bytes = uVar23;
                    if (*(byte *)((long)&param_2->_flags + sVar14) == 0x28) {
                      lVar5 = (long)local_170 - (sVar14 + 1);
                      if (lVar5 != 0) {
                        pbVar22 = (byte *)((long)&param_2->_flags + sVar14 + 1);
                        for (param_2 = (FILE *)(lVar5 + -1); param_2 != (FILE *)0x0;
                            param_2 = (FILE *)(param_2[-1]._unused2 + 0x13)) {
                          pbVar20 = pbVar22 + (long)param_2;
                          if (pbVar22[(long)param_2] == 0x29) goto LAB_001037f1;
                        }
                        if (*pbVar22 == 0x29) {
                          param_2 = (FILE *)0x0;
                          pbVar20 = pbVar22;
LAB_001037f1:
                          if ((local_10c == '\0') ||
                             (lVar5 = filename_unescape(pbVar22), lVar5 != 0)) {
                            *pbVar20 = 0;
                            do {
                              do {
                                param_2 = (FILE *)((long)&param_2->_flags + 1);
                                bVar2 = pbVar22[(long)param_2];
                              } while (bVar2 == 0x20);
                            } while (bVar2 == 9);
                            if (bVar2 == 0x3d) {
                              pbVar6 = pbVar22 + 1 + (long)param_2;
                              do {
                                do {
                                  pbVar20 = pbVar6;
                                  pbVar6 = pbVar20 + 1;
                                } while (*pbVar20 == 0x20);
                              } while (*pbVar20 == 9);
                              param_2 = (FILE *)0x0;
                              local_170 = pFVar9;
                              do {
                                pbVar6 = pbVar20 + (long)param_2;
                                pFVar9 = (FILE *)(ulong)*pbVar6;
                                if (uVar23 <= ((ulong)param_2 & 0xffffffff)) {
                                  param_2 = pFVar9;
                                  pFVar9 = local_170;
                                  if (*pbVar6 == 0) goto LAB_00103148;
                                  break;
                                }
                                ppuVar11 = __ctype_b_loc();
                                param_2 = (FILE *)((long)&param_2->_flags + 1);
                                pFVar9 = local_170;
                              } while ((*(byte *)((long)*ppuVar11 + (long)pFVar9 * 2 + 1) & 0x10) !=
                                       0);
                            }
                          }
                        }
                      }
                    }
                  }
                  goto LAB_00102f18;
                }
                if ((ulong)((long)local_170 - lVar5) <
                    (ulong)(*pbVar20 == 0x5c) + min_digest_line_length) goto LAB_00102f18;
                pbVar6 = pbVar20;
                if ((int)local_158 == 9) {
                  digest_hex_bytes = 0;
                  ppuVar11 = __ctype_b_loc();
                  uVar13 = 0;
                  cVar1 = '\0';
                  do {
                    cVar18 = cVar1;
                    uVar23 = uVar13;
                    uVar13 = uVar23 + 1;
                    cVar1 = local_161;
                  } while ((*(byte *)((long)*ppuVar11 + (ulong)pbVar20[uVar23] * 2 + 1) & 0x10) != 0
                          );
                  if (((cVar18 != '\0') && (digest_hex_bytes = uVar23, uVar23 - 2 < 0x7f)) &&
                     ((uVar23 & 1) == 0)) {
                    digest_length = uVar23 * 4;
                    pFVar19 = (FILE *)(lVar5 + uVar23);
                    pbVar22 = (byte *)((long)&pFVar19->_flags + (long)&param_2->_flags);
                    bVar2 = *pbVar22;
                    if ((bVar2 == 0x20) || (bVar2 == 9)) {
                      *pbVar22 = 0;
                      pbVar22 = (byte *)((long)&pFVar19->_flags + 1);
                      goto LAB_001030bb;
                    }
                  }
                  goto LAB_00102f18;
                }
                pFVar19 = (FILE *)(lVar5 + digest_hex_bytes);
                pbVar22 = (byte *)((long)&pFVar19->_flags + (long)&param_2->_flags);
                bVar2 = *pbVar22;
                if ((bVar2 != 0x20) && (bVar2 != 9)) goto LAB_00102f18;
                *pbVar22 = 0;
                pbVar22 = (byte *)((long)&pFVar19->_flags + 1);
                if (uVar23 != 0) {
LAB_001030bb:
                  local_158 = pFVar19;
                  ppuVar11 = __ctype_b_loc();
                  uVar13 = 0;
                  do {
                    if ((*(byte *)((long)*ppuVar11 + (ulong)*pbVar6 * 2 + 1) & 0x10) == 0)
                    goto LAB_00102f18;
                    uVar13 = (ulong)((int)uVar13 + 1);
                    pbVar6 = pbVar6 + 1;
                    pFVar19 = local_158;
                  } while (uVar13 < uVar23);
                }
                if (*pbVar6 != 0) goto LAB_00102f18;
                if (((long)local_170 - (long)pbVar22 == 1) ||
                   ((pbVar22[(long)&param_2->_flags] != 0x20 &&
                    (pbVar22[(long)&param_2->_flags] != 0x2a)))) {
                  if (bsd_reversed == 0) goto LAB_00102f18;
                  bsd_reversed = 1;
                }
                else if (bsd_reversed != 1) {
                  bsd_reversed = 0;
                  pbVar22 = (byte *)((long)&pFVar19->_flags + 2);
                }
                pbVar22 = pbVar22 + (long)&param_2->_flags;
                if ((local_10c != '\0') && (lVar5 = filename_unescape(pbVar22), lVar5 == 0))
                goto LAB_00102f18;
LAB_00103148:
                if ((local_110 == 0) && (iVar3 = strcmp((char *)pbVar22,"-"), iVar3 == 0))
                goto LAB_00102f18;
                bVar24 = false;
                if (status_only == 0) {
                  pcVar21 = strchr((char *)pbVar22,10);
                  bVar24 = pcVar21 != (char *)0x0;
                }
                local_170._0_1_ = digest_file_constprop_0(pbVar22,local_130,&local_e8);
                if ((byte)local_170 == 0) {
                  local_120 = local_120 + 1;
                  if (status_only == 0) {
                    if (bVar24 != false) {
                      pcVar21 = stdout->_IO_write_ptr;
                      if (pcVar21 < stdout->_IO_write_end) {
                        stdout->_IO_write_ptr = pcVar21 + 1;
                        *pcVar21 = '\\';
                      }
                      else {
                        __overflow(stdout,0x5c);
                      }
                    }
                    print_filename(pbVar22,bVar24);
                    pcVar21 = "FAILED open or read";
LAB_001031fc:
                    uVar26 = dcgettext(0,pcVar21,5);
                    __printf_chk(1,": %s\n",uVar26);
                  }
                }
                else if ((ignore_missing == '\0') || (local_e8 == 0)) {
                  pFVar19 = (FILE *)(digest_hex_bytes >> 1);
                  if (pFVar19 == (FILE *)0x0) {
                    param_2 = (FILE *)0x0;
                  }
                  else {
                    param_2 = (FILE *)0x0;
                    local_170 = (FILE *)((ulong)local_170 & 0xffffffffffffff00 |
                                        (ulong)(byte)local_170);
                    local_158 = pFVar19;
                    pp_Var8 = __ctype_tolower_loc();
                    do {
                      pFVar19 = local_158;
                      if (((*pp_Var8)[pbVar20[(long)param_2 * 2]] !=
                           (int)"0123456789abcdef"[(byte)local_130[(long)param_2] >> 4]) ||
                         ((*pp_Var8)[pbVar20[(long)param_2 * 2 + 1]] !=
                          (int)"0123456789abcdef"[(byte)local_130[(long)param_2] & 0xf])) {
                        local_118 = local_118 + 1;
                        if (status_only != 0) goto LAB_00103219;
                        local_170._0_1_ = local_109;
                        if (bVar24 != false) goto LAB_00103922;
                        goto LAB_00103587;
                      }
                      param_2 = (FILE *)((long)&param_2->_flags + 1);
                    } while (local_158 != param_2);
                  }
                  local_109 = status_only;
                  if ((status_only == 0) && (local_109 = quiet, quiet == 0)) {
                    if (bVar24 == false) {
                      local_170 = (FILE *)((ulong)local_170 & 0xffffffffffffff00 |
                                          (ulong)(byte)local_170);
                      print_filename(pbVar22,0);
                      local_109 = (byte)local_170;
                    }
                    else {
LAB_00103922:
                      local_109 = (byte)local_170;
                      pcVar21 = stdout->_IO_write_ptr;
                      if (pcVar21 < stdout->_IO_write_end) {
                        stdout->_IO_write_ptr = pcVar21 + 1;
                        *pcVar21 = '\\';
                      }
                      else {
                        local_170 = pFVar19;
                        __overflow(stdout,0x5c);
                        pFVar19 = local_170;
                      }
LAB_00103587:
                      local_170 = pFVar19;
                      print_filename(pbVar22,bVar24);
                      if (local_170 != param_2) {
                        pcVar21 = "FAILED";
                        goto LAB_001031fc;
                      }
                    }
                    if (quiet == 0) {
                      pcVar21 = "OK";
                      goto LAB_001031fc;
                    }
                  }
                }
LAB_00103219:
                bVar2 = *(byte *)&pFVar9->_flags;
                local_10b = local_161;
              }
              if ((bVar2 & 0x30) != 0) goto LAB_0010322c;
              local_178 = local_178 + 1;
            } while (local_178 != (char *)0x0);
            quotearg_n_style_colon(0,3,local_150);
            uVar26 = dcgettext(0,"%s: too many checksum lines",5);
            error(1,0,uVar26);
            uVar23 = extraout_RDX;
LAB_00102f81:
            digest_length = (ulong)*(int *)(algorithm_bits + (uVar23 & 0xffffffff) * 4);
            digest_hex_bytes = digest_length >> 2;
            if ((uint)uVar23 < 3) {
              if (local_161 == '\0') {
                if (digest_delim == '\n') {
                  if (ignore_missing != '\0') goto LAB_00103b57;
                  if (status_only != 0) {
LAB_00103b9f:
                    pcVar21 = "the --status option is meaningful only when verifying checksums";
                    goto LAB_00103011;
                  }
                  if (warn == '\0') break;
LAB_0010397d:
                  pcVar21 = "the --warn option is meaningful only when verifying checksums";
                }
                else {
LAB_00102c2c:
                  if (ignore_missing == '\0') goto LAB_00102c39;
LAB_00103b57:
                  pcVar21 = 
                  "the --ignore-missing option is meaningful only when verifying checksums";
                }
                goto LAB_00103011;
              }
              if (algorithm_specified != '\0') {
                uVar26 = dcgettext(0,"--check is not supported with --algorithm={bsd,sysv,crc}",5);
                auVar25 = error(1,0,uVar26);
                uVar26 = uStack392;
                uStack392 = SUB168(auVar25,0);
                (*(code *)PTR___libc_start_main_00123fd0)
                          (main,uVar26,&local_180,0,0,SUB168(auVar25 >> 0x40,0),&uStack392);
                do {
                    /* WARNING: Do nothing block with infinite loop */
                } while( true );
              }
              if (digest_delim != '\n') {
LAB_00103c4b:
                pcVar21 = "the --zero option is not supported when verifying checksums";
                goto LAB_00103011;
              }
              if (ignore_missing != '\0') goto LAB_00102fdb;
              if (status_only == 0) goto LAB_0010388b;
LAB_00102c51:
              if (warn == '\0') goto LAB_00103030;
              goto LAB_00102c5e;
            }
LAB_00102c14:
            if (digest_delim != '\n') {
              if (local_161 == '\0') goto LAB_00102c2c;
              goto LAB_00103c4b;
            }
            if (ignore_missing == '\0') {
LAB_00102c39:
              if (status_only != 0) {
                if (local_161 == '\0') goto LAB_00103b9f;
                goto LAB_00102c51;
              }
LAB_0010388b:
              if (warn == '\0') break;
              if (local_161 == '\0') goto LAB_0010397d;
              goto LAB_00102c5e;
            }
            if (local_161 == '\0') goto LAB_00103b57;
LAB_00102fdb:
            if (status_only == 0) goto LAB_0010388b;
          } while (warn != '\0');
        } while ((quiet != 1) ||
                (pcVar21 = "the --quiet option is meaningful only when verifying checksums",
                local_161 != '\0'));
LAB_00103011:
        uVar26 = dcgettext(0,pcVar21,5);
        error(0,0,uVar26);
      } while( true );
    }
    if (iVar3 < 0x61) {
      if (iVar3 == -0x83) {
        version_etc(stdout,"cksum","GNU coreutils",Version,"Padraig Brady","Q. Frank Xia",0,uVar26);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 != -0x82) goto switchD_001029b5_caseD_62;
      usage();
switchD_001029b5_caseD_84:
      local_180 = (size_t *)CONCAT71(local_180._1_7_,1);
      goto LAB_00102970;
    }
    switch(iVar3) {
    case 0x61:
      lVar5 = __xargmatch_internal
                        ("--algorithm",optarg,algorithm_args,algorithm_types,4,argmatch_die,0);
      cksum_algorithm = *(uint *)(algorithm_types + lVar5 * 4);
      algorithm_specified = '\x01';
      break;
    default:
      goto switchD_001029b5_caseD_62;
    case 0x6c:
      uVar26 = dcgettext(0,"invalid length",5);
      digest_length = xdectoumax(optarg,0,0xffffffffffffffff,"",uVar26,0);
      local_178 = optarg;
      if ((digest_length & 7) == 0) break;
      pcVar21 = (char *)quote(optarg);
      uVar26 = dcgettext(0,"invalid length: %s",5);
      error(0,0,uVar26,pcVar21);
      uVar26 = dcgettext(0,"length is not a multiple of 8",5);
      error(1,0,uVar26);
    case 99:
      local_161 = '\x01';
      break;
    case 0x77:
      status_only = 0;
      warn = '\x01';
      quiet = 0;
      break;
    case 0x7a:
      digest_delim = '\0';
      break;
    case 0x80:
      ignore_missing = '\x01';
      break;
    case 0x81:
      status_only = 1;
      warn = '\0';
      quiet = 0;
      break;
    case 0x82:
      status_only = 0;
      warn = '\0';
      quiet = 1;
      break;
    case 0x83:
      strict = 1;
      break;
    case 0x84:
      goto switchD_001029b5_caseD_84;
    case 0x85:
      local_180 = (size_t *)((ulong)local_180 & 0xffffffffffffff00);
      break;
    case 0x86:
      goto switchD_001029b5_caseD_86;
    }
  } while( true );
switchD_001029b5_caseD_86:
  cksum_debug = 1;
  goto LAB_00102970;
LAB_0010322c:
  free(local_f8);
  uVar16 = pFVar9->_flags & 0x20;
  iVar3 = -(uint)(uVar16 == 0);
  if (local_110 == 0) {
    clearerr_unlocked(pFVar9);
LAB_0010325c:
    lVar5 = local_138;
    if (iVar3 < 0) {
      if (local_10b == '\0') {
        uVar26 = quotearg_n_style_colon(0,3,local_150);
        pcVar21 = "%s: no properly formatted checksum lines found";
        goto LAB_00103628;
      }
      if (status_only == 0) {
        if (local_138 != 0) {
          uVar26 = dcngettext(0,"WARNING: %lu line is improperly formatted",
                              "WARNING: %lu lines are improperly formatted",local_138);
          error(0,0,uVar26,lVar5);
        }
        uVar23 = local_120;
        if (local_120 != 0) {
          uVar26 = dcngettext(0,"WARNING: %lu listed file could not be read",
                              "WARNING: %lu listed files could not be read",local_120);
          error(0,0,uVar26,uVar23);
        }
        uVar23 = local_118;
        if (local_118 != 0) {
          uVar26 = dcngettext(0,"WARNING: %lu computed checksum did NOT match",
                              "WARNING: %lu computed checksums did NOT match",local_118);
          error(0,0,uVar26,uVar23);
        }
        if (ignore_missing != '\0') {
          if (local_109 == 0) goto LAB_001039cf;
          goto LAB_001032b9;
        }
      }
      if (local_109 == 0) {
        bVar2 = 0;
        goto LAB_001032e1;
      }
LAB_001032b9:
      if ((local_120 | local_118) != 0) goto LAB_00103640;
      bVar2 = strict ^ 1 | local_138 == 0;
      goto LAB_001032e1;
    }
    uVar26 = quotearg_n_style_colon(0,3,local_150);
    puVar15 = &DAT_0011f104;
    if (iVar3 != 0) goto LAB_00103689;
  }
  else {
    iVar4 = rpl_fclose();
    if (iVar4 == 0) goto LAB_0010325c;
    if (uVar16 == 0) {
      piVar12 = __errno_location();
      iVar3 = *piVar12;
      goto LAB_0010325c;
    }
    uVar26 = quotearg_n_style_colon(0,3,local_150);
  }
  iVar3 = 0;
  puVar15 = (undefined *)dcgettext(0,"%s: read error",5);
LAB_00103689:
  error(0,iVar3,puVar15,uVar26);
  bVar2 = 0;
LAB_001032e4:
  while( true ) {
    bVar2 = local_10a & bVar2;
LAB_00103427:
    local_10a = bVar2;
    local_160 = local_160 + 1;
    if (local_160 < local_128) break;
LAB_0010343d:
    if ((have_read_stdin == '\0') || (iVar3 = rpl_fclose(stdin), iVar3 != -1)) {
      if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
      return local_10a ^ 1;
    }
    uVar26 = dcgettext(0,"standard input",5);
    piVar12 = __errno_location();
    error(1,*piVar12,uVar26);
LAB_001039cf:
    uVar26 = quotearg_n_style_colon(0,3,local_150);
    pcVar21 = "%s: no file was verified";
LAB_00103628:
    uVar10 = dcgettext(0,pcVar21,5);
    error(0,0,uVar10,uVar26);
LAB_00103640:
    bVar2 = 0;
LAB_001032e1:
    bVar2 = bVar2 & 1;
  }
  goto LAB_00102cd0;
}