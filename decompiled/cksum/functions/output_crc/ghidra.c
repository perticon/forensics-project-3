void output_crc(undefined8 param_1,undefined8 param_2,undefined4 *param_3,undefined8 param_4,
               byte param_5,char param_6,undefined8 param_7)

{
  byte *pbVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined auStack72 [24];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = umaxtostr(param_7,auStack72);
  __printf_chk(1,"%u %s",*param_3,uVar2);
  if (param_6 != '\0') {
    __printf_chk(1,&DAT_0011f103,param_1);
  }
  pbVar1 = (byte *)stdout->_IO_write_ptr;
  if (pbVar1 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = (char *)(pbVar1 + 1);
    *pbVar1 = param_5;
  }
  else {
    __overflow(stdout,(uint)param_5);
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}