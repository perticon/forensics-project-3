output_crc (char const *file, int binary_file, void const *digest,
            bool tagged, unsigned char delim, bool args, uintmax_t length)
{
  char length_buf[INT_BUFSIZE_BOUND (uintmax_t)];
  printf ("%u %s", *(unsigned int *)digest, umaxtostr (length, length_buf));
  if (args)
    printf (" %s", file);
  putchar (delim);
}