undefined8 crc_sum_stream(undefined8 param_1,uint *param_2,ulong *param_3)

{
  int *piVar1;
  long lVar2;
  char cVar3;
  undefined8 uVar4;
  uint uVar5;
  ulong uVar6;
  ulong uVar7;
  char *pcVar8;
  long in_FS_OFFSET;
  ulong local_40;
  ulong local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  local_40 = 0;
  local_38 = 0;
  if (cksum_fp != (code *)0x0) goto LAB_00106d49;
  piVar1 = (int *)cpuid_basic_info(0);
  uVar5 = piVar1[3];
  if (*piVar1 == 0) {
    if (cksum_debug != '\0') {
      pcVar8 = "failed to get cpuid";
LAB_00106e13:
      uVar4 = dcgettext(0,pcVar8,5,uVar5);
      error(0,0,&DAT_0011f104,uVar4);
    }
  }
  else {
    lVar2 = cpuid_Version_info(1);
    uVar5 = *(uint *)(lVar2 + 0xc) & 0x10000002;
    if (uVar5 == 0x10000002) {
      if (cksum_debug != '\0') {
        uVar4 = dcgettext(0,"using pclmul hardware support",5);
        error(0,0,&DAT_0011f104,uVar4);
      }
      cksum_fp = cksum_pclmul;
      goto LAB_00106d49;
    }
    if (cksum_debug != '\0') {
      pcVar8 = "pclmul support not detected";
      goto LAB_00106e13;
    }
  }
  cksum_fp = cksum_slice8;
LAB_00106d49:
  cVar3 = (*cksum_fp)(param_1,&local_38,&local_40);
  if (cVar3 == '\0') {
    uVar4 = 0xffffffff;
  }
  else {
    *param_3 = local_40;
    uVar7 = local_38;
    for (uVar6 = local_40; uVar6 != 0; uVar6 = uVar6 >> 8) {
      uVar7 = uVar7 << 8 ^
              *(ulong *)(crctab + (ulong)(byte)((byte)(uVar7 >> 0x18) ^ (byte)uVar6) * 8);
    }
    uVar4 = 0;
    *param_2 = ~(uint)uVar7;
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar4;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}