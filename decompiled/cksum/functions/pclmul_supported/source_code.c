pclmul_supported (void)
{
# if USE_PCLMUL_CRC32
  unsigned int eax = 0;
  unsigned int ebx = 0;
  unsigned int ecx = 0;
  unsigned int edx = 0;

  if (! __get_cpuid (1, &eax, &ebx, &ecx, &edx))
    {
      if (cksum_debug)
        error (0, 0, "%s", _("failed to get cpuid"));
      return false;
    }

  if (! (ecx & bit_PCLMUL) || ! (ecx & bit_AVX))
    {
      if (cksum_debug)
        error (0, 0, "%s", _("pclmul support not detected"));
      return false;
    }

  if (cksum_debug)
    error (0, 0, "%s", _("using pclmul hardware support"));

  return true;
# else
  if (cksum_debug)
    error (0, 0, "%s", _("using generic hardware support"));
  return false;
# endif /* USE_PCLMUL_CRC32 */
}