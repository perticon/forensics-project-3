compare_occurs (const void *void_first, const void *void_second)
{
#define first ((const OCCURS *) void_first)
#define second ((const OCCURS *) void_second)
  int value;

  value = compare_words (&first->key, &second->key);
  return (value ? value
          : ((first->key.start > second->key.start)
             - (first->key.start < second->key.start)));
#undef first
#undef second
}