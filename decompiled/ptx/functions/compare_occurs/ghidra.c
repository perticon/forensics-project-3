ulong compare_occurs(ulong *param_1,ulong *param_2)

{
  ulong uVar1;
  
  uVar1 = compare_words();
  if ((int)uVar1 == 0) {
    uVar1 = *param_2;
    uVar1 = (ulong)((uint)(*param_1 >= uVar1 && *param_1 != uVar1) - (uint)(*param_1 < uVar1));
  }
  return uVar1;
}