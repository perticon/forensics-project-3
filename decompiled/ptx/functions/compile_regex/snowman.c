void compile_regex(struct s0* rdi) {
    int1_t zf2;
    void** rbp3;
    int64_t rax4;
    void** rax5;
    void** rax6;
    void** rax7;
    void** r8_8;
    void** rcx9;
    void** rbx10;
    void** rsp11;
    void** rax12;
    int1_t zf13;
    int1_t zf14;
    int1_t zf15;
    void** rdi16;
    void** rdx17;
    void** rsi18;
    void** rax19;
    void** rsp20;
    void** rdi21;
    void** rax22;
    void* v23;
    void* rax24;
    int64_t rbp25;
    void** rax26;
    uint32_t* rax27;
    void** r13_28;
    void** r14_29;
    void** rax30;
    void** v31;
    void** r9_32;
    void** r12_33;
    void** v34;
    void** rbp35;
    void** v36;
    void** rdi37;
    void** rsi38;
    void* rax39;
    void** rax40;
    void** rdx41;
    struct s1* rdx42;
    int64_t v43;
    int64_t v44;
    int64_t r15_45;
    void** v46;
    void** r14_47;
    void** v48;
    void** v49;
    void** v50;
    void** rbp51;
    void** v52;
    void** rdx53;
    int64_t rax54;
    uint32_t ebx55;
    void** r9_56;
    int32_t eax57;
    void** r9_58;
    void** v59;
    void** v60;
    void** v61;
    void** v62;
    void** v63;
    int64_t v64;
    void** rdi65;
    void** rax66;
    void** rdi67;
    void** rax68;
    int32_t eax69;
    void** rdi70;
    void** rax71;
    void** rdi72;
    void** rax73;
    void** rdi74;
    void** rax75;
    int64_t v76;

    zf2 = ignore_case == 0;
    rbp3 = rdi->f0;
    rdi->f28 = reinterpret_cast<int64_t>(rdi) + 72;
    rax4 = 0x234c0;
    if (zf2) {
        rax4 = 0;
    }
    *reinterpret_cast<void***>(&rdi->f8) = reinterpret_cast<void**>(0);
    rdi->f10 = 0;
    rdi->f30 = rax4;
    rax5 = fun_3700(rbp3);
    rax6 = rpl_re_compile_pattern(rbp3, rax5, &rdi->f8);
    if (!rax6) {
    }
    rax7 = quote(rbp3, rbp3);
    fun_36e0();
    r8_8 = rax7;
    rcx9 = rax6;
    fun_3990();
    rbx10 = reinterpret_cast<void**>(0);
    rsp11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 24);
    rax12 = g28;
    if (0) 
        goto addr_6877_8;
    zf13 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 1) == 0;
    if (zf13) 
        goto addr_6877_8;
    zf14 = *reinterpret_cast<unsigned char*>(&__cxa_finalize + 1) == 45;
    if (zf14) {
        zf15 = *reinterpret_cast<signed char*>(&__cxa_finalize + 2) == 0;
        if (zf15) {
            addr_6877_8:
            rdi16 = stdin;
            rdx17 = rsp11;
            *reinterpret_cast<uint32_t*>(&rsi18) = 0;
            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
            rax19 = fread_file(rdi16, 0, rdx17, rdi16, 0, rdx17);
            rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp11 - 8) + 8);
            __cxa_finalize = rax19;
            if (rax19) {
                rdi21 = stdin;
                fun_3680(rdi21);
                rsp20 = rsp20 - 1 + 1;
                rax22 = __cxa_finalize;
                goto addr_689f_14;
            }
        } else {
            goto addr_68c5_16;
        }
    } else {
        addr_68c5_16:
        rdx17 = rsp11;
        *reinterpret_cast<uint32_t*>(&rsi18) = 0;
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        rdi21 = reinterpret_cast<void**>(1);
        rax22 = read_file(1);
        rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp11 - 8) + 8);
        __cxa_finalize = rax22;
        if (rax22) {
            addr_689f_14:
            g8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax22) + reinterpret_cast<uint64_t>(v23));
            rax24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) - reinterpret_cast<unsigned char>(g28));
            if (!rax24) {
                goto rbp25;
            }
        }
    }
    rax26 = quotearg_n_style_colon();
    rax27 = fun_35d0();
    rcx9 = rax26;
    *reinterpret_cast<int32_t*>(&rdi21) = 1;
    *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
    rdx17 = reinterpret_cast<void**>("%s");
    *reinterpret_cast<uint32_t*>(&rsi18) = *rax27;
    *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
    fun_3990();
    rsp20 = rsp20 - 1 + 1 - 1 + 1 - 1 + 1;
    fun_3710();
    r13_28 = rsi18;
    r14_29 = r13_28 + 8;
    rax30 = g28;
    v31 = rax30;
    swallow_file_in_memory(rdi21, rsp20 - 1 + 1 - 1 - 1 - 1 - 1 - 1 - 4, rdx17, rcx9, r8_8, r9_32);
    r12_33 = v34;
    rbp35 = v36;
    *reinterpret_cast<int32_t*>(&rdi37) = 0;
    *reinterpret_cast<int32_t*>(&rdi37 + 4) = 0;
    *reinterpret_cast<void***>(r13_28) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi38) = 0;
    *reinterpret_cast<int32_t*>(&rsi38 + 4) = 0;
    *reinterpret_cast<void***>(r13_28 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_28 + 16) = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(r12_33) >= reinterpret_cast<unsigned char>(rbp35)) {
        addr_69cd_22:
        rax39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v31) - reinterpret_cast<unsigned char>(g28));
        if (!rax39) {
        }
    } else {
        do {
            rbx10 = r12_33;
            do {
                if (*reinterpret_cast<void***>(rbx10) == 10) 
                    break;
                ++rbx10;
            } while (reinterpret_cast<unsigned char>(rbx10) < reinterpret_cast<unsigned char>(rbp35));
            if (reinterpret_cast<unsigned char>(rbx10) > reinterpret_cast<unsigned char>(r12_33)) {
                if (*reinterpret_cast<void***>(r13_28 + 8) == rsi38) {
                    rax40 = x2nrealloc();
                    rsi38 = *reinterpret_cast<void***>(r13_28 + 16);
                    *reinterpret_cast<void***>(r13_28) = rax40;
                    rdi37 = rax40;
                }
                rdx41 = rsi38;
                ++rsi38;
                rdx42 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx41) << 4) + reinterpret_cast<unsigned char>(rdi37));
                rdx42->f0 = r12_33;
                rdx42->f8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx10) - reinterpret_cast<unsigned char>(r12_33));
                *reinterpret_cast<void***>(r13_28 + 16) = rsi38;
            }
            if (reinterpret_cast<unsigned char>(rbx10) >= reinterpret_cast<unsigned char>(rbp35)) 
                goto addr_69cd_22;
            r12_33 = rbx10 + 1;
        } while (reinterpret_cast<unsigned char>(r12_33) < reinterpret_cast<unsigned char>(rbp35));
        goto addr_6a0d_35;
    }
    fun_3710();
    if (reinterpret_cast<unsigned char>(rsi38) > reinterpret_cast<unsigned char>(rdi37)) 
        goto addr_6a49_38;
    goto v43;
    addr_6a49_38:
    v44 = r15_45;
    v46 = r14_29;
    r14_47 = rsi38;
    v48 = r13_28;
    v49 = r12_33;
    v50 = rbp35;
    rbp51 = rdi37;
    v52 = rbx10;
    do {
        addr_6ad9_40:
        *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp51));
        *reinterpret_cast<int32_t*>(&rdx53 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax54) = *reinterpret_cast<unsigned char*>(&rdx53);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        ebx55 = *reinterpret_cast<uint32_t*>(&rdx53);
        if (*reinterpret_cast<signed char*>(0x231e0 + rax54)) {
            if (*reinterpret_cast<unsigned char*>(&rdx53) == 92) {
                rcx9 = stdout;
                fun_3900("\\backslash{}", 1, 12, rcx9, r8_8, r9_56);
            } else {
                if (*reinterpret_cast<signed char*>(&rdx53) > reinterpret_cast<signed char>(92)) {
                    eax57 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx53 + 0xffffffffffffff85));
                    if (!(*reinterpret_cast<unsigned char*>(&eax57) & 0xfd)) {
                        fun_3930(1, "$\\%c$", rdx53, rcx9, r8_8, r9_58, v59, v60, v61, v52, v50, v49, v48, v46, v44, v62, v63, v64, v31, 0);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&rdx53) == 95) {
                            addr_6b27_47:
                            rdi65 = stdout;
                            rax66 = *reinterpret_cast<void***>(rdi65 + 40);
                            if (reinterpret_cast<unsigned char>(rax66) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi65 + 48))) {
                                *reinterpret_cast<uint32_t*>(&v60 + 4) = *reinterpret_cast<uint32_t*>(&rdx53);
                                fun_3770();
                                goto addr_6ae9_49;
                            } else {
                                rcx9 = rax66 + 1;
                                *reinterpret_cast<void***>(rdi65 + 40) = rcx9;
                                *reinterpret_cast<void***>(rax66) = reinterpret_cast<void**>(92);
                                goto addr_6ae9_49;
                            }
                        } else {
                            addr_6b5c_51:
                            rdi67 = stdout;
                            rax68 = *reinterpret_cast<void***>(rdi67 + 40);
                            if (reinterpret_cast<unsigned char>(rax68) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi67 + 48))) {
                                fun_3770();
                            } else {
                                *reinterpret_cast<void***>(rdi67 + 40) = rax68 + 1;
                                *reinterpret_cast<void***>(rax68) = reinterpret_cast<void**>(32);
                            }
                        }
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rdx53) != 34) {
                        eax69 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx53 + 0xffffffffffffffdd));
                        if (*reinterpret_cast<unsigned char*>(&eax69) > 3) 
                            goto addr_6b5c_51; else 
                            goto addr_6b27_47;
                    } else {
                        rdi70 = stdout;
                        rax71 = *reinterpret_cast<void***>(rdi70 + 40);
                        if (reinterpret_cast<unsigned char>(rax71) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi70 + 48))) {
                            fun_3770();
                        } else {
                            *reinterpret_cast<void***>(rdi70 + 40) = rax71 + 1;
                            *reinterpret_cast<void***>(rax71) = reinterpret_cast<void**>(34);
                        }
                        rdi72 = stdout;
                        rax73 = *reinterpret_cast<void***>(rdi72 + 40);
                        if (reinterpret_cast<unsigned char>(rax73) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
                            fun_3770();
                        } else {
                            *reinterpret_cast<void***>(rdi72 + 40) = rax73 + 1;
                            *reinterpret_cast<void***>(rax73) = reinterpret_cast<void**>(34);
                        }
                    }
                }
            }
        } else {
            addr_6ae9_49:
            rdi74 = stdout;
            rax75 = *reinterpret_cast<void***>(rdi74 + 40);
            if (reinterpret_cast<unsigned char>(rax75) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi74 + 48))) {
                fun_3770();
            } else {
                ++rbp51;
                *reinterpret_cast<void***>(rdi74 + 40) = rax75 + 1;
                *reinterpret_cast<void***>(rax75) = *reinterpret_cast<void***>(&ebx55);
                if (r14_47 != rbp51) 
                    goto addr_6ad9_40; else 
                    break;
            }
        }
        ++rbp51;
    } while (r14_47 != rbp51);
    goto v76;
    addr_6a0d_35:
    goto addr_69cd_22;
}