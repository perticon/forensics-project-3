void compile_regex(char **param_1)

{
  undefined1 *puVar1;
  size_t sVar2;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  int *piVar6;
  long *plVar7;
  char *pcVar8;
  long in_FS_OFFSET;
  bool bVar9;
  long lStack64;
  long lStack56;
  
  bVar9 = ignore_case == '\0';
  pcVar8 = *param_1;
  param_1[5] = (char *)(param_1 + 9);
  puVar1 = folded_chars;
  if (bVar9) {
    puVar1 = (char *)0x0;
  }
  param_1[1] = (char *)0x0;
  param_1[2] = (char *)0x0;
  param_1[6] = puVar1;
  sVar2 = strlen(pcVar8);
  lVar3 = rpl_re_compile_pattern(pcVar8,sVar2,param_1 + 1);
  if (lVar3 == 0) {
    rpl_re_compile_fastmap(param_1 + 1);
    return;
  }
  uVar4 = quote(pcVar8);
  uVar5 = dcgettext(0,"%s (for regexp %s)",5);
  plVar7 = (long *)0x0;
  pcVar8 = (char *)0x1;
  error(1,0,uVar5,lVar3,uVar4);
  lStack56 = *(long *)(in_FS_OFFSET + 0x28);
  if (((pcVar8 == (char *)0x0) || (*pcVar8 == '\0')) || ((*pcVar8 == '-' && (pcVar8[1] == '\0')))) {
    lVar3 = fread_file(stdin,0,&lStack64);
    *plVar7 = lVar3;
    if (lVar3 == 0) {
      pcVar8 = "-";
      goto LAB_001068f7;
    }
    clearerr_unlocked(stdin);
    lVar3 = *plVar7;
  }
  else {
    lVar3 = read_file(pcVar8,0,&lStack64);
    *plVar7 = lVar3;
    if (lVar3 == 0) {
LAB_001068f7:
      uVar4 = quotearg_n_style_colon(0,3,pcVar8);
      piVar6 = __errno_location();
      error(1,*piVar6,&DAT_0011cd8d,uVar4);
      goto LAB_00106926;
    }
  }
  plVar7[1] = lVar3 + lStack64;
  if (lStack56 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00106926:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}