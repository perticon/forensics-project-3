void compile_regex(int32_t * regex) {
    int64_t v1 = (int64_t)regex;
    int64_t v2 = v1 + 8; // 0x67c4
    *(int64_t *)(v1 + 40) = v1 + 72;
    int64_t v3 = *(char *)&ignore_case == 0 ? 0 : (int64_t)&folded_chars; // 0x67d7
    *(int64_t *)v2 = 0;
    *(int64_t *)(v1 + 16) = 0;
    *(int64_t *)(v1 + 48) = v3;
    int64_t v4 = function_3700(); // 0x67f2
    int32_t * v5 = (int32_t *)v2; // 0x6800
    if (rpl_re_compile_pattern((char *)regex, v4, v5) == NULL) {
        // 0x680a
        rpl_re_compile_fastmap(v5);
        return;
    }
    // 0x6817
    quote((char *)regex);
    function_36e0();
    function_3990();
}