void digest_word_file(char * file_name2, int32_t * table) {
    int64_t v1 = (int64_t)table;
    int64_t v2 = __readfsqword(40); // 0x6943
    char * v3; // bp-72, 0x6930
    swallow_file_in_memory(file_name2, (int32_t *)&v3);
    int64_t v4 = (int64_t)v3; // 0x695b
    *(int64_t *)table = 0;
    int64_t * v5 = (int64_t *)(v1 + 8); // 0x6970
    *v5 = 0;
    int64_t * v6 = (int64_t *)(v1 + 16); // 0x6978
    *v6 = 0;
    int64_t v7; // 0x6930
    if (v7 > v4) {
        int64_t v8 = 0;
        int64_t v9 = 0;
        int64_t v10 = v4;
        int64_t v11 = v10; // 0x6993
        int64_t v12; // 0x6995
        while (*(char *)v10 != 10) {
            // 0x6995
            v12 = v10 + 1;
            v11 = v12;
            if (v12 >= v7) {
                // break -> 0x699e
                break;
            }
            v10 = v12;
            v11 = v10;
        }
        uint64_t v13 = v11;
        int64_t v14 = v9; // 0x69a1
        int64_t v15 = v8; // 0x69a1
        int64_t v16; // 0x6a18
        int64_t v17; // 0x6a1d
        int64_t v18; // 0x69ba
        if (v13 > v4) {
            // 0x69a3
            v17 = v9;
            v16 = v8;
            if (*v5 == v9) {
                // 0x6a10
                v16 = x2nrealloc();
                v17 = *v6;
                *(int64_t *)table = v16;
            }
            // 0x69a9
            v15 = v16;
            v14 = v17 + 1;
            v18 = 16 * v17 + v15;
            *(int64_t *)v18 = v4;
            *(int64_t *)(v18 + 8) = v13 - v4;
            *v6 = v14;
        }
        while (v13 < v7) {
            int64_t v19 = v13 + 1; // 0x6a00
            if (v19 >= v7) {
                // break -> 0x69cd
                break;
            }
            int64_t v20 = v19;
            v8 = v15;
            v9 = v14;
            v10 = v20;
            v11 = v10;
            while (*(char *)v10 != 10) {
                // 0x6995
                v12 = v10 + 1;
                v11 = v12;
                if (v12 >= v7) {
                    // break -> 0x699e
                    break;
                }
                v10 = v12;
                v11 = v10;
            }
            // 0x699e
            v13 = v11;
            v14 = v9;
            v15 = v8;
            if (v13 > v20) {
                // 0x69a3
                v17 = v9;
                v16 = v8;
                if (*v5 == v9) {
                    // 0x6a10
                    v16 = x2nrealloc();
                    v17 = *v6;
                    *(int64_t *)table = v16;
                }
                // 0x69a9
                v15 = v16;
                v14 = v17 + 1;
                v18 = 16 * v17 + v15;
                *(int64_t *)v18 = v20;
                *(int64_t *)(v18 + 8) = v13 - v20;
                *v6 = v14;
            }
        }
    }
    // 0x69cd
    if (v2 != __readfsqword(40)) {
        // 0x6a2d
        function_3710();
        return;
    }
    // 0x69dd
    function_3640();
}