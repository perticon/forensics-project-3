search_table (WORD *word, WORD_TABLE *table)
{
  ptrdiff_t lowest;		/* current lowest possible index */
  ptrdiff_t highest;		/* current highest possible index */
  ptrdiff_t middle;		/* current middle index */
  int value;			/* value from last comparison */

  lowest = 0;
  highest = table->length - 1;
  while (lowest <= highest)
    {
      middle = (lowest + highest) / 2;
      value = compare_words (word, table->start + middle);
      if (value < 0)
        highest = middle - 1;
      else if (value > 0)
        lowest = middle + 1;
      else
        return true;
    }
  return false;
}