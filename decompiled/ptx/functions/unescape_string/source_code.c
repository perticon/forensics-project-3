unescape_string (char *string)
{
  char *cursor;			/* cursor in result */
  int value;			/* value of \nnn escape */
  int length;			/* length of \nnn escape */

  cursor = string;

  while (*string)
    {
      if (*string == '\\')
        {
          string++;
          switch (*string)
            {
            case 'x':		/* \xhhh escape, 3 chars maximum */
              value = 0;
              for (length = 0, string++;
                   length < 3 && isxdigit (to_uchar (*string));
                   length++, string++)
                value = value * 16 + HEXTOBIN (*string);
              if (length == 0)
                {
                  *cursor++ = '\\';
                  *cursor++ = 'x';
                }
              else
                *cursor++ = value;
              break;

            case '0':		/* \0ooo escape, 3 chars maximum */
              value = 0;
              for (length = 0, string++;
                   length < 3 && ISODIGIT (*string);
                   length++, string++)
                value = value * 8 + OCTTOBIN (*string);
              *cursor++ = value;
              break;

            case 'a':		/* alert */
#if __STDC__
              *cursor++ = '\a';
#else
              *cursor++ = 7;
#endif
              string++;
              break;

            case 'b':		/* backspace */
              *cursor++ = '\b';
              string++;
              break;

            case 'c':		/* cancel the rest of the output */
              while (*string)
                string++;
              break;

            case 'f':		/* form feed */
              *cursor++ = '\f';
              string++;
              break;

            case 'n':		/* new line */
              *cursor++ = '\n';
              string++;
              break;

            case 'r':		/* carriage return */
              *cursor++ = '\r';
              string++;
              break;

            case 't':		/* horizontal tab */
              *cursor++ = '\t';
              string++;
              break;

            case 'v':		/* vertical tab */
#if __STDC__
              *cursor++ = '\v';
#else
              *cursor++ = 11;
#endif
              string++;
              break;

            case '\0':		/* lone backslash at end of string */
              /* ignore it */
              break;

            default:
              *cursor++ = '\\';
              *cursor++ = *string++;
              break;
            }
        }
      else
        *cursor++ = *string++;
    }

  *cursor = '\0';
}