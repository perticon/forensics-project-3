void unescape_string(char * string) {
    int64_t v1 = (int64_t)string;
    int64_t v2; // 0x65d0
    char v3 = v2;
    char v4 = v3; // 0x65df
    int64_t v5 = v1; // 0x65df
    int64_t v6 = v1; // 0x65df
    if (v3 == 0) {
        // 0x6639
        *string = 0;
        return;
    }
    char * v7; // 0x65d0
    while (true) {
        int64_t v8 = v6;
        int64_t v9 = v5;
        char v10 = v4;
        int64_t v11; // 0x65d0
        int64_t v12; // 0x65d0
        if (v10 != 92) {
            // 0x65f0
            *(char *)v8 = v10;
            v11 = v9 + 1;
            v12 = v8 + 1;
        } else {
            char * v13 = (char *)v8;
            char * v14 = (char *)(v9 + 1); // 0x660a
            char v15 = *v14; // 0x660a
            v7 = v13;
            if (v15 == 0) {
                // break -> 0x6639
                break;
            }
            if (v15 < 121) {
                // 0x6619
                return;
            }
            // 0x6648
            *v13 = 92;
            *(char *)(v8 + 1) = *v14;
            v11 = v9 + 2;
            v12 = v8 + 2;
        }
        // 0x65fe
        v6 = v12;
        v5 = v11;
        v4 = *(char *)v5;
        if (v4 == 0) {
            // 0x6639
            v7 = (char *)v6;
            goto lab_0x6639_2;
        }
    }
  lab_0x6639_2:
    // 0x6639
    *v7 = 0;
}