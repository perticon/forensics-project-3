void unescape_string(void** rdi) {
    void** rbp2;
    uint32_t eax3;
    void** rbx4;
    uint32_t eax5;
    uint32_t eax6;
    uint32_t eax7;
    int64_t rax8;

    rbp2 = rdi;
    eax3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    rbx4 = rdi;
    if (!*reinterpret_cast<void***>(&eax3)) {
        addr_6639_2:
        *reinterpret_cast<void***>(rbp2) = reinterpret_cast<void**>(0);
        return;
    } else {
        do {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax3) == 92)) {
                *reinterpret_cast<void***>(rbp2) = *reinterpret_cast<void***>(&eax3);
                ++rbp2;
                eax3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 1));
                ++rbx4;
            } else {
                eax5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 1));
                if (!*reinterpret_cast<signed char*>(&eax5)) 
                    goto addr_6639_2;
                eax6 = eax5 - 48;
                if (*reinterpret_cast<unsigned char*>(&eax6) <= 72) 
                    break;
                *reinterpret_cast<void***>(rbp2) = reinterpret_cast<void**>(92);
                eax7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4 + 1));
                rbp2 = rbp2 + 2;
                rbx4 = rbx4 + 2;
                *reinterpret_cast<void***>(rbp2 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax7);
                eax3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx4));
            }
        } while (*reinterpret_cast<void***>(&eax3));
        goto addr_6639_2;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax6);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1c7f0 + rax8 * 4) + 0x1c7f0;
}