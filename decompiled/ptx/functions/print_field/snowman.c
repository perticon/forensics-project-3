void print_field(void** rdi, void** rsi, ...) {
    void** v3;
    int64_t v4;
    int64_t r15_5;
    void** v6;
    void** r14_7;
    void** r14_8;
    void** v9;
    void** r13_10;
    void** v11;
    void** r12_12;
    void** v13;
    void** rbp14;
    void** rbp15;
    void** v16;
    void** rbx17;
    void** rdx18;
    int64_t rax19;
    uint32_t ebx20;
    void** rcx21;
    void** r8_22;
    void** r9_23;
    int32_t eax24;
    void** r8_25;
    void** r9_26;
    void** v27;
    void** v28;
    void** v29;
    void** v30;
    int64_t v31;
    void** v32;
    void** v33;
    void** rdi34;
    void** rax35;
    void** rdi36;
    void** rax37;
    int32_t eax38;
    void** rdi39;
    void** rax40;
    void** rdi41;
    void** rax42;
    void** rdi43;
    void** rax44;

    v3 = reinterpret_cast<void**>(__return_address());
    if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(rdi)) {
        return;
    } else {
        v4 = r15_5;
        v6 = r14_7;
        r14_8 = rsi;
        v9 = r13_10;
        v11 = r12_12;
        v13 = rbp14;
        rbp15 = rdi;
        v16 = rbx17;
        do {
            addr_6ad9_4:
            *reinterpret_cast<uint32_t*>(&rdx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp15));
            *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax19) = *reinterpret_cast<unsigned char*>(&rdx18);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
            ebx20 = *reinterpret_cast<uint32_t*>(&rdx18);
            if (*reinterpret_cast<signed char*>(0x231e0 + rax19)) {
                if (*reinterpret_cast<unsigned char*>(&rdx18) == 92) {
                    rcx21 = stdout;
                    fun_3900("\\backslash{}", 1, 12, rcx21, r8_22, r9_23);
                } else {
                    if (*reinterpret_cast<signed char*>(&rdx18) > reinterpret_cast<signed char>(92)) {
                        eax24 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx18 + 0xffffffffffffff85));
                        if (!(*reinterpret_cast<unsigned char*>(&eax24) & 0xfd)) {
                            fun_3930(1, "$\\%c$", rdx18, rcx21, r8_25, r9_26, v27, v28, v29, v16, v13, v11, v9, v6, v4, v3, v30, v31, v32, v33);
                        } else {
                            if (*reinterpret_cast<unsigned char*>(&rdx18) == 95) {
                                addr_6b27_11:
                                rdi34 = stdout;
                                rax35 = *reinterpret_cast<void***>(rdi34 + 40);
                                if (reinterpret_cast<unsigned char>(rax35) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi34 + 48))) {
                                    *reinterpret_cast<uint32_t*>(&v28 + 4) = *reinterpret_cast<uint32_t*>(&rdx18);
                                    fun_3770();
                                    goto addr_6ae9_13;
                                } else {
                                    rcx21 = rax35 + 1;
                                    *reinterpret_cast<void***>(rdi34 + 40) = rcx21;
                                    *reinterpret_cast<void***>(rax35) = reinterpret_cast<void**>(92);
                                    goto addr_6ae9_13;
                                }
                            } else {
                                addr_6b5c_15:
                                rdi36 = stdout;
                                rax37 = *reinterpret_cast<void***>(rdi36 + 40);
                                if (reinterpret_cast<unsigned char>(rax37) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi36 + 48))) {
                                    fun_3770();
                                } else {
                                    *reinterpret_cast<void***>(rdi36 + 40) = rax37 + 1;
                                    *reinterpret_cast<void***>(rax37) = reinterpret_cast<void**>(32);
                                }
                            }
                        }
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&rdx18) != 34) {
                            eax38 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx18 + 0xffffffffffffffdd));
                            if (*reinterpret_cast<unsigned char*>(&eax38) > 3) 
                                goto addr_6b5c_15; else 
                                goto addr_6b27_11;
                        } else {
                            rdi39 = stdout;
                            rax40 = *reinterpret_cast<void***>(rdi39 + 40);
                            if (reinterpret_cast<unsigned char>(rax40) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi39 + 48))) {
                                fun_3770();
                            } else {
                                *reinterpret_cast<void***>(rdi39 + 40) = rax40 + 1;
                                *reinterpret_cast<void***>(rax40) = reinterpret_cast<void**>(34);
                            }
                            rdi41 = stdout;
                            rax42 = *reinterpret_cast<void***>(rdi41 + 40);
                            if (reinterpret_cast<unsigned char>(rax42) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi41 + 48))) {
                                fun_3770();
                            } else {
                                *reinterpret_cast<void***>(rdi41 + 40) = rax42 + 1;
                                *reinterpret_cast<void***>(rax42) = reinterpret_cast<void**>(34);
                            }
                        }
                    }
                }
            } else {
                addr_6ae9_13:
                rdi43 = stdout;
                rax44 = *reinterpret_cast<void***>(rdi43 + 40);
                if (reinterpret_cast<unsigned char>(rax44) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi43 + 48))) {
                    fun_3770();
                } else {
                    ++rbp15;
                    *reinterpret_cast<void***>(rdi43 + 40) = rax44 + 1;
                    *reinterpret_cast<void***>(rax44) = *reinterpret_cast<void***>(&ebx20);
                    if (r14_8 != rbp15) 
                        goto addr_6ad9_4; else 
                        break;
                }
            }
            ++rbp15;
        } while (r14_8 != rbp15);
        return;
    }
}