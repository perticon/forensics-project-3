print_field (BLOCK field)
{
  char *cursor;			/* Cursor in field to print */

  /* Whitespace is not really compressed.  Instead, each white space
     character (tab, vt, ht etc.) is printed as one single space.  */

  for (cursor = field.start; cursor < field.end; cursor++)
    {
      unsigned char character = *cursor;
      if (edited_flag[character])
        {
          /* Handle cases which are specific to 'roff' or TeX.  All
             white space processing is done as the default case of
             this switch.  */

          switch (character)
            {
            case '"':
              /* In roff output format, double any quote.  */
              putchar ('"');
              putchar ('"');
              break;

            case '$':
            case '%':
            case '&':
            case '#':
            case '_':
              /* In TeX output format, precede these with a backslash.  */
              putchar ('\\');
              putchar (character);
              break;

            case '{':
            case '}':
              /* In TeX output format, precede these with a backslash and
                 force mathematical mode.  */
              printf ("$\\%c$", character);
              break;

            case '\\':
              /* In TeX output mode, request production of a backslash.  */
              fputs ("\\backslash{}", stdout);
              break;

            default:
              /* Any other flagged character produces a single space.  */
              putchar (' ');
            }
        }
      else
        putchar (*cursor);
    }
}