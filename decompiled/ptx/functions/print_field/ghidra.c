void print_field(byte *param_1,byte *param_2)

{
  byte bVar1;
  char *pcVar2;
  byte *pbVar3;
  
  if (param_2 <= param_1) {
    return;
  }
  do {
    bVar1 = *param_1;
    if (edited_flag[bVar1] == '\0') {
LAB_00106ae9:
      pbVar3 = (byte *)stdout->_IO_write_ptr;
      if (pbVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = (char *)(pbVar3 + 1);
        *pbVar3 = bVar1;
      }
      else {
        __overflow(stdout,(uint)bVar1);
      }
    }
    else if (bVar1 == 0x5c) {
      fwrite_unlocked("\\backslash{}",1,0xc,stdout);
    }
    else if ((char)bVar1 < ']') {
      if (bVar1 == 0x22) {
        pcVar2 = stdout->_IO_write_ptr;
        if (pcVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar2 + 1;
          *pcVar2 = '\"';
        }
        else {
          __overflow(stdout,0x22);
        }
        pcVar2 = stdout->_IO_write_ptr;
        if (pcVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar2 + 1;
          *pcVar2 = '\"';
        }
        else {
          __overflow(stdout,0x22);
        }
      }
      else {
        if ((byte)(bVar1 - 0x23) < 4) {
LAB_00106b27:
          pcVar2 = stdout->_IO_write_ptr;
          if (pcVar2 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar2 + 1;
            *pcVar2 = '\\';
          }
          else {
            __overflow(stdout,0x5c);
          }
          goto LAB_00106ae9;
        }
LAB_00106b5c:
        pcVar2 = stdout->_IO_write_ptr;
        if (pcVar2 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar2 + 1;
          *pcVar2 = ' ';
        }
        else {
          __overflow(stdout,0x20);
        }
      }
    }
    else {
      if ((bVar1 + 0x85 & 0xfd) != 0) {
        if (bVar1 == 0x5f) goto LAB_00106b27;
        goto LAB_00106b5c;
      }
      __printf_chk(1);
    }
    param_1 = param_1 + 1;
    if (param_2 == param_1) {
      return;
    }
  } while( true );
}