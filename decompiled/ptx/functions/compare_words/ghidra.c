int compare_words(long *param_1,long *param_2)

{
  long lVar1;
  long lVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  
  lVar1 = param_2[1];
  lVar2 = param_1[1];
  lVar5 = lVar2;
  if (lVar1 <= lVar2) {
    lVar5 = lVar1;
  }
  if (ignore_case == '\0') {
    if (0 < lVar5) {
      lVar4 = 0;
      do {
        iVar3 = (uint)*(byte *)(*param_1 + lVar4) - (uint)*(byte *)(*param_2 + lVar4);
        if (iVar3 != 0) {
          return iVar3;
        }
        lVar4 = lVar4 + 1;
      } while (lVar5 != lVar4);
    }
  }
  else if (0 < lVar5) {
    lVar4 = 0;
    do {
      if ((uint)(byte)folded_chars[*(byte *)(*param_1 + lVar4)] -
          (uint)(byte)folded_chars[*(byte *)(*param_2 + lVar4)] != 0) {
        return (uint)(byte)folded_chars[*(byte *)(*param_1 + lVar4)] -
               (uint)(byte)folded_chars[*(byte *)(*param_2 + lVar4)];
      }
      lVar4 = lVar4 + 1;
    } while (lVar5 != lVar4);
  }
  return (uint)(lVar1 < lVar2) - (uint)(lVar2 < lVar1);
}