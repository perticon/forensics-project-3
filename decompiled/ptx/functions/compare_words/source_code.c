compare_words (const void *void_first, const void *void_second)
{
#define first ((const WORD *) void_first)
#define second ((const WORD *) void_second)
  ptrdiff_t length;		/* minimum of two lengths */
  ptrdiff_t counter;		/* cursor in words */
  int value;			/* value of comparison */

  length = first->size < second->size ? first->size : second->size;

  if (ignore_case)
    {
      for (counter = 0; counter < length; counter++)
        {
          value = (folded_chars [to_uchar (first->start[counter])]
                   - folded_chars [to_uchar (second->start[counter])]);
          if (value != 0)
            return value;
        }
    }
  else
    {
      for (counter = 0; counter < length; counter++)
        {
          value = (to_uchar (first->start[counter])
                   - to_uchar (second->start[counter]));
          if (value != 0)
            return value;
        }
    }

  return (first->size > second->size) - (first->size < second->size);
#undef first
#undef second
}