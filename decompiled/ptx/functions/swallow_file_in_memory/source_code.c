swallow_file_in_memory (char const *file_name, BLOCK *block)
{
  size_t used_length;		/* used length in memory buffer */

  /* As special cases, a file name which is NULL or "-" indicates standard
     input, which is already opened.  In all other cases, open the file from
     its name.  */
  bool using_stdin = !file_name || !*file_name || STREQ (file_name, "-");
  if (using_stdin)
    block->start = fread_file (stdin, 0, &used_length);
  else
    block->start = read_file (file_name, 0, &used_length);

  if (!block->start)
    die (EXIT_FAILURE, errno, "%s", quotef (using_stdin ? "-" : file_name));

  if (using_stdin)
    clearerr (stdin);

  block->end = block->start + used_length;
}