void swallow_file_in_memory(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbx7;
    void** rsp8;
    void** rax9;
    void** rdi10;
    void** rdx11;
    void** rsi12;
    void** rax13;
    void** rsp14;
    void** rdi15;
    void** rax16;
    void* v17;
    void* rax18;
    void** rax19;
    uint32_t* rax20;
    void** r13_21;
    void** r14_22;
    void** v23;
    void** rax24;
    void** v25;
    void** r12_26;
    void** v27;
    void** rbp28;
    void** v29;
    void** rdi30;
    void** rsi31;
    void* rax32;
    void** rax33;
    void** rdx34;
    struct s3* rdx35;
    int64_t v36;
    int64_t v37;
    int64_t r15_38;
    void** v39;
    void** r14_40;
    void** v41;
    void** v42;
    void** v43;
    void** rbp44;
    void** v45;
    void** rdx46;
    int64_t rax47;
    uint32_t ebx48;
    int32_t eax49;
    void** v50;
    void** v51;
    void** v52;
    void** v53;
    void** v54;
    int64_t v55;
    void** rdi56;
    void** rax57;
    void** rdi58;
    void** rax59;
    int32_t eax60;
    void** rdi61;
    void** rax62;
    void** rdi63;
    void** rax64;
    void** rdi65;
    void** rax66;
    int64_t v67;

    rbx7 = rsi;
    rsp8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 24);
    rax9 = g28;
    if (!rdi) 
        goto addr_6877_2;
    if (!*reinterpret_cast<void***>(rdi)) 
        goto addr_6877_2;
    if (*reinterpret_cast<void***>(rdi) == 45) {
        if (!*reinterpret_cast<void***>(rdi + 1)) {
            addr_6877_2:
            rdi10 = stdin;
            rdx11 = rsp8;
            *reinterpret_cast<uint32_t*>(&rsi12) = 0;
            *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
            rax13 = fread_file(rdi10, 0, rdx11);
            rsp14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8);
            *reinterpret_cast<void***>(rbx7) = rax13;
            if (rax13) {
                rdi15 = stdin;
                fun_3680(rdi15);
                rsp14 = rsp14 - 1 + 1;
                rax16 = *reinterpret_cast<void***>(rbx7);
                goto addr_689f_8;
            }
        } else {
            goto addr_68c5_10;
        }
    } else {
        addr_68c5_10:
        rdx11 = rsp8;
        *reinterpret_cast<uint32_t*>(&rsi12) = 0;
        *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
        rdi15 = rdi;
        rax16 = read_file(rdi15);
        rsp14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8);
        *reinterpret_cast<void***>(rbx7) = rax16;
        if (rax16) {
            addr_689f_8:
            *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) + reinterpret_cast<uint64_t>(v17));
            rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
            if (!rax18) {
                return;
            }
        }
    }
    rax19 = quotearg_n_style_colon();
    rax20 = fun_35d0();
    rcx = rax19;
    *reinterpret_cast<int32_t*>(&rdi15) = 1;
    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
    rdx11 = reinterpret_cast<void**>("%s");
    *reinterpret_cast<uint32_t*>(&rsi12) = *rax20;
    *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
    fun_3990();
    rsp14 = rsp14 - 1 + 1 - 1 + 1 - 1 + 1;
    fun_3710();
    r13_21 = rsi12;
    r14_22 = r13_21 + 8;
    v23 = rbx7;
    rax24 = g28;
    v25 = rax24;
    swallow_file_in_memory(rdi15, rsp14 - 1 + 1 - 1 - 1 - 1 - 1 - 1 - 4, rdx11, rcx, r8, r9);
    r12_26 = v27;
    rbp28 = v29;
    *reinterpret_cast<int32_t*>(&rdi30) = 0;
    *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
    *reinterpret_cast<void***>(r13_21) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi31) = 0;
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    *reinterpret_cast<void***>(r13_21 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_21 + 16) = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(r12_26) >= reinterpret_cast<unsigned char>(rbp28)) {
        addr_69cd_16:
        rax32 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(g28));
        if (!rax32) {
        }
    } else {
        do {
            rbx7 = r12_26;
            do {
                if (*reinterpret_cast<void***>(rbx7) == 10) 
                    break;
                ++rbx7;
            } while (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rbp28));
            if (reinterpret_cast<unsigned char>(rbx7) > reinterpret_cast<unsigned char>(r12_26)) {
                if (*reinterpret_cast<void***>(r13_21 + 8) == rsi31) {
                    rax33 = x2nrealloc();
                    rsi31 = *reinterpret_cast<void***>(r13_21 + 16);
                    *reinterpret_cast<void***>(r13_21) = rax33;
                    rdi30 = rax33;
                }
                rdx34 = rsi31;
                ++rsi31;
                rdx35 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx34) << 4) + reinterpret_cast<unsigned char>(rdi30));
                rdx35->f0 = r12_26;
                rdx35->f8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx7) - reinterpret_cast<unsigned char>(r12_26));
                *reinterpret_cast<void***>(r13_21 + 16) = rsi31;
            }
            if (reinterpret_cast<unsigned char>(rbx7) >= reinterpret_cast<unsigned char>(rbp28)) 
                goto addr_69cd_16;
            r12_26 = rbx7 + 1;
        } while (reinterpret_cast<unsigned char>(r12_26) < reinterpret_cast<unsigned char>(rbp28));
        goto addr_6a0d_29;
    }
    fun_3710();
    if (reinterpret_cast<unsigned char>(rsi31) > reinterpret_cast<unsigned char>(rdi30)) 
        goto addr_6a49_32;
    goto v36;
    addr_6a49_32:
    v37 = r15_38;
    v39 = r14_22;
    r14_40 = rsi31;
    v41 = r13_21;
    v42 = r12_26;
    v43 = rbp28;
    rbp44 = rdi30;
    v45 = rbx7;
    do {
        addr_6ad9_34:
        *reinterpret_cast<uint32_t*>(&rdx46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp44));
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax47) = *reinterpret_cast<unsigned char*>(&rdx46);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax47) + 4) = 0;
        ebx48 = *reinterpret_cast<uint32_t*>(&rdx46);
        if (*reinterpret_cast<signed char*>(0x231e0 + rax47)) {
            if (*reinterpret_cast<unsigned char*>(&rdx46) == 92) {
                rcx = stdout;
                fun_3900("\\backslash{}", 1, 12, rcx, r8, r9);
            } else {
                if (*reinterpret_cast<signed char*>(&rdx46) > reinterpret_cast<signed char>(92)) {
                    eax49 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx46 + 0xffffffffffffff85));
                    if (!(*reinterpret_cast<unsigned char*>(&eax49) & 0xfd)) {
                        fun_3930(1, "$\\%c$", rdx46, rcx, r8, r9, v50, v51, v52, v45, v43, v42, v41, v39, v37, v53, v54, v55, v25, v23);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&rdx46) == 95) {
                            addr_6b27_41:
                            rdi56 = stdout;
                            rax57 = *reinterpret_cast<void***>(rdi56 + 40);
                            if (reinterpret_cast<unsigned char>(rax57) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi56 + 48))) {
                                *reinterpret_cast<uint32_t*>(&v51 + 4) = *reinterpret_cast<uint32_t*>(&rdx46);
                                fun_3770();
                                goto addr_6ae9_43;
                            } else {
                                rcx = rax57 + 1;
                                *reinterpret_cast<void***>(rdi56 + 40) = rcx;
                                *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(92);
                                goto addr_6ae9_43;
                            }
                        } else {
                            addr_6b5c_45:
                            rdi58 = stdout;
                            rax59 = *reinterpret_cast<void***>(rdi58 + 40);
                            if (reinterpret_cast<unsigned char>(rax59) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi58 + 48))) {
                                fun_3770();
                            } else {
                                *reinterpret_cast<void***>(rdi58 + 40) = rax59 + 1;
                                *reinterpret_cast<void***>(rax59) = reinterpret_cast<void**>(32);
                            }
                        }
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rdx46) != 34) {
                        eax60 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx46 + 0xffffffffffffffdd));
                        if (*reinterpret_cast<unsigned char*>(&eax60) > 3) 
                            goto addr_6b5c_45; else 
                            goto addr_6b27_41;
                    } else {
                        rdi61 = stdout;
                        rax62 = *reinterpret_cast<void***>(rdi61 + 40);
                        if (reinterpret_cast<unsigned char>(rax62) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi61 + 48))) {
                            fun_3770();
                        } else {
                            *reinterpret_cast<void***>(rdi61 + 40) = rax62 + 1;
                            *reinterpret_cast<void***>(rax62) = reinterpret_cast<void**>(34);
                        }
                        rdi63 = stdout;
                        rax64 = *reinterpret_cast<void***>(rdi63 + 40);
                        if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi63 + 48))) {
                            fun_3770();
                        } else {
                            *reinterpret_cast<void***>(rdi63 + 40) = rax64 + 1;
                            *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(34);
                        }
                    }
                }
            }
        } else {
            addr_6ae9_43:
            rdi65 = stdout;
            rax66 = *reinterpret_cast<void***>(rdi65 + 40);
            if (reinterpret_cast<unsigned char>(rax66) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi65 + 48))) {
                fun_3770();
            } else {
                ++rbp44;
                *reinterpret_cast<void***>(rdi65 + 40) = rax66 + 1;
                *reinterpret_cast<void***>(rax66) = *reinterpret_cast<void***>(&ebx48);
                if (r14_40 != rbp44) 
                    goto addr_6ad9_34; else 
                    break;
            }
        }
        ++rbp44;
    } while (r14_40 != rbp44);
    goto v67;
    addr_6a0d_29:
    goto addr_69cd_16;
}