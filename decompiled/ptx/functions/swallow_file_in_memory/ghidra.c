void swallow_file_in_memory(char *param_1,long *param_2)

{
  long lVar1;
  undefined8 uVar2;
  int *piVar3;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (((param_1 == (char *)0x0) || (*param_1 == '\0')) ||
     ((*param_1 == '-' && (param_1[1] == '\0')))) {
    lVar1 = fread_file(stdin,0,&local_28);
    *param_2 = lVar1;
    if (lVar1 == 0) {
      param_1 = "-";
      goto LAB_001068f7;
    }
    clearerr_unlocked(stdin);
    lVar1 = *param_2;
  }
  else {
    lVar1 = read_file(param_1,0,&local_28);
    *param_2 = lVar1;
    if (lVar1 == 0) {
LAB_001068f7:
      uVar2 = quotearg_n_style_colon(0,3,param_1);
      piVar3 = __errno_location();
      error(1,*piVar3,&DAT_0011cd8d,uVar2);
      goto LAB_00106926;
    }
  }
  param_2[1] = lVar1 + local_28;
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00106926:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}