undefined8 main(int param_1,undefined8 *param_2)

{
  undefined4 *puVar1;
  undefined2 *puVar2;
  byte bVar3;
  ushort *puVar4;
  bool bVar5;
  char **ppcVar6;
  char cVar7;
  int iVar8;
  int iVar9;
  __int32_t **pp_Var10;
  ushort **ppuVar11;
  byte *pbVar12;
  long lVar13;
  byte *pbVar14;
  long lVar15;
  ulong uVar16;
  byte **ppbVar17;
  int *piVar18;
  char *pcVar19;
  size_t sVar20;
  undefined8 uVar21;
  char cVar22;
  char cVar23;
  uint uVar24;
  long lVar25;
  byte *pbVar26;
  byte *pbVar27;
  byte *pbVar28;
  byte *pbVar29;
  byte *pbVar30;
  byte *pbVar31;
  undefined8 *puVar32;
  byte *pbVar33;
  undefined1 *puVar34;
  long lVar35;
  byte *pbVar36;
  byte **ppbVar37;
  long in_FS_OFFSET;
  byte bVar38;
  undefined8 uVar39;
  ushort **local_128;
  long local_120;
  byte *local_110;
  byte *local_108;
  long local_f0;
  long local_c8;
  byte *local_c0;
  byte *local_b8;
  byte *local_b0;
  byte *local_a8;
  byte *local_a0;
  byte *local_98;
  byte *local_90;
  byte *local_88;
  byte *local_80;
  byte *local_78;
  undefined8 local_68;
  undefined8 uStack96;
  undefined local_58 [24];
  long local_40;
  
  bVar38 = 0;
  ppbVar37 = (byte **)&format_vals;
  puVar34 = long_options;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  pbVar36 = gap_size;
  pbVar27 = line_width;
switchD_00103c19_caseD_74:
  line_width = pbVar27;
  gap_size = pbVar36;
  uVar39 = 0x103bef;
  iVar8 = getopt_long(param_1,param_2,"AF:GM:ORS:TW:b:i:fg:o:trw:");
  if (iVar8 == -1) {
    if (optind == param_1) {
      input_file_name = (char **)xmalloc(8);
      file_line_count = xmalloc(8);
      text_buffers = xmalloc(0x10);
      number_input_files = 1;
      *input_file_name = (char *)0x0;
      goto LAB_00103fc9;
    }
    if (gnu_extensions != 0) {
      number_input_files = param_1 - optind;
      input_file_name = (char **)xnmalloc((long)number_input_files,8);
      file_line_count = xnmalloc((long)number_input_files,8);
      text_buffers = xnmalloc((long)number_input_files,0x10);
      ppcVar6 = input_file_name;
      if (0 < number_input_files) {
        lVar15 = (long)optind;
        uVar24 = number_input_files - 1;
        lVar13 = (long)(optind + 1);
        do {
          pcVar19 = (char *)param_2[lVar13 + -1];
          if (*pcVar19 == '\0') {
            pcVar19 = (char *)0x0;
          }
          else {
            iVar8 = strcmp(pcVar19,"-");
            if (iVar8 == 0) {
              pcVar19 = (char *)0x0;
            }
          }
          ppcVar6[(lVar13 + -1) - lVar15] = pcVar19;
          optind = (int)lVar13;
          lVar13 = lVar13 + 1;
        } while (lVar15 + 2 + (ulong)uVar24 != lVar13);
      }
      goto LAB_00103fc9;
    }
    number_input_files = 1;
    input_file_name = (char **)xmalloc(8);
    file_line_count = xmalloc(8);
    text_buffers = xmalloc(0x10);
    iVar8 = optind;
    lVar15 = (long)optind;
    pcVar19 = (char *)param_2[lVar15];
    if ((*pcVar19 == '\0') || (iVar9 = strcmp(pcVar19,"-"), iVar9 == 0)) {
      *input_file_name = (char *)0x0;
    }
    else {
      *input_file_name = pcVar19;
    }
    optind = iVar8 + 1;
    if (param_1 <= optind) goto LAB_00103fc9;
    lVar15 = freopen_safer(param_2[lVar15 + 1],&DAT_0011cc34,stdout);
    if (lVar15 == 0) {
      uVar39 = quotearg_n_style_colon(0,3,param_2[optind]);
      piVar18 = __errno_location();
      error(1,*piVar18,&DAT_0011cd8d,uVar39);
      goto LAB_00104d27;
    }
    optind = optind + 1;
    if (param_1 <= optind) goto LAB_00103fc9;
    quote(param_2[optind]);
    uVar39 = dcgettext(0,"extra operand %s",5);
    error(0,0,uVar39);
  }
  else if (iVar8 < 0x78) {
    if (9 < iVar8) goto code_r0x00103c12;
    if (iVar8 == -0x83) {
      uVar39 = proper_name_utf8("F. Pinard",&DAT_0011cbfe);
      version_etc(stdout,&DAT_0011cb00,"GNU coreutils",Version,uVar39,0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar8 == -0x82) {
      usage(0);
      goto switchD_00103c19_caseD_77;
    }
  }
  goto switchD_00103c19_caseD_b;
LAB_00103fc9:
  if (output_format == 0) {
    output_format = 2 - gnu_extensions;
  }
  if (ignore_case != '\0') {
    pp_Var10 = __ctype_toupper_loc();
    lVar15 = 0;
    puVar34 = folded_chars;
    do {
      puVar1 = (undefined4 *)((long)*pp_Var10 + lVar15);
      lVar15 = lVar15 + 4;
      *puVar34 = (char)*puVar1;
      puVar34 = puVar34 + 1;
    } while (lVar15 != 0x400);
  }
  if (context_regex._0_8_ == (char *)0x0) {
    if (gnu_extensions != 0) goto LAB_00104ccf;
    goto LAB_00104bec;
  }
  if (*context_regex._0_8_ == '\0') {
    context_regex._0_8_ = (char *)0x0;
    goto LAB_0010403d;
  }
LAB_00104bfa:
  compile_regex(context_regex);
LAB_0010403d:
  if (word_regex._0_8_ == (char *)0x0) goto LAB_00104b5b;
  compile_regex(word_regex);
  if (break_file != (char *)0x0) goto LAB_00104b6b;
LAB_00104067:
  if ((ignore_file != (char *)0x0) &&
     (digest_word_file(ignore_file,ignore_table), ignore_table._16_8_ == 0)) {
    ignore_file = (char *)0x0;
  }
  if ((only_file != (char *)0x0) && (digest_word_file(only_file,only_table), only_table._16_8_ == 0)
     ) {
    only_file = (char *)0x0;
  }
  number_of_occurs = 0;
  total_line_count = (byte *)0x0;
  maximum_word_length = (byte *)0x0;
  reference_max_width = 0;
  local_c8 = 0;
  if (0 < number_input_files) {
LAB_00104103:
    lVar15 = local_c8 * 8;
    swallow_file_in_memory(input_file_name[local_c8]);
    ppbVar37 = (byte **)(text_buffers + local_c8 * 0x10);
    local_110 = *ppbVar37;
    pbVar36 = local_110;
    if (input_reference == '\0') {
      local_f0 = 0;
      pbVar27 = ppbVar37[1];
      goto LAB_00104875;
    }
    pbVar27 = ppbVar37[1];
    pbVar33 = local_110;
    if (pbVar27 <= local_110) goto LAB_001062a9;
    ppuVar11 = __ctype_b_loc();
    pbVar12 = local_110;
    while ((*(byte *)((long)*ppuVar11 + (ulong)*pbVar12 * 2 + 1) & 0x20) == 0) {
      pbVar12 = pbVar12 + 1;
      pbVar33 = pbVar27;
      if (pbVar27 == pbVar12) goto LAB_001062a9;
    }
    local_f0 = (long)pbVar12 - (long)local_110;
    pbVar33 = pbVar12;
    if (pbVar12 < pbVar27) {
      do {
        pbVar33 = pbVar12;
        if ((*(byte *)((long)*ppuVar11 + (ulong)*pbVar12 * 2 + 1) & 0x20) == 0) break;
        pbVar12 = pbVar12 + 1;
        pbVar33 = pbVar12;
      } while (pbVar27 != pbVar12);
    }
joined_r0x001041e9:
    pbVar12 = pbVar27;
    if ((context_regex._0_8_ != (char *)0x0) &&
       (lVar13 = rpl_re_search(0x123728,pbVar36,(long)pbVar27 - (long)pbVar36,0,
                               (long)pbVar27 - (long)pbVar36,context_regs), lVar13 != -1)) {
      if (lVar13 == 0) {
        quote(context_regex._0_8_);
        uVar39 = dcgettext(0,"error: regular expression has a match of length zero: %s",5);
        error(1,0,uVar39);
        pbVar36 = pbVar33;
LAB_001062a9:
        local_f0 = (long)pbVar33 - (long)pbVar36;
        local_110 = pbVar33;
LAB_00104875:
        pbVar33 = local_110;
        local_110 = pbVar36;
        if (pbVar27 <= pbVar36) goto LAB_0010487e;
        goto joined_r0x001041e9;
      }
      if (lVar13 == -2) goto LAB_00104b56;
      pbVar12 = pbVar36 + *context_regs._16_8_;
    }
    pcVar19 = word_regex._0_8_;
    pbVar27 = pbVar12;
    local_108 = pbVar36;
    if (pbVar36 < pbVar12) {
      ppuVar11 = __ctype_b_loc();
      do {
        pcVar19 = word_regex._0_8_;
        if ((*(byte *)((long)*ppuVar11 + (ulong)pbVar27[-1] * 2 + 1) & 0x20) == 0) break;
        pbVar27 = pbVar27 + -1;
      } while (pbVar27 != pbVar36);
    }
joined_r0x0010428b:
    do {
      if (pcVar19 != (char *)0x0) {
        lVar13 = rpl_re_search(0x1235c8);
        if (lVar13 == -2) goto LAB_00104b56;
        if (lVar13 != -1) {
          pbVar28 = pbVar36 + *word_regs._8_8_;
          pbVar36 = pbVar36 + *word_regs._16_8_;
          pcVar19 = word_regex._0_8_;
          if (pbVar28 == pbVar36) goto LAB_001044d2;
LAB_001042e6:
          cVar23 = input_reference;
          uStack96 = pbVar36 + -(long)pbVar28;
          if ((long)maximum_word_length < (long)uStack96) {
            maximum_word_length = uStack96;
          }
          local_68 = pbVar28;
          if (input_reference != '\0') goto LAB_001045b0;
          goto LAB_0010431e;
        }
        goto LAB_00104790;
      }
      pbVar28 = pbVar36;
      if (pbVar36 < pbVar27) {
        while (word_fastmap[*pbVar28] == '\0') {
          pbVar28 = pbVar28 + 1;
          if (pbVar28 == pbVar27) goto LAB_00104790;
        }
        if (pbVar28 == pbVar27) goto LAB_00104790;
        pbVar30 = pbVar28;
        if (pbVar28 < pbVar27) {
          do {
            if (word_fastmap[*pbVar30] == '\0') {
              pcVar19 = word_regex._0_8_;
              pbVar36 = pbVar30;
              if (pbVar28 == pbVar30) goto LAB_001044d2;
              break;
            }
            pbVar30 = pbVar30 + 1;
            pbVar36 = pbVar27;
          } while (pbVar30 != pbVar27);
          goto LAB_001042e6;
        }
      }
      else if (pbVar36 == pbVar27) goto LAB_00104790;
LAB_001044d2:
      pbVar36 = pbVar28 + 1;
    } while( true );
  }
  goto LAB_001048ca;
code_r0x00103c12:
  pbVar36 = gap_size;
  pbVar27 = line_width;
  switch(iVar8) {
  case 10:
    lVar15 = __xargmatch_internal
                       ("--format",optarg,format_args,&format_vals,4,argmatch_die,1,uVar39);
    output_format = *(uint *)((long)&format_vals + lVar15 * 4);
    pbVar36 = gap_size;
    pbVar27 = line_width;
    goto switchD_00103c19_caseD_74;
  default:
    goto switchD_00103c19_caseD_b;
  case 0x41:
    auto_reference = '\x01';
    goto switchD_00103c19_caseD_74;
  case 0x46:
    truncation_string = optarg;
    unescape_string();
    pbVar36 = gap_size;
    pbVar27 = line_width;
    goto switchD_00103c19_caseD_74;
  case 0x47:
    gnu_extensions = 0;
    goto switchD_00103c19_caseD_74;
  case 0x4d:
    macro_name = optarg;
    goto switchD_00103c19_caseD_74;
  case 0x4f:
    output_format = 2;
    goto switchD_00103c19_caseD_74;
  case 0x52:
    right_reference = '\x01';
    goto switchD_00103c19_caseD_74;
  case 0x53:
    context_regex._0_8_ = optarg;
    unescape_string();
    pbVar36 = gap_size;
    pbVar27 = line_width;
    goto switchD_00103c19_caseD_74;
  case 0x54:
    output_format = 3;
    goto switchD_00103c19_caseD_74;
  case 0x57:
    word_regex._0_8_ = optarg;
    unescape_string();
    pbVar36 = gap_size;
    pbVar27 = line_width;
    if (*word_regex._0_8_ == '\0') {
      word_regex._0_8_ = (char *)0x0;
    }
    goto switchD_00103c19_caseD_74;
  case 0x62:
    break_file = optarg;
    goto switchD_00103c19_caseD_74;
  case 0x66:
    ignore_case = '\x01';
    goto switchD_00103c19_caseD_74;
  case 0x67:
    iVar8 = xstrtoimax(optarg,0,0,&local_68);
    if ((iVar8 == 0) && (pbVar36 = local_68, pbVar27 = line_width, 0 < (long)local_68))
    goto switchD_00103c19_caseD_74;
    uVar39 = quote(optarg);
    uVar21 = dcgettext(0,"invalid gap width: %s",5);
    error(1,0,uVar21,uVar39);
    break;
  case 0x69:
    ignore_file = optarg;
    goto switchD_00103c19_caseD_74;
  case 0x6f:
    only_file = optarg;
    goto switchD_00103c19_caseD_74;
  case 0x72:
    input_reference = '\x01';
  case 0x74:
    goto switchD_00103c19_caseD_74;
  case 0x77:
switchD_00103c19_caseD_77:
    iVar8 = xstrtoimax(optarg,0,0,&local_68);
    if ((iVar8 == 0) && (pbVar36 = gap_size, pbVar27 = local_68, 0 < (long)local_68))
    goto switchD_00103c19_caseD_74;
  }
  uVar39 = quote(optarg);
  uVar21 = dcgettext(0,"invalid line width: %s",5);
  error(1,0,uVar21,uVar39);
  goto LAB_00106334;
switchD_00103c19_caseD_b:
  usage(1);
LAB_00104ccf:
  if (input_reference == '\0') {
    context_regex._0_8_ = "[.?!][]\"\')}]*\\($\\|\t\\|  \\)[ \t\n]*";
  }
  else {
LAB_00104bec:
    context_regex._0_8_ = "\n";
  }
  goto LAB_00104bfa;
LAB_00104790:
  pbVar27 = ppbVar37[1];
  pbVar36 = pbVar12;
  if (pbVar27 <= pbVar12) goto LAB_0010487e;
  goto joined_r0x001041e9;
LAB_001045b0:
  if (pbVar33 < pbVar28) {
    pbVar30 = total_line_count;
    cVar7 = '\0';
    do {
      while (pbVar29 = pbVar33 + 1, *pbVar33 != 10) {
        pbVar33 = pbVar29;
        if (pbVar28 <= pbVar29) goto LAB_00104640;
      }
      pbVar30 = pbVar30 + 1;
      pbVar31 = ppbVar37[1];
      if (pbVar29 < pbVar31) {
        ppuVar11 = __ctype_b_loc();
        pbVar26 = pbVar29;
        do {
          pbVar14 = pbVar26;
          if ((*(byte *)((long)*ppuVar11 + (ulong)*pbVar14 * 2 + 1) & 0x20) != 0) {
            local_f0 = (long)pbVar14 - (long)pbVar29;
            pbVar33 = pbVar14;
            goto LAB_0010462c;
          }
          pbVar26 = pbVar14 + 1;
        } while (pbVar14 + 1 != pbVar31);
        local_f0 = (long)pbVar14 - (long)pbVar33;
        pbVar33 = pbVar31;
      }
      else {
        local_f0 = 0;
        pbVar33 = pbVar29;
      }
LAB_0010462c:
      local_110 = pbVar29;
      cVar7 = cVar23;
    } while (pbVar33 < pbVar28);
LAB_00104640:
    if (cVar7 != '\0') {
      total_line_count = pbVar30;
    }
  }
  pcVar19 = word_regex._0_8_;
  if (pbVar28 < pbVar33) goto joined_r0x0010428b;
LAB_0010431e:
  if ((ignore_file != (char *)0x0) && (lVar13 = ignore_table._16_8_ + -1, -1 < lVar13)) {
    lVar35 = 0;
    do {
      while( true ) {
        lVar25 = lVar35 + lVar13 >> 1;
        iVar8 = compare_words(&local_68);
        if (-1 < iVar8) break;
        lVar13 = lVar25 + -1;
        if (lVar13 < lVar35) goto LAB_001043b7;
      }
      pcVar19 = word_regex._0_8_;
      if (iVar8 == 0) goto joined_r0x0010428b;
      lVar35 = lVar25 + 1;
    } while (lVar35 <= lVar13);
  }
LAB_001043b7:
  if (only_file != (char *)0x0) {
    lVar13 = 0;
    lVar35 = only_table._16_8_ + -1;
    pcVar19 = word_regex._0_8_;
    if (-1 < lVar35) {
      do {
        while( true ) {
          lVar25 = lVar13 + lVar35 >> 1;
          iVar8 = compare_words(&local_68);
          pcVar19 = word_regex._0_8_;
          if (-1 < iVar8) break;
          lVar35 = lVar25 + -1;
          if (lVar35 < lVar13) goto joined_r0x0010428b;
        }
        if (iVar8 == 0) goto LAB_001044ef;
        lVar13 = lVar25 + 1;
      } while (lVar13 <= lVar35);
    }
    goto joined_r0x0010428b;
  }
LAB_001044ef:
  if (number_of_occurs == occurs_alloc) {
    occurs_table = (byte **)x2nrealloc(occurs_table,&occurs_alloc,0x30);
    cVar23 = input_reference;
  }
  cVar7 = auto_reference;
  pbVar30 = total_line_count;
  sVar20 = number_of_occurs;
  ppbVar17 = occurs_table + number_of_occurs * 6;
  if (auto_reference == '\0') {
    if (cVar23 == '\0') goto LAB_00104563;
    ppbVar17[4] = local_110 + -(long)pbVar28;
    if (reference_max_width < local_f0) {
      reference_max_width = local_f0;
    }
  }
  else {
    if (pbVar33 < pbVar28) {
      pbVar29 = total_line_count;
      cVar22 = '\0';
      do {
        while (pbVar31 = pbVar33 + 1, *pbVar33 != 10) {
LAB_001046b0:
          pbVar33 = pbVar31;
          if (pbVar28 <= pbVar31) goto LAB_00104710;
        }
        pbVar29 = pbVar29 + 1;
        pbVar26 = ppbVar37[1];
        local_110 = pbVar31;
        cVar22 = cVar7;
        if (pbVar26 <= pbVar31) goto LAB_001046b0;
        ppuVar11 = __ctype_b_loc();
        pbVar33 = pbVar31;
        do {
          if ((*(byte *)((long)*ppuVar11 + (ulong)*pbVar33 * 2 + 1) & 0x20) != 0) break;
          pbVar33 = pbVar33 + 1;
        } while (pbVar33 != pbVar26);
      } while (pbVar33 < pbVar28);
LAB_00104710:
      if (cVar22 != '\0') {
        pbVar30 = pbVar29;
        total_line_count = pbVar29;
      }
    }
    ppbVar17[4] = pbVar30;
  }
  if (((local_110 == local_108) && (cVar23 != '\0')) && (local_108 < pbVar27)) {
    ppuVar11 = __ctype_b_loc();
    do {
      if ((*(byte *)((long)*ppuVar11 + (ulong)*local_108 * 2 + 1) & 0x20) != 0) {
        if (local_108 < pbVar27) goto LAB_00104818;
        break;
      }
      local_108 = local_108 + 1;
    } while (local_108 != pbVar27);
  }
LAB_00104563:
  number_of_occurs = sVar20 + 1;
  *(undefined4 *)ppbVar17 = (undefined4)local_68;
  *(undefined4 *)((long)ppbVar17 + 4) = local_68._4_4_;
  *(undefined4 *)(ppbVar17 + 1) = (undefined4)uStack96;
  *(undefined4 *)((long)ppbVar17 + 0xc) = uStack96._4_4_;
  ppbVar17[2] = local_108 + -(long)pbVar28;
  ppbVar17[3] = pbVar27 + -(long)pbVar28;
  *(int *)(ppbVar17 + 5) = (int)local_c8;
  pcVar19 = word_regex._0_8_;
  goto joined_r0x0010428b;
  while ((*(byte *)((long)*ppuVar11 + (ulong)*local_108 * 2 + 1) & 0x20) != 0) {
LAB_00104818:
    local_108 = local_108 + 1;
    if (local_108 == pbVar27) break;
  }
  goto LAB_00104563;
LAB_0010487e:
  local_c8 = local_c8 + 1;
  total_line_count = total_line_count + 1;
  *(byte **)(file_line_count + lVar15) = total_line_count;
  if (number_input_files <= (int)local_c8) goto code_r0x001048ba;
  goto LAB_00104103;
code_r0x001048ba:
  if (number_of_occurs != 0) {
    qsort(occurs_table,number_of_occurs,0x30,compare_occurs);
  }
LAB_001048ca:
  if (auto_reference == '\0') {
LAB_001048d7:
    if (input_reference == '\0') goto LAB_00104908;
  }
  else {
    reference_max_width = 0;
    for (uVar16 = 0; uVar16 < (ulong)(long)number_input_files; uVar16 = uVar16 + 1) {
      iVar8 = __sprintf_chk(local_58,1,0x15,&DAT_0011cc48);
      lVar15 = (long)iVar8;
      if (input_file_name[uVar16] != (char *)0x0) {
        sVar20 = strlen(input_file_name[uVar16]);
        lVar15 = (long)iVar8 + sVar20;
      }
      if (reference_max_width < lVar15) {
        reference_max_width = lVar15;
      }
    }
    lVar15 = reference_max_width + 2;
    reference_max_width = reference_max_width + 1;
    reference._0_8_ = (byte *)xmalloc(lVar15);
    if (auto_reference == '\0') goto LAB_001048d7;
  }
  if (right_reference == '\0') {
    line_width = line_width + -(long)(gap_size + reference_max_width);
  }
LAB_00104908:
  if ((long)line_width < 0) {
    line_width = (byte *)0x0;
  }
  lVar15 = (long)line_width / 2;
  lVar13 = lVar15 - (long)gap_size;
  before_max_width = lVar13;
  half_line_width = lVar15;
  if ((truncation_string == (undefined *)0x0) || (*truncation_string == '\0')) {
    truncation_string = (undefined *)0x0;
  }
  else {
    keyafter_max_width = lVar15;
    truncation_string_length = strlen(truncation_string);
  }
  if (gnu_extensions == 0) {
    lVar13 = -(truncation_string_length * 2 + 1);
  }
  else {
    before_max_width = lVar13 + truncation_string_length * -2;
    if (before_max_width < 0) {
      before_max_width = 0;
    }
    lVar13 = truncation_string_length * -2;
  }
  keyafter_max_width = lVar15 + lVar13;
  local_128 = __ctype_b_loc();
  lVar15 = 0;
  puVar4 = *local_128;
  do {
    edited_flag[lVar15] = (byte)(puVar4[lVar15] >> 0xd) & 1;
    lVar15 = lVar15 + 1;
  } while (lVar15 != 0x100);
  edited_flag[12] = 1;
  if (output_format == 2) {
    edited_flag[34] = 1;
  }
  else if (output_format == 3) {
    pbVar36 = &DAT_0011cba7;
    uVar16 = 0x24;
    do {
      pbVar36 = pbVar36 + 1;
      edited_flag[uVar16] = 1;
      uVar16 = (ulong)*pbVar36;
    } while (*pbVar36 != 0);
  }
  tail_truncation = '\0';
  puVar34 = word_fastmap;
  tail._0_8_ = (byte *)0x0;
  tail._8_8_ = (byte *)0x0;
  head._0_8_ = (byte *)0x0;
  head._8_8_ = (byte *)0x0;
  head_truncation = '\0';
  local_120 = 0;
  ppbVar37 = occurs_table;
  if ((long)number_of_occurs < 1) {
LAB_001059fc:
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return 0;
  }
  do {
    pbVar27 = *ppbVar37;
    pbVar33 = ppbVar37[2];
    pbVar36 = pbVar27 + (long)pbVar33;
    pbVar30 = ppbVar37[1] + (long)pbVar27;
    pbVar29 = ppbVar37[3] + (long)pbVar27;
    ppbVar17 = (byte **)((long)*(int *)(ppbVar37 + 5) * 0x10 + text_buffers);
    pbVar12 = *ppbVar17;
    pbVar28 = ppbVar17[1];
    lVar15 = keyafter_max_width;
    keyafter._0_8_ = pbVar27;
    keyafter._8_8_ = pbVar30;
    if (pbVar30 < pbVar29) {
      do {
        if (pbVar27 + lVar15 < pbVar30) {
          pbVar33 = ppbVar37[2];
          goto LAB_00104dfb;
        }
        keyafter._8_8_ = pbVar30;
        if (word_regex._0_8_ == (char *)0x0) {
          if (puVar34[*pbVar30] == '\0') {
            pbVar30 = pbVar30 + 1;
          }
          else {
            if (pbVar29 <= pbVar30) break;
            do {
              pbVar30 = pbVar30 + 1;
              if (pbVar29 == pbVar30) goto LAB_00104dd8;
            } while (puVar34[*pbVar30] != '\0');
          }
        }
        else {
          lVar15 = rpl_re_match(0x1235c8,pbVar30,(long)pbVar29 - (long)pbVar30,0);
          if (lVar15 == -2) goto LAB_00104b56;
          if (lVar15 == -1) {
            lVar15 = 1;
          }
          pbVar30 = pbVar30 + lVar15;
          lVar15 = keyafter_max_width;
          pbVar27 = keyafter._0_8_;
        }
      } while (pbVar30 < pbVar29);
LAB_00104dd8:
      pbVar33 = ppbVar37[2];
    }
    pbVar27 = keyafter._0_8_;
    if (pbVar30 <= keyafter._0_8_ + lVar15) {
      keyafter._8_8_ = pbVar30;
    }
LAB_00104dfb:
    keyafter_truncation = truncation_string != (undefined *)0x0 && keyafter._8_8_ < pbVar29;
    pbVar30 = keyafter._8_8_;
    if (pbVar27 < keyafter._8_8_) {
      bVar5 = false;
      pbVar31 = local_a0;
      local_a0 = keyafter._8_8_;
      do {
        local_a0 = local_a0 + -1;
        if ((*(byte *)((long)*local_128 + (ulong)*local_a0 * 2 + 1) & 0x20) == 0) {
          pbVar30 = keyafter._8_8_;
          if (bVar5) {
            pbVar30 = pbVar31;
          }
          break;
        }
        bVar5 = true;
        pbVar31 = local_a0;
        pbVar30 = pbVar27;
      } while (pbVar27 != local_a0);
    }
    keyafter._8_8_ = pbVar30;
    pbVar30 = maximum_word_length + half_line_width;
    if ((byte *)-(long)pbVar30 == pbVar33 || -(long)pbVar33 < (long)pbVar30) {
      pbVar33 = pbVar27 + (long)pbVar33;
    }
    else {
      pbVar31 = pbVar27 + -(long)pbVar30;
      if (word_regex._0_8_ == (char *)0x0) {
        if (puVar34[*pbVar31] == '\0') {
          pbVar33 = pbVar31 + 1;
        }
        else {
          pbVar33 = pbVar31;
          if (pbVar31 < pbVar27) {
            do {
              pbVar31 = pbVar31 + 1;
              pbVar33 = pbVar27;
              if (pbVar31 == pbVar27) break;
              pbVar33 = pbVar31;
            } while (puVar34[*pbVar31] != '\0');
          }
        }
      }
      else {
        lVar15 = rpl_re_match(0x1235c8,pbVar31,pbVar30,0);
        if (lVar15 == -2) break;
        if (lVar15 == -1) {
          lVar15 = 1;
        }
        pbVar33 = pbVar31 + lVar15;
      }
    }
    before._8_8_ = keyafter._0_8_;
    pbVar27 = before._8_8_;
    if (pbVar33 < keyafter._0_8_) {
      bVar5 = false;
      pbVar30 = local_b0;
      local_b0 = keyafter._0_8_;
      do {
        local_b0 = local_b0 + -1;
        if ((*(byte *)((long)*local_128 + (ulong)*local_b0 * 2 + 1) & 0x20) == 0) {
          pbVar27 = before._8_8_;
          if (bVar5) {
            pbVar27 = pbVar30;
          }
          break;
        }
        bVar5 = true;
        pbVar30 = local_b0;
        pbVar27 = pbVar33;
      } while (pbVar33 != local_b0);
    }
    before._8_8_ = pbVar27;
    pbVar27 = before._8_8_;
    lVar15 = before_max_width;
    pbVar30 = local_88;
    before._0_8_ = pbVar33;
    if (pbVar33 + before_max_width < before._8_8_) {
      do {
        while (word_regex._0_8_ != (char *)0x0) {
          lVar13 = rpl_re_match(0x1235c8,before._0_8_,(long)pbVar27 - (long)before._0_8_,0);
          if (lVar13 == -2) goto LAB_00104b56;
          if (lVar13 == -1) {
            lVar13 = 1;
          }
          pbVar27 = before._8_8_;
          lVar15 = before_max_width;
          pbVar31 = before._0_8_ + lVar13;
LAB_00104fd4:
          before._0_8_ = pbVar31;
          if (pbVar27 <= before._0_8_ + lVar15) goto LAB_00105017;
        }
        if (puVar34[*before._0_8_] != '\0') {
          bVar5 = false;
          pbVar26 = before._0_8_;
          pbVar14 = pbVar30;
          pbVar31 = before._0_8_;
          if (before._0_8_ < pbVar27) {
            do {
              pbVar30 = pbVar14;
              if (puVar34[*pbVar26] == '\0') {
                pbVar31 = before._0_8_;
                if (bVar5) {
                  pbVar31 = pbVar30;
                }
                break;
              }
              pbVar26 = pbVar26 + 1;
              bVar5 = true;
              pbVar14 = pbVar26;
              pbVar31 = pbVar27;
            } while (pbVar26 != pbVar27);
          }
          goto LAB_00104fd4;
        }
        before._0_8_ = before._0_8_ + 1;
      } while (before._0_8_ + lVar15 < pbVar27);
    }
LAB_00105017:
    local_88 = pbVar30;
    before_truncation = false;
    if (truncation_string != (undefined *)0x0) {
      pbVar30 = before._0_8_;
      if (pbVar12 < before._0_8_) {
        do {
          if ((*(byte *)((long)*local_128 + (ulong)pbVar30[-1] * 2 + 1) & 0x20) == 0) break;
          pbVar30 = pbVar30 + -1;
        } while (pbVar12 != pbVar30);
      }
      before_truncation = pbVar36 < pbVar30;
    }
    pbVar30 = before._0_8_;
    pbVar31 = before._0_8_;
    if (before._0_8_ < pbVar28) {
      bVar5 = false;
      pbVar26 = local_90;
      do {
        local_90 = pbVar26;
        if ((*(byte *)((long)*local_128 + (ulong)*pbVar30 * 2 + 1) & 0x20) == 0) {
          pbVar31 = before._0_8_;
          if (bVar5) {
            before._0_8_ = local_90;
            pbVar31 = before._0_8_;
          }
          break;
        }
        pbVar30 = pbVar30 + 1;
        bVar5 = true;
        pbVar31 = pbVar28;
        pbVar26 = pbVar30;
      } while (pbVar28 != pbVar30);
    }
    before._0_8_ = pbVar31;
    lVar15 = (lVar15 - ((long)pbVar27 - (long)pbVar30)) - (long)gap_size;
    if (lVar15 < 1) {
      tail._0_8_ = (byte *)0x0;
      tail._8_8_ = (byte *)0x0;
LAB_00105ad4:
      tail_truncation = '\0';
      pbVar27 = tail._8_8_;
    }
    else {
      pbVar27 = keyafter._8_8_;
      tail._8_8_ = keyafter._8_8_;
      pbVar30 = keyafter._8_8_;
      tail._0_8_ = keyafter._8_8_;
      if (keyafter._8_8_ < pbVar28) {
        bVar5 = false;
        pbVar30 = local_78;
        do {
          local_78 = pbVar30;
          if ((*(byte *)((long)*local_128 + (ulong)*pbVar27 * 2 + 1) & 0x20) == 0) {
            tail._8_8_ = pbVar27;
            pbVar30 = keyafter._8_8_;
            tail._0_8_ = keyafter._8_8_;
            if (bVar5) {
              pbVar30 = local_78;
              tail._0_8_ = local_78;
            }
            break;
          }
          pbVar27 = pbVar27 + 1;
          bVar5 = true;
          pbVar30 = pbVar27;
          tail._8_8_ = pbVar27;
          tail._0_8_ = pbVar27;
        } while (pbVar28 != pbVar27);
      }
joined_r0x0010512d:
      if (pbVar27 < pbVar29) {
        if (pbVar30 + lVar15 <= pbVar27) goto LAB_0010526c;
        tail._8_8_ = pbVar27;
        if (word_regex._0_8_ == (char *)0x0) {
          if (puVar34[*pbVar27] == '\0') {
            pbVar27 = pbVar27 + 1;
          }
          else {
            if (pbVar29 <= pbVar27) goto LAB_00105256;
            do {
              pbVar27 = pbVar27 + 1;
              if (pbVar29 == pbVar27) goto LAB_00105256;
            } while (puVar34[*pbVar27] != '\0');
          }
        }
        else {
          lVar13 = rpl_re_match(0x1235c8,pbVar27,(long)pbVar29 - (long)pbVar27,0);
          if (lVar13 == -2) break;
          if (lVar13 == -1) {
            lVar13 = 1;
          }
          pbVar27 = pbVar27 + lVar13;
          pbVar30 = tail._0_8_;
        }
        goto joined_r0x0010512d;
      }
LAB_00105256:
      pbVar30 = tail._0_8_;
      if (pbVar27 < tail._0_8_ + lVar15) {
        tail._8_8_ = pbVar27;
      }
LAB_0010526c:
      if (tail._8_8_ <= pbVar30) goto LAB_00105ad4;
      keyafter_truncation = '\0';
      tail_truncation = truncation_string != (undefined *)0x0 && tail._8_8_ < pbVar29;
      bVar5 = false;
      pbVar28 = local_a8;
      local_a8 = tail._8_8_;
      do {
        local_a8 = local_a8 + -1;
        if ((*(byte *)((long)*local_128 + (ulong)*local_a8 * 2 + 1) & 0x20) == 0) {
          pbVar27 = tail._8_8_;
          if (bVar5) {
            pbVar27 = pbVar28;
          }
          break;
        }
        bVar5 = true;
        pbVar28 = local_a8;
        pbVar27 = local_a8;
      } while (pbVar30 != local_a8);
    }
    tail._8_8_ = pbVar27;
    lVar15 = (keyafter_max_width - ((long)keyafter._8_8_ - (long)keyafter._0_8_)) - (long)gap_size;
    if (lVar15 < 1) {
      head._0_8_ = (byte *)0x0;
      head._8_8_ = (byte *)0x0;
      head_truncation = '\0';
      pbVar27 = head._0_8_;
    }
    else {
      head._8_8_ = before._0_8_;
      pbVar27 = head._8_8_;
      if (pbVar12 < before._0_8_) {
        bVar5 = false;
        pbVar28 = local_b8;
        local_b8 = before._0_8_;
        do {
          local_b8 = local_b8 + -1;
          if ((*(byte *)((long)*local_128 + (ulong)*local_b8 * 2 + 1) & 0x20) == 0) {
            pbVar27 = head._8_8_;
            if (bVar5) {
              pbVar27 = pbVar28;
            }
            break;
          }
          bVar5 = true;
          pbVar28 = local_b8;
          pbVar27 = local_b8;
        } while (pbVar12 != local_b8);
      }
      head._8_8_ = pbVar27;
      pbVar12 = head._8_8_;
      head._0_8_ = pbVar33;
      if (pbVar33 + lVar15 < head._8_8_) {
        do {
          while (word_regex._0_8_ != (char *)0x0) {
            lVar13 = rpl_re_match(0x1235c8,head._0_8_,(long)pbVar12 - (long)head._0_8_);
            if (lVar13 == -2) goto LAB_00104b56;
            if (lVar13 == -1) {
              lVar13 = 1;
            }
            pbVar12 = head._8_8_;
            pbVar27 = head._0_8_ + lVar13;
LAB_001053d6:
            head._0_8_ = pbVar27;
            if (pbVar12 <= head._0_8_ + lVar15) goto LAB_0010540e;
          }
          if (puVar34[*head._0_8_] != '\0') {
            bVar5 = false;
            pbVar33 = head._0_8_;
            pbVar28 = local_c0;
            pbVar27 = head._0_8_;
            if (head._0_8_ < pbVar12) {
              do {
                local_c0 = pbVar28;
                if (puVar34[*pbVar33] == '\0') {
                  pbVar27 = head._0_8_;
                  if (bVar5) {
                    pbVar27 = local_c0;
                  }
                  break;
                }
                pbVar33 = pbVar33 + 1;
                bVar5 = true;
                pbVar28 = pbVar33;
                pbVar27 = pbVar12;
              } while (pbVar33 != pbVar12);
            }
            goto LAB_001053d6;
          }
          head._0_8_ = head._0_8_ + 1;
        } while (head._0_8_ + lVar15 < pbVar12);
      }
LAB_0010540e:
      head_truncation = '\0';
      pbVar27 = head._0_8_;
      if (head._0_8_ < pbVar12) {
        before_truncation = '\0';
        head_truncation = truncation_string != (undefined *)0x0 && pbVar36 < head._0_8_;
        bVar5 = false;
        pbVar36 = local_80;
        do {
          local_80 = pbVar36;
          if ((*(byte *)((long)*local_128 + (ulong)*pbVar27 * 2 + 1) & 0x20) == 0) {
            pbVar27 = head._0_8_;
            if (bVar5) {
              head._0_8_ = local_80;
              pbVar27 = head._0_8_;
            }
            break;
          }
          pbVar27 = pbVar27 + 1;
          bVar5 = true;
          pbVar36 = pbVar27;
        } while (pbVar27 != pbVar12);
      }
    }
    head._0_8_ = pbVar27;
    if (auto_reference == '\0') {
      if ((input_reference != '\0') &&
         (reference._0_8_ = ppbVar37[4] + (long)keyafter._0_8_, reference._8_8_ = reference._0_8_,
         reference._0_8_ < pbVar29)) {
        pbVar36 = reference._0_8_;
        pbVar27 = local_98;
        cVar23 = auto_reference;
        do {
          local_98 = pbVar27;
          if ((*(byte *)((long)*local_128 + (ulong)*pbVar36 * 2 + 1) & 0x20) != 0) {
            reference._8_8_ = reference._0_8_;
            if (cVar23 != '\0') {
              reference._8_8_ = local_98;
            }
            break;
          }
          pbVar36 = pbVar36 + 1;
          reference._8_8_ = pbVar29;
          pbVar27 = pbVar36;
          cVar23 = input_reference;
        } while (pbVar29 != pbVar36);
      }
    }
    else {
      pcVar19 = input_file_name[*(int *)(ppbVar37 + 5)];
      if (pcVar19 == (char *)0x0) {
        pcVar19 = "";
      }
      pcVar19 = stpcpy((char *)reference._0_8_,pcVar19);
      iVar8 = __sprintf_chk(pcVar19,1,0xffffffffffffffff);
      reference._8_8_ = (byte *)(pcVar19 + iVar8);
    }
    if (output_format == 2) {
      __printf_chk(1,&DAT_0011cc4c,macro_name);
      print_field(tail._0_8_,tail._8_8_);
      if (tail_truncation != '\0') {
        fputs_unlocked(truncation_string,stdout);
      }
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '\"';
      }
      else {
        __overflow(stdout,0x22);
      }
      param_2 = (undefined8 *)&DAT_0011cc4f;
      fwrite_unlocked(&DAT_0011cc4f,1,2,stdout);
      if (before_truncation != '\0') {
LAB_00106334:
        fputs_unlocked(truncation_string,stdout);
      }
      print_field(before._0_8_,before._8_8_);
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '\"';
      }
      else {
        __overflow(stdout,0x22);
      }
      fwrite_unlocked(param_2,1,2,stdout);
      print_field(keyafter._0_8_,keyafter._8_8_);
      if (keyafter_truncation != '\0') {
        fputs_unlocked(truncation_string,stdout);
      }
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '\"';
      }
      else {
        __overflow(stdout,0x22);
      }
      fwrite_unlocked(param_2,1,2,stdout);
      if (head_truncation != '\0') {
        fputs_unlocked(truncation_string,stdout);
      }
      print_field(head._0_8_,head._8_8_);
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '\"';
      }
      else {
        __overflow(stdout,0x22);
      }
      if ((auto_reference != '\0') || (input_reference != '\0')) {
        fwrite_unlocked(param_2,1,2,stdout);
        print_field(reference._0_8_,reference._8_8_);
        pcVar19 = stdout->_IO_write_ptr;
        if (pcVar19 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar19 + 1;
          *pcVar19 = '\"';
        }
        else {
          __overflow(stdout,0x22);
        }
      }
      goto LAB_001059c0;
    }
    if (output_format < 3) {
      if (right_reference == '\0') {
        if (auto_reference == '\0') {
          print_field(reference._0_8_,reference._8_8_);
          for (pbVar36 = gap_size +
                         (reference_max_width - ((long)reference._8_8_ - (long)reference._0_8_));
              0 < (long)pbVar36; pbVar36 = pbVar36 + -1) {
            pcVar19 = stdout->_IO_write_ptr;
            if (pcVar19 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar19 + 1;
              *pcVar19 = ' ';
            }
            else {
              __overflow(stdout,0x20);
            }
          }
        }
        else {
          print_field(reference._0_8_,reference._8_8_);
          pcVar19 = stdout->_IO_write_ptr;
          if (pcVar19 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar19 + 1;
            *pcVar19 = ':';
          }
          else {
            __overflow(stdout,0x3a);
          }
          pbVar36 = gap_size +
                    (reference_max_width - ((long)reference._8_8_ - (long)reference._0_8_));
          while (pbVar36 = pbVar36 + -1, 0 < (long)pbVar36) {
            pcVar19 = stdout->_IO_write_ptr;
            if (pcVar19 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar19 + 1;
              *pcVar19 = ' ';
            }
            else {
              __overflow(stdout,0x20);
            }
          }
        }
      }
      if (tail._0_8_ < tail._8_8_) {
        print_field(tail._0_8_,tail._8_8_);
        if (tail_truncation == '\0') {
          lVar15 = (half_line_width - (long)gap_size) - ((long)before._8_8_ - (long)before._0_8_);
          sVar20 = truncation_string_length;
          if (before_truncation != '\0') goto LAB_00106153;
          lVar15 = lVar15 - ((long)tail._8_8_ - (long)tail._0_8_);
          sVar20 = 0;
        }
        else {
          fputs_unlocked(truncation_string,stdout);
          lVar15 = (half_line_width - (long)gap_size) - ((long)before._8_8_ - (long)before._0_8_);
          sVar20 = truncation_string_length;
          if (before_truncation == '\0') {
            sVar20 = 0;
          }
LAB_00106153:
          lVar15 = (lVar15 - sVar20) - ((long)tail._8_8_ - (long)tail._0_8_);
          sVar20 = truncation_string_length;
          if (tail_truncation == '\0') {
            sVar20 = 0;
          }
        }
        for (lVar15 = lVar15 - sVar20; 0 < lVar15; lVar15 = lVar15 + -1) {
          pcVar19 = stdout->_IO_write_ptr;
          if (pcVar19 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar19 + 1;
            *pcVar19 = ' ';
          }
          else {
            __overflow(stdout,0x20);
          }
        }
LAB_001056be:
        if (before_truncation != '\0') {
LAB_0010600f:
          fputs_unlocked(truncation_string,stdout);
        }
      }
      else {
        lVar15 = (half_line_width - (long)gap_size) - ((long)before._8_8_ - (long)before._0_8_);
        if (before_truncation != '\0') {
          lVar15 = lVar15 - truncation_string_length;
          if (0 < lVar15) goto LAB_00105698;
          goto LAB_0010600f;
        }
        if (0 < lVar15) {
LAB_00105698:
          do {
            pcVar19 = stdout->_IO_write_ptr;
            if (pcVar19 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar19 + 1;
              *pcVar19 = ' ';
            }
            else {
              __overflow(stdout,0x20);
            }
            lVar15 = lVar15 + -1;
          } while (lVar15 != 0);
          goto LAB_001056be;
        }
      }
      print_field(before._0_8_,before._8_8_);
      pbVar36 = gap_size;
      if (0 < (long)gap_size) {
        do {
          pcVar19 = stdout->_IO_write_ptr;
          if (pcVar19 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar19 + 1;
            *pcVar19 = ' ';
          }
          else {
            __overflow(stdout,0x20);
          }
          pbVar36 = pbVar36 + -1;
        } while (pbVar36 != (byte *)0x0);
      }
      print_field(keyafter._0_8_,keyafter._8_8_);
      if (keyafter_truncation == '\0') {
        if (head._0_8_ < head._8_8_) {
          lVar15 = half_line_width - ((long)keyafter._8_8_ - (long)keyafter._0_8_);
          sVar20 = 0;
          goto LAB_00105f2b;
        }
LAB_00105751:
        if (auto_reference != '\0') {
          if (right_reference == '\0') goto LAB_001059c0;
LAB_00105e99:
          sVar20 = 0;
          if (keyafter_truncation != '\0') {
            sVar20 = truncation_string_length;
          }
          for (lVar15 = (half_line_width - ((long)keyafter._8_8_ - (long)keyafter._0_8_)) - sVar20;
              0 < lVar15; lVar15 = lVar15 + -1) {
            pcVar19 = stdout->_IO_write_ptr;
            if (pcVar19 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar19 + 1;
              *pcVar19 = ' ';
            }
            else {
              __overflow(stdout,0x20);
            }
          }
          goto LAB_00105ef0;
        }
        if (input_reference != '\0') {
          if (right_reference != '\0') goto LAB_00105e99;
          goto LAB_00105778;
        }
      }
      else {
        fputs_unlocked(truncation_string,stdout);
        if (head._8_8_ <= head._0_8_) goto LAB_00105751;
        lVar15 = half_line_width - ((long)keyafter._8_8_ - (long)keyafter._0_8_);
        sVar20 = truncation_string_length;
        if (keyafter_truncation == '\0') {
          sVar20 = 0;
        }
LAB_00105f2b:
        lVar15 = (lVar15 - sVar20) - ((long)head._8_8_ - (long)head._0_8_);
        if (head_truncation == '\0') {
          if (0 < lVar15) goto LAB_00105f54;
        }
        else {
          lVar15 = lVar15 - truncation_string_length;
          if (0 < lVar15) {
LAB_00105f54:
            do {
              pcVar19 = stdout->_IO_write_ptr;
              if (pcVar19 < stdout->_IO_write_end) {
                stdout->_IO_write_ptr = pcVar19 + 1;
                *pcVar19 = ' ';
              }
              else {
                __overflow(stdout,0x20);
              }
              lVar15 = lVar15 + -1;
            } while (lVar15 != 0);
            if (head_truncation == '\0') goto LAB_00105f87;
          }
          fputs_unlocked(truncation_string,stdout);
        }
LAB_00105f87:
        print_field(head._0_8_,head._8_8_);
LAB_00105ef0:
        if (auto_reference == '\0') {
LAB_00105778:
          if (input_reference == '\0') goto LAB_001059c0;
        }
        pbVar36 = gap_size;
        if (right_reference != '\0') {
          for (; 0 < (long)pbVar36; pbVar36 = pbVar36 + -1) {
            pcVar19 = stdout->_IO_write_ptr;
            if (pcVar19 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar19 + 1;
              *pcVar19 = ' ';
            }
            else {
              __overflow(stdout,0x20);
            }
          }
          print_field(reference._0_8_,reference._8_8_);
        }
      }
LAB_001059c0:
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
    }
    else if (output_format == 3) {
      __printf_chk(1,&DAT_0011cc52,macro_name);
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '{';
      }
      else {
        __overflow(stdout,0x7b);
      }
      print_field(tail._0_8_,tail._8_8_);
      fwrite_unlocked(&DAT_0011cc57,1,2,stdout);
      print_field(before._0_8_,before._8_8_);
      fwrite_unlocked(&DAT_0011cc57,1,2,stdout);
      pbVar27 = keyafter._8_8_;
      pbVar36 = keyafter._0_8_;
      if (word_regex._0_8_ == (char *)0x0) {
        pbVar33 = keyafter._0_8_;
        if (puVar34[*keyafter._0_8_] == '\0') {
          pbVar33 = keyafter._0_8_ + 1;
        }
        else {
          while ((pbVar33 < keyafter._8_8_ && (puVar34[*pbVar33] != '\0'))) {
            pbVar33 = pbVar33 + 1;
          }
        }
      }
      else {
        lVar15 = rpl_re_match(0x1235c8,keyafter._0_8_,(long)keyafter._8_8_ - (long)keyafter._0_8_,0)
        ;
        if (lVar15 == -2) break;
        if (lVar15 == -1) {
          lVar15 = 1;
        }
        pbVar33 = pbVar36 + lVar15;
      }
      print_field(pbVar36,pbVar33);
      fwrite_unlocked(&DAT_0011cc57,1,2,stdout);
      print_field(pbVar33,pbVar27);
      fwrite_unlocked(&DAT_0011cc57,1,2,stdout);
      print_field(head._0_8_,head._8_8_);
      pcVar19 = stdout->_IO_write_ptr;
      if (pcVar19 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar19 + 1;
        *pcVar19 = '}';
      }
      else {
        __overflow(stdout,0x7d);
      }
      if ((auto_reference != '\0') || (input_reference != '\0')) {
        pcVar19 = stdout->_IO_write_ptr;
        if (pcVar19 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar19 + 1;
          *pcVar19 = '{';
        }
        else {
          __overflow(stdout,0x7b);
        }
        print_field(reference._0_8_,reference._8_8_);
        pcVar19 = stdout->_IO_write_ptr;
        if (pcVar19 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar19 + 1;
          *pcVar19 = '}';
        }
        else {
          __overflow(stdout,0x7d);
        }
      }
      goto LAB_001059c0;
    }
    local_120 = local_120 + 1;
    ppbVar37 = ppbVar37 + 6;
    if ((long)number_of_occurs <= local_120) goto LAB_001059fc;
  } while( true );
LAB_00104b56:
  matcher_error();
LAB_00104b5b:
  if (break_file == (char *)0x0) {
LAB_00104d27:
    if (gnu_extensions == 0) {
      puVar32 = (undefined8 *)word_fastmap;
      for (lVar15 = 0x20; lVar15 != 0; lVar15 = lVar15 + -1) {
        *puVar32 = 0x101010101010101;
        puVar32 = puVar32 + (ulong)bVar38 * -2 + 1;
      }
      word_fastmap[32] = 0;
      word_fastmap._9_2_ = 0;
    }
    else {
      ppuVar11 = __ctype_b_loc();
      lVar15 = 0;
      pbVar36 = word_fastmap;
      do {
        puVar2 = (undefined2 *)((long)*ppuVar11 + lVar15);
        lVar15 = lVar15 + 2;
        *pbVar36 = (byte)((ushort)*puVar2 >> 10) & 1;
        pbVar36 = pbVar36 + 1;
      } while (lVar15 != 0x200);
    }
  }
  else {
LAB_00104b6b:
    swallow_file_in_memory();
    puVar32 = (undefined8 *)word_fastmap;
    for (lVar15 = 0x20; lVar15 != 0; lVar15 = lVar15 + -1) {
      *puVar32 = 0x101010101010101;
      puVar32 = puVar32 + (ulong)bVar38 * -2 + 1;
    }
    pbVar36 = local_68;
    if (local_68 < uStack96) {
      do {
        bVar3 = *pbVar36;
        pbVar36 = pbVar36 + 1;
        word_fastmap[bVar3] = 0;
      } while (pbVar36 != uStack96);
    }
    if (gnu_extensions == 0) {
      word_fastmap[32] = 0;
      word_fastmap._9_2_ = 0;
    }
    free(local_68);
  }
  goto LAB_00104067;
}