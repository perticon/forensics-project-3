print_spaces (ptrdiff_t number)
{
  for (ptrdiff_t counter = number; counter > 0; counter--)
    putchar (' ');
}