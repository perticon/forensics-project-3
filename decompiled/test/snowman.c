
struct s0 {
    struct s0* f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
    signed char[4] pad8;
    struct s0* f8;
    signed char[7] pad16;
    uint32_t f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[7] pad40;
    uint64_t f28;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    uint64_t f40;
    signed char[8] pad80;
    int64_t f50;
};

uint32_t pos = 0;

struct s0* argv = reinterpret_cast<struct s0*>(0);

struct s1 {
    unsigned char f0;
    unsigned char f1;
};

uint32_t binop(struct s0* rdi, struct s0* rsi, ...);

struct s2 {
    unsigned char f0;
    signed char f1;
};

struct s3 {
    signed char f0;
    signed char f1;
};

uint32_t argc = 0;

uint32_t unary_operator(struct s0* rdi, struct s0* rsi);

void beyond();

uint32_t binary_operator(uint32_t edi, struct s0* rsi);

struct s0* quote(struct s0* rdi, ...);

struct s0* fun_2420();

int64_t* test_syntax_error(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, int64_t r9);

struct s0* quote_n();

struct s4 {
    signed char f0;
    signed char f1;
};

struct s0* g28;

int32_t fun_24b0(struct s0* rdi, void* rsi);

void fun_2450();

int64_t fun_2370();

int32_t fun_24f0(struct s0* rdi, struct s0* rsi, ...);

struct s0* find_int(struct s0* rdi, ...);

struct s0* fun_2440(struct s0* rdi, ...);

struct s0* umaxtostr(struct s0* rdi, void* rsi);

uint32_t strintcmp(struct s0* rdi, struct s0* rsi);

int32_t fun_2500(int64_t rdi, void* rsi, ...);

uint32_t two_arguments();

uint32_t three_arguments();

uint32_t posixtest(struct s0* rdi, struct s0* rsi) {
    void* rsp3;
    void* rsp4;
    struct s0* v5;
    struct s0* r13_6;
    struct s0* v7;
    struct s0* r12_8;
    struct s0* rax9;
    struct s0* rbx10;
    struct s1* rax11;
    struct s0* rbp12;
    uint32_t eax13;
    void* rsp14;
    uint32_t edx15;
    struct s3* rax16;
    struct s0* rax17;
    int1_t less18;
    void* rsp19;
    void* rsp20;
    int64_t rbx21;
    int64_t rdx22;
    unsigned char v23;
    uint32_t ebp24;
    int1_t zf25;
    struct s0* rcx26;
    uint32_t ebx27;
    void* r14_28;
    int64_t r15_29;
    struct s0* rcx30;
    struct s0* r8_31;
    struct s0* rdi32;
    uint32_t eax33;
    int64_t r9_34;
    uint32_t eax35;
    uint32_t ecx36;
    struct s0** rcx37;
    uint32_t r8d38;
    struct s0* rdx39;
    struct s0* rax40;
    struct s0* rax41;
    struct s0* rax42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* rdx45;
    int1_t less46;
    int1_t less47;
    void* rsp48;
    int64_t rax49;
    int64_t rdx50;
    int64_t rdx51;
    struct s0* rax52;
    struct s0* rdi53;
    int32_t edx54;
    struct s0* rax55;
    struct s0* rax56;
    int64_t* rax57;
    void* rsp58;
    uint32_t edx59;
    int1_t less60;
    void* rsp61;
    uint32_t eax62;
    int32_t eax63;
    uint32_t eax64;
    uint32_t v65;
    void* rdx66;
    void* rsp67;
    struct s0* rax68;
    int64_t rax69;
    uint32_t edx70;
    struct s0* rcx71;
    struct s0* rbp72;
    int64_t r12_73;
    int64_t rbx74;
    struct s0* rdi75;
    void* r13_76;
    uint32_t eax77;
    uint32_t eax78;
    uint32_t edx79;
    uint32_t eax80;
    int64_t rax81;
    int64_t rbx82;
    int64_t rax83;
    struct s0* rsi84;
    struct s0* rdi85;
    int32_t eax86;
    int64_t rax87;
    int64_t rbx88;
    int64_t rax89;
    struct s0* rsi90;
    struct s0* rdi91;
    int32_t eax92;
    uint32_t eax93;
    struct s0* rdi94;
    struct s0* rax95;
    void* rsp96;
    struct s0* rbp97;
    struct s0* rax98;
    void* rsp99;
    struct s0* rax100;
    struct s0* rax101;
    struct s0* rax102;
    int64_t* rax103;
    struct s0* rax104;
    struct s0* rdi105;
    struct s0* rax106;
    struct s0* rsi107;
    struct s0* rdi108;
    struct s0* rax109;
    struct s0* rax110;
    uint32_t eax111;
    struct s0* rdx112;
    unsigned char dl113;
    uint32_t tmp32_114;
    int64_t rdi115;
    int32_t eax116;
    void* rax117;
    struct s0* rsi118;
    struct s0* rax119;
    uint32_t tmp32_120;
    int64_t rdi121;
    int32_t eax122;
    int64_t rdi123;
    int32_t eax124;
    int64_t v125;
    int64_t v126;
    int64_t v127;
    int64_t v128;
    uint32_t eax129;
    uint32_t tmp32_130;
    int64_t rdi131;
    void* r14_132;
    int32_t eax133;
    int64_t* rdx134;
    int64_t rdi135;
    int32_t eax136;
    int64_t rdi137;
    int32_t eax138;
    int32_t edx139;
    int64_t v140;
    int64_t v141;
    int64_t rdx142;
    int64_t v143;
    int32_t eax144;
    int64_t v145;
    int64_t v146;
    int64_t v147;
    int64_t rax148;
    uint32_t tmp32_149;
    int64_t rdi150;
    int32_t eax151;
    int64_t rdi152;
    int64_t v153;
    int32_t eax154;
    int32_t edx155;
    int64_t v156;
    int64_t rdx157;
    int64_t v158;
    int32_t eax159;
    int64_t v160;
    int64_t v161;
    int64_t v162;
    int64_t rax163;
    int32_t eax164;
    struct s0* rax165;
    struct s0* rax166;
    uint32_t eax167;
    uint32_t eax168;
    int1_t less169;
    uint32_t eax170;
    struct s0* rax171;
    uint32_t tmp32_172;
    int64_t rax173;
    struct s0* rdx174;
    signed char* rax175;
    int64_t rdx176;

    while (1) {
        rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
        if (*reinterpret_cast<uint32_t*>(&rdi) == 3) {
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) + 8);
            while (1) {
                v5 = r13_6;
                v7 = r12_8;
                rax9 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                r12_8 = argv;
                rbx10 = rax9;
                rax11 = reinterpret_cast<struct s1*>(&rax9->f1);
                rbp12 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rax11) * 8);
                r13_6 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax11) * 8);
                rdi = rbp12;
                eax13 = binop(rdi, rsi, rdi, rsi);
                rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 - 8 - 8 - 8 - 8 - 8 + 8);
                if (*reinterpret_cast<signed char*>(&eax13)) 
                    goto addr_3d78_4;
                edx15 = (*reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(r13_6) + 0xfffffffffffffff8))->f0;
                if (edx15 != 33) 
                    goto addr_3d00_6;
                if (!(*reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(r13_6) + 0xfffffffffffffff8))->f1) 
                    break;
                addr_3d00_6:
                if (edx15 != 40) 
                    goto addr_3d30_8;
                if ((*reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(r13_6) + 0xfffffffffffffff8))->f1) 
                    goto addr_3d30_8;
                rax16 = *reinterpret_cast<struct s3**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(r13_6) + 8);
                if (rax16->f0 != 41) 
                    goto addr_3d30_8;
                if (!rax16->f1) 
                    goto addr_3d1b_12;
                addr_3d30_8:
                *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp12->f0));
                if (*reinterpret_cast<uint32_t*>(&rax17) != 45) 
                    goto addr_3d4a_13;
                if (rbp12->f1 != 97 || rbp12->f2) {
                    if (*reinterpret_cast<uint32_t*>(&rax17) != 45) 
                        goto addr_3d4a_13;
                    if (rbp12->f1 != 0x6f) 
                        goto addr_3d4a_13;
                    if (rbp12->f2) 
                        goto addr_3db6_18;
                }
                less18 = *reinterpret_cast<int32_t*>(&rbx10) < reinterpret_cast<int32_t>(argc);
                if (!less18) 
                    goto addr_3db8_20;
                r12_8 = v7;
                r13_6 = v5;
                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) + 8 + 8 + 8 + 8 + 8);
                addr_3900_22:
                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
                *reinterpret_cast<uint32_t*>(&rbx21) = pos;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx22) = argc;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                v23 = 0;
                while (1) {
                    ebp24 = 1;
                    zf25 = *reinterpret_cast<uint32_t*>(&rbx21) == *reinterpret_cast<uint32_t*>(&rdx22);
                    if (*reinterpret_cast<int32_t*>(&rbx21) >= *reinterpret_cast<int32_t*>(&rdx22)) 
                        goto addr_3a9d_24;
                    goto addr_3930_26;
                    addr_3a5a_27:
                    v23 = reinterpret_cast<unsigned char>(v23 | *reinterpret_cast<unsigned char*>(&ebp24));
                    if (*reinterpret_cast<uint32_t*>(&rcx26) != 45) 
                        goto addr_3a68_28;
                    if (rax17->f1 != 0x6f) 
                        goto addr_3a68_28;
                    if (rax17->f2) 
                        goto addr_3a68_28;
                    *reinterpret_cast<uint32_t*>(&rbx21) = ebx27 + 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
                    pos = *reinterpret_cast<uint32_t*>(&rbx21);
                    continue;
                    while (1) {
                        addr_3a0c_32:
                        if (*reinterpret_cast<signed char*>(&r14_28) != 45 || !r13_6->f1) {
                            addr_3a28_33:
                            ebx27 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx26->f1));
                            pos = ebx27;
                            *reinterpret_cast<unsigned char*>(&rax17) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&r14_28));
                        } else {
                            if (!r13_6->f2) {
                                *reinterpret_cast<uint32_t*>(&rax17) = unary_operator(rdi, rsi);
                                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                                ebx27 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx22) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                            } else {
                                goto addr_3a28_33;
                            }
                        }
                        while (1) {
                            ebp24 = ebp24 & (*reinterpret_cast<uint32_t*>(&rax17) ^ *reinterpret_cast<uint32_t*>(&r15_29));
                            if (reinterpret_cast<int32_t>(ebx27) >= *reinterpret_cast<int32_t*>(&rdx22)) 
                                goto addr_3af0_38;
                            while ((rcx30 = argv, rax17 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx30) + reinterpret_cast<int32_t>(ebx27) * 8), *reinterpret_cast<uint32_t*>(&rcx26) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax17->f0)), *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx26) == 45) && (rax17->f1 == 97 && !rax17->f2)) {
                                *reinterpret_cast<uint32_t*>(&rbx21) = ebx27 + 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
                                pos = *reinterpret_cast<uint32_t*>(&rbx21);
                                zf25 = *reinterpret_cast<uint32_t*>(&rbx21) == *reinterpret_cast<uint32_t*>(&rdx22);
                                if (*reinterpret_cast<int32_t*>(&rbx21) < *reinterpret_cast<int32_t*>(&rdx22)) {
                                    addr_3930_26:
                                    r8_31 = argv;
                                    rbx21 = *reinterpret_cast<int32_t*>(&rbx21);
                                    *reinterpret_cast<uint32_t*>(&rdi) = 0;
                                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&r15_29) = 0;
                                    while (r13_6 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + rbx21 * 8), *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<uint32_t*>(&rbx21), *reinterpret_cast<int32_t*>(&rax17 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<uint32_t*>(&rbx21), *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0, *reinterpret_cast<uint32_t*>(&r14_28) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r13_6->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_28) + 4) = 0, *reinterpret_cast<signed char*>(&r14_28) == 33) {
                                        if (r13_6->f1) 
                                            goto addr_3ab8_43;
                                        ++rbx21;
                                        *reinterpret_cast<uint32_t*>(&r12_8) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rax17->f1));
                                        if (*reinterpret_cast<int32_t*>(&rdx22) <= *reinterpret_cast<int32_t*>(&rbx21)) 
                                            goto addr_3bf6_45;
                                        *reinterpret_cast<uint32_t*>(&r15_29) = *reinterpret_cast<uint32_t*>(&r15_29) ^ 1;
                                        *reinterpret_cast<uint32_t*>(&rdi) = 1;
                                        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                                    }
                                } else {
                                    addr_3a9d_24:
                                    beyond();
                                    rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                                    goto addr_3aa8_47;
                                }
                                if (*reinterpret_cast<signed char*>(&rdi)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_8);
                                }
                                if (*reinterpret_cast<signed char*>(&r14_28) != 40 || r13_6->f1) {
                                    addr_399c_51:
                                    *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<uint32_t*>(&rdx22) - *reinterpret_cast<uint32_t*>(&rcx26);
                                    zf25 = *reinterpret_cast<uint32_t*>(&rax17) == 3;
                                    if (*reinterpret_cast<int32_t*>(&rax17) <= reinterpret_cast<int32_t>(3)) {
                                        addr_3aa8_47:
                                        if (!zf25) 
                                            goto addr_3a0c_32;
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_6->f0) == 45)) 
                                            goto addr_39ea_53;
                                        if (r13_6->f1 != 0x6c) 
                                            goto addr_39ea_53;
                                        if (r13_6->f2) 
                                            goto addr_39ea_53;
                                        rdi32 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + rbx21 * 8 + 16);
                                        eax33 = binop(rdi32, rsi);
                                        rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                                        r8_31 = r8_31;
                                        *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<uint32_t*>(&rcx26);
                                        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx22);
                                        if (*reinterpret_cast<signed char*>(&eax33)) 
                                            goto addr_3ba3_57; else 
                                            goto addr_39ea_53;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r9_34) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx26->f1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_34) + 4) = 0;
                                    pos = *reinterpret_cast<uint32_t*>(&r9_34);
                                    zf25 = *reinterpret_cast<uint32_t*>(&r9_34) == *reinterpret_cast<uint32_t*>(&rdx22);
                                    if (*reinterpret_cast<int32_t*>(&r9_34) >= *reinterpret_cast<int32_t*>(&rdx22)) 
                                        goto addr_3a9d_24; else 
                                        goto addr_3b10_59;
                                }
                                addr_39ea_53:
                                rdi = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + rbx21 * 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rax17) = binop(rdi, rsi);
                                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<uint32_t*>(&rcx26);
                                *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx22);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                                if (!*reinterpret_cast<unsigned char*>(&rax17)) 
                                    goto addr_3a0c_32;
                                *reinterpret_cast<uint32_t*>(&rdi) = 0;
                                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                                eax35 = binary_operator(0, rsi);
                                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                                ebx27 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx22) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                                ebp24 = ebp24 & (eax35 ^ *reinterpret_cast<uint32_t*>(&r15_29));
                                if (reinterpret_cast<int32_t>(ebx27) < *reinterpret_cast<int32_t*>(&rdx22)) 
                                    continue; else 
                                    goto addr_3af0_38;
                                addr_3ab8_43:
                                if (*reinterpret_cast<signed char*>(&rdi)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_8);
                                    goto addr_399c_51;
                                }
                            }
                            goto addr_3a5a_27;
                            addr_3bf6_45:
                            pos = *reinterpret_cast<uint32_t*>(&r12_8);
                            beyond();
                            rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                            goto addr_3c02_63;
                            addr_3ba3_57:
                            *reinterpret_cast<uint32_t*>(&rdi) = 1;
                            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rax17) = binary_operator(1, rsi);
                            rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                            ebx27 = pos;
                            *reinterpret_cast<uint32_t*>(&rdx22) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                            continue;
                            addr_3b10_59:
                            ecx36 = *reinterpret_cast<uint32_t*>(&rcx26) + 2;
                            if (reinterpret_cast<int32_t>(ecx36) >= *reinterpret_cast<int32_t*>(&rdx22)) {
                                addr_3c02_63:
                                *reinterpret_cast<uint32_t*>(&rdi) = 1;
                                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rdi) = 1;
                                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                                rcx37 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + reinterpret_cast<int32_t>(ecx36) * 8);
                                r8d38 = static_cast<int32_t>(rdx22 - 1) - *reinterpret_cast<uint32_t*>(&rax17);
                                do {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*rcx37)->f0) == 41)) 
                                        goto addr_3b3c_66;
                                    if (!(*rcx37)->f1) 
                                        break;
                                    addr_3b3c_66:
                                    if (*reinterpret_cast<uint32_t*>(&rdi) == 4) 
                                        goto addr_3b9c_68;
                                    *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) + 1;
                                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                                    rcx37 = rcx37 + 8;
                                } while (*reinterpret_cast<uint32_t*>(&rdi) != r8d38);
                                goto addr_3b4d_70;
                            }
                            addr_3b50_71:
                            *reinterpret_cast<uint32_t*>(&rax17) = posixtest(rdi, rsi);
                            rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
                            rcx26 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                            rdx39 = argv;
                            r8_31 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx39) + reinterpret_cast<unsigned char>(rcx26) * 8);
                            rbx10 = rcx26;
                            if (!r8_31) 
                                goto addr_3c0c_72;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r8_31->f0) == 41)) 
                                goto addr_3c3b_74;
                            if (r8_31->f1) 
                                goto addr_3c3b_74;
                            ebx27 = *reinterpret_cast<uint32_t*>(&rbx10) + 1;
                            *reinterpret_cast<uint32_t*>(&rdx22) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                            pos = ebx27;
                            continue;
                            addr_3b9c_68:
                            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdx22) - *reinterpret_cast<uint32_t*>(&r9_34);
                            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                            goto addr_3b50_71;
                            addr_3b4d_70:
                            goto addr_3b50_71;
                        }
                    }
                }
                addr_3c0c_72:
                rax40 = quote(")", ")");
                rax41 = fun_2420();
                test_syntax_error(rax41, rax40, 5, rcx26, r8_31, r9_34);
                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_3c3b_74:
                rax42 = quote_n();
                r12_8 = rax42;
                rax43 = quote_n();
                rax44 = fun_2420();
                rsi = rax43;
                test_syntax_error(rax44, rsi, r12_8, rcx26, r8_31, r9_34);
                rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rdi) > reinterpret_cast<int32_t>(3)) {
                if (*reinterpret_cast<uint32_t*>(&rdi) != 4) {
                    addr_3e60_79:
                    *reinterpret_cast<uint32_t*>(&rax17) = pos;
                } else {
                    rdx45 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                    rsi = argv;
                    rax17 = rdx45;
                    rdi = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx45) * 8);
                    *reinterpret_cast<uint32_t*>(&rcx26) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx45) * 8))->f0));
                    *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rcx26) != 33) 
                        goto addr_3e1e_81;
                    if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx45) * 8))->f1) 
                        goto addr_3eca_83;
                    addr_3e1e_81:
                    if (*reinterpret_cast<uint32_t*>(&rcx26) != 40) 
                        goto addr_3e66_84;
                    if ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx45) * 8))->f1) 
                        goto addr_3e66_84;
                    if ((*reinterpret_cast<struct s4**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdi) + 24))->f0 != 41) 
                        goto addr_3e66_84;
                    if (!(*reinterpret_cast<struct s4**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdi) + 24))->f1) 
                        goto addr_3e39_88;
                }
                addr_3e66_84:
                less46 = *reinterpret_cast<int32_t*>(&rax17) < reinterpret_cast<int32_t>(argc);
                if (!less46) 
                    goto addr_3ee5_89;
                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) + 8);
                goto addr_3900_22;
            }
            if (*reinterpret_cast<uint32_t*>(&rdi) == 1) 
                goto addr_3e90_92;
            if (*reinterpret_cast<uint32_t*>(&rdi) != 2) 
                goto addr_3e50_94; else 
                goto addr_3ddd_95;
        }
        *reinterpret_cast<uint32_t*>(&rbx10) = *reinterpret_cast<uint32_t*>(&rbx10) + 1;
        less47 = *reinterpret_cast<int32_t*>(&rbx10) < reinterpret_cast<int32_t>(argc);
        pos = *reinterpret_cast<uint32_t*>(&rbx10);
        if (less47) 
            goto addr_3ce3_97;
        addr_3db8_20:
        beyond();
        continue;
        addr_3e50_94:
        *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) - 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdi)) 
            goto addr_2665_98;
        goto addr_3e60_79;
        addr_3ddd_95:
        rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) + 8);
        while ((rax49 = reinterpret_cast<int32_t>(pos), rcx26 = argv, rdx50 = rax49, rsi = reinterpret_cast<struct s0*>(rax49 * 8), rax17 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx26) + rax49 * 8), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax17->f0) == 33)) || rax17->f1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax17->f0) == 45)) 
                goto addr_38f6_102;
            if (!rax17->f1) 
                goto addr_38f6_102;
            if (rax17->f2) 
                goto addr_38f6_102;
            rdx51 = reinterpret_cast<int32_t>(pos);
            r8_31 = argv;
            rax52 = g28;
            rdi53 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + rdx51 * 8);
            *reinterpret_cast<uint32_t*>(&rbx10) = rdi53->f1;
            *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0;
            edx54 = static_cast<int32_t>(reinterpret_cast<unsigned char>(rbx10) + 0xffffffffffffffb9);
            if (*reinterpret_cast<unsigned char*>(&edx54) <= 51) 
                goto addr_3165_107;
            rax55 = quote(rdi53, rdi53);
            rax56 = fun_2420();
            rdi = rax56;
            rax57 = test_syntax_error(rdi, rax55, 5, rcx26, r8_31, r9_34);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
            edx59 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax57) + 1);
            less60 = reinterpret_cast<int32_t>(edx59) < reinterpret_cast<int32_t>(argc);
            pos = edx59;
            if (!less60) {
                beyond();
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
            } else {
                eax62 = *reinterpret_cast<int32_t*>(&rax57) + 2;
                pos = eax62;
                rdi = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_31) + reinterpret_cast<int32_t>(eax62) * 8 - 8);
                eax63 = fun_24b0(rdi, rsp58);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r8_31) = eax63;
                *reinterpret_cast<int32_t*>(&r8_31 + 4) = 0;
                eax64 = 0;
                if (!*reinterpret_cast<int32_t*>(&r8_31)) {
                    eax64 = v65 & 0xf000;
                    *reinterpret_cast<unsigned char*>(&eax64) = reinterpret_cast<uint1_t>(eax64 == 0xa000);
                }
                rdx66 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax52) - reinterpret_cast<unsigned char>(g28));
                if (!rdx66) 
                    goto addr_3207_113;
            }
            fun_2450();
            rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
        }
        goto addr_38c8_115;
        addr_38f6_102:
        beyond();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 - 8 + 8);
        goto addr_3900_22;
    }
    addr_3d78_4:
    rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax68 = g28;
    *reinterpret_cast<uint32_t*>(&rax69) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
    if (0) {
        *reinterpret_cast<uint32_t*>(&rax69) = *reinterpret_cast<uint32_t*>(&rax69) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax69);
    }
    edx70 = argc;
    *reinterpret_cast<uint32_t*>(&rcx71) = static_cast<uint32_t>(rax69 + 1);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    rbp72 = argv;
    *reinterpret_cast<uint32_t*>(&r12_73) = 0;
    if (reinterpret_cast<int32_t>(edx70 - 2) > *reinterpret_cast<int32_t*>(&rcx71)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f2)) {
            *reinterpret_cast<uint32_t*>(&r12_73) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx71);
            *reinterpret_cast<uint32_t*>(&r12_73) = 1;
        }
    }
    rbx74 = *reinterpret_cast<int32_t*>(&rcx71);
    rdi75 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + rbx74 * 8);
    r13_76 = reinterpret_cast<void*>(rbx74 * 8);
    eax77 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi75->f0));
    if (*reinterpret_cast<signed char*>(&eax77) == 45) {
        eax78 = rdi75->f1;
        if ((*reinterpret_cast<signed char*>(&eax78) == 0x6c || *reinterpret_cast<signed char*>(&eax78) == 0x67) && ((edx79 = rdi75->f2, *reinterpret_cast<signed char*>(&edx79) == 0x65) || *reinterpret_cast<signed char*>(&edx79) == 0x74)) {
            if (rdi75->f3) 
                goto addr_2d44_125; else 
                goto addr_2ea4_126;
        }
        if (*reinterpret_cast<signed char*>(&eax78) != 0x65) 
            goto addr_2d3c_128;
    } else {
        if (*reinterpret_cast<signed char*>(&eax77) != 61 || (eax80 = rdi75->f1, !!*reinterpret_cast<signed char*>(&eax80)) && (*reinterpret_cast<signed char*>(&eax80) != 61 || rdi75->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi75->f0) == 33) || (rdi75->f1 != 61 || rdi75->f2)) {
                fun_2370();
                goto addr_2665_98;
            } else {
                rax81 = reinterpret_cast<int32_t>(pos);
                rbx82 = rax81;
                rax83 = rax81 + 2;
                rsi84 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8));
                rdi85 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8) - 16);
                eax86 = fun_24f0(rdi85, rsi84);
                *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(!!eax86);
                pos = *reinterpret_cast<int32_t*>(&rbx82) + 3;
                goto addr_2cc9_133;
            }
        } else {
            rax87 = reinterpret_cast<int32_t>(pos);
            rbx88 = rax87;
            rax89 = rax87 + 2;
            rsi90 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax89 * 8));
            rdi91 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax89 * 8) - 16);
            eax92 = fun_24f0(rdi91, rsi90);
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(eax92 == 0);
            pos = *reinterpret_cast<int32_t*>(&rbx88) + 3;
            goto addr_2cc9_133;
        }
    }
    eax93 = rdi75->f2;
    if (*reinterpret_cast<signed char*>(&eax93) == 0x71) {
        addr_300c_136:
        if (!rdi75->f3) {
            addr_2ea4_126:
            rdi94 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
            if (1) {
                rax95 = find_int(rdi94);
                rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rbp97 = rax95;
            } else {
                rax98 = fun_2440(rdi94);
                rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rax100 = umaxtostr(rax98, reinterpret_cast<int64_t>(rsp99) + 0x120);
                rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
                rbp97 = rax100;
            }
        } else {
            addr_3016_139:
            rax101 = quote(rdi75);
            rax102 = fun_2420();
            rax103 = test_syntax_error(rax102, rax101, 5, rcx71, r8_31, r9_34);
            goto addr_3040_140;
        }
    } else {
        addr_2e0c_141:
        if (*reinterpret_cast<signed char*>(&eax93) != 0x66) 
            goto addr_3016_139;
        if (rdi75->f3) 
            goto addr_3016_139; else 
            goto addr_2e1e_143;
    }
    rax104 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi105 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax104) + reinterpret_cast<uint64_t>(r13_76) + 8);
        rax106 = find_int(rdi105, rdi105);
        rsi107 = rax106;
    } else {
        rdi108 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax104) + reinterpret_cast<uint64_t>(r13_76) + 16);
        rax109 = fun_2440(rdi108, rdi108);
        rax110 = umaxtostr(rax109, reinterpret_cast<int64_t>(rsp96) - 8 + 8 + 0x140);
        rsi107 = rax110;
    }
    eax111 = strintcmp(rbp97, rsi107);
    rdx112 = argv;
    *reinterpret_cast<uint32_t*>(&rcx71) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx112) + rbx74 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    dl113 = reinterpret_cast<uint1_t>((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx112) + rbx74 * 8))->f2 == 0x65);
    tmp32_114 = pos + 3;
    pos = tmp32_114;
    if (*reinterpret_cast<signed char*>(&rcx71) == 0x6c) {
        *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(static_cast<uint32_t>(dl113)) > reinterpret_cast<int32_t>(eax111));
        goto addr_2cc9_133;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx71) == 0x67) {
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(-static_cast<uint32_t>(dl113)) < reinterpret_cast<int32_t>(eax111));
            goto addr_2cc9_133;
        } else {
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!eax111)) == dl113);
            goto addr_2cc9_133;
        }
    }
    addr_3040_140:
    rdi115 = *rax103;
    eax116 = fun_2500(rdi115, r14_28, rdi115, r14_28);
    if (!eax116) {
        addr_2cc9_133:
        rax117 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax68) - reinterpret_cast<unsigned char>(g28));
        if (rax117) {
            fun_2450();
        } else {
            return *reinterpret_cast<uint32_t*>(&r12_73);
        }
    } else {
        addr_3053_154:
        *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx74) == 0);
        goto addr_2cc9_133;
    }
    rsi118 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_156:
        rax119 = fun_2420();
        test_syntax_error(rax119, rsi118, 5, rcx71, r8_31, r9_34);
        addr_3102_157:
        rsi118 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_143:
    tmp32_120 = pos + 3;
    pos = tmp32_120;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi121 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
        eax122 = fun_2500(rdi121, rsp67);
        if (!eax122 && ((rdi123 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8), eax124 = fun_2500(rdi123, reinterpret_cast<int64_t>(rsp67) - 8 + 8 + 0x90), !eax124) && v125 == v126)) {
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(v127 == v128);
            goto addr_2cc9_133;
        }
    }
    addr_2d3c_128:
    if (*reinterpret_cast<signed char*>(&eax78) == 0x6e) {
        eax129 = rdi75->f2;
        if (*reinterpret_cast<signed char*>(&eax129) == 0x65) 
            goto addr_300c_136;
    } else {
        addr_2d44_125:
        if (*reinterpret_cast<signed char*>(&eax78) != 0x6f) {
            if (*reinterpret_cast<signed char*>(&eax78) > 0x6f) 
                goto addr_3016_139;
            if (*reinterpret_cast<signed char*>(&eax78) == 0x65) 
                goto addr_30a8_163; else 
                goto addr_2f4e_164;
        } else {
            if (rdi75->f2 != 0x74) 
                goto addr_3016_139;
            if (rdi75->f3) 
                goto addr_3016_139;
            tmp32_130 = pos + 3;
            pos = tmp32_130;
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
            if (*reinterpret_cast<unsigned char*>(&r12_73)) {
                rsi118 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                goto addr_30f1_156;
            } else {
                rdi131 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
                r14_132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
                eax133 = fun_2500(rdi131, r14_132);
                rdx134 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
                if (eax133) {
                    rdi135 = *rdx134;
                    eax136 = fun_2500(rdi135, r14_132);
                    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(eax136 == 0);
                    goto addr_2cc9_133;
                } else {
                    rdi137 = *rdx134;
                    eax138 = fun_2500(rdi137, r14_132);
                    if (!eax138) {
                        edx139 = 0;
                        *reinterpret_cast<unsigned char*>(&edx139) = reinterpret_cast<uint1_t>(v140 > v141);
                        *reinterpret_cast<uint32_t*>(&rdx142) = edx139 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(v140 < v143));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx142) + 4) = 0;
                        eax144 = 0;
                        *reinterpret_cast<unsigned char*>(&eax144) = reinterpret_cast<uint1_t>(v145 > v146);
                        *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v145 < v147);
                        *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rax148) = eax144 - *reinterpret_cast<uint32_t*>(&rcx71);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax148) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r12_73) = static_cast<uint32_t>(rax148 + rdx142 * 2) >> 31;
                        goto addr_2cc9_133;
                    }
                }
            }
        }
    }
    addr_2f5a_173:
    if (*reinterpret_cast<signed char*>(&eax129) != 0x74) 
        goto addr_3016_139;
    if (rdi75->f3) 
        goto addr_3016_139;
    tmp32_149 = pos + 3;
    pos = tmp32_149;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (*reinterpret_cast<unsigned char*>(&r12_73)) 
        goto addr_3102_157;
    rdi150 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
    r14_28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
    eax151 = fun_2500(rdi150, r14_28);
    *reinterpret_cast<int32_t*>(&rbx74) = eax151;
    rax103 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
    if (*reinterpret_cast<int32_t*>(&rbx74)) 
        goto addr_3040_140;
    rdi152 = *rax103;
    r12_73 = v153;
    eax154 = fun_2500(rdi152, r14_28);
    if (eax154) 
        goto addr_3053_154;
    edx155 = 0;
    *reinterpret_cast<unsigned char*>(&edx155) = reinterpret_cast<uint1_t>(r12_73 > v156);
    *reinterpret_cast<uint32_t*>(&rdx157) = edx155 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_73 < v158));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx157) + 4) = 0;
    eax159 = 0;
    *reinterpret_cast<unsigned char*>(&eax159) = reinterpret_cast<uint1_t>(v160 > v161);
    *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v160 < v162);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax163) = eax159 - *reinterpret_cast<uint32_t*>(&rcx71);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax163) + 4) = 0;
    eax164 = static_cast<int32_t>(rax163 + rdx157 * 2);
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax164 < 0) | reinterpret_cast<uint1_t>(eax164 == 0)));
    goto addr_2cc9_133;
    addr_30a8_163:
    eax93 = rdi75->f2;
    goto addr_2e0c_141;
    addr_2f4e_164:
    if (*reinterpret_cast<signed char*>(&eax78) != 0x6e) 
        goto addr_3016_139;
    eax129 = rdi75->f2;
    goto addr_2f5a_173;
    addr_2665_98:
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    addr_3d1b_12:
    *reinterpret_cast<unsigned char*>(&rax16) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<struct s0**>(&rbp12->f0));
    pos = *reinterpret_cast<uint32_t*>(&rbx10) + 3;
    addr_3ceb_194:
    return *reinterpret_cast<uint32_t*>(&rax16);
    addr_3d4a_13:
    rax165 = quote(rbp12, rbp12);
    rax166 = fun_2420();
    test_syntax_error(rax166, rax165, 5, rcx26, r8_31, r9_34);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d78_4;
    addr_3db6_18:
    goto addr_3d4a_13;
    addr_3af0_38:
    v23 = reinterpret_cast<unsigned char>(v23 | *reinterpret_cast<unsigned char*>(&ebp24));
    addr_3a68_28:
    return static_cast<uint32_t>(v23);
    addr_3ce3_97:
    eax167 = two_arguments();
    *reinterpret_cast<uint32_t*>(&rax16) = eax167 ^ 1;
    goto addr_3ceb_194;
    addr_3eca_83:
    eax168 = *reinterpret_cast<uint32_t*>(&rax17) + 1;
    less169 = reinterpret_cast<int32_t>(eax168) < reinterpret_cast<int32_t>(argc);
    pos = eax168;
    if (!less169) {
        addr_3ee5_89:
        beyond();
    } else {
        eax170 = three_arguments();
        *reinterpret_cast<uint32_t*>(&rax171) = eax170 ^ 1;
    }
    addr_3eb1_196:
    return *reinterpret_cast<uint32_t*>(&rax171);
    addr_3e39_88:
    pos = *reinterpret_cast<uint32_t*>(&rax17) + 1;
    *reinterpret_cast<uint32_t*>(&rax171) = two_arguments();
    tmp32_172 = pos + 1;
    pos = tmp32_172;
    goto addr_3eb1_196;
    addr_3e90_92:
    rax173 = reinterpret_cast<int32_t>(pos);
    pos = static_cast<uint32_t>(rax173 + 1);
    rdx174 = argv;
    rax171 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx174) + rax173 * 8);
    *reinterpret_cast<unsigned char*>(&rax171) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<struct s0**>(&rax171->f0));
    goto addr_3eb1_196;
    addr_38c8_115:
    rax175 = *reinterpret_cast<signed char**>(reinterpret_cast<unsigned char>(rcx26) + reinterpret_cast<unsigned char>(rsi) + 8);
    pos = *reinterpret_cast<int32_t*>(&rdx50) + 2;
    *reinterpret_cast<unsigned char*>(&rax175) = reinterpret_cast<uint1_t>(*rax175 == 0);
    return *reinterpret_cast<uint32_t*>(&rax175);
    addr_3165_107:
    *reinterpret_cast<uint32_t*>(&rdx176) = *reinterpret_cast<unsigned char*>(&edx54);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx176) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa1f0 + rdx176 * 4) + 0xa1f0;
    addr_3207_113:
    return eax64;
}

void verror();

void fun_2600(struct s0* rdi, ...);

unsigned char** fun_2650(struct s0* rdi, ...);

int64_t* test_syntax_error(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, int64_t r9) {
    int64_t rax7;
    int64_t rax8;
    unsigned char** rax9;
    void* rsp10;
    struct s0* r8_11;
    unsigned char* rsi12;
    int64_t rdx13;
    int64_t rax14;
    struct s0* rdx15;
    void* rdx16;
    struct s1* rax17;
    struct s0* rcx18;
    int32_t ecx19;
    struct s0* rdx20;
    int32_t ecx21;
    struct s0* rax22;
    struct s0* rax23;
    int64_t rdx24;
    struct s0* rax25;
    struct s0* rdi26;
    struct s0* rax27;
    struct s0* rax28;
    struct s0* rdi29;
    int32_t esi30;
    void* rsp31;
    struct s0* rax32;
    int64_t rax33;
    uint32_t edx34;
    struct s0* rcx35;
    struct s0* rbp36;
    int64_t r12_37;
    int64_t rbx38;
    struct s0* rdi39;
    void* r13_40;
    uint32_t eax41;
    uint32_t eax42;
    int64_t rax43;
    int64_t rbx44;
    int64_t rax45;
    struct s0* rsi46;
    struct s0* rdi47;
    int64_t rax48;
    int64_t rbx49;
    int64_t rax50;
    struct s0* rsi51;
    struct s0* rdi52;
    uint32_t eax53;
    uint32_t edx54;
    uint32_t eax55;
    struct s0* rdi56;
    struct s0* rax57;
    void* rsp58;
    struct s0* rbp59;
    struct s0* rax60;
    void* rsp61;
    struct s0* rax62;
    struct s0* rax63;
    struct s0* rax64;
    int64_t* rax65;
    uint32_t eax66;
    uint32_t tmp32_67;
    struct s0* rsi68;
    int64_t rdi69;
    void* r14_70;
    int32_t eax71;
    int64_t* rdx72;
    int64_t rdi73;
    int64_t rdi74;
    int32_t eax75;
    int64_t v76;
    int64_t v77;
    struct s0* rax78;
    struct s0* rdi79;
    struct s0* rax80;
    struct s0* rsi81;
    struct s0* rdi82;
    struct s0* rax83;
    struct s0* rax84;
    struct s0* rdx85;
    uint32_t tmp32_86;
    int64_t rdi87;
    void* r14_88;
    int32_t eax89;
    void* rax90;
    struct s0* rax91;
    uint32_t tmp32_92;
    int64_t rdi93;
    int32_t eax94;
    int64_t rdi95;
    int32_t eax96;
    int64_t v97;
    int64_t v98;
    uint32_t tmp32_99;
    int64_t rdi100;
    int32_t eax101;
    int64_t rdi102;
    int32_t eax103;
    int64_t v104;
    int64_t v105;

    rax7 = rax8;
    if (*reinterpret_cast<signed char*>(&rax7)) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    verror();
    fun_2600(2);
    rax9 = fun_2650(2);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 - 0xd8 - 8 + 8 - 8 + 8 - 8 - 8 + 8);
    r8_11 = reinterpret_cast<struct s0*>(2);
    rsi12 = *rax9;
    while (*reinterpret_cast<uint32_t*>(&rdx13) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8_11->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0, rax14 = rdx13, !!(rsi12[rdx13 * 2] & 1)) {
        r8_11 = reinterpret_cast<struct s0*>(&r8_11->f1);
    }
    if (*reinterpret_cast<signed char*>(&rdx13) == 43) {
        rdx15 = reinterpret_cast<struct s0*>(&r8_11->f1);
        r8_11 = rdx15;
    } else {
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx16) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx16) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax14) == 45);
        rdx15 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdx16) + reinterpret_cast<unsigned char>(r8_11));
    }
    rax17 = reinterpret_cast<struct s1*>(&rdx15->f1);
    *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&rdx15->f0)) - 48;
    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rcx18) <= 9) {
        ecx19 = reinterpret_cast<signed char>(rdx15->f1);
        *reinterpret_cast<int32_t*>(&rdx20) = ecx19;
        if (ecx19 - 48 <= 9) {
            do {
                ecx21 = reinterpret_cast<signed char>(rax17->f1);
                rax17 = reinterpret_cast<struct s1*>(&rax17->f1);
                *reinterpret_cast<int32_t*>(&rdx20) = ecx21;
            } while (ecx21 - 48 <= 9);
        }
        *reinterpret_cast<uint32_t*>(&rcx18) = *reinterpret_cast<unsigned char*>(&rdx20);
        *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
        if (rsi12[reinterpret_cast<unsigned char>(rcx18) * 2] & 1) {
            do {
                *reinterpret_cast<uint32_t*>(&rcx18) = rax17->f1;
                *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                rax17 = reinterpret_cast<struct s1*>(&rax17->f1);
                rdx20 = rcx18;
            } while (rsi12[reinterpret_cast<unsigned char>(rcx18) * 2] & 1);
        }
        if (!*reinterpret_cast<unsigned char*>(&rdx20)) {
            goto 0x3000000008;
        }
    }
    rax22 = quote(2);
    rax23 = fun_2420();
    test_syntax_error(rax23, rax22, 5, rcx18, r8_11, r9);
    rdx24 = reinterpret_cast<int32_t>(argc);
    rax25 = argv;
    rdi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax25) + rdx24 * 8 - 8);
    rax27 = quote(rdi26, rdi26);
    rax28 = fun_2420();
    rdi29 = rax28;
    test_syntax_error(rdi29, rax27, 5, rcx18, r8_11, r9);
    esi30 = *reinterpret_cast<int32_t*>(&rdi29);
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax32 = g28;
    *reinterpret_cast<uint32_t*>(&rax33) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi29)) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<uint32_t*>(&rax33) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax33);
    }
    edx34 = argc;
    *reinterpret_cast<uint32_t*>(&rcx35) = static_cast<uint32_t>(rax33 + 1);
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    rbp36 = argv;
    *reinterpret_cast<int32_t*>(&r12_37) = 0;
    if (reinterpret_cast<int32_t>(edx34 - 2) > *reinterpret_cast<int32_t*>(&rcx35)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_37) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx35);
            *reinterpret_cast<int32_t*>(&r12_37) = 1;
        }
    }
    rbx38 = *reinterpret_cast<int32_t*>(&rcx35);
    rdi39 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + rbx38 * 8);
    r13_40 = reinterpret_cast<void*>(rbx38 * 8);
    eax41 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi39->f0));
    if (*reinterpret_cast<signed char*>(&eax41) != 45) {
        if (*reinterpret_cast<signed char*>(&eax41) != 61 || (eax42 = rdi39->f1, !!*reinterpret_cast<signed char*>(&eax42)) && (*reinterpret_cast<signed char*>(&eax42) != 61 || rdi39->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi39->f0) == 33) || (rdi39->f1 != 61 || rdi39->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax43 = reinterpret_cast<int32_t>(pos);
                rbx44 = rax43;
                rax45 = rax43 + 2;
                rsi46 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax45 * 8));
                rdi47 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax45 * 8) - 16);
                fun_24f0(rdi47, rsi46);
                pos = *reinterpret_cast<int32_t*>(&rbx44) + 3;
                goto addr_2cc9_47;
            }
        } else {
            rax48 = reinterpret_cast<int32_t>(pos);
            rbx49 = rax48;
            rax50 = rax48 + 2;
            rsi51 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax50 * 8));
            rdi52 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax50 * 8) - 16);
            fun_24f0(rdi52, rsi51);
            pos = *reinterpret_cast<int32_t*>(&rbx49) + 3;
            goto addr_2cc9_47;
        }
    }
    eax53 = rdi39->f1;
    if ((*reinterpret_cast<signed char*>(&eax53) == 0x6c || *reinterpret_cast<signed char*>(&eax53) == 0x67) && ((edx54 = rdi39->f2, *reinterpret_cast<signed char*>(&edx54) == 0x65) || *reinterpret_cast<signed char*>(&edx54) == 0x74)) {
        if (rdi39->f3) 
            goto addr_2d44_51; else 
            goto addr_2ea4_52;
    }
    if (*reinterpret_cast<signed char*>(&eax53) == 0x65) {
        eax55 = rdi39->f2;
        if (*reinterpret_cast<signed char*>(&eax55) == 0x71) {
            addr_300c_55:
            if (!rdi39->f3) {
                addr_2ea4_52:
                rdi56 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi30)) {
                    rax57 = find_int(rdi56);
                    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
                    rbp59 = rax57;
                } else {
                    rax60 = fun_2440(rdi56);
                    rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
                    rax62 = umaxtostr(rax60, reinterpret_cast<int64_t>(rsp61) + 0x120);
                    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                    rbp59 = rax62;
                }
            } else {
                addr_3016_58:
                rax63 = quote(rdi39);
                rax64 = fun_2420();
                rax65 = test_syntax_error(rax64, rax63, 5, rcx35, r8_11, r9);
                goto addr_3040_59;
            }
        } else {
            addr_2e0c_60:
            if (*reinterpret_cast<signed char*>(&eax55) != 0x66) 
                goto addr_3016_58;
            if (rdi39->f3) 
                goto addr_3016_58; else 
                goto addr_2e1e_62;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax53) == 0x6e) {
            eax66 = rdi39->f2;
            if (*reinterpret_cast<signed char*>(&eax66) != 0x65) 
                goto addr_2f5a_65; else 
                goto addr_300c_55;
        } else {
            addr_2d44_51:
            if (*reinterpret_cast<signed char*>(&eax53) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax53) > 0x6f) 
                    goto addr_3016_58;
                if (*reinterpret_cast<signed char*>(&eax53) == 0x65) 
                    goto addr_30a8_68; else 
                    goto addr_2f4e_69;
            } else {
                if (rdi39->f2 != 0x74) 
                    goto addr_3016_58;
                if (rdi39->f3) 
                    goto addr_3016_58;
                tmp32_67 = pos + 3;
                pos = tmp32_67;
                *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
                if (*reinterpret_cast<unsigned char*>(&r12_37)) {
                    rsi68 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_74;
                } else {
                    rdi69 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
                    r14_70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) + 0x90);
                    eax71 = fun_2500(rdi69, r14_70);
                    rdx72 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8);
                    if (eax71) {
                        rdi73 = *rdx72;
                        fun_2500(rdi73, r14_70);
                        goto addr_2cc9_47;
                    } else {
                        rdi74 = *rdx72;
                        eax75 = fun_2500(rdi74, r14_70);
                        if (!eax75) {
                            *reinterpret_cast<uint32_t*>(&rcx35) = reinterpret_cast<uint1_t>(v76 < v77);
                            *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
                            goto addr_2cc9_47;
                        }
                    }
                }
            }
        }
    }
    rax78 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_37)) {
        rdi79 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax78) + reinterpret_cast<uint64_t>(r13_40) + 8);
        rax80 = find_int(rdi79, rdi79);
        rsi81 = rax80;
    } else {
        rdi82 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax78) + reinterpret_cast<uint64_t>(r13_40) + 16);
        rax83 = fun_2440(rdi82, rdi82);
        rax84 = umaxtostr(rax83, reinterpret_cast<int64_t>(rsp58) - 8 + 8 + 0x140);
        rsi81 = rax84;
    }
    strintcmp(rbp59, rsi81);
    rdx85 = argv;
    *reinterpret_cast<uint32_t*>(&rcx35) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx85) + rbx38 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    tmp32_86 = pos + 3;
    pos = tmp32_86;
    if (*reinterpret_cast<signed char*>(&rcx35) == 0x6c) {
        goto addr_2cc9_47;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx35) == 0x67) {
            goto addr_2cc9_47;
        } else {
            goto addr_2cc9_47;
        }
    }
    addr_3040_59:
    rdi87 = *rax65;
    eax89 = fun_2500(rdi87, r14_88, rdi87, r14_88);
    if (!eax89) {
        addr_2cc9_47:
        rax90 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax32) - reinterpret_cast<unsigned char>(g28));
        if (rax90) {
            fun_2450();
        } else {
            goto rax22;
        }
    } else {
        addr_3053_89:
        goto addr_2cc9_47;
    }
    rsi68 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_74:
        rax91 = fun_2420();
        test_syntax_error(rax91, rsi68, 5, rcx35, r8_11, r9);
        addr_3102_91:
        rsi68 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_62:
    tmp32_92 = pos + 3;
    pos = tmp32_92;
    *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
    if (!*reinterpret_cast<unsigned char*>(&r12_37)) {
        rdi93 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
        eax94 = fun_2500(rdi93, rsp31);
        if (!eax94 && ((rdi95 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8), eax96 = fun_2500(rdi95, reinterpret_cast<int64_t>(rsp31) - 8 + 8 + 0x90), !eax96) && v97 == v98)) {
            goto addr_2cc9_47;
        }
    }
    addr_2f5a_65:
    if (*reinterpret_cast<signed char*>(&eax66) != 0x74) 
        goto addr_3016_58;
    if (rdi39->f3) 
        goto addr_3016_58;
    tmp32_99 = pos + 3;
    pos = tmp32_99;
    *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
    if (*reinterpret_cast<unsigned char*>(&r12_37)) 
        goto addr_3102_91;
    rdi100 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
    r14_88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) + 0x90);
    eax101 = fun_2500(rdi100, r14_88);
    rax65 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8);
    if (eax101) 
        goto addr_3040_59;
    rdi102 = *rax65;
    eax103 = fun_2500(rdi102, r14_88);
    if (eax103) 
        goto addr_3053_89;
    *reinterpret_cast<uint32_t*>(&rcx35) = reinterpret_cast<uint1_t>(v104 < v105);
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    goto addr_2cc9_47;
    addr_30a8_68:
    eax55 = rdi39->f2;
    goto addr_2e0c_60;
    addr_2f4e_69:
    if (*reinterpret_cast<signed char*>(&eax53) != 0x6e) 
        goto addr_3016_58;
    eax66 = rdi39->f2;
    goto addr_2f5a_65;
}

struct s0* find_int(struct s0* rdi, ...) {
    struct s0* rbp2;
    unsigned char** rax3;
    void* rsp4;
    struct s0* r8_5;
    unsigned char* rsi6;
    int64_t rdx7;
    int64_t rax8;
    struct s0* rdx9;
    void* rdx10;
    struct s1* rax11;
    struct s0* rcx12;
    int32_t ecx13;
    struct s0* rdx14;
    int32_t ecx15;
    struct s0* rax16;
    struct s0* rax17;
    int64_t r9_18;
    int64_t rdx19;
    struct s0* rax20;
    struct s0* rdi21;
    struct s0* rax22;
    struct s0* rax23;
    struct s0* rdi24;
    int64_t r9_25;
    int32_t esi26;
    void* rsp27;
    struct s0* rax28;
    int64_t rax29;
    uint32_t edx30;
    struct s0* rcx31;
    struct s0* rbp32;
    int64_t r12_33;
    int64_t rbx34;
    struct s0* rdi35;
    void* r13_36;
    uint32_t eax37;
    uint32_t eax38;
    int64_t rax39;
    int64_t rbx40;
    int64_t rax41;
    struct s0* rsi42;
    struct s0* rdi43;
    int64_t rax44;
    int64_t rbx45;
    int64_t rax46;
    struct s0* rsi47;
    struct s0* rdi48;
    uint32_t eax49;
    uint32_t edx50;
    uint32_t eax51;
    struct s0* rdi52;
    struct s0* rax53;
    void* rsp54;
    struct s0* rbp55;
    struct s0* rax56;
    void* rsp57;
    struct s0* rax58;
    struct s0* rax59;
    struct s0* rax60;
    int64_t r9_61;
    int64_t* rax62;
    uint32_t eax63;
    uint32_t tmp32_64;
    struct s0* rsi65;
    int64_t rdi66;
    void* r14_67;
    int32_t eax68;
    int64_t* rdx69;
    int64_t rdi70;
    int64_t rdi71;
    int32_t eax72;
    int64_t v73;
    int64_t v74;
    struct s0* rax75;
    struct s0* rdi76;
    struct s0* rax77;
    struct s0* rsi78;
    struct s0* rdi79;
    struct s0* rax80;
    struct s0* rax81;
    struct s0* rdx82;
    uint32_t tmp32_83;
    int64_t rdi84;
    void* r14_85;
    int32_t eax86;
    void* rax87;
    struct s0* rax88;
    int64_t r9_89;
    uint32_t tmp32_90;
    int64_t rdi91;
    int32_t eax92;
    int64_t rdi93;
    int32_t eax94;
    int64_t v95;
    int64_t v96;
    uint32_t tmp32_97;
    int64_t rdi98;
    int32_t eax99;
    int64_t rdi100;
    int32_t eax101;
    int64_t v102;
    int64_t v103;

    rbp2 = rdi;
    rax3 = fun_2650(rdi);
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 + 8);
    r8_5 = rbp2;
    rsi6 = *rax3;
    while (*reinterpret_cast<uint32_t*>(&rdx7) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8_5->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0, rax8 = rdx7, !!(rsi6[rdx7 * 2] & 1)) {
        r8_5 = reinterpret_cast<struct s0*>(&r8_5->f1);
    }
    if (*reinterpret_cast<signed char*>(&rdx7) == 43) {
        rdx9 = reinterpret_cast<struct s0*>(&r8_5->f1);
        r8_5 = rdx9;
    } else {
        *reinterpret_cast<int32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax8) == 45);
        rdx9 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdx10) + reinterpret_cast<unsigned char>(r8_5));
    }
    rax11 = reinterpret_cast<struct s1*>(&rdx9->f1);
    *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&rdx9->f0)) - 48;
    *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rcx12) <= 9) {
        ecx13 = reinterpret_cast<signed char>(rdx9->f1);
        *reinterpret_cast<int32_t*>(&rdx14) = ecx13;
        if (ecx13 - 48 <= 9) {
            do {
                ecx15 = reinterpret_cast<signed char>(rax11->f1);
                rax11 = reinterpret_cast<struct s1*>(&rax11->f1);
                *reinterpret_cast<int32_t*>(&rdx14) = ecx15;
            } while (ecx15 - 48 <= 9);
        }
        *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<unsigned char*>(&rdx14);
        *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
        if (rsi6[reinterpret_cast<unsigned char>(rcx12) * 2] & 1) {
            do {
                *reinterpret_cast<uint32_t*>(&rcx12) = rax11->f1;
                *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                rax11 = reinterpret_cast<struct s1*>(&rax11->f1);
                rdx14 = rcx12;
            } while (rsi6[reinterpret_cast<unsigned char>(rcx12) * 2] & 1);
        }
        if (!*reinterpret_cast<unsigned char*>(&rdx14)) {
            return r8_5;
        }
    }
    rax16 = quote(rbp2);
    rax17 = fun_2420();
    test_syntax_error(rax17, rax16, 5, rcx12, r8_5, r9_18);
    rdx19 = reinterpret_cast<int32_t>(argc);
    rax20 = argv;
    rdi21 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax20) + rdx19 * 8 - 8);
    rax22 = quote(rdi21, rdi21);
    rax23 = fun_2420();
    rdi24 = rax23;
    test_syntax_error(rdi24, rax22, 5, rcx12, r8_5, r9_25);
    esi26 = *reinterpret_cast<int32_t*>(&rdi24);
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax28 = g28;
    *reinterpret_cast<uint32_t*>(&rax29) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi24)) {
        *reinterpret_cast<uint32_t*>(&rax29) = *reinterpret_cast<uint32_t*>(&rax29) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax29);
    }
    edx30 = argc;
    *reinterpret_cast<uint32_t*>(&rcx31) = static_cast<uint32_t>(rax29 + 1);
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    rbp32 = argv;
    *reinterpret_cast<int32_t*>(&r12_33) = 0;
    if (reinterpret_cast<int32_t>(edx30 - 2) > *reinterpret_cast<int32_t*>(&rcx31)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_33) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx31);
            *reinterpret_cast<int32_t*>(&r12_33) = 1;
        }
    }
    rbx34 = *reinterpret_cast<int32_t*>(&rcx31);
    rdi35 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + rbx34 * 8);
    r13_36 = reinterpret_cast<void*>(rbx34 * 8);
    eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi35->f0));
    if (*reinterpret_cast<signed char*>(&eax37) != 45) {
        if (*reinterpret_cast<signed char*>(&eax37) != 61 || (eax38 = rdi35->f1, !!*reinterpret_cast<signed char*>(&eax38)) && (*reinterpret_cast<signed char*>(&eax38) != 61 || rdi35->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi35->f0) == 33) || (rdi35->f1 != 61 || rdi35->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax39 = reinterpret_cast<int32_t>(pos);
                rbx40 = rax39;
                rax41 = rax39 + 2;
                rsi42 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax41 * 8));
                rdi43 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax41 * 8) - 16);
                fun_24f0(rdi43, rsi42);
                pos = *reinterpret_cast<int32_t*>(&rbx40) + 3;
                goto addr_2cc9_44;
            }
        } else {
            rax44 = reinterpret_cast<int32_t>(pos);
            rbx45 = rax44;
            rax46 = rax44 + 2;
            rsi47 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax46 * 8));
            rdi48 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax46 * 8) - 16);
            fun_24f0(rdi48, rsi47);
            pos = *reinterpret_cast<int32_t*>(&rbx45) + 3;
            goto addr_2cc9_44;
        }
    }
    eax49 = rdi35->f1;
    if ((*reinterpret_cast<signed char*>(&eax49) == 0x6c || *reinterpret_cast<signed char*>(&eax49) == 0x67) && ((edx50 = rdi35->f2, *reinterpret_cast<signed char*>(&edx50) == 0x65) || *reinterpret_cast<signed char*>(&edx50) == 0x74)) {
        if (rdi35->f3) 
            goto addr_2d44_48; else 
            goto addr_2ea4_49;
    }
    if (*reinterpret_cast<signed char*>(&eax49) == 0x65) {
        eax51 = rdi35->f2;
        if (*reinterpret_cast<signed char*>(&eax51) == 0x71) {
            addr_300c_52:
            if (!rdi35->f3) {
                addr_2ea4_49:
                rdi52 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi26)) {
                    rax53 = find_int(rdi52);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
                    rbp55 = rax53;
                } else {
                    rax56 = fun_2440(rdi52);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
                    rax58 = umaxtostr(rax56, reinterpret_cast<int64_t>(rsp57) + 0x120);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
                    rbp55 = rax58;
                }
            } else {
                addr_3016_55:
                rax59 = quote(rdi35);
                rax60 = fun_2420();
                rax62 = test_syntax_error(rax60, rax59, 5, rcx31, r8_5, r9_61);
                goto addr_3040_56;
            }
        } else {
            addr_2e0c_57:
            if (*reinterpret_cast<signed char*>(&eax51) != 0x66) 
                goto addr_3016_55;
            if (rdi35->f3) 
                goto addr_3016_55; else 
                goto addr_2e1e_59;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax49) == 0x6e) {
            eax63 = rdi35->f2;
            if (*reinterpret_cast<signed char*>(&eax63) != 0x65) 
                goto addr_2f5a_62; else 
                goto addr_300c_52;
        } else {
            addr_2d44_48:
            if (*reinterpret_cast<signed char*>(&eax49) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax49) > 0x6f) 
                    goto addr_3016_55;
                if (*reinterpret_cast<signed char*>(&eax49) == 0x65) 
                    goto addr_30a8_65; else 
                    goto addr_2f4e_66;
            } else {
                if (rdi35->f2 != 0x74) 
                    goto addr_3016_55;
                if (rdi35->f3) 
                    goto addr_3016_55;
                tmp32_64 = pos + 3;
                pos = tmp32_64;
                *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
                if (*reinterpret_cast<unsigned char*>(&r12_33)) {
                    rsi65 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_71;
                } else {
                    rdi66 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
                    r14_67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) + 0x90);
                    eax68 = fun_2500(rdi66, r14_67);
                    rdx69 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8);
                    if (eax68) {
                        rdi70 = *rdx69;
                        fun_2500(rdi70, r14_67);
                        goto addr_2cc9_44;
                    } else {
                        rdi71 = *rdx69;
                        eax72 = fun_2500(rdi71, r14_67);
                        if (!eax72) {
                            *reinterpret_cast<uint32_t*>(&rcx31) = reinterpret_cast<uint1_t>(v73 < v74);
                            *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
                            goto addr_2cc9_44;
                        }
                    }
                }
            }
        }
    }
    rax75 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_33)) {
        rdi76 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<uint64_t>(r13_36) + 8);
        rax77 = find_int(rdi76, rdi76);
        rsi78 = rax77;
    } else {
        rdi79 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<uint64_t>(r13_36) + 16);
        rax80 = fun_2440(rdi79, rdi79);
        rax81 = umaxtostr(rax80, reinterpret_cast<int64_t>(rsp54) - 8 + 8 + 0x140);
        rsi78 = rax81;
    }
    strintcmp(rbp55, rsi78);
    rdx82 = argv;
    *reinterpret_cast<uint32_t*>(&rcx31) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx82) + rbx34 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    tmp32_83 = pos + 3;
    pos = tmp32_83;
    if (*reinterpret_cast<signed char*>(&rcx31) == 0x6c) {
        goto addr_2cc9_44;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx31) == 0x67) {
            goto addr_2cc9_44;
        } else {
            goto addr_2cc9_44;
        }
    }
    addr_3040_56:
    rdi84 = *rax62;
    eax86 = fun_2500(rdi84, r14_85, rdi84, r14_85);
    if (!eax86) {
        addr_2cc9_44:
        rax87 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax28) - reinterpret_cast<unsigned char>(g28));
        if (rax87) {
            fun_2450();
        } else {
            goto rax16;
        }
    } else {
        addr_3053_86:
        goto addr_2cc9_44;
    }
    rsi65 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_71:
        rax88 = fun_2420();
        test_syntax_error(rax88, rsi65, 5, rcx31, r8_5, r9_89);
        addr_3102_88:
        rsi65 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_59:
    tmp32_90 = pos + 3;
    pos = tmp32_90;
    *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
    if (!*reinterpret_cast<unsigned char*>(&r12_33)) {
        rdi91 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
        eax92 = fun_2500(rdi91, rsp27);
        if (!eax92 && ((rdi93 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8), eax94 = fun_2500(rdi93, reinterpret_cast<int64_t>(rsp27) - 8 + 8 + 0x90), !eax94) && v95 == v96)) {
            goto addr_2cc9_44;
        }
    }
    addr_2f5a_62:
    if (*reinterpret_cast<signed char*>(&eax63) != 0x74) 
        goto addr_3016_55;
    if (rdi35->f3) 
        goto addr_3016_55;
    tmp32_97 = pos + 3;
    pos = tmp32_97;
    *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
    if (*reinterpret_cast<unsigned char*>(&r12_33)) 
        goto addr_3102_88;
    rdi98 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
    r14_85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) + 0x90);
    eax99 = fun_2500(rdi98, r14_85);
    rax62 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8);
    if (eax99) 
        goto addr_3040_56;
    rdi100 = *rax62;
    eax101 = fun_2500(rdi100, r14_85);
    if (eax101) 
        goto addr_3053_86;
    *reinterpret_cast<uint32_t*>(&rcx31) = reinterpret_cast<uint1_t>(v102 < v103);
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    goto addr_2cc9_44;
    addr_30a8_65:
    eax51 = rdi35->f2;
    goto addr_2e0c_57;
    addr_2f4e_66:
    if (*reinterpret_cast<signed char*>(&eax49) != 0x6e) 
        goto addr_3016_55;
    eax63 = rdi35->f2;
    goto addr_2f5a_62;
}

struct s5 {
    unsigned char f0;
    signed char f1;
};

struct s6 {
    signed char f0;
    signed char f1;
};

struct s7 {
    signed char f0;
    signed char f1;
};

uint32_t unary_operator(struct s0* rdi, struct s0* rsi) {
    int64_t rdx3;
    struct s0* r8_4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s0* rbx7;
    int32_t edx8;
    struct s0* rax9;
    struct s0* rax10;
    struct s0* rdi11;
    struct s0* rcx12;
    int64_t r9_13;
    int64_t* rax14;
    void* rsp15;
    uint32_t edx16;
    int1_t less17;
    void* rsp18;
    uint32_t eax19;
    int32_t eax20;
    uint32_t eax21;
    uint32_t v22;
    void* rdx23;
    int64_t* rsp24;
    int64_t rax25;
    int64_t rdx26;
    struct s0* rsi27;
    struct s0* v28;
    void* rsp29;
    void* rsp30;
    int64_t rbx31;
    int64_t rdx32;
    int1_t zf33;
    struct s0* rax34;
    uint32_t ebx35;
    void* r14_36;
    struct s0* r13_37;
    struct s0* rcx38;
    struct s0* r12_39;
    struct s0* rdi40;
    uint32_t eax41;
    uint32_t ecx42;
    struct s0** rcx43;
    uint32_t r8d44;
    struct s0* rdx45;
    struct s0* rax46;
    struct s0* rax47;
    struct s0* rax48;
    struct s0* rax49;
    struct s0* rax50;
    void* rsp51;
    struct s0* v52;
    struct s0* v53;
    struct s0* rax54;
    struct s1* rax55;
    struct s0* rbp56;
    uint32_t eax57;
    void* rsp58;
    uint32_t edx59;
    int1_t less60;
    int1_t less61;
    void* rsp62;
    struct s0* rdx63;
    int1_t less64;
    int64_t rdx65;
    int64_t v66;
    void* rsp67;
    struct s0* rax68;
    int64_t rax69;
    uint32_t edx70;
    struct s0* rcx71;
    struct s0* rbp72;
    int64_t r12_73;
    int64_t rbx74;
    struct s0* rdi75;
    void* r13_76;
    uint32_t eax77;
    uint32_t eax78;
    uint32_t edx79;
    uint32_t eax80;
    int64_t rax81;
    int64_t rbx82;
    int64_t rax83;
    struct s0* rsi84;
    struct s0* rdi85;
    int64_t rax86;
    int64_t rbx87;
    int64_t rax88;
    struct s0* rsi89;
    struct s0* rdi90;
    uint32_t eax91;
    struct s0* rdi92;
    struct s0* rax93;
    void* rsp94;
    struct s0* rbp95;
    struct s0* rax96;
    void* rsp97;
    struct s0* rax98;
    struct s0* rax99;
    struct s0* rax100;
    int64_t* rax101;
    struct s0* rax102;
    struct s0* rdi103;
    struct s0* rax104;
    struct s0* rsi105;
    struct s0* rdi106;
    struct s0* rax107;
    struct s0* rax108;
    struct s0* rdx109;
    uint32_t tmp32_110;
    int64_t rdi111;
    int32_t eax112;
    void* rax113;
    int64_t v114;
    struct s0* rsi115;
    struct s0* rax116;
    uint32_t tmp32_117;
    int64_t rdi118;
    int32_t eax119;
    int64_t rdi120;
    int32_t eax121;
    int64_t v122;
    int64_t v123;
    uint32_t eax124;
    uint32_t tmp32_125;
    int64_t rdi126;
    void* r14_127;
    int32_t eax128;
    int64_t* rdx129;
    int64_t rdi130;
    int64_t rdi131;
    int32_t eax132;
    int64_t v133;
    int64_t v134;
    uint32_t tmp32_135;
    int64_t rdi136;
    int32_t eax137;
    int64_t rdi138;
    int32_t eax139;
    int64_t v140;
    int64_t v141;
    int64_t v142;
    struct s0* rax143;
    struct s0* rax144;
    uint32_t eax145;
    int1_t less146;
    int64_t v147;
    uint32_t tmp32_148;
    int64_t rax149;

    while (rdx3 = reinterpret_cast<int32_t>(pos), r8_4 = argv, rax5 = g28, rdi6 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rdx3 * 8), *reinterpret_cast<uint32_t*>(&rbx7) = rdi6->f1, *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0, edx8 = static_cast<int32_t>(reinterpret_cast<unsigned char>(rbx7) + 0xffffffffffffffb9), *reinterpret_cast<unsigned char*>(&edx8) > 51) {
        rax9 = quote(rdi6, rdi6);
        rax10 = fun_2420();
        rdi11 = rax10;
        rax14 = test_syntax_error(rdi11, rax9, 5, rcx12, r8_4, r9_13);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
        edx16 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax14) + 1);
        less17 = reinterpret_cast<int32_t>(edx16) < reinterpret_cast<int32_t>(argc);
        pos = edx16;
        if (!less17) {
            beyond();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        } else {
            eax19 = *reinterpret_cast<int32_t*>(&rax14) + 2;
            pos = eax19;
            rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<int32_t>(eax19) * 8 - 8);
            eax20 = fun_24b0(rdi11, rsp15);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_4) = eax20;
            *reinterpret_cast<int32_t*>(&r8_4 + 4) = 0;
            eax21 = 0;
            if (!*reinterpret_cast<int32_t*>(&r8_4)) {
                eax21 = v22 & 0xf000;
                *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(eax21 == 0xa000);
            }
            rdx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (!rdx23) 
                goto addr_3207_7;
        }
        fun_2450();
        rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        while ((rax25 = reinterpret_cast<int32_t>(pos), rcx12 = argv, rdx26 = rax25, rsi27 = reinterpret_cast<struct s0*>(rax25 * 8), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f0) == 33)) || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f0) == 45)) 
                goto addr_38f6_11;
            if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f1) 
                goto addr_38f6_11;
            if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f2) 
                goto addr_38f1_14;
            addr_38f6_11:
            v28 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8);
            beyond();
            rsp29 = reinterpret_cast<void*>(rsp24 - 1 - 1 + 1);
            while (1) {
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
                *reinterpret_cast<uint32_t*>(&rbx31) = pos;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                while (1) {
                    zf33 = *reinterpret_cast<uint32_t*>(&rbx31) == *reinterpret_cast<uint32_t*>(&rdx32);
                    if (*reinterpret_cast<int32_t*>(&rbx31) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                        goto addr_3a9d_17;
                    goto addr_3930_19;
                    addr_3a5a_20:
                    if (*reinterpret_cast<uint32_t*>(&rcx12) != 45) 
                        goto addr_3a68_21;
                    if (rax34->f1 != 0x6f) 
                        goto addr_3a68_21;
                    if (rax34->f2) 
                        goto addr_3a68_21;
                    *reinterpret_cast<uint32_t*>(&rbx31) = ebx35 + 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                    pos = *reinterpret_cast<uint32_t*>(&rbx31);
                    continue;
                    while (1) {
                        addr_3a0c_25:
                        if (*reinterpret_cast<signed char*>(&r14_36) != 45 || !r13_37->f1) {
                            addr_3a28_26:
                            ebx35 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx12->f1));
                            pos = ebx35;
                        } else {
                            if (!r13_37->f2) {
                                unary_operator(rdi11, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                ebx35 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            } else {
                                goto addr_3a28_26;
                            }
                        }
                        while (1) {
                            if (reinterpret_cast<int32_t>(ebx35) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                                goto addr_3af0_31;
                            while ((rcx38 = argv, rax34 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx38) + reinterpret_cast<int32_t>(ebx35) * 8), *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax34->f0)), *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx12) == 45) && (rax34->f1 == 97 && !rax34->f2)) {
                                *reinterpret_cast<uint32_t*>(&rbx31) = ebx35 + 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                                pos = *reinterpret_cast<uint32_t*>(&rbx31);
                                zf33 = *reinterpret_cast<uint32_t*>(&rbx31) == *reinterpret_cast<uint32_t*>(&rdx32);
                                if (*reinterpret_cast<int32_t*>(&rbx31) < *reinterpret_cast<int32_t*>(&rdx32)) {
                                    addr_3930_19:
                                    r8_4 = argv;
                                    rbx31 = *reinterpret_cast<int32_t*>(&rbx31);
                                    *reinterpret_cast<uint32_t*>(&rdi11) = 0;
                                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    while (r13_37 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8), *reinterpret_cast<uint32_t*>(&rax34) = *reinterpret_cast<uint32_t*>(&rbx31), *reinterpret_cast<int32_t*>(&rax34 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rbx31), *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<uint32_t*>(&r14_36) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r13_37->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_36) + 4) = 0, *reinterpret_cast<signed char*>(&r14_36) == 33) {
                                        if (r13_37->f1) 
                                            goto addr_3ab8_36;
                                        ++rbx31;
                                        *reinterpret_cast<uint32_t*>(&r12_39) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rax34->f1));
                                        if (*reinterpret_cast<int32_t*>(&rdx32) <= *reinterpret_cast<int32_t*>(&rbx31)) 
                                            goto addr_3bf6_38;
                                        *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                        *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    }
                                } else {
                                    addr_3a9d_17:
                                    beyond();
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    goto addr_3aa8_40;
                                }
                                if (*reinterpret_cast<signed char*>(&rdi11)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_39);
                                }
                                if (*reinterpret_cast<signed char*>(&r14_36) != 40 || r13_37->f1) {
                                    addr_399c_44:
                                    *reinterpret_cast<uint32_t*>(&rax34) = *reinterpret_cast<uint32_t*>(&rdx32) - *reinterpret_cast<uint32_t*>(&rcx12);
                                    zf33 = *reinterpret_cast<uint32_t*>(&rax34) == 3;
                                    if (*reinterpret_cast<int32_t*>(&rax34) <= reinterpret_cast<int32_t>(3)) {
                                        addr_3aa8_40:
                                        if (!zf33) 
                                            goto addr_3a0c_25;
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_37->f0) == 45)) 
                                            goto addr_39ea_46;
                                        if (r13_37->f1 != 0x6c) 
                                            goto addr_39ea_46;
                                        if (r13_37->f2) 
                                            goto addr_39ea_46;
                                        rdi40 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8 + 16);
                                        eax41 = binop(rdi40, rsi27);
                                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                        r8_4 = r8_4;
                                        *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx12);
                                        *reinterpret_cast<uint32_t*>(&rdx32) = *reinterpret_cast<uint32_t*>(&rdx32);
                                        if (*reinterpret_cast<signed char*>(&eax41)) 
                                            goto addr_3ba3_50; else 
                                            goto addr_39ea_46;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r9_13) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx12->f1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_13) + 4) = 0;
                                    pos = *reinterpret_cast<uint32_t*>(&r9_13);
                                    zf33 = *reinterpret_cast<uint32_t*>(&r9_13) == *reinterpret_cast<uint32_t*>(&rdx32);
                                    if (*reinterpret_cast<int32_t*>(&r9_13) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                                        goto addr_3a9d_17; else 
                                        goto addr_3b10_52;
                                }
                                addr_39ea_46:
                                rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rax34) = binop(rdi11, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx12);
                                *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx32) = *reinterpret_cast<uint32_t*>(&rdx32);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                                if (!*reinterpret_cast<signed char*>(&rax34)) 
                                    goto addr_3a0c_25;
                                *reinterpret_cast<uint32_t*>(&rdi11) = 0;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                binary_operator(0, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                ebx35 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                                if (reinterpret_cast<int32_t>(ebx35) < *reinterpret_cast<int32_t*>(&rdx32)) 
                                    continue; else 
                                    goto addr_3af0_31;
                                addr_3ab8_36:
                                if (*reinterpret_cast<signed char*>(&rdi11)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_39);
                                    goto addr_399c_44;
                                }
                            }
                            goto addr_3a5a_20;
                            addr_3bf6_38:
                            pos = *reinterpret_cast<uint32_t*>(&r12_39);
                            beyond();
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            goto addr_3c02_56;
                            addr_3ba3_50:
                            *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                            *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            binary_operator(1, rsi27);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            ebx35 = pos;
                            *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            continue;
                            addr_3b10_52:
                            ecx42 = *reinterpret_cast<uint32_t*>(&rcx12) + 2;
                            if (reinterpret_cast<int32_t>(ecx42) >= *reinterpret_cast<int32_t*>(&rdx32)) {
                                addr_3c02_56:
                                *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                rcx43 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<int32_t>(ecx42) * 8);
                                r8d44 = static_cast<int32_t>(rdx32 - 1) - *reinterpret_cast<uint32_t*>(&rax34);
                                do {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*rcx43)->f0) == 41)) 
                                        goto addr_3b3c_59;
                                    if (!(*rcx43)->f1) 
                                        break;
                                    addr_3b3c_59:
                                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 4) 
                                        goto addr_3b9c_61;
                                    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdi11) + 1;
                                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    rcx43 = rcx43 + 8;
                                } while (*reinterpret_cast<uint32_t*>(&rdi11) != r8d44);
                                goto addr_3b4d_63;
                            }
                            addr_3b50_64:
                            posixtest(rdi11, rsi27);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            rcx12 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                            rdx45 = argv;
                            r8_4 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx45) + reinterpret_cast<unsigned char>(rcx12) * 8);
                            rbx7 = rcx12;
                            if (!r8_4) 
                                goto addr_3c0c_65;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r8_4->f0) == 41)) 
                                goto addr_3c3b_67;
                            if (r8_4->f1) 
                                goto addr_3c3b_67;
                            ebx35 = *reinterpret_cast<uint32_t*>(&rbx7) + 1;
                            *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            pos = ebx35;
                            continue;
                            addr_3b9c_61:
                            *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdx32) - *reinterpret_cast<uint32_t*>(&r9_13);
                            *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            goto addr_3b50_64;
                            addr_3b4d_63:
                            goto addr_3b50_64;
                        }
                    }
                }
                addr_3c0c_65:
                rax46 = quote(")", ")");
                rax47 = fun_2420();
                test_syntax_error(rax47, rax46, 5, rcx12, r8_4, r9_13);
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_3c3b_67:
                rax48 = quote_n();
                r12_39 = rax48;
                rax49 = quote_n();
                rax50 = fun_2420();
                rsi27 = rax49;
                test_syntax_error(rax50, rsi27, r12_39, rcx12, r8_4, r9_13);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                while (v52 = r13_37, v53 = r12_39, rax54 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos))), r12_39 = argv, rbx7 = rax54, rax55 = reinterpret_cast<struct s1*>(&rax54->f1), rbp56 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<uint64_t>(rax55) * 8), r13_37 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax55) * 8), rdi11 = rbp56, eax57 = binop(rdi11, rsi27, rdi11, rsi27), rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 - 8 - 8 - 8 - 8 - 8 + 8), !*reinterpret_cast<signed char*>(&eax57)) {
                    edx59 = (*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f0;
                    if (edx59 != 33 || (*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f1) {
                        if (edx59 != 40) 
                            goto addr_3d30_73;
                        if ((*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f1) 
                            goto addr_3d30_73;
                        if ((*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 8))->f0 != 41) 
                            goto addr_3d30_73;
                        if (!(*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 8))->f1) 
                            goto addr_3d1b_77;
                        addr_3d30_73:
                        *reinterpret_cast<uint32_t*>(&rax34) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp56->f0));
                        if (*reinterpret_cast<uint32_t*>(&rax34) != 45) 
                            goto addr_3d4a_78;
                        if (rbp56->f1 != 97) 
                            goto addr_3d3f_80;
                        if (!rbp56->f2) 
                            goto addr_3d96_82;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rbx7) = *reinterpret_cast<uint32_t*>(&rbx7) + 1;
                        less60 = *reinterpret_cast<int32_t*>(&rbx7) < reinterpret_cast<int32_t>(argc);
                        pos = *reinterpret_cast<uint32_t*>(&rbx7);
                        if (!less60) 
                            goto addr_3db8_84; else 
                            goto addr_3ce3_85;
                    }
                    addr_3d3f_80:
                    if (*reinterpret_cast<uint32_t*>(&rax34) != 45) 
                        goto addr_3d4a_78;
                    if (rbp56->f1 != 0x6f) 
                        goto addr_3d4a_78;
                    if (rbp56->f2) 
                        goto addr_3db6_88;
                    addr_3d96_82:
                    less61 = *reinterpret_cast<int32_t*>(&rbx7) < reinterpret_cast<int32_t>(argc);
                    if (less61) 
                        goto addr_3d9e_89;
                    addr_3db8_84:
                    beyond();
                    rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8);
                    if (*reinterpret_cast<uint32_t*>(&rdi11) != 3) 
                        goto addr_3dcd_91;
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) + 8);
                }
                goto addr_3d78_93;
                addr_3d9e_89:
                r12_39 = v53;
                r13_37 = v52;
                rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) + 8 + 8 + 8 + 8 + 8);
                continue;
                addr_3dcd_91:
                if (*reinterpret_cast<int32_t*>(&rdi11) > reinterpret_cast<int32_t>(3)) {
                    if (*reinterpret_cast<uint32_t*>(&rdi11) != 4) {
                        addr_3e60_95:
                        *reinterpret_cast<uint32_t*>(&rax34) = pos;
                    } else {
                        rdx63 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                        rsi27 = argv;
                        rax34 = rdx63;
                        rdi11 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx63) * 8);
                        *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f0));
                        *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rcx12) != 33) 
                            goto addr_3e1e_97;
                        if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f1) 
                            goto addr_3eca_99;
                        addr_3e1e_97:
                        if (*reinterpret_cast<uint32_t*>(&rcx12) != 40) 
                            goto addr_3e66_100;
                        if ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f1) 
                            goto addr_3e66_100;
                        if ((*reinterpret_cast<struct s7**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdi11) + 24))->f0 != 41) 
                            goto addr_3e66_100;
                        if (!(*reinterpret_cast<struct s7**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdi11) + 24))->f1) 
                            goto addr_3e39_104;
                    }
                    addr_3e66_100:
                    less64 = *reinterpret_cast<int32_t*>(&rax34) < reinterpret_cast<int32_t>(argc);
                    if (!less64) 
                        goto addr_3ee5_105;
                    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) + 8);
                } else {
                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 1) 
                        goto addr_3e90_108;
                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 2) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdi11) - 1;
                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rdi11)) 
                        goto addr_2665_111;
                    goto addr_3e60_95;
                }
            }
            rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp62) + 8);
        }
        goto addr_38c8_114;
        addr_38f1_14:
    }
    *reinterpret_cast<uint32_t*>(&rdx65) = *reinterpret_cast<unsigned char*>(&edx8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx65) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa1f0 + rdx65 * 4) + 0xa1f0;
    addr_38c8_114:
    pos = *reinterpret_cast<int32_t*>(&rdx26) + 2;
    goto v66;
    addr_3af0_31:
    addr_3a68_21:
    goto v28;
    addr_3d78_93:
    rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax68 = g28;
    *reinterpret_cast<uint32_t*>(&rax69) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
    if (0) {
        *reinterpret_cast<uint32_t*>(&rax69) = *reinterpret_cast<uint32_t*>(&rax69) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax69);
    }
    edx70 = argc;
    *reinterpret_cast<uint32_t*>(&rcx71) = static_cast<uint32_t>(rax69 + 1);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    rbp72 = argv;
    *reinterpret_cast<int32_t*>(&r12_73) = 0;
    if (reinterpret_cast<int32_t>(edx70 - 2) > *reinterpret_cast<int32_t*>(&rcx71)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_73) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx71);
            *reinterpret_cast<int32_t*>(&r12_73) = 1;
        }
    }
    rbx74 = *reinterpret_cast<int32_t*>(&rcx71);
    rdi75 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + rbx74 * 8);
    r13_76 = reinterpret_cast<void*>(rbx74 * 8);
    eax77 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi75->f0));
    if (*reinterpret_cast<signed char*>(&eax77) == 45) {
        eax78 = rdi75->f1;
        if ((*reinterpret_cast<signed char*>(&eax78) == 0x6c || *reinterpret_cast<signed char*>(&eax78) == 0x67) && ((edx79 = rdi75->f2, *reinterpret_cast<signed char*>(&edx79) == 0x65) || *reinterpret_cast<signed char*>(&edx79) == 0x74)) {
            if (rdi75->f3) 
                goto addr_2d44_125; else 
                goto addr_2ea4_126;
        }
        if (*reinterpret_cast<signed char*>(&eax78) != 0x65) 
            goto addr_2d3c_128;
    } else {
        if (*reinterpret_cast<signed char*>(&eax77) != 61 || (eax80 = rdi75->f1, !!*reinterpret_cast<signed char*>(&eax80)) && (*reinterpret_cast<signed char*>(&eax80) != 61 || rdi75->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi75->f0) == 33) || (rdi75->f1 != 61 || rdi75->f2)) {
                fun_2370();
                goto addr_2665_111;
            } else {
                rax81 = reinterpret_cast<int32_t>(pos);
                rbx82 = rax81;
                rax83 = rax81 + 2;
                rsi84 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8));
                rdi85 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8) - 16);
                fun_24f0(rdi85, rsi84);
                pos = *reinterpret_cast<int32_t*>(&rbx82) + 3;
                goto addr_2cc9_133;
            }
        } else {
            rax86 = reinterpret_cast<int32_t>(pos);
            rbx87 = rax86;
            rax88 = rax86 + 2;
            rsi89 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax88 * 8));
            rdi90 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax88 * 8) - 16);
            fun_24f0(rdi90, rsi89);
            pos = *reinterpret_cast<int32_t*>(&rbx87) + 3;
            goto addr_2cc9_133;
        }
    }
    eax91 = rdi75->f2;
    if (*reinterpret_cast<signed char*>(&eax91) == 0x71) {
        addr_300c_136:
        if (!rdi75->f3) {
            addr_2ea4_126:
            rdi92 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
            if (1) {
                rax93 = find_int(rdi92);
                rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rbp95 = rax93;
            } else {
                rax96 = fun_2440(rdi92);
                rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rax98 = umaxtostr(rax96, reinterpret_cast<int64_t>(rsp97) + 0x120);
                rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
                rbp95 = rax98;
            }
        } else {
            addr_3016_139:
            rax99 = quote(rdi75);
            rax100 = fun_2420();
            rax101 = test_syntax_error(rax100, rax99, 5, rcx71, r8_4, r9_13);
            goto addr_3040_140;
        }
    } else {
        addr_2e0c_141:
        if (*reinterpret_cast<signed char*>(&eax91) != 0x66) 
            goto addr_3016_139;
        if (rdi75->f3) 
            goto addr_3016_139; else 
            goto addr_2e1e_143;
    }
    rax102 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi103 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax102) + reinterpret_cast<uint64_t>(r13_76) + 8);
        rax104 = find_int(rdi103, rdi103);
        rsi105 = rax104;
    } else {
        rdi106 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax102) + reinterpret_cast<uint64_t>(r13_76) + 16);
        rax107 = fun_2440(rdi106, rdi106);
        rax108 = umaxtostr(rax107, reinterpret_cast<int64_t>(rsp94) - 8 + 8 + 0x140);
        rsi105 = rax108;
    }
    strintcmp(rbp95, rsi105);
    rdx109 = argv;
    *reinterpret_cast<uint32_t*>(&rcx71) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx109) + rbx74 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    tmp32_110 = pos + 3;
    pos = tmp32_110;
    if (*reinterpret_cast<signed char*>(&rcx71) == 0x6c) {
        goto addr_2cc9_133;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx71) == 0x67) {
            goto addr_2cc9_133;
        } else {
            goto addr_2cc9_133;
        }
    }
    addr_3040_140:
    rdi111 = *rax101;
    eax112 = fun_2500(rdi111, r14_36, rdi111, r14_36);
    if (!eax112) {
        addr_2cc9_133:
        rax113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax68) - reinterpret_cast<unsigned char>(g28));
        if (rax113) {
            fun_2450();
        } else {
            goto v114;
        }
    } else {
        addr_3053_154:
        goto addr_2cc9_133;
    }
    rsi115 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_156:
        rax116 = fun_2420();
        test_syntax_error(rax116, rsi115, 5, rcx71, r8_4, r9_13);
        addr_3102_157:
        rsi115 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_143:
    tmp32_117 = pos + 3;
    pos = tmp32_117;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi118 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
        eax119 = fun_2500(rdi118, rsp67);
        if (!eax119 && ((rdi120 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8), eax121 = fun_2500(rdi120, reinterpret_cast<int64_t>(rsp67) - 8 + 8 + 0x90), !eax121) && v122 == v123)) {
            goto addr_2cc9_133;
        }
    }
    addr_2d3c_128:
    if (*reinterpret_cast<signed char*>(&eax78) == 0x6e) {
        eax124 = rdi75->f2;
        if (*reinterpret_cast<signed char*>(&eax124) == 0x65) 
            goto addr_300c_136;
    } else {
        addr_2d44_125:
        if (*reinterpret_cast<signed char*>(&eax78) != 0x6f) {
            if (*reinterpret_cast<signed char*>(&eax78) > 0x6f) 
                goto addr_3016_139;
            if (*reinterpret_cast<signed char*>(&eax78) == 0x65) 
                goto addr_30a8_163; else 
                goto addr_2f4e_164;
        } else {
            if (rdi75->f2 != 0x74) 
                goto addr_3016_139;
            if (rdi75->f3) 
                goto addr_3016_139;
            tmp32_125 = pos + 3;
            pos = tmp32_125;
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
            if (*reinterpret_cast<unsigned char*>(&r12_73)) {
                rsi115 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                goto addr_30f1_156;
            } else {
                rdi126 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
                r14_127 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
                eax128 = fun_2500(rdi126, r14_127);
                rdx129 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
                if (eax128) {
                    rdi130 = *rdx129;
                    fun_2500(rdi130, r14_127);
                    goto addr_2cc9_133;
                } else {
                    rdi131 = *rdx129;
                    eax132 = fun_2500(rdi131, r14_127);
                    if (!eax132) {
                        *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v133 < v134);
                        *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
                        goto addr_2cc9_133;
                    }
                }
            }
        }
    }
    addr_2f5a_173:
    if (*reinterpret_cast<signed char*>(&eax124) != 0x74) 
        goto addr_3016_139;
    if (rdi75->f3) 
        goto addr_3016_139;
    tmp32_135 = pos + 3;
    pos = tmp32_135;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (*reinterpret_cast<unsigned char*>(&r12_73)) 
        goto addr_3102_157;
    rdi136 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
    r14_36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
    eax137 = fun_2500(rdi136, r14_36);
    rax101 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
    if (eax137) 
        goto addr_3040_140;
    rdi138 = *rax101;
    eax139 = fun_2500(rdi138, r14_36);
    if (eax139) 
        goto addr_3053_154;
    *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v140 < v141);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    goto addr_2cc9_133;
    addr_30a8_163:
    eax91 = rdi75->f2;
    goto addr_2e0c_141;
    addr_2f4e_164:
    if (*reinterpret_cast<signed char*>(&eax78) != 0x6e) 
        goto addr_3016_139;
    eax124 = rdi75->f2;
    goto addr_2f5a_173;
    addr_2665_111:
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    addr_3d1b_77:
    pos = *reinterpret_cast<uint32_t*>(&rbx7) + 3;
    addr_3ceb_194:
    goto v142;
    addr_3d4a_78:
    rax143 = quote(rbp56, rbp56);
    rax144 = fun_2420();
    test_syntax_error(rax144, rax143, 5, rcx12, r8_4, r9_13);
    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d78_93;
    addr_3db6_88:
    goto addr_3d4a_78;
    addr_3ce3_85:
    two_arguments();
    goto addr_3ceb_194;
    addr_3eca_99:
    eax145 = *reinterpret_cast<uint32_t*>(&rax34) + 1;
    less146 = reinterpret_cast<int32_t>(eax145) < reinterpret_cast<int32_t>(argc);
    pos = eax145;
    if (!less146) {
        addr_3ee5_105:
        beyond();
    } else {
        three_arguments();
    }
    addr_3eb1_196:
    goto v147;
    addr_3e39_104:
    pos = *reinterpret_cast<uint32_t*>(&rax34) + 1;
    two_arguments();
    tmp32_148 = pos + 1;
    pos = tmp32_148;
    goto addr_3eb1_196;
    addr_3e90_108:
    rax149 = reinterpret_cast<int32_t>(pos);
    pos = static_cast<uint32_t>(rax149 + 1);
    goto addr_3eb1_196;
    addr_3207_7:
    return eax21;
}

uint32_t binop(struct s0* rdi, struct s0* rsi, ...) {
    uint32_t r12d3;
    uint32_t eax4;
    int32_t eax5;
    int32_t eax6;
    int32_t eax7;
    int32_t eax8;
    int32_t eax9;
    int32_t eax10;
    int32_t eax11;
    int32_t eax12;
    int32_t eax13;

    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 61)) 
        goto addr_28c0_2;
    r12d3 = 1;
    if (!rdi->f1) 
        goto addr_28ab_4;
    addr_28c0_2:
    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0));
    if (eax4 == 33 && (rdi->f1 == 61 && !rdi->f2)) {
        return 1;
    }
    if (eax4 != 61) 
        goto addr_28d6_7;
    if (rdi->f1 == 61) 
        goto addr_29d2_9;
    addr_28d6_7:
    r12d3 = 1;
    eax5 = fun_24f0(rdi, "-nt");
    if (eax5 && ((eax6 = fun_24f0(rdi, "-ot"), !!eax6) && ((eax7 = fun_24f0(rdi, "-ef"), !!eax7) && ((eax8 = fun_24f0(rdi, "-eq"), !!eax8) && ((eax9 = fun_24f0(rdi, "-ne"), !!eax9) && ((eax10 = fun_24f0(rdi, "-lt"), !!eax10) && ((eax11 = fun_24f0(rdi, "-le"), !!eax11) && (eax12 = fun_24f0(rdi, "-gt"), !!eax12)))))))) {
        eax13 = fun_24f0(rdi, "-ge");
        *reinterpret_cast<unsigned char*>(&r12d3) = reinterpret_cast<uint1_t>(eax13 == 0);
        goto addr_28ab_4;
    }
    addr_29d2_9:
    r12d3 = 1;
    if (!rdi->f2) {
        addr_28ab_4:
        return r12d3;
    } else {
        goto addr_28d6_7;
    }
}

void beyond() {
    int64_t rdx1;
    struct s0* rax2;
    struct s0* rdi3;
    struct s0* rax4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s0* rcx7;
    struct s0* r8_8;
    int64_t r9_9;
    int32_t esi10;
    void* rsp11;
    struct s0* rax12;
    int64_t rax13;
    uint32_t edx14;
    struct s0* rcx15;
    struct s0* rbp16;
    int64_t r12_17;
    int64_t rbx18;
    struct s0* rdi19;
    void* r13_20;
    uint32_t eax21;
    uint32_t eax22;
    int64_t rax23;
    int64_t rbx24;
    int64_t rax25;
    struct s0* rsi26;
    struct s0* rdi27;
    int64_t rax28;
    int64_t rbx29;
    int64_t rax30;
    struct s0* rsi31;
    struct s0* rdi32;
    uint32_t eax33;
    uint32_t edx34;
    uint32_t eax35;
    struct s0* rdi36;
    struct s0* rax37;
    void* rsp38;
    struct s0* rbp39;
    struct s0* rax40;
    void* rsp41;
    struct s0* rax42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* r8_45;
    int64_t r9_46;
    int64_t* rax47;
    uint32_t eax48;
    uint32_t tmp32_49;
    struct s0* rsi50;
    int64_t rdi51;
    void* r14_52;
    int32_t eax53;
    int64_t* rdx54;
    int64_t rdi55;
    int64_t rdi56;
    int32_t eax57;
    int64_t v58;
    int64_t v59;
    struct s0* rax60;
    struct s0* rdi61;
    struct s0* rax62;
    struct s0* rsi63;
    struct s0* rdi64;
    struct s0* rax65;
    struct s0* rax66;
    struct s0* rdx67;
    uint32_t tmp32_68;
    int64_t rdi69;
    void* r14_70;
    int32_t eax71;
    void* rax72;
    int64_t rbp73;
    struct s0* rax74;
    struct s0* r8_75;
    int64_t r9_76;
    uint32_t tmp32_77;
    int64_t rdi78;
    int32_t eax79;
    int64_t rdi80;
    int32_t eax81;
    int64_t v82;
    int64_t v83;
    uint32_t tmp32_84;
    int64_t rdi85;
    int32_t eax86;
    int64_t rdi87;
    int32_t eax88;
    int64_t v89;
    int64_t v90;

    rdx1 = reinterpret_cast<int32_t>(argc);
    rax2 = argv;
    rdi3 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax2) + rdx1 * 8 - 8);
    rax4 = quote(rdi3);
    rax5 = fun_2420();
    rdi6 = rax5;
    test_syntax_error(rdi6, rax4, 5, rcx7, r8_8, r9_9);
    esi10 = *reinterpret_cast<int32_t*>(&rdi6);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax12 = g28;
    *reinterpret_cast<uint32_t*>(&rax13) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi6)) {
        *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax13) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax13);
    }
    edx14 = argc;
    *reinterpret_cast<uint32_t*>(&rcx15) = static_cast<uint32_t>(rax13 + 1);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    rbp16 = argv;
    *reinterpret_cast<int32_t*>(&r12_17) = 0;
    if (reinterpret_cast<int32_t>(edx14 - 2) > *reinterpret_cast<int32_t*>(&rcx15)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx15);
            *reinterpret_cast<int32_t*>(&r12_17) = 1;
        }
    }
    rbx18 = *reinterpret_cast<int32_t*>(&rcx15);
    rdi19 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + rbx18 * 8);
    r13_20 = reinterpret_cast<void*>(rbx18 * 8);
    eax21 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi19->f0));
    if (*reinterpret_cast<signed char*>(&eax21) != 45) {
        if (*reinterpret_cast<signed char*>(&eax21) != 61 || (eax22 = rdi19->f1, !!*reinterpret_cast<signed char*>(&eax22)) && (*reinterpret_cast<signed char*>(&eax22) != 61 || rdi19->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi19->f0) == 33) || (rdi19->f1 != 61 || rdi19->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax23 = reinterpret_cast<int32_t>(pos);
                rbx24 = rax23;
                rax25 = rax23 + 2;
                rsi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax25 * 8));
                rdi27 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax25 * 8) - 16);
                fun_24f0(rdi27, rsi26);
                pos = *reinterpret_cast<int32_t*>(&rbx24) + 3;
                goto addr_2cc9_28;
            }
        } else {
            rax28 = reinterpret_cast<int32_t>(pos);
            rbx29 = rax28;
            rax30 = rax28 + 2;
            rsi31 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax30 * 8));
            rdi32 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax30 * 8) - 16);
            fun_24f0(rdi32, rsi31);
            pos = *reinterpret_cast<int32_t*>(&rbx29) + 3;
            goto addr_2cc9_28;
        }
    }
    eax33 = rdi19->f1;
    if ((*reinterpret_cast<signed char*>(&eax33) == 0x6c || *reinterpret_cast<signed char*>(&eax33) == 0x67) && ((edx34 = rdi19->f2, *reinterpret_cast<signed char*>(&edx34) == 0x65) || *reinterpret_cast<signed char*>(&edx34) == 0x74)) {
        if (rdi19->f3) 
            goto addr_2d44_32; else 
            goto addr_2ea4_33;
    }
    if (*reinterpret_cast<signed char*>(&eax33) == 0x65) {
        eax35 = rdi19->f2;
        if (*reinterpret_cast<signed char*>(&eax35) == 0x71) {
            addr_300c_36:
            if (!rdi19->f3) {
                addr_2ea4_33:
                rdi36 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi10)) {
                    rax37 = find_int(rdi36);
                    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    rbp39 = rax37;
                } else {
                    rax40 = fun_2440(rdi36);
                    rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    rax42 = umaxtostr(rax40, reinterpret_cast<int64_t>(rsp41) + 0x120);
                    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                    rbp39 = rax42;
                }
            } else {
                addr_3016_39:
                rax43 = quote(rdi19);
                rax44 = fun_2420();
                rax47 = test_syntax_error(rax44, rax43, 5, rcx15, r8_45, r9_46);
                goto addr_3040_40;
            }
        } else {
            addr_2e0c_41:
            if (*reinterpret_cast<signed char*>(&eax35) != 0x66) 
                goto addr_3016_39;
            if (rdi19->f3) 
                goto addr_3016_39; else 
                goto addr_2e1e_43;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax33) == 0x6e) {
            eax48 = rdi19->f2;
            if (*reinterpret_cast<signed char*>(&eax48) != 0x65) 
                goto addr_2f5a_46; else 
                goto addr_300c_36;
        } else {
            addr_2d44_32:
            if (*reinterpret_cast<signed char*>(&eax33) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax33) > 0x6f) 
                    goto addr_3016_39;
                if (*reinterpret_cast<signed char*>(&eax33) == 0x65) 
                    goto addr_30a8_49; else 
                    goto addr_2f4e_50;
            } else {
                if (rdi19->f2 != 0x74) 
                    goto addr_3016_39;
                if (rdi19->f3) 
                    goto addr_3016_39;
                tmp32_49 = pos + 3;
                pos = tmp32_49;
                *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
                if (*reinterpret_cast<unsigned char*>(&r12_17)) {
                    rsi50 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_55;
                } else {
                    rdi51 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
                    r14_52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x90);
                    eax53 = fun_2500(rdi51, r14_52);
                    rdx54 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8);
                    if (eax53) {
                        rdi55 = *rdx54;
                        fun_2500(rdi55, r14_52);
                        goto addr_2cc9_28;
                    } else {
                        rdi56 = *rdx54;
                        eax57 = fun_2500(rdi56, r14_52);
                        if (!eax57) {
                            *reinterpret_cast<uint32_t*>(&rcx15) = reinterpret_cast<uint1_t>(v58 < v59);
                            *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                            goto addr_2cc9_28;
                        }
                    }
                }
            }
        }
    }
    rax60 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_17)) {
        rdi61 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<uint64_t>(r13_20) + 8);
        rax62 = find_int(rdi61, rdi61);
        rsi63 = rax62;
    } else {
        rdi64 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<uint64_t>(r13_20) + 16);
        rax65 = fun_2440(rdi64, rdi64);
        rax66 = umaxtostr(rax65, reinterpret_cast<int64_t>(rsp38) - 8 + 8 + 0x140);
        rsi63 = rax66;
    }
    strintcmp(rbp39, rsi63);
    rdx67 = argv;
    *reinterpret_cast<uint32_t*>(&rcx15) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx67) + rbx18 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    tmp32_68 = pos + 3;
    pos = tmp32_68;
    if (*reinterpret_cast<signed char*>(&rcx15) == 0x6c) {
        goto addr_2cc9_28;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx15) == 0x67) {
            goto addr_2cc9_28;
        } else {
            goto addr_2cc9_28;
        }
    }
    addr_3040_40:
    rdi69 = *rax47;
    eax71 = fun_2500(rdi69, r14_70, rdi69, r14_70);
    if (!eax71) {
        addr_2cc9_28:
        rax72 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) - reinterpret_cast<unsigned char>(g28));
        if (rax72) {
            fun_2450();
        } else {
            goto rbp73;
        }
    } else {
        addr_3053_70:
        goto addr_2cc9_28;
    }
    rsi50 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_55:
        rax74 = fun_2420();
        test_syntax_error(rax74, rsi50, 5, rcx15, r8_75, r9_76);
        addr_3102_72:
        rsi50 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_43:
    tmp32_77 = pos + 3;
    pos = tmp32_77;
    *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
    if (!*reinterpret_cast<unsigned char*>(&r12_17)) {
        rdi78 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
        eax79 = fun_2500(rdi78, rsp11);
        if (!eax79 && ((rdi80 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8), eax81 = fun_2500(rdi80, reinterpret_cast<int64_t>(rsp11) - 8 + 8 + 0x90), !eax81) && v82 == v83)) {
            goto addr_2cc9_28;
        }
    }
    addr_2f5a_46:
    if (*reinterpret_cast<signed char*>(&eax48) != 0x74) 
        goto addr_3016_39;
    if (rdi19->f3) 
        goto addr_3016_39;
    tmp32_84 = pos + 3;
    pos = tmp32_84;
    *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
    if (*reinterpret_cast<unsigned char*>(&r12_17)) 
        goto addr_3102_72;
    rdi85 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
    r14_70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x90);
    eax86 = fun_2500(rdi85, r14_70);
    rax47 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8);
    if (eax86) 
        goto addr_3040_40;
    rdi87 = *rax47;
    eax88 = fun_2500(rdi87, r14_70);
    if (eax88) 
        goto addr_3053_70;
    *reinterpret_cast<uint32_t*>(&rcx15) = reinterpret_cast<uint1_t>(v89 < v90);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    goto addr_2cc9_28;
    addr_30a8_49:
    eax35 = rdi19->f2;
    goto addr_2e0c_41;
    addr_2f4e_50:
    if (*reinterpret_cast<signed char*>(&eax33) != 0x6e) 
        goto addr_3016_39;
    eax48 = rdi19->f2;
    goto addr_2f5a_46;
}

struct s8 {
    unsigned char f0;
    signed char f1;
};

struct s9 {
    signed char f0;
    signed char f1;
};

struct s10 {
    signed char f0;
    signed char f1;
};

uint32_t two_arguments() {
    int64_t rax1;
    struct s0* rcx2;
    int64_t rdx3;
    struct s0* rsi4;
    struct s0* v5;
    void* rsp6;
    void* rsp7;
    int64_t rbx8;
    int64_t rdx9;
    int1_t zf10;
    struct s0* rax11;
    uint32_t ebx12;
    void* r14_13;
    struct s0* r13_14;
    struct s0* rdi15;
    struct s0* rcx16;
    struct s0* r8_17;
    struct s0* r12_18;
    struct s0* rdi19;
    uint32_t eax20;
    int64_t r9_21;
    uint32_t ecx22;
    struct s0** rcx23;
    uint32_t r8d24;
    struct s0* rdx25;
    struct s0* rbx26;
    struct s0* rax27;
    struct s0* rax28;
    struct s0* rax29;
    struct s0* rax30;
    struct s0* rax31;
    void* rsp32;
    struct s0* v33;
    struct s0* v34;
    struct s0* rax35;
    struct s1* rax36;
    struct s0* rbp37;
    uint32_t eax38;
    void* rsp39;
    uint32_t edx40;
    int1_t less41;
    int1_t less42;
    void* rsp43;
    struct s0* rdx44;
    int1_t less45;
    int64_t rdx46;
    struct s0* rax47;
    struct s0* rdi48;
    int32_t edx49;
    struct s0* rax50;
    struct s0* rax51;
    int64_t* rax52;
    uint32_t edx53;
    int1_t less54;
    uint32_t eax55;
    int32_t eax56;
    uint32_t eax57;
    uint32_t v58;
    void* rdx59;
    signed char* rax60;
    void* rsp61;
    struct s0* rax62;
    int64_t rax63;
    uint32_t edx64;
    struct s0* rcx65;
    struct s0* rbp66;
    int64_t r12_67;
    int64_t rbx68;
    struct s0* rdi69;
    void* r13_70;
    uint32_t eax71;
    uint32_t eax72;
    uint32_t edx73;
    uint32_t eax74;
    int64_t rax75;
    int64_t rbx76;
    int64_t rax77;
    struct s0* rsi78;
    struct s0* rdi79;
    int64_t rax80;
    int64_t rbx81;
    int64_t rax82;
    struct s0* rsi83;
    struct s0* rdi84;
    uint32_t eax85;
    struct s0* rdi86;
    struct s0* rax87;
    void* rsp88;
    struct s0* rbp89;
    struct s0* rax90;
    void* rsp91;
    struct s0* rax92;
    struct s0* rax93;
    struct s0* rax94;
    int64_t* rax95;
    struct s0* rax96;
    struct s0* rdi97;
    struct s0* rax98;
    struct s0* rsi99;
    struct s0* rdi100;
    struct s0* rax101;
    struct s0* rax102;
    struct s0* rdx103;
    uint32_t tmp32_104;
    int64_t rdi105;
    int32_t eax106;
    void* rax107;
    int64_t v108;
    struct s0* rsi109;
    struct s0* rax110;
    uint32_t tmp32_111;
    int64_t rdi112;
    int32_t eax113;
    int64_t rdi114;
    int32_t eax115;
    int64_t v116;
    int64_t v117;
    uint32_t eax118;
    uint32_t tmp32_119;
    int64_t rdi120;
    void* r14_121;
    int32_t eax122;
    int64_t* rdx123;
    int64_t rdi124;
    int64_t rdi125;
    int32_t eax126;
    int64_t v127;
    int64_t v128;
    uint32_t tmp32_129;
    int64_t rdi130;
    int32_t eax131;
    int64_t rdi132;
    int32_t eax133;
    int64_t v134;
    int64_t v135;
    int64_t v136;
    struct s0* rax137;
    struct s0* rax138;
    uint32_t eax139;
    int1_t less140;
    int64_t v141;
    uint32_t tmp32_142;
    int64_t rax143;
    int64_t rdx144;

    while ((rax1 = reinterpret_cast<int32_t>(pos), rcx2 = argv, rdx3 = rax1, rsi4 = reinterpret_cast<struct s0*>(rax1 * 8), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8))->f0) == 33)) || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8))->f1) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8))->f0) == 45) || (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8))->f1 || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8))->f2)) {
            v5 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx2) + rax1 * 8);
            beyond();
            rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 + 8);
            while (1) {
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
                *reinterpret_cast<uint32_t*>(&rbx8) = pos;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx9) = argc;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                while (1) {
                    zf10 = *reinterpret_cast<uint32_t*>(&rbx8) == *reinterpret_cast<uint32_t*>(&rdx9);
                    if (*reinterpret_cast<int32_t*>(&rbx8) >= *reinterpret_cast<int32_t*>(&rdx9)) 
                        goto addr_3a9d_6;
                    goto addr_3930_8;
                    addr_3a5a_9:
                    if (*reinterpret_cast<uint32_t*>(&rcx2) != 45) 
                        goto addr_3a68_10;
                    if (rax11->f1 != 0x6f) 
                        goto addr_3a68_10;
                    if (rax11->f2) 
                        goto addr_3a68_10;
                    *reinterpret_cast<uint32_t*>(&rbx8) = ebx12 + 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
                    pos = *reinterpret_cast<uint32_t*>(&rbx8);
                    continue;
                    while (1) {
                        addr_3a0c_14:
                        if (*reinterpret_cast<signed char*>(&r14_13) != 45 || !r13_14->f1) {
                            addr_3a28_15:
                            ebx12 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx2->f1));
                            pos = ebx12;
                        } else {
                            if (!r13_14->f2) {
                                unary_operator(rdi15, rsi4);
                                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                                ebx12 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx9) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                            } else {
                                goto addr_3a28_15;
                            }
                        }
                        while (1) {
                            if (reinterpret_cast<int32_t>(ebx12) >= *reinterpret_cast<int32_t*>(&rdx9)) 
                                goto addr_3af0_20;
                            while ((rcx16 = argv, rax11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx16) + reinterpret_cast<int32_t>(ebx12) * 8), *reinterpret_cast<uint32_t*>(&rcx2) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax11->f0)), *reinterpret_cast<int32_t*>(&rcx2 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx2) == 45) && (rax11->f1 == 97 && !rax11->f2)) {
                                *reinterpret_cast<uint32_t*>(&rbx8) = ebx12 + 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
                                pos = *reinterpret_cast<uint32_t*>(&rbx8);
                                zf10 = *reinterpret_cast<uint32_t*>(&rbx8) == *reinterpret_cast<uint32_t*>(&rdx9);
                                if (*reinterpret_cast<int32_t*>(&rbx8) < *reinterpret_cast<int32_t*>(&rdx9)) {
                                    addr_3930_8:
                                    r8_17 = argv;
                                    rbx8 = *reinterpret_cast<int32_t*>(&rbx8);
                                    *reinterpret_cast<uint32_t*>(&rdi15) = 0;
                                    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                                    while (r13_14 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + rbx8 * 8), *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<uint32_t*>(&rbx8), *reinterpret_cast<int32_t*>(&rax11 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx2) = *reinterpret_cast<uint32_t*>(&rbx8), *reinterpret_cast<int32_t*>(&rcx2 + 4) = 0, *reinterpret_cast<uint32_t*>(&r14_13) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r13_14->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_13) + 4) = 0, *reinterpret_cast<signed char*>(&r14_13) == 33) {
                                        if (r13_14->f1) 
                                            goto addr_3ab8_25;
                                        ++rbx8;
                                        *reinterpret_cast<uint32_t*>(&r12_18) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rax11->f1));
                                        if (*reinterpret_cast<int32_t*>(&rdx9) <= *reinterpret_cast<int32_t*>(&rbx8)) 
                                            goto addr_3bf6_27;
                                        *reinterpret_cast<uint32_t*>(&rdi15) = 1;
                                        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                                    }
                                } else {
                                    addr_3a9d_6:
                                    beyond();
                                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                                    goto addr_3aa8_29;
                                }
                                if (*reinterpret_cast<signed char*>(&rdi15)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_18);
                                }
                                if (*reinterpret_cast<signed char*>(&r14_13) != 40 || r13_14->f1) {
                                    addr_399c_33:
                                    *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<uint32_t*>(&rdx9) - *reinterpret_cast<uint32_t*>(&rcx2);
                                    zf10 = *reinterpret_cast<uint32_t*>(&rax11) == 3;
                                    if (*reinterpret_cast<int32_t*>(&rax11) <= reinterpret_cast<int32_t>(3)) {
                                        addr_3aa8_29:
                                        if (!zf10) 
                                            goto addr_3a0c_14;
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_14->f0) == 45)) 
                                            goto addr_39ea_35;
                                        if (r13_14->f1 != 0x6c) 
                                            goto addr_39ea_35;
                                        if (r13_14->f2) 
                                            goto addr_39ea_35;
                                        rdi19 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + rbx8 * 8 + 16);
                                        eax20 = binop(rdi19, rsi4);
                                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                                        r8_17 = r8_17;
                                        *reinterpret_cast<uint32_t*>(&rcx2) = *reinterpret_cast<uint32_t*>(&rcx2);
                                        *reinterpret_cast<uint32_t*>(&rdx9) = *reinterpret_cast<uint32_t*>(&rdx9);
                                        if (*reinterpret_cast<signed char*>(&eax20)) 
                                            goto addr_3ba3_39; else 
                                            goto addr_39ea_35;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r9_21) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx2->f1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_21) + 4) = 0;
                                    pos = *reinterpret_cast<uint32_t*>(&r9_21);
                                    zf10 = *reinterpret_cast<uint32_t*>(&r9_21) == *reinterpret_cast<uint32_t*>(&rdx9);
                                    if (*reinterpret_cast<int32_t*>(&r9_21) >= *reinterpret_cast<int32_t*>(&rdx9)) 
                                        goto addr_3a9d_6; else 
                                        goto addr_3b10_41;
                                }
                                addr_39ea_35:
                                rdi15 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + rbx8 * 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rax11) = binop(rdi15, rsi4);
                                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx2) = *reinterpret_cast<uint32_t*>(&rcx2);
                                *reinterpret_cast<int32_t*>(&rcx2 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx9) = *reinterpret_cast<uint32_t*>(&rdx9);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                                if (!*reinterpret_cast<signed char*>(&rax11)) 
                                    goto addr_3a0c_14;
                                *reinterpret_cast<uint32_t*>(&rdi15) = 0;
                                *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                                binary_operator(0, rsi4);
                                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                                ebx12 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx9) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                                if (reinterpret_cast<int32_t>(ebx12) < *reinterpret_cast<int32_t*>(&rdx9)) 
                                    continue; else 
                                    goto addr_3af0_20;
                                addr_3ab8_25:
                                if (*reinterpret_cast<signed char*>(&rdi15)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_18);
                                    goto addr_399c_33;
                                }
                            }
                            goto addr_3a5a_9;
                            addr_3bf6_27:
                            pos = *reinterpret_cast<uint32_t*>(&r12_18);
                            beyond();
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                            goto addr_3c02_45;
                            addr_3ba3_39:
                            *reinterpret_cast<uint32_t*>(&rdi15) = 1;
                            *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                            binary_operator(1, rsi4);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                            ebx12 = pos;
                            *reinterpret_cast<uint32_t*>(&rdx9) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                            continue;
                            addr_3b10_41:
                            ecx22 = *reinterpret_cast<uint32_t*>(&rcx2) + 2;
                            if (reinterpret_cast<int32_t>(ecx22) >= *reinterpret_cast<int32_t*>(&rdx9)) {
                                addr_3c02_45:
                                *reinterpret_cast<uint32_t*>(&rdi15) = 1;
                                *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rdi15) = 1;
                                *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                                rcx23 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + reinterpret_cast<int32_t>(ecx22) * 8);
                                r8d24 = static_cast<int32_t>(rdx9 - 1) - *reinterpret_cast<uint32_t*>(&rax11);
                                do {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*rcx23)->f0) == 41)) 
                                        goto addr_3b3c_48;
                                    if (!(*rcx23)->f1) 
                                        break;
                                    addr_3b3c_48:
                                    if (*reinterpret_cast<uint32_t*>(&rdi15) == 4) 
                                        goto addr_3b9c_50;
                                    *reinterpret_cast<uint32_t*>(&rdi15) = *reinterpret_cast<uint32_t*>(&rdi15) + 1;
                                    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                                    rcx23 = rcx23 + 8;
                                } while (*reinterpret_cast<uint32_t*>(&rdi15) != r8d24);
                                goto addr_3b4d_52;
                            }
                            addr_3b50_53:
                            posixtest(rdi15, rsi4);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                            rcx2 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                            rdx25 = argv;
                            r8_17 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx25) + reinterpret_cast<unsigned char>(rcx2) * 8);
                            rbx26 = rcx2;
                            if (!r8_17) 
                                goto addr_3c0c_54;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r8_17->f0) == 41)) 
                                goto addr_3c3b_56;
                            if (r8_17->f1) 
                                goto addr_3c3b_56;
                            ebx12 = *reinterpret_cast<uint32_t*>(&rbx26) + 1;
                            *reinterpret_cast<uint32_t*>(&rdx9) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
                            pos = ebx12;
                            continue;
                            addr_3b9c_50:
                            *reinterpret_cast<uint32_t*>(&rdi15) = *reinterpret_cast<uint32_t*>(&rdx9) - *reinterpret_cast<uint32_t*>(&r9_21);
                            *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                            goto addr_3b50_53;
                            addr_3b4d_52:
                            goto addr_3b50_53;
                        }
                    }
                }
                addr_3c0c_54:
                rax27 = quote(")", ")");
                rax28 = fun_2420();
                test_syntax_error(rax28, rax27, 5, rcx2, r8_17, r9_21);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_3c3b_56:
                rax29 = quote_n();
                r12_18 = rax29;
                rax30 = quote_n();
                rax31 = fun_2420();
                rsi4 = rax30;
                test_syntax_error(rax31, rsi4, r12_18, rcx2, r8_17, r9_21);
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                while (v33 = r13_14, v34 = r12_18, rax35 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos))), r12_18 = argv, rbx26 = rax35, rax36 = reinterpret_cast<struct s1*>(&rax35->f1), rbp37 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<uint64_t>(rax36) * 8), r13_14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax36) * 8), rdi15 = rbp37, eax38 = binop(rdi15, rsi4, rdi15, rsi4), rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 - 8 - 8 - 8 - 8 - 8 + 8), !*reinterpret_cast<signed char*>(&eax38)) {
                    edx40 = (*reinterpret_cast<struct s8**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r13_14) + 0xfffffffffffffff8))->f0;
                    if (edx40 != 33 || (*reinterpret_cast<struct s8**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r13_14) + 0xfffffffffffffff8))->f1) {
                        if (edx40 != 40) 
                            goto addr_3d30_62;
                        if ((*reinterpret_cast<struct s8**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r13_14) + 0xfffffffffffffff8))->f1) 
                            goto addr_3d30_62;
                        if ((*reinterpret_cast<struct s9**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r13_14) + 8))->f0 != 41) 
                            goto addr_3d30_62;
                        if (!(*reinterpret_cast<struct s9**>(reinterpret_cast<unsigned char>(r12_18) + reinterpret_cast<unsigned char>(r13_14) + 8))->f1) 
                            goto addr_3d1b_66;
                        addr_3d30_62:
                        *reinterpret_cast<uint32_t*>(&rax11) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp37->f0));
                        if (*reinterpret_cast<uint32_t*>(&rax11) != 45) 
                            goto addr_3d4a_67;
                        if (rbp37->f1 != 97) 
                            goto addr_3d3f_69;
                        if (!rbp37->f2) 
                            goto addr_3d96_71;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rbx26) = *reinterpret_cast<uint32_t*>(&rbx26) + 1;
                        less41 = *reinterpret_cast<int32_t*>(&rbx26) < reinterpret_cast<int32_t>(argc);
                        pos = *reinterpret_cast<uint32_t*>(&rbx26);
                        if (!less41) 
                            goto addr_3db8_73; else 
                            goto addr_3ce3_74;
                    }
                    addr_3d3f_69:
                    if (*reinterpret_cast<uint32_t*>(&rax11) != 45) 
                        goto addr_3d4a_67;
                    if (rbp37->f1 != 0x6f) 
                        goto addr_3d4a_67;
                    if (rbp37->f2) 
                        goto addr_3db6_77;
                    addr_3d96_71:
                    less42 = *reinterpret_cast<int32_t*>(&rbx26) < reinterpret_cast<int32_t>(argc);
                    if (less42) 
                        goto addr_3d9e_78;
                    addr_3db8_73:
                    beyond();
                    rsp43 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) - 8 + 8 - 8);
                    if (*reinterpret_cast<uint32_t*>(&rdi15) != 3) 
                        goto addr_3dcd_80;
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp43) + 8);
                }
                goto addr_3d78_82;
                addr_3d9e_78:
                r12_18 = v34;
                r13_14 = v33;
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) + 8 + 8 + 8 + 8 + 8);
                continue;
                addr_3dcd_80:
                if (*reinterpret_cast<int32_t*>(&rdi15) > reinterpret_cast<int32_t>(3)) {
                    if (*reinterpret_cast<uint32_t*>(&rdi15) != 4) {
                        addr_3e60_84:
                        *reinterpret_cast<uint32_t*>(&rax11) = pos;
                    } else {
                        rdx44 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                        rsi4 = argv;
                        rax11 = rdx44;
                        rdi15 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx44) * 8);
                        *reinterpret_cast<uint32_t*>(&rcx2) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(rdx44) * 8))->f0));
                        *reinterpret_cast<int32_t*>(&rcx2 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rcx2) != 33) 
                            goto addr_3e1e_86;
                        if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(rdx44) * 8))->f1) 
                            goto addr_3eca_88;
                        addr_3e1e_86:
                        if (*reinterpret_cast<uint32_t*>(&rcx2) != 40) 
                            goto addr_3e66_89;
                        if ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(rdx44) * 8))->f1) 
                            goto addr_3e66_89;
                        if ((*reinterpret_cast<struct s10**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(rdi15) + 24))->f0 != 41) 
                            goto addr_3e66_89;
                        if (!(*reinterpret_cast<struct s10**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(rdi15) + 24))->f1) 
                            goto addr_3e39_93;
                    }
                    addr_3e66_89:
                    less45 = *reinterpret_cast<int32_t*>(&rax11) < reinterpret_cast<int32_t>(argc);
                    if (!less45) 
                        goto addr_3ee5_94;
                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp43) + 8);
                } else {
                    if (*reinterpret_cast<uint32_t*>(&rdi15) == 1) 
                        goto addr_3e90_97;
                    if (*reinterpret_cast<uint32_t*>(&rdi15) == 2) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rdi15) = *reinterpret_cast<uint32_t*>(&rdi15) - 1;
                    *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rdi15)) 
                        goto addr_2665_100;
                    goto addr_3e60_84;
                }
            }
            continue;
        }
        rdx46 = reinterpret_cast<int32_t>(pos);
        r8_17 = argv;
        rax47 = g28;
        rdi48 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + rdx46 * 8);
        *reinterpret_cast<uint32_t*>(&rbx26) = rdi48->f1;
        *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
        edx49 = static_cast<int32_t>(reinterpret_cast<unsigned char>(rbx26) + 0xffffffffffffffb9);
        if (*reinterpret_cast<unsigned char*>(&edx49) <= 51) 
            goto addr_3165_105;
        rax50 = quote(rdi48, rdi48);
        rax51 = fun_2420();
        rdi15 = rax51;
        rax52 = test_syntax_error(rdi15, rax50, 5, rcx2, r8_17, r9_21);
        edx53 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax52) + 1);
        less54 = reinterpret_cast<int32_t>(edx53) < reinterpret_cast<int32_t>(argc);
        pos = edx53;
        if (less54) 
            goto addr_31bd_107;
        beyond();
        addr_3893_109:
        fun_2450();
        continue;
        addr_31bd_107:
        eax55 = *reinterpret_cast<int32_t*>(&rax52) + 2;
        pos = eax55;
        rdi15 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_17) + reinterpret_cast<int32_t>(eax55) * 8 - 8);
        eax56 = fun_24b0(rdi15, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
        *reinterpret_cast<int32_t*>(&r8_17) = eax56;
        *reinterpret_cast<int32_t*>(&r8_17 + 4) = 0;
        eax57 = 0;
        if (!*reinterpret_cast<int32_t*>(&r8_17)) {
            eax57 = v58 & 0xf000;
            *reinterpret_cast<unsigned char*>(&eax57) = reinterpret_cast<uint1_t>(eax57 == 0xa000);
        }
        rdx59 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax47) - reinterpret_cast<unsigned char>(g28));
        if (rdx59) 
            goto addr_3893_109; else 
            goto addr_3207_112;
    }
    rax60 = *reinterpret_cast<signed char**>(reinterpret_cast<unsigned char>(rcx2) + reinterpret_cast<unsigned char>(rsi4) + 8);
    pos = *reinterpret_cast<int32_t*>(&rdx3) + 2;
    *reinterpret_cast<unsigned char*>(&rax60) = reinterpret_cast<uint1_t>(*rax60 == 0);
    return *reinterpret_cast<uint32_t*>(&rax60);
    addr_3af0_20:
    addr_3a68_10:
    goto v5;
    addr_3d78_82:
    rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax62 = g28;
    *reinterpret_cast<uint32_t*>(&rax63) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax63) + 4) = 0;
    if (0) {
        *reinterpret_cast<uint32_t*>(&rax63) = *reinterpret_cast<uint32_t*>(&rax63) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax63) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax63);
    }
    edx64 = argc;
    *reinterpret_cast<uint32_t*>(&rcx65) = static_cast<uint32_t>(rax63 + 1);
    *reinterpret_cast<int32_t*>(&rcx65 + 4) = 0;
    rbp66 = argv;
    *reinterpret_cast<int32_t*>(&r12_67) = 0;
    if (reinterpret_cast<int32_t>(edx64 - 2) > *reinterpret_cast<int32_t*>(&rcx65)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax63) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax63) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax63) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_67) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx65);
            *reinterpret_cast<int32_t*>(&r12_67) = 1;
        }
    }
    rbx68 = *reinterpret_cast<int32_t*>(&rcx65);
    rdi69 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + rbx68 * 8);
    r13_70 = reinterpret_cast<void*>(rbx68 * 8);
    eax71 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi69->f0));
    if (*reinterpret_cast<signed char*>(&eax71) == 45) {
        eax72 = rdi69->f1;
        if ((*reinterpret_cast<signed char*>(&eax72) == 0x6c || *reinterpret_cast<signed char*>(&eax72) == 0x67) && ((edx73 = rdi69->f2, *reinterpret_cast<signed char*>(&edx73) == 0x65) || *reinterpret_cast<signed char*>(&edx73) == 0x74)) {
            if (rdi69->f3) 
                goto addr_2d44_123; else 
                goto addr_2ea4_124;
        }
        if (*reinterpret_cast<signed char*>(&eax72) != 0x65) 
            goto addr_2d3c_126;
    } else {
        if (*reinterpret_cast<signed char*>(&eax71) != 61 || (eax74 = rdi69->f1, !!*reinterpret_cast<signed char*>(&eax74)) && (*reinterpret_cast<signed char*>(&eax74) != 61 || rdi69->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi69->f0) == 33) || (rdi69->f1 != 61 || rdi69->f2)) {
                fun_2370();
                goto addr_2665_100;
            } else {
                rax75 = reinterpret_cast<int32_t>(pos);
                rbx76 = rax75;
                rax77 = rax75 + 2;
                rsi78 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(rax77 * 8));
                rdi79 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(rax77 * 8) - 16);
                fun_24f0(rdi79, rsi78);
                pos = *reinterpret_cast<int32_t*>(&rbx76) + 3;
                goto addr_2cc9_131;
            }
        } else {
            rax80 = reinterpret_cast<int32_t>(pos);
            rbx81 = rax80;
            rax82 = rax80 + 2;
            rsi83 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(rax82 * 8));
            rdi84 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(rax82 * 8) - 16);
            fun_24f0(rdi84, rsi83);
            pos = *reinterpret_cast<int32_t*>(&rbx81) + 3;
            goto addr_2cc9_131;
        }
    }
    eax85 = rdi69->f2;
    if (*reinterpret_cast<signed char*>(&eax85) == 0x71) {
        addr_300c_134:
        if (!rdi69->f3) {
            addr_2ea4_124:
            rdi86 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) - 8);
            if (1) {
                rax87 = find_int(rdi86);
                rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                rbp89 = rax87;
            } else {
                rax90 = fun_2440(rdi86);
                rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                rax92 = umaxtostr(rax90, reinterpret_cast<int64_t>(rsp91) + 0x120);
                rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
                rbp89 = rax92;
            }
        } else {
            addr_3016_137:
            rax93 = quote(rdi69);
            rax94 = fun_2420();
            rax95 = test_syntax_error(rax94, rax93, 5, rcx65, r8_17, r9_21);
            goto addr_3040_138;
        }
    } else {
        addr_2e0c_139:
        if (*reinterpret_cast<signed char*>(&eax85) != 0x66) 
            goto addr_3016_137;
        if (rdi69->f3) 
            goto addr_3016_137; else 
            goto addr_2e1e_141;
    }
    rax96 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_67)) {
        rdi97 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax96) + reinterpret_cast<uint64_t>(r13_70) + 8);
        rax98 = find_int(rdi97, rdi97);
        rsi99 = rax98;
    } else {
        rdi100 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax96) + reinterpret_cast<uint64_t>(r13_70) + 16);
        rax101 = fun_2440(rdi100, rdi100);
        rax102 = umaxtostr(rax101, reinterpret_cast<int64_t>(rsp88) - 8 + 8 + 0x140);
        rsi99 = rax102;
    }
    strintcmp(rbp89, rsi99);
    rdx103 = argv;
    *reinterpret_cast<uint32_t*>(&rcx65) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx103) + rbx68 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx65 + 4) = 0;
    tmp32_104 = pos + 3;
    pos = tmp32_104;
    if (*reinterpret_cast<signed char*>(&rcx65) == 0x6c) {
        goto addr_2cc9_131;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx65) == 0x67) {
            goto addr_2cc9_131;
        } else {
            goto addr_2cc9_131;
        }
    }
    addr_3040_138:
    rdi105 = *rax95;
    eax106 = fun_2500(rdi105, r14_13, rdi105, r14_13);
    if (!eax106) {
        addr_2cc9_131:
        rax107 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax62) - reinterpret_cast<unsigned char>(g28));
        if (rax107) {
            fun_2450();
        } else {
            goto v108;
        }
    } else {
        addr_3053_152:
        goto addr_2cc9_131;
    }
    rsi109 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_154:
        rax110 = fun_2420();
        test_syntax_error(rax110, rsi109, 5, rcx65, r8_17, r9_21);
        addr_3102_155:
        rsi109 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_141:
    tmp32_111 = pos + 3;
    pos = tmp32_111;
    *reinterpret_cast<unsigned char*>(&r12_67) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_67)));
    if (!*reinterpret_cast<unsigned char*>(&r12_67)) {
        rdi112 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) - 8);
        eax113 = fun_2500(rdi112, rsp61);
        if (!eax113 && ((rdi114 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) + 8), eax115 = fun_2500(rdi114, reinterpret_cast<int64_t>(rsp61) - 8 + 8 + 0x90), !eax115) && v116 == v117)) {
            goto addr_2cc9_131;
        }
    }
    addr_2d3c_126:
    if (*reinterpret_cast<signed char*>(&eax72) == 0x6e) {
        eax118 = rdi69->f2;
        if (*reinterpret_cast<signed char*>(&eax118) == 0x65) 
            goto addr_300c_134;
    } else {
        addr_2d44_123:
        if (*reinterpret_cast<signed char*>(&eax72) != 0x6f) {
            if (*reinterpret_cast<signed char*>(&eax72) > 0x6f) 
                goto addr_3016_137;
            if (*reinterpret_cast<signed char*>(&eax72) == 0x65) 
                goto addr_30a8_161; else 
                goto addr_2f4e_162;
        } else {
            if (rdi69->f2 != 0x74) 
                goto addr_3016_137;
            if (rdi69->f3) 
                goto addr_3016_137;
            tmp32_119 = pos + 3;
            pos = tmp32_119;
            *reinterpret_cast<unsigned char*>(&r12_67) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_67)));
            if (*reinterpret_cast<unsigned char*>(&r12_67)) {
                rsi109 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                goto addr_30f1_154;
            } else {
                rdi120 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) - 8);
                r14_121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) + 0x90);
                eax122 = fun_2500(rdi120, r14_121);
                rdx123 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) + 8);
                if (eax122) {
                    rdi124 = *rdx123;
                    fun_2500(rdi124, r14_121);
                    goto addr_2cc9_131;
                } else {
                    rdi125 = *rdx123;
                    eax126 = fun_2500(rdi125, r14_121);
                    if (!eax126) {
                        *reinterpret_cast<uint32_t*>(&rcx65) = reinterpret_cast<uint1_t>(v127 < v128);
                        *reinterpret_cast<int32_t*>(&rcx65 + 4) = 0;
                        goto addr_2cc9_131;
                    }
                }
            }
        }
    }
    addr_2f5a_171:
    if (*reinterpret_cast<signed char*>(&eax118) != 0x74) 
        goto addr_3016_137;
    if (rdi69->f3) 
        goto addr_3016_137;
    tmp32_129 = pos + 3;
    pos = tmp32_129;
    *reinterpret_cast<unsigned char*>(&r12_67) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_67)));
    if (*reinterpret_cast<unsigned char*>(&r12_67)) 
        goto addr_3102_155;
    rdi130 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) - 8);
    r14_13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) + 0x90);
    eax131 = fun_2500(rdi130, r14_13);
    rax95 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp66) + reinterpret_cast<uint64_t>(r13_70) + 8);
    if (eax131) 
        goto addr_3040_138;
    rdi132 = *rax95;
    eax133 = fun_2500(rdi132, r14_13);
    if (eax133) 
        goto addr_3053_152;
    *reinterpret_cast<uint32_t*>(&rcx65) = reinterpret_cast<uint1_t>(v134 < v135);
    *reinterpret_cast<int32_t*>(&rcx65 + 4) = 0;
    goto addr_2cc9_131;
    addr_30a8_161:
    eax85 = rdi69->f2;
    goto addr_2e0c_139;
    addr_2f4e_162:
    if (*reinterpret_cast<signed char*>(&eax72) != 0x6e) 
        goto addr_3016_137;
    eax118 = rdi69->f2;
    goto addr_2f5a_171;
    addr_2665_100:
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    addr_3d1b_66:
    pos = *reinterpret_cast<uint32_t*>(&rbx26) + 3;
    addr_3ceb_192:
    goto v136;
    addr_3d4a_67:
    rax137 = quote(rbp37, rbp37);
    rax138 = fun_2420();
    test_syntax_error(rax138, rax137, 5, rcx2, r8_17, r9_21);
    rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d78_82;
    addr_3db6_77:
    goto addr_3d4a_67;
    addr_3ce3_74:
    two_arguments();
    goto addr_3ceb_192;
    addr_3eca_88:
    eax139 = *reinterpret_cast<uint32_t*>(&rax11) + 1;
    less140 = reinterpret_cast<int32_t>(eax139) < reinterpret_cast<int32_t>(argc);
    pos = eax139;
    if (!less140) {
        addr_3ee5_94:
        beyond();
    } else {
        three_arguments();
    }
    addr_3eb1_194:
    goto v141;
    addr_3e39_93:
    pos = *reinterpret_cast<uint32_t*>(&rax11) + 1;
    two_arguments();
    tmp32_142 = pos + 1;
    pos = tmp32_142;
    goto addr_3eb1_194;
    addr_3e90_97:
    rax143 = reinterpret_cast<int32_t>(pos);
    pos = static_cast<uint32_t>(rax143 + 1);
    goto addr_3eb1_194;
    addr_3165_105:
    *reinterpret_cast<uint32_t*>(&rdx144) = *reinterpret_cast<unsigned char*>(&edx49);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx144) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa1f0 + rdx144 * 4) + 0xa1f0;
    addr_3207_112:
    return eax57;
}

uint32_t binary_operator(uint32_t edi, struct s0* rsi) {
    uint32_t esi3;
    void* rsp4;
    struct s0* rax5;
    int64_t rax6;
    uint32_t edx7;
    struct s0* rcx8;
    struct s0* rbp9;
    int64_t r12_10;
    int64_t rbx11;
    struct s0* rdi12;
    void* r13_13;
    uint32_t eax14;
    uint32_t eax15;
    int64_t rax16;
    int64_t rbx17;
    int64_t rax18;
    struct s0* rsi19;
    struct s0* rdi20;
    int32_t eax21;
    int64_t rax22;
    int64_t rbx23;
    int64_t rax24;
    struct s0* rsi25;
    struct s0* rdi26;
    int32_t eax27;
    uint32_t eax28;
    uint32_t edx29;
    uint32_t eax30;
    struct s0* rdi31;
    struct s0* rax32;
    void* rsp33;
    struct s0* rbp34;
    struct s0* rax35;
    void* rsp36;
    struct s0* rax37;
    struct s0* rax38;
    struct s0* rax39;
    struct s0* r8_40;
    int64_t r9_41;
    int64_t* rax42;
    uint32_t eax43;
    uint32_t tmp32_44;
    struct s0* rsi45;
    int64_t rdi46;
    void* r14_47;
    int32_t eax48;
    int64_t* rdx49;
    int64_t rdi50;
    int32_t eax51;
    int64_t rdi52;
    int32_t eax53;
    int32_t edx54;
    int64_t v55;
    int64_t v56;
    int64_t rdx57;
    int64_t v58;
    int32_t eax59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t rax63;
    struct s0* rax64;
    struct s0* rdi65;
    struct s0* rax66;
    struct s0* rsi67;
    struct s0* rdi68;
    struct s0* rax69;
    struct s0* rax70;
    uint32_t eax71;
    struct s0* rdx72;
    unsigned char dl73;
    uint32_t tmp32_74;
    int64_t rdi75;
    void* r14_76;
    int32_t eax77;
    void* rax78;
    struct s0* rax79;
    struct s0* r8_80;
    int64_t r9_81;
    uint32_t tmp32_82;
    int64_t rdi83;
    int32_t eax84;
    int64_t rdi85;
    int32_t eax86;
    int64_t v87;
    int64_t v88;
    int64_t v89;
    int64_t v90;
    uint32_t tmp32_91;
    int64_t rdi92;
    int32_t eax93;
    int64_t rdi94;
    int64_t v95;
    int32_t eax96;
    int32_t edx97;
    int64_t v98;
    int64_t rdx99;
    int64_t v100;
    int32_t eax101;
    int64_t v102;
    int64_t v103;
    int64_t v104;
    int64_t rax105;
    int32_t eax106;

    esi3 = edi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax5 = g28;
    *reinterpret_cast<uint32_t*>(&rax6) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&edi)) {
        *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<uint32_t*>(&rax6) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax6);
    }
    edx7 = argc;
    *reinterpret_cast<uint32_t*>(&rcx8) = static_cast<uint32_t>(rax6 + 1);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    rbp9 = argv;
    *reinterpret_cast<uint32_t*>(&r12_10) = 0;
    if (reinterpret_cast<int32_t>(edx7 - 2) > *reinterpret_cast<int32_t*>(&rcx8)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f2)) {
            *reinterpret_cast<uint32_t*>(&r12_10) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx8);
            *reinterpret_cast<uint32_t*>(&r12_10) = 1;
        }
    }
    rbx11 = *reinterpret_cast<int32_t*>(&rcx8);
    rdi12 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + rbx11 * 8);
    r13_13 = reinterpret_cast<void*>(rbx11 * 8);
    eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi12->f0));
    if (*reinterpret_cast<signed char*>(&eax14) != 45) {
        if (*reinterpret_cast<signed char*>(&eax14) != 61 || (eax15 = rdi12->f1, !!*reinterpret_cast<signed char*>(&eax15)) && (*reinterpret_cast<signed char*>(&eax15) != 61 || rdi12->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi12->f0) == 33) || (rdi12->f1 != 61 || rdi12->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax16 = reinterpret_cast<int32_t>(pos);
                rbx17 = rax16;
                rax18 = rax16 + 2;
                rsi19 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax18 * 8));
                rdi20 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax18 * 8) - 16);
                eax21 = fun_24f0(rdi20, rsi19);
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!!eax21);
                pos = *reinterpret_cast<int32_t*>(&rbx17) + 3;
                goto addr_2cc9_27;
            }
        } else {
            rax22 = reinterpret_cast<int32_t>(pos);
            rbx23 = rax22;
            rax24 = rax22 + 2;
            rsi25 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax24 * 8));
            rdi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax24 * 8) - 16);
            eax27 = fun_24f0(rdi26, rsi25);
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(eax27 == 0);
            pos = *reinterpret_cast<int32_t*>(&rbx23) + 3;
            goto addr_2cc9_27;
        }
    }
    eax28 = rdi12->f1;
    if ((*reinterpret_cast<signed char*>(&eax28) == 0x6c || *reinterpret_cast<signed char*>(&eax28) == 0x67) && ((edx29 = rdi12->f2, *reinterpret_cast<signed char*>(&edx29) == 0x65) || *reinterpret_cast<signed char*>(&edx29) == 0x74)) {
        if (rdi12->f3) 
            goto addr_2d44_31; else 
            goto addr_2ea4_32;
    }
    if (*reinterpret_cast<signed char*>(&eax28) == 0x65) {
        eax30 = rdi12->f2;
        if (*reinterpret_cast<signed char*>(&eax30) == 0x71) {
            addr_300c_35:
            if (!rdi12->f3) {
                addr_2ea4_32:
                rdi31 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi3)) {
                    rax32 = find_int(rdi31);
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
                    rbp34 = rax32;
                } else {
                    rax35 = fun_2440(rdi31);
                    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
                    rax37 = umaxtostr(rax35, reinterpret_cast<int64_t>(rsp36) + 0x120);
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                    rbp34 = rax37;
                }
            } else {
                addr_3016_38:
                rax38 = quote(rdi12);
                rax39 = fun_2420();
                rax42 = test_syntax_error(rax39, rax38, 5, rcx8, r8_40, r9_41);
                goto addr_3040_39;
            }
        } else {
            addr_2e0c_40:
            if (*reinterpret_cast<signed char*>(&eax30) != 0x66) 
                goto addr_3016_38;
            if (rdi12->f3) 
                goto addr_3016_38; else 
                goto addr_2e1e_42;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax28) == 0x6e) {
            eax43 = rdi12->f2;
            if (*reinterpret_cast<signed char*>(&eax43) != 0x65) 
                goto addr_2f5a_45; else 
                goto addr_300c_35;
        } else {
            addr_2d44_31:
            if (*reinterpret_cast<signed char*>(&eax28) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax28) > 0x6f) 
                    goto addr_3016_38;
                if (*reinterpret_cast<signed char*>(&eax28) == 0x65) 
                    goto addr_30a8_48; else 
                    goto addr_2f4e_49;
            } else {
                if (rdi12->f2 != 0x74) 
                    goto addr_3016_38;
                if (rdi12->f3) 
                    goto addr_3016_38;
                tmp32_44 = pos + 3;
                pos = tmp32_44;
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
                if (*reinterpret_cast<unsigned char*>(&r12_10)) {
                    rsi45 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_54;
                } else {
                    rdi46 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
                    r14_47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 0x90);
                    eax48 = fun_2500(rdi46, r14_47);
                    rdx49 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8);
                    if (eax48) {
                        rdi50 = *rdx49;
                        eax51 = fun_2500(rdi50, r14_47);
                        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(eax51 == 0);
                        goto addr_2cc9_27;
                    } else {
                        rdi52 = *rdx49;
                        eax53 = fun_2500(rdi52, r14_47);
                        if (!eax53) {
                            edx54 = 0;
                            *reinterpret_cast<unsigned char*>(&edx54) = reinterpret_cast<uint1_t>(v55 > v56);
                            *reinterpret_cast<uint32_t*>(&rdx57) = edx54 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(v55 < v58));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx57) + 4) = 0;
                            eax59 = 0;
                            *reinterpret_cast<unsigned char*>(&eax59) = reinterpret_cast<uint1_t>(v60 > v61);
                            *reinterpret_cast<uint32_t*>(&rcx8) = reinterpret_cast<uint1_t>(v60 < v62);
                            *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rax63) = eax59 - *reinterpret_cast<uint32_t*>(&rcx8);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax63) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&r12_10) = static_cast<uint32_t>(rax63 + rdx57 * 2) >> 31;
                            goto addr_2cc9_27;
                        }
                    }
                }
            }
        }
    }
    rax64 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_10)) {
        rdi65 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax64) + reinterpret_cast<uint64_t>(r13_13) + 8);
        rax66 = find_int(rdi65, rdi65);
        rsi67 = rax66;
    } else {
        rdi68 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax64) + reinterpret_cast<uint64_t>(r13_13) + 16);
        rax69 = fun_2440(rdi68, rdi68);
        rax70 = umaxtostr(rax69, reinterpret_cast<int64_t>(rsp33) - 8 + 8 + 0x140);
        rsi67 = rax70;
    }
    eax71 = strintcmp(rbp34, rsi67);
    rdx72 = argv;
    *reinterpret_cast<uint32_t*>(&rcx8) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx72) + rbx11 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    dl73 = reinterpret_cast<uint1_t>((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx72) + rbx11 * 8))->f2 == 0x65);
    tmp32_74 = pos + 3;
    pos = tmp32_74;
    if (*reinterpret_cast<signed char*>(&rcx8) == 0x6c) {
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(static_cast<uint32_t>(dl73)) > reinterpret_cast<int32_t>(eax71));
        goto addr_2cc9_27;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx8) == 0x67) {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(-static_cast<uint32_t>(dl73)) < reinterpret_cast<int32_t>(eax71));
            goto addr_2cc9_27;
        } else {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!eax71)) == dl73);
            goto addr_2cc9_27;
        }
    }
    addr_3040_39:
    rdi75 = *rax42;
    eax77 = fun_2500(rdi75, r14_76, rdi75, r14_76);
    if (!eax77) {
        addr_2cc9_27:
        rax78 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax78) {
            fun_2450();
        } else {
            return *reinterpret_cast<uint32_t*>(&r12_10);
        }
    } else {
        addr_3053_69:
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx11) == 0);
        goto addr_2cc9_27;
    }
    rsi45 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_54:
        rax79 = fun_2420();
        test_syntax_error(rax79, rsi45, 5, rcx8, r8_80, r9_81);
        addr_3102_71:
        rsi45 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_42:
    tmp32_82 = pos + 3;
    pos = tmp32_82;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
    if (!*reinterpret_cast<unsigned char*>(&r12_10)) {
        rdi83 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
        eax84 = fun_2500(rdi83, rsp4);
        if (!eax84 && ((rdi85 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8), eax86 = fun_2500(rdi85, reinterpret_cast<int64_t>(rsp4) - 8 + 8 + 0x90), !eax86) && v87 == v88)) {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(v89 == v90);
            goto addr_2cc9_27;
        }
    }
    addr_2f5a_45:
    if (*reinterpret_cast<signed char*>(&eax43) != 0x74) 
        goto addr_3016_38;
    if (rdi12->f3) 
        goto addr_3016_38;
    tmp32_91 = pos + 3;
    pos = tmp32_91;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
    if (*reinterpret_cast<unsigned char*>(&r12_10)) 
        goto addr_3102_71;
    rdi92 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
    r14_76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 0x90);
    eax93 = fun_2500(rdi92, r14_76);
    *reinterpret_cast<int32_t*>(&rbx11) = eax93;
    rax42 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8);
    if (*reinterpret_cast<int32_t*>(&rbx11)) 
        goto addr_3040_39;
    rdi94 = *rax42;
    r12_10 = v95;
    eax96 = fun_2500(rdi94, r14_76);
    if (eax96) 
        goto addr_3053_69;
    edx97 = 0;
    *reinterpret_cast<unsigned char*>(&edx97) = reinterpret_cast<uint1_t>(r12_10 > v98);
    *reinterpret_cast<uint32_t*>(&rdx99) = edx97 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_10 < v100));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx99) + 4) = 0;
    eax101 = 0;
    *reinterpret_cast<unsigned char*>(&eax101) = reinterpret_cast<uint1_t>(v102 > v103);
    *reinterpret_cast<uint32_t*>(&rcx8) = reinterpret_cast<uint1_t>(v102 < v104);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax105) = eax101 - *reinterpret_cast<uint32_t*>(&rcx8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
    eax106 = static_cast<int32_t>(rax105 + rdx99 * 2);
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax106 < 0) | reinterpret_cast<uint1_t>(eax106 == 0)));
    goto addr_2cc9_27;
    addr_30a8_48:
    eax30 = rdi12->f2;
    goto addr_2e0c_40;
    addr_2f4e_49:
    if (*reinterpret_cast<signed char*>(&eax28) != 0x6e) 
        goto addr_3016_38;
    eax43 = rdi12->f2;
    goto addr_2f5a_45;
}

struct s11 {
    unsigned char f0;
    signed char f1;
};

struct s12 {
    signed char f0;
    signed char f1;
};

struct s13 {
    signed char f0;
    signed char f1;
};

uint32_t three_arguments() {
    struct s0* v1;
    struct s0* r13_2;
    struct s0* v3;
    struct s0* r12_4;
    struct s0* v5;
    struct s0* rbp6;
    struct s0* rax7;
    struct s0* rbx8;
    struct s1* rax9;
    struct s0* rdi10;
    struct s0* rsi11;
    uint32_t eax12;
    void* rsp13;
    uint32_t edx14;
    int1_t less15;
    struct s12* rax16;
    struct s0* rax17;
    int1_t less18;
    struct s0* rdx19;
    struct s0* rcx20;
    int1_t less21;
    struct s0* v22;
    int64_t rbx23;
    int64_t rdx24;
    unsigned char v25;
    uint32_t ebp26;
    int1_t zf27;
    uint32_t ebx28;
    void* r14_29;
    int64_t r15_30;
    struct s0* rcx31;
    struct s0* r8_32;
    struct s0* rdi33;
    uint32_t eax34;
    int64_t r9_35;
    uint32_t eax36;
    uint32_t ecx37;
    struct s0** rcx38;
    uint32_t r8d39;
    struct s0* rdx40;
    struct s0* rax41;
    struct s0* rax42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* rax45;
    struct s0** rsp46;
    int64_t rax47;
    int64_t rdx48;
    int64_t rdx49;
    struct s0* rax50;
    struct s0* rdi51;
    int32_t edx52;
    struct s0* rax53;
    struct s0* rax54;
    int64_t* rax55;
    void* rsp56;
    uint32_t edx57;
    int1_t less58;
    void* rsp59;
    uint32_t eax60;
    int32_t eax61;
    void* rdx62;
    void* rsp63;
    struct s0* rax64;
    int64_t rax65;
    uint32_t edx66;
    struct s0* rcx67;
    struct s0* rbp68;
    int64_t r12_69;
    int64_t rbx70;
    struct s0* rdi71;
    void* r13_72;
    uint32_t eax73;
    uint32_t eax74;
    uint32_t edx75;
    uint32_t eax76;
    int64_t rax77;
    int64_t rbx78;
    int64_t rax79;
    struct s0* rsi80;
    struct s0* rdi81;
    int32_t eax82;
    int64_t rax83;
    int64_t rbx84;
    int64_t rax85;
    struct s0* rsi86;
    struct s0* rdi87;
    int32_t eax88;
    uint32_t eax89;
    struct s0* rdi90;
    struct s0* rax91;
    void* rsp92;
    struct s0* rbp93;
    struct s0* rax94;
    void* rsp95;
    struct s0* rax96;
    struct s0* rax97;
    struct s0* rax98;
    int64_t* rax99;
    struct s0* rax100;
    struct s0* rdi101;
    struct s0* rax102;
    struct s0* rsi103;
    struct s0* rdi104;
    struct s0* rax105;
    struct s0* rax106;
    uint32_t eax107;
    struct s0* rdx108;
    unsigned char dl109;
    uint32_t tmp32_110;
    int64_t rdi111;
    int32_t eax112;
    void* rax113;
    struct s0* rsi114;
    struct s0* rax115;
    uint32_t tmp32_116;
    int64_t rdi117;
    int32_t eax118;
    int64_t rdi119;
    int32_t eax120;
    int64_t v121;
    int64_t v122;
    int64_t v123;
    int64_t v124;
    uint32_t eax125;
    uint32_t tmp32_126;
    int64_t rdi127;
    void* r14_128;
    int32_t eax129;
    int64_t* rdx130;
    int64_t rdi131;
    int32_t eax132;
    int64_t rdi133;
    int32_t eax134;
    int32_t edx135;
    int64_t v136;
    int64_t v137;
    int64_t rdx138;
    int64_t v139;
    int32_t eax140;
    int64_t v141;
    int64_t v142;
    int64_t v143;
    int64_t rax144;
    uint32_t tmp32_145;
    int64_t rdi146;
    int32_t eax147;
    int64_t rdi148;
    int64_t v149;
    int32_t eax150;
    int32_t edx151;
    int64_t v152;
    int64_t rdx153;
    int64_t v154;
    int32_t eax155;
    int64_t v156;
    int64_t v157;
    int64_t v158;
    int64_t rax159;
    int32_t eax160;
    struct s0* rax161;
    struct s0* rax162;
    uint32_t eax163;
    int1_t less164;
    uint32_t tmp32_165;
    int64_t rax166;
    int64_t rdx167;
    uint32_t eax168;

    while (v1 = r13_2, v3 = r12_4, v5 = rbp6, rax7 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos))), r12_4 = argv, rbx8 = rax7, rax9 = reinterpret_cast<struct s1*>(&rax7->f1), rbp6 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<uint64_t>(rax9) * 8), r13_2 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax9) * 8), rdi10 = rbp6, eax12 = binop(rdi10, rsi11, rdi10, rsi11), rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 + 8), !*reinterpret_cast<signed char*>(&eax12)) {
        edx14 = (*reinterpret_cast<struct s11**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r13_2) + 0xfffffffffffffff8))->f0;
        if (edx14 == 33 && !(*reinterpret_cast<struct s11**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r13_2) + 0xfffffffffffffff8))->f1) {
            *reinterpret_cast<uint32_t*>(&rbx8) = *reinterpret_cast<uint32_t*>(&rbx8) + 1;
            less15 = *reinterpret_cast<int32_t*>(&rbx8) < reinterpret_cast<int32_t>(argc);
            pos = *reinterpret_cast<uint32_t*>(&rbx8);
            if (!less15) 
                goto addr_3db8_4; else 
                goto addr_3ce3_5;
        }
        if (edx14 != 40) 
            goto addr_3d30_7;
        if ((*reinterpret_cast<struct s11**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r13_2) + 0xfffffffffffffff8))->f1) 
            goto addr_3d30_7;
        rax16 = *reinterpret_cast<struct s12**>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(r13_2) + 8);
        if (rax16->f0 != 41) 
            goto addr_3d30_7;
        if (!rax16->f1) 
            goto addr_3d1b_11;
        addr_3d30_7:
        *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp6->f0));
        if (*reinterpret_cast<uint32_t*>(&rax17) != 45) 
            goto addr_3d4a_12;
        if (rbp6->f1 != 97) 
            goto addr_3d3f_14;
        if (!rbp6->f2) 
            goto addr_3d96_16;
        addr_3d3f_14:
        if (*reinterpret_cast<uint32_t*>(&rax17) != 45) 
            goto addr_3d4a_12;
        if (rbp6->f1 != 0x6f) 
            goto addr_3d4a_12;
        if (rbp6->f2) 
            goto addr_3db6_19;
        addr_3d96_16:
        less18 = *reinterpret_cast<int32_t*>(&rbx8) < reinterpret_cast<int32_t>(argc);
        if (!less18) {
            addr_3db8_4:
            beyond();
        } else {
            rbp6 = v5;
            r12_4 = v3;
            r13_2 = v1;
            goto addr_3900_21;
        }
        if (*reinterpret_cast<uint32_t*>(&rdi10) == 3) {
            continue;
        }
        if (*reinterpret_cast<int32_t*>(&rdi10) > reinterpret_cast<int32_t>(3)) {
            if (*reinterpret_cast<uint32_t*>(&rdi10) != 4) {
                addr_3e60_26:
                *reinterpret_cast<uint32_t*>(&rax17) = pos;
            } else {
                rdx19 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                rsi11 = argv;
                rax17 = rdx19;
                rdi10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx19) * 8);
                *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi11) + reinterpret_cast<unsigned char>(rdx19) * 8))->f0));
                *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rcx20) != 33) 
                    goto addr_3e1e_28;
                if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi11) + reinterpret_cast<unsigned char>(rdx19) * 8))->f1) 
                    goto addr_3eca_30;
                addr_3e1e_28:
                if (*reinterpret_cast<uint32_t*>(&rcx20) != 40) 
                    goto addr_3e66_31;
                if ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi11) + reinterpret_cast<unsigned char>(rdx19) * 8))->f1) 
                    goto addr_3e66_31;
                if ((*reinterpret_cast<struct s13**>(reinterpret_cast<unsigned char>(rsi11) + reinterpret_cast<unsigned char>(rdi10) + 24))->f0 != 41) 
                    goto addr_3e66_31;
                if (!(*reinterpret_cast<struct s13**>(reinterpret_cast<unsigned char>(rsi11) + reinterpret_cast<unsigned char>(rdi10) + 24))->f1) 
                    goto addr_3e39_35;
            }
            addr_3e66_31:
            less21 = *reinterpret_cast<int32_t*>(&rax17) < reinterpret_cast<int32_t>(argc);
            if (!less21) 
                goto addr_3ee5_36;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rdi10) == 1) 
                goto addr_3e90_39;
            if (*reinterpret_cast<uint32_t*>(&rdi10) != 2) 
                goto addr_3e50_41; else 
                goto addr_3ddd_42;
        }
        addr_3900_21:
        v22 = rbp6;
        *reinterpret_cast<uint32_t*>(&rbx23) = pos;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx23) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx24) = argc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
        v25 = 0;
        while (1) {
            ebp26 = 1;
            zf27 = *reinterpret_cast<uint32_t*>(&rbx23) == *reinterpret_cast<uint32_t*>(&rdx24);
            if (*reinterpret_cast<int32_t*>(&rbx23) >= *reinterpret_cast<int32_t*>(&rdx24)) 
                goto addr_3a9d_44;
            goto addr_3930_46;
            addr_3a5a_47:
            v25 = reinterpret_cast<unsigned char>(v25 | *reinterpret_cast<unsigned char*>(&ebp26));
            if (*reinterpret_cast<uint32_t*>(&rcx20) != 45) 
                goto addr_3a68_48;
            if (rax17->f1 != 0x6f) 
                goto addr_3a68_48;
            if (rax17->f2) 
                goto addr_3a68_48;
            *reinterpret_cast<uint32_t*>(&rbx23) = ebx28 + 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx23) + 4) = 0;
            pos = *reinterpret_cast<uint32_t*>(&rbx23);
            continue;
            while (1) {
                addr_3a0c_52:
                if (*reinterpret_cast<signed char*>(&r14_29) != 45 || !r13_2->f1) {
                    addr_3a28_53:
                    ebx28 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx20->f1));
                    pos = ebx28;
                    *reinterpret_cast<unsigned char*>(&rax17) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&r14_29));
                } else {
                    if (!r13_2->f2) {
                        *reinterpret_cast<uint32_t*>(&rax17) = unary_operator(rdi10, rsi11);
                        ebx28 = pos;
                        *reinterpret_cast<uint32_t*>(&rdx24) = argc;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                    } else {
                        goto addr_3a28_53;
                    }
                }
                while (1) {
                    ebp26 = ebp26 & (*reinterpret_cast<uint32_t*>(&rax17) ^ *reinterpret_cast<uint32_t*>(&r15_30));
                    if (reinterpret_cast<int32_t>(ebx28) >= *reinterpret_cast<int32_t*>(&rdx24)) 
                        goto addr_3af0_58;
                    while ((rcx31 = argv, rax17 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx31) + reinterpret_cast<int32_t>(ebx28) * 8), *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax17->f0)), *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx20) == 45) && (rax17->f1 == 97 && !rax17->f2)) {
                        *reinterpret_cast<uint32_t*>(&rbx23) = ebx28 + 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx23) + 4) = 0;
                        pos = *reinterpret_cast<uint32_t*>(&rbx23);
                        zf27 = *reinterpret_cast<uint32_t*>(&rbx23) == *reinterpret_cast<uint32_t*>(&rdx24);
                        if (*reinterpret_cast<int32_t*>(&rbx23) < *reinterpret_cast<int32_t*>(&rdx24)) {
                            addr_3930_46:
                            r8_32 = argv;
                            rbx23 = *reinterpret_cast<int32_t*>(&rbx23);
                            *reinterpret_cast<uint32_t*>(&rdi10) = 0;
                            *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&r15_30) = 0;
                            while (r13_2 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + rbx23 * 8), *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<uint32_t*>(&rbx23), *reinterpret_cast<int32_t*>(&rax17 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx20) = *reinterpret_cast<uint32_t*>(&rbx23), *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0, *reinterpret_cast<uint32_t*>(&r14_29) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r13_2->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_29) + 4) = 0, *reinterpret_cast<signed char*>(&r14_29) == 33) {
                                if (r13_2->f1) 
                                    goto addr_3ab8_63;
                                ++rbx23;
                                *reinterpret_cast<uint32_t*>(&r12_4) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rax17->f1));
                                if (*reinterpret_cast<int32_t*>(&rdx24) <= *reinterpret_cast<int32_t*>(&rbx23)) 
                                    goto addr_3bf6_65;
                                *reinterpret_cast<uint32_t*>(&r15_30) = *reinterpret_cast<uint32_t*>(&r15_30) ^ 1;
                                *reinterpret_cast<uint32_t*>(&rdi10) = 1;
                                *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                            }
                        } else {
                            addr_3a9d_44:
                            beyond();
                            goto addr_3aa8_67;
                        }
                        if (*reinterpret_cast<signed char*>(&rdi10)) {
                            pos = *reinterpret_cast<uint32_t*>(&r12_4);
                        }
                        if (*reinterpret_cast<signed char*>(&r14_29) != 40 || r13_2->f1) {
                            addr_399c_71:
                            *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<uint32_t*>(&rdx24) - *reinterpret_cast<uint32_t*>(&rcx20);
                            zf27 = *reinterpret_cast<uint32_t*>(&rax17) == 3;
                            if (*reinterpret_cast<int32_t*>(&rax17) <= reinterpret_cast<int32_t>(3)) {
                                addr_3aa8_67:
                                if (!zf27) 
                                    goto addr_3a0c_52;
                            } else {
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_2->f0) == 45)) 
                                    goto addr_39ea_73;
                                if (r13_2->f1 != 0x6c) 
                                    goto addr_39ea_73;
                                if (r13_2->f2) 
                                    goto addr_39ea_73;
                                rdi33 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + rbx23 * 8 + 16);
                                eax34 = binop(rdi33, rsi11);
                                r8_32 = r8_32;
                                *reinterpret_cast<uint32_t*>(&rcx20) = *reinterpret_cast<uint32_t*>(&rcx20);
                                *reinterpret_cast<uint32_t*>(&rdx24) = *reinterpret_cast<uint32_t*>(&rdx24);
                                if (*reinterpret_cast<signed char*>(&eax34)) 
                                    goto addr_3ba3_77; else 
                                    goto addr_39ea_73;
                            }
                        } else {
                            *reinterpret_cast<uint32_t*>(&r9_35) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx20->f1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_35) + 4) = 0;
                            pos = *reinterpret_cast<uint32_t*>(&r9_35);
                            zf27 = *reinterpret_cast<uint32_t*>(&r9_35) == *reinterpret_cast<uint32_t*>(&rdx24);
                            if (*reinterpret_cast<int32_t*>(&r9_35) >= *reinterpret_cast<int32_t*>(&rdx24)) 
                                goto addr_3a9d_44; else 
                                goto addr_3b10_79;
                        }
                        addr_39ea_73:
                        rdi10 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + rbx23 * 8 + 8);
                        *reinterpret_cast<uint32_t*>(&rax17) = binop(rdi10, rsi11);
                        *reinterpret_cast<uint32_t*>(&rcx20) = *reinterpret_cast<uint32_t*>(&rcx20);
                        *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rdx24) = *reinterpret_cast<uint32_t*>(&rdx24);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                        if (!*reinterpret_cast<unsigned char*>(&rax17)) 
                            goto addr_3a0c_52;
                        *reinterpret_cast<uint32_t*>(&rdi10) = 0;
                        *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                        eax36 = binary_operator(0, rsi11);
                        ebx28 = pos;
                        *reinterpret_cast<uint32_t*>(&rdx24) = argc;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                        ebp26 = ebp26 & (eax36 ^ *reinterpret_cast<uint32_t*>(&r15_30));
                        if (reinterpret_cast<int32_t>(ebx28) < *reinterpret_cast<int32_t*>(&rdx24)) 
                            continue; else 
                            goto addr_3af0_58;
                        addr_3ab8_63:
                        if (*reinterpret_cast<signed char*>(&rdi10)) {
                            pos = *reinterpret_cast<uint32_t*>(&r12_4);
                            goto addr_399c_71;
                        }
                    }
                    goto addr_3a5a_47;
                    addr_3bf6_65:
                    pos = *reinterpret_cast<uint32_t*>(&r12_4);
                    beyond();
                    goto addr_3c02_83;
                    addr_3ba3_77:
                    *reinterpret_cast<uint32_t*>(&rdi10) = 1;
                    *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax17) = binary_operator(1, rsi11);
                    ebx28 = pos;
                    *reinterpret_cast<uint32_t*>(&rdx24) = argc;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                    continue;
                    addr_3b10_79:
                    ecx37 = *reinterpret_cast<uint32_t*>(&rcx20) + 2;
                    if (reinterpret_cast<int32_t>(ecx37) >= *reinterpret_cast<int32_t*>(&rdx24)) {
                        addr_3c02_83:
                        *reinterpret_cast<uint32_t*>(&rdi10) = 1;
                        *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rdi10) = 1;
                        *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                        rcx38 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + reinterpret_cast<int32_t>(ecx37) * 8);
                        r8d39 = static_cast<int32_t>(rdx24 - 1) - *reinterpret_cast<uint32_t*>(&rax17);
                        do {
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*rcx38)->f0) == 41)) 
                                goto addr_3b3c_86;
                            if (!(*rcx38)->f1) 
                                break;
                            addr_3b3c_86:
                            if (*reinterpret_cast<uint32_t*>(&rdi10) == 4) 
                                goto addr_3b9c_88;
                            *reinterpret_cast<uint32_t*>(&rdi10) = *reinterpret_cast<uint32_t*>(&rdi10) + 1;
                            *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                            rcx38 = rcx38 + 8;
                        } while (*reinterpret_cast<uint32_t*>(&rdi10) != r8d39);
                        goto addr_3b4d_90;
                    }
                    addr_3b50_91:
                    *reinterpret_cast<uint32_t*>(&rax17) = posixtest(rdi10, rsi11);
                    rcx20 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                    rdx40 = argv;
                    r8_32 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx40) + reinterpret_cast<unsigned char>(rcx20) * 8);
                    rbx8 = rcx20;
                    if (!r8_32) 
                        goto addr_3c0c_92;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r8_32->f0) == 41)) 
                        goto addr_3c3b_94;
                    if (r8_32->f1) 
                        goto addr_3c3b_94;
                    ebx28 = *reinterpret_cast<uint32_t*>(&rbx8) + 1;
                    *reinterpret_cast<uint32_t*>(&rdx24) = argc;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                    pos = ebx28;
                    continue;
                    addr_3b9c_88:
                    *reinterpret_cast<uint32_t*>(&rdi10) = *reinterpret_cast<uint32_t*>(&rdx24) - *reinterpret_cast<uint32_t*>(&r9_35);
                    *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
                    goto addr_3b50_91;
                    addr_3b4d_90:
                    goto addr_3b50_91;
                }
            }
        }
        addr_3c0c_92:
        rax41 = quote(")", ")");
        rax42 = fun_2420();
        test_syntax_error(rax42, rax41, 5, rcx20, r8_32, r9_35);
        addr_3c3b_94:
        rax43 = quote_n();
        r12_4 = rax43;
        rax44 = quote_n();
        rbp6 = rax44;
        rax45 = fun_2420();
        rsi11 = rbp6;
        test_syntax_error(rax45, rsi11, r12_4, rcx20, r8_32, r9_35);
        continue;
        addr_3e50_41:
        *reinterpret_cast<uint32_t*>(&rdi10) = *reinterpret_cast<uint32_t*>(&rdi10) - 1;
        *reinterpret_cast<int32_t*>(&rdi10 + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdi10)) 
            goto addr_2665_97;
        goto addr_3e60_26;
        addr_3ddd_42:
        rsp46 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8);
        while ((rax47 = reinterpret_cast<int32_t>(pos), rcx20 = argv, rdx48 = rax47, rsi11 = reinterpret_cast<struct s0*>(rax47 * 8), rax17 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx20) + rax47 * 8), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax17->f0) == 33)) || rax17->f1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax17->f0) == 45)) 
                goto addr_38f6_101;
            if (!rax17->f1) 
                goto addr_38f6_101;
            if (rax17->f2) 
                goto addr_38f6_101;
            rdx49 = reinterpret_cast<int32_t>(pos);
            r8_32 = argv;
            rax50 = g28;
            rdi51 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + rdx49 * 8);
            *reinterpret_cast<uint32_t*>(&rbx8) = rdi51->f1;
            *reinterpret_cast<int32_t*>(&rbx8 + 4) = 0;
            edx52 = static_cast<int32_t>(reinterpret_cast<unsigned char>(rbx8) + 0xffffffffffffffb9);
            if (*reinterpret_cast<unsigned char*>(&edx52) <= 51) 
                goto addr_3165_106;
            rax53 = quote(rdi51, rdi51);
            rbp6 = rax53;
            rax54 = fun_2420();
            rdi10 = rax54;
            rax55 = test_syntax_error(rdi10, rbp6, 5, rcx20, r8_32, r9_35);
            rsp56 = reinterpret_cast<void*>(rsp46 - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
            edx57 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax55) + 1);
            less58 = reinterpret_cast<int32_t>(edx57) < reinterpret_cast<int32_t>(argc);
            pos = edx57;
            if (!less58) {
                beyond();
                rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            } else {
                eax60 = *reinterpret_cast<int32_t*>(&rax55) + 2;
                pos = eax60;
                rdi10 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_32) + reinterpret_cast<int32_t>(eax60) * 8 - 8);
                eax61 = fun_24b0(rdi10, rsp56);
                rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r8_32) = eax61;
                *reinterpret_cast<int32_t*>(&r8_32 + 4) = 0;
                if (!*reinterpret_cast<int32_t*>(&r8_32)) {
                }
                rdx62 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax50) - reinterpret_cast<unsigned char>(g28));
                if (!rdx62) 
                    goto addr_3207_112;
            }
            fun_2450();
            rsp46 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
        }
        goto addr_38c8_114;
        addr_38f6_101:
        beyond();
        goto addr_3900_21;
    }
    addr_3d78_115:
    rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax64 = g28;
    *reinterpret_cast<uint32_t*>(&rax65) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax65) + 4) = 0;
    if (0) {
        *reinterpret_cast<uint32_t*>(&rax65) = *reinterpret_cast<uint32_t*>(&rax65) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax65) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax65);
    }
    edx66 = argc;
    *reinterpret_cast<uint32_t*>(&rcx67) = static_cast<uint32_t>(rax65 + 1);
    *reinterpret_cast<int32_t*>(&rcx67 + 4) = 0;
    rbp68 = argv;
    *reinterpret_cast<uint32_t*>(&r12_69) = 0;
    if (reinterpret_cast<int32_t>(edx66 - 2) > *reinterpret_cast<int32_t*>(&rcx67)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax65) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax65) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax65) + 2) * 8))->f2)) {
            *reinterpret_cast<uint32_t*>(&r12_69) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx67);
            *reinterpret_cast<uint32_t*>(&r12_69) = 1;
        }
    }
    rbx70 = *reinterpret_cast<int32_t*>(&rcx67);
    rdi71 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + rbx70 * 8);
    r13_72 = reinterpret_cast<void*>(rbx70 * 8);
    eax73 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi71->f0));
    if (*reinterpret_cast<signed char*>(&eax73) == 45) {
        eax74 = rdi71->f1;
        if ((*reinterpret_cast<signed char*>(&eax74) == 0x6c || *reinterpret_cast<signed char*>(&eax74) == 0x67) && ((edx75 = rdi71->f2, *reinterpret_cast<signed char*>(&edx75) == 0x65) || *reinterpret_cast<signed char*>(&edx75) == 0x74)) {
            if (rdi71->f3) 
                goto addr_2d44_125; else 
                goto addr_2ea4_126;
        }
        if (*reinterpret_cast<signed char*>(&eax74) != 0x65) 
            goto addr_2d3c_128;
    } else {
        if (*reinterpret_cast<signed char*>(&eax73) != 61 || (eax76 = rdi71->f1, !!*reinterpret_cast<signed char*>(&eax76)) && (*reinterpret_cast<signed char*>(&eax76) != 61 || rdi71->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi71->f0) == 33) || (rdi71->f1 != 61 || rdi71->f2)) {
                fun_2370();
                goto addr_2665_97;
            } else {
                rax77 = reinterpret_cast<int32_t>(pos);
                rbx78 = rax77;
                rax79 = rax77 + 2;
                rsi80 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(rax79 * 8));
                rdi81 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(rax79 * 8) - 16);
                eax82 = fun_24f0(rdi81, rsi80);
                *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(!!eax82);
                pos = *reinterpret_cast<int32_t*>(&rbx78) + 3;
                goto addr_2cc9_133;
            }
        } else {
            rax83 = reinterpret_cast<int32_t>(pos);
            rbx84 = rax83;
            rax85 = rax83 + 2;
            rsi86 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(rax85 * 8));
            rdi87 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(rax85 * 8) - 16);
            eax88 = fun_24f0(rdi87, rsi86);
            *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(eax88 == 0);
            pos = *reinterpret_cast<int32_t*>(&rbx84) + 3;
            goto addr_2cc9_133;
        }
    }
    eax89 = rdi71->f2;
    if (*reinterpret_cast<signed char*>(&eax89) == 0x71) {
        addr_300c_136:
        if (!rdi71->f3) {
            addr_2ea4_126:
            rdi90 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) - 8);
            if (1) {
                rax91 = find_int(rdi90);
                rsp92 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                rbp93 = rax91;
            } else {
                rax94 = fun_2440(rdi90);
                rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                rax96 = umaxtostr(rax94, reinterpret_cast<int64_t>(rsp95) + 0x120);
                rsp92 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                rbp93 = rax96;
            }
        } else {
            addr_3016_139:
            rax97 = quote(rdi71);
            rax98 = fun_2420();
            rax99 = test_syntax_error(rax98, rax97, 5, rcx67, r8_32, r9_35);
            goto addr_3040_140;
        }
    } else {
        addr_2e0c_141:
        if (*reinterpret_cast<signed char*>(&eax89) != 0x66) 
            goto addr_3016_139;
        if (rdi71->f3) 
            goto addr_3016_139; else 
            goto addr_2e1e_143;
    }
    rax100 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_69)) {
        rdi101 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax100) + reinterpret_cast<uint64_t>(r13_72) + 8);
        rax102 = find_int(rdi101, rdi101);
        rsi103 = rax102;
    } else {
        rdi104 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax100) + reinterpret_cast<uint64_t>(r13_72) + 16);
        rax105 = fun_2440(rdi104, rdi104);
        rax106 = umaxtostr(rax105, reinterpret_cast<int64_t>(rsp92) - 8 + 8 + 0x140);
        rsi103 = rax106;
    }
    eax107 = strintcmp(rbp93, rsi103);
    rdx108 = argv;
    *reinterpret_cast<uint32_t*>(&rcx67) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx108) + rbx70 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx67 + 4) = 0;
    dl109 = reinterpret_cast<uint1_t>((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx108) + rbx70 * 8))->f2 == 0x65);
    tmp32_110 = pos + 3;
    pos = tmp32_110;
    if (*reinterpret_cast<signed char*>(&rcx67) == 0x6c) {
        *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(static_cast<uint32_t>(dl109)) > reinterpret_cast<int32_t>(eax107));
        goto addr_2cc9_133;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx67) == 0x67) {
            *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(-static_cast<uint32_t>(dl109)) < reinterpret_cast<int32_t>(eax107));
            goto addr_2cc9_133;
        } else {
            *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!eax107)) == dl109);
            goto addr_2cc9_133;
        }
    }
    addr_3040_140:
    rdi111 = *rax99;
    eax112 = fun_2500(rdi111, r14_29, rdi111, r14_29);
    if (!eax112) {
        addr_2cc9_133:
        rax113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax64) - reinterpret_cast<unsigned char>(g28));
        if (rax113) {
            fun_2450();
        } else {
            return *reinterpret_cast<uint32_t*>(&r12_69);
        }
    } else {
        addr_3053_154:
        *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx70) == 0);
        goto addr_2cc9_133;
    }
    rsi114 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_156:
        rax115 = fun_2420();
        test_syntax_error(rax115, rsi114, 5, rcx67, r8_32, r9_35);
        addr_3102_157:
        rsi114 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_143:
    tmp32_116 = pos + 3;
    pos = tmp32_116;
    *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_69)));
    if (!*reinterpret_cast<unsigned char*>(&r12_69)) {
        rdi117 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) - 8);
        eax118 = fun_2500(rdi117, rsp63);
        if (!eax118 && ((rdi119 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) + 8), eax120 = fun_2500(rdi119, reinterpret_cast<int64_t>(rsp63) - 8 + 8 + 0x90), !eax120) && v121 == v122)) {
            *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(v123 == v124);
            goto addr_2cc9_133;
        }
    }
    addr_2d3c_128:
    if (*reinterpret_cast<signed char*>(&eax74) == 0x6e) {
        eax125 = rdi71->f2;
        if (*reinterpret_cast<signed char*>(&eax125) == 0x65) 
            goto addr_300c_136;
    } else {
        addr_2d44_125:
        if (*reinterpret_cast<signed char*>(&eax74) != 0x6f) {
            if (*reinterpret_cast<signed char*>(&eax74) > 0x6f) 
                goto addr_3016_139;
            if (*reinterpret_cast<signed char*>(&eax74) == 0x65) 
                goto addr_30a8_163; else 
                goto addr_2f4e_164;
        } else {
            if (rdi71->f2 != 0x74) 
                goto addr_3016_139;
            if (rdi71->f3) 
                goto addr_3016_139;
            tmp32_126 = pos + 3;
            pos = tmp32_126;
            *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_69)));
            if (*reinterpret_cast<unsigned char*>(&r12_69)) {
                rsi114 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                goto addr_30f1_156;
            } else {
                rdi127 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) - 8);
                r14_128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) + 0x90);
                eax129 = fun_2500(rdi127, r14_128);
                rdx130 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) + 8);
                if (eax129) {
                    rdi131 = *rdx130;
                    eax132 = fun_2500(rdi131, r14_128);
                    *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(eax132 == 0);
                    goto addr_2cc9_133;
                } else {
                    rdi133 = *rdx130;
                    eax134 = fun_2500(rdi133, r14_128);
                    if (!eax134) {
                        edx135 = 0;
                        *reinterpret_cast<unsigned char*>(&edx135) = reinterpret_cast<uint1_t>(v136 > v137);
                        *reinterpret_cast<uint32_t*>(&rdx138) = edx135 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(v136 < v139));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx138) + 4) = 0;
                        eax140 = 0;
                        *reinterpret_cast<unsigned char*>(&eax140) = reinterpret_cast<uint1_t>(v141 > v142);
                        *reinterpret_cast<uint32_t*>(&rcx67) = reinterpret_cast<uint1_t>(v141 < v143);
                        *reinterpret_cast<int32_t*>(&rcx67 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rax144) = eax140 - *reinterpret_cast<uint32_t*>(&rcx67);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax144) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r12_69) = static_cast<uint32_t>(rax144 + rdx138 * 2) >> 31;
                        goto addr_2cc9_133;
                    }
                }
            }
        }
    }
    addr_2f5a_173:
    if (*reinterpret_cast<signed char*>(&eax125) != 0x74) 
        goto addr_3016_139;
    if (rdi71->f3) 
        goto addr_3016_139;
    tmp32_145 = pos + 3;
    pos = tmp32_145;
    *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_69)));
    if (*reinterpret_cast<unsigned char*>(&r12_69)) 
        goto addr_3102_157;
    rdi146 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) - 8);
    r14_29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) + 0x90);
    eax147 = fun_2500(rdi146, r14_29);
    *reinterpret_cast<int32_t*>(&rbx70) = eax147;
    rax99 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp68) + reinterpret_cast<uint64_t>(r13_72) + 8);
    if (*reinterpret_cast<int32_t*>(&rbx70)) 
        goto addr_3040_140;
    rdi148 = *rax99;
    r12_69 = v149;
    eax150 = fun_2500(rdi148, r14_29);
    if (eax150) 
        goto addr_3053_154;
    edx151 = 0;
    *reinterpret_cast<unsigned char*>(&edx151) = reinterpret_cast<uint1_t>(r12_69 > v152);
    *reinterpret_cast<uint32_t*>(&rdx153) = edx151 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_69 < v154));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx153) + 4) = 0;
    eax155 = 0;
    *reinterpret_cast<unsigned char*>(&eax155) = reinterpret_cast<uint1_t>(v156 > v157);
    *reinterpret_cast<uint32_t*>(&rcx67) = reinterpret_cast<uint1_t>(v156 < v158);
    *reinterpret_cast<int32_t*>(&rcx67 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax159) = eax155 - *reinterpret_cast<uint32_t*>(&rcx67);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax159) + 4) = 0;
    eax160 = static_cast<int32_t>(rax159 + rdx153 * 2);
    *reinterpret_cast<unsigned char*>(&r12_69) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax160 < 0) | reinterpret_cast<uint1_t>(eax160 == 0)));
    goto addr_2cc9_133;
    addr_30a8_163:
    eax89 = rdi71->f2;
    goto addr_2e0c_141;
    addr_2f4e_164:
    if (*reinterpret_cast<signed char*>(&eax74) != 0x6e) 
        goto addr_3016_139;
    eax125 = rdi71->f2;
    goto addr_2f5a_173;
    addr_2665_97:
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    addr_3d1b_11:
    *reinterpret_cast<unsigned char*>(&rax16) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<struct s0**>(&rbp6->f0));
    pos = *reinterpret_cast<uint32_t*>(&rbx8) + 3;
    addr_3ceb_194:
    return *reinterpret_cast<uint32_t*>(&rax16);
    addr_3d4a_12:
    rax161 = quote(rbp6, rbp6);
    rax162 = fun_2420();
    test_syntax_error(rax162, rax161, 5, rcx20, r8_32, r9_35);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d78_115;
    addr_3db6_19:
    goto addr_3d4a_12;
    addr_3af0_58:
    v25 = reinterpret_cast<unsigned char>(v25 | *reinterpret_cast<unsigned char*>(&ebp26));
    addr_3a68_48:
    return static_cast<uint32_t>(v25);
    addr_3eca_30:
    eax163 = *reinterpret_cast<uint32_t*>(&rax17) + 1;
    less164 = reinterpret_cast<int32_t>(eax163) < reinterpret_cast<int32_t>(argc);
    pos = eax163;
    if (!less164) {
        addr_3ee5_36:
        beyond();
    } else {
        three_arguments();
    }
    addr_3eb1_196:
    goto v22;
    addr_3e39_35:
    pos = *reinterpret_cast<uint32_t*>(&rax17) + 1;
    two_arguments();
    tmp32_165 = pos + 1;
    pos = tmp32_165;
    goto addr_3eb1_196;
    addr_3e90_39:
    rax166 = reinterpret_cast<int32_t>(pos);
    pos = static_cast<uint32_t>(rax166 + 1);
    goto addr_3eb1_196;
    addr_38c8_114:
    pos = *reinterpret_cast<int32_t*>(&rdx48) + 2;
    goto v22;
    addr_3165_106:
    *reinterpret_cast<uint32_t*>(&rdx167) = *reinterpret_cast<unsigned char*>(&edx52);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx167) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa1f0 + rdx167 * 4) + 0xa1f0;
    addr_3207_112:
    goto v22;
    addr_3ce3_5:
    eax168 = two_arguments();
    *reinterpret_cast<uint32_t*>(&rax16) = eax168 ^ 1;
    goto addr_3ceb_194;
}

int64_t fun_2430();

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb100 + rax11 * 4) + 0xb100;
    }
}

struct s14 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_2380();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2490();

struct s15 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2360(struct s0* rdi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

struct s0* quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s14* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s15* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x5b3f;
    rax8 = fun_2380();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
        fun_2370();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xf070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x9391]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5bcb;
            fun_2490();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s15*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xf0e0) {
                fun_2360(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5c5a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xf080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s16 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s16* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s16* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0xb09b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0xb094);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0xb09f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0xb090);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gee38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gee38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2343() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_2353() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_2363() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2373() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2383() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t error_at_line = 0x2070;

void fun_2393() {
    __asm__("cli ");
    goto error_at_line;
}

int64_t strncmp = 0x2080;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x20b0;

void fun_23d3() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x20c0;

void fun_23e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20d0;

void fun_23f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2403() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t mbrtowc = 0x2140;

void fun_2463() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2150;

void fun_2473() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_2483() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2170;

void fun_2493() {
    __asm__("cli ");
    goto memset;
}

int64_t geteuid = 0x2180;

void fun_24a3() {
    __asm__("cli ");
    goto geteuid;
}

int64_t lstat = 0x2190;

void fun_24b3() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x21a0;

void fun_24c3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24d3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24e3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_24f3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t stat = 0x21e0;

void fun_2503() {
    __asm__("cli ");
    goto stat;
}

int64_t strtol = 0x21f0;

void fun_2513() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x2200;

void fun_2523() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2210;

void fun_2533() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2220;

void fun_2543() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2230;

void fun_2553() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2240;

void fun_2563() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t getegid = 0x2250;

void fun_2573() {
    __asm__("cli ");
    goto getegid;
}

int64_t __freading = 0x2260;

void fun_2583() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2270;

void fun_2593() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2280;

void fun_25a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2290;

void fun_25b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22a0;

void fun_25c3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22b0;

void fun_25d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t euidaccess = 0x22c0;

void fun_25e3() {
    __asm__("cli ");
    goto euidaccess;
}

int64_t __cxa_atexit = 0x22d0;

void fun_25f3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22e0;

void fun_2603() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22f0;

void fun_2613() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2300;

void fun_2623() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2310;

void fun_2633() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2320;

void fun_2643() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2330;

void fun_2653() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_25a0(int64_t rdi, ...);

void fun_2410(int64_t rdi, struct s0* rsi);

void fun_23f0(int64_t rdi, struct s0* rsi);

int32_t exit_failure = 1;

void atexit(int64_t rdi, struct s0* rsi);

int64_t fun_26c3(uint32_t edi, struct s0* rsi) {
    int64_t rbx3;
    struct s0* rdi4;
    int64_t rax5;
    struct s0* rdi6;
    uint32_t eax7;
    int64_t rdx8;
    int1_t zf9;
    struct s0* rax10;
    struct s0* rdi11;
    struct s0* rax12;
    struct s0* rax13;
    struct s0* rcx14;
    struct s0* r8_15;
    int64_t r9_16;
    uint32_t eax17;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rbx3) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx3) + 4) = 0;
    rdi4 = *reinterpret_cast<struct s0**>(&rsi->f0);
    set_program_name(rdi4);
    fun_25a0(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    fun_23f0("coreutils", "/usr/local/share/locale");
    exit_failure = 2;
    atexit(0x4450, "/usr/local/share/locale");
    argv = rsi;
    argc = *reinterpret_cast<uint32_t*>(&rbx3);
    pos = 1;
    if (*reinterpret_cast<int32_t*>(&rbx3) <= reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax5) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    } else {
        *reinterpret_cast<int32_t*>(&rdi6) = static_cast<int32_t>(rbx3 - 1);
        *reinterpret_cast<int32_t*>(&rdi6 + 4) = 0;
        eax7 = posixtest(rdi6, "/usr/local/share/locale");
        rdx8 = reinterpret_cast<int32_t>(pos);
        zf9 = *reinterpret_cast<uint32_t*>(&rdx8) == argc;
        if (!zf9) {
            rax10 = argv;
            rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax10) + rdx8 * 8);
            rax12 = quote(rdi11, rdi11);
            rax13 = fun_2420();
            test_syntax_error(rax13, rax12, 5, rcx14, r8_15, r9_16);
        } else {
            eax17 = eax7 ^ 1;
            *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<unsigned char*>(&eax17);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
    }
    return rax5;
}

int64_t __libc_start_main = 0;

void fun_27a3() {
    __asm__("cli ");
    __libc_start_main(0x26c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xf008;

void fun_2340(int64_t rdi);

int64_t fun_2843() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2340(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2883() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

int64_t stdout = 0;

struct s17 {
    signed char f0;
    signed char f1;
    signed char f2;
};

void fun_24d0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s17* rcx);

void fun_25b0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s17* rcx);

int32_t fun_23a0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s17* rcx);

struct s17* program_name = reinterpret_cast<struct s17*>(0);

int64_t stderr = 0;

void fun_2620(int64_t rdi, int64_t rsi, struct s0* rdx, struct s17* rcx);

void fun_3ef3(int32_t edi) {
    int32_t ebp2;
    struct s0* rax3;
    struct s0* v4;
    int64_t r12_5;
    struct s0* rax6;
    struct s17* rcx7;
    int64_t r12_8;
    struct s0* rax9;
    int64_t r12_10;
    struct s0* rax11;
    int64_t r12_12;
    struct s0* rax13;
    int64_t r12_14;
    struct s0* rax15;
    int64_t r12_16;
    struct s0* rax17;
    int64_t r12_18;
    struct s0* rax19;
    int64_t r12_20;
    struct s0* rax21;
    int64_t r12_22;
    struct s0* rax23;
    int64_t r12_24;
    struct s0* rax25;
    int64_t r12_26;
    struct s0* rax27;
    int64_t r12_28;
    struct s0* rax29;
    int64_t r12_30;
    struct s0* rax31;
    int64_t r12_32;
    struct s0* rax33;
    int64_t r12_34;
    struct s0* rax35;
    int64_t r12_36;
    struct s0* rax37;
    struct s0* rax38;
    struct s0* rax39;
    int32_t eax40;
    struct s0* r13_41;
    struct s0* rax42;
    struct s0* rax43;
    int32_t eax44;
    struct s0* rax45;
    struct s17* r12_46;
    struct s0* rax47;
    struct s0* rax48;
    int32_t eax49;
    struct s0* rax50;
    int64_t r15_51;
    struct s0* rax52;
    struct s0* rax53;
    struct s0* rdi54;
    struct s17* r12_55;
    struct s0* rax56;
    int64_t rdi57;

    __asm__("cli ");
    ebp2 = edi;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            r12_5 = stdout;
            rax6 = fun_2420();
            fun_24d0(rax6, r12_5, 5, rcx7);
            r12_8 = stdout;
            rax9 = fun_2420();
            fun_24d0(rax9, r12_8, 5, rcx7);
            r12_10 = stdout;
            rax11 = fun_2420();
            fun_24d0(rax11, r12_10, 5, rcx7);
            r12_12 = stdout;
            rax13 = fun_2420();
            fun_24d0(rax13, r12_12, 5, rcx7);
            r12_14 = stdout;
            rax15 = fun_2420();
            fun_24d0(rax15, r12_14, 5, rcx7);
            r12_16 = stdout;
            rax17 = fun_2420();
            fun_24d0(rax17, r12_16, 5, rcx7);
            r12_18 = stdout;
            rax19 = fun_2420();
            fun_24d0(rax19, r12_18, 5, rcx7);
            r12_20 = stdout;
            rax21 = fun_2420();
            fun_24d0(rax21, r12_20, 5, rcx7);
            r12_22 = stdout;
            rax23 = fun_2420();
            fun_24d0(rax23, r12_22, 5, rcx7);
            r12_24 = stdout;
            rax25 = fun_2420();
            fun_24d0(rax25, r12_24, 5, rcx7);
            r12_26 = stdout;
            rax27 = fun_2420();
            fun_24d0(rax27, r12_26, 5, rcx7);
            r12_28 = stdout;
            rax29 = fun_2420();
            fun_24d0(rax29, r12_28, 5, rcx7);
            r12_30 = stdout;
            rax31 = fun_2420();
            fun_24d0(rax31, r12_30, 5, rcx7);
            r12_32 = stdout;
            rax33 = fun_2420();
            fun_24d0(rax33, r12_32, 5, rcx7);
            r12_34 = stdout;
            rax35 = fun_2420();
            fun_24d0(rax35, r12_34, 5, rcx7);
            r12_36 = stdout;
            rax37 = fun_2420();
            fun_24d0(rax37, r12_36, 5, rcx7);
            rax38 = fun_2420();
            rax39 = fun_2420();
            fun_25b0(1, rax39, rax38, rcx7);
            do {
                if (1) 
                    break;
                eax40 = fun_24f0("test", 0, "test", 0);
            } while (eax40);
            r13_41 = v4;
            if (!r13_41) {
                rax42 = fun_2420();
                fun_25b0(1, rax42, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax43 = fun_25a0(5);
                if (!rax43 || (eax44 = fun_23a0(rax43, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax44)) {
                    rax45 = fun_2420();
                    r13_41 = reinterpret_cast<struct s0*>("test");
                    fun_25b0(1, rax45, "https://www.gnu.org/software/coreutils/", "test");
                    r12_46 = reinterpret_cast<struct s17*>(" invocation");
                } else {
                    r13_41 = reinterpret_cast<struct s0*>("test");
                    goto addr_4400_9;
                }
            } else {
                rax47 = fun_2420();
                fun_25b0(1, rax47, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax48 = fun_25a0(5);
                if (!rax48 || (eax49 = fun_23a0(rax48, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax49)) {
                    addr_4306_11:
                    rax50 = fun_2420();
                    fun_25b0(1, rax50, "https://www.gnu.org/software/coreutils/", "test");
                    r12_46 = reinterpret_cast<struct s17*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_41 == "test")) {
                        r12_46 = reinterpret_cast<struct s17*>(0xa197);
                    }
                } else {
                    addr_4400_9:
                    r15_51 = stdout;
                    rax52 = fun_2420();
                    fun_24d0(rax52, r15_51, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_4306_11;
                }
            }
            rax53 = fun_2420();
            rcx7 = r12_46;
            fun_25b0(1, rax53, r13_41, rcx7);
            addr_3f4e_14:
            *reinterpret_cast<int32_t*>(&rdi54) = ebp2;
            *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
            fun_2600(rdi54, rdi54);
        }
    } else {
        r12_55 = program_name;
        rax56 = fun_2420();
        rdi57 = stderr;
        rcx7 = r12_55;
        fun_2620(rdi57, 1, rax56, rcx7);
        goto addr_3f4e_14;
    }
}

int64_t file_name = 0;

void fun_4433(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4443(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

void fun_25c0();

struct s0* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_4453() {
    int64_t rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2380(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_44e3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25c0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_44e3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25c0();
    }
}

struct s18 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_4503(uint64_t rdi, struct s18* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_2610(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s19 {
    signed char[1] pad1;
    signed char f1;
    signed char[2] pad4;
    signed char f4;
};

struct s19* fun_2470();

struct s17* __progname = reinterpret_cast<struct s17*>(0);

struct s17* __progname_full = reinterpret_cast<struct s17*>(0);

void fun_4563(struct s17* rdi) {
    int64_t rcx2;
    struct s17* rbx3;
    struct s19* rax4;
    struct s17* r12_5;
    struct s17* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2610("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2370();
    } else {
        rbx3 = rdi;
        rax4 = fun_2470();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s17*>(&rax4->f1), reinterpret_cast<int64_t>(r12_5) - reinterpret_cast<int64_t>(rbx3) > 6) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (rax4->f1 != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s17*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5d03(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2380();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xf1e0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5d43(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5d63(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf1e0);
    }
    *rdi = esi;
    return 0xf1e0;
}

int64_t fun_5d83(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xf1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s20 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5dc3(struct s20* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s20*>(0xf1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s21 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s21* fun_5de3(struct s21* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s21*>(0xf1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x2674;
    if (!rdx) 
        goto 0x2674;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xf1e0;
}

struct s22 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5e23(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s22* r8) {
    struct s22* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s22*>(0xf1e0);
    }
    rax7 = fun_2380();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5e56);
    *rax7 = r15d8;
    return rax13;
}

struct s23 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5ea3(int64_t rdi, int64_t rsi, struct s0** rdx, struct s23* rcx) {
    struct s23* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s23*>(0xf1e0);
    }
    rax6 = fun_2380();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5ed1);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5f2c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5f93() {
    __asm__("cli ");
}

struct s0* gf078 = reinterpret_cast<struct s0*>(0xe0);

int64_t slotvec0 = 0x100;

void fun_5fa3() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2360(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xf0e0) {
        fun_2360(rdi7);
        gf078 = reinterpret_cast<struct s0*>(0xf0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xf070) {
        fun_2360(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xf070);
    }
    nslots = 1;
    return;
}

void fun_6043() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6063() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6073(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6093(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_60b3(int32_t edi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s14* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x267a;
    rcx5 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_6143(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s14* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x267f;
    rcx6 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_61d3(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s14* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x2684;
    rcx4 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

struct s0* fun_6263(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s14* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2689;
    rcx5 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_62f3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s14* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8ee0]");
    __asm__("movdqa xmm1, [rip+0x8ee8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8ed1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

struct s0* fun_6393(int64_t rdi, uint32_t esi) {
    struct s14* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8e40]");
    __asm__("movdqa xmm1, [rip+0x8e48]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8e31]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_6433(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8da0]");
    __asm__("movdqa xmm1, [rip+0x8da8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8d89]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s0* fun_64c3(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8d10]");
    __asm__("movdqa xmm1, [rip+0x8d18]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8d06]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

struct s0* fun_6553(int32_t edi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s14* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x268e;
    rcx5 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_65f3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s14* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8bda]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8bd2]");
    __asm__("movdqa xmm2, [rip+0x8bda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2693;
    if (!rdx) 
        goto 0x2693;
    rcx6 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_6693(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s14* rcx7;
    struct s0* rax8;
    void* rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8b3a]");
    __asm__("movdqa xmm1, [rip+0x8b42]");
    __asm__("movdqa xmm2, [rip+0x8b4a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2698;
    if (!rdx) 
        goto 0x2698;
    rcx7 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2450();
    } else {
        return rax8;
    }
}

struct s0* fun_6743(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s14* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8a8a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8a82]");
    __asm__("movdqa xmm2, [rip+0x8a8a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x269d;
    if (!rsi) 
        goto 0x269d;
    rcx5 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_67e3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s14* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x89ea]");
    __asm__("movdqa xmm1, [rip+0x89f2]");
    __asm__("movdqa xmm2, [rip+0x89fa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26a2;
    if (!rsi) 
        goto 0x26a2;
    rcx6 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_6883() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6893(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s24 {
    unsigned char f0;
    unsigned char f1;
};

struct s25 {
    unsigned char f0;
    unsigned char f1;
};

int64_t fun_68f3(struct s24* rdi, struct s25* rsi) {
    uint32_t ecx3;
    struct s25* rax4;
    struct s24* rdx5;
    uint32_t esi6;
    uint32_t ecx7;
    uint32_t esi8;
    uint32_t r8d9;
    int64_t rsi10;
    int64_t rcx11;
    uint32_t ecx12;
    uint64_t rsi13;
    int64_t r8_14;
    uint32_t edi15;
    int64_t r8_16;
    int64_t rcx17;
    uint32_t edi18;
    uint32_t esi19;
    int64_t rax20;
    int32_t eax21;
    uint32_t edx22;
    uint64_t rcx23;
    uint32_t edx24;
    uint32_t ecx25;
    int32_t r8d26;
    int32_t r8d27;
    uint32_t r8d28;
    int32_t r8d29;
    int64_t rsi30;
    int64_t rcx31;
    uint32_t ecx32;
    int64_t r8_33;
    uint32_t esi34;
    uint32_t ecx35;
    uint32_t edi36;
    int64_t r10_37;
    uint32_t r9d38;
    struct s24* rax39;
    struct s25* rdx40;
    uint64_t rsi41;
    uint32_t r8d42;
    uint32_t edi43;
    uint32_t ecx44;
    uint64_t rcx45;
    uint32_t edx46;
    int64_t rax47;
    int64_t rax48;
    uint32_t ecx49;
    int32_t r8d50;
    int64_t rax51;

    __asm__("cli ");
    ecx3 = rdi->f0;
    rax4 = rsi;
    rdx5 = rdi;
    esi6 = rsi->f0;
    if (*reinterpret_cast<unsigned char*>(&ecx3) == 45) {
        addr_6a80_2:
        ecx7 = rdx5->f1;
        rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
        if (*reinterpret_cast<unsigned char*>(&ecx7) == 0x80) 
            goto addr_6a80_2;
        if (*reinterpret_cast<unsigned char*>(&ecx7) == 48) 
            goto addr_6a80_2;
        if (*reinterpret_cast<unsigned char*>(&esi6) != 45) 
            goto addr_6a98_5;
    } else {
        if (*reinterpret_cast<unsigned char*>(&esi6) != 45) {
            while (*reinterpret_cast<unsigned char*>(&ecx3) == 48 || *reinterpret_cast<unsigned char*>(&ecx3) == 0x80) {
                ecx3 = rdx5->f1;
                rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
            }
            if (*reinterpret_cast<unsigned char*>(&esi6) == 48) 
                goto addr_6990_10; else 
                goto addr_6988_11;
        } else {
            addr_6910_13:
            esi8 = rax4->f1;
            rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
            if (*reinterpret_cast<unsigned char*>(&esi8) == 48) 
                goto addr_6910_13;
            if (*reinterpret_cast<unsigned char*>(&esi8) == 0x80) 
                goto addr_6910_13;
            r8d9 = 1;
            if (*reinterpret_cast<unsigned char*>(&esi8) - 48 <= 9) 
                goto addr_6ad1_16;
            if (*reinterpret_cast<unsigned char*>(&ecx3) == 0x80) 
                goto addr_6948_18; else 
                goto addr_693f_19;
        }
    }
    addr_6ad8_20:
    *reinterpret_cast<uint32_t*>(&rsi10) = rax4->f1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi10) + 4) = 0;
    rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
    if (*reinterpret_cast<unsigned char*>(&rsi10) == 0x80) 
        goto addr_6ad8_20;
    if (*reinterpret_cast<unsigned char*>(&rsi10) == 48) 
        goto addr_6ad8_20;
    addr_6af0_23:
    while (*reinterpret_cast<unsigned char*>(&ecx7) == *reinterpret_cast<unsigned char*>(&rsi10)) {
        *reinterpret_cast<uint32_t*>(&rcx11) = *reinterpret_cast<unsigned char*>(&rsi10);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (static_cast<uint32_t>(rcx11 - 48) > 9) 
            goto addr_6bc1_25;
        do {
            ecx7 = rdx5->f1;
            rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
        } while (*reinterpret_cast<unsigned char*>(&ecx7) == 0x80);
        do {
            *reinterpret_cast<uint32_t*>(&rsi10) = rax4->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi10) + 4) = 0;
            rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
            if (*reinterpret_cast<unsigned char*>(&rsi10) != 0x80) 
                goto addr_6af0_23;
            *reinterpret_cast<uint32_t*>(&rsi10) = rax4->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi10) + 4) = 0;
            rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
        } while (*reinterpret_cast<unsigned char*>(&rsi10) == 0x80);
    }
    *reinterpret_cast<uint32_t*>(&rcx11) = *reinterpret_cast<unsigned char*>(&ecx7);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
    addr_6bc1_25:
    if (*reinterpret_cast<uint32_t*>(&rcx11) == 0x80) {
        if (static_cast<uint32_t>(rsi10 - 48) > 9 || *reinterpret_cast<uint32_t*>(&rsi10) == 0x80) {
            addr_6d24_34:
            ecx12 = rdx5->f0;
            if (rax4->f0 != 0x80) {
                r8d9 = 0;
                if (*reinterpret_cast<signed char*>(&ecx12) != 0x80) 
                    goto addr_6ad1_16; else 
                    goto addr_6d38_36;
            }
        } else {
            r8d9 = static_cast<uint32_t>(rsi10 - 0x80);
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
            goto addr_6c0e_38;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&r8_14) = *reinterpret_cast<unsigned char*>(&rsi10);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_14) + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(&rsi10) == 0x80) {
            r8d9 = *reinterpret_cast<uint32_t*>(&r8_14) - *reinterpret_cast<uint32_t*>(&rcx11);
            edi15 = 80;
            if (static_cast<uint32_t>(rcx11 - 48) <= 9) 
                goto addr_6bee_41; else 
                goto addr_6d24_34;
        }
        edi15 = static_cast<uint32_t>(r8_14 - 48);
        r8d9 = *reinterpret_cast<uint32_t*>(&r8_14) - *reinterpret_cast<uint32_t*>(&rcx11);
        if (*reinterpret_cast<uint32_t*>(&rcx11) - 48 > 9) {
            if (edi15 > 9) {
                addr_6c6f_44:
                r8d9 = 0;
                goto addr_6ad1_16;
            } else {
                *reinterpret_cast<int32_t*>(&rsi13) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
                goto addr_6c0e_38;
            }
        } else {
            addr_6bee_41:
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
            goto addr_6bf0_46;
        }
    }
    if (*reinterpret_cast<signed char*>(&ecx12) != 0x80) 
        goto addr_6d54_48;
    do {
        *reinterpret_cast<int32_t*>(&r8_16) = reinterpret_cast<signed char>(rax4->f1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdx5->f1)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
        rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
        rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
        edi18 = static_cast<uint32_t>(r8_16 - 48);
        if (*reinterpret_cast<signed char*>(&r8_16) != *reinterpret_cast<signed char*>(&rcx17)) 
            break;
    } while (edi18 <= 9);
    goto addr_6c6f_44;
    esi19 = static_cast<uint32_t>(rcx17 - 48);
    if (edi18 > 9) {
        r8d9 = 0;
        if (esi19 > 9) {
            addr_6ad1_16:
            *reinterpret_cast<uint32_t*>(&rax20) = r8d9;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
            return rax20;
        } else {
            while (eax21 = reinterpret_cast<signed char>(rdx5->f0), *reinterpret_cast<signed char*>(&eax21) == 48) {
                addr_6d38_36:
                rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
            }
        }
    } else {
        r8d9 = *reinterpret_cast<int32_t*>(&r8_16) - *reinterpret_cast<uint32_t*>(&rcx17);
        if (esi19 > 9) {
            while (edx22 = rax4->f0, *reinterpret_cast<signed char*>(&edx22) == 48) {
                addr_6d54_48:
                rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
            }
            r8d9 = 0;
            *reinterpret_cast<unsigned char*>(&r8d9) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edx22) - 48 <= 9);
            goto addr_6ad1_16;
        } else {
            goto addr_6ad1_16;
        }
    }
    addr_6b94_60:
    r8d9 = -static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(eax21 - 48 < 10))));
    goto addr_6ad1_16;
    addr_6c0e_38:
    *reinterpret_cast<int32_t*>(&rcx23) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx23) + 4) = 0;
    addr_6c10_61:
    edx24 = rax4->f1;
    rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
    if (*reinterpret_cast<signed char*>(&edx24) == 0x80) 
        goto addr_6c10_61;
    ++rcx23;
    if (edx24 - 48 <= 9) 
        goto addr_6c10_61;
    if (rsi13 == rcx23) {
        if (!rsi13) {
            r8d9 = 0;
        }
        goto addr_6ad1_16;
    } else {
        r8d9 = (r8d9 - (r8d9 + reinterpret_cast<uint1_t>(r8d9 < r8d9 + reinterpret_cast<uint1_t>(rsi13 < rcx23))) & 2) - 1;
        goto addr_6ad1_16;
    }
    addr_6bf0_46:
    ecx25 = rdx5->f1;
    rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
    if (*reinterpret_cast<signed char*>(&ecx25) == 0x80) 
        goto addr_6bf0_46;
    ++rsi13;
    if (ecx25 - 48 <= 9) 
        goto addr_6bf0_46;
    if (edi15 > 9) {
        r8d9 = r8d9 - (r8d9 + reinterpret_cast<uint1_t>(r8d9 < r8d9 + reinterpret_cast<uint1_t>(!!rsi13)));
        goto addr_6ad1_16;
    }
    addr_6a98_5:
    if (*reinterpret_cast<unsigned char*>(&ecx7) - 48 > 9) {
        while (*reinterpret_cast<unsigned char*>(&esi6) == 48 || *reinterpret_cast<unsigned char*>(&esi6) == 0x80) {
            esi6 = rax4->f1;
            rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
        }
        r8d9 = r8d26 - (r8d27 + reinterpret_cast<uint1_t>(r8d28 < r8d29 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&esi6) - 48 < 10)));
        goto addr_6ad1_16;
    } else {
        r8d9 = 0xffffffff;
        goto addr_6ad1_16;
    }
    addr_6990_10:
    esi6 = rax4->f1;
    rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
    if (*reinterpret_cast<unsigned char*>(&esi6) == 0x80) 
        goto addr_6990_10;
    if (*reinterpret_cast<unsigned char*>(&esi6) == 48) 
        goto addr_6990_10; else 
        goto addr_69a4_77;
    addr_6988_11:
    if (*reinterpret_cast<unsigned char*>(&esi6) != 0x80) {
        addr_69a4_77:
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<unsigned char*>(&esi6);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi30) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx31) = *reinterpret_cast<unsigned char*>(&ecx3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx31) + 4) = 0;
        if (*reinterpret_cast<unsigned char*>(&esi6) != *reinterpret_cast<unsigned char*>(&ecx3)) {
            addr_69f0_78:
            if (*reinterpret_cast<uint32_t*>(&rcx31) == 0x80) {
                if (static_cast<uint32_t>(rsi30 - 48) > 9 || *reinterpret_cast<uint32_t*>(&rsi30) == 0x80) {
                    addr_6b60_80:
                    ecx32 = rax4->f0;
                    if (rdx5->f0 == 0x80) {
                        if (*reinterpret_cast<signed char*>(&ecx32) == 0x80) {
                            do {
                                *reinterpret_cast<uint32_t*>(&r8_33) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdx5->f1)));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_33) + 4) = 0;
                                esi34 = rax4->f1;
                                rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
                                rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
                                ecx35 = *reinterpret_cast<uint32_t*>(&r8_33);
                                edi36 = static_cast<uint32_t>(r8_33 - 48);
                                if (*reinterpret_cast<signed char*>(&r8_33) != *reinterpret_cast<signed char*>(&esi34)) 
                                    break;
                            } while (edi36 <= 9);
                            goto addr_6c6f_44;
                            *reinterpret_cast<uint32_t*>(&r10_37) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&esi34)));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_37) + 4) = 0;
                            r9d38 = static_cast<uint32_t>(r10_37 - 48);
                            if (edi36 <= 9) 
                                goto addr_6cfe_85;
                        } else {
                            ecx35 = rdx5->f1;
                            rax39 = reinterpret_cast<struct s24*>(&rdx5->f1);
                            goto addr_6cb1_87;
                        }
                    } else {
                        r8d9 = 0;
                        if (*reinterpret_cast<signed char*>(&ecx32) != 0x80) 
                            goto addr_6ad1_16;
                        esi34 = rax4->f1;
                        rdx40 = reinterpret_cast<struct s25*>(&rax4->f1);
                        goto addr_6b8a_90;
                    }
                } else {
                    *reinterpret_cast<int32_t*>(&rsi41) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
                    r8d42 = *reinterpret_cast<uint32_t*>(&rcx31) - *reinterpret_cast<uint32_t*>(&rsi30);
                    goto addr_6a42_92;
                }
            } else {
                if (*reinterpret_cast<uint32_t*>(&rsi30) == 0x80) {
                    r8d42 = static_cast<uint32_t>(rcx31 - 0x80);
                    edi43 = 80;
                    if (static_cast<uint32_t>(rcx31 - 48) <= 9) 
                        goto addr_6a1d_95; else 
                        goto addr_6b60_80;
                }
                edi43 = static_cast<uint32_t>(rsi30 - 48);
                r8d42 = *reinterpret_cast<uint32_t*>(&rcx31) - *reinterpret_cast<uint32_t*>(&rsi30);
                if (*reinterpret_cast<uint32_t*>(&rcx31) - 48 > 9) {
                    if (edi43 > 9) 
                        goto addr_6c6f_44;
                    *reinterpret_cast<int32_t*>(&rsi41) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
                    goto addr_6a42_92;
                } else {
                    addr_6a1d_95:
                    *reinterpret_cast<int32_t*>(&rsi41) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
                    goto addr_6a20_99;
                }
            }
        } else {
            if (static_cast<uint32_t>(rsi30 - 48) > 9) {
                *reinterpret_cast<uint32_t*>(&rcx31) = *reinterpret_cast<uint32_t*>(&rsi30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx31) + 4) = 0;
                goto addr_69f0_78;
            } else {
                do {
                    addr_69c0_103:
                    ecx44 = rdx5->f1;
                    rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
                    if (*reinterpret_cast<unsigned char*>(&ecx44) == 0x80) 
                        goto addr_69c0_103;
                    do {
                        *reinterpret_cast<uint32_t*>(&rsi30) = rax4->f1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi30) + 4) = 0;
                        rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
                    } while (*reinterpret_cast<unsigned char*>(&rsi30) == 0x80);
                    *reinterpret_cast<uint32_t*>(&rcx31) = *reinterpret_cast<unsigned char*>(&ecx44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx31) + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&ecx44) != *reinterpret_cast<unsigned char*>(&rsi30)) 
                        goto addr_69f0_78;
                } while (static_cast<uint32_t>(rcx31 - 48) <= 9);
                goto addr_69ee_108;
            }
        }
    } else {
        goto addr_6990_10;
    }
    r8d9 = 0;
    rdx40 = rax4;
    if (r9d38 > 9) 
        goto addr_6ad1_16;
    addr_6b8a_90:
    while (*reinterpret_cast<signed char*>(&esi34) == 48) {
        esi34 = rdx40->f1;
        rdx40 = reinterpret_cast<struct s25*>(&rdx40->f1);
    }
    eax21 = *reinterpret_cast<signed char*>(&esi34);
    goto addr_6b94_60;
    addr_6cfe_85:
    rax39 = rdx5;
    r8d9 = *reinterpret_cast<uint32_t*>(&r8_33) - *reinterpret_cast<uint32_t*>(&r10_37);
    if (r9d38 <= 9) {
        goto addr_6ad1_16;
    }
    addr_6cb1_87:
    while (*reinterpret_cast<signed char*>(&ecx35) == 48) {
        ecx35 = rax39->f1;
        rax39 = reinterpret_cast<struct s24*>(&rax39->f1);
    }
    r8d9 = 0;
    *reinterpret_cast<unsigned char*>(&r8d9) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&ecx35) - 48 <= 9);
    goto addr_6ad1_16;
    addr_6a42_92:
    *reinterpret_cast<int32_t*>(&rcx45) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx45) + 4) = 0;
    addr_6a48_117:
    edx46 = rax4->f1;
    rax4 = reinterpret_cast<struct s25*>(&rax4->f1);
    if (*reinterpret_cast<signed char*>(&edx46) == 0x80) 
        goto addr_6a48_117;
    ++rcx45;
    if (edx46 - 48 <= 9) 
        goto addr_6a48_117;
    if (rcx45 == rsi41) {
        if (!rcx45) {
            r8d42 = 0;
        }
        *reinterpret_cast<uint32_t*>(&rax47) = r8d42;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax47) + 4) = 0;
        return rax47;
    } else {
        *reinterpret_cast<uint32_t*>(&rax48) = r8d42 - (r8d42 + reinterpret_cast<uint1_t>(r8d42 < r8d42 + reinterpret_cast<uint1_t>(rsi41 < rcx45))) | 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        return rax48;
    }
    addr_6a20_99:
    ecx49 = rdx5->f1;
    rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
    if (*reinterpret_cast<signed char*>(&ecx49) == 0x80) 
        goto addr_6a20_99;
    ++rsi41;
    if (ecx49 - 48 <= 9) 
        goto addr_6a20_99;
    if (edi43 > 9) {
        r8d9 = 0;
        *reinterpret_cast<unsigned char*>(&r8d9) = reinterpret_cast<uint1_t>(!!rsi41);
        goto addr_6ad1_16;
    }
    addr_69ee_108:
    *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&rcx31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi30) + 4) = 0;
    goto addr_69f0_78;
    addr_6948_18:
    ecx3 = rdx5->f1;
    rdx5 = reinterpret_cast<struct s24*>(&rdx5->f1);
    if (*reinterpret_cast<unsigned char*>(&ecx3) == 48) 
        goto addr_6948_18;
    if (*reinterpret_cast<unsigned char*>(&ecx3) == 0x80) 
        goto addr_6948_18; else 
        goto addr_695a_128;
    addr_693f_19:
    if (*reinterpret_cast<unsigned char*>(&ecx3) != 48) {
        addr_695a_128:
        r8d50 = 0;
        *reinterpret_cast<unsigned char*>(&r8d50) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&ecx3) - 48 <= 9);
        *reinterpret_cast<int32_t*>(&rax51) = r8d50;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        return rax51;
    } else {
        goto addr_6948_18;
    }
}

int64_t xvasprintf(int64_t rdi, int64_t rsi);

void fun_6e13(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rax5;

    __asm__("cli ");
    rax5 = xvasprintf(rdx, rcx);
    if (!rax5) {
        fun_2420();
        fun_2380();
        fun_25c0();
        fun_2370();
    } else {
        fun_25c0();
        goto fun_2360;
    }
}

void fun_2390(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_6e93(int32_t edi, int32_t esi, int64_t rdx, int32_t ecx, int64_t r8, int64_t r9) {
    int64_t rax7;
    int64_t rcx8;
    int64_t rsi9;
    int64_t rdi10;

    __asm__("cli ");
    rax7 = xvasprintf(r8, r9);
    if (!rax7) {
        fun_2420();
        fun_2380();
        fun_25c0();
        fun_2370();
    } else {
        if (!rdx) {
            fun_25c0();
        } else {
            *reinterpret_cast<int32_t*>(&rcx8) = ecx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi9) = esi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi10) = edi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            fun_2390(rdi10, rsi9, rdx, rcx8, "%s", rax7);
        }
        goto fun_2360;
    }
}

int64_t fun_23e0();

void xalloc_die();

void fun_6f43(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

struct s0* fun_2540(struct s0* rdi, struct s0* rsi, ...);

void fun_6f83(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fa3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fc3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* fun_2590(struct s0* rdi, struct s0* rsi, ...);

void fun_6fe3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7013(struct s0* rdi, uint64_t rsi) {
    uint64_t rax3;
    struct s0* rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2590(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7043(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7083() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70f3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7143(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23e0();
            if (rax5) 
                break;
            addr_718d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_718d_5;
        rax8 = fun_23e0();
        if (rax8) 
            goto addr_7176_9;
        if (rbx4) 
            goto addr_718d_5;
        addr_7176_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_71d3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23e0();
            if (rax8) 
                break;
            addr_721a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_721a_5;
        rax11 = fun_23e0();
        if (rax11) 
            goto addr_7202_9;
        if (!rbx6) 
            goto addr_7202_9;
        if (r12_4) 
            goto addr_721a_5;
        addr_7202_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7263(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx, uint64_t r8) {
    struct s0* r13_6;
    struct s0* rdi7;
    struct s0** r12_8;
    struct s0* rsi9;
    struct s0* rcx10;
    struct s0* rbx11;
    struct s0* rax12;
    struct s0* rbp13;
    void* rbp14;
    struct s0* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<struct s0*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_730d_9:
            rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_7320_10:
                *r12_8 = reinterpret_cast<struct s0*>(0);
            }
            addr_72c0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_72e6_12;
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7334_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_72dd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_7334_14;
            addr_72dd_16:
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7334_14;
            addr_72e6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2590(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7334_14;
            if (!rbp13) 
                break;
            addr_7334_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_730d_9;
        } else {
            if (!r13_6) 
                goto addr_7320_10;
            goto addr_72c0_11;
        }
    }
}

int64_t fun_24e0();

void fun_7363() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7393() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_73c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_73e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* fun_2520(struct s0* rdi, struct s0* rsi, struct s0* rdx, ...);

void fun_7403(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_7443(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

void fun_7483(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2540(&rsi->f1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2520;
    }
}

void fun_74c3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;

    __asm__("cli ");
    rax3 = fun_2440(rdi);
    rax4 = fun_2540(&rax3->f1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2520;
    }
}

int64_t g10;

struct s0* xmalloc(uint64_t rdi);

uint32_t __cxa_finalize;

struct s0** g8;

void fun_7503() {
    int64_t v1;
    int64_t rax2;
    int64_t rdi3;
    struct s0* rax4;
    struct s0* v5;
    int64_t r15_6;
    struct s0* rax7;
    struct s0* rbp8;
    int64_t r13_9;
    int64_t rbp10;
    uint64_t rbx11;
    uint32_t v12;
    int64_t rdx13;
    struct s0** rdx14;
    struct s0** v15;
    struct s0* rdi16;
    struct s0* rax17;
    uint64_t tmp64_18;
    int1_t cf19;
    void* rax20;
    int32_t* rax21;
    struct s0* rax22;
    uint32_t eax23;
    int64_t rdx24;
    struct s0** tmp64_25;
    struct s0** rdx26;
    struct s0* r15_27;
    struct s0* rax28;

    __asm__("cli ");
    v1 = rax2;
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi3) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
    fun_25c0();
    fun_2370();
    rax4 = g28;
    v5 = rax4;
    __asm__("movdqu xmm0, [rsi]");
    __asm__("movups [rsp], xmm0");
    r15_6 = g10;
    if (!rdi3) {
        rax7 = xmalloc(1);
        rbp8 = rax7;
    } else {
        r13_9 = rdi3;
        rbp10 = rdi3;
        *reinterpret_cast<int32_t*>(&rbx11) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
        do {
            if (v12 <= 47) {
                *reinterpret_cast<uint32_t*>(&rdx13) = v12;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
                v12 = v12 + 8;
                rdx14 = reinterpret_cast<struct s0**>(rdx13 + r15_6);
            } else {
                rdx14 = v15;
                v15 = rdx14 + 8;
            }
            rdi16 = *rdx14;
            rax17 = fun_2440(rdi16, rdi16);
            tmp64_18 = rbx11 + reinterpret_cast<unsigned char>(rax17);
            cf19 = tmp64_18 < rbx11;
            rbx11 = tmp64_18;
            if (cf19) {
                rbx11 = 0xffffffffffffffff;
            }
            --rbp10;
        } while (rbp10);
        if (rbx11 > 0x7fffffff) 
            goto addr_7680_12; else 
            goto addr_75dd_13;
    }
    addr_7650_14:
    *reinterpret_cast<struct s0**>(&rbp8->f0) = reinterpret_cast<struct s0*>(0);
    addr_7654_15:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        goto v1;
    }
    addr_7680_12:
    rax21 = fun_2380();
    *rax21 = 75;
    goto addr_7654_15;
    addr_75dd_13:
    rax22 = xmalloc(rbx11 + 1);
    rbp8 = rax22;
    do {
        eax23 = __cxa_finalize;
        if (eax23 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx24) = eax23;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
            tmp64_25 = reinterpret_cast<struct s0**>(rdx24 + g10);
            rdx26 = tmp64_25;
            __cxa_finalize = eax23 + 8;
        } else {
            rdx26 = g8;
            g8 = rdx26 + 8;
        }
        r15_27 = *rdx26;
        rax28 = fun_2440(r15_27, r15_27);
        fun_2520(rbp8, r15_27, rax28, rbp8, r15_27, rax28);
        rbp8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax28));
        --r13_9;
    } while (r13_9);
    goto addr_7650_14;
}

int32_t rpl_vasprintf(int64_t* rdi, unsigned char* rsi);

int64_t fun_76a3(unsigned char* rdi, int64_t rsi) {
    int64_t* rsp3;
    unsigned char* r8_4;
    struct s0* rax5;
    struct s0* v6;
    uint32_t eax7;
    int64_t rdi8;
    int32_t eax9;
    int64_t rax10;
    int64_t v11;
    int32_t* rax12;
    void* rdx13;
    void* rax14;

    __asm__("cli ");
    rsp3 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 24);
    r8_4 = rdi;
    rax5 = g28;
    v6 = rax5;
    eax7 = *rdi;
    *reinterpret_cast<int32_t*>(&rdi8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax7)) {
        do {
            if (*reinterpret_cast<signed char*>(&eax7) != 37) 
                break;
            if (*reinterpret_cast<signed char*>(r8_4 + rdi8 * 2 + 1) != 0x73) 
                break;
            ++rdi8;
            eax7 = r8_4[rdi8 * 2];
        } while (*reinterpret_cast<signed char*>(&eax7));
        goto addr_7730_5;
    } else {
        goto addr_7730_5;
    }
    eax9 = rpl_vasprintf(rsp3, r8_4);
    rax10 = v11;
    if (eax9 < 0) {
        rax12 = fun_2380();
        if (*rax12 == 12) {
            xalloc_die();
        } else {
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            goto addr_7700_11;
        }
    } else {
        addr_7700_11:
        rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx13) {
            return rax10;
        }
    }
    fun_2450();
    addr_7730_5:
    rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rax14) {
        goto 0x7540;
    }
}

struct s0* vasnprintf();

uint64_t fun_7763(struct s0** rdi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t v7;
    int32_t* rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_2360(rax5, rax5);
            rax8 = fun_2380();
            *rax8 = 75;
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2450();
    } else {
        return rax6;
    }
}

int64_t fun_23c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_77e3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_783e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2380();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_783e_3;
            rax6 = fun_2380();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s26 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2530(struct s26* rdi);

int32_t fun_2580(struct s26* rdi);

int64_t fun_2480(int64_t rdi, ...);

int32_t rpl_fflush(struct s26* rdi);

int64_t fun_2400(struct s26* rdi);

int64_t fun_7853(struct s26* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2530(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2580(rdi);
        if (!(eax3 && (eax4 = fun_2530(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2480(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2380();
            r12d9 = *rax8;
            rax10 = fun_2400(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2400;
}

void rpl_fseeko(struct s26* rdi);

void fun_78e3(struct s26* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2580(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7933(struct s26* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2530(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2480(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2560(int64_t rdi);

signed char* fun_79b3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2560(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2460(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_79f3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2460(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(struct s0* rdi, void* rsi, struct s0* rdx);

int32_t printf_fetchargs(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s27 {
    signed char[7] pad7;
    struct s0* f7;
};

struct s28 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[7] pad40;
    int64_t f28;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    struct s0* f58;
};

struct s29 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

struct s0* fun_7a83(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx) {
    void* rsp5;
    void* rbp6;
    struct s0* r14_7;
    struct s0* r13_8;
    struct s0* r12_9;
    struct s0* v10;
    struct s0** v11;
    struct s0* rax12;
    struct s0* v13;
    int32_t eax14;
    void* rsp15;
    struct s0* r10_16;
    void* rax17;
    int64_t* rsp18;
    struct s0* rbx19;
    int64_t* rsp20;
    struct s0* rax21;
    struct s0* v22;
    int64_t* rsp23;
    struct s0* v24;
    int64_t* rsp25;
    struct s0* v26;
    int64_t* rsp27;
    struct s0* v28;
    int64_t* rsp29;
    int32_t* rax30;
    struct s0* r15_31;
    int32_t* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    struct s0* v35;
    int64_t* rsp36;
    struct s0* v37;
    int64_t* rsp38;
    struct s0* rsi39;
    int32_t eax40;
    int32_t* rax41;
    struct s0* rax42;
    struct s27* v43;
    struct s0* tmp64_44;
    void* v45;
    struct s0* r8_46;
    struct s0* tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    struct s0* v53;
    struct s0* rax54;
    int32_t* rax55;
    struct s28* r14_56;
    struct s28* v57;
    struct s0* r9_58;
    struct s0* r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    struct s0* tmp64_63;
    struct s0* r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    struct s0* rax67;
    int64_t* rsp68;
    struct s0* rax69;
    int64_t* rsp70;
    struct s0* rax71;
    int64_t* rsp72;
    struct s0* rax73;
    int64_t* rsp74;
    struct s0* rax75;
    int64_t* rsp76;
    struct s0* rax77;
    struct s0* tmp64_78;
    int64_t* rsp79;
    struct s0* rax80;
    int64_t* rsp81;
    struct s0* rax82;
    int64_t* rsp83;
    struct s0* rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    struct s0* rsi89;
    struct s0* rax90;
    int64_t* rsp91;
    struct s0* rsi92;
    struct s0* rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s29* rcx98;
    int64_t rax99;
    struct s0* tmp64_100;
    struct s0* r15_101;
    int32_t* rax102;
    int64_t rax103;
    int64_t* rsp104;
    struct s0* rax105;
    int64_t* rsp106;
    int32_t* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    struct s0* rax110;
    int64_t* rsp111;
    int32_t* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x89ba;
                fun_2450();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_89ba_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_89c4_6;
                addr_88ae_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x88d3, rax21 = fun_2590(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x88f9;
                    fun_2360(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x891f;
                    fun_2360(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x8945;
                    fun_2360(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_89c4_6:
            addr_85a8_16:
            v28 = r10_16;
            addr_85af_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x85b4;
            rax30 = fun_2380();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_85c2_18:
            *v32 = 12;
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x8161;
                fun_2360(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x8179;
                fun_2360(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_7db8_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x7dd0;
                fun_2360(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x7de8;
            fun_2360(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_2380();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = 22;
        goto addr_7db8_23;
    }
    rax42 = reinterpret_cast<struct s0*>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_7dad_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(tmp64_44) + 6);
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_7dad_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(tmp64_44) + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<struct s0*>(0);
        v53 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_2540(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_7dad_33:
            rax55 = fun_2380();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = 12;
            goto addr_7db8_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_7bac_46;
    while (1) {
        addr_8504_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_7c6f_50;
            if (r14_56->f50 != -1) 
                goto 0x26a7;
            r9_58 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = reinterpret_cast<struct s0*>(&r12_9->f1);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_84df_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_85a8_16;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_85a8_16;
                if (r10_16 == v10) 
                    goto addr_87f4_64; else 
                    goto addr_84b3_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s28*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_8504_47;
            addr_7bac_46:
            r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8660_73;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8660_73;
                if (r15_31 == v10) 
                    goto addr_85f0_79; else 
                    goto addr_7c07_80;
            }
            addr_7c2c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x7c42;
            fun_2520(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_85f0_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x85f8;
            rax67 = fun_2540(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_8660_73;
            if (!r9_58) 
                goto addr_7c2c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x8637;
            rax69 = fun_2520(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_7c2c_81;
            addr_7c07_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x7c12;
            rax71 = fun_2590(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_8660_73; else 
                goto addr_7c2c_81;
            addr_87f4_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x880a;
            rax73 = fun_2540(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_89c9_84;
            if (r12_9) 
                goto addr_882a_86;
            r10_16 = rax73;
            goto addr_84df_55;
            addr_882a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x883f;
            rax75 = fun_2520(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_84df_55;
            addr_84b3_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x84cc;
            rax77 = fun_2590(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_85af_17;
            r10_16 = rax77;
            goto addr_84df_55;
        }
        break;
    }
    tmp64_78 = reinterpret_cast<struct s0*>(&r12_9->f1);
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_89ba_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_88ae_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_85a8_16;
        rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_896d_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_896d_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_85a8_16; else 
                goto addr_8977_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_8883_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x898e;
        rax80 = fun_2540(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_88ae_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x89ad;
                rax82 = fun_2520(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_88ae_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x88a2;
        rax84 = fun_2590(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_85af_17; else 
            goto addr_88ae_7;
    }
    addr_8977_96:
    rbx19 = r13_8;
    goto addr_8883_98;
    addr_7c6f_50:
    if (r14_56->f50 == -1) 
        goto 0x26a7;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x26ac;
        goto *reinterpret_cast<int32_t*>(0xb510 + r13_87 * 4) + 0xb510;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<struct s0**>(&v53->f0) = reinterpret_cast<struct s0*>(37);
    r13_8 = reinterpret_cast<struct s0*>(&v53->f1);
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        v53->f1 = 39;
        r13_8 = reinterpret_cast<struct s0*>(&v53->f2);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(45);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(43);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(32);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(35);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(73);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(48);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x7d3f;
        fun_2520(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x7d79;
        fun_2520(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xb4a0 + rax95 * 4) + 0xb4a0;
    }
    eax96 = r14_56->f48;
    r13_8->f1 = 0;
    *reinterpret_cast<struct s0**>(&r13_8->f0) = *reinterpret_cast<struct s0**>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x26a7;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s29*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x26a7;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = reinterpret_cast<struct s0*>(&r12_9->f2);
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_7eb2_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_85a8_16;
    }
    addr_7eb2_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_85a8_16;
            rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_7ed3_142; else 
                goto addr_8762_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_8762_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_85a8_16; else 
                    goto addr_876c_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_7ed3_142;
            }
        }
    }
    addr_7f05_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x7f0f;
    rax102 = fun_2380();
    *rax102 = 0;
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x26ac;
    goto *reinterpret_cast<int32_t*>(0xb4c8 + rax103 * 4) + 0xb4c8;
    addr_7ed3_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x87c8;
        rax105 = fun_2540(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_89c9_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x89ce;
            rax107 = fun_2380();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_85c2_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x87ef;
                fun_2520(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_7f05_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x7ef2;
        rax110 = fun_2590(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_85a8_16; else 
            goto addr_7f05_147;
    }
    addr_876c_145:
    rbx19 = tmp64_100;
    goto addr_7ed3_142;
    addr_8660_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x8665;
    rax112 = fun_2380();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_85c2_18;
}

int32_t setlocale_null_r();

int64_t fun_8a03() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

int64_t fun_8a83(int64_t rdi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25a0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax6 = fun_2440(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2520(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2520(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8b33() {
    __asm__("cli ");
    goto fun_25a0;
}

struct s30 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_8b43(int64_t rdi, struct s30* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_8b79_5;
    return 0xffffffff;
    addr_8b79_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb540 + rdx3 * 4) + 0xb540;
}

struct s31 {
    int64_t f0;
    struct s0* f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    struct s0* f20;
};

struct s32 {
    uint64_t f0;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
};

struct s33 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

struct s34 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

int64_t fun_8d73(struct s0* rdi, struct s31* rsi, struct s32* rdx) {
    struct s0* r10_4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s32* r15_7;
    struct s31* r14_8;
    struct s0* rcx9;
    uint64_t r9_10;
    struct s0* v11;
    uint64_t v12;
    uint32_t edx13;
    struct s0* rbx14;
    int64_t rax15;
    struct s0* r12_16;
    int64_t rbp17;
    int32_t edx18;
    struct s0* rdx19;
    int64_t rcx20;
    int32_t esi21;
    struct s0* rdx22;
    struct s33* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s33* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    struct s0* rdx38;
    uint32_t eax39;
    void* rax40;
    struct s0* rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    struct s0* rcx45;
    int64_t rsi46;
    int32_t eax47;
    struct s1* rbx48;
    struct s0* rax49;
    int64_t rsi50;
    int32_t edi51;
    uint64_t rbp52;
    struct s0* rdx53;
    struct s0* r8_54;
    uint64_t rdx55;
    struct s0** rax56;
    struct s0** rcx57;
    uint64_t r9_58;
    struct s0** rbp59;
    struct s0* rsi60;
    struct s0* rax61;
    struct s0* rax62;
    uint64_t rdx63;
    struct s0* rax64;
    struct s33* rbx65;
    uint64_t rsi66;
    int32_t eax67;
    struct s33* rdx68;
    uint64_t rax69;
    uint64_t rcx70;
    uint64_t rcx71;
    uint64_t tmp64_72;
    int32_t eax73;
    struct s0* rax74;
    int64_t rdx75;
    int32_t edi76;
    uint64_t rbx77;
    uint64_t r9_78;
    uint64_t rdx79;
    struct s0** rax80;
    struct s0** rsi81;
    struct s0* rsi82;
    struct s0* rax83;
    struct s0* rdi84;
    struct s0* r8_85;
    struct s0* rdi86;
    uint64_t rdx87;
    struct s0* rax88;
    struct s0* rax89;
    int32_t* rax90;
    struct s0** rax91;
    struct s0* rdi92;
    int32_t* rax93;
    struct s34* rbx94;
    uint64_t rdi95;
    int32_t eax96;
    struct s34* rcx97;
    uint64_t rax98;
    uint64_t rdx99;
    uint64_t rdx100;
    uint64_t tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<struct s0*>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<struct s0*>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_10) + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = 0;
    rdx->f8 = rdi6;
    v12 = 0;
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax5->f0)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = reinterpret_cast<struct s0*>(&rax5->f1);
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_8e28_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<struct s0**>(&rcx9->f0) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_8e15_7:
    return rax15;
    addr_8e28_4:
    r12_16 = reinterpret_cast<struct s0*>(&rcx9->f0);
    *reinterpret_cast<struct s0**>(&r12_16->f0) = rax5;
    r12_16->f10 = 0;
    r12_16->f18 = reinterpret_cast<struct s0*>(0);
    r12_16->f20 = reinterpret_cast<struct s0*>(0);
    r12_16->f28 = 0xffffffffffffffff;
    r12_16->f30 = reinterpret_cast<struct s0*>(0);
    r12_16->f38 = reinterpret_cast<struct s0*>(0);
    r12_16->f40 = 0xffffffffffffffff;
    r12_16->f50 = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = rax5->f1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = rdx19->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            rdx19 = reinterpret_cast<struct s0*>(&rdx19->f1);
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_8e99_11;
    } else {
        addr_8e99_11:
        rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                r12_16->f10 = r12_16->f10 | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx22->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_8eb0_14;
        } else {
            goto addr_8eb0_14;
        }
    }
    rax23 = reinterpret_cast<struct s33*>(&rax5->f2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s33*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_9378_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s33*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s33*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_9378_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<struct s0*>(&rcx26->f2);
    goto addr_8e99_11;
    addr_8eb0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb5bc + rax33 * 4) + 0xb5bc;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        r12_16->f18 = rbx14;
        r12_16->f20 = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = rbx14->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_8ff7_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            r12_16->f18 = rbx14;
            eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0)) - 48;
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_96f9_36:
                r12_16->f20 = rbx14;
                goto addr_96fe_37;
            } else {
                rdx38 = rbx14;
                do {
                    rdx38 = reinterpret_cast<struct s0*>(&rdx38->f1);
                    eax39 = rdx38->f1 - 48;
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_96f4_42;
            }
        } else {
            addr_8edd_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_90f8_44:
                if (rbx14->f1 != 42) {
                    r12_16->f30 = rbx14;
                    rdx41 = reinterpret_cast<struct s0*>(&rbx14->f1);
                    eax42 = rbx14->f1 - 48;
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            rdx41 = reinterpret_cast<struct s0*>(&rdx41->f1);
                            eax44 = rdx41->f1 - 48;
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    r12_16->f38 = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_8ee7_52;
                } else {
                    rcx45 = reinterpret_cast<struct s0*>(&rbx14->f2);
                    r12_16->f30 = rbx14;
                    r12_16->f38 = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = rbx14->f2;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_98e4_56; else 
                        goto addr_9135_57;
                }
            } else {
                addr_8ee7_52:
                rbx48 = reinterpret_cast<struct s1*>(&rbx14->f1);
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = rbx48->f0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        rbx48 = reinterpret_cast<struct s1*>(&rbx48->f1);
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_8f08_60;
                } else {
                    goto addr_8f08_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = rax49->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        rax49 = reinterpret_cast<struct s0*>(&rax49->f1);
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_947b_65;
    addr_8ff7_33:
    r12_16->f28 = 0;
    if (0) 
        goto addr_9378_22;
    rbp52 = 0;
    v12 = 1;
    rbx14 = rdx22;
    addr_901c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (7 > rbp52) {
        addr_909a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<struct s0**>((rdx55 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx55 <= rbp52) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<struct s0*>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (rdx55 <= rbp52);
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<struct s0*>(0);
        }
    } else {
        r9_58 = 14;
        if (14 <= rbp52) {
            r9_58 = rbp52 + 1;
        }
        if (r9_58 >> 59) 
            goto addr_999b_75; else 
            goto addr_9043_76;
    }
    rbp59 = reinterpret_cast<struct s0**>((rbp52 << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_937c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<struct s0*>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_8ee7_52;
        goto addr_90f8_44;
    }
    addr_96fe_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_8edd_43;
    addr_999b_75:
    if (v11 != rdx53) {
        fun_2360(rdx53);
        r10_4 = r10_4;
        goto addr_97aa_84;
    } else {
        goto addr_97aa_84;
    }
    addr_9043_76:
    rsi60 = reinterpret_cast<struct s0*>(r9_58 << 5);
    if (v11 == rdx53) {
        rax61 = fun_2540(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2590(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_999b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2520(r8_54, v11, rdx63 << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_909a_68;
    addr_947b_65:
    rbx65 = reinterpret_cast<struct s33*>(&rbx14->f2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s33*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (rsi66 > 0x1999999999999999) {
            rcx70 = 0xffffffffffffffff;
        } else {
            rcx71 = rsi66 + rsi66 * 4;
            rcx70 = rcx71 + rcx71;
        }
        while (tmp64_72 = rcx70 + rax69, rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), tmp64_72 < rcx70) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_9378_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s33*>(&rbx65->pad2);
            rcx70 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s33*>(&rbx65->pad2);
    }
    rbp52 = tmp64_72 - 1;
    if (rbp52 > 0xfffffffffffffffd) 
        goto addr_9378_22;
    r12_16->f28 = rbp52;
    rbx14 = reinterpret_cast<struct s0*>(&rdx68->f2);
    goto addr_901c_67;
    label_41:
    addr_96f4_42:
    goto addr_96f9_36;
    addr_98e4_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = rax74->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        rax74 = reinterpret_cast<struct s0*>(&rax74->f1);
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_990a_104;
    addr_9135_57:
    rbx77 = r12_16->f40;
    if (rbx77 == 0xffffffffffffffff) {
        r12_16->f40 = v12;
        if (0) {
            addr_9378_22:
            r8_54 = r15_7->f8;
            goto addr_937c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_9144_107:
    r8_54 = r15_7->f8;
    if (r9_10 <= rbx77) {
        r9_78 = r9_10 + r9_10;
        if (r9_78 <= rbx77) {
            r9_78 = rbx77 + 1;
        }
        if (!(r9_78 >> 59)) 
            goto addr_97f2_111;
    } else {
        addr_9151_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<struct s0**>((rdx79 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx79 <= rbx77) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<struct s0*>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (rdx79 <= rbx77);
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<struct s0*>(0);
            goto addr_9187_116;
        }
    }
    rdx53 = r8_54;
    goto addr_999b_75;
    addr_97f2_111:
    rsi82 = reinterpret_cast<struct s0*>(r9_78 << 5);
    if (v11 == r8_54) {
        rax83 = fun_2540(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_97aa_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_2360(rdi86);
            }
        } else {
            addr_9a30_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2520(rdi84, r8_85, rdx87 << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_984f_121;
        }
    } else {
        rax89 = fun_2590(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_999b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_9a30_120;
            }
        }
    }
    rax90 = fun_2380();
    *rax90 = 12;
    return 0xffffffff;
    addr_984f_121:
    r15_7->f8 = r8_54;
    goto addr_9151_112;
    addr_9187_116:
    rax91 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_54) + (rbx77 << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_937c_80:
            if (v11 != r8_54) {
                fun_2360(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_8ee7_52;
        }
    } else {
        *rax91 = reinterpret_cast<struct s0*>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_8ee7_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_2360(rdi92, rdi92);
    }
    rax93 = fun_2380();
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_8e15_7;
    addr_990a_104:
    rbx94 = reinterpret_cast<struct s34*>(&rbx14->f3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s34*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (rdi95 > 0x1999999999999999) {
            rdx99 = 0xffffffffffffffff;
        } else {
            rdx100 = rdi95 + rdi95 * 4;
            rdx99 = rdx100 + rdx100;
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = rdx99 + rax98, rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), tmp64_101 < rdx99) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_9378_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s34*>(&rbx94->pad2);
            rdx99 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s34*>(&rbx94->pad2);
    }
    rbx77 = tmp64_101 + 0xffffffffffffffff;
    if (rbx77 > 0xfffffffffffffffd) 
        goto addr_9378_22;
    r12_16->f40 = rbx77;
    rcx45 = reinterpret_cast<struct s0*>(&rcx97->f2);
    goto addr_9144_107;
    addr_8f08_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb720 + rax105 * 4) + 0xb720;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb664 + rax106 * 4) + 0xb664;
    }
}

void fun_9b03() {
    __asm__("cli ");
}

void fun_9b17() {
    __asm__("cli ");
    return;
}

void fun_3218() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3268() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_32c0() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3318() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3370() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) {
        goto 0x31f0;
    } else {
        goto 0x31f0;
    }
}

void fun_33e8() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3440() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    eax8 = fun_2500(rdi4, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3490() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_34e0() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    eax8 = fun_2500(rdi4, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

int32_t fun_25e0(int64_t rdi, int64_t rsi);

void fun_3530() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    fun_25e0(rdi4, 1);
    goto 0x31f0;
}

void fun_3570() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    fun_2500(rdi6, __zero_stack_offset());
    goto 0x31f0;
}

void fun_35b0() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int32_t eax4;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    pos = eax4 + 2;
    goto 0x31f0;
}

void fun_35e0() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    eax8 = fun_2500(rdi4, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

void fun_3630() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    fun_25e0(rdi4, 4);
    goto 0x31f0;
}

void fun_3670() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;
    int32_t eax8;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    eax8 = fun_2500(rdi4, __zero_stack_offset());
    if (eax8) 
        goto 0x31f0;
    goto 0x31f0;
}

uint64_t fun_2510(struct s0* rdi);

int32_t fun_23d0();

void fun_36c8() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    struct s0* rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;
    struct s0* rax8;
    int32_t* rax9;
    uint64_t rax10;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<struct s0**>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    rax8 = find_int(rdi4);
    rax9 = fun_2380();
    *rax9 = 0;
    rax10 = fun_2510(rax8);
    if (*rax9 == 34) 
        goto 0x31f0;
    if (rax10 > 0x7fffffff) 
        goto 0x31f0;
    fun_23d0();
    goto 0x31f0;
}

void fun_3740() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int32_t eax4;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    pos = eax4 + 2;
    goto 0x31f0;
}

int32_t fun_2570(int64_t rdi, void* rsi);

void fun_3770() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;
    int32_t* rax9;
    int32_t eax10;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x3868;
    rax9 = fun_2380();
    *rax9 = 0;
    eax10 = fun_2570(rdi6, __zero_stack_offset());
    if (eax10 == -1) {
        if (*rax9) 
            goto 0x31f0;
    }
    goto 0x31f0;
}

int32_t fun_24a0(int64_t rdi, void* rsi);

void fun_37d0() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    uint32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t r8_7;
    int32_t eax8;
    int32_t* rax9;
    int32_t eax10;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    eax4 = eax5 + 2;
    pos = eax4;
    rdi6 = *reinterpret_cast<int64_t*>(r8_7 + eax4 * 8 - 8);
    eax8 = fun_2500(rdi6, __zero_stack_offset());
    if (eax8) 
        goto 0x3868;
    rax9 = fun_2380();
    *rax9 = 0;
    eax10 = fun_24a0(rdi6, __zero_stack_offset());
    if (eax10 == -1) {
        if (*rax9) 
            goto 0x31f0;
    }
    goto 0x31f0;
}

void fun_3830() {
    uint32_t edx1;
    int64_t rax2;
    int1_t less3;
    int64_t rdi4;
    int64_t r8_5;
    int64_t rsi6;
    int32_t eax7;

    edx1 = static_cast<uint32_t>(rax2 + 1);
    less3 = reinterpret_cast<int32_t>(edx1) < reinterpret_cast<int32_t>(argc);
    pos = edx1;
    if (!less3) 
        goto 0x388e;
    rdi4 = *reinterpret_cast<int64_t*>(r8_5 + rsi6 + 8);
    pos = eax7 + 2;
    fun_25e0(rdi4, 2);
    goto 0x31f0;
}

uint32_t fun_24c0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2640(int64_t rdi, unsigned char* rsi);

uint32_t fun_2630(struct s0* rdi, unsigned char* rsi);

void fun_4795() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rax10;
    struct s0* r11_11;
    struct s0* v12;
    int32_t ebp13;
    struct s0* rax14;
    void* r15_15;
    int32_t ebx16;
    struct s0* rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    void* rsi30;
    void* v31;
    struct s0* v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rdx44;
    struct s0* rcx45;
    uint32_t edx46;
    unsigned char al47;
    struct s0* v48;
    int64_t v49;
    struct s0* v50;
    struct s0* v51;
    struct s0* rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    struct s0* v61;
    unsigned char v62;
    void* v63;
    void* v64;
    struct s0* v65;
    signed char* v66;
    struct s0* r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    struct s0* r13_72;
    unsigned char* rsi73;
    void* v74;
    struct s0* r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    unsigned char** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    uint64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx17->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4a93_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4a93_22; else 
                            goto addr_4e8d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4f4d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_52a0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4a90_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4a90_30; else 
                                goto addr_52b9_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_52a0_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24c0(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_52a0_28; else 
                            goto addr_493c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5400_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5280_40:
                        if (r11_27 == 1) {
                            addr_4e0d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_53c8_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_4a47_44;
                            }
                        } else {
                            goto addr_5290_46;
                        }
                    } else {
                        addr_540f_47:
                        rax24 = v48;
                        if (!rax24->f1) {
                            goto addr_4e0d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4a93_22:
                                if (v49 != 1) {
                                    addr_4fe9_52:
                                    v50 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2440(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_5034_54;
                                    }
                                } else {
                                    goto addr_4aa0_56;
                                }
                            } else {
                                addr_4a45_57:
                                ebp36 = 0;
                                goto addr_4a47_44;
                            }
                        } else {
                            addr_5274_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_540f_47; else 
                                goto addr_527e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4e0d_41;
                        if (v49 == 1) 
                            goto addr_4aa0_56; else 
                            goto addr_4fe9_52;
                    }
                }
                addr_4b01_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<struct s0*>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4998_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_49bd_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_4cc0_65;
                    } else {
                        addr_4b29_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5378_67;
                    }
                } else {
                    goto addr_4b20_69;
                }
                addr_49d1_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_4a1c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5378_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_4a1c_81;
                }
                addr_4b20_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_49bd_64; else 
                    goto addr_4b29_66;
                addr_4a47_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_4aff_91;
                if (v22) 
                    goto addr_4a5f_93;
                addr_4aff_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4b01_62;
                addr_5034_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_57bb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_582b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_562f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2640(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2630(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_512e_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_4aec_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_5138_112;
                    }
                } else {
                    addr_5138_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5209_114;
                }
                addr_4af8_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4aff_91;
                while (1) {
                    addr_5209_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_5717_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_5176_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_5725_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_51f7_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_51f7_128;
                        }
                    }
                    addr_51a5_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_51f7_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_5176_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_51a5_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4a1c_81;
                addr_5725_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_5378_67;
                addr_57bb_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_512e_109;
                addr_582b_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_512e_109;
                addr_4aa0_56:
                rax98 = fun_2650(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>(((*rax98 + reinterpret_cast<unsigned char>(rax24) * 2)[1] & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_4aec_110;
                addr_527e_59:
                goto addr_5280_40;
                addr_4f4d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4a93_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_4af8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4a45_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4a93_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4f92_160;
                if (!v22) 
                    goto addr_5367_162; else 
                    goto addr_5573_163;
                addr_4f92_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_5367_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_5378_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_4e3b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_4ca3_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_49d1_70; else 
                    goto addr_4cb7_169;
                addr_4e3b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4998_63;
                goto addr_4b20_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5274_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_53af_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a90_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_4988_178; else 
                        goto addr_5332_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5274_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a93_22;
                }
                addr_53af_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4a90_30:
                    r8d42 = 0;
                    goto addr_4a93_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4b01_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_53c8_42;
                    }
                }
                addr_4988_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4998_63;
                addr_5332_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5290_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4b01_62;
                } else {
                    addr_5342_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4a93_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_5af2_188;
                if (v28) 
                    goto addr_5367_162;
                addr_5af2_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_4ca3_168;
                addr_493c_37:
                if (v22) 
                    goto addr_5933_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4953_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5400_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_548b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a93_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_4988_178; else 
                        goto addr_5467_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5274_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a93_22;
                }
                addr_548b_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4a93_22;
                }
                addr_5467_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5290_46;
                goto addr_5342_186;
                addr_4953_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4a93_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4a93_22; else 
                    goto addr_4964_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_5a3e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_58c4_210;
            if (1) 
                goto addr_58c2_212;
            if (!v29) 
                goto addr_54fe_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v49 = rax108;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_5a31_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4cc0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_4a7b_219; else 
            goto addr_4cda_220;
        addr_4a5f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_4a73_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_4cda_220; else 
            goto addr_4a7b_219;
        addr_562f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_4cda_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_564d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_5ac0_225:
        v31 = r13_34;
        addr_5526_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_5717_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a73_221;
        addr_5573_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a73_221;
        addr_4cb7_169:
        goto addr_4cc0_65;
        addr_5a3e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_4cda_220;
        goto addr_564d_222;
        addr_58c4_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = reinterpret_cast<uint64_t>(v119 - reinterpret_cast<unsigned char>(g28));
        if (!rax118) 
            goto addr_591e_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5ac0_225;
        addr_58c2_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_58c4_210;
        addr_54fe_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_58c4_210;
        } else {
            goto addr_5526_226;
        }
        addr_5a31_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4e8d_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb22c + rax120 * 4) + 0xb22c;
    addr_52b9_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb32c + rax121 * 4) + 0xb32c;
    addr_5933_190:
    addr_4a7b_219:
    goto 0x4760;
    addr_4964_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb12c + rax122 * 4) + 0xb12c;
    addr_591e_236:
    goto v123;
}

void fun_4980() {
}

void fun_4b38() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4832;
}

void fun_4b91() {
    goto 0x4832;
}

void fun_4c7e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4b01;
    }
    if (v2) 
        goto 0x5573;
    if (!r10_3) 
        goto addr_56de_5;
    if (!v4) 
        goto addr_55ae_7;
    addr_56de_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_55ae_7:
    goto 0x49b4;
}

void fun_4c9c() {
}

void fun_4d47() {
    signed char v1;

    if (v1) {
        goto 0x4ccf;
    } else {
        goto 0x4a0a;
    }
}

void fun_4d61() {
    signed char v1;

    if (!v1) 
        goto 0x4d5a; else 
        goto "???";
}

void fun_4d88() {
    goto 0x4ca3;
}

void fun_4e08() {
}

void fun_4e20() {
}

void fun_4e4f() {
    goto 0x4ca3;
}

void fun_4ea1() {
    goto 0x4e30;
}

void fun_4ed0() {
    goto 0x4e30;
}

void fun_4f03() {
    goto 0x4e30;
}

void fun_52d0() {
    goto 0x4988;
}

void fun_55ce() {
    signed char v1;

    if (v1) 
        goto 0x5573;
    goto 0x49b4;
}

void fun_5675() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x49b4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4998;
        goto 0x49b4;
    }
}

void fun_5a92() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4d00;
    } else {
        goto 0x4832;
    }
}

int32_t fun_2350(int64_t rdi);

struct s35 {
    signed char[1] pad1;
    signed char f1;
};

struct s36 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_7f78() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    struct s0* rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    struct s0* rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    struct s0* rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    struct s0* rax30;
    struct s0* rcx31;
    void* rbx32;
    void* rbx33;
    struct s0* tmp64_34;
    void* r12_35;
    struct s0* rax36;
    struct s0* rbx37;
    int64_t r15_38;
    int64_t rbp39;
    struct s0* r15_40;
    struct s0* rax41;
    struct s0* rax42;
    int64_t r12_43;
    struct s0* r15_44;
    struct s0* r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s36* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_2350(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<struct s0**>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8103_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_2350(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_2350(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8103_5;
    }
    rcx19 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x26a7;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_800d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x84e7;
        }
    } else {
        addr_8005_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_800d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x8150;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x7f28;
        goto 0x85c2;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x85c2;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_8046_26;
    rax36 = rcx31;
    addr_8046_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x7f28;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x85c2;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2590(r15_40, rax36);
        if (!rax41) 
            goto 0x85c2;
        goto 0x7f28;
    }
    rax42 = fun_2540(rax36, rsi10);
    if (!rax42) 
        goto 0x85c2;
    if (r12_43) 
        goto addr_840a_36;
    goto 0x7f28;
    addr_840a_36:
    fun_2520(rax42, r15_44, r12_45);
    goto 0x7f28;
    addr_8103_5:
    if ((*reinterpret_cast<struct s35**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s35**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x7f28;
    }
    if (eax7 >= 0) 
        goto addr_8005_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x8150;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_8147_42;
    eax50 = 84;
    addr_8147_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_8090() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_8180() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x82c5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_2350(r15_4 + r12_5);
            goto 0x7fdd;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x7fb3;
        }
    }
}

void fun_81c8() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_2350(rdi5);
            goto 0x7fdd;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_2350(rdi5);
    goto 0x7fdd;
}

void fun_8230() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x80b6;
}

void fun_8280() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x8260;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x80bf;
}

void fun_8330() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x80b6;
    goto 0x8260;
}

void fun_83c3() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x7e32;
}

void fun_8560() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x84e7;
}

struct s37 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s38 {
    signed char[8] pad8;
    int64_t f8;
};

struct s39 {
    signed char[16] pad16;
    int64_t f10;
};

struct s40 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8b88() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s37* rcx3;
    struct s38* rcx4;
    int64_t r11_5;
    struct s39* rcx6;
    uint32_t* rcx7;
    struct s40* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x8b70; else 
        goto "???";
}

struct s41 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s42 {
    signed char[8] pad8;
    int64_t f8;
};

struct s43 {
    signed char[16] pad16;
    int64_t f10;
};

struct s44 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8bc0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s41* rcx3;
    struct s42* rcx4;
    int64_t r11_5;
    struct s43* rcx6;
    uint32_t* rcx7;
    struct s44* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8ba6;
}

struct s45 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s46 {
    signed char[8] pad8;
    int64_t f8;
};

struct s47 {
    signed char[16] pad16;
    int64_t f10;
};

struct s48 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8be0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s45* rcx3;
    struct s46* rcx4;
    int64_t r11_5;
    struct s47* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s48* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8ba6;
}

struct s49 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s50 {
    signed char[8] pad8;
    int64_t f8;
};

struct s51 {
    signed char[16] pad16;
    int64_t f10;
};

struct s52 {
    signed char[16] pad16;
    signed char f10;
};

void fun_8c00() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s49* rcx3;
    struct s50* rcx4;
    int64_t r11_5;
    struct s51* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s52* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8ba6;
}

struct s53 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s54 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8c80() {
    struct s53* rcx1;
    struct s54* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8ba6;
}

struct s55 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s56 {
    signed char[8] pad8;
    int64_t f8;
};

struct s57 {
    signed char[16] pad16;
    int64_t f10;
};

struct s58 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8cd0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s55* rcx3;
    struct s56* rcx4;
    int64_t r11_5;
    struct s57* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s58* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8ba6;
}

void fun_8f1c() {
}

void fun_8f4b() {
    goto 0x8f28;
}

void fun_8fa0() {
}

void fun_91b0() {
    goto 0x8fa3;
}

struct s59 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s60 {
    signed char[8] pad8;
    int64_t f8;
};

struct s61 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s62 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_9258() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    struct s0* rbp4;
    struct s59* r14_5;
    struct s0* rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    struct s0* r10_10;
    struct s0* rax11;
    struct s0* rcx12;
    int64_t v13;
    struct s60* r15_14;
    struct s0* rax15;
    struct s61* r14_16;
    struct s0* r10_17;
    int64_t r13_18;
    struct s0* rax19;
    struct s62* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x9997;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x9997;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<struct s0*>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_2540(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x97b8; else 
                goto "???";
        }
    } else {
        rax15 = fun_2590(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x9997;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_9aea_9; else 
            goto addr_92ce_10;
    }
    addr_9424_11:
    rax19 = fun_2520(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_92ce_10:
    r14_20->f8 = rcx12;
    goto 0x8de9;
    addr_9aea_9:
    r13_18 = *r14_21;
    goto addr_9424_11;
}

struct s63 {
    signed char[11] pad11;
    struct s0* fb;
};

struct s64 {
    signed char[80] pad80;
    int64_t f50;
};

struct s65 {
    signed char[80] pad80;
    int64_t f50;
};

struct s66 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s67 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s68 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s69 {
    signed char[72] pad72;
    signed char f48;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_94e1() {
    struct s0* ecx1;
    int32_t edx2;
    struct s63* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s64* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s65* r12_10;
    int64_t r13_11;
    struct s0* r8_12;
    struct s66* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    struct s0* rsi18;
    struct s0* v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    struct s0** rax22;
    struct s0** rsi23;
    uint64_t* r15_24;
    struct s0* rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    struct s0* rdi28;
    struct s0* r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    struct s0* rax32;
    struct s67* r15_33;
    struct s0* rax34;
    uint64_t r11_35;
    struct s0* v36;
    struct s68* r15_37;
    struct s0** r13_38;
    struct s69* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s70* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<struct s0*>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s63*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<struct s0*>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x9378;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x9acc;
        rsi18 = reinterpret_cast<struct s0*>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_963d_12;
    } else {
        addr_91e4_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<struct s0**>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<struct s0*>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<struct s0*>(0);
            goto addr_921f_17;
        }
    }
    rax25 = fun_2540(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x97aa;
    addr_9750_19:
    rdx30 = *r15_31;
    rax32 = fun_2520(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_9686_20:
    r15_33->f8 = r8_12;
    goto addr_91e4_13;
    addr_963d_12:
    rax34 = fun_2590(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x9997;
    if (v36 != r15_37->f8) 
        goto addr_9686_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_9750_19;
    addr_921f_17:
    r13_38 = reinterpret_cast<struct s0**>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x937c;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x9260;
    goto 0x8de9;
}

void fun_94ff() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91c8;
    if (dl2 & 4) 
        goto 0x91c8;
    if (edx3 > 7) 
        goto 0x91c8;
    if (dl4 & 2) 
        goto 0x91c8;
    goto 0x91c8;
}

void fun_9548() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91c8;
    if (dl2 & 4) 
        goto 0x91c8;
    if (edx3 > 7) 
        goto 0x91c8;
    if (dl4 & 2) 
        goto 0x91c8;
    goto 0x91c8;
}

void fun_9590() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91c8;
    if (dl2 & 4) 
        goto 0x91c8;
    if (edx3 > 7) 
        goto 0x91c8;
    if (dl4 & 2) 
        goto 0x91c8;
    goto 0x91c8;
}

void fun_95d8() {
    goto 0x91c8;
}

void fun_4bbe() {
    goto 0x4832;
}

void fun_4d94() {
    goto 0x4d4c;
}

void fun_4e5b() {
    goto 0x4988;
}

void fun_4ead() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4e30;
    goto 0x4a5f;
}

void fun_4edf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4e3b;
        goto 0x4860;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4cda;
        goto 0x4a7b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5678;
    if (r10_8 > r15_9) 
        goto addr_4dc5_9;
    addr_4dca_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5683;
    goto 0x49b4;
    addr_4dc5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4dca_10;
}

void fun_4f12() {
    goto 0x4a47;
}

void fun_52e0() {
    goto 0x4a47;
}

void fun_5a7f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4b9c;
    } else {
        goto 0x4d00;
    }
}

struct s71 {
    signed char[1] pad1;
    signed char f1;
};

void fun_7e20() {
    signed char* r13_1;
    struct s71* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_856e() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x84e7;
}

struct s72 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s73 {
    signed char[8] pad8;
    int64_t f8;
};

struct s74 {
    signed char[16] pad16;
    int64_t f10;
};

struct s75 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8ca0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s72* rcx3;
    struct s73* rcx4;
    int64_t r11_5;
    struct s74* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s75* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8ba6;
}

void fun_8f55() {
    goto 0x8f28;
}

void fun_98a4() {
    goto 0x91c8;
}

void fun_91b8() {
}

void fun_95e8() {
    goto 0x91c8;
}

void fun_4f1c() {
    goto 0x4eb7;
}

void fun_52ea() {
    goto 0x4e0d;
}

void fun_8300() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x80b6;
    goto 0x8260;
}

void fun_857c() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x84e7;
}

struct s76 {
    int32_t f0;
    int32_t f4;
};

struct s77 {
    int32_t f0;
    int32_t f4;
};

struct s78 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s79 {
    signed char[8] pad8;
    int64_t f8;
};

struct s80 {
    signed char[8] pad8;
    int64_t f8;
};

struct s81 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_8c50(struct s76* rdi, struct s77* rsi) {
    struct s78* rcx3;
    struct s79* rcx4;
    struct s80* rcx5;
    struct s81* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8ba6;
}

void fun_8f5f() {
    goto 0x8f28;
}

void fun_98ae() {
    goto 0x91c8;
}

void fun_4bed() {
    goto 0x4832;
}

void fun_4f28() {
    goto 0x4eb7;
}

void fun_52f7() {
    goto 0x4e5e;
}

void fun_858b() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x84e7;
}

void fun_8f69() {
    goto 0x8f28;
}

void fun_4c1a() {
    goto 0x4832;
}

void fun_4f34() {
    goto 0x4e30;
}

void fun_8f73() {
    goto 0x8f28;
}

void fun_4c3c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x55d0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4b01;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4b01;
    }
    if (v11) 
        goto 0x5933;
    if (r10_12 > r15_13) 
        goto addr_5983_8;
    addr_5988_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x56c1;
    addr_5983_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5988_9;
}
