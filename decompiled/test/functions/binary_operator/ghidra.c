ulong binary_operator(byte param_1)

{
  long lVar1;
  char **ppcVar2;
  long lVar3;
  ulong uVar4;
  char cVar5;
  int iVar6;
  int iVar7;
  size_t sVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  long lVar11;
  char *pcVar12;
  byte bVar13;
  ulong uVar14;
  long in_FS_OFFSET;
  bool bVar15;
  stat local_188;
  stat local_f8;
  undefined local_68 [32];
  undefined local_48 [24];
  long local_30;
  
  lVar3 = argv;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 != 0) {
    pos = pos + 1;
  }
  iVar6 = pos + 1;
  bVar13 = 0;
  if (iVar6 < argc + -2) {
    pcVar12 = *(char **)(argv + (long)(pos + 2) * 8);
    if (((*pcVar12 == '-') && (pcVar12[1] == 'l')) && (pcVar12[2] == '\0')) {
      bVar13 = 1;
      pos = iVar6;
    }
    else {
      bVar13 = 0;
    }
  }
  iVar7 = pos;
  lVar11 = (long)iVar6;
  pcVar12 = *(char **)(argv + lVar11 * 8);
  lVar1 = lVar11 * 8;
  if (*pcVar12 != '-') {
    if ((*pcVar12 == '=') && ((pcVar12[1] == '\0' || ((pcVar12[1] == '=' && (pcVar12[2] == '\0')))))
       ) {
      iVar6 = strcmp(*(char **)(argv + -0x10 + ((long)pos + 2) * 8),
                     *(char **)(argv + ((long)pos + 2) * 8));
      uVar14 = (ulong)(iVar6 == 0);
      pos = iVar7 + 3;
    }
    else {
      if ((*pcVar12 != '!') || ((pcVar12[1] != '=' || (pcVar12[2] != '\0')))) {
                    /* WARNING: Subroutine does not return */
        abort();
      }
      iVar6 = strcmp(*(char **)(argv + -0x10 + ((long)pos + 2) * 8),
                     *(char **)(argv + ((long)pos + 2) * 8));
      uVar14 = (ulong)(iVar6 != 0);
      pos = iVar7 + 3;
    }
    goto LAB_00102cc9;
  }
  cVar5 = pcVar12[1];
  if ((cVar5 == 'l') || (cVar5 == 'g')) {
    if ((pcVar12[2] != 'e') && (pcVar12[2] != 't')) goto LAB_00102d34;
    if (pcVar12[3] != '\0') goto LAB_00102d44;
  }
  else {
LAB_00102d34:
    if (cVar5 != 'e') {
      if (cVar5 == 'n') {
        cVar5 = pcVar12[2];
        if (cVar5 == 'e') goto LAB_0010300c;
      }
      else {
LAB_00102d44:
        if (cVar5 == 'o') {
          if ((pcVar12[2] != 't') || (pcVar12[3] != '\0')) goto LAB_00103016;
          pos = pos + 3;
          uVar14 = (ulong)(bVar13 | param_1);
          if ((bVar13 | param_1) != 0) {
            pcVar12 = "-ot does not accept -l";
            goto LAB_001030f1;
          }
          iVar6 = stat(*(char **)(argv + -8 + lVar1),&local_f8);
          lVar11 = local_f8.st_mtim.tv_nsec;
          uVar4 = local_f8.st_mtim.tv_sec;
          ppcVar2 = (char **)(lVar3 + 8 + lVar1);
          if (iVar6 == 0) {
            iVar6 = stat(*ppcVar2,&local_f8);
            if (iVar6 == 0) {
              uVar14 = (ulong)(((uint)(local_f8.st_mtim.tv_nsec < lVar11) -
                               (uint)(lVar11 < local_f8.st_mtim.tv_nsec)) +
                               ((uint)(local_f8.st_mtim.tv_sec < (long)uVar4) -
                               (uint)((long)uVar4 < local_f8.st_mtim.tv_sec)) * 2 >> 0x1f);
            }
          }
          else {
            iVar6 = stat(*ppcVar2,&local_f8);
            uVar14 = (ulong)(iVar6 == 0);
          }
          goto LAB_00102cc9;
        }
        if ('o' < cVar5) goto LAB_00103016;
        if (cVar5 == 'e') {
          cVar5 = pcVar12[2];
          goto LAB_00102e0c;
        }
        if (cVar5 != 'n') goto LAB_00103016;
        cVar5 = pcVar12[2];
      }
      if ((cVar5 != 't') || (pcVar12[3] != '\0')) {
LAB_00103016:
        uVar10 = quote();
        uVar9 = dcgettext(0,"%s: unknown binary operator",5);
                    /* WARNING: Subroutine does not return */
        test_syntax_error(uVar9,uVar10);
      }
      pos = pos + 3;
      uVar14 = (ulong)(bVar13 | param_1);
      if ((bVar13 | param_1) != 0) {
        pcVar12 = "-nt does not accept -l";
LAB_001030f1:
        pos = iVar7 + 3;
        uVar10 = dcgettext(0,pcVar12,5);
                    /* WARNING: Subroutine does not return */
        test_syntax_error(uVar10);
      }
      iVar6 = stat(*(char **)(argv + -8 + lVar1),&local_f8);
      lVar11 = local_f8.st_mtim.tv_nsec;
      uVar4 = local_f8.st_mtim.tv_sec;
      ppcVar2 = (char **)(lVar3 + 8 + lVar1);
      if (iVar6 == 0) {
        iVar7 = stat(*ppcVar2,&local_f8);
        uVar14 = uVar4;
        if (iVar7 == 0) {
          uVar14 = uVar4 & 0xffffffffffffff00 |
                   (ulong)(0 < (int)(((uint)(local_f8.st_mtim.tv_nsec < lVar11) -
                                     (uint)(lVar11 < local_f8.st_mtim.tv_nsec)) +
                                    ((uint)(local_f8.st_mtim.tv_sec < (long)uVar4) -
                                    (uint)((long)uVar4 < local_f8.st_mtim.tv_sec)) * 2));
          goto LAB_00102cc9;
        }
      }
      else {
        iVar7 = stat(*ppcVar2,&local_f8);
        if (iVar7 == 0) goto LAB_00102cc9;
      }
      uVar14 = uVar14 & 0xffffffffffffff00 | (ulong)(iVar6 == 0);
      goto LAB_00102cc9;
    }
    cVar5 = pcVar12[2];
    if (cVar5 != 'q') {
LAB_00102e0c:
      if ((cVar5 != 'f') || (pcVar12[3] != '\0')) goto LAB_00103016;
      pos = pos + 3;
      uVar14 = (ulong)(bVar13 | param_1);
      if ((bVar13 | param_1) != 0) {
        pcVar12 = "-ef does not accept -l";
        goto LAB_001030f1;
      }
      iVar6 = stat(*(char **)(argv + -8 + lVar1),&local_188);
      if (iVar6 == 0) {
        iVar6 = stat(*(char **)(lVar3 + 8 + lVar1),&local_f8);
        if ((iVar6 == 0) && (local_188.st_dev == local_f8.st_dev)) {
          uVar14 = (ulong)(local_188.st_ino == local_f8.st_ino);
        }
      }
      goto LAB_00102cc9;
    }
LAB_0010300c:
    if (pcVar12[3] != '\0') goto LAB_00103016;
  }
  if (param_1 == 0) {
    uVar10 = find_int();
  }
  else {
    sVar8 = strlen(*(char **)(argv + -8 + lVar1));
    uVar10 = umaxtostr(sVar8,local_68);
  }
  if (bVar13 == 0) {
    uVar9 = find_int(*(undefined8 *)(argv + 8 + lVar1));
  }
  else {
    sVar8 = strlen(*(char **)(argv + 0x10 + lVar1));
    uVar9 = umaxtostr(sVar8,local_48);
  }
  iVar6 = strintcmp(uVar10,uVar9);
  lVar3 = *(long *)(argv + lVar11 * 8);
  bVar15 = *(char *)(lVar3 + 2) == 'e';
  cVar5 = *(char *)(lVar3 + 1);
  pos = pos + 3;
  if (cVar5 == 'l') {
    uVar14 = (ulong)(iVar6 < (int)(uint)bVar15);
  }
  else if (cVar5 == 'g') {
    uVar14 = (ulong)((int)-(uint)bVar15 < iVar6);
  }
  else {
    uVar14 = (ulong)((iVar6 != 0) == bVar15);
  }
LAB_00102cc9:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar14 & 0xffffffff;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}