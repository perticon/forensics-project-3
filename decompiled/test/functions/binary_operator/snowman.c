uint32_t binary_operator(uint32_t edi, struct s0* rsi) {
    uint32_t esi3;
    void* rsp4;
    struct s0* rax5;
    int64_t rax6;
    uint32_t edx7;
    struct s0* rcx8;
    struct s0* rbp9;
    int64_t r12_10;
    int64_t rbx11;
    struct s0* rdi12;
    void* r13_13;
    uint32_t eax14;
    uint32_t eax15;
    int64_t rax16;
    int64_t rbx17;
    int64_t rax18;
    struct s0* rsi19;
    struct s0* rdi20;
    int32_t eax21;
    int64_t rax22;
    int64_t rbx23;
    int64_t rax24;
    struct s0* rsi25;
    struct s0* rdi26;
    int32_t eax27;
    uint32_t eax28;
    uint32_t edx29;
    uint32_t eax30;
    struct s0* rdi31;
    struct s0* rax32;
    void* rsp33;
    struct s0* rbp34;
    struct s0* rax35;
    void* rsp36;
    struct s0* rax37;
    struct s0* rax38;
    struct s0* rax39;
    struct s0* r8_40;
    int64_t r9_41;
    int64_t* rax42;
    uint32_t eax43;
    uint32_t tmp32_44;
    struct s0* rsi45;
    int64_t rdi46;
    void* r14_47;
    int32_t eax48;
    int64_t* rdx49;
    int64_t rdi50;
    int32_t eax51;
    int64_t rdi52;
    int32_t eax53;
    int32_t edx54;
    int64_t v55;
    int64_t v56;
    int64_t rdx57;
    int64_t v58;
    int32_t eax59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t rax63;
    struct s0* rax64;
    struct s0* rdi65;
    struct s0* rax66;
    struct s0* rsi67;
    struct s0* rdi68;
    struct s0* rax69;
    struct s0* rax70;
    uint32_t eax71;
    struct s0* rdx72;
    unsigned char dl73;
    uint32_t tmp32_74;
    int64_t rdi75;
    void* r14_76;
    int32_t eax77;
    void* rax78;
    struct s0* rax79;
    struct s0* r8_80;
    int64_t r9_81;
    uint32_t tmp32_82;
    int64_t rdi83;
    int32_t eax84;
    int64_t rdi85;
    int32_t eax86;
    int64_t v87;
    int64_t v88;
    int64_t v89;
    int64_t v90;
    uint32_t tmp32_91;
    int64_t rdi92;
    int32_t eax93;
    int64_t rdi94;
    int64_t v95;
    int32_t eax96;
    int32_t edx97;
    int64_t v98;
    int64_t rdx99;
    int64_t v100;
    int32_t eax101;
    int64_t v102;
    int64_t v103;
    int64_t v104;
    int64_t rax105;
    int32_t eax106;

    esi3 = edi;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax5 = g28;
    *reinterpret_cast<uint32_t*>(&rax6) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&edi)) {
        *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<uint32_t*>(&rax6) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax6);
    }
    edx7 = argc;
    *reinterpret_cast<uint32_t*>(&rcx8) = static_cast<uint32_t>(rax6 + 1);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    rbp9 = argv;
    *reinterpret_cast<uint32_t*>(&r12_10) = 0;
    if (reinterpret_cast<int32_t>(edx7 - 2) > *reinterpret_cast<int32_t*>(&rcx8)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax6) + 2) * 8))->f2)) {
            *reinterpret_cast<uint32_t*>(&r12_10) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx8);
            *reinterpret_cast<uint32_t*>(&r12_10) = 1;
        }
    }
    rbx11 = *reinterpret_cast<int32_t*>(&rcx8);
    rdi12 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + rbx11 * 8);
    r13_13 = reinterpret_cast<void*>(rbx11 * 8);
    eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi12->f0));
    if (*reinterpret_cast<signed char*>(&eax14) != 45) {
        if (*reinterpret_cast<signed char*>(&eax14) != 61 || (eax15 = rdi12->f1, !!*reinterpret_cast<signed char*>(&eax15)) && (*reinterpret_cast<signed char*>(&eax15) != 61 || rdi12->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi12->f0) == 33) || (rdi12->f1 != 61 || rdi12->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax16 = reinterpret_cast<int32_t>(pos);
                rbx17 = rax16;
                rax18 = rax16 + 2;
                rsi19 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax18 * 8));
                rdi20 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax18 * 8) - 16);
                eax21 = fun_24f0(rdi20, rsi19);
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!!eax21);
                pos = *reinterpret_cast<int32_t*>(&rbx17) + 3;
                goto addr_2cc9_27;
            }
        } else {
            rax22 = reinterpret_cast<int32_t>(pos);
            rbx23 = rax22;
            rax24 = rax22 + 2;
            rsi25 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax24 * 8));
            rdi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax24 * 8) - 16);
            eax27 = fun_24f0(rdi26, rsi25);
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(eax27 == 0);
            pos = *reinterpret_cast<int32_t*>(&rbx23) + 3;
            goto addr_2cc9_27;
        }
    }
    eax28 = rdi12->f1;
    if ((*reinterpret_cast<signed char*>(&eax28) == 0x6c || *reinterpret_cast<signed char*>(&eax28) == 0x67) && ((edx29 = rdi12->f2, *reinterpret_cast<signed char*>(&edx29) == 0x65) || *reinterpret_cast<signed char*>(&edx29) == 0x74)) {
        if (rdi12->f3) 
            goto addr_2d44_31; else 
            goto addr_2ea4_32;
    }
    if (*reinterpret_cast<signed char*>(&eax28) == 0x65) {
        eax30 = rdi12->f2;
        if (*reinterpret_cast<signed char*>(&eax30) == 0x71) {
            addr_300c_35:
            if (!rdi12->f3) {
                addr_2ea4_32:
                rdi31 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi3)) {
                    rax32 = find_int(rdi31);
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
                    rbp34 = rax32;
                } else {
                    rax35 = fun_2440(rdi31);
                    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
                    rax37 = umaxtostr(rax35, reinterpret_cast<int64_t>(rsp36) + 0x120);
                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                    rbp34 = rax37;
                }
            } else {
                addr_3016_38:
                rax38 = quote(rdi12);
                rax39 = fun_2420();
                rax42 = test_syntax_error(rax39, rax38, 5, rcx8, r8_40, r9_41);
                goto addr_3040_39;
            }
        } else {
            addr_2e0c_40:
            if (*reinterpret_cast<signed char*>(&eax30) != 0x66) 
                goto addr_3016_38;
            if (rdi12->f3) 
                goto addr_3016_38; else 
                goto addr_2e1e_42;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax28) == 0x6e) {
            eax43 = rdi12->f2;
            if (*reinterpret_cast<signed char*>(&eax43) != 0x65) 
                goto addr_2f5a_45; else 
                goto addr_300c_35;
        } else {
            addr_2d44_31:
            if (*reinterpret_cast<signed char*>(&eax28) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax28) > 0x6f) 
                    goto addr_3016_38;
                if (*reinterpret_cast<signed char*>(&eax28) == 0x65) 
                    goto addr_30a8_48; else 
                    goto addr_2f4e_49;
            } else {
                if (rdi12->f2 != 0x74) 
                    goto addr_3016_38;
                if (rdi12->f3) 
                    goto addr_3016_38;
                tmp32_44 = pos + 3;
                pos = tmp32_44;
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
                if (*reinterpret_cast<unsigned char*>(&r12_10)) {
                    rsi45 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_54;
                } else {
                    rdi46 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
                    r14_47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 0x90);
                    eax48 = fun_2500(rdi46, r14_47);
                    rdx49 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8);
                    if (eax48) {
                        rdi50 = *rdx49;
                        eax51 = fun_2500(rdi50, r14_47);
                        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(eax51 == 0);
                        goto addr_2cc9_27;
                    } else {
                        rdi52 = *rdx49;
                        eax53 = fun_2500(rdi52, r14_47);
                        if (!eax53) {
                            edx54 = 0;
                            *reinterpret_cast<unsigned char*>(&edx54) = reinterpret_cast<uint1_t>(v55 > v56);
                            *reinterpret_cast<uint32_t*>(&rdx57) = edx54 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(v55 < v58));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx57) + 4) = 0;
                            eax59 = 0;
                            *reinterpret_cast<unsigned char*>(&eax59) = reinterpret_cast<uint1_t>(v60 > v61);
                            *reinterpret_cast<uint32_t*>(&rcx8) = reinterpret_cast<uint1_t>(v60 < v62);
                            *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rax63) = eax59 - *reinterpret_cast<uint32_t*>(&rcx8);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax63) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&r12_10) = static_cast<uint32_t>(rax63 + rdx57 * 2) >> 31;
                            goto addr_2cc9_27;
                        }
                    }
                }
            }
        }
    }
    rax64 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_10)) {
        rdi65 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax64) + reinterpret_cast<uint64_t>(r13_13) + 8);
        rax66 = find_int(rdi65, rdi65);
        rsi67 = rax66;
    } else {
        rdi68 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax64) + reinterpret_cast<uint64_t>(r13_13) + 16);
        rax69 = fun_2440(rdi68, rdi68);
        rax70 = umaxtostr(rax69, reinterpret_cast<int64_t>(rsp33) - 8 + 8 + 0x140);
        rsi67 = rax70;
    }
    eax71 = strintcmp(rbp34, rsi67);
    rdx72 = argv;
    *reinterpret_cast<uint32_t*>(&rcx8) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx72) + rbx11 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    dl73 = reinterpret_cast<uint1_t>((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx72) + rbx11 * 8))->f2 == 0x65);
    tmp32_74 = pos + 3;
    pos = tmp32_74;
    if (*reinterpret_cast<signed char*>(&rcx8) == 0x6c) {
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(static_cast<uint32_t>(dl73)) > reinterpret_cast<int32_t>(eax71));
        goto addr_2cc9_27;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx8) == 0x67) {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(-static_cast<uint32_t>(dl73)) < reinterpret_cast<int32_t>(eax71));
            goto addr_2cc9_27;
        } else {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!eax71)) == dl73);
            goto addr_2cc9_27;
        }
    }
    addr_3040_39:
    rdi75 = *rax42;
    eax77 = fun_2500(rdi75, r14_76, rdi75, r14_76);
    if (!eax77) {
        addr_2cc9_27:
        rax78 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax78) {
            fun_2450();
        } else {
            return *reinterpret_cast<uint32_t*>(&r12_10);
        }
    } else {
        addr_3053_69:
        *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx11) == 0);
        goto addr_2cc9_27;
    }
    rsi45 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_54:
        rax79 = fun_2420();
        test_syntax_error(rax79, rsi45, 5, rcx8, r8_80, r9_81);
        addr_3102_71:
        rsi45 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_42:
    tmp32_82 = pos + 3;
    pos = tmp32_82;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
    if (!*reinterpret_cast<unsigned char*>(&r12_10)) {
        rdi83 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
        eax84 = fun_2500(rdi83, rsp4);
        if (!eax84 && ((rdi85 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8), eax86 = fun_2500(rdi85, reinterpret_cast<int64_t>(rsp4) - 8 + 8 + 0x90), !eax86) && v87 == v88)) {
            *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(v89 == v90);
            goto addr_2cc9_27;
        }
    }
    addr_2f5a_45:
    if (*reinterpret_cast<signed char*>(&eax43) != 0x74) 
        goto addr_3016_38;
    if (rdi12->f3) 
        goto addr_3016_38;
    tmp32_91 = pos + 3;
    pos = tmp32_91;
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_10) | *reinterpret_cast<unsigned char*>(&esi3));
    if (*reinterpret_cast<unsigned char*>(&r12_10)) 
        goto addr_3102_71;
    rdi92 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) - 8);
    r14_76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 0x90);
    eax93 = fun_2500(rdi92, r14_76);
    *reinterpret_cast<int32_t*>(&rbx11) = eax93;
    rax42 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(r13_13) + 8);
    if (*reinterpret_cast<int32_t*>(&rbx11)) 
        goto addr_3040_39;
    rdi94 = *rax42;
    r12_10 = v95;
    eax96 = fun_2500(rdi94, r14_76);
    if (eax96) 
        goto addr_3053_69;
    edx97 = 0;
    *reinterpret_cast<unsigned char*>(&edx97) = reinterpret_cast<uint1_t>(r12_10 > v98);
    *reinterpret_cast<uint32_t*>(&rdx99) = edx97 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_10 < v100));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx99) + 4) = 0;
    eax101 = 0;
    *reinterpret_cast<unsigned char*>(&eax101) = reinterpret_cast<uint1_t>(v102 > v103);
    *reinterpret_cast<uint32_t*>(&rcx8) = reinterpret_cast<uint1_t>(v102 < v104);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax105) = eax101 - *reinterpret_cast<uint32_t*>(&rcx8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
    eax106 = static_cast<int32_t>(rax105 + rdx99 * 2);
    *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax106 < 0) | reinterpret_cast<uint1_t>(eax106 == 0)));
    goto addr_2cc9_27;
    addr_30a8_48:
    eax30 = rdi12->f2;
    goto addr_2e0c_40;
    addr_2f4e_49:
    if (*reinterpret_cast<signed char*>(&eax28) != 0x6e) 
        goto addr_3016_38;
    eax43 = rdi12->f2;
    goto addr_2f5a_45;
}