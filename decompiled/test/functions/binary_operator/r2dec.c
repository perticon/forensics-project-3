int64_t binary_operator (int64_t arg_8h, int64_t arg1) {
    char * s1;
    int64_t var_8h;
    uint32_t var_sp_8h;
    int64_t var_90h;
    int64_t var_98h;
    uint32_t var_e8h;
    uint32_t var_f0h;
    int64_t var_120h;
    int64_t var_140h;
    int64_t var_158h_2;
    int64_t var_158h;
    rdi = arg1;
    esi = edi;
    rax = *(fs:0x28);
    *((rsp + 0x158)) = rax;
    eax = pos;
    if (dil != 0) {
        goto label_11;
    }
label_3:
    edx = argc;
    ecx = rax + 1;
    rbp = argv;
    r12d = 0;
    edx -= 2;
    if (edx > ecx) {
        eax += 2;
        rax = (int64_t) eax;
        rax = *((rbp + rax*8));
        if (*(rax) == 0x2d) {
            goto label_12;
        }
label_0:
        r12d = 0;
    }
label_1:
    rbx = (int64_t) ecx;
    rdi = *((rbp + rbx*8));
    r13 = rbx*8;
    eax = *(rdi);
    if (al == 0x2d) {
        goto label_13;
    }
    if (al == 0x3d) {
        eax = *((rdi + 1));
        if (al != 0) {
            if (al != 0x3d) {
                goto label_14;
            }
            if (*((rdi + 2)) != 0) {
                goto label_14;
            }
        }
        rax = *(obj.pos);
        rbx = rax;
        rax += 2;
        eax = strcmp (*((rbp + rax*8 - 0x10)), *((rbp + rax*8)));
        r12b = (eax == 0) ? 1 : 0;
        ebx += 3;
        *(obj.pos) = ebx;
        goto label_2;
    }
label_14:
    if (*(rdi) != 0x21) {
        void (*0x2660)() ();
    }
    if (*((rdi + 1)) != 0x3d) {
        void (*0x2660)() ();
    }
    if (*((rdi + 2)) != 0) {
        void (*0x2660)() ();
    }
    rax = *(obj.pos);
    rbx = rax;
    rax += 2;
    eax = strcmp (*((rbp + rax*8 - 0x10)), *((rbp + rax*8)));
    r12b = (eax != 0) ? 1 : 0;
    ebx += 3;
    *(obj.pos) = ebx;
label_2:
    rax = *((rsp + 0x158));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_15;
    }
    eax = r12d;
    return rax;
label_12:
    if (*((rax + 1)) != 0x6c) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
    *(obj.pos) = ecx;
    r12d = 1;
    goto label_1;
label_13:
    eax = *((rdi + 1));
    if (al == 0x6c) {
        goto label_16;
    }
    if (al == 0x67) {
        goto label_16;
    }
label_4:
    if (al == 0x65) {
        goto label_17;
    }
    if (al == 0x6e) {
        goto label_18;
    }
label_5:
    if (al != 0x6f) {
        goto label_19;
    }
    if (*((rdi + 2)) != 0x74) {
        goto label_20;
    }
    if (*((rdi + 3)) != 0) {
        goto label_20;
    }
    *(obj.pos) += 3;
    r12b |= sil;
    if (r12b != 0) {
        goto label_21;
    }
    rdi = *((rbp + r13 - 8));
    r14 = rsp + 0x90;
    rsi = r14;
    eax = stat ();
    rdx = rbp + r13 + 8;
    if (eax != 0) {
        goto label_22;
    }
    rdi = *(rdx);
    rsi = r14;
    rbp = *((rsp + 0xe8));
    rbx = *((rsp + 0xf0));
    eax = stat ();
    if (eax != 0) {
        goto label_2;
    }
    edx = 0;
    al = (rbp < *((rsp + 0xe8))) ? 1 : 0;
    dl = (rbp > *((rsp + 0xe8))) ? 1 : 0;
    eax = (int32_t) al;
    edx -= eax;
    eax = 0;
    cl = (rbx < *((rsp + 0xf0))) ? 1 : 0;
    al = (rbx > *((rsp + 0xf0))) ? 1 : 0;
    ecx = (int32_t) cl;
    eax -= ecx;
    r12d = rax + rdx*2;
    r12d >>= 0x1f;
    goto label_2;
label_11:
    eax++;
    *(obj.pos) = eax;
    goto label_3;
label_17:
    eax = *((rdi + 2));
    if (al == 0x71) {
        goto label_23;
    }
label_9:
    if (al != 0x66) {
        goto label_20;
    }
    if (*((rdi + 3)) != 0) {
        goto label_20;
    }
    *(obj.pos) += 3;
    r12b |= sil;
    if (r12b != 0) {
        goto label_24;
    }
    rdi = *((rbp + r13 - 8));
    rsi = rsp;
    eax = stat ();
    if (eax != 0) {
        goto label_2;
    }
    rdi = *((rbp + r13 + 8));
    rsi = rsp + 0x90;
    eax = stat ();
    if (eax != 0) {
        goto label_2;
    }
    rax = *((rsp + 0x90));
    if (*(rsp) != rax) {
        goto label_2;
    }
    rax = *((rsp + 0x98));
    r12b = (*((rsp + 8)) == rax) ? 1 : 0;
    goto label_2;
label_16:
    edx = *((rdi + 2));
    if (dl == 0x65) {
        goto label_25;
    }
    if (dl != 0x74) {
        goto label_4;
    }
label_25:
    if (*((rdi + 3)) != 0) {
        goto label_5;
    }
label_6:
    rdi = *((rbp + r13 - 8));
    if (sil == 0) {
        goto label_26;
    }
    rax = strlen (rdi);
    rax = umaxtostr (rax, rsp + 0x120, rdx);
label_7:
    rax = argv;
    if (r12b == 0) {
        goto label_27;
    }
    rax = strlen (*((rax + r13 + 0x10)));
    rax = umaxtostr (rax, rsp + 0x140, rdx);
label_8:
    eax = strintcmp (rbp, rax, rdx);
    rdx = argv;
    rcx = *((rdx + rbx*8));
    ecx = *((rcx + 1));
    dl = (*((rcx + 2)) == 0x65) ? 1 : 0;
    *(obj.pos) += 3;
    if (cl == 0x6c) {
        goto label_28;
    }
    if (cl == 0x67) {
        goto label_29;
    }
    al = (eax != 0) ? 1 : 0;
    r12b = (al == dl) ? 1 : 0;
    goto label_2;
    if (al > dl) {
label_19:
        goto label_20;
    }
    if (al == 0x65) {
        goto label_30;
    }
    if (al != 0x6e) {
        goto label_20;
    }
    eax = *((rdi + 2));
    do {
        if (al != 0x74) {
            goto label_20;
        }
        if (*((rdi + 3)) != 0) {
            goto label_20;
        }
        *(obj.pos) += 3;
        r12b |= sil;
        if (r12b != 0) {
            goto label_31;
        }
        rdi = *((rbp + r13 - 8));
        r14 = rsp + 0x90;
        rsi = r14;
        eax = stat ();
        ebx = eax;
        rax = rbp + r13 + 8;
        if (ebx != 0) {
            goto label_32;
        }
        rdi = *(rax);
        rsi = r14;
        r12 = *((rsp + 0xe8));
        rbp = *((rsp + 0xf0));
        eax = stat ();
        if (eax != 0) {
            goto label_33;
        }
        edx = 0;
        al = (r12 < *((rsp + 0xe8))) ? 1 : 0;
        dl = (r12 > *((rsp + 0xe8))) ? 1 : 0;
        eax = (int32_t) al;
        edx -= eax;
        eax = 0;
        cl = (rbp < *((rsp + 0xf0))) ? 1 : 0;
        al = (rbp > *((rsp + 0xf0))) ? 1 : 0;
        ecx = (int32_t) cl;
        eax -= ecx;
        eax = rax + rdx*2;
        r12b = (eax > 0) ? 1 : 0;
        goto label_2;
label_18:
        eax = *((rdi + 2));
    } while (al != 0x65);
label_23:
    if (*((rdi + 3)) == 0) {
        goto label_6;
    }
label_20:
    rax = quote (rdi, rsi, rdx, rcx, r8);
    edx = 5;
    rax = dcgettext (0, "%s: unknown binary operator");
    eax = 0;
    rax = test_syntax_error (rax, rbp, rdx, rcx, r8, r9);
label_32:
    rdi = *(rax);
    rsi = r14;
    eax = stat ();
    if (eax == 0) {
        goto label_2;
    }
label_33:
    r12b = (ebx == 0) ? 1 : 0;
    goto label_2;
label_22:
    rdi = *(rdx);
    rsi = r14;
    eax = stat ();
    r12b = (eax == 0) ? 1 : 0;
    goto label_2;
label_26:
    rax = find_int (rdi);
    goto label_7;
label_27:
    rax = find_int (*((rax + r13 + 8)));
    rsi = rax;
    goto label_8;
label_30:
    eax = *((rdi + 2));
    goto label_9;
label_28:
    edx = (int32_t) dl;
    r12b = (edx > eax) ? 1 : 0;
    goto label_2;
label_29:
    edx = (int32_t) dl;
    edx = -edx;
    r12b = (edx < eax) ? 1 : 0;
    goto label_2;
label_15:
    stack_chk_fail ();
label_24:
    edx = 5;
    do {
label_10:
        rax = dcgettext (0, "-ef does not accept -l");
        eax = 0;
        test_syntax_error (rax, rsi, rdx, rcx, r8, r9);
label_31:
        edx = 5;
        rsi = "-nt does not accept -l";
    } while (1);
label_21:
    edx = 5;
    rsi = "-ot does not accept -l";
    goto label_10;
}