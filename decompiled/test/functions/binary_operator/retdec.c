int64_t binary_operator(uint32_t a1) {
    int64_t v1 = __readfsqword(40); // 0x2bd1
    int32_t v2 = pos; // 0x2be2
    int32_t v3 = v2; // 0x2beb
    if ((char)a1 != 0) {
        // 0x2df0
        v3 = v2 + 1;
        pos = v3;
    }
    int64_t v4 = v3;
    int64_t v5 = v4 + 1; // 0x2bf7
    int64_t v6 = (int64_t)argv; // 0x2bfa
    int32_t v7 = v3; // 0x2c09
    int64_t v8 = 0; // 0x2c09
    if (v3 + 1 < argc - 2) {
        int64_t v9 = *(int64_t *)((0x100000000 * v4 + 0x200000000 >> 29) + v6); // 0x2c10
        v7 = v3;
        v8 = 0;
        if (*(char *)v9 == 45) {
            // 0x2cf8
            v7 = v3;
            v8 = 0;
            if (*(char *)(v9 + 1) == 108) {
                // 0x2d02
                v7 = v3;
                v8 = 0;
                if (*(char *)(v9 + 2) == 0) {
                    int32_t v10 = v5; // 0x2d0c
                    pos = v10;
                    v7 = v10;
                    v8 = 1;
                }
            }
        }
    }
    int64_t v11 = 0x100000000 * v5;
    int64_t v12 = v11 >> 29; // 0x2c24
    int64_t v13 = v12 + v6;
    int64_t v14 = *(int64_t *)v13; // 0x2c24
    char * v15 = (char *)v14; // 0x2c31
    char v16 = *v15; // 0x2c31
    int64_t v17; // 0x2bc0
    char v18; // 0x2bc0
    char v19; // 0x2bc0
    uint64_t v20; // 0x2bc2
    int64_t v21; // 0x30f3
    if (v16 == 45) {
        // 0x2d20
        v20 = (int64_t)a1;
        char v22 = *(char *)(v14 + 1); // 0x2d20
        switch (v22) {
            case 108: {
                goto lab_0x2e88;
            }
            case 103: {
                goto lab_0x2e88;
            }
            case 101: {
                char v23 = *(char *)(v14 + 2); // 0x2e00
                v18 = v23;
                if (v23 == 113) {
                    goto lab_0x300c;
                } else {
                    goto lab_0x2e0c;
                }
            }
            case 110: {
                char v24 = *(char *)(v14 + 2); // 0x3000
                v19 = v24;
                if (v24 != 101) {
                    goto lab_0x2f5a;
                } else {
                    goto lab_0x300c;
                }
            }
            default: {
                if (v22 != 111) {
                    if (v22 > 111) {
                        goto lab_0x3016;
                    } else {
                        if (v22 == 101) {
                            // 0x30a8
                            v18 = *(char *)(v14 + 2);
                            goto lab_0x2e0c;
                        } else {
                            if (v22 != 110) {
                                goto lab_0x3016;
                            } else {
                                // 0x2f56
                                v19 = *(char *)(v14 + 2);
                                goto lab_0x2f5a;
                            }
                        }
                    }
                } else {
                    // 0x2d4c
                    if (*(char *)(v14 + 2) != 116) {
                        goto lab_0x3016;
                    } else {
                        // 0x2d56
                        if (*(char *)(v14 + 3) != 0) {
                            goto lab_0x3016;
                        } else {
                            // 0x2d60
                            pos = v7 + 3;
                            if ((char)(v8 || v20) != 0) {
                                // 0x30f1
                                v21 = function_2420();
                                test_syntax_error((char *)v21);
                                goto lab_0x3102;
                            } else {
                                int64_t v25 = function_2500(); // 0x2d80
                                int64_t v26 = function_2500();
                                if ((int32_t)v25 != 0) {
                                    // 0x3060
                                    v17 = (int32_t)v26 == 0;
                                } else {
                                    // 0x2d92
                                    v17 = (int32_t)v26 != 0 ? v8 | v20 % 256 : 0;
                                }
                                goto lab_0x2cc9;
                            }
                        }
                    }
                }
            }
        }
    } else {
        if (v16 != 61) {
            goto lab_0x2c80;
        } else {
            char v27 = *(char *)(v14 + 1); // 0x2c40
            if (v27 == 0) {
                goto lab_0x2c52;
            } else {
                if (v27 != 61) {
                    goto lab_0x2c80;
                } else {
                    // 0x2c4c
                    if (*(char *)(v14 + 2) != 0) {
                        goto lab_0x2c80;
                    } else {
                        goto lab_0x2c52;
                    }
                }
            }
        }
    }
  lab_0x2e88:;
    char v28 = *(char *)(v14 + 2); // 0x2e88
    if (v28 != 101 == (v28 != 116)) {
        goto lab_0x3016;
    } else {
        // 0x2e9a
        if (*(char *)(v14 + 3) != 0) {
            goto lab_0x3016;
        } else {
            goto lab_0x2ea4;
        }
    }
  lab_0x2c80:
    if (v16 != 33) {
        binary_operator_cold();
    }
    // 0x2c89
    if (*(char *)(v14 + 1) != 61) {
        binary_operator_cold();
    }
    // 0x2c93
    if (*(char *)(v14 + 2) != 0) {
        binary_operator_cold();
    }
    int64_t v29 = function_24f0(); // 0x2cb5
    pos += 3;
    v17 = (int32_t)v29 != 0;
    goto lab_0x2cc9;
  lab_0x3016:
    // 0x3016
    quote(v15);
    test_syntax_error((char *)function_2420());
    int64_t v33 = v11 >> 32; // 0x303e
    int64_t v31 = v8; // 0x303e
    goto lab_0x3040;
  lab_0x300c:
    // 0x300c
    if (*(char *)(v14 + 3) == 0) {
        goto lab_0x2ea4;
    } else {
        goto lab_0x3016;
    }
  lab_0x2e0c:
    // 0x2e0c
    if (v18 != 102) {
        goto lab_0x3016;
    } else {
        // 0x2e14
        if (*(char *)(v14 + 3) != 0) {
            goto lab_0x3016;
        } else {
            // 0x2e1e
            pos = v7 + 3;
            if ((char)(v8 || v20) != 0) {
                // 0x30f1
                v21 = function_2420();
                test_syntax_error((char *)v21);
                goto lab_0x3102;
            } else {
                int64_t v48 = v8 | v20 % 256; // 0x2e25
                v17 = v48;
                if ((int32_t)function_2500() == 0) {
                    // 0x2e43
                    v17 = v48;
                    if (true == (int32_t)function_2500() == 0) {
                        // 0x2e6f
                        v17 = true;
                    }
                }
                goto lab_0x2cc9;
            }
        }
    }
  lab_0x2f5a:;
    // 0x2f5a
    int64_t v32; // 0x2bc0
    if (v19 != 116) {
        goto lab_0x3016;
    } else {
        // 0x2f62
        if (*(char *)(v14 + 3) != 0) {
            goto lab_0x3016;
        } else {
            // 0x2f6c
            pos = v7 + 3;
            if ((char)(v8 || v20) != 0) {
                goto lab_0x3102;
            } else {
                int64_t v49 = function_2500(); // 0x2f8c
                int64_t v50 = v49 & 0xffffffff; // 0x2f91
                v33 = v50;
                v31 = v8 | v20 % 256;
                if ((int32_t)v49 != 0) {
                    goto lab_0x3040;
                } else {
                    // 0x2fa0
                    v32 = v50;
                    if ((int32_t)function_2500() != 0) {
                        goto lab_0x3053;
                    } else {
                        // 0x2fc3
                        int64_t v51; // 0x2bc0
                        v17 = v51 & -256;
                        goto lab_0x2cc9;
                    }
                }
            }
        }
    }
  lab_0x2cc9:
    // 0x2cc9
    if (v1 == __readfsqword(40)) {
        // 0x2ce0
        return v17 & 0xffffffff;
    }
    // 0x30e0
    function_2450();
    // 0x30f1
    v21 = function_2420();
    test_syntax_error((char *)v21);
    goto lab_0x3102;
  lab_0x2c52:;
    int64_t v30 = function_24f0(); // 0x2c6a
    pos = v7 + 3;
    v17 = (int32_t)v30 == 0;
    goto lab_0x2cc9;
  lab_0x3040:
    // 0x3040
    v17 = v31;
    v32 = v33;
    int64_t v34 = v31; // 0x304d
    if ((int32_t)function_2500() == 0) {
        goto lab_0x2cc9;
    } else {
        goto lab_0x3053;
    }
  lab_0x2ea4:;
    char * v35; // 0x2bc0
    if ((char)a1 == 0) {
        // 0x3080
        v35 = find_int((char *)*(int64_t *)(v13 - 8));
    } else {
        int64_t v36 = function_2440(); // 0x2eb2
        int64_t v37; // bp-104, 0x2bc0
        v35 = umaxtostr(v36, (char *)&v37);
    }
    // 0x2eca
    char * v38; // 0x2bc0
    if (v8 == 0) {
        int64_t v39 = *(int64_t *)(v12 + 8 + (int64_t)argv); // 0x3090
        v38 = find_int((char *)v39);
    } else {
        int64_t v40 = function_2440(); // 0x2edf
        int64_t v41; // bp-72, 0x2bc0
        v38 = umaxtostr(v40, (char *)&v41);
    }
    int32_t v42 = strintcmp(v35, v38); // 0x2efa
    int64_t v43 = *(int64_t *)(v12 + (int64_t)argv); // 0x2f06
    char v44 = *(char *)(v43 + 2); // 0x2f0a
    pos += 3;
    switch (*(char *)(v43 + 1)) {
        case 108: {
            int32_t v45 = (int32_t)(v44 == 101) - v42; // 0x30bb
            v17 = v45 < 0 == (v45 & v42) < 0 == (v45 != 0);
            // break -> 0x2cc9
            break;
        }
        case 103: {
            int32_t v46 = v44 == 101; // 0x30d3
            int32_t v47 = v46 - v42; // 0x30d5
            v17 = v47 < 0 != ((v47 ^ v46) & (v42 ^ v46)) < 0;
            // break -> 0x2cc9
            break;
        }
        default: {
            // 0x2f2e
            v17 = v42 != 0 == v44 == 101;
            // break -> 0x2cc9
            break;
        }
    }
    goto lab_0x2cc9;
  lab_0x3053:
    // 0x3053
    v17 = v34 & -256 | (int64_t)((int32_t)v32 == 0);
    goto lab_0x2cc9;
  lab_0x3102:
    // 0x30f1
    v21 = function_2420();
    test_syntax_error((char *)v21);
    goto lab_0x3102;
}