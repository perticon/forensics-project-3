get_mtime (char const *filename, struct timespec *mtime)
{
  struct stat finfo;
  bool ok = (stat (filename, &finfo) == 0);
  if (ok)
    *mtime = get_stat_mtime (&finfo);
  return ok;
}