int64_t* test_syntax_error(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, int64_t r9) {
    int64_t rax7;
    int64_t rax8;
    unsigned char** rax9;
    void* rsp10;
    struct s0* r8_11;
    unsigned char* rsi12;
    int64_t rdx13;
    int64_t rax14;
    struct s0* rdx15;
    void* rdx16;
    struct s1* rax17;
    struct s0* rcx18;
    int32_t ecx19;
    struct s0* rdx20;
    int32_t ecx21;
    struct s0* rax22;
    struct s0* rax23;
    int64_t rdx24;
    struct s0* rax25;
    struct s0* rdi26;
    struct s0* rax27;
    struct s0* rax28;
    struct s0* rdi29;
    int32_t esi30;
    void* rsp31;
    struct s0* rax32;
    int64_t rax33;
    uint32_t edx34;
    struct s0* rcx35;
    struct s0* rbp36;
    int64_t r12_37;
    int64_t rbx38;
    struct s0* rdi39;
    void* r13_40;
    uint32_t eax41;
    uint32_t eax42;
    int64_t rax43;
    int64_t rbx44;
    int64_t rax45;
    struct s0* rsi46;
    struct s0* rdi47;
    int64_t rax48;
    int64_t rbx49;
    int64_t rax50;
    struct s0* rsi51;
    struct s0* rdi52;
    uint32_t eax53;
    uint32_t edx54;
    uint32_t eax55;
    struct s0* rdi56;
    struct s0* rax57;
    void* rsp58;
    struct s0* rbp59;
    struct s0* rax60;
    void* rsp61;
    struct s0* rax62;
    struct s0* rax63;
    struct s0* rax64;
    int64_t* rax65;
    uint32_t eax66;
    uint32_t tmp32_67;
    struct s0* rsi68;
    int64_t rdi69;
    void* r14_70;
    int32_t eax71;
    int64_t* rdx72;
    int64_t rdi73;
    int64_t rdi74;
    int32_t eax75;
    int64_t v76;
    int64_t v77;
    struct s0* rax78;
    struct s0* rdi79;
    struct s0* rax80;
    struct s0* rsi81;
    struct s0* rdi82;
    struct s0* rax83;
    struct s0* rax84;
    struct s0* rdx85;
    uint32_t tmp32_86;
    int64_t rdi87;
    void* r14_88;
    int32_t eax89;
    void* rax90;
    struct s0* rax91;
    uint32_t tmp32_92;
    int64_t rdi93;
    int32_t eax94;
    int64_t rdi95;
    int32_t eax96;
    int64_t v97;
    int64_t v98;
    uint32_t tmp32_99;
    int64_t rdi100;
    int32_t eax101;
    int64_t rdi102;
    int32_t eax103;
    int64_t v104;
    int64_t v105;

    rax7 = rax8;
    if (*reinterpret_cast<signed char*>(&rax7)) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    verror();
    fun_2600(2);
    rax9 = fun_2650(2);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 - 0xd8 - 8 + 8 - 8 + 8 - 8 - 8 + 8);
    r8_11 = reinterpret_cast<struct s0*>(2);
    rsi12 = *rax9;
    while (*reinterpret_cast<uint32_t*>(&rdx13) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8_11->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0, rax14 = rdx13, !!(rsi12[rdx13 * 2] & 1)) {
        r8_11 = reinterpret_cast<struct s0*>(&r8_11->f1);
    }
    if (*reinterpret_cast<signed char*>(&rdx13) == 43) {
        rdx15 = reinterpret_cast<struct s0*>(&r8_11->f1);
        r8_11 = rdx15;
    } else {
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx16) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx16) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax14) == 45);
        rdx15 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdx16) + reinterpret_cast<unsigned char>(r8_11));
    }
    rax17 = reinterpret_cast<struct s1*>(&rdx15->f1);
    *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&rdx15->f0)) - 48;
    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rcx18) <= 9) {
        ecx19 = reinterpret_cast<signed char>(rdx15->f1);
        *reinterpret_cast<int32_t*>(&rdx20) = ecx19;
        if (ecx19 - 48 <= 9) {
            do {
                ecx21 = reinterpret_cast<signed char>(rax17->f1);
                rax17 = reinterpret_cast<struct s1*>(&rax17->f1);
                *reinterpret_cast<int32_t*>(&rdx20) = ecx21;
            } while (ecx21 - 48 <= 9);
        }
        *reinterpret_cast<uint32_t*>(&rcx18) = *reinterpret_cast<unsigned char*>(&rdx20);
        *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
        if (rsi12[reinterpret_cast<unsigned char>(rcx18) * 2] & 1) {
            do {
                *reinterpret_cast<uint32_t*>(&rcx18) = rax17->f1;
                *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                rax17 = reinterpret_cast<struct s1*>(&rax17->f1);
                rdx20 = rcx18;
            } while (rsi12[reinterpret_cast<unsigned char>(rcx18) * 2] & 1);
        }
        if (!*reinterpret_cast<unsigned char*>(&rdx20)) {
            goto 0x3000000008;
        }
    }
    rax22 = quote(2);
    rax23 = fun_2420();
    test_syntax_error(rax23, rax22, 5, rcx18, r8_11, r9);
    rdx24 = reinterpret_cast<int32_t>(argc);
    rax25 = argv;
    rdi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax25) + rdx24 * 8 - 8);
    rax27 = quote(rdi26, rdi26);
    rax28 = fun_2420();
    rdi29 = rax28;
    test_syntax_error(rdi29, rax27, 5, rcx18, r8_11, r9);
    esi30 = *reinterpret_cast<int32_t*>(&rdi29);
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax32 = g28;
    *reinterpret_cast<uint32_t*>(&rax33) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi29)) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<uint32_t*>(&rax33) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax33);
    }
    edx34 = argc;
    *reinterpret_cast<uint32_t*>(&rcx35) = static_cast<uint32_t>(rax33 + 1);
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    rbp36 = argv;
    *reinterpret_cast<int32_t*>(&r12_37) = 0;
    if (reinterpret_cast<int32_t>(edx34 - 2) > *reinterpret_cast<int32_t*>(&rcx35)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax33) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_37) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx35);
            *reinterpret_cast<int32_t*>(&r12_37) = 1;
        }
    }
    rbx38 = *reinterpret_cast<int32_t*>(&rcx35);
    rdi39 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + rbx38 * 8);
    r13_40 = reinterpret_cast<void*>(rbx38 * 8);
    eax41 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi39->f0));
    if (*reinterpret_cast<signed char*>(&eax41) != 45) {
        if (*reinterpret_cast<signed char*>(&eax41) != 61 || (eax42 = rdi39->f1, !!*reinterpret_cast<signed char*>(&eax42)) && (*reinterpret_cast<signed char*>(&eax42) != 61 || rdi39->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi39->f0) == 33) || (rdi39->f1 != 61 || rdi39->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax43 = reinterpret_cast<int32_t>(pos);
                rbx44 = rax43;
                rax45 = rax43 + 2;
                rsi46 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax45 * 8));
                rdi47 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax45 * 8) - 16);
                fun_24f0(rdi47, rsi46);
                pos = *reinterpret_cast<int32_t*>(&rbx44) + 3;
                goto addr_2cc9_47;
            }
        } else {
            rax48 = reinterpret_cast<int32_t>(pos);
            rbx49 = rax48;
            rax50 = rax48 + 2;
            rsi51 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax50 * 8));
            rdi52 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(rax50 * 8) - 16);
            fun_24f0(rdi52, rsi51);
            pos = *reinterpret_cast<int32_t*>(&rbx49) + 3;
            goto addr_2cc9_47;
        }
    }
    eax53 = rdi39->f1;
    if ((*reinterpret_cast<signed char*>(&eax53) == 0x6c || *reinterpret_cast<signed char*>(&eax53) == 0x67) && ((edx54 = rdi39->f2, *reinterpret_cast<signed char*>(&edx54) == 0x65) || *reinterpret_cast<signed char*>(&edx54) == 0x74)) {
        if (rdi39->f3) 
            goto addr_2d44_51; else 
            goto addr_2ea4_52;
    }
    if (*reinterpret_cast<signed char*>(&eax53) == 0x65) {
        eax55 = rdi39->f2;
        if (*reinterpret_cast<signed char*>(&eax55) == 0x71) {
            addr_300c_55:
            if (!rdi39->f3) {
                addr_2ea4_52:
                rdi56 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi30)) {
                    rax57 = find_int(rdi56);
                    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
                    rbp59 = rax57;
                } else {
                    rax60 = fun_2440(rdi56);
                    rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
                    rax62 = umaxtostr(rax60, reinterpret_cast<int64_t>(rsp61) + 0x120);
                    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                    rbp59 = rax62;
                }
            } else {
                addr_3016_58:
                rax63 = quote(rdi39);
                rax64 = fun_2420();
                rax65 = test_syntax_error(rax64, rax63, 5, rcx35, r8_11, r9);
                goto addr_3040_59;
            }
        } else {
            addr_2e0c_60:
            if (*reinterpret_cast<signed char*>(&eax55) != 0x66) 
                goto addr_3016_58;
            if (rdi39->f3) 
                goto addr_3016_58; else 
                goto addr_2e1e_62;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax53) == 0x6e) {
            eax66 = rdi39->f2;
            if (*reinterpret_cast<signed char*>(&eax66) != 0x65) 
                goto addr_2f5a_65; else 
                goto addr_300c_55;
        } else {
            addr_2d44_51:
            if (*reinterpret_cast<signed char*>(&eax53) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax53) > 0x6f) 
                    goto addr_3016_58;
                if (*reinterpret_cast<signed char*>(&eax53) == 0x65) 
                    goto addr_30a8_68; else 
                    goto addr_2f4e_69;
            } else {
                if (rdi39->f2 != 0x74) 
                    goto addr_3016_58;
                if (rdi39->f3) 
                    goto addr_3016_58;
                tmp32_67 = pos + 3;
                pos = tmp32_67;
                *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
                if (*reinterpret_cast<unsigned char*>(&r12_37)) {
                    rsi68 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_74;
                } else {
                    rdi69 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
                    r14_70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) + 0x90);
                    eax71 = fun_2500(rdi69, r14_70);
                    rdx72 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8);
                    if (eax71) {
                        rdi73 = *rdx72;
                        fun_2500(rdi73, r14_70);
                        goto addr_2cc9_47;
                    } else {
                        rdi74 = *rdx72;
                        eax75 = fun_2500(rdi74, r14_70);
                        if (!eax75) {
                            *reinterpret_cast<uint32_t*>(&rcx35) = reinterpret_cast<uint1_t>(v76 < v77);
                            *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
                            goto addr_2cc9_47;
                        }
                    }
                }
            }
        }
    }
    rax78 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_37)) {
        rdi79 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax78) + reinterpret_cast<uint64_t>(r13_40) + 8);
        rax80 = find_int(rdi79, rdi79);
        rsi81 = rax80;
    } else {
        rdi82 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax78) + reinterpret_cast<uint64_t>(r13_40) + 16);
        rax83 = fun_2440(rdi82, rdi82);
        rax84 = umaxtostr(rax83, reinterpret_cast<int64_t>(rsp58) - 8 + 8 + 0x140);
        rsi81 = rax84;
    }
    strintcmp(rbp59, rsi81);
    rdx85 = argv;
    *reinterpret_cast<uint32_t*>(&rcx35) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx85) + rbx38 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    tmp32_86 = pos + 3;
    pos = tmp32_86;
    if (*reinterpret_cast<signed char*>(&rcx35) == 0x6c) {
        goto addr_2cc9_47;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx35) == 0x67) {
            goto addr_2cc9_47;
        } else {
            goto addr_2cc9_47;
        }
    }
    addr_3040_59:
    rdi87 = *rax65;
    eax89 = fun_2500(rdi87, r14_88, rdi87, r14_88);
    if (!eax89) {
        addr_2cc9_47:
        rax90 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax32) - reinterpret_cast<unsigned char>(g28));
        if (rax90) {
            fun_2450();
        } else {
            goto rax22;
        }
    } else {
        addr_3053_89:
        goto addr_2cc9_47;
    }
    rsi68 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_74:
        rax91 = fun_2420();
        test_syntax_error(rax91, rsi68, 5, rcx35, r8_11, r9);
        addr_3102_91:
        rsi68 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_62:
    tmp32_92 = pos + 3;
    pos = tmp32_92;
    *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
    if (!*reinterpret_cast<unsigned char*>(&r12_37)) {
        rdi93 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
        eax94 = fun_2500(rdi93, rsp31);
        if (!eax94 && ((rdi95 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8), eax96 = fun_2500(rdi95, reinterpret_cast<int64_t>(rsp31) - 8 + 8 + 0x90), !eax96) && v97 == v98)) {
            goto addr_2cc9_47;
        }
    }
    addr_2f5a_65:
    if (*reinterpret_cast<signed char*>(&eax66) != 0x74) 
        goto addr_3016_58;
    if (rdi39->f3) 
        goto addr_3016_58;
    tmp32_99 = pos + 3;
    pos = tmp32_99;
    *reinterpret_cast<unsigned char*>(&r12_37) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_37) | *reinterpret_cast<unsigned char*>(&esi30));
    if (*reinterpret_cast<unsigned char*>(&r12_37)) 
        goto addr_3102_91;
    rdi100 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) - 8);
    r14_88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) + 0x90);
    eax101 = fun_2500(rdi100, r14_88);
    rax65 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp36) + reinterpret_cast<uint64_t>(r13_40) + 8);
    if (eax101) 
        goto addr_3040_59;
    rdi102 = *rax65;
    eax103 = fun_2500(rdi102, r14_88);
    if (eax103) 
        goto addr_3053_89;
    *reinterpret_cast<uint32_t*>(&rcx35) = reinterpret_cast<uint1_t>(v104 < v105);
    *reinterpret_cast<int32_t*>(&rcx35 + 4) = 0;
    goto addr_2cc9_47;
    addr_30a8_68:
    eax55 = rdi39->f2;
    goto addr_2e0c_60;
    addr_2f4e_69:
    if (*reinterpret_cast<signed char*>(&eax53) != 0x6e) 
        goto addr_3016_58;
    eax66 = rdi39->f2;
    goto addr_2f5a_65;
}