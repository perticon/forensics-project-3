void test_syntax_error(char * format, ...) {
    // 0x29f0
    int64_t v1; // 0x29f0
    if ((char)v1 != 0) {
        // 0x2a19
        int128_t v2; // 0x29f0
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
        __asm_movaps(v2);
    }
    // 0x2a50
    __readfsqword(40);
    int64_t v3 = 8; // bp-216, 0x2a7c
    verror(0, 0, format, (int32_t *)&v3);
    function_2600();
}