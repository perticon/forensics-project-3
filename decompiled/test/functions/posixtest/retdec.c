int64_t posixtest(int64_t a1) {
    int32_t v1 = a1; // 0x3dc4
    if (v1 == 3) {
        // 0x3e80
        return three_arguments();
    }
    int32_t v2; // 0x3dc0
    if (v1 > 3) {
        if (v1 != 4) {
            // 0x3e60
            v2 = pos;
            goto lab_0x3e66;
        } else {
            int32_t v3 = pos; // 0x3df5
            int64_t v4 = 8 * (int64_t)v3 + (int64_t)argv;
            int64_t v5 = *(int64_t *)v4; // 0x3e0e
            char v6 = *(char *)v5; // 0x3e12
            if (v6 == 33) {
                // 0x3ec0
                v2 = v3;
                if (*(char *)(v5 + 1) == 0) {
                    int32_t v7 = v3 + 1; // 0x3eca
                    pos = v7;
                    if (argc > v7) {
                        // 0x3eb1
                        return three_arguments() ? 0xfffffffe : 1;
                    }
                    // 0x3ee5
                    beyond();
                    return &g35;
                }
            } else {
                // 0x3e1e
                v2 = v3;
                if (v6 == 40) {
                    // 0x3e23
                    v2 = v3;
                    if (*(char *)(v5 + 1) == 0) {
                        int64_t v8 = *(int64_t *)(v4 + 24); // 0x3e29
                        v2 = v3;
                        if (*(char *)v8 == 41) {
                            // 0x3e33
                            v2 = v3;
                            if (*(char *)(v8 + 1) == 0) {
                                // 0x3e39
                                pos = v3 + 1;
                                bool result = two_arguments(); // 0x3e42
                                pos++;
                                // 0x3eb1
                                return result;
                            }
                        }
                    }
                }
            }
            goto lab_0x3e66;
        }
    } else {
        if (v1 == 1) {
            int32_t v9 = pos; // 0x3e90
            pos = v9 + 1;
            int64_t v10 = *(int64_t *)(8 * (int64_t)v9 + (int64_t)argv); // 0x3ea7
            // 0x3eb1
            return v10 & -256 | (int64_t)(*(char *)v10 != 0);
        }
        if (v1 == 2) {
            // 0x3ddd
            return two_arguments();
        }
        // 0x3e50
        posixtest_cold();
        // 0x3e60
        v2 = pos;
        goto lab_0x3e66;
    }
  lab_0x3e66:
    // 0x3e66
    if (argc > v2) {
        // 0x3e6e
        return or();
    }
    // 0x3ee5
    beyond();
    return &g35;
}