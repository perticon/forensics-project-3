posixtest (int nargs)
{
  bool value;

  switch (nargs)
    {
      case 1:
        value = one_argument ();
        break;

      case 2:
        value = two_arguments ();
        break;

      case 3:
        value = three_arguments ();
        break;

      case 4:
        if (STREQ (argv[pos], "!"))
          {
            advance (true);
            value = !three_arguments ();
            break;
          }
        if (STREQ (argv[pos], "(") && STREQ (argv[pos + 3], ")"))
          {
            advance (false);
            value = two_arguments ();
            advance (false);
            break;
          }
        FALLTHROUGH;
      case 5:
      default:
        if (nargs <= 0)
          abort ();
        value = expr ();
    }

  return (value);
}