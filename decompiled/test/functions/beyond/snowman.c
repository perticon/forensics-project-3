void beyond() {
    int64_t rdx1;
    struct s0* rax2;
    struct s0* rdi3;
    struct s0* rax4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s0* rcx7;
    struct s0* r8_8;
    int64_t r9_9;
    int32_t esi10;
    void* rsp11;
    struct s0* rax12;
    int64_t rax13;
    uint32_t edx14;
    struct s0* rcx15;
    struct s0* rbp16;
    int64_t r12_17;
    int64_t rbx18;
    struct s0* rdi19;
    void* r13_20;
    uint32_t eax21;
    uint32_t eax22;
    int64_t rax23;
    int64_t rbx24;
    int64_t rax25;
    struct s0* rsi26;
    struct s0* rdi27;
    int64_t rax28;
    int64_t rbx29;
    int64_t rax30;
    struct s0* rsi31;
    struct s0* rdi32;
    uint32_t eax33;
    uint32_t edx34;
    uint32_t eax35;
    struct s0* rdi36;
    struct s0* rax37;
    void* rsp38;
    struct s0* rbp39;
    struct s0* rax40;
    void* rsp41;
    struct s0* rax42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* r8_45;
    int64_t r9_46;
    int64_t* rax47;
    uint32_t eax48;
    uint32_t tmp32_49;
    struct s0* rsi50;
    int64_t rdi51;
    void* r14_52;
    int32_t eax53;
    int64_t* rdx54;
    int64_t rdi55;
    int64_t rdi56;
    int32_t eax57;
    int64_t v58;
    int64_t v59;
    struct s0* rax60;
    struct s0* rdi61;
    struct s0* rax62;
    struct s0* rsi63;
    struct s0* rdi64;
    struct s0* rax65;
    struct s0* rax66;
    struct s0* rdx67;
    uint32_t tmp32_68;
    int64_t rdi69;
    void* r14_70;
    int32_t eax71;
    void* rax72;
    int64_t rbp73;
    struct s0* rax74;
    struct s0* r8_75;
    int64_t r9_76;
    uint32_t tmp32_77;
    int64_t rdi78;
    int32_t eax79;
    int64_t rdi80;
    int32_t eax81;
    int64_t v82;
    int64_t v83;
    uint32_t tmp32_84;
    int64_t rdi85;
    int32_t eax86;
    int64_t rdi87;
    int32_t eax88;
    int64_t v89;
    int64_t v90;

    rdx1 = reinterpret_cast<int32_t>(argc);
    rax2 = argv;
    rdi3 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax2) + rdx1 * 8 - 8);
    rax4 = quote(rdi3);
    rax5 = fun_2420();
    rdi6 = rax5;
    test_syntax_error(rdi6, rax4, 5, rcx7, r8_8, r9_9);
    esi10 = *reinterpret_cast<int32_t*>(&rdi6);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax12 = g28;
    *reinterpret_cast<uint32_t*>(&rax13) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi6)) {
        *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax13) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax13);
    }
    edx14 = argc;
    *reinterpret_cast<uint32_t*>(&rcx15) = static_cast<uint32_t>(rax13 + 1);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    rbp16 = argv;
    *reinterpret_cast<int32_t*>(&r12_17) = 0;
    if (reinterpret_cast<int32_t>(edx14 - 2) > *reinterpret_cast<int32_t*>(&rcx15)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax13) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx15);
            *reinterpret_cast<int32_t*>(&r12_17) = 1;
        }
    }
    rbx18 = *reinterpret_cast<int32_t*>(&rcx15);
    rdi19 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + rbx18 * 8);
    r13_20 = reinterpret_cast<void*>(rbx18 * 8);
    eax21 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi19->f0));
    if (*reinterpret_cast<signed char*>(&eax21) != 45) {
        if (*reinterpret_cast<signed char*>(&eax21) != 61 || (eax22 = rdi19->f1, !!*reinterpret_cast<signed char*>(&eax22)) && (*reinterpret_cast<signed char*>(&eax22) != 61 || rdi19->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi19->f0) == 33) || (rdi19->f1 != 61 || rdi19->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax23 = reinterpret_cast<int32_t>(pos);
                rbx24 = rax23;
                rax25 = rax23 + 2;
                rsi26 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax25 * 8));
                rdi27 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax25 * 8) - 16);
                fun_24f0(rdi27, rsi26);
                pos = *reinterpret_cast<int32_t*>(&rbx24) + 3;
                goto addr_2cc9_28;
            }
        } else {
            rax28 = reinterpret_cast<int32_t>(pos);
            rbx29 = rax28;
            rax30 = rax28 + 2;
            rsi31 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax30 * 8));
            rdi32 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(rax30 * 8) - 16);
            fun_24f0(rdi32, rsi31);
            pos = *reinterpret_cast<int32_t*>(&rbx29) + 3;
            goto addr_2cc9_28;
        }
    }
    eax33 = rdi19->f1;
    if ((*reinterpret_cast<signed char*>(&eax33) == 0x6c || *reinterpret_cast<signed char*>(&eax33) == 0x67) && ((edx34 = rdi19->f2, *reinterpret_cast<signed char*>(&edx34) == 0x65) || *reinterpret_cast<signed char*>(&edx34) == 0x74)) {
        if (rdi19->f3) 
            goto addr_2d44_32; else 
            goto addr_2ea4_33;
    }
    if (*reinterpret_cast<signed char*>(&eax33) == 0x65) {
        eax35 = rdi19->f2;
        if (*reinterpret_cast<signed char*>(&eax35) == 0x71) {
            addr_300c_36:
            if (!rdi19->f3) {
                addr_2ea4_33:
                rdi36 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi10)) {
                    rax37 = find_int(rdi36);
                    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    rbp39 = rax37;
                } else {
                    rax40 = fun_2440(rdi36);
                    rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    rax42 = umaxtostr(rax40, reinterpret_cast<int64_t>(rsp41) + 0x120);
                    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                    rbp39 = rax42;
                }
            } else {
                addr_3016_39:
                rax43 = quote(rdi19);
                rax44 = fun_2420();
                rax47 = test_syntax_error(rax44, rax43, 5, rcx15, r8_45, r9_46);
                goto addr_3040_40;
            }
        } else {
            addr_2e0c_41:
            if (*reinterpret_cast<signed char*>(&eax35) != 0x66) 
                goto addr_3016_39;
            if (rdi19->f3) 
                goto addr_3016_39; else 
                goto addr_2e1e_43;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax33) == 0x6e) {
            eax48 = rdi19->f2;
            if (*reinterpret_cast<signed char*>(&eax48) != 0x65) 
                goto addr_2f5a_46; else 
                goto addr_300c_36;
        } else {
            addr_2d44_32:
            if (*reinterpret_cast<signed char*>(&eax33) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax33) > 0x6f) 
                    goto addr_3016_39;
                if (*reinterpret_cast<signed char*>(&eax33) == 0x65) 
                    goto addr_30a8_49; else 
                    goto addr_2f4e_50;
            } else {
                if (rdi19->f2 != 0x74) 
                    goto addr_3016_39;
                if (rdi19->f3) 
                    goto addr_3016_39;
                tmp32_49 = pos + 3;
                pos = tmp32_49;
                *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
                if (*reinterpret_cast<unsigned char*>(&r12_17)) {
                    rsi50 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_55;
                } else {
                    rdi51 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
                    r14_52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x90);
                    eax53 = fun_2500(rdi51, r14_52);
                    rdx54 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8);
                    if (eax53) {
                        rdi55 = *rdx54;
                        fun_2500(rdi55, r14_52);
                        goto addr_2cc9_28;
                    } else {
                        rdi56 = *rdx54;
                        eax57 = fun_2500(rdi56, r14_52);
                        if (!eax57) {
                            *reinterpret_cast<uint32_t*>(&rcx15) = reinterpret_cast<uint1_t>(v58 < v59);
                            *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
                            goto addr_2cc9_28;
                        }
                    }
                }
            }
        }
    }
    rax60 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_17)) {
        rdi61 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<uint64_t>(r13_20) + 8);
        rax62 = find_int(rdi61, rdi61);
        rsi63 = rax62;
    } else {
        rdi64 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<uint64_t>(r13_20) + 16);
        rax65 = fun_2440(rdi64, rdi64);
        rax66 = umaxtostr(rax65, reinterpret_cast<int64_t>(rsp38) - 8 + 8 + 0x140);
        rsi63 = rax66;
    }
    strintcmp(rbp39, rsi63);
    rdx67 = argv;
    *reinterpret_cast<uint32_t*>(&rcx15) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx67) + rbx18 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    tmp32_68 = pos + 3;
    pos = tmp32_68;
    if (*reinterpret_cast<signed char*>(&rcx15) == 0x6c) {
        goto addr_2cc9_28;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx15) == 0x67) {
            goto addr_2cc9_28;
        } else {
            goto addr_2cc9_28;
        }
    }
    addr_3040_40:
    rdi69 = *rax47;
    eax71 = fun_2500(rdi69, r14_70, rdi69, r14_70);
    if (!eax71) {
        addr_2cc9_28:
        rax72 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax12) - reinterpret_cast<unsigned char>(g28));
        if (rax72) {
            fun_2450();
        } else {
            goto rbp73;
        }
    } else {
        addr_3053_70:
        goto addr_2cc9_28;
    }
    rsi50 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_55:
        rax74 = fun_2420();
        test_syntax_error(rax74, rsi50, 5, rcx15, r8_75, r9_76);
        addr_3102_72:
        rsi50 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_43:
    tmp32_77 = pos + 3;
    pos = tmp32_77;
    *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
    if (!*reinterpret_cast<unsigned char*>(&r12_17)) {
        rdi78 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
        eax79 = fun_2500(rdi78, rsp11);
        if (!eax79 && ((rdi80 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8), eax81 = fun_2500(rdi80, reinterpret_cast<int64_t>(rsp11) - 8 + 8 + 0x90), !eax81) && v82 == v83)) {
            goto addr_2cc9_28;
        }
    }
    addr_2f5a_46:
    if (*reinterpret_cast<signed char*>(&eax48) != 0x74) 
        goto addr_3016_39;
    if (rdi19->f3) 
        goto addr_3016_39;
    tmp32_84 = pos + 3;
    pos = tmp32_84;
    *reinterpret_cast<unsigned char*>(&r12_17) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_17) | *reinterpret_cast<unsigned char*>(&esi10));
    if (*reinterpret_cast<unsigned char*>(&r12_17)) 
        goto addr_3102_72;
    rdi85 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) - 8);
    r14_70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x90);
    eax86 = fun_2500(rdi85, r14_70);
    rax47 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<uint64_t>(r13_20) + 8);
    if (eax86) 
        goto addr_3040_40;
    rdi87 = *rax47;
    eax88 = fun_2500(rdi87, r14_70);
    if (eax88) 
        goto addr_3053_70;
    *reinterpret_cast<uint32_t*>(&rcx15) = reinterpret_cast<uint1_t>(v89 < v90);
    *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
    goto addr_2cc9_28;
    addr_30a8_49:
    eax35 = rdi19->f2;
    goto addr_2e0c_41;
    addr_2f4e_50:
    if (*reinterpret_cast<signed char*>(&eax33) != 0x6e) 
        goto addr_3016_39;
    eax48 = rdi19->f2;
    goto addr_2f5a_46;
}