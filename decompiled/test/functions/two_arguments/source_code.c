two_arguments (void)
{
  bool value;

  if (STREQ (argv[pos], "!"))
    {
      advance (false);
      value = ! one_argument ();
    }
  else if (argv[pos][0] == '-'
           && argv[pos][1] != '\0'
           && argv[pos][2] == '\0')
    {
      value = unary_operator ();
    }
  else
    beyond ();
  return (value);
}