bool two_arguments(void) {
    int64_t v1 = 8 * (int64_t)pos + (int64_t)argv;
    int64_t v2 = *(int64_t *)v1; // 0x38b9
    char v3 = *(char *)v2; // 0x38bd
    if (v3 == 33) {
        // 0x38c2
        if (*(char *)(v2 + 1) == 0) {
            // 0x38c8
            pos += 2;
            return *(char *)*(int64_t *)(v1 + 8) == 0;
        }
    }
    if (v3 != 45 || *(char *)(v2 + 1) == 0) {
        // 0x38f6
        beyond();
        return false;
    }
    // 0x38eb
    if (*(char *)(v2 + 2) == 0) {
        // 0x38f1
        return unary_operator();
    }
    // 0x38f6
    beyond();
    return false;
}