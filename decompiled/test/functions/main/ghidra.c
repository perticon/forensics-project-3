byte main(int param_1,undefined8 *param_2)

{
  byte bVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  exit_failure = 2;
  atexit(close_stdout);
  pos = 1;
  argv = param_2;
  argc = param_1;
  if (param_1 < 2) {
    bVar1 = 1;
  }
  else {
    bVar1 = posixtest(param_1 + -1);
    if (pos != argc) {
      uVar2 = quote(argv[pos]);
      uVar3 = dcgettext(0,"extra argument %s",5);
                    /* WARNING: Subroutine does not return */
      test_syntax_error(uVar3,uVar2);
    }
    bVar1 = bVar1 ^ 1;
  }
  return bVar1;
}