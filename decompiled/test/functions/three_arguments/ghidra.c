ulong three_arguments(void)

{
  long lVar1;
  char *pcVar2;
  char *pcVar3;
  long lVar4;
  int iVar5;
  char cVar6;
  uint uVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  ulong uVar10;
  
  iVar5 = pos;
  lVar4 = argv;
  pcVar2 = *(char **)(argv + ((long)pos + 1) * 8);
  lVar1 = ((long)pos + 1) * 8;
  cVar6 = binop(pcVar2);
  if (cVar6 != '\0') {
    uVar10 = binary_operator(0);
    return uVar10;
  }
  pcVar3 = *(char **)(lVar4 + -8 + lVar1);
  if ((*pcVar3 == '!') && (pcVar3[1] == '\0')) {
    pos = iVar5 + 1;
    if (pos < argc) {
      uVar7 = two_arguments();
      return (ulong)(uVar7 ^ 1);
    }
  }
  else {
    if ((*pcVar3 == '(') &&
       (((pcVar3[1] == '\0' && (pcVar3 = *(char **)(lVar4 + 8 + lVar1), *pcVar3 == ')')) &&
        (pcVar3[1] == '\0')))) {
      pos = iVar5 + 3;
      return (ulong)pcVar3 & 0xffffffffffffff00 | (ulong)(*pcVar2 != '\0');
    }
    if ((*pcVar2 != '-') ||
       (((pcVar2[1] != 'a' || (pcVar2[2] != '\0')) && ((pcVar2[1] != 'o' || (pcVar2[2] != '\0'))))))
    {
      uVar8 = quote(pcVar2);
      uVar9 = dcgettext(0,"%s: binary operator expected",5);
                    /* WARNING: Subroutine does not return */
      test_syntax_error(uVar9,uVar8);
    }
    if (iVar5 < argc) {
      uVar10 = or();
      return uVar10;
    }
  }
                    /* WARNING: Subroutine does not return */
  beyond();
}