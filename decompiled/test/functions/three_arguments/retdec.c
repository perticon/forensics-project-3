bool three_arguments(void) {
    int32_t v1 = pos; // 0x3c8a
    int64_t v2 = 8 * (int64_t)v1 + (int64_t)argv;
    int64_t v3 = *(int64_t *)(v2 + 8); // 0x3c9f
    char * v4 = (char *)v3; // 0x3cae
    if (binop(v4)) {
        // 0x3d78
        return binary_operator(0) % 2 != 0;
    }
    int64_t v5 = *(int64_t *)v2; // 0x3cbb
    char v6 = *(char *)v5; // 0x3cc0
    if (v6 == 33) {
        // 0x3cc8
        if (*(char *)(v5 + 1) == 0) {
            int32_t v7 = v1 + 1; // 0x3cce
            pos = v7;
            if (argc > v7) {
                // 0x3ceb
                return (two_arguments() ? 0xfffffffe : 1) % 2 != 0;
            }
            // 0x3db8
            beyond();
            return false;
        }
    }
    if (v6 == 40) {
        // 0x3d05
        if (*(char *)(v5 + 1) == 0) {
            int64_t v8 = *(int64_t *)(v2 + 16); // 0x3d0b
            if (*(char *)v8 == 41) {
                // 0x3d15
                if (*(char *)(v8 + 1) == 0) {
                    // 0x3d1b
                    pos = v1 + 3;
                    // 0x3ceb
                    return (int64_t)(*v4 != 0) % 2 != 0;
                }
            }
        }
    }
    // 0x3d30
    int64_t v9; // 0x3d63
    if (*v4 != 45) {
        // 0x3d4a
        quote(v4);
        v9 = function_2420();
        test_syntax_error((char *)v9);
        // 0x3d78
        return binary_operator(0) % 2 != 0;
    }
    // 0x3d39
    switch (*(char *)(v3 + 1)) {
        case 97: {
            // 0x3d90
            if (*(char *)(v3 + 2) != 0) {
                // 0x3d4a
                quote(v4);
                v9 = function_2420();
                test_syntax_error((char *)v9);
                // 0x3d78
                return binary_operator(0) % 2 != 0;
            }
            // break -> 0x3d96
            break;
        }
        case 111: {
            // 0x3db0
            if (*(char *)(v3 + 2) != 0) {
                // 0x3d4a
                quote(v4);
                v9 = function_2420();
                test_syntax_error((char *)v9);
                // 0x3d78
                return binary_operator(0) % 2 != 0;
            }
            // break -> 0x3d96
            break;
        }
        default: {
            // 0x3d4a
            quote(v4);
            v9 = function_2420();
            test_syntax_error((char *)v9);
            // 0x3d78
            return binary_operator(0) % 2 != 0;
        }
    }
    // 0x3d96
    if (argc > v1) {
        // 0x3d9e
        return or();
    }
    // 0x3db8
    beyond();
    return false;
}