ulong unary_operator(void)

{
  uint uVar1;
  long lVar2;
  int iVar3;
  __gid_t _Var4;
  __uid_t _Var5;
  undefined8 uVar6;
  undefined8 uVar7;
  ulong uVar8;
  undefined4 extraout_var;
  undefined4 extraout_var_00;
  undefined4 extraout_var_01;
  char *pcVar9;
  int *piVar10;
  ulong uVar11;
  undefined4 extraout_var_02;
  undefined4 extraout_var_03;
  undefined4 extraout_var_04;
  undefined4 extraout_var_05;
  long in_FS_OFFSET;
  stat sStack184;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  lVar2 = (long)pos * 8;
  switch(*(undefined *)(*(long *)(argv + (long)pos * 8) + 1)) {
  case 0x47:
    if (argc <= pos + 1) {
LAB_0010388e:
      pos = pos + 1;
                    /* WARNING: Subroutine does not return */
      beyond();
    }
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    if (iVar3 == 0) {
      piVar10 = __errno_location();
      *piVar10 = 0;
      _Var4 = getegid();
      uVar11 = CONCAT44(extraout_var_03,_Var4);
      if (_Var4 == 0xffffffff) {
        uVar11 = 0;
        uVar8 = 0;
        if (*piVar10 != 0) break;
      }
      uVar8 = uVar11 & 0xffffffffffffff00 | (ulong)(sStack184.st_gid == _Var4);
      break;
    }
    goto LAB_00103868;
  default:
    uVar6 = quote();
    uVar7 = dcgettext(0,"%s: unary operator expected",5);
                    /* WARNING: Subroutine does not return */
    test_syntax_error(uVar7,uVar6);
  case 0x4c:
  case 0x68:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = FUN_001024b0(*(undefined8 *)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0xa000));
    }
    break;
  case 0x4e:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    if (iVar3 == 0) {
      uVar1 = ((uint)(sStack184.st_atim.tv_nsec < sStack184.st_mtim.tv_nsec) -
              (uint)(sStack184.st_mtim.tv_nsec < sStack184.st_atim.tv_nsec)) +
              ((uint)(sStack184.st_atim.tv_sec < sStack184.st_mtim.tv_sec) -
              (uint)(sStack184.st_mtim.tv_sec < sStack184.st_atim.tv_sec)) * 2;
      uVar8 = (ulong)(uVar1 & 0xffffff00 | (uint)(0 < (int)uVar1));
      break;
    }
    goto LAB_00103868;
  case 0x4f:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    if (iVar3 == 0) {
      piVar10 = __errno_location();
      *piVar10 = 0;
      _Var5 = geteuid();
      uVar11 = CONCAT44(extraout_var_04,_Var5);
      if (_Var5 == 0xffffffff) {
        uVar11 = 0;
        uVar8 = 0;
        if (*piVar10 != 0) break;
      }
      uVar8 = uVar11 & 0xffffffffffffff00 | (ulong)(sStack184.st_uid == _Var5);
      break;
    }
LAB_00103868:
    uVar8 = 0;
    break;
  case 0x53:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0xc000));
    }
    break;
  case 0x62:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x6000));
    }
    break;
  case 99:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x2000));
    }
    break;
  case 100:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x4000));
    }
    break;
  case 0x65:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = CONCAT44(extraout_var_00,iVar3) & 0xffffffffffffff00 | (ulong)(iVar3 == 0);
    break;
  case 0x66:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x8000));
    }
    break;
  case 0x67:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + -8 + (long)pos * 8),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode >> 10 & 1);
    }
    break;
  case 0x6b:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + 8 + lVar2),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode >> 9 & 1);
    }
    break;
  case 0x6e:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    pcVar9 = *(char **)(argv + 8 + lVar2);
    uVar8 = (ulong)pcVar9 & 0xffffffffffffff00 | (ulong)(*pcVar9 != '\0');
    break;
  case 0x70:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + 8 + lVar2),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x1000));
    }
    break;
  case 0x72:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = euidaccess(*(char **)(argv + 8 + lVar2),4);
    uVar8 = CONCAT44(extraout_var_01,iVar3) & 0xffffffffffffff00 | (ulong)(iVar3 == 0);
    break;
  case 0x73:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + 8 + lVar2),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(0 < sStack184.st_size);
    }
    break;
  case 0x74:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    pcVar9 = (char *)find_int(*(undefined8 *)(argv + 8 + lVar2));
    piVar10 = __errno_location();
    *piVar10 = 0;
    uVar11 = strtol(pcVar9,(char **)0x0,10);
    uVar8 = 0;
    if ((*piVar10 != 0x22) && (uVar8 = 0, uVar11 < 0x80000000)) {
      iVar3 = isatty((int)uVar11);
      uVar8 = CONCAT44(extraout_var_02,iVar3) & 0xffffffffffffff00 | (ulong)(iVar3 != 0);
    }
    break;
  case 0x75:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = stat(*(char **)(argv + 8 + lVar2),&sStack184);
    uVar8 = 0;
    if (iVar3 == 0) {
      uVar8 = (ulong)(sStack184.st_mode >> 0xb & 1);
    }
    break;
  case 0x77:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = euidaccess(*(char **)(argv + 8 + lVar2),2);
    uVar8 = CONCAT44(extraout_var_05,iVar3) & 0xffffffffffffff00 | (ulong)(iVar3 == 0);
    break;
  case 0x78:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    iVar3 = euidaccess(*(char **)(argv + 8 + lVar2),1);
    uVar8 = CONCAT44(extraout_var,iVar3) & 0xffffffffffffff00 | (ulong)(iVar3 == 0);
    break;
  case 0x7a:
    if (argc <= pos + 1) goto LAB_0010388e;
    pos = pos + 2;
    pcVar9 = *(char **)(argv + 8 + lVar2);
    uVar8 = (ulong)pcVar9 & 0xffffffffffffff00 | (ulong)(*pcVar9 == '\0');
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}