word32 unary_operator(struct Eq_780 * fs)
{
	ui32 dwLocA0;
	word64 rax_14 = fs->qw0028;
	cu8 dl_25 = (&argv[(int64) g_dwF0BC * 0x08 /64 24].a0000[0])[1] - 0x47;
	if (dl_25 > 0x33)
	{
		quote();
		fn0000000000002420(0x05, "%s: unary operator expected", null);
		word64 rdx_162;
		word64 r8_163;
		union Eq_12 * rax_54 = test_syntax_error(0x00, out rdx_162, out r8_163);
		int32 edx_61 = (word32) rax_54 + 0x01;
		g_dwF0BC = edx_61;
		word32 eax_72 = (word32) rax_54;
		if (edx_61 < g_dwF0B8)
		{
			g_dwF0BC = eax_72 + 0x02;
			fn00000000000024B0();
			uint64 rax_141 = 0x00;
			if (eax_72 == 0x02)
			{
				uint64 rax_94 = (uint64) (dwLocA0 & 0xF000);
				rax_141 = SEQ(SLICE(rax_94, word56, 8), (int8) ((word32) rax_94 == 0xA000));
			}
			if (rax_14 - fs->qw0028 == 0x00)
				return (word32) rax_141;
		}
		else
		{
			word32 edi_167;
			word32 edx_166;
			word32 ecx_165;
			word64 rax_164;
			word64 r8_168;
			beyond(out rax_164, out ecx_165, out edx_166, out edi_167, out r8_168);
		}
		fn0000000000002450();
	}
	else
	{
		<anonymous> * rdx_116 = (int64) g_aA1F0[(uint64) dl_25 * 0x04] + 0xA1F0;
		word64 rax_120;
		rdx_116();
		return (word32) rax_120;
	}
}