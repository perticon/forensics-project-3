uint32_t unary_operator(struct s0* rdi, struct s0* rsi) {
    int64_t rdx3;
    struct s0* r8_4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s0* rbx7;
    int32_t edx8;
    struct s0* rax9;
    struct s0* rax10;
    struct s0* rdi11;
    struct s0* rcx12;
    int64_t r9_13;
    int64_t* rax14;
    void* rsp15;
    uint32_t edx16;
    int1_t less17;
    void* rsp18;
    uint32_t eax19;
    int32_t eax20;
    uint32_t eax21;
    uint32_t v22;
    void* rdx23;
    int64_t* rsp24;
    int64_t rax25;
    int64_t rdx26;
    struct s0* rsi27;
    struct s0* v28;
    void* rsp29;
    void* rsp30;
    int64_t rbx31;
    int64_t rdx32;
    int1_t zf33;
    struct s0* rax34;
    uint32_t ebx35;
    void* r14_36;
    struct s0* r13_37;
    struct s0* rcx38;
    struct s0* r12_39;
    struct s0* rdi40;
    uint32_t eax41;
    uint32_t ecx42;
    struct s0** rcx43;
    uint32_t r8d44;
    struct s0* rdx45;
    struct s0* rax46;
    struct s0* rax47;
    struct s0* rax48;
    struct s0* rax49;
    struct s0* rax50;
    void* rsp51;
    struct s0* v52;
    struct s0* v53;
    struct s0* rax54;
    struct s1* rax55;
    struct s0* rbp56;
    uint32_t eax57;
    void* rsp58;
    uint32_t edx59;
    int1_t less60;
    int1_t less61;
    void* rsp62;
    struct s0* rdx63;
    int1_t less64;
    int64_t rdx65;
    int64_t v66;
    void* rsp67;
    struct s0* rax68;
    int64_t rax69;
    uint32_t edx70;
    struct s0* rcx71;
    struct s0* rbp72;
    int64_t r12_73;
    int64_t rbx74;
    struct s0* rdi75;
    void* r13_76;
    uint32_t eax77;
    uint32_t eax78;
    uint32_t edx79;
    uint32_t eax80;
    int64_t rax81;
    int64_t rbx82;
    int64_t rax83;
    struct s0* rsi84;
    struct s0* rdi85;
    int64_t rax86;
    int64_t rbx87;
    int64_t rax88;
    struct s0* rsi89;
    struct s0* rdi90;
    uint32_t eax91;
    struct s0* rdi92;
    struct s0* rax93;
    void* rsp94;
    struct s0* rbp95;
    struct s0* rax96;
    void* rsp97;
    struct s0* rax98;
    struct s0* rax99;
    struct s0* rax100;
    int64_t* rax101;
    struct s0* rax102;
    struct s0* rdi103;
    struct s0* rax104;
    struct s0* rsi105;
    struct s0* rdi106;
    struct s0* rax107;
    struct s0* rax108;
    struct s0* rdx109;
    uint32_t tmp32_110;
    int64_t rdi111;
    int32_t eax112;
    void* rax113;
    int64_t v114;
    struct s0* rsi115;
    struct s0* rax116;
    uint32_t tmp32_117;
    int64_t rdi118;
    int32_t eax119;
    int64_t rdi120;
    int32_t eax121;
    int64_t v122;
    int64_t v123;
    uint32_t eax124;
    uint32_t tmp32_125;
    int64_t rdi126;
    void* r14_127;
    int32_t eax128;
    int64_t* rdx129;
    int64_t rdi130;
    int64_t rdi131;
    int32_t eax132;
    int64_t v133;
    int64_t v134;
    uint32_t tmp32_135;
    int64_t rdi136;
    int32_t eax137;
    int64_t rdi138;
    int32_t eax139;
    int64_t v140;
    int64_t v141;
    int64_t v142;
    struct s0* rax143;
    struct s0* rax144;
    uint32_t eax145;
    int1_t less146;
    int64_t v147;
    uint32_t tmp32_148;
    int64_t rax149;

    while (rdx3 = reinterpret_cast<int32_t>(pos), r8_4 = argv, rax5 = g28, rdi6 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rdx3 * 8), *reinterpret_cast<uint32_t*>(&rbx7) = rdi6->f1, *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0, edx8 = static_cast<int32_t>(reinterpret_cast<unsigned char>(rbx7) + 0xffffffffffffffb9), *reinterpret_cast<unsigned char*>(&edx8) > 51) {
        rax9 = quote(rdi6, rdi6);
        rax10 = fun_2420();
        rdi11 = rax10;
        rax14 = test_syntax_error(rdi11, rax9, 5, rcx12, r8_4, r9_13);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
        edx16 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax14) + 1);
        less17 = reinterpret_cast<int32_t>(edx16) < reinterpret_cast<int32_t>(argc);
        pos = edx16;
        if (!less17) {
            beyond();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        } else {
            eax19 = *reinterpret_cast<int32_t*>(&rax14) + 2;
            pos = eax19;
            rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<int32_t>(eax19) * 8 - 8);
            eax20 = fun_24b0(rdi11, rsp15);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_4) = eax20;
            *reinterpret_cast<int32_t*>(&r8_4 + 4) = 0;
            eax21 = 0;
            if (!*reinterpret_cast<int32_t*>(&r8_4)) {
                eax21 = v22 & 0xf000;
                *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(eax21 == 0xa000);
            }
            rdx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (!rdx23) 
                goto addr_3207_7;
        }
        fun_2450();
        rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        while ((rax25 = reinterpret_cast<int32_t>(pos), rcx12 = argv, rdx26 = rax25, rsi27 = reinterpret_cast<struct s0*>(rax25 * 8), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f0) == 33)) || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f1) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f0) == 45)) 
                goto addr_38f6_11;
            if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f1) 
                goto addr_38f6_11;
            if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8))->f2) 
                goto addr_38f1_14;
            addr_38f6_11:
            v28 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx12) + rax25 * 8);
            beyond();
            rsp29 = reinterpret_cast<void*>(rsp24 - 1 - 1 + 1);
            while (1) {
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
                *reinterpret_cast<uint32_t*>(&rbx31) = pos;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                while (1) {
                    zf33 = *reinterpret_cast<uint32_t*>(&rbx31) == *reinterpret_cast<uint32_t*>(&rdx32);
                    if (*reinterpret_cast<int32_t*>(&rbx31) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                        goto addr_3a9d_17;
                    goto addr_3930_19;
                    addr_3a5a_20:
                    if (*reinterpret_cast<uint32_t*>(&rcx12) != 45) 
                        goto addr_3a68_21;
                    if (rax34->f1 != 0x6f) 
                        goto addr_3a68_21;
                    if (rax34->f2) 
                        goto addr_3a68_21;
                    *reinterpret_cast<uint32_t*>(&rbx31) = ebx35 + 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                    pos = *reinterpret_cast<uint32_t*>(&rbx31);
                    continue;
                    while (1) {
                        addr_3a0c_25:
                        if (*reinterpret_cast<signed char*>(&r14_36) != 45 || !r13_37->f1) {
                            addr_3a28_26:
                            ebx35 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx12->f1));
                            pos = ebx35;
                        } else {
                            if (!r13_37->f2) {
                                unary_operator(rdi11, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                ebx35 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            } else {
                                goto addr_3a28_26;
                            }
                        }
                        while (1) {
                            if (reinterpret_cast<int32_t>(ebx35) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                                goto addr_3af0_31;
                            while ((rcx38 = argv, rax34 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rcx38) + reinterpret_cast<int32_t>(ebx35) * 8), *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax34->f0)), *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx12) == 45) && (rax34->f1 == 97 && !rax34->f2)) {
                                *reinterpret_cast<uint32_t*>(&rbx31) = ebx35 + 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
                                pos = *reinterpret_cast<uint32_t*>(&rbx31);
                                zf33 = *reinterpret_cast<uint32_t*>(&rbx31) == *reinterpret_cast<uint32_t*>(&rdx32);
                                if (*reinterpret_cast<int32_t*>(&rbx31) < *reinterpret_cast<int32_t*>(&rdx32)) {
                                    addr_3930_19:
                                    r8_4 = argv;
                                    rbx31 = *reinterpret_cast<int32_t*>(&rbx31);
                                    *reinterpret_cast<uint32_t*>(&rdi11) = 0;
                                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    while (r13_37 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8), *reinterpret_cast<uint32_t*>(&rax34) = *reinterpret_cast<uint32_t*>(&rbx31), *reinterpret_cast<int32_t*>(&rax34 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rbx31), *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0, *reinterpret_cast<uint32_t*>(&r14_36) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r13_37->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_36) + 4) = 0, *reinterpret_cast<signed char*>(&r14_36) == 33) {
                                        if (r13_37->f1) 
                                            goto addr_3ab8_36;
                                        ++rbx31;
                                        *reinterpret_cast<uint32_t*>(&r12_39) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rax34->f1));
                                        if (*reinterpret_cast<int32_t*>(&rdx32) <= *reinterpret_cast<int32_t*>(&rbx31)) 
                                            goto addr_3bf6_38;
                                        *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                        *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    }
                                } else {
                                    addr_3a9d_17:
                                    beyond();
                                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    goto addr_3aa8_40;
                                }
                                if (*reinterpret_cast<signed char*>(&rdi11)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_39);
                                }
                                if (*reinterpret_cast<signed char*>(&r14_36) != 40 || r13_37->f1) {
                                    addr_399c_44:
                                    *reinterpret_cast<uint32_t*>(&rax34) = *reinterpret_cast<uint32_t*>(&rdx32) - *reinterpret_cast<uint32_t*>(&rcx12);
                                    zf33 = *reinterpret_cast<uint32_t*>(&rax34) == 3;
                                    if (*reinterpret_cast<int32_t*>(&rax34) <= reinterpret_cast<int32_t>(3)) {
                                        addr_3aa8_40:
                                        if (!zf33) 
                                            goto addr_3a0c_25;
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_37->f0) == 45)) 
                                            goto addr_39ea_46;
                                        if (r13_37->f1 != 0x6c) 
                                            goto addr_39ea_46;
                                        if (r13_37->f2) 
                                            goto addr_39ea_46;
                                        rdi40 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8 + 16);
                                        eax41 = binop(rdi40, rsi27);
                                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                        r8_4 = r8_4;
                                        *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx12);
                                        *reinterpret_cast<uint32_t*>(&rdx32) = *reinterpret_cast<uint32_t*>(&rdx32);
                                        if (*reinterpret_cast<signed char*>(&eax41)) 
                                            goto addr_3ba3_50; else 
                                            goto addr_39ea_46;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r9_13) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(&rcx12->f1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_13) + 4) = 0;
                                    pos = *reinterpret_cast<uint32_t*>(&r9_13);
                                    zf33 = *reinterpret_cast<uint32_t*>(&r9_13) == *reinterpret_cast<uint32_t*>(&rdx32);
                                    if (*reinterpret_cast<int32_t*>(&r9_13) >= *reinterpret_cast<int32_t*>(&rdx32)) 
                                        goto addr_3a9d_17; else 
                                        goto addr_3b10_52;
                                }
                                addr_39ea_46:
                                rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + rbx31 * 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rax34) = binop(rdi11, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx12);
                                *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx32) = *reinterpret_cast<uint32_t*>(&rdx32);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                                if (!*reinterpret_cast<signed char*>(&rax34)) 
                                    goto addr_3a0c_25;
                                *reinterpret_cast<uint32_t*>(&rdi11) = 0;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                binary_operator(0, rsi27);
                                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                ebx35 = pos;
                                *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                                if (reinterpret_cast<int32_t>(ebx35) < *reinterpret_cast<int32_t*>(&rdx32)) 
                                    continue; else 
                                    goto addr_3af0_31;
                                addr_3ab8_36:
                                if (*reinterpret_cast<signed char*>(&rdi11)) {
                                    pos = *reinterpret_cast<uint32_t*>(&r12_39);
                                    goto addr_399c_44;
                                }
                            }
                            goto addr_3a5a_20;
                            addr_3bf6_38:
                            pos = *reinterpret_cast<uint32_t*>(&r12_39);
                            beyond();
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            goto addr_3c02_56;
                            addr_3ba3_50:
                            *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                            *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            binary_operator(1, rsi27);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            ebx35 = pos;
                            *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            continue;
                            addr_3b10_52:
                            ecx42 = *reinterpret_cast<uint32_t*>(&rcx12) + 2;
                            if (reinterpret_cast<int32_t>(ecx42) >= *reinterpret_cast<int32_t*>(&rdx32)) {
                                addr_3c02_56:
                                *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rdi11) = 1;
                                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                rcx43 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<int32_t>(ecx42) * 8);
                                r8d44 = static_cast<int32_t>(rdx32 - 1) - *reinterpret_cast<uint32_t*>(&rax34);
                                do {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&(*rcx43)->f0) == 41)) 
                                        goto addr_3b3c_59;
                                    if (!(*rcx43)->f1) 
                                        break;
                                    addr_3b3c_59:
                                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 4) 
                                        goto addr_3b9c_61;
                                    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdi11) + 1;
                                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                                    rcx43 = rcx43 + 8;
                                } while (*reinterpret_cast<uint32_t*>(&rdi11) != r8d44);
                                goto addr_3b4d_63;
                            }
                            addr_3b50_64:
                            posixtest(rdi11, rsi27);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            rcx12 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                            rdx45 = argv;
                            r8_4 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx45) + reinterpret_cast<unsigned char>(rcx12) * 8);
                            rbx7 = rcx12;
                            if (!r8_4) 
                                goto addr_3c0c_65;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r8_4->f0) == 41)) 
                                goto addr_3c3b_67;
                            if (r8_4->f1) 
                                goto addr_3c3b_67;
                            ebx35 = *reinterpret_cast<uint32_t*>(&rbx7) + 1;
                            *reinterpret_cast<uint32_t*>(&rdx32) = argc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                            pos = ebx35;
                            continue;
                            addr_3b9c_61:
                            *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdx32) - *reinterpret_cast<uint32_t*>(&r9_13);
                            *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                            goto addr_3b50_64;
                            addr_3b4d_63:
                            goto addr_3b50_64;
                        }
                    }
                }
                addr_3c0c_65:
                rax46 = quote(")", ")");
                rax47 = fun_2420();
                test_syntax_error(rax47, rax46, 5, rcx12, r8_4, r9_13);
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_3c3b_67:
                rax48 = quote_n();
                r12_39 = rax48;
                rax49 = quote_n();
                rax50 = fun_2420();
                rsi27 = rax49;
                test_syntax_error(rax50, rsi27, r12_39, rcx12, r8_4, r9_13);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                while (v52 = r13_37, v53 = r12_39, rax54 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos))), r12_39 = argv, rbx7 = rax54, rax55 = reinterpret_cast<struct s1*>(&rax54->f1), rbp56 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<uint64_t>(rax55) * 8), r13_37 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax55) * 8), rdi11 = rbp56, eax57 = binop(rdi11, rsi27, rdi11, rsi27), rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 - 8 - 8 - 8 - 8 - 8 + 8), !*reinterpret_cast<signed char*>(&eax57)) {
                    edx59 = (*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f0;
                    if (edx59 != 33 || (*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f1) {
                        if (edx59 != 40) 
                            goto addr_3d30_73;
                        if ((*reinterpret_cast<struct s5**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 0xfffffffffffffff8))->f1) 
                            goto addr_3d30_73;
                        if ((*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 8))->f0 != 41) 
                            goto addr_3d30_73;
                        if (!(*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r12_39) + reinterpret_cast<unsigned char>(r13_37) + 8))->f1) 
                            goto addr_3d1b_77;
                        addr_3d30_73:
                        *reinterpret_cast<uint32_t*>(&rax34) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp56->f0));
                        if (*reinterpret_cast<uint32_t*>(&rax34) != 45) 
                            goto addr_3d4a_78;
                        if (rbp56->f1 != 97) 
                            goto addr_3d3f_80;
                        if (!rbp56->f2) 
                            goto addr_3d96_82;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rbx7) = *reinterpret_cast<uint32_t*>(&rbx7) + 1;
                        less60 = *reinterpret_cast<int32_t*>(&rbx7) < reinterpret_cast<int32_t>(argc);
                        pos = *reinterpret_cast<uint32_t*>(&rbx7);
                        if (!less60) 
                            goto addr_3db8_84; else 
                            goto addr_3ce3_85;
                    }
                    addr_3d3f_80:
                    if (*reinterpret_cast<uint32_t*>(&rax34) != 45) 
                        goto addr_3d4a_78;
                    if (rbp56->f1 != 0x6f) 
                        goto addr_3d4a_78;
                    if (rbp56->f2) 
                        goto addr_3db6_88;
                    addr_3d96_82:
                    less61 = *reinterpret_cast<int32_t*>(&rbx7) < reinterpret_cast<int32_t>(argc);
                    if (less61) 
                        goto addr_3d9e_89;
                    addr_3db8_84:
                    beyond();
                    rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8);
                    if (*reinterpret_cast<uint32_t*>(&rdi11) != 3) 
                        goto addr_3dcd_91;
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) + 8);
                }
                goto addr_3d78_93;
                addr_3d9e_89:
                r12_39 = v53;
                r13_37 = v52;
                rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) + 8 + 8 + 8 + 8 + 8);
                continue;
                addr_3dcd_91:
                if (*reinterpret_cast<int32_t*>(&rdi11) > reinterpret_cast<int32_t>(3)) {
                    if (*reinterpret_cast<uint32_t*>(&rdi11) != 4) {
                        addr_3e60_95:
                        *reinterpret_cast<uint32_t*>(&rax34) = pos;
                    } else {
                        rdx63 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(pos)));
                        rsi27 = argv;
                        rax34 = rdx63;
                        rdi11 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx63) * 8);
                        *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f0));
                        *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rcx12) != 33) 
                            goto addr_3e1e_97;
                        if (!(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f1) 
                            goto addr_3eca_99;
                        addr_3e1e_97:
                        if (*reinterpret_cast<uint32_t*>(&rcx12) != 40) 
                            goto addr_3e66_100;
                        if ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdx63) * 8))->f1) 
                            goto addr_3e66_100;
                        if ((*reinterpret_cast<struct s7**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdi11) + 24))->f0 != 41) 
                            goto addr_3e66_100;
                        if (!(*reinterpret_cast<struct s7**>(reinterpret_cast<unsigned char>(rsi27) + reinterpret_cast<unsigned char>(rdi11) + 24))->f1) 
                            goto addr_3e39_104;
                    }
                    addr_3e66_100:
                    less64 = *reinterpret_cast<int32_t*>(&rax34) < reinterpret_cast<int32_t>(argc);
                    if (!less64) 
                        goto addr_3ee5_105;
                    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) + 8);
                } else {
                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 1) 
                        goto addr_3e90_108;
                    if (*reinterpret_cast<uint32_t*>(&rdi11) == 2) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<uint32_t*>(&rdi11) - 1;
                    *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rdi11)) 
                        goto addr_2665_111;
                    goto addr_3e60_95;
                }
            }
            rsp24 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp62) + 8);
        }
        goto addr_38c8_114;
        addr_38f1_14:
    }
    *reinterpret_cast<uint32_t*>(&rdx65) = *reinterpret_cast<unsigned char*>(&edx8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx65) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa1f0 + rdx65 * 4) + 0xa1f0;
    addr_38c8_114:
    pos = *reinterpret_cast<int32_t*>(&rdx26) + 2;
    goto v66;
    addr_3af0_31:
    addr_3a68_21:
    goto v28;
    addr_3d78_93:
    rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax68 = g28;
    *reinterpret_cast<uint32_t*>(&rax69) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
    if (0) {
        *reinterpret_cast<uint32_t*>(&rax69) = *reinterpret_cast<uint32_t*>(&rax69) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax69);
    }
    edx70 = argc;
    *reinterpret_cast<uint32_t*>(&rcx71) = static_cast<uint32_t>(rax69 + 1);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    rbp72 = argv;
    *reinterpret_cast<int32_t*>(&r12_73) = 0;
    if (reinterpret_cast<int32_t>(edx70 - 2) > *reinterpret_cast<int32_t*>(&rcx71)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax69) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_73) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx71);
            *reinterpret_cast<int32_t*>(&r12_73) = 1;
        }
    }
    rbx74 = *reinterpret_cast<int32_t*>(&rcx71);
    rdi75 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + rbx74 * 8);
    r13_76 = reinterpret_cast<void*>(rbx74 * 8);
    eax77 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi75->f0));
    if (*reinterpret_cast<signed char*>(&eax77) == 45) {
        eax78 = rdi75->f1;
        if ((*reinterpret_cast<signed char*>(&eax78) == 0x6c || *reinterpret_cast<signed char*>(&eax78) == 0x67) && ((edx79 = rdi75->f2, *reinterpret_cast<signed char*>(&edx79) == 0x65) || *reinterpret_cast<signed char*>(&edx79) == 0x74)) {
            if (rdi75->f3) 
                goto addr_2d44_125; else 
                goto addr_2ea4_126;
        }
        if (*reinterpret_cast<signed char*>(&eax78) != 0x65) 
            goto addr_2d3c_128;
    } else {
        if (*reinterpret_cast<signed char*>(&eax77) != 61 || (eax80 = rdi75->f1, !!*reinterpret_cast<signed char*>(&eax80)) && (*reinterpret_cast<signed char*>(&eax80) != 61 || rdi75->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi75->f0) == 33) || (rdi75->f1 != 61 || rdi75->f2)) {
                fun_2370();
                goto addr_2665_111;
            } else {
                rax81 = reinterpret_cast<int32_t>(pos);
                rbx82 = rax81;
                rax83 = rax81 + 2;
                rsi84 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8));
                rdi85 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax83 * 8) - 16);
                fun_24f0(rdi85, rsi84);
                pos = *reinterpret_cast<int32_t*>(&rbx82) + 3;
                goto addr_2cc9_133;
            }
        } else {
            rax86 = reinterpret_cast<int32_t>(pos);
            rbx87 = rax86;
            rax88 = rax86 + 2;
            rsi89 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax88 * 8));
            rdi90 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(rax88 * 8) - 16);
            fun_24f0(rdi90, rsi89);
            pos = *reinterpret_cast<int32_t*>(&rbx87) + 3;
            goto addr_2cc9_133;
        }
    }
    eax91 = rdi75->f2;
    if (*reinterpret_cast<signed char*>(&eax91) == 0x71) {
        addr_300c_136:
        if (!rdi75->f3) {
            addr_2ea4_126:
            rdi92 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
            if (1) {
                rax93 = find_int(rdi92);
                rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rbp95 = rax93;
            } else {
                rax96 = fun_2440(rdi92);
                rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                rax98 = umaxtostr(rax96, reinterpret_cast<int64_t>(rsp97) + 0x120);
                rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
                rbp95 = rax98;
            }
        } else {
            addr_3016_139:
            rax99 = quote(rdi75);
            rax100 = fun_2420();
            rax101 = test_syntax_error(rax100, rax99, 5, rcx71, r8_4, r9_13);
            goto addr_3040_140;
        }
    } else {
        addr_2e0c_141:
        if (*reinterpret_cast<signed char*>(&eax91) != 0x66) 
            goto addr_3016_139;
        if (rdi75->f3) 
            goto addr_3016_139; else 
            goto addr_2e1e_143;
    }
    rax102 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi103 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax102) + reinterpret_cast<uint64_t>(r13_76) + 8);
        rax104 = find_int(rdi103, rdi103);
        rsi105 = rax104;
    } else {
        rdi106 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax102) + reinterpret_cast<uint64_t>(r13_76) + 16);
        rax107 = fun_2440(rdi106, rdi106);
        rax108 = umaxtostr(rax107, reinterpret_cast<int64_t>(rsp94) - 8 + 8 + 0x140);
        rsi105 = rax108;
    }
    strintcmp(rbp95, rsi105);
    rdx109 = argv;
    *reinterpret_cast<uint32_t*>(&rcx71) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx109) + rbx74 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    tmp32_110 = pos + 3;
    pos = tmp32_110;
    if (*reinterpret_cast<signed char*>(&rcx71) == 0x6c) {
        goto addr_2cc9_133;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx71) == 0x67) {
            goto addr_2cc9_133;
        } else {
            goto addr_2cc9_133;
        }
    }
    addr_3040_140:
    rdi111 = *rax101;
    eax112 = fun_2500(rdi111, r14_36, rdi111, r14_36);
    if (!eax112) {
        addr_2cc9_133:
        rax113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax68) - reinterpret_cast<unsigned char>(g28));
        if (rax113) {
            fun_2450();
        } else {
            goto v114;
        }
    } else {
        addr_3053_154:
        goto addr_2cc9_133;
    }
    rsi115 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_156:
        rax116 = fun_2420();
        test_syntax_error(rax116, rsi115, 5, rcx71, r8_4, r9_13);
        addr_3102_157:
        rsi115 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_143:
    tmp32_117 = pos + 3;
    pos = tmp32_117;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (!*reinterpret_cast<unsigned char*>(&r12_73)) {
        rdi118 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
        eax119 = fun_2500(rdi118, rsp67);
        if (!eax119 && ((rdi120 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8), eax121 = fun_2500(rdi120, reinterpret_cast<int64_t>(rsp67) - 8 + 8 + 0x90), !eax121) && v122 == v123)) {
            goto addr_2cc9_133;
        }
    }
    addr_2d3c_128:
    if (*reinterpret_cast<signed char*>(&eax78) == 0x6e) {
        eax124 = rdi75->f2;
        if (*reinterpret_cast<signed char*>(&eax124) == 0x65) 
            goto addr_300c_136;
    } else {
        addr_2d44_125:
        if (*reinterpret_cast<signed char*>(&eax78) != 0x6f) {
            if (*reinterpret_cast<signed char*>(&eax78) > 0x6f) 
                goto addr_3016_139;
            if (*reinterpret_cast<signed char*>(&eax78) == 0x65) 
                goto addr_30a8_163; else 
                goto addr_2f4e_164;
        } else {
            if (rdi75->f2 != 0x74) 
                goto addr_3016_139;
            if (rdi75->f3) 
                goto addr_3016_139;
            tmp32_125 = pos + 3;
            pos = tmp32_125;
            *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
            if (*reinterpret_cast<unsigned char*>(&r12_73)) {
                rsi115 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                goto addr_30f1_156;
            } else {
                rdi126 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
                r14_127 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
                eax128 = fun_2500(rdi126, r14_127);
                rdx129 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
                if (eax128) {
                    rdi130 = *rdx129;
                    fun_2500(rdi130, r14_127);
                    goto addr_2cc9_133;
                } else {
                    rdi131 = *rdx129;
                    eax132 = fun_2500(rdi131, r14_127);
                    if (!eax132) {
                        *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v133 < v134);
                        *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
                        goto addr_2cc9_133;
                    }
                }
            }
        }
    }
    addr_2f5a_173:
    if (*reinterpret_cast<signed char*>(&eax124) != 0x74) 
        goto addr_3016_139;
    if (rdi75->f3) 
        goto addr_3016_139;
    tmp32_135 = pos + 3;
    pos = tmp32_135;
    *reinterpret_cast<unsigned char*>(&r12_73) = reinterpret_cast<unsigned char>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_73)));
    if (*reinterpret_cast<unsigned char*>(&r12_73)) 
        goto addr_3102_157;
    rdi136 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) - 8);
    r14_36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 0x90);
    eax137 = fun_2500(rdi136, r14_36);
    rax101 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp72) + reinterpret_cast<uint64_t>(r13_76) + 8);
    if (eax137) 
        goto addr_3040_140;
    rdi138 = *rax101;
    eax139 = fun_2500(rdi138, r14_36);
    if (eax139) 
        goto addr_3053_154;
    *reinterpret_cast<uint32_t*>(&rcx71) = reinterpret_cast<uint1_t>(v140 < v141);
    *reinterpret_cast<int32_t*>(&rcx71 + 4) = 0;
    goto addr_2cc9_133;
    addr_30a8_163:
    eax91 = rdi75->f2;
    goto addr_2e0c_141;
    addr_2f4e_164:
    if (*reinterpret_cast<signed char*>(&eax78) != 0x6e) 
        goto addr_3016_139;
    eax124 = rdi75->f2;
    goto addr_2f5a_173;
    addr_2665_111:
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    fun_2370();
    addr_3d1b_77:
    pos = *reinterpret_cast<uint32_t*>(&rbx7) + 3;
    addr_3ceb_194:
    goto v142;
    addr_3d4a_78:
    rax143 = quote(rbp56, rbp56);
    rax144 = fun_2420();
    test_syntax_error(rax144, rax143, 5, rcx12, r8_4, r9_13);
    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3d78_93;
    addr_3db6_88:
    goto addr_3d4a_78;
    addr_3ce3_85:
    two_arguments();
    goto addr_3ceb_194;
    addr_3eca_99:
    eax145 = *reinterpret_cast<uint32_t*>(&rax34) + 1;
    less146 = reinterpret_cast<int32_t>(eax145) < reinterpret_cast<int32_t>(argc);
    pos = eax145;
    if (!less146) {
        addr_3ee5_105:
        beyond();
    } else {
        three_arguments();
    }
    addr_3eb1_196:
    goto v147;
    addr_3e39_104:
    pos = *reinterpret_cast<uint32_t*>(&rax34) + 1;
    two_arguments();
    tmp32_148 = pos + 1;
    pos = tmp32_148;
    goto addr_3eb1_196;
    addr_3e90_108:
    rax149 = reinterpret_cast<int32_t>(pos);
    pos = static_cast<uint32_t>(rax149 + 1);
    goto addr_3eb1_196;
    addr_3207_7:
    return eax21;
}