bool unary_operator(void) {
    uint64_t v1 = (int64_t)pos; // 0x3129
    int64_t v2 = __readfsqword(40); // 0x3137
    int64_t v3 = *(int64_t *)(8 * v1 + (int64_t)argv); // 0x314a
    if (*(char *)(v3 + 1) < 123) {
        // 0x3165
        return v1 % 2 != 0;
    }
    // 0x3179
    quote((char *)v3);
    test_syntax_error((char *)function_2420());
    pos = (int64_t)&g35 + 1;
    if (argc <= (int32_t)((int64_t)&g35 + 1)) {
        // 0x388e
        beyond();
        // 0x3893
        return function_2450() % 2 != 0;
    }
    // 0x31bd
    pos = (int32_t)&g35 + 2;
    int64_t v4 = function_24b0(); // 0x31d0
    if (v2 == __readfsqword(40)) {
        // 0x3207
        int32_t v5; // 0x3120
        return (v5 & (int32_t)&g20) == (int32_t)&g2 == (int32_t)v4 == 0;
    }
    // 0x3893
    return function_2450() % 2 != 0;
}