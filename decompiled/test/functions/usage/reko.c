void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002620(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000003F4E;
	}
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "Usage: test EXPRESSION\n  or:  test\n  or:  [ EXPRESSION ]\n  or:  [ ]\n  or:  [ OPTION\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "Exit with the status determined by EXPRESSION.\n\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\nAn omitted EXPRESSION defaults to false.  Otherwise,\nEXPRESSION is true or false and sets exit status.  It is one of:\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\n  ( EXPRESSION )               EXPRESSION is true\n  ! EXPRESSION                 EXPRESSION is false\n  EXPRESSION1 -a EXPRESSION2   both EXPRESSION1 and EXPRESSION2 are true\n  EXPRESSION1 -o EXPRESSION2   either EXPRESSION1 or EXPRESSION2 is true\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\n  -n STRING            the length of STRING is nonzero\n  STRING               equivalent to -n STRING\n  -z STRING            the length of STRING is zero\n  STRING1 = STRING2    the strings are equal\n  STRING1 != STRING2   the strings are not equal\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\n  INTEGER1 -eq INTEGER2   INTEGER1 is equal to INTEGER2\n  INTEGER1 -ge INTEGER2   INTEGER1 is greater than or equal to INTEGER2\n  INTEGER1 -gt INTEGER2   INTEGER1 is greater than INTEGER2\n  INTEGER1 -le INTEGER2   INTEGER1 is less than or equal to INTEGER2\n  INTEGER1 -lt INTEGER2   INTEGER1 is less than INTEGER2\n  INTEGER1 -ne INTEGER2   INTEGER1 is not equal to INTEGER2\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\n  FILE1 -ef FILE2   FILE1 and FILE2 have the same device and inode numbers\n  FILE1 -nt FILE2   FILE1 is newer (modification date) than FILE2\n  FILE1 -ot FILE2   FILE1 is older than FILE2\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\n  -b FILE     FILE exists and is block special\n  -c FILE     FILE exists and is character special\n  -d FILE     FILE exists and is a directory\n  -e FILE     FILE exists\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "  -f FILE     FILE exists and is a regular file\n  -g FILE     FILE exists and is set-group-ID\n  -G FILE     FILE exists and is owned by the effective group ID\n  -h FILE     FILE exists and is a symbolic link (same as -L)\n  -k FILE     FILE exists and has its sticky bit set\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "  -L FILE     FILE exists and is a symbolic link (same as -h)\n  -N FILE     FILE exists and has been modified since it was last read\n  -O FILE     FILE exists and is owned by the effective user ID\n  -p FILE     FILE exists and is a named pipe\n  -r FILE     FILE exists and the user has read access\n  -s FILE     FILE exists and has a size greater than zero\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "  -S FILE     FILE exists and is a socket\n  -t FD       file descriptor FD is opened on a terminal\n  -u FILE     FILE exists and its set-user-ID bit is set\n  -w FILE     FILE exists and the user has write access\n  -x FILE     FILE exists and the user has execute (or search) access\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\nExcept for -h and -L, all FILE-related tests dereference symbolic links.\nBeware that parentheses need to be escaped (e.g., by backslashes) for shells.\nINTEGER may also be -l STRING, which evaluates to the length of STRING.\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\nNOTE: Binary -a and -o are inherently ambiguous.  Use 'test EXPR1 && test\nEXPR2' or 'test EXPR1 || test EXPR2' instead.\n", null));
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "\nNOTE: [ honors the --help and --version options, but test does not.\ntest treats each of those as it treats any other nonempty STRING.\n", null));
	fn0000000000002420(0x05, "test and/or [", null);
	fn00000000000025B0(fn0000000000002420(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_2408 * rbx_320 = fp - 0xB8 + 16;
	do
	{
		char * rsi_322 = rbx_320->qw0000;
		++rbx_320;
	} while (rsi_322 != null && fn00000000000024F0(rsi_322, 0xA111) != 0x00);
	ptr64 r13_335 = rbx_320->qw0008;
	if (r13_335 != 0x00)
	{
		fn00000000000025B0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_12 rax_422 = fn00000000000025A0(null, 0x05);
		if (rax_422 == 0x00 || fn00000000000023A0(0x03, "en_", rax_422) == 0x00)
			goto l0000000000004306;
	}
	else
	{
		fn00000000000025B0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_12 rax_364 = fn00000000000025A0(null, 0x05);
		if (rax_364 == 0x00 || fn00000000000023A0(0x03, "en_", rax_364) == 0x00)
		{
			fn00000000000025B0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000004343:
			fn00000000000025B0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000003F4E:
			fn0000000000002600(edi);
		}
		r13_335 = 0xA111;
	}
	fn00000000000024D0(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000004306:
	fn00000000000025B0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000004343;
}