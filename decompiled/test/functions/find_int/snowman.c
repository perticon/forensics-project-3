struct s0* find_int(struct s0* rdi, ...) {
    struct s0* rbp2;
    unsigned char** rax3;
    void* rsp4;
    struct s0* r8_5;
    unsigned char* rsi6;
    int64_t rdx7;
    int64_t rax8;
    struct s0* rdx9;
    void* rdx10;
    struct s1* rax11;
    struct s0* rcx12;
    int32_t ecx13;
    struct s0* rdx14;
    int32_t ecx15;
    struct s0* rax16;
    struct s0* rax17;
    int64_t r9_18;
    int64_t rdx19;
    struct s0* rax20;
    struct s0* rdi21;
    struct s0* rax22;
    struct s0* rax23;
    struct s0* rdi24;
    int64_t r9_25;
    int32_t esi26;
    void* rsp27;
    struct s0* rax28;
    int64_t rax29;
    uint32_t edx30;
    struct s0* rcx31;
    struct s0* rbp32;
    int64_t r12_33;
    int64_t rbx34;
    struct s0* rdi35;
    void* r13_36;
    uint32_t eax37;
    uint32_t eax38;
    int64_t rax39;
    int64_t rbx40;
    int64_t rax41;
    struct s0* rsi42;
    struct s0* rdi43;
    int64_t rax44;
    int64_t rbx45;
    int64_t rax46;
    struct s0* rsi47;
    struct s0* rdi48;
    uint32_t eax49;
    uint32_t edx50;
    uint32_t eax51;
    struct s0* rdi52;
    struct s0* rax53;
    void* rsp54;
    struct s0* rbp55;
    struct s0* rax56;
    void* rsp57;
    struct s0* rax58;
    struct s0* rax59;
    struct s0* rax60;
    int64_t r9_61;
    int64_t* rax62;
    uint32_t eax63;
    uint32_t tmp32_64;
    struct s0* rsi65;
    int64_t rdi66;
    void* r14_67;
    int32_t eax68;
    int64_t* rdx69;
    int64_t rdi70;
    int64_t rdi71;
    int32_t eax72;
    int64_t v73;
    int64_t v74;
    struct s0* rax75;
    struct s0* rdi76;
    struct s0* rax77;
    struct s0* rsi78;
    struct s0* rdi79;
    struct s0* rax80;
    struct s0* rax81;
    struct s0* rdx82;
    uint32_t tmp32_83;
    int64_t rdi84;
    void* r14_85;
    int32_t eax86;
    void* rax87;
    struct s0* rax88;
    int64_t r9_89;
    uint32_t tmp32_90;
    int64_t rdi91;
    int32_t eax92;
    int64_t rdi93;
    int32_t eax94;
    int64_t v95;
    int64_t v96;
    uint32_t tmp32_97;
    int64_t rdi98;
    int32_t eax99;
    int64_t rdi100;
    int32_t eax101;
    int64_t v102;
    int64_t v103;

    rbp2 = rdi;
    rax3 = fun_2650(rdi);
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 + 8);
    r8_5 = rbp2;
    rsi6 = *rax3;
    while (*reinterpret_cast<uint32_t*>(&rdx7) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8_5->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0, rax8 = rdx7, !!(rsi6[rdx7 * 2] & 1)) {
        r8_5 = reinterpret_cast<struct s0*>(&r8_5->f1);
    }
    if (*reinterpret_cast<signed char*>(&rdx7) == 43) {
        rdx9 = reinterpret_cast<struct s0*>(&r8_5->f1);
        r8_5 = rdx9;
    } else {
        *reinterpret_cast<int32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax8) == 45);
        rdx9 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rdx10) + reinterpret_cast<unsigned char>(r8_5));
    }
    rax11 = reinterpret_cast<struct s1*>(&rdx9->f1);
    *reinterpret_cast<uint32_t*>(&rcx12) = reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&rdx9->f0)) - 48;
    *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rcx12) <= 9) {
        ecx13 = reinterpret_cast<signed char>(rdx9->f1);
        *reinterpret_cast<int32_t*>(&rdx14) = ecx13;
        if (ecx13 - 48 <= 9) {
            do {
                ecx15 = reinterpret_cast<signed char>(rax11->f1);
                rax11 = reinterpret_cast<struct s1*>(&rax11->f1);
                *reinterpret_cast<int32_t*>(&rdx14) = ecx15;
            } while (ecx15 - 48 <= 9);
        }
        *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<unsigned char*>(&rdx14);
        *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
        if (rsi6[reinterpret_cast<unsigned char>(rcx12) * 2] & 1) {
            do {
                *reinterpret_cast<uint32_t*>(&rcx12) = rax11->f1;
                *reinterpret_cast<int32_t*>(&rcx12 + 4) = 0;
                rax11 = reinterpret_cast<struct s1*>(&rax11->f1);
                rdx14 = rcx12;
            } while (rsi6[reinterpret_cast<unsigned char>(rcx12) * 2] & 1);
        }
        if (!*reinterpret_cast<unsigned char*>(&rdx14)) {
            return r8_5;
        }
    }
    rax16 = quote(rbp2);
    rax17 = fun_2420();
    test_syntax_error(rax17, rax16, 5, rcx12, r8_5, r9_18);
    rdx19 = reinterpret_cast<int32_t>(argc);
    rax20 = argv;
    rdi21 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax20) + rdx19 * 8 - 8);
    rax22 = quote(rdi21, rdi21);
    rax23 = fun_2420();
    rdi24 = rax23;
    test_syntax_error(rdi24, rax22, 5, rcx12, r8_5, r9_25);
    esi26 = *reinterpret_cast<int32_t*>(&rdi24);
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 0x160);
    rax28 = g28;
    *reinterpret_cast<uint32_t*>(&rax29) = pos;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rdi24)) {
        *reinterpret_cast<uint32_t*>(&rax29) = *reinterpret_cast<uint32_t*>(&rax29) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        pos = *reinterpret_cast<uint32_t*>(&rax29);
    }
    edx30 = argc;
    *reinterpret_cast<uint32_t*>(&rcx31) = static_cast<uint32_t>(rax29 + 1);
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    rbp32 = argv;
    *reinterpret_cast<int32_t*>(&r12_33) = 0;
    if (reinterpret_cast<int32_t>(edx30 - 2) > *reinterpret_cast<int32_t*>(&rcx31)) {
        if (*reinterpret_cast<struct s0**>(&(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f0) != 45 || ((*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f1 != 0x6c || (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax29) + 2) * 8))->f2)) {
            *reinterpret_cast<int32_t*>(&r12_33) = 0;
        } else {
            pos = *reinterpret_cast<uint32_t*>(&rcx31);
            *reinterpret_cast<int32_t*>(&r12_33) = 1;
        }
    }
    rbx34 = *reinterpret_cast<int32_t*>(&rcx31);
    rdi35 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + rbx34 * 8);
    r13_36 = reinterpret_cast<void*>(rbx34 * 8);
    eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi35->f0));
    if (*reinterpret_cast<signed char*>(&eax37) != 45) {
        if (*reinterpret_cast<signed char*>(&eax37) != 61 || (eax38 = rdi35->f1, !!*reinterpret_cast<signed char*>(&eax38)) && (*reinterpret_cast<signed char*>(&eax38) != 61 || rdi35->f2)) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi35->f0) == 33) || (rdi35->f1 != 61 || rdi35->f2)) {
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
                fun_2370();
            } else {
                rax39 = reinterpret_cast<int32_t>(pos);
                rbx40 = rax39;
                rax41 = rax39 + 2;
                rsi42 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax41 * 8));
                rdi43 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax41 * 8) - 16);
                fun_24f0(rdi43, rsi42);
                pos = *reinterpret_cast<int32_t*>(&rbx40) + 3;
                goto addr_2cc9_44;
            }
        } else {
            rax44 = reinterpret_cast<int32_t>(pos);
            rbx45 = rax44;
            rax46 = rax44 + 2;
            rsi47 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax46 * 8));
            rdi48 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(rax46 * 8) - 16);
            fun_24f0(rdi48, rsi47);
            pos = *reinterpret_cast<int32_t*>(&rbx45) + 3;
            goto addr_2cc9_44;
        }
    }
    eax49 = rdi35->f1;
    if ((*reinterpret_cast<signed char*>(&eax49) == 0x6c || *reinterpret_cast<signed char*>(&eax49) == 0x67) && ((edx50 = rdi35->f2, *reinterpret_cast<signed char*>(&edx50) == 0x65) || *reinterpret_cast<signed char*>(&edx50) == 0x74)) {
        if (rdi35->f3) 
            goto addr_2d44_48; else 
            goto addr_2ea4_49;
    }
    if (*reinterpret_cast<signed char*>(&eax49) == 0x65) {
        eax51 = rdi35->f2;
        if (*reinterpret_cast<signed char*>(&eax51) == 0x71) {
            addr_300c_52:
            if (!rdi35->f3) {
                addr_2ea4_49:
                rdi52 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
                if (!*reinterpret_cast<unsigned char*>(&esi26)) {
                    rax53 = find_int(rdi52);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
                    rbp55 = rax53;
                } else {
                    rax56 = fun_2440(rdi52);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
                    rax58 = umaxtostr(rax56, reinterpret_cast<int64_t>(rsp57) + 0x120);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
                    rbp55 = rax58;
                }
            } else {
                addr_3016_55:
                rax59 = quote(rdi35);
                rax60 = fun_2420();
                rax62 = test_syntax_error(rax60, rax59, 5, rcx31, r8_5, r9_61);
                goto addr_3040_56;
            }
        } else {
            addr_2e0c_57:
            if (*reinterpret_cast<signed char*>(&eax51) != 0x66) 
                goto addr_3016_55;
            if (rdi35->f3) 
                goto addr_3016_55; else 
                goto addr_2e1e_59;
        }
    } else {
        if (*reinterpret_cast<signed char*>(&eax49) == 0x6e) {
            eax63 = rdi35->f2;
            if (*reinterpret_cast<signed char*>(&eax63) != 0x65) 
                goto addr_2f5a_62; else 
                goto addr_300c_52;
        } else {
            addr_2d44_48:
            if (*reinterpret_cast<signed char*>(&eax49) != 0x6f) {
                if (*reinterpret_cast<signed char*>(&eax49) > 0x6f) 
                    goto addr_3016_55;
                if (*reinterpret_cast<signed char*>(&eax49) == 0x65) 
                    goto addr_30a8_65; else 
                    goto addr_2f4e_66;
            } else {
                if (rdi35->f2 != 0x74) 
                    goto addr_3016_55;
                if (rdi35->f3) 
                    goto addr_3016_55;
                tmp32_64 = pos + 3;
                pos = tmp32_64;
                *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
                if (*reinterpret_cast<unsigned char*>(&r12_33)) {
                    rsi65 = reinterpret_cast<struct s0*>("-ot does not accept -l");
                    goto addr_30f1_71;
                } else {
                    rdi66 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
                    r14_67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) + 0x90);
                    eax68 = fun_2500(rdi66, r14_67);
                    rdx69 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8);
                    if (eax68) {
                        rdi70 = *rdx69;
                        fun_2500(rdi70, r14_67);
                        goto addr_2cc9_44;
                    } else {
                        rdi71 = *rdx69;
                        eax72 = fun_2500(rdi71, r14_67);
                        if (!eax72) {
                            *reinterpret_cast<uint32_t*>(&rcx31) = reinterpret_cast<uint1_t>(v73 < v74);
                            *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
                            goto addr_2cc9_44;
                        }
                    }
                }
            }
        }
    }
    rax75 = argv;
    if (!*reinterpret_cast<unsigned char*>(&r12_33)) {
        rdi76 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<uint64_t>(r13_36) + 8);
        rax77 = find_int(rdi76, rdi76);
        rsi78 = rax77;
    } else {
        rdi79 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<uint64_t>(r13_36) + 16);
        rax80 = fun_2440(rdi79, rdi79);
        rax81 = umaxtostr(rax80, reinterpret_cast<int64_t>(rsp54) - 8 + 8 + 0x140);
        rsi78 = rax81;
    }
    strintcmp(rbp55, rsi78);
    rdx82 = argv;
    *reinterpret_cast<uint32_t*>(&rcx31) = (*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rdx82) + rbx34 * 8))->f1;
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    tmp32_83 = pos + 3;
    pos = tmp32_83;
    if (*reinterpret_cast<signed char*>(&rcx31) == 0x6c) {
        goto addr_2cc9_44;
    } else {
        if (*reinterpret_cast<signed char*>(&rcx31) == 0x67) {
            goto addr_2cc9_44;
        } else {
            goto addr_2cc9_44;
        }
    }
    addr_3040_56:
    rdi84 = *rax62;
    eax86 = fun_2500(rdi84, r14_85, rdi84, r14_85);
    if (!eax86) {
        addr_2cc9_44:
        rax87 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax28) - reinterpret_cast<unsigned char>(g28));
        if (rax87) {
            fun_2450();
        } else {
            goto rax16;
        }
    } else {
        addr_3053_86:
        goto addr_2cc9_44;
    }
    rsi65 = reinterpret_cast<struct s0*>("-ef does not accept -l");
    while (1) {
        addr_30f1_71:
        rax88 = fun_2420();
        test_syntax_error(rax88, rsi65, 5, rcx31, r8_5, r9_89);
        addr_3102_88:
        rsi65 = reinterpret_cast<struct s0*>("-nt does not accept -l");
    }
    addr_2e1e_59:
    tmp32_90 = pos + 3;
    pos = tmp32_90;
    *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
    if (!*reinterpret_cast<unsigned char*>(&r12_33)) {
        rdi91 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
        eax92 = fun_2500(rdi91, rsp27);
        if (!eax92 && ((rdi93 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8), eax94 = fun_2500(rdi93, reinterpret_cast<int64_t>(rsp27) - 8 + 8 + 0x90), !eax94) && v95 == v96)) {
            goto addr_2cc9_44;
        }
    }
    addr_2f5a_62:
    if (*reinterpret_cast<signed char*>(&eax63) != 0x74) 
        goto addr_3016_55;
    if (rdi35->f3) 
        goto addr_3016_55;
    tmp32_97 = pos + 3;
    pos = tmp32_97;
    *reinterpret_cast<unsigned char*>(&r12_33) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12_33) | *reinterpret_cast<unsigned char*>(&esi26));
    if (*reinterpret_cast<unsigned char*>(&r12_33)) 
        goto addr_3102_88;
    rdi98 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) - 8);
    r14_85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) + 0x90);
    eax99 = fun_2500(rdi98, r14_85);
    rax62 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<uint64_t>(r13_36) + 8);
    if (eax99) 
        goto addr_3040_56;
    rdi100 = *rax62;
    eax101 = fun_2500(rdi100, r14_85);
    if (eax101) 
        goto addr_3053_86;
    *reinterpret_cast<uint32_t*>(&rcx31) = reinterpret_cast<uint1_t>(v102 < v103);
    *reinterpret_cast<int32_t*>(&rcx31 + 4) = 0;
    goto addr_2cc9_44;
    addr_30a8_65:
    eax51 = rdi35->f2;
    goto addr_2e0c_57;
    addr_2f4e_66:
    if (*reinterpret_cast<signed char*>(&eax49) != 0x6e) 
        goto addr_3016_55;
    eax63 = rdi35->f2;
    goto addr_2f5a_62;
}