byte * find_int(byte *param_1)

{
  byte bVar1;
  byte bVar2;
  ushort *puVar3;
  ushort **ppuVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  byte *pbVar7;
  byte *pbVar8;
  
  ppuVar4 = __ctype_b_loc();
  puVar3 = *ppuVar4;
  pbVar8 = param_1;
  while (bVar1 = *pbVar8, (*(byte *)(puVar3 + bVar1) & 1) != 0) {
    pbVar8 = pbVar8 + 1;
  }
  if (bVar1 == 0x2b) {
    pbVar7 = pbVar8 + 1;
    pbVar8 = pbVar7;
  }
  else {
    pbVar7 = pbVar8 + (bVar1 == 0x2d);
  }
  if ((int)(char)*pbVar7 - 0x30U < 10) {
    bVar1 = pbVar7[1];
    while( true ) {
      pbVar7 = pbVar7 + 1;
      if (9 < (int)(char)bVar1 - 0x30U) break;
      bVar1 = pbVar7[1];
    }
    bVar2 = *(byte *)(puVar3 + ((ulong)(uint)(int)(char)bVar1 & 0xff));
    while ((bVar2 & 1) != 0) {
      bVar1 = pbVar7[1];
      pbVar7 = pbVar7 + 1;
      bVar2 = *(byte *)(puVar3 + bVar1);
    }
    if (bVar1 == 0) {
      return pbVar8;
    }
  }
  uVar5 = quote(param_1);
  uVar6 = dcgettext(0,"invalid integer %s",5);
                    /* WARNING: Subroutine does not return */
  test_syntax_error(uVar6,uVar5);
}