Eq_12 find_int(Eq_12 rdi)
{
	Eq_12 r8_10 = rdi;
	Eq_9295 rsi_11[] = *fn0000000000002650();
	while (true)
	{
		uint64 rdx_15 = (uint64) *r8_10;
		byte dl_23 = (byte) rdx_15;
		byte al_30 = (byte) rdx_15;
		if ((rsi_11[rdx_15].b0000 & 0x01) == 0x00)
			break;
		r8_10 = (word64) r8_10 + 1;
	}
	Eq_12 rdx_26;
	if (dl_23 != 0x2B)
		rdx_26 = (word64) r8_10 + (uint64) ((int8) (al_30 == 0x2D));
	else
	{
		rdx_26 = (word64) r8_10 + 1;
		r8_10 = rdx_26;
	}
	struct Eq_687 * rax_162 = (word64) rdx_26 + 1 + 1;
	if ((int32) *rdx_26 <= 0x39 && (int32) (*rdx_26) >= 0x30)
	{
		Eq_664 ecx_47 = (int32) *((word64) rdx_26 + 1);
		uint64 rdx_205 = (uint64) ecx_47;
		if (ecx_47 <= 0x39 && ecx_47 >= 0x30)
		{
			do
			{
				Eq_686 ecx_57 = (int32) rax_162->b0000;
				++rax_162;
				rdx_205 = (uint64) ecx_57;
			} while (ecx_57 <= 0x39);
		}
		uint64 rcx_81 = rdx_205;
		if ((rsi_11[(uint64) (byte) rdx_205].b0000 & 0x01) != 0x00)
		{
			do
			{
				rcx_81 = (uint64) rax_162[1];
				++rax_162;
			} while ((rsi_11[rcx_81].b0000 & 0x01) != 0x00);
		}
		if ((byte) rcx_81 == 0x00)
			return r8_10;
	}
	quote();
	fn0000000000002420(0x05, "invalid integer %s", null);
	word64 rdx_206;
	word64 r8_207;
	test_syntax_error(0x00, out rdx_206, out r8_207);
	word64 r8_211;
	word32 edx_209;
	word32 ecx_208;
	word32 edi_210;
	Eq_12 rax_149;
	beyond(out rax_149, out ecx_208, out edx_209, out edi_210, out r8_211);
	return rax_149;
}