char * find_int(char * string) {
    int64_t v1 = *(int64_t *)function_2650(); // 0x2aac
    int64_t v2 = (int64_t)string;
    unsigned char v3 = *(char *)v2; // 0x2aaf
    int64_t v4 = v2 + 1;
    while (*(char *)(2 * (int64_t)v3 + v1) % 2 != 0) {
        // 0x2aaf
        v2 = v4;
        v3 = *(char *)v2;
        v4 = v2 + 1;
    }
    int64_t v5 = v3 == 43 ? v4 : v2 + (int64_t)(v3 == 45);
    char v6 = *(char *)v5; // 0x2acb
    int64_t v7; // 0x2b62
    if (v6 != 57 && (int32_t)v6 >= 57) {
        // 0x2b49
        quote(string);
        v7 = function_2420();
        test_syntax_error((char *)v7);
        return (char *)&g35;
    }
    int64_t v8 = v5 + 1; // 0x2ace
    char v9 = *(char *)v8; // 0x2ada
    int64_t v10 = v8; // 0x2ae6
    int64_t v11 = v8; // 0x2ae6
    char v12 = v9; // 0x2ae6
    if (v9 == 57 || (int32_t)v9 < 57) {
        v10++;
        char v13 = *(char *)v10; // 0x2af0
        v11 = v10;
        v12 = v13;
        while (v13 == 57 || (int32_t)v13 < 57) {
            // 0x2af0
            v10++;
            v13 = *(char *)v10;
            v11 = v10;
            v12 = v13;
        }
    }
    int64_t v14 = (int64_t)v12 & 0xffffffff;
    int64_t v15 = v11; // 0x2b09
    int64_t v16 = v14; // 0x2b09
    if (*(char *)((2 * v14 & 510) + v1) % 2 != 0) {
        v15++;
        int64_t v17 = (int64_t)*(char *)v15; // 0x2b10
        v16 = v17;
        while (*(char *)(2 * v17 + v1) % 2 != 0) {
            // 0x2b10
            v15++;
            v17 = (int64_t)*(char *)v15;
            v16 = v17;
        }
    }
    // 0x2b21
    if ((char)v16 == 0) {
        // 0x2b25
        return (char *)(v3 == 43 ? v4 : v2);
    }
    // 0x2b49
    quote(string);
    v7 = function_2420();
    test_syntax_error((char *)v7);
    return (char *)&g35;
}