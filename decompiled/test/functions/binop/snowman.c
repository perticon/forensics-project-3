uint32_t binop(struct s0* rdi, struct s0* rsi, ...) {
    uint32_t r12d3;
    uint32_t eax4;
    int32_t eax5;
    int32_t eax6;
    int32_t eax7;
    int32_t eax8;
    int32_t eax9;
    int32_t eax10;
    int32_t eax11;
    int32_t eax12;
    int32_t eax13;

    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 61)) 
        goto addr_28c0_2;
    r12d3 = 1;
    if (!rdi->f1) 
        goto addr_28ab_4;
    addr_28c0_2:
    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0));
    if (eax4 == 33 && (rdi->f1 == 61 && !rdi->f2)) {
        return 1;
    }
    if (eax4 != 61) 
        goto addr_28d6_7;
    if (rdi->f1 == 61) 
        goto addr_29d2_9;
    addr_28d6_7:
    r12d3 = 1;
    eax5 = fun_24f0(rdi, "-nt");
    if (eax5 && ((eax6 = fun_24f0(rdi, "-ot"), !!eax6) && ((eax7 = fun_24f0(rdi, "-ef"), !!eax7) && ((eax8 = fun_24f0(rdi, "-eq"), !!eax8) && ((eax9 = fun_24f0(rdi, "-ne"), !!eax9) && ((eax10 = fun_24f0(rdi, "-lt"), !!eax10) && ((eax11 = fun_24f0(rdi, "-le"), !!eax11) && (eax12 = fun_24f0(rdi, "-gt"), !!eax12)))))))) {
        eax13 = fun_24f0(rdi, "-ge");
        *reinterpret_cast<unsigned char*>(&r12d3) = reinterpret_cast<uint1_t>(eax13 == 0);
        goto addr_28ab_4;
    }
    addr_29d2_9:
    r12d3 = 1;
    if (!rdi->f2) {
        addr_28ab_4:
        return r12d3;
    } else {
        goto addr_28d6_7;
    }
}