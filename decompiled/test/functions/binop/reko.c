uint32 binop(Eq_12 rdi)
{
	uint32 r12d_116;
	if (*rdi == 0x3D)
	{
		r12d_116 = 0x01;
		if (*((word64) rdi + 1) == 0x00)
			return r12d_116;
	}
	word32 eax_23 = (word32) *rdi;
	if (eax_23 == 33 && (*((word64) rdi + 1) == 0x3D && *((word64) rdi + 2) == 0x00))
		return 0x01;
	if (eax_23 == 0x3D && *((word64) rdi + 1) == 0x3D)
	{
		r12d_116 = 0x01;
		if (*((word64) rdi + 2) == 0x00)
			return r12d_116;
	}
	r12d_116 = 0x01;
	if (fn00000000000024F0("-nt", rdi) != 0x00 && (fn00000000000024F0("-ot", rdi) != 0x00 && (fn00000000000024F0("-ef", rdi) != 0x00 && (fn00000000000024F0("-eq", rdi) != 0x00 && (fn00000000000024F0("-ne", rdi) != 0x00 && (fn00000000000024F0("-lt", rdi) != 0x00 && (fn00000000000024F0("-le", rdi) != 0x00 && fn00000000000024F0("-gt", rdi) != 0x00)))))))
		r12d_116 = (uint32) (int8) (fn00000000000024F0("-ge", rdi) == 0x00);
	return r12d_116;
}