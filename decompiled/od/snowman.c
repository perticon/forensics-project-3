
uint64_t n_specs = 0;

uint64_t n_specs_allocated = 0;

struct s0 {
    int32_t f0;
    void** f4;
};

struct s0* spec = reinterpret_cast<struct s0*>(0);

struct s1 {
    int32_t f0;
    int32_t f4;
    int64_t f8;
    void** f10;
    signed char[7] pad24;
    unsigned char f18;
    signed char[3] pad28;
    int32_t f1c;
};

struct s0* x2nrealloc();

int32_t g13130 = 0;

void*** fun_24d0();

void** fun_2550(void** rdi, ...);

int32_t g13160 = 0;

int32_t g13140 = 0;

void** quote(void** rdi, void** rsi, ...);

void** fun_2530();

uint32_t fun_2720();

uint32_t g131a0 = 0;

int64_t g13184 = 0;

uint32_t g13190 = 0;

void fun_27f0();

void fun_25d0(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, ...);

int64_t fun_2440();

/* decode_format_string.part.0 */
signed char decode_format_string_part_0(void** rdi, void** rsi) {
    void** r12_3;
    int1_t zf4;
    uint64_t rax5;
    void** v6;
    uint32_t r9d7;
    int1_t below_or_equal8;
    struct s0* rdi9;
    struct s1* rbp10;
    void** rsi11;
    struct s0* rax12;
    uint64_t rdx13;
    int32_t ecx14;
    void** rbx15;
    int32_t r15d16;
    int32_t edx17;
    int32_t r14d18;
    int64_t rax19;
    int64_t r9_20;
    int64_t r8_21;
    void** r8_22;
    uint32_t r9d23;
    int32_t eax24;
    unsigned char al25;
    void* rax26;
    void** rbx27;
    uint64_t rax28;
    int1_t zf29;
    void*** rax30;
    void** r15_31;
    void** rdi32;
    void** rax33;
    uint32_t eax34;
    void*** rcx35;
    uint32_t eax36;
    uint32_t eax37;
    uint32_t eax38;
    int64_t r14_39;
    uint64_t r8_40;
    int1_t less_or_equal41;
    void** rdi42;
    uint32_t ecx43;
    void** rbx44;
    int64_t rcx45;
    uint32_t eax46;
    void** rdi47;
    void** rax48;
    int64_t rax49;

    r12_3 = rdi;
    zf4 = *reinterpret_cast<void***>(rdi) == 0;
    rax5 = n_specs;
    v6 = rdi;
    if (zf4) {
        addr_5620_2:
        r9d7 = 1;
    } else {
        while (1) {
            below_or_equal8 = n_specs_allocated <= rax5;
            rdi9 = spec;
            if (!below_or_equal8) {
                rbp10 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rdi9) + (rax5 + rax5 * 4) * 8);
                if (!rbp10) 
                    goto addr_5828_5;
                *reinterpret_cast<uint32_t*>(&rsi11) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3));
                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rsi11) == 97) 
                    goto addr_51c6_7; else 
                    goto addr_5104_8;
            }
            rax12 = x2nrealloc();
            rdx13 = n_specs;
            *reinterpret_cast<uint32_t*>(&rsi11) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3));
            *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
            spec = rax12;
            rbp10 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(rax12) + (rdx13 + rdx13 * 4) * 8);
            if (*reinterpret_cast<signed char*>(&rsi11) != 97) {
                addr_5104_8:
                ecx14 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rsi11 + 0xffffffffffffff9d));
                if (*reinterpret_cast<unsigned char*>(&ecx14) > 21) 
                    goto addr_5138_10;
            } else {
                addr_51c6_7:
                rbx15 = r12_3 + 1;
                r15d16 = 3;
                edx17 = 5;
                r14d18 = 1;
                rax19 = 0x3c30;
                goto addr_51e3_11;
            }
            r9_20 = 1 << *reinterpret_cast<unsigned char*>(&ecx14);
            r8_21 = r9_20;
            *reinterpret_cast<uint32_t*>(&r8_22) = *reinterpret_cast<uint32_t*>(&r8_21) & 0x241002;
            *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&r8_22)) 
                goto addr_5308_13;
            r9d23 = *reinterpret_cast<uint32_t*>(&r9_20) & 1;
            if (r9d23) {
                rbx15 = r12_3 + 1;
                r15d16 = 3;
                edx17 = 6;
                r14d18 = 1;
                rax19 = 0x45f0;
            } else {
                if (*reinterpret_cast<unsigned char*>(&ecx14) != 3) 
                    goto addr_5138_10;
                eax24 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_3 + 1));
                if (*reinterpret_cast<signed char*>(&eax24) == 70) 
                    goto addr_55d0_18; else 
                    goto addr_523e_19;
            }
            addr_51e3_11:
            rbp10->f8 = rax19;
            rbp10->f4 = r14d18;
            rbp10->f0 = edx17;
            rbp10->f1c = r15d16;
            al25 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx15) == 0x7a);
            rbp10->f18 = al25;
            *reinterpret_cast<uint32_t*>(&rax26) = al25;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
            rbx27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx15) + reinterpret_cast<uint64_t>(rax26));
            if (rbx27 == r12_3) 
                goto addr_5809_20;
            rax28 = n_specs;
            rax5 = rax28 + 1;
            zf29 = *reinterpret_cast<void***>(rbx27) == 0;
            n_specs = rax5;
            if (zf29) 
                goto addr_5620_2;
            r12_3 = rbx27;
            continue;
            addr_55d0_18:
            r14d18 = g13130;
            rbx15 = r12_3 + 2;
            addr_553c_23:
            rax30 = fun_24d0();
            *reinterpret_cast<int32_t*>(&r15_31) = 1;
            rdi32 = *rax30;
            if (*reinterpret_cast<void***>(rdi32)) {
                rax33 = fun_2550(rdi32);
                r15_31 = rax33;
            }
            if (r14d18 == 7) {
                r15d16 = *reinterpret_cast<int32_t*>(&r15_31) + 23;
                rax19 = 0x43b0;
                edx17 = 4;
                goto addr_51e3_11;
            } else {
                if (r14d18 != 8) {
                    if (r14d18 != 6) 
                        goto addr_2800_29;
                    r15d16 = *reinterpret_cast<int32_t*>(&r15_31) + 14;
                    rax19 = 0x44c0;
                    edx17 = 4;
                    goto addr_51e3_11;
                } else {
                    r15d16 = *reinterpret_cast<int32_t*>(&r15_31) + 28;
                    rax19 = 0x4290;
                    edx17 = 4;
                    goto addr_51e3_11;
                }
            }
            addr_523e_19:
            if (*reinterpret_cast<signed char*>(&eax24) == 76) {
                r14d18 = g13160;
                rbx15 = r12_3 + 2;
                goto addr_553c_23;
            } else {
                if (*reinterpret_cast<signed char*>(&eax24) == 68) {
                    r14d18 = g13140;
                    rbx15 = r12_3 + 2;
                    goto addr_553c_23;
                } else {
                    eax34 = eax24 - 48;
                    rsi11 = r12_3 + 1;
                    rbx15 = r12_3 + 2;
                    rcx35 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax34)));
                    if (eax34 > 9) {
                        r14d18 = g13140;
                        rbx15 = rsi11;
                        goto addr_553c_23;
                    } else {
                        do {
                            r8_22 = reinterpret_cast<void**>(rcx35 + reinterpret_cast<uint64_t>(r8_22 + reinterpret_cast<unsigned char>(r8_22) * 4) * 2);
                            eax36 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx15) - 48);
                            if (eax36 > 9) 
                                break;
                            rcx35 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax36)));
                            ++rbx15;
                        } while (reinterpret_cast<unsigned char>(__intrinsic() >> 3) >= reinterpret_cast<unsigned char>(r8_22));
                        goto addr_52a3_40;
                        r14d18 = g13140;
                        if (rsi11 == rbx15) 
                            goto addr_553c_23;
                        if (reinterpret_cast<unsigned char>(r8_22) > reinterpret_cast<unsigned char>(16)) 
                            goto addr_572a_43;
                        r14d18 = *reinterpret_cast<int32_t*>(0x13120 + reinterpret_cast<unsigned char>(r8_22) * 4);
                        if (r14d18) 
                            goto addr_553c_23; else 
                            goto addr_572a_43;
                    }
                }
            }
        }
    }
    addr_5174_45:
    eax37 = r9d7;
    return *reinterpret_cast<signed char*>(&eax37);
    addr_5138_10:
    quote(v6, rsi11);
    fun_2530();
    fun_2720();
    r9d7 = 0;
    goto addr_5174_45;
    addr_5308_13:
    eax38 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 1));
    if (*reinterpret_cast<signed char*>(&eax38) == 76) {
        *reinterpret_cast<uint32_t*>(&r14_39) = g131a0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_40) = 8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
        goto addr_533e_47;
    }
    if (*reinterpret_cast<signed char*>(&eax38) > 76) {
        if (*reinterpret_cast<signed char*>(&eax38) != 83) {
            goto addr_57e8_51;
        } else {
            *reinterpret_cast<uint32_t*>(&r14_39) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&g13184) + 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_40) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
            less_or_equal41 = *reinterpret_cast<signed char*>(&rsi11) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi11) != 0x75) 
                goto addr_5348_53; else 
                goto addr_5404_54;
        }
    }
    if (*reinterpret_cast<signed char*>(&eax38) != 67) 
        goto addr_5324_56;
    *reinterpret_cast<uint32_t*>(&r14_39) = *reinterpret_cast<uint32_t*>(&g13184);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_40) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
    goto addr_533e_47;
    addr_5324_56:
    if (*reinterpret_cast<signed char*>(&eax38) == 73) {
        *reinterpret_cast<uint32_t*>(&r14_39) = g13190;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_40) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
        goto addr_533e_47;
    }
    rdi42 = r12_3 + 1;
    ecx43 = *reinterpret_cast<signed char*>(&eax38) - 48;
    if (ecx43 > 9) 
        goto addr_5866_60;
    rbx44 = r12_3 + 2;
    rcx45 = reinterpret_cast<int32_t>(ecx43);
    *reinterpret_cast<int32_t*>(&r8_40) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
    do {
        r8_40 = rcx45 + (r8_40 + r8_40 * 4) * 2;
        eax46 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx44) - 48);
        if (eax46 > 9) 
            break;
        rcx45 = reinterpret_cast<int32_t>(eax46);
        ++rbx44;
    } while (__intrinsic() >> 3 >= r8_40);
    goto addr_5683_64;
    if (rdi42 == rbx44) 
        goto addr_57e8_51;
    if (r8_40 > 8 || (*reinterpret_cast<uint32_t*>(&r14_39) = *reinterpret_cast<uint32_t*>(0x13180 + r8_40 * 4), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0, !*reinterpret_cast<uint32_t*>(&r14_39))) {
        quote(v6, rsi11);
        fun_2530();
        fun_2720();
        r9d7 = 0;
        goto addr_5174_45;
    } else {
        while (1) {
            addr_533e_47:
            less_or_equal41 = *reinterpret_cast<signed char*>(&rsi11) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi11) == 0x75) {
                addr_5404_54:
                r8_22 = reinterpret_cast<void**>("lu");
                if (static_cast<uint32_t>(r14_39 - 4) >= 2) {
                    r8_22 = reinterpret_cast<void**>("u");
                }
                fun_27f0();
                rdi47 = reinterpret_cast<void**>(&rbp10->f10);
            } else {
                addr_5348_53:
                if (!less_or_equal41) {
                    if (*reinterpret_cast<signed char*>(&rsi11) != 0x78) 
                        goto addr_586e_71;
                    if (static_cast<uint32_t>(r14_39 - 4) >= 2) 
                        goto label_73; else 
                        goto addr_54fe_74;
                } else {
                    if (*reinterpret_cast<signed char*>(&rsi11) != 100) {
                        if (*reinterpret_cast<signed char*>(&rsi11) != 0x6f) 
                            goto addr_5873_77;
                        if (static_cast<uint32_t>(r14_39 - 4) >= 2) 
                            goto label_79; else 
                            goto addr_5496_80;
                    } else {
                        r8_22 = reinterpret_cast<void**>("ld");
                        if (static_cast<uint32_t>(r14_39 - 4) >= 2) {
                            r8_22 = reinterpret_cast<void**>("d");
                        }
                        fun_27f0();
                        rdi47 = reinterpret_cast<void**>(&rbp10->f10);
                    }
                }
            }
            addr_53a4_84:
            rax48 = fun_2550(rdi47, rdi47);
            if (reinterpret_cast<unsigned char>(rax48) <= reinterpret_cast<unsigned char>(7)) 
                break;
            addr_5847_85:
            rsi11 = reinterpret_cast<void**>("src/od.c");
            fun_25d0("strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8_22, "strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8_22);
            addr_5866_60:
            addr_57e8_51:
            *reinterpret_cast<uint32_t*>(&r14_39) = g13190;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_39) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_40) = 4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
            continue;
            label_73:
            addr_54fe_74:
            *reinterpret_cast<uint32_t*>(&r8_22) = *reinterpret_cast<uint32_t*>(0xd560 + r8_40 * 4);
            *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
            fun_27f0();
            rdi47 = reinterpret_cast<void**>(&rbp10->f10);
            goto addr_53a4_84;
            label_79:
            addr_5496_80:
            *reinterpret_cast<uint32_t*>(&r8_22) = *reinterpret_cast<uint32_t*>(0xd680 + r8_40 * 4);
            *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
            fun_27f0();
            rdi47 = reinterpret_cast<void**>(&rbp10->f10);
            goto addr_53a4_84;
        }
    }
    if (*reinterpret_cast<uint32_t*>(&r14_39) <= 5) {
        *reinterpret_cast<uint32_t*>(&rax49) = *reinterpret_cast<uint32_t*>(&r14_39);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd298 + rax49 * 4) + 0xd298;
    }
    addr_2800_29:
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    addr_586e_71:
    goto addr_2800_29;
    addr_5873_77:
    goto addr_2800_29;
    addr_5683_64:
    addr_52a3_40:
    quote(v6, rsi11);
    fun_2530();
    fun_2720();
    r9d7 = 0;
    goto addr_5174_45;
    addr_5809_20:
    fun_25d0("s != next", "src/od.c", 0x3e5, "decode_format_string", r8_22);
    addr_5828_5:
    fun_25d0("tspec != NULL", "src/od.c", 0x288, "decode_one_format", r8_22);
    goto addr_5847_85;
    addr_572a_43:
    quote(v6, rsi11);
    fun_2530();
    fun_2720();
    r9d7 = *reinterpret_cast<unsigned char*>(&r9d23);
    goto addr_5174_45;
}

int64_t fun_2590(void** rdi, ...);

int32_t xstrtoumax(void** rdi, ...);

unsigned char parse_old_offset(void** rdi, void** rsi) {
    uint32_t edx3;
    void** rbp4;
    int64_t rax5;
    int32_t eax6;

    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (!*reinterpret_cast<signed char*>(&edx3)) {
        return 0;
    } else {
        rbp4 = rdi;
        if (*reinterpret_cast<signed char*>(&edx3) == 43) {
            ++rbp4;
        }
        rax5 = fun_2590(rbp4, rbp4);
        if (!rax5 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4) == 48)) {
        }
        eax6 = xstrtoumax(rbp4, rbp4);
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax6 == 0));
    }
}

int64_t g28;

void** in_stream = reinterpret_cast<void**>(0);

void** quotearg_n_style_colon();

int32_t* fun_2450();

uint32_t check_and_close(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8);

uint32_t open_next_file();

int32_t fun_2670(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_27d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t rpl_fseeko(void** rdi, ...);

void** fun_2750();

void fun_2560();

void** fun_2600();

uint32_t skip(void** rdi, void** rsi) {
    void** rsp3;
    int64_t rax4;
    int64_t v5;
    int64_t r12_6;
    void** r13_7;
    void** rbp8;
    void** r14_9;
    void** rbx10;
    void** rax11;
    int32_t* rax12;
    void** rcx13;
    void** rdx14;
    void** r8_15;
    uint32_t eax16;
    uint32_t eax17;
    int32_t eax18;
    int64_t rdi19;
    int32_t eax20;
    uint32_t v21;
    void** rax22;
    void** v23;
    void** v24;
    int32_t eax25;
    void** r13_26;
    void** rax27;
    void** rax28;
    void** eax29;
    int64_t rax30;
    void** rbp31;
    void** rdi32;
    void** eax33;
    int32_t* rax34;
    int32_t edi35;
    void** rdi36;
    int64_t v37;
    int32_t eax38;

    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0xa8);
    rax4 = g28;
    v5 = rax4;
    if (!rdi) {
        *reinterpret_cast<uint32_t*>(&r12_6) = 1;
    } else {
        r13_7 = in_stream;
        *reinterpret_cast<uint32_t*>(&r12_6) = 1;
        rbp8 = rsp3;
        if (r13_7) {
            r14_9 = rdi;
            rbx10 = rsp3 + 0x90;
            goto addr_4a30_5;
        }
        while (1) {
            addr_4b5a_6:
            fun_2530();
            fun_2720();
            while (1) {
                rax11 = quotearg_n_style_colon();
                rax12 = fun_2450();
                rcx13 = rax11;
                rdx14 = reinterpret_cast<void**>("%s");
                *reinterpret_cast<int32_t*>(&rsi) = *rax12;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r12_6) = 0;
                fun_2720();
                do {
                    addr_4b38_8:
                    eax16 = check_and_close(0, rsi, rdx14, rcx13, r8_15);
                    eax17 = open_next_file();
                    r13_7 = in_stream;
                    *reinterpret_cast<uint32_t*>(&r12_6) = *reinterpret_cast<uint32_t*>(&r12_6) & eax16 & eax17;
                    if (!r13_7) 
                        goto addr_4b5a_6;
                    addr_4a30_5:
                    eax18 = fun_2670(r13_7, rsi, rdx14, rcx13, r8_15);
                    rsi = rbp8;
                    *reinterpret_cast<int32_t*>(&rdi19) = eax18;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                    eax20 = fun_27d0(rdi19, rsi, rdx14, rcx13, r8_15);
                    if (eax20) 
                        break;
                    if ((v21 & 0xd000) == 0x8000) {
                        rax22 = v23;
                        *reinterpret_cast<int32_t*>(&rcx13) = 0x200;
                        *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
                        rdx14 = v24;
                        if (reinterpret_cast<uint64_t>(rax22 + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                            rax22 = reinterpret_cast<void**>(0x200);
                        }
                        if (reinterpret_cast<signed char>(rdx14) > reinterpret_cast<signed char>(rax22)) {
                            if (reinterpret_cast<unsigned char>(rdx14) >= reinterpret_cast<unsigned char>(r14_9)) 
                                goto addr_4bd8_14;
                            r14_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_9) - reinterpret_cast<unsigned char>(rdx14));
                            goto addr_4b38_8;
                        }
                    } else {
                        rsi = r14_9;
                        rdi = r13_7;
                        eax25 = rpl_fseeko(rdi, rdi);
                        if (!eax25) 
                            goto addr_4ac9_17;
                    }
                    *reinterpret_cast<int32_t*>(&r13_26) = 0x2000;
                    *reinterpret_cast<int32_t*>(&r13_26 + 4) = 0;
                    do {
                        *reinterpret_cast<int32_t*>(&rdx14) = 1;
                        *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rsi) = 0x2000;
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        rdi = rbx10;
                        if (reinterpret_cast<unsigned char>(r13_26) > reinterpret_cast<unsigned char>(r14_9)) {
                            r13_26 = r14_9;
                        }
                        r8_15 = in_stream;
                        rcx13 = r13_26;
                        rax27 = fun_2750();
                        r14_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_9) - reinterpret_cast<unsigned char>(rax27));
                        if (r13_26 != rax27) {
                            rax28 = in_stream;
                            eax29 = *reinterpret_cast<void***>(rax28);
                            if (*reinterpret_cast<unsigned char*>(&eax29) & 32) 
                                goto addr_4bd0_23;
                            if (*reinterpret_cast<unsigned char*>(&eax29) & 16) 
                                break;
                        }
                    } while (r14_9);
                    goto addr_4ac9_17;
                } while (r14_9);
                goto addr_4bc1_27;
            }
        }
    }
    addr_4ac9_17:
    rax30 = v5 - g28;
    if (!rax30) {
        return *reinterpret_cast<uint32_t*>(&r12_6);
    }
    fun_2560();
    rbp31 = rdi;
    *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0xffffffff);
    rdi32 = in_stream;
    if (!rdi32) 
        goto addr_4c2f_31;
    do {
        eax33 = fun_2600();
        *reinterpret_cast<void***>(rbp31) = eax33;
        if (eax33 != 0xffffffff) 
            break;
        rax34 = fun_2450();
        edi35 = *rax34;
        check_and_close(edi35, rsi, 1, rcx13, r8_15);
        open_next_file();
        rdi36 = in_stream;
    } while (rdi36);
    addr_4c69_34:
    goto v37;
    addr_4c2f_31:
    goto addr_4c69_34;
    addr_4bd0_23:
    *reinterpret_cast<uint32_t*>(&r12_6) = 0;
    goto addr_4ac9_17;
    addr_4bc1_27:
    goto addr_4ac9_17;
    addr_4bd8_14:
    rsi = r14_9;
    rdi = r13_7;
    eax38 = rpl_fseeko(rdi, rdi);
    if (eax38) {
        *reinterpret_cast<uint32_t*>(&r12_6) = 0;
    }
    goto addr_4ac9_17;
}

uint32_t read_char(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    uint32_t r12d6;
    void** rbp7;
    void** rdi8;
    void** eax9;
    int32_t* rax10;
    int32_t edi11;
    uint32_t eax12;
    uint32_t eax13;
    void** rdi14;

    r12d6 = 1;
    rbp7 = rdi;
    *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0xffffffff);
    rdi8 = in_stream;
    if (rdi8) {
        do {
            eax9 = fun_2600();
            *reinterpret_cast<void***>(rbp7) = eax9;
            if (eax9 != 0xffffffff) 
                break;
            rax10 = fun_2450();
            edi11 = *rax10;
            eax12 = check_and_close(edi11, rsi, rdx, rcx, r8);
            eax13 = open_next_file();
            rdi14 = in_stream;
            r12d6 = r12d6 & (eax12 & eax13);
        } while (rdi14);
    }
    return r12d6;
}

signed char abbreviate_duplicate_blocks = 1;

/* first.1 */
signed char first_1 = 1;

void** bytes_per_block = reinterpret_cast<void**>(0);

uint32_t fun_2610(void** rdi, void** rsi, void** rdx, ...);

/* prev_pair_equal.0 */
signed char prev_pair_equal_0 = 0;

int64_t format_address = 0;

int32_t address_pad_len = 0;

void fun_26f0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7, void** a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18);

struct s2 {
    int32_t f0;
    signed char[4] pad8;
    int64_t f8;
    void** f10;
    signed char[11] pad28;
    int32_t f1c;
    int32_t f20;
};

struct s3 {
    int32_t f0;
    signed char[20] pad24;
    signed char f18;
    signed char[3] pad28;
    int32_t f1c;
    int32_t f20;
};

void** stdout = reinterpret_cast<void**>(0);

void fun_26c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7);

void** fun_27e0(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void fun_25a0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, ...);

void fun_2490(int64_t rdi, void** rsi, void** rdx);

void write_block(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int64_t v6;
    int64_t v7;
    int64_t r15_8;
    int64_t v9;
    int64_t r14_10;
    void** r14_11;
    int64_t v12;
    int64_t r13_13;
    void** r13_14;
    int64_t v15;
    int64_t r12_16;
    int64_t v17;
    int64_t rbp18;
    int64_t v19;
    int64_t rbx20;
    int1_t zf21;
    void** v22;
    int1_t zf23;
    int1_t zf24;
    uint32_t eax25;
    int1_t zf26;
    uint64_t rbp27;
    struct s0* rdx28;
    uint64_t rax29;
    void** rbx30;
    void* r12_31;
    int64_t rax32;
    uint64_t rcx33;
    uint64_t rax34;
    int64_t v35;
    uint64_t r15_36;
    uint64_t rbx37;
    void** rdx38;
    int64_t r9_39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    struct s0* r10_43;
    void** rdx44;
    struct s2* r10_45;
    void** rcx46;
    struct s3* tmp64_47;
    int64_t rbx48;
    int64_t rax49;
    void** rdx50;
    int64_t v51;
    int64_t v52;
    int64_t v53;
    void** rax54;
    void** r15_55;
    void** rbx56;
    void** r12_57;
    void** rax58;
    int64_t rsi59;
    void** rdi60;
    void** rdi61;
    void** rax62;
    void** rdi63;
    void** rax64;
    int1_t below_or_equal65;
    int1_t zf66;

    v6 = reinterpret_cast<int64_t>(__return_address());
    v7 = r15_8;
    v9 = r14_10;
    r14_11 = rcx;
    v12 = r13_13;
    r13_14 = rsi;
    v15 = r12_16;
    v17 = rbp18;
    v19 = rbx20;
    zf21 = abbreviate_duplicate_blocks == 0;
    v22 = rdi;
    if (zf21 || ((zf23 = first_1 == 0, !zf23) || ((zf24 = bytes_per_block == rsi, !zf24) || (eax25 = fun_2610(rdx, rcx, rsi), !!eax25)))) {
        zf26 = n_specs == 0;
        prev_pair_equal_0 = 0;
        if (!zf26) {
            *reinterpret_cast<int32_t*>(&rbp27) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp27) + 4) = 0;
            do {
                rdx28 = spec;
                rax29 = rbp27 + rbp27 * 4;
                rbx30 = bytes_per_block;
                r12_31 = reinterpret_cast<void*>(rax29 * 8);
                *reinterpret_cast<int32_t*>(&rax32) = *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rdx28) + rax29 * 8 + 4);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
                rcx33 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax32 * 4)));
                rax34 = reinterpret_cast<unsigned char>(rbx30) / rcx33;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v35) + 4) = *reinterpret_cast<int32_t*>(&rax34);
                r15_36 = rax34;
                rbx37 = (reinterpret_cast<unsigned char>(rbx30) - reinterpret_cast<unsigned char>(r13_14)) / rcx33;
                if (!rbp27) {
                    format_address(v22);
                } else {
                    *reinterpret_cast<int32_t*>(&rdx38) = address_pad_len;
                    *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
                    fun_26f0(1, "%*s", rdx38, 0xec81, r8, r9_39, v35, v22, v40, v19, v17, v15, v12, v9, v7, v6, v41, v42);
                }
                r10_43 = spec;
                rdx44 = r14_11;
                r10_45 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(r10_43) + reinterpret_cast<uint64_t>(r12_31));
                *reinterpret_cast<int32_t*>(&r9_39) = r10_45->f20;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_39) + 4) = 0;
                *reinterpret_cast<int32_t*>(&r8) = r10_45->f1c;
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                rcx46 = reinterpret_cast<void**>(&r10_45->f10);
                r10_45->f8(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r15_36)), static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx37)), rdx44, rcx46, r8, r9_39);
                tmp64_47 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(r12_31) + reinterpret_cast<uint64_t>(spec));
                if (tmp64_47->f18) {
                    *reinterpret_cast<int32_t*>(&rbx48) = *reinterpret_cast<int32_t*>(&rbx37) * tmp64_47->f1c;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx48) + 4) = 0;
                    __asm__("cdq ");
                    *reinterpret_cast<int32_t*>(&rax49) = tmp64_47->f20 * *reinterpret_cast<int32_t*>(&rbx37) / *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v35) + 4);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx50) = static_cast<int32_t>(rax49 + rbx48);
                    *reinterpret_cast<int32_t*>(&rdx50 + 4) = 0;
                    fun_26f0(1, "%*s", rdx50, 0xec81, r8, r9_39, v35, v22, v51, v19, v17, v15, v12, v9, v7, v6, v52, v53);
                    *reinterpret_cast<int32_t*>(&rdx44) = 3;
                    *reinterpret_cast<int32_t*>(&rdx44 + 4) = 0;
                    rcx46 = stdout;
                    fun_26c0("  >", 1, 3, rcx46, r8, r9_39, v35);
                    if (r13_14) {
                        rax54 = fun_27e0("  >", 1, 3, rcx46, "  >", 1, 3, rcx46);
                        r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_11) + reinterpret_cast<unsigned char>(r13_14));
                        rbx56 = r14_11;
                        r12_57 = rax54;
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx56));
                            *reinterpret_cast<int32_t*>(&rcx46 + 4) = 0;
                            ++rbx56;
                            rax58 = rcx46;
                            *reinterpret_cast<uint32_t*>(&rsi59) = *reinterpret_cast<unsigned char*>(&rcx46);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                            if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(r12_57) + reinterpret_cast<unsigned char>(rcx46) * 2) + 1) & 64)) {
                                *reinterpret_cast<uint32_t*>(&rsi59) = 46;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rax58) = 46;
                            }
                            rdi60 = stdout;
                            rdx44 = *reinterpret_cast<void***>(rdi60 + 40);
                            if (reinterpret_cast<unsigned char>(rdx44) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi60 + 48))) {
                                fun_25a0(rdi60, rsi59, rdx44, rcx46, r8, rdi60, rsi59, rdx44, rcx46, r8);
                            } else {
                                rcx46 = rdx44 + 1;
                                *reinterpret_cast<void***>(rdi60 + 40) = rcx46;
                                *reinterpret_cast<void***>(rdx44) = rax58;
                            }
                        } while (rbx56 != r15_55);
                    }
                    rdi61 = stdout;
                    rax62 = *reinterpret_cast<void***>(rdi61 + 40);
                    if (reinterpret_cast<unsigned char>(rax62) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi61 + 48))) {
                        fun_25a0(rdi61, 60, rdx44, rcx46, r8, rdi61, 60, rdx44, rcx46, r8);
                    } else {
                        rdx44 = rax62 + 1;
                        *reinterpret_cast<void***>(rdi61 + 40) = rdx44;
                        *reinterpret_cast<void***>(rax62) = reinterpret_cast<void**>(60);
                    }
                }
                rdi63 = stdout;
                rax64 = *reinterpret_cast<void***>(rdi63 + 40);
                if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi63 + 48))) {
                    fun_25a0(rdi63, 10, rdx44, rcx46, r8, rdi63, 10, rdx44, rcx46, r8);
                } else {
                    *reinterpret_cast<void***>(rdi63 + 40) = rax64 + 1;
                    *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(10);
                }
                ++rbp27;
                below_or_equal65 = n_specs <= rbp27;
            } while (!below_or_equal65);
        }
    } else {
        zf66 = prev_pair_equal_0 == 0;
        if (zf66) {
            fun_2490("*", rcx, rsi);
            prev_pair_equal_0 = 1;
        }
    }
    first_1 = 0;
    return;
}

uint32_t get_lcm(void** rdi, void** rsi, ...) {
    uint64_t rax3;
    uint32_t r8d4;
    struct s0* rdx5;
    void** r9_6;
    void** r11_7;
    int64_t rax8;
    uint64_t r8_9;
    uint64_t rcx10;
    uint64_t rdi11;
    uint64_t rdx12;
    uint64_t rax13;
    uint64_t rax14;

    rax3 = n_specs;
    if (!rax3) {
        r8d4 = 1;
    } else {
        rdx5 = spec;
        r8d4 = 1;
        r9_6 = reinterpret_cast<void**>(&rdx5->f4);
        r11_7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx5) + (rax3 + rax3 * 4) * 8 + 4);
        do {
            *reinterpret_cast<void***>(&rax8) = *reinterpret_cast<void***>(r9_6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            r8_9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(r8d4)));
            rcx10 = r8_9;
            rdi11 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax8 * 4)));
            rdx12 = rdi11;
            do {
                rax13 = rcx10;
                rcx10 = rdx12;
                rdx12 = rax13 % rcx10;
            } while (rdx12);
            r9_6 = r9_6 + 40;
            rax14 = rdi11 / rcx10;
            r8d4 = *reinterpret_cast<int32_t*>(&r8_9) * *reinterpret_cast<uint32_t*>(&rax14);
        } while (r11_7 != r9_6);
    }
    return r8d4;
}

void** fun_24f0();

void** gd06a = reinterpret_cast<void**>(48);

struct s4 {
    int32_t f0;
    int32_t f4;
    int64_t f8;
    void** f10;
    signed char[7] pad24;
    unsigned char f18;
    signed char[3] pad28;
    int32_t f1c;
};

uint32_t read_block(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int64_t v6;
    int64_t rbx7;
    int1_t cf8;
    void** r13_9;
    void** rcx10;
    void** rbp11;
    uint32_t r12d12;
    void** rdi13;
    void** rbx14;
    void** rax15;
    int32_t* rax16;
    int32_t edi17;
    uint32_t eax18;
    uint32_t eax19;
    void** r12_20;
    int1_t zf21;
    uint64_t rax22;
    int1_t below_or_equal23;
    struct s0* rdi24;
    struct s4* rbp25;
    void** rsi26;
    struct s0* rax27;
    uint64_t rdx28;
    int32_t ecx29;
    void** rbx30;
    int32_t r15d31;
    int32_t edx32;
    int32_t r14d33;
    int64_t rax34;
    int64_t r9_35;
    int64_t r8_36;
    int32_t eax37;
    unsigned char al38;
    void* rax39;
    void** rbx40;
    uint64_t rax41;
    int1_t zf42;
    void*** rax43;
    void** r15_44;
    void** rdi45;
    void** rax46;
    uint32_t eax47;
    void*** rcx48;
    uint32_t eax49;
    uint32_t eax50;
    int64_t r14_51;
    uint64_t r8_52;
    int1_t less_or_equal53;
    void** rdi54;
    uint32_t ecx55;
    void** rbx56;
    int64_t rcx57;
    uint32_t eax58;
    void** rdi59;
    void** rax60;
    int64_t rax61;

    v6 = rbx7;
    if (rdi && (cf8 = reinterpret_cast<unsigned char>(bytes_per_block) < reinterpret_cast<unsigned char>(rdi), r13_9 = rdi, !cf8)) {
        rcx10 = in_stream;
        *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
        rbp11 = rdx;
        if (!rcx10) {
            r12d12 = 1;
        } else {
            *reinterpret_cast<int32_t*>(&rdi13) = 0;
            *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
            r12d12 = 1;
            while ((rbx14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) - reinterpret_cast<unsigned char>(rdi13)), rax15 = fun_24f0(), *reinterpret_cast<void***>(rbp11) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11)) + reinterpret_cast<unsigned char>(rax15)), rbx14 != rax15) && (rax16 = fun_2450(), edi17 = *rax16, eax18 = check_and_close(edi17, 1, rbx14, rcx10, r8), eax19 = open_next_file(), rcx10 = in_stream, r12d12 = r12d12 & (eax18 & eax19), !!rcx10)) {
                rdi13 = *reinterpret_cast<void***>(rbp11);
            }
        }
        return r12d12;
    }
    fun_25d0("0 < n && n <= bytes_per_block", "src/od.c", 0x50d, "read_block", r8);
    r12_20 = reinterpret_cast<void**>("0 < n && n <= bytes_per_block");
    zf21 = gd06a == 0;
    rax22 = n_specs;
    if (!zf21) 
        goto addr_50d0_10;
    addr_5620_11:
    addr_5174_12:
    goto v6;
    while (1) {
        addr_50d0_10:
        below_or_equal23 = n_specs_allocated <= rax22;
        rdi24 = spec;
        if (!below_or_equal23) {
            rbp25 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rdi24) + (rax22 + rax22 * 4) * 8);
            if (!rbp25) 
                goto addr_5828_14;
            *reinterpret_cast<uint32_t*>(&rsi26) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20));
            *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
            if (*reinterpret_cast<signed char*>(&rsi26) == 97) 
                goto addr_51c6_16; else 
                goto addr_5104_17;
        }
        rax27 = x2nrealloc();
        rdx28 = n_specs;
        *reinterpret_cast<uint32_t*>(&rsi26) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20));
        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
        spec = rax27;
        rbp25 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rax27) + (rdx28 + rdx28 * 4) * 8);
        if (*reinterpret_cast<signed char*>(&rsi26) != 97) {
            addr_5104_17:
            ecx29 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rsi26 + 0xffffffffffffff9d));
            if (*reinterpret_cast<unsigned char*>(&ecx29) > 21) 
                break;
        } else {
            addr_51c6_16:
            rbx30 = r12_20 + 1;
            r15d31 = 3;
            edx32 = 5;
            r14d33 = 1;
            rax34 = 0x3c30;
            goto addr_51e3_19;
        }
        r9_35 = 1 << *reinterpret_cast<unsigned char*>(&ecx29);
        r8_36 = r9_35;
        *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(&r8_36) & 0x241002;
        *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r8)) 
            goto addr_5308_21;
        if (*reinterpret_cast<uint32_t*>(&r9_35) & 1) {
            rbx30 = r12_20 + 1;
            r15d31 = 3;
            edx32 = 6;
            r14d33 = 1;
            rax34 = 0x45f0;
        } else {
            if (*reinterpret_cast<unsigned char*>(&ecx29) != 3) 
                break;
            eax37 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_20 + 1));
            if (*reinterpret_cast<signed char*>(&eax37) == 70) 
                goto addr_55d0_26; else 
                goto addr_523e_27;
        }
        addr_51e3_19:
        rbp25->f8 = rax34;
        rbp25->f4 = r14d33;
        rbp25->f0 = edx32;
        rbp25->f1c = r15d31;
        al38 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx30) == 0x7a);
        rbp25->f18 = al38;
        *reinterpret_cast<uint32_t*>(&rax39) = al38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
        rbx40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx30) + reinterpret_cast<uint64_t>(rax39));
        if (rbx40 == r12_20) 
            goto addr_5809_28;
        rax41 = n_specs;
        rax22 = rax41 + 1;
        zf42 = *reinterpret_cast<void***>(rbx40) == 0;
        n_specs = rax22;
        if (zf42) 
            goto addr_5620_11;
        r12_20 = rbx40;
        continue;
        addr_55d0_26:
        r14d33 = g13130;
        rbx30 = r12_20 + 2;
        addr_553c_31:
        rax43 = fun_24d0();
        *reinterpret_cast<int32_t*>(&r15_44) = 1;
        rdi45 = *rax43;
        if (*reinterpret_cast<void***>(rdi45)) {
            rax46 = fun_2550(rdi45, rdi45);
            r15_44 = rax46;
        }
        if (r14d33 == 7) {
            r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 23;
            rax34 = 0x43b0;
            edx32 = 4;
            goto addr_51e3_19;
        } else {
            if (r14d33 != 8) {
                if (r14d33 != 6) 
                    goto addr_2800_37;
                r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 14;
                rax34 = 0x44c0;
                edx32 = 4;
                goto addr_51e3_19;
            } else {
                r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 28;
                rax34 = 0x4290;
                edx32 = 4;
                goto addr_51e3_19;
            }
        }
        addr_523e_27:
        if (*reinterpret_cast<signed char*>(&eax37) == 76) {
            r14d33 = g13160;
            rbx30 = r12_20 + 2;
            goto addr_553c_31;
        } else {
            if (*reinterpret_cast<signed char*>(&eax37) == 68) {
                r14d33 = g13140;
                rbx30 = r12_20 + 2;
                goto addr_553c_31;
            } else {
                eax47 = eax37 - 48;
                rsi26 = r12_20 + 1;
                rbx30 = r12_20 + 2;
                rcx48 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax47)));
                if (eax47 > 9) {
                    r14d33 = g13140;
                    rbx30 = rsi26;
                    goto addr_553c_31;
                } else {
                    do {
                        r8 = reinterpret_cast<void**>(rcx48 + reinterpret_cast<uint64_t>(r8 + reinterpret_cast<unsigned char>(r8) * 4) * 2);
                        eax49 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx30) - 48);
                        if (eax49 > 9) 
                            break;
                        rcx48 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax49)));
                        ++rbx30;
                    } while (reinterpret_cast<unsigned char>(__intrinsic() >> 3) >= reinterpret_cast<unsigned char>(r8));
                    goto addr_52a3_48;
                    r14d33 = g13140;
                    if (rsi26 == rbx30) 
                        goto addr_553c_31;
                    if (reinterpret_cast<unsigned char>(r8) > reinterpret_cast<unsigned char>(16)) 
                        goto addr_572a_51;
                    r14d33 = *reinterpret_cast<int32_t*>(0x13120 + reinterpret_cast<unsigned char>(r8) * 4);
                    if (r14d33) 
                        goto addr_553c_31; else 
                        goto addr_572a_51;
                }
            }
        }
    }
    quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
    addr_5308_21:
    eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20 + 1));
    if (*reinterpret_cast<signed char*>(&eax50) == 76) {
        *reinterpret_cast<uint32_t*>(&r14_51) = g131a0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_52) = 8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
        goto addr_533e_55;
    }
    if (*reinterpret_cast<signed char*>(&eax50) > 76) {
        if (*reinterpret_cast<signed char*>(&eax50) != 83) {
            goto addr_57e8_59;
        } else {
            *reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&g13184) + 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_52) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
            less_or_equal53 = *reinterpret_cast<signed char*>(&rsi26) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi26) != 0x75) 
                goto addr_5348_61; else 
                goto addr_5404_62;
        }
    }
    if (*reinterpret_cast<signed char*>(&eax50) != 67) 
        goto addr_5324_64;
    *reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(&g13184);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_52) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
    goto addr_533e_55;
    addr_5324_64:
    if (*reinterpret_cast<signed char*>(&eax50) == 73) {
        *reinterpret_cast<uint32_t*>(&r14_51) = g13190;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_52) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
        goto addr_533e_55;
    }
    rdi54 = r12_20 + 1;
    ecx55 = *reinterpret_cast<signed char*>(&eax50) - 48;
    if (ecx55 > 9) 
        goto addr_5866_68;
    rbx56 = r12_20 + 2;
    rcx57 = reinterpret_cast<int32_t>(ecx55);
    *reinterpret_cast<int32_t*>(&r8_52) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
    do {
        r8_52 = rcx57 + (r8_52 + r8_52 * 4) * 2;
        eax58 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx56) - 48);
        if (eax58 > 9) 
            break;
        rcx57 = reinterpret_cast<int32_t>(eax58);
        ++rbx56;
    } while (__intrinsic() >> 3 >= r8_52);
    goto addr_5683_72;
    if (rdi54 == rbx56) 
        goto addr_57e8_59;
    if (r8_52 > 8 || (*reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(0x13180 + r8_52 * 4), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0, !*reinterpret_cast<uint32_t*>(&r14_51))) {
        quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
        fun_2530();
        fun_2720();
        goto addr_5174_12;
    } else {
        while (1) {
            addr_533e_55:
            less_or_equal53 = *reinterpret_cast<signed char*>(&rsi26) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi26) == 0x75) {
                addr_5404_62:
                r8 = reinterpret_cast<void**>("lu");
                if (static_cast<uint32_t>(r14_51 - 4) >= 2) {
                    r8 = reinterpret_cast<void**>("u");
                }
                fun_27f0();
                rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            } else {
                addr_5348_61:
                if (!less_or_equal53) {
                    if (*reinterpret_cast<signed char*>(&rsi26) != 0x78) 
                        goto addr_586e_79;
                    if (static_cast<uint32_t>(r14_51 - 4) >= 2) 
                        goto label_81; else 
                        goto addr_54fe_82;
                } else {
                    if (*reinterpret_cast<signed char*>(&rsi26) != 100) {
                        if (*reinterpret_cast<signed char*>(&rsi26) != 0x6f) 
                            goto addr_5873_85;
                        if (static_cast<uint32_t>(r14_51 - 4) >= 2) 
                            goto label_87; else 
                            goto addr_5496_88;
                    } else {
                        r8 = reinterpret_cast<void**>("ld");
                        if (static_cast<uint32_t>(r14_51 - 4) >= 2) {
                            r8 = reinterpret_cast<void**>("d");
                        }
                        fun_27f0();
                        rdi59 = reinterpret_cast<void**>(&rbp25->f10);
                    }
                }
            }
            addr_53a4_92:
            rax60 = fun_2550(rdi59, rdi59);
            if (reinterpret_cast<unsigned char>(rax60) <= reinterpret_cast<unsigned char>(7)) 
                break;
            addr_5847_93:
            rsi26 = reinterpret_cast<void**>("src/od.c");
            fun_25d0("strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8, "strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8);
            addr_5866_68:
            addr_57e8_59:
            *reinterpret_cast<uint32_t*>(&r14_51) = g13190;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_52) = 4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
            continue;
            label_81:
            addr_54fe_82:
            *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(0xd560 + r8_52 * 4);
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27f0();
            rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            goto addr_53a4_92;
            label_87:
            addr_5496_88:
            *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(0xd680 + r8_52 * 4);
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27f0();
            rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            goto addr_53a4_92;
        }
    }
    if (*reinterpret_cast<uint32_t*>(&r14_51) <= 5) {
        *reinterpret_cast<uint32_t*>(&rax61) = *reinterpret_cast<uint32_t*>(&r14_51);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax61) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd298 + rax61 * 4) + 0xd298;
    }
    addr_2800_37:
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    addr_586e_79:
    goto addr_2800_37;
    addr_5873_85:
    goto addr_2800_37;
    addr_5683_72:
    addr_52a3_48:
    quote("0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
    addr_5809_28:
    fun_25d0("s != next", "src/od.c", 0x3e5, "decode_format_string", r8);
    addr_5828_14:
    fun_25d0("tspec != NULL", "src/od.c", 0x288, "decode_one_format", r8);
    goto addr_5847_93;
    addr_572a_51:
    quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
}

void*** file_list = reinterpret_cast<void***>(0);

struct s5 {
    signed char f0;
    signed char f1;
};

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void fun_24e0();

uint32_t check_and_close(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t ebp6;
    void** rdi7;
    uint32_t eax8;
    void** rdx9;
    void*** rax10;
    int64_t rax11;
    int32_t* rax12;

    ebp6 = edi;
    rdi7 = in_stream;
    eax8 = 1;
    if (!rdi7) {
        addr_492b_2:
        rdx9 = stdout;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx9)) & 32) {
            fun_2530();
            fun_2720();
            return 0;
        } else {
            return eax8;
        }
    } else {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi7)) & 32)) {
            ebp6 = 0;
        }
        rax10 = file_list;
        if ((*reinterpret_cast<struct s5**>(rax10 - 8))->f0 != 45) 
            goto addr_4940_8;
        if (!(*reinterpret_cast<struct s5**>(rax10 - 8))->f1) 
            goto addr_4912_10;
    }
    addr_4940_8:
    rax11 = rpl_fclose(rdi7, rsi, rdx, rcx, r8);
    if (*reinterpret_cast<int32_t*>(&rax11)) {
        if (ebp6) 
            goto addr_4988_12;
        rax12 = fun_2450();
        ebp6 = *rax12;
    }
    addr_4917_14:
    eax8 = 1;
    if (ebp6) {
        addr_4988_12:
        quotearg_n_style_colon();
        fun_2720();
        eax8 = 0;
        goto addr_4920_15;
    } else {
        addr_4920_15:
        in_stream = reinterpret_cast<void**>(0);
        goto addr_492b_2;
    }
    addr_4912_10:
    fun_24e0();
    goto addr_4917_14;
}

void** input_filename = reinterpret_cast<void**>(0);

void** fun_2740();

signed char have_read_stdin = 0;

void** stdin = reinterpret_cast<void**>(0);

unsigned char limit_bytes_to_format = 0;

unsigned char flag_dump_strings = 0;

void fun_2700(void** rdi);

uint32_t open_next_file() {
    uint32_t r12d1;
    void*** rax2;
    void** rdi3;
    int1_t zf4;
    void** rax5;
    void** rax6;
    int1_t zf7;
    int1_t zf8;

    r12d1 = 1;
    do {
        rax2 = file_list;
        rdi3 = *rax2;
        input_filename = rdi3;
        if (!rdi3) 
            break;
        zf4 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi3) == 45);
        file_list = rax2 + 8;
        if (!zf4 || *reinterpret_cast<void***>(rdi3 + 1)) {
            rax5 = fun_2740();
            in_stream = rax5;
            if (rax5) 
                goto addr_4870_5;
            quotearg_n_style_colon();
            fun_2450();
            r12d1 = 0;
            fun_2720();
            rax5 = in_stream;
        } else {
            rax6 = fun_2530();
            have_read_stdin = 1;
            input_filename = rax6;
            rax5 = stdin;
            in_stream = rax5;
        }
    } while (!rax5);
    goto addr_4870_5;
    addr_4882_9:
    return r12d1;
    addr_4870_5:
    zf7 = limit_bytes_to_format == 0;
    if (!zf7 && (zf8 = flag_dump_strings == 0, zf8)) {
        fun_2700(rax5);
        goto addr_4882_9;
    }
}

int64_t fun_2540();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2540();
    if (r8d > 10) {
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xe8c0 + rax11 * 4) + 0xe8c0;
    }
}

struct s6 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void** fun_25f0(void** rdi, ...);

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2430(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s6* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    int64_t rax15;
    uint32_t r8d16;
    struct s7* rbx17;
    uint32_t r15d18;
    void** rsi19;
    void** r14_20;
    int64_t v21;
    int64_t v22;
    uint32_t r15d23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    int64_t rax30;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x7e2f;
    rax8 = fun_2450();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
        fun_2440();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x13090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xb0c1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            v7 = 0x7ebb;
            fun_25f0((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            rax15 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax15);
        }
        r8d16 = rcx->f0;
        rbx17 = reinterpret_cast<struct s7*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d18 = rcx->f4;
        rsi19 = rbx17->f0;
        r14_20 = rbx17->f8;
        v21 = rcx->f30;
        v22 = rcx->f28;
        r15d23 = r15d18 | 1;
        rax24 = quotearg_buffer_restyled(r14_20, rsi19, rsi, rdx, r8d16, r15d23, &rcx->f8, v22, v21, v7);
        if (reinterpret_cast<unsigned char>(rsi19) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx17->f0 = rsi25;
            if (r14_20 != 0x13240) {
                fun_2430(r14_20, r14_20);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx17->f8 = rax26;
            v28 = rcx->f30;
            r14_20 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d23, rsi25, v29, v28, 0x7f4a);
        }
        *rax8 = v10;
        rax30 = rax6 - g28;
        if (rax30) {
            fun_2560();
        } else {
            return r14_20;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x130a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s8 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s8* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s8* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xe869);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xe864);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xe86d);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xe860);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g12dd8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g12dd8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2413() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_2423() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_2433() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2443() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2453() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2463() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2473() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_2483() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x20a0;

void fun_2493() {
    __asm__("cli ");
    goto puts;
}

int64_t ferror = 0x20b0;

void fun_24a3() {
    __asm__("cli ");
    goto ferror;
}

int64_t reallocarray = 0x20c0;

void fun_24b3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t strtod = 0x20d0;

void fun_24c3() {
    __asm__("cli ");
    goto strtod;
}

int64_t localeconv = 0x20e0;

void fun_24d3() {
    __asm__("cli ");
    goto localeconv;
}

int64_t clearerr_unlocked = 0x20f0;

void fun_24e3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t fread_unlocked = 0x2100;

void fun_24f3() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x2110;

void fun_2503() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2120;

void fun_2513() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2130;

void fun_2523() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2140;

void fun_2533() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2150;

void fun_2543() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2160;

void fun_2553() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2170;

void fun_2563() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2180;

void fun_2573() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2190;

void fun_2583() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21a0;

void fun_2593() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x21b0;

void fun_25a3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21c0;

void fun_25b3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21d0;

void fun_25c3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21e0;

void fun_25d3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t strtof = 0x21f0;

void fun_25e3() {
    __asm__("cli ");
    goto strtof;
}

int64_t memset = 0x2200;

void fun_25f3() {
    __asm__("cli ");
    goto memset;
}

int64_t fgetc = 0x2210;

void fun_2603() {
    __asm__("cli ");
    goto fgetc;
}

int64_t memcmp = 0x2220;

void fun_2613() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2230;

void fun_2623() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2240;

void fun_2633() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2250;

void fun_2643() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2260;

void fun_2653() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2270;

void fun_2663() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2280;

void fun_2673() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2290;

void fun_2683() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22a0;

void fun_2693() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22b0;

void fun_26a3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22c0;

void fun_26b3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22d0;

void fun_26c3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22e0;

void fun_26d3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22f0;

void fun_26e3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2300;

void fun_26f3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x2310;

void fun_2703() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t strtold = 0x2320;

void fun_2713() {
    __asm__("cli ");
    goto strtold;
}

int64_t error = 0x2330;

void fun_2723() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2340;

void fun_2733() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2350;

void fun_2743() {
    __asm__("cli ");
    goto fopen;
}

int64_t __fread_unlocked_chk = 0x2360;

void fun_2753() {
    __asm__("cli ");
    goto __fread_unlocked_chk;
}

int64_t strtoumax = 0x2370;

void fun_2763() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2380;

void fun_2773() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2390;

void fun_2783() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23a0;

void fun_2793() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23b0;

void fun_27a3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23c0;

void fun_27b3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23d0;

void fun_27c3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x23e0;

void fun_27d3() {
    __asm__("cli ");
    goto fstat;
}

int64_t __ctype_b_loc = 0x23f0;

void fun_27e3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2400;

void fun_27f3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_26e0(int64_t rdi, ...);

void fun_2520(int64_t rdi, int64_t rsi);

void fun_2500(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void format_address_std(int64_t rdi, int64_t rsi);

int32_t address_base = 0;

int32_t fun_2570(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

int32_t optind = 0;

unsigned char traditional = 0;

int64_t Version = 0xe758;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

int32_t fun_2780();

void usage();

void** optarg = reinterpret_cast<void**>(0);

int32_t xstrtol_fatal(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

signed char flag_pseudo_start = 0;

void** max_bytes_to_format = reinterpret_cast<void**>(0);

void** n_bytes_to_skip = reinterpret_cast<void**>(0);

void** end_offset = reinterpret_cast<void**>(0);

void* pseudo_offset = reinterpret_cast<void*>(0);

void** xnmalloc(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** string_min = reinterpret_cast<void**>(0);

void** xmalloc();

void** x2realloc(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_2863(int32_t edi, void** rsi) {
    void** r14_3;
    void** r12_4;
    void** rbp5;
    int32_t* rbx6;
    void** rdi7;
    int64_t rax8;
    int64_t v9;
    void* rsp10;
    void** r13_11;
    void* rsp12;
    void** v13;
    signed char v14;
    uint32_t v15;
    void** v16;
    void** r8_17;
    void** rcx18;
    void** rsi19;
    int64_t rdi20;
    void** v21;
    int32_t eax22;
    void* rsp23;
    int1_t zf24;
    int1_t zf25;
    int64_t rax26;
    void** rdx27;
    int32_t r12d28;
    void** rdi29;
    int64_t rcx30;
    int64_t rax31;
    void** rdi32;
    unsigned char al33;
    int1_t zf34;
    void** rdi35;
    void** rax36;
    void** rdi37;
    unsigned char al38;
    int64_t rax39;
    void** rdi40;
    unsigned char al41;
    int32_t eax42;
    int64_t rax43;
    int64_t rdx44;
    uint32_t eax45;
    void** r8_46;
    int64_t rsi47;
    int64_t rdi48;
    int32_t eax49;
    void** r8_50;
    int64_t rsi51;
    int64_t rdi52;
    int1_t zf53;
    int1_t zf54;
    int1_t zf55;
    void** rax56;
    void** tmp64_57;
    int1_t zf58;
    void*** rax59;
    int64_t rax60;
    uint32_t eax61;
    int1_t zf62;
    uint32_t ebp63;
    void** rdi64;
    uint32_t eax65;
    int1_t zf66;
    void* rax67;
    int1_t zf68;
    uint32_t eax69;
    void* rsp70;
    void** rbp71;
    void** rax72;
    int1_t zf73;
    void** rdi74;
    int64_t rax75;
    void** rax76;
    uint64_t rax77;
    struct s0* rdx78;
    void** r11_79;
    uint64_t rdi80;
    void** rsi81;
    int64_t rax82;
    int64_t rbx83;
    uint64_t rax84;
    uint64_t rax85;
    void** r10_86;
    int64_t rax87;
    uint64_t rax88;
    void** r12_89;
    void** rax90;
    void* rsp91;
    void** rdi92;
    uint32_t ebp93;
    void** v94;
    void** rdi95;
    int64_t rax96;
    void** rax97;
    void** rsi98;
    uint32_t eax99;
    void** r15_100;
    void** v101;
    int1_t cf102;
    int1_t zf103;
    void** rdi104;
    int64_t rax105;
    uint32_t eax106;
    int1_t cf107;
    int1_t zf108;
    void** rdi109;
    int64_t rax110;
    void** rdx111;
    void** rdi112;
    void** rax113;
    void* rsp114;
    void** rbp115;
    void** r12_116;
    int1_t zf117;
    void** rax118;
    uint32_t eax119;
    int32_t r15d120;
    void** rax121;
    int1_t zf122;
    int1_t cf123;
    void** rax124;
    uint32_t eax125;
    int32_t r15d126;
    void** rax127;
    void** r15_128;
    int32_t eax129;
    int32_t eax130;
    void** rax131;
    int64_t rsi132;
    void** rax133;
    void* rsp134;
    int1_t zf135;
    int1_t cf136;
    uint32_t eax137;
    uint32_t ebp138;
    void* rsp139;
    void** r12_140;
    void* rsp141;
    void** rdi142;
    uint32_t eax143;
    int64_t rax144;
    uint32_t eax145;
    uint32_t ebp146;
    int64_t rax147;
    int1_t zf148;
    void** rdi149;
    void** rax150;
    unsigned char al151;
    int32_t* v152;
    void** v153;
    int32_t* v154;
    int64_t rax155;
    void** rdi156;
    unsigned char al157;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>(0x129c0);
    *reinterpret_cast<int32_t*>(&r12_4) = edi;
    rbp5 = rsi;
    rbx6 = reinterpret_cast<int32_t*>(0xd2b0);
    rdi7 = *reinterpret_cast<void***>(rsi);
    rax8 = g28;
    v9 = rax8;
    set_program_name(rdi7);
    fun_26e0(6, 6);
    fun_2520("coreutils", "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8 - 8 + 8);
    r13_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 32);
    fun_2500("coreutils", "/usr/local/share/locale");
    atexit(0x6220, "/usr/local/share/locale");
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
    __asm__("pxor xmm0, xmm0");
    *reinterpret_cast<unsigned char*>(&v13) = 1;
    __asm__("movaps [rip+0x10881], xmm0");
    g13184 = 0x200000001;
    __asm__("movaps [rip+0x1087c], xmm0");
    __asm__("movaps [rip+0x10815], xmm0");
    __asm__("movaps [rip+0x1081e], xmm0");
    g13190 = 3;
    g131a0 = 5;
    g13130 = 6;
    g13160 = 8;
    g13140 = 7;
    n_specs = 0;
    n_specs_allocated = 0;
    spec = reinterpret_cast<struct s0*>(0);
    format_address = reinterpret_cast<int64_t>(format_address_std);
    address_base = 8;
    address_pad_len = 7;
    flag_dump_strings = 0;
    v14 = 0;
    *reinterpret_cast<signed char*>(&v15) = 0;
    v16 = reinterpret_cast<void**>(0);
    __asm__("movaps [rip+0x1076f], xmm0");
    __asm__("movaps [rip+0x10798], xmm0");
    while (1) {
        r8_17 = r13_11;
        rcx18 = reinterpret_cast<void**>(0x129c0);
        rsi19 = rbp5;
        *reinterpret_cast<int32_t*>(&rdi20) = *reinterpret_cast<int32_t*>(&r12_4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&v21) = -1;
        eax22 = fun_2570(rdi20, rsi19, "A:aBbcDdeFfHhIij:LlN:OoS:st:vw::Xx", 0x129c0, r8_17);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        if (eax22 == -1) {
            if (0) 
                break;
            zf24 = flag_dump_strings == 0;
            if (zf24) 
                goto addr_2dfa_5;
            zf25 = n_specs == 0;
            if (!zf25) 
                goto addr_3837_7;
            addr_2dfa_5:
            rax26 = optind;
            *reinterpret_cast<uint32_t*>(&rdx27) = traditional;
            *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
            r12d28 = *reinterpret_cast<int32_t*>(&r12_4) - *reinterpret_cast<int32_t*>(&rax26);
            if (!*reinterpret_cast<signed char*>(&v15)) 
                goto addr_2e16_8;
        } else {
            if (eax22 <= 0x81) {
                if (eax22 <= 64) {
                    if (eax22 == 0xffffff7d) {
                        rdi29 = stdout;
                        rcx30 = Version;
                        version_etc(rdi29, "od", "GNU coreutils", rcx30, "Jim Meyering");
                        eax22 = fun_2780();
                        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
                    }
                    if (eax22 == 0xffffff7e) {
                        usage();
                        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                        v14 = 1;
                        *reinterpret_cast<signed char*>(&v15) = 1;
                        v16 = reinterpret_cast<void**>(32);
                        continue;
                    }
                } else {
                    *reinterpret_cast<uint32_t*>(&rax31) = eax22 - 65;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax31) > 64) 
                        goto addr_2ea1_17; else 
                        goto addr_2a05_18;
                }
            }
        }
        if (!rdx27) 
            goto addr_2f59_20;
        if (r12d28 != 2) 
            goto addr_2f11_22;
        rdi32 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax26 * 8) + 8);
        addr_32d9_24:
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 40);
        al33 = parse_old_offset(rdi32, rsi19);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        if (al33) 
            goto addr_32ee_25;
        r12d28 = 2;
        addr_32bb_27:
        zf34 = traditional == 0;
        if (zf34) 
            goto addr_2f59_20;
        rax26 = optind;
        addr_2e48_29:
        rdi35 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax26 * 8) + 8);
        rax36 = quote(rdi35, rsi19, rdi35, rsi19);
        r12_4 = rax36;
        fun_2530();
        fun_2720();
        fun_2530();
        fun_2720();
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_2ea1_17:
        usage();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        *reinterpret_cast<signed char*>(&v15) = 1;
        format_address = reinterpret_cast<int64_t>(format_address_std);
        address_base = 10;
        address_pad_len = 7;
        continue;
        addr_2f11_22:
        if (r12d28 == 3) {
            addr_32a0_31:
            rdi37 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax26 * 8) + 8);
            rsi19 = r13_11;
            al38 = parse_old_offset(rdi37, rsi19);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            if (!al38) 
                goto addr_32b5_32;
            rax39 = optind;
            rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 40);
            rdi40 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax39 * 8) + 16);
            al41 = parse_old_offset(rdi40, rsi19);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            if (al41) 
                goto addr_3720_34;
        } else {
            if (r12d28 != 1) 
                goto addr_2e3c_36; else 
                goto addr_2f25_37;
        }
        addr_32b5_32:
        r12d28 = 3;
        goto addr_32bb_27;
        addr_2e3c_36:
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        if (r12d28 <= 1) 
            goto addr_2f5b_38; else 
            goto addr_2e48_29;
        addr_2e16_8:
        if (r12d28 == 2) {
            rdi32 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax26 * 8) + 8);
            if (!rdx27 && (eax42 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi32)), *reinterpret_cast<signed char*>(&eax42) != 43)) {
                if (eax42 - 48 > 9) 
                    goto addr_2f59_20;
                goto addr_32d9_24;
            }
        } else {
            if (r12d28 == 3) {
                if (rdx27) 
                    goto addr_32a0_31; else 
                    goto addr_2f59_20;
            } else {
                if (r12d28 == 1) 
                    goto addr_3573_45;
                if (!rdx27) 
                    goto addr_2f59_20; else 
                    goto addr_2e3c_36;
            }
        }
    }
    *reinterpret_cast<uint32_t*>(&rax43) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
    addr_3200_48:
    rdx44 = v9 - g28;
    if (!rdx44) {
        return rax43;
    }
    addr_385b_50:
    fun_2560();
    fun_25d0("s != NULL", "src/od.c", 0x3d9, "decode_format_string", r8_17);
    fun_2530();
    eax45 = fun_2720();
    r8_46 = optarg;
    *reinterpret_cast<int32_t*>(&rsi47) = *reinterpret_cast<int32_t*>(&v21);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi47) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdi48) = eax45;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi48) + 4) = 0;
    eax49 = xstrtol_fatal(rdi48, rsi47, 83, r14_3, r8_46);
    r8_50 = optarg;
    *reinterpret_cast<int32_t*>(&rsi51) = *reinterpret_cast<int32_t*>(&v21);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi51) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi52) = eax49;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
    xstrtol_fatal(rdi52, rsi51, 0x77, r14_3, r8_50);
    addr_3837_7:
    fun_2530();
    fun_2720();
    goto addr_385b_50;
    addr_2f59_20:
    *reinterpret_cast<uint32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    addr_2f5b_38:
    zf53 = flag_pseudo_start == 0;
    if (!zf53) {
        zf54 = format_address == 0x39d0;
        if (zf54) {
            address_base = 8;
            address_pad_len = 7;
            format_address = 0x4f20;
        } else {
            format_address = 0x4fa0;
        }
    }
    zf55 = limit_bytes_to_format == 0;
    if (zf55 || (rax56 = max_bytes_to_format, tmp64_57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax56) + reinterpret_cast<unsigned char>(n_bytes_to_skip)), end_offset = tmp64_57, reinterpret_cast<unsigned char>(tmp64_57) >= reinterpret_cast<unsigned char>(rax56))) {
        zf58 = n_specs == 0;
        if (zf58) {
            decode_format_string_part_0("oS", rsi19);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        }
        rax59 = reinterpret_cast<void***>(0x12b60);
        if (!(reinterpret_cast<uint1_t>(r12d28 < 0) | reinterpret_cast<uint1_t>(r12d28 == 0))) {
            rax60 = optind;
            rax59 = reinterpret_cast<void***>(rbp5 + rax60 * 8);
        }
        file_list = rax59;
        eax61 = open_next_file();
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        zf62 = in_stream == 0;
        ebp63 = eax61;
        if (!zf62 && (rdi64 = n_bytes_to_skip, eax65 = skip(rdi64, rsi19), rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8), ebp63 = ebp63 & eax65, zf66 = in_stream == 0, v15 = *reinterpret_cast<unsigned char*>(&ebp63), !zf66)) {
            *reinterpret_cast<int32_t*>(&rax67) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
            zf68 = flag_pseudo_start == 0;
            if (!zf68) {
                rax67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx6) - reinterpret_cast<unsigned char>(n_bytes_to_skip));
            }
            pseudo_offset = rax67;
            eax69 = get_lcm(rdi64, rsi19);
            rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx6) = eax69;
            if (!v14) 
                goto addr_3278_67;
            rbp71 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax69)));
            if (!v16) 
                goto addr_3055_69;
            rdx27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) % reinterpret_cast<unsigned char>(rbp71));
            if (!rdx27) 
                goto addr_3083_71;
            addr_3055_69:
            rax72 = fun_2530();
            rcx18 = v16;
            *reinterpret_cast<uint32_t*>(&r8_17) = *reinterpret_cast<uint32_t*>(&rbx6);
            *reinterpret_cast<int32_t*>(&r8_17 + 4) = 0;
            rdx27 = rax72;
            fun_2720();
            rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8 - 8 + 8);
            v16 = rbp71;
            goto addr_3083_71;
        }
        while ((zf73 = have_read_stdin == 0, !zf73) && (rdi74 = stdin, rax75 = rpl_fclose(rdi74, rsi19, rdx27, rcx18, r8_17), rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8), !(*reinterpret_cast<int32_t*>(&rax75) + 1))) {
            rax76 = fun_2530();
            fun_2450();
            rdx27 = rax76;
            eax69 = fun_2720();
            rsp70 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8);
            addr_3278_67:
            if (reinterpret_cast<int32_t>(eax69) > reinterpret_cast<int32_t>(15)) {
                v16 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax69)));
            } else {
                __asm__("cdq ");
                *reinterpret_cast<uint32_t*>(&rdx27) = reinterpret_cast<uint32_t>(16 % *reinterpret_cast<int32_t*>(&rbx6));
                *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
                v16 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(16 / *reinterpret_cast<int32_t*>(&rbx6) * *reinterpret_cast<uint32_t*>(&rbx6))));
            }
            addr_3083_71:
            bytes_per_block = v16;
            rax77 = n_specs;
            if (rax77) {
                rdx78 = spec;
                r11_79 = v16;
                *reinterpret_cast<int32_t*>(&rdi80) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi80) + 4) = 0;
                rcx18 = reinterpret_cast<void**>(&rdx78->f4);
                r8_17 = reinterpret_cast<void**>(&(rdx78 + rax77 * 8)->f4);
                rsi81 = rcx18;
                do {
                    *reinterpret_cast<void***>(&rax82) = *reinterpret_cast<void***>(rsi81);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax82) + 4) = 0;
                    *reinterpret_cast<void***>(&rbx83) = *reinterpret_cast<void***>(rsi81 + 24);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx83) + 4) = 0;
                    rax84 = reinterpret_cast<unsigned char>(r11_79) / reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax82 * 4)));
                    rax85 = reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(rbx83 + 1) * *reinterpret_cast<int32_t*>(&rax84)));
                    if (rdi80 < rax85) {
                        rdi80 = rax85;
                    }
                    rsi81 = rsi81 + 40;
                } while (r8_17 != rsi81);
                r10_86 = v16;
                do {
                    *reinterpret_cast<void***>(&rax87) = *reinterpret_cast<void***>(rcx18);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                    rcx18 = rcx18 + 40;
                    rax88 = reinterpret_cast<unsigned char>(r10_86) / reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax87 * 4)));
                    *reinterpret_cast<uint32_t*>(&rdx27) = *reinterpret_cast<int32_t*>(&rdi80) - *reinterpret_cast<int32_t*>(&rax88) * *reinterpret_cast<uint32_t*>(rcx18 + 0xfffffffffffffff0);
                    *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(rcx18 + 0xfffffffffffffff4) = *reinterpret_cast<uint32_t*>(&rdx27);
                } while (r8_17 != rcx18);
            }
            *reinterpret_cast<uint32_t*>(&rbx6) = flag_dump_strings;
            if (!*reinterpret_cast<unsigned char*>(&rbx6)) {
                r12_89 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp70) + 40);
                rax90 = xnmalloc(2, v16, rdx27, rcx18, r8_17);
                rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                rdi92 = bytes_per_block;
                ebp93 = limit_bytes_to_format;
                v94 = rax90;
                r14_3 = n_bytes_to_skip;
                if (*reinterpret_cast<unsigned char*>(&ebp93)) {
                    while (rdi95 = end_offset, reinterpret_cast<unsigned char>(rdi95) > reinterpret_cast<unsigned char>(r14_3)) {
                        rdi92 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) - reinterpret_cast<unsigned char>(r14_3));
                        rdx27 = r12_89;
                        *reinterpret_cast<uint32_t*>(&v13) = *reinterpret_cast<unsigned char*>(&rbx6);
                        *reinterpret_cast<uint32_t*>(&rax96) = *reinterpret_cast<unsigned char*>(&rbx6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax96) + 4) = 0;
                        r13_11 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp91) + rax96 * 8 + 48);
                        rax97 = bytes_per_block;
                        rsi98 = r13_11;
                        if (reinterpret_cast<unsigned char>(rdi92) > reinterpret_cast<unsigned char>(rax97)) {
                            rdi92 = rax97;
                        }
                        eax99 = read_block(rdi92, rsi98, rdx27, rcx18, r8_17);
                        rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
                        r15_100 = v101;
                        ebp93 = ebp93 & eax99;
                        cf102 = reinterpret_cast<unsigned char>(r15_100) < reinterpret_cast<unsigned char>(bytes_per_block);
                        zf103 = r15_100 == bytes_per_block;
                        if (cf102) 
                            goto addr_36cf_88;
                        if (!zf103) 
                            goto addr_37f4_90;
                        *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) ^ 1;
                        rdi104 = r14_3;
                        rcx18 = r13_11;
                        *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&rbx6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                        r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r15_100));
                        rdx27 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp91) + rax105 * 8 + 48);
                        write_block(rdi104, r15_100, rdx27, rcx18, r8_17);
                        rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
                    }
                } else {
                    ebp93 = *reinterpret_cast<unsigned char*>(&v13);
                    *reinterpret_cast<uint32_t*>(&r13_11) = 0;
                    *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
                    while (rdx27 = r12_89, rcx18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp91) + *reinterpret_cast<int32_t*>(&r13_11) * 8 + 48), rsi98 = rcx18, v13 = rcx18, eax106 = read_block(rdi92, rsi98, rdx27, rcx18, r8_17), rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8), r15_100 = v101, ebp93 = ebp93 & eax106, cf107 = reinterpret_cast<unsigned char>(r15_100) < reinterpret_cast<unsigned char>(bytes_per_block), zf108 = r15_100 == bytes_per_block, !cf107) {
                        if (!zf108) 
                            goto addr_37d5_95;
                        *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) ^ 1;
                        rdi109 = r14_3;
                        r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r15_100));
                        *reinterpret_cast<uint32_t*>(&rax110) = *reinterpret_cast<unsigned char*>(&rbx6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax110) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r13_11) = *reinterpret_cast<unsigned char*>(&rbx6);
                        *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
                        rdx111 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp91) + rax110 * 8 + 48);
                        write_block(rdi109, r15_100, rdx111, v13, r8_17);
                        rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
                        rdi92 = bytes_per_block;
                    }
                    goto addr_33d6_97;
                }
            } else {
                rdi112 = string_min;
                if (reinterpret_cast<unsigned char>(rdi112) < reinterpret_cast<unsigned char>(100)) {
                    rdi112 = reinterpret_cast<void**>(100);
                }
                v101 = rdi112;
                rax113 = xmalloc();
                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp70) - 8 + 8);
                rbp115 = n_bytes_to_skip;
                r12_116 = rax113;
                while (1) {
                    addr_3148_101:
                    rsi19 = string_min;
                    addr_314f_102:
                    while ((zf117 = limit_bytes_to_format == 0, r14_3 = rsi19, zf117) || (rax118 = end_offset, reinterpret_cast<unsigned char>(rax118) >= reinterpret_cast<unsigned char>(rsi19)) && reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax118) - reinterpret_cast<unsigned char>(rsi19)) > reinterpret_cast<unsigned char>(rbp115)) {
                        if (rsi19) {
                            *reinterpret_cast<int32_t*>(&r14_3) = 0;
                            *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
                            v13 = rbp115 + 1;
                            do {
                                eax119 = read_char(r13_11, rsi19, rdx27, rcx18, r8_17);
                                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                                r15d120 = *reinterpret_cast<int32_t*>(&v21);
                                *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) & eax119;
                                rbp115 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v13) + reinterpret_cast<unsigned char>(r14_3));
                                if (r15d120 < 0) 
                                    goto addr_31da_106;
                                rax121 = fun_27e0(r13_11, rsi19, rdx27, rcx18, r13_11, rsi19, rdx27, rcx18);
                                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                                rsi19 = string_min;
                                r8_17 = rax121;
                                rcx18 = *reinterpret_cast<void***>(r8_17);
                                if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx18 + r15d120 * 2) + 1) & 64)) 
                                    goto addr_314f_102;
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_116) + reinterpret_cast<unsigned char>(r14_3)) = *reinterpret_cast<signed char*>(&r15d120);
                                ++r14_3;
                            } while (reinterpret_cast<unsigned char>(r14_3) < reinterpret_cast<unsigned char>(rsi19));
                        }
                        v13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp114) + 40);
                        while ((zf122 = limit_bytes_to_format == 0, zf122) || (cf123 = reinterpret_cast<unsigned char>(rbp115) < reinterpret_cast<unsigned char>(end_offset), cf123)) {
                            if (v101 == r14_3) {
                                rsi19 = v13;
                                rax124 = x2realloc(r12_116, rsi19, rdx27, rcx18, r8_17);
                                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                                r12_116 = rax124;
                            }
                            ++rbp115;
                            eax125 = read_char(r13_11, rsi19, rdx27, rcx18, r8_17);
                            rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                            r15d126 = *reinterpret_cast<int32_t*>(&v21);
                            *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) & eax125;
                            if (r15d126 < 0) 
                                goto addr_31da_106;
                            if (!r15d126) 
                                break;
                            rax127 = fun_27e0(r13_11, rsi19, rdx27, rcx18, r13_11, rsi19, rdx27, rcx18);
                            rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                            r8_17 = rax127;
                            rcx18 = *reinterpret_cast<void***>(r8_17);
                            if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx18 + r15d126 * 2) + 1) & 64)) 
                                goto addr_3148_101;
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_116) + reinterpret_cast<unsigned char>(r14_3)) = *reinterpret_cast<signed char*>(&r15d126);
                            ++r14_3;
                        }
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_116) + reinterpret_cast<unsigned char>(r14_3)) = 0;
                        r15_128 = r12_116;
                        format_address();
                        rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                        while (*reinterpret_cast<uint32_t*>(&rdx27) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_128)), *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0, rcx18 = stdout, eax129 = reinterpret_cast<signed char>(rdx27), *reinterpret_cast<int32_t*>(&v21) = eax129, !!eax129) {
                            eax130 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx27 + 0xfffffffffffffff9));
                            if (*reinterpret_cast<unsigned char*>(&eax130) <= 6) 
                                goto addr_3605_120;
                            rax131 = *reinterpret_cast<void***>(rcx18 + 40);
                            if (reinterpret_cast<unsigned char>(rax131) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx18 + 48))) {
                                *reinterpret_cast<uint32_t*>(&rsi132) = reinterpret_cast<unsigned char>(rdx27);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi132) + 4) = 0;
                                fun_25a0(rcx18, rsi132, rdx27, rcx18, r8_17);
                                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                            } else {
                                *reinterpret_cast<void***>(rcx18 + 40) = rax131 + 1;
                                *reinterpret_cast<void***>(rax131) = rdx27;
                            }
                            ++r15_128;
                        }
                        rax133 = *reinterpret_cast<void***>(rcx18 + 40);
                        if (reinterpret_cast<unsigned char>(rax133) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx18 + 48))) {
                            fun_25a0(rcx18, 10, rdx27, rcx18, r8_17);
                            rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
                            rsi19 = string_min;
                        } else {
                            rdx27 = rax133 + 1;
                            rsi19 = string_min;
                            *reinterpret_cast<void***>(rcx18 + 40) = rdx27;
                            *reinterpret_cast<void***>(rax133) = reinterpret_cast<void**>(10);
                        }
                    }
                    goto addr_3758_128;
                }
            }
            addr_3425_130:
            *reinterpret_cast<int32_t*>(&rsi19) = 10;
            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
            format_address(r14_3, 10, rdx27, rcx18, r8_17);
            rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
            zf135 = limit_bytes_to_format == 0;
            if (!zf135 && (cf136 = reinterpret_cast<unsigned char>(r14_3) < reinterpret_cast<unsigned char>(end_offset), !cf136)) {
                eax137 = check_and_close(0, 10, rdx27, rcx18, r8_17);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
                ebp93 = ebp93 & eax137;
            }
            ebp138 = *reinterpret_cast<unsigned char*>(&ebp93);
            fun_2430(v94, v94);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
            addr_31e5_133:
            ebp63 = v15 & ebp138 & 1;
            continue;
            addr_36cf_88:
            r13_11 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v13)));
            addr_33d6_97:
            if (r15_100) {
                get_lcm(rdi92, rsi98, rdi92, rsi98);
                rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp91) - 8 + 8);
                r12_140 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp139) + reinterpret_cast<unsigned char>(r13_11) * 8 + 48);
                fun_25f0(reinterpret_cast<unsigned char>(r12_140) + reinterpret_cast<unsigned char>(r15_100));
                rsp141 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
                rdi142 = r14_3;
                rcx18 = r12_140;
                eax143 = *reinterpret_cast<uint32_t*>(&rbx6) ^ 1;
                r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r15_100));
                *reinterpret_cast<uint32_t*>(&rax144) = *reinterpret_cast<unsigned char*>(&eax143);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax144) + 4) = 0;
                rdx27 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp141) + rax144 * 8 + 48);
                write_block(rdi142, r15_100, rdx27, rcx18, r8_17);
                rsp91 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp141) - 8 + 8);
                goto addr_3425_130;
            }
            addr_3758_128:
            fun_2430(r12_116, r12_116);
            eax145 = check_and_close(0, rsi19, rdx27, rcx18, r8_17);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8 - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) & eax145;
            addr_31e2_135:
            ebp138 = *reinterpret_cast<unsigned char*>(&rbx6);
            goto addr_31e5_133;
            addr_31da_106:
            fun_2430(r12_116, r12_116);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
            goto addr_31e2_135;
        }
    } else {
        addr_3813_136:
        fun_2530();
        fun_2720();
        goto addr_3837_7;
    }
    ebp146 = ebp63 ^ 1;
    *reinterpret_cast<uint32_t*>(&rax43) = *reinterpret_cast<unsigned char*>(&ebp146);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
    goto addr_3200_48;
    addr_37f4_90:
    fun_25d0("n_bytes_read == bytes_per_block", "src/od.c", 0x57c, "dump", r8_17);
    goto addr_3813_136;
    addr_37d5_95:
    fun_25d0("n_bytes_read == bytes_per_block", "src/od.c", 0x58a, "dump", r8_17);
    goto addr_37f4_90;
    addr_3605_120:
    *reinterpret_cast<uint32_t*>(&rax147) = *reinterpret_cast<unsigned char*>(&eax130);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax147) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd3b4 + rax147 * 4) + 0xd3b4;
    addr_32ee_25:
    rdx27 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
    zf148 = traditional == 0;
    rdi149 = *reinterpret_cast<void***>(rbp5 + reinterpret_cast<unsigned char>(rdx27) * 8);
    rax150 = rdx27;
    if (!zf148) {
        rsi19 = r13_11;
        al151 = parse_old_offset(rdi149, rsi19);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        if (al151) {
            rbx6 = v152;
            rbp5 = rbp5 + 16;
            r12d28 = 0;
            flag_pseudo_start = 1;
            n_bytes_to_skip = v21;
            goto addr_2f5b_38;
        } else {
            rax150 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
            goto addr_330a_141;
        }
    } else {
        addr_330a_141:
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        r12d28 = 1;
        n_bytes_to_skip = v153;
        rdx27 = *reinterpret_cast<void***>(rbp5 + reinterpret_cast<unsigned char>(rax150) * 8);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + reinterpret_cast<unsigned char>(rax150) * 8) + 8) = rdx27;
        rbp5 = rbp5 + 8;
        goto addr_2f5b_38;
    }
    addr_3720_34:
    flag_pseudo_start = 1;
    r12d28 = 1;
    rbx6 = v154;
    n_bytes_to_skip = v21;
    rax155 = optind;
    rdx27 = *reinterpret_cast<void***>(rbp5 + rax155 * 8);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5 + rax155 * 8) + 16) = rdx27;
    rbp5 = rbp5 + 16;
    goto addr_2f5b_38;
    addr_2f25_37:
    rdi156 = *reinterpret_cast<void***>(rbp5 + rax26 * 8);
    addr_2f2a_142:
    rsi19 = r13_11;
    al157 = parse_old_offset(rdi156, rsi19);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    if (!al157) {
        addr_3589_143:
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        r12d28 = 1;
        goto addr_2f5b_38;
    } else {
        rbp5 = rbp5 + 8;
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        r12d28 = 0;
        n_bytes_to_skip = v21;
        goto addr_2f5b_38;
    }
    addr_3573_45:
    rdi156 = *reinterpret_cast<void***>(rbp5 + rax26 * 8);
    if (rdx27) 
        goto addr_2f2a_142;
    if (*reinterpret_cast<void***>(rdi156) == 43) 
        goto addr_2f2a_142; else 
        goto addr_3589_143;
    addr_2a05_18:
    goto rbx6[rax31] + reinterpret_cast<int64_t>(rbx6);
}

int64_t __libc_start_main = 0;

void fun_38e3() {
    __asm__("cli ");
    __libc_start_main(0x2860, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x13008;

void fun_2410(int64_t rdi);

int64_t fun_3983() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2410(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_39c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_39d3() {
    __asm__("cli ");
    return;
}

void fun_2620(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void fun_3a63(void** rdi, signed char sil) {
    void** rcx3;
    int64_t rax4;
    int64_t v5;
    uint64_t rax6;
    void** rsi7;
    void** r8_8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** r8_12;
    void** rax13;
    void** rsi14;
    int64_t rax15;

    __asm__("cli ");
    rcx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = reinterpret_cast<uint64_t>(static_cast<int64_t>(address_pad_len));
    rsi7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 56 + 23);
    r8_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) - rax6);
    eax9 = address_base;
    if (eax9 == 10) {
        do {
            --rsi7;
            rdx10 = reinterpret_cast<void**>(__intrinsic() >> 3);
            rax11 = rcx3;
            rcx3 = rdx10;
        } while (reinterpret_cast<unsigned char>(rax11) > reinterpret_cast<unsigned char>(9));
    } else {
        if (eax9 == 16) {
            rdx10 = reinterpret_cast<void**>("0123456789abcdef");
            do {
                --rsi7;
                rcx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx3) >> 4);
            } while (rcx3);
            if (reinterpret_cast<unsigned char>(r8_8) < reinterpret_cast<unsigned char>(rsi7)) 
                goto addr_3ab9_9; else 
                goto addr_3b4f_10;
        } else {
            if (eax9 == 8) {
                do {
                    --rsi7;
                    rcx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx3) >> 3);
                } while (rcx3);
            }
        }
    }
    if (reinterpret_cast<unsigned char>(r8_8) >= reinterpret_cast<unsigned char>(rsi7)) {
        addr_3b4f_10:
        r8_12 = rsi7;
    } else {
        addr_3ab9_9:
        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) - reinterpret_cast<unsigned char>(r8_8));
        rax13 = fun_25f0(r8_8, r8_8);
        r8_12 = rax13;
    }
    rsi14 = stdout;
    fun_2620(r8_12, rsi14, rdx10, rcx3, r8_12, r8_12, rsi14, rdx10, rcx3, r8_12);
    rax15 = v5 - g28;
    if (rax15) {
        fun_2560();
    } else {
        return;
    }
}

void xprintf(void** rdi, ...);

void fun_3c33(uint64_t rdi, uint64_t rsi, int64_t rdx) {
    uint64_t v4;
    int64_t rax5;
    int64_t v6;
    uint64_t r12_7;
    int64_t rbp8;
    int64_t rbx9;
    uint32_t edx10;
    int64_t rax11;

    __asm__("cli ");
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    if (rdi > rsi) {
        r12_7 = rdi;
        rbp8 = rdx;
        rbx9 = rdx;
        do {
            ++rbx9;
            edx10 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx9 - 1)) & 0x7f;
            if (*reinterpret_cast<unsigned char*>(&edx10) != 0x7f) {
                if (*reinterpret_cast<unsigned char*>(&edx10) <= 32) {
                }
            }
            xprintf("%*s");
        } while (v4 < r12_7 - rbx9 + rbp8);
    }
    rax11 = v6 - g28;
    if (rax11) {
        fun_2560();
    } else {
        return;
    }
}

signed char input_swap = 0;

void fun_3d43(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    void* rsp7;
    uint64_t v8;
    int64_t rax9;
    int64_t v10;
    void** r13_11;
    uint64_t r12_12;
    void* rbp13;
    int1_t zf14;
    void* rdx15;
    int64_t rax16;

    __asm__("cli ");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    v8 = rsi;
    rax9 = g28;
    v10 = rax9;
    if (rdi > rsi) {
        r13_11 = rcx;
        r12_12 = rdi;
        rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 40);
        do {
            --r12_12;
            zf14 = input_swap == 0;
            if (!zf14) {
                rdx15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 32);
                do {
                    rdx15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx15) + 1);
                } while (rbp13 != rdx15);
            }
            xprintf(r13_11);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        } while (r12_12 > v8);
    }
    rax16 = v10 - g28;
    if (rax16) {
        fun_2560();
    } else {
        return;
    }
}

void fun_3e43(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    void* rsp7;
    uint64_t v8;
    int64_t rax9;
    int64_t v10;
    void** r13_11;
    uint64_t r12_12;
    void* rbp13;
    int1_t zf14;
    void* rdx15;
    int64_t rax16;

    __asm__("cli ");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    v8 = rsi;
    rax9 = g28;
    v10 = rax9;
    if (rdi > rsi) {
        r13_11 = rcx;
        r12_12 = rdi;
        rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 40);
        do {
            --r12_12;
            zf14 = input_swap == 0;
            if (!zf14) {
                rdx15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 32);
                do {
                    rdx15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx15) + 1);
                } while (rbp13 != rdx15);
            }
            xprintf(r13_11);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        } while (r12_12 > v8);
    }
    rax16 = v10 - g28;
    if (rax16) {
        fun_2560();
    } else {
        return;
    }
}

void fun_3f43(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    uint64_t v7;
    void** r12_8;
    uint64_t rbp9;
    int1_t zf10;

    __asm__("cli ");
    v7 = rsi;
    if (rdi > rsi) {
        r12_8 = rcx;
        rbp9 = rdi;
        do {
            --rbp9;
            zf10 = input_swap == 0;
            if (zf10) {
            }
            xprintf(r12_8, r12_8);
        } while (rbp9 > v7);
    }
    return;
}

void fun_4013(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    uint64_t v7;
    void** r12_8;
    uint64_t rbp9;
    int1_t zf10;

    __asm__("cli ");
    v7 = rsi;
    if (rdi > rsi) {
        r12_8 = rcx;
        rbp9 = rdi;
        do {
            --rbp9;
            zf10 = input_swap == 0;
            if (zf10) {
            }
            xprintf(r12_8);
        } while (rbp9 > v7);
    }
    return;
}

void fun_40c3(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    uint64_t v7;
    void** r12_8;
    uint64_t rbp9;
    int1_t zf10;

    __asm__("cli ");
    v7 = rsi;
    if (rdi > rsi) {
        r12_8 = rcx;
        rbp9 = rdi;
        do {
            --rbp9;
            zf10 = input_swap == 0;
            if (zf10) {
            }
            xprintf(r12_8);
        } while (rbp9 > v7);
    }
    return;
}

void fun_4173(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    uint64_t v7;
    uint64_t v8;
    void** r12_9;
    int64_t r15_10;

    __asm__("cli ");
    v7 = rsi;
    if (rdi > rsi) {
        v8 = rdx + rdi;
        r12_9 = rcx;
        r15_10 = rdx;
        do {
            ++r15_10;
            xprintf(r12_9);
        } while (v7 < v8 - r15_10);
    }
    return;
}

void fun_4203(uint64_t rdi, uint64_t rsi, int64_t rdx, void** rcx, int32_t r8d, int32_t r9d) {
    uint64_t v7;
    uint64_t v8;
    void** r12_9;
    int64_t r15_10;

    __asm__("cli ");
    v7 = rsi;
    if (rdi > rsi) {
        v8 = rdx + rdi;
        r12_9 = rcx;
        r15_10 = rdx;
        do {
            ++r15_10;
            xprintf(r12_9);
        } while (v7 < v8 - r15_10);
    }
    return;
}

void ldtoastr(void* rdi, int64_t rsi);

void fun_4293(uint64_t rdi, uint64_t rsi, int64_t rdx) {
    void* rsp4;
    uint64_t v5;
    int64_t rax6;
    int64_t v7;
    uint64_t r13_8;
    void* rbx9;
    int1_t zf10;
    void* rax11;
    int64_t rax12;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    if (rdi > rsi) {
        r13_8 = rdi;
        rbx9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 48);
        do {
            --r13_8;
            zf10 = input_swap == 0;
            if (zf10) {
                __asm__("fld tword [r15]");
            } else {
                rax11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) + 32);
                do {
                    rax11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax11) + 1);
                } while (rbx9 != rax11);
                __asm__("fld tword [rsp+0x20]");
            }
            __asm__("fstp tword [rsp]");
            ldtoastr(rbx9, 45);
            xprintf("%*s", "%*s");
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 16 - 8 + 8 - 8 + 8 + 8 + 8);
        } while (r13_8 > v5);
    }
    rax12 = v7 - g28;
    if (rax12) {
        fun_2560();
    } else {
        return;
    }
}

void dtoastr(void* rdi, int64_t rsi);

void fun_43b3(void** rdi, void** rsi, int64_t rdx) {
    void*** rsp4;
    void** v5;
    void** v6;
    int32_t v7;
    int32_t r8d8;
    int64_t rax9;
    int64_t v10;
    uint64_t rax11;
    int32_t r9d12;
    int32_t r12d13;
    int32_t r9d14;
    uint64_t v15;
    uint64_t rbx16;
    void** r14_17;
    void* r13_18;
    uint64_t rax19;
    int32_t ebp20;
    int1_t zf21;
    void* rax22;
    int64_t rax23;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68);
    v5 = rdi;
    v6 = rsi;
    v7 = r8d8;
    rax9 = g28;
    v10 = rax9;
    if (reinterpret_cast<unsigned char>(rdi) > reinterpret_cast<unsigned char>(rsi)) {
        rax11 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r9d12));
        r12d13 = r9d14;
        v15 = rax11;
        rbx16 = reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffff) * rax11;
        r14_17 = rdi;
        r13_18 = reinterpret_cast<void*>(rsp4 + 48);
        do {
            --r14_17;
            rax19 = rbx16 / reinterpret_cast<unsigned char>(v5);
            ebp20 = r12d13 - *reinterpret_cast<int32_t*>(&rax19) + v7;
            zf21 = input_swap == 0;
            r12d13 = *reinterpret_cast<int32_t*>(&rax19);
            if (zf21) {
                *reinterpret_cast<void***>(rdi) = *reinterpret_cast<void***>(rsi);
            } else {
                rax22 = reinterpret_cast<void*>(rsp4 + 40);
                do {
                    rax22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax22) + 1);
                } while (r13_18 != rax22);
                *reinterpret_cast<void***>(rdi) = *reinterpret_cast<void***>(rsi);
            }
            dtoastr(r13_18, 40);
            *reinterpret_cast<int32_t*>(&rsi) = ebp20;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rdi = reinterpret_cast<void**>("%*s");
            xprintf("%*s");
            rsp4 = rsp4 - 8 + 8 - 8 + 8;
            rbx16 = rbx16 - v15;
        } while (reinterpret_cast<unsigned char>(r14_17) > reinterpret_cast<unsigned char>(v6));
    }
    rax23 = v10 - g28;
    if (rax23) {
        fun_2560();
    } else {
        return;
    }
}

void ftoastr(void* rdi, int64_t rsi);

void fun_44c3(uint64_t rdi, uint64_t rsi, int64_t rdx) {
    uint64_t v4;
    int64_t rax5;
    int64_t v6;
    uint64_t r14_7;
    void* r13_8;
    int1_t zf9;
    int64_t rax10;

    __asm__("cli ");
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    if (rdi > rsi) {
        r14_7 = rdi;
        r13_8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 + 32);
        do {
            --r14_7;
            zf9 = input_swap == 0;
            if (zf9) {
                __asm__("movss xmm0, [r12]");
            } else {
                __asm__("movd xmm0, r11d");
            }
            ftoastr(r13_8, 31);
            xprintf("%*s", "%*s");
        } while (r14_7 > v4);
    }
    rax10 = v6 - g28;
    if (rax10) {
        fun_2560();
    } else {
        return;
    }
}

void fun_45f3(void** rdi, void** rsi, unsigned char* rdx) {
    void** v4;
    unsigned char* v5;
    int32_t v6;
    int32_t r8d7;
    int64_t rax8;
    int64_t v9;
    int64_t rax10;
    uint64_t rax11;
    int32_t r9d12;
    int32_t r12d13;
    int32_t r9d14;
    void** rbp15;
    uint64_t v16;
    unsigned char* r13_17;
    uint64_t r14_18;
    uint32_t r8d19;
    int32_t ebx20;
    uint64_t rax21;
    void** rdx22;
    void** rcx23;
    void** rax24;
    void** rax25;
    uint64_t r11_26;
    int64_t rax27;
    int64_t rbx28;

    __asm__("cli ");
    v4 = rsi;
    v5 = rdx;
    v6 = r8d7;
    rax8 = g28;
    v9 = rax8;
    if (reinterpret_cast<unsigned char>(rdi) <= reinterpret_cast<unsigned char>(rsi)) {
        addr_471f_2:
        rax10 = v9 - g28;
        if (rax10) {
            fun_2560();
        } else {
            return;
        }
    } else {
        rax11 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r9d12));
        r12d13 = r9d14;
        rbp15 = rdi;
        v16 = rax11;
        r13_17 = rdx;
        r14_18 = reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffff) * rax11;
        do {
            r8d19 = *r13_17;
            ebx20 = r12d13;
            rax21 = r14_18 / reinterpret_cast<unsigned char>(rbp15);
            ++r13_17;
            *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<unsigned char*>(&r8d19);
            *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
            r12d13 = *reinterpret_cast<int32_t*>(&rax21);
            if (*reinterpret_cast<unsigned char*>(&r8d19) <= 13) 
                break;
            rax24 = fun_27e0(rdi, rsi, rdx22, rcx23);
            rcx23 = reinterpret_cast<void**>("%03o");
            rax25 = *reinterpret_cast<void***>(rax24);
            if (*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax25 + reinterpret_cast<unsigned char>(rdx22) * 2) + 1) & 64) {
                rcx23 = reinterpret_cast<void**>("%c");
            }
            fun_27f0();
            r11_26 = rax21;
            *reinterpret_cast<int32_t*>(&rax27) = v6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbx28) = ebx20 - *reinterpret_cast<int32_t*>(&r11_26);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx28) + 4) = 0;
            rdi = reinterpret_cast<void**>("%*s");
            *reinterpret_cast<int32_t*>(&rsi) = static_cast<int32_t>(rbx28 + rax27);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            xprintf("%*s", "%*s");
            r14_18 = r14_18 - v16;
        } while (reinterpret_cast<unsigned char>(v4) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp15) - reinterpret_cast<uint64_t>(r13_17) + reinterpret_cast<uint64_t>(v5)));
        goto addr_471f_2;
    }
    goto *reinterpret_cast<int32_t*>(0xd260 + reinterpret_cast<unsigned char>(rdx22) * 4) + 0xd260;
}

void fun_4f23(int64_t rdi, int32_t esi, void** rdx, void** rcx, void** r8) {
    int32_t ebx6;
    void** rdi7;
    void** rax8;
    void** rdi9;
    void** rax10;

    __asm__("cli ");
    ebx6 = esi;
    rdi7 = stdout;
    rax8 = *reinterpret_cast<void***>(rdi7 + 40);
    if (reinterpret_cast<unsigned char>(rax8) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi7 + 48))) {
        fun_25a0(rdi7, 40, rdx, rcx, r8);
    } else {
        *reinterpret_cast<void***>(rdi7 + 40) = rax8 + 1;
        *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(40);
    }
    format_address_std(rdi, 41);
    if (*reinterpret_cast<void***>(&ebx6)) {
        rdi9 = stdout;
        rax10 = *reinterpret_cast<void***>(rdi9 + 40);
        if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi9 + 48))) {
            goto fun_25a0;
        } else {
            *reinterpret_cast<void***>(rdi9 + 40) = rax10 + 1;
            *reinterpret_cast<void***>(rax10) = *reinterpret_cast<void***>(&ebx6);
        }
    }
    return;
}

void fun_4fa3(int64_t rdi, int32_t esi) {
    __asm__("cli ");
    format_address_std(rdi, 32);
}

void** program_name = reinterpret_cast<void**>(0);

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

int32_t fun_2460(void** rdi, void** rsi, void** rdx, ...);

void** stderr = reinterpret_cast<void**>(0);

void fun_27a0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void*** a8, int64_t a9, int64_t a10, int64_t a11, void*** a12);

void fun_5883(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** r8_4;
    int64_t r9_5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** r12_24;
    void** rax25;
    void** r12_26;
    void** rax27;
    void** r12_28;
    void** rax29;
    void** r12_30;
    void** rax31;
    void** r12_32;
    void** rax33;
    void** r12_34;
    void** rax35;
    uint32_t ecx36;
    uint32_t ecx37;
    int1_t zf38;
    void** rax39;
    int64_t r9_40;
    void** rax41;
    int32_t eax42;
    void** rax43;
    void** r13_44;
    int64_t r9_45;
    void** rax46;
    int64_t r9_47;
    void** rax48;
    int32_t eax49;
    void** rax50;
    int64_t r9_51;
    void** r14_52;
    void** rax53;
    void** rax54;
    int64_t r9_55;
    void** rax56;
    void** rdi57;
    void** r8_58;
    void** r9_59;
    int64_t v60;
    void*** v61;
    int64_t v62;
    int64_t v63;
    int64_t v64;
    void*** v65;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2530();
            r8_4 = r12_2;
            fun_26f0(1, rax3, r12_2, r12_2, r8_4, r9_5, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            r12_6 = stdout;
            rax7 = fun_2530();
            fun_2620(rax7, r12_6, 5, r12_2, r8_4, rax7, r12_6, 5, r12_2, r8_4);
            r12_8 = stdout;
            rax9 = fun_2530();
            fun_2620(rax9, r12_8, 5, r12_2, r8_4, rax9, r12_8, 5, r12_2, r8_4);
            r12_10 = stdout;
            rax11 = fun_2530();
            fun_2620(rax11, r12_10, 5, r12_2, r8_4, rax11, r12_10, 5, r12_2, r8_4);
            r12_12 = stdout;
            rax13 = fun_2530();
            fun_2620(rax13, r12_12, 5, r12_2, r8_4, rax13, r12_12, 5, r12_2, r8_4);
            r12_14 = stdout;
            rax15 = fun_2530();
            fun_2620(rax15, r12_14, 5, r12_2, r8_4, rax15, r12_14, 5, r12_2, r8_4);
            r12_16 = stdout;
            rax17 = fun_2530();
            fun_2620(rax17, r12_16, 5, r12_2, r8_4, rax17, r12_16, 5, r12_2, r8_4);
            r12_18 = stdout;
            rax19 = fun_2530();
            fun_2620(rax19, r12_18, 5, r12_2, r8_4, rax19, r12_18, 5, r12_2, r8_4);
            r12_20 = stdout;
            rax21 = fun_2530();
            fun_2620(rax21, r12_20, 5, r12_2, r8_4, rax21, r12_20, 5, r12_2, r8_4);
            r12_22 = stdout;
            rax23 = fun_2530();
            fun_2620(rax23, r12_22, 5, r12_2, r8_4, rax23, r12_22, 5, r12_2, r8_4);
            r12_24 = stdout;
            rax25 = fun_2530();
            fun_2620(rax25, r12_24, 5, r12_2, r8_4, rax25, r12_24, 5, r12_2, r8_4);
            r12_26 = stdout;
            rax27 = fun_2530();
            fun_2620(rax27, r12_26, 5, r12_2, r8_4, rax27, r12_26, 5, r12_2, r8_4);
            r12_28 = stdout;
            rax29 = fun_2530();
            fun_2620(rax29, r12_28, 5, r12_2, r8_4, rax29, r12_28, 5, r12_2, r8_4);
            r12_30 = stdout;
            rax31 = fun_2530();
            fun_2620(rax31, r12_30, 5, r12_2, r8_4, rax31, r12_30, 5, r12_2, r8_4);
            r12_32 = stdout;
            rax33 = fun_2530();
            fun_2620(rax33, r12_32, 5, r12_2, r8_4, rax33, r12_32, 5, r12_2, r8_4);
            r12_34 = stdout;
            rax35 = fun_2530();
            fun_2620(rax35, r12_34, 5, r12_2, r8_4, rax35, r12_34, 5, r12_2, r8_4);
            do {
                if (1) 
                    break;
                ecx36 = __cxa_finalize;
            } while (0x6f != ecx36 || ((ecx37 = g1, 100 != ecx37) || (zf38 = g2 == 0, !zf38)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax39 = fun_2530();
                fun_26f0(1, rax39, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_4, r9_40, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                rax41 = fun_26e0(5);
                if (!rax41 || (eax42 = fun_2460(rax41, "en_", 3, rax41, "en_", 3), !eax42)) {
                    rax43 = fun_2530();
                    r12_2 = reinterpret_cast<void**>("od");
                    r13_44 = reinterpret_cast<void**>(" invocation");
                    fun_26f0(1, rax43, "https://www.gnu.org/software/coreutils/", "od", r8_4, r9_45, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                } else {
                    r12_2 = reinterpret_cast<void**>("od");
                    goto addr_5d63_9;
                }
            } else {
                rax46 = fun_2530();
                fun_26f0(1, rax46, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_4, r9_47, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                rax48 = fun_26e0(5);
                if (!rax48 || (eax49 = fun_2460(rax48, "en_", 3, rax48, "en_", 3), !eax49)) {
                    addr_5c66_11:
                    rax50 = fun_2530();
                    r13_44 = reinterpret_cast<void**>(" invocation");
                    fun_26f0(1, rax50, "https://www.gnu.org/software/coreutils/", "od", r8_4, r9_51, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
                    if (!reinterpret_cast<int1_t>(r12_2 == "od")) {
                        r13_44 = reinterpret_cast<void**>(0xec81);
                    }
                } else {
                    addr_5d63_9:
                    r14_52 = stdout;
                    rax53 = fun_2530();
                    fun_2620(rax53, r14_52, 5, "https://www.gnu.org/software/coreutils/", r8_4, rax53, r14_52, 5, "https://www.gnu.org/software/coreutils/", r8_4);
                    goto addr_5c66_11;
                }
            }
            rax54 = fun_2530();
            fun_26f0(1, rax54, r12_2, r13_44, r8_4, r9_55, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities");
            addr_58d9_14:
            fun_2780();
        }
    } else {
        rax56 = fun_2530();
        rdi57 = stderr;
        fun_27a0(rdi57, 1, rax56, r12_2, r8_58, r9_59, v60, v61, v62, v63, v64, v65);
        goto addr_58d9_14;
    }
}

void fun_5d93() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_5da3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2550(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2460(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_5e23_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_5e70_6; else 
                    continue;
            } else {
                rax18 = fun_2550(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_5ea0_8;
                if (v16 == -1) 
                    goto addr_5e5e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_5e23_5;
            } else {
                eax19 = fun_2610(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_5e23_5;
            }
            addr_5e5e_10:
            v16 = rbx15;
            goto addr_5e23_5;
        }
    }
    addr_5e85_16:
    return v12;
    addr_5e70_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_5e85_16;
    addr_5ea0_8:
    v12 = rbx15;
    goto addr_5e85_16;
}

int32_t fun_2640();

int64_t fun_5eb3(int64_t rdi, int64_t* rsi) {
    int64_t* rbp3;
    int64_t rbx4;
    int32_t eax5;

    __asm__("cli ");
    if (!*rsi) {
        addr_5ef8_2:
        return -1;
    } else {
        rbp3 = rsi;
        *reinterpret_cast<int32_t*>(&rbx4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
        do {
            eax5 = fun_2640();
            if (!eax5) 
                break;
            ++rbx4;
        } while (rbp3[rbx4]);
        goto addr_5ef8_2;
    }
    return rbx4;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_5f13(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2530();
    } else {
        fun_2530();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_2720;
}

void fun_5fa3(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void*** v8;
    void*** r12_9;
    void** r12_10;
    int64_t v11;
    int64_t rbp12;
    void** rbp13;
    int64_t v14;
    int64_t rbx15;
    void** r14_16;
    void*** v17;
    void** rax18;
    void** rsi19;
    void** r15_20;
    int64_t rbx21;
    uint32_t eax22;
    void** rax23;
    void** rdi24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    void** rdi28;
    int64_t v29;
    int64_t v30;
    void** rdi31;
    void** rax32;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    v11 = rbp12;
    rbp13 = rsi;
    v14 = rbx15;
    r14_16 = stderr;
    v17 = rdi;
    rax18 = fun_2530();
    rsi19 = r14_16;
    fun_2620(rax18, rsi19, 5, rcx, r8);
    r15_20 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx21) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
    if (r15_20) {
        do {
            if (!rbx21 || (rsi19 = rbp13, eax22 = fun_2610(r13_7, rsi19, r12_10, r13_7, rsi19, r12_10), !!eax22)) {
                r13_7 = rbp13;
                rax23 = quote(r15_20, rsi19, r15_20, rsi19);
                rdi24 = stderr;
                *reinterpret_cast<int32_t*>(&rsi19) = 1;
                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                fun_27a0(rdi24, 1, "\n  - %s", rax23, r8, r9, v25, v17, v26, v14, v11, v8);
            } else {
                rax27 = quote(r15_20, rsi19, r15_20, rsi19);
                rdi28 = stderr;
                *reinterpret_cast<int32_t*>(&rsi19) = 1;
                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                fun_27a0(rdi28, 1, ", %s", rax27, r8, r9, v29, v17, v30, v14, v11, v8);
            }
            ++rbx21;
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r12_10));
            r15_20 = v17[rbx21 * 8];
        } while (r15_20);
    }
    rdi31 = stderr;
    rax32 = *reinterpret_cast<void***>(rdi31 + 40);
    if (reinterpret_cast<unsigned char>(rax32) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi31 + 48))) {
        goto fun_25a0;
    } else {
        *reinterpret_cast<void***>(rdi31 + 40) = rax32 + 1;
        *reinterpret_cast<void***>(rax32) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(int64_t* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_60d3(int64_t rdi, int64_t rsi, int64_t* rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    int64_t r14_9;
    int64_t r13_10;
    int64_t r12_11;
    int64_t* rbp12;
    int64_t v13;
    int64_t rax14;
    int64_t rbx15;
    int32_t eax16;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_6117_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_618e_4;
        } else {
            addr_618e_4:
            return rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
    if (*rdx) {
        do {
            eax16 = fun_2640();
            if (!eax16) 
                break;
            ++rbx15;
        } while (rbp12[rbx15]);
        goto addr_6110_8;
    } else {
        goto addr_6110_8;
    }
    return rbx15;
    addr_6110_8:
    rax14 = -1;
    goto addr_6117_3;
}

struct s9 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_61a3(void** rdi, struct s9* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2610(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_6203(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_6213(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

uint32_t exit_failure = 1;

void** fun_2470(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_6223() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2450(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2530();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_62b3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2720();
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2470(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_62b3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2720();
    }
}

struct s10 {
    signed char[4] pad4;
    int32_t f4;
};

struct s11 {
    signed char[4] pad4;
    int32_t f4;
};

struct s12 {
    int32_t f0;
    int32_t f4;
};

int32_t g2e2a = 0x1fc8341;

struct s13 {
    int32_t f0;
    int32_t f4;
};

int32_t g2e2e = 0x73f840f;

struct s14 {
    int16_t f0;
    signed char f2;
    signed char f3;
    signed char f4;
};

int32_t fun_2420(struct s12* rdi, int32_t* rsi, int64_t rdx, int64_t rcx, void* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11);

void fun_24c0();

int64_t fun_62d3(struct s10* rdi, struct s11* rsi, uint32_t edx, int32_t ecx) {
    int64_t r15_5;
    int32_t r13d6;
    struct s12* rdi7;
    struct s12* rbp8;
    int32_t* rbx9;
    void* rsp10;
    struct s13* rdi11;
    void* r14_12;
    int64_t rax13;
    int64_t v14;
    int64_t rax15;
    int64_t rcx16;
    signed char* rax17;
    struct s12* rdi18;
    int32_t* rsi19;
    signed char* rax20;
    int64_t rcx21;
    signed char* rax22;
    uint32_t edx23;
    int64_t rcx24;
    struct s14* rax25;
    uint32_t edx26;
    uint32_t tmp32_27;
    uint32_t edx28;
    int64_t r9_29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int32_t eax33;
    int64_t rax34;
    int64_t rax35;

    __asm__("cli ");
    __asm__("movapd xmm4, xmm0");
    __asm__("pxor xmm2, xmm2");
    *reinterpret_cast<int32_t*>(&r15_5) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    __asm__("movapd xmm1, xmm4");
    r13d6 = ecx;
    rdi7 = reinterpret_cast<struct s12*>(&rdi->f4);
    rbp8 = rdi7;
    rbx9 = &rsi->f4;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rdi7->f0 = g2e2a;
    rdi11 = reinterpret_cast<struct s13*>(&rdi7->f4);
    __asm__("xorpd xmm0, [rip+0x84b6]");
    r14_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 29);
    rax13 = g28;
    v14 = rax13;
    *reinterpret_cast<uint32_t*>(&rax15) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    __asm__("andpd xmm0, xmm1");
    __asm__("andnpd xmm1, xmm4");
    *reinterpret_cast<uint32_t*>(&rcx16) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    rax17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp10) + rax15 + 30);
    __asm__("orpd xmm0, xmm1");
    rdi11->f0 = g2e2e;
    rdi18 = reinterpret_cast<struct s12*>(&rdi11->f4);
    rsi19 = reinterpret_cast<int32_t*>(0x2e32);
    *rax17 = 43;
    rax20 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax17) + rcx16);
    *rax20 = 32;
    *reinterpret_cast<uint32_t*>(&rcx21) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
    rax22 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax20) + rcx21);
    edx23 = edx & 16;
    *rax22 = 48;
    *reinterpret_cast<uint32_t*>(&rcx24) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
    rax25 = reinterpret_cast<struct s14*>(reinterpret_cast<int64_t>(rax22) + rcx24);
    rax25->f0 = 0x2e32;
    edx26 = edx23 - (edx23 + reinterpret_cast<uint1_t>(edx23 < edx23 + reinterpret_cast<uint1_t>(edx23 < 1))) & 32;
    rax25->f2 = 42;
    tmp32_27 = edx26 + 71;
    edx28 = tmp32_27;
    __asm__("comisd xmm1, xmm0");
    rax25->f4 = 0;
    rax25->f3 = *reinterpret_cast<signed char*>(&edx28);
    if (reinterpret_cast<uint1_t>(tmp32_27 < edx26) | reinterpret_cast<uint1_t>(edx28 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_5) = 15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&r9_29) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_29) + 4) = 0;
        rdi18->f0 = *rsi19;
        rsi19 = rbx9;
        rdi18 = rbp8;
        eax33 = fun_2420(rdi18, rsi19, 1, -1, r14_12, r9_29, r15_5, 0x6404, v30, v31, v32);
        if (eax33 < 0) 
            break;
        if (*reinterpret_cast<int32_t*>(&r15_5) > 16) 
            break;
        if (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax33)) >= reinterpret_cast<uint64_t>(rbx9)) 
            goto addr_63b0_19;
        *reinterpret_cast<int32_t*>(&rsi19) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi19) + 4) = 0;
        rdi18 = rbp8;
        fun_24c0();
        __asm__("ucomisd xmm0, [rsp+0x8]");
        if (__intrinsic()) 
            goto addr_63b0_19;
        if (!0) 
            break;
        addr_63b0_19:
        *reinterpret_cast<int32_t*>(&r15_5) = *reinterpret_cast<int32_t*>(&r15_5) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    rax34 = v14 - g28;
    if (rax34) {
        fun_2560();
    } else {
        *reinterpret_cast<int32_t*>(&rax35) = eax33;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0;
        return rax35;
    }
}

int32_t fun_26b0(void** rdi);

int64_t fun_25c0(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_2510(void** rdi);

int64_t fun_6443(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t eax6;
    int32_t eax7;
    int32_t eax8;
    int64_t rdi9;
    int64_t rax10;
    int32_t eax11;
    int32_t* rax12;
    int32_t r12d13;
    int64_t rax14;

    __asm__("cli ");
    eax6 = fun_2670(rdi, rsi, rdx, rcx, r8);
    if (eax6 >= 0) {
        eax7 = fun_26b0(rdi);
        if (!(eax7 && (eax8 = fun_2670(rdi, rsi, rdx, rcx, r8), *reinterpret_cast<int32_t*>(&rdi9) = eax8, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0, rax10 = fun_25c0(rdi9), rax10 == -1) || (eax11 = rpl_fflush(rdi), eax11 == 0))) {
            rax12 = fun_2450();
            r12d13 = *rax12;
            rax14 = fun_2510(rdi);
            if (r12d13) {
                *rax12 = r12d13;
                *reinterpret_cast<int32_t*>(&rax14) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            }
            return rax14;
        }
    }
    goto fun_2510;
}

void fun_64d3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26b0(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6523(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t eax6;
    int64_t rdi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax6 = fun_2670(rdi, rsi, rdx, rcx, r8);
        *reinterpret_cast<int32_t*>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_25c0(rdi7, rdi7);
        if (rax8 == -1) {
            *reinterpret_cast<uint32_t*>(&rax9) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax8;
            *reinterpret_cast<uint32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
        return rax9;
    }
}

struct s15 {
    int16_t f0;
    signed char f2;
    signed char f3;
    signed char f4;
};

void fun_25e0();

int64_t fun_65a3(struct s12* rdi, int32_t* rsi, uint32_t edx, int32_t ecx) {
    int64_t r15_5;
    int32_t r13d6;
    struct s12* rbp7;
    int32_t* rbx8;
    void* rsp9;
    void* r14_10;
    int64_t rax11;
    int64_t v12;
    int64_t rax13;
    int64_t rcx14;
    signed char* rax15;
    struct s12* rdi16;
    int32_t* rsi17;
    signed char* rax18;
    int64_t rcx19;
    signed char* rax20;
    uint32_t edx21;
    int64_t rcx22;
    struct s15* rax23;
    uint32_t edx24;
    uint32_t tmp32_25;
    uint32_t edx26;
    int64_t r9_27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int32_t eax31;
    int64_t rax32;
    int64_t rax33;

    __asm__("cli ");
    __asm__("movaps xmm3, xmm0");
    __asm__("pxor xmm2, xmm2");
    __asm__("pxor xmm5, xmm5");
    __asm__("movaps xmm1, xmm3");
    __asm__("cvtss2sd xmm5, xmm0");
    *reinterpret_cast<int32_t*>(&r15_5) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    r13d6 = ecx;
    __asm__("cmpss xmm1, xmm2, 0x1");
    rbp7 = rdi;
    rbx8 = rsi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    __asm__("movss [rsp+0xc], xmm0");
    __asm__("xorps xmm0, [rip+0x8201]");
    r14_10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) + 29);
    rax11 = g28;
    v12 = rax11;
    *reinterpret_cast<uint32_t*>(&rax13) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    __asm__("andps xmm0, xmm1");
    __asm__("andnps xmm1, xmm3");
    *reinterpret_cast<uint32_t*>(&rcx14) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
    rax15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp9) + rax13 + 30);
    __asm__("orps xmm0, xmm1");
    rdi->f0 = g2e2a;
    rdi16 = reinterpret_cast<struct s12*>(&rdi->f4);
    rsi17 = reinterpret_cast<int32_t*>(0x2e2e);
    *rax15 = 43;
    rax18 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax15) + rcx14);
    __asm__("movss xmm1, [rip+0x81c8]");
    *rax18 = 32;
    *reinterpret_cast<uint32_t*>(&rcx19) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
    rax20 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax18) + rcx19);
    edx21 = edx & 16;
    *rax20 = 48;
    *reinterpret_cast<uint32_t*>(&rcx22) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
    rax23 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rax20) + rcx22);
    rax23->f0 = 0x2e2e;
    edx24 = edx21 - (edx21 + reinterpret_cast<uint1_t>(edx21 < edx21 + reinterpret_cast<uint1_t>(edx21 < 1))) & 32;
    rax23->f2 = 42;
    tmp32_25 = edx24 + 71;
    edx26 = tmp32_25;
    __asm__("comiss xmm1, xmm0");
    rax23->f4 = 0;
    rax23->f3 = *reinterpret_cast<signed char*>(&edx26);
    if (reinterpret_cast<uint1_t>(tmp32_25 < edx24) | reinterpret_cast<uint1_t>(edx26 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_5) = 6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&r9_27) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_27) + 4) = 0;
        rdi16->f0 = *rsi17;
        rsi17 = rbx8;
        rdi16 = rbp7;
        eax31 = fun_2420(rdi16, rsi17, 1, -1, r14_10, r9_27, r15_5, 0x66d4, v28, v29, v30);
        if (eax31 < 0) 
            break;
        if (*reinterpret_cast<int32_t*>(&r15_5) > 8) 
            break;
        if (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax31)) >= reinterpret_cast<uint64_t>(rbx8)) 
            goto addr_6680_13;
        *reinterpret_cast<int32_t*>(&rsi17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        rdi16 = rbp7;
        fun_25e0();
        __asm__("ucomiss xmm0, [rsp+0xc]");
        if (__intrinsic()) 
            goto addr_6680_13;
        if (!0) 
            break;
        addr_6680_13:
        *reinterpret_cast<int32_t*>(&r15_5) = *reinterpret_cast<int32_t*>(&r15_5) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_5) + 4) = 0;
    }
    rax32 = v12 - g28;
    if (rax32) {
        fun_2560();
    } else {
        *reinterpret_cast<int32_t*>(&rax33) = eax31;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        return rax33;
    }
}

struct s16 {
    int32_t f0;
    signed char f4;
    signed char f5;
};

void fun_2710(struct s12* rdi);

int64_t fun_6713(struct s12* rdi, int32_t* rsi, uint32_t edx, int32_t ecx) {
    int64_t v5;
    int64_t r15_6;
    int32_t r13d7;
    struct s12* rbp8;
    int32_t* rbx9;
    void* rsp10;
    int64_t rax11;
    int64_t v12;
    void* r14_13;
    int64_t rax14;
    int64_t rcx15;
    signed char* rax16;
    signed char* rax17;
    int64_t rcx18;
    signed char* rax19;
    uint32_t edx20;
    int64_t rcx21;
    struct s16* rax22;
    uint32_t edx23;
    uint32_t tmp32_24;
    uint32_t edx25;
    int64_t r9_26;
    int64_t v27;
    int64_t v28;
    int64_t v29;
    int32_t eax30;
    int64_t rax31;
    int64_t rax32;

    v5 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_6) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_6) + 4) = 0;
    r13d7 = ecx;
    rbp8 = rdi;
    rbx9 = rsi;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rax11 = g28;
    v12 = rax11;
    __asm__("fld tword [rsp+0x60]");
    r14_13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 13);
    __asm__("fchs ");
    __asm__("fld tword [rsp+0x60]");
    __asm__("fldz ");
    __asm__("fcomip st0, st1");
    __asm__("fcmovnbe st0, st1");
    __asm__("fstp st1");
    *reinterpret_cast<uint32_t*>(&rax14) = edx & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rcx15) = edx >> 1 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx15) + 4) = 0;
    rax16 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp10) + rax14 + 14);
    *rax16 = 43;
    __asm__("fld tword [rip+0x8094]");
    rax17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax16) + rcx15);
    *rax17 = 32;
    *reinterpret_cast<uint32_t*>(&rcx18) = edx >> 2 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx18) + 4) = 0;
    rax19 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax17) + rcx18);
    edx20 = edx & 16;
    *rax19 = 48;
    *reinterpret_cast<uint32_t*>(&rcx21) = edx >> 3 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
    rax22 = reinterpret_cast<struct s16*>(reinterpret_cast<int64_t>(rax19) + rcx21);
    rax22->f0 = 0x4c2a2e2a;
    edx23 = edx20 - (edx20 + reinterpret_cast<uint1_t>(edx20 < edx20 + reinterpret_cast<uint1_t>(edx20 < 1))) & 32;
    rax22->f5 = 0;
    tmp32_24 = edx23 + 71;
    edx25 = tmp32_24;
    __asm__("fcomip st0, st1");
    __asm__("fstp st0");
    rax22->f4 = *reinterpret_cast<signed char*>(&edx25);
    if (reinterpret_cast<uint1_t>(tmp32_24 < edx23) | reinterpret_cast<uint1_t>(edx25 == 0)) {
        *reinterpret_cast<int32_t*>(&r15_6) = 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_6) + 4) = 0;
    }
    while ((*reinterpret_cast<int32_t*>(&r9_26) = r13d7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_26) + 4) = 0, eax30 = fun_2420(rbp8, rbx9, 1, -1, r14_13, r9_26, r15_6, v27, v5, v28, v29), eax30 >= 0) && (*reinterpret_cast<int32_t*>(&r15_6) <= 20 && (reinterpret_cast<uint64_t>(static_cast<int64_t>(eax30)) >= reinterpret_cast<uint64_t>(rbx9) || (fun_2710(rbp8), __intrinsic())))) {
        *reinterpret_cast<int32_t*>(&r15_6) = *reinterpret_cast<int32_t*>(&r15_6) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_6) + 4) = 0;
    }
    rax31 = v12 - g28;
    if (rax31) {
        fun_2560();
    } else {
        *reinterpret_cast<int32_t*>(&rax32) = eax30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
        return rax32;
    }
}

uint64_t fun_2790(void** rdi, void* rsi, uint64_t rdx, void** rcx);

struct s17 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s17* fun_25b0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_6853(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s17* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2790("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2440();
    } else {
        rbx3 = rdi;
        rax4 = fun_25b0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2460(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 2) == 45))) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_7ff3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2450();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x13340;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_8033(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13340);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_8053(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13340);
    }
    *rdi = esi;
    return 0x13340;
}

int64_t fun_8073(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x13340);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s18 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_80b3(struct s18* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s18*>(0x13340);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s19 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s19* fun_80d3(struct s19* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s19*>(0x13340);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x280f;
    if (!rdx) 
        goto 0x280f;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x13340;
}

struct s20 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8113(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s20* r8) {
    struct s20* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s20*>(0x13340);
    }
    rax7 = fun_2450();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x8146);
    *rax7 = r15d8;
    return rax13;
}

struct s21 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8193(int64_t rdi, int64_t rsi, void*** rdx, struct s21* rcx) {
    struct s21* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s21*>(0x13340);
    }
    rax6 = fun_2450();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x81c1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x821c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_8283() {
    __asm__("cli ");
}

void** g13098 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_8293() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2430(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x13240) {
        fun_2430(rdi7);
        g13098 = reinterpret_cast<void**>(0x13240);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x13090) {
        fun_2430(r12_2);
        slotvec = reinterpret_cast<void**>(0x13090);
    }
    nslots = 1;
    return;
}

void fun_8333() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8353() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8363(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8383(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_83a3(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s6* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2815;
    rcx5 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_8433(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s6* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x281a;
    rcx6 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_84c3(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s6* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x281f;
    rcx4 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_2560();
    } else {
        return rax5;
    }
}

void** fun_8553(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s6* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2824;
    rcx5 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_85e3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s6* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xad50]");
    __asm__("movdqa xmm1, [rip+0xad58]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xad41]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_2560();
    } else {
        return rax10;
    }
}

void** fun_8683(int64_t rdi, uint32_t esi) {
    struct s6* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xacb0]");
    __asm__("movdqa xmm1, [rip+0xacb8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xaca1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_2560();
    } else {
        return rax9;
    }
}

void** fun_8723(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xac10]");
    __asm__("movdqa xmm1, [rip+0xac18]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xabf9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2560();
    } else {
        return rax3;
    }
}

void** fun_87b3(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xab80]");
    __asm__("movdqa xmm1, [rip+0xab88]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xab76]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2560();
    } else {
        return rax4;
    }
}

void** fun_8843(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s6* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2829;
    rcx5 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_88e3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s6* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xaa4a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xaa42]");
    __asm__("movdqa xmm2, [rip+0xaa4a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x282e;
    if (!rdx) 
        goto 0x282e;
    rcx6 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_8983(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s6* rcx7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa9aa]");
    __asm__("movdqa xmm1, [rip+0xa9b2]");
    __asm__("movdqa xmm2, [rip+0xa9ba]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2833;
    if (!rdx) 
        goto 0x2833;
    rcx7 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = rcx6 - g28;
    if (rdx9) {
        fun_2560();
    } else {
        return rax8;
    }
}

void** fun_8a33(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s6* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa8fa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xa8f2]");
    __asm__("movdqa xmm2, [rip+0xa8fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2838;
    if (!rsi) 
        goto 0x2838;
    rcx5 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_8ad3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s6* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa85a]");
    __asm__("movdqa xmm1, [rip+0xa862]");
    __asm__("movdqa xmm2, [rip+0xa86a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x283d;
    if (!rsi) 
        goto 0x283d;
    rcx6 = reinterpret_cast<struct s6*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void fun_8b73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8b83(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8ba3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8bc3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s22 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void*** f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    void*** f40;
};

void fun_2650(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_8be3(void** rdi, void** rsi, void** rdx, void** rcx, struct s22* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void*** v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;
    void*** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    void*** v19;
    void** rax20;
    int64_t v21;
    void*** v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    void*** v26;
    void** rax27;
    int64_t v28;
    void*** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    void*** v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    void*** rcx37;
    int64_t r15_38;
    void*** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27a0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_27a0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2530();
    fun_27a0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2650(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2530();
    fun_27a0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2650(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2530();
        fun_27a0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xef28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xef28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_9053() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s23 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_9073(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s23* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s23* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_2560();
    } else {
        return;
    }
}

void fun_9113(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_91b6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_91c0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_2560();
    } else {
        return;
    }
    addr_91b6_5:
    goto addr_91c0_7;
}

void fun_91f3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    int64_t r9_9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    void** rax21;
    void** r8_22;
    int64_t r9_23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    int64_t v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2650(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2530();
    fun_26f0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20);
    rax21 = fun_2530();
    fun_26f0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_22, r9_23, v24, __return_address(), v25, v26, v27, v28, v29, v30, v31, v32, v33, v34);
    fun_2530();
    goto fun_26f0;
}

int64_t fun_24b0();

void xalloc_die();

void fun_9293(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_2680(void** rdi, void** rsi, ...);

void fun_92d3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_92f3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9313(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_26d0(void** rdi, void** rsi, ...);

void fun_9333(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26d0(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9363(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_26d0(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9393(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_93d3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9413(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9443(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9493(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24b0();
            if (rax5) 
                break;
            addr_94dd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_94dd_5;
        rax8 = fun_24b0();
        if (rax8) 
            goto addr_94c6_9;
        if (rbx4) 
            goto addr_94dd_5;
        addr_94c6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_9523(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24b0();
            if (rax8) 
                break;
            addr_956a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_956a_5;
        rax11 = fun_24b0();
        if (rax11) 
            goto addr_9552_9;
        if (!rbx6) 
            goto addr_9552_9;
        if (r12_4) 
            goto addr_956a_5;
        addr_9552_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_95b3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_965d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_9670_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_9610_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_9636_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_9684_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_962d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_9684_14;
            addr_962d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_9684_14;
            addr_9636_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_26d0(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_9684_14;
            if (!rbp13) 
                break;
            addr_9684_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_965d_9;
        } else {
            if (!r13_6) 
                goto addr_9670_10;
            goto addr_9610_11;
        }
    }
}

int64_t fun_2630();

void fun_96b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2630();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_96e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2630();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9713() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2630();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9733() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2630();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2660(void** rdi, void** rsi, void** rdx);

void fun_9753(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

void fun_9793(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

void fun_97d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2680(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2660;
    }
}

void fun_9813(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_2550(rdi);
    rax4 = fun_2680(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

void fun_9853() {
    __asm__("cli ");
    fun_2530();
    fun_2720();
    fun_2440();
}

int32_t rpl_vprintf();

int32_t fun_24a0(void** rdi);

int64_t fun_9893() {
    int32_t eax1;
    void** rdi2;
    int32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax1 = rpl_vprintf();
    if (eax1 < 0 && (rdi2 = stdout, eax3 = fun_24a0(rdi2), !eax3)) {
        fun_2530();
        fun_2450();
        fun_2720();
    }
    *reinterpret_cast<int32_t*>(&rax4) = eax1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void xvprintf();

void fun_9903() {
    signed char al1;
    int64_t rax2;
    int64_t rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    xvprintf();
    rdx3 = rax2 - g28;
    if (rdx3) {
        fun_2560();
    } else {
        return;
    }
}

int32_t rpl_vfprintf(void** rdi);

int64_t fun_99c3(void** rdi) {
    signed char al2;
    int64_t rax3;
    int32_t eax4;
    int32_t eax5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    eax4 = rpl_vfprintf(rdi);
    if (eax4 < 0 && (eax5 = fun_24a0(rdi), !eax5)) {
        fun_2530();
        fun_2450();
        fun_2720();
    }
    rax6 = rax3 - g28;
    if (rax6) {
        fun_2560();
    } else {
        *reinterpret_cast<int32_t*>(&rax7) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

int64_t fun_9ad3(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax2 = rpl_vfprintf(rdi);
    if (eax2 < 0 && (eax3 = fun_24a0(rdi), !eax3)) {
        fun_2530();
        fun_2450();
        fun_2720();
    }
    *reinterpret_cast<int32_t*>(&rax4) = eax2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void fun_9b43(uint32_t edi, int32_t esi) {
    uint32_t ebp3;

    __asm__("cli ");
    ebp3 = exit_failure;
    if (edi > 3) {
        while (1) {
            if (edi != 4) 
                goto 0x2842;
            if (esi >= 0) {
                addr_9b89_4:
            } else {
                addr_9bdf_5:
            }
            fun_2530();
            esi = 0;
            edi = ebp3;
            fun_2720();
            fun_2440();
        }
    } else {
        if (edi <= 1) {
            if (edi != 1) {
                goto 0x2842;
            }
        }
        if (esi < 0) 
            goto addr_9bdf_5; else 
            goto addr_9b89_4;
    }
}

void** fun_2760(void** rdi);

int64_t fun_9c13(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    int64_t rax7;
    int64_t v8;
    void** rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    int64_t rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void**>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_25d0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax", r8);
        do {
            fun_2560();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_9f84_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<unsigned char>(rcx9));
                } while (!__intrinsic());
            }
            addr_9f84_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_9ccd_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_9cd5_13:
            rax15 = v8 - g28;
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2450();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_27e0(rdi, rsi, rdx, rcx);
    rcx9 = *reinterpret_cast<void***>(rax21);
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_9d0b_21;
    rsi = r15_14;
    rax24 = fun_2760(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2590(r13_18, r13_18), r8 = r8, rax26 == 0))) {
            addr_9d0b_21:
            r12d11 = 4;
            goto addr_9cd5_13;
        } else {
            addr_9d49_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2590(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void**>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xf060 + rbp33 * 4) + 0xf060;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_9d0b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_9ccd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_9ccd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2590(r13_18, r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_9d49_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_9cd5_13;
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, void** rcx);

void fseterr(void** rdi, void* rsi, uint64_t rdx, void** rcx);

int64_t fun_a043(void** rdi, uint64_t rsi, void** rdx) {
    void** rcx4;
    uint64_t rdx5;
    void* rsp6;
    int64_t rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    uint64_t rax12;
    int64_t rdx13;
    int32_t* rax14;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_a117_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = 0x7d0;
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax12 = fun_2790(rax10, 1, 0x7d0, rcx4);
        if (rax12 < 0x7d0) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_2430(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                fun_2430(rax10, rax10);
            }
            if (0) 
                goto addr_a10c_9; else 
                goto addr_a0ca_10;
        }
    }
    addr_a0cc_11:
    rdx13 = rax7 - g28;
    if (rdx13) {
        fun_2560();
    } else {
        return rax11;
    }
    addr_a10c_9:
    rax14 = fun_2450();
    *rax14 = 75;
    goto addr_a117_2;
    addr_a0ca_10:
    *reinterpret_cast<int32_t*>(&rax11) = 0x7d0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_a0cc_11;
}

void fun_a133(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto rpl_vfprintf;
}

int64_t fun_2480();

int64_t fun_a153(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int64_t rax6;
    uint32_t ebx7;
    int64_t rax8;
    int32_t* rax9;
    int32_t* rax10;

    __asm__("cli ");
    rax6 = fun_2480();
    ebx7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax8 = rpl_fclose(rdi, rsi, rdx, rcx, r8);
    if (ebx7) {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            addr_a1ae_3:
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        } else {
            rax9 = fun_2450();
            *rax9 = 0;
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            if (rax6) 
                goto addr_a1ae_3;
            rax10 = fun_2450();
            *reinterpret_cast<int32_t*>(&rax8) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax10 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    }
    return rax8;
}

void fun_a1c3(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

signed char* fun_26a0(int64_t rdi);

signed char* fun_a1d3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26a0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2580(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_a213(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2580(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_2560();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s24 {
    signed char[7] pad7;
    void** f7;
};

struct s25 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s26 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_a2a3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    int64_t rax12;
    int64_t v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    int64_t rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    int32_t* rax30;
    void** r15_31;
    int32_t* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    int32_t* rax41;
    void** rax42;
    struct s24* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    int32_t* rax55;
    struct s25* r14_56;
    struct s25* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s26* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    int32_t* rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    int32_t* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    int32_t* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = v13 - g28, !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0xb1da;
                fun_2560();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_b1da_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_b1e4_6;
                addr_b0ce_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0xb0f3, rax21 = fun_26d0(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0xb119;
                    fun_2430(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0xb13f;
                    fun_2430(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0xb165;
                    fun_2430(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_b1e4_6:
            addr_adc8_16:
            v28 = r10_16;
            addr_adcf_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0xadd4;
            rax30 = fun_2450();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_ade2_18:
            *v32 = 12;
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0xa981;
                fun_2430(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0xa999;
                fun_2430(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_a5d8_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0xa5f0;
                fun_2430(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0xa608;
            fun_2430(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_2450();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = 22;
        goto addr_a5d8_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_a5cd_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_a5cd_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_2680(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_a5cd_33:
            rax55 = fun_2450();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = 12;
            goto addr_a5d8_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_a3cc_46;
    while (1) {
        addr_ad24_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_a48f_50;
            if (r14_56->f50 != -1) 
                goto 0x2847;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_acff_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_adc8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_adc8_16;
                if (r10_16 == v10) 
                    goto addr_b014_64; else 
                    goto addr_acd3_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s25*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_ad24_47;
            addr_a3cc_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_ae80_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_ae80_73;
                if (r15_31 == v10) 
                    goto addr_ae10_79; else 
                    goto addr_a427_80;
            }
            addr_a44c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0xa462;
            fun_2660(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_ae10_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0xae18;
            rax67 = fun_2680(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_ae80_73;
            if (!r9_58) 
                goto addr_a44c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0xae57;
            rax69 = fun_2660(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_a44c_81;
            addr_a427_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0xa432;
            rax71 = fun_26d0(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_ae80_73; else 
                goto addr_a44c_81;
            addr_b014_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0xb02a;
            rax73 = fun_2680(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_b1e9_84;
            if (r12_9) 
                goto addr_b04a_86;
            r10_16 = rax73;
            goto addr_acff_55;
            addr_b04a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0xb05f;
            rax75 = fun_2660(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_acff_55;
            addr_acd3_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0xacec;
            rax77 = fun_26d0(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_adcf_17;
            r10_16 = rax77;
            goto addr_acff_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_b1da_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_b0ce_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_adc8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_b18d_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_b18d_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_adc8_16; else 
                goto addr_b197_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_b0a3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0xb1ae;
        rax80 = fun_2680(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_b0ce_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0xb1cd;
                rax82 = fun_2660(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_b0ce_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0xb0c2;
        rax84 = fun_26d0(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_adcf_17; else 
            goto addr_b0ce_7;
    }
    addr_b197_96:
    rbx19 = r13_8;
    goto addr_b0a3_98;
    addr_a48f_50:
    if (r14_56->f50 == -1) 
        goto 0x2847;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x284c;
        goto *reinterpret_cast<int32_t*>(0xf1bc + r13_87 * 4) + 0xf1bc;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0xa55f;
        fun_2660(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0xa599;
        fun_2660(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xf14c + rax95 * 4) + 0xf14c;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x2847;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s26*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x2847;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_a6d2_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_adc8_16;
    }
    addr_a6d2_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_adc8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_a6f3_142; else 
                goto addr_af82_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_af82_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_adc8_16; else 
                    goto addr_af8c_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_a6f3_142;
            }
        }
    }
    addr_a725_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0xa72f;
    rax102 = fun_2450();
    *rax102 = 0;
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x284c;
    goto *reinterpret_cast<int32_t*>(0xf174 + rax103 * 4) + 0xf174;
    addr_a6f3_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0xafe8;
        rax105 = fun_2680(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_b1e9_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0xb1ee;
            rax107 = fun_2450();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_ade2_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0xb00f;
                fun_2660(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_a725_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0xa712;
        rax110 = fun_26d0(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_adc8_16; else 
            goto addr_a725_147;
    }
    addr_af8c_145:
    rbx19 = tmp64_100;
    goto addr_a6f3_142;
    addr_ae80_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0xae85;
    rax112 = fun_2450();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_ade2_18;
}

int32_t setlocale_null_r();

int64_t fun_b223() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax3;
    }
}

int64_t fun_b2a3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_26e0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2550(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2660(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2660(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_b353() {
    __asm__("cli ");
    goto fun_26e0;
}

struct s27 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_b363(int64_t rdi, struct s27* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_b399_5;
    return 0xffffffff;
    addr_b399_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xf1e0 + rdx3 * 4) + 0xf1e0;
}

struct s28 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s29 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s30 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_b593(void** rdi, struct s28* rsi, struct s29* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s29* r15_7;
    struct s28* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    void** rax23;
    uint64_t rdi24;
    int32_t edx25;
    void** rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    void** rbx65;
    void* rsi66;
    int32_t eax67;
    void** rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    int32_t* rax90;
    void*** rax91;
    void** rdi92;
    int32_t* rax93;
    struct s30* rbx94;
    void* rdi95;
    int32_t eax96;
    struct s30* rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_b648_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_b635_7:
    return rax15;
    addr_b648_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_b6b9_11;
    } else {
        addr_b6b9_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_16 + 16)) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_b6d0_14;
        } else {
            goto addr_b6d0_14;
        }
    }
    rax23 = rax5 + 2;
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = rax23 + 0xffffffffffffffff;
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax23)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_bb98_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            ++rax23;
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        ++rax23;
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_bb98_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx26 + 2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = rcx26 + 2;
    goto addr_b6b9_11;
    addr_b6d0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xf25c + rax33 * 4) + 0xf25c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_b817_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_bf19_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_bf1e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_bf14_42;
            }
        } else {
            addr_b6fd_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_b918_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_b707_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 2));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_c104_56; else 
                        goto addr_b955_57;
                }
            } else {
                addr_b707_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_b728_60;
                } else {
                    goto addr_b728_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_bc9b_65;
    addr_b817_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_bb98_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_b83c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_b8ba_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_c1bb_75; else 
            goto addr_b863_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_bb9c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_b707_52;
        goto addr_b918_44;
    }
    addr_bf1e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_b6fd_43;
    addr_c1bb_75:
    if (v11 != rdx53) {
        fun_2430(rdx53);
        r10_4 = r10_4;
        goto addr_bfca_84;
    } else {
        goto addr_bfca_84;
    }
    addr_b863_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_2680(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_26d0(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_c1bb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2660(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_b8ba_68;
    addr_bc9b_65:
    rbx65 = rbx14 + 2;
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = rbx65 + 0xffffffffffffffff;
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx65)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_bb98_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            ++rbx65;
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        ++rbx65;
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_bb98_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = rdx68 + 2;
    goto addr_b83c_67;
    label_41:
    addr_bf14_42:
    goto addr_bf19_36;
    addr_c104_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_c12a_104;
    addr_b955_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_bb98_22:
            r8_54 = r15_7->f8;
            goto addr_bb9c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_b964_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_c012_111;
    } else {
        addr_b971_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_b9a7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_c1bb_75;
    addr_c012_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_2680(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_bfca_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_2430(rdi86);
            }
        } else {
            addr_c250_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2660(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_c06f_121;
        }
    } else {
        rax89 = fun_26d0(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_c1bb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_c250_120;
            }
        }
    }
    rax90 = fun_2450();
    *rax90 = 12;
    return 0xffffffff;
    addr_c06f_121:
    r15_7->f8 = r8_54;
    goto addr_b971_112;
    addr_b9a7_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_bb9c_80:
            if (v11 != r8_54) {
                fun_2430(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_b707_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_b707_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_2430(rdi92, rdi92);
    }
    rax93 = fun_2450();
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_b635_7;
    addr_c12a_104:
    rbx94 = reinterpret_cast<struct s30*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s30*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_bb98_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s30*>(&rbx94->pad2);
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s30*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_bb98_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_b964_107;
    addr_b728_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xf3c0 + rax105 * 4) + 0xf3c0;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xf304 + rax106 * 4) + 0xf304;
    }
}

void fun_c323() {
    __asm__("cli ");
}

void fun_c337() {
    __asm__("cli ");
    return;
}

void fun_2a0f() {
    void** rsi1;

    decode_format_string_part_0("dL", rsi1);
    goto 0x29c0;
}

void fun_2acc() {
    void** rsi1;

    decode_format_string_part_0("u4", rsi1);
    goto 0x29c0;
}

int64_t argmatch_die = 0x5d90;

int64_t __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_2b2e() {
    int64_t r9_1;
    void** rsi2;
    int64_t rax3;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    rax3 = __xargmatch_internal("--endian", rsi2, 0x12b40, 0xd428, 4, r9_1);
    if (!*reinterpret_cast<int32_t*>(0xd428 + rax3 * 4)) {
        input_swap = 0;
        goto 0x29c0;
    } else {
        if (*reinterpret_cast<int32_t*>(0xd428 + rax3 * 4) - 1) 
            goto 0x29c0;
        input_swap = 1;
        goto 0x29c0;
    }
}

void fun_2b88() {
    traditional = 1;
    goto 0x29c0;
}

void fun_2bdd() {
    abbreviate_duplicate_blocks = 0;
    goto 0x29c0;
}

void fun_2c11() {
    void** rsi1;

    decode_format_string_part_0("fF", rsi1);
    goto 0x29c0;
}

void fun_2cc4() {
    void** rsi1;

    decode_format_string_part_0("o4", rsi1);
    goto 0x29c0;
}

void fun_3623() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\f", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
}

void fun_4748() {
    goto 0x46f0;
}

void fun_5690() {
    goto 0x51e3;
}

void fun_56e0() {
    int32_t edx1;

    if (edx1) {
    }
    goto 0x51e3;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_27c0(int64_t rdi, void** rsi);

uint32_t fun_27b0(void** rdi, void** rsi);

void fun_6a85() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2530();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2530();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2550(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_6d83_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_6d83_22; else 
                            goto addr_717d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_723d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_7590_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_6d80_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_6d80_30; else 
                                goto addr_75a9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2550(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_7590_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2610(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_7590_28; else 
                            goto addr_6c2c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_76f0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_7570_40:
                        if (r11_27 == 1) {
                            addr_70fd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_76b8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_6d37_44;
                            }
                        } else {
                            goto addr_7580_46;
                        }
                    } else {
                        addr_76ff_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_70fd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_6d83_22:
                                if (v47 != 1) {
                                    addr_72d9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2550(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_7324_54;
                                    }
                                } else {
                                    goto addr_6d90_56;
                                }
                            } else {
                                addr_6d35_57:
                                ebp36 = 0;
                                goto addr_6d37_44;
                            }
                        } else {
                            addr_7564_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_76ff_47; else 
                                goto addr_756e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_70fd_41;
                        if (v47 == 1) 
                            goto addr_6d90_56; else 
                            goto addr_72d9_52;
                    }
                }
                addr_6df1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_6c88_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_6cad_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_6fb0_65;
                    } else {
                        addr_6e19_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_7668_67;
                    }
                } else {
                    goto addr_6e10_69;
                }
                addr_6cc1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_6d0c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_7668_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6d0c_81;
                }
                addr_6e10_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_6cad_64; else 
                    goto addr_6e19_66;
                addr_6d37_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_6def_91;
                if (v22) 
                    goto addr_6d4f_93;
                addr_6def_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6df1_62;
                addr_7324_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_7aab_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_7b1b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_791f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_27c0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_27b0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_741e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_6ddc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_7428_112;
                    }
                } else {
                    addr_7428_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_74f9_114;
                }
                addr_6de8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_6def_91;
                while (1) {
                    addr_74f9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_7a07_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_7466_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_7a15_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_74e7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_74e7_128;
                        }
                    }
                    addr_7495_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_74e7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_7466_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_7495_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6d0c_81;
                addr_7a15_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_7668_67;
                addr_7aab_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_741e_109;
                addr_7b1b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_741e_109;
                addr_6d90_56:
                rax93 = fun_27e0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rax93) + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_6ddc_110;
                addr_756e_59:
                goto addr_7570_40;
                addr_723d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_6d83_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_6de8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6d35_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_6d83_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_7282_160;
                if (!v22) 
                    goto addr_7657_162; else 
                    goto addr_7863_163;
                addr_7282_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_7657_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_7668_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_712b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_6f93_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_6cc1_70; else 
                    goto addr_6fa7_169;
                addr_712b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_6c88_63;
                goto addr_6e10_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_7564_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_769f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6d80_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6c78_178; else 
                        goto addr_7622_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7564_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6d83_22;
                }
                addr_769f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_6d80_30:
                    r8d42 = 0;
                    goto addr_6d83_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_6df1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_76b8_42;
                    }
                }
                addr_6c78_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6c88_63;
                addr_7622_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_7580_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_6df1_62;
                } else {
                    addr_7632_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_6d83_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_7de2_188;
                if (v28) 
                    goto addr_7657_162;
                addr_7de2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_6f93_168;
                addr_6c2c_37:
                if (v22) 
                    goto addr_7c23_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_6c43_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_76f0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_777b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6d83_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6c78_178; else 
                        goto addr_7757_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7564_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6d83_22;
                }
                addr_777b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_6d83_22;
                }
                addr_7757_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7580_46;
                goto addr_7632_186;
                addr_6c43_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_6d83_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_6d83_22; else 
                    goto addr_6c54_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_7d2e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_7bb4_210;
            if (1) 
                goto addr_7bb2_212;
            if (!v29) 
                goto addr_77ee_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_7d21_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_6fb0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_6d6b_219; else 
            goto addr_6fca_220;
        addr_6d4f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_6d63_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_6fca_220; else 
            goto addr_6d6b_219;
        addr_791f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_6fca_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_793d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_7db0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_7816_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_7a07_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6d63_221;
        addr_7863_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6d63_221;
        addr_6fa7_169:
        goto addr_6fb0_65;
        addr_7d2e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_6fca_220;
        goto addr_793d_222;
        addr_7bb4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_7c0e_236;
        fun_2560();
        rsp25 = rsp25 - 8 + 8;
        goto addr_7db0_225;
        addr_7bb2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_7bb4_210;
        addr_77ee_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_7bb4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_7816_226;
        }
        addr_7d21_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_717d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe9ec + rax113 * 4) + 0xe9ec;
    addr_75a9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xeaec + rax114 * 4) + 0xeaec;
    addr_7c23_190:
    addr_6d6b_219:
    goto 0x6a50;
    addr_6c54_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe8ec + rax115 * 4) + 0xe8ec;
    addr_7c0e_236:
    goto v116;
}

void fun_6c70() {
}

void fun_6e28() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x6b22;
}

void fun_6e81() {
    goto 0x6b22;
}

void fun_6f6e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x6df1;
    }
    if (v2) 
        goto 0x7863;
    if (!r10_3) 
        goto addr_79ce_5;
    if (!v4) 
        goto addr_789e_7;
    addr_79ce_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_789e_7:
    goto 0x6ca4;
}

void fun_6f8c() {
}

void fun_7037() {
    signed char v1;

    if (v1) {
        goto 0x6fbf;
    } else {
        goto 0x6cfa;
    }
}

void fun_7051() {
    signed char v1;

    if (!v1) 
        goto 0x704a; else 
        goto "???";
}

void fun_7078() {
    goto 0x6f93;
}

void fun_70f8() {
}

void fun_7110() {
}

void fun_713f() {
    goto 0x6f93;
}

void fun_7191() {
    goto 0x7120;
}

void fun_71c0() {
    goto 0x7120;
}

void fun_71f3() {
    goto 0x7120;
}

void fun_75c0() {
    goto 0x6c78;
}

void fun_78be() {
    signed char v1;

    if (v1) 
        goto 0x7863;
    goto 0x6ca4;
}

void fun_7965() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x6ca4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x6c88;
        goto 0x6ca4;
    }
}

void fun_7d82() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x6ff0;
    } else {
        goto 0x6b22;
    }
}

void fun_8cb8() {
    fun_2530();
}

void fun_9dbc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x9dcb;
}

void fun_9e8c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x9e99;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x9dcb;
}

void fun_9eb0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_9edc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x9dcb;
}

void fun_9efd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x9dcb;
}

void fun_9f21() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x9dcb;
}

void fun_9f45() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x9ed4;
}

void fun_9f69() {
}

void fun_9f89() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x9ed4;
}

void fun_9fa5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x9ed4;
}

struct s31 {
    signed char[80] pad80;
    int64_t f50;
};

struct s32 {
    signed char[1] pad1;
    signed char f1;
};

struct s33 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_a798() {
    struct s12* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t r9_4;
    struct s31* r14_5;
    int64_t rbp6;
    int64_t rbp7;
    int64_t r9_8;
    int64_t rbp9;
    void* r8_10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int32_t* rsi14;
    int64_t rbp15;
    int64_t v16;
    int64_t v17;
    int32_t eax18;
    int64_t rdx19;
    int64_t rbp20;
    void** rsi21;
    int64_t rbp22;
    int64_t rax23;
    int64_t rbp24;
    int64_t r9_25;
    int64_t rbp26;
    void* r8_27;
    int64_t rbp28;
    int64_t rbp29;
    int64_t rsi30;
    int32_t* rsi31;
    int64_t rbp32;
    int64_t v33;
    int64_t rbp34;
    void* r8_35;
    int64_t rbp36;
    int64_t rbp37;
    int64_t rsi38;
    int32_t* rsi39;
    int64_t rbp40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t rbp44;
    int64_t rbp45;
    void** rcx46;
    uint64_t r15_47;
    int64_t r12_48;
    void** rax49;
    int64_t rbp50;
    int64_t rbp51;
    int64_t rbp52;
    uint64_t r13_53;
    int64_t rbp54;
    int64_t rbx55;
    int64_t rbx56;
    void** rax57;
    void** rcx58;
    void* rbx59;
    void* rbx60;
    void** tmp64_61;
    void* r12_62;
    void** rax63;
    void** rbx64;
    int64_t r15_65;
    int64_t rbp66;
    void** r15_67;
    void** rax68;
    void** rax69;
    int64_t r12_70;
    void** r15_71;
    void** r12_72;
    int64_t rbp73;
    int64_t rbp74;
    uint32_t eax75;
    struct s33* r14_76;
    int32_t eax77;
    int64_t rbp78;

    rdi1 = reinterpret_cast<struct s12*>(r15_2 + r12_3);
    r9_4 = *reinterpret_cast<int64_t*>((r14_5->f50 << 5) + reinterpret_cast<int64_t>(*reinterpret_cast<void**>(rbp6 - 0x3a8)) + 16);
    if (*reinterpret_cast<int32_t*>(rbp7 - 0x3d8) == 1) {
        *reinterpret_cast<int32_t*>(&r9_8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3b8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0;
        r8_10 = *reinterpret_cast<void**>(rbp11 - 0x3e0);
        *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
        eax18 = fun_2420(rdi1, rsi14, 1, -1, r8_10, r9_8, r9_4, rbp15 - 0x3bc, __return_address(), v16, v17);
        *reinterpret_cast<int32_t*>(&rdx19) = *reinterpret_cast<int32_t*>(rbp20 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        rsi21 = *reinterpret_cast<void***>(rbp22 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx19) < 0) 
            goto addr_a923_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp7 - 0x3d8) == 2) {
            *reinterpret_cast<int32_t*>(&rax23) = *reinterpret_cast<int32_t*>(rbp24 - 0x3b4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_25) = *reinterpret_cast<int32_t*>(rbp26 - 0x3b8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_25) + 4) = 0;
            r8_27 = *reinterpret_cast<void**>(rbp28 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp29 - 0x418) = rsi30;
            eax18 = fun_2420(rdi1, rsi31, 1, -1, r8_27, r9_25, rax23, r9_4, rbp32 - 0x3bc, v33, __return_address());
            rsi21 = *reinterpret_cast<void***>(rbp34 - 0x418);
        } else {
            r8_35 = *reinterpret_cast<void**>(rbp36 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp37 - 0x418) = rsi38;
            eax18 = fun_2420(rdi1, rsi39, 1, -1, r8_35, r9_4, rbp40 - 0x3bc, v41, __return_address(), v42, v43);
            rsi21 = *reinterpret_cast<void***>(rbp44 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx19) = *reinterpret_cast<int32_t*>(rbp45 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx19) < 0) 
            goto addr_a923_5;
    }
    rcx46 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx19)));
    if (reinterpret_cast<unsigned char>(rcx46) < reinterpret_cast<unsigned char>(rsi21)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx46) + r15_47 + r12_48)) 
            goto 0x2847;
    }
    if (*reinterpret_cast<int32_t*>(&rdx19) >= eax18) {
        addr_a82d_16:
        *reinterpret_cast<int32_t*>(&rax49) = static_cast<int32_t>(rdx19 + 1);
        *reinterpret_cast<int32_t*>(&rax49 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax49) < reinterpret_cast<unsigned char>(rsi21)) {
            **reinterpret_cast<int32_t**>(rbp50 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp51 - 0x40c);
            goto 0xad07;
        }
    } else {
        addr_a825_18:
        *reinterpret_cast<int32_t*>(rbp52 - 0x3bc) = eax18;
        *reinterpret_cast<int32_t*>(&rdx19) = eax18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        goto addr_a82d_16;
    }
    if (r13_53 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp54 - 0x3d0) = 75;
        goto 0xa970;
    }
    if (rbx55 < 0) {
        if (rbx56 == -1) 
            goto 0xa748;
        goto 0xade2;
    }
    *reinterpret_cast<int32_t*>(&rax57) = static_cast<int32_t>(rdx19 + 2);
    *reinterpret_cast<int32_t*>(&rax57 + 4) = 0;
    rcx58 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx59) + reinterpret_cast<uint64_t>(rbx60));
    tmp64_61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<uint64_t>(r12_62));
    rax63 = tmp64_61;
    if (reinterpret_cast<unsigned char>(tmp64_61) < reinterpret_cast<unsigned char>(rax57)) 
        goto 0xade2;
    if (reinterpret_cast<unsigned char>(rax63) >= reinterpret_cast<unsigned char>(rcx58)) 
        goto addr_a866_26;
    rax63 = rcx58;
    addr_a866_26:
    if (reinterpret_cast<unsigned char>(rbx64) >= reinterpret_cast<unsigned char>(rax63)) 
        goto 0xa748;
    if (reinterpret_cast<unsigned char>(rcx58) >= reinterpret_cast<unsigned char>(rax63)) {
        rax63 = rcx58;
    }
    if (rax63 == 0xffffffffffffffff) 
        goto 0xade2;
    if (r15_65 != *reinterpret_cast<int64_t*>(rbp66 - 0x3e8)) {
        rax68 = fun_26d0(r15_67, rax63);
        if (!rax68) 
            goto 0xade2;
        goto 0xa748;
    }
    rax69 = fun_2680(rax63, rsi21);
    if (!rax69) 
        goto 0xade2;
    if (r12_70) 
        goto addr_ac2a_36;
    goto 0xa748;
    addr_ac2a_36:
    fun_2660(rax69, r15_71, r12_72);
    goto 0xa748;
    addr_a923_5:
    if ((*reinterpret_cast<struct s32**>(rbp73 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s32**>(rbp73 - 0x3f0))->f1 = 0;
        goto 0xa748;
    }
    if (eax18 >= 0) 
        goto addr_a825_18;
    if (**reinterpret_cast<int32_t**>(rbp74 - 0x3d0)) 
        goto 0xa970;
    eax75 = static_cast<uint32_t>(r14_76->f48) & 0xffffffef;
    eax77 = 22;
    if (*reinterpret_cast<signed char*>(&eax75) != 99) 
        goto addr_a967_42;
    eax77 = 84;
    addr_a967_42:
    **reinterpret_cast<int32_t**>(rbp78 - 0x3d0) = eax77;
}

void fun_a8b0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_a9a0() {
    int64_t rbp1;
    int64_t r9_2;
    int64_t rbp3;
    void* r8_4;
    int64_t rbp5;
    int64_t rax6;
    int64_t rbp7;
    int64_t rbp8;
    int64_t rsi9;
    int64_t r15_10;
    int64_t r12_11;
    int32_t* rsi12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t rbp16;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0xaae5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int32_t*>(&r9_2) = *reinterpret_cast<int32_t*>(rbp3 - 0x3b8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_2) + 4) = 0;
            r8_4 = *reinterpret_cast<void**>(rbp5 - 0x3e0);
            *reinterpret_cast<int32_t*>(&rax6) = *reinterpret_cast<int32_t*>(rbp7 - 0x3b4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
            *reinterpret_cast<int64_t*>(rbp8 - 0x418) = rsi9;
            __asm__("fstp tword [rsp+0x8]");
            fun_2420(r15_10 + r12_11, rsi12, 1, -1, r8_4, r9_2, rax6, v13, v14, v15, rbp16 - 0x3bc);
            goto 0xa7fd;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0xa7d3;
        }
    }
}

struct s34 {
    int32_t f0;
    int32_t f4;
};

struct s35 {
    signed char[4] pad4;
    int32_t f4;
};

void fun_a9e8() {
    struct s34* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    struct s12* rdi5;
    int32_t* rsi6;
    struct s35* rsi7;
    int64_t rbp8;
    int64_t v9;
    int64_t rbp10;
    int64_t v11;
    int64_t rbp12;
    int64_t rax13;
    int64_t rbp14;
    void* r8_15;
    int64_t rbp16;
    int64_t rbp17;
    int64_t rbp18;
    int64_t v19;
    int64_t v20;
    int64_t v21;
    int64_t v22;
    int64_t r9_23;
    int64_t rbp24;
    void* r8_25;
    int64_t rbp26;
    int64_t rbp27;
    int64_t v28;
    int64_t v29;

    rdi1 = reinterpret_cast<struct s34*>(r15_2 + r12_3);
    rdi1->f0 = *rsi4;
    rdi5 = reinterpret_cast<struct s12*>(&rdi1->f4);
    rsi6 = &rsi7->f4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) == 1) {
        v9 = rbp10 - 0x3bc;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) == 2) {
            v11 = rbp12 - 0x3bc;
            *reinterpret_cast<int32_t*>(&rax13) = *reinterpret_cast<int32_t*>(rbp14 - 0x3b4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            v9 = rax13;
        } else {
            r8_15 = *reinterpret_cast<void**>(rbp16 - 0x3e0);
            *reinterpret_cast<int32_t**>(rbp17 - 0x418) = rsi6;
            fun_2420(rdi5, rsi6, 1, -1, r8_15, rbp18 - 0x3bc, __return_address(), v19, v20, v21, v22);
            goto 0xa7fd;
        }
    }
    *reinterpret_cast<int32_t*>(&r9_23) = *reinterpret_cast<int32_t*>(rbp24 - 0x3b8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
    r8_25 = *reinterpret_cast<void**>(rbp26 - 0x3e0);
    *reinterpret_cast<int32_t**>(rbp27 - 0x418) = rsi6;
    fun_2420(rdi5, rsi6, 1, -1, r8_25, r9_23, v9, v11, __return_address(), v28, v29);
    goto 0xa7fd;
}

void fun_aa50() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xa8d6;
}

void fun_aaa0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0xaa80;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0xa8df;
}

void fun_ab50() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xa8d6;
    goto 0xaa80;
}

void fun_abe3() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0xa652;
}

void fun_ad80() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0xad07;
}

struct s36 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s37 {
    signed char[8] pad8;
    int64_t f8;
};

struct s38 {
    signed char[16] pad16;
    int64_t f10;
};

struct s39 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_b3a8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s36* rcx3;
    struct s37* rcx4;
    int64_t r11_5;
    struct s38* rcx6;
    uint32_t* rcx7;
    struct s39* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0xb390; else 
        goto "???";
}

struct s40 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

struct s43 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_b3e0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s40* rcx3;
    struct s41* rcx4;
    int64_t r11_5;
    struct s42* rcx6;
    uint32_t* rcx7;
    struct s43* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0xb3c6;
}

struct s44 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s45 {
    signed char[8] pad8;
    int64_t f8;
};

struct s46 {
    signed char[16] pad16;
    int64_t f10;
};

struct s47 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_b400() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s44* rcx3;
    struct s45* rcx4;
    int64_t r11_5;
    struct s46* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s47* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0xb3c6;
}

struct s48 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s49 {
    signed char[8] pad8;
    int64_t f8;
};

struct s50 {
    signed char[16] pad16;
    int64_t f10;
};

struct s51 {
    signed char[16] pad16;
    signed char f10;
};

void fun_b420() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s48* rcx3;
    struct s49* rcx4;
    int64_t r11_5;
    struct s50* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s51* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0xb3c6;
}

struct s52 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s53 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_b4a0() {
    struct s52* rcx1;
    struct s53* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0xb3c6;
}

struct s54 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s55 {
    signed char[8] pad8;
    int64_t f8;
};

struct s56 {
    signed char[16] pad16;
    int64_t f10;
};

struct s57 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_b4f0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s54* rcx3;
    struct s55* rcx4;
    int64_t r11_5;
    struct s56* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s57* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0xb3c6;
}

void fun_b73c() {
}

void fun_b76b() {
    goto 0xb748;
}

void fun_b7c0() {
}

void fun_b9d0() {
    goto 0xb7c3;
}

struct s58 {
    signed char[8] pad8;
    void** f8;
};

struct s59 {
    signed char[8] pad8;
    int64_t f8;
};

struct s60 {
    signed char[8] pad8;
    void** f8;
};

struct s61 {
    signed char[8] pad8;
    void** f8;
};

void fun_ba78() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s58* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s59* r15_14;
    void** rax15;
    struct s60* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s61* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0xc1b7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0xc1b7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_2680(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0xbfd8; else 
                goto "???";
        }
    } else {
        rax15 = fun_26d0(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0xc1b7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_c30a_9; else 
            goto addr_baee_10;
    }
    addr_bc44_11:
    rax19 = fun_2660(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_baee_10:
    r14_20->f8 = rcx12;
    goto 0xb609;
    addr_c30a_9:
    r13_18 = *r14_21;
    goto addr_bc44_11;
}

struct s62 {
    signed char[11] pad11;
    void** fb;
};

struct s63 {
    signed char[80] pad80;
    int64_t f50;
};

struct s64 {
    signed char[80] pad80;
    int64_t f50;
};

struct s65 {
    signed char[8] pad8;
    void** f8;
};

struct s66 {
    signed char[8] pad8;
    void** f8;
};

struct s67 {
    signed char[8] pad8;
    void** f8;
};

struct s68 {
    signed char[72] pad72;
    signed char f48;
};

struct s69 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_bd01() {
    void** ecx1;
    int32_t edx2;
    struct s62* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s63* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s64* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s65* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s66* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s67* r15_37;
    void*** r13_38;
    struct s68* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s69* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s62*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0xbb98;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0xc2ec;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_be5d_12;
    } else {
        addr_ba04_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_ba3f_17;
        }
    }
    rax25 = fun_2680(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0xbfca;
    addr_bf70_19:
    rdx30 = *r15_31;
    rax32 = fun_2660(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_bea6_20:
    r15_33->f8 = r8_12;
    goto addr_ba04_13;
    addr_be5d_12:
    rax34 = fun_26d0(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0xc1b7;
    if (v36 != r15_37->f8) 
        goto addr_bea6_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_bf70_19;
    addr_ba3f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0xbb9c;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0xba80;
    goto 0xb609;
}

void fun_bd1f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xb9e8;
    if (dl2 & 4) 
        goto 0xb9e8;
    if (edx3 > 7) 
        goto 0xb9e8;
    if (dl4 & 2) 
        goto 0xb9e8;
    goto 0xb9e8;
}

void fun_bd68() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xb9e8;
    if (dl2 & 4) 
        goto 0xb9e8;
    if (edx3 > 7) 
        goto 0xb9e8;
    if (dl4 & 2) 
        goto 0xb9e8;
    goto 0xb9e8;
}

void fun_bdb0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xb9e8;
    if (dl2 & 4) 
        goto 0xb9e8;
    if (edx3 > 7) 
        goto 0xb9e8;
    if (dl4 & 2) 
        goto 0xb9e8;
    goto 0xb9e8;
}

void fun_bdf8() {
    goto 0xb9e8;
}

void fun_2a21() {
    void** rsi1;

    decode_format_string_part_0("x4", rsi1);
    goto 0x29c0;
}

void fun_2ae1() {
    void** rax1;
    uint32_t ecx2;

    rax1 = optarg;
    ecx2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax1));
    if (*reinterpret_cast<signed char*>(&ecx2) == 0x6f) {
        format_address = reinterpret_cast<int64_t>(format_address_std);
        address_base = 8;
        address_pad_len = 7;
        goto 0x29c0;
    } else {
        if (*reinterpret_cast<signed char*>(&ecx2) > 0x6f) {
            if (*reinterpret_cast<signed char*>(&ecx2) != 0x78) 
                goto 0x387f;
            format_address = reinterpret_cast<int64_t>(format_address_std);
            address_base = 16;
            address_pad_len = 6;
            goto 0x29c0;
        } else {
            if (*reinterpret_cast<signed char*>(&ecx2) == 100) 
                goto 0x2eab;
            if (*reinterpret_cast<signed char*>(&ecx2) != 0x6e) 
                goto 0x387f;
            format_address = 0x39d0;
            address_pad_len = 0;
            goto 0x29c0;
        }
    }
}

void fun_2b94() {
    void** rdi1;
    int32_t eax2;

    rdi1 = optarg;
    if (!rdi1) 
        goto 0x2daa;
    eax2 = xstrtoumax(rdi1, rdi1);
    if (eax2) 
        goto 0x38c6;
    goto 0x29c0;
}

void fun_2bee() {
    void** rdi1;
    void** rsi2;

    rdi1 = optarg;
    if (!rdi1) 
        goto 0x3860;
    decode_format_string_part_0(rdi1, rsi2);
    goto 0x29c0;
}

void fun_2c26() {
    void** rsi1;

    decode_format_string_part_0("u2", rsi1);
    goto 0x29c0;
}

void fun_2cd9() {
    void** rdi1;
    int32_t eax2;
    void** r8_3;
    int64_t rsi4;
    int32_t v5;
    int64_t rdi6;
    void** r14_7;

    rdi1 = optarg;
    limit_bytes_to_format = 1;
    eax2 = xstrtoumax(rdi1);
    if (!eax2) {
        goto 0x29c0;
    } else {
        r8_3 = optarg;
        *reinterpret_cast<int32_t*>(&rsi4) = v5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi4) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi6) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        xstrtol_fatal(rdi6, rsi4, 78, r14_7, r8_3);
    }
}

void fun_3635() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\v", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x362f;
}

void fun_4758() {
    goto 0x46f0;
}

void fun_56a0() {
    goto 0x51e3;
}

void fun_6eae() {
    goto 0x6b22;
}

void fun_7084() {
    goto 0x703c;
}

void fun_714b() {
    goto 0x6c78;
}

void fun_719d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x7120;
    goto 0x6d4f;
}

void fun_71cf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x712b;
        goto 0x6b50;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x6fca;
        goto 0x6d6b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x7968;
    if (r10_8 > r15_9) 
        goto addr_70b5_9;
    addr_70ba_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x7973;
    goto 0x6ca4;
    addr_70b5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_70ba_10;
}

void fun_7202() {
    goto 0x6d37;
}

void fun_75d0() {
    goto 0x6d37;
}

void fun_7d6f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x6e8c;
    } else {
        goto 0x6ff0;
    }
}

void fun_8d70() {
}

void fun_9e5f() {
    if (__intrinsic()) 
        goto 0x9e99; else 
        goto "???";
}

struct s70 {
    signed char[1] pad1;
    signed char f1;
};

void fun_a640() {
    signed char* r13_1;
    struct s70* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_ad8e() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0xad07;
}

struct s71 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s72 {
    signed char[8] pad8;
    int64_t f8;
};

struct s73 {
    signed char[16] pad16;
    int64_t f10;
};

struct s74 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_b4c0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s71* rcx3;
    struct s72* rcx4;
    int64_t r11_5;
    struct s73* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s74* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0xb3c6;
}

void fun_b775() {
    goto 0xb748;
}

void fun_c0c4() {
    goto 0xb9e8;
}

void fun_b9d8() {
}

void fun_be08() {
    goto 0xb9e8;
}

void fun_2a33() {
    void** rsi1;

    decode_format_string_part_0("fD", rsi1);
    goto 0x29c0;
}

void fun_2c3b() {
    void** rsi1;

    decode_format_string_part_0("c", rsi1);
    goto 0x29c0;
}

void fun_3643() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\n", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x362f;
}

void fun_4768() {
    goto 0x46f0;
}

void fun_56b0() {
    goto 0x51e3;
}

void fun_720c() {
    goto 0x71a7;
}

void fun_75da() {
    goto 0x70fd;
}

void fun_8dd0() {
    fun_2530();
    goto fun_27a0;
}

void fun_ab20() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xa8d6;
    goto 0xaa80;
}

void fun_ad9c() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0xad07;
}

struct s75 {
    int32_t f0;
    int32_t f4;
};

struct s76 {
    int32_t f0;
    int32_t f4;
};

struct s77 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s78 {
    signed char[8] pad8;
    int64_t f8;
};

struct s79 {
    signed char[8] pad8;
    int64_t f8;
};

struct s80 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_b470(struct s75* rdi, struct s76* rsi) {
    struct s77* rcx3;
    struct s78* rcx4;
    struct s79* rcx5;
    struct s80* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0xb3c6;
}

void fun_b77f() {
    goto 0xb748;
}

void fun_c0ce() {
    goto 0xb9e8;
}

void fun_2a48() {
    void** rsi1;

    decode_format_string_part_0("o2", rsi1);
    goto 0x29c0;
}

void fun_2c50() {
    void** rsi1;

    decode_format_string_part_0("o1", rsi1);
    goto 0x29c0;
}

void fun_3651() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\t", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x362f;
}

void fun_4778() {
    goto 0x46f0;
}

void fun_56c0() {
    int32_t edx1;

    if (edx1) {
    }
    goto 0x51e3;
}

void fun_6edd() {
    goto 0x6b22;
}

void fun_7218() {
    goto 0x71a7;
}

void fun_75e7() {
    goto 0x714e;
}

void fun_8e10() {
    fun_2530();
    goto fun_27a0;
}

void fun_adab() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0xad07;
}

void fun_b789() {
    goto 0xb748;
}

void fun_2a5d() {
    void** rsi1;

    decode_format_string_part_0("x2", rsi1);
    goto 0x29c0;
}

void fun_2c65() {
    void** rsi1;

    decode_format_string_part_0("a", rsi1);
    goto 0x29c0;
}

void fun_365f() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\b", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x362f;
}

void fun_4788() {
    goto 0x46f0;
}

void fun_6f0a() {
    goto 0x6b22;
}

void fun_7224() {
    goto 0x7120;
}

void fun_8e50() {
    fun_2530();
    goto fun_27a0;
}

void fun_b793() {
    goto 0xb748;
}

void fun_2a72() {
    void** rsi1;

    decode_format_string_part_0("dI", rsi1);
    goto 0x29c0;
}

void fun_2c7a() {
    void** rdi1;
    void** rax2;
    int32_t eax3;
    void** v4;

    rdi1 = optarg;
    if (!rdi1) {
        *reinterpret_cast<int32_t*>(&rax2) = 3;
        *reinterpret_cast<int32_t*>(&rax2 + 4) = 0;
    } else {
        eax3 = xstrtoumax(rdi1);
        if (eax3) 
            goto 0x38ac;
        rax2 = v4;
    }
    string_min = rax2;
    flag_dump_strings = 1;
    goto 0x29c0;
}

void fun_366d() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    fun_26c0("\\a", rsi1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x362f;
}

void fun_4798() {
    goto 0x46f0;
}

void fun_6f2c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x78c0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x6df1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x6df1;
    }
    if (v11) 
        goto 0x7c23;
    if (r10_12 > r15_13) 
        goto addr_7c73_8;
    addr_7c78_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x79b1;
    addr_7c73_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_7c78_9;
}

struct s81 {
    signed char[24] pad24;
    int64_t f18;
};

struct s82 {
    signed char[16] pad16;
    void** f10;
};

struct s83 {
    signed char[8] pad8;
    void** f8;
};

void fun_8ea0() {
    int64_t r15_1;
    struct s81* rbx2;
    void** r14_3;
    struct s82* rbx4;
    void** r13_5;
    struct s83* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2530();
    fun_27a0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x8ec2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2a87() {
    void** rsi1;

    decode_format_string_part_0("d2", rsi1);
    goto 0x29c0;
}

void fun_367b() {
    void** r14_1;
    void** rsi2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    int64_t r9_6;

    fun_26c0(r14_1, rsi2, rdx3, rcx4, r8_5, r9_6, __return_address());
    goto 0x362f;
}

void fun_47a8() {
    goto 0x46f0;
}

void fun_8ef8() {
    fun_2530();
    goto 0x8ec9;
}

void fun_2a9c() {
    void** rdi1;
    int32_t eax2;
    void** r8_3;
    int64_t rsi4;
    int32_t v5;
    int64_t rdi6;
    void** r14_7;

    rdi1 = optarg;
    eax2 = xstrtoumax(rdi1);
    if (!eax2) 
        goto "???";
    r8_3 = optarg;
    *reinterpret_cast<int32_t*>(&rsi4) = v5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi6) = eax2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
    xstrtol_fatal(rdi6, rsi4, 0x6a, r14_7, r8_3);
}

void fun_47b8() {
    goto 0x46f0;
}

struct s84 {
    signed char[32] pad32;
    void*** f20;
};

struct s85 {
    signed char[24] pad24;
    int64_t f18;
};

struct s86 {
    signed char[16] pad16;
    void** f10;
};

struct s87 {
    signed char[8] pad8;
    void** f8;
};

struct s88 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_8f30() {
    void*** rcx1;
    struct s84* rbx2;
    int64_t r15_3;
    struct s85* rbx4;
    void** r14_5;
    struct s86* rbx6;
    void** r13_7;
    struct s87* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s88* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2530();
    fun_27a0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x8f64, __return_address(), rcx1);
    goto v15;
}

void fun_8fa8() {
    fun_2530();
    goto 0x8f6b;
}
