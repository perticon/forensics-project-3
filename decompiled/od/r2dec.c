#include <stdint.h>

/* /tmp/tmpwp339vba @ 0x38e0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpwp339vba @ 0x39d0 */
 
void dbg_format_address_none (uintmax_t address, char c) {
    rdi = address;
    rsi = c;
    /* void format_address_none(uintmax_t address,char c); */
}

/* /tmp/tmpwp339vba @ 0x39e0 */
 
int64_t dbg_get_lcm (void) {
    /* int get_lcm(); */
    rax = n_specs;
    if (rax == 0) {
        goto label_1;
    }
    rdx = spec;
    rax *= 5;
    r8d = 1;
    r10 = obj_width_bytes;
    r9 = rdx + 4;
    r11 = rdx + rax*8 + 4;
label_0:
    eax = *(r9);
    r8 = (int64_t) r8d;
    rcx = r8;
    rdi = *((r10 + rax*4));
    rdx = rdi;
    do {
        rax = rcx;
        rcx = rdx;
        edx = 0;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    } while (rdx != 0);
    rax = rdi;
    edx = 0;
    r9 += 0x28;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    r8d *= eax;
    if (r11 != r9) {
        goto label_0;
    }
    do {
        eax = r8d;
        return rax;
label_1:
        r8d = 1;
    } while (1);
}

/* /tmp/tmpwp339vba @ 0x3a60 */
 
int64_t dbg_format_address_std (int64_t arg1, int64_t canary) {
    char[25] buf;
    int64_t var_17h;
    int64_t var_18h;
    int64_t var_28h;
    rdi = arg1;
    rdx = canary;
    /* void format_address_std(uintmax_t address,char c); */
    rcx = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = *(obj.address_pad_len);
    *((rsp + 0x17)) = sil;
    rsi = rsp + 0x17;
    r8 = rsi;
    *((rsp + 0x18)) = 0;
    r8 -= rax;
    eax = address_base;
    if (eax == 0xa) {
        goto label_3;
    }
    if (eax == 0x10) {
        goto label_4;
    }
    if (eax == 8) {
        goto label_5;
    }
label_0:
    if (r8 >= rsi) {
        goto label_6;
    }
label_1:
    rdx -= r8;
    rax = memset (r8, 0x30, rsi);
    r8 = rax;
label_2:
    rsi = stdout;
    rdi = r8;
    fputs_unlocked ();
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_7;
    }
    return rax;
    do {
label_5:
        eax = ecx;
        rsi--;
        eax &= 7;
        eax += 0x30;
        rcx >>= 3;
        *(rsi) = al;
    } while (rcx != 0);
    goto label_0;
label_4:
    rdx = "0123456789abcdef";
    do {
        rax = rcx;
        rsi--;
        eax &= 0xf;
        rcx >>= 4;
        eax = *((rdx + rax));
        *(rsi) = al;
    } while (rcx != 0);
    if (r8 < rsi) {
        goto label_1;
    }
label_6:
    r8 = rsi;
    goto label_2;
label_3:
    r9 = 0xcccccccccccccccd;
    do {
        rax = rcx;
        rsi--;
        rdx:rax = rax * r9;
        rax = rcx;
        rdx >>= 3;
        rdi = rdx * 5;
        rdi += rdi;
        rax -= rdi;
        eax += 0x30;
        *(rsi) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    goto label_0;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x3bb0 */
 
uint64_t dbg_parse_old_offset (int64_t arg_1h, int64_t arg1, uint32_t arg2, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    /* _Bool parse_old_offset(char const * s,uintmax_t * offset); */
    edx = *(rdi);
    if (dl == 0) {
        goto label_1;
    }
    r12 = rsi;
    while (1) {
        rax = strchr (rbp, 0x2e);
        edx = 0xa;
        if (rax == 0) {
            goto label_2;
        }
label_0:
        eax = xstrtoumax (rbp, 0, rdx, r12, 0x0000d015);
        al = (eax == 0) ? 1 : 0;
        return rax;
        rbp++;
    }
label_1:
    eax = 0;
    return rax;
label_2:
    edx = 8;
    if (*(rbp) != 0x30) {
        goto label_0;
    }
    eax = *((rbp + 1));
    edx = 0;
    eax &= 0xffffffdf;
    dl = (al == 0x58) ? 1 : 0;
    edx = rdx*8 + 8;
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x3c30 */
 
int64_t dbg_print_named_ascii (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg5, int64_t arg6) {
    char[2] buf;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_26h;
    int64_t var_27h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    /* void print_named_ascii(size_t fields,size_t blank, const * block,char const * unused_fmt_string,int width,int pad); */
    *(rsp) = rsi;
    *((rsp + 0xc)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_2;
    }
    r8 = rdi - 1;
    rax = (int64_t) r9d;
    r12 = rdi;
    rbx = r8;
    *((rsp + 0x10)) = rax;
    r13d = r9d;
    r15 = 0x0000d01c;
    rbx *= rax;
    rax = rsp + 0x26;
    *((rsp + 0x18)) = rax;
    rax = rbx;
    rbx = rdx;
    r14 = rax;
    goto label_3;
label_0:
    edx = (int32_t) dl;
    rdi = obj_charname;
    do {
label_1:
        esi -= eax;
        esi += *((rsp + 0xc));
        eax = 0;
        xprintf (r15, rsi, r9, rcx, r8, rdi + rdx*4);
        rax = r12;
        r14 -= *((rsp + 0x10));
        rax -= rbx;
        rax += rbp;
        if (*(rsp) >= rax) {
            goto label_2;
        }
label_3:
        rax = r14;
        edx = 0;
        rbx++;
        esi = r13d;
        rax = rdx:rax / r12;
        rdx = rdx:rax % r12;
        edx = *((rbx - 1));
        edx &= 0x7f;
        r9 = 0x0000d018;
        r13d = eax;
    } while (dl == 0x7f);
    if (dl <= 0x20) {
        goto label_0;
    }
    *((rsp + 0x26)) = dl;
    r9 = *((rsp + 0x18));
    *((rsp + 0x27)) = 0;
    goto label_1;
label_2:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x3d40 */
 
int64_t dbg_print_long_long (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_long_long(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r13 = rcx;
    rcx = rdi - 1;
    rbx = rdx;
    rcx *= rax;
    *((rsp + 0x18)) = rax;
    r15d = r9d;
    r12 = rdi;
    rbp = rsp + 0x28;
    r14 = rcx;
    while (*(obj.input_swap) == 0) {
label_0:
        eax = 0;
        rbx += 8;
        xprintf (r13, rsi, *(rbx), rcx, r8, r9);
        r14 -= *((rsp + 0x18));
        if (r12 <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r14;
        esi = r15d;
        r12--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        esi -= eax;
        esi += *((rsp + 0x14));
        r15d = eax;
    }
    rax = rbx + 7;
    rdx = rsp + 0x20;
    do {
        ecx = *(rax);
        rdx++;
        rax--;
        *((rdx - 1)) = cl;
    } while (rbp != rdx);
    rdx = *((rsp + 0x20));
    goto label_0;
label_1:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x3e40 */
 
int64_t dbg_print_long (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_long(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r13 = rcx;
    rcx = rdi - 1;
    rbx = rdx;
    rcx *= rax;
    *((rsp + 0x18)) = rax;
    r15d = r9d;
    r12 = rdi;
    rbp = rsp + 0x28;
    r14 = rcx;
    while (*(obj.input_swap) == 0) {
label_0:
        eax = 0;
        rbx += 8;
        xprintf (r13, rsi, *(rbx), rcx, r8, r9);
        r14 -= *((rsp + 0x18));
        if (r12 <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r14;
        esi = r15d;
        r12--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        esi -= eax;
        esi += *((rsp + 0x14));
        r15d = eax;
    }
    rax = rbx + 7;
    rdx = rsp + 0x20;
    do {
        ecx = *(rax);
        rdx++;
        rax--;
        *((rdx - 1)) = cl;
    } while (rbp != rdx);
    rdx = *((rsp + 0x20));
    goto label_0;
label_1:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x3f40 */
 
int64_t dbg_print_int (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_int(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r8 = rdi - 1;
    r15 = rdx;
    r12 = rcx;
    r8 *= rax;
    *((rsp + 0x18)) = rax;
    r14d = r9d;
    r13 = r8;
    while (*(obj.input_swap) == 0) {
label_0:
        eax = 0;
        r15 += 4;
        xprintf (r12, rsi, *(r15), rcx, r8, r9);
        r13 -= *((rsp + 0x18));
        if (rbp <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r13;
        ecx = r14d;
        rbp--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        ecx -= eax;
        r14d = eax;
        eax = *((rsp + 0x14));
        esi = rcx + rax;
    }
    eax = *((r15 + 2));
    bl = *((r15 + 3));
    edx = *((r15 + 1));
    bh = al;
    eax = *(r15);
    edx <<= 0x10;
    r10d = (int32_t) bx;
    r10d |= edx;
    eax <<= 0x18;
    ebx = r10d;
    ebx |= eax;
    edx = ebx;
    goto label_0;
label_1:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x4010 */
 
int64_t dbg_print_short (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_short(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r12 = rcx;
    rcx = rdi - 1;
    r15 = rdx;
    rcx *= rax;
    *((rsp + 0x18)) = rax;
    r14d = r9d;
    r13 = rcx;
    while (*(obj.input_swap) == 0) {
        edx = *(r15);
label_0:
        eax = 0;
        r15 += 2;
        xprintf (r12, rsi, rdx, rcx, r8, r9);
        r13 -= *((rsp + 0x18));
        if (rbp <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r13;
        esi = r14d;
        rbp--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        esi -= eax;
        esi += *((rsp + 0x14));
        r14d = eax;
    }
    bl = *((r15 + 1));
    eax = *(r15);
    bh = al;
    edx = (int32_t) bx;
    goto label_0;
label_1:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x40c0 */
 
int64_t dbg_print_s_short (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_s_short(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r12 = rcx;
    rcx = rdi - 1;
    r15 = rdx;
    rcx *= rax;
    *((rsp + 0x18)) = rax;
    r14d = r9d;
    r13 = rcx;
    while (*(obj.input_swap) == 0) {
        edx = *(r15);
label_0:
        eax = 0;
        r15 += 2;
        xprintf (r12, rsi, rdx, rcx, r8, r9);
        r13 -= *((rsp + 0x18));
        if (rbp <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r13;
        esi = r14d;
        rbp--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        esi -= eax;
        esi += *((rsp + 0x14));
        r14d = eax;
    }
    bl = *((r15 + 1));
    eax = *(r15);
    bh = al;
    edx = (int32_t) bx;
    goto label_0;
label_1:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x4170 */
 
uint64_t dbg_print_char (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_char(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    if (rdi <= rsi) {
        goto label_0;
    }
    r14 = (int64_t) r9d;
    rax = rdx + rdi;
    rbx = rdi;
    r8 *= r14;
    *((rsp + 0x18)) = rax;
    r12 = rcx;
    r13d = r9d;
    r15 = rdx;
    do {
        rax = rbp;
        rax = rdx:rax / rbx;
        rdx = rdx:rax % rbx;
        edx = *(r15);
        r15++;
        rbp -= r14;
        esi -= eax;
        r13d = eax;
        esi += *((rsp + 0x14));
        eax = 0;
        xprintf (r12, r13d, 0, rcx, rdi - 1, r9);
        rax = *((rsp + 0x18));
        rax -= r15;
    } while (*((rsp + 8)) < rax);
label_0:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x4200 */
 
uint64_t dbg_print_s_char (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void print_s_char(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    if (rdi <= rsi) {
        goto label_0;
    }
    r14 = (int64_t) r9d;
    rax = rdx + rdi;
    rbx = rdi;
    r8 *= r14;
    *((rsp + 0x18)) = rax;
    r12 = rcx;
    r13d = r9d;
    r15 = rdx;
    do {
        rax = rbp;
        rax = rdx:rax / rbx;
        rdx = rdx:rax % rbx;
        edx = *(r15);
        r15++;
        rbp -= r14;
        esi -= eax;
        r13d = eax;
        esi += *((rsp + 0x14));
        eax = 0;
        xprintf (r12, r13d, 0, rcx, rdi - 1, r9);
        rax = *((rsp + 0x18));
        rax -= r15;
    } while (*((rsp + 8)) < rax);
label_0:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x4290 */
 
void dbg_print_long_double (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg5, int64_t arg6) {
    char[45] buf;
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_68h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    /* void print_long_double(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r8 = rdi - 1;
    r15 = rdx;
    r12d = r9d;
    r8 *= rax;
    *((rsp + 0x18)) = rax;
    r13 = rdi;
    rbx = rsp + 0x30;
    r14 = r8;
    while (*(obj.input_swap) == 0) {
        *(fp_stack--) = fp_stack[?];
label_0:
        ? = fp_stack[0];
        fp_stack--;
        r15 += 0x10;
        eax = ldtoastr (rbx, 0x2d, 0, 0, r8, r9);
        eax = 0;
        xprintf (0x0000d01c, ebp, rbx, rcx, r8, r9);
        r14 -= *((rsp + 0x28));
        if (r13 <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = r14;
        r13--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        ebp -= eax;
        ebp += *((rsp + 0x14));
        r12d = eax;
    }
    rdx = r15 + 0xf;
    rax = rsp + 0x20;
    do {
        ecx = *(rdx);
        rax++;
        rdx--;
        *((rax - 1)) = cl;
    } while (rbx != rax);
    *(fp_stack--) = fp_stack[?];
    goto label_0;
label_1:
    rax = *((rsp + 0x68));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x43b0 */
 
int64_t dbg_print_double (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg5, int64_t arg6) {
    char[40] buf;
    uint32_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    /* void print_double(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    rbx = rdi - 1;
    r15 = rdx;
    r12d = r9d;
    *((rsp + 0x18)) = rax;
    rbx *= rax;
    r14 = rdi;
    r13 = rsp + 0x30;
    while (*(obj.input_swap) == 0) {
        xmm0 = *(r15);
label_0:
        eax = dtoastr (r13, 0x28, 0, 0, r8);
        eax = 0;
        r15 += 8;
        xprintf (0x0000d01c, ebp, r13, rcx, r8, r9);
        rbx -= *((rsp + 0x18));
        if (r14 <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = rbx;
        r14--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        ebp -= eax;
        ebp += *((rsp + 0x14));
        r12d = eax;
    }
    rdx = r15 + 7;
    rax = rsp + 0x28;
    do {
        ecx = *(rdx);
        rax++;
        rdx--;
        *((rax - 1)) = cl;
    } while (r13 != rax);
    xmm0 = *((rsp + 0x28));
    goto label_0;
label_1:
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x44c0 */
 
int64_t dbg_print_float (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg5, int64_t arg6) {
    char[31] buf;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    /* void print_float(size_t fields,size_t blank, const * block,char const * fmt_string,int width,int pad); */
    *(rsp) = rdi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x14)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    rbx = rdi - 1;
    *((rsp + 0x10)) = r9d;
    r14 = rdi;
    *((rsp + 0x18)) = rax;
    rbx *= rax;
    r13 = rsp + 0x20;
    r12 = rdx;
    while (*(obj.input_swap) == 0) {
        xmm0 = *(r12);
label_0:
        eax = ftoastr (r13, 0x1f, 0, 0, r8);
        eax = 0;
        r12 += 4;
        xprintf (0x0000d01c, ebp, r13, rcx, r8, r9);
        rbx -= *((rsp + 0x18));
        if (r14 <= *((rsp + 8))) {
            goto label_1;
        }
        edx = 0;
        rax = rbx;
        ebp = *((rsp + 0x10));
        r14--;
        rax = *(rdx:rax) / rsp;
        rdx = *(rdx:rax) % rsp;
        ebp -= eax;
        ebp += *((rsp + 0x14));
        *((rsp + 0x10)) = eax;
    }
    r15b = *((r12 + 3));
    ecx = *((r12 + 2));
    edx = *((r12 + 1));
    eax = r15d;
    ah = cl;
    edx <<= 0x10;
    r11d = (int32_t) ax;
    eax = *(r12);
    r11d |= edx;
    eax <<= 0x18;
    r11d |= eax;
    r15d = r11d;
    xmm0 = r11d;
    goto label_0;
label_1:
    rax = *((rsp + 0x48));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x45f0 */
 
int64_t dbg_print_ascii (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg5, int64_t arg6) {
    char[4] buf;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_44h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    /* void print_ascii(size_t fields,size_t blank, const * block,char const * unused_fmt_string,int width,int pad); */
    *((rsp + 8)) = rsi;
    *((rsp + 0x10)) = rdx;
    *((rsp + 0x1c)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    if (rdi <= rsi) {
        goto label_1;
    }
    rax = (int64_t) r9d;
    r12d = r9d;
    r9 = rdi - 1;
    r9 *= rax;
    *((rsp + 0x20)) = rax;
    r15 = 0x0000d01c;
    r13 = rdx;
    r14 = r9;
    do {
        rax = r14;
        edx = 0;
        r8d = *(r13);
        ebx = r12d;
        rax = rdx:rax / rbp;
        rdx = rdx:rax % rbp;
        r13++;
        edx = (int32_t) r8b;
        r11 = rax;
        r12d = eax;
        if (r8b <= 0xd) {
            rcx = 0x0000d260;
            rax = *((rcx + rdx*4));
            rax += rcx;
            /* switch table (14 cases) at 0xd260 */
            void (*rax)() ();
        }
        *((rsp + 0x30)) = r11;
        *((rsp + 0x38)) = rdx;
        *((rsp + 0x28)) = r8b;
        rax = ctype_b_loc ();
        rdx = *((rsp + 0x38));
        r8d = *((rsp + 0x28));
        rcx = "%03o";
        rax = *(rax);
        rdi = rsp + 0x44;
        esi = 1;
        *((rsp + 0x28)) = rdi;
        rax = 0x0000d020;
        edx = 4;
        if ((*((rax + rdx*2 + 1)) & 0x40) != 0) {
            rcx = rax;
        }
        eax = 0;
        sprintf_chk ();
        r11 = *((rsp + 0x30));
label_0:
        eax = *((rsp + 0x1c));
        ebx -= r11d;
        eax = 0;
        xprintf (r15, rbx + rax, *((rsp + 0x28)), rcx, r8, r9);
        rax = rbp;
        r14 -= *((rsp + 0x20));
        rax -= r13;
        rax += *((rsp + 0x10));
    } while (*((rsp + 8)) < rax);
label_1:
    rax = *((rsp + 0x48));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
        rdx = 0x0000d037;
        goto label_0;
        rdx = 0x0000d031;
        goto label_0;
        rdx = 0x0000d03d;
        goto label_0;
        rdx = 0x0000d034;
        goto label_0;
        rdx = 0x0000d03a;
        goto label_0;
        rdx = 0x0000d02e;
        goto label_0;
        rdx = 0x0000d028;
        goto label_0;
        rdx = 0x0000d02b;
        goto label_0;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x47d0 */
 
uint64_t dbg_open_next_file (void) {
    /* _Bool open_next_file(); */
    r13 = 0x0000e7bc;
    r12d = 1;
    rbp = 0x0000d038;
    rbx = "standard input";
    while (*(rdi) == 0x2d) {
        if (*((rdi + 1)) != 0) {
            goto label_1;
        }
        edx = 5;
        rax = dcgettext (0, rbx);
        *(obj.have_read_stdin) = 1;
        *(obj.input_filename) = rax;
        rax = stdin;
        *(obj.in_stream) = rax;
label_0:
        if (rax != 0) {
            goto label_2;
        }
        rax = file_list;
        rdi = *(rax);
        *(obj.input_filename) = rdi;
        if (rdi == 0) {
            goto label_3;
        }
        rax += 8;
        *(obj.file_list) = rax;
    }
label_1:
    rax = fopen (rdi, rbp);
    *(obj.in_stream) = rax;
    if (rax == 0) {
        goto label_4;
    }
label_2:
    if (*(obj.limit_bytes_to_format) == 0) {
        goto label_3;
    }
    while (1) {
label_3:
        eax = r12d;
        return rax;
        setvbuf (rax, 0, 2, 0);
    }
label_4:
    rdx = input_filename;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    r12d = 0;
    error (0, *(rax), r13);
    rax = in_stream;
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x48e0 */
 
int64_t dbg_check_and_close (int64_t arg1) {
    rdi = arg1;
    /* _Bool check_and_close(int in_errno); */
    rdi = in_stream;
    eax = 1;
    if (rdi == 0) {
        goto label_2;
    }
    eax = 0;
    if ((*(rdi) & 0x20) == 0) {
    }
    rax = file_list;
    rax = *((rax - 8));
    if (*(rax) != 0x2d) {
        goto label_3;
    }
    if (*((rax + 1)) != 0) {
        goto label_3;
    }
    clearerr_unlocked ();
    do {
label_0:
        eax = 1;
        if (ebp != 0) {
            goto label_4;
        }
label_1:
        *(obj.in_stream) = 0;
label_2:
        rdx = stdout;
        if ((*(rdx) & 0x20) != 0) {
            goto label_5;
        }
        return rax;
label_3:
        eax = rpl_fclose (rdi);
    } while (eax == 0);
    if (ebp == 0) {
        rax = errno_location ();
        ebp = *(rax);
        goto label_0;
label_5:
        edx = 5;
        rax = dcgettext (0, "write error");
        eax = 0;
        eax = error (0, 0, rax);
        eax = 0;
        return rax;
    }
label_4:
    rdx = input_filename;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    rcx = rax;
    eax = 0;
    eax = error (0, ebp, 0x0000e7bc);
    eax = 0;
    goto label_1;
}

/* /tmp/tmpwp339vba @ 0x49c0 */
 
int64_t dbg_skip (int64_t arg1) {
    stat file_stats;
    char[8192] buf;
    int64_t var_2098h;
    rdi = arg1;
    /* _Bool skip(uintmax_t n_skip); */
    rax = *(fs:0x28);
    *((rsp + 0x2098)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
    r13 = in_stream;
    r12d = 1;
    if (r13 == 0) {
        goto label_5;
    }
    r14 = rdi;
    r15 = 0x0000e7bc;
    rbx = rsp + 0x90;
label_1:
    eax = fileno (r13);
    eax = fstat (eax, rbp);
    if (eax != 0) {
        goto label_6;
    }
    eax = *((rsp + 0x18));
    eax &= obj._IO_stdin_used;
    if (eax == 0x8000) {
        goto label_7;
    }
    eax = rpl_fseeko (r13, r14, 1, rcx);
    if (eax == 0) {
        goto label_3;
    }
label_0:
    r13d = _init;
    do {
        edx = 1;
        esi = _init;
        rdi = rbx;
        if (r13 > r14) {
            r13 = r14;
        }
        r8 = in_stream;
        rcx = r13;
        rax = fread_unlocked_chk ();
        r14 -= rax;
        if (r13 != rax) {
            rax = in_stream;
            eax = *(rax);
            if ((al & 0x20) != 0) {
                goto label_8;
            }
            if ((al & 0x10) != 0) {
                goto label_9;
            }
        }
    } while (r14 != 0);
label_3:
    rax = *((rsp + 0x2098));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_10;
    }
    eax = r12d;
    return rax;
label_7:
    rcx = 0x1fffffffffffffff;
    rax = *((rsp + 0x38));
    rdx = rax - 1;
    ecx = 0x200;
    rdx = *((rsp + 0x30));
    if (rdx > rcx) {
        rax = rcx;
    }
    if (rdx <= rax) {
        goto label_0;
    }
    if (rdx >= r14) {
        goto label_11;
    }
    r14 -= rdx;
label_2:
    eax = check_and_close (0);
    r12d &= eax;
    eax = open_next_file ();
    r13 = in_stream;
    r12d &= eax;
    if (r13 != 0) {
        goto label_1;
    }
label_5:
    edx = 5;
    rax = dcgettext (0, "cannot skip past end of combined input");
    eax = 0;
    error (1, 0, rax);
label_6:
    rdx = input_filename;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    r12d = 0;
    error (0, *(rax), r15);
    goto label_2;
label_9:
    if (r14 != 0) {
        goto label_2;
    }
    goto label_3;
label_8:
    r12d = 0;
    goto label_3;
label_11:
    eax = rpl_fseeko (r13, r14, 1, rcx);
    eax = 0;
    if (eax != 0) {
        r12d = eax;
    }
    goto label_3;
label_4:
    r12d = 1;
    goto label_3;
label_10:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x4c10 */
 
uint64_t dbg_read_char (int64_t arg1) {
    rdi = arg1;
    /* _Bool read_char(int * c); */
    r12d = 1;
    *(rdi) = 0xffffffff;
    rdi = in_stream;
    if (rdi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rax = errno_location ();
        eax = check_and_close (*(rax));
        ebx = eax;
        eax = open_next_file ();
        rdi = in_stream;
        ebx &= eax;
        r12d &= ebx;
        if (rdi == 0) {
            goto label_1;
        }
label_0:
        eax = fgetc (rdi);
        *(rbp) = eax;
    } while (eax == 0xffffffff);
label_1:
    eax = r12d;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x4c80 */
 
int64_t dbg_write_block (int64_t arg1, size_t n, void * s1, void * s2) {
    signed int64_t var_4h;
    int64_t var_8h;
    rdi = arg1;
    rsi = n;
    rdx = s1;
    rcx = s2;
    /* void write_block(uintmax_t current_offset,size_t n_bytes,char const * prev_block,char const * curr_block); */
    r14 = rcx;
    r13 = rsi;
    *((rsp + 8)) = rdi;
    if (*(obj.abbreviate_duplicate_blocks) != 0) {
        if (*(0x00013010) == 0) {
            goto label_5;
        }
    }
label_3:
    *(0x00013100) = 0;
    if (*(obj.n_specs) == 0) {
        goto label_6;
    }
    ebp = 0;
    while (rbp == 0) {
        rdi = *((rsp + 8));
        esi = 0;
        uint64_t (*format_address)() ();
label_0:
        r10 = spec;
        rsi = (int64_t) ebx;
        rdi = (int64_t) r15d;
        rdx = r14;
        r10 += r12;
        r9d = *((r10 + 0x20));
        r8d = *((r10 + 0x1c));
        rcx = r10 + 0x10;
        uint64_t (*r10 + 8)() ();
        r12 += *(obj.spec);
        if (*((r12 + 0x18)) != 0) {
            goto label_7;
        }
label_1:
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_8;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
label_4:
        rbp++;
        if (*(obj.n_specs) <= rbp) {
            goto label_6;
        }
        rdx = spec;
        rax = rbp + rbp*4;
        rbx = bytes_per_block;
        rsi = obj_width_bytes;
        r12 = rax*8;
        eax = *((rdx + rax*8 + 4));
        edx = 0;
        rcx = *((rsi + rax*4));
        rax = rbx;
        rbx -= r13;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        edx = 0;
        *((rsp + 4)) = eax;
        r15 = rax;
        rax = rbx;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        rbx = rax;
    }
    edx = address_pad_len;
    rcx = 0x0000ec81;
    rsi = 0x0000d01c;
    eax = 0;
    edi = 1;
    printf_chk ();
    goto label_0;
label_7:
    eax = *((r12 + 0x20));
    rcx = 0x0000ec81;
    edi = 1;
    rsi = 0x0000d01c;
    eax *= ebx;
    ebx *= *((r12 + 0x1c));
    edx:eax = (int64_t) eax;
    eax = *(edx:eax) / rsp + 4;
    edx = *(edx:eax) % rsp + 4;
    edx = rax + rbx;
    eax = 0;
    printf_chk ();
    edx = 3;
    rcx = stdout;
    esi = 1;
    rdi = 0x0000d05d;
    fwrite_unlocked ();
    if (r13 == 0) {
        goto label_9;
    }
    rax = ctype_b_loc ();
    r15 = r14 + r13;
    rbx = r14;
    r12 = rax;
    do {
        ecx = *(rbx);
        rdx = *(r12);
        rbx++;
        rax = rcx;
        esi = (int32_t) cl;
        if ((*((rdx + rcx*2 + 1)) & 0x40) == 0) {
            esi = 0x2e;
            eax = 0x2e;
        }
        rdi = stdout;
        rdx = *((rdi + 0x28));
        if (rdx >= *((rdi + 0x30))) {
            goto label_10;
        }
        rcx = rdx + 1;
        *((rdi + 0x28)) = rcx;
        *(rdx) = al;
label_2:
    } while (rbx != r15);
label_9:
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_11;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x3c;
    goto label_1;
label_10:
    overflow ();
    goto label_2;
label_5:
    if (*(obj.bytes_per_block) != rsi) {
        goto label_3;
    }
    eax = memcmp (rdx, rcx, rsi);
    if (eax != 0) {
        goto label_3;
    }
    if (*(0x00013100) == 0) {
        puts (0x0000d05b);
        *(0x00013100) = 1;
    }
label_6:
    *(0x00013010) = 0;
    return rax;
label_8:
    esi = 0xa;
    overflow ();
    goto label_4;
label_11:
    esi = 0x3c;
    overflow ();
    goto label_1;
}

/* /tmp/tmpwp339vba @ 0x4f20 */
 
int64_t format_address_paren (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_0;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x28;
    do {
        format_address_std (rbp, 0x29);
        if (bl != 0) {
            rdi = stdout;
            rax = *((rdi + 0x28));
            if (rax >= *((rdi + 0x30))) {
                goto label_1;
            }
            rdx = rax + 1;
            *((rdi + 0x28)) = rdx;
            *(rax) = bl;
        }
        return rax;
label_1:
        esi = (int32_t) bl;
        void (*0x25a0)() ();
label_0:
        esi = 0x28;
        overflow ();
    } while (1);
}

/* /tmp/tmpwp339vba @ 0x4fa0 */
 
void dbg_format_address_label (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void format_address_label(uintmax_t address,char c); */
    rbx = rdi;
    format_address_std (rdi, 0x20);
    rbx += *(obj.pseudo_offset);
    esi = (int32_t) bpl;
    rdi = rbx;
    return format_address_paren ();
}

/* /tmp/tmpwp339vba @ 0x4fe0 */
 
uint64_t read_block (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    if (rdi == 0) {
        goto label_0;
    }
    r13 = rdi;
    if (*(obj.bytes_per_block) < rdi) {
        goto label_0;
    }
    rcx = in_stream;
    *(rdx) = 0;
    if (rcx == 0) {
        goto label_1;
    }
    r14 = rsi;
    edi = 0;
    r12d = 1;
    while (rbx != rax) {
        rax = errno_location ();
        eax = check_and_close (*(rax));
        ebx = eax;
        eax = open_next_file ();
        rcx = in_stream;
        ebx &= eax;
        r12d &= ebx;
        if (rcx == 0) {
            goto label_2;
        }
        rdi = *(rbp);
        rbx = r13;
        esi = 1;
        rbx -= rdi;
        rdi += r14;
        rdx = rbx;
        rax = fread_unlocked ();
        *(rbp) += rax;
    }
    do {
label_2:
        eax = r12d;
        return rax;
label_1:
        r12d = 1;
    } while (1);
label_0:
    return assert_fail ("0 < n && n <= bytes_per_block", "src/od.c", 0x50d, "read_block");
}

/* /tmp/tmpwp339vba @ 0x50a0 */
 
int64_t decode_format_string_part_0 (int64_t arg_4h, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1ch, uint32_t arg1) {
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    r13 = 0xcccccccccccccccd;
    r12 = rdi;
    rax = n_specs;
    *((rsp + 0x10)) = rdi;
    if (*(rdi) == 0) {
        goto label_8;
    }
label_0:
    rdi = spec;
    if (*(obj.n_specs_allocated) <= rax) {
        goto label_9;
    }
    rax *= 5;
    rbp = rdi + rax*8;
    if (rbp == 0) {
        goto label_10;
    }
    esi = *(r12);
    if (sil == 0x61) {
        goto label_11;
    }
    do {
        ecx = rsi - 0x63;
        if (cl <= 0x15) {
            r9d = 1;
            r9 <<= cl;
            r8 = r9;
            r8d &= 0x241002;
            if (r8d != 0) {
                goto label_12;
            }
            r9d &= 1;
            if (r9d != 0) {
                goto label_13;
            }
            if (cl == 3) {
                goto label_14;
            }
        }
        rax = quote (*((rsp + 0x10)), rsi, rdx, rcx, r8);
        r12d = *(r12);
        edx = 5;
        rbx = rax;
        rax = dcgettext (0, "invalid character '%c' in type string %s");
        r8 = rbx;
        ecx = r12d;
        eax = 0;
        error (0, 0, rax);
        r9d = 0;
label_1:
        eax = r9d;
        return rax;
label_9:
        edx = 0x28;
        rsi = obj_n_specs_allocated;
        rax = x2nrealloc ();
        rdx = n_specs;
        esi = *(r12);
        *(obj.spec) = rax;
        rdx *= 5;
        rbp = rax + rdx*8;
    } while (sil != 0x61);
label_11:
    rbx = r12 + 1;
    r15d = 3;
    edx = 5;
    r14d = 1;
    rax = dbg_print_named_ascii;
label_2:
    *((rbp + 8)) = rax;
    *((rbp + 4)) = r14d;
    *(rbp) = edx;
    *((rbp + 0x1c)) = r15d;
    al = (*(rbx) == 0x7a) ? 1 : 0;
    *((rbp + 0x18)) = al;
    eax = (int32_t) al;
    rbx += rax;
    if (rbx == r12) {
        goto label_15;
    }
    rax = n_specs;
    rax++;
    *(obj.n_specs) = rax;
    if (*(rbx) == 0) {
        goto label_8;
    }
    r12 = rbx;
    goto label_0;
label_14:
    eax = *((r12 + 1));
    if (al == 0x46) {
        goto label_16;
    }
    if (al == 0x4c) {
        goto label_17;
    }
    if (al == 0x44) {
        goto label_18;
    }
    eax -= 0x30;
    rsi = r12 + 1;
    rbx = r12 + 2;
    rcx = (int64_t) eax;
    if (eax > 9) {
        goto label_19;
    }
    do {
        rax = r8 * 5;
        r8 = rcx + rax*2;
        eax = *(rbx);
        eax -= 0x30;
        if (eax > 9) {
            goto label_20;
        }
        rcx = (int64_t) eax;
        rbx++;
        rdx = rcx;
        rdx = ~rdx;
        rax = rdx;
        rdx:rax = rax * r13;
        rdx >>= 3;
    } while (rdx >= r8);
label_7:
    rax = quote (*((rsp + 0x10)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid type string %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    r9d = 0;
    goto label_1;
label_13:
    rbx = r12 + 1;
    r15d = 3;
    edx = 6;
    r14d = 1;
    rax = dbg_print_ascii;
    goto label_2;
label_12:
    eax = *((r12 + 1));
    if (al == 0x4c) {
        goto label_21;
    }
    if (al > 0x4c) {
        goto label_22;
    }
    if (al == 0x43) {
        goto label_23;
    }
    if (al != 0x49) {
        goto label_24;
    }
    r14d = *(0x00013190);
    rbx = r12 + 2;
    r8d = 4;
label_6:
    if (sil == 0x75) {
        goto label_25;
    }
    if (sil > 0x75) {
label_3:
        goto label_26;
    }
    if (sil != 0x64) {
        goto label_27;
    }
    rax = obj_bytes_to_signed_dec_digits;
    rdi = rbp + 0x10;
    edx = 8;
    esi = 1;
    r15d = *((rax + r8*4));
    eax = r14 - 4;
    r8 = 0x0000d088;
    *((rsp + 8)) = rdi;
    rax = 0x0000ef9e;
    rcx = "%%*%s";
    if (eax >= 2) {
        r8 = rax;
    }
    eax = 0;
    sprintf_chk ();
    edx = 0;
label_4:
    *((rsp + 8)) = edx;
    rax = strlen (*((rsp + 8)));
    edx = *((rsp + 8));
    if (rax > 7) {
        goto label_28;
    }
    if (r14d > 5) {
        void (*0x2800)() ();
    }
    rcx = 0x0000d298;
    eax = r14d;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (6 cases) at 0xd298 */
    al = void (*rax)() ();
label_22:
    if (al != 0x53) {
        goto label_29;
    }
    r14d = *(0x00013188);
    rbx = r12 + 2;
    r8d = 2;
    if (sil != 0x75) {
        goto label_3;
    }
label_25:
    rax = obj_bytes_to_unsigned_dec_digits;
    rdi = rbp + 0x10;
    edx = 8;
    esi = 1;
    r15d = *((rax + r8*4));
    eax = r14 - 4;
    r8 = 0x0000d08e;
    *((rsp + 8)) = rdi;
    rax = 0x0000d08f;
    rcx = "%%*%s";
    if (eax >= 2) {
        r8 = rax;
    }
    eax = 0;
    sprintf_chk ();
    rdi = *((rsp + 8));
    edx = 1;
    goto label_4;
label_27:
    if (sil != 0x6f) {
        goto label_30;
    }
    eax = r14 - 4;
    rdi = rbp + 0x10;
    edx = 8;
    esi = 1;
    r9 = 0x0000d08b;
    *((rsp + 8)) = rdi;
    rax = 0x0000d026;
    if (eax >= 2) {
        r9 = rax;
    }
    rax = obj_bytes_to_oct_digits;
    rcx = "%%*.%d%s";
    r15d = *((rax + r8*4));
    eax = 0;
    r8d = r15d;
    sprintf_chk ();
    rdi = *((rsp + 8));
    edx = 2;
    goto label_4;
label_26:
    if (sil != 0x78) {
        goto label_31;
    }
    eax = r14 - 4;
    rdi = rbp + 0x10;
    edx = 8;
    esi = 1;
    r9 = 0x0000d091;
    *((rsp + 8)) = rdi;
    rax = 0x0000d1ff;
    if (eax >= 2) {
        r9 = rax;
    }
    rax = obj_bytes_to_hex_digits;
    rcx = "%%*.%d%s";
    r15d = *((rax + r8*4));
    eax = 0;
    r8d = r15d;
    sprintf_chk ();
    rdi = *((rsp + 8));
    edx = 3;
    goto label_4;
label_18:
    r14d = *(0x00013140);
    rbx = r12 + 2;
label_5:
    rax = localeconv ();
    r15d = 1;
    rdi = *(rax);
    if (*(rdi) != 0) {
        rax = strlen (rdi);
        r15 = rax;
    }
    if (r14d == 7) {
        goto label_32;
    }
    if (r14d == 8) {
        r15d += 0x1c;
        rax = dbg_print_long_double;
        edx = 4;
        goto label_2;
    }
    if (r14d != 6) {
        void (*0x2800)() ();
    }
    r15d += 0xe;
    rax = dbg_print_float;
    edx = 4;
    goto label_2;
label_32:
    r15d += 0x17;
    rax = dbg_print_double;
    edx = 4;
    goto label_2;
label_17:
    r14d = *(0x00013160);
    rbx = r12 + 2;
    goto label_5;
label_16:
    r14d = *(0x00013130);
    rbx = r12 + 2;
    goto label_5;
label_21:
    r14d = *(0x000131a0);
    rbx = r12 + 2;
    r8d = 8;
    goto label_6;
label_23:
    r14d = *(0x00013184);
    rbx = r12 + 2;
    r8d = 1;
    goto label_6;
label_8:
    r9d = 1;
    goto label_1;
label_24:
    ecx = (int32_t) al;
    rdi = r12 + 1;
    ecx -= 0x30;
    if (ecx > 9) {
        goto label_33;
    }
    rbx = r12 + 2;
    rcx = (int64_t) ecx;
    r8d = 0;
    do {
        rax = r8 * 5;
        r8 = rcx + rax*2;
        eax = *(rbx);
        eax -= 0x30;
        if (eax > 9) {
            goto label_34;
        }
        rcx = (int64_t) eax;
        rbx++;
        rdx = rcx;
        rdx = ~rdx;
        rax = rdx;
        rdx:rax = rax * r13;
        rdx >>= 3;
    } while (rdx >= r8);
    goto label_7;
    rax = dbg_print_long_long;
    goto label_2;
    rax = dbg_print_long;
    goto label_2;
    rax = dbg_print_int;
    goto label_2;
    rax = dbg_print_s_short;
    rcx = dbg_print_short;
    if (edx != 0) {
        rax = rcx;
    }
    goto label_2;
    rax = dbg_print_s_char;
    rcx = dbg_print_char;
    if (edx != 0) {
        rax = rcx;
    }
    goto label_2;
label_20:
    r14d = *(0x00013140);
    if (rsi == rbx) {
        goto label_5;
    }
    if (r8 > 0x10) {
        goto label_35;
    }
    rax = obj_fp_type_size;
    r14d = *((rax + r8*4));
    if (r14d != 0) {
        goto label_5;
    }
label_35:
    *((rsp + 8)) = r9b;
    *((rsp + 0x18)) = r8;
    rax = quote (*((rsp + 0x10)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid type string %s;\nthis system doesn't provide a %lu-byte floating point type");
    r8 = *((rsp + 0x18));
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    r9d = *((rsp + 8));
    goto label_1;
label_34:
    if (rdi == rbx) {
        goto label_36;
    }
    if (r8 > 8) {
        goto label_37;
    }
    rax = obj_integral_type_size;
    r14d = *((rax + r8*4));
    if (r14d != 0) {
        goto label_6;
    }
label_37:
    *((rsp + 8)) = r8;
    rax = quote (*((rsp + 0x10)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid type string %s;\nthis system doesn't provide a %lu-byte integral type");
    r8 = *((rsp + 8));
    rcx = r12;
    eax = 0;
    rax = error (0, 0, rax);
    r9d = 0;
    goto label_1;
label_29:
    rbx = r12 + 1;
    do {
label_36:
        r14d = *(0x00013190);
        r8d = 4;
        goto label_6;
label_19:
        r14d = *(0x00013140);
        rbx = rsi;
        goto label_5;
label_15:
        assert_fail ("s != next", "src/od.c", 0x3e5, "decode_format_string");
label_10:
        assert_fail ("tspec != NULL", "src/od.c", 0x288, "decode_one_format");
label_28:
        assert_fail ("strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format");
label_33:
        rbx = rdi;
    } while (1);
label_31:
    void (*0x2800)() ();
label_30:
    return decode_format_string_part_0_cold ();
}

/* /tmp/tmpwp339vba @ 0x2800 */
 
void decode_format_string_part_0_cold (void) {
    /* [16] -r-x section size 39730 named .text */
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x6900 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000e86d;
        rdx = 0x0000e860;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000e867;
        rdx = 0x0000efd9;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000e869;
    rdx = 0x0000e864;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpwp339vba @ 0xa1d0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpwp339vba @ 0x26a0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpwp339vba @ 0x69e0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2805)() ();
    }
    rdx = 0x0000e8c0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xe8c0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000e871;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000efd9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000e8ec;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xe8ec */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000e867;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000efd9;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000e867;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000efd9;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000e9ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xe9ec */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000eaec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xeaec */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000efd9;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000e867;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000e867;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000efd9;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpwp339vba @ 0x2805 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x7e00 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x280a)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x280a */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x280f */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2440 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpwp339vba @ 0x2815 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x281a */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x281f */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2824 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2829 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x282e */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2833 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2838 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x283d */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2842 */
 
void xstrtol_fatal_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2847 */
 
void vasnprintf_cold (void) {
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x3910 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x3940 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x3980 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002410 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpwp339vba @ 0x2410 */
 
void fcn_00002410 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpwp339vba @ 0x39c0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpwp339vba @ 0x5d90 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x5880)() ();
}

/* /tmp/tmpwp339vba @ 0x2530 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpwp339vba @ 0x27a0 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpwp339vba @ 0x2780 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpwp339vba @ 0x26f0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpwp339vba @ 0x2620 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpwp339vba @ 0x26e0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpwp339vba @ 0x2460 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpwp339vba @ 0xc320 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpwp339vba @ 0x8350 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x8680 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x2560 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpwp339vba @ 0xa1c0 */
 
void dbg_fseterr (FILE * fp) {
    rdi = fp;
    /* void fseterr(FILE * fp); */
    *(rdi) |= 0x20;
}

/* /tmp/tmpwp339vba @ 0x65a0 */
 
void dbg_ftoastr (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg7) {
    char[11] format;
    int64_t var_10h;
    int64_t var_ch;
    int64_t var_1dh;
    int64_t var_1eh;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    /* int ftoastr(char * buf,size_t bufsize,int flags,int width,float x); */
    xmm3 = xmm0;
    xmm2 = 0;
    xmm5 = 0;
    xmm1 = xmm3;
    __asm ("cvtss2sd xmm5, xmm0");
    r15d = 1;
    r13d = ecx;
    __asm ("cmpltss xmm1, xmm2");
    ecx = 0x2d25;
    rbx = rsi;
    esi = 0x2e2a;
    *((rsp + 0xc)) = xmm0;
    __asm ("xorps xmm0, xmmword [0x0000e7f0]");
    r14 = rsp + 0x1d;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    *((rsp + 0x1d)) = cx;
    eax = edx;
    ecx = edx;
    eax &= 1;
    ecx >>= 1;
    __asm ("andps xmm0, xmm1");
    __asm ("andnps xmm1, xmm3");
    ecx &= 1;
    rax = rsp + rax + 0x1e;
    __asm ("orps xmm0, xmm1");
    *(rsp) = xmm5;
    *(rax) = 0x2b;
    rax += rcx;
    ecx = edx;
    xmm1 = *(0x0000e800);
    ecx >>= 2;
    *(rax) = 0x20;
    ecx &= 1;
    rax += rcx;
    ecx = edx;
    edx &= 0x10;
    ecx >>= 3;
    *(rax) = 0x30;
    ecx &= 1;
    rax += rcx;
    edx -= edx;
    *(rax) = si;
    edx &= 0x20;
    *((rax + 2)) = 0x2a;
    edx += 0x47;
    __asm ("comiss xmm1, xmm0");
    *((rax + 4)) = 0;
    *((rax + 3)) = dl;
    eax = 6;
    if (edx <= 0) {
        r15d = eax;
    }
    while (rax >= rbx) {
label_0:
        r15d++;
        edx = 1;
        r9d = r13d;
        r8 = r14;
        xmm0 = *((rsp + 0x10));
        rsi = rbx;
        rdi = rbp;
        rcx = 0xffffffffffffffff;
        eax = 1;
        eax = snprintf_chk ();
        r12d = eax;
        if (r12d < 0) {
            goto label_1;
        }
        if (r15d > 8) {
            goto label_1;
        }
        rax = (int64_t) r12d;
    }
    strtof (rbp, 0);
    __asm ("ucomiss xmm0, dword [rsp + 0xc]");
    if (rax == rbx) {
        goto label_0;
    }
    if (rax != rbx) {
        goto label_0;
    }
label_1:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        eax = r12d;
        return;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x9110 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x9890 */
 
uint64_t xvprintf (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    eax = rpl_vprintf (rdi, rsi);
    r12d = eax;
    while (eax != 0) {
label_0:
        eax = r12d;
        return eax;
        eax = ferror (*(obj.stdout));
    }
    edx = 5;
    rax = dcgettext (0, "cannot perform formatted output");
    r13 = rax;
    rax = errno_location ();
    eax = 0;
    error (*(obj.exit_failure), *(rax), r13);
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x8190 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x2450 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpwp339vba @ 0x9310 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x2680 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpwp339vba @ 0x9850 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000e7bc);
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x2720 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpwp339vba @ 0x26d0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpwp339vba @ 0x24b0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpwp339vba @ 0xa040 */
 
int64_t dbg_rpl_vfprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    size_t lenbuf;
    char[2000] buf;
    int64_t var_ch;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_7f8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_vfprintf(FILE * fp,char const * format,__va_list_tag * args); */
    r8 = rsi;
    rcx = rdx;
    rdx = r8;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x7f8)) = rax;
    eax = 0;
    r13 = rsp + 0x20;
    rsi = rsp + 0x18;
    *((rsp + 0x18)) = 0x7d0;
    rdi = r13;
    rax = vasnprintf ();
    rbx = *((rsp + 0x18));
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = fwrite (rdi, 1, rbx, r12);
    if (rax < rbx) {
        goto label_2;
    }
    if (rbp != r13) {
        free (rbp);
    }
    if (rbx > 0x7fffffff) {
        goto label_3;
    }
    eax = ebx;
    do {
label_0:
        rdx = *((rsp + 0x7f8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_4;
        }
        return rax;
label_2:
        eax = 0xffffffff;
    } while (rbp == r13);
    *((rsp + 0xc)) = eax;
    free (rbp);
    eax = *((rsp + 0xc));
    goto label_0;
label_3:
    errno_location ();
    *(rax) = 0x4b;
label_1:
    rdi = r12;
    fseterr ();
    eax = 0xffffffff;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x80b0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpwp339vba @ 0xa210 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x83a0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2815)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8110 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x6850 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpwp339vba @ 0x25b0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpwp339vba @ 0x2790 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpwp339vba @ 0x5f10 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x2720)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmpwp339vba @ 0x8ba0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x88e0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x282e)() ();
    }
    if (rdx == 0) {
        void (*0x282e)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8980 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2833)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2833)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x84c0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x281f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8840 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2829)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0xc334 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpwp339vba @ 0x9390 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x9b40 */
 
int64_t xstrtol_fatal (uint32_t arg1, char * arg2, int64_t arg4, int64_t arg5) {
    int64_t var_6h;
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r8 = arg5;
    rbx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    ebp = exit_failure;
    if (edi <= 3) {
        if (edi > 1) {
            goto label_3;
        }
        if (edi != 1) {
            goto label_4;
        }
        r9 = "%s%s argument '%s' too large";
label_2:
        rax = (int64_t) esi;
        if (esi < 0) {
            goto label_5;
        }
label_0:
        rax <<= 5;
        r12 = 0x0000eff8;
        r13 = *((rcx + rax));
label_1:
        edx = 5;
        rax = dcgettext (0, r9);
        r9 = rbx;
        r8 = r13;
        rcx = r12;
        eax = 0;
        error (ebp, 0, rax);
        abort ();
    }
    if (edi != 4) {
        void (*0x2842)() ();
    }
    r9 = "invalid %s%s argument '%s';
    rax = (int64_t) esi;
    if (esi >= 0) {
        goto label_0;
    }
label_5:
    r12 = 0x0000eff8;
    *((rsp + 6)) = dl;
    r13 = rsp + 6;
    *((rsp + 7)) = 0;
    r12 -= rax;
    goto label_1;
label_3:
    r9 = "invalid suffix in %s%s argument '%s';
    goto label_2;
label_4:
    return xstrtol_fatal_cold ();
}

/* /tmp/tmpwp339vba @ 0x9490 */
 
int64_t dbg_x2realloc (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x9ad0 */
 
uint64_t xvfprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = rpl_vfprintf (rdi, rsi, rdx);
    r12d = eax;
    while (eax != 0) {
label_0:
        eax = r12d;
        return eax;
        eax = ferror (rbp);
    }
    edx = 5;
    rax = dcgettext (0, "cannot perform formatted output");
    r13 = rax;
    rax = errno_location ();
    eax = 0;
    error (*(obj.exit_failure), *(rax), r13);
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x9330 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x64d0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2690)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpwp339vba @ 0x92d0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x8380 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x9810 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2660)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x2550 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpwp339vba @ 0x92f0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x5da0 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmpwp339vba @ 0x9050 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x8be0)() ();
}

/* /tmp/tmpwp339vba @ 0x6220 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000e7bc);
    } while (1);
}

/* /tmp/tmpwp339vba @ 0xb2a0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x9290 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0xa130 */
 
void dbg_rpl_vprintf (int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_7f8h;
    rdi = arg1;
    rsi = arg2;
    /* int rpl_vprintf(char const * format,__va_list_tag * args); */
    rdx = rsi;
    rsi = rdi;
    rdi = stdout;
    return void (*0xa040)() ();
}

/* /tmp/tmpwp339vba @ 0xb220 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x60d0 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmpwp339vba @ 0x8a30 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2838)() ();
    }
    if (rax == 0) {
        void (*0x2838)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x87b0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x96e0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x2630 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpwp339vba @ 0x8050 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x8b70 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x7ff0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x9750 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2660)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x8290 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x9900 */
 
int64_t dbg_xprintf (int64_t arg_e0h, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* int xprintf(char const * restrict format,va_args ...); */
    *((rsp + 0x28)) = rsi;
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = rsp + 0xe0;
    rsi = rsp;
    *(rsp) = 8;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    xvprintf ();
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x6210 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpwp339vba @ 0x9360 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x9520 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x5eb0 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x2640 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpwp339vba @ 0x6440 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2510)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x9440 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x8b80 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x8ad0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x283d)() ();
    }
    if (rax == 0) {
        void (*0x283d)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x62d0 */
 
void dbg_dtoastr (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg7) {
    char[11] format;
    int64_t var_8h;
    int64_t var_1dh;
    int64_t var_1eh;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    /* int dtoastr(char * buf,size_t bufsize,int flags,int width,double x); */
    __asm ("movapd xmm4, xmm0");
    xmm2 = 0;
    r15d = 1;
    __asm ("movapd xmm1, xmm4");
    r13d = ecx;
    __asm ("cmpltsd xmm1, xmm2");
    ecx = 0x2d25;
    rbx = rsi;
    esi = 0x2e2a;
    *((rsp + 8)) = xmm0;
    __asm ("xorpd xmm0, xmmword [0x0000e7d0]");
    r14 = rsp + 0x1d;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    *((rsp + 0x1d)) = cx;
    eax = edx;
    ecx = edx;
    eax &= 1;
    ecx >>= 1;
    __asm ("andpd xmm0, xmm1");
    __asm ("andnpd xmm1, xmm4");
    ecx &= 1;
    rax = rsp + rax + 0x1e;
    __asm ("orpd xmm0, xmm1");
    xmm1 = *(0x0000e7e0);
    *(rax) = 0x2b;
    rax += rcx;
    ecx = edx;
    ecx >>= 2;
    *(rax) = 0x20;
    ecx &= 1;
    rax += rcx;
    ecx = edx;
    edx &= 0x10;
    ecx >>= 3;
    *(rax) = 0x30;
    ecx &= 1;
    rax += rcx;
    edx -= edx;
    *(rax) = si;
    edx &= 0x20;
    *((rax + 2)) = 0x2a;
    edx += 0x47;
    __asm ("comisd xmm1, xmm0");
    *((rax + 4)) = 0;
    *((rax + 3)) = dl;
    eax = 0xf;
    if (edx <= 0) {
        r15d = eax;
    }
    while (rax >= rbx) {
label_0:
        r15d++;
        edx = 1;
        r9d = r13d;
        r8 = r14;
        xmm0 = *((rsp + 0x18));
        rsi = rbx;
        rdi = rbp;
        rcx = 0xffffffffffffffff;
        eax = 1;
        eax = snprintf_chk ();
        r12d = eax;
        if (r12d < 0) {
            goto label_1;
        }
        if (r15d > 0x10) {
            goto label_1;
        }
        rax = (int64_t) r12d;
    }
    strtod (rbp, 0);
    __asm ("ucomisd xmm0, qword [rsp + 8]");
    if (rax == rbx) {
        goto label_0;
    }
    if (rax != rbx) {
        goto label_0;
    }
label_1:
    rax = *((rsp + 0x28));
    rax -= *(fs:0x28);
    if (rax == 0) {
        eax = r12d;
        return;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x5fa0 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmpwp339vba @ 0x9790 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2660)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x8280 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x8190)() ();
}

/* /tmp/tmpwp339vba @ 0xa2a0 */
 
int64_t vasnprintf (void * arg1, void ** arg2, int64_t arg3, int64_t arg4) {
    int64_t var_418h;
    int64_t var_40ch;
    int64_t var_408h;
    void ** var_400h;
    int64_t var_3f8h;
    int64_t var_3f0h;
    void * s2;
    void * var_3e0h;
    int64_t var_3d8h;
    void ** var_3d0h;
    void ** var_3c8h;
    int64_t var_3bch;
    int64_t var_3b8h;
    int64_t var_3b4h;
    int64_t var_3b0h;
    void ** var_3a8h;
    int64_t var_3a0h;
    uint32_t var_2c0h;
    void ** var_2b8h;
    int64_t var_2b0h;
    int64_t var_2a8h;
    int64_t var_2a0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_0:
    abort ();
    r14 = section__gnu_hash;
    r13 = rdx;
    r12 = rcx;
    *((rbp - 0x3e8)) = rdi;
    *((rbp - section..dynsym)) = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = printf_parse (r13, rbp - 0x2c0, r14);
    if (eax < 0) {
        goto label_34;
    }
    eax = printf_fetchargs (r12, r14);
    if (eax < 0) {
        goto label_35;
    }
    rdx = *((rbp - 0x2b0));
    rax = rdx + 7;
    rdx = 0xffffffffffffffff;
    if (rdx >= 0xfffffffffffffff9) {
        rax = rdx;
    }
    rax += *((rbp - 0x2a8));
    if (rax < 0) {
        goto label_36;
    }
    rdi = rax;
    r8d = 0;
    rdi += 6;
    r8b = (rdi < 0) ? 1 : 0;
    if (rdi < 0) {
        goto label_36;
    }
    if (rdi <= 0xf9f) {
        goto label_37;
    }
    *((rbp - 0x3c8)) = r8;
    if (rdi == -1) {
        goto label_36;
    }
    rax = malloc (rdi);
    *((rbp - 0x3e0)) = rax;
    if (rax == 0) {
        goto label_36;
    }
    *((rbp - 0x408)) = rax;
    r8 = *((rbp - 0x3c8));
label_11:
    ebx = 0;
    if (*((rbp - 0x3e8)) != 0) {
        rax = *((rbp - section..dynsym));
        rbx = *(rax);
    }
    r14 = *((rbp - 0x2b8));
    r9 = r8;
    r8 = r13;
    *((rbp - 0x3f8)) = 0;
    r15 = *((rbp - 0x3e8));
    r13 = *(r14);
    if (r13 == r8) {
        goto label_38;
    }
label_15:
    r13 -= r8;
    r12 = r9;
    rax = 0xffffffffffffffff;
    r12 += r13;
    if (r12 < 0) {
        r12 = rax;
    }
    if (rbx >= r12) {
        goto label_39;
    }
    if (rbx == 0) {
        goto label_40;
    }
    if (rbx < 0) {
        goto label_41;
    }
    rbx += rbx;
label_17:
    if (rbx < r12) {
        rbx = r12;
    }
    if (rbx == -1) {
        goto label_41;
    }
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    if (r15 == *((rbp - 0x3e8))) {
        goto label_42;
    }
    rax = realloc (r15, rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
label_18:
    *((rbp - 0x3c8)) = r10;
    memcpy (r10 + r9, r8, r13);
    r10 = *((rbp - 0x3c8));
label_16:
    rax = *((rbp - 0x3f8));
    if (*((rbp - 0x2c0)) == rax) {
        goto label_43;
    }
    edx = *((r14 + 0x48));
    rax = *((r14 + 0x50));
    if (dl == 0x25) {
        goto label_44;
    }
    if (rax == -1) {
        void (*0x2847)() ();
    }
    r15 = *((rbp - 0x3a8));
    rax <<= 5;
    rax += r15;
    ecx = *(rax);
    *((rbp - 0x3c8)) = ecx;
    if (dl == 0x6e) {
        goto label_45;
    }
    rcx = *((rbp - 0x3e0));
    eax = *((r14 + 0x10));
    *((rbp - 0x3b8)) = 0;
    *(rcx) = 0x25;
    r13 = rcx + 1;
    if ((al & 1) != 0) {
        rdx = *((rbp - 0x3e0));
        *((rdx + 1)) = 0x27;
        r13 = rdx + 2;
    }
    if ((al & 2) != 0) {
        *(r13) = 0x2d;
        r13++;
    }
    if ((al & 4) != 0) {
        *(r13) = 0x2b;
        r13++;
    }
    if ((al & 8) != 0) {
        *(r13) = 0x20;
        r13++;
    }
    if ((al & 0x10) != 0) {
        *(r13) = 0x23;
        r13++;
    }
    if ((al & 0x40) != 0) {
        *(r13) = 0x49;
        r13++;
    }
    if ((al & 0x20) != 0) {
        *(r13) = 0x30;
        r13++;
    }
    rsi = *((r14 + 0x18));
    rax = *((r14 + 0x20));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    rsi = *((r14 + 0x30));
    rax = *((r14 + 0x38));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    eax = *((rbp - 0x3c8));
    eax -= 7;
    if (eax > 9) {
        goto label_12;
    }
    rcx = 0x0000f14c;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (10 cases) at 0xf14c */
    void (*rax)() ();
label_36:
    errno_location ();
    *(rax) = 0xc;
    do {
label_4:
        rdi = *((rbp - 0x2b8));
        rax = rbp - 0x2a0;
        if (rdi != rax) {
            free (rdi);
        }
        rdi = *((rbp - 0x3a8));
        rax = rbp - 0x3a0;
        if (rdi != rax) {
            free (rdi);
        }
label_34:
        r10d = 0;
label_31:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_46;
        }
        rsp = rbp - 0x28;
        rax = r10;
        return rax;
label_35:
        errno_location ();
        *(rax) = 0x16;
    } while (1);
    *(r13) = 0x6c;
    r13++;
    *(r13) = 0x6c;
    r13++;
label_12:
    eax = *((r14 + 0x48));
    *((r13 + 1)) = 0;
    *(r13) = al;
    rax = *((r14 + 0x28));
    if (rax == -1) {
        goto label_47;
    }
    rax <<= 5;
    rax += r15;
    if (*(rax) != 5) {
        void (*0x2847)() ();
    }
    *((rbp - 0x3d8)) = 1;
    eax = *((rax + 0x10));
    *((rbp - 0x3b8)) = eax;
label_21:
    rax = *((r14 + 0x40));
    if (rax == -1) {
        goto label_48;
    }
    rax <<= 5;
    rcx = r15 + rax;
    if (*(rcx) != 5) {
        void (*0x2847)() ();
    }
    eax = *((rbp - 0x3d8));
    edx = *((rcx + 0x10));
    *((rbp + rax*4 - 0x3b8)) = edx;
    eax = rax + 1;
    *((rbp - 0x3d8)) = eax;
label_48:
    rax = r12;
    rax += 2;
    if (rax < 0) {
        goto label_49;
    }
    if (rbx >= rax) {
        goto label_50;
    }
    if (rbx != 0) {
        goto label_51;
    }
    if (rax > 0xc) {
        goto label_52;
    }
    ebx = 0xc;
label_26:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_53;
    }
    rdi = r10;
    *((rbp - 0x3d0)) = r10;
    rax = realloc (rdi, rbx);
    r10 = *((rbp - 0x3d0));
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
label_22:
    *((r15 + r12)) = 0;
    rax = errno_location ();
    *((rbp - 0x3f0)) = r13;
    *((rbp - 0x3d0)) = rax;
    eax = *(rax);
    *((rbp - 0x40c)) = eax;
label_1:
    rax = *((rbp - 0x3d0));
    r13 = rbx;
    esi = 0x7fffffff;
    *((rbp - 0x3bc)) = 0xffffffff;
    r13 -= r12;
    *(rax) = 0;
    eax = *((rbp - 0x3c8));
    if (r13 <= rsi) {
        rsi = r13;
    }
    if (eax > 0x11) {
        goto label_0;
    }
    rdi = 0x0000f174;
    rax = *((rdi + rax*4));
    rax += rdi;
    /* switch table (18 cases) at 0xf174 */
    void (*rax)() ();
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_54;
    }
    if (eax == 2) {
        goto label_55;
    }
    rax = rbp - 0x3bc;
label_5:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    rsi = *((rbp - 0x418));
label_6:
    edx = *((rbp - 0x3bc));
    if (edx < 0) {
        goto label_56;
    }
label_2:
    rcx = (int64_t) edx;
    if (rcx >= rsi) {
        goto label_57;
    }
    rcx += r15;
    if (*((rcx + r12)) != 0) {
        void (*0x2847)() ();
    }
label_57:
    if (edx < eax) {
label_3:
        *((rbp - 0x3bc)) = eax;
        edx = eax;
    }
    eax = rdx + 1;
    if (rax < rsi) {
        goto label_58;
    }
    if (r13 > 0x7ffffffe) {
        goto label_59;
    }
    if (rbx < 0) {
        goto label_60;
    }
    eax = rdx + 2;
    rcx = rbx + rbx;
    rax += r12;
    if (rax < 0) {
        goto label_23;
    }
    if (rax < rcx) {
        rax = rcx;
    }
    if (rbx >= rax) {
        goto label_1;
    }
    if (rcx >= rax) {
        rax = rcx;
    }
    rbx = rax;
    if (rax == -1) {
        goto label_23;
    }
    if (r15 == *((rbp - 0x3e8))) {
        goto label_61;
    }
    rax = realloc (r15, rax);
    if (rax == 0) {
        goto label_23;
    }
    r15 = rax;
    goto label_1;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_10;
    }
label_7:
    if (eax == 2) {
        goto label_62;
    }
label_9:
    rax = rbp - 0x3bc;
label_8:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    edx = *((rbp - 0x3bc));
    rsi = *((rbp - 0x418));
    if (edx >= 0) {
        goto label_2;
    }
label_56:
    rcx = *((rbp - 0x3f0));
    if (*((rcx + 1)) != 0) {
        goto label_63;
    }
    if (eax >= 0) {
        goto label_3;
    }
    rax = *((rbp - 0x3d0));
    eax = *(rax);
    if (eax == 0) {
        eax = *((r14 + 0x48));
        edx = 0x54;
        rbx = *((rbp - 0x3d0));
        eax &= 0xffffffef;
        eax = 0x16;
        if (al == 0x63) {
            eax = edx;
        }
        *(rbx) = eax;
    }
label_20:
    if (r15 != *((rbp - 0x3e8))) {
        free (r15);
    }
    rax = *((rbp - 0x408));
    if (rax == 0) {
        goto label_4;
    }
    free (rax);
    goto label_4;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    *(fp_stack--) = fp_stack[?];
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_64;
    }
    if (eax == 2) {
        goto label_65;
    }
    r9 = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_5;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    xmm0 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_66;
    }
    if (eax == 2) {
        goto label_67;
    }
    r8 = *((rbp - 0x3e0));
    edx = 1;
    eax = 1;
    r9 = rbp - 0x3bc;
    rcx = 0xffffffffffffffff;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    do {
label_10:
        rax = rbp - 0x3bc;
label_14:
        r9d = *((rbp - 0x3b8));
        goto label_8;
        rax = *((r14 + 0x50));
        rdi = r15 + r12;
        rax <<= 5;
        rax += *((rbp - 0x3a8));
        r9d = *((rax + 0x10));
        eax = *((rbp - 0x3d8));
    } while (eax == 1);
    if (eax != 2) {
        goto label_9;
    }
label_62:
    rax = rbp - 0x3bc;
label_13:
    eax = *((rbp - 0x3b4));
label_24:
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    eax = 0;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
label_37:
    rax += 0x1d;
    rcx = rsp;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_68;
    }
    do {
    } while (rsp != rcx);
label_68:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_69;
    }
label_29:
    *((rbp - 0x408)) = 0;
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3e0)) = rax;
    goto label_11;
    *(r13) = 0x4c;
    r13++;
    goto label_12;
label_60:
    if (rbx == -1) {
        goto label_1;
    }
    goto label_23;
label_61:
    rax = malloc (rax);
    if (rax == 0) {
        goto label_23;
    }
    if (r12 == 0) {
        goto label_70;
    }
    rax = memcpy (rax, r15, r12);
    r15 = rax;
    goto label_1;
label_55:
    rax = rbp - 0x3bc;
    goto label_13;
label_54:
    rax = rbp - 0x3bc;
    goto label_14;
label_63:
    *((rcx + 1)) = 0;
    goto label_1;
label_44:
    if (rax != -1) {
        void (*0x2847)() ();
    }
    r15 = r12 + 1;
    r9 = 0xffffffffffffffff;
    if (r12 < -1) {
        r9 = r15;
    }
    if (rbx < r9) {
        if (rbx == 0) {
            goto label_71;
        }
        if (rbx < 0) {
            goto label_25;
        }
        rbx += rbx;
label_27:
        if (rbx < r9) {
            rbx = r9;
        }
        if (rbx == -1) {
            goto label_25;
        }
        if (r10 == *((rbp - 0x3e8))) {
            goto label_72;
        }
        rdi = r10;
        *((rbp - 0x3d0)) = r9;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, rbx);
        r9 = *((rbp - 0x3d0));
        if (rax == 0) {
            goto label_30;
        }
        r10 = rax;
    }
label_28:
    *((r10 + r12)) = 0x25;
    r15 = r10;
label_19:
    r8 = *((r14 + 8));
    r13 = *((r14 + 0x58));
    r14 += 0x58;
    *((rbp - 0x3f8))++;
    if (r13 != r8) {
        goto label_15;
    }
label_38:
    r12 = r9;
    r10 = r15;
    goto label_16;
label_40:
    ebx = 0xc;
    goto label_17;
label_39:
    r10 = r15;
    goto label_18;
label_45:
    r13d = ecx;
    r13d -= 0x12;
    if (r13d > 4) {
        goto label_0;
    }
    rcx = 0x0000f1bc;
    rax = *((rax + 0x10));
    rdx = *((rcx + r13*4));
    rdx += rcx;
    /* switch table (5 cases) at 0xf1bc */
    void (*rdx)() ();
    *(rax) = r12;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12d;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12w;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12b;
    r9 = r12;
    r15 = r10;
    goto label_19;
label_49:
    if (rbx == -1) {
        goto label_50;
    }
label_25:
    *((rbp - 0x3c8)) = r10;
label_30:
    rax = errno_location ();
    r15 = *((rbp - 0x3c8));
    *((rbp - 0x3d0)) = rax;
label_23:
    rax = *((rbp - 0x3d0));
    *(rax) = 0xc;
    goto label_20;
label_47:
    *((rbp - 0x3d8)) = 0;
    goto label_21;
label_42:
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
    if (r9 == 0) {
        goto label_18;
    }
    rdx = r9;
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, *((rbp - 0x3e8)), rdx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    goto label_18;
label_50:
    r15 = r10;
    goto label_22;
label_41:
    rax = errno_location ();
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_65:
    rax = rbp - 0x3bc;
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    eax = *((rbp - 0x3b4));
    rcx = 0xffffffffffffffff;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
label_64:
    rax = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_24;
label_67:
    rax = rbp - 0x3bc;
    eax = *((rbp - 0x3b4));
    do {
        r9d = *((rbp - 0x3b8));
        edx = 1;
        eax = 1;
        r8 = *((rbp - 0x3e0));
        rcx = 0xffffffffffffffff;
        *((rbp - 0x418)) = rsi;
        snprintf_chk ();
        rsi = *((rbp - 0x418));
        goto label_6;
label_66:
        rax = rbp - 0x3bc;
    } while (1);
    if (r9 < 0) {
label_51:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= rax) {
        goto label_26;
    }
label_52:
    if (rax == -1) {
        goto label_25;
    }
    rbx = rax;
    goto label_26;
label_58:
    rax = (int64_t) edx;
    edx = *((rbp - 0x40c));
    r9 = rax + r12;
    rax = *((rbp - 0x3d0));
    *(rax) = edx;
    goto label_19;
label_59:
    rax = *((rbp - 0x3d0));
    *(rax) = 0x4b;
    goto label_20;
label_71:
    ebx = 0xc;
    goto label_27;
label_53:
    rax = malloc (rbx);
    r15 = rax;
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_22;
    }
    memcpy (rax, *((rbp - 0x3e8)), r12);
    goto label_22;
label_72:
    *((rbp - 0x3d0)) = r10;
    *((rbp - 0x3c8)) = r9;
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r10 = *((rbp - 0x3d0));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_74;
    }
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, r10, r12);
    r9 = *((rbp - 0x3c8));
    r10 = rax;
    goto label_28;
label_69:
    goto label_29;
label_43:
    r13 = r12;
    r13++;
    if (r13 < 0) {
        goto label_75;
    }
    if (rbx >= r13) {
        goto label_33;
    }
    if (rbx != 0) {
        goto label_76;
    }
    if (r13 > 0xc) {
        goto label_77;
    }
    ebx = 0xc;
label_32:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_78;
    }
    rdi = r10;
    *((rbp - 0x3c8)) = r10;
    rax = realloc (rdi, rbx);
    r10 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_33:
    *((r10 + r12)) = 0;
    if (rbx > r13) {
        if (r10 == *((rbp - 0x3e8))) {
            goto label_79;
        }
        rdi = r10;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, r13);
        r10 = *((rbp - 0x3c8));
        if (rax == 0) {
            r10 = rax;
            goto label_79;
        }
    }
label_79:
    rdi = *((rbp - 0x408));
    if (rdi != 0) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x2b8));
    rax = rbp - 0x2a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x3a8));
    rax = rbp - 0x3a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rax = *((rbp - section..dynsym));
    *(rax) = r12;
    goto label_31;
    if (rdi < rax) {
label_76:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= r13) {
        goto label_32;
    }
label_77:
    if (r13 == -1) {
        goto label_25;
    }
    rbx = r13;
    goto label_32;
label_78:
    *((rbp - 0x3c8)) = r10;
    rax = malloc (rbx);
    r10 = *((rbp - 0x3c8));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_80;
    }
    rax = memcpy (rax, r10, r12);
    r10 = rax;
    goto label_33;
label_46:
    stack_chk_fail ();
label_75:
    if (rbx == -1) {
        goto label_33;
    }
    goto label_25;
label_73:
    rax = errno_location ();
    r15 = *((rbp - 0x3e8));
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_70:
    r15 = rax;
    goto label_1;
label_74:
    r10 = rax;
    goto label_28;
label_80:
    r10 = rax;
    goto label_33;
}

/* /tmp/tmpwp339vba @ 0xb360 */
 
int64_t dbg_printf_fetchargs (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int printf_fetchargs(__va_list_tag * args,arguments * a); */
    r8 = *(rsi);
    rax = *((rsi + 8));
    rcx = rdi;
    if (r8 == 0) {
        goto label_6;
    }
    esi = 0;
    rdi = 0x0000f1e0;
    r10 = "(NULL)";
    r9 = "(NULL)";
    do {
        if (*(rax) > 0x16) {
            goto label_7;
        }
        edx = *(rax);
        rdx = *((rdi + rdx*4));
        rdx += rdi;
        /* switch table (23 cases) at 0xf1e0 */
        rax = void (*rdx)() ();
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_8;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_1:
        rdx = *(r11);
        *((rax + 0x10)) = rdx;
label_0:
        rsi++;
        rax += 0x20;
    } while (rsi != r8);
label_6:
    eax = 0;
    return rax;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_9;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        edx = *(r11);
        *((rax + 0x10)) = edx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_10;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_2:
        edx = *(r11);
        *((rax + 0x10)) = dx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_11;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_3:
        edx = *(r11);
        *((rax + 0x10)) = dl;
        goto label_0;
label_8:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_1;
label_9:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
    edx = *((rcx + 4));
    if (edx > 0xaf) {
        goto label_12;
    }
    r11d = edx;
    edx += 0x10;
    r11 += *((rcx + 0x10));
    *((rcx + 4)) = edx;
label_4:
    xmm0 = *(r11);
    *((rax + 0x10)) = xmm0;
    goto label_0;
    rdx = *((rcx + 8));
    rdx += 0xf;
    rdx &= 0xfffffffffffffff0;
    r11 = rdx + 0x10;
    *((rcx + 8)) = r11;
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_13;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
label_5:
    rdx = *(r11);
    if (rdx == 0) {
        rdx = r10;
    }
    *((rax + 0x10)) = rdx;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_14;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        rdx = *(r11);
        if (rdx == 0) {
            rdx = r9;
        }
        *((rax + 0x10)) = rdx;
        goto label_0;
label_10:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_2;
label_11:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_3;
label_14:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
label_12:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_4;
label_13:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_5;
label_7:
    eax |= 0xffffffff;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x8360 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x2860 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    int32_t c;
    size_t n_bytes_read;
    char *[2] block;
    uint32_t var_8h;
    void * ptr;
    uint32_t var_18h;
    uint32_t var_1fh;
    int64_t var_20h;
    uint32_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_48h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = "A:aBbcDdeFfHhIij:LlN:OoS:st:vw::Xx";
    r14 = obj_long_options;
    r13 = 0x0000d14f;
    r12d = edi;
    rbx = 0x0000d2b0;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000ec81);
    bindtextdomain (r13, "/usr/local/share/locale");
    r13 = rsp + 0x20;
    textdomain (r13, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    xmm0 = 0;
    *((rsp + 8)) = 1;
    rax = 0x200000001;
    *(obj.integral_type_size) = xmm0;
    *(0x00013184) = rax;
    rax = dbg_format_address_std;
    *(0x00013190) = xmm0;
    *(0x00013130) = xmm0;
    *(0x00013140) = xmm0;
    *(0x00013190) = 3;
    *(0x000131a0) = 5;
    *(0x00013130) = 6;
    *(0x00013160) = 8;
    *(0x00013140) = 7;
    *(obj.n_specs) = 0;
    *(obj.n_specs_allocated) = 0;
    *(obj.spec) = 0;
    *(obj.format_address) = rax;
    *(obj.address_base) = 8;
    *(obj.address_pad_len) = 7;
    *(obj.flag_dump_strings) = 0;
    *((rsp + 0x1f)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x10)) = 0;
    *(obj.fp_type_size) = xmm0;
    *(0x00013150) = xmm0;
    do {
label_0:
        r8 = r13;
        rcx = r14;
        rdx = r15;
        rsi = rbp;
        edi = r12d;
        *((rsp + 0x20)) = 0xffffffff;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_26;
        }
        if (eax > 0x81) {
            goto label_27;
        }
        if (eax <= 0x40) {
            goto label_28;
        }
        eax -= 0x41;
        if (eax > 0x40) {
            goto label_27;
        }
        rax = *((rbx + rax*4));
        rax += rbx;
        /* switch table (65 cases) at 0xd2b0 */
        void (*rax)() ();
        rdi = 0x0000d1b8;
        al = decode_format_string_part_0 ();
    } while (1);
    rdi = 0x0000d1b2;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1ac;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1be;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1c4;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1b5;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1c1;
    al = decode_format_string_part_0 ();
    goto label_0;
    eax = xstrtoumax (*(obj.optarg), 0, 0, obj.n_bytes_to_skip, "bEGKkMmPTYZ0");
    if (eax != 0) {
        goto label_29;
    }
label_1:
    *((rsp + 0x18)) = 1;
    goto label_0;
    rdi = 0x0000d1a6;
    al = decode_format_string_part_0 ();
    goto label_0;
    rax = optarg;
    ecx = *(rax);
    if (cl == 0x6f) {
        goto label_30;
    }
    if (cl > 0x6f) {
        goto label_31;
    }
    if (cl == 0x64) {
        goto label_32;
    }
    if (cl != 0x6e) {
        goto label_33;
    }
    rax = dbg_format_address_none;
    *((rsp + 0x18)) = 1;
    *(obj.format_address) = rax;
    *(obj.address_pad_len) = 0;
    goto label_0;
    _xargmatch_internal ("--endian", *(obj.optarg), obj.endian_args, obj.endian_types, 4, *(obj.argmatch_die));
    rcx = obj_endian_types;
    eax = *((rcx + rax*4));
    if (eax == 0) {
        goto label_34;
    }
    eax--;
    if (eax != 0) {
        goto label_0;
    }
    *(obj.input_swap) = 1;
    goto label_0;
    *(obj.traditional) = 1;
    goto label_0;
    rdi = optarg;
    if (rdi == 0) {
        goto label_35;
    }
    eax = xstrtoumax (rdi, 0, 0xa, rsp + 0x28, 0x0000ec81);
    if (eax != 0) {
        goto label_36;
    }
    rax = *((rsp + 0x28));
    *((rsp + 0x1f)) = 1;
    *((rsp + 0x18)) = 1;
    *((rsp + 0x10)) = rax;
    goto label_0;
    *(obj.abbreviate_duplicate_blocks) = 0;
    *((rsp + 0x18)) = 1;
    goto label_0;
    rdi = optarg;
    if (rdi == 0) {
        goto label_37;
    }
    al = decode_format_string_part_0 ();
    *((rsp + 0x18)) = 1;
    goto label_0;
    rdi = 0x0000d1af;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1a9;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d021;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d1a3;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = 0x0000d02c;
    al = decode_format_string_part_0 ();
    goto label_0;
    rdi = optarg;
    if (rdi == 0) {
        goto label_38;
    }
    eax = xstrtoumax (rdi, 0, 0, rsp + 0x28, "bEGKkMmPTYZ0");
    if (eax != 0) {
        goto label_39;
    }
    rax = *((rsp + 0x28));
label_2:
    *(obj.string_min) = rax;
    *(obj.flag_dump_strings) = 1;
    *((rsp + 0x18)) = 1;
    goto label_0;
    rdi = 0x0000d1bb;
    al = decode_format_string_part_0 ();
    goto label_0;
    *(obj.limit_bytes_to_format) = 1;
    eax = xstrtoumax (*(obj.optarg), 0, 0, obj.max_bytes_to_format, "bEGKkMmPTYZ0");
    if (eax == 0) {
        goto label_1;
    }
    r8 = optarg;
    esi = *((rsp + 0x20));
    rcx = r14;
    edi = eax;
    edx = 0x4e;
    xstrtol_fatal ();
label_31:
    if (cl != 0x78) {
        goto label_33;
    }
    rax = dbg_format_address_std;
    *((rsp + 0x18)) = 1;
    *(obj.format_address) = rax;
    *(obj.address_base) = 0x10;
    *(obj.address_pad_len) = 6;
    goto label_0;
label_28:
    if (eax == 0xffffff7d) {
        eax = 0;
        version_etc (*(obj.stdout), 0x0000d0d2, "GNU coreutils", *(obj.Version), "Jim Meyering", 0);
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_27;
    }
    usage (0);
label_35:
    *((rsp + 0x1f)) = 1;
    *((rsp + 0x18)) = 1;
    *((rsp + 0x10)) = 0x20;
    goto label_0;
label_38:
    eax = 3;
    goto label_2;
label_34:
    *(obj.input_swap) = 0;
    goto label_0;
label_26:
    if (*((rsp + 8)) == 0) {
        goto label_40;
    }
    if (*(obj.flag_dump_strings) != 0) {
        if (*(obj.n_specs) != 0) {
            goto label_41;
        }
    }
    rax = *(obj.optind);
    edx = *(obj.traditional);
    r12d -= eax;
    if (*((rsp + 0x18)) != 0) {
        goto label_42;
    }
    if (r12d == 2) {
        goto label_43;
    }
    if (r12d == 3) {
        goto label_44;
    }
    if (r12d == 1) {
        goto label_45;
    }
    if (dl == 0) {
        goto label_7;
    }
    do {
        ebx = 0;
        if (r12d <= 1) {
            goto label_9;
        }
label_8:
        rax = quote (*((rbp + rax*8 + 8)), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "extra operand %s");
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        edx = 5;
        rax = dcgettext (0, "compatibility mode supports at most one file");
        rcx = rax;
        eax = 0;
        error (0, 0, 0x0000e7bc);
label_27:
        usage (1);
label_32:
        rax = dbg_format_address_std;
        *((rsp + 0x18)) = 1;
        *(obj.format_address) = rax;
        *(obj.address_base) = 0xa;
        *(obj.address_pad_len) = 7;
        goto label_0;
label_30:
        rax = dbg_format_address_std;
        *((rsp + 0x18)) = 1;
        *(obj.format_address) = rax;
        *(obj.address_base) = 8;
        *(obj.address_pad_len) = 7;
        goto label_0;
label_42:
        if (dl == 0) {
            goto label_7;
        }
        if (r12d == 2) {
            goto label_46;
        }
        if (r12d == 3) {
            goto label_47;
        }
    } while (r12d != 1);
label_16:
    al = parse_old_offset (*((rbp + rax*8)), r13, rdx, rcx);
    if (al == 0) {
        goto label_48;
    }
    rax = *((rsp + 0x20));
    rbp += 8;
    ebx = 0;
    r12d = 0;
    *(obj.n_bytes_to_skip) = rax;
    goto label_9;
label_44:
    if (dl != 0) {
        goto label_47;
    }
label_7:
    ebx = 0;
label_9:
    if (*(obj.flag_pseudo_start) != 0) {
        rax = dbg_format_address_none;
        if (*(obj.format_address) == rax) {
            goto label_49;
        }
        rax = dbg_format_address_label;
        *(obj.format_address) = rax;
    }
label_15:
    if (*(obj.limit_bytes_to_format) != 0) {
        rax = max_bytes_to_format;
        rax += *(obj.n_bytes_to_skip);
        *(obj.end_offset) = rax;
        if (rax < 0) {
            goto label_50;
        }
    }
    if (*(obj.n_specs) == 0) {
        goto label_51;
    }
label_4:
    rax = obj_default_file_list;
    if (r12d > 0) {
        rax = *(obj.optind);
        rax = rbp + rax*8;
    }
    *(obj.file_list) = rax;
    eax = open_next_file ();
    if (*(obj.in_stream) == 0) {
        goto label_52;
    }
    eax = skip (*(obj.n_bytes_to_skip));
    ebp &= eax;
    eax = (int32_t) bpl;
    *((rsp + 0x18)) = eax;
    if (*(obj.in_stream) == 0) {
        goto label_52;
    }
    eax = 0;
    if (*(obj.flag_pseudo_start) != 0) {
        rax = rbx;
        rax -= *(obj.n_bytes_to_skip);
    }
    *(obj.pseudo_offset) = rax;
    eax = get_lcm ();
    ebx = eax;
    if (*((rsp + 0x1f)) == 0) {
        goto label_53;
    }
    rbp = (int64_t) eax;
    rax = *((rsp + 0x10));
    if (rax != 0) {
        edx = 0;
        rax = rdx:rax / rbp;
        rdx = rdx:rax % rbp;
        if (rdx == 0) {
            goto label_6;
        }
    }
    edx = 5;
    rax = dcgettext (0, "warning: invalid width %lu; using %d instead");
    rcx = *((rsp + 0x10));
    r8d = ebx;
    eax = 0;
    error (0, 0, rax);
    *((rsp + 0x10)) = rbp;
label_6:
    rax = *((rsp + 0x10));
    *(obj.bytes_per_block) = rax;
    rax = n_specs;
    if (rax == 0) {
        goto label_54;
    }
    rdx = spec;
    rax *= 0x28;
    r11 = *((rsp + 0x10));
    edi = 0;
    r9 = obj_width_bytes;
    rcx = rdx + 4;
    r8 = rdx + rax + 4;
    rsi = rcx;
    do {
        eax = *(rsi);
        edx = 0;
        ebx = *((rsi + 0x18));
        r10 = *((r9 + rax*4));
        rax = r11;
        rax = rdx:rax / r10;
        rdx = rdx:rax % r10;
        edx = rbx + 1;
        edx *= eax;
        rax = (int64_t) edx;
        if (rdi < rax) {
            rdi = rax;
        }
        rsi += 0x28;
    } while (r8 != rsi);
    r10 = *((rsp + 0x10));
    do {
        eax = *(rcx);
        edx = 0;
        rcx += 0x28;
        rsi = *((r9 + rax*4));
        rax = r10;
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
        edx = edi;
        eax *= *((rcx - 0x10));
        edx -= eax;
        *((rcx - 0xc)) = edx;
    } while (r8 != rcx);
label_54:
    ebx = *(obj.flag_dump_strings);
    if (bl == 0) {
        goto label_55;
    }
    rdi = string_min;
    eax = 0x64;
    if (rdi < rax) {
        rdi = rax;
    }
    *((rsp + 0x28)) = rdi;
    rax = xmalloc (rdi);
    rbp = n_bytes_to_skip;
    r12 = rax;
label_13:
    rsi = string_min;
    do {
label_19:
        r14 = rsi;
        if (*(obj.limit_bytes_to_format) != 0) {
            rax = end_offset;
            if (rax < rsi) {
                goto label_56;
            }
            rax -= rsi;
            if (rax <= rbp) {
                goto label_56;
            }
        }
        if (rsi == 0) {
            goto label_57;
        }
        rax = rbp + 1;
        r14d = 0;
        *((rsp + 8)) = rax;
        goto label_58;
label_3:
        rax = ctype_b_loc ();
        rsi = string_min;
        r8 = rax;
        rax = (int64_t) r15d;
        rcx = *(r8);
    } while ((*((rcx + rax*2 + 1)) & 0x40) == 0);
    *((r12 + r14)) = r15b;
    r14++;
    if (r14 >= rsi) {
        goto label_57;
    }
label_58:
    eax = read_char (r13);
    r15d = *((rsp + 0x20));
    ebx &= eax;
    rax = *((rsp + 8));
    rbp = rax + r14;
    if (r15d >= 0) {
        goto label_3;
    }
label_12:
    free (r12);
label_25:
    ebp = (int32_t) bl;
label_10:
    eax = *((rsp + 0x18));
    eax &= ebp;
    ebp &= 1;
label_52:
    if (*(obj.have_read_stdin) != 0) {
        goto label_59;
    }
label_5:
    ebp ^= 1;
    eax = (int32_t) bpl;
    do {
        rdx = *((rsp + 0x48));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_60;
        }
        return rax;
label_40:
        eax = 1;
    } while (1);
label_51:
    rdi = 0x0000d1e5;
    decode_format_string_part_0 ();
    goto label_4;
label_59:
    eax = rpl_fclose (*(obj.stdin));
    eax++;
    if (eax != 0) {
        goto label_5;
    }
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    eax = error (1, *(rax), r12);
label_53:
    if (eax <= 0xf) {
        eax = 0x10;
        edx:eax = (int64_t) eax;
        eax = edx:eax / ebx;
        edx = edx:eax % ebx;
        eax *= ebx;
        rax = (int64_t) eax;
        *((rsp + 0x10)) = rax;
        goto label_6;
    }
    rax = (int64_t) eax;
    *((rsp + 0x10)) = rax;
    goto label_6;
label_47:
    al = parse_old_offset (*((rbp + rax*8 + 8)), r13, rdx, rcx);
    if (al != 0) {
        goto label_61;
    }
label_24:
    r12d = 3;
label_20:
    if (*(obj.traditional) == 0) {
        goto label_7;
    }
    rax = *(obj.optind);
    goto label_8;
label_46:
label_17:
    r12 = rsp + 0x28;
    al = parse_old_offset (*((rbp + rax*8 + 8)), r12, rdx, rcx);
    if (al == 0) {
        goto label_62;
    }
    rdx = *(obj.optind);
    rdi = *((rbp + rdx*8));
    rax = rdx;
    if (*(obj.traditional) != 0) {
        goto label_63;
    }
label_23:
    rdx = *((rsp + 0x28));
    ebx = 0;
    r12d = 1;
    *(obj.n_bytes_to_skip) = rdx;
    rdx = *((rbp + rax*8));
    *((rbp + rax*8 + 8)) = rdx;
    rbp += 8;
    goto label_9;
label_55:
    r12 = rsp + 0x28;
    rax = xnmalloc (2, *((rsp + 0x10)));
    rdi = bytes_per_block;
    ebp = *(obj.limit_bytes_to_format);
    *((rsp + 0x10)) = rax;
    r14 = n_bytes_to_skip;
    *((rsp + 0x30)) = rax;
    rax += rdi;
    *((rsp + 0x38)) = rax;
    if (bpl != 0) {
        goto label_64;
    }
    ebp = *((rsp + 8));
    r13d = 0;
    while (r15 >= *(obj.bytes_per_block)) {
        rcx = *((rsp + 8));
        if (bpl != 0) {
            goto label_65;
        }
        ebx ^= 1;
        r14 += r15;
        eax = (int32_t) bl;
        r13d = (int32_t) bl;
        write_block (r14, r15, *((rsp + rax*8 + 0x30)), rcx);
        rdi = bytes_per_block;
        rax = (int64_t) r13d;
        rdx = r12;
        rcx = *((rsp + rax*8 + 0x30));
        rsi = *((rsp + rax*8 + 0x30));
        *((rsp + 8)) = rcx;
        eax = read_block ();
        r15 = *((rsp + 0x28));
        ebp &= eax;
    }
label_21:
    if (r15 != 0) {
        eax = get_lcm ();
        edx = 0;
        r12 = *((rsp + r13*8 + 0x30));
        rcx = (int64_t) eax;
        rax = rcx + r15 - 1;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        rax *= rcx;
        rax -= r15;
        memset (r12 + r15, 0, rax);
        eax = ebx;
        eax ^= 1;
        r14 += r15;
        eax = (int32_t) al;
        write_block (r14, r15, *((rsp + rax*8 + 0x30)), r12);
    }
label_11:
    esi = 0xa;
    rdi = r14;
    uint64_t (*format_address)() ();
    if (*(obj.limit_bytes_to_format) != 0) {
        if (r14 >= *(obj.end_offset)) {
            goto label_66;
        }
    }
label_22:
    ebp = (int32_t) bpl;
    free (*((rsp + 0x10)));
    goto label_10;
    do {
        eax = (int32_t) bl;
        rdi -= r14;
        rdx = r12;
        *((rsp + 8)) = eax;
        eax = (int32_t) bl;
        r13 = *((rsp + rax*8 + 0x30));
        rax = bytes_per_block;
        rsi = r13;
        if (rdi > rax) {
            rdi = rax;
        }
        eax = read_block ();
        r15 = *((rsp + 0x28));
        ebp &= eax;
        if (r15 < *(obj.bytes_per_block)) {
            goto label_67;
        }
        if (r15 != *(obj.bytes_per_block)) {
            goto label_68;
        }
        ebx ^= 1;
        eax = (int32_t) bl;
        r14 += r15;
        write_block (r14, r15, *((rsp + rax*8 + 0x30)), r13);
label_64:
        rdi = end_offset;
    } while (rdi > r14);
    goto label_11;
label_57:
    rax = rsp + 0x28;
    *((rsp + 8)) = rax;
    goto label_69;
label_14:
    rbp++;
    eax = read_char (r13);
    r15d = *((rsp + 0x20));
    ebx &= eax;
    if (r15d < 0) {
        goto label_12;
    }
    if (r15d == 0) {
        goto label_70;
    }
    rax = ctype_b_loc ();
    r8 = rax;
    rax = (int64_t) r15d;
    rcx = *(r8);
    if ((*((rcx + rax*2 + 1)) & 0x40) == 0) {
        goto label_13;
    }
    *((r12 + r14)) = r15b;
    r14++;
label_69:
    if (*(obj.limit_bytes_to_format) != 0) {
        if (rbp >= *(obj.end_offset)) {
            goto label_70;
        }
    }
    if (*((rsp + 0x28)) != r14) {
        goto label_14;
    }
    rax = x2realloc (r12, *((rsp + 8)));
    r12 = rax;
    goto label_14;
label_49:
    rax = sym_format_address_paren;
    *(obj.address_base) = 8;
    *(obj.address_pad_len) = 7;
    *(obj.format_address) = rax;
    goto label_15;
label_45:
    rdi = *((rbp + rax*8));
    if (dl != 0) {
        goto label_16;
    }
    if (*(rdi) == 0x2b) {
        goto label_16;
    }
label_48:
    ebx = 0;
    r12d = 1;
    goto label_9;
label_43:
    rdi = *((rbp + rax*8 + 8));
    if (dl != 0) {
        goto label_17;
    }
    eax = *(rdi);
    if (al == 0x2b) {
        goto label_17;
    }
    eax -= 0x30;
    if (eax > 9) {
        goto label_7;
    }
    goto label_17;
label_70:
    rdi = rbp - 1;
    *((r12 + r14)) = 0;
    esi = 0x20;
    r15 = r12;
    rdi -= r14;
    r14 = 0x0000d037;
    uint64_t (*format_address)() ();
    do {
        edx = *(r15);
        rcx = stdout;
        eax = (int32_t) dl;
        *((rsp + 0x20)) = eax;
        if (eax == 0) {
            goto label_71;
        }
        eax = rdx - 7;
        if (al > 6) {
            goto label_72;
        }
        rdx = 0x0000d3b4;
        eax = (int32_t) al;
        esi = 1;
        rax = *((rdx + rax*4));
        rax += rdx;
        edx = 2;
        /* switch table (7 cases) at 0xd3b4 */
        void (*rax)() ();
        rdi = 0x0000d031;
        fwrite_unlocked ();
label_18:
        r15++;
    } while (1);
    rdi = 0x0000d03d;
    fwrite_unlocked ();
    goto label_18;
    rdi = 0x0000d034;
    fwrite_unlocked ();
    goto label_18;
    rdi = 0x0000d03a;
    fwrite_unlocked ();
    goto label_18;
    rdi = 0x0000d02e;
    fwrite_unlocked ();
    goto label_18;
    rdi = 0x0000d02b;
    fwrite_unlocked ();
    goto label_18;
    rdi = r14;
    fwrite_unlocked ();
    goto label_18;
label_72:
    rax = *((rcx + 0x28));
    if (rax >= *((rcx + 0x30))) {
        goto label_73;
    }
    rsi = rax + 1;
    *((rcx + 0x28)) = rsi;
    *(rax) = dl;
    goto label_18;
label_71:
    rax = *((rcx + 0x28));
    if (rax >= *((rcx + 0x30))) {
        goto label_74;
    }
    rdx = rax + 1;
    rsi = string_min;
    *((rcx + 0x28)) = rdx;
    *(rax) = 0xa;
    goto label_19;
label_62:
    r12d = 2;
    goto label_20;
label_67:
    r13 = *((rsp + 8));
    goto label_21;
label_66:
    eax = check_and_close (0);
    ebp &= eax;
    goto label_22;
label_63:
    al = parse_old_offset (rdi, r13, rdx, rcx);
    if (al != 0) {
        goto label_75;
    }
    rax = *(obj.optind);
    goto label_23;
label_61:
    rax = *(obj.optind);
    r12 = rsp + 0x28;
    al = parse_old_offset (*((rbp + rax*8 + 0x10)), r12, rdx, rcx);
    if (al == 0) {
        goto label_24;
    }
    rax = *((rsp + 0x20));
    *(obj.flag_pseudo_start) = 1;
    r12d = 1;
    rbx = *((rsp + 0x28));
    *(obj.n_bytes_to_skip) = rax;
    rax = *(obj.optind);
    rdx = *((rbp + rax*8));
    *((rbp + rax*8 + 0x10)) = rdx;
    rbp += 0x10;
    goto label_9;
label_56:
    free (r12);
    eax = check_and_close (0);
    ebx &= eax;
    goto label_25;
label_75:
    rax = *((rsp + 0x20));
    rbx = *((rsp + 0x28));
    rbp += 0x10;
    r12d = 0;
    *(obj.flag_pseudo_start) = 1;
    *(obj.n_bytes_to_skip) = rax;
    goto label_9;
label_73:
    esi = (int32_t) dl;
    rdi = rcx;
    overflow ();
    goto label_18;
label_74:
    esi = 0xa;
    rdi = rcx;
    eax = overflow ();
    rsi = string_min;
    goto label_19;
label_29:
    r8 = optarg;
    esi = *((rsp + 0x20));
    rcx = r14;
    edi = eax;
    edx = 0x6a;
    xstrtol_fatal ();
label_65:
    assert_fail ("n_bytes_read == bytes_per_block", "src/od.c", 0x58a, "dump");
label_68:
    assert_fail ("n_bytes_read == bytes_per_block", "src/od.c", 0x57c, "dump");
label_50:
    edx = 5;
    rax = dcgettext (0, "skip-bytes + read-bytes is too large");
    eax = 0;
    error (1, 0, rax);
label_41:
    edx = 5;
    rax = dcgettext (0, "no type may be specified when dumping strings");
    eax = 0;
    error (1, 0, rax);
label_60:
    stack_chk_fail ();
label_37:
    assert_fail ("s != NULL", "src/od.c", 0x3d9, "decode_format_string");
label_33:
    edx = 5;
    *((rsp + 8)) = cl;
    rax = dcgettext (0, "invalid output address radix '%c'; it must be one character from [doxn]");
    ecx = *((rsp + 8));
    eax = 0;
    eax = error (1, 0, rax);
label_39:
    r8 = optarg;
    esi = *((rsp + 0x20));
    rcx = r14;
    edi = eax;
    edx = 0x53;
    eax = xstrtol_fatal ();
label_36:
    r8 = optarg;
    esi = *((rsp + 0x20));
    rcx = r14;
    edi = eax;
    edx = 0x77;
    return xstrtol_fatal ();
}

/* /tmp/tmpwp339vba @ 0x5880 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n  or:  %s [-abcdfilosx]... [FILE] [[+]OFFSET[.][b]]\n  or:  %s --traditional [OPTION]... [FILE] [[+]OFFSET[.][b] [+][LABEL][.][b]]\n");
    r8 = r12;
    rcx = r12;
    rdx = r12;
    rsi = rax;
    edi = 1;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWrite an unambiguous representation, octal bytes by default,\nof FILE to standard output.  With more than one FILE argument,\nconcatenate them in the listed order to form the input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nIf first and second call formats both apply, the second format is assumed\nif the last operand begins with + or (if there are 2 operands) a digit.\nAn OFFSET operand means -j OFFSET.  LABEL is the pseudo-address\nat first byte printed, incremented when dump is progressing.\nFor OFFSET and LABEL, a 0x or 0X prefix indicates hexadecimal;\nsuffixes may be . for octal and b for multiply by 512.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -A, --address-radix=RADIX   output format for file offsets; RADIX is one\n                                of [doxn], for Decimal, Octal, Hex or None\n      --endian={big|little}   swap input bytes according the specified order\n  -j, --skip-bytes=BYTES      skip BYTES input bytes first\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -N, --read-bytes=BYTES      limit dump to BYTES input bytes\n  -S BYTES, --strings[=BYTES]  output strings of at least BYTES graphic chars;\n                                3 is implied when BYTES is not specified\n  -t, --format=TYPE           select output format or formats\n  -v, --output-duplicates     do not use * to mark line suppression\n  -w[BYTES], --width[=BYTES]  output BYTES bytes per output line;\n                                32 is implied when BYTES is not specified\n      --traditional           accept arguments in third form above\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\n\nTraditional format specifications may be intermixed; they accumulate:\n  -a   same as -t a,  select named characters, ignoring high-order bit\n  -b   same as -t o1, select octal bytes\n  -c   same as -t c,  select printable characters or backslash escapes\n  -d   same as -t u2, select unsigned decimal 2-byte units\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -f   same as -t fF, select floats\n  -i   same as -t dI, select decimal ints\n  -l   same as -t dL, select decimal longs\n  -o   same as -t o2, select octal 2-byte units\n  -s   same as -t d2, select decimal 2-byte units\n  -x   same as -t x2, select hexadecimal 2-byte units\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\n\nTYPE is made up of one or more of these specifications:\n  a          named character, ignoring high-order bit\n  c          printable character or backslash escape\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  d[SIZE]    signed decimal, SIZE bytes per integer\n  f[SIZE]    floating point, SIZE bytes per float\n  o[SIZE]    octal, SIZE bytes per integer\n  u[SIZE]    unsigned decimal, SIZE bytes per integer\n  x[SIZE]    hexadecimal, SIZE bytes per integer\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nSIZE is a number.  For TYPE in [doux], SIZE may also be C for\nsizeof(char), S for sizeof(short), I for sizeof(int) or L for\nsizeof(long).  If TYPE is f, SIZE may also be F for sizeof(float), D\nfor sizeof(double) or L for sizeof(long double).\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nAdding a z suffix to any type displays printable characters at the end of\neach output line.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\n\nBYTES is hex with 0x or 0X prefix, and may have a multiplier suffix:\n  b    512\n  KB   1000\n  K    1024\n  MB   1000*1000\n  M    1024*1024\nand so on for G, T, P, E, Z, Y.\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000d0d5;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x6f;
    *((rsp + 8)) = rax;
    rax = 0x0000d14f;
    edi = 0x64;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0000d0d2;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000d159, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x0000d0f1;
    printf_chk ();
    rax = 0x0000ec81;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000d159, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0000d0d2;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x0000d0f1;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0000d0d2;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmpwp339vba @ 0x8030 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpwp339vba @ 0x6710 */
 
int64_t dbg_ldtoastr (int64_t arg_68h_2, int64_t arg_68h, int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4) {
    long double x;
    char[11] format;
    int64_t var_dh;
    int64_t var_eh;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int ldtoastr(char * buf,size_t bufsize,int flags,int width,long double x); */
    r15d = 1;
    r13d = ecx;
    ecx = edx;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    *(fp_stack--) = fp_stack[?];
    eax = 0x2d25;
    r14 = rsp + 0xd;
    *((rsp + 0xd)) = ax;
    eax = edx;
    fp_stack[0] = -fp_stack[0];
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(1)");
    __asm ("fcmovnbe st(0), st(1)");
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    eax &= 1;
    ecx >>= 1;
    ecx &= 1;
    rax = rsp + rax + 0xe;
    *(rax) = 0x2b;
    *(fp_stack--) = fp_stack[?];
    rax += rcx;
    ecx = edx;
    ecx >>= 2;
    *(rax) = 0x20;
    ecx &= 1;
    rax += rcx;
    ecx = edx;
    edx &= 0x10;
    ecx >>= 3;
    *(rax) = 0x30;
    ecx &= 1;
    rax += rcx;
    edx -= edx;
    *(rax) = 0x4c2a2e2a;
    edx &= 0x20;
    *((rax + 5)) = 0;
    edx += 0x47;
    __asm ("fcompi st(1)");
    fp_stack++;
    *((rax + 4)) = dl;
    eax = 0x12;
    if (edx <= 0) {
        r15d = eax;
    }
    while (rax >= rbx) {
label_0:
        r15d++;
        r9d = r13d;
        r8 = r14;
        edx = 1;
        rcx = 0xffffffffffffffff;
        rsi = rbx;
        eax = 0;
        rdi = rbp;
        eax = snprintf_chk ();
        r12d = eax;
        if (eax < 0) {
            goto label_1;
        }
        if (r15d > 0x14) {
            goto label_1;
        }
        rax = (int64_t) eax;
    }
    strtold (rbp, 0);
    *(fp_stack--) = fp_stack[?];
    fp_tmp_0 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_0;
    __asm ("fucompi st(1)");
    fp_stack++;
    if (rax == rbx) {
        goto label_0;
    }
    if (rax != rbx) {
        goto label_0;
    }
label_1:
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax == 0) {
        eax = r12d;
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8720 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8550 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x2824)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8430 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x281a)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x8bc0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x9c10 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = section..dynsym;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000f060;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xf060 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = section..dynsym;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmpwp339vba @ 0x80d0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x280f)() ();
    }
    if (rdx == 0) {
        void (*0x280f)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x85e0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013350]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00013360]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x96b0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x9730 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x8330 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpwp339vba @ 0x93d0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x6520 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2730)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpwp339vba @ 0x8070 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x95b0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpwp339vba @ 0x91f0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpwp339vba @ 0x2650 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpwp339vba @ 0x99c0 */
 
int64_t dbg_xfprintf (int64_t arg_f0h, int64_t arg1, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* int xfprintf(FILE * restrict stream,char const * restrict format,va_args ...); */
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = rsp + 0xf0;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *(rsp) = 0x10;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    eax = rpl_vfprintf (rbp, rsi, rsp);
    r12d = eax;
    while (eax != 0) {
label_0:
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        eax = ferror (rbp);
    }
    edx = 5;
    rax = dcgettext (0, "cannot perform formatted output");
    r13 = rax;
    rax = errno_location ();
    eax = 0;
    error (*(obj.exit_failure), *(rax), r13);
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x6200 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpwp339vba @ 0xa150 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpwp339vba @ 0x2480 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpwp339vba @ 0x8be0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000ec28;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000ec3b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000ef28;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xef28 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x27a0)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x27a0)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x27a0)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpwp339vba @ 0xb590 */
 
int64_t dbg_printf_parse (int64_t arg1, int64_t arg2, int64_t arg3, size_t sum) {
    int64_t var_1h;
    int64_t var_4ch;
    int64_t var_30h;
    int64_t var_25h;
    int64_t var_bp_20h;
    int64_t var_8h;
    int64_t var_10h;
    void * s2;
    int64_t var_20h;
    void ** var_28h;
    void ** var_sp_30h;
    void ** var_38h;
    void ** var_40h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = sum;
    /* int printf_parse(char const * format,char_directives * d,arguments * a); */
    r10 = rsi + 0x20;
    rax = rdi;
    rdi = rdx + 0x10;
    r15 = rdx;
    r14 = rsi;
    rcx = r10;
    r9d = 7;
    r13d = 0;
    r11d = 7;
    *(rsi) = 0;
    *((rsi + 8)) = r10;
    *((rsp + 0x18)) = rdi;
    *(rdx) = 0;
    *((rdx + 8)) = rdi;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x20)) = 0;
    while (dl != 0) {
        rbx = rax + 1;
        if (dl == 0x25) {
            goto label_34;
        }
label_4:
        rax = rbx;
        edx = *(rax);
    }
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    *((rcx + rdx*8)) = rax;
    rax = *((rsp + 8));
    *((r14 + 0x10)) = rax;
    rax = *((rsp + 0x10));
    *((r14 + 0x18)) = rax;
    eax = 0;
label_7:
    return rax;
label_34:
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    r13 = 0xffffffffffffffff;
    r12 = rcx + rdx*8;
    *(r12) = rax;
    *((r12 + 0x10)) = 0;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x28)) = 0xffffffffffffffff;
    *((r12 + 0x30)) = 0;
    *((r12 + 0x38)) = 0;
    *((r12 + 0x40)) = 0xffffffffffffffff;
    *((r12 + 0x50)) = 0xffffffffffffffff;
    ebp = *((rax + 1));
    edx = rbp - 0x30;
    if (dl <= 9) {
        goto label_35;
    }
label_5:
    rcx = 0x0000f25c;
    rdx = rbx + 1;
    if (bpl == 0x27) {
        goto label_36;
    }
    do {
        eax = rbp - 0x20;
        if (al <= 0x29) {
            eax = (int32_t) al;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (42 cases) at 0xf25c */
            void (*rax)() ();
        }
        if (bpl == 0x2a) {
            goto label_37;
        }
        eax = rbp - 0x30;
        if (al <= 9) {
            goto label_38;
        }
label_17:
        if (bpl == 0x2e) {
            goto label_39;
        }
label_3:
        edx = 0;
        rsi = 0x0000f304;
        edi = 1;
        rbx++;
        if (bpl == 0x68) {
            goto label_40;
        }
label_1:
        eax = rbp - 0x4c;
        if (al > 0x2e) {
            goto label_41;
        }
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (47 cases) at 0xf304 */
        void (*rax)() ();
        eax = *((r12 + 0x10));
        eax |= 0x40;
label_0:
        *((r12 + 0x10)) = eax;
        ebp = *(rdx);
        rbx = rdx;
        rdx = rbx + 1;
    } while (bpl != 0x27);
label_36:
    eax = *((r12 + 0x10));
    eax |= 1;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x20;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 2;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 4;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x10;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 8;
    goto label_0;
label_41:
    eax = rbp - 0x25;
    if (al > 0x53) {
        goto label_10;
    }
    rcx = 0x0000f3c0;
    eax = (int32_t) al;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (84 cases) at 0xf3c0 */
    void (*rax)() ();
    edx += 8;
label_2:
    ebp = *(rbx);
    rbx++;
    if (bpl != 0x68) {
        goto label_1;
    }
label_40:
    ecx = edx;
    eax = edi;
    ecx &= 1;
    eax <<= cl;
    edx |= eax;
    goto label_2;
label_37:
    rdi = *((rsp + 8));
    eax = 1;
    *((r12 + 0x18)) = rbx;
    *((r12 + 0x20)) = rdx;
    ecx = *((rbx + 1));
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
    eax = rcx - 0x30;
    if (al <= 9) {
        goto label_42;
    }
label_9:
    rdi = *((rsp + 0x20));
    *((r12 + 0x28)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbp = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    rbx = rdx;
label_29:
    rdx = *((r15 + 8));
    r8 = *((r15 + 8));
    if (r9 <= rbp) {
        r9 += r9;
        rax = rbp + 1;
        if (r9 <= rbp) {
            r9 = rax;
        }
        rax = r9;
        rax >>= 0x3b;
        if (rax != 0) {
            goto label_30;
        }
        rsi = r9;
        rsi <<= 5;
        if (*((rsp + 0x18)) == rdx) {
            goto label_43;
        }
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r11;
        *((rsp + 0x28)) = r10;
        rax = realloc (rdx, rsi);
        rdx = *((r15 + 8));
        r10 = *((rsp + 0x28));
        r11 = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r8 = rax;
label_20:
        if (r8 == 0) {
            goto label_30;
        }
        if (*((rsp + 0x18)) == rdx) {
            goto label_44;
        }
label_26:
        *((r15 + 8)) = r8;
    }
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbp) {
        goto label_45;
    }
    do {
        rdx++;
        *(rax) = 0;
        rcx = rax;
        rax += 0x20;
    } while (rdx <= rbp);
    *(r15) = rdx;
    *(rcx) = 0;
label_45:
    rbp <<= 5;
    rbp += r8;
    eax = *(rbp);
    if (eax != 0) {
        goto label_46;
    }
    *(rbp) = 5;
    ebp = *(rbx);
    if (bpl != 0x2e) {
        goto label_3;
    }
label_39:
    if (*((rbx + 1)) != 0x2a) {
        goto label_47;
    }
    rdi = *((rsp + 0x10));
    eax = 2;
    rcx = rbx + 2;
    *((r12 + 0x30)) = rbx;
    *((r12 + 0x38)) = rcx;
    esi = *((rbx + 2));
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    eax = rsi - 0x30;
    if (al <= 9) {
        goto label_48;
    }
label_22:
    rbx = *((r12 + 0x40));
    if (rbx == -1) {
        goto label_49;
    }
label_21:
    r8 = *((r15 + 8));
    if (r9 <= rbx) {
        goto label_50;
    }
label_19:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbx) {
        goto label_51;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= rbx);
    *(r15) = rdx;
    *(rsi) = 0;
label_51:
    rbx <<= 5;
    rax = r8 + rbx;
    edx = *(rax);
    if (edx != 0) {
        goto label_52;
    }
    *(rax) = 5;
    rbx = rcx;
    ebp = *(rcx);
    goto label_3;
    edx |= 4;
    goto label_2;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xf;
label_12:
    if (r13 == -1) {
        goto label_53;
    }
    *((r12 + 0x50)) = r13;
label_16:
    r8 = *((r15 + 8));
    if (r9 <= r13) {
        goto label_54;
    }
label_15:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > r13) {
        goto label_55;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= r13);
    *(r15) = rdx;
    *(rsi) = 0;
label_55:
    r13 <<= 5;
    r13 += r8;
    eax = *(r13);
    if (eax != 0) {
        goto label_56;
    }
    *(r13) = ecx;
label_13:
    *((r12 + 0x48)) = bpl;
    rax = *(r14);
    *((r12 + 8)) = rbx;
    r13 = rax + 1;
    *(r14) = r13;
    if (r11 > r13) {
        rcx = *((r14 + 8));
        goto label_4;
    }
    if (r11 < 0) {
        goto label_57;
    }
    rax = 0x2e8ba2e8ba2e8ba;
    r12 = r11 + r11;
    if (r12 > rax) {
        goto label_57;
    }
    rax = r11 * 5;
    rbp = *((r14 + 8));
    *((rsp + 0x30)) = r9;
    rsi = r11 + rax*2;
    *((rsp + 0x28)) = r10;
    rsi <<= 4;
    if (r10 == rbp) {
        goto label_58;
    }
    rax = realloc (rbp, rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_57;
    }
    rbp = *((r14 + 8));
    if (r10 == rbp) {
        goto label_59;
    }
label_8:
    *((r14 + 8)) = rcx;
    r13 = *(r14);
    r11 = r12;
    goto label_4;
label_35:
    rdx = rbx;
    do {
        ecx = *((rdx + 1));
        rdx++;
        esi = rcx - 0x30;
    } while (sil <= 9);
    r13 = 0xffffffffffffffff;
    if (cl != 0x24) {
        goto label_5;
    }
    rax += 2;
    edi = 0;
    while (rsi >= 0) {
        if (dl > 9) {
            goto label_60;
        }
        rax++;
        edx = rbp - 0x30;
        rcx = rax - 1;
        rsi = 0x1999999999999999;
        rdx = (int64_t) dl;
        if (rdi > rsi) {
            goto label_61;
        }
        rsi = rdi * 5;
        rsi += rsi;
label_6:
        ebp = *(rax);
        rsi += rdx;
        rdi = rsi;
        edx = rbp - 0x30;
    }
    if (dl <= 9) {
        rcx = rax;
        rdx = (int64_t) dl;
        rax++;
        rsi = 0xffffffffffffffff;
        goto label_6;
    }
label_10:
    r8 = *((r15 + 8));
label_14:
    if (*((rsp + 0x18)) != r8) {
        *((rsp + 8)) = r10;
        free (r8);
        r10 = *((rsp + 8));
    }
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_7;
label_47:
    *((r12 + 0x30)) = rbx;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    eax -= 0x30;
    if (al > 9) {
        goto label_62;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
label_28:
    rdi = *((rsp + 0x10));
    *((r12 + 0x38)) = rdx;
    ebp = *(rdx);
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_58:
    rax = malloc (rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_63;
    }
label_33:
    rax = r13 * 5;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r10;
    rdx <<= 3;
    rax = memcpy (rcx, rbp, r13 + rax*2);
    r9 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    rcx = rax;
    goto label_8;
label_42:
    rax = rdx;
    do {
        esi = *((rax + 1));
        rax++;
        edi = rsi - 0x30;
    } while (dil <= 9);
    if (sil != 0x24) {
        goto label_9;
    }
    rbx += 2;
    esi = 0;
    while (rcx >= 0) {
        if (al > 9) {
            goto label_64;
        }
        rbx++;
        eax = rcx - 0x30;
        rdx = rbx - 1;
        rdi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rsi > rdi) {
            goto label_65;
        }
        rcx = rsi * 5;
        rcx += rcx;
label_11:
        rcx += rax;
        rsi = rcx;
        ecx = *(rbx);
        eax = rcx - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rdx = rbx;
    rax = (int64_t) al;
    rbx++;
    rcx = 0xffffffffffffffff;
    goto label_11;
    ecx = 0xc;
    if (edx > 0xf) {
        goto label_12;
    }
    ecx = 0;
    edx &= 4;
    cl = (edx != 0) ? 1 : 0;
    ecx += 0xb;
    goto label_12;
    ecx = 0xa;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 8;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 2;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 4;
    goto label_12;
    ecx = 9;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 7;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 1;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 3;
    goto label_12;
    ecx = 0x16;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 0x15;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 0x12;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx = 0x14;
    ecx -= edx;
    goto label_12;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xd;
    goto label_12;
    ecx = 0xe;
    goto label_12;
label_56:
    if (eax == ecx) {
        goto label_13;
    }
    goto label_14;
label_54:
    r9 += r9;
    rax = r13 + 1;
    if (r9 <= r13) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_67;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = realloc (r8, rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_68;
    }
label_18:
    *((r15 + 8)) = r8;
    goto label_15;
label_53:
    rdi = *((rsp + 0x20));
    *((r12 + 0x50)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    r13 = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_16;
label_38:
    *((r12 + 0x18)) = rbx;
    eax = *(rbx);
    eax -= 0x30;
    if (al > 9) {
        goto label_69;
    }
    rdx = rbx;
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rdi = *((rsp + 8));
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
label_69:
    *((r12 + 0x20)) = rbx;
    do {
        ebp = *(rbx);
        goto label_17;
label_46:
    } while (eax == 5);
    goto label_14;
label_67:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = malloc (rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax != 0) {
label_31:
        *((rsp + 0x40)) = r9;
        *((rsp + 0x38)) = r11;
        rdx <<= 5;
        *((rsp + 0x30)) = r10;
        *((rsp + 0x28)) = ecx;
        rax = memcpy (rdi, r8, *(r15));
        r9 = *((rsp + 0x40));
        r11 = *((rsp + 0x38));
        r10 = *((rsp + 0x30));
        ecx = *((rsp + 0x28));
        r8 = rax;
        goto label_18;
label_63:
        rdx = *((r15 + 8));
        if (*((rsp + 0x18)) == rdx) {
            goto label_70;
        }
label_24:
        *((rsp + 8)) = r10;
        free (rdx);
        r10 = *((rsp + 8));
    }
label_25:
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
label_70:
    errno_location ();
    *(rax) = 0xc;
    eax = 0xffffffff;
    return rax;
label_50:
    r9 += r9;
    rax = rbx + 1;
    if (r9 <= rbx) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_71;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = realloc (r8, rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_72;
    }
label_27:
    *((r15 + 8)) = r8;
    goto label_19;
label_52:
    if (edx != 5) {
        goto label_14;
    }
    ebp = *(rcx);
    rbx = rcx;
    goto label_3;
label_43:
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x28)) = r10;
    rax = malloc (rsi);
    rdx = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_20;
    ecx = 0x11;
    goto label_12;
    ecx = 0x10;
    goto label_12;
label_49:
    rdi = *((rsp + 0x20));
    *((r12 + 0x40)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbx = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_21;
label_48:
    rax = rcx;
    do {
        edx = *((rax + 1));
        rax++;
        edi = rdx - 0x30;
    } while (dil <= 9);
    if (dl != 0x24) {
        goto label_22;
    }
    rbx += 3;
    edi = 0;
    while (rdx >= 0) {
        if (al > 9) {
            goto label_73;
        }
        rbx++;
        eax = rsi - 0x30;
        rcx = rbx - 1;
        rsi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rdi > rsi) {
            goto label_74;
        }
        rdx = rdi * 5;
        rdx += rdx;
label_23:
        esi = *(rbx);
        rdx += rax;
        rdi = rdx;
        eax = rsi - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rcx = rbx;
    rax = (int64_t) al;
    rbx++;
    rdx = 0xffffffffffffffff;
    goto label_23;
label_61:
    rsi = 0xffffffffffffffff;
    goto label_6;
label_60:
    r13 = rsi;
    r13--;
    if (r13 > 0xfffffffffffffffd) {
        goto label_10;
    }
    ebp = *((rcx + 2));
    rbx = rcx + 2;
    goto label_5;
label_57:
    rdx = *((r15 + 8));
label_30:
    if (*((rsp + 0x18)) != rdx) {
        goto label_24;
    }
    goto label_25;
label_44:
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    rdx <<= 5;
    *((rsp + 0x28)) = r10;
    rax = memcpy (r8, *((rsp + 0x18)), *(r15));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_26;
label_71:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = malloc (rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax == 0) {
        goto label_25;
    }
label_32:
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    rdx <<= 5;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = memcpy (rdi, r8, *(r15));
    r9 = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    r10 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    r8 = rax;
    goto label_27;
label_65:
    rcx = 0xffffffffffffffff;
    goto label_11;
label_62:
    rbx = rdx;
    eax = 1;
    goto label_28;
label_64:
    rbp--;
    if (rbp > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x28)) = rbp;
    rbx = rdx + 2;
    goto label_29;
label_74:
    rdx = 0xffffffffffffffff;
    goto label_23;
label_73:
    rbx = rdx - 1;
    if (rbx > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x40)) = rbx;
    rcx += 2;
    goto label_21;
label_66:
    rdx = r8;
    goto label_30;
label_68:
    rdi = r8;
    r8 = rax;
    goto label_31;
label_72:
    rdi = r8;
    r8 = rax;
    goto label_32;
label_59:
    r13 = *(r14);
    goto label_33;
}

/* /tmp/tmpwp339vba @ 0x9070 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpwp339vba @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpwp339vba @ 0x9710 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0xb350 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpwp339vba @ 0x9410 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x97d0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2660)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpwp339vba @ 0x61a0 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmpwp339vba @ 0x2420 */
 
void snprintf_chk (void) {
    /* [15] -r-x section size 992 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpwp339vba @ 0x2430 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpwp339vba @ 0x0 */
 
int64_t libc_start_main (func init, char ** ubp_av) {
    rcx = init;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    ch |= *(rsi);
    *((rax + rax)) ^= bh;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += ah;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    eax |= 0xfffffe00;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0x0000005d) += cl;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    bh += bh;
    *(rbx)++;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) -= eax;
    *(rax) += al;
    *(rax) += al;
    al += dh;
    *(rax) -= eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(0x000000f1) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0xa341000000000000) = eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("clc");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("clc");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpwp339vba @ 0x2470 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpwp339vba @ 0x2490 */
 
void puts (void) {
    __asm ("bnd jmp qword [reloc.puts]");
}

/* /tmp/tmpwp339vba @ 0x24a0 */
 
void ferror (void) {
    __asm ("bnd jmp qword [reloc.ferror]");
}

/* /tmp/tmpwp339vba @ 0x24c0 */
 
void strtod (void) {
    __asm ("bnd jmp qword [reloc.strtod]");
}

/* /tmp/tmpwp339vba @ 0x24d0 */
 
void localeconv (void) {
    __asm ("bnd jmp qword [reloc.localeconv]");
}

/* /tmp/tmpwp339vba @ 0x24e0 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmpwp339vba @ 0x24f0 */
 
void fread_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fread_unlocked]");
}

/* /tmp/tmpwp339vba @ 0x2500 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpwp339vba @ 0x2510 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpwp339vba @ 0x2520 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpwp339vba @ 0x2540 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpwp339vba @ 0x2570 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpwp339vba @ 0x2580 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpwp339vba @ 0x2590 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpwp339vba @ 0x25a0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpwp339vba @ 0x25c0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpwp339vba @ 0x25d0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpwp339vba @ 0x25e0 */
 
void strtof (void) {
    __asm ("bnd jmp qword [reloc.strtof]");
}

/* /tmp/tmpwp339vba @ 0x25f0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpwp339vba @ 0x2600 */
 
void fgetc (void) {
    __asm ("bnd jmp qword [reloc.fgetc]");
}

/* /tmp/tmpwp339vba @ 0x2610 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpwp339vba @ 0x2660 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpwp339vba @ 0x2670 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpwp339vba @ 0x2690 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpwp339vba @ 0x26b0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpwp339vba @ 0x26c0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpwp339vba @ 0x2700 */
 
void setvbuf (void) {
    __asm ("bnd jmp qword [reloc.setvbuf]");
}

/* /tmp/tmpwp339vba @ 0x2710 */
 
void strtold (void) {
    __asm ("bnd jmp qword [reloc.strtold]");
}

/* /tmp/tmpwp339vba @ 0x2730 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpwp339vba @ 0x2740 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpwp339vba @ 0x2750 */
 
void fread_unlocked_chk (void) {
    __asm ("bnd jmp qword [reloc.__fread_unlocked_chk]");
}

/* /tmp/tmpwp339vba @ 0x2760 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmpwp339vba @ 0x2770 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpwp339vba @ 0x27b0 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpwp339vba @ 0x27c0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpwp339vba @ 0x27d0 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmpwp339vba @ 0x27e0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpwp339vba @ 0x27f0 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmpwp339vba @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1008 named .plt */
    __asm ("bnd jmp qword [0x00012dd8]");
}

/* /tmp/tmpwp339vba @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpwp339vba @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}
