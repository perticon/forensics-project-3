uint read_block(ulong param_1,long param_2,long *param_3)

{
  uint uVar1;
  uint uVar2;
  int *piVar3;
  size_t sVar4;
  FILE *__stream;
  long lVar5;
  uint uVar6;
  
  __stream = in_stream;
  if ((param_1 != 0) && (param_1 <= bytes_per_block)) {
    *param_3 = 0;
    if (__stream == (FILE *)0x0) {
      uVar6 = 1;
    }
    else {
      lVar5 = 0;
      uVar6 = 1;
      while( true ) {
        sVar4 = fread_unlocked((void *)(lVar5 + param_2),1,param_1 - lVar5,__stream);
        *param_3 = *param_3 + sVar4;
        if (param_1 - lVar5 == sVar4) break;
        piVar3 = __errno_location();
        uVar1 = check_and_close(*piVar3);
        uVar2 = open_next_file();
        uVar6 = uVar6 & uVar1 & uVar2;
        if (in_stream == (FILE *)0x0) {
          return uVar6;
        }
        lVar5 = *param_3;
        __stream = in_stream;
      }
    }
    return uVar6;
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("0 < n && n <= bytes_per_block","src/od.c",0x50d,"read_block");
}