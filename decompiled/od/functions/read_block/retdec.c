int64_t read_block(int64_t a1, int64_t a2, int64_t * a3) {
    // 0x4fe0
    if (a1 - 1 >= (uint64_t)bytes_per_block) {
        // 0x507b
        return function_25d0();
    }
    // 0x4ffd
    *a3 = 0;
    int64_t v1 = 0; // 0x5011
    if (in_stream == NULL) {
        // 0x5067
        return 1;
    }
    int64_t v2 = 1; // 0x5011
    v1 = a1 - v1;
    int64_t v3 = function_24f0(); // 0x5059
    *a3 = v3 + v1;
    while (v1 != v3) {
        int32_t v4 = *(int32_t *)function_2450(); // 0x5025
        bool v5 = check_and_close(v4); // 0x5027
        v2 = v5 == open_next_file() ? v2 : 0;
        if (in_stream == NULL) {
            // break -> 0x5067
            break;
        }
        v1 = a1 - v1;
        v3 = function_24f0();
        *a3 = v3 + v1;
    }
    // 0x5067
    return v2 & 0xffffffff;
}