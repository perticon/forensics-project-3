uint64_t read_block (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    if (rdi == 0) {
        goto label_0;
    }
    r13 = rdi;
    if (*(obj.bytes_per_block) < rdi) {
        goto label_0;
    }
    rcx = in_stream;
    *(rdx) = 0;
    if (rcx == 0) {
        goto label_1;
    }
    r14 = rsi;
    edi = 0;
    r12d = 1;
    while (rbx != rax) {
        rax = errno_location ();
        eax = check_and_close (*(rax));
        ebx = eax;
        eax = open_next_file ();
        rcx = in_stream;
        ebx &= eax;
        r12d &= ebx;
        if (rcx == 0) {
            goto label_2;
        }
        rdi = *(rbp);
        rbx = r13;
        esi = 1;
        rbx -= rdi;
        rdi += r14;
        rdx = rbx;
        rax = fread_unlocked ();
        *(rbp) += rax;
    }
    do {
label_2:
        eax = r12d;
        return rax;
label_1:
        r12d = 1;
    } while (1);
label_0:
    return assert_fail ("0 < n && n <= bytes_per_block", "src/od.c", 0x50d, "read_block");
}