read_block (size_t n, char *block, size_t *n_bytes_in_buffer)
{
  bool ok = true;

  assert (0 < n && n <= bytes_per_block);

  *n_bytes_in_buffer = 0;

  while (in_stream != NULL)	/* EOF.  */
    {
      size_t n_needed;
      size_t n_read;

      n_needed = n - *n_bytes_in_buffer;
      n_read = fread (block + *n_bytes_in_buffer, 1, n_needed, in_stream);

      *n_bytes_in_buffer += n_read;

      if (n_read == n_needed)
        break;

      ok &= check_and_close (errno);

      ok &= open_next_file ();
    }

  return ok;
}