uint32_t read_block(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int64_t v6;
    int64_t rbx7;
    int1_t cf8;
    void** r13_9;
    void** rcx10;
    void** rbp11;
    uint32_t r12d12;
    void** rdi13;
    void** rbx14;
    void** rax15;
    int32_t* rax16;
    int32_t edi17;
    uint32_t eax18;
    uint32_t eax19;
    void** r12_20;
    int1_t zf21;
    uint64_t rax22;
    int1_t below_or_equal23;
    struct s0* rdi24;
    struct s4* rbp25;
    void** rsi26;
    struct s0* rax27;
    uint64_t rdx28;
    int32_t ecx29;
    void** rbx30;
    int32_t r15d31;
    int32_t edx32;
    int32_t r14d33;
    int64_t rax34;
    int64_t r9_35;
    int64_t r8_36;
    int32_t eax37;
    unsigned char al38;
    void* rax39;
    void** rbx40;
    uint64_t rax41;
    int1_t zf42;
    void*** rax43;
    void** r15_44;
    void** rdi45;
    void** rax46;
    uint32_t eax47;
    void*** rcx48;
    uint32_t eax49;
    uint32_t eax50;
    int64_t r14_51;
    uint64_t r8_52;
    int1_t less_or_equal53;
    void** rdi54;
    uint32_t ecx55;
    void** rbx56;
    int64_t rcx57;
    uint32_t eax58;
    void** rdi59;
    void** rax60;
    int64_t rax61;

    v6 = rbx7;
    if (rdi && (cf8 = reinterpret_cast<unsigned char>(bytes_per_block) < reinterpret_cast<unsigned char>(rdi), r13_9 = rdi, !cf8)) {
        rcx10 = in_stream;
        *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
        rbp11 = rdx;
        if (!rcx10) {
            r12d12 = 1;
        } else {
            *reinterpret_cast<int32_t*>(&rdi13) = 0;
            *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
            r12d12 = 1;
            while ((rbx14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) - reinterpret_cast<unsigned char>(rdi13)), rax15 = fun_24f0(), *reinterpret_cast<void***>(rbp11) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11)) + reinterpret_cast<unsigned char>(rax15)), rbx14 != rax15) && (rax16 = fun_2450(), edi17 = *rax16, eax18 = check_and_close(edi17, 1, rbx14, rcx10, r8), eax19 = open_next_file(), rcx10 = in_stream, r12d12 = r12d12 & (eax18 & eax19), !!rcx10)) {
                rdi13 = *reinterpret_cast<void***>(rbp11);
            }
        }
        return r12d12;
    }
    fun_25d0("0 < n && n <= bytes_per_block", "src/od.c", 0x50d, "read_block", r8);
    r12_20 = reinterpret_cast<void**>("0 < n && n <= bytes_per_block");
    zf21 = gd06a == 0;
    rax22 = n_specs;
    if (!zf21) 
        goto addr_50d0_10;
    addr_5620_11:
    addr_5174_12:
    goto v6;
    while (1) {
        addr_50d0_10:
        below_or_equal23 = n_specs_allocated <= rax22;
        rdi24 = spec;
        if (!below_or_equal23) {
            rbp25 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rdi24) + (rax22 + rax22 * 4) * 8);
            if (!rbp25) 
                goto addr_5828_14;
            *reinterpret_cast<uint32_t*>(&rsi26) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20));
            *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
            if (*reinterpret_cast<signed char*>(&rsi26) == 97) 
                goto addr_51c6_16; else 
                goto addr_5104_17;
        }
        rax27 = x2nrealloc();
        rdx28 = n_specs;
        *reinterpret_cast<uint32_t*>(&rsi26) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20));
        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
        spec = rax27;
        rbp25 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rax27) + (rdx28 + rdx28 * 4) * 8);
        if (*reinterpret_cast<signed char*>(&rsi26) != 97) {
            addr_5104_17:
            ecx29 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rsi26 + 0xffffffffffffff9d));
            if (*reinterpret_cast<unsigned char*>(&ecx29) > 21) 
                break;
        } else {
            addr_51c6_16:
            rbx30 = r12_20 + 1;
            r15d31 = 3;
            edx32 = 5;
            r14d33 = 1;
            rax34 = 0x3c30;
            goto addr_51e3_19;
        }
        r9_35 = 1 << *reinterpret_cast<unsigned char*>(&ecx29);
        r8_36 = r9_35;
        *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(&r8_36) & 0x241002;
        *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r8)) 
            goto addr_5308_21;
        if (*reinterpret_cast<uint32_t*>(&r9_35) & 1) {
            rbx30 = r12_20 + 1;
            r15d31 = 3;
            edx32 = 6;
            r14d33 = 1;
            rax34 = 0x45f0;
        } else {
            if (*reinterpret_cast<unsigned char*>(&ecx29) != 3) 
                break;
            eax37 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_20 + 1));
            if (*reinterpret_cast<signed char*>(&eax37) == 70) 
                goto addr_55d0_26; else 
                goto addr_523e_27;
        }
        addr_51e3_19:
        rbp25->f8 = rax34;
        rbp25->f4 = r14d33;
        rbp25->f0 = edx32;
        rbp25->f1c = r15d31;
        al38 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx30) == 0x7a);
        rbp25->f18 = al38;
        *reinterpret_cast<uint32_t*>(&rax39) = al38;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
        rbx40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx30) + reinterpret_cast<uint64_t>(rax39));
        if (rbx40 == r12_20) 
            goto addr_5809_28;
        rax41 = n_specs;
        rax22 = rax41 + 1;
        zf42 = *reinterpret_cast<void***>(rbx40) == 0;
        n_specs = rax22;
        if (zf42) 
            goto addr_5620_11;
        r12_20 = rbx40;
        continue;
        addr_55d0_26:
        r14d33 = g13130;
        rbx30 = r12_20 + 2;
        addr_553c_31:
        rax43 = fun_24d0();
        *reinterpret_cast<int32_t*>(&r15_44) = 1;
        rdi45 = *rax43;
        if (*reinterpret_cast<void***>(rdi45)) {
            rax46 = fun_2550(rdi45, rdi45);
            r15_44 = rax46;
        }
        if (r14d33 == 7) {
            r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 23;
            rax34 = 0x43b0;
            edx32 = 4;
            goto addr_51e3_19;
        } else {
            if (r14d33 != 8) {
                if (r14d33 != 6) 
                    goto addr_2800_37;
                r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 14;
                rax34 = 0x44c0;
                edx32 = 4;
                goto addr_51e3_19;
            } else {
                r15d31 = *reinterpret_cast<int32_t*>(&r15_44) + 28;
                rax34 = 0x4290;
                edx32 = 4;
                goto addr_51e3_19;
            }
        }
        addr_523e_27:
        if (*reinterpret_cast<signed char*>(&eax37) == 76) {
            r14d33 = g13160;
            rbx30 = r12_20 + 2;
            goto addr_553c_31;
        } else {
            if (*reinterpret_cast<signed char*>(&eax37) == 68) {
                r14d33 = g13140;
                rbx30 = r12_20 + 2;
                goto addr_553c_31;
            } else {
                eax47 = eax37 - 48;
                rsi26 = r12_20 + 1;
                rbx30 = r12_20 + 2;
                rcx48 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax47)));
                if (eax47 > 9) {
                    r14d33 = g13140;
                    rbx30 = rsi26;
                    goto addr_553c_31;
                } else {
                    do {
                        r8 = reinterpret_cast<void**>(rcx48 + reinterpret_cast<uint64_t>(r8 + reinterpret_cast<unsigned char>(r8) * 4) * 2);
                        eax49 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx30) - 48);
                        if (eax49 > 9) 
                            break;
                        rcx48 = reinterpret_cast<void***>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax49)));
                        ++rbx30;
                    } while (reinterpret_cast<unsigned char>(__intrinsic() >> 3) >= reinterpret_cast<unsigned char>(r8));
                    goto addr_52a3_48;
                    r14d33 = g13140;
                    if (rsi26 == rbx30) 
                        goto addr_553c_31;
                    if (reinterpret_cast<unsigned char>(r8) > reinterpret_cast<unsigned char>(16)) 
                        goto addr_572a_51;
                    r14d33 = *reinterpret_cast<int32_t*>(0x13120 + reinterpret_cast<unsigned char>(r8) * 4);
                    if (r14d33) 
                        goto addr_553c_31; else 
                        goto addr_572a_51;
                }
            }
        }
    }
    quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
    addr_5308_21:
    eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_20 + 1));
    if (*reinterpret_cast<signed char*>(&eax50) == 76) {
        *reinterpret_cast<uint32_t*>(&r14_51) = g131a0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_52) = 8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
        goto addr_533e_55;
    }
    if (*reinterpret_cast<signed char*>(&eax50) > 76) {
        if (*reinterpret_cast<signed char*>(&eax50) != 83) {
            goto addr_57e8_59;
        } else {
            *reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&g13184) + 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_52) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
            less_or_equal53 = *reinterpret_cast<signed char*>(&rsi26) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi26) != 0x75) 
                goto addr_5348_61; else 
                goto addr_5404_62;
        }
    }
    if (*reinterpret_cast<signed char*>(&eax50) != 67) 
        goto addr_5324_64;
    *reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(&g13184);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_52) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
    goto addr_533e_55;
    addr_5324_64:
    if (*reinterpret_cast<signed char*>(&eax50) == 73) {
        *reinterpret_cast<uint32_t*>(&r14_51) = g13190;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_52) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
        goto addr_533e_55;
    }
    rdi54 = r12_20 + 1;
    ecx55 = *reinterpret_cast<signed char*>(&eax50) - 48;
    if (ecx55 > 9) 
        goto addr_5866_68;
    rbx56 = r12_20 + 2;
    rcx57 = reinterpret_cast<int32_t>(ecx55);
    *reinterpret_cast<int32_t*>(&r8_52) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
    do {
        r8_52 = rcx57 + (r8_52 + r8_52 * 4) * 2;
        eax58 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx56) - 48);
        if (eax58 > 9) 
            break;
        rcx57 = reinterpret_cast<int32_t>(eax58);
        ++rbx56;
    } while (__intrinsic() >> 3 >= r8_52);
    goto addr_5683_72;
    if (rdi54 == rbx56) 
        goto addr_57e8_59;
    if (r8_52 > 8 || (*reinterpret_cast<uint32_t*>(&r14_51) = *reinterpret_cast<uint32_t*>(0x13180 + r8_52 * 4), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0, !*reinterpret_cast<uint32_t*>(&r14_51))) {
        quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
        fun_2530();
        fun_2720();
        goto addr_5174_12;
    } else {
        while (1) {
            addr_533e_55:
            less_or_equal53 = *reinterpret_cast<signed char*>(&rsi26) <= 0x75;
            if (*reinterpret_cast<signed char*>(&rsi26) == 0x75) {
                addr_5404_62:
                r8 = reinterpret_cast<void**>("lu");
                if (static_cast<uint32_t>(r14_51 - 4) >= 2) {
                    r8 = reinterpret_cast<void**>("u");
                }
                fun_27f0();
                rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            } else {
                addr_5348_61:
                if (!less_or_equal53) {
                    if (*reinterpret_cast<signed char*>(&rsi26) != 0x78) 
                        goto addr_586e_79;
                    if (static_cast<uint32_t>(r14_51 - 4) >= 2) 
                        goto label_81; else 
                        goto addr_54fe_82;
                } else {
                    if (*reinterpret_cast<signed char*>(&rsi26) != 100) {
                        if (*reinterpret_cast<signed char*>(&rsi26) != 0x6f) 
                            goto addr_5873_85;
                        if (static_cast<uint32_t>(r14_51 - 4) >= 2) 
                            goto label_87; else 
                            goto addr_5496_88;
                    } else {
                        r8 = reinterpret_cast<void**>("ld");
                        if (static_cast<uint32_t>(r14_51 - 4) >= 2) {
                            r8 = reinterpret_cast<void**>("d");
                        }
                        fun_27f0();
                        rdi59 = reinterpret_cast<void**>(&rbp25->f10);
                    }
                }
            }
            addr_53a4_92:
            rax60 = fun_2550(rdi59, rdi59);
            if (reinterpret_cast<unsigned char>(rax60) <= reinterpret_cast<unsigned char>(7)) 
                break;
            addr_5847_93:
            rsi26 = reinterpret_cast<void**>("src/od.c");
            fun_25d0("strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8, "strlen (tspec->fmt_string) < FMT_BYTES_ALLOCATED", "src/od.c", 0x2eb, "decode_one_format", r8);
            addr_5866_68:
            addr_57e8_59:
            *reinterpret_cast<uint32_t*>(&r14_51) = g13190;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_52) = 4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
            continue;
            label_81:
            addr_54fe_82:
            *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(0xd560 + r8_52 * 4);
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27f0();
            rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            goto addr_53a4_92;
            label_87:
            addr_5496_88:
            *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(0xd680 + r8_52 * 4);
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27f0();
            rdi59 = reinterpret_cast<void**>(&rbp25->f10);
            goto addr_53a4_92;
        }
    }
    if (*reinterpret_cast<uint32_t*>(&r14_51) <= 5) {
        *reinterpret_cast<uint32_t*>(&rax61) = *reinterpret_cast<uint32_t*>(&r14_51);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax61) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd298 + rax61 * 4) + 0xd298;
    }
    addr_2800_37:
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    fun_2440();
    addr_586e_79:
    goto addr_2800_37;
    addr_5873_85:
    goto addr_2800_37;
    addr_5683_72:
    addr_52a3_48:
    quote("0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
    addr_5809_28:
    fun_25d0("s != next", "src/od.c", 0x3e5, "decode_format_string", r8);
    addr_5828_14:
    fun_25d0("tspec != NULL", "src/od.c", 0x288, "decode_one_format", r8);
    goto addr_5847_93;
    addr_572a_51:
    quote("0 < n && n <= bytes_per_block", rsi26, "0 < n && n <= bytes_per_block", rsi26);
    fun_2530();
    fun_2720();
    goto addr_5174_12;
}