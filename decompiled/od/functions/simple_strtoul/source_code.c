simple_strtoul (char const *s, char const **p, unsigned long int *val)
{
  unsigned long int sum;

  sum = 0;
  while (ISDIGIT (*s))
    {
      int c = *s++ - '0';
      if (sum > (ULONG_MAX - c) / 10)
        return false;
      sum = sum * 10 + c;
    }
  *p = s;
  *val = sum;
  return true;
}