void write_block(int64_t current_offset, int64_t n_bytes, char * prev_block, char * curr_block) {
    // 0x4c80
    if (*(char *)&abbreviate_duplicate_blocks != 0 == g2 == 0) {
        // 0x4ea0
        if (bytes_per_block == n_bytes) {
            // 0x4ead
            if ((int32_t)function_2610() == 0) {
                // 0x4ec3
                if (g13 == 0) {
                    // 0x4ecc
                    function_2490();
                    g13 = 1;
                }
                // 0x4edf
                g2 = 0;
                return;
            }
        }
    }
    // 0x4caf
    g13 = 0;
    if (n_specs == 0) {
        // 0x4edf
        g2 = 0;
        return;
    }
    int64_t v1 = (int64_t)curr_block;
    for (int64_t i = 0; i < n_specs; i++) {
        // 0x4d44
        if (i != 0) {
            // 0x4d94
            function_26f0();
        }
        // 0x4cdd
        if (*(char *)(40 * i + 24 + (int64_t)spec) != 0) {
            // 0x4dc0
            function_26f0();
            function_26c0();
            if (n_bytes != 0) {
                int64_t * v2 = (int64_t *)function_27e0();
                int64_t v3 = (int64_t)g7; // 0x4e4a
                int64_t * v4 = (int64_t *)(v3 + 40); // 0x4e51
                uint64_t v5 = *v4; // 0x4e51
                unsigned char v6; // 0x4e28
                char v7; // 0x4e39
                if (v5 >= *(int64_t *)(v3 + 48)) {
                    // 0x4e90
                    function_25a0();
                } else {
                    // 0x4e5b
                    v6 = *(char *)v1;
                    v7 = *(char *)(*v2 + (2 * (int64_t)v6 | 1));
                    *v4 = v5 + 1;
                    *(char *)v5 = (v7 & 64) != 0 ? v6 : 46;
                }
                int64_t v8 = v1 + 1; // 0x4e2f
                while (v8 != v1 + n_bytes) {
                    int64_t v9 = v8;
                    v3 = (int64_t)g7;
                    v4 = (int64_t *)(v3 + 40);
                    v5 = *v4;
                    if (v5 >= *(int64_t *)(v3 + 48)) {
                        // 0x4e90
                        function_25a0();
                    } else {
                        // 0x4e5b
                        v6 = *(char *)v9;
                        v7 = *(char *)(*v2 + (2 * (int64_t)v6 | 1));
                        *v4 = v5 + 1;
                        *(char *)v5 = (v7 & 64) != 0 ? v6 : 46;
                    }
                    // 0x4e65
                    v8 = v9 + 1;
                }
            }
            int64_t v10 = (int64_t)g7; // 0x4e6a
            int64_t * v11 = (int64_t *)(v10 + 40); // 0x4e71
            uint64_t v12 = *v11; // 0x4e71
            if (v12 >= *(int64_t *)(v10 + 48)) {
                // 0x4f10
                function_25a0();
            } else {
                // 0x4e7f
                *v11 = v12 + 1;
                *(char *)v12 = 60;
            }
        }
        int64_t v13 = (int64_t)g7; // 0x4d13
        int64_t * v14 = (int64_t *)(v13 + 40); // 0x4d1a
        uint64_t v15 = *v14; // 0x4d1a
        if (v15 >= *(int64_t *)(v13 + 48)) {
            // 0x4ef8
            function_25a0();
        } else {
            // 0x4d28
            *v14 = v15 + 1;
            *(char *)v15 = 10;
        }
    }
    // 0x4edf
    g2 = 0;
}