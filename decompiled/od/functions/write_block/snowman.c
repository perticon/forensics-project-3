void write_block(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int64_t v6;
    int64_t v7;
    int64_t r15_8;
    int64_t v9;
    int64_t r14_10;
    void** r14_11;
    int64_t v12;
    int64_t r13_13;
    void** r13_14;
    int64_t v15;
    int64_t r12_16;
    int64_t v17;
    int64_t rbp18;
    int64_t v19;
    int64_t rbx20;
    int1_t zf21;
    void** v22;
    int1_t zf23;
    int1_t zf24;
    uint32_t eax25;
    int1_t zf26;
    uint64_t rbp27;
    struct s0* rdx28;
    uint64_t rax29;
    void** rbx30;
    void* r12_31;
    int64_t rax32;
    uint64_t rcx33;
    uint64_t rax34;
    int64_t v35;
    uint64_t r15_36;
    uint64_t rbx37;
    void** rdx38;
    int64_t r9_39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    struct s0* r10_43;
    void** rdx44;
    struct s2* r10_45;
    void** rcx46;
    struct s3* tmp64_47;
    int64_t rbx48;
    int64_t rax49;
    void** rdx50;
    int64_t v51;
    int64_t v52;
    int64_t v53;
    void** rax54;
    void** r15_55;
    void** rbx56;
    void** r12_57;
    void** rax58;
    int64_t rsi59;
    void** rdi60;
    void** rdi61;
    void** rax62;
    void** rdi63;
    void** rax64;
    int1_t below_or_equal65;
    int1_t zf66;

    v6 = reinterpret_cast<int64_t>(__return_address());
    v7 = r15_8;
    v9 = r14_10;
    r14_11 = rcx;
    v12 = r13_13;
    r13_14 = rsi;
    v15 = r12_16;
    v17 = rbp18;
    v19 = rbx20;
    zf21 = abbreviate_duplicate_blocks == 0;
    v22 = rdi;
    if (zf21 || ((zf23 = first_1 == 0, !zf23) || ((zf24 = bytes_per_block == rsi, !zf24) || (eax25 = fun_2610(rdx, rcx, rsi), !!eax25)))) {
        zf26 = n_specs == 0;
        prev_pair_equal_0 = 0;
        if (!zf26) {
            *reinterpret_cast<int32_t*>(&rbp27) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp27) + 4) = 0;
            do {
                rdx28 = spec;
                rax29 = rbp27 + rbp27 * 4;
                rbx30 = bytes_per_block;
                r12_31 = reinterpret_cast<void*>(rax29 * 8);
                *reinterpret_cast<int32_t*>(&rax32) = *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rdx28) + rax29 * 8 + 4);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
                rcx33 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax32 * 4)));
                rax34 = reinterpret_cast<unsigned char>(rbx30) / rcx33;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v35) + 4) = *reinterpret_cast<int32_t*>(&rax34);
                r15_36 = rax34;
                rbx37 = (reinterpret_cast<unsigned char>(rbx30) - reinterpret_cast<unsigned char>(r13_14)) / rcx33;
                if (!rbp27) {
                    format_address(v22);
                } else {
                    *reinterpret_cast<int32_t*>(&rdx38) = address_pad_len;
                    *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
                    fun_26f0(1, "%*s", rdx38, 0xec81, r8, r9_39, v35, v22, v40, v19, v17, v15, v12, v9, v7, v6, v41, v42);
                }
                r10_43 = spec;
                rdx44 = r14_11;
                r10_45 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(r10_43) + reinterpret_cast<uint64_t>(r12_31));
                *reinterpret_cast<int32_t*>(&r9_39) = r10_45->f20;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_39) + 4) = 0;
                *reinterpret_cast<int32_t*>(&r8) = r10_45->f1c;
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                rcx46 = reinterpret_cast<void**>(&r10_45->f10);
                r10_45->f8(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r15_36)), static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx37)), rdx44, rcx46, r8, r9_39);
                tmp64_47 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(r12_31) + reinterpret_cast<uint64_t>(spec));
                if (tmp64_47->f18) {
                    *reinterpret_cast<int32_t*>(&rbx48) = *reinterpret_cast<int32_t*>(&rbx37) * tmp64_47->f1c;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx48) + 4) = 0;
                    __asm__("cdq ");
                    *reinterpret_cast<int32_t*>(&rax49) = tmp64_47->f20 * *reinterpret_cast<int32_t*>(&rbx37) / *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v35) + 4);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx50) = static_cast<int32_t>(rax49 + rbx48);
                    *reinterpret_cast<int32_t*>(&rdx50 + 4) = 0;
                    fun_26f0(1, "%*s", rdx50, 0xec81, r8, r9_39, v35, v22, v51, v19, v17, v15, v12, v9, v7, v6, v52, v53);
                    *reinterpret_cast<int32_t*>(&rdx44) = 3;
                    *reinterpret_cast<int32_t*>(&rdx44 + 4) = 0;
                    rcx46 = stdout;
                    fun_26c0("  >", 1, 3, rcx46, r8, r9_39, v35);
                    if (r13_14) {
                        rax54 = fun_27e0("  >", 1, 3, rcx46, "  >", 1, 3, rcx46);
                        r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_11) + reinterpret_cast<unsigned char>(r13_14));
                        rbx56 = r14_11;
                        r12_57 = rax54;
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx56));
                            *reinterpret_cast<int32_t*>(&rcx46 + 4) = 0;
                            ++rbx56;
                            rax58 = rcx46;
                            *reinterpret_cast<uint32_t*>(&rsi59) = *reinterpret_cast<unsigned char*>(&rcx46);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                            if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(r12_57) + reinterpret_cast<unsigned char>(rcx46) * 2) + 1) & 64)) {
                                *reinterpret_cast<uint32_t*>(&rsi59) = 46;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rax58) = 46;
                            }
                            rdi60 = stdout;
                            rdx44 = *reinterpret_cast<void***>(rdi60 + 40);
                            if (reinterpret_cast<unsigned char>(rdx44) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi60 + 48))) {
                                fun_25a0(rdi60, rsi59, rdx44, rcx46, r8, rdi60, rsi59, rdx44, rcx46, r8);
                            } else {
                                rcx46 = rdx44 + 1;
                                *reinterpret_cast<void***>(rdi60 + 40) = rcx46;
                                *reinterpret_cast<void***>(rdx44) = rax58;
                            }
                        } while (rbx56 != r15_55);
                    }
                    rdi61 = stdout;
                    rax62 = *reinterpret_cast<void***>(rdi61 + 40);
                    if (reinterpret_cast<unsigned char>(rax62) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi61 + 48))) {
                        fun_25a0(rdi61, 60, rdx44, rcx46, r8, rdi61, 60, rdx44, rcx46, r8);
                    } else {
                        rdx44 = rax62 + 1;
                        *reinterpret_cast<void***>(rdi61 + 40) = rdx44;
                        *reinterpret_cast<void***>(rax62) = reinterpret_cast<void**>(60);
                    }
                }
                rdi63 = stdout;
                rax64 = *reinterpret_cast<void***>(rdi63 + 40);
                if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi63 + 48))) {
                    fun_25a0(rdi63, 10, rdx44, rcx46, r8, rdi63, 10, rdx44, rcx46, r8);
                } else {
                    *reinterpret_cast<void***>(rdi63 + 40) = rax64 + 1;
                    *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(10);
                }
                ++rbp27;
                below_or_equal65 = n_specs <= rbp27;
            } while (!below_or_equal65);
        }
    } else {
        zf66 = prev_pair_equal_0 == 0;
        if (zf66) {
            fun_2490("*", rcx, rsi);
            prev_pair_equal_0 = 1;
        }
    }
    first_1 = 0;
    return;
}