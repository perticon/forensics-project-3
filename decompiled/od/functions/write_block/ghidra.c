void write_block(undefined8 param_1,size_t param_2,void *param_3,byte *param_4)

{
  char *pcVar1;
  byte *pbVar2;
  byte bVar3;
  int iVar4;
  int iVar5;
  ushort **ppuVar6;
  ulong uVar7;
  byte *pbVar8;
  ulong uVar9;
  uint uVar10;
  long lVar11;
  
  if ((((abbreviate_duplicate_blocks != '\0') && (first_1 == '\0')) && (bytes_per_block == param_2))
     && (iVar5 = memcmp(param_3,param_4,param_2), iVar5 == 0)) {
    if (prev_pair_equal_0 != '\0') {
      first_1 = 0;
      return;
    }
    puts("*");
    first_1 = 0;
    prev_pair_equal_0 = 1;
    return;
  }
  prev_pair_equal_0 = 0;
  if (n_specs != 0) {
    uVar9 = 0;
    do {
      iVar5 = *(int *)(width_bytes + (ulong)*(uint *)(spec + 4 + uVar9 * 0x28) * 4);
      uVar7 = bytes_per_block - param_2;
      iVar4 = (int)(bytes_per_block / (ulong)(long)iVar5);
      if (uVar9 == 0) {
        (*format_address)(param_1,0);
      }
      else {
        __printf_chk(1,&DAT_0010d01c,address_pad_len,"");
      }
      iVar5 = (int)(uVar7 / (ulong)(long)iVar5);
      lVar11 = spec + uVar9 * 0x28;
      (**(code **)(lVar11 + 8))
                ((long)iVar4,(long)iVar5,param_4,lVar11 + 0x10,*(undefined4 *)(lVar11 + 0x1c),
                 *(undefined4 *)(lVar11 + 0x20));
      lVar11 = uVar9 * 0x28 + spec;
      if (*(char *)(lVar11 + 0x18) != '\0') {
        __printf_chk(1,&DAT_0010d01c,
                     (*(int *)(lVar11 + 0x20) * iVar5) / iVar4 + iVar5 * *(int *)(lVar11 + 0x1c),"")
        ;
        fwrite_unlocked(&DAT_0010d05d,1,3,stdout);
        if (param_2 != 0) {
          ppuVar6 = __ctype_b_loc();
          pbVar8 = param_4;
          do {
            bVar3 = *pbVar8;
            pbVar8 = pbVar8 + 1;
            uVar10 = (uint)bVar3;
            if ((*(byte *)((long)*ppuVar6 + (ulong)bVar3 * 2 + 1) & 0x40) == 0) {
              uVar10 = 0x2e;
              bVar3 = 0x2e;
            }
            pbVar2 = (byte *)stdout->_IO_write_ptr;
            if (pbVar2 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
              *pbVar2 = bVar3;
            }
            else {
              __overflow(stdout,uVar10);
            }
          } while (pbVar8 != param_4 + param_2);
        }
        pcVar1 = stdout->_IO_write_ptr;
        if (pcVar1 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar1 + 1;
          *pcVar1 = '<';
        }
        else {
          __overflow(stdout,0x3c);
        }
      }
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
      uVar9 = uVar9 + 1;
    } while (uVar9 < n_specs);
  }
  first_1 = 0;
  return;
}