dump_hexl_mode_trailer (size_t n_bytes, char const *block)
{
  fputs ("  >", stdout);
  for (size_t i = n_bytes; i > 0; i--)
    {
      unsigned char c = *block++;
      unsigned char c2 = (isprint (c) ? c : '.');
      putchar (c2);
    }
  putchar ('<');
}