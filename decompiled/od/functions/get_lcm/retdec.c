int32_t get_lcm(void) {
    // 0x39e0
    if (n_specs == 0) {
        // 0x3a45
        return 1;
    }
    int64_t v1 = (int64_t)&g22; // 0x3a0d
    uint32_t v2 = *(int32_t *)v1; // 0x3a10
    int64_t v3 = 0x100000000 >> 32; // 0x3a13
    int32_t v4 = *(int32_t *)(4 * (int64_t)v2 + (int64_t)&width_bytes); // 0x3a19
    int64_t v5 = v4; // 0x3a19
    uint64_t v6 = v5;
    int64_t v7 = v3 % v6;
    int64_t v8 = v6; // 0x3a2e
    while (v7 != 0) {
        // 0x3a20
        v6 = v7;
        v7 = v8 % v6;
        v8 = v6;
    }
    // 0x3a30
    v1 += 40;
    int64_t result = 0x100000000 * v5 / v6 / 0x100000000 * v3; // 0x3a3c
    while ((40 * n_specs || 4) + (int64_t)spec != v1) {
        // 0x3a10
        v2 = *(int32_t *)v1;
        v3 = 0x100000000 * (result & 0xffffffff) >> 32;
        v4 = *(int32_t *)(4 * (int64_t)v2 + (int64_t)&width_bytes);
        v5 = v4;
        v6 = v5;
        v7 = v3 % v6;
        v8 = v6;
        while (v7 != 0) {
            // 0x3a20
            v6 = v7;
            v7 = v8 % v6;
            v8 = v6;
        }
        // 0x3a30
        v1 += 40;
        result = 0x100000000 * v5 / v6 / 0x100000000 * v3;
    }
    // 0x3a45
    return result;
}