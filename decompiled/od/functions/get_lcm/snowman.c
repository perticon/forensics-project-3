uint32_t get_lcm(void** rdi, void** rsi, ...) {
    uint64_t rax3;
    uint32_t r8d4;
    struct s0* rdx5;
    void** r9_6;
    void** r11_7;
    int64_t rax8;
    uint64_t r8_9;
    uint64_t rcx10;
    uint64_t rdi11;
    uint64_t rdx12;
    uint64_t rax13;
    uint64_t rax14;

    rax3 = n_specs;
    if (!rax3) {
        r8d4 = 1;
    } else {
        rdx5 = spec;
        r8d4 = 1;
        r9_6 = reinterpret_cast<void**>(&rdx5->f4);
        r11_7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx5) + (rax3 + rax3 * 4) * 8 + 4);
        do {
            *reinterpret_cast<void***>(&rax8) = *reinterpret_cast<void***>(r9_6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            r8_9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(r8d4)));
            rcx10 = r8_9;
            rdi11 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(0xd520 + rax8 * 4)));
            rdx12 = rdi11;
            do {
                rax13 = rcx10;
                rcx10 = rdx12;
                rdx12 = rax13 % rcx10;
            } while (rdx12);
            r9_6 = r9_6 + 40;
            rax14 = rdi11 / rcx10;
            r8d4 = *reinterpret_cast<int32_t*>(&r8_9) * *reinterpret_cast<uint32_t*>(&rax14);
        } while (r11_7 != r9_6);
    }
    return r8d4;
}