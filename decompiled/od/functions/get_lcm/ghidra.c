int get_lcm(void)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  int iVar5;
  uint *puVar6;
  
  if (n_specs == 0) {
    iVar5 = 1;
  }
  else {
    iVar5 = 1;
    puVar6 = (uint *)(spec + 4);
    do {
      uVar1 = *puVar6;
      uVar2 = (long)iVar5;
      uVar4 = (long)*(int *)(width_bytes + (ulong)uVar1 * 4);
      do {
        uVar3 = uVar4;
        uVar4 = uVar2 % uVar3;
        uVar2 = uVar3;
      } while (uVar4 != 0);
      puVar6 = puVar6 + 10;
      iVar5 = iVar5 * (int)((ulong)(long)*(int *)(width_bytes + (ulong)uVar1 * 4) / uVar3);
    } while ((uint *)(spec + 4 + n_specs * 0x28) != puVar6);
  }
  return iVar5;
}