get_lcm (void)
{
  int l_c_m = 1;

  for (size_t i = 0; i < n_specs; i++)
    l_c_m = lcm (l_c_m, width_bytes[spec[i].size]);
  return l_c_m;
}