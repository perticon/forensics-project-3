uint read_char(int *param_1)

{
  uint uVar1;
  uint uVar2;
  int iVar3;
  int *piVar4;
  uint uVar5;
  
  uVar5 = 1;
  *param_1 = -1;
  while( true ) {
    if (in_stream == (FILE *)0x0) {
      return uVar5;
    }
    iVar3 = fgetc(in_stream);
    *param_1 = iVar3;
    if (iVar3 != -1) break;
    piVar4 = __errno_location();
    uVar1 = check_and_close(*piVar4);
    uVar2 = open_next_file();
    uVar5 = uVar5 & uVar1 & uVar2;
  }
  return uVar5;
}