read_char (int *c)
{
  bool ok = true;

  *c = EOF;

  while (in_stream != NULL)	/* EOF.  */
    {
      *c = fgetc (in_stream);

      if (*c != EOF)
        break;

      ok &= check_and_close (errno);

      ok &= open_next_file ();
    }

  return ok;
}