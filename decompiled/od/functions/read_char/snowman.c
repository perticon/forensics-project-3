uint32_t read_char(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    uint32_t r12d6;
    void** rbp7;
    void** rdi8;
    void** eax9;
    int32_t* rax10;
    int32_t edi11;
    uint32_t eax12;
    uint32_t eax13;
    void** rdi14;

    r12d6 = 1;
    rbp7 = rdi;
    *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0xffffffff);
    rdi8 = in_stream;
    if (rdi8) {
        do {
            eax9 = fun_2600();
            *reinterpret_cast<void***>(rbp7) = eax9;
            if (eax9 != 0xffffffff) 
                break;
            rax10 = fun_2450();
            edi11 = *rax10;
            eax12 = check_and_close(edi11, rsi, rdx, rcx, r8);
            eax13 = open_next_file();
            rdi14 = in_stream;
            r12d6 = r12d6 & (eax12 & eax13);
        } while (rdi14);
    }
    return r12d6;
}