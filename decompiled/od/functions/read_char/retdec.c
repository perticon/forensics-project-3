bool read_char(int32_t * c) {
    // 0x4c10
    *c = -1;
    if (in_stream == NULL) {
        // 0x4c69
        return true;
    }
    int64_t v1 = 1; // 0x4c2d
    int32_t v2 = function_2600(); // 0x4c61
    *c = v2;
    while (v2 == -1) {
        int32_t v3 = *(int32_t *)function_2450(); // 0x4c3d
        bool v4 = check_and_close(v3); // 0x4c3f
        v1 = v4 == open_next_file() ? v1 : 0;
        if (in_stream == NULL) {
            // break -> 0x4c69
            break;
        }
        v2 = function_2600();
        *c = v2;
    }
    // 0x4c69
    return v1 % 2 != 0;
}