uint32_t check_and_close(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t ebp6;
    void** rdi7;
    uint32_t eax8;
    void** rdx9;
    void*** rax10;
    int64_t rax11;
    int32_t* rax12;

    ebp6 = edi;
    rdi7 = in_stream;
    eax8 = 1;
    if (!rdi7) {
        addr_492b_2:
        rdx9 = stdout;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx9)) & 32) {
            fun_2530();
            fun_2720();
            return 0;
        } else {
            return eax8;
        }
    } else {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi7)) & 32)) {
            ebp6 = 0;
        }
        rax10 = file_list;
        if ((*reinterpret_cast<struct s5**>(rax10 - 8))->f0 != 45) 
            goto addr_4940_8;
        if (!(*reinterpret_cast<struct s5**>(rax10 - 8))->f1) 
            goto addr_4912_10;
    }
    addr_4940_8:
    rax11 = rpl_fclose(rdi7, rsi, rdx, rcx, r8);
    if (*reinterpret_cast<int32_t*>(&rax11)) {
        if (ebp6) 
            goto addr_4988_12;
        rax12 = fun_2450();
        ebp6 = *rax12;
    }
    addr_4917_14:
    eax8 = 1;
    if (ebp6) {
        addr_4988_12:
        quotearg_n_style_colon();
        fun_2720();
        eax8 = 0;
        goto addr_4920_15;
    } else {
        addr_4920_15:
        in_stream = reinterpret_cast<void**>(0);
        goto addr_492b_2;
    }
    addr_4912_10:
    fun_24e0();
    goto addr_4917_14;
}