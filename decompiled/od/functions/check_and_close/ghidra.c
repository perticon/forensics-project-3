undefined8 check_and_close(int param_1)

{
  int iVar1;
  undefined8 uVar2;
  int *piVar3;
  
  uVar2 = 1;
  if (in_stream == (FILE *)0x0) goto LAB_0010492b;
  if ((*(byte *)&in_stream->_flags & 0x20) == 0) {
    param_1 = 0;
  }
  if ((**(char **)(file_list + -8) == '-') && ((*(char **)(file_list + -8))[1] == '\0')) {
    clearerr_unlocked(in_stream);
LAB_00104917:
    uVar2 = 1;
    if (param_1 != 0) goto LAB_00104988;
  }
  else {
    iVar1 = rpl_fclose();
    if (iVar1 == 0) goto LAB_00104917;
    if (param_1 == 0) {
      piVar3 = __errno_location();
      param_1 = *piVar3;
      goto LAB_00104917;
    }
LAB_00104988:
    uVar2 = quotearg_n_style_colon(0,3,input_filename);
    error(0,param_1,&DAT_0010e7bc,uVar2);
    uVar2 = 0;
  }
  in_stream = (FILE *)0x0;
LAB_0010492b:
  if ((*stdout & 0x20) == 0) {
    return uVar2;
  }
  uVar2 = dcgettext(0,"write error",5);
  error(0,0,uVar2);
  return 0;
}