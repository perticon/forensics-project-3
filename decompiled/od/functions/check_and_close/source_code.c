check_and_close (int in_errno)
{
  bool ok = true;

  if (in_stream != NULL)
    {
      if (!ferror (in_stream))
        in_errno = 0;
      if (STREQ (file_list[-1], "-"))
        clearerr (in_stream);
      else if (fclose (in_stream) != 0 && !in_errno)
        in_errno = errno;
      if (in_errno)
        {
          error (0, in_errno, "%s", quotef (input_filename));
          ok = false;
        }

      in_stream = NULL;
    }

  if (ferror (stdout))
    {
      error (0, 0, _("write error"));
      ok = false;
    }

  return ok;
}