void print_ascii(byte *param_1,byte *param_2,byte *param_3,undefined8 param_4,int param_5,
                int param_6)

{
  byte bVar1;
  int iVar2;
  ushort **ppuVar3;
  undefined *puVar4;
  undefined *puVar5;
  ulong uVar6;
  int iVar7;
  byte *pbVar8;
  long in_FS_OFFSET;
  undefined local_44 [4];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 < param_1) {
    uVar6 = (long)(param_1 + -1) * (long)param_6;
    pbVar8 = param_3;
    iVar7 = param_6;
    do {
      bVar1 = *pbVar8;
      pbVar8 = pbVar8 + 1;
      iVar2 = (int)(uVar6 / (ulong)param_1);
      switch((ulong)bVar1) {
      case 0:
        puVar5 = &DAT_0010d028;
        break;
      default:
        ppuVar3 = __ctype_b_loc();
        puVar4 = &DAT_0010d023;
        puVar5 = local_44;
        if ((*(byte *)((long)*ppuVar3 + (ulong)bVar1 * 2 + 1) & 0x40) != 0) {
          puVar4 = &DAT_0010d020;
        }
        __sprintf_chk(puVar5,1,4,puVar4);
        break;
      case 7:
        puVar5 = &DAT_0010d02b;
        break;
      case 8:
        puVar5 = &DAT_0010d02e;
        break;
      case 9:
        puVar5 = &DAT_0010d03a;
        break;
      case 10:
        puVar5 = &DAT_0010d034;
        break;
      case 0xb:
        puVar5 = &DAT_0010d03d;
        break;
      case 0xc:
        puVar5 = &DAT_0010d031;
        break;
      case 0xd:
        puVar5 = &DAT_0010d037;
      }
      xprintf(&DAT_0010d01c,(iVar7 - iVar2) + param_5,puVar5);
      uVar6 = uVar6 - (long)param_6;
      iVar7 = iVar2;
    } while (param_2 < param_3 + ((long)param_1 - (long)pbVar8));
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}