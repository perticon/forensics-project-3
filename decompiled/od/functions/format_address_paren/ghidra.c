void format_address_paren(undefined8 param_1,byte param_2)

{
  char *pcVar1;
  byte *pbVar2;
  
  pcVar1 = stdout->_IO_write_ptr;
  if (pcVar1 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar1 + 1;
    *pcVar1 = '(';
  }
  else {
    __overflow(stdout,0x28);
  }
  format_address_std(param_1);
  if (param_2 != 0) {
    pbVar2 = (byte *)stdout->_IO_write_ptr;
    if (stdout->_IO_write_end <= pbVar2) {
      __overflow(stdout,(uint)param_2);
      return;
    }
    stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
    *pbVar2 = param_2;
  }
  return;
}