bool parse_old_offset(char * s, int64_t * offset) {
    int64_t v1 = (int64_t)s;
    char v2 = v1;
    if (v2 == 0) {
        // 0x3c01
        return false;
    }
    int64_t v3 = (int64_t)(v2 == 43) + v1;
    char * v4 = (char *)v3;
    if (function_2590() != 0) {
        // 0x3bdd
        return xstrtoumax(v4, NULL, 10, offset, "Bb") == 0;
    }
    int32_t v5 = 8; // 0x3c0d
    if (*v4 == 48) {
        // 0x3c0f
        v5 = 8 * (int32_t)((*(char *)(v3 + 1) & -33) == 88) + 8;
    }
    // 0x3bdd
    return xstrtoumax(v4, NULL, v5, offset, "Bb") == 0;
}