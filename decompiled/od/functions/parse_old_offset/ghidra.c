parse_old_offset(char *param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4)

{
  char *pcVar1;
  ulong uVar2;
  char cVar3;
  
  if (*param_1 != '\0') {
    if (*param_1 == '+') {
      param_1 = param_1 + 1;
    }
    pcVar1 = strchr(param_1,0x2e);
    cVar3 = '\n';
    if ((pcVar1 == (char *)0x0) && (cVar3 = '\b', *param_1 == '0')) {
      cVar3 = ((param_1[1] & 0xdfU) == 0x58) * '\b' + '\b';
    }
    uVar2 = xstrtoumax(param_1,0,cVar3,param_2,&DAT_0010d015);
    return CONCAT88(param_4,uVar2 & 0xffffffffffffff00 | (ulong)((int)uVar2 == 0));
  }
  return ZEXT116(0) << 0x40;
}