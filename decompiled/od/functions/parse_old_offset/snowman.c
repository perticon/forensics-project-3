unsigned char parse_old_offset(void** rdi, void** rsi) {
    uint32_t edx3;
    void** rbp4;
    int64_t rax5;
    int32_t eax6;

    edx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (!*reinterpret_cast<signed char*>(&edx3)) {
        return 0;
    } else {
        rbp4 = rdi;
        if (*reinterpret_cast<signed char*>(&edx3) == 43) {
            ++rbp4;
        }
        rax5 = fun_2590(rbp4, rbp4);
        if (!rax5 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp4) == 48)) {
        }
        eax6 = xstrtoumax(rbp4, rbp4);
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax6 == 0));
    }
}