void format_address_std(ulong param_1,undefined param_2)

{
  bool bVar1;
  byte bVar2;
  uint uVar3;
  char *__s;
  char *__s_00;
  long in_FS_OFFSET;
  char local_22 [18];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  __s = local_22 + 1;
  local_22[2] = 0;
  __s_00 = __s + -(long)address_pad_len;
  if (address_base == 10) {
    do {
      __s = __s + -1;
      *__s = (char)param_1 + (char)(param_1 / 10) * -10 + '0';
      bVar1 = 9 < param_1;
      param_1 = param_1 / 10;
    } while (bVar1);
  }
  else if (address_base == 0x10) {
    do {
      __s = __s + -1;
      uVar3 = (uint)param_1;
      param_1 = param_1 >> 4;
      *__s = "0123456789abcdef"[uVar3 & 0xf];
    } while (param_1 != 0);
  }
  else if (address_base == 8) {
    do {
      __s = __s + -1;
      bVar2 = (byte)param_1;
      param_1 = param_1 >> 3;
      *__s = (bVar2 & 7) + 0x30;
    } while (param_1 != 0);
  }
  local_22[1] = param_2;
  if (__s_00 < __s) {
    __s = (char *)memset(__s_00,0x30,(long)__s - (long)__s_00);
  }
  fputs_unlocked(__s,stdout);
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}