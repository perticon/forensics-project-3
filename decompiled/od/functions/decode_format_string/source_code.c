decode_format_string (char const *s)
{
  char const *s_orig = s;
  assert (s != NULL);

  while (*s != '\0')
    {
      char const *next;

      if (n_specs_allocated <= n_specs)
        spec = X2NREALLOC (spec, &n_specs_allocated);

      if (! decode_one_format (s_orig, s, &next, &spec[n_specs]))
        return false;

      assert (s != next);
      s = next;
      ++n_specs;
    }

  return true;
}