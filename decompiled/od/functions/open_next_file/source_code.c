open_next_file (void)
{
  bool ok = true;

  do
    {
      input_filename = *file_list;
      if (input_filename == NULL)
        return ok;
      ++file_list;

      if (STREQ (input_filename, "-"))
        {
          input_filename = _("standard input");
          in_stream = stdin;
          have_read_stdin = true;
          xset_binary_mode (STDIN_FILENO, O_BINARY);
        }
      else
        {
          in_stream = fopen (input_filename, (O_BINARY ? "rb" : "r"));
          if (in_stream == NULL)
            {
              error (0, errno, "%s", quotef (input_filename));
              ok = false;
            }
        }
    }
  while (in_stream == NULL);

  if (limit_bytes_to_format && !flag_dump_strings)
    setvbuf (in_stream, NULL, _IONBF, 0);

  return ok;
}