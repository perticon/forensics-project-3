undefined8 open_next_file(void)

{
  int *piVar1;
  undefined8 uVar2;
  
  uVar2 = 1;
  do {
    input_filename = *file_list;
    if (input_filename == (char *)0x0) {
      return uVar2;
    }
    file_list = file_list + 1;
    if ((*input_filename == '-') && (input_filename[1] == '\0')) {
      input_filename = (char *)dcgettext(0,"standard input",5);
      have_read_stdin = 1;
      in_stream = stdin;
    }
    else {
      in_stream = fopen(input_filename,"r");
      if (in_stream != (FILE *)0x0) break;
      quotearg_n_style_colon(0,3,input_filename);
      piVar1 = __errno_location();
      uVar2 = 0;
      error(0,*piVar1,&DAT_0010e7bc);
    }
  } while (in_stream == (FILE *)0x0);
  if ((limit_bytes_to_format != '\0') && (flag_dump_strings == '\0')) {
    setvbuf(in_stream,(char *)0x0,2,0);
  }
  return uVar2;
}