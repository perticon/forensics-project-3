uint32_t open_next_file() {
    uint32_t r12d1;
    void*** rax2;
    void** rdi3;
    int1_t zf4;
    void** rax5;
    void** rax6;
    int1_t zf7;
    int1_t zf8;

    r12d1 = 1;
    do {
        rax2 = file_list;
        rdi3 = *rax2;
        input_filename = rdi3;
        if (!rdi3) 
            break;
        zf4 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi3) == 45);
        file_list = rax2 + 8;
        if (!zf4 || *reinterpret_cast<void***>(rdi3 + 1)) {
            rax5 = fun_2740();
            in_stream = rax5;
            if (rax5) 
                goto addr_4870_5;
            quotearg_n_style_colon();
            fun_2450();
            r12d1 = 0;
            fun_2720();
            rax5 = in_stream;
        } else {
            rax6 = fun_2530();
            have_read_stdin = 1;
            input_filename = rax6;
            rax5 = stdin;
            in_stream = rax5;
        }
    } while (!rax5);
    goto addr_4870_5;
    addr_4882_9:
    return r12d1;
    addr_4870_5:
    zf7 = limit_bytes_to_format == 0;
    if (!zf7 && (zf8 = flag_dump_strings == 0, zf8)) {
        fun_2700(rax5);
        goto addr_4882_9;
    }
}