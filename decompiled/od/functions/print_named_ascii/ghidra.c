void print_named_ascii(byte *param_1,byte *param_2,byte *param_3,undefined8 param_4,int param_5,
                      uint param_6)

{
  byte bVar1;
  ulong uVar2;
  byte *pbVar3;
  byte *pbVar4;
  byte *pbVar5;
  int iVar6;
  ulong uVar7;
  long in_FS_OFFSET;
  byte local_42;
  undefined local_41;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_2 < param_1) {
    uVar7 = (ulong)param_6;
    uVar2 = (long)(param_1 + -1) * (long)(int)param_6;
    pbVar3 = param_3;
    do {
      pbVar4 = pbVar3 + 1;
      iVar6 = (int)uVar7;
      uVar7 = uVar2 / (ulong)param_1 & 0xffffffff;
      bVar1 = (byte)(*pbVar3 & 0x7f);
      pbVar5 = &DAT_0010d018;
      if (bVar1 != 0x7f) {
        if (bVar1 < 0x21) {
          pbVar5 = charname + (ulong)(*pbVar3 & 0x7f) * 4;
        }
        else {
          local_41 = 0;
          pbVar5 = &local_42;
          local_42 = bVar1;
        }
      }
      xprintf(&DAT_0010d01c,(iVar6 - (int)(uVar2 / (ulong)param_1)) + param_5,pbVar5);
      uVar2 = uVar2 - (long)(int)param_6;
      pbVar3 = pbVar4;
    } while (param_2 < param_3 + ((long)param_1 - (long)pbVar4));
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}