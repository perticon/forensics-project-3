uint skip(ulong param_1)

{
  FILE *pFVar1;
  int iVar2;
  uint uVar3;
  uint uVar4;
  ulong uVar5;
  long lVar6;
  undefined8 uVar7;
  int *piVar8;
  undefined *unaff_RBX;
  uint uVar9;
  ulong uVar10;
  ulong unaff_R14;
  undefined *unaff_R15;
  long in_FS_OFFSET;
  stat sStack8408;
  undefined local_2048 [8200];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    uVar9 = 1;
LAB_00104ac9:
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return uVar9;
  }
  uVar9 = 1;
  if (in_stream == (FILE *)0x0) goto LAB_00104b5a;
  unaff_R15 = &DAT_0010e7bc;
  unaff_RBX = local_2048;
LAB_00104a30:
  pFVar1 = in_stream;
  iVar2 = fileno(in_stream);
  iVar2 = fstat(iVar2,&sStack8408);
  if (iVar2 != 0) goto LAB_00104b80;
  if ((sStack8408.st_mode & 0xd000) == 0x8000) {
    lVar6 = sStack8408.st_blksize;
    if (0x1fffffffffffffff < sStack8408.st_blksize - 1U) {
      lVar6 = 0x200;
    }
    if (sStack8408.st_size <= lVar6) goto LAB_00104a72;
    if ((ulong)sStack8408.st_size < param_1) {
      param_1 = param_1 - sStack8408.st_size;
      goto LAB_00104b38;
    }
    iVar2 = rpl_fseeko(pFVar1,param_1,1);
    if (iVar2 != 0) {
      uVar9 = 0;
    }
  }
  else {
    iVar2 = rpl_fseeko(pFVar1,param_1,1);
    if (iVar2 != 0) {
LAB_00104a72:
      uVar10 = 0x2000;
      do {
        if (param_1 < uVar10) {
          uVar10 = param_1;
        }
        uVar5 = __fread_unlocked_chk(unaff_RBX,0x2000,1,uVar10,in_stream);
        param_1 = param_1 - uVar5;
        if (uVar10 != uVar5) {
          if ((in_stream->_flags & 0x20U) != 0) {
            uVar9 = 0;
            break;
          }
          if ((in_stream->_flags & 0x10U) != 0) goto LAB_00104bb8;
        }
        if (param_1 == 0) break;
      } while( true );
    }
  }
  goto LAB_00104ac9;
LAB_00104bb8:
  if (param_1 == 0) goto LAB_00104ac9;
LAB_00104b38:
  while( true ) {
    uVar3 = check_and_close(0);
    uVar4 = open_next_file();
    uVar9 = uVar9 & uVar3 & uVar4;
    unaff_R14 = param_1;
    if (in_stream != (FILE *)0x0) break;
LAB_00104b5a:
    uVar7 = dcgettext(0,"cannot skip past end of combined input",5);
    error(1,0,uVar7);
    param_1 = unaff_R14;
LAB_00104b80:
    uVar7 = quotearg_n_style_colon(0,3,input_filename);
    piVar8 = __errno_location();
    uVar9 = 0;
    error(0,*piVar8,unaff_R15,uVar7);
  }
  goto LAB_00104a30;
}