undefined  [16] main(int param_1,undefined8 *param_2,ulong param_3,ulong param_4)

{
  char *__s1;
  int iVar1;
  
  if (param_1 != 2) {
    return ZEXT816(param_3) << 0x40;
  }
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  __s1 = (char *)param_2[1];
  iVar1 = strcmp(__s1,"--help");
  if (iVar1 != 0) {
    iVar1 = strcmp(__s1,"--version");
    if (iVar1 == 0) {
      version_etc(stdout,&DAT_00106004,"GNU coreutils",Version,"Jim Meyering",0);
    }
    return ZEXT816(param_4) << 0x40;
  }
                    /* WARNING: Subroutine does not return */
  usage(0);
}