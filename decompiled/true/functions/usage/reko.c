void usage(word32 edi)
{
	ptr64 fp;
	fn00000000000024C0(fn0000000000002370(0x05, "Usage: %s [ignored command line arguments]\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002370(0x05, "Exit with a status code indicating success.", null);
	fn00000000000024C0(0x6009, 0x01);
	fn0000000000002400(stdout, fn0000000000002370(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002400(stdout, fn0000000000002370(0x05, "      --version     output version information and exit\n", null));
	fn00000000000024C0(fn0000000000002370(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_512 * rbx_141 = fp - 0xB8 + 16;
	do
	{
		char * rsi_143 = rbx_141->qw0000;
		++rbx_141;
	} while (rsi_143 != null && fn0000000000002420(rsi_143, "true") != 0x00);
	ptr64 r13_156 = rbx_141->qw0008;
	if (r13_156 != 0x00)
	{
		fn00000000000024C0(fn0000000000002370(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_185 = fn00000000000024B0(null, 0x05);
		if (rax_185 == 0x00 || fn0000000000002300(0x03, "en_", rax_185) == 0x00)
			goto l000000000000295E;
	}
	else
	{
		fn00000000000024C0(fn0000000000002370(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_270 = fn00000000000024B0(null, 0x05);
		if (rax_270 == 0x00 || fn0000000000002300(0x03, "en_", rax_270) == 0x00)
		{
			fn00000000000024C0(fn0000000000002370(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000299B:
			fn00000000000024C0(fn0000000000002370(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
			fn0000000000002500(edi);
		}
		r13_156 = 0x6004;
	}
	fn0000000000002400(stdout, fn0000000000002370(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000295E:
	fn00000000000024C0(fn0000000000002370(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000299B;
}