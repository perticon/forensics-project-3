uint64_t realpath_canon (int64_t arg2) {
    rsi = arg2;
    ebx = esi;
    rax = canonicalize_filename_mode ();
    r12 = rax;
    if (*(obj.logical) != 0) {
        if (rax == 0) {
            goto label_0;
        }
        ebx &= 0xfffffffb;
        rdi = rax;
        esi = ebx;
        rax = canonicalize_filename_mode ();
        rbx = rax;
        free (r12);
        r12 = rbx;
    }
label_0:
    rax = r12;
    return rax;
}