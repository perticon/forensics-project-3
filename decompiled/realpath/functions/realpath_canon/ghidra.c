void * realpath_canon(undefined8 param_1,uint param_2)

{
  void *__ptr;
  void *pvVar1;
  
  __ptr = (void *)canonicalize_filename_mode();
  pvVar1 = __ptr;
  if ((logical != '\0') && (__ptr != (void *)0x0)) {
    pvVar1 = (void *)canonicalize_filename_mode(__ptr,param_2 & 0xfffffffb);
    free(__ptr);
  }
  return pvVar1;
}