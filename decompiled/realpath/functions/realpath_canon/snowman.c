void** realpath_canon(void** rdi, int32_t esi, void** rdx, void** rcx, void* r8) {
    void** rax6;
    int1_t zf7;
    void** r12_8;
    void** rax9;

    rax6 = canonicalize_filename_mode(rdi);
    zf7 = logical == 0;
    r12_8 = rax6;
    if (!zf7 && rax6) {
        rax9 = canonicalize_filename_mode(rax6, rax6);
        fun_2370(r12_8, r12_8);
        r12_8 = rax9;
    }
    return r12_8;
}