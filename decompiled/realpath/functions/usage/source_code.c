usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("Usage: %s [OPTION]... FILE...\n"), program_name);
      fputs (_("\
Print the resolved absolute file name;\n\
all but the last component must exist\n\
\n\
"), stdout);
      fputs (_("\
  -e, --canonicalize-existing  all components of the path must exist\n\
  -m, --canonicalize-missing   no path components need exist or be a directory\
\n\
  -L, --logical                resolve '..' components before symlinks\n\
  -P, --physical               resolve symlinks as encountered (default)\n\
  -q, --quiet                  suppress most error messages\n\
      --relative-to=DIR        print the resolved path relative to DIR\n\
      --relative-base=DIR      print absolute paths unless paths below DIR\n\
  -s, --strip, --no-symlinks   don't expand symlinks\n\
  -z, --zero                   end each output line with NUL, not newline\n\
"), stdout);
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}