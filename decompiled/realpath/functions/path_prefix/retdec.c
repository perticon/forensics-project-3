bool path_prefix(char * prefix, char * path) {
    int64_t v1 = (int64_t)prefix;
    int64_t v2 = v1 + 1; // 0x2d10
    char v3 = *(char *)v2; // 0x2d10
    int64_t v4 = (int64_t)path + 1; // 0x2d18
    if (v3 == 0) {
        // 0x2d60
        return *(char *)v4 != 47;
    }
    if (v3 == 47) {
        // 0x2d24
        if (*(char *)(v1 + 2) == 0) {
            // 0x2d2a
            return *(char *)v4 == 47;
        }
    }
    char v5 = v3; // 0x2d3d
    int64_t v6 = v2; // 0x2d3d
    int64_t v7 = v4; // 0x2d45
    char v8 = *(char *)v7; // 0x2d4d
    while (v8 != 0 && v8 == v5) {
        // 0x2d3d
        v6++;
        v5 = *(char *)v6;
        v7++;
        if (v5 == 0) {
            char v9 = *(char *)v7; // 0x2d70
            return v9 == 0 | v9 == 47;
        }
        v8 = *(char *)v7;
    }
    // 0x2d55
    return false;
}