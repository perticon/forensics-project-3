bool path_prefix(long param_1,long param_2)

{
  char cVar1;
  long lVar2;
  char *pcVar3;
  
  cVar1 = *(char *)(param_1 + 1);
  lVar2 = param_1 + 1;
  pcVar3 = (char *)(param_2 + 1);
  if (cVar1 == '\0') {
    return *(char *)(param_2 + 1) != '/';
  }
  if ((cVar1 == '/') && (*(char *)(param_1 + 2) == '\0')) {
    return *(char *)(param_2 + 1) == '/';
  }
  while ((*pcVar3 != '\0' && (*pcVar3 == cVar1))) {
    cVar1 = *(char *)(lVar2 + 1);
    lVar2 = lVar2 + 1;
    pcVar3 = pcVar3 + 1;
    if (cVar1 == '\0') {
      return *pcVar3 == '/' || *pcVar3 == '\0';
    }
  }
  return false;
}