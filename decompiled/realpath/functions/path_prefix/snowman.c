unsigned char path_prefix(void** rdi, void** rsi, void** rdx, void** rcx, void* r8) {
    uint32_t eax6;
    void** rcx7;
    void** rdx8;
    uint32_t esi9;
    uint32_t edx10;
    uint32_t eax11;

    eax6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    rcx7 = rdi + 1;
    rdx8 = rsi + 1;
    if (!*reinterpret_cast<unsigned char*>(&eax6)) {
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 1) == 47)));
    }
    if (*reinterpret_cast<unsigned char*>(&eax6) == 47 && !*reinterpret_cast<unsigned char*>(rdi + 2)) {
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi + 1) == 47));
    }
    do {
        esi9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx8));
        if (!*reinterpret_cast<unsigned char*>(&esi9)) 
            break;
        if (*reinterpret_cast<unsigned char*>(&esi9) != *reinterpret_cast<unsigned char*>(&eax6)) 
            break;
        eax6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx7 + 1));
        ++rcx7;
        ++rdx8;
    } while (*reinterpret_cast<unsigned char*>(&eax6));
    goto addr_2d70_8;
    return 0;
    addr_2d70_8:
    edx10 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx8));
    *reinterpret_cast<unsigned char*>(&eax6) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edx10) == 47);
    *reinterpret_cast<unsigned char*>(&edx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edx10) == 0);
    eax11 = eax6 | edx10;
    return *reinterpret_cast<unsigned char*>(&eax11);
}