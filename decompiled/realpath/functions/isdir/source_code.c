isdir (char const *path)
{
  struct stat sb;
  if (stat (path, &sb) != 0)
    die (EXIT_FAILURE, errno, _("cannot stat %s"), quoteaf (path));
  return S_ISDIR (sb.st_mode);
}