unsigned char isdir(void** rdi, void*** rsi, void** rdx, void** rcx, void* r8) {
    void** rax6;
    int32_t eax7;
    void* rdx8;
    uint32_t v9;

    rax6 = g28;
    eax7 = fun_2550(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0xa0);
    if (eax7) {
        quotearg_style(4, rdi);
        fun_2430();
        fun_2390();
        fun_2610();
    } else {
        rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx8) {
            return static_cast<unsigned char>(reinterpret_cast<uint1_t>((v9 & 0xf000) == 0x4000));
        }
    }
    fun_2460();
}