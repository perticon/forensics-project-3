int64_t isdir (int64_t arg1) {
    int64_t var_18h;
    int64_t var_98h;
    rdi = arg1;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    rsi = rsp;
    eax = stat ();
    if (eax == 0) {
        eax = *((rsp + 0x18));
        eax &= 0xf000;
        al = (eax == 0x4000) ? 1 : 0;
        rdx = *((rsp + 0x98));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot stat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_0:
    return stack_chk_fail ();
}