uint isdir(char *param_1)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  long in_FS_OFFSET;
  stat sStack184;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = stat(param_1,&sStack184);
  if (iVar1 == 0) {
    if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
      return sStack184.st_mode & 0xf000 | (uint)((sStack184.st_mode & 0xf000) == 0x4000);
    }
  }
  else {
    uVar2 = quotearg_style(4,param_1);
    uVar3 = dcgettext(0,"cannot stat %s",5);
    piVar4 = __errno_location();
    error(1,*piVar4,uVar3,uVar2);
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}