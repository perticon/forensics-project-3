process_path (char const *fname, int can_mode)
{
  char *can_fname = realpath_canon (fname, can_mode);
  if (!can_fname)
    {
      if (verbose)
        error (0, errno, "%s", quotef (fname));
      return false;
    }

  if (!can_relative_to
      || (can_relative_base && !path_prefix (can_relative_base, can_fname))
      || (can_relative_to && !relpath (can_fname, can_relative_to, NULL, 0)))
    fputs (can_fname, stdout);

  putchar (use_nuls ? '\0' : '\n');

  free (can_fname);

  return true;
}