byte main(int param_1,undefined8 *param_2)

{
  long lVar1;
  char cVar2;
  int iVar3;
  char *pcVar4;
  char *__s;
  int *piVar5;
  uint *puVar6;
  undefined8 uVar7;
  uint uVar8;
  uint uVar9;
  uint *__ptr;
  uint *puVar10;
  undefined auVar11 [16];
  undefined8 local_48;
  long local_40 [2];
  
  puVar10 = &switchD_001027e5::switchdataD_001095a4;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  uVar8 = 1;
  textdomain("coreutils");
  atexit(close_stdout);
  local_48 = 0;
  local_40[0] = 0;
  while (iVar3 = getopt_long(param_1,param_2,"eLmPqsz",longopts,0), iVar3 != -1) {
    if (0x81 < iVar3) goto switchD_001027e5_caseD_4d;
    if (iVar3 < 0x4c) {
      if (iVar3 == -0x83) {
        version_etc(stdout,"realpath","GNU coreutils",Version,"Padraig Brady",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 != -0x82) goto switchD_001027e5_caseD_4d;
      usage(0);
      break;
    }
    switch(iVar3) {
    case 0x4c:
      logical = '\x01';
      uVar8 = uVar8 | 4;
      break;
    default:
      goto switchD_001027e5_caseD_4d;
    case 0x50:
      logical = '\0';
      uVar8 = uVar8 & 0xfffffffb;
      break;
    case 0x65:
      uVar8 = uVar8 & 0xfffffffc;
      break;
    case 0x6d:
      uVar8 = uVar8 & 0xfffffffc | 2;
      break;
    case 0x71:
      verbose = 0;
      break;
    case 0x73:
      logical = '\0';
      uVar8 = uVar8 | 4;
      break;
    case 0x7a:
      use_nuls = '\x01';
      break;
    case 0x80:
      local_40[0] = optarg;
      break;
    case 0x81:
      local_48 = optarg;
    }
  }
  if (param_1 <= optind) {
    uVar7 = dcgettext(0,"missing operand",5);
    error(0,0,uVar7);
switchD_001027e5_caseD_4d:
    puVar6 = (uint *)usage(1);
LAB_00102b27:
    cVar2 = isdir(puVar6);
    if (cVar2 != '\0') goto LAB_00102922;
    uVar7 = quotearg_n_style_colon(0,3,local_40[0]);
    __ptr = (uint *)0x1;
    error(1,0x14,"%s",uVar7);
LAB_00102b63:
    free(__ptr);
    can_relative_to = (uint *)0x0;
    puVar6 = puVar10;
LAB_0010297a:
    can_relative_base = puVar6;
    local_48 = CONCAT71(local_48._1_7_,1);
    do {
      if (param_1 <= optind) {
        return (byte)local_48 ^ 1;
      }
      uVar7 = param_2[optind];
      pcVar4 = (char *)canonicalize_filename_mode(uVar7,uVar8);
      __s = pcVar4;
      if (logical == '\0') {
LAB_00102a66:
        if (__s == (char *)0x0) goto LAB_00102a6f;
        if ((can_relative_to == (uint *)0x0) ||
           (((puVar10 = can_relative_to, can_relative_base != (uint *)0x0 &&
             (cVar2 = path_prefix(can_relative_base,__s), cVar2 == '\0')) ||
            (cVar2 = relpath(__s,puVar10,0,0), cVar2 == '\0')))) {
          fputs_unlocked(__s,stdout);
        }
        pcVar4 = stdout->_IO_write_ptr;
        uVar9 = -(uint)(use_nuls == '\0') & 10;
        if (pcVar4 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar4 + 1;
          *pcVar4 = (char)uVar9;
        }
        else {
          __overflow(stdout,uVar9);
        }
        free(__s);
      }
      else {
        if (pcVar4 != (char *)0x0) {
          __s = (char *)canonicalize_filename_mode(pcVar4,uVar8 & 0xfffffffb);
          free(pcVar4);
          goto LAB_00102a66;
        }
LAB_00102a6f:
        local_48 = local_48 & 0xffffffffffffff00 | (ulong)verbose;
        if (verbose != 0) {
          quotearg_n_style_colon(0,3,uVar7);
          piVar5 = __errno_location();
          error(0,*piVar5,"%s");
          local_48 = local_48 & 0xffffffffffffff00;
        }
      }
      optind = optind + 1;
    } while( true );
  }
  puVar10 = (uint *)(ulong)(uVar8 & 3);
  if (local_48 == 0) {
    if (local_40[0] != 0) goto LAB_001028fd;
  }
  else {
    if (local_40[0] == 0) {
      local_40[0] = local_48;
    }
LAB_001028fd:
    puVar6 = (uint *)realpath_canon(local_40[0],uVar8);
    can_relative_to = puVar6;
    if (puVar6 == (uint *)0x0) goto LAB_00102bea;
    if ((uVar8 & 3) == 0) goto LAB_00102b27;
  }
LAB_00102922:
  if (local_48 == local_40[0]) {
    can_relative_base = can_relative_to;
    puVar6 = can_relative_base;
    goto LAB_0010297a;
  }
  puVar6 = can_relative_base;
  if (local_48 == 0) goto LAB_0010297a;
  puVar6 = (uint *)realpath_canon(local_48,uVar8);
  if (puVar6 != (uint *)0x0) {
    if (((int)puVar10 != 0) || (cVar2 = isdir(puVar6), cVar2 != '\0')) {
      puVar10 = can_relative_to;
      __ptr = puVar6;
      cVar2 = path_prefix(puVar6,can_relative_to);
      if (cVar2 != '\0') goto LAB_0010297a;
      goto LAB_00102b63;
    }
    uVar7 = quotearg_n_style_colon(0,3,local_48);
    error(1,0x14,"%s",uVar7);
  }
  uVar7 = quotearg_n_style_colon(0,3,local_48);
  piVar5 = __errno_location();
  error(1,*piVar5,"%s",uVar7);
LAB_00102bea:
  uVar7 = quotearg_n_style_colon(0,3,local_40[0]);
  piVar5 = __errno_location();
  auVar11 = error(1,*piVar5,"%s",uVar7);
  lVar1 = local_48;
  local_48 = SUB168(auVar11,0);
  (*(code *)PTR___libc_start_main_0010cfd8)
            (main,lVar1,local_40,0,0,SUB168(auVar11 >> 0x40,0),&local_48);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}