#include <stdint.h>

/* /tmp/tmp79u99nvo @ 0x2ee0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp79u99nvo @ 0x4e40 */
 
int64_t dbg_AD_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool AD_compare( const * x, const * y); */
    rdx = *((rsi + 8));
    eax = 0;
    if (*((rdi + 8)) != rdx) {
        return eax;
    }
    rax = *(rsi);
    al = (*(rdi) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4e70 */
 
int64_t dbg_AD_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t AD_hash( const * x,size_t table_size); */
    rax = *((rdi + 8));
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4e90 */
 
int64_t dbg_dev_type_hash (size_t table_size,  const * x) {
    rsi = table_size;
    rdi = x;
    /* size_t dev_type_hash( const * x,size_t table_size); */
    rax = *(rdi);
    edx = 0;
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4ea0 */
 
int64_t dbg_dev_type_compare ( const * x,  const * y) {
    rdi = x;
    rsi = y;
    /* _Bool dev_type_compare( const * x, const * y); */
    rax = *(rsi);
    al = (*(rdi) == rax) ? 1 : 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4eb0 */
 
int64_t dbg_fts_compare_ino (_ftsent const ** a, _ftsent const ** b) {
    rdi = a;
    rsi = b;
    /* int fts_compare_ino(_ftsent const ** a,_ftsent const ** b); */
    *(rax) += al;
    rdx = *(rdi);
    rax = *(rsi);
    rax = *((rax + 0x78));
    al = (*((rdx + 0x78)) > rax) ? 1 : 0;
    eax = (int32_t) al;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4ed0 */
 
uint64_t dbg_fts_stat (int64_t arg_8h, int64_t arg_88h, int64_t arg_90h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* short unsigned int fts_stat(FTS * sp,FTSENT * p,_Bool follow); */
    r12 = rdi;
    rbp = rsi + 0x70;
    rbx = rsi;
    eax = *((rdi + 0x48));
    if ((al & 2) == 0) {
        if ((al & 1) == 0) {
            goto label_4;
        }
        if (*((rsi + 0x58)) != 0) {
            goto label_4;
        }
    }
label_0:
    rsi = *((rbx + 0x30));
    edi = *((r12 + 0x2c));
    ecx = 0;
    rdx = rbp;
    eax = fstatat ();
    if (eax >= 0) {
label_1:
        eax = *((rbx + 0x88));
        eax &= 0xf000;
        if (eax == 0x4000) {
            goto label_5;
        }
        if (eax == sym.set_custom_quoting) {
            goto label_6;
        }
        al = (eax == 0x8000) ? 1 : 0;
        eax = (int32_t) al;
        eax = rax * 5;
label_3:
        return eax;
    }
    rax = errno_location ();
    r13 = rax;
    eax = *(rax);
    if (eax == 2) {
        goto label_7;
    }
label_2:
    rdi = rbp + 8;
    *((rbx + 0x40)) = eax;
    eax = 0;
    rdi &= 0xfffffffffffffff8;
    *((rbx + 0x70)) = 0;
    *((rbp + 0x88)) = 0;
    rbp -= rdi;
    ecx = rbp + 0x90;
    ecx >>= 3;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    eax = 0xa;
    return rax;
label_4:
    if (dl != 0) {
        goto label_0;
    }
    rsi = *((rbx + 0x30));
    edi = *((r12 + 0x2c));
    ecx = 0x100;
    rdx = rbp;
    eax = fstatat ();
    if (eax >= 0) {
        goto label_1;
    }
    rax = errno_location ();
    eax = *(rax);
    goto label_2;
label_5:
    eax = 1;
    if (*((rbx + 0x100)) != 0x2e) {
        goto label_3;
    }
    if (*((rbx + 0x101)) == 0) {
        goto label_8;
    }
    edx = *((rbx + 0x100));
    edx &= 0xffff00;
    if (edx != 0x2e00) {
        goto label_3;
    }
label_8:
    eax -= eax;
    eax &= 0xfffffffc;
    eax += 5;
    goto label_3;
label_7:
    rsi = *((rbx + 0x30));
    edi = *((r12 + 0x2c));
    ecx = 0x100;
    rdx = rbp;
    eax = fstatat ();
    if (eax >= 0) {
        *(r13) = 0;
        eax = 0xd;
        return rax;
    }
    eax = *(r13);
    goto label_2;
label_6:
    eax = 0xc;
    goto label_3;
}

/* /tmp/tmp79u99nvo @ 0x5080 */
 
int64_t dbg_fts_sort (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* FTSENT * fts_sort(FTS * sp,FTSENT * head,size_t nitems); */
    r12 = rdi;
    rbx = rsi;
    r13 = *((rdi + 0x40));
    rdi = *((rdi + 0x10));
    if (*((r12 + 0x38)) < rdx) {
        rsi = rdx + 0x28;
        r8 = rdi;
        rax = rsi;
        *((r12 + 0x38)) = rsi;
        rax >>= 0x3d;
        if (rax != 0) {
            goto label_0;
        }
        rsi <<= 3;
        rax = realloc (rdi, rsi);
        rdi = rax;
        if (rax == 0) {
            goto label_1;
        }
        *((r12 + 0x10)) = rax;
    }
    rdx = rdi;
    if (rbx == 0) {
        goto label_2;
    }
    do {
        *(rdx) = rbx;
        rbx = *((rbx + 0x10));
        rdx += 8;
    } while (rbx != 0);
label_2:
    rcx = r13;
    edx = 8;
    rsi = rbp;
    qsort ();
    r8 = *((r12 + 0x10));
    rcx = rbp;
    rax = *(r8);
    rdx = r8;
    rsi = rax;
    rcx--;
    if (rcx != 0) {
        goto label_3;
    }
    goto label_4;
    do {
        rsi = *(rdx);
label_3:
        rdi = *((rdx + 8));
        rdx += 8;
        *((rsi + 0x10)) = rdi;
        rcx--;
    } while (rcx != 0);
    rdx = *((r8 + rbp*8 - 8));
    do {
        *((rdx + 0x10)) = 0;
        return rax;
label_1:
        r8 = *((r12 + 0x10));
label_0:
        rdi = *((r12 + 0x10));
        fcn_00002450 ();
        rax = rbx;
        *((r12 + 0x10)) = 0;
        *((r12 + 0x38)) = 0;
        return rax;
label_4:
        rdx = rax;
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0x5190 */
 
uint64_t dbg_fts_alloc (int64_t arg_20h, int64_t arg1, int64_t arg2, size_t size) {
    rdi = arg1;
    rsi = arg2;
    rdx = size;
    /* FTSENT * fts_alloc(FTS * sp,char const * name,size_t namelen); */
    r13 = rsi;
    rdi &= 0xfffffffffffffff8;
    rbx = rdx;
    rax = malloc (rdx + 0x108);
    r12 = rax;
    if (rax != 0) {
        memcpy (rax + 0x100, r13, rbx);
        rax = *((rbp + 0x20));
        *((r12 + rbx + 0x100)) = 0;
        *((r12 + 0x60)) = rbx;
        *((r12 + 0x50)) = rbp;
        *((r12 + 0x38)) = rax;
        *((r12 + 0x40)) = 0;
        *((r12 + 0x18)) = 0;
        *((r12 + 0x6a)) = 0x30000;
        *((r12 + 0x20)) = 0;
        *((r12 + 0x28)) = 0;
    }
    rax = r12;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2700 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp79u99nvo @ 0x26d0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp79u99nvo @ 0x5230 */
 
int64_t filesystem_type (int64_t arg1, int64_t arg2) {
    int64_t var_10h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    r12 = *((rdi + 0x50));
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rbp = *((r12 + 0x50));
    if ((*((r12 + 0x49)) & 2) == 0) {
        goto label_1;
    }
    rbx = rdi;
    r13d = esi;
    while (rax != 0) {
        rax = *((rbx + 0x70));
        rsi = rsp;
        rdi = rbp;
        *(rsp) = rax;
        rax = hash_lookup ();
        if (rax == 0) {
            goto label_2;
        }
        rax = *((rax + 8));
        goto label_0;
        rax = hash_initialize (0xd, 0, dbg.dev_type_hash, dbg.dev_type_compare, *(reloc.free));
        *((r12 + 0x50)) = rax;
    }
label_2:
    if (r13d >= 0) {
        rsi = rsp + 0x10;
        edi = r13d;
        eax = fstatfs ();
        if (eax == 0) {
            goto label_3;
        }
    }
label_1:
    eax = 0;
label_0:
    rdx = *((rsp + 0x88));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_4;
    }
    return rax;
label_3:
    r12 = *((rsp + 0x10));
    if (rbp == 0) {
        goto label_5;
    }
    rax = malloc (0x10);
    r13 = rax;
    if (rax == 0) {
        goto label_6;
    }
    rax = *((rbx + 0x70));
    *((r13 + 8)) = r12;
    rsi = r13;
    *(r13) = rax;
    rax = hash_insert (rbp);
    if (rax == 0) {
        goto label_7;
    }
    if (r13 != rax) {
        void (*0x2890)() ();
    }
    do {
label_6:
        r12 = *((rsp + 0x10));
label_5:
        rax = *((rsp + 0x10));
        goto label_0;
label_7:
        rdi = r13;
        fcn_00002450 ();
    } while (1);
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x2890 */
 
void filesystem_type_cold (void) {
    /* [16] -r-x section size 39010 named .text */
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x5360 */
 
uint32_t cwd_advance_fd (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rdi;
    esi = *((rdi + 0x2c));
    if (esi != ebp) {
        goto label_0;
    }
    if (esi != 0xffffff9c) {
        void (*0x2895)() ();
    }
label_0:
    if (dl != 0) {
        goto label_1;
    }
    if ((*((rbx + 0x48)) & 4) != 0) {
        goto label_2;
    }
    if (esi >= 0) {
        goto label_3;
    }
    do {
label_2:
        *((rbx + 0x2c)) = ebp;
        return;
label_1:
        rdi = rbx + 0x60;
        eax = i_ring_push ();
        edi = eax;
    } while (eax < 0);
    close (rdi);
    do {
        *((rbx + 0x2c)) = ebp;
        return eax;
label_3:
        close (esi);
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0x2895 */
 
void cwd_advance_fd_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x53d0 */
 
uint64_t fts_palloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rdi;
    rsi += 0x100;
    rdi = *((rdi + 0x20));
    rsi += *((rbx + 0x30));
    if (rsi >= 0) {
        *((rbx + 0x30)) = rsi;
        rax = realloc (rdi, rsi);
        if (rax == 0) {
            goto label_0;
        }
        *((rbx + 0x20)) = rax;
        eax = 1;
        return rax;
    }
    fcn_00002450 ();
    *((rbx + 0x20)) = 0;
    errno_location ();
    *(rax) = 0x24;
    eax = 0;
    return rax;
label_0:
    rdi = *((rbx + 0x20));
    eax = fcn_00002450 ();
    *((rbx + 0x20)) = 0;
    eax = 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2750 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp79u99nvo @ 0x2450 */
 
void fcn_00002450 (void) {
    /* [14] -r-x section size 32 named .plt.got */
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp79u99nvo @ 0x2480 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp79u99nvo @ 0x5440 */
 
uint64_t setup_dir (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    if ((*((rdi + 0x48)) & 0x102) != 0) {
        rax = hash_initialize (0x1f, 0, dbg.AD_hash, dbg.AD_compare, *(reloc.free));
        *((rbx + 0x58)) = rax;
        al = (rax != 0) ? 1 : 0;
        return rax;
    }
    rax = malloc (0x20);
    *((rbx + 0x58)) = rax;
    rdi = rax;
    if (rax != 0) {
        cycle_check_init ();
        eax = 1;
        return rax;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x7da0 */
 
uint64_t dbg_hash_initialize (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* Hash_table * hash_initialize(size_t candidate,Hash_tuning const * tuning,Hash_hasher hasher,Hash_comparator comparator,Hash_data_freer data_freer); */
    rax = dbg_raw_hasher;
    r15 = rsi;
    r14 = r8;
    r13 = rdi;
    edi = 0x50;
    rbx = rcx;
    if (rdx == 0) {
    }
    rax = dbg_raw_comparator;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = malloc (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = obj_default_tuning;
    rdi = r12;
    if (r15 == 0) {
        r15 = rax;
    }
    *((r12 + 0x28)) = r15;
    al = check_tuning ();
    if (al == 0) {
        goto label_1;
    }
    esi = *((r15 + 0x10));
    xmm0 = *((r15 + 8));
    rdi = r13;
    rax = compute_bucket_size_isra_0 ();
    *((r12 + 0x10)) = rax;
    r13 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = calloc (rax, 0x10);
    *(r12) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r13 <<= 4;
    *((r12 + 0x30)) = rbp;
    rax += r13;
    *((r12 + 0x38)) = rbx;
    *((r12 + 8)) = rax;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x40)) = r14;
    *((r12 + 0x48)) = 0;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rdi = r12;
        r12d = 0;
        fcn_00002450 ();
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0x7450 */
 
int64_t dbg_check_tuning (Hash_table * table) {
    rdi = table;
    __asm ("loope 0x7453");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("loope 0x746b");
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) &= dh;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) != 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    ah += bh;
    __asm ("insb byte [rdi], dx");
    *(rax) += al;
    if (*(rax) >= 0) {
        __asm ("addss xmm1, dword [0x0000e174]");
        xmm2 = *((rax + 4));
        __asm ("comiss xmm2, xmm1");
        if (*(rax) <= 0) {
            goto label_0;
        }
        xmm3 = *(0x0000e180);
        __asm ("comiss xmm3, xmm2");
        if (*(rax) < 0) {
            goto label_0;
        }
        __asm ("comiss xmm0, xmm1");
        eax = 1;
        if (*(rax) > 0) {
            goto label_1;
        }
    }
label_0:
    *((rdi + 0x28)) = rdx;
    eax = 0;
    return rax;
    eax = 1;
label_1:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x75f0 */
 
int64_t compute_bucket_size_isra_0 (uint32_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    if (sil == 0) {
        if (rdi < 0) {
            goto label_5;
        }
        xmm1 = 0;
        __asm ("cvtsi2ss xmm1, rdi");
label_2:
        __asm ("divss xmm1, xmm0");
        r8d = 0;
        __asm ("comiss xmm1, dword [0x0000e184]");
        if (rdi >= 0) {
            goto label_6;
        }
        __asm ("comiss xmm1, dword [0x0000e188]");
        if (rdi < 0) {
            goto label_7;
        }
        __asm ("subss xmm1, dword [0x0000e188]");
        __asm ("cvttss2si rdi, xmm1");
        __asm ("btc rdi, 0x3f");
    }
label_4:
    r9 = 0xaaaaaaaaaaaaaaab;
    eax = 0xa;
    if (rdi >= rax) {
        rax = rdi;
    }
    r8 = rax;
    r8 |= 1;
    if (r8 == -1) {
        goto label_1;
    }
label_0:
    rax = r8;
    rdx:rax = rax * r9;
    rax = rdx;
    rdx &= 0xfffffffffffffffe;
    rax >>= 1;
    rdx += rax;
    rax = r8;
    rax -= rdx;
    if (r8 <= 9) {
        goto label_8;
    }
    if (rax == 0) {
        goto label_9;
    }
    edi = 0x10;
    esi = 9;
    ecx = 3;
    while (r8 > rsi) {
        rdi += 8;
        if (rdx == 0) {
            goto label_9;
        }
        rcx += 2;
        rax = r8;
        edx = 0;
        rsi += rdi;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    }
label_3:
    rax = r8;
    edx = 0;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rdx != 0) {
        goto label_10;
    }
label_9:
    r8 += 2;
    if (r8 != -1) {
        goto label_0;
    }
    do {
label_1:
        r8d = 0;
        rax = r8;
        return rax;
label_10:
        rax = r8;
        rax >>= 0x3d;
        al = (rax != 0) ? 1 : 0;
        eax = (int32_t) al;
    } while (((r8 >> 0x3c) & 1) < 0);
    if (rax != 0) {
        goto label_1;
    }
label_6:
    rax = r8;
    return rax;
label_5:
    rax = rdi;
    edi &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rdi;
    __asm ("cvtsi2ss xmm1, rax");
    __asm ("addss xmm1, xmm1");
    goto label_2;
label_8:
    ecx = 3;
    goto label_3;
label_7:
    __asm ("cvttss2si rdi, xmm1");
    goto label_4;
}

/* /tmp/tmp79u99nvo @ 0x2670 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp79u99nvo @ 0xba80 */
 
void dbg_cycle_check_init (cycle_check_state * state) {
    rdi = state;
    /* void cycle_check_init(cycle_check_state * state); */
    *((rdi + 0x10)) = 0;
    *((rdi + 0x18)) = 0x95f616;
}

/* /tmp/tmp79u99nvo @ 0x54b0 */
 
uint64_t enter_dir (int64_t arg_10h, void ** arg_58h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    if ((*((rdi + 0x48)) & 0x102) == 0) {
        goto label_1;
    }
    rax = malloc (0x18);
    r12 = rax;
    if (rax == 0) {
        goto label_2;
    }
    rax = *((rbx + 0x70));
    rsi = r12;
    *((r12 + 0x10)) = rbx;
    *(r12) = rax;
    rax = *((rbx + 0x78));
    *((r12 + 8)) = rax;
    rax = hash_insert (*((rbp + 0x58)));
    if (r12 == rax) {
        goto label_3;
    }
    rdi = r12;
    fcn_00002450 ();
    if (rbp == 0) {
        goto label_2;
    }
    rax = *((rbp + 0x10));
    ecx = 2;
    *((rbx + 0x68)) = cx;
    *(rbx) = rax;
    do {
label_3:
        eax = 1;
label_0:
        return rax;
label_1:
        al = cycle_check (*((rdi + 0x58)), rsi + 0x70);
    } while (al == 0);
    edx = 2;
    *(rbx) = rbx;
    *((rbx + 0x68)) = dx;
    return rax;
label_2:
    eax = 0;
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0x83f0 */
 
int64_t dbg_hash_insert (int64_t arg2) {
     const * matched_ent;
    int64_t var_8h;
    rsi = arg2;
    /* void * hash_insert(Hash_table * table, const * entry); */
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    eax = hash_insert_if_absent ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    rax = rbx;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xbaa0 */
 
int64_t dbg_cycle_check (uint32_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool cycle_check(cycle_check_state * state,stat const * sb); */
    if (*((rdi + 0x18)) != 0x95f616) {
        goto label_1;
    }
    rax = *((rdi + 0x10));
    rdx = *((rsi + 8));
    if (rax == 0) {
        goto label_2;
    }
    while (*(rsi) != rcx) {
        rcx = rax + 1;
        *((rdi + 0x10)) = rcx;
        if ((rax & rcx) == 0) {
            goto label_3;
        }
        eax = 0;
        return rax;
label_2:
        *((rdi + 0x10)) = 1;
label_0:
        rax = *(rsi);
        *(rdi) = rdx;
        *((rdi + 8)) = rax;
        eax = 0;
        return rax;
        rcx = *((rdi + 8));
    }
    eax = 1;
    return rax;
label_3:
    if (rcx != 0) {
        goto label_0;
    }
    eax = 1;
    return rax;
label_1:
    return assert_fail ("state->magic == 9827862", "lib/cycle-check.c", 0x3c, "cycle_check");
}

/* /tmp/tmp79u99nvo @ 0x5560 */
 
int64_t leave_dir (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if ((*((rdi + 0x48)) & 0x102) != 0) {
        goto label_1;
    }
    rax = *((rsi + 8));
    if (rax == 0) {
        goto label_0;
    }
    if (*((rax + 0x58)) < 0) {
        goto label_0;
    }
    rdx = *((rdi + 0x58));
    if (*((rdx + 0x10)) == 0) {
        void (*0x289a)() ();
    }
    rcx = *((rsi + 0x78));
    if (*(rdx) == rcx) {
        goto label_2;
    }
label_0:
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_3;
    }
    return rax;
label_1:
    rax = *((rsi + 0x70));
    *(rsp) = rax;
    rax = *((rsi + 0x78));
    *((rsp + 8)) = rax;
    rax = hash_remove (*((rdi + 0x58)), rsp);
    rdi = rax;
    if (rax == 0) {
        void (*0x289a)() ();
    }
    fcn_00002450 ();
    goto label_0;
label_2:
    rcx = *((rsi + 0x70));
    if (*((rdx + 8)) != rcx) {
        goto label_0;
    }
    rcx = *((rax + 0x70));
    rax = *((rax + 0x78));
    *((rdx + 8)) = rcx;
    *(rdx) = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x289a */
 
void leave_dir_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x5610 */
 
int32_t dbg_restore_initial_cwd (int64_t arg1) {
    rdi = arg1;
    /* int restore_initial_cwd(FTS * sp); */
    rbx = rdi;
    eax = *((rdi + 0x48));
    r12d = *((rdi + 0x48));
    r12d &= 4;
    if (r12d == 0) {
        if ((ah & 2) == 0) {
            goto label_2;
        }
        edx = 1;
        esi = 0xffffff9c;
        cwd_advance_fd ();
    } else {
        r12d = 0;
    }
label_1:
    rbx += 0x60;
    while (al == 0) {
        rdi = rbx;
        eax = i_ring_pop ();
        if (eax >= 0) {
            goto label_3;
        }
label_0:
        rdi = rbx;
        al = i_ring_empty ();
    }
    eax = r12d;
    return eax;
label_3:
    close (eax);
    goto label_0;
label_2:
    edi = *((rdi + 0x28));
    r12d = 0;
    eax = fchdir ();
    r12b = (eax != 0) ? 1 : 0;
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0x56a0 */
 
int64_t dbg_fts_safe_changedir (int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4) {
    stat sb;
    int64_t var_fh;
    void * buf;
    int64_t var_18h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fts_safe_changedir(FTS * sp,FTSENT * p,int fd,char const * dir); */
    r15 = rdi;
    r14d = edx;
    r12 = rsi;
    rbx = rcx;
    rax = *(fs:0x28);
    *((rsp + 0xa8)) = rax;
    eax = 0;
    if (rcx == 0) {
        goto label_0;
    }
    eax = *(rcx);
    while (*((rcx + 1)) != 0x2e) {
label_0:
        ebp = *((r15 + 0x48));
        if ((bpl & 4) != 0) {
            goto label_10;
        }
        if (r14d < 0) {
            goto label_11;
        }
        *((rsp + 0xf)) = 0;
        r13d = r14d;
        if ((bpl & 2) != 0) {
            goto label_4;
        }
label_1:
        if (rbx != 0) {
            eax = *(rbx);
label_8:
            if (eax == 0x2e) {
                goto label_12;
            }
        }
label_2:
        ebp &= 0x200;
        if (ebp != 0) {
            goto label_13;
        }
        edi = r13d;
        eax = fchdir ();
        r12d = eax;
        if (r14d < 0) {
            goto label_14;
        }
label_3:
        rax = *((rsp + 0xa8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
    }
    if (*((rcx + 2)) != 0) {
        goto label_0;
    }
    ebp = *((rdi + 0x48));
    if ((bpl & 4) != 0) {
        goto label_10;
    }
    if (edx >= 0) {
        goto label_16;
    }
    if ((ebp & 0x200) != 0) {
        goto label_17;
    }
    *((rsp + 0xf)) = 1;
    edx <<= 0xd;
    edx &= 0x20000;
    edx |= 0x90900;
label_7:
    eax = 0;
    eax = open_safer (rbx, edx, ebp, rcx);
    r13d = eax;
label_5:
    if (r13d < 0) {
        goto label_18;
    }
    ebp = *((r15 + 0x48));
    if ((bpl & 2) == 0) {
        goto label_1;
    }
label_4:
    eax = fstat (r13d, rsp + 0x10);
    if (eax == 0) {
        rax = *((rsp + 0x10));
        if (*((r12 + 0x70)) == rax) {
            rax = *((rsp + 0x18));
            if (*((r12 + 0x78)) != rax) {
                goto label_19;
            }
            ebp = *((r15 + 0x48));
            goto label_2;
label_10:
            ebp &= 0x200;
            if (ebp != 0) {
                if (r14d >= 0) {
                    goto label_20;
                }
            }
            r12d = 0;
            goto label_3;
        }
label_19:
        errno_location ();
        *(rax) = 2;
    }
    r12d = 0xffffffff;
    if (r14d >= 0) {
        goto label_3;
    }
label_14:
    rax = errno_location ();
    ebp = *(rax);
    rbx = rax;
    close (r13d);
    *(rbx) = ebp;
    goto label_3;
label_12:
    if (*((rbx + 1)) != 0x2e) {
        goto label_2;
    }
    if (*((rbx + 2)) == 0) {
        goto label_4;
    }
    goto label_2;
label_20:
    r12d = 0;
    close (r14d);
    goto label_3;
label_17:
    r13 = rdi + 0x60;
    rdi = r13;
    al = i_ring_empty ();
    *((rsp + 0xf)) = al;
    if (al != 0) {
        edx <<= 0xd;
        edx &= 0x20000;
        edx |= 0x90900;
label_6:
        eax = 0;
        eax = openat_safer (*((r15 + 0x2c)), rbx, ebp, rcx, r8);
        r13d = eax;
        goto label_5;
label_13:
        eax = *((rsp + 0xf));
        esi = r13d;
        rdi = r15;
        r12d = 0;
        eax ^= 1;
        edx = (int32_t) al;
        cwd_advance_fd ();
        goto label_3;
    }
    rdi = r13;
    eax = i_ring_pop ();
    ebp = *((r15 + 0x48));
    r13d = eax;
    if (eax < 0) {
        goto label_21;
    }
    *((rsp + 0xf)) = 1;
    r14d = eax;
    if ((bpl & 2) == 0) {
        goto label_2;
    }
    goto label_4;
label_21:
    *((rsp + 0xf)) = 1;
    eax = ebp;
    eax &= 0x200;
label_9:
    edx = ebp;
    edi = *((r15 + 0x2c));
    edx <<= 0xd;
    edx &= 0x20000;
    edx |= 0x90900;
    if (eax != 0) {
        goto label_6;
    }
    goto label_7;
label_18:
    r12d = 0xffffffff;
    goto label_3;
label_15:
    stack_chk_fail ();
label_16:
    *((rsp + 0xf)) = 1;
    r13d = edx;
    if ((bpl & 2) == 0) {
        goto label_8;
    }
    goto label_4;
label_11:
    eax = ebp;
    *((rsp + 0xf)) = 0;
    eax &= 0x200;
    goto label_9;
}

/* /tmp/tmp79u99nvo @ 0x59a0 */
 
int64_t dbg_fts_build (int64_t arg1, uint32_t arg2) {
    uint32_t var_18h;
    int64_t var_30h;
    void ** s1;
    uint32_t var_40h;
    int64_t var_48h;
    int64_t var_6ah;
    uint32_t var_80h;
    int32_t dir_fd;
    int64_t var_8h;
    int64_t var_10h;
    uint32_t var_20h;
    uint32_t var_28h;
    uint32_t var_50h;
    uint32_t var_58h;
    uint32_t var_5dh;
    uint32_t var_5eh;
    uint32_t var_5fh;
    int64_t var_64h;
    uint32_t var_68h;
    rdi = arg1;
    rsi = arg2;
    /* FTSENT * fts_build(FTS * sp,int type); */
    r14 = rdi;
    rbp = *(rdi);
    *((rsp + 0x58)) = esi;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    rax = *((rbp + 0x18));
    *((rsp + 0x50)) = rax;
    if (rax == 0) {
        goto label_32;
    }
    rdi = rax;
    eax = dirfd ();
    *((rsp + 0x64)) = eax;
    if (eax < 0) {
        goto label_33;
    }
    if (*((r14 + 0x40)) == 0) {
        goto label_34;
    }
    *((rsp + 0x40)) = 0xffffffffffffffff;
label_25:
    *((rsp + 0x5f)) = 1;
    edx = *((r14 + 0x48));
label_12:
    rcx = *((rbp + 0x48));
    rax = rcx - 1;
    *((rsp + 8)) = rcx;
    *((rsp + 0x48)) = rax;
    rax = *((rbp + 0x38));
    if (*((rax + rcx - 1)) != 0x2f) {
        rax = rcx + 1;
        *((rsp + 0x48)) = rcx;
        *((rsp + 8)) = rax;
    }
    *((rsp + 0x38)) = 0;
    edx &= 4;
    if (edx != 0) {
        rax = *((rsp + 0x48));
        rax += *((r14 + 0x20));
        rcx = rax + 1;
        *(rax) = 0x2f;
        *((rsp + 0x38)) = rcx;
    }
    rdi = *((rbp + 0x18));
    rax = *((rbp + 0x58));
    r15 = *((r14 + 0x30));
    r15 -= *((rsp + 8));
    rax++;
    *((rsp + 0x18)) = rdi;
    *((rsp + 0x30)) = rax;
    if (rdi == 0) {
        goto label_35;
    }
    rax = errno_location ();
    r13d = 0;
    rdi = *((rsp + 0x18));
    r12d = 0;
    *((rsp + 0x20)) = rax;
    rbx = r14;
    *((rsp + 0x5e)) = 0;
    *((rsp + 0x5d)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x28)) = rbp;
    *((rsp + 0x18)) = r13;
    while (r12 != sym.imp.fflush) {
        r12++;
        if (r12 >= *((rsp + 0x40))) {
            goto label_36;
        }
        *((rsp + 0x10)) = r14;
label_6:
        rax = *((rsp + 0x28));
        rdi = *((rax + 0x18));
        if (rdi == 0) {
            goto label_37;
        }
label_0:
        rax = *((rsp + 0x20));
        *(rax) = 0;
        rax = readdir ();
        r13 = rax;
        if (rax == 0) {
            goto label_38;
        }
        if ((*((rbx + 0x48)) & 0x20) == 0) {
            if (*((rax + 0x13)) == 0x2e) {
                goto label_39;
            }
        }
label_7:
        r14 = r13 + 0x13;
        rax = strlen (r14);
        rdx = rax;
        rax = fts_alloc (rbx, r14, rdx, rcx);
        r14 = rax;
        if (rax == 0) {
            goto label_40;
        }
        if (rbp >= r15) {
            goto label_41;
        }
label_2:
        rdx = rbp;
        rdx += *((rsp + 8));
        if (rdx < 0) {
            goto label_42;
        }
        rax = *((rsp + 0x30));
        rsi = r14 + 0x100;
        *((r14 + 0x58)) = rax;
        rax = *(rbx);
        *((r14 + 0x48)) = rdx;
        edx = *((rbx + 0x48));
        *((r14 + 8)) = rax;
        rax = *(r13);
        *((r14 + 0x78)) = rax;
        if ((dl & 4) != 0) {
            goto label_43;
        }
        *((r14 + 0x30)) = rsi;
label_4:
        if (*((rbx + 0x40)) != 0) {
            if ((dh & 4) == 0) {
                goto label_44;
            }
        }
        eax = *((r13 + 0x12));
        esi = eax;
        eax--;
        if ((dl & 8) != 0) {
            if ((sil & 0xfb) != 0) {
                goto label_45;
            }
        }
        esi = 0xb;
        *((r14 + 0x68)) = si;
        if (eax <= 0xb) {
            goto label_46;
        }
        eax = 0;
        edx = 2;
label_3:
        *((r14 + 0x88)) = eax;
        *((r14 + 0xa0)) = rdx;
label_8:
        *((r14 + 0x10)) = 0;
        if (*((rsp + 0x18)) == 0) {
            goto label_47;
        }
        rax = *((rsp + 0x10));
        *((rax + 0x10)) = r14;
label_5:
    }
    if (*((rbx + 0x40)) == 0) {
        goto label_48;
    }
label_11:
    rax = *((rsp + 0x28));
    *((rsp + 0x10)) = r14;
    r12d = 0x2711;
    rdi = *((rax + 0x18));
    if (rdi != 0) {
        goto label_0;
    }
label_37:
    r13 = *((rsp + 0x18));
    r14 = rbx;
    if (*((rsp + 0x5d)) == 0) {
        goto label_49;
    }
label_17:
    rax = *((r14 + 8));
    rcx = *((r14 + 0x20));
    if (rax == 0) {
        goto label_50;
    }
    do {
        rdx = *((rax + 0x30));
        rsi = rax + 0x100;
        if (rdx != rsi) {
            rdx -= *((rax + 0x38));
            rdx += rcx;
            *((rax + 0x30)) = rdx;
        }
        *((rax + 0x38)) = rcx;
        rax = *((rax + 0x10));
    } while (rax != 0);
label_50:
    rax = r13;
    if (*((r13 + 0x58)) >= 0) {
        goto label_51;
    }
    goto label_49;
    do {
        rax = rdx;
label_1:
        if (*((rax + 0x58)) < 0) {
            goto label_49;
        }
label_51:
        rdx = *((rax + 0x30));
        rsi = rax + 0x100;
        if (rdx != rsi) {
            rdx -= *((rax + 0x38));
            rdx += rcx;
            *((rax + 0x30)) = rdx;
        }
        rdx = *((rax + 0x10));
        *((rax + 0x38)) = rcx;
    } while (rdx != 0);
    rax = *((rax + 8));
    goto label_1;
label_41:
    rax = *((rsp + 0x48));
    rdi = rbx;
    r15 = *((rbx + 0x20));
    rsi = rax + rbp + 2;
    al = fts_palloc ();
    if (al == 0) {
        goto label_40;
    }
    rsi = *((rbx + 0x20));
    if (rsi == r15) {
        goto label_52;
    }
    rsi += *((rsp + 8));
    if ((*((rbx + 0x48)) & 4) == 0) {
        rsi = *((rsp + 0x38));
    }
    *((rsp + 0x38)) = rsi;
label_10:
    r15 = *((rbx + 0x30));
    r15 -= *((rsp + 8));
    *((rsp + 0x5d)) = al;
    goto label_2;
label_9:
    edx = 0xb;
    *((r14 + 0x68)) = dx;
label_46:
    edx = 2;
label_13:
    rcx = obj_CSWTCH_46;
    eax = *((rcx + rax*4));
    goto label_3;
label_43:
    rax = *((r14 + 0x38));
    *((r14 + 0x30)) = rax;
    rax = *((r14 + 0x60));
    memmove (*((rsp + 0x38)), rsi, rax + 1);
    edx = *((rbx + 0x48));
    goto label_4;
label_47:
    *((rsp + 0x18)) = r14;
    goto label_5;
label_39:
    if (*((rax + 0x14)) == 0) {
        goto label_6;
    }
    if (*((rax + 0x14)) != 0x2e) {
        goto label_7;
    }
    goto label_6;
label_44:
    ax = fts_stat (rbx, r14, 0, rcx, r8);
    *((r14 + 0x68)) = ax;
    goto label_8;
label_45:
    edx &= 0x10;
    if (edx != 0) {
        goto label_53;
    }
    if (sil == 0xa) {
        goto label_9;
    }
label_53:
    ecx = 0xb;
    *((r14 + 0x68)) = cx;
    if (eax > 0xb) {
        eax = 0;
        edx = 1;
        goto label_3;
label_52:
        eax = *((rsp + 0x5d));
        goto label_10;
label_48:
        esi = *((rsp + 0x64));
        rdi = *((rsp + 0x28));
        rax = filesystem_type ();
        if (rax == 0x1021994) {
            goto label_54;
        }
        ecx = 0xff534d42;
        if (rax == rcx) {
            goto label_54;
        }
        if (rax == 0x6969) {
            goto label_54;
        }
        *((rsp + 0x5e)) = 1;
        goto label_11;
label_32:
        eax = *((rdi + 0x48));
        edx = *((rdi + 0x48));
        edx &= 0x10;
        if (edx != 0) {
            edx = 0x20000;
            if ((al & 1) != 0) {
                goto label_55;
            }
        }
label_14:
        eax &= 0x204;
        rsi = *((rbp + 0x30));
        edi = 0xffffff9c;
        if (eax == 0x200) {
        }
        rax = opendirat (*((r14 + 0x2c)), rsi, rdx, rsp + 0x64);
        *((rbp + 0x18)) = rax;
        r13 = rax;
        if (rax == 0) {
            goto label_56;
        }
        if (*((rbp + 0x68)) == 0xb) {
            goto label_57;
        }
        if ((*((r14 + 0x49)) & 1) != 0) {
            goto label_58;
        }
label_21:
        rax -= rax;
        eax &= 0x186a1;
        rax--;
        *((rsp + 0x40)) = rax;
        if (*((rsp + 0x58)) == 2) {
            goto label_59;
        }
        eax = *((r14 + 0x48));
        edi = *((rsp + 0x64));
        eax &= 0x38;
        if (eax == 0x18) {
            goto label_60;
        }
label_15:
        r12d = 1;
        bl = (*((rsp + 0x58)) == 3) ? 1 : 0;
label_16:
        if ((*((r14 + 0x49)) & 2) != 0) {
            goto label_61;
        }
label_27:
        if (edi >= 0) {
            goto label_62;
        }
label_24:
        if (bl != 0) {
            if (r12b == 0) {
                goto label_63;
            }
            rax = errno_location ();
            eax = *(rax);
            *((rbp + 0x40)) = eax;
        }
label_63:
        *((rbp + 0x6a)) |= 1;
        rdi = *((rbp + 0x18));
        closedir ();
        edx = *((r14 + 0x48));
        *((rbp + 0x18)) = 0;
        if ((dh & 2) != 0) {
            edi = *((rsp + 0x64));
            if (edi >= 0) {
                goto label_64;
            }
        }
label_26:
        *((rbp + 0x18)) = 0;
        *((rsp + 0x5f)) = 0;
        goto label_12;
    }
    edx = 1;
    goto label_13;
label_54:
    *((rsp + 0x10)) = r14;
    r12d = 0x2711;
    *((rsp + 0x5e)) = 0;
    goto label_6;
label_40:
    r15 = r14;
    r13 = *((rsp + 0x18));
    rax = *((rsp + 0x20));
    r14 = rbx;
    rdi = r15;
    rbp = *((rsp + 0x28));
    ebx = *(rax);
    rax = fcn_00002450 ();
    if (r13 == 0) {
        goto label_65;
    }
    do {
        r12 = r13;
        r13 = *((r13 + 0x10));
        rdi = *((r12 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r12;
        fcn_00002450 ();
    } while (r13 != 0);
label_65:
    rdi = *((rbp + 0x18));
    r13d = 0;
    closedir ();
    rax = *((rsp + 0x20));
    r10d = 7;
    *((rbp + 0x18)) = 0;
    *((rbp + 0x68)) = r10w;
    *((r14 + 0x48)) |= sym._init;
    *(rax) = ebx;
label_18:
    rax = *((rsp + 0x68));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_66;
    }
    rax = r13;
    return rax;
label_55:
    edx = 0;
    dl = (*((rbp + 0x58)) != 0) ? 1 : 0;
    edx <<= 0x11;
    goto label_14;
label_60:
    if (*((rbp + 0x80)) != 2) {
        goto label_15;
    }
    esi = edi;
    rdi = rbp;
    rax = filesystem_type ();
    if (rax == sym.set_char_quoting) {
        goto label_67;
    }
    if (rax > sym.set_char_quoting) {
        goto label_68;
    }
    if (rax == 0) {
        goto label_67;
    }
    if (rax == 0x6969) {
        goto label_67;
    }
label_31:
    if (*((rsp + 0x58)) != 3) {
        goto label_59;
    }
    edi = *((rsp + 0x64));
    r12d = 0;
    ebx = 1;
    goto label_16;
label_38:
    rax = *((rsp + 0x20));
    rbp = *((rsp + 0x28));
    r14 = rbx;
    r13 = *((rsp + 0x18));
    eax = *(rax);
    if (eax != 0) {
        *((rbp + 0x40)) = eax;
        rax = *((rsp + 0x50));
        rax |= r12;
        eax -= eax;
        eax &= 0xfffffffd;
        eax += 7;
        *((rbp + 0x68)) = ax;
    }
    rdi = *((rbp + 0x18));
    if (rdi != 0) {
        closedir ();
        *((rbp + 0x18)) = 0;
    }
label_20:
    if (*((rsp + 0x5d)) != 0) {
        goto label_17;
    }
label_49:
    if ((*((r14 + 0x48)) & 4) != 0) {
        rax = *((rsp + 8));
        if (*((r14 + 0x30)) != rax) {
            if (r12 != 0) {
                goto label_69;
            }
        }
label_30:
label_69:
        rax = *((rsp + 0x38));
        *(rax) = 0;
    }
    if (*((rsp + 0x50)) != 0) {
        goto label_22;
    }
    if (*((rsp + 0x5f)) == 0) {
        goto label_22;
    }
    if (*((rsp + 0x58)) == 1) {
        goto label_70;
    }
    if (r12 == 0) {
        goto label_70;
    }
label_19:
    if (*((rsp + 0x5e)) != 0) {
        goto label_71;
    }
    if (*((r14 + 0x40)) == 0) {
        goto label_18;
    }
    if (r12 == 1) {
        goto label_18;
    }
    rax = fts_sort (r14, r13, r12);
    r13 = rax;
    goto label_18;
label_29:
    al = (*((rsp + 0x50)) == 0) ? 1 : 0;
    r13d = 0;
    if ((*((rsp + 0x5f)) & al) == 0) {
        goto label_72;
    }
    *((rsp + 0x5e)) = 0;
    r12d = 0;
label_70:
    if (*((rbp + 0x58)) != 0) {
        goto label_73;
    }
    eax = restore_initial_cwd (r14);
    if (eax != 0) {
        goto label_74;
    }
label_22:
    if (r12 != 0) {
        goto label_19;
    }
label_72:
    if (*((rsp + 0x58)) == 3) {
        goto label_75;
    }
label_28:
    if (r13 == 0) {
        goto label_23;
    }
    do {
        r13 = *((r13 + 0x10));
        rdi = *((rbp + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = rbp;
        fcn_00002450 ();
    } while (r13 != 0);
label_23:
    r13d = 0;
    goto label_18;
label_36:
    rbp = *((rsp + 0x28));
    r13 = *((rsp + 0x18));
    r14 = rbx;
    goto label_20;
label_71:
    rax = dbg_fts_compare_ino;
    *((r14 + 0x40)) = rax;
    rax = fts_sort (r14, r13, r12);
    *((r14 + 0x40)) = 0;
    r13 = rax;
    goto label_18;
label_59:
    *((rsp + 0x5f)) = 0;
    edx = *((r14 + 0x48));
    goto label_12;
label_58:
    rsi = rbp;
    rdi = r14;
    leave_dir ();
    fts_stat (r14, rbp, 0, rcx, r8);
    rsi = rbp;
    rdi = r14;
    al = enter_dir ();
    if (al != 0) {
        goto label_21;
    }
    errno_location ();
    r13d = 0;
    *(rax) = 0xc;
    goto label_18;
label_34:
    *((rsp + 0x40)) = 0x186a0;
    edx = *((r14 + 0x48));
    *((rsp + 0x5f)) = 1;
    goto label_12;
label_57:
    ax = fts_stat (r14, rbp, 0, rcx, r8);
    *((rbp + 0x68)) = ax;
    goto label_21;
label_73:
    eax = fts_safe_changedir (r14, *((rbp + 8)), 0xffffffff, 0x0000dc7e);
    if (eax == 0) {
        goto label_22;
    }
label_74:
    r8d = 7;
    *((rbp + 0x68)) = r8w;
    *((r14 + 0x48)) |= sym._init;
    if (r13 == 0) {
        goto label_23;
    }
    do {
        r13 = *((r13 + 0x10));
        rdi = *((rbp + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = rbp;
        fcn_00002450 ();
    } while (r13 != 0);
    goto label_23;
label_62:
    eax = fts_safe_changedir (r14, rbp, edi, 0);
    if (eax != 0) {
        goto label_24;
    }
    goto label_25;
label_33:
    rdi = *((rbp + 0x18));
    closedir ();
    *((rbp + 0x18)) = 0;
    if (*((rsp + 0x58)) != 3) {
        goto label_23;
    }
    r11d = 4;
    *((rbp + 0x68)) = r11w;
    rax = errno_location ();
    eax = *(rax);
    *((rbp + 0x40)) = eax;
    goto label_23;
label_64:
    eax = close (rdi);
    edx = *((r14 + 0x48));
    goto label_26;
label_61:
    eax = 0;
    eax = rpl_fcntl (rdi, 0x406, 3, rcx, r8, r9);
    *((rsp + 0x64)) = eax;
    edi = eax;
    goto label_27;
label_75:
    eax = *((rbp + 0x68));
    if (ax == 7) {
        goto label_28;
    }
    if (ax == 4) {
        goto label_28;
    }
    edi = 6;
    *((rbp + 0x68)) = di;
    goto label_28;
label_35:
    if ((*((r14 + 0x48)) & 4) == 0) {
        goto label_29;
    }
    *((rsp + 0x5e)) = 0;
    r13d = 0;
    r12d = 0;
    goto label_30;
label_68:
    if (rax == 0x5346414f) {
        goto label_67;
    }
    edx = 0xff534d42;
    if (rax != rdx) {
        goto label_31;
    }
label_67:
    edi = *((rsp + 0x64));
    goto label_15;
label_42:
    r15 = r14;
    r13 = *((rsp + 0x18));
    rbp = *((rsp + 0x28));
    r14 = rbx;
    rdi = r15;
    fcn_00002450 ();
    if (r13 == 0) {
        goto label_76;
    }
    do {
        r12 = r13;
        r13 = *((r13 + 0x10));
        rdi = *((r12 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r12;
        fcn_00002450 ();
    } while (r13 != 0);
label_76:
    rdi = *((rbp + 0x18));
    r13d = 0;
    closedir ();
    rax = *((rsp + 0x20));
    r9d = 7;
    *((rbp + 0x18)) = 0;
    *((rbp + 0x68)) = r9w;
    *((r14 + 0x48)) |= sym._init;
    *(rax) = 0x24;
    goto label_18;
label_56:
    if (*((rsp + 0x58)) != 3) {
        goto label_23;
    }
    *((rbp + 0x68)) = 4;
    rax = errno_location ();
    eax = *(rax);
    *((rbp + 0x40)) = eax;
    goto label_18;
label_66:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x289f */
 
void rpl_fts_read_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x2470 */
 
void abort (void) {
    /* [15] -r-x section size 1056 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp79u99nvo @ 0x7420 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_raw_hasher ( const * data, size_t n) {
    rdi = data;
    rsi = n;
    /* size_t raw_hasher( const * data,size_t n); */
    rax = rdi;
    edx = 0;
    rax = rotate_right64 (rax, 3);
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rax = rdx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x7440 */
 
int64_t dbg_raw_comparator ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    __asm ("loope 0x7443");
    *(rax) += al;
    *(rax) += al;
    *((rax - 9)) += ah;
}

/* /tmp/tmp79u99nvo @ 0x74e0 */
 
uint64_t hash_find_entry (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    r14 = rdx;
    r13 = rsi;
    r12d = ecx;
    rsi = *((rdi + 0x10));
    rdi = r13;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13);
    if (rax >= *((rbp + 0x10))) {
        void (*0x28a4)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    *(r14) = rbx;
    rsi = *(rbx);
    if (rsi == 0) {
        goto label_2;
    }
    if (rsi == r13) {
        goto label_3;
    }
    rdi = r13;
    al = uint64_t (*rbp + 0x38)() ();
    if (al == 0) {
        goto label_4;
    }
    rax = *(rbx);
label_1:
    if (r12b == 0) {
        goto label_0;
    }
    rdx = *((rbx + 8));
    if (rdx == 0) {
        goto label_5;
    }
    __asm ("movdqu xmm0, xmmword [rdx]");
    __asm ("movups xmmword [rbx], xmm0");
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
    do {
        rsi = *(rax);
        if (rsi == r13) {
            goto label_6;
        }
        rdi = r13;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_6;
        }
        rbx = *((rbx + 8));
label_4:
        rax = *((rbx + 8));
    } while (rax != 0);
label_2:
    eax = 0;
    do {
label_0:
        return rax;
label_6:
        rdx = *((rbx + 8));
        rax = *(rdx);
    } while (r12b == 0);
    rcx = *((rdx + 8));
    *((rbx + 8)) = rcx;
    *(rdx) = 0;
    rcx = *((rbp + 0x48));
    *((rdx + 8)) = rcx;
    *((rbp + 0x48)) = rdx;
    return rax;
label_5:
    *(rbx) = 0;
    goto label_0;
label_3:
    rax = rsi;
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0x28a4 */
 
void hash_find_entry_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x7730 */
 
uint64_t transfer_entries (uint32_t arg_8h, int64_t arg_18h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r14 = rdi;
    r12d = edx;
    rbx = *(rsi);
    if (rbx < *((rsi + 8))) {
        goto label_3;
    }
    goto label_6;
    do {
label_2:
        rbx += 0x10;
        if (*((rbp + 8)) <= rbx) {
            goto label_6;
        }
label_3:
        r15 = *(rbx);
    } while (r15 == 0);
    r13 = *((rbx + 8));
    if (r13 == 0) {
        goto label_7;
    }
    rsi = *((r14 + 0x10));
    goto label_8;
label_0:
    rcx = *((rax + 8));
    *((r13 + 8)) = rcx;
    *((rax + 8)) = r13;
    if (rdx == 0) {
        goto label_9;
    }
label_1:
    r13 = rdx;
label_8:
    r15 = *(r13);
    rdi = *(r13);
    rax = uint64_t (*r14 + 0x30)() ();
    rsi = *((r14 + 0x10));
    if (rax >= rsi) {
        void (*0x28a9)() ();
    }
    rax <<= 4;
    rax += *(r14);
    rdx = *((r13 + 8));
    if (*(rax) != 0) {
        goto label_0;
    }
    *(rax) = r15;
    rax = *((r14 + 0x48));
    *((r14 + 0x18))++;
    *(r13) = 0;
    *((r13 + 8)) = rax;
    *((r14 + 0x48)) = r13;
    if (rdx != 0) {
        goto label_1;
    }
label_9:
    r15 = *(rbx);
label_7:
    *((rbx + 8)) = 0;
    if (r12b != 0) {
        goto label_2;
    }
    rsi = *((r14 + 0x10));
    rdi = r15;
    rax = uint64_t (*r14 + 0x30)() ();
    r13 = rax;
    if (rax >= *((r14 + 0x10))) {
        void (*0x28a9)() ();
    }
    r13 <<= 4;
    r13 += *(r14);
    if (*(r13) == 0) {
        goto label_10;
    }
    rax = *((r14 + 0x48));
    if (rax == 0) {
        goto label_11;
    }
    rdx = *((rax + 8));
    *((r14 + 0x48)) = rdx;
label_5:
    rdx = *((r13 + 8));
    *(rax) = r15;
    *((rax + 8)) = rdx;
    *((r13 + 8)) = rax;
label_4:
    *(rbx) = 0;
    rbx += 0x10;
    *((rbp + 0x18))--;
    if (*((rbp + 8)) > rbx) {
        goto label_3;
    }
label_6:
    eax = 1;
    return rax;
label_10:
    *(r13) = r15;
    *((r14 + 0x18))++;
    goto label_4;
label_11:
    rax = malloc (0x10);
    if (rax != 0) {
        goto label_5;
    }
    eax = 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x28a9 */
 
void transfer_entries_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28ae */
 
void hash_lookup_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28b3 */
 
void hash_get_first_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28b9 */
 
void hash_get_next_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28be */
 
void hash_rehash_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28c3 */
 
void hash_insert_if_absent_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28c8 */
 
void i_ring_pop_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x8830 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000e1e7;
        rdx = 0x0000e1d8;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000e1df;
        rdx = 0x0000e1e1;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000e1e3;
    rdx = 0x0000e1dc;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xbeb0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2720 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp79u99nvo @ 0x8910 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x28ce)() ();
    }
    rdx = 0x0000e240;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xe240 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000e1eb;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000e1e1;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000e26c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xe26c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000e1df;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000e1e1;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000e1df;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000e1e1;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000e36c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xe36c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000e46c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xe46c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000e1e1;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000e1df;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000e1df;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000e1e1;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp79u99nvo @ 0x28ce */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x9d30 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x28d3)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                rdi = r14;
                *((rsp + 0x10)) = rsi;
                fcn_00002450 ();
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x28d3 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28d8 */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28de */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28e3 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28e8 */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28ed */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28f2 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28f7 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x28fc */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x2901 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x2906 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x2f10 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2f40 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2f80 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002460 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp79u99nvo @ 0x2460 */
 
void fcn_00002460 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp79u99nvo @ 0x2fc0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp79u99nvo @ 0x3430 */
 
int64_t cache_fstatat_constprop_0 (int64_t arg3) {
    rdx = arg3;
    rax = *((rdx + 0x30));
    rbx = rdx;
    while (1) {
        if (rax < 0) {
            goto label_1;
        }
        eax = 0;
        return rax;
        ecx = 0x100;
        eax = fstatat ();
        if (eax != 0) {
            *((rbx + 0x30)) = 0xfffffffffffffffe;
            rax = errno_location ();
            rcx = *(rax);
            *((rbx + 8)) = rcx;
            rdx = rcx;
label_0:
            *(rax) = edx;
            eax = 0xffffffff;
            return rax;
        }
        rax = *((rbx + 0x30));
    }
label_1:
    errno_location ();
    edx = *((rbx + 8));
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0x3490 */
 
int64_t prompt_isra_0 (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    uint32_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_38h;
    int64_t var_50h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13d = edi;
    r12 = rcx;
    rbx = rsi;
    r14 = *((rsi + 0x30));
    *((rsp + 0xc)) = edx;
    *((rsp + 0x18)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    rax = *((rsi + 0x38));
    *(rsp) = rax;
    if (r9 == 0) {
        goto label_9;
    }
    *(r9) = 2;
    eax = 0;
    *((rsp + 0x50)) = 0xffffffffffffffff;
    eax = openat (rdi, r14, 0x30900);
    edi = eax;
    if (eax >= 0) {
        *((rsp + 0x10)) = eax;
        rax = fdopendir ();
        edi = *((rsp + 0x10));
        r15 = rax;
        if (rax == 0) {
            goto label_10;
        }
        rax = errno_location ();
        *((rsp + 0x10)) = rax;
        *(rax) = 0;
label_1:
        rdi = r15;
        rax = readdir ();
        if (rax != 0) {
            if (*((rax + 0x13)) == 0x2e) {
                goto label_11;
            }
label_2:
            rax = *((rsp + 0x10));
            rdi = r15;
            edx = *(rax);
            *((rsp + 0x1c)) = edx;
            closedir ();
            rax = *((rsp + 0x10));
            edx = *((rsp + 0x1c));
            r10d = 0;
            *(rax) = edx;
            eax = 3;
            goto label_8;
        }
        rax = *((rsp + 0x10));
        rdi = r15;
        edx = *(rax);
        *((rsp + 0x1c)) = edx;
        closedir ();
        rax = *((rsp + 0x10));
        edx = *((rsp + 0x1c));
        r10d = 1;
        *(rax) = edx;
        eax = 4;
        if (edx == 0) {
            goto label_8;
        }
    }
    r10d = 0;
    eax = 3;
label_8:
    *(rbp) = eax;
    do {
        eax = 3;
        if (*((rbx + 0x20)) == 0) {
            eax = *((r12 + 4));
            if (eax != 5) {
                if (*(r12) == 0) {
                    if (eax != 3) {
                        if (*((r12 + 0x19)) == 0) {
                            goto label_6;
                        }
                    }
                    *((rsp + 0x10)) = r10b;
                    al = can_write_any_file ();
                    r10d = *((rsp + 0x10));
                    if (al == 0) {
                        rbp = rsp + 0x20;
                        rsi = r14;
                        edi = r13d;
                        *((rsp + 0x10)) = r10b;
                        rdx = rbp;
                        eax = cache_fstatat_constprop_0 ();
                        if (eax != 0) {
                            goto label_12;
                        }
                        eax = *((rsp + 0x38));
                        r10d = *((rsp + 0x10));
                        eax &= 0xf000;
                        if (eax == sym.set_custom_quoting) {
                            goto label_13;
                        }
                        ecx = 0x200;
                        edx = 2;
                        rsi = r14;
                        edi = r13d;
                        *((rsp + 0x10)) = r10b;
                        eax = faccessat ();
                        r10d = *((rsp + 0x10));
                        if (eax == 0) {
                            goto label_13;
                        }
                        *((rsp + 0x10)) = r10b;
                        rax = errno_location ();
                        r10d = *((rsp + 0x10));
                        ebp = *(rax);
                        if (ebp != 0xd) {
                            goto label_14;
                        }
                        ebx = 1;
                        goto label_15;
                    }
label_13:
                    eax = *((r12 + 4));
                }
                if (eax == 3) {
                    goto label_16;
                }
            }
label_6:
            eax = 2;
        }
label_0:
        rdx = *((rsp + 0xb8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_17;
        }
        return rax;
label_9:
        *((rsp + 0x50)) = 0xffffffffffffffff;
        r10d = 0;
    } while (1);
label_16:
    ebx = 0;
label_15:
    eax = *((rsp + 0xc));
    eax <<= 2;
    if (eax == 0) {
        goto label_18;
    }
    if (eax != 4) {
        goto label_19;
    }
label_3:
    if (*((r12 + 9)) == 0) {
        if (*((r12 + 0xa)) != 0) {
            if (r10b != 0) {
                goto label_19;
            }
        }
        rsi = *(rsp);
        edi = 4;
        rax = quotearg_style ();
        r12 = rax;
label_4:
        edx = 5;
        rax = dcgettext (0, "cannot remove %s");
        rcx = r12;
        eax = 0;
        error (0, ebp, rax);
        eax = 4;
        goto label_0;
    }
    rsi = *(rsp);
    edi = 4;
    *((rsp + 0xc)) = r10b;
    rax = quotearg_style ();
    r10d = *((rsp + 0xc));
    r12 = rax;
    if (*((rsp + 0x18)) != 2) {
        goto label_20;
    }
    if (r10b != 0) {
        goto label_20;
    }
    r13 = program_name;
    edx = 5;
    if (ebx != 0) {
        goto label_21;
    }
    rax = dcgettext (0, "%s: descend into directory %s? ");
    rdx = rax;
label_5:
    rdi = stderr;
    r8 = r12;
    rcx = r13;
    eax = 0;
    esi = 1;
    fprintf_chk ();
    goto label_22;
label_11:
    edx = 0;
    dl = (*((rax + 0x14)) == 0x2e) ? 1 : 0;
    eax = *((rax + rdx + 0x14));
    if (al == 0) {
        goto label_1;
    }
    if (al == 0x2f) {
        goto label_1;
    }
    goto label_2;
label_18:
    rbp = rsp + 0x20;
    rsi = r14;
    edi = r13d;
    *((rsp + 0xc)) = r10b;
    rdx = rbp;
    eax = cache_fstatat_constprop_0 ();
    if (eax != 0) {
        goto label_12;
    }
    eax = *((rsp + 0x38));
    eax &= 0xf000;
    if (eax == sym.set_custom_quoting) {
        goto label_23;
    }
    r10d = *((rsp + 0xc));
    if (eax == 0x4000) {
        goto label_3;
    }
label_7:
    rsi = *(rsp);
    edi = 4;
    rax = quotearg_style ();
    r12 = rax;
    goto label_24;
label_12:
    rax = errno_location ();
    ebp = *(rax);
label_14:
    rsi = *(rsp);
    edi = 4;
    rax = quotearg_style ();
    r12 = rax;
    goto label_4;
label_20:
    rbp = rsp + 0x20;
label_24:
    rdx = rbp;
    rsi = r14;
    edi = r13d;
    eax = cache_fstatat_constprop_0 ();
    if (eax != 0) {
        goto label_25;
    }
    rax = file_type (rbp);
    r13 = program_name;
    edx = 5;
    if (ebx != 0) {
        goto label_26;
    }
    rax = dcgettext (0, "%s: remove %s %s? ");
    rdx = rax;
    do {
        rdi = stderr;
        r9 = r12;
        r8 = rbp;
        rcx = r13;
        esi = 1;
        eax = 0;
        fprintf_chk ();
label_22:
        al = yesno ();
        edx = (int32_t) al;
        eax = 3;
        eax -= edx;
        goto label_0;
label_26:
        rax = dcgettext (0, "%s: remove write-protected %s %s? ");
        rdx = rax;
    } while (1);
label_21:
    rax = dcgettext (0, "%s: descend into write-protected directory %s? ");
    rdx = rax;
    goto label_5;
label_23:
    if (*((r12 + 4)) != 3) {
        goto label_6;
    }
    goto label_7;
label_25:
    edx = 5;
    rax = dcgettext (0, "cannot remove %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (0, *(rax), r13);
    eax = 4;
    goto label_0;
label_19:
    rbp = rsp + 0x20;
    goto label_7;
label_17:
    stack_chk_fail ();
label_10:
    close (rdi);
    r10d = 0;
    eax = 3;
    goto label_8;
}

/* /tmp/tmp79u99nvo @ 0x3980 */
 
int64_t dbg_excise (int64_t arg_2ch, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    stat st;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* RM_status excise(FTS * fts,FTSENT * ent,rm_options const * x,_Bool is_dir); */
    r13 = rdx;
    edx = (int32_t) cl;
    edx <<= 9;
    r12d = ecx;
    rbx = rsi;
    rsi = *((rsi + 0x30));
    edi = *((rdi + 0x2c));
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    eax = unlinkat ();
    if (eax == 0) {
        goto label_6;
    }
    rax = errno_location ();
    r14d = *(rax);
    r12 = rax;
    if (r14d == 0x1e) {
        goto label_7;
    }
    if (*(r13) != 0) {
        goto label_8;
    }
label_2:
    if (*((rbx + 0x68)) == 4) {
        goto label_9;
    }
label_1:
    rsi = *((rbx + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot remove %s");
    rcx = r13;
    eax = 0;
    error (0, *(r12), rax);
    rax = *((rbx + 8));
    if (*((rax + 0x58)) >= 0) {
        goto label_10;
    }
    goto label_11;
    do {
        *((rax + 0x20)) = 1;
        rax = *((rax + 8));
        if (*((rax + 0x58)) < 0) {
            goto label_11;
        }
label_10:
    } while (*((rax + 0x20)) == 0);
label_11:
    eax = 4;
    do {
label_0:
        rdx = *((rsp + 0x98));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_12;
        }
        return rax;
label_8:
        eax = 2;
    } while (r14d == 0x16);
    if (r14d > 0x16) {
        goto label_13;
    }
    if (r14d == 2) {
        goto label_0;
    }
    if (r14d == 0x14) {
        goto label_0;
    }
    if (*((rbx + 0x68)) != 4) {
        goto label_1;
    }
label_9:
    if (r14d > 0x27) {
        goto label_1;
    }
label_3:
    rax = 0x8000320000;
    if (((rax >> r14) & 1) >= 0) {
        goto label_1;
    }
    eax = *((rbx + 0x40));
    if (eax == 1) {
        goto label_14;
    }
    if (eax != 0xd) {
        goto label_1;
    }
label_14:
    *(r12) = eax;
    goto label_1;
label_13:
    if (r14d != 0x54) {
        goto label_2;
    }
    goto label_0;
label_6:
    eax = 2;
    if (*((r13 + 0x1a)) == 0) {
        goto label_0;
    }
    rsi = *((rbx + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    if (r12b == 0) {
        goto label_15;
    }
    rax = dcgettext (0, "removed directory %s\n");
    rsi = rax;
label_4:
    rdx = r13;
    edi = 1;
    eax = 0;
    printf_chk ();
    eax = 2;
    goto label_0;
label_7:
    rsi = *((rbx + 0x30));
    edi = *((rbp + 0x2c));
    rdx = rsp;
    ecx = 0x100;
    eax = fstatat ();
    if (eax != 0) {
        if (*(r12) == 2) {
            goto label_16;
        }
    }
    *(r12) = 0x1e;
    if (*(r13) == 0) {
        goto label_1;
    }
label_5:
    if (*((rbx + 0x68)) != 4) {
        goto label_1;
    }
    goto label_3;
label_15:
    rax = dcgettext (0, "removed %s\n");
    rsi = rax;
    goto label_4;
label_16:
    eax = 2;
    r14d = 2;
    if (*(r13) == 0) {
        goto label_5;
    }
    goto label_0;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x4450 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x2fd0)() ();
}

/* /tmp/tmp79u99nvo @ 0x2550 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp79u99nvo @ 0x2830 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp79u99nvo @ 0x2810 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp79u99nvo @ 0x2770 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp79u99nvo @ 0x2660 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp79u99nvo @ 0x2760 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp79u99nvo @ 0x2490 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp79u99nvo @ 0xc0e0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp79u99nvo @ 0xa280 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0x4920 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0xa5b0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x2590 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp79u99nvo @ 0xb120 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa0c0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb360 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xb8a0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000e00a);
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x27a0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp79u99nvo @ 0x24f0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp79u99nvo @ 0x85f0 */
 
void dbg_i_ring_init (int32_t default_val, I_ring * ir) {
    rsi = default_val;
    rdi = ir;
    /* void i_ring_init(I_ring * ir,int default_val); */
    *((rdi + 0x14)) = 0;
    *((rdi + 0x1c)) = 1;
    *(rdi) = esi;
    *((rdi + 4)) = esi;
    *((rdi + 8)) = esi;
    *((rdi + 0xc)) = esi;
    *((rdi + 0x10)) = esi;
}

/* /tmp/tmp79u99nvo @ 0x3bd0 */
 
int64_t dbg_rm (uint32_t arg_18h, int64_t fd, uint32_t arg1, uint32_t arg2) {
    Ternary is_empty_directory;
    stat statbuf;
    int64_t var_ch;
    uint32_t var_1ch;
    void * buf;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    /* RM_status rm(char * const * file,rm_options const * x); */
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
    if (*(rdi) == 0) {
        goto label_15;
    }
    rbx = rsi;
    r12d = 2;
    esi -= esi;
    edx = 0;
    esi &= 0xffffffc0;
    esi += 0x258;
    rax = xfts_open (rdi);
    do {
label_1:
        rdi = rbp;
        rax = rpl_fts_read ();
        r15 = rax;
        if (rax == 0) {
            goto label_16;
        }
label_0:
        ecx = *((rax + 0x68));
        if (cx == 1) {
            goto label_17;
        }
        eax = rcx - 2;
        if (ax > 0xb) {
            goto label_18;
        }
        eax = 1;
        rax <<= cl;
        if ((eax & 0x3d58) == 0) {
            goto label_19;
        }
        if (cx == 6) {
            if (*((rbx + 8)) == 0) {
                goto label_20;
            }
            if (*((r15 + 0x58)) <= 0) {
                goto label_20;
            }
            rax = *((rbp + 0x18));
            if (*((r15 + 0x70)) != rax) {
                goto label_21;
            }
        }
label_20:
        ecx &= 0xfffffffd;
        r14d = 0;
        edi = *((rbp + 0x2c));
        r8d = 3;
        rsi = r15;
        rcx = rbx;
        r14b = (cx == 4) ? 1 : 0;
        r9d = 0;
        edx = r14d;
        eax = prompt_isra_0 ();
        r13d = eax;
        if (eax == 2) {
            goto label_22;
        }
label_3:
        eax = r13 - 2;
        if (eax > 2) {
            goto label_23;
        }
        if (r13d == 4) {
            goto label_24;
        }
    } while (r13d != 3);
    eax = 3;
    rdi = rbp;
    if (r12d == 2) {
        r12d = eax;
    }
    rax = rpl_fts_read ();
    r15 = rax;
    if (rax != 0) {
        goto label_0;
    }
label_16:
    rax = errno_location ();
    rbx = rax;
    eax = *(rax);
    if (eax != 0) {
        goto label_25;
    }
    eax = rpl_fts_close (rbp, rsi, rdx);
    if (eax != 0) {
        goto label_26;
    }
label_13:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_27;
    }
    eax = r12d;
    return rax;
label_19:
    if (cx == 7) {
        goto label_28;
    }
    if (cx != 2) {
label_18:
        rdx = *((r15 + 0x38));
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r12d = *((r15 + 0x68));
        edx = 5;
        rbx = rax;
        rax = dcgettext (0, "unexpected failure: fts_info=%d: %s\nplease report to %s");
        r9 = "bug-coreutils@gnu.org";
        r8 = rbx;
        ecx = r12d;
        eax = 0;
        error (0, 0, rax);
        eax = abort ();
label_17:
        if (*((rbx + 9)) != 0) {
            goto label_29;
        }
        if (*((rbx + 0xa)) == 0) {
            goto label_30;
        }
        eax = 0;
        eax = openat (*((rbp + 0x2c)), *((rax + 0x30)), 0x30900);
        r13d = eax;
        if (eax >= 0) {
            edi = eax;
            rax = fdopendir ();
            r14 = rax;
            if (rax == 0) {
                goto label_31;
            }
            errno_location ();
            *(rax) = 0;
            r13 = rax;
label_11:
            rdi = r14;
            rax = readdir ();
            if (rax == 0) {
                goto label_32;
            }
            if (*((rax + 0x13)) == 0x2e) {
                goto label_33;
            }
label_12:
            r12d = *(r13);
            rdi = r14;
            closedir ();
            *(r13) = r12d;
        }
label_2:
        if (*((rbx + 0xa)) == 0) {
            goto label_30;
        }
        r12d = 0x27;
        goto label_34;
    }
    rdx = *((r15 + 0x38));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "WARNING: Circular directory structure.\nThis almost certainly means that you have a corrupted file system.\nNOTIFY YOUR SYSTEM MANAGER.\nThe following directory is part of the cycle:\n  %s\n");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    do {
label_5:
        edx = 4;
        r12d = 4;
        rpl_fts_set (rbp, r15);
        rdi = rbp;
        rpl_fts_read ();
        goto label_1;
label_28:
        rdx = *((r15 + 0x38));
        edi = 0;
        esi = 3;
        rax = quotearg_n_style_colon ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "traversal failed: %s");
        rcx = r12;
        eax = 0;
        error (0, *((r15 + 0x40)), rax);
    } while (1);
label_32:
    edx = *(r13);
    rdi = r14;
    *((rsp + 0xc)) = edx;
    closedir ();
    edx = *((rsp + 0xc));
    *(r13) = edx;
    if (edx != 0) {
        goto label_2;
    }
label_29:
    if (*((r15 + 0x58)) == 0) {
        r13 = *((r15 + 0x30));
        rdi = *((r15 + 0x30));
        rax = last_component ();
        if (*(rax) == 0x2e) {
            goto label_35;
        }
label_7:
        rax = *((rbx + 0x10));
        if (rax != 0) {
            rsi = *(rax);
            if (*((r15 + 0x78)) == rsi) {
                goto label_36;
            }
        }
label_9:
        if (*((rbx + 0x18)) != 0) {
            goto label_37;
        }
    }
label_8:
    edi = *((rbp + 0x2c));
    r9 = rsp + 0x1c;
    rcx = rbx;
    edx = 1;
    r8d = 2;
    rsi = r15;
    eax = prompt_isra_0 ();
    r13d = eax;
    if (eax == 2) {
        goto label_38;
    }
label_4:
    rdx = *((r15 + 8));
    if (*((rdx + 0x58)) >= 0) {
        goto label_39;
    }
    goto label_40;
    do {
        *((rdx + 0x20)) = 1;
        rdx = *((rdx + 8));
        if (*((rdx + 0x58)) < 0) {
            goto label_40;
        }
label_39:
    } while (*((rdx + 0x20)) == 0);
label_40:
    edx = 4;
    rpl_fts_set (rbp, r15);
    rdi = rbp;
    rpl_fts_read ();
    goto label_3;
label_22:
    eax = excise (rbp, r15, rbx, r14d, r8);
    r13d = eax;
    goto label_3;
label_38:
    if (*((rsp + 0x1c)) != 4) {
        goto label_1;
    }
    eax = excise (rbp, r15, rbx, 1, r8);
    r13d = eax;
    if (eax != 2) {
        goto label_4;
    }
    edx = 4;
    rpl_fts_set (rbp, r15);
    rdi = rbp;
    rpl_fts_read ();
    goto label_1;
label_30:
    r12d = 0x15;
label_34:
    rsi = *((r15 + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot remove %s");
    rcx = r13;
    eax = 0;
    error (0, r12d, rax);
    rax = *((r15 + 8));
    if (*((rax + 0x58)) >= 0) {
        goto label_41;
    }
    goto label_5;
label_6:
    *((rax + 0x20)) = 1;
    rax = *((rax + 8));
    if (*((rax + 0x58)) < 0) {
        goto label_5;
    }
label_41:
    if (*((rax + 0x20)) == 0) {
        goto label_6;
    }
    goto label_5;
label_35:
    edx = 0;
    dl = (*((rax + 1)) == 0x2e) ? 1 : 0;
    eax = *((rax + rdx + 1));
    if (al == 0) {
        goto label_42;
    }
    if (al != 0x2f) {
        goto label_7;
    }
label_42:
    rdx = *((r15 + 0x38));
    esi = 4;
    edi = 2;
    rax = quotearg_n_style ();
    esi = 4;
    edi = 1;
    rdx = 0x0000dc7e;
    r14 = rax;
    rax = quotearg_n_style ();
    edi = 0;
    esi = 4;
    rdx = 0x0000dc7f;
    r13 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "refusing to remove %s or %s directory: skipping %s");
    r9 = r14;
    r8 = r13;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_5;
label_37:
    rdi = r13;
    edx = 0;
    rsi = 0x0000dc7e;
    rax = file_name_concat ();
    rdi = rax;
    r13 = rax;
    eax = lstat (rdi, rsp + 0x20);
    if (eax != 0) {
        goto label_43;
    }
    rdi = r13;
    fcn_00002450 ();
    rax = *((rsp + 0x20));
    if (*((rbp + 0x18)) == rax) {
        goto label_8;
    }
    rsi = *((r15 + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "skipping %s, since it's on a different device");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    edx = 5;
label_10:
    rax = dcgettext (0, "and --preserve-root=all is in effect");
    eax = 0;
    error (0, 0, rax);
    goto label_5;
label_36:
    rax = *((rax + 8));
    if (*((r15 + 0x70)) != rax) {
        goto label_9;
    }
    rsi = *((r15 + 0x38));
    if (*(rsi) != 0x2f) {
        goto label_44;
    }
    if (*((rsi + 1)) != 0) {
        goto label_44;
    }
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "it is dangerous to operate recursively on %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
label_14:
    edx = 5;
    rsi = "use --no-preserve-root to override this failsafe";
    goto label_10;
label_33:
    edx = 0;
    dl = (*((rax + 0x14)) == 0x2e) ? 1 : 0;
    eax = *((rax + rdx + 0x14));
    if (al == 0) {
        goto label_11;
    }
    if (al == 0x2f) {
        goto label_11;
    }
    goto label_12;
label_25:
    edx = 5;
    r12d = 4;
    rax = dcgettext (0, "fts_read failed");
    eax = 0;
    error (0, *(rbx), rax);
    eax = rpl_fts_close (rbp, rsi, rdx);
    if (eax == 0) {
        goto label_13;
    }
label_26:
    edx = 5;
    r12d = 4;
    rax = dcgettext (0, "fts_close failed");
    eax = 0;
    error (0, *(rbx), rax);
    goto label_13;
label_43:
    rdx = *((r15 + 0x30));
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = r13;
    edi = 0;
    esi = 4;
    r14 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "failed to stat %s: skipping %s");
    r8 = r14;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    rdi = r13;
    fcn_00002450 ();
    goto label_5;
label_21:
    rax = *((r15 + 8));
    if (*((rax + 0x58)) >= 0) {
        goto label_45;
    }
    goto label_46;
    do {
        *((rax + 0x20)) = 1;
        rax = *((rax + 8));
        if (*((rax + 0x58)) < 0) {
            goto label_46;
        }
label_45:
    } while (*((rax + 0x20)) == 0);
label_46:
    rsi = *((r15 + 0x38));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "skipping %s, since it's on a different device");
    rcx = r12;
    eax = 0;
    r12d = 4;
    error (0, 0, rax);
    goto label_1;
label_15:
    r12d = 2;
    goto label_13;
label_44:
    rdx = 0x0000e645;
    esi = 4;
    edi = 1;
    rax = quotearg_n_style ();
    rdx = *((r15 + 0x38));
    edi = 0;
    esi = 4;
    r13 = rax;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "it is dangerous to operate recursively on %s (same as %s)");
    r8 = r13;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_14;
label_23:
    assert_fail ("VALID_STATUS (s)", "src/remove.c", 0x263, global___PRETTY_FUNCTION__.2);
label_27:
    stack_chk_fail ();
label_31:
    close (r13d);
    goto label_2;
label_24:
    r12d = 4;
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0x8660 */
 
int64_t i_ring_pop (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x1c)) != 0) {
        void (*0x28c8)() ();
    }
    edx = *((rdi + 0x14));
    ecx = *((rdi + 0x10));
    r8d = *((rdi + rdx*4));
    rax = rdx;
    *((rdi + rdx*4)) = ecx;
    if (edx != *((rdi + 0x18))) {
        eax += 3;
        eax &= 3;
        *((rdi + 0x14)) = eax;
        eax = r8d;
        return rax;
    }
    *((rdi + 0x1c)) = 1;
    eax = r8d;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x9fe0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xbef0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa2d0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x28de)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa040 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x79a0 */
 
int64_t dbg_hash_print_statistics (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void hash_print_statistics(Hash_table const * table,FILE * stream); */
    r12d = 0;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8 = *((rdi + 0x20));
    rbx = *((rdi + 0x10));
    r13 = *((rdi + 0x18));
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_2;
    do {
        rcx += 0x10;
        if (rsi <= rcx) {
            goto label_2;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_3;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_3:
    if (r12 < rdx) {
        r12 = rdx;
    }
    rcx += 0x10;
    if (rsi > rcx) {
        goto label_0;
    }
label_2:
    rcx = r8;
    rdx = "# entries:         %lu\n";
    rdi = rbp;
    eax = 0;
    esi = 1;
    eax = fprintf_chk ();
    eax = 0;
    rcx = rbx;
    esi = 1;
    rdx = "# buckets:         %lu\n";
    rdi = rbp;
    fprintf_chk ();
    if (r13 < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, r13");
    __asm ("mulsd xmm0, qword [0x0000e190]");
    if (rbx < 0) {
        goto label_5;
    }
    do {
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rbx");
label_1:
        __asm ("divsd xmm0, xmm1");
        rcx = r13;
        rdi = rbp;
        esi = 1;
        rdx = "# buckets used:    %lu (%.2f%%)\n";
        eax = 1;
        eax = fprintf_chk ();
        rcx = r12;
        rdi = rbp;
        rdx = "max bucket length: %lu\n";
        esi = 1;
        eax = 0;
        void (*0x2830)() ();
label_4:
        rax = r13;
        rdx = r13;
        xmm0 = 0;
        rax >>= 1;
        edx &= 1;
        rax |= rdx;
        __asm ("cvtsi2sd xmm0, rax");
        __asm ("addsd xmm0, xmm0");
        __asm ("mulsd xmm0, qword [0x0000e190]");
    } while (rbx >= 0);
label_5:
    rax = rbx;
    ebx &= 1;
    xmm1 = 0;
    rax >>= 1;
    rax |= rbx;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("addsd xmm1, xmm1");
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0x8780 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite (0x0000e198, 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp79u99nvo @ 0x25d0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp79u99nvo @ 0x2820 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp79u99nvo @ 0x45d0 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x27a0)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0xaad0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0xa810 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x28f7)() ();
    }
    if (rdx == 0) {
        void (*0x28f7)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa8b0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28fc)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x28fc)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa3f0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x28e8)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa770 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x28f2)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xc0f4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp79u99nvo @ 0x7f50 */
 
int64_t dbg_hash_free (int64_t arg_8h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_free(Hash_table * table); */
    r12 = rdi;
    r13 = *(rdi);
    rax = *((rdi + 8));
    if (*((rdi + 0x40)) == 0) {
        goto label_2;
    }
    if (*((rdi + 0x20)) == 0) {
        goto label_2;
    }
    if (r13 < rax) {
        goto label_0;
    }
    goto label_3;
    do {
        r13 += 0x10;
        if (rax <= r13) {
            goto label_4;
        }
label_0:
        rdi = *(r13);
    } while (rdi == 0);
    rbx = r13;
    while (rbx != 0) {
        rdi = *(rbx);
        uint64_t (*r12 + 0x40)() ();
        rbx = *((rbx + 8));
    }
    rax = *((r12 + 8));
    r13 += 0x10;
    if (rax > r13) {
        goto label_0;
    }
label_4:
    rbp = *(r12);
label_2:
    if (rax <= rbp) {
        goto label_3;
    }
label_1:
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_5;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00002450 ();
    } while (rbx != 0);
label_5:
    rbp += 0x10;
    if (*((r12 + 8)) > rbp) {
        goto label_1;
    }
label_3:
    rbx = *((r12 + 0x48));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        rdi = rbx;
        rbx = *((rbx + 8));
        fcn_00002450 ();
    } while (rbx != 0);
label_6:
    rdi = *(r12);
    fcn_00002450 ();
    rdi = r12;
    return void (*0x2450)() ();
}

/* /tmp/tmp79u99nvo @ 0xb3e0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xb4e0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb380 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x4c90 */
 
uint64_t dbg_mfile_name_concat (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * mfile_name_concat(char const * dir,char const * base,char ** base_in_result); */
    r12 = rsi;
    *((rsp + 8)) = rdx;
    rax = last_component ();
    r13 = rax;
    rax = base_len (rax);
    r13 -= rbp;
    r14 = r13 + rax;
    rbx = rax;
    rax = strlen (r12);
    r13 = rax;
    if (rbx == 0) {
        goto label_1;
    }
    if (*((rbp + r14 - 1)) == 0x2f) {
        goto label_2;
    }
    ebx = 0;
    r15d = 0;
    eax = 0x2f;
    if (*(r12) == 0x2f) {
        eax = r15d;
    }
    bl = (*(r12) != 0x2f) ? 1 : 0;
    *((rsp + 7)) = al;
    do {
label_0:
        rdi += rbx;
        rax = malloc (r14 + r13 + 1);
        r15 = rax;
        if (rax != 0) {
            rdi = rax;
            rdx = r14;
            rsi = rbp;
            mempcpy ();
            ecx = *((rsp + 7));
            rdi = rax + rbx;
            *(rax) = cl;
            rax = *((rsp + 8));
            if (rax != 0) {
                *(rax) = rdi;
            }
            rdx = r13;
            rsi = r12;
            mempcpy ();
            *(rax) = 0;
        }
        rax = r15;
        return rax;
label_1:
        ebx = 0;
        r15d = 0;
        eax = 0x2e;
        if (*(r12) != 0x2f) {
            eax = r15d;
        }
        bl = (*(r12) == 0x2f) ? 1 : 0;
        *((rsp + 7)) = al;
    } while (1);
label_2:
    *((rsp + 7)) = 0;
    ebx = 0;
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0x4b30 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2710)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp79u99nvo @ 0x7d30 */
 
int64_t dbg_hash_string (int64_t arg1, size_t n_buckets) {
    rdi = arg1;
    rsi = n_buckets;
    /* size_t hash_string(char const * string,size_t n_buckets); */
    ecx = *(rdi);
    edx = 0;
    if (cl == 0) {
        goto label_0;
    }
    do {
        rax = rdx;
        rdi++;
        rax <<= 5;
        rax -= rdx;
        edx = 0;
        rax += rcx;
        ecx = *(rdi);
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    } while (cl != 0);
label_0:
    rax = rdx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x8720 */
 
uint64_t dbg_opendirat (int64_t arg3, int64_t arg4, int32_t fd, const char * path) {
    rdx = arg3;
    rcx = arg4;
    rdi = fd;
    rsi = path;
    /* DIR * opendirat(int dir_fd,char const * dir,int extra_flags,int * pnew_fd); */
    edx |= 0x90900;
    eax = 0;
    r12d = 0;
    rbx = rcx;
    eax = openat_safer (rdi, rsi, rdx, rcx, r8);
    if (eax < 0) {
        goto label_0;
    }
    edi = eax;
    rax = fdopendir ();
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rbx) = ebp;
    do {
label_0:
        rax = r12;
        return rax;
label_1:
        rax = errno_location ();
        r13d = *(rax);
        rbx = rax;
        close (ebp);
        *(rbx) = r13d;
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0x86a0 */
 
int64_t dbg_openat_safer (int64_t arg_60h, int64_t arg4, int32_t fd, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_38h;
    rcx = arg4;
    rdi = fd;
    rdx = oflag;
    rsi = path;
    /* int openat_safer(int fd,char const * file,int flags,va_args ...); */
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = openat (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x18;
        ecx = *((rsp + 0x38));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x27e0 */
 
void fdopendir (void) {
    __asm ("bnd jmp qword [reloc.fdopendir]");
}

/* /tmp/tmp79u99nvo @ 0x2620 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmp79u99nvo @ 0xb320 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xa2b0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0xb860 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x2570 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp79u99nvo @ 0xb340 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x4460 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp79u99nvo @ 0xb060 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xabf0)() ();
}

/* /tmp/tmp79u99nvo @ 0x4a80 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000e00a);
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0x48c0 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xc020 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0xb2e0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x7930 */
 
int64_t dbg_hash_table_ok (Hash_table const * table) {
    rdi = table;
    /* _Bool hash_table_ok(Hash_table const * table); */
    rcx = *(rdi);
    rsi = *((rdi + 8));
    edx = 0;
    r8d = 0;
    if (rcx < rsi) {
        goto label_1;
    }
    goto label_2;
    do {
label_0:
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_2;
        }
label_1:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    r8++;
    rdx++;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_1;
    }
label_2:
    eax = 0;
    if (*((rdi + 0x18)) != r8) {
        return rax;
    }
    al = (*((rdi + 0x20)) == rdx) ? 1 : 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xbfa0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x4790 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xa960 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2901)() ();
    }
    if (rax == 0) {
        void (*0x2901)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x6490 */
 
uint64_t dbg_rpl_fts_open (int64_t arg_10h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    uint32_t var_16h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* FTS * rpl_fts_open(char * const * argv,int options,int (*)() compar); */
    if ((esi & 0xfffff000) != 0) {
        goto label_13;
    }
    eax = esi;
    eax &= 0x204;
    if (eax == 0x204) {
        goto label_13;
    }
    if ((sil & 0x12) == 0) {
        goto label_13;
    }
    rbx = rdi;
    r14 = rdx;
    rax = calloc (1, 0x80);
    r12 = rax;
    if (rax == 0) {
        goto label_2;
    }
    *((rax + 0x40)) = r14;
    eax = ebp;
    rdi = *(rbx);
    *((r12 + 0x2c)) = 0xffffff9c;
    ah &= 0xfd;
    eax |= 4;
    if ((bpl & 2) == 0) {
        eax = ebp;
    }
    *((r12 + 0x48)) = eax;
    if (rdi == 0) {
        goto label_14;
    }
    r15 = rbx;
    r13d = 0;
    do {
        rax = strlen (rdi);
        if (r13 < rax) {
            r13 = rax;
        }
        rdi = *((r15 + 8));
        r15 += 8;
    } while (rdi != 0);
    rsi = r13 + 1;
    eax = 0x1000;
    if (rsi < rax) {
        rsi = rax;
    }
label_8:
    rdi = r12;
    al = fts_palloc ();
    *((rsp + 0x16)) = al;
    if (al == 0) {
        goto label_15;
    }
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_16;
    }
    rax = fts_alloc (r12, 0x0000e601, 0, rcx);
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_17;
    }
    *((rax + 0x58)) = 0xffffffffffffffff;
    r15 = *(rbx);
label_7:
    if (r14 != 0) {
        eax = *((r12 + 0x48));
        eax >>= 0xa;
        eax &= 1;
        *((rsp + 0x16)) = al;
    }
    if (r15 == 0) {
        goto label_18;
    }
    *((rsp + 0x18)) = 0;
    ebp >>= 0xb;
    r13d = 0;
    ebp ^= 1;
    eax = ebp;
    ebp = 0;
    eax &= 1;
    *((rsp + 0x17)) = al;
    while (rax <= 2) {
label_0:
        rax = fts_alloc (r12, r15, rdx, rcx);
        r15 = rax;
        if (rax == 0) {
            goto label_5;
        }
        *((rax + 0x58)) = 0;
        rax = *((rsp + 8));
        *((r15 + 8)) = rax;
        rax = r15 + 0x100;
        *((r15 + 0x30)) = rax;
        if (rbp == 0) {
            goto label_19;
        }
        if (*((rsp + 0x16)) == 0) {
            goto label_19;
        }
        *((r15 + 0xa0)) = 2;
        esi = 0xb;
        *((r15 + 0x68)) = si;
        if (r14 == 0) {
            goto label_20;
        }
label_3:
        *((r15 + 0x10)) = rbp;
label_6:
        r13++;
        r15 = *((rbx + r13*8));
        if (r15 == 0) {
            goto label_21;
        }
label_4:
        rax = strlen (r15);
        rdx = rax;
    }
    if (*((rsp + 0x17)) == 0) {
        goto label_0;
    }
    if (*((r15 + rax - 1)) != 0x2f) {
        goto label_0;
    }
label_1:
    if (*((r15 + rdx - 2)) != 0x2f) {
        goto label_0;
    }
    rdx--;
    if (rdx != 1) {
        goto label_1;
    }
    goto label_0;
label_13:
    errno_location ();
    r12d = 0;
    *(rax) = 0x16;
label_2:
    rax = r12;
    return rax;
    do {
        r13 = rbp;
        rbp = *((rbp + 0x10));
        rdi = *((r13 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r13;
        fcn_00002450 ();
label_5:
    } while (rbp != 0);
label_11:
    rdi = *((rsp + 8));
    fcn_00002450 ();
label_17:
    rdi = *((r12 + 0x20));
    fcn_00002450 ();
label_15:
    rdi = r12;
    r12d = 0;
    fcn_00002450 ();
    goto label_2;
label_19:
    ax = fts_stat (r12, r15, 0, rcx, r8);
    *((r15 + 0x68)) = ax;
    if (r14 != 0) {
        goto label_3;
    }
    *((r15 + 0x10)) = 0;
    if (rbp != 0) {
        goto label_22;
    }
    r13++;
    *((rsp + 0x18)) = r15;
    r15 = *((rbx + r13*8));
    if (r15 != 0) {
        goto label_4;
    }
label_21:
    if (r14 != 0) {
        if (r13 <= 1) {
            goto label_23;
        }
        rax = fts_sort (r12, rbp, r13);
    }
label_23:
    rax = fts_alloc (r12, 0x0000e601, 0, rcx);
    *(r12) = rax;
    if (rax == 0) {
        goto label_5;
    }
    ecx = 9;
    *((rax + 0x10)) = rbp;
    rdi = r12;
    *((rax + 0x68)) = cx;
    *((rax + 0x58)) = 1;
    al = setup_dir ();
    if (al == 0) {
        goto label_5;
    }
label_12:
    eax = *((r12 + 0x48));
    while (eax >= 0) {
label_9:
        rdi = r12 + 0x60;
        esi = 0xffffffff;
        i_ring_init ();
        goto label_2;
label_20:
        *((r15 + 0x10)) = 0;
label_22:
        rax = *((rsp + 0x18));
        *((rsp + 0x18)) = r15;
        *((rax + 0x10)) = r15;
        goto label_6;
label_16:
        *((rsp + 8)) = 0;
        goto label_7;
label_14:
        esi = 0x1000;
        goto label_8;
        edx = eax;
        edi = *((r12 + 0x2c));
        edx <<= 0xd;
        edx &= 0x20000;
        edx |= 0x90900;
        if ((ah & 2) == 0) {
            goto label_24;
        }
        eax = 0;
        eax = openat_safer (rdi, 0x0000dc7f, rdx, rcx, r8);
label_10:
        *((r12 + 0x28)) = eax;
    }
    *((r12 + 0x48)) |= 4;
    goto label_9;
label_24:
    eax = 0;
    open_safer (0x0000dc7f, edx, rdx, rcx);
    goto label_10;
label_18:
    rax = fts_alloc (r12, 0x0000e601, 0, rcx);
    *(r12) = rax;
    if (rax == 0) {
        goto label_11;
    }
    edx = 9;
    *((rax + 0x10)) = 0;
    rdi = r12;
    *((rax + 0x68)) = dx;
    *((rax + 0x58)) = 1;
    al = setup_dir ();
    if (al != 0) {
        goto label_12;
    }
    goto label_11;
}

/* /tmp/tmp79u99nvo @ 0x81d0 */
 
int64_t hash_insert_if_absent (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rsi == 0) {
        void (*0x28c3)() ();
    }
    r12 = rsp;
    r13 = rdx;
    ecx = 0;
    rbx = rdi;
    rdx = r12;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_8;
    }
    r8d = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r13) = rax;
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        eax = r8d;
        return rax;
label_8:
        rax = *((rbx + 0x18));
        if (rax < 0) {
            goto label_10;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_11;
        }
label_0:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_1:
        rax = *((rbx + 0x28));
        xmm0 = *((rax + 8));
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm5, xmm0");
        if (rax > 0) {
            goto label_12;
        }
label_2:
        r12 = *(rsp);
        if (*(r12) == 0) {
            goto label_13;
        }
        rax = *((rbx + 0x48));
        if (rax == 0) {
            goto label_14;
        }
        rdx = *((rax + 8));
        *((rbx + 0x48)) = rdx;
label_4:
        rdx = *((r12 + 8));
        *(rax) = rbp;
        r8d = 1;
        *((rax + 8)) = rdx;
        *((r12 + 8)) = rax;
        *((rbx + 0x20))++;
    } while (1);
label_10:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_0;
    }
label_11:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_1;
label_12:
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm1 = xmm4;
    xmm0 = *((rax + 8));
    __asm ("mulss xmm1, xmm0");
    __asm ("comiss xmm5, xmm1");
    if (rdx <= 0) {
        goto label_2;
    }
    __asm ("mulss xmm4, dword [rax + 0xc]");
    if (*((rax + 0x10)) == 0) {
        goto label_15;
    }
label_5:
    __asm ("comiss xmm4, dword [0x0000e184]");
    if (*((rax + 0x10)) < 0) {
        goto label_16;
    }
    do {
label_6:
        r8d = 0xffffffff;
        goto label_3;
label_13:
        *(r12) = rbp;
        r8d = 1;
        *((rbx + 0x20))++;
        *((rbx + 0x18))++;
        goto label_3;
label_14:
        rax = malloc (0x10);
    } while (rax == 0);
    goto label_4;
label_15:
    __asm ("mulss xmm4, xmm0");
    goto label_5;
label_16:
    __asm ("comiss xmm4, dword [0x0000e188]");
    if (rax >= 0) {
        goto label_17;
    }
    __asm ("cvttss2si rsi, xmm4");
label_7:
    rdi = rbx;
    al = hash_rehash ();
    if (al == 0) {
        goto label_6;
    }
    ecx = 0;
    rdx = r12;
    rsi = rbp;
    rdi = rbx;
    rax = hash_find_entry ();
    if (rax == 0) {
        goto label_2;
    }
    void (*0x28c3)() ();
label_17:
    __asm ("subss xmm4, dword [0x0000e188]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_7;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x78b0 */
 
int64_t hash_get_n_buckets_used (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x18));
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xa6e0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xb730 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x9f80 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xaaa0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0x9f20 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb7a0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xa1c0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rdi = *(rbx);
        rbx += 0x10;
        fcn_00002450 ();
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        fcn_00002450 ();
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        rdi = r12;
        fcn_00002450 ();
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x8610 */
 
uint32_t i_ring_empty (int64_t arg1) {
    rdi = arg1;
    eax = *((rdi + 0x1c));
    return eax;
}

/* /tmp/tmp79u99nvo @ 0x7b00 */
 
uint64_t hash_lookup (uint32_t arg_10h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 0x10));
    rdi = r12;
    rax = uint64_t (*rbp + 0x30)(uint64_t, uint64_t) (rbx, rbp);
    if (rax >= *((rbp + 0x10))) {
        void (*0x28ae)() ();
    }
    rbx = rax;
    rbx <<= 4;
    rbx += *(rbp);
    rsi = *(rbx);
    if (rsi != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        rsi = *(rbx);
label_0:
        if (rsi == r12) {
            goto label_2;
        }
        rdi = r12;
        al = uint64_t (*rbp + 0x38)() ();
        if (al != 0) {
            goto label_3;
        }
        rbx = *((rbx + 8));
    } while (rbx != 0);
label_1:
    eax = 0;
    return rax;
label_3:
    r12 = *(rbx);
label_2:
    rax = *(rbx);
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x6aa0 */
 
uint64_t rpl_fts_read (int64_t arg_8h, int64_t arg_18h, void ** s1, int64_t arg_2ch, int64_t arg_48h, int64_t arg_49h, int64_t arg_58h, int64_t arg1) {
    rdi = arg1;
    r12 = *(rdi);
    if (r12 == 0) {
        goto label_3;
    }
    edx = *((rdi + 0x48));
    if ((dh & 0x20) != 0) {
        goto label_3;
    }
    eax = *((r12 + 0x6c));
    ebx = 3;
    *((r12 + 0x6c)) = bx;
    if (ax == 1) {
        goto label_19;
    }
    ecx = *((r12 + 0x68));
    if (ax == 2) {
        goto label_20;
    }
    if (cx != 1) {
        goto label_0;
    }
    goto label_21;
    do {
        *(rbp) = r12;
        rdi = r13;
        fcn_00002450 ();
        if (*((r12 + 0x58)) == 0) {
            goto label_22;
        }
        eax = *((r12 + 0x6c));
        if (ax != 4) {
            goto label_23;
        }
label_0:
        r13 = r12;
        r12 = *((r12 + 0x10));
    } while (r12 != 0);
    r14 = *((r13 + 8));
    if (*((r14 + 0x18)) != 0) {
        goto label_24;
    }
label_8:
    *(rbp) = r14;
    rdi = r13;
    fcn_00002450 ();
    if (*((r14 + 0x58)) == -1) {
        goto label_25;
    }
    if (*((r14 + 0x68)) == 0xb) {
        void (*0x289f)() ();
    }
    rdx = *((rbp + 0x20));
    rax = *((r14 + 0x48));
    *((rdx + rax)) = 0;
    if (*((r14 + 0x58)) == 0) {
        goto label_26;
    }
    eax = *((r14 + 0x6a));
    if ((al & 2) != 0) {
        goto label_27;
    }
    if ((al & 1) == 0) {
        goto label_28;
    }
label_4:
    if (*((r14 + 0x68)) != 2) {
        ecx = *((r14 + 0x40));
        if (ecx != 0) {
            goto label_29;
        }
        edx = 6;
        rsi = r14;
        rdi = rbp;
        *((r14 + 0x68)) = dx;
        rax = leave_dir ();
    }
label_7:
    r12 = r14;
    if ((*((rbp + 0x49)) & 0x20) != 0) {
label_3:
        r12d = 0;
    }
label_2:
    rax = r12;
    return rax;
label_20:
    eax = rcx - 0xc;
    if (ax <= 1) {
        goto label_30;
    }
    if (cx != 1) {
        goto label_0;
    }
label_1:
    if ((dl & 0x40) != 0) {
        rax = *((rbp + 0x18));
        if (*((r12 + 0x70)) != rax) {
            goto label_31;
        }
    }
    r13 = *((rbp + 8));
    if (r13 == 0) {
        goto label_32;
    }
    if ((dh & 0x10) != 0) {
        goto label_33;
    }
    eax = fts_safe_changedir (rbp, r12, 0xffffffff, *((r12 + 0x30)));
    if (eax == 0) {
        goto label_34;
    }
    rax = errno_location ();
    eax = *(rax);
    *((r12 + 0x6a)) |= 1;
    *((r12 + 0x40)) = eax;
    r12 = *((rbp + 8));
    if (r12 == 0) {
        goto label_35;
    }
    rax = r12;
    do {
        rdx = *((rax + 8));
        rdx = *((rdx + 0x30));
        *((rax + 0x30)) = rdx;
        rax = *((rax + 0x10));
    } while (rax != 0);
    goto label_35;
label_21:
    if (ax != 4) {
        goto label_1;
    }
label_31:
    if ((*((r12 + 0x6a)) & 2) != 0) {
        goto label_36;
    }
label_9:
    r13 = *((rbp + 8));
    if (r13 == 0) {
        goto label_37;
    }
    do {
        r14 = r13;
        r13 = *((r13 + 0x10));
        rdi = *((r14 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r14;
        fcn_00002450 ();
    } while (r13 != 0);
    *((rbp + 8)) = 0;
label_37:
    r10d = 6;
    rsi = r12;
    rdi = rbp;
    *((r12 + 0x68)) = r10w;
    leave_dir ();
    goto label_2;
label_22:
    eax = restore_initial_cwd (rbp);
    if (eax != 0) {
        goto label_38;
    }
    rdi = *((rbp + 0x58));
    if ((*((rbp + 0x48)) & 0x102) == 0) {
        goto label_39;
    }
    if (rdi != 0) {
        hash_free (rdi, rsi);
    }
label_6:
    rax = *((r12 + 0x60));
    r14 = r12 + 0x100;
    *((r12 + 0x48)) = rax;
    memmove (*((rbp + 0x20)), r14, rax + 1);
    rax = strrchr (r14, 0x2f);
    if (rax != 0) {
        if (r14 == rax) {
            goto label_40;
        }
label_12:
        r13 = rax + 1;
        rax = strlen (r13);
        rbx = rax;
        memmove (r14, r13, rax + 1);
        *((r12 + 0x60)) = rbx;
    }
label_11:
    rax = *((rbp + 0x20));
    rdi = rbp;
    *((r12 + 0x38)) = rax;
    *((r12 + 0x30)) = rax;
    setup_dir ();
    eax = *((r12 + 0x68));
    goto label_17;
label_23:
    if (ax == 2) {
        goto label_41;
    }
label_5:
    rdx = *((r12 + 8));
    rsi = r12 + 0x100;
    rax = *((rdx + 0x48));
    rdx = *((rdx + 0x38));
    rdi = rax - 1;
    if (*((rdx + rax - 1)) != 0x2f) {
        rdi = rax;
    }
    rdi += *((rbp + 0x20));
    rax = *((r12 + 0x60));
    rdi++;
    memmove (0x2f, rsi, rax + 1);
    eax = *((r12 + 0x68));
label_17:
    *(rbp) = r12;
    if (ax == 0xb) {
        goto label_42;
    }
label_10:
    if (ax != 1) {
        goto label_2;
    }
label_15:
    if (*((r12 + 0x58)) == 0) {
        rax = *((r12 + 0x70));
        *((rbp + 0x18)) = rax;
    }
    rsi = r12;
    rdi = rbp;
    al = enter_dir ();
    if (al != 0) {
        goto label_2;
    }
    errno_location ();
    r12d = 0;
    *(rax) = 0xc;
    goto label_2;
label_30:
    ax = fts_stat (rdi, r12, 1, rcx, r8);
    *((r12 + 0x68)) = ax;
    if (ax == 1) {
        goto label_43;
    }
    *(rbp) = r12;
    if (ax != 0xb) {
        goto label_2;
    }
label_42:
    rax = *((r12 + 0xa0));
    if (rax == 2) {
        goto label_44;
    }
    if (rax == 1) {
        goto label_2;
    }
    void (*0x289f)() ();
label_19:
    ax = fts_stat (rdi, r12, 0, rcx, r8);
    *((r12 + 0x68)) = ax;
    goto label_2;
label_33:
    dh &= 0xef;
    *((rbp + 0x48)) = edx;
    do {
        r14 = r13;
        r13 = *((r13 + 0x10));
        rdi = *((r14 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r14;
        fcn_00002450 ();
    } while (r13 != 0);
    *((rbp + 8)) = 0;
label_32:
    rax = fts_build (rbp, 3);
    *((rbp + 8)) = rax;
    if (rax != 0) {
        r12 = rax;
        goto label_35;
label_38:
        *((rbp + 0x48)) |= sym._init;
        goto label_3;
label_27:
        eax = *((rbp + 0x48));
        edi = *((r14 + 0x44));
        if ((al & 4) == 0) {
            if ((ah & 2) == 0) {
                goto label_45;
            }
            esi = edi;
            edx = 1;
            rdi = rbp;
            cwd_advance_fd ();
        }
label_14:
        close (*((r14 + 0x44)));
        goto label_4;
label_34:
        r12 = *((rbp + 8));
label_35:
        *((rbp + 8)) = 0;
        goto label_5;
    }
    if ((*((rbp + 0x49)) & 0x20) != 0) {
        goto label_3;
    }
    r9d = *((r12 + 0x40));
    if (r9d != 0) {
        if (*((r12 + 0x68)) == 4) {
            goto label_46;
        }
        r8d = 7;
        *((r12 + 0x68)) = r8w;
    }
label_46:
    rsi = r12;
    rdi = rbp;
    leave_dir ();
    goto label_2;
label_39:
    fcn_00002450 ();
    goto label_6;
label_25:
    rdi = r14;
    fcn_00002450 ();
    errno_location ();
    *(rax) = 0;
    *(rbp) = 0;
    goto label_2;
label_29:
    eax = 7;
    *((r14 + 0x68)) = ax;
    goto label_7;
label_26:
    eax = restore_initial_cwd (rbp);
    if (eax == 0) {
        goto label_4;
    }
label_13:
    rax = errno_location ();
    eax = *(rax);
    *((r14 + 0x40)) = eax;
    *((rbp + 0x48)) |= sym._init;
    goto label_4;
label_24:
    rdx = *((rbp + 0x20));
    rax = *((r14 + 0x48));
    *(rbp) = r14;
    *((rdx + rax)) = 0;
    rax = fts_build (rbp, 3);
    rbx = rax;
    if (rax != 0) {
        goto label_47;
    }
    if ((*((rbp + 0x49)) & 0x20) != 0) {
        goto label_3;
    }
    r14 = *((r13 + 8));
    goto label_8;
label_36:
    close (*((r12 + 0x44)));
    goto label_9;
label_41:
    ax = fts_stat (rbp, r12, 1, rcx, r8);
    *((r12 + 0x68)) = ax;
    if (ax == 1) {
        goto label_48;
    }
label_16:
    esi = 3;
    *((r12 + 0x6c)) = si;
    goto label_5;
label_44:
    ax = fts_stat (rbp, r12, 0, rcx, r8);
    *((r12 + 0x68)) = ax;
    goto label_10;
label_40:
    if (*((r14 + 1)) == 0) {
        goto label_11;
    }
    goto label_12;
label_28:
    eax = fts_safe_changedir (rbp, *((r14 + 8)), 0xffffffff, 0x0000dc7e);
    if (eax == 0) {
        goto label_4;
    }
    goto label_13;
label_47:
    rdi = r13;
    r12 = rbx;
    fcn_00002450 ();
    goto label_5;
label_45:
    eax = fchdir ();
    if (eax != 0) {
        rax = errno_location ();
        eax = *(rax);
        *((r14 + 0x40)) = eax;
        *((rbp + 0x48)) |= sym._init;
    }
    edi = *((r14 + 0x44));
    goto label_14;
label_43:
    eax = *((rbp + 0x48));
    if ((al & 4) == 0) {
        goto label_49;
    }
    *(rbp) = r12;
    goto label_15;
label_48:
    eax = *((rbp + 0x48));
    if ((al & 4) != 0) {
        goto label_16;
    }
    edx = eax;
    edi = *((rbp + 0x2c));
    edx <<= 0xd;
    edx &= 0x20000;
    edx |= 0x90900;
    if ((ah & 2) == 0) {
        goto label_50;
    }
    eax = 0;
    eax = openat_safer (rdi, 0x0000dc7f, rdx, rcx, r8);
label_18:
    *((r12 + 0x44)) = eax;
    if (eax < 0) {
        goto label_51;
    }
    *((r12 + 0x6a)) |= 2;
    goto label_16;
label_49:
    edx = eax;
    edi = *((rbp + 0x2c));
    edx <<= 0xd;
    edx &= 0x20000;
    edx |= 0x90900;
    if ((ah & 2) == 0) {
        goto label_52;
    }
    eax = 0;
    eax = openat_safer (rdi, 0x0000dc7f, rdx, rcx, r8);
    do {
        *((r12 + 0x44)) = eax;
        if (eax < 0) {
            goto label_53;
        }
        *((r12 + 0x6a)) |= 2;
        eax = *((r12 + 0x68));
        goto label_17;
label_52:
        eax = 0;
        eax = open_safer (0x0000dc7f, edx, rdx, rcx);
    } while (1);
label_50:
    eax = 0;
    open_safer (0x0000dc7f, edx, rdx, rcx);
    goto label_18;
label_53:
    rax = errno_location ();
    r11d = 7;
    eax = *(rax);
    *((r12 + 0x68)) = r11w;
    *((r12 + 0x40)) = eax;
    *(rbp) = r12;
    goto label_2;
label_51:
    rax = errno_location ();
    edi = 7;
    eax = *(rax);
    *((r12 + 0x68)) = di;
    *((r12 + 0x40)) = eax;
    goto label_16;
}

/* /tmp/tmp79u99nvo @ 0x68b0 */
 
uint64_t dbg_rpl_fts_close (int64_t arg_18h, signed int64_t arg_58h, uint32_t arg1) {
    rdi = arg1;
    /* int rpl_fts_close(FTS * sp); */
    r12 = rdi;
    rdi = *(rdi);
    if (rdi == 0) {
        goto label_7;
    }
    if (*((rdi + 0x58)) >= 0) {
        goto label_8;
    }
    goto label_9;
    do {
        fcn_00002450 ();
        if (*((rbp + 0x58)) < 0) {
            goto label_5;
        }
label_0:
        rdi = rbp;
label_8:
        rbp = *((rdi + 0x10));
    } while (rbp != 0);
    rbp = *((rdi + 8));
    fcn_00002450 ();
    if (*((rbp + 0x58)) >= 0) {
        goto label_0;
    }
label_5:
    rdi = rbp;
    fcn_00002450 ();
label_7:
    rbx = *((r12 + 8));
    if (rbx == 0) {
        goto label_10;
    }
    do {
        rbx = *((rbx + 0x10));
        rdi = *((rbp + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = rbp;
        fcn_00002450 ();
    } while (rbx != 0);
label_10:
    rdi = *((r12 + 0x10));
    fcn_00002450 ();
    rdi = *((r12 + 0x20));
    fcn_00002450 ();
    eax = *((r12 + 0x48));
    if ((ah & 2) == 0) {
        goto label_11;
    }
    edi = *((r12 + 0x2c));
    if (edi >= 0) {
        goto label_12;
    }
label_2:
    r13d = 0;
label_3:
    rbx = r12 + 0x60;
    while (al == 0) {
        rdi = rbx;
        eax = i_ring_pop ();
        if (eax >= 0) {
            goto label_13;
        }
label_1:
        rdi = rbx;
        al = i_ring_empty ();
    }
    rdi = *((r12 + 0x50));
    if (rdi != 0) {
        hash_free (rdi, rsi);
    }
    rdi = *((r12 + 0x58));
    if ((*((r12 + 0x48)) & 0x102) == 0) {
        goto label_14;
    }
    if (rdi != 0) {
        hash_free (rdi, rsi);
    }
label_4:
    rdi = r12;
    fcn_00002450 ();
    if (r13d != 0) {
        goto label_15;
    }
label_6:
    eax = r13d;
    return eax;
label_13:
    al = close (eax);
    goto label_1;
label_11:
    if ((al & 4) != 0) {
        goto label_2;
    }
    edi = *((r12 + 0x28));
    eax = fchdir ();
    if (eax != 0) {
        goto label_16;
    }
    eax = close (*((r12 + 0x28)));
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    rbx = rax;
    goto label_17;
label_12:
    eax = close (rdi);
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    r13d = *(rax);
    goto label_3;
label_14:
    fcn_00002450 ();
    goto label_4;
label_16:
    rax = errno_location ();
    r13d = *(rax);
    rbx = rax;
    eax = close (*((r12 + 0x28)));
    if (r13d != 0) {
        goto label_3;
    }
    if (eax == 0) {
        goto label_3;
    }
label_17:
    r13d = *(rbx);
    goto label_3;
label_9:
    goto label_5;
label_15:
    errno_location ();
    *(rax) = r13d;
    r13d = 0xffffffff;
    goto label_6;
}

/* /tmp/tmp79u99nvo @ 0x7ea0 */
 
int64_t dbg_hash_clear (uint32_t arg_8h, int64_t arg_18h, int64_t arg_20h, int64_t arg_40h, int64_t arg_48h, uint32_t arg1) {
    rdi = arg1;
    /* void hash_clear(Hash_table * table); */
    r12 = *(rdi);
    if (r12 < *((rdi + 8))) {
        goto label_0;
    }
    goto label_1;
    do {
        r12 += 0x10;
        if (*((rbp + 8)) <= r12) {
            goto label_1;
        }
label_0:
    } while (*(r12) == 0);
    rbx = *((r12 + 8));
    rdx = *((rbp + 0x40));
    if (rbx != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        rbx = rax;
label_2:
        if (rdx != 0) {
            rdi = *(rbx);
            void (*rdx)() ();
            rdx = *((rbp + 0x40));
        }
        rax = *((rbx + 8));
        rcx = *((rbp + 0x48));
        *(rbx) = 0;
        *((rbx + 8)) = rcx;
        *((rbp + 0x48)) = rbx;
    } while (rax != 0);
label_3:
    if (rdx != 0) {
        rdi = *(r12);
        void (*rdx)() ();
    }
    *(r12) = 0;
    r12 += 0x10;
    *((r12 - 8)) = 0;
    if (*((rbp + 8)) > r12) {
        goto label_0;
    }
label_1:
    *((rbp + 0x18)) = 0;
    *((rbp + 0x20)) = 0;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x7d70 */
 
int64_t dbg_hash_reset_tuning (Hash_tuning * tuning) {
    rdi = tuning;
    /* void hash_reset_tuning(Hash_tuning * tuning); */
    rax = 0x3f80000000000000;
    *((rdi + 0x10)) = 0;
    *(rdi) = rax;
    rax = 0x3fb4fdf43f4ccccd;
    *((rdi + 8)) = rax;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x7c60 */
 
int64_t dbg_hash_get_entries (void ** buffer, size_t buffer_size, Hash_table const * table) {
    rsi = buffer;
    rdx = buffer_size;
    rdi = table;
    /* size_t hash_get_entries(Hash_table const * table,void ** buffer,size_t buffer_size); */
    r9 = *(rdi);
    eax = 0;
    if (r9 >= *((rdi + 8))) {
        goto label_2;
    }
    do {
        if (*(r9) != 0) {
            goto label_3;
        }
label_0:
        r9 += 0x10;
    } while (*((rdi + 8)) > r9);
    return eax;
label_3:
    rcx = r9;
    goto label_4;
label_1:
    r8 = *(rcx);
    rax++;
    *((rsi + rax*8 - 8)) = r8;
    rcx = *((rcx + 8));
    if (rcx == 0) {
        goto label_0;
    }
label_4:
    if (rdx > rax) {
        goto label_1;
    }
label_2:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4a70 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp79u99nvo @ 0xb3b0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xb570 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x4570 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2690 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp79u99nvo @ 0x8040 */
 
int64_t hash_rehash (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t canary;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdi = rsi;
    r12 = *((rbp + 0x28));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    esi = *((r12 + 0x10));
    xmm0 = *((r12 + 8));
    rax = compute_bucket_size_isra_0 ();
    if (rax == 0) {
        goto label_1;
    }
    rbx = rax;
    if (*((rbp + 0x10)) == rax) {
        goto label_2;
    }
    rax = calloc (rax, 0x10);
    *(rsp) = rax;
    if (rax == 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = rbx;
    rbx <<= 4;
    r13 = rsp;
    edx = 0;
    rax += rbx;
    rsi = rbp;
    rdi = r13;
    *((rsp + 0x28)) = r12;
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x30));
    *((rsp + 0x18)) = 0;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0x38));
    *((rsp + 0x20)) = 0;
    *((rsp + 0x38)) = rax;
    rax = *((rbp + 0x40));
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0x48));
    *((rsp + 0x48)) = rax;
    eax = transfer_entries ();
    r12d = eax;
    if (al != 0) {
        goto label_3;
    }
    rax = *((rsp + 0x48));
    edx = 1;
    rsi = r13;
    rdi = rbp;
    *((rbp + 0x48)) = rax;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x28be)() ();
    }
    edx = 0;
    rsi = r13;
    rdi = rbp;
    al = transfer_entries ();
    if (al == 0) {
        void (*0x28be)() ();
    }
    rdi = *(rsp);
    fcn_00002450 ();
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        eax = r12d;
        return rax;
label_2:
        r12d = 1;
    } while (1);
label_1:
    r12d = 0;
    goto label_0;
label_3:
    rdi = *(rbp);
    fcn_00002450 ();
    rax = *(rsp);
    *(rbp) = rax;
    rax = *((rsp + 8));
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x10));
    *((rbp + 0x10)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    rax = *((rsp + 0x48));
    *((rbp + 0x48)) = rax;
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xb2a0 */
 
uint32_t dbg_can_write_any_file (void) {
    /* _Bool can_write_any_file(); */
    if (*(0x00013259) != 0) {
        eax = *(0x00013258);
        return eax;
    }
    eax = geteuid ();
    *(0x00013259) = 1;
    al = (eax == 0) ? 1 : 0;
    *(0x00013258) = al;
    return eax;
}

/* /tmp/tmp79u99nvo @ 0x2610 */
 
void geteuid (void) {
    __asm ("bnd jmp qword [reloc.geteuid]");
}

/* /tmp/tmp79u99nvo @ 0xbb30 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2530)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0xb490 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xaab0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0x4d90 */
 
int64_t dbg_freadahead (FILE * fp) {
    rdi = fp;
    /* size_t freadahead(FILE * fp); */
    rcx = *((rdi + 0x20));
    eax = 0;
    if (*((rdi + 0x28)) <= rcx) {
        rax = *((rdi + 0x10));
        rax -= *((rdi + 8));
        if ((*(rdi) & 0x100) == 0) {
            goto label_0;
        }
        rdx = *((rdi + 0x58));
        rdx -= *((rdi + 0x48));
        rax += rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xaa00 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2906)() ();
    }
    if (rax == 0) {
        void (*0x2906)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x78d0 */
 
int64_t hash_get_max_bucket_length (int64_t arg1) {
    rdi = arg1;
    rcx = *(rdi);
    rsi = *((rdi + 8));
    r8d = 0;
    if (rcx < rsi) {
        goto label_0;
    }
    goto label_1;
    do {
        rcx += 0x10;
        if (rcx >= rsi) {
            goto label_1;
        }
label_0:
    } while (*(rcx) == 0);
    rax = *((rcx + 8));
    edx = 1;
    if (rax == 0) {
        goto label_2;
    }
    do {
        rax = *((rax + 8));
        rdx++;
    } while (rax != 0);
label_2:
    if (r8 < rdx) {
        r8 = rdx;
    }
    rcx += 0x10;
    if (rcx < rsi) {
        goto label_0;
    }
label_1:
    rax = r8;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x7220 */
 
uint64_t dbg_rpl_fts_children (int64_t arg_8h, int64_t arg_2ch, int64_t arg_48h, int64_t arg_49h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* FTSENT * rpl_fts_children(FTS * sp,int instr); */
    r13d = esi;
    rax = errno_location ();
    r14 = rax;
    if ((r13d & 0xffffefff) != 0) {
        goto label_3;
    }
    r15 = *(rbp);
    *(rax) = 0;
    if ((*((rbp + 0x49)) & 0x20) != 0) {
        goto label_4;
    }
    edx = *((r15 + 0x68));
    if (dx == 9) {
        goto label_5;
    }
    eax = 0;
    if (dx != 1) {
        goto label_2;
    }
    rbx = *((rbp + 8));
    if (rbx == 0) {
        goto label_6;
    }
    do {
        r12 = rbx;
        rbx = *((rbx + 0x10));
        rdi = *((r12 + 0x18));
        if (rdi != 0) {
            closedir ();
        }
        rdi = r12;
        fcn_00002450 ();
    } while (rbx != 0);
label_6:
    r12d = 1;
    if (r13d == 0x1000) {
        *((rbp + 0x48)) |= 0x1000;
        r12d = 2;
    }
    if (*((r15 + 0x58)) == 0) {
        rax = *((r15 + 0x30));
        if (*(rax) == 0x2f) {
            goto label_7;
        }
        eax = *((rbp + 0x48));
        if ((al & 4) == 0) {
            goto label_8;
        }
    }
label_7:
    rax = fts_build (rbp, r12d);
    *((rbp + 8)) = rax;
    do {
label_2:
        return rax;
label_8:
        edx = eax;
        edi = *((rbp + 0x2c));
        edx <<= 0xd;
        edx &= 0x20000;
        edx |= 0x90900;
        if ((ah & 2) == 0) {
            goto label_9;
        }
        eax = 0;
        eax = openat_safer (rdi, 0x0000dc7f, rdx, rcx, r8);
        r13d = eax;
label_0:
        if (r13d < 0) {
            goto label_10;
        }
        rax = fts_build (rbp, r12d);
        *((rbp + 8)) = rax;
        if ((*((rbp + 0x49)) & 2) != 0) {
            goto label_11;
        }
        edi = r13d;
        eax = fchdir ();
        if (eax != 0) {
            goto label_12;
        }
        close (r13d);
label_1:
        rax = *((rbp + 8));
    } while (1);
label_3:
    *(rax) = 0x16;
    eax = 0;
    return rax;
label_4:
    eax = 0;
    return rax;
label_5:
    rax = *((r15 + 0x10));
    return rax;
label_9:
    eax = 0;
    eax = open_safer (0x0000dc7f, edx, rdx, rcx);
    r13d = eax;
    goto label_0;
label_11:
    edx = 1;
    esi = r13d;
    rdi = rbp;
    eax = cwd_advance_fd ();
    goto label_1;
label_10:
    *((rbp + 8)) = 0;
    eax = 0;
    goto label_2;
label_12:
    ebx = *(r14);
    eax = close (r13d);
    eax = 0;
    *(r14) = ebx;
    goto label_2;
}

/* /tmp/tmp79u99nvo @ 0x4660 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp79u99nvo @ 0xb7e0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xbf80 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0xbbc0)() ();
}

/* /tmp/tmp79u99nvo @ 0x4950 */
 
void dbg_close_stdin_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdin_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp79u99nvo @ 0x8450 */
 
int64_t dbg_hash_remove (int64_t arg_8h, int64_t arg1) {
    hash_entry * bucket;
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_remove(Hash_table * table, const * entry); */
    ecx = 1;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rdx = rsp;
    rax = hash_find_entry ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rax = *(rsp);
    *((rbx + 0x20))--;
    while (rax <= 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
        rax = *((rbx + 0x18));
        rax--;
        *((rbx + 0x18)) = rax;
        if (rax < 0) {
            goto label_5;
        }
        xmm5 = 0;
        __asm ("cvtsi2ss xmm5, rax");
        rax = *((rbx + 0x10));
        if (rax < 0) {
            goto label_6;
        }
label_1:
        xmm4 = 0;
        __asm ("cvtsi2ss xmm4, rax");
label_2:
        rax = *((rbx + 0x28));
        xmm0 = *(rax);
        __asm ("mulss xmm0, xmm4");
        __asm ("comiss xmm0, xmm5");
    }
    rdi = rbx;
    check_tuning ();
    rax = *((rbx + 0x28));
    xmm0 = *(rax);
    __asm ("mulss xmm0, xmm4");
    __asm ("comiss xmm0, xmm5");
    if (rax <= 0) {
        goto label_0;
    }
    __asm ("mulss xmm4, dword [rax + 4]");
    if (*((rax + 0x10)) == 0) {
        __asm ("mulss xmm4, dword [rax + 8]");
    }
    __asm ("comiss xmm4, dword [0x0000e188]");
    if (*((rax + 0x10)) >= 0) {
        goto label_7;
    }
    __asm ("cvttss2si rsi, xmm4");
label_3:
    rdi = rbx;
    al = hash_rehash ();
    if (al != 0) {
        goto label_0;
    }
    rbp = *((rbx + 0x48));
    if (rbp == 0) {
        goto label_8;
    }
    do {
        rdi = rbp;
        rbp = *((rbp + 8));
        rax = fcn_00002450 ();
    } while (rbp != 0);
label_8:
    *((rbx + 0x48)) = 0;
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm5 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rbx + 0x10));
    __asm ("cvtsi2ss xmm5, rdx");
    __asm ("addss xmm5, xmm5");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm4 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2ss xmm4, rdx");
    __asm ("addss xmm4, xmm4");
    goto label_2;
label_7:
    __asm ("subss xmm4, dword [0x0000e188]");
    __asm ("cvttss2si rsi, xmm4");
    __asm ("btc rsi, 0x3f");
    goto label_3;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa1b0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0xa0c0)() ();
}

/* /tmp/tmp79u99nvo @ 0xb970 */
 
int64_t dbg_yesno (void) {
    char * response;
    size_t response_size;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    /* _Bool yesno(); */
    edx = 0xa;
    r12d = 0;
    rcx = stdin;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rsi = rsp + 0x10;
    rdi = rsp + 8;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    rax = getdelim ();
    if (rax <= 0) {
        goto label_0;
    }
    rdi = *((rsp + 8));
    rax = rdi + rax - 1;
    while (1) {
        eax = rpmatch ();
        r12b = (eax > 0) ? 1 : 0;
label_0:
        rdi = *((rsp + 8));
        fcn_00002450 ();
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        eax = r12d;
        return rax;
        *(rax) = 0;
        rdi = *((rsp + 8));
    }
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x78a0 */
 
int64_t hash_get_n_buckets (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x10));
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xa290 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0x7bd0 */
 
uint64_t hash_get_next (int64_t arg_8h, uint32_t arg_10h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rsi = *((rdi + 0x10));
    rdi = rbx;
    rax = uint64_t (*rbp + 0x30)(uint64_t) (rbx);
    if (rax >= *((rbp + 0x10))) {
        void (*0x28b9)() ();
    }
    rax <<= 4;
    rax += *(rbp);
    rdx = rax;
    while (rcx != rbx) {
        if (rdx == 0) {
            goto label_0;
        }
        rcx = *(rdx);
        rdx = *((rdx + 8));
    }
    if (rdx != 0) {
        goto label_1;
    }
label_0:
    rdx = *((rbp + 8));
    while (rdx > rax) {
        r8 = *(rax);
        if (r8 != 0) {
            goto label_2;
        }
        rax += 0x10;
    }
    r8d = 0;
label_2:
    rax = r8;
    return rax;
label_1:
    r8 = *(rdx);
    rax = r8;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x2910 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    rm_options x;
    stat st;
    uint32_t var_8h;
    uint32_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    uint32_t var_19h;
    int64_t var_1ah;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_29h;
    int64_t var_2ah;
    int64_t var_30h;
    int64_t var_c8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r14 = obj_long_opts;
    r13 = "dfirvIR";
    r12 = 0x0000d081;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0xc8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000e601);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x0000db10;
    textdomain (r12, rsi);
    rdi = dbg_close_stdin;
    atexit ();
    esi = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x14)) = 4;
    *((rsp + 0x18)) = si;
    *((rsp + 0x1a)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    eax = isatty (0);
    *((rsp + 8)) = 1;
    rsp + 0x29 = (eax != 0) ? 1 : 0;
    edi = 0;
    r15d = 0;
    *((rsp + 0x2a)) = di;
label_0:
    r8d = 0;
    rcx = r14;
    rdx = r13;
    rsi = rbx;
    edi = ebp;
    eax = getopt_long ();
    if (eax == 0xffffffff) {
        goto label_9;
    }
    if (eax <= 0x84) {
        if (eax <= 0x48) {
            goto label_10;
        }
        eax -= 0x49;
        if (eax > 0x3b) {
            goto label_1;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (60 cases) at 0xdb10 */
        void (*rax)() ();
    }
label_1:
    r13d = 1;
    r12 = rsp + 0x30;
    do {
        if (ebp <= r13d) {
            goto label_8;
        }
        r14 = *((rbx + r13*8));
        if (*(r14) == 0x2d) {
            goto label_11;
        }
label_6:
        r13++;
    } while (1);
    *((rsp + 0x19)) = 1;
    goto label_0;
    rsi = optarg;
    if (rsi != 0) {
        goto label_12;
    }
label_3:
    *((rsp + 0x14)) = 3;
    r15d = 0;
    *((rsp + 0x10)) = 0;
    goto label_0;
label_2:
    *((rsp + 0x14)) = 4;
    r15d = 1;
    *((rsp + 0x10)) = 0;
    goto label_0;
    *((rsp + 0x1a)) = 1;
    goto label_0;
    *((rsp + 0x14)) = 5;
    r15d = 0;
    *((rsp + 0x10)) = 1;
    goto label_0;
    *((rsp + 0x2a)) = 1;
    goto label_0;
    *((rsp + 0x29)) = 1;
    goto label_0;
    r8 = optarg;
    if (r8 == 0) {
        goto label_13;
    }
    rdi = r8;
    *((rsp + 8)) = r8;
    eax = strcmp (rdi, 0x0000d0e3);
    r8 = *((rsp + 8));
    if (eax != 0) {
        goto label_14;
    }
    *((rsp + 0x28)) = 1;
    *((rsp + 8)) = 1;
    goto label_0;
    rax = *(obj.optind);
    eax = strcmp (*((rbx + rax*8 - 8)), "--no-preserve-root");
    if (eax != 0) {
        goto label_15;
    }
    *((rsp + 8)) = 0;
    goto label_0;
    *((rsp + 0x18)) = 1;
    goto label_0;
label_10:
    if (eax == 0xffffff7d) {
        rax = "Jim Meyering";
        rax = "Richard M. Stallman";
        eax = 0;
        version_etc (*(obj.stdout), 0x0000d004, "GNU coreutils", *(obj.Version), "Paul Rubin", "David MacKenzie");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_1;
    }
    rax = dbg_usage (0);
label_12:
    _xargmatch_internal ("--interactive", rsi, obj.interactive_args, obj.interactive_types, 4, *(obj.argmatch_die));
    rcx = obj_interactive_types;
    eax = *((rcx + rax*4));
    if (eax == 1) {
        goto label_2;
    }
    if (eax == 2) {
        goto label_3;
    }
    if (eax != 0) {
        goto label_0;
    }
    *((rsp + 0x14)) = 5;
    r15d = 0;
    goto label_0;
label_13:
    *((rsp + 8)) = 1;
    goto label_0;
label_9:
    if (*(obj.optind) < ebp) {
        goto label_16;
    }
    if (*((rsp + 0x10)) == 0) {
        goto label_17;
    }
    do {
        eax = 0;
label_4:
        rdx = *((rsp + 0xc8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_18;
        }
        return rax;
label_16:
        if (*((rsp + 0x19)) != 0) {
            if (*((rsp + 8)) != 0) {
                goto label_19;
            }
        }
label_7:
        rdx = *(obj.optind);
        r12 = rbx + rdx*8;
        if (r15b == 0) {
            goto label_20;
        }
        ebp -= edx;
        rbp = (int64_t) ebp;
        if (*((rsp + 0x19)) != 0) {
            goto label_21;
        }
        if (rbp <= 3) {
            goto label_20;
        }
        rdx = "%s: remove %lu arguments? ";
        r8d = 5;
        rcx = rbp;
        edi = 0;
        rsi = "%s: remove %lu argument? ";
        r13 = program_name;
        rax = dcngettext ();
        rdx = rax;
label_5:
        rdi = stderr;
        r8 = rbp;
        rcx = r13;
        eax = 0;
        esi = 1;
        fprintf_chk ();
        al = yesno ();
    } while (al == 0);
label_20:
    eax = rm (r12, rsp + 0x10, rdx, rcx);
    edx = rax - 2;
    al = (eax == 4) ? 1 : 0;
    eax = (int32_t) al;
    if (edx <= 2) {
        goto label_4;
    }
    assert_fail ("VALID_STATUS (status)", "src/rm.c", 0x173, "main");
label_21:
    rdx = "%s: remove %lu arguments recursively? ";
    r8d = 5;
    rcx = rbp;
    edi = 0;
    rsi = "%s: remove %lu argument recursively? ";
    r13 = program_name;
    rax = dcngettext ();
    rdx = rax;
    goto label_5;
label_11:
    if (*((r14 + 1)) == 0) {
        goto label_6;
    }
    eax = lstat (r14, r12);
    if (eax != 0) {
        goto label_6;
    }
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    rdx = r14;
    esi = 3;
    edi = 1;
    r12 = rax;
    rax = quotearg_n_style ();
    r13 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Try '%s ./%s' to remove the file %s.\n");
    r9 = r12;
    r8 = rbp;
    rcx = r13;
    rdi = stderr;
    rdx = rax;
    esi = 1;
    eax = 0;
    fprintf_chk ();
label_8:
    dbg_usage (1);
label_19:
    rax = get_root_dev_ino (0x000130f0);
    *((rsp + 0x20)) = rax;
    if (rax != 0) {
        goto label_7;
    }
    rsi = 0x0000e645;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to get attributes of %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_18:
    stack_chk_fail ();
label_15:
    edx = 5;
    rax = dcgettext (0, "you may not abbreviate the --no-preserve-root option");
    eax = 0;
    error (1, 0, rax);
label_14:
    rsi = r8;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "unrecognized --preserve-root argument: %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_17:
    edx = 5;
    rax = dcgettext (0, "missing operand");
    eax = 0;
    error (0, 0, rax);
    goto label_8;
}

/* /tmp/tmp79u99nvo @ 0x2fd0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Remove (unlink) the FILE(s).\n\n  -f, --force           ignore nonexistent files and arguments, never prompt\n  -i                    prompt before every removal\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -I                    prompt once before removing more than three files, or\n                          when removing recursively; less intrusive than -i,\n                          while still giving protection against most mistakes\n      --interactive[=WHEN]  prompt according to WHEN: never, once (-I), or\n                          always (-i); without WHEN, prompt always\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --one-file-system  when removing a hierarchy recursively, skip any\n                          directory that is on a file system different from\n                          that of the corresponding command line argument\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --no-preserve-root  do not treat '/' specially\n      --preserve-root[=all]  do not remove '/' (default);\n                              with 'all', reject any command line argument\n                              on a separate device from its parent\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -r, -R, --recursive   remove directories and their contents recursively\n  -d, --dir             remove empty directories\n  -v, --verbose         explain what is being done\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nBy default, rm does not remove directories.  Use the --recursive (-r or -R)\noption to remove each listed directory, too, along with all of its contents.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = program_name;
    edx = 5;
    rax = dcgettext (0, "\nTo remove a file whose name starts with a '-', for example '-foo',\nuse one of these commands:\n  %s -- -foo\n\n  %s ./-foo\n");
    rcx = r12;
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\nNote that if you use rm to remove a file, it might be possible to recover\nsome of its contents, given sufficient expertise and/or time.  For greater\nassurance that the contents are truly unrecoverable, consider using shred(1).\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000d007;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x72;
    *((rsp + 8)) = rax;
    rax = 0x0000d081;
    edi = 0x6d;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0000d004;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000d08b, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x0000d023;
    printf_chk ();
    rax = 0x0000e601;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000d08b, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0000d004;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x0000d023;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0000d004;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmp79u99nvo @ 0x9f60 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xa650 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xbbc0 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = section__dynsym;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa480 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x28ed)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x7b80 */
 
int64_t hash_get_first (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rax = *(rdi);
    rdx = *((rdi + 8));
    if (rax < rdx) {
        goto label_1;
    }
    void (*0x28b3)() ();
    do {
        rax += 0x10;
        if (rax >= rdx) {
            goto label_2;
        }
label_1:
        r8 = *(rax);
    } while (r8 == 0);
    rax = r8;
    return rax;
label_0:
    r8d = 0;
    rax = r8;
    return rax;
label_2:
    return hash_get_first_cold ();
}

/* /tmp/tmp79u99nvo @ 0xa360 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x28e3)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x4c70 */
 
uint64_t dbg_file_name_concat (void) {
    int64_t var_7h;
    int64_t var_8h;
    /* char * file_name_concat(char const * dir,char const * base,char ** base_in_result); */
    rax = mfile_name_concat (rdi, rsi, rdx);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xaaf0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0x71f0 */
 
int64_t dbg_rpl_fts_set (int64_t arg2, uint32_t arg3) {
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fts_set(FTS * sp,FTSENT * p,int instr); */
    if (edx <= 4) {
        *((rsi + 0x6c)) = dx;
        eax = 0;
        return eax;
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 1;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x85e0 */
 
void dbg_hash_delete (int64_t arg_8h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* void * hash_delete(Hash_table * table, const * entry); */
    return void (*0x8450)() ();
}

/* /tmp/tmp79u99nvo @ 0xab10 */
 
int64_t dbg_get_root_dev_ino (int64_t arg1) {
    stat statbuf;
    int64_t var_8h;
    int64_t var_98h;
    rdi = arg1;
    /* dev_ino * get_root_dev_ino(dev_ino * root_d_i); */
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    eax = lstat (0x0000e645, rsp);
    if (eax != 0) {
        goto label_0;
    }
    rax = *((rsp + 8));
    *(rbx) = rax;
    rax = *(rsp);
    *((rbx + 8)) = rax;
    rax = rbx;
    do {
        rdx = *((rsp + 0x98));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
label_0:
        eax = 0;
    } while (1);
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xa000 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x28d8)() ();
    }
    if (rdx == 0) {
        void (*0x28d8)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb930 */
 
int32_t dbg_cycle_warning_required (FTSENT const * ent, FTS const * fts) {
    rsi = ent;
    rdi = fts;
    /* _Bool cycle_warning_required(FTS const * fts,FTSENT const * ent); */
    eax = *((rdi + 0x48));
    r8d = 1;
    eax &= 0x11;
    if (eax != 0x10) {
        r8d = 0;
        if (eax == 0x11) {
            goto label_0;
        }
    }
    eax = r8d;
    return eax;
label_0:
    r8b = (*((rsi + 0x58)) != 0) ? 1 : 0;
    eax = r8d;
    return eax;
}

/* /tmp/tmp79u99nvo @ 0xa510 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00013230]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00013240]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0xb700 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xb780 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x4b80 */
 
int32_t dbg_file_type (uint32_t arg1) {
    rdi = arg1;
    /* char const * file_type(stat const * st); */
    eax = *((rdi + 0x18));
    edx = 5;
    eax &= 0xf000;
    if (eax != 0x8000) {
        if (eax == 0x4000) {
            goto label_0;
        }
        if (eax == sym.set_custom_quoting) {
            goto label_1;
        }
        if (eax == 0x6000) {
            goto label_2;
        }
        if (eax == sym._init) {
            goto label_3;
        }
        if (eax == 0x1000) {
            goto label_4;
        }
        if (eax == 0xc000) {
            goto label_5;
        }
        rsi = "weird file";
        edi = 0;
        void (*0x2550)() ();
    }
    if (*((rdi + 0x30)) == 0) {
        rsi = "regular empty file";
        edi = 0;
        void (*0x2550)() ();
    }
    rsi = "regular file";
    edi = 0;
    void (*0x2550)() ();
label_3:
    rsi = "character special file";
    edi = 0;
    void (*0x2550)() ();
label_0:
    rsi = "directory";
    edi = 0;
    void (*0x2550)() ();
label_5:
    rsi = "socket";
    edi = 0;
    void (*0x2550)() ();
label_1:
    rsi = "symbolic link";
    edi = 0;
    void (*0x2550)() ();
label_2:
    rsi = "block special file";
    edi = 0;
    void (*0x2550)() ();
label_4:
    rsi = "fifo";
    edi = 0;
    return dcgettext ();
}

/* /tmp/tmp79u99nvo @ 0x8620 */
 
uint32_t dbg_i_ring_push (I_ring * ir, int32_t val) {
    rdi = ir;
    rsi = val;
    /* int i_ring_push(I_ring * ir,int val); */
    eax = *((rdi + 0x1c));
    edx = *((rdi + 0x14));
    eax ^= 1;
    eax = (int32_t) al;
    edx += eax;
    edx &= 3;
    ecx = edx;
    r8d = *((rdi + rcx*4));
    *((rdi + rcx*4)) = esi;
    ecx = *((rdi + 0x18));
    *((rdi + 0x14)) = edx;
    if (ecx == edx) {
        eax += ecx;
        eax &= 3;
        *((rdi + 0x18)) = eax;
    }
    *((rdi + 0x1c)) = 0;
    eax = r8d;
    return eax;
}

/* /tmp/tmp79u99nvo @ 0xa260 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp79u99nvo @ 0xb8e0 */
 
uint64_t dbg_xfts_open (int64_t arg2) {
    rsi = arg2;
    /* FTS * xfts_open(char * const * argv,int options,int (*)() compar); */
    esi |= 0x200;
    rax = rpl_fts_open (rdi, rsi, rdx, rcx);
    if (rax != 0) {
        return rax;
    }
    rax = errno_location ();
    if (*(rax) != 0x16) {
        xalloc_die ();
    }
    return assert_fail ("errno != EINVAL", "lib/xfts.c", 0x29, "xfts_open");
}

/* /tmp/tmp79u99nvo @ 0x25f0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp79u99nvo @ 0xb420 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x7cb0 */
 
int64_t dbg_hash_do_for_each (uint32_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t hash_do_for_each(Hash_table const * table,Hash_processor processor,void * processor_data); */
    r14 = *(rdi);
    if (r14 >= *((rdi + 8))) {
        goto label_3;
    }
    r15 = rdi;
    r13 = rdx;
    r12d = 0;
    do {
        rdi = *(r14);
        if (rdi != 0) {
            goto label_4;
        }
label_0:
        r14 += 0x10;
    } while (*((r15 + 8)) > r14);
label_2:
    rax = r12;
    return rax;
label_4:
    rbx = r14;
    goto label_5;
label_1:
    rbx = *((rbx + 8));
    r12++;
    if (rbx == 0) {
        goto label_0;
    }
    rdi = *(rbx);
label_5:
    rsi = r13;
    al = void (*rbp)() ();
    if (al != 0) {
        goto label_1;
    }
    goto label_2;
label_3:
    r12d = 0;
    goto label_2;
}

/* /tmp/tmp79u99nvo @ 0x4dc0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x27c0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp79u99nvo @ 0xbe30 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x9fa0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb600 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp79u99nvo @ 0xb200 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp79u99nvo @ 0x26b0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp79u99nvo @ 0x4a60 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name_1) = rdi;
}

/* /tmp/tmp79u99nvo @ 0xba10 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp79u99nvo @ 0x24b0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp79u99nvo @ 0xabf0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000e5a8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000e5bb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000e890;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xe890 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2830)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2830)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2830)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0x4960 */
 
uint64_t dbg_close_stdin (void) {
    /* void close_stdin(); */
    rbp = stdin;
    rdi = stdin;
    rax = freadahead ();
    if (rax != 0) {
        goto label_2;
    }
    eax = close_stream (rbp);
    if (eax != 0) {
        goto label_1;
    }
    do {
        void (*0x4a80)() ();
label_2:
        eax = rpl_fseeko (rbp, 0, 1, rcx);
        rdi = stdin;
        if (eax == 0) {
            goto label_3;
        }
label_0:
        eax = close_stream (rdi);
    } while (eax == 0);
label_1:
    edx = 5;
    rax = dcgettext (0, "error closing file");
    r13 = file_name;
    r12 = rax;
    rax = errno_location ();
    if (r13 == 0) {
        goto label_4;
    }
    rax = quotearg_colon (r13, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbp), "%s: %s");
    close_stdout ();
    do {
        rax = exit (*(obj.exit_failure));
label_4:
        rcx = r12;
        eax = 0;
        error (0, *(rax), 0x0000e00a);
        close_stdout ();
    } while (1);
label_3:
    eax = rpl_fflush (rdi);
    rdi = stdin;
    if (eax == 0) {
        goto label_0;
    }
    close_stream (rdi);
    goto label_1;
}

/* /tmp/tmp79u99nvo @ 0xab90 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb080 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp79u99nvo @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    *(rax) += al;
    *(rax) += al;
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xb760 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x78c0 */
 
int64_t hash_get_n_entries (int64_t arg1) {
    rdi = arg1;
    rax = *((rdi + 0x20));
    return rax;
}

/* /tmp/tmp79u99nvo @ 0xc0d0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp79u99nvo @ 0xb460 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0xb820 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26d0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp79u99nvo @ 0x4860 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp79u99nvo @ 0x0 */
 
int64_t libc_start_main (func init, func main, char ** ubp_av) {
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x17)) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = uint64_t (*rdx)() ();
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax -= 0x400006c;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += bl;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al -= 2;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += esp;
    *(rax) += al;
    *(rcx) += esp;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = *(0xa00000000000003d);
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmp79u99nvo @ 0x24a0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp79u99nvo @ 0x24c0 */
 
void unlinkat (void) {
    __asm ("bnd jmp qword [reloc.unlinkat]");
}

/* /tmp/tmp79u99nvo @ 0x24d0 */
 
void qsort (void) {
    __asm ("bnd jmp qword [reloc.qsort]");
}

/* /tmp/tmp79u99nvo @ 0x24e0 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmp79u99nvo @ 0x2500 */
 
void faccessat (void) {
    __asm ("bnd jmp qword [reloc.faccessat]");
}

/* /tmp/tmp79u99nvo @ 0x2510 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmp79u99nvo @ 0x2520 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp79u99nvo @ 0x2530 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp79u99nvo @ 0x2540 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp79u99nvo @ 0x2560 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp79u99nvo @ 0x2580 */
 
void openat (void) {
    __asm ("bnd jmp qword [reloc.openat]");
}

/* /tmp/tmp79u99nvo @ 0x25a0 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp79u99nvo @ 0x25b0 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp79u99nvo @ 0x25c0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp79u99nvo @ 0x25e0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp79u99nvo @ 0x2600 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp79u99nvo @ 0x2630 */
 
void closedir (void) {
    __asm ("bnd jmp qword [reloc.closedir]");
}

/* /tmp/tmp79u99nvo @ 0x2640 */
 
void lstat (void) {
    __asm ("bnd jmp qword [reloc.lstat]");
}

/* /tmp/tmp79u99nvo @ 0x2650 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp79u99nvo @ 0x2680 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.__getdelim]");
}

/* /tmp/tmp79u99nvo @ 0x26a0 */
 
void dirfd (void) {
    __asm ("bnd jmp qword [reloc.dirfd]");
}

/* /tmp/tmp79u99nvo @ 0x26c0 */
 
void rpmatch (void) {
    __asm ("bnd jmp qword [reloc.rpmatch]");
}

/* /tmp/tmp79u99nvo @ 0x26e0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp79u99nvo @ 0x26f0 */
 
void readdir (void) {
    __asm ("bnd jmp qword [reloc.readdir]");
}

/* /tmp/tmp79u99nvo @ 0x2710 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp79u99nvo @ 0x2730 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp79u99nvo @ 0x2740 */
 
void fchdir (void) {
    __asm ("bnd jmp qword [reloc.fchdir]");
}

/* /tmp/tmp79u99nvo @ 0x2780 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmp79u99nvo @ 0x2790 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmp79u99nvo @ 0x27b0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmp79u99nvo @ 0x27c0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp79u99nvo @ 0x27d0 */
 
void dcngettext (void) {
    __asm ("bnd jmp qword [reloc.dcngettext]");
}

/* /tmp/tmp79u99nvo @ 0x27f0 */
 
void fstatfs (void) {
    __asm ("bnd jmp qword [reloc.fstatfs]");
}

/* /tmp/tmp79u99nvo @ 0x2800 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp79u99nvo @ 0x2840 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp79u99nvo @ 0x2850 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp79u99nvo @ 0x2860 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmp79u99nvo @ 0x2870 */
 
void fstatat (void) {
    __asm ("bnd jmp qword [reloc.fstatat]");
}

/* /tmp/tmp79u99nvo @ 0x2880 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp79u99nvo @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1072 named .plt */
    __asm ("bnd jmp qword [0x00012db8]");
}

/* /tmp/tmp79u99nvo @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp79u99nvo @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}
