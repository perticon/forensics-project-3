
int32_t fun_2870(void** rdi, void** rsi, void** rdx, ...);

void** fun_2480();

/* cache_fstatat.constprop.0 */
int32_t cache_fstatat_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    int32_t eax8;
    void** rax9;
    void** rcx10;
    void** rdx11;

    rax7 = *reinterpret_cast<void***>(rdx + 48);
    if (rax7 == 0xffffffffffffffff) {
        eax8 = fun_2870(rdi, rsi, rdx, rdi, rsi, rdx);
        if (!eax8) {
            rax7 = *reinterpret_cast<void***>(rdx + 48);
            goto addr_343e_4;
        } else {
            *reinterpret_cast<void***>(rdx + 48) = reinterpret_cast<void**>(0xfffffffffffffffe);
            rax9 = fun_2480();
            rcx10 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rax9))));
            *reinterpret_cast<void***>(rdx + 8) = rcx10;
            rdx11 = rcx10;
        }
    } else {
        addr_343e_4:
        if (reinterpret_cast<signed char>(rax7) < reinterpret_cast<signed char>(0)) {
            rax9 = fun_2480();
            rdx11 = *reinterpret_cast<void***>(rdx + 8);
        } else {
            return 0;
        }
    }
    *reinterpret_cast<void***>(rax9) = rdx11;
    return -1;
}

int64_t g28;

uint32_t fun_2590();

int32_t fun_2620();

void** quotearg_style(int64_t rdi, void** rsi, ...);

void** program_name = reinterpret_cast<void**>(0);

void** fun_2550();

void** stderr = reinterpret_cast<void**>(0);

void fun_2830(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, int64_t* a8, ...);

void fun_27a0();

void** file_type(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

unsigned char yesno(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

signed char can_write_any_file(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

int32_t fun_2500(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

int32_t fun_2580(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9, int32_t a7, int64_t* a8, void* a9);

void** fun_27e0(void** rdi, void** rsi, ...);

struct s0 {
    void** f0;
    signed char[17] pad18;
    unsigned char f12;
    void** f13;
    signed char f14;
};

struct s0* fun_26f0(void** rdi, void** rsi, ...);

void fun_2630(void** rdi, void** rsi, ...);

/* prompt.isra.0 */
uint32_t prompt_isra_0(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    int64_t rdx3;
    void** r13_7;
    void** r12_8;
    void** rbx9;
    void*** rsp10;
    void** r14_11;
    int64_t* v12;
    int32_t v13;
    int64_t rax14;
    int64_t v15;
    void** v16;
    uint32_t r10d17;
    uint32_t eax18;
    int32_t eax19;
    void* rsp20;
    void** eax21;
    void** rbp22;
    uint32_t eax23;
    int32_t eax24;
    void** rax25;
    uint32_t eax26;
    uint32_t v27;
    void** rax28;
    void* rsp29;
    uint32_t r10d30;
    void** rax31;
    void* rsp32;
    void** rdx33;
    void** rax34;
    void* rsp35;
    void** rax36;
    void* rsp37;
    void** rax38;
    void** rdi39;
    int32_t eax40;
    void* rsp41;
    void** rax42;
    void** rax43;
    void** rax44;
    void* rsp45;
    void** rax46;
    void* rsp47;
    void** rax48;
    unsigned char al49;
    signed char al50;
    int32_t eax51;
    uint32_t v52;
    int32_t eax53;
    void** rax54;
    void** rax55;
    void* v56;
    int32_t eax57;
    void** rax58;
    void** r15_59;
    void** rax60;
    void* rsp61;
    void** v62;
    struct s0* rax63;
    void* rdx64;
    uint32_t eax65;
    void** v66;
    void** v67;

    *reinterpret_cast<void***>(&rdx3) = edx;
    *reinterpret_cast<int32_t*>(&r13_7) = *reinterpret_cast<int32_t*>(&rdi);
    r12_8 = rcx;
    rbx9 = rsi;
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8);
    r14_11 = *reinterpret_cast<void***>(rsi + 48);
    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v12) + 4) = *reinterpret_cast<void***>(&rdx3);
    v13 = *reinterpret_cast<int32_t*>(&r8);
    rax14 = g28;
    v15 = rax14;
    v16 = *reinterpret_cast<void***>(rsi + 56);
    if (!r9) {
        r10d17 = 0;
        while (1) {
            eax18 = 3;
            if (*reinterpret_cast<void***>(rbx9 + 32)) {
                addr_3687_4:
                rdx3 = v15 - g28;
                if (!rdx3) 
                    break;
            } else {
                eax19 = *reinterpret_cast<int32_t*>(r12_8 + 4);
                if (eax19 == 5) 
                    goto addr_3682_6;
                if (*reinterpret_cast<void***>(r12_8)) 
                    goto addr_367d_8; else 
                    goto addr_35d4_9;
            }
            fun_2590();
            rsp20 = reinterpret_cast<void*>(rsp10 - 8 + 8);
            addr_395f_11:
            fun_2620();
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
            r10d17 = 0;
            eax21 = reinterpret_cast<void**>(3);
            addr_35a8_12:
            *reinterpret_cast<void***>(rbp22) = eax21;
            continue;
            addr_367d_8:
            if (eax19 != 3) 
                goto addr_3682_6;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
            addr_36ca_14:
            eax23 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v12) + 4)) << 2;
            if (eax23) {
                if (eax23 != 4) 
                    goto addr_3950_16; else 
                    goto addr_36e1_17;
            }
            rbp22 = reinterpret_cast<void**>(rsp10 + 32);
            rsi = r14_11;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v12) + 4) = *reinterpret_cast<unsigned char*>(&r10d17);
            eax24 = cache_fstatat_constprop_0(rdi, rsi, rbp22, rcx, r8, r9);
            rsp10 = rsp10 - 8 + 8;
            if (eax24) {
                addr_3838_19:
                rax25 = fun_2480();
                rsp10 = rsp10 - 8 + 8;
                rbp22 = *reinterpret_cast<void***>(rax25);
                *reinterpret_cast<int32_t*>(&rbp22 + 4) = 0;
                goto addr_383f_20;
            } else {
                eax26 = v27 & 0xf000;
                if (eax26 == 0xa000) {
                    if (*reinterpret_cast<int32_t*>(r12_8 + 4) != 3) {
                        addr_3682_6:
                        eax18 = 2;
                        goto addr_3687_4;
                    } else {
                        goto addr_3821_24;
                    }
                } else {
                    r10d17 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v12) + 4);
                    if (eax26 == 0x4000) {
                        addr_36e1_17:
                        if (*reinterpret_cast<signed char*>(r12_8 + 9)) {
                            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v12) + 4) = *reinterpret_cast<unsigned char*>(&r10d17);
                            rax28 = quotearg_style(4, v16, 4, v16);
                            rsp29 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                            r10d30 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v12) + 4);
                            r12_8 = rax28;
                            if (v13 != 2 || *reinterpret_cast<signed char*>(&r10d30)) {
                                rbp22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp29) + 32);
                            } else {
                                r13_7 = program_name;
                                if (*reinterpret_cast<int32_t*>(&rbx9)) {
                                    rax31 = fun_2550();
                                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
                                    rdx33 = rax31;
                                } else {
                                    rax34 = fun_2550();
                                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
                                    rdx33 = rax34;
                                }
                                rdi = stderr;
                                r8 = r12_8;
                                rcx = r13_7;
                                rsi = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                fun_2830(rdi, 1, rdx33, rcx, r8, r9, v16, v12);
                                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                goto addr_38bb_32;
                            }
                        } else {
                            if (!*reinterpret_cast<signed char*>(r12_8 + 10) || !*reinterpret_cast<unsigned char*>(&r10d17)) {
                                rbp22 = reinterpret_cast<void**>(21);
                                *reinterpret_cast<int32_t*>(&rbp22 + 4) = 0;
                                rax36 = quotearg_style(4, v16, 4, v16);
                                rsp37 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                                r12_8 = rax36;
                                goto addr_3710_35;
                            } else {
                                addr_3950_16:
                                rbp22 = reinterpret_cast<void**>(rsp10 + 32);
                                goto addr_3821_24;
                            }
                        }
                    } else {
                        addr_3821_24:
                        rax38 = quotearg_style(4, v16, 4, v16);
                        rsp29 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                        r12_8 = rax38;
                    }
                }
            }
            *reinterpret_cast<int32_t*>(&rdi39) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi39 + 4) = 0;
            eax40 = cache_fstatat_constprop_0(rdi39, r14_11, rbp22, rcx, r8, r9);
            rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
            if (eax40) {
                rax42 = fun_2550();
                r13_7 = rax42;
                rax43 = fun_2480();
                rcx = r12_8;
                *reinterpret_cast<int32_t*>(&rdi) = 0;
                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                rsi = *reinterpret_cast<void***>(rax43);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_27a0();
                rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8 - 8 + 8 - 8 + 8);
                eax18 = 4;
                goto addr_3687_4;
            } else {
                rax44 = file_type(rbp22, r14_11, rbp22, rcx, r8, r9);
                rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                r13_7 = program_name;
                rbp22 = rax44;
                if (*reinterpret_cast<int32_t*>(&rbx9)) {
                    rax46 = fun_2550();
                    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    rdx33 = rax46;
                } else {
                    rax48 = fun_2550();
                    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    rdx33 = rax48;
                }
                rdi = stderr;
                r9 = r12_8;
                r8 = rbp22;
                rcx = r13_7;
                rsi = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_2830(rdi, 1, rdx33, rcx, r8, r9, v16, v12);
                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            }
            addr_38bb_32:
            al49 = yesno(rdi, 1, rdx33, rcx, r8, r9);
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            eax18 = 3 - al49;
            goto addr_3687_4;
            addr_3710_35:
            fun_2550();
            rcx = r12_8;
            rsi = rbp22;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi) = 0;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_27a0();
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp37) - 8 + 8 - 8 + 8);
            eax18 = 4;
            goto addr_3687_4;
            addr_35d4_9:
            if (eax19 == 3) 
                goto addr_35e5_42;
            if (!*reinterpret_cast<signed char*>(r12_8 + 25)) 
                goto addr_3682_6;
            addr_35e5_42:
            al50 = can_write_any_file(rdi, rsi, rdx3, rcx, r8, r9);
            rsp10 = rsp10 - 8 + 8;
            r10d17 = *reinterpret_cast<unsigned char*>(&r10d17);
            if (al50) 
                goto addr_3678_44;
            rbp22 = reinterpret_cast<void**>(rsp10 + 32);
            rsi = r14_11;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax51 = cache_fstatat_constprop_0(rdi, rsi, rbp22, rcx, r8, r9);
            rsp10 = rsp10 - 8 + 8;
            if (eax51) 
                goto addr_3838_19;
            r10d17 = *reinterpret_cast<unsigned char*>(&r10d17);
            if ((v52 & 0xf000) == 0xa000 || (*reinterpret_cast<int32_t*>(&rcx) = 0x200, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, rsi = r14_11, *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, eax53 = fun_2500(rdi, rsi, 2, 0x200, r8, r9), rsp10 = rsp10 - 8 + 8, r10d17 = *reinterpret_cast<unsigned char*>(&r10d17), eax53 == 0)) {
                addr_3678_44:
                eax19 = *reinterpret_cast<int32_t*>(r12_8 + 4);
                goto addr_367d_8;
            } else {
                rax54 = fun_2480();
                rsp10 = rsp10 - 8 + 8;
                r10d17 = *reinterpret_cast<unsigned char*>(&r10d17);
                rbp22 = *reinterpret_cast<void***>(rax54);
                *reinterpret_cast<int32_t*>(&rbp22 + 4) = 0;
                if (!reinterpret_cast<int1_t>(rbp22 == 13)) {
                    addr_383f_20:
                    rax55 = quotearg_style(4, v16, 4, v16);
                    rsp37 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                    r12_8 = rax55;
                    goto addr_3710_35;
                } else {
                    *reinterpret_cast<int32_t*>(&rbx9) = 1;
                    *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
                    goto addr_36ca_14;
                }
            }
        }
        return eax18;
    }
    *reinterpret_cast<void***>(r9) = reinterpret_cast<void**>(2);
    rsi = r14_11;
    rbp22 = r9;
    *reinterpret_cast<void***>(&rdx3) = reinterpret_cast<void**>(0x30900);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    eax57 = fun_2580(rdi, rsi, 0x30900, rcx, r8, r9, *reinterpret_cast<int32_t*>(&v16), v12, v56);
    rsp10 = rsp10 - 8 + 8;
    *reinterpret_cast<int32_t*>(&rdi) = eax57;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    if (eax57 < 0) {
        addr_35a0_51:
        r10d17 = 0;
        eax21 = reinterpret_cast<void**>(3);
        goto addr_35a8_12;
    } else {
        rax58 = fun_27e0(rdi, rsi, rdi, rsi);
        rsp20 = reinterpret_cast<void*>(rsp10 - 8 + 8);
        *reinterpret_cast<int32_t*>(&rdi) = eax57;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        r15_59 = rax58;
        if (!rax58) 
            goto addr_395f_11;
        rax60 = fun_2480();
        rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
        v62 = rax60;
        *reinterpret_cast<void***>(rax60) = reinterpret_cast<void**>(0);
        do {
            rax63 = fun_26f0(r15_59, rsi, r15_59, rsi);
            rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
            if (!rax63) 
                break;
            if (*reinterpret_cast<void***>(&rax63->f13) != 46) 
                goto addr_3545_56;
            *reinterpret_cast<int32_t*>(&rdx64) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx64) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx64) = reinterpret_cast<uint1_t>(rax63->f14 == 46);
            eax65 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax63) + reinterpret_cast<int64_t>(rdx64) + 20);
        } while (!*reinterpret_cast<signed char*>(&eax65) || *reinterpret_cast<signed char*>(&eax65) == 47);
        goto addr_37d6_58;
    }
    rdi = r15_59;
    v66 = *reinterpret_cast<void***>(v62);
    fun_2630(rdi, rsi);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
    *reinterpret_cast<void***>(&rdx3) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    r10d17 = 1;
    *reinterpret_cast<void***>(v62) = *reinterpret_cast<void***>(&rdx3);
    eax21 = reinterpret_cast<void**>(4);
    if (!*reinterpret_cast<void***>(&rdx3)) 
        goto addr_35a8_12;
    goto addr_35a0_51;
    addr_3545_56:
    rdi = r15_59;
    v67 = *reinterpret_cast<void***>(v62);
    fun_2630(rdi, rsi);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
    *reinterpret_cast<void***>(&rdx3) = v67;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    r10d17 = 0;
    *reinterpret_cast<void***>(v62) = *reinterpret_cast<void***>(&rdx3);
    eax21 = reinterpret_cast<void**>(3);
    goto addr_35a8_12;
    addr_37d6_58:
    goto addr_3545_56;
}

struct s1 {
    signed char[24] pad24;
    void** f18;
    signed char[19] pad44;
    int32_t f2c;
};

int32_t fun_24c0(int64_t rdi, void** rsi);

void fun_2770(int64_t rdi, void** rsi, void** rdx, void** rcx);

uint32_t excise(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    int32_t r12d6;
    void** rsi7;
    int64_t rdi8;
    int64_t rax9;
    int64_t v10;
    int32_t eax11;
    uint32_t eax12;
    void** rsi13;
    void** rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    void** rax18;
    int64_t r14_19;
    void** rsi20;
    void** rdi21;
    void** rdx22;
    int32_t eax23;
    int1_t zf24;
    void** rsi25;
    void** rax26;
    int64_t rdx27;

    r12d6 = *reinterpret_cast<int32_t*>(&rcx);
    rsi7 = *reinterpret_cast<void***>(rsi + 48);
    *reinterpret_cast<int32_t*>(&rdi8) = rdi->f2c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = g28;
    v10 = rax9;
    eax11 = fun_24c0(rdi8, rsi7);
    if (!eax11) {
        eax12 = 2;
        if (*reinterpret_cast<signed char*>(rdx + 26)) {
            rsi13 = *reinterpret_cast<void***>(rsi + 56);
            rax14 = quotearg_style(4, rsi13);
            if (!*reinterpret_cast<signed char*>(&r12d6)) {
                rax15 = fun_2550();
                rsi16 = rax15;
            } else {
                rax17 = fun_2550();
                rsi16 = rax17;
            }
            fun_2770(1, rsi16, rax14, rcx);
            eax12 = 2;
            goto addr_3a5f_7;
        }
    }
    rax18 = fun_2480();
    *reinterpret_cast<void***>(&r14_19) = *reinterpret_cast<void***>(rax18);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_19) + 4) = 0;
    if (*reinterpret_cast<void***>(&r14_19) == 30) {
        rsi20 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<int32_t*>(&rdi21) = rdi->f2c;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xa0 - 8 + 8) - 8 + 8);
        eax23 = fun_2870(rdi21, rsi20, rdx22, rdi21, rsi20, rdx22);
        if (!eax23 || *reinterpret_cast<void***>(rax18) != 2) {
            zf24 = *reinterpret_cast<void***>(rdx) == 0;
            *reinterpret_cast<void***>(rax18) = reinterpret_cast<void**>(30);
            if (zf24) 
                goto addr_39f3_11; else 
                goto addr_3b8a_12;
        }
        eax12 = 2;
        *reinterpret_cast<void***>(&r14_19) = reinterpret_cast<void**>(2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_19) + 4) = 0;
        if (!*reinterpret_cast<void***>(rdx)) {
            addr_3b8a_12:
            if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) {
                addr_39f3_11:
                rsi25 = *reinterpret_cast<void***>(rsi + 56);
                quotearg_style(4, rsi25, 4, rsi25);
                fun_2550();
                fun_27a0();
                rax26 = *reinterpret_cast<void***>(rsi + 8);
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax26 + 88)) >= reinterpret_cast<signed char>(0)) {
                    do {
                        if (*reinterpret_cast<void***>(rax26 + 32)) 
                            break;
                        *reinterpret_cast<void***>(rax26 + 32) = reinterpret_cast<void**>(1);
                        rax26 = *reinterpret_cast<void***>(rax26 + 8);
                    } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax26 + 88)) >= reinterpret_cast<signed char>(0));
                }
            } else {
                goto addr_3abe_18;
            }
        } else {
            goto addr_3a5f_7;
        }
    } else {
        if (!*reinterpret_cast<void***>(rdx)) 
            goto addr_39e8_21;
        eax12 = 2;
        if (*reinterpret_cast<void***>(&r14_19) == 22) 
            goto addr_3a5f_7;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&r14_19)) > reinterpret_cast<signed char>(22)) 
            goto addr_3af0_24; else 
            goto addr_3a9d_25;
    }
    eax12 = 4;
    goto addr_3a5f_7;
    addr_3abe_18:
    if (static_cast<int1_t>(0x8000320000 >> r14_19) && (*reinterpret_cast<void***>(rsi + 64) == 1 || reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 64) == 13))) {
        *reinterpret_cast<void***>(rax18) = *reinterpret_cast<void***>(rsi + 64);
        goto addr_39f3_11;
    }
    addr_3af0_24:
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&r14_19) == 84)) {
        addr_39e8_21:
        if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) 
            goto addr_39f3_11;
    } else {
        goto addr_3a5f_7;
    }
    addr_3ab4_29:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r14_19)) > reinterpret_cast<unsigned char>(39)) 
        goto addr_39f3_11; else 
        goto addr_3abe_18;
    addr_3a9d_25:
    if (*reinterpret_cast<void***>(&r14_19) == 2 || *reinterpret_cast<void***>(&r14_19) == 20) {
        addr_3a5f_7:
        rdx27 = v10 - g28;
        if (rdx27) {
            fun_2590();
        } else {
            return eax12;
        }
    } else {
        if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) 
            goto addr_39f3_11; else 
            goto addr_3ab4_29;
    }
}

void** fun_2470();

int32_t i_ring_push(void*** rdi);

int32_t cwd_advance_fd(void** rdi, void** esi, signed char dl) {
    int32_t eax4;
    int32_t eax5;

    if (*reinterpret_cast<void***>(rdi + 44) == esi && !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) {
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
    }
    if (dl) {
        eax4 = i_ring_push(rdi + 96);
        if (eax4 < 0) {
            addr_5389_27:
            *reinterpret_cast<void***>(rdi + 44) = esi;
            return eax4;
        } else {
            eax5 = fun_2620();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_5389_27;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_5389_27;
        eax5 = fun_2620();
    }
    *reinterpret_cast<void***>(rdi + 44) = esi;
    return eax5;
}

void** fun_2700(void** rdi, void** rsi, void** rdx, ...);

void fun_26d0(void** rdi, void** rsi, void** rdx);

void** fts_alloc(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    rax4 = fun_2700(reinterpret_cast<uint64_t>(rdx + 0x108) & 0xfffffffffffffff8, rsi, rdx);
    if (rax4) {
        fun_26d0(rax4 + 0x100, rsi, rdx);
        rax5 = *reinterpret_cast<void***>(rdi + 32);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rdx) + 0x100) = 0;
        *reinterpret_cast<void***>(rax4 + 96) = rdx;
        *reinterpret_cast<void***>(rax4 + 80) = rdi;
        *reinterpret_cast<void***>(rax4 + 56) = rax5;
        *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 24) = reinterpret_cast<void**>(0);
        *reinterpret_cast<unsigned char*>(rax4 + 0x6a) = reinterpret_cast<unsigned char>(0x30000);
        *reinterpret_cast<void***>(rax4 + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 40) = reinterpret_cast<void**>(0);
    }
    return rax4;
}

int64_t free = 0;

void** hash_initialize(void** rdi);

struct s2 {
    signed char[8] pad8;
    void** f8;
};

struct s2* hash_lookup();

int32_t fun_27f0();

void** hash_insert(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void fun_2450(void** rdi, ...);

void** filesystem_type(void** rdi, void** esi, ...) {
    void** rsi2;
    void** rsp3;
    void** r12_4;
    int64_t rax5;
    void** rbp6;
    void** rbx7;
    void** r13d8;
    int64_t r8_9;
    void** rax10;
    void** v11;
    struct s2* rax12;
    void** rax13;
    int32_t eax14;
    void** r12_15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rax19;
    int64_t rdx20;
    int32_t eax21;
    void** v22;

    rsi2 = esi;
    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98);
    r12_4 = *reinterpret_cast<void***>(rdi + 80);
    rax5 = g28;
    rbp6 = *reinterpret_cast<void***>(r12_4 + 80);
    if (!(*reinterpret_cast<unsigned char*>(r12_4 + 73) & 2)) 
        goto addr_52d8_2;
    rbx7 = rdi;
    r13d8 = rsi2;
    if (!rbp6 && (r8_9 = free, rsi2 = reinterpret_cast<void**>(0), rdi = reinterpret_cast<void**>(13), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax10 = hash_initialize(13), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), *reinterpret_cast<void***>(r12_4 + 80) = rax10, rbp6 = rax10, !rax10) || (rsi2 = rsp3, rdi = rbp6, v11 = *reinterpret_cast<void***>(rbx7 + 0x70), rax12 = hash_lookup(), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), rax12 == 0)) {
        if (reinterpret_cast<signed char>(r13d8) < reinterpret_cast<signed char>(0)) {
            addr_52d8_2:
            *reinterpret_cast<int32_t*>(&rax13) = 0;
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
        } else {
            rsi2 = rsp3 + 16;
            rdi = r13d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax14 = fun_27f0();
            if (!eax14) {
                r12_15 = v16;
                if (!rbp6) {
                    addr_5346_7:
                    rax13 = r12_15;
                } else {
                    rdi = reinterpret_cast<void**>(16);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax17 = fun_2700(16, rsi2, 0x4e90, 16, rsi2, 0x4e90);
                    if (!rax17) 
                        goto addr_5341_9;
                    rax18 = *reinterpret_cast<void***>(rbx7 + 0x70);
                    *reinterpret_cast<void***>(rax17 + 8) = r12_15;
                    rsi2 = rax17;
                    rdi = rbp6;
                    *reinterpret_cast<void***>(rax17) = rax18;
                    rax19 = hash_insert(rdi, rsi2, 0x4e90, 0x4ea0, r8_9);
                    if (!rax19) 
                        goto addr_5350_11; else 
                        goto addr_5338_12;
                }
            } else {
                goto addr_52d8_2;
            }
        }
    } else {
        rax13 = rax12->f8;
    }
    rdx20 = rax5 - g28;
    if (!rdx20) {
        return rax13;
    }
    fun_2590();
    if (*reinterpret_cast<void***>(rdi + 44) != rsi2) 
        goto addr_537b_19;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) 
        goto addr_2895_21;
    addr_537b_19:
    if (*reinterpret_cast<signed char*>(&rdx20)) {
        eax21 = i_ring_push(rdi + 96);
        if (eax21 < 0) {
            addr_5389_23:
            *reinterpret_cast<void***>(rdi + 44) = rsi2;
            goto v11;
        } else {
            fun_2620();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_5389_23;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_5389_23;
        fun_2620();
    }
    *reinterpret_cast<void***>(rdi + 44) = rsi2;
    goto v11;
    addr_2895_21:
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    addr_5350_11:
    rdi = rax17;
    fun_2450(rdi, rdi);
    goto addr_5341_9;
    addr_5338_12:
    if (rax17 != rax19) {
        fun_2470();
        goto addr_2895_21;
    } else {
        addr_5341_9:
        r12_15 = v22;
        goto addr_5346_7;
    }
}

void** fun_2750(void** rdi);

void fun_24d0();

void** fts_sort(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rsi9;
    void** r8_10;
    void** rax11;
    void** rdx12;
    void** r8_13;
    void** rax14;
    void** rdx15;
    void** rsi16;
    void** rcx17;
    void** rdx18;

    r12_5 = rdi;
    rbp6 = rdx;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rdi + 16);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 56)) < reinterpret_cast<unsigned char>(rdx)) {
        rsi9 = rdx + 40;
        r8_10 = rdi8;
        *reinterpret_cast<void***>(r12_5 + 56) = rsi9;
        if (reinterpret_cast<unsigned char>(rsi9) >> 61) {
            addr_5152_3:
            fun_2450(r8_10);
            *reinterpret_cast<void***>(r12_5 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_5 + 56) = reinterpret_cast<void**>(0);
            return rbx7;
        } else {
            rax11 = fun_2750(rdi8);
            rdi8 = rax11;
            if (!rax11) {
                r8_10 = *reinterpret_cast<void***>(r12_5 + 16);
                goto addr_5152_3;
            } else {
                *reinterpret_cast<void***>(r12_5 + 16) = rax11;
            }
        }
    }
    rdx12 = rdi8;
    if (rbx7) {
        do {
            *reinterpret_cast<void***>(rdx12) = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdx12 = rdx12 + 8;
        } while (rbx7);
    }
    fun_24d0();
    r8_13 = *reinterpret_cast<void***>(r12_5 + 16);
    rax14 = *reinterpret_cast<void***>(r8_13);
    rdx15 = r8_13;
    rsi16 = rax14;
    rcx17 = rbp6 - 1;
    if (rcx17) {
        while (rdx15 = rdx15 + 8, *reinterpret_cast<void***>(rsi16 + 16) = *reinterpret_cast<void***>(rdx15 + 8), --rcx17, !!rcx17) {
            rsi16 = *reinterpret_cast<void***>(rdx15);
        }
        rdx18 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_13 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
    } else {
        rdx18 = rax14;
    }
    *reinterpret_cast<void***>(rdx18 + 16) = reinterpret_cast<void**>(0);
    return rax14;
}

int32_t fun_2740();

unsigned char i_ring_empty(void*** rdi, void** rsi, void** rdx);

void** i_ring_pop(void*** rdi, void** rsi, void** rdx);

uint32_t restore_initial_cwd(void** rdi, void** rsi) {
    void** eax3;
    uint32_t r12d4;
    int32_t eax5;
    void*** rbx6;
    unsigned char al7;
    void** eax8;

    eax3 = *reinterpret_cast<void***>(rdi + 72);
    r12d4 = reinterpret_cast<unsigned char>(eax3) & 4;
    if (r12d4) {
        r12d4 = 0;
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&eax3 + 1) & 2)) {
            r12d4 = 0;
            eax5 = fun_2740();
            *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<uint1_t>(!!eax5);
        } else {
            rsi = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            cwd_advance_fd(rdi, 0xffffff9c, 1);
        }
    }
    rbx6 = reinterpret_cast<void***>(rdi + 96);
    while (al7 = i_ring_empty(rbx6, rsi, 1), al7 == 0) {
        eax8 = i_ring_pop(rbx6, rsi, 1);
        if (reinterpret_cast<signed char>(eax8) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2620();
    }
    return r12d4;
}

unsigned char fts_palloc(void** rdi, void** rsi, void** rdx) {
    void** rsi4;
    void** rdi5;
    void** tmp64_6;
    void** rax7;
    void** rax8;
    void** rdi9;

    rsi4 = rsi + 0x100;
    rdi5 = *reinterpret_cast<void***>(rdi + 32);
    tmp64_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)));
    if (reinterpret_cast<unsigned char>(tmp64_6) < reinterpret_cast<unsigned char>(rsi4)) {
        fun_2450(rdi5);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        rax7 = fun_2480();
        *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(36);
        return 0;
    } else {
        *reinterpret_cast<void***>(rdi + 48) = tmp64_6;
        rax8 = fun_2750(rdi5);
        if (!rax8) {
            rdi9 = *reinterpret_cast<void***>(rdi + 32);
            fun_2450(rdi9);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            return 0;
        } else {
            *reinterpret_cast<void***>(rdi + 32) = rax8;
            return 1;
        }
    }
}

void cycle_check_init(void** rdi);

unsigned char setup_dir(void** rdi, void** rsi, void** rdx, ...) {
    void** rax4;
    void** rax5;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rax4 = fun_2700(32, rsi, rdx);
        *reinterpret_cast<void***>(rdi + 88) = rax4;
        if (!rax4) {
            return 0;
        } else {
            cycle_check_init(rax4);
            return 1;
        }
    } else {
        rax5 = hash_initialize(31);
        *reinterpret_cast<void***>(rdi + 88) = rax5;
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax5));
    }
}

void** hash_remove(void** rdi, void** rsi);

void leave_dir(void** rdi, void** rsi, ...) {
    int64_t rax3;
    void** rdi4;
    void** v5;
    void** rax6;
    void** rdx7;
    void** rax8;
    int64_t rax9;
    void** eax10;
    void*** rbx11;
    unsigned char al12;
    void** eax13;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102) {
        rdi4 = *reinterpret_cast<void***>(rdi + 88);
        v5 = *reinterpret_cast<void***>(rsi + 0x70);
        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        rax6 = hash_remove(rdi4, rsi);
        rdi = rax6;
        if (!rax6) {
            addr_289a_3:
            fun_2470();
        } else {
            fun_2450(rdi, rdi);
            goto addr_55a8_5;
        }
    } else {
        if (*reinterpret_cast<void***>(rsi + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 88)) >= reinterpret_cast<signed char>(0)) {
            rdx7 = *reinterpret_cast<void***>(rdi + 88);
            if (!*reinterpret_cast<void***>(rdx7 + 16)) 
                goto addr_289a_3;
            if (*reinterpret_cast<void***>(rdx7) == *reinterpret_cast<void***>(rsi + 0x78)) {
                if (*reinterpret_cast<void***>(rdx7 + 8) == *reinterpret_cast<void***>(rsi + 0x70)) {
                    rax8 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x78);
                    *reinterpret_cast<void***>(rdx7 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x70);
                    *reinterpret_cast<void***>(rdx7) = rax8;
                    goto addr_55a8_5;
                }
            } else {
                goto addr_55a8_5;
            }
        }
    }
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    addr_55a8_5:
    rax9 = rax3 - g28;
    if (!rax9) {
        return;
    }
    fun_2590();
    eax10 = *reinterpret_cast<void***>(rdi + 72);
    if (!(reinterpret_cast<unsigned char>(eax10) & 4)) 
        goto addr_5626_36;
    addr_5643_38:
    rbx11 = reinterpret_cast<void***>(rdi + 96);
    while (al12 = i_ring_empty(rbx11, rsi, rdx7), al12 == 0) {
        eax13 = i_ring_pop(rbx11, rsi, rdx7);
        if (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2620();
    }
    goto v5;
    addr_5626_36:
    if (!(*reinterpret_cast<unsigned char*>(&eax10 + 1) & 2)) {
        fun_2740();
        goto addr_5643_38;
    } else {
        *reinterpret_cast<int32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        rsi = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        cwd_advance_fd(rdi, 0xffffff9c, 1);
        goto addr_5643_38;
    }
}

int32_t fun_2860(int64_t rdi, void* rsi);

void** openat_safer(int64_t rdi, void** rsi);

void** open_safer(void** rdi, int64_t rsi);

int32_t fts_safe_changedir(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r14d6;
    void** r12_7;
    void** rbx8;
    void* rsp9;
    int64_t rax10;
    int64_t v11;
    uint32_t eax12;
    void** ebp13;
    unsigned char v14;
    uint32_t eax15;
    void** r13d16;
    int64_t rax17;
    uint32_t eax18;
    int32_t eax19;
    int64_t rdi20;
    int32_t eax21;
    void** v22;
    void** v23;
    void** rax24;
    void** rax25;
    int64_t rdi26;
    void** eax27;
    int64_t rsi28;
    void** eax29;
    void*** r13_30;
    unsigned char al31;
    void** eax32;

    r15_5 = rdi;
    r14d6 = rdx;
    r12_7 = rsi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rax10 = g28;
    v11 = rax10;
    if (!rcx || ((eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)), eax12 != 46) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx + 1) == 46) || *reinterpret_cast<signed char*>(rcx + 2)))) {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) {
            addr_5820_3:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (!ebp13 || reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                fun_2620();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            }
        } else {
            if (reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                v14 = 0;
                eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
                goto addr_593c_8;
            } else {
                v14 = 0;
                r13d16 = r14d6;
                if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) 
                    goto addr_57e8_10; else 
                    goto addr_570a_11;
            }
        }
    } else {
        ebp13 = *reinterpret_cast<void***>(rdi + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) 
            goto addr_5820_3;
        if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(0)) 
            goto addr_5970_14;
        if (reinterpret_cast<unsigned char>(ebp13) & 0x200) 
            goto addr_58a0_16; else 
            goto addr_57a5_17;
    }
    addr_5740_18:
    while (rax17 = v11 - g28, !!rax17) {
        eax12 = fun_2590();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        addr_5970_14:
        v14 = 1;
        r13d16 = rdx;
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_57e8_10;
        }
        addr_5712_21:
        if (eax12 != 46 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 1) == 46)) {
            addr_571b_22:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (ebp13) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                eax18 = static_cast<uint32_t>(v14) ^ 1;
                rdx = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax18)));
                cwd_advance_fd(r15_5, r13d16, *reinterpret_cast<signed char*>(&rdx));
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                continue;
            } else {
                eax19 = fun_2740();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r12_7) = eax19;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) {
                    continue;
                }
            }
        } else {
            if (!*reinterpret_cast<signed char*>(rbx8 + 2)) {
                addr_57e8_10:
                *reinterpret_cast<void***>(&rdi20) = r13d16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_2860(rdi20, reinterpret_cast<int64_t>(rsp9) + 16);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                if (eax21) {
                    addr_5843_27:
                    *reinterpret_cast<int32_t*>(&r12_7) = -1;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) 
                        continue;
                } else {
                    if (*reinterpret_cast<void***>(r12_7 + 0x70) != v22 || *reinterpret_cast<void***>(r12_7 + 0x78) != v23) {
                        rax24 = fun_2480();
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(2);
                        goto addr_5843_27;
                    } else {
                        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
                        goto addr_571b_22;
                    }
                }
            } else {
                goto addr_571b_22;
            }
        }
        rax25 = fun_2480();
        ebp13 = *reinterpret_cast<void***>(rax25);
        rbx8 = rax25;
        fun_2620();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        *reinterpret_cast<void***>(rbx8) = ebp13;
    }
    return *reinterpret_cast<int32_t*>(&r12_7);
    addr_593c_8:
    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    if (eax15) {
        addr_58c9_34:
        eax27 = openat_safer(rdi26, rbx8);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r13d16 = eax27;
    } else {
        goto addr_57bb_36;
    }
    addr_57ca_37:
    if (reinterpret_cast<signed char>(r13d16) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&r12_7) = -1;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        goto addr_5740_18;
    } else {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_57e8_10;
        }
    }
    addr_570a_11:
    if (!rbx8) 
        goto addr_571b_22;
    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    goto addr_5712_21;
    addr_57bb_36:
    *reinterpret_cast<void***>(&rsi28) = rdx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi28) + 4) = 0;
    eax29 = open_safer(rbx8, rsi28);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r13d16 = eax29;
    goto addr_57ca_37;
    addr_58a0_16:
    r13_30 = reinterpret_cast<void***>(rdi + 96);
    al31 = i_ring_empty(r13_30, rsi, rdx);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    v14 = al31;
    if (!al31) {
        eax32 = i_ring_pop(r13_30, rsi, rdx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        r13d16 = eax32;
        if (reinterpret_cast<signed char>(eax32) < reinterpret_cast<signed char>(0)) {
            v14 = 1;
            eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
            goto addr_593c_8;
        } else {
            v14 = 1;
            r14d6 = eax32;
            if (!(*reinterpret_cast<unsigned char*>(&ebp13) & 2)) 
                goto addr_571b_22;
            goto addr_57e8_10;
        }
    } else {
        *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
        goto addr_58c9_34;
    }
    addr_57a5_17:
    v14 = 1;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    goto addr_57bb_36;
}

void** opendirat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void** fun_26a0(void** rdi);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, ...);

void** rpl_fcntl();

struct s3 {
    signed char f0;
    void** f1;
};

void** fun_2570(void** rdi, ...);

void fun_2790(void** rdi, void** rsi, void** rdx, ...);

void** fts_build(void** rdi, void** rsi) {
    void** r14_3;
    void** rbp4;
    void** v5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** v9;
    void** eax10;
    int64_t rdx11;
    uint32_t edx12;
    int64_t rdi13;
    void** rcx14;
    void** rax15;
    void** r13_16;
    void** eax17;
    void** v18;
    void** rdi19;
    void** rax20;
    void** rax21;
    int64_t rax22;
    int64_t r8_23;
    uint64_t rax24;
    void* rax25;
    void** v26;
    void** rax27;
    void** edi28;
    void** v29;
    int32_t r12d30;
    int32_t ebx31;
    void** rax32;
    void** eax33;
    void** rdx34;
    int32_t eax35;
    void** rax36;
    void** rdi37;
    void** edx38;
    unsigned char v39;
    void** rcx40;
    void** v41;
    void** v42;
    void** v43;
    struct s3* rax44;
    void** r15_45;
    void** rax46;
    int1_t zf47;
    void** v48;
    void** v49;
    signed char v50;
    void** r12_51;
    void** rax52;
    void** rdi53;
    void** v54;
    void** rbx55;
    unsigned char v56;
    void** v57;
    void** v58;
    void** v59;
    struct s0* rax60;
    void** rax61;
    void** r14_62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** r15_66;
    uint32_t eax67;
    void** tmp64_68;
    void** rsi69;
    void** rax70;
    void** edx71;
    void** rdx72;
    void** eax73;
    int64_t rax74;
    int64_t rdx75;
    void** eax76;
    void** rax77;
    void** rax78;
    uint32_t eax79;
    int32_t eax80;
    void** rax81;
    void** rax82;
    uint32_t eax83;
    void** rbp84;
    void** rdi85;
    void** rbp86;
    void** rdi87;
    uint64_t rax88;
    uint32_t eax89;
    void** rdi90;
    void** rax91;
    void** rax92;
    void** rdx93;
    void** r13_94;
    void** r14_95;
    void** rbp96;
    void** ebx97;
    void** r12_98;
    void** rdi99;
    void** rdi100;
    void** r13_101;
    void** rbp102;
    void** r14_103;
    void** r12_104;
    void** rdi105;
    void** rdi106;
    void** v107;
    void** v108;

    r14_3 = rdi;
    rbp4 = *reinterpret_cast<void***>(rdi);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = *reinterpret_cast<void***>(rbp4 + 24);
    v9 = rax8;
    if (!rax8) {
        eax10 = *reinterpret_cast<void***>(rdi + 72);
        *reinterpret_cast<uint32_t*>(&rdx11) = reinterpret_cast<unsigned char>(eax10) & 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdx11) && (*reinterpret_cast<uint32_t*>(&rdx11) = 0x20000, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(&eax10) & 1))) {
            edx12 = 0;
            *reinterpret_cast<unsigned char*>(&edx12) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbp4 + 88));
            *reinterpret_cast<uint32_t*>(&rdx11) = edx12 << 17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        }
        rsi = *reinterpret_cast<void***>(rbp4 + 48);
        *reinterpret_cast<void***>(&rdi13) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        if ((reinterpret_cast<unsigned char>(eax10) & 0x204) == 0x200) {
            *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(r14_3 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 + 100);
        rax15 = opendirat(rdi13, rsi, rdx11, rcx14);
        *reinterpret_cast<void***>(rbp4 + 24) = rax15;
        r13_16 = rax15;
        if (rax15) 
            goto addr_5e8a_7;
    } else {
        eax17 = fun_26a0(rax8);
        v18 = eax17;
        if (reinterpret_cast<signed char>(eax17) < reinterpret_cast<signed char>(0)) {
            rdi19 = *reinterpret_cast<void***>(rbp4 + 24);
            fun_2630(rdi19, rsi);
            *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<int1_t>(v5 == 3)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
                rax20 = fun_2480();
                *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax20);
                goto addr_61da_11;
            }
        }
        if (!*reinterpret_cast<void***>(r14_3 + 64)) 
            goto addr_6280_13; else 
            goto addr_59f9_14;
    }
    if (!reinterpret_cast<int1_t>(v5 == 3)) {
        addr_61da_11:
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    } else {
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
        rax21 = fun_2480();
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax21);
    }
    addr_5ff1_17:
    rax22 = v7 - g28;
    if (rax22) {
        fun_2590();
    } else {
        return r13_16;
    }
    addr_5e8a_7:
    if (*reinterpret_cast<uint16_t*>(rbp4 + 0x68) == 11) {
        rsi = rbp4;
        rax15 = fts_stat(r14_3, rsi, 0, r14_3, rsi, 0);
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&rax15);
        goto addr_5ea0_22;
    }
    if (!(*reinterpret_cast<unsigned char*>(r14_3 + 73) & 1) || (leave_dir(r14_3, rbp4, r14_3, rbp4), fts_stat(r14_3, rbp4, 0, r14_3, rbp4, 0), rsi = rbp4, rax15 = enter_dir(r14_3, rsi, 0, rcx14, r8_23), !!*reinterpret_cast<signed char*>(&rax15))) {
        addr_5ea0_22:
        rax24 = reinterpret_cast<unsigned char>(rax15) - (reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 64)) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax25) = *reinterpret_cast<uint32_t*>(&rax24) & 0x186a1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
        v26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax25) - 1);
        if (v5 == 2) 
            goto addr_6230_24;
    } else {
        rax27 = fun_2480();
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
        *reinterpret_cast<void***>(rax27) = reinterpret_cast<void**>(12);
        goto addr_5ff1_17;
    }
    edi28 = v29;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 56) != 24 || *reinterpret_cast<int64_t*>(rbp4 + 0x80) != 2) {
        addr_5ed5_27:
        r12d30 = 1;
        *reinterpret_cast<unsigned char*>(&ebx31) = reinterpret_cast<uint1_t>(v5 == 3);
    } else {
        rsi = edi28;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax32 = filesystem_type(rbp4, rsi, rbp4, rsi);
        if (rax32 == 0x9fa0) 
            goto addr_63ea_29;
        if (reinterpret_cast<signed char>(rax32) > reinterpret_cast<signed char>(0x9fa0)) 
            goto addr_63d4_31; else 
            goto addr_605c_32;
    }
    addr_5ee3_33:
    if (*reinterpret_cast<unsigned char*>(r14_3 + 73) & 2) {
        rsi = reinterpret_cast<void**>(0x406);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax33 = rpl_fcntl();
        v18 = eax33;
        edi28 = eax33;
    }
    if (reinterpret_cast<signed char>(edi28) < reinterpret_cast<signed char>(0) || (rdx34 = edi28, *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0, rsi = rbp4, eax35 = fts_safe_changedir(r14_3, rsi, rdx34, 0), !!eax35)) {
        if (*reinterpret_cast<unsigned char*>(&ebx31) && *reinterpret_cast<signed char*>(&r12d30)) {
            rax36 = fun_2480();
            *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax36);
        }
        *reinterpret_cast<unsigned char*>(rbp4 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(rbp4 + 0x6a) | 1);
        rdi37 = *reinterpret_cast<void***>(rbp4 + 24);
        fun_2630(rdi37, rsi, rdi37, rsi);
        edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&edx38 + 1) & 2 && reinterpret_cast<signed char>(v18) >= reinterpret_cast<signed char>(0)) {
            fun_2620();
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        }
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        v39 = 0;
    } else {
        goto addr_5a02_42;
    }
    addr_5a0b_43:
    rcx40 = *reinterpret_cast<void***>(rbp4 + 72);
    v41 = rcx40;
    v42 = rcx40 + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 56)) + reinterpret_cast<unsigned char>(rcx40) + 0xffffffffffffffff) != 47) {
        v42 = rcx40;
        v41 = rcx40 + 1;
    }
    v43 = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(edx38) & 4) {
        rax44 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 32)));
        rcx40 = reinterpret_cast<void**>(&rax44->f1);
        rax44->f0 = 47;
        v43 = rcx40;
    }
    r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 48)) - reinterpret_cast<unsigned char>(v41));
    rax46 = *reinterpret_cast<void***>(rbp4 + 88) + 1;
    zf47 = *reinterpret_cast<void***>(rbp4 + 24) == 0;
    v48 = *reinterpret_cast<void***>(rbp4 + 24);
    v49 = rax46;
    if (zf47) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4)) {
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            if (!(v39 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(v9 == 0)))) 
                goto addr_61a5_50;
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        } else {
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
            goto addr_60fb_53;
        }
    } else {
        rax52 = fun_2480();
        rdi53 = v48;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v54 = rax52;
        rbx55 = r14_3;
        v50 = 0;
        v56 = 0;
        v57 = reinterpret_cast<void**>(0);
        v58 = rbp4;
        v59 = reinterpret_cast<void**>(0);
        while (*reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(0), rax60 = fun_26f0(rdi53, rsi), !!rax60) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 32) 
                goto addr_5b12_57;
            if (*reinterpret_cast<void***>(&rax60->f13) != 46) 
                goto addr_5b12_57;
            if (!rax60->f14) {
                addr_5ad4_60:
                rax61 = v58;
                rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                if (!rdi53) 
                    goto addr_5c28_61; else 
                    continue;
            } else {
                if (rax60->f14 != 46) {
                    addr_5b12_57:
                    r14_62 = reinterpret_cast<void**>(&rax60->f13);
                    rax63 = fun_2570(r14_62, r14_62);
                    rsi64 = r14_62;
                    rax65 = fts_alloc(rbx55, rsi64, rax63);
                    if (!rax65) 
                        goto addr_5f78_63;
                } else {
                    goto addr_5ad4_60;
                }
            }
            if (reinterpret_cast<unsigned char>(rax63) >= reinterpret_cast<unsigned char>(r15_45)) {
                r15_66 = *reinterpret_cast<void***>(rbx55 + 32);
                rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(rax63) + 2);
                *reinterpret_cast<unsigned char*>(&eax67) = fts_palloc(rbx55, rsi64, rax63);
                if (!*reinterpret_cast<unsigned char*>(&eax67)) 
                    goto addr_5f78_63;
                rsi64 = *reinterpret_cast<void***>(rbx55 + 32);
                if (rsi64 != r15_66) 
                    goto addr_5cfb_68;
            } else {
                addr_5b44_69:
                tmp64_68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax63) + reinterpret_cast<unsigned char>(v41));
                if (reinterpret_cast<unsigned char>(tmp64_68) < reinterpret_cast<unsigned char>(rax63)) 
                    goto addr_63f3_70; else 
                    goto addr_5b52_71;
            }
            eax67 = v56;
            addr_5d0f_73:
            r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 48)) - reinterpret_cast<unsigned char>(v41));
            v56 = *reinterpret_cast<unsigned char*>(&eax67);
            goto addr_5b44_69;
            addr_5cfb_68:
            rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi64) + reinterpret_cast<unsigned char>(v41));
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 4)) {
                rsi64 = v43;
            }
            v43 = rsi64;
            goto addr_5d0f_73;
            addr_5b52_71:
            rsi69 = rax65 + 0x100;
            *reinterpret_cast<void***>(rax65 + 88) = v49;
            rax70 = *reinterpret_cast<void***>(rbx55);
            *reinterpret_cast<void***>(rax65 + 72) = tmp64_68;
            edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            *reinterpret_cast<void***>(rax65 + 8) = rax70;
            *reinterpret_cast<void***>(rax65 + 0x78) = rax60->f0;
            if (*reinterpret_cast<unsigned char*>(&edx71) & 4) {
                *reinterpret_cast<void***>(rax65 + 48) = *reinterpret_cast<void***>(rax65 + 56);
                rdx72 = *reinterpret_cast<void***>(rax65 + 96) + 1;
                fun_2790(v43, rsi69, rdx72);
                edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            } else {
                *reinterpret_cast<void***>(rax65 + 48) = rsi69;
            }
            if (!*reinterpret_cast<void***>(rbx55 + 64) || *reinterpret_cast<unsigned char*>(&edx71 + 1) & 4) {
                eax73 = reinterpret_cast<void**>(static_cast<uint32_t>(rax60->f12));
                rsi = eax73;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(&rax74) = eax73 - 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                if (!(*reinterpret_cast<unsigned char*>(&edx71) & 8) || !(*reinterpret_cast<unsigned char*>(&rsi) & 0xfb)) {
                    rsi = reinterpret_cast<void**>(11);
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) {
                        addr_5d38_81:
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                    } else {
                        eax76 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                        goto addr_5bc8_83;
                    }
                } else {
                    if (!(reinterpret_cast<unsigned char>(edx71) & 16) && *reinterpret_cast<unsigned char*>(&rsi) == 10) {
                        *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                        goto addr_5d38_81;
                    }
                    *reinterpret_cast<int32_t*>(&rcx40) = 11;
                    *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) 
                        goto addr_5f50_87; else 
                        goto addr_5de2_88;
                }
            } else {
                rsi = rax65;
                rax77 = fts_stat(rbx55, rsi, 0);
                *reinterpret_cast<uint16_t*>(rax65 + 0x68) = *reinterpret_cast<uint16_t*>(&rax77);
                goto addr_5bd6_90;
            }
            addr_5d3d_91:
            rcx40 = reinterpret_cast<void**>(0xe0c0);
            eax76 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xe0c0) + reinterpret_cast<uint64_t>(rax74 * 4));
            addr_5bc8_83:
            *reinterpret_cast<void***>(rax65 + 0x88) = eax76;
            *reinterpret_cast<int64_t*>(rax65 + 0xa0) = rdx75;
            addr_5bd6_90:
            *reinterpret_cast<void***>(rax65 + 16) = reinterpret_cast<void**>(0);
            if (!v59) {
                v59 = rax65;
            } else {
                *reinterpret_cast<void***>(v57 + 16) = rax65;
            }
            if (!reinterpret_cast<int1_t>(r12_51 == 0x2710)) {
                ++r12_51;
                if (reinterpret_cast<unsigned char>(r12_51) >= reinterpret_cast<unsigned char>(v26)) 
                    goto addr_61e8_96;
                v57 = rax65;
                goto addr_5ad4_60;
            } else {
                if (!*reinterpret_cast<void***>(rbx55 + 64)) {
                    rsi = v18;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    rax78 = filesystem_type(v58, rsi);
                    if (rax78 == 0x1021994 || ((*reinterpret_cast<int32_t*>(&rcx40) = 0xff534d42, *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0, rax78 == 0xff534d42) || rax78 == 0x6969)) {
                        v57 = rax65;
                        *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                        v50 = 0;
                        goto addr_5ad4_60;
                    } else {
                        v50 = 1;
                        goto addr_5c0b_102;
                    }
                } else {
                    addr_5c0b_102:
                    rax61 = v58;
                    v57 = rax65;
                    *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                    rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                    if (rdi53) 
                        continue; else 
                        goto addr_5c28_61;
                }
            }
            addr_5f50_87:
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_5d3d_91;
            addr_5de2_88:
            eax76 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_5bc8_83;
        }
        goto addr_6090_103;
    }
    addr_6180_104:
    if (!*reinterpret_cast<void***>(rbp4 + 88)) {
        eax79 = restore_initial_cwd(r14_3, rsi);
        if (eax79) 
            goto addr_62cd_106;
        goto addr_61a0_108;
    }
    rsi = *reinterpret_cast<void***>(rbp4 + 8);
    rcx40 = reinterpret_cast<void**>("..");
    eax80 = fts_safe_changedir(r14_3, rsi, 0xffffffff, "..");
    if (!eax80) {
        addr_61a0_108:
        if (r12_51) {
            addr_612c_110:
            if (v50) {
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0x4eb0);
                rax81 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0);
                r13_16 = rax81;
                goto addr_5ff1_17;
            } else {
                if (*reinterpret_cast<void***>(r14_3 + 64) && r12_51 != 1) {
                    rax82 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                    r13_16 = rax82;
                    goto addr_5ff1_17;
                }
            }
        } else {
            addr_61a5_50:
            if (v5 == 3 && ((eax83 = *reinterpret_cast<uint16_t*>(rbp4 + 0x68), *reinterpret_cast<int16_t*>(&eax83) != 7) && *reinterpret_cast<int16_t*>(&eax83) != 4)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 6;
            }
        }
    } else {
        addr_62cd_106:
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 7;
        *reinterpret_cast<void***>(r14_3 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) | 0x2000);
        if (r13_16) {
            do {
                rbp84 = r13_16;
                r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
                rdi85 = *reinterpret_cast<void***>(rbp84 + 24);
                if (rdi85) {
                    fun_2630(rdi85, rsi, rdi85, rsi);
                }
                fun_2450(rbp84, rbp84);
            } while (r13_16);
            goto addr_61da_11;
        }
    }
    if (r13_16) {
        do {
            rbp86 = r13_16;
            r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
            rdi87 = *reinterpret_cast<void***>(rbp86 + 24);
            if (rdi87) {
                fun_2630(rdi87, rsi, rdi87, rsi);
            }
            fun_2450(rbp86, rbp86);
        } while (r13_16);
        goto addr_61da_11;
    }
    addr_6090_103:
    rbp4 = v58;
    r14_3 = rbx55;
    r13_16 = v59;
    if (*reinterpret_cast<void***>(v54)) {
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(v54);
        rax88 = reinterpret_cast<unsigned char>(v9) | reinterpret_cast<unsigned char>(r12_51);
        eax89 = (*reinterpret_cast<uint32_t*>(&rax88) - (*reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax88) < *reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(rax88 < 1))) & 0xfffffffd) + 7;
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&eax89);
    }
    rdi90 = *reinterpret_cast<void***>(rbp4 + 24);
    if (rdi90) {
        fun_2630(rdi90, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
    }
    addr_60d9_128:
    if (v56) {
        addr_5c3e_129:
        rax91 = *reinterpret_cast<void***>(r14_3 + 8);
        rcx40 = *reinterpret_cast<void***>(r14_3 + 32);
        if (rax91) {
            do {
                rsi = rax91 + 0x100;
                if (*reinterpret_cast<void***>(rax91 + 48) != rsi) {
                    *reinterpret_cast<void***>(rax91 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 56))) + reinterpret_cast<unsigned char>(rcx40));
                }
                *reinterpret_cast<void***>(rax91 + 56) = rcx40;
                rax91 = *reinterpret_cast<void***>(rax91 + 16);
            } while (rax91);
        }
    } else {
        addr_60e4_134:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4) {
            if (*reinterpret_cast<void***>(r14_3 + 48) == v41 || !r12_51) {
                addr_60fb_53:
                --v43;
                goto addr_6101_136;
            } else {
                addr_6101_136:
                *reinterpret_cast<void***>(v43) = reinterpret_cast<void**>(0);
                goto addr_6109_137;
            }
        }
    }
    rax92 = r13_16;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_16 + 88)) >= reinterpret_cast<signed char>(0)) {
        do {
            rsi = rax92 + 0x100;
            if (*reinterpret_cast<void***>(rax92 + 48) != rsi) {
                *reinterpret_cast<void***>(rax92 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 56))) + reinterpret_cast<unsigned char>(rcx40));
            }
            rdx93 = *reinterpret_cast<void***>(rax92 + 16);
            *reinterpret_cast<void***>(rax92 + 56) = rcx40;
            if (rdx93) {
                rax92 = rdx93;
            } else {
                rax92 = *reinterpret_cast<void***>(rax92 + 8);
            }
        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax92 + 88)) >= reinterpret_cast<signed char>(0));
        goto addr_60e4_134;
    } else {
        goto addr_60e4_134;
    }
    addr_6109_137:
    if (v9) 
        goto addr_61a0_108;
    if (!v39) 
        goto addr_61a0_108;
    if (v5 == 1) 
        goto addr_6180_104;
    if (!r12_51) 
        goto addr_6180_104; else 
        goto addr_612c_110;
    addr_5f78_63:
    r13_94 = v59;
    r14_95 = rbx55;
    rbp96 = v58;
    ebx97 = *reinterpret_cast<void***>(v54);
    fun_2450(rax65, rax65);
    if (r13_94) {
        do {
            r12_98 = r13_94;
            r13_94 = *reinterpret_cast<void***>(r13_94 + 16);
            rdi99 = *reinterpret_cast<void***>(r12_98 + 24);
            if (rdi99) {
                fun_2630(rdi99, rsi64, rdi99, rsi64);
            }
            fun_2450(r12_98, r12_98);
        } while (r13_94);
    }
    rdi100 = *reinterpret_cast<void***>(rbp96 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2630(rdi100, rsi64, rdi100, rsi64);
    *reinterpret_cast<void***>(rbp96 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp96 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_95 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_95 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = ebx97;
    goto addr_5ff1_17;
    addr_63f3_70:
    r13_101 = v59;
    rbp102 = v58;
    r14_103 = rbx55;
    fun_2450(rax65, rax65);
    if (r13_101) {
        do {
            r12_104 = r13_101;
            r13_101 = *reinterpret_cast<void***>(r13_101 + 16);
            rdi105 = *reinterpret_cast<void***>(r12_104 + 24);
            if (rdi105) {
                fun_2630(rdi105, rsi64);
            }
            fun_2450(r12_104, r12_104);
        } while (r13_101);
    }
    rdi106 = *reinterpret_cast<void***>(rbp102 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2630(rdi106, rsi64);
    *reinterpret_cast<void***>(rbp102 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp102 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_103 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_103 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(36);
    goto addr_5ff1_17;
    addr_61e8_96:
    rbp4 = v58;
    r13_16 = v59;
    r14_3 = rbx55;
    goto addr_60d9_128;
    addr_5c28_61:
    r13_16 = v59;
    rbp4 = rax61;
    r14_3 = rbx55;
    if (!v56) 
        goto addr_60e4_134; else 
        goto addr_5c3e_129;
    addr_5a02_42:
    v39 = 1;
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    goto addr_5a0b_43;
    addr_63d4_31:
    if (rax32 == 0x5346414f || reinterpret_cast<int1_t>(rax32 == 0xff534d42)) {
        addr_63ea_29:
        edi28 = v107;
        goto addr_5ed5_27;
    } else {
        addr_6071_158:
        if (!reinterpret_cast<int1_t>(v5 == 3)) {
            addr_6230_24:
            v39 = 0;
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
            goto addr_5a0b_43;
        } else {
            edi28 = v108;
            r12d30 = 0;
            ebx31 = 1;
            goto addr_5ee3_33;
        }
    }
    addr_605c_32:
    if (!rax32) 
        goto addr_63ea_29;
    if (rax32 == 0x6969) 
        goto addr_63ea_29; else 
        goto addr_6071_158;
    addr_6280_13:
    v26 = reinterpret_cast<void**>(0x186a0);
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    v39 = 1;
    goto addr_5a0b_43;
    addr_59f9_14:
    v26 = reinterpret_cast<void**>(0xffffffffffffffff);
    goto addr_5a02_42;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xe160);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xe160);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xe160) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x6d08]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xe160);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x6b6f]");
        if (1) 
            goto addr_76fc_6;
        __asm__("comiss xmm1, [rip+0x6b66]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x6b58]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_76dc_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_76d2_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_76dc_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_76d2_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_76d2_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_76dc_13:
        return 0;
    } else {
        addr_76fc_6:
        return r8_3;
    }
}

struct s5 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s5* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int32_t transfer_entries(struct s4* rdi, struct s4* rsi, int32_t edx) {
    void** rdx3;
    struct s4* r14_4;
    int32_t r12d5;
    struct s4* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rax12;
    struct s6* rax13;
    void** rax14;
    void** rsi15;
    void** rax16;
    struct s7* r13_17;
    void** rax18;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_7766_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_7758_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_7766_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rax12 = reinterpret_cast<void**>(r14_4->f30(r15_11, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax13 = reinterpret_cast<struct s6*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax13->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax13->f8;
                            rax13->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_77de_9;
                        } else {
                            rax13->f0 = r15_11;
                            rax14 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14;
                            r14_4->f48 = r13_9;
                            if (!rdx3) 
                                goto addr_77de_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_28a9_12;
                    addr_77de_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_7758_3;
            }
            rsi15 = r14_4->f10;
            rax16 = reinterpret_cast<void**>(r14_4->f30(r15_8, rsi15));
            if (reinterpret_cast<unsigned char>(rax16) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_28a9_12;
            r13_17 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax16) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_17->f0) 
                goto addr_7818_16;
            r13_17->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_7818_16:
            rax18 = r14_4->f48;
            if (!rax18) {
                rax18 = fun_2700(16, rsi15, rdx3);
                if (!rax18) 
                    goto addr_788a_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax18 + 8);
            }
            rdx3 = r13_17->f8;
            *reinterpret_cast<void***>(rax18) = r15_8;
            *reinterpret_cast<void***>(rax18 + 8) = rdx3;
            r13_17->f8 = rax18;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_28a9_12:
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    fun_2470();
    addr_788a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_758f_23:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_7534_26;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_75a0_30;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_75a0_30;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_758f_23;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_7534_26;
            }
        }
    }
    addr_7591_34:
    return rax11;
    addr_7534_26:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_7591_34;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_75a0_30:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_2560();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2560();
    if (r8d > 10) {
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xe240 + rax11 * 4) + 0xe240;
    }
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2600();

struct s9 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s8* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s9* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x9d5f;
    rax8 = fun_2480();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
        fun_2470();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x13090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x9191]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x9deb;
            fun_2600();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s9*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x13120) {
                fun_2450(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x9e7a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_2590();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x130a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

void** fts_stat(void** rdi, void** rsi, signed char dl, ...) {
    void** rbp4;
    void** eax5;
    void** rsi6;
    void** rdi7;
    int32_t eax8;
    void** rax9;
    void** eax10;
    void** rdi11;
    int32_t eax12;
    uint32_t eax13;
    void** rax14;
    int64_t rax15;
    void** rdi16;
    int32_t eax17;
    void** rax18;
    int64_t* rdi19;
    int64_t rcx20;

    rbp4 = rsi + 0x70;
    eax5 = *reinterpret_cast<void***>(rdi + 72);
    if (*reinterpret_cast<unsigned char*>(&eax5) & 2) 
        goto addr_4f00_2;
    if (*reinterpret_cast<unsigned char*>(&eax5) & 1 && !*reinterpret_cast<void***>(rsi + 88)) {
        goto addr_4f00_2;
    }
    if (dl) {
        addr_4f00_2:
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        rdi7 = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(&rdi7 + 4) = 0;
        eax8 = fun_2870(rdi7, rsi6, rbp4);
        if (eax8 < 0) {
            rax9 = fun_2480();
            eax10 = *reinterpret_cast<void***>(rax9);
            if (eax10 == 2) {
                rsi6 = *reinterpret_cast<void***>(rsi + 48);
                rdi11 = *reinterpret_cast<void***>(rdi + 44);
                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                eax12 = fun_2870(rdi11, rsi6, rbp4, rdi11, rsi6, rbp4);
                if (eax12 < 0) {
                    eax10 = *reinterpret_cast<void***>(rax9);
                } else {
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
                    return 13;
                }
            }
        } else {
            addr_4f17_10:
            eax13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x88)) & 0xf000;
            if (eax13 == 0x4000) {
                *reinterpret_cast<uint32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 0x100) == 46) && (!*reinterpret_cast<signed char*>(rsi + 0x101) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x100)) & 0xffff00) == 0x2e00)) {
                    *reinterpret_cast<uint32_t*>(&rax14) = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 88)) < reinterpret_cast<unsigned char>(1)))) & 0xfffffffc) + 5;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_4f47_13;
                }
            } else {
                if (eax13 == 0xa000) {
                    *reinterpret_cast<uint32_t*>(&rax14) = 12;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_4f47_13;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<uint1_t>(eax13 == 0x8000);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax14) = static_cast<uint32_t>(rax15 + rax15 * 4 + 3);
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_4f47_13;
                }
            }
        }
    } else {
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        rdi16 = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
        eax17 = fun_2870(rdi16, rsi6, rbp4, rdi16, rsi6, rbp4);
        if (eax17 >= 0) 
            goto addr_4f17_10;
        rax18 = fun_2480();
        eax10 = *reinterpret_cast<void***>(rax18);
    }
    *reinterpret_cast<void***>(rsi + 64) = eax10;
    rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp4 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rsi + 0x70) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbp4 + 0x88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rcx20) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<uint64_t>(rdi19) + 0x90) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
    while (rcx20) {
        --rcx20;
        *rdi19 = 0;
        ++rdi19;
    }
    return 10;
    addr_4f47_13:
    return rax14;
}

void** cycle_check(void** rdi, void*** rsi);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, ...) {
    void** rdi6;
    void** rax7;
    void** rax8;
    void** rax9;
    void** rdi10;
    void** rax11;
    void** rax12;
    void** rax13;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rdi6 = *reinterpret_cast<void***>(rdi + 88);
        rax7 = cycle_check(rdi6, rsi + 0x70);
        if (*reinterpret_cast<signed char*>(&rax7)) {
            *reinterpret_cast<void***>(rsi) = rsi;
            *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
            return rax7;
        }
    } else {
        rax8 = fun_2700(24, rsi, rdx);
        if (!rax8) 
            goto addr_5558_5;
        rax9 = *reinterpret_cast<void***>(rsi + 0x70);
        rdi10 = *reinterpret_cast<void***>(rdi + 88);
        *reinterpret_cast<void***>(rax8 + 16) = rsi;
        *reinterpret_cast<void***>(rax8) = rax9;
        *reinterpret_cast<void***>(rax8 + 8) = *reinterpret_cast<void***>(rsi + 0x78);
        rax11 = hash_insert(rdi10, rax8, rdx, rcx, r8);
        if (rax8 != rax11) 
            goto addr_5503_7;
    }
    addr_5520_8:
    *reinterpret_cast<int32_t*>(&rax12) = 1;
    *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
    addr_5525_9:
    return rax12;
    addr_5503_7:
    fun_2450(rax8, rax8);
    if (!rax11) {
        addr_5558_5:
        *reinterpret_cast<int32_t*>(&rax12) = 0;
        *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
        goto addr_5525_9;
    } else {
        rax13 = *reinterpret_cast<void***>(rax11 + 16);
        *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
        *reinterpret_cast<void***>(rsi) = rax13;
        goto addr_5520_8;
    }
}

struct s10 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s10* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s10* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xe1e3);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xe1dc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xe1e7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xe1d8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g12db8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g12db8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_2463() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t abort = 0x2030;

void fun_2473() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2040;

void fun_2483() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2050;

void fun_2493() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2060;

void fun_24a3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2070;

void fun_24b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t unlinkat = 0x2080;

void fun_24c3() {
    __asm__("cli ");
    goto unlinkat;
}

int64_t qsort = 0x2090;

void fun_24d3() {
    __asm__("cli ");
    goto qsort;
}

int64_t isatty = 0x20a0;

void fun_24e3() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x20b0;

void fun_24f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t faccessat = 0x20c0;

void fun_2503() {
    __asm__("cli ");
    goto faccessat;
}

int64_t fcntl = 0x20d0;

void fun_2513() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20e0;

void fun_2523() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20f0;

void fun_2533() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2100;

void fun_2543() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2110;

void fun_2553() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2120;

void fun_2563() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2130;

void fun_2573() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2140;

void fun_2583() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x2150;

void fun_2593() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2160;

void fun_25a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2170;

void fun_25b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2180;

void fun_25c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2190;

void fun_25d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21a0;

void fun_25e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21b0;

void fun_25f3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21c0;

void fun_2603() {
    __asm__("cli ");
    goto memset;
}

int64_t geteuid = 0x21d0;

void fun_2613() {
    __asm__("cli ");
    goto geteuid;
}

int64_t close = 0x21e0;

void fun_2623() {
    __asm__("cli ");
    goto close;
}

int64_t closedir = 0x21f0;

void fun_2633() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x2200;

void fun_2643() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x2210;

void fun_2653() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2220;

void fun_2663() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2230;

void fun_2673() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x2240;

void fun_2683() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x2250;

void fun_2693() {
    __asm__("cli ");
    goto strcmp;
}

int64_t dirfd = 0x2260;

void fun_26a3() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x2270;

void fun_26b3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t rpmatch = 0x2280;

void fun_26c3() {
    __asm__("cli ");
    goto rpmatch;
}

int64_t memcpy = 0x2290;

void fun_26d3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x22a0;

void fun_26e3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x22b0;

void fun_26f3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x22c0;

void fun_2703() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22d0;

void fun_2713() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22e0;

void fun_2723() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22f0;

void fun_2733() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x2300;

void fun_2743() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x2310;

void fun_2753() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2320;

void fun_2763() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2330;

void fun_2773() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x2340;

void fun_2783() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x2350;

void fun_2793() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2360;

void fun_27a3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2370;

void fun_27b3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2380;

void fun_27c3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t dcngettext = 0x2390;

void fun_27d3() {
    __asm__("cli ");
    goto dcngettext;
}

int64_t fdopendir = 0x23a0;

void fun_27e3() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t fstatfs = 0x23b0;

void fun_27f3() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x23c0;

void fun_2803() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23d0;

void fun_2813() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23e0;

void fun_2823() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23f0;

void fun_2833() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2400;

void fun_2843() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2410;

void fun_2853() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2420;

void fun_2863() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x2430;

void fun_2873() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x2440;

void fun_2883() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2760(int64_t rdi, ...);

void fun_2540(int64_t rdi, int64_t rsi);

void fun_2520(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_24e0();

int32_t fun_25a0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** stdout = reinterpret_cast<void**>(0);

void** Version = reinterpret_cast<void**>(0xa6);

void version_etc(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2810();

int64_t usage();

void** argmatch_die = reinterpret_cast<void**>(80);

int64_t __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9);

int32_t optind = 0;

int32_t fun_2640(void** rdi, void** rsi, ...);

void** quotearg_n_style();

int64_t rm(void*** rdi, void* rsi, void** rdx, void** rcx);

int64_t get_root_dev_ino(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** fun_27d0();

void fun_25f0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, void** r8, void** r9, int64_t a7);

int64_t fun_2913(int32_t edi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void** rdi5;
    int64_t rax6;
    int64_t v7;
    int16_t v8;
    void* rsp9;
    int64_t* v10;
    int32_t r15d11;
    void** r8_12;
    void** rcx13;
    void** rdx14;
    void** rsi15;
    int64_t rdi16;
    int32_t eax17;
    void* rsp18;
    void** rdi19;
    void** r9_20;
    int64_t rax21;
    int1_t less22;
    int64_t r13_23;
    void** r12_24;
    void** r14_25;
    int32_t eax26;
    void** rax27;
    void** rax28;
    void** r13_29;
    void** rax30;
    void** rdi31;
    void** v32;
    int64_t v33;
    int64_t rax34;
    int64_t rax35;
    void*** r12_36;
    void** rdx37;
    int64_t rax38;
    int64_t rdx39;
    void** rax40;
    void** rax41;
    int64_t rax42;
    void** r13_43;
    void** rax44;
    void* rsp45;
    void** rax46;
    void** rdi47;
    void** v48;
    int64_t v49;
    unsigned char al50;
    int64_t v51;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(&rbp3 + 4) = 0;
    rbx4 = rsi;
    rdi5 = *reinterpret_cast<void***>(rsi);
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_2760(6, 6);
    fun_2540("coreutils", "/usr/local/share/locale");
    fun_2520("coreutils", "/usr/local/share/locale");
    atexit(0x4960, "/usr/local/share/locale");
    v8 = 0;
    fun_24e0();
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xd8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    *reinterpret_cast<signed char*>(&v10) = 1;
    r15d11 = 0;
    while (*reinterpret_cast<int32_t*>(&r8_12) = 0, *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0, rcx13 = reinterpret_cast<void**>(0x129e0), rdx14 = reinterpret_cast<void**>("dfirvIR"), rsi15 = rbx4, *reinterpret_cast<int32_t*>(&rdi16) = *reinterpret_cast<int32_t*>(&rbp3), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0, eax17 = fun_25a0(rdi16, rsi15, "dfirvIR", 0x129e0), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), eax17 != -1) {
        if (eax17 > 0x84) 
            goto addr_2a20_4;
        if (eax17 > 72) 
            goto addr_2a0c_6;
        if (eax17 == 0xffffff7d) {
            rdi19 = stdout;
            rcx13 = Version;
            r9_20 = reinterpret_cast<void**>("David MacKenzie");
            r8_12 = reinterpret_cast<void**>("Paul Rubin");
            rdx14 = reinterpret_cast<void**>("GNU coreutils");
            rsi15 = reinterpret_cast<void**>("rm");
            version_etc(rdi19, "rm", "GNU coreutils", rcx13);
            eax17 = fun_2810();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
        }
        if (eax17 != 0xffffff7e) 
            goto addr_2a20_4;
        usage();
        r9_20 = argmatch_die;
        rax21 = __xargmatch_internal("--interactive", rsi15, 0x129a0, 0xdc10, 4, r9_20);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
        if (*reinterpret_cast<int32_t*>(0xdc10 + rax21 * 4) == 1) {
            r15d11 = 1;
        } else {
            if (*reinterpret_cast<int32_t*>(0xdc10 + rax21 * 4) == 2) {
                r15d11 = 0;
            } else {
                if (*reinterpret_cast<int32_t*>(0xdc10 + rax21 * 4)) 
                    continue;
                r15d11 = 0;
            }
        }
    }
    less22 = optind < *reinterpret_cast<int32_t*>(&rbp3);
    if (less22) {
        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v8) + 1)) 
            goto addr_2c95_19;
        if (1) 
            goto addr_2df9_21; else 
            goto addr_2c95_19;
    } else {
        if (1) 
            goto addr_2eb6_23; else 
            goto addr_2c58_24;
    }
    addr_2a20_4:
    *reinterpret_cast<int32_t*>(&r13_23) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_23) + 4) = 0;
    r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 48);
    while (1) {
        if (*reinterpret_cast<int32_t*>(&rbp3) <= *reinterpret_cast<int32_t*>(&r13_23)) 
            goto addr_2def_26;
        r14_25 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<uint64_t>(r13_23 * 8));
        if (*reinterpret_cast<void***>(r14_25) != 45) 
            goto addr_2a42_28;
        if (!*reinterpret_cast<void***>(r14_25 + 1)) 
            goto addr_2a42_28;
        rsi15 = r12_24;
        eax26 = fun_2640(r14_25, rsi15, r14_25, rsi15);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        if (!eax26) 
            break;
        addr_2a42_28:
        ++r13_23;
    }
    rax27 = quotearg_style(4, r14_25, 4, r14_25);
    rax28 = quotearg_n_style();
    r13_29 = *reinterpret_cast<void***>(rbx4);
    rbp3 = rax28;
    rax30 = fun_2550();
    r9_20 = rax27;
    r8_12 = rbp3;
    rcx13 = r13_29;
    rdi31 = stderr;
    rdx14 = rax30;
    *reinterpret_cast<int32_t*>(&rsi15) = 1;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    fun_2830(rdi31, 1, rdx14, rcx13, r8_12, r9_20, v32, v10, rdi31, 1, rdx14, rcx13, r8_12, r9_20, v33, v10);
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_2def_26;
    addr_2a0c_6:
    *reinterpret_cast<uint32_t*>(&rax34) = eax17 - 73;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax34) <= 59) {
        goto *reinterpret_cast<int32_t*>(0xdb10 + rax34 * 4) + 0xdb10;
    }
    addr_2c71_33:
    return rax35;
    while (1) {
        rax38 = rm(r12_36, reinterpret_cast<int64_t>(rsp18) + 16, rdx37, rcx13);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&rax35) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax38) == 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0;
        if (static_cast<uint32_t>(rax38 - 2) <= 2) {
            while (1) {
                rdx39 = v7 - g28;
                if (!rdx39) 
                    goto addr_2c71_33;
                while (1) {
                    fun_2590();
                    fun_2550();
                    fun_27a0();
                    rax40 = quotearg_style(4, r8_12, 4, r8_12);
                    fun_2550();
                    rcx13 = rax40;
                    fun_27a0();
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    addr_2eb6_23:
                    rax41 = fun_2550();
                    *reinterpret_cast<int32_t*>(&rsi15) = 0;
                    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                    rdx14 = rax41;
                    fun_27a0();
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
                    addr_2def_26:
                    usage();
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    addr_2df9_21:
                    rax42 = get_root_dev_ino(0x130f0, rsi15, rdx14, rcx13);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    if (rax42) 
                        break;
                    quotearg_style(4, "/", 4, "/");
                    fun_2550();
                    fun_2480();
                    fun_27a0();
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                }
                addr_2c95_19:
                rdx37 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
                r12_36 = reinterpret_cast<void***>(rbx4 + reinterpret_cast<unsigned char>(rdx37) * 8);
                if (!*reinterpret_cast<signed char*>(&r15d11)) 
                    break;
                rbp3 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3) - *reinterpret_cast<int32_t*>(&rdx37)));
                if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v8) + 1)) {
                    addr_2d4a_41:
                    r13_43 = program_name;
                    rax44 = fun_27d0();
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    rdx37 = rax44;
                } else {
                    if (reinterpret_cast<unsigned char>(rbp3) <= reinterpret_cast<unsigned char>(3)) 
                        break;
                    r13_43 = program_name;
                    rax46 = fun_27d0();
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    rdx37 = rax46;
                }
                rdi47 = stderr;
                r8_12 = rbp3;
                rcx13 = r13_43;
                fun_2830(rdi47, 1, rdx37, rcx13, r8_12, r9_20, v48, v10, rdi47, 1, rdx37, rcx13, r8_12, r9_20, v49, v10);
                al50 = yesno(rdi47, 1, rdx37, rcx13, r8_12, r9_20);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8 - 8 + 8);
                if (al50) 
                    break;
                addr_2c58_24:
                *reinterpret_cast<uint32_t*>(&rax35) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0;
            }
        } else {
            fun_25f0("VALID_STATUS (status)", "src/rm.c", 0x173, "main", r8_12, r9_20, v51);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            goto addr_2d4a_41;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_2ee3() {
    __asm__("cli ");
    __libc_start_main(0x2910, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x13008;

void fun_2460(int64_t rdi);

int64_t fun_2f83() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2460(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2fc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_2660(void** rdi, void** rsi, void** rdx, void** rcx);

unsigned char free;

unsigned char g1;

signed char g2;

int32_t fun_2490(void** rdi, void** rsi, void** rdx, ...);

void fun_2fd3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** rcx4;
    void** r12_5;
    void** rax6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** r12_23;
    void** rax24;
    uint32_t ecx25;
    uint32_t ecx26;
    int1_t zf27;
    void** rax28;
    void** rax29;
    int32_t eax30;
    void** rax31;
    void** r13_32;
    void** rax33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    void** r14_37;
    void** rax38;
    void** rax39;
    void** rax40;
    void** rdi41;
    void** r8_42;
    void** r9_43;
    void** v44;
    int64_t* v45;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2550();
            fun_2770(1, rax3, r12_2, rcx4);
            r12_5 = stdout;
            rax6 = fun_2550();
            fun_2660(rax6, r12_5, 5, rcx4);
            r12_7 = stdout;
            rax8 = fun_2550();
            fun_2660(rax8, r12_7, 5, rcx4);
            r12_9 = stdout;
            rax10 = fun_2550();
            fun_2660(rax10, r12_9, 5, rcx4);
            r12_11 = stdout;
            rax12 = fun_2550();
            fun_2660(rax12, r12_11, 5, rcx4);
            r12_13 = stdout;
            rax14 = fun_2550();
            fun_2660(rax14, r12_13, 5, rcx4);
            r12_15 = stdout;
            rax16 = fun_2550();
            fun_2660(rax16, r12_15, 5, rcx4);
            r12_17 = stdout;
            rax18 = fun_2550();
            fun_2660(rax18, r12_17, 5, rcx4);
            r12_19 = stdout;
            rax20 = fun_2550();
            fun_2660(rax20, r12_19, 5, rcx4);
            r12_21 = program_name;
            rax22 = fun_2550();
            fun_2770(1, rax22, r12_21, r12_21);
            r12_23 = stdout;
            rax24 = fun_2550();
            fun_2660(rax24, r12_23, 5, r12_21);
            do {
                if (1) 
                    break;
                ecx25 = free;
            } while (0x72 != ecx25 || ((ecx26 = g1, 0x6d != ecx26) || (zf27 = g2 == 0, !zf27)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax28 = fun_2550();
                fun_2770(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax29 = fun_2760(5);
                if (!rax29 || (eax30 = fun_2490(rax29, "en_", 3, rax29, "en_", 3), !eax30)) {
                    rax31 = fun_2550();
                    r12_2 = reinterpret_cast<void**>("rm");
                    r13_32 = reinterpret_cast<void**>(" invocation");
                    fun_2770(1, rax31, "https://www.gnu.org/software/coreutils/", "rm");
                } else {
                    r12_2 = reinterpret_cast<void**>("rm");
                    goto addr_33fb_9;
                }
            } else {
                rax33 = fun_2550();
                fun_2770(1, rax33, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax34 = fun_2760(5);
                if (!rax34 || (eax35 = fun_2490(rax34, "en_", 3, rax34, "en_", 3), !eax35)) {
                    addr_32fe_11:
                    rax36 = fun_2550();
                    r13_32 = reinterpret_cast<void**>(" invocation");
                    fun_2770(1, rax36, "https://www.gnu.org/software/coreutils/", "rm");
                    if (!reinterpret_cast<int1_t>(r12_2 == "rm")) {
                        r13_32 = reinterpret_cast<void**>(0xe601);
                    }
                } else {
                    addr_33fb_9:
                    r14_37 = stdout;
                    rax38 = fun_2550();
                    fun_2660(rax38, r14_37, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_32fe_11;
                }
            }
            rax39 = fun_2550();
            rcx4 = r13_32;
            fun_2770(1, rax39, r12_2, rcx4);
            addr_3029_14:
            fun_2810();
        }
    } else {
        rax40 = fun_2550();
        rdi41 = stderr;
        rcx4 = r12_2;
        fun_2830(rdi41, 1, rax40, rcx4, r8_42, r9_43, v44, v45);
        goto addr_3029_14;
    }
}

struct s1* xfts_open();

void rpl_fts_set(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** rpl_fts_read(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** quotearg_n_style_colon();

struct s11 {
    signed char f0;
    signed char f1;
};

struct s11* last_component(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** file_name_concat(void** rdi, int64_t rsi);

int32_t rpl_fts_close(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t fun_3bd3(int64_t* rdi, void** rsi) {
    void* rsp3;
    int64_t rax4;
    int64_t v5;
    uint32_t r12d6;
    void** rbx7;
    void** rdx8;
    void** rsi9;
    struct s1* rax10;
    void* rsp11;
    struct s1* rbp12;
    int64_t rax13;
    void** rsi14;
    void** r15_15;
    void** rax16;
    void** rcx17;
    void* rsp18;
    void* rsp19;
    void** rax20;
    void** r8_21;
    void** r9_22;
    void** rax23;
    void** rcx24;
    void** rdi25;
    int32_t v26;
    int64_t* v27;
    void* v28;
    int32_t eax29;
    void** rdi30;
    void** rax31;
    void* rsp32;
    void** r14_33;
    void** rax34;
    void* rsp35;
    void** r13_36;
    struct s0* rax37;
    void* rdx38;
    uint32_t eax39;
    int32_t eax40;
    void** rax41;
    void** rax42;
    int64_t rax43;
    uint32_t ecx44;
    void** r14d45;
    void** rdi46;
    uint32_t eax47;
    void** r13_48;
    uint32_t eax49;
    void** rdi50;
    uint32_t eax51;
    int32_t v52;
    uint32_t eax53;
    struct s11* rax54;
    int64_t rdx55;
    uint32_t eax56;
    void** rdx57;
    void** rax58;
    void* rsp59;
    int32_t eax60;
    void* rsp61;
    void** v62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** rax66;
    void** rax67;
    void** rax68;
    void** rax69;
    void** rsi70;
    void** rax71;
    void** rax72;
    void* rsp73;
    void** rax74;
    void** rax75;
    void** rax76;
    void** rsi77;
    void** rax78;
    void** rax79;
    void** rax80;
    void** rax81;
    void* rsp82;
    void** rax83;
    void** rsi84;
    int32_t eax85;
    int64_t rax86;
    int32_t eax87;
    int64_t v88;
    void** r12d89;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8);
    rax4 = g28;
    v5 = rax4;
    if (!*rdi) {
        r12d6 = 2;
        goto addr_3d28_3;
    } else {
        rbx7 = rsi;
        r12d6 = 2;
        rdx8 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rsi9) = (*reinterpret_cast<uint32_t*>(&rsi) - (*reinterpret_cast<uint32_t*>(&rsi) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi) < *reinterpret_cast<uint32_t*>(&rsi) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) < 1))) & 0xffffffc0) + 0x258;
        *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        rax10 = xfts_open();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8);
        rbp12 = rax10;
        goto addr_3c28_5;
    }
    addr_3d3f_6:
    *reinterpret_cast<uint32_t*>(&rax13) = r12d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    return rax13;
    while (1) {
        addr_4030_7:
        while (1) {
            rsi14 = *reinterpret_cast<void***>(r15_15 + 56);
            rax16 = quotearg_style(4, rsi14, 4, rsi14);
            fun_2550();
            rcx17 = rax16;
            fun_27a0();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
            rax20 = *reinterpret_cast<void***>(r15_15 + 8);
            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax20 + 88)) >= reinterpret_cast<signed char>(0)) {
                do {
                    if (*reinterpret_cast<void***>(rax20 + 32)) 
                        break;
                    *reinterpret_cast<void***>(rax20 + 32) = reinterpret_cast<void**>(1);
                    rax20 = *reinterpret_cast<void***>(rax20 + 8);
                } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax20 + 88)) >= reinterpret_cast<signed char>(0));
                goto addr_3e87_11;
            } else {
                goto addr_3e87_11;
            }
            while (1) {
                addr_3e87_11:
                rdx8 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                rsi9 = r15_15;
                r12d6 = 4;
                rpl_fts_set(rbp12, rsi9, 4, rcx17, r8_21, r9_22);
                rpl_fts_read(rbp12, rsi9, 4, rcx17, r8_21, r9_22);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
                addr_3c28_5:
                while (rax23 = rpl_fts_read(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22), rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), r15_15 = rax23, !!rax23) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rcx24) = *reinterpret_cast<uint16_t*>(rax23 + 0x68);
                        *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                        if (*reinterpret_cast<int16_t*>(&rcx24) == 1) {
                            addr_3dc0_15:
                            if (!*reinterpret_cast<signed char*>(rbx7 + 9)) {
                                if (!*reinterpret_cast<signed char*>(rbx7 + 10)) 
                                    goto addr_4030_7;
                                rsi9 = *reinterpret_cast<void***>(rax23 + 48);
                                *reinterpret_cast<int32_t*>(&rdi25) = rbp12->f2c;
                                *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0;
                                eax29 = fun_2580(rdi25, rsi9, 0x30900, rcx24, r8_21, r9_22, v26, v27, v28);
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                if (eax29 < 0) 
                                    goto addr_3e3a_18;
                                *reinterpret_cast<int32_t*>(&rdi30) = eax29;
                                *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
                                rax31 = fun_27e0(rdi30, rsi9, rdi30, rsi9);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                r14_33 = rax31;
                                if (!rax31) 
                                    goto addr_4437_20;
                                rax34 = fun_2480();
                                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                *reinterpret_cast<void***>(rax34) = reinterpret_cast<void**>(0);
                                r13_36 = rax34;
                                do {
                                    rax37 = fun_26f0(r14_33, rsi9, r14_33, rsi9);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                    if (!rax37) 
                                        break;
                                    if (*reinterpret_cast<void***>(&rax37->f13) != 46) 
                                        goto addr_3e2a_24;
                                    *reinterpret_cast<int32_t*>(&rdx38) = 0;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    *reinterpret_cast<unsigned char*>(&rdx38) = reinterpret_cast<uint1_t>(rax37->f14 == 46);
                                    eax39 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax37) + reinterpret_cast<int64_t>(rdx38) + 20);
                                } while (!*reinterpret_cast<signed char*>(&eax39) || *reinterpret_cast<signed char*>(&eax39) == 47);
                                goto addr_426e_26;
                                *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v27) + 4) = *reinterpret_cast<void***>(r13_36);
                                fun_2630(r14_33, rsi9, r14_33, rsi9);
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                rdx8 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v27) + 4);
                                *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                                *reinterpret_cast<void***>(r13_36) = rdx8;
                                if (rdx8) 
                                    goto addr_3e3a_18;
                            }
                        } else {
                            eax40 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx24 + 0xfffffffffffffffe));
                            if (*reinterpret_cast<uint16_t*>(&eax40) > 11) {
                                addr_3d6c_30:
                                rax41 = quotearg_n_style_colon();
                                r12d6 = *reinterpret_cast<uint16_t*>(r15_15 + 0x68);
                                rbx7 = rax41;
                                rax42 = fun_2550();
                                r9_22 = reinterpret_cast<void**>("bug-coreutils@gnu.org");
                                *reinterpret_cast<uint32_t*>(&rsi9) = 0;
                                *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
                                rdx8 = rax42;
                                r8_21 = rbx7;
                                *reinterpret_cast<uint32_t*>(&rcx24) = r12d6;
                                *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                fun_27a0();
                                rax23 = fun_2470();
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_3dc0_15;
                            } else {
                                rax43 = 1 << *reinterpret_cast<unsigned char*>(&rcx24);
                                if (!(*reinterpret_cast<uint32_t*>(&rax43) & 0x3d58)) {
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 7) 
                                        goto addr_3eb0_33;
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 2) 
                                        goto addr_3e50_35; else 
                                        goto addr_3d6c_30;
                                } else {
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 6 && (*reinterpret_cast<void***>(rbx7 + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_15 + 88)) > reinterpret_cast<signed char>(0))) {
                                        if (*reinterpret_cast<void***>(r15_15 + 0x70) != rbp12->f18) 
                                            goto addr_433c_38;
                                    }
                                    ecx44 = *reinterpret_cast<uint32_t*>(&rcx24) & 0xfffffffd;
                                    r14d45 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdi46) = rbp12->f2c;
                                    *reinterpret_cast<int32_t*>(&rdi46 + 4) = 0;
                                    *reinterpret_cast<int32_t*>(&r8_21) = 3;
                                    *reinterpret_cast<int32_t*>(&r8_21 + 4) = 0;
                                    rsi9 = r15_15;
                                    rcx17 = rbx7;
                                    *reinterpret_cast<unsigned char*>(&r14d45) = reinterpret_cast<uint1_t>(*reinterpret_cast<int16_t*>(&ecx44) == 4);
                                    *reinterpret_cast<int32_t*>(&r9_22) = 0;
                                    *reinterpret_cast<int32_t*>(&r9_22 + 4) = 0;
                                    rdx8 = r14d45;
                                    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                                    eax47 = prompt_isra_0(rdi46, rsi9, rdx8, rcx17, 3, 0);
                                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                    *reinterpret_cast<uint32_t*>(&r13_48) = eax47;
                                    *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                    if (eax47 == 2) {
                                        rcx17 = r14d45;
                                        *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                                        rdx8 = rbx7;
                                        rsi9 = r15_15;
                                        eax49 = excise(rbp12, rsi9, rdx8, rcx17, 3);
                                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                        *reinterpret_cast<uint32_t*>(&r13_48) = eax49;
                                        *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                        goto addr_3cc4_42;
                                    }
                                }
                            }
                        }
                        if (*reinterpret_cast<void***>(r15_15 + 88)) {
                            addr_3f4c_44:
                            *reinterpret_cast<int32_t*>(&rdi50) = rbp12->f2c;
                            *reinterpret_cast<int32_t*>(&rdi50 + 4) = 0;
                            r9_22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp19) + 28);
                            rcx17 = rbx7;
                            rdx8 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&r8_21) = 2;
                            *reinterpret_cast<int32_t*>(&r8_21 + 4) = 0;
                            rsi9 = r15_15;
                            eax51 = prompt_isra_0(rdi50, rsi9, 1, rcx17, 2, r9_22);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                            *reinterpret_cast<uint32_t*>(&r13_48) = eax51;
                            *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                            if (eax51 == 2) {
                                if (v52 != 4) 
                                    goto addr_3c28_5;
                                rcx17 = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                                eax53 = excise(rbp12, r15_15, rbx7, 1, 2, rbp12, r15_15, rbx7, 1, 2);
                                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&r13_48) = eax53;
                                *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                if (eax53 == 2) 
                                    break;
                            }
                        } else {
                            r13_48 = *reinterpret_cast<void***>(r15_15 + 48);
                            rax54 = last_component(r13_48, rsi9, rdx8, rcx24, r8_21, r9_22);
                            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                            if (rax54->f0 != 46) 
                                goto addr_3f2c_48;
                            *reinterpret_cast<int32_t*>(&rdx55) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0;
                            *reinterpret_cast<unsigned char*>(&rdx55) = reinterpret_cast<uint1_t>(rax54->f1 == 46);
                            eax56 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax54) + rdx55 + 1);
                            if (!*reinterpret_cast<signed char*>(&eax56)) 
                                goto addr_40c2_50;
                            if (*reinterpret_cast<signed char*>(&eax56) == 47) 
                                goto addr_40c2_50;
                            addr_3f2c_48:
                            if (!*reinterpret_cast<void***>(rbx7 + 16)) 
                                goto addr_3f42_52;
                            if (*reinterpret_cast<void***>(r15_15 + 0x78) != *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx7 + 16))) 
                                goto addr_3f42_52;
                            if (*reinterpret_cast<void***>(r15_15 + 0x70) == *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx7 + 16) + 8)) 
                                goto addr_41ee_55;
                            addr_3f42_52:
                            if (*reinterpret_cast<void***>(rbx7 + 24)) 
                                goto addr_4140_56; else 
                                goto addr_3f4c_44;
                        }
                        rdx57 = *reinterpret_cast<void***>(r15_15 + 8);
                        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx57 + 88)) >= reinterpret_cast<signed char>(0)) {
                            do {
                                if (*reinterpret_cast<void***>(rdx57 + 32)) 
                                    break;
                                *reinterpret_cast<void***>(rdx57 + 32) = reinterpret_cast<void**>(1);
                                rdx57 = *reinterpret_cast<void***>(rdx57 + 8);
                            } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx57 + 88)) >= reinterpret_cast<signed char>(0));
                        }
                        rdx8 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                        rsi9 = r15_15;
                        rpl_fts_set(rbp12, rsi9, 4, rcx17, 2, r9_22);
                        rpl_fts_read(rbp12, rsi9, 4, rcx17, 2, r9_22);
                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                        addr_3cc4_42:
                        if (static_cast<uint32_t>(reinterpret_cast<uint64_t>(r13_48 + 0xfffffffffffffffe)) > 2) 
                            goto addr_4413_62;
                        if (*reinterpret_cast<uint32_t*>(&r13_48) == 4) 
                            goto addr_4444_64;
                        if (*reinterpret_cast<uint32_t*>(&r13_48) != 3) 
                            goto addr_3c28_5;
                        if (r12d6 == 2) {
                            r12d6 = 3;
                            continue;
                        }
                        addr_4140_56:
                        rax58 = file_name_concat(r13_48, "..");
                        rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                        eax60 = fun_2640(rax58, reinterpret_cast<int64_t>(rsp59) + 32);
                        rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                        if (eax60) 
                            goto addr_42e0_68;
                        fun_2450(rax58, rax58);
                        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                        if (rbp12->f18 == v62) 
                            goto addr_3f4c_44; else 
                            goto addr_4180_70;
                        rax23 = rpl_fts_read(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22);
                        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        r15_15 = rax23;
                    } while (rax23);
                    break;
                    rdx8 = reinterpret_cast<void**>(4);
                    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                    rsi9 = r15_15;
                    rpl_fts_set(rbp12, rsi9, 4, 1, 2, r9_22);
                    rpl_fts_read(rbp12, rsi9, 4, 1, 2, r9_22);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                    continue;
                    addr_4444_64:
                    r12d6 = 4;
                    continue;
                    addr_433c_38:
                    rax63 = *reinterpret_cast<void***>(r15_15 + 8);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax63 + 88)) >= reinterpret_cast<signed char>(0)) {
                        do {
                            if (*reinterpret_cast<void***>(rax63 + 32)) 
                                break;
                            *reinterpret_cast<void***>(rax63 + 32) = reinterpret_cast<void**>(1);
                            rax63 = *reinterpret_cast<void***>(rax63 + 8);
                        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax63 + 88)) >= reinterpret_cast<signed char>(0));
                    }
                    rsi64 = *reinterpret_cast<void***>(r15_15 + 56);
                    rax65 = quotearg_style(4, rsi64, 4, rsi64);
                    rax66 = fun_2550();
                    rcx17 = rax65;
                    *reinterpret_cast<uint32_t*>(&rsi9) = 0;
                    *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
                    rdx8 = rax66;
                    r12d6 = 4;
                    fun_27a0();
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                break;
                addr_40c2_50:
                rax67 = quotearg_n_style();
                rax68 = quotearg_n_style();
                rax69 = quotearg_n_style();
                fun_2550();
                r9_22 = rax67;
                r8_21 = rax68;
                rcx17 = rax69;
                fun_27a0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_41ee_55:
                rsi70 = *reinterpret_cast<void***>(r15_15 + 56);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi70) == 47) || *reinterpret_cast<void***>(rsi70 + 1)) {
                    rax71 = quotearg_n_style();
                    rax72 = quotearg_n_style();
                    fun_2550();
                    r8_21 = rax71;
                    rcx17 = rax72;
                    fun_27a0();
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    rax74 = quotearg_style(4, rsi70, 4, rsi70);
                    fun_2550();
                    rcx17 = rax74;
                    fun_27a0();
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                addr_41c1_80:
                fun_2550();
                fun_27a0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8);
                continue;
                addr_42e0_68:
                rax75 = quotearg_n_style();
                rax76 = quotearg_n_style();
                fun_2550();
                r8_21 = rax75;
                rcx17 = rax76;
                fun_27a0();
                fun_2450(rax58, rax58);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_4180_70:
                rsi77 = *reinterpret_cast<void***>(r15_15 + 56);
                rax78 = quotearg_style(4, rsi77);
                fun_2550();
                rcx17 = rax78;
                fun_27a0();
                rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_41c1_80;
                addr_3eb0_33:
                rax79 = quotearg_n_style_colon();
                fun_2550();
                rcx17 = rax79;
                fun_27a0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_3e50_35:
                rax80 = quotearg_n_style_colon();
                fun_2550();
                rcx17 = rax80;
                fun_27a0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            rax81 = fun_2480();
            rsp82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            rbx7 = rax81;
            if (*reinterpret_cast<void***>(rax81)) {
                r12d6 = 4;
                rax83 = fun_2550();
                rsi84 = *reinterpret_cast<void***>(rbx7);
                *reinterpret_cast<int32_t*>(&rsi84 + 4) = 0;
                fun_27a0();
                eax85 = rpl_fts_close(rbp12, rsi84, rax83, rcx17, r8_21, r9_22);
                rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8 - 8 + 8 - 8 + 8);
                if (!eax85) {
                    addr_3d28_3:
                    rax86 = v5 - g28;
                    if (!rax86) 
                        goto addr_3d3f_6;
                } else {
                    addr_42af_83:
                    r12d6 = 4;
                    fun_2550();
                    fun_27a0();
                    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8 - 8 + 8);
                    goto addr_3d28_3;
                }
            } else {
                eax87 = rpl_fts_close(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22);
                rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8);
                if (eax87) 
                    goto addr_42af_83; else 
                    goto addr_3d28_3;
            }
            addr_4432_85:
            fun_2590();
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8);
            addr_4437_20:
            fun_2620();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
            addr_3e3a_18:
            if (!*reinterpret_cast<signed char*>(rbx7 + 10)) 
                break;
            continue;
            addr_4413_62:
            fun_25f0("VALID_STATUS (s)", "src/remove.c", 0x263, "rm", r8_21, r9_22, v88);
            rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            goto addr_4432_85;
            addr_3e2a_24:
            r12d89 = *reinterpret_cast<void***>(r13_36);
            fun_2630(r14_33, rsi9, r14_33, rsi9);
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            *reinterpret_cast<void***>(r13_36) = r12d89;
            goto addr_3e3a_18;
            addr_426e_26:
            goto addr_3e2a_24;
        }
    }
}

void fun_4453() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_2650(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_4463(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2570(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2490(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_44e3_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_4530_6; else 
                    continue;
            } else {
                rax18 = fun_2570(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_4560_8;
                if (v16 == -1) 
                    goto addr_451e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_44e3_5;
            } else {
                eax19 = fun_2650(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_44e3_5;
            }
            addr_451e_10:
            v16 = rbx15;
            goto addr_44e3_5;
        }
    }
    addr_4545_16:
    return v12;
    addr_4530_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_4545_16;
    addr_4560_8:
    v12 = rbx15;
    goto addr_4545_16;
}

int32_t fun_2690(int64_t rdi, int64_t rsi);

int64_t fun_4573(int64_t rdi, int64_t* rsi) {
    int64_t r12_3;
    int64_t rdi4;
    int64_t* rbp5;
    int64_t rbx6;
    int32_t eax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_45b8_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            eax7 = fun_2690(rdi4, r12_3);
            if (!eax7) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6];
        } while (rdi4);
        goto addr_45b8_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_45d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2550();
    } else {
        fun_2550();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_27a0;
}

void** quote(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_4663(int64_t* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rdx9;
    void** rbp10;
    void** r14_11;
    int64_t* v12;
    void** rax13;
    void** rsi14;
    int64_t r15_15;
    int64_t rbx16;
    uint32_t eax17;
    void** rax18;
    void** rdi19;
    void** v20;
    void** rax21;
    void** rdi22;
    void** v23;
    void** rdi24;
    void** rax25;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    r12_8 = rdx;
    *reinterpret_cast<int32_t*>(&rdx9) = 5;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    rbp10 = rsi;
    r14_11 = stderr;
    v12 = rdi;
    rax13 = fun_2550();
    rsi14 = r14_11;
    fun_2660(rax13, rsi14, 5, rcx);
    r15_15 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (r15_15) {
        do {
            if (!rbx16 || (rdx9 = r12_8, rsi14 = rbp10, eax17 = fun_2650(r13_7, rsi14, rdx9, r13_7, rsi14, rdx9), !!eax17)) {
                r13_7 = rbp10;
                rax18 = quote(r15_15, rsi14, rdx9, rcx);
                rdi19 = stderr;
                rdx9 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                rcx = rax18;
                fun_2830(rdi19, 1, "\n  - %s", rcx, r8, r9, v20, v12);
            } else {
                rax21 = quote(r15_15, rsi14, rdx9, rcx);
                rdi22 = stderr;
                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                rdx9 = reinterpret_cast<void**>(", %s");
                rcx = rax21;
                fun_2830(rdi22, 1, ", %s", rcx, r8, r9, v23, v12);
            }
            ++rbx16;
            rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r12_8));
            r15_15 = v12[rbx16];
        } while (r15_15);
    }
    rdi24 = stderr;
    rax25 = *reinterpret_cast<void***>(rdi24 + 40);
    if (reinterpret_cast<unsigned char>(rax25) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi24 + 48))) {
        *reinterpret_cast<void***>(rdi24 + 40) = rax25 + 1;
        *reinterpret_cast<void***>(rax25) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(int64_t* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_4793(int64_t rdi, int64_t rsi, int64_t* rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    int64_t r14_9;
    int64_t r13_10;
    int64_t r12_11;
    int64_t* rbp12;
    int64_t v13;
    int64_t rax14;
    int64_t rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_47d7_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_484e_4;
        } else {
            addr_484e_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_2690(rdi15, r14_9);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16];
        } while (rdi15);
        goto addr_47d0_8;
    } else {
        goto addr_47d0_8;
    }
    return rbx16;
    addr_47d0_8:
    rax14 = -1;
    goto addr_47d7_3;
}

struct s12 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_4863(void** rdi, struct s12* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2650(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

struct s13 {
    unsigned char f0;
    unsigned char f1;
};

struct s13* fun_48c3(struct s13* rdi) {
    uint32_t edx2;
    struct s13* rax3;
    struct s13* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s13*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s13*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s13*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_4923(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2570(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int64_t file_name = 0;

void fun_4953(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

void** stdin = reinterpret_cast<void**>(0);

int64_t freadahead(void** rdi);

int32_t rpl_fseeko(void** rdi);

int32_t rpl_fflush(void** rdi);

int32_t close_stream(void** rdi);

void close_stdout();

int32_t exit_failure = 1;

void** fun_24a0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** quotearg_colon(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_4963() {
    void** rbp1;
    int64_t rax2;
    int32_t eax3;
    void** rdi4;
    int32_t eax5;
    int32_t eax6;
    void** rax7;
    int64_t r13_8;
    void** r12_9;
    void** rax10;
    int64_t rsi11;
    void** rcx12;
    int64_t rdx13;
    int64_t rdi14;
    void** r8_15;
    void** rax16;
    int32_t eax17;

    __asm__("cli ");
    rbp1 = stdin;
    rax2 = freadahead(rbp1);
    if (rax2) {
        eax3 = rpl_fseeko(rbp1);
        rdi4 = stdin;
        if (eax3 || (eax5 = rpl_fflush(rdi4), rdi4 = stdin, eax5 == 0)) {
            eax6 = close_stream(rdi4);
            if (!eax6) {
                addr_4989_4:
                goto close_stdout;
            } else {
                addr_49bf_5:
                rax7 = fun_2550();
                r13_8 = file_name;
                r12_9 = rax7;
                rax10 = fun_2480();
                if (!r13_8) {
                    while (1) {
                        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                        rcx12 = r12_9;
                        rdx13 = reinterpret_cast<int64_t>("%s");
                        fun_27a0();
                        close_stdout();
                        addr_4a0f_7:
                        *reinterpret_cast<int32_t*>(&rdi14) = exit_failure;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                        rax10 = fun_24a0(rdi14, rsi11, rdx13, rcx12, r8_15);
                    }
                } else {
                    rax16 = quotearg_colon(r13_8, "error closing file", 5);
                    *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                    r8_15 = r12_9;
                    rcx12 = rax16;
                    rdx13 = reinterpret_cast<int64_t>("%s: %s");
                    fun_27a0();
                    close_stdout();
                    goto addr_4a0f_7;
                }
            }
        } else {
            close_stream(rdi4);
            goto addr_49bf_5;
        }
    } else {
        eax17 = close_stream(rbp1);
        if (eax17) 
            goto addr_49bf_5; else 
            goto addr_4989_4;
    }
}

int64_t file_name = 0;

void fun_4a63(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4a73(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

void fun_4a83() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2480(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2550();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4b13_5;
        rax10 = quotearg_colon(rdi9, "write error", 5);
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_27a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24a0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4b13_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_27a0();
    }
}

int32_t fun_2730(void** rdi);

void fun_4b33(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2730(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

struct s14 {
    signed char[24] pad24;
    uint32_t f18;
    signed char[20] pad48;
    int64_t f30;
};

void fun_4b83(struct s14* rdi) {
    uint32_t eax2;

    __asm__("cli ");
    eax2 = rdi->f18 & 0xf000;
    if (eax2 == 0x8000) {
        if (rdi->f30) {
            goto fun_2550;
        } else {
            goto fun_2550;
        }
    } else {
        if (eax2 == 0x4000) {
            goto fun_2550;
        } else {
            if (eax2 == 0xa000) {
                goto fun_2550;
            } else {
                if (eax2 == 0x6000) {
                    goto fun_2550;
                } else {
                    if (eax2 == 0x2000) {
                        goto fun_2550;
                    } else {
                        if (eax2 == 0x1000) {
                            goto fun_2550;
                        } else {
                            if (eax2 == 0xc000) {
                                goto fun_2550;
                            } else {
                                goto fun_2550;
                            }
                        }
                    }
                }
            }
        }
    }
}

int64_t mfile_name_concat();

void xalloc_die();

void fun_4c73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* base_len(struct s11* rdi);

signed char* fun_2780(void** rdi, void** rsi, void** rdx);

void** fun_4c93(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    struct s11* rax7;
    void* rax8;
    void** r14_9;
    void** rax10;
    void* rbx11;
    uint1_t zf12;
    int32_t eax13;
    unsigned char v14;
    int1_t zf15;
    int32_t eax16;
    void** rax17;
    signed char* rax18;
    uint32_t ecx19;
    void** rdi20;
    signed char* rax21;

    __asm__("cli ");
    rax7 = last_component(rdi, rsi, rdx, rcx, r8, r9);
    rax8 = base_len(rax7);
    r14_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax7) - reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax8));
    rax10 = fun_2570(rsi);
    if (!rax8) {
        *reinterpret_cast<int32_t*>(&rbx11) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
        zf12 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi) == 47);
        eax13 = 46;
        if (!zf12) {
            eax13 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx11) = zf12;
        v14 = *reinterpret_cast<unsigned char*>(&eax13);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_9) + 0xffffffffffffffff) == 47) {
            v14 = 0;
            *reinterpret_cast<int32_t*>(&rbx11) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx11) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
            zf15 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47);
            eax16 = 47;
            if (zf15) {
                eax16 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx11) = reinterpret_cast<uint1_t>(!zf15);
            v14 = *reinterpret_cast<unsigned char*>(&eax16);
        }
    }
    rax17 = fun_2700(reinterpret_cast<unsigned char>(r14_9) + reinterpret_cast<unsigned char>(rax10) + 1 + reinterpret_cast<uint64_t>(rbx11), rsi, rdx);
    if (rax17) {
        rax18 = fun_2780(rax17, rdi, r14_9);
        ecx19 = v14;
        rdi20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax18) + reinterpret_cast<uint64_t>(rbx11));
        *rax18 = *reinterpret_cast<signed char*>(&ecx19);
        if (rdx) {
            *reinterpret_cast<void***>(rdx) = rdi20;
        }
        rax21 = fun_2780(rdi20, rsi, rax10);
        *rax21 = 0;
    }
    return rax17;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[8] pad88;
    int64_t f58;
};

int64_t fun_4d93(struct s15* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    if (rdi->f28 <= rdi->f20 && (rax2 = rdi->f10 - rdi->f8, !!(rdi->f0 & 0x100))) {
        rax2 = rax2 + (rdi->f58 - rdi->f48);
    }
    return rax2;
}

int32_t fun_26e0(void** rdi);

int64_t fun_25e0(int64_t rdi, ...);

int64_t fun_4dc3(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        eax4 = fun_26e0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25e0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

struct s16 {
    int64_t f0;
    int64_t f8;
};

struct s17 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_4e43(struct s16* rdi, struct s17* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f8 == rsi->f8) {
        rax3 = rsi->f0;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f0 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s18 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_4e73(struct s18* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

uint64_t fun_4e93(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_4ea3(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

struct s19 {
    signed char[120] pad120;
    uint64_t f78;
};

struct s20 {
    signed char[120] pad120;
    uint64_t f78;
};

int64_t fun_4eb3(struct s19** rdi, struct s20** rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>((*rdi)->f78 > (*rsi)->f78);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>((*rdi)->f78 < (*rsi)->f78)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s21 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_2670(void** rdi, int64_t rsi);

void i_ring_init(void*** rdi, int64_t rsi);

void** fun_6493(struct s21* rdi, void** esi, void** rdx, void** rcx) {
    void** ebp5;
    void** rax6;
    void** r12_7;
    struct s21* rbx8;
    void** r14_9;
    void** rax10;
    void** eax11;
    void** rdi12;
    void** eax13;
    void** rsi14;
    unsigned char al15;
    unsigned char v16;
    void** rdi17;
    void** r15_18;
    void** v19;
    void** rax20;
    void** rdi21;
    uint32_t eax22;
    void** rax23;
    unsigned char al24;
    void** eax25;
    int64_t rdi26;
    int64_t rsi27;
    void** eax28;
    void** v29;
    void** r13_30;
    void** rbp31;
    uint32_t eax32;
    signed char v33;
    void** rax34;
    void** rdx35;
    void** rsi36;
    void** rax37;
    void** rax38;
    void** rax39;
    void** r13_40;
    void** rdi41;
    void** rax42;
    void** rax43;
    unsigned char al44;
    struct s21* r15_45;
    void** r13_46;
    void** rax47;

    __asm__("cli ");
    if (reinterpret_cast<unsigned char>(esi) & 0xfffff000 || ((ebp5 = esi, (reinterpret_cast<unsigned char>(esi) & 0x204) == 0x204) || !(*reinterpret_cast<unsigned char*>(&esi) & 18))) {
        rax6 = fun_2480();
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(22);
        goto addr_66ae_3;
    }
    rbx8 = rdi;
    r14_9 = rdx;
    rax10 = fun_2670(1, 0x80);
    r12_7 = rax10;
    if (!rax10) {
        addr_66ae_3:
        return r12_7;
    } else {
        *reinterpret_cast<void***>(rax10 + 64) = r14_9;
        eax11 = ebp5;
        rdi12 = rbx8->f0;
        *reinterpret_cast<void***>(r12_7 + 44) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<unsigned char*>(&eax11 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax11 + 1) & 0xfd);
        eax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax11) | 4);
        if (!(*reinterpret_cast<unsigned char*>(&ebp5) & 2)) {
            eax13 = ebp5;
        }
        *reinterpret_cast<void***>(r12_7 + 72) = eax13;
        if (rdi12) 
            goto addr_651a_8;
    }
    *reinterpret_cast<int32_t*>(&rsi14) = 0x1000;
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    addr_6549_10:
    al15 = fts_palloc(r12_7, rsi14, rdx);
    v16 = al15;
    if (!al15) {
        addr_66f6_11:
        rdi17 = r12_7;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        fun_2450(rdi17, rdi17);
        goto addr_66ae_3;
    } else {
        r15_18 = rbx8->f0;
        if (!r15_18) {
            v19 = reinterpret_cast<void**>(0);
        } else {
            rax20 = fts_alloc(r12_7, 0xe601, 0);
            v19 = rax20;
            if (!rax20) {
                addr_66ec_15:
                rdi21 = *reinterpret_cast<void***>(r12_7 + 32);
                fun_2450(rdi21, rdi21);
                goto addr_66f6_11;
            } else {
                *reinterpret_cast<void***>(rax20 + 88) = reinterpret_cast<void**>(0xffffffffffffffff);
                r15_18 = rbx8->f0;
            }
        }
    }
    if (r14_9) {
        eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) >> 10 & 1;
        v16 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    if (!r15_18) {
        rax23 = fts_alloc(r12_7, 0xe601, 0);
        *reinterpret_cast<void***>(r12_7) = rax23;
        if (!rax23) {
            addr_66e2_21:
            fun_2450(v19, v19);
            goto addr_66ec_15;
        } else {
            *reinterpret_cast<void***>(rax23 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint16_t*>(rax23 + 0x68) = 9;
            *reinterpret_cast<void***>(rax23 + 88) = reinterpret_cast<void**>(1);
            al24 = setup_dir(r12_7, 0xe601, 9);
            if (al24) {
                addr_67af_23:
                eax25 = *reinterpret_cast<void***>(r12_7 + 72);
                if (!(reinterpret_cast<unsigned char>(eax25) & 0x204)) {
                    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r12_7 + 44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax25 + 1) & 2)) {
                        *reinterpret_cast<uint32_t*>(&rsi27) = reinterpret_cast<unsigned char>(eax25) << 13 & 0x20000 | 0x90900;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi27) + 4) = 0;
                        eax28 = open_safer(".", rsi27);
                    } else {
                        eax28 = openat_safer(rdi26, ".");
                    }
                    *reinterpret_cast<void***>(r12_7 + 40) = eax28;
                    if (reinterpret_cast<signed char>(eax28) < reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(r12_7 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) | 4);
                    }
                }
            } else {
                goto addr_66e2_21;
            }
        }
    } else {
        v29 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_30) = 0;
        *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
        eax32 = (reinterpret_cast<unsigned char>(ebp5) >> 11 ^ 1) & 1;
        v33 = *reinterpret_cast<signed char*>(&eax32);
        do {
            addr_664d_31:
            rax34 = fun_2570(r15_18, r15_18);
            rdx35 = rax34;
            if (reinterpret_cast<unsigned char>(rax34) <= reinterpret_cast<unsigned char>(2) || (!v33 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax34) + 0xffffffffffffffff) != 47)) {
                addr_65d0_32:
                rsi36 = r15_18;
                rax37 = fts_alloc(r12_7, rsi36, rdx35);
                if (!rax37) 
                    goto addr_66dd_33;
            } else {
                do {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rdx35) + 0xfffffffffffffffe) != 47) 
                        goto addr_65d0_32;
                    --rdx35;
                } while (!reinterpret_cast<int1_t>(rdx35 == 1));
                goto addr_6696_37;
            }
            *reinterpret_cast<void***>(rax37 + 88) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax37 + 8) = v19;
            *reinterpret_cast<void***>(rax37 + 48) = rax37 + 0x100;
            if (!rbp31 || !v16) {
                rax38 = fts_stat(r12_7, rax37, 0);
                *reinterpret_cast<uint16_t*>(rax37 + 0x68) = *reinterpret_cast<uint16_t*>(&rax38);
                if (r14_9) {
                    addr_6635_40:
                    *reinterpret_cast<void***>(rax37 + 16) = rbp31;
                    rbp31 = rax37;
                    continue;
                } else {
                    *reinterpret_cast<void***>(rax37 + 16) = reinterpret_cast<void**>(0);
                    if (!rbp31) {
                        ++r13_30;
                        v29 = rax37;
                        rbp31 = rax37;
                        r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
                        if (r15_18) 
                            goto addr_664d_31; else 
                            goto addr_674d_43;
                    }
                }
            } else {
                *reinterpret_cast<int64_t*>(rax37 + 0xa0) = 2;
                *reinterpret_cast<uint16_t*>(rax37 + 0x68) = 11;
                if (r14_9) 
                    goto addr_6635_40;
                *reinterpret_cast<void***>(rax37 + 16) = reinterpret_cast<void**>(0);
            }
            rax39 = v29;
            v29 = rax37;
            *reinterpret_cast<void***>(rax39 + 16) = rax37;
            continue;
            addr_6696_37:
            goto addr_65d0_32;
            ++r13_30;
            r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
        } while (r15_18);
        goto addr_6750_48;
    }
    i_ring_init(r12_7 + 96, 0xffffffff);
    goto addr_66ae_3;
    addr_66dd_33:
    while (rbp31) {
        r13_40 = rbp31;
        rbp31 = *reinterpret_cast<void***>(rbp31 + 16);
        rdi41 = *reinterpret_cast<void***>(r13_40 + 24);
        if (rdi41) {
            fun_2630(rdi41, rsi36);
        }
        fun_2450(r13_40, r13_40);
    }
    goto addr_66e2_21;
    addr_6750_48:
    if (r14_9 && reinterpret_cast<unsigned char>(r13_30) > reinterpret_cast<unsigned char>(1)) {
        rax42 = fts_sort(r12_7, rbp31, r13_30, rcx);
        rbp31 = rax42;
    }
    rsi36 = reinterpret_cast<void**>(0xe601);
    rax43 = fts_alloc(r12_7, 0xe601, 0);
    *reinterpret_cast<void***>(r12_7) = rax43;
    if (!rax43) 
        goto addr_66dd_33;
    *reinterpret_cast<void***>(rax43 + 16) = rbp31;
    *reinterpret_cast<uint16_t*>(rax43 + 0x68) = 9;
    *reinterpret_cast<void***>(rax43 + 88) = reinterpret_cast<void**>(1);
    al44 = setup_dir(r12_7, 0xe601, 0);
    if (!al44) 
        goto addr_66dd_33; else 
        goto addr_67af_23;
    addr_674d_43:
    goto addr_6750_48;
    addr_651a_8:
    r15_45 = rbx8;
    *reinterpret_cast<int32_t*>(&r13_46) = 0;
    *reinterpret_cast<int32_t*>(&r13_46 + 4) = 0;
    do {
        rax47 = fun_2570(rdi12, rdi12);
        if (reinterpret_cast<unsigned char>(r13_46) < reinterpret_cast<unsigned char>(rax47)) {
            r13_46 = rax47;
        }
        rdi12 = r15_45->f8;
        r15_45 = reinterpret_cast<struct s21*>(&r15_45->f8);
    } while (rdi12);
    rsi14 = r13_46 + 1;
    if (reinterpret_cast<unsigned char>(rsi14) >= reinterpret_cast<unsigned char>(0x1000)) 
        goto addr_6549_10;
    rsi14 = reinterpret_cast<void**>(0x1000);
    goto addr_6549_10;
}

void hash_free();

int64_t fun_68b3(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void** rbp6;
    void** rbx7;
    void** rbp8;
    void** rdi9;
    void** rdi10;
    void** rdi11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    void** r13d15;
    void** rbx16;
    int32_t eax17;
    void*** rbx18;
    unsigned char al19;
    void** eax20;
    void** rdi21;
    void** rax22;
    int64_t rax23;
    int32_t eax24;
    void** rax25;
    int32_t eax26;
    void** rax27;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *reinterpret_cast<void***>(rdi);
    if (rdi5) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi5 + 88)) >= reinterpret_cast<signed char>(0)) {
            while (1) {
                rbp6 = *reinterpret_cast<void***>(rdi5 + 16);
                if (rbp6) {
                    fun_2450(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                } else {
                    rbp6 = *reinterpret_cast<void***>(rdi5 + 8);
                    fun_2450(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                }
                rdi5 = rbp6;
            }
        } else {
            rbp6 = rdi5;
        }
        fun_2450(rbp6);
    }
    rbx7 = *reinterpret_cast<void***>(r12_4 + 8);
    if (rbx7) {
        do {
            rbp8 = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdi9 = *reinterpret_cast<void***>(rbp8 + 24);
            if (rdi9) {
                fun_2630(rdi9, rsi);
            }
            fun_2450(rbp8);
        } while (rbx7);
    }
    rdi10 = *reinterpret_cast<void***>(r12_4 + 16);
    fun_2450(rdi10);
    rdi11 = *reinterpret_cast<void***>(r12_4 + 32);
    fun_2450(rdi11);
    eax12 = *reinterpret_cast<void***>(r12_4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 4)) {
            eax13 = fun_2740();
            if (eax13) {
                rax14 = fun_2480();
                r13d15 = *reinterpret_cast<void***>(rax14);
                rbx16 = rax14;
                eax17 = fun_2620();
                if (r13d15 || !eax17) {
                    addr_696c_19:
                    rbx18 = reinterpret_cast<void***>(r12_4 + 96);
                } else {
                    addr_6a78_20:
                    r13d15 = *reinterpret_cast<void***>(rbx16);
                    goto addr_696c_19;
                }
                while (al19 = i_ring_empty(rbx18, rsi, rdx), al19 == 0) {
                    eax20 = i_ring_pop(rbx18, rsi, rdx);
                    if (reinterpret_cast<signed char>(eax20) < reinterpret_cast<signed char>(0)) 
                        continue;
                    fun_2620();
                }
                if (*reinterpret_cast<void***>(r12_4 + 80)) {
                    hash_free();
                }
                rdi21 = *reinterpret_cast<void***>(r12_4 + 88);
                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_4 + 72)) & 0x102)) {
                    fun_2450(rdi21);
                } else {
                    if (rdi21) {
                        hash_free();
                    }
                }
                fun_2450(r12_4);
                if (r13d15) {
                    rax22 = fun_2480();
                    *reinterpret_cast<void***>(rax22) = r13d15;
                    r13d15 = reinterpret_cast<void**>(0xffffffff);
                }
                *reinterpret_cast<void***>(&rax23) = r13d15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                return rax23;
            } else {
                eax24 = fun_2620();
                if (eax24) {
                    rax25 = fun_2480();
                    rbx16 = rax25;
                    goto addr_6a78_20;
                }
            }
        }
    } else {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_4 + 44)) >= reinterpret_cast<signed char>(0) && (eax26 = fun_2620(), !!eax26)) {
            rax27 = fun_2480();
            r13d15 = *reinterpret_cast<void***>(rax27);
            goto addr_696c_19;
        }
    }
    r13d15 = reinterpret_cast<void**>(0);
    goto addr_696c_19;
}

struct s22 {
    signed char f0;
    void** f1;
};

void** fun_25d0(void** rdi, void** rsi, void** rdx);

void** fun_6aa3(void** rdi, void** rsi) {
    void** r12_3;
    void** edx4;
    void** rbp5;
    uint32_t eax6;
    void** rax7;
    void** rcx8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** eax12;
    int64_t rdi13;
    int64_t rsi14;
    void** eax15;
    void** r13_16;
    uint32_t eax17;
    void** r13_18;
    void** rax19;
    void** r14_20;
    void** rdi21;
    int32_t eax22;
    void** rax23;
    void** eax24;
    void** rax25;
    void** rax26;
    void** eax27;
    void** rax28;
    int64_t r8_29;
    void** rax30;
    void** rax31;
    void** r14_32;
    void** rdx33;
    void** rax34;
    void** rax35;
    void** rax36;
    void** rsi37;
    void** rdi38;
    struct s22* rdi39;
    void** rdi40;
    uint32_t eax41;
    void** rax42;
    uint32_t eax43;
    void** eax44;
    int32_t eax45;
    void** rax46;
    void** esi47;
    void** rsi48;
    int32_t eax49;
    uint32_t eax50;
    void** rdi51;
    void** rax52;
    void** rdi53;
    void** r14_54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13_58;
    void** rax59;
    void** rax60;
    void** eax61;
    int64_t rdi62;
    int64_t rsi63;
    void** eax64;
    void** rax65;
    void** eax66;
    void** r13_67;
    void** r14_68;
    void** rdi69;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi);
    if (!r12_3) 
        goto addr_6bc8_2;
    edx4 = *reinterpret_cast<void***>(rdi + 72);
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 32) 
        goto addr_6bc8_2;
    eax6 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
    if (*reinterpret_cast<int16_t*>(&eax6) == 1) {
        rax7 = fts_stat(rdi, r12_3, 0);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax7);
        goto addr_6bcb_6;
    }
    *reinterpret_cast<uint32_t*>(&rcx8) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&eax6) != 2) 
        goto addr_6af2_8;
    eax9 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx8 + 0xfffffffffffffff4));
    if (*reinterpret_cast<uint16_t*>(&eax9) <= 1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fts_stat(rdi, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax11);
        if (*reinterpret_cast<uint16_t*>(&rax11) != 1) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            if (*reinterpret_cast<uint16_t*>(&rax11) != 11) 
                goto addr_6bcb_6;
            goto addr_6e78_13;
        }
        eax12 = *reinterpret_cast<void***>(rbp5 + 72);
        if (*reinterpret_cast<unsigned char*>(&eax12) & 4) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            goto addr_6e0f_16;
        }
        *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(rbp5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(eax12) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
            *reinterpret_cast<uint32_t*>(&rsi14) = *reinterpret_cast<uint32_t*>(&rdx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi14) + 4) = 0;
            eax15 = open_safer(".", rsi14);
        } else {
            eax15 = openat_safer(rdi13, ".");
        }
        *reinterpret_cast<void***>(r12_3 + 68) = eax15;
        if (reinterpret_cast<signed char>(eax15) >= reinterpret_cast<signed char>(0)) 
            goto addr_7176_21;
    } else {
        if (*reinterpret_cast<int16_t*>(&rcx8) != 1) {
            do {
                addr_6b28_23:
                r13_16 = r12_3;
                r12_3 = *reinterpret_cast<void***>(r12_3 + 16);
                if (!r12_3) 
                    goto addr_6b35_24;
                *reinterpret_cast<void***>(rbp5) = r12_3;
                fun_2450(r13_16);
                if (!*reinterpret_cast<void***>(r12_3 + 88)) 
                    goto addr_6d00_26;
                eax17 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
            } while (*reinterpret_cast<int16_t*>(&eax17) == 4);
            goto addr_6db0_28;
        } else {
            addr_6bf7_29:
            if (!(*reinterpret_cast<unsigned char*>(&edx4) & 64) || *reinterpret_cast<void***>(r12_3 + 0x70) == *reinterpret_cast<void***>(rbp5 + 24)) {
                r13_18 = *reinterpret_cast<void***>(rbp5 + 8);
                if (!r13_18) {
                    addr_6eea_31:
                    rax19 = fts_build(rbp5, 3);
                    *reinterpret_cast<void***>(rbp5 + 8) = rax19;
                    if (!rax19) {
                        if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) {
                            if (*reinterpret_cast<void***>(r12_3 + 64) && *reinterpret_cast<uint16_t*>(r12_3 + 0x68) != 4) {
                                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                            }
                            leave_dir(rbp5, r12_3);
                            goto addr_6bcb_6;
                        }
                    } else {
                        r12_3 = rax19;
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 16) {
                        *reinterpret_cast<unsigned char*>(&edx4 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx4 + 1) & 0xef);
                        *reinterpret_cast<void***>(rbp5 + 72) = edx4;
                        do {
                            r14_20 = r13_18;
                            r13_18 = *reinterpret_cast<void***>(r13_18 + 16);
                            rdi21 = *reinterpret_cast<void***>(r14_20 + 24);
                            if (rdi21) {
                                fun_2630(rdi21, rsi);
                            }
                            fun_2450(r14_20);
                        } while (r13_18);
                        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                        goto addr_6eea_31;
                    } else {
                        rcx8 = *reinterpret_cast<void***>(r12_3 + 48);
                        eax22 = fts_safe_changedir(rbp5, r12_3, 0xffffffff, rcx8);
                        if (!eax22) {
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                        } else {
                            rax23 = fun_2480();
                            eax24 = *reinterpret_cast<void***>(rax23);
                            *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 1);
                            *reinterpret_cast<void***>(r12_3 + 64) = eax24;
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                            if (r12_3) {
                                rax25 = r12_3;
                                do {
                                    *reinterpret_cast<void***>(rax25 + 48) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax25 + 8) + 48);
                                    rax25 = *reinterpret_cast<void***>(rax25 + 16);
                                } while (rax25);
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                goto addr_6dba_50;
            } else {
                addr_6c92_51:
                if (*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) & 2) {
                    fun_2620();
                    goto addr_6c9e_53;
                }
            }
        }
    }
    rax26 = fun_2480();
    eax27 = *reinterpret_cast<void***>(rax26);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
    *reinterpret_cast<void***>(r12_3 + 64) = eax27;
    *reinterpret_cast<void***>(rbp5) = r12_3;
    goto addr_6bcb_6;
    addr_7176_21:
    *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    addr_6dfb_55:
    *reinterpret_cast<void***>(rbp5) = r12_3;
    if (*reinterpret_cast<uint16_t*>(&rax28) == 11) {
        addr_6e78_13:
        if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) == 2) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax28 = fts_stat(rbp5, r12_3, 0, rbp5, r12_3, 0);
            *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax28);
            goto addr_6e05_57;
        } else {
            if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) != 1) {
                goto 0x289f;
            }
        }
    } else {
        addr_6e05_57:
        if (*reinterpret_cast<uint16_t*>(&rax28) != 1) {
            addr_6bcb_6:
            return r12_3;
        } else {
            addr_6e0f_16:
            if (!*reinterpret_cast<void***>(r12_3 + 88)) {
                *reinterpret_cast<void***>(rbp5 + 24) = *reinterpret_cast<void***>(r12_3 + 0x70);
            }
        }
    }
    rax30 = enter_dir(rbp5, r12_3, rdx10, rcx8, r8_29, rbp5, r12_3);
    if (!*reinterpret_cast<signed char*>(&rax30)) {
        rax31 = fun_2480();
        *reinterpret_cast<int32_t*>(&r12_3) = 0;
        *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(12);
        goto addr_6bcb_6;
    }
    addr_6b35_24:
    r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
    if (*reinterpret_cast<void***>(r14_32 + 24)) {
        rdx33 = *reinterpret_cast<void***>(rbp5 + 32);
        rax34 = *reinterpret_cast<void***>(r14_32 + 72);
        *reinterpret_cast<void***>(rbp5) = r14_32;
        *reinterpret_cast<int32_t*>(&rsi) = 3;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx33) + reinterpret_cast<unsigned char>(rax34)) = 0;
        rax35 = fts_build(rbp5, 3);
        if (rax35) {
            r12_3 = rax35;
            fun_2450(r13_16, r13_16);
        } else {
            if (*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32) {
                addr_6bc8_2:
                *reinterpret_cast<int32_t*>(&r12_3) = 0;
                *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
                goto addr_6bcb_6;
            } else {
                r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
                goto addr_6b44_67;
            }
        }
    } else {
        addr_6b44_67:
        *reinterpret_cast<void***>(rbp5) = r14_32;
        fun_2450(r13_16, r13_16);
        if (*reinterpret_cast<void***>(r14_32 + 88) == 0xffffffffffffffff) {
            fun_2450(r14_32, r14_32);
            rax36 = fun_2480();
            *reinterpret_cast<void***>(rax36) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
            goto addr_6bcb_6;
        }
    }
    addr_6dba_50:
    rsi37 = r12_3 + 0x100;
    rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72) + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 56)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72)) + 0xffffffffffffffff) != 47) {
        rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72);
    }
    rdi39 = reinterpret_cast<struct s22*>(reinterpret_cast<unsigned char>(rdi38) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)));
    rdi39->f0 = 47;
    rdi40 = reinterpret_cast<void**>(&rdi39->f1);
    rdx10 = *reinterpret_cast<void***>(r12_3 + 96) + 1;
    fun_2790(rdi40, rsi37, rdx10, rdi40, rsi37, rdx10);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    goto addr_6dfb_55;
    if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) == 11) 
        goto 0x289f;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_32 + 72))) = 0;
    if (*reinterpret_cast<void***>(r14_32 + 88)) 
        goto addr_6b7e_73;
    eax41 = restore_initial_cwd(rbp5, rsi);
    if (!eax41) {
        addr_6b93_75:
        if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) != 2) {
            if (*reinterpret_cast<void***>(r14_32 + 64)) {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 7;
            } else {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 6;
                leave_dir(rbp5, r14_32);
            }
        }
    } else {
        addr_6fd8_79:
        rax42 = fun_2480();
        *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax42);
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_6b93_75;
    }
    r12_3 = r14_32;
    if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) 
        goto addr_6bcb_6;
    goto addr_6bc8_2;
    addr_6b7e_73:
    eax43 = *reinterpret_cast<unsigned char*>(r14_32 + 0x6a);
    if (*reinterpret_cast<unsigned char*>(&eax43) & 2) {
        eax44 = *reinterpret_cast<void***>(rbp5 + 72);
        if (!(*reinterpret_cast<unsigned char*>(&eax44) & 4)) {
            if (!(*reinterpret_cast<unsigned char*>(&eax44 + 1) & 2)) {
                eax45 = fun_2740();
                if (eax45) {
                    rax46 = fun_2480();
                    *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax46);
                    *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
                }
            } else {
                esi47 = *reinterpret_cast<void***>(r14_32 + 68);
                cwd_advance_fd(rbp5, esi47, 1);
            }
        }
        fun_2620();
        goto addr_6b93_75;
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax43) & 1) 
            goto addr_6b93_75;
        rsi48 = *reinterpret_cast<void***>(r14_32 + 8);
        eax49 = fts_safe_changedir(rbp5, rsi48, 0xffffffff, "..");
        if (!eax49) 
            goto addr_6b93_75;
        goto addr_6fd8_79;
    }
    addr_6d00_26:
    eax50 = restore_initial_cwd(rbp5, rsi);
    if (eax50) {
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_6bc8_2;
    }
    rdi51 = *reinterpret_cast<void***>(rbp5 + 88);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) & 0x102)) {
        fun_2450(rdi51);
    } else {
        if (rdi51) {
            hash_free();
        }
    }
    rax52 = *reinterpret_cast<void***>(r12_3 + 96);
    rdi53 = *reinterpret_cast<void***>(rbp5 + 32);
    r14_54 = r12_3 + 0x100;
    *reinterpret_cast<void***>(r12_3 + 72) = rax52;
    rdx10 = rax52 + 1;
    fun_2790(rdi53, r14_54, rdx10);
    *reinterpret_cast<int32_t*>(&rsi55) = 47;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    rax56 = fun_25d0(r14_54, 47, rdx10);
    if (!rax56) 
        goto addr_6d8b_98;
    if (r14_54 == rax56) {
        if (!*reinterpret_cast<void***>(r14_54 + 1)) {
            addr_6d8b_98:
            rax57 = *reinterpret_cast<void***>(rbp5 + 32);
            *reinterpret_cast<void***>(r12_3 + 56) = rax57;
            *reinterpret_cast<void***>(r12_3 + 48) = rax57;
            setup_dir(rbp5, rsi55, rdx10, rbp5, rsi55);
            *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
            goto addr_6dfb_55;
        } else {
            goto addr_6d68_102;
        }
    } else {
        addr_6d68_102:
        r13_58 = rax56 + 1;
        rax59 = fun_2570(r13_58, r13_58);
        rsi55 = r13_58;
        rdx10 = rax59 + 1;
        fun_2790(r14_54, rsi55, rdx10);
        *reinterpret_cast<void***>(r12_3 + 96) = rax59;
        goto addr_6d8b_98;
    }
    addr_6db0_28:
    if (*reinterpret_cast<int16_t*>(&eax17) == 2) {
        rax60 = fts_stat(rbp5, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax60);
        if (*reinterpret_cast<uint16_t*>(&rax60) == 1 && (eax61 = *reinterpret_cast<void***>(rbp5 + 72), !(*reinterpret_cast<unsigned char*>(&eax61) & 4))) {
            *reinterpret_cast<void***>(&rdi62) = *reinterpret_cast<void***>(rbp5 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(&eax61 + 1) & 2)) {
                *reinterpret_cast<uint32_t*>(&rsi63) = reinterpret_cast<unsigned char>(eax61) << 13 & 0x20000 | 0x90900;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi63) + 4) = 0;
                eax64 = open_safer(".", rsi63);
            } else {
                eax64 = openat_safer(rdi62, ".");
            }
            *reinterpret_cast<void***>(r12_3 + 68) = eax64;
            if (reinterpret_cast<signed char>(eax64) < reinterpret_cast<signed char>(0)) {
                rax65 = fun_2480();
                eax66 = *reinterpret_cast<void***>(rax65);
                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                *reinterpret_cast<void***>(r12_3 + 64) = eax66;
            } else {
                *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
            }
        }
        *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
        goto addr_6dba_50;
    }
    addr_6c9e_53:
    r13_67 = *reinterpret_cast<void***>(rbp5 + 8);
    if (r13_67) {
        do {
            r14_68 = r13_67;
            r13_67 = *reinterpret_cast<void***>(r13_67 + 16);
            rdi69 = *reinterpret_cast<void***>(r14_68 + 24);
            if (rdi69) {
                fun_2630(rdi69, rsi);
            }
            fun_2450(r14_68);
        } while (r13_67);
        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 6;
    leave_dir(rbp5, r12_3);
    goto addr_6bcb_6;
    addr_6af2_8:
    if (*reinterpret_cast<int16_t*>(&rcx8) != 1) 
        goto addr_6b28_23;
    if (*reinterpret_cast<int16_t*>(&eax6) != 4) 
        goto addr_6bf7_29; else 
        goto addr_6c92_51;
}

struct s23 {
    signed char[108] pad108;
    int16_t f6c;
};

int64_t fun_71f3() {
    uint32_t edx1;
    void** rax2;
    struct s23* rsi3;
    int16_t dx4;

    __asm__("cli ");
    if (edx1 > 4) {
        rax2 = fun_2480();
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(22);
        return 1;
    } else {
        rsi3->f6c = dx4;
        return 0;
    }
}

void** fun_7223(void** rdi, void** rsi) {
    uint32_t r13d3;
    void** rbp4;
    void** rax5;
    void** r14_6;
    void** r15_7;
    uint32_t edx8;
    void** rax9;
    void** rbx10;
    void** r12_11;
    void** rdi12;
    int32_t r12d13;
    void** eax14;
    void** rsi15;
    int64_t rdi16;
    int64_t rsi17;
    void** eax18;
    void** r13d19;
    void** eax20;
    void** rsi21;
    void** rax22;
    int32_t eax23;
    void** ebx24;

    __asm__("cli ");
    r13d3 = *reinterpret_cast<uint32_t*>(&rsi);
    rbp4 = rdi;
    rax5 = fun_2480();
    r14_6 = rax5;
    if (r13d3 & 0xffffefff) {
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        return 0;
    }
    r15_7 = *reinterpret_cast<void***>(rbp4);
    *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 32) {
        return 0;
    }
    edx8 = *reinterpret_cast<uint16_t*>(r15_7 + 0x68);
    if (*reinterpret_cast<int16_t*>(&edx8) == 9) {
        return *reinterpret_cast<void***>(r15_7 + 16);
    }
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&edx8) == 1) 
        goto addr_7278_8;
    addr_72ed_9:
    return rax9;
    addr_7278_8:
    rbx10 = *reinterpret_cast<void***>(rbp4 + 8);
    if (rbx10) {
        do {
            r12_11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 16);
            rdi12 = *reinterpret_cast<void***>(r12_11 + 24);
            if (rdi12) {
                fun_2630(rdi12, rsi);
            }
            fun_2450(r12_11);
        } while (rbx10);
    }
    r12d13 = 1;
    if (r13d3 == 0x1000) {
        *reinterpret_cast<void***>(rbp4 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 72)) | 0x1000);
        r12d13 = 2;
    }
    if (*reinterpret_cast<void***>(r15_7 + 88)) 
        goto addr_72de_17;
    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_7 + 48)) == 47) 
        goto addr_72de_17;
    eax14 = *reinterpret_cast<void***>(rbp4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax14) & 4)) 
        goto addr_7300_20;
    addr_72de_17:
    *reinterpret_cast<int32_t*>(&rsi15) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    rax9 = fts_build(rbp4, rsi15);
    *reinterpret_cast<void***>(rbp4 + 8) = rax9;
    goto addr_72ed_9;
    addr_7300_20:
    *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbp4 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&eax14 + 1) & 2)) {
        *reinterpret_cast<uint32_t*>(&rsi17) = reinterpret_cast<unsigned char>(eax14) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        eax18 = open_safer(".", rsi17);
        r13d19 = eax18;
    } else {
        eax20 = openat_safer(rdi16, ".");
        r13d19 = eax20;
    }
    if (reinterpret_cast<signed char>(r13d19) >= reinterpret_cast<signed char>(0)) 
        goto addr_7337_24;
    *reinterpret_cast<void***>(rbp4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    goto addr_72ed_9;
    addr_7337_24:
    *reinterpret_cast<int32_t*>(&rsi21) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    rax22 = fts_build(rbp4, rsi21);
    *reinterpret_cast<void***>(rbp4 + 8) = rax22;
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 2) {
        cwd_advance_fd(rbp4, r13d19, 1);
    } else {
        eax23 = fun_2740();
        if (eax23) {
            ebx24 = *reinterpret_cast<void***>(r14_6);
            fun_2620();
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
            *reinterpret_cast<void***>(r14_6) = ebx24;
            goto addr_72ed_9;
        } else {
            fun_2620();
        }
    }
    rax9 = *reinterpret_cast<void***>(rbp4 + 8);
    goto addr_72ed_9;
}

uint64_t fun_7423(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_7443(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s24 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_78a3(struct s24* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s25 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_78b3(struct s25* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s26 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_78c3(struct s26* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s29 {
    signed char[8] pad8;
    struct s29* f8;
};

struct s28 {
    int64_t f0;
    struct s29* f8;
};

struct s27 {
    struct s28* f0;
    struct s28* f8;
};

uint64_t fun_78d3(struct s27* rdi) {
    struct s28* rcx2;
    struct s28* rsi3;
    uint64_t r8_4;
    struct s29* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s32 {
    signed char[8] pad8;
    struct s32* f8;
};

struct s31 {
    int64_t f0;
    struct s32* f8;
};

struct s30 {
    struct s31* f0;
    struct s31* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_7933(struct s30* rdi) {
    struct s31* rcx2;
    struct s31* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s32* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s35 {
    signed char[8] pad8;
    struct s35* f8;
};

struct s34 {
    int64_t f0;
    struct s35* f8;
};

struct s33 {
    struct s34* f0;
    struct s34* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_79a3(struct s33* rdi, void** rsi) {
    uint64_t r12_3;
    void** rbp4;
    int64_t* v5;
    int64_t* rbx6;
    struct s34* rcx7;
    struct s34* rsi8;
    void** r8_9;
    void** rbx10;
    void** r13_11;
    struct s35* rax12;
    uint64_t rdx13;
    void** r9_14;
    void** v15;
    int64_t r9_16;
    int64_t v17;
    void** r9_18;
    void** v19;
    int64_t r9_20;
    int64_t v21;
    void** r9_22;
    void** v23;
    int64_t r9_24;
    int64_t v25;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_3) + 4) = 0;
    rbp4 = rsi;
    v5 = rbx6;
    rcx7 = rdi->f0;
    rsi8 = rdi->f8;
    r8_9 = rdi->f20;
    rbx10 = rdi->f10;
    r13_11 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx7) < reinterpret_cast<uint64_t>(rsi8)) {
        while (1) {
            if (!rcx7->f0) {
                ++rcx7;
                if (reinterpret_cast<uint64_t>(rsi8) <= reinterpret_cast<uint64_t>(rcx7)) 
                    break;
            } else {
                rax12 = rcx7->f8;
                *reinterpret_cast<int32_t*>(&rdx13) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
                if (rax12) {
                    do {
                        rax12 = rax12->f8;
                        ++rdx13;
                    } while (rax12);
                }
                if (r12_3 < rdx13) {
                    r12_3 = rdx13;
                }
                ++rcx7;
                if (reinterpret_cast<uint64_t>(rsi8) <= reinterpret_cast<uint64_t>(rcx7)) 
                    break;
            }
        }
    }
    fun_2830(rbp4, 1, "# entries:         %lu\n", r8_9, r8_9, r9_14, v15, v5, rbp4, 1, "# entries:         %lu\n", r8_9, r8_9, r9_16, v17, v5);
    fun_2830(rbp4, 1, "# buckets:         %lu\n", rbx10, r8_9, r9_18, v19, v5, rbp4, 1, "# buckets:         %lu\n", rbx10, r8_9, r9_20, v21, v5);
    if (reinterpret_cast<signed char>(r13_11) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x66bc]");
        if (reinterpret_cast<signed char>(rbx10) >= reinterpret_cast<signed char>(0)) {
            addr_7a5a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_7ad9_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2830(rbp4, 1, "# buckets used:    %lu (%.2f%%)\n", r13_11, r8_9, r9_22, v23, v5, rbp4, 1, "# buckets used:    %lu (%.2f%%)\n", r13_11, r8_9, r9_24, v25, v5);
        goto fun_2830;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x673b]");
        if (reinterpret_cast<signed char>(rbx10) < reinterpret_cast<signed char>(0)) 
            goto addr_7ad9_14; else 
            goto addr_7a5a_13;
    }
}

struct s36 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s37 {
    int64_t f0;
    struct s37* f8;
};

int64_t fun_7b03(struct s36* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s36* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s37* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x28ae;
    rbx7 = reinterpret_cast<struct s37*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_7b68_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_7b5b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_7b5b_7;
    }
    addr_7b6b_10:
    return r12_3;
    addr_7b68_5:
    r12_3 = rbx7->f0;
    goto addr_7b6b_10;
    addr_7b5b_7:
    return 0;
}

struct s38 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_7b83(struct s38* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x28b3;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_7bbf_7;
    return *rax2;
    addr_7bbf_7:
    goto 0x28b3;
}

struct s40 {
    int64_t f0;
    struct s40* f8;
};

struct s39 {
    int64_t* f0;
    struct s40* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_7bd3(struct s39* rdi, int64_t rsi) {
    struct s39* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s40* rax7;
    struct s40* rdx8;
    struct s40* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x28b9;
    rax7 = reinterpret_cast<struct s40*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_7c1e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_7c1e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_7c3c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_7c3c_10:
    return r8_10;
}

struct s42 {
    int64_t f0;
    struct s42* f8;
};

struct s41 {
    struct s42* f0;
    struct s42* f8;
};

void fun_7c63(struct s41* rdi, int64_t rsi, uint64_t rdx) {
    struct s42* r9_4;
    uint64_t rax5;
    struct s42* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_7ca2_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_7ca2_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s44 {
    int64_t f0;
    struct s44* f8;
};

struct s43 {
    struct s44* f0;
    struct s44* f8;
};

int64_t fun_7cb3(struct s43* rdi, int64_t rsi, int64_t rdx) {
    struct s44* r14_4;
    int64_t r12_5;
    struct s43* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s44* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_7cdf_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_7d21_10;
            }
            addr_7cdf_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_7ce9_11:
    return r12_5;
    addr_7d21_10:
    goto addr_7ce9_11;
}

uint64_t fun_7d33(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s45 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_7d73(struct s45* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_7da3(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x7420);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x7440);
    }
    rax9 = fun_2700(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xe160);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_2670(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2450(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s48 {
    int64_t f0;
    struct s48* f8;
};

struct s47 {
    int64_t f0;
    struct s48* f8;
};

struct s46 {
    struct s47* f0;
    struct s47* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s48* f48;
};

void fun_7ea3(struct s46* rdi) {
    struct s46* rbp2;
    struct s47* r12_3;
    struct s48* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s48* rax7;
    struct s48* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s49 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_7f53(struct s49* rdi) {
    struct s49* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_7fc3_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_2450(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_7fc3_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_2450(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_2450(rdi12);
    goto fun_2450;
}

int64_t fun_8043(struct s4* rdi, uint64_t rsi) {
    struct s5* r12_3;
    int64_t rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s4* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    int64_t rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_8180_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2670(rax6, 16);
        if (!rax8) {
            addr_8180_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_2450(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x28be;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x28be;
                fun_2450(rax8, rax8);
            }
        }
    }
    rax15 = rax4 - g28;
    if (rax15) {
        fun_2590();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s50 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_81d3(void** rdi, void** rsi, void*** rdx) {
    int64_t rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s50* v17;
    int32_t r8d18;
    void** rax19;
    int64_t rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x28c3;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_82ee_5; else 
                goto addr_825f_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_825f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_82ee_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x5e41]");
            if (!cf14) 
                goto addr_8345_12;
            __asm__("comiss xmm4, [rip+0x5df1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x5db0]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_8345_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x28c3;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2700(16, rsi, rdx6);
                if (!rax19) {
                    addr_8345_12:
                    r8d18 = -1;
                } else {
                    goto addr_82a2_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_82a2_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_821e_28:
    rax20 = rax4 - g28;
    if (rax20) {
        fun_2590();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_82a2_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_821e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_83f3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    int64_t rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = rax1 - g28;
    if (rdx6) {
        fun_2590();
    } else {
        return rax3;
    }
}

void** fun_8453(void** rdi, void** rsi) {
    void** rbx3;
    int64_t rax4;
    int64_t v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    int64_t rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_84e0_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_8596_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x5c5e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x5bc8]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2450(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_8596_5; else 
                goto addr_84e0_4;
        }
    }
    rax16 = v5 - g28;
    if (rax16) {
        fun_2590();
    } else {
        return r12_7;
    }
}

void fun_85e3() {
    __asm__("cli ");
    goto hash_remove;
}

struct s51 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int64_t f14;
    signed char f1c;
};

void fun_85f3(struct s51* rdi, int32_t esi) {
    __asm__("cli ");
    rdi->f14 = 0;
    rdi->f1c = 1;
    rdi->f0 = esi;
    rdi->f4 = esi;
    rdi->f8 = esi;
    rdi->fc = esi;
    rdi->f10 = esi;
    return;
}

struct s52 {
    signed char[28] pad28;
    unsigned char f1c;
};

int64_t fun_8613(struct s52* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rax2) = rdi->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

struct s53 {
    int32_t f0;
    signed char[16] pad20;
    uint32_t f14;
    uint32_t f18;
    unsigned char f1c;
};

int64_t fun_8623(struct s53* rdi, int32_t esi) {
    uint32_t eax3;
    uint32_t eax4;
    uint32_t edx5;
    int64_t rcx6;
    int32_t r8d7;
    uint32_t ecx8;
    int64_t rax9;

    __asm__("cli ");
    eax3 = static_cast<uint32_t>(rdi->f1c) ^ 1;
    eax4 = *reinterpret_cast<unsigned char*>(&eax3);
    edx5 = rdi->f14 + eax4 & 3;
    *reinterpret_cast<uint32_t*>(&rcx6) = edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    r8d7 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4) = esi;
    ecx8 = rdi->f18;
    rdi->f14 = edx5;
    if (ecx8 == edx5) {
        rdi->f18 = eax4 + ecx8 & 3;
    }
    rdi->f1c = 0;
    *reinterpret_cast<int32_t*>(&rax9) = r8d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

struct s54 {
    int32_t f0;
    signed char[12] pad16;
    int32_t f10;
    uint32_t f14;
    uint32_t f18;
    signed char f1c;
};

int64_t fun_8663(struct s54* rdi) {
    int64_t rdx2;
    int32_t r8d3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f1c) 
        goto 0x28c8;
    *reinterpret_cast<uint32_t*>(&rdx2) = rdi->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    r8d3 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4);
    rax4 = rdx2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4) = rdi->f10;
    if (*reinterpret_cast<uint32_t*>(&rdx2) == rdi->f18) {
        rdi->f1c = 1;
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        rdi->f14 = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax4) + 3) & 3;
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

void fd_safer(int64_t rdi);

void fun_86a3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, void** r8, void** r9) {
    void* rsp7;
    int64_t v8;
    void** rcx9;
    int64_t rax10;
    int64_t* v11;
    void* v12;
    int32_t eax13;
    int64_t rdi14;
    int64_t rdx15;

    __asm__("cli ");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 88);
    v8 = rcx;
    *reinterpret_cast<int32_t*>(&rcx9) = 0;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    rax10 = g28;
    if (*reinterpret_cast<unsigned char*>(&rdx) & 64) {
        *reinterpret_cast<int32_t*>(&rcx9) = *reinterpret_cast<int32_t*>(&v8);
        *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
        v11 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp7) + 96);
        v12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 32);
    }
    eax13 = fun_2580(rdi, rsi, rdx, rcx9, r8, r9, 24, v11, v12);
    *reinterpret_cast<int32_t*>(&rdi14) = eax13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    fd_safer(rdi14);
    rdx15 = rax10 - g28;
    if (rdx15) {
        fun_2590();
    } else {
        return;
    }
}

void** fun_8723(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    void** r12_5;
    void** eax6;
    void** rdi7;
    void** rax8;
    void** rax9;
    void** r13d10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(&r12_5 + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        rdi7 = eax6;
        *reinterpret_cast<int32_t*>(&rdi7 + 4) = 0;
        rax8 = fun_27e0(rdi7, rsi);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_2480();
            r13d10 = *reinterpret_cast<void***>(rax9);
            fun_2620();
            *reinterpret_cast<void***>(rax9) = r13d10;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_2820(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_8783(void** rdi) {
    void** rcx2;
    void** rbx3;
    void** rdx4;
    void** rax5;
    void** r12_6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2820("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2470();
    } else {
        rbx3 = rdi;
        rax5 = fun_25d0(rdi, 47, rdx4);
        if (rax5 && ((r12_6 = rax5 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2490(rax5 + 0xfffffffffffffffa, "/.libs/", 7), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx3 = r12_6;
            } else {
                rbx3 = rax5 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_9f23(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2480();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x13220;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_9f63(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_9f83(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x13220);
    }
    *rdi = esi;
    return 0x13220;
}

int64_t fun_9fa3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x13220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s55 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_9fe3(struct s55* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s55*>(0x13220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s56 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s56* fun_a003(struct s56* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s56*>(0x13220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x28d8;
    if (!rdx) 
        goto 0x28d8;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x13220;
}

struct s57 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_a043(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s57* r8) {
    struct s57* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s57*>(0x13220);
    }
    rax7 = fun_2480();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xa076);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s58 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_a0c3(int64_t rdi, int64_t rsi, void*** rdx, struct s58* rcx) {
    struct s58* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s58*>(0x13220);
    }
    rax6 = fun_2480();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xa0f1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xa14c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_a1b3() {
    __asm__("cli ");
}

void** g13098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_a1c3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2450(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x13120) {
        fun_2450(rdi7);
        g13098 = reinterpret_cast<void**>(0x13120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x13090) {
        fun_2450(r12_2);
        slotvec = reinterpret_cast<void**>(0x13090);
    }
    nslots = 1;
    return;
}

void fun_a263() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a283() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a293(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a2b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_a2d3(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s8* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28de;
    rcx5 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_a363(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s8* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x28e3;
    rcx6 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_a3f3(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s8* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x28e8;
    rcx4 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_2590();
    } else {
        return rax5;
    }
}

void** fun_a483(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s8* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x28ed;
    rcx5 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_a513(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s8* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8d00]");
    __asm__("movdqa xmm1, [rip+0x8d08]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8cf1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_2590();
    } else {
        return rax10;
    }
}

void** fun_a5b3(int64_t rdi, uint32_t esi) {
    struct s8* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8c60]");
    __asm__("movdqa xmm1, [rip+0x8c68]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8c51]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_2590();
    } else {
        return rax9;
    }
}

void** fun_a653(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8bc0]");
    __asm__("movdqa xmm1, [rip+0x8bc8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8ba9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2590();
    } else {
        return rax3;
    }
}

void** fun_a6e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8b30]");
    __asm__("movdqa xmm1, [rip+0x8b38]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8b26]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2590();
    } else {
        return rax4;
    }
}

void** fun_a773(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s8* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28f2;
    rcx5 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_a813(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s8* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x89fa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x89f2]");
    __asm__("movdqa xmm2, [rip+0x89fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28f7;
    if (!rdx) 
        goto 0x28f7;
    rcx6 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_a8b3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s8* rcx7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x895a]");
    __asm__("movdqa xmm1, [rip+0x8962]");
    __asm__("movdqa xmm2, [rip+0x896a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28fc;
    if (!rdx) 
        goto 0x28fc;
    rcx7 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = rcx6 - g28;
    if (rdx9) {
        fun_2590();
    } else {
        return rax8;
    }
}

void** fun_a963(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s8* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x88aa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x88a2]");
    __asm__("movdqa xmm2, [rip+0x88aa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2901;
    if (!rsi) 
        goto 0x2901;
    rcx5 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_aa03(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s8* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x880a]");
    __asm__("movdqa xmm1, [rip+0x8812]");
    __asm__("movdqa xmm2, [rip+0x881a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2906;
    if (!rsi) 
        goto 0x2906;
    rcx6 = reinterpret_cast<struct s8*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void fun_aaa3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_aab3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_aad3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_aaf3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s59 {
    int64_t f0;
    int64_t f8;
};

struct s59* fun_ab13(struct s59* rdi) {
    int64_t rax2;
    int32_t eax3;
    struct s59* rax4;
    int64_t v5;
    int64_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_2640("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    if (eax3) {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        rdi->f0 = v5;
        rdi->f8 = v6;
        rax4 = rdi;
    }
    rdx7 = rax2 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax4;
    }
}

int32_t dup_safer();

int64_t fun_ab93(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_2480();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_2620();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s60 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    int64_t* f20;
};

void fun_26b0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_abf3(void** rdi, void** rsi, void** rdx, void** rcx, struct s60* r8, void** r9) {
    void** r12_7;
    void** v8;
    int64_t* v9;
    void** v10;
    int64_t* v11;
    void** rax12;
    void** v13;
    int64_t* v14;
    void** rax15;
    void** v16;
    int64_t* v17;
    int64_t* rcx18;
    void** r15_19;
    void** r14_20;
    void** r13_21;
    void** r12_22;
    void** rax23;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2830(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9);
    } else {
        r9 = rcx;
        fun_2830(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v10, v11);
    }
    rax12 = fun_2550();
    fun_2830(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax12, 0x7e6, r9, v13, v14);
    fun_26b0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax12, 0x7e6, r9);
    rax15 = fun_2550();
    fun_2830(rdi, 1, rax15, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v16, v17);
    fun_26b0(10, rdi, rax15, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        rcx18 = r8->f20;
        r15_19 = r8->f18;
        r14_20 = r8->f10;
        r13_21 = r8->f8;
        r12_22 = r8->f0;
        rax23 = fun_2550();
        fun_2830(rdi, 1, rax23, r12_22, r13_21, r14_20, r15_19, rcx18, rdi, 1, rax23, r12_22, r13_21, r14_20, r15_19, rcx18);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xe890 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xe890;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_b063() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s61 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_b083(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s61* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s61* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_2590();
    } else {
        return;
    }
}

void fun_b123(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_b1c6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_b1d0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_2590();
    } else {
        return;
    }
    addr_b1c6_5:
    goto addr_b1d0_7;
}

void fun_b203() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_26b0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2550();
    fun_2770(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2550();
    fun_2770(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2550();
    goto fun_2770;
}

/* initialized.1 */
signed char initialized_1 = 0;

int64_t fun_2610();

/* can_write.0 */
unsigned char can_write_0 = 0;

int64_t fun_b2a3() {
    int1_t zf1;
    int64_t rax2;
    int64_t rax3;

    __asm__("cli ");
    zf1 = initialized_1 == 0;
    if (zf1) {
        rax2 = fun_2610();
        initialized_1 = 1;
        *reinterpret_cast<unsigned char*>(&rax2) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax2) == 0);
        can_write_0 = *reinterpret_cast<unsigned char*>(&rax2);
        return rax2;
    } else {
        *reinterpret_cast<uint32_t*>(&rax3) = can_write_0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    }
}

int64_t fun_24f0();

void fun_b2e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_b323(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b343(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b363(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b383(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2750(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_b3b3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2750(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b3e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_b423() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b463(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b493(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b4e3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24f0();
            if (rax5) 
                break;
            addr_b52d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_b52d_5;
        rax8 = fun_24f0();
        if (rax8) 
            goto addr_b516_9;
        if (rbx4) 
            goto addr_b52d_5;
        addr_b516_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_b573(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24f0();
            if (rax8) 
                break;
            addr_b5ba_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_b5ba_5;
        rax11 = fun_24f0();
        if (rax11) 
            goto addr_b5a2_9;
        if (!rbx6) 
            goto addr_b5a2_9;
        if (r12_4) 
            goto addr_b5ba_5;
        addr_b5a2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_b603(void** rdi, void** rsi, void** rdx, void* rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_b6ad_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_b6c0_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_b660_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_b686_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_b6d4_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_b67d_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_b6d4_14;
            addr_b67d_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_b6d4_14;
            addr_b686_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2750(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_b6d4_14;
            if (!rbp13) 
                break;
            addr_b6d4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_b6ad_9;
        } else {
            if (!r13_6) 
                goto addr_b6c0_10;
            goto addr_b660_11;
        }
    }
}

void fun_b703(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2670(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b733(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2670(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b763(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b783(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2670(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b7a3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_26d0;
    }
}

void fun_b7e3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_26d0;
    }
}

void fun_b823(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2700(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_26d0;
    }
}

void fun_b863(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_2570(rdi);
    rax5 = fun_2700(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_26d0;
    }
}

void fun_b8a3() {
    __asm__("cli ");
    fun_2550();
    fun_27a0();
    fun_2470();
}

int64_t rpl_fts_open();

void fun_b8e3() {
    int64_t rax1;
    void** rax2;
    void** r8_3;
    void** r9_4;
    int64_t v5;

    __asm__("cli ");
    rax1 = rpl_fts_open();
    if (!rax1) {
        rax2 = fun_2480();
        if (*reinterpret_cast<void***>(rax2) != 22) {
            xalloc_die();
        }
        fun_25f0("errno != EINVAL", "lib/xfts.c", 41, "xfts_open", r8_3, r9_4, v5);
    } else {
        return;
    }
}

struct s62 {
    signed char[72] pad72;
    uint32_t f48;
};

struct s63 {
    signed char[88] pad88;
    int64_t f58;
};

int64_t fun_b933(struct s62* rdi, struct s63* rsi) {
    int32_t r8d3;
    uint32_t eax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8d3 = 1;
    eax4 = rdi->f48 & 17;
    if (eax4 == 16 || (r8d3 = 0, eax4 != 17)) {
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!rsi->f58);
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int64_t fun_2680(void* rdi, void* rsi, int64_t rdx, void** rcx);

int32_t fun_26c0(int64_t rdi, void* rsi, int64_t rdx, void** rcx);

int64_t fun_b973() {
    int32_t r12d1;
    void* rsp2;
    void** rcx3;
    int64_t rax4;
    void* rsi5;
    int64_t rax6;
    signed char* rax7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;

    __asm__("cli ");
    r12d1 = 0;
    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    rcx3 = stdin;
    rax4 = g28;
    rsi5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 16);
    rax6 = fun_2680(reinterpret_cast<int64_t>(rsp2) + 8, rsi5, 10, rcx3);
    if (!(reinterpret_cast<uint1_t>(rax6 < 0) | reinterpret_cast<uint1_t>(rax6 == 0))) {
        rax7 = reinterpret_cast<signed char*>(rax6 - 1);
        if (*rax7 == 10) {
            *rax7 = 0;
        }
        eax8 = fun_26c0(0, rsi5, 10, rcx3);
        *reinterpret_cast<unsigned char*>(&r12d1) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax8 < 0) | reinterpret_cast<uint1_t>(eax8 == 0)));
    }
    fun_2450(0, 0);
    rax9 = rax4 - g28;
    if (rax9) {
        fun_2590();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

int64_t fun_24b0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_ba13(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_24b0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_ba6e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2480();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_ba6e_3;
            rax6 = fun_2480();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s64 {
    signed char[16] pad16;
    int64_t f10;
    int32_t f18;
};

void fun_ba83(struct s64* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f18 = 0x95f616;
    return;
}

struct s65 {
    int64_t f0;
    int64_t f8;
    uint64_t f10;
    int32_t f18;
};

struct s66 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_baa3(struct s65* rdi, struct s66* rsi) {
    void** r8_3;
    void** r9_4;
    int64_t rax5;
    uint64_t rax6;
    int64_t rdx7;
    uint64_t rcx8;
    int64_t rax9;

    __asm__("cli ");
    if (rdi->f18 != 0x95f616) {
        fun_25f0("state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check", r8_3, r9_4, rax5);
    } else {
        rax6 = rdi->f10;
        rdx7 = rsi->f8;
        if (!rax6) {
            rdi->f10 = 1;
        } else {
            if (rdi->f0 != rdx7 || rsi->f0 != rdi->f8) {
                rcx8 = rax6 + 1;
                rdi->f10 = rcx8;
                if (!(rax6 & rcx8)) {
                    if (!rcx8) {
                        return 1;
                    }
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        rax9 = rsi->f0;
        rdi->f0 = rdx7;
        rdi->f8 = rax9;
        return 0;
    }
}

int64_t fun_2530(void** rdi);

int64_t fun_bb33(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_26e0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2730(rdi);
        if (!(eax3 && (eax4 = fun_26e0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25e0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2480();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2530(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2530;
}

uint32_t fun_2510(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_bbc3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    int64_t rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2510(rdi);
        r12d10 = eax9;
        goto addr_bcc4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2510(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_bcc4_3:
                rax14 = rax8 - g28;
                if (rax14) {
                    fun_2590();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_bd79_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2510(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2510(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2480();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_2620();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_bcc4_3;
                }
            }
        } else {
            eax22 = fun_2510(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2480(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_bcc4_3;
            } else {
                eax25 = fun_2510(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_bcc4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_bd79_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_bc29_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_bc2d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_bc2d_18:
            if (0) {
            }
        } else {
            addr_bc75_23:
            eax28 = fun_2510(rdi);
            r12d10 = eax28;
            goto addr_bcc4_3;
        }
        eax29 = fun_2510(rdi);
        r12d10 = eax29;
        goto addr_bcc4_3;
    }
    if (0) {
    }
    eax30 = fun_2510(rdi);
    r12d10 = eax30;
    goto addr_bcc4_3;
    addr_bc29_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_bc2d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_bc75_23;
        goto addr_bc2d_18;
    }
}

int32_t fun_27b0();

void fun_be33() {
    int64_t rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    int64_t rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_27b0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = rax1 - g28;
    if (rdx5) {
        fun_2590();
    } else {
        return;
    }
}

signed char* fun_2720(int64_t rdi);

signed char* fun_beb3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2720(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25b0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_bef3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_2590();
    } else {
        return r12_7;
    }
}

void fun_bf83() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_bfa3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_2590();
    } else {
        return rax3;
    }
}

int64_t fun_c023(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2760(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2570(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_26d0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_26d0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_c0d3() {
    __asm__("cli ");
    goto fun_2760;
}

void fun_c0e3() {
    __asm__("cli ");
}

void fun_c0f7() {
    __asm__("cli ");
    return;
}

void fun_2a50() {
    goto 0x29e0;
}

void fun_2b30() {
    int64_t rax1;
    int64_t rdi2;
    int64_t rbx3;
    int32_t eax4;

    rax1 = optind;
    rdi2 = *reinterpret_cast<int64_t*>(rbx3 + rax1 * 8 - 8);
    eax4 = fun_2690(rdi2, "--no-preserve-root");
    if (eax4) 
        goto 0x2e5b;
    goto 0x29e0;
}

void fun_2b60() {
    goto 0x29e0;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2850(int64_t rdi, void** rsi);

uint32_t fun_2840(void** rdi, void** rsi);

void** fun_2880(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_89b5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2550();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2550();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2570(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_8cb3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_8cb3_22; else 
                            goto addr_90ad_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_916d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_94c0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_8cb0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_8cb0_30; else 
                                goto addr_94d9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2570(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_94c0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_94c0_28; else 
                            goto addr_8b5c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_9620_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_94a0_40:
                        if (r11_27 == 1) {
                            addr_902d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_95e8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_8c67_44;
                            }
                        } else {
                            goto addr_94b0_46;
                        }
                    } else {
                        addr_962f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_902d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_8cb3_22:
                                if (v47 != 1) {
                                    addr_9209_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2570(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_9254_54;
                                    }
                                } else {
                                    goto addr_8cc0_56;
                                }
                            } else {
                                addr_8c65_57:
                                ebp36 = 0;
                                goto addr_8c67_44;
                            }
                        } else {
                            addr_9494_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_962f_47; else 
                                goto addr_949e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_902d_41;
                        if (v47 == 1) 
                            goto addr_8cc0_56; else 
                            goto addr_9209_52;
                    }
                }
                addr_8d21_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_8bb8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_8bdd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_8ee0_65;
                    } else {
                        addr_8d49_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_9598_67;
                    }
                } else {
                    goto addr_8d40_69;
                }
                addr_8bf1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_8c3c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_9598_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_8c3c_81;
                }
                addr_8d40_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_8bdd_64; else 
                    goto addr_8d49_66;
                addr_8c67_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_8d1f_91;
                if (v22) 
                    goto addr_8c7f_93;
                addr_8d1f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8d21_62;
                addr_9254_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_99db_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_9a4b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_984f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2850(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2840(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_934e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_8d0c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_9358_112;
                    }
                } else {
                    addr_9358_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_9429_114;
                }
                addr_8d18_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_8d1f_91;
                while (1) {
                    addr_9429_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_9937_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_9396_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_9945_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_9417_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_9417_128;
                        }
                    }
                    addr_93c5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_9417_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_9396_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_93c5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8c3c_81;
                addr_9945_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_9598_67;
                addr_99db_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_934e_109;
                addr_9a4b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_934e_109;
                addr_8cc0_56:
                rax93 = fun_2880(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_8d0c_110;
                addr_949e_59:
                goto addr_94a0_40;
                addr_916d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_8cb3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_8d18_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8c65_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_8cb3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_91b2_160;
                if (!v22) 
                    goto addr_9587_162; else 
                    goto addr_9793_163;
                addr_91b2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_9587_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_9598_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_905b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_8ec3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_8bf1_70; else 
                    goto addr_8ed7_169;
                addr_905b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_8bb8_63;
                goto addr_8d40_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_9494_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_95cf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_8cb0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_8ba8_178; else 
                        goto addr_9552_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9494_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_8cb3_22;
                }
                addr_95cf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_8cb0_30:
                    r8d42 = 0;
                    goto addr_8cb3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_8d21_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_95e8_42;
                    }
                }
                addr_8ba8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8bb8_63;
                addr_9552_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_94b0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_8d21_62;
                } else {
                    addr_9562_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_8cb3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_9d12_188;
                if (v28) 
                    goto addr_9587_162;
                addr_9d12_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_8ec3_168;
                addr_8b5c_37:
                if (v22) 
                    goto addr_9b53_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_8b73_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_9620_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_96ab_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_8cb3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_8ba8_178; else 
                        goto addr_9687_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9494_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_8cb3_22;
                }
                addr_96ab_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_8cb3_22;
                }
                addr_9687_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_94b0_46;
                goto addr_9562_186;
                addr_8b73_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_8cb3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_8cb3_22; else 
                    goto addr_8b84_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_9c5e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_9ae4_210;
            if (1) 
                goto addr_9ae2_212;
            if (!v29) 
                goto addr_971e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_9c51_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_8ee0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_8c9b_219; else 
            goto addr_8efa_220;
        addr_8c7f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_8c93_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_8efa_220; else 
            goto addr_8c9b_219;
        addr_984f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_8efa_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_986d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2560();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_9ce0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_9746_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_9937_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_8c93_221;
        addr_9793_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_8c93_221;
        addr_8ed7_169:
        goto addr_8ee0_65;
        addr_9c5e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_8efa_220;
        goto addr_986d_222;
        addr_9ae4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_9b3e_236;
        fun_2590();
        rsp25 = rsp25 - 8 + 8;
        goto addr_9ce0_225;
        addr_9ae2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_9ae4_210;
        addr_971e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_9ae4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_9746_226;
        }
        addr_9c51_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_90ad_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe36c + rax113 * 4) + 0xe36c;
    addr_94d9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe46c + rax114 * 4) + 0xe46c;
    addr_9b53_190:
    addr_8c9b_219:
    goto 0x8980;
    addr_8b84_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xe26c + rax115 * 4) + 0xe26c;
    addr_9b3e_236:
    goto v116;
}

void fun_8ba0() {
}

void fun_8d58() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x8a52;
}

void fun_8db1() {
    goto 0x8a52;
}

void fun_8e9e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x8d21;
    }
    if (v2) 
        goto 0x9793;
    if (!r10_3) 
        goto addr_98fe_5;
    if (!v4) 
        goto addr_97ce_7;
    addr_98fe_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_97ce_7:
    goto 0x8bd4;
}

void fun_8ebc() {
}

void fun_8f67() {
    signed char v1;

    if (v1) {
        goto 0x8eef;
    } else {
        goto 0x8c2a;
    }
}

void fun_8f81() {
    signed char v1;

    if (!v1) 
        goto 0x8f7a; else 
        goto "???";
}

void fun_8fa8() {
    goto 0x8ec3;
}

void fun_9028() {
}

void fun_9040() {
}

void fun_906f() {
    goto 0x8ec3;
}

void fun_90c1() {
    goto 0x9050;
}

void fun_90f0() {
    goto 0x9050;
}

void fun_9123() {
    goto 0x9050;
}

void fun_94f0() {
    goto 0x8ba8;
}

void fun_97ee() {
    signed char v1;

    if (v1) 
        goto 0x9793;
    goto 0x8bd4;
}

void fun_9895() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x8bd4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x8bb8;
        goto 0x8bd4;
    }
}

void fun_9cb2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x8f20;
    } else {
        goto 0x8a52;
    }
}

void fun_acc8() {
    fun_2550();
}

int64_t optarg = 0;

void fun_2a60() {
    int64_t rsi1;

    rsi1 = optarg;
    if (rsi1) 
        goto 0x2bdb; else 
        goto "???";
}

void fun_2aa0() {
    goto 0x29e0;
}

void fun_8dde() {
    goto 0x8a52;
}

void fun_8fb4() {
    goto 0x8f6c;
}

void fun_907b() {
    goto 0x8ba8;
}

void fun_90cd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x9050;
    goto 0x8c7f;
}

void fun_90ff() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x905b;
        goto 0x8a80;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x8efa;
        goto 0x8c9b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x9898;
    if (r10_8 > r15_9) 
        goto addr_8fe5_9;
    addr_8fea_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x98a3;
    goto 0x8bd4;
    addr_8fe5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_8fea_10;
}

void fun_9132() {
    goto 0x8c67;
}

void fun_9500() {
    goto 0x8c67;
}

void fun_9c9f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x8dbc;
    } else {
        goto 0x8f20;
    }
}

void fun_ad80() {
}

void fun_2ab0() {
    goto 0x29e0;
}

void fun_913c() {
    goto 0x90d7;
}

void fun_950a() {
    goto 0x902d;
}

void fun_ade0() {
    fun_2550();
    goto fun_2830;
}

void fun_2ac8() {
    goto 0x29e0;
}

void fun_8e0d() {
    goto 0x8a52;
}

void fun_9148() {
    goto 0x90d7;
}

void fun_9517() {
    goto 0x907e;
}

void fun_ae20() {
    fun_2550();
    goto fun_2830;
}

void fun_2ad8() {
    goto 0x29e0;
}

void fun_8e3a() {
    goto 0x8a52;
}

void fun_9154() {
    goto 0x9050;
}

void fun_ae60() {
    fun_2550();
    goto fun_2830;
}

void fun_2ae8() {
    int64_t r8_1;
    int32_t eax2;

    r8_1 = optarg;
    if (!r8_1) {
        goto 0x29e0;
    } else {
        eax2 = fun_2690(r8_1, "all");
        if (eax2) 
            goto 0x2e7f;
        goto 0x29e0;
    }
}

void fun_8e5c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x97f0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x8d21;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x8d21;
    }
    if (v11) 
        goto 0x9b53;
    if (r10_12 > r15_13) 
        goto addr_9ba3_8;
    addr_9ba8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x98e1;
    addr_9ba3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_9ba8_9;
}

struct s67 {
    signed char[24] pad24;
    void** f18;
};

struct s68 {
    signed char[16] pad16;
    void** f10;
};

struct s69 {
    signed char[8] pad8;
    void** f8;
};

void fun_aeb0() {
    void** r15_1;
    struct s67* rbx2;
    void** r14_3;
    struct s68* rbx4;
    void** r13_5;
    struct s69* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2550();
    fun_2830(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xaed2, rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xaed2);
    goto v11;
}

void fun_af08() {
    fun_2550();
    goto 0xaed9;
}

struct s70 {
    signed char[32] pad32;
    int64_t* f20;
};

struct s71 {
    signed char[24] pad24;
    void** f18;
};

struct s72 {
    signed char[16] pad16;
    void** f10;
};

struct s73 {
    signed char[8] pad8;
    void** f8;
};

void fun_af40() {
    int64_t* rcx1;
    struct s70* rbx2;
    void** r15_3;
    struct s71* rbx4;
    void** r14_5;
    struct s72* rbx6;
    void** r13_7;
    struct s73* rbx8;
    void** r12_9;
    void*** rbx10;
    void** rax11;
    void** rbp12;
    int64_t v13;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    rax11 = fun_2550();
    fun_2830(rbp12, 1, rax11, r12_9, r13_7, r14_5, r15_3, rcx1, rbp12, 1, rax11, r12_9, r13_7, r14_5, r15_3, rcx1);
    goto v13;
}

void fun_afb8() {
    fun_2550();
    goto 0xaf7b;
}
