bool main(ulong param_1,undefined8 *param_2)

{
  char *pcVar1;
  bool bVar2;
  char cVar3;
  int iVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  int *piVar7;
  int iVar8;
  ulong uVar9;
  char *pcVar10;
  long lVar11;
  long in_FS_OFFSET;
  bool bVar12;
  undefined8 uVar13;
  char local_f8 [4];
  undefined4 local_f4;
  undefined local_f0;
  char cStack239;
  undefined local_ee;
  long local_e8;
  undefined local_e0;
  undefined local_df;
  undefined local_de;
  undefined uStack221;
  stat local_d8;
  long local_40;
  
  param_1 = param_1 & 0xffffffff;
  iVar8 = (int)param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils");
  textdomain("coreutils");
  atexit(close_stdin);
  local_f8[0] = '\0';
  local_f4 = 4;
  local_f0 = 0;
  cStack239 = '\0';
  local_ee = 0;
  local_e8 = 0;
  local_e0 = 0;
  iVar4 = isatty(0);
  bVar2 = true;
  local_df = iVar4 != 0;
  bVar12 = false;
  local_de = 0;
  uStack221 = 0;
LAB_001029e0:
  while( true ) {
    pcVar10 = (char *)0x0;
    uVar13 = 0x1029f3;
    iVar4 = getopt_long(param_1,param_2,"dfirvIR",long_opts,0);
    pcVar1 = optarg;
    if (iVar4 == -1) break;
    if (0x84 < iVar4) {
switchD_00102a1b_caseD_4a:
      lVar11 = 1;
      goto LAB_00102a2b;
    }
    if (iVar4 < 0x49) {
      if (iVar4 == -0x83) {
        version_etc(stdout,&DAT_0010d004,"GNU coreutils",Version,"Paul Rubin","David MacKenzie",
                    "Richard M. Stallman","Jim Meyering",0,uVar13);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar4 != -0x82) goto switchD_00102a1b_caseD_4a;
      usage(0);
LAB_00102bdb:
      lVar11 = __xargmatch_internal();
      iVar4 = *(int *)(interactive_types + lVar11 * 4);
      if (iVar4 == 1) {
switchD_00102a1b_caseD_49:
        local_f4 = 4;
        bVar12 = true;
        local_f8[0] = '\0';
      }
      else {
        if (iVar4 == 2) goto switchD_00102a1b_caseD_69;
        if (iVar4 == 0) {
          local_f4 = 5;
          bVar12 = false;
        }
      }
    }
    else {
      switch(iVar4) {
      case 0x49:
        goto switchD_00102a1b_caseD_49;
      default:
        goto switchD_00102a1b_caseD_4a;
      case 0x52:
      case 0x72:
        cStack239 = '\x01';
        break;
      case 100:
        local_ee = 1;
        break;
      case 0x66:
        local_f4 = 5;
        bVar12 = false;
        local_f8[0] = '\x01';
        break;
      case 0x76:
        local_de = 1;
        break;
      case 0x80:
        if (optarg != (char *)0x0) goto LAB_00102bdb;
      case 0x69:
switchD_00102a1b_caseD_69:
        local_f4 = 3;
        bVar12 = false;
        local_f8[0] = '\0';
        break;
      case 0x81:
        local_f0 = 1;
        break;
      case 0x82:
        iVar4 = strcmp((char *)param_2[(long)optind + -1],"--no-preserve-root");
        if (iVar4 != 0) {
          uVar13 = dcgettext(0,"you may not abbreviate the --no-preserve-root option",5);
          error(1,0,uVar13);
          goto LAB_00102e7f;
        }
        bVar2 = false;
        break;
      case 0x83:
        if (optarg == (char *)0x0) {
          bVar2 = true;
        }
        else {
          iVar4 = strcmp(optarg,"all");
          pcVar10 = pcVar1;
          if (iVar4 != 0) {
LAB_00102e7f:
            uVar13 = quotearg_style(4,pcVar10);
            uVar5 = dcgettext(0,"unrecognized --preserve-root argument: %s",5);
            error(1,0,uVar5,uVar13);
            goto LAB_00102eb6;
          }
          local_e0 = 1;
          bVar2 = true;
        }
        break;
      case 0x84:
        goto switchD_00102a1b_caseD_84;
      }
    }
  }
  if (optind < iVar8) {
    if ((cStack239 != '\0') && (bVar2)) goto LAB_00102df9;
LAB_00102c95:
    uVar13 = program_name;
    lVar11 = (long)optind;
    if (bVar12) {
      uVar9 = (ulong)(iVar8 - optind);
      if (cStack239 == '\0') {
        if (uVar9 < 4) goto LAB_00102d09;
        uVar5 = dcngettext(0,"%s: remove %lu argument? ","%s: remove %lu arguments? ",uVar9,5);
      }
      else {
        uVar5 = dcngettext(0,"%s: remove %lu argument recursively? ",
                           "%s: remove %lu arguments recursively? ",uVar9,5);
      }
      __fprintf_chk(stderr,1,uVar5,uVar13,uVar9);
      cVar3 = yesno();
      if (cVar3 == '\0') goto LAB_00102c58;
    }
LAB_00102d09:
    iVar4 = rm(param_2 + lVar11,local_f8);
    bVar12 = iVar4 == 4;
    if (2 < iVar4 - 2U) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("VALID_STATUS (status)","src/rm.c",0x173,(char *)&__PRETTY_FUNCTION___0);
    }
  }
  else {
    if (local_f8[0] == '\0') {
LAB_00102eb6:
      uVar13 = dcgettext(0,"missing operand",5);
      error(0,0,uVar13);
LAB_00102def:
      iVar8 = (int)param_1;
      usage(1);
LAB_00102df9:
      local_e8 = get_root_dev_ino(dev_ino_buf_1);
      if (local_e8 == 0) {
        uVar13 = quotearg_style(4,"/");
        uVar5 = dcgettext(0,"failed to get attributes of %s",5);
        piVar7 = __errno_location();
        error(1,*piVar7,uVar5,uVar13);
        goto LAB_00102e56;
      }
      goto LAB_00102c95;
    }
LAB_00102c58:
    bVar12 = false;
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar12;
  }
LAB_00102e56:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
switchD_00102a1b_caseD_84:
  local_df = 1;
  goto LAB_001029e0;
LAB_00102a2b:
  if (iVar8 <= (int)lVar11) goto LAB_00102def;
  pcVar1 = (char *)param_2[lVar11];
  if (((*pcVar1 == '-') && (pcVar1[1] != '\0')) && (iVar4 = lstat(pcVar1,&local_d8), iVar4 == 0)) {
    uVar5 = quotearg_style(4,pcVar1);
    param_1 = quotearg_n_style(1,3,pcVar1);
    uVar13 = *param_2;
    uVar6 = dcgettext(0,"Try \'%s ./%s\' to remove the file %s.\n",5);
    __fprintf_chk(stderr,1,uVar6,uVar13,param_1,uVar5);
    goto LAB_00102def;
  }
  lVar11 = lVar11 + 1;
  goto LAB_00102a2b;
}