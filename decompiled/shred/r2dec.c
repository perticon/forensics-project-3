#include <stdint.h>

/* /tmp/tmpqdrhff3j @ 0x3370 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpqdrhff3j @ 0x6210 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000bebf;
        rdx = 0x0000beb0;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000beb7;
        rdx = 0x0000beb9;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000bebb;
    rdx = 0x0000beb4;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0xa770 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x27e0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpqdrhff3j @ 0x62f0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x29e0)() ();
    }
    rdx = 0x0000bf20;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xbf20 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000bec3;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000beb9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000bf4c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xbf4c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000beb7;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000beb9;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000beb7;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000beb9;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000c04c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xc04c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000c14c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xc14c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000beb9;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000beb7;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000beb7;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000beb9;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpqdrhff3j @ 0x29e0 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 32722 named .text */
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x7710 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x29e5)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x29e5 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x29ea */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2530 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpqdrhff3j @ 0x29f0 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x29f5 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x29fa */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x29ff */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2a04 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2a09 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2a0e */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2a13 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2a18 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x86f0 */
 
uint64_t randread_error (int64_t arg1) {
    rdi = arg1;
    if (rdi == 0) {
        void (*0x2a1d)() ();
    }
    rax = quote (rdi, rsi, rdx, rcx, r8);
    r12 = rax;
    rax = errno_location ();
    edx = 5;
    rbx = rax;
    eax = *(rax);
    if (eax != 0) {
        goto label_0;
    }
    rax = dcgettext (0, 0x0000c288);
    do {
        rcx = r12;
        eax = 0;
        error (*(obj.exit_failure), *(rbx), rax);
        void (*0x2a1d)() ();
label_0:
        rax = dcgettext (0, "%s: read error");
        rdx = rax;
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x2a1d */
 
void randread_error_cold (void) {
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x3460 */
 
void dbg_clear_random_data (void) {
    /* void clear_random_data(); */
    rdi = randint_source;
    return void (*0x86b0)() ();
}

/* /tmp/tmpqdrhff3j @ 0x8ac0 */
 
uint32_t dbg_randread_free (int64_t arg1) {
    rdi = arg1;
    /* int randread_free(randread_source * s); */
    rdx = 0xffffffffffffffff;
    esi = 0x1038;
    r12 = *(rdi);
    explicit_bzero_chk ();
    eax = free (rbp);
    if (r12 != 0) {
        rdi = r12;
        void (*0xa570)() ();
    }
    eax = 0;
    return eax;
}

/* /tmp/tmpqdrhff3j @ 0x2540 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpqdrhff3j @ 0x8680 */
 
void dbg_randint_free (int64_t arg1) {
    rdi = arg1;
    /* void randint_free(randint_source * s); */
    rdx = 0xffffffffffffffff;
    esi = 0x18;
    explicit_bzero_chk ();
    rdi = rbp;
    return free ();
}

/* /tmp/tmpqdrhff3j @ 0x28b0 */
 
void explicit_bzero_chk (void) {
    __asm ("bnd jmp qword [reloc.__explicit_bzero_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x3470 */
 
uint64_t dosync_part_0 (int64_t arg1, void * arg2) {
    int64_t var_15h;
    rdi = arg1;
    rsi = arg2;
    r13 = rsi;
    rax = errno_location ();
    r12d = *(rax);
    rbx = rax;
    if (r12d != 9) {
        eax = r12 - 0x15;
        if (eax > 1) {
            goto label_1;
        }
    }
    edi = ebp;
    eax = fsync ();
    if (eax == 0) {
        goto label_0;
    }
    ebp = *(rbx);
    if (ebp != 9) {
        eax = rbp - 0x15;
        if (eax > 1) {
            goto label_2;
        }
    }
    eax = sync ();
    eax = 0;
    do {
label_0:
        return rax;
label_2:
        edx = 5;
        rax = dcgettext (0, "%s: fsync failed");
        rcx = r13;
        eax = 0;
        error (0, ebp, rax);
        *(rbx) = ebp;
        eax = 0xffffffff;
    } while (1);
label_1:
    edx = 5;
    rax = dcgettext (0, "%s: fdatasync failed");
    rcx = r13;
    eax = 0;
    error (0, r12d, rax);
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpqdrhff3j @ 0x3530 */
 
uint16_t rotate_left16 (uint16_t value, uint32_t count) {
    const uint16_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
void dbg_dopass (int64_t arg_1h, int64_t arg_2h, int64_t arg_80h, int64_t arg_880h, uint32_t arg_878h, int64_t arg_828h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    long unsigned int k;
    long unsigned int n;
    mtop op;
    char[7] pass_string;
    char[652] previous_offset_buf;
    char[652] offset_buf;
    char[652] size_buf;
    int64_t var_8h;
    int64_t canary;
    int64_t var_18h;
    uint32_t var_23h;
    int64_t var_24h;
    uint32_t var_28h;
    int64_t errname;
    int64_t var_38h;
    signed int64_t var_40h;
    int64_t var_48h;
    char * s1;
    time_t var_58h;
    int64_t var_60h;
    int64_t var_64h;
    int64_t var_69h;
    int64_t var_6dh;
    int64_t var_6fh;
    int64_t var_70h;
    int64_t var_300h;
    int64_t var_590h;
    int64_t var_828h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dopass(int fd,stat const * st,char const * qname,off_t * sizep,int type,randread_source * s,long unsigned int k,long unsigned int n); */
    r13 = rsi;
    r12d = edi;
    ebx = r8d;
    r14 = *(rcx);
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x24)) = r8d;
    *((rsp + 0x38)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x828)) = rax;
    eax = 0;
    eax = getpagesize ();
    if (ebx <= 0) {
        goto label_22;
    }
    ebx &= 0xfff;
    ecx = ebx;
    edx = ebx;
    ecx <<= 0xc;
    edx |= ecx;
    ecx = edx;
    ecx >>= 4;
    if (dh == cl) {
        if (cl == dl) {
            goto label_22;
        }
    }
    *((rsp + 0x28)) = 0xf000;
    esi = 0xf000;
    ebx = 0xf000;
label_4:
    rdi = (int64_t) eax;
    rax = xalignalloc ();
    dl = (r14 > 0) ? 1 : 0;
    al = (r14 < *((rsp + 0x28))) ? 1 : 0;
    dl &= al;
    *((rsp + 0x23)) = dl;
    if (dl == 0) {
        goto label_23;
    }
label_10:
    eax = *((r13 + 0x18));
    eax &= 0xf000;
    if (eax == sym._init) {
        goto label_24;
    }
label_7:
    edx = 0;
    esi = 0;
    edi = r12d;
    rax = lseek ();
    if (rax > 0) {
        errno_location ();
        *(rax) = 0x16;
        rbx = rax;
label_21:
        edx = 5;
        rax = dcgettext (0, "%s: cannot rewind");
        rcx = *((rsp + 0x18));
label_16:
        eax = 0;
        error (0, *(rbx), rax);
label_0:
label_17:
        free (rbp);
        eax = 0xffffffff;
label_18:
        rdx = *((rsp + 0x828));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_25;
        }
        return rax;
    }
    if (rdx != 0) {
        goto label_26;
    }
    eax = *((rsp + 0x24));
    if (eax < 0) {
        goto label_27;
    }
label_8:
    eax &= 0xfff;
    edx = eax;
    eax <<= 0xc;
    eax |= edx;
    edx = eax;
    edx >>= 4;
    if (r14 < 0) {
        goto label_28;
    }
    *(rbp) = dl;
    if (rbx > r14) {
        rbx = r14;
    }
    ax = rotate_left16 (ax, 8);
    *((rbp + 1)) = ax;
    r13 = rbx;
    r13 >>= 1;
    if (rbx <= 5) {
        goto label_29;
    }
label_3:
    r15d = 3;
    do {
        r15 += r15;
        eax = memcpy (rbp + r15, rbp, r15);
    } while (r15 <= r13);
label_14:
    r13d = *((rsp + 0x24));
    r13d &= 0x1000;
    if (r15 < rbx) {
        goto label_30;
    }
    if (r13d == 0) {
        goto label_12;
    }
    if (rbx == 0) {
        goto label_12;
    }
label_13:
    eax = 0;
    do {
        *((rbp + rax)) += 0x80;
        rax += 0x200;
    } while (rax < rbx);
label_12:
    eax = *((rbp + 2));
    rdi = rsp + 0x69;
    r9d = *((rbp + 1));
    r8d = *(rbp);
    edx = 7;
    esi = 1;
    rcx = "%02x%02x%02x";
    eax = 0;
    sprintf_chk ();
label_9:
    *((rsp + 0x40)) = 0;
    if (*((rsp + 0x878)) != 0) {
        goto label_31;
    }
label_11:
    rax = 0x0000c303;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x50)) = rax;
    *((rsp + 0x58)) = 0;
    *((rsp + 8)) = 0;
label_2:
    if (r14 < 0) {
        goto label_32;
    }
    rax = *((rsp + 8));
    rbx = r14;
    rbx -= rax;
    if (rbx >= *((rsp + 0x28))) {
        goto label_32;
    }
    if (rbx == 0) {
        goto label_33;
    }
    if (r14 < rax) {
        goto label_33;
    }
    edi = *((rsp + 0x24));
    if (edi < 0) {
        goto label_34;
    }
label_6:
    eax = 0;
    r15 = r14;
    r14 = rax;
    while (rax > 0) {
        r14 += rax;
label_5:
        if (rbx <= r14) {
            goto label_35;
        }
label_1:
        rdx -= r14;
        rax = write (r12d, rbp + r14, rbx);
    }
    if (r15 < 0) {
        goto label_36;
    }
    rax = errno_location ();
    r13d = *(rax);
    *((rsp + 0x30)) = rax;
    if (r13d == 0x16) {
        if (*((rsp + 0x23)) == 0) {
            goto label_37;
        }
    }
    rax = *((rsp + 8));
    rax = umaxtostr (rax + r14, rsp + 0x590, rdx);
    edx = 5;
    *((rsp + 0x10)) = rax;
    rax = dcgettext (0, "%s: error writing at offset %s");
    r8 = *((rsp + 0x10));
    rcx = *((rsp + 0x18));
    eax = 0;
    error (0, r13d, rax);
    if (r13d != 5) {
        goto label_0;
    }
    r9 = r14;
    r9 |= 0x1ff;
    if (r9 >= rbx) {
        goto label_0;
    }
    rax = *((rsp + 8));
    r14 = r9 + 1;
    edx = 0;
    edi = r12d;
    rsi = rax + r14;
    rax = lseek ();
    if (rax == -1) {
        goto label_38;
    }
    *((rsp + 0x10)) = 1;
    if (rbx > r14) {
        goto label_1;
    }
label_35:
    rax = r14;
    rcx = *((rsp + 8));
    r14 = r15;
    r15 = rax;
    rax = 0x7fffffffffffffff;
    rax -= rcx;
    if (rax < r15) {
        goto label_39;
    }
    r15 += rcx;
    *((rsp + 8)) = r15;
    if (*((rsp + 0x878)) == 0) {
        goto label_2;
    }
    if (r14 == r15) {
        goto label_40;
    }
    rax = time (0);
    *((rsp + 0x58)) = rax;
    if (*((rsp + 0x40)) > rax) {
        goto label_2;
    }
    r8d = 1;
    rax = human_readable (*((rsp + 8)), rsp + 0x300, 0x1b2, 1, r8d, r9);
    rsi = rax;
    r13 = rax;
    eax = strcmp (*((rsp + 0x50)), rsi);
    if (eax == 0) {
        goto label_2;
    }
label_15:
    if (r14 < 0) {
        goto label_41;
    }
    r9d = 0x64;
    if (r14 != 0) {
        rax = 0x28f5c28f5c28f5c;
        if (r15 > rax) {
            goto label_42;
        }
        rax = r15 * 5;
        edx = 0;
        rax *= 5;
        rax <<= 2;
        rax = rdx:rax / r14;
        rdx = rdx:rax % r14;
    }
label_20:
    r8d = 1;
    *((rsp + 0x30)) = r9d;
    rax = human_readable (r14, rsp + 0x590, 0x1b0, 1, r8d, rax);
    edx = 5;
    rsi = "%s: pass %lu/%lu (%s)...%s/%s %d%%";
    if (r14 == r15) {
        r13 = rax;
    }
    rbx = rax;
    rax = dcgettext (0, rsi);
    r9d = *((rsp + 0x30));
    rax = rsp + 0x81;
    r9 = *((rsp + 0x898));
    eax = 0;
    r8 = *((rsp + 0x890));
    rcx = *((rsp + 0x38));
    error (0, 0, rax);
label_19:
    rbx = rsp + 0x70;
    edx = 0x28c;
    rsi = r13;
    rdi = rbx;
    strcpy_chk ();
    rax = *((rsp + 0x58));
    edi = r12d;
    rax += 5;
    *((rsp + 0x40)) = rax;
    eax = fdatasync ();
    if (eax == 0) {
        goto label_43;
    }
    rsi = *((rsp + 0x18));
    edi = r12d;
    eax = dosync_part_0 ();
    if (eax == 0) {
        goto label_43;
    }
    rax = errno_location ();
    if (*(rax) != 5) {
        goto label_0;
    }
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x10)) = 1;
    goto label_2;
label_28:
    ax = rotate_left16 (ax, 8);
    r13 = rbx;
    *(rbp) = dl;
    *((rbp + 1)) = ax;
    r13 >>= 1;
    goto label_3;
label_22:
    *((rsp + 0x28)) = loc.data_start;
    esi = 0x10002;
    ebx = 0x10002;
    goto label_4;
label_36:
    if (rax == 0) {
        goto label_44;
    }
    rax = errno_location ();
    r13d = *(rax);
    if (r13d == 0x1c) {
        goto label_44;
    }
    if (r13d != 0x16) {
        goto label_45;
    }
    if (*((rsp + 0x23)) != 0) {
        goto label_45;
    }
label_37:
    eax = 0;
    eax = rpl_fcntl (r12d, 3, rdx, rcx, r8, r9);
    *((rsp + 0x23)) = 1;
    if (eax <= 0) {
        goto label_5;
    }
    edx = eax;
    dh &= 0xbf;
    if ((ah & 0x40) == 0) {
        goto label_5;
    }
    eax = 0;
    rpl_fcntl (r12d, 4, rdx, rcx, r8, r9);
    goto label_5;
label_32:
    edi = *((rsp + 0x24));
    rbx = *((rsp + 0x28));
    if (edi >= 0) {
        goto label_6;
    }
label_34:
    randread (*((rsp + 0x38)), rbp, rbx, rcx, r8, r9);
    goto label_6;
label_38:
    edx = 5;
    rax = dcgettext (0, "%s: lseek failed");
    rcx = *((rsp + 0x18));
    rdx = rax;
    rax = *((rsp + 0x30));
    eax = 0;
    eax = error (0, *(rax), rdx);
    goto label_0;
label_24:
    r11d = 6;
    eax = 0;
    rdx = rsp + 0x60;
    *((rsp + 0x60)) = r11w;
    *((rsp + 0x64)) = 1;
    eax = ioctl (r12d, 0x40086d01);
    if (eax != 0) {
        goto label_7;
    }
    eax = *((rsp + 0x24));
    if (eax >= 0) {
        goto label_8;
    }
label_27:
    r10d = 0x6d6f;
    *((rsp + 0x69)) = 0x646e6172;
    *((rsp + 0x6d)) = r10w;
    *((rsp + 0x6f)) = 0;
    goto label_9;
label_23:
    eax = 0;
    eax = rpl_fcntl (r12d, 3, rdx, rcx, r8, r9);
    if (eax <= 0) {
        goto label_10;
    }
    edx = eax;
    dh |= 0x40;
    if (eax == edx) {
        goto label_10;
    }
    eax = 0;
    rpl_fcntl (r12d, 4, rdx, rcx, r8, r9);
    goto label_10;
label_31:
    edx = 5;
    rax = dcgettext (0, "%s: pass %lu/%lu (%s)...");
    rax = rsp + 0x71;
    r9 = *((rsp + 0x888));
    eax = 0;
    r8 = *((rsp + 0x880));
    rcx = *((rsp + 0x28));
    error (0, 0, rax);
    rax = time (0);
    rax += 5;
    *((rsp + 0x50)) = rax;
    goto label_11;
label_30:
    rdx -= r15;
    memcpy (rbp + r15, rbp, rbx);
    if (r13d == 0) {
        goto label_12;
    }
    goto label_13;
label_45:
    rdi += r14;
    rax = umaxtostr (*((rsp + 8)), rsp + 0x590, rdx);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "%s: error writing at offset %s");
    rcx = *((rsp + 0x18));
    r8 = rbx;
    eax = 0;
    error (0, r13d, rax);
    goto label_0;
label_29:
    r15d = 3;
    goto label_14;
label_44:
    rax = 0x7fffffffffffffff;
    rax -= *((rsp + 8));
    r15 = r14;
    if (rax < r14) {
        goto label_39;
    }
    rcx = *((rsp + 0x48));
    r15 += *((rsp + 8));
    *((rsp + 8)) = r15;
    r14 = r15;
    *(rcx) = r15;
    if (*((rsp + 0x878)) == 0) {
        goto label_2;
    }
label_40:
    rax = *((rsp + 0x50));
    if (*(rax) != 0) {
        goto label_46;
    }
    rax = time (0);
    *((rsp + 0x58)) = rax;
    if (*((rsp + 0x40)) > rax) {
        goto label_2;
    }
label_46:
    r8d = 1;
    rax = human_readable (r15, rsp + 0x300, 0x1b2, 1, r8d, r9);
    r13 = rax;
    goto label_15;
label_39:
    edx = 5;
    rax = dcgettext (0, "%s: file too large");
    rcx = *((rsp + 0x18));
    esi = 0;
    rdx = rax;
    goto label_16;
label_43:
    *((rsp + 0x50)) = rbx;
    goto label_2;
label_33:
    edi = r12d;
    eax = fdatasync ();
    if (eax == 0) {
        goto label_47;
    }
    rsi = *((rsp + 0x18));
    edi = r12d;
    eax = dosync_part_0 ();
    if (eax == 0) {
        goto label_47;
    }
    rax = errno_location ();
    rdi = rbp;
    if (*(rax) != 5) {
        goto label_17;
    }
    free (rdi);
    eax = 1;
    goto label_18;
label_41:
    edx = 5;
    rax = dcgettext (0, "%s: pass %lu/%lu (%s)...%s");
    rax = rsp + 0x71;
    r9 = *((rsp + 0x888));
    eax = 0;
    r8 = *((rsp + 0x880));
    rcx = *((rsp + 0x28));
    error (0, 0, rax);
    goto label_19;
label_42:
    rax = 0xa3d70a3d70a3d70b;
    rdx:rax = rax * r14;
    rax = r14;
    rax >>= 0x3f;
    rcx = rdx + r14;
    edx = 0;
    rcx >>= 6;
    rcx -= rax;
    rax = r15;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    r9 = rax;
    goto label_20;
label_47:
    free (rbp);
    eax = *((rsp + 0x10));
    goto label_18;
label_26:
    rax = errno_location ();
    rbx = rax;
    goto label_21;
label_25:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x3e80 */
 
int64_t dbg_do_wipefd (int64_t arg_4h, int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4) {
    off_t pass_size;
    stat st;
    int64_t var_8h_2;
    int64_t var_8h;
    void * ptr;
    int64_t canary;
    int64_t var_27h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_68h;
    void * buf;
    int64_t var_88h;
    signed int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_108h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool do_wipefd(int fd,char const * qname,randint_source * s,Options const * flags); */
    r12 = rsi;
    rbx = rcx;
    *((rsp + 8)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    *((rsp + 0x30)) = 0;
    if (*((rcx + 0x1c)) != 0) {
        eax = *((rcx + 0x1e));
        rax += *((rcx + 8));
        *((rsp + 0x30)) = rax;
    }
    r15 = rsp + 0x70;
    eax = fstat (ebp, r15);
    if (eax != 0) {
        goto label_17;
    }
    eax = *((rsp + 0x88));
    eax &= 0xf000;
    if (eax == sym._init) {
        goto label_18;
    }
label_10:
    cl = (eax == 0x1000) ? 1 : 0;
    dl = (eax == 0xc000) ? 1 : 0;
    cl |= dl;
    *((rsp + 0x27)) = cl;
    if (cl != 0) {
        goto label_9;
    }
    if (eax == 0x8000) {
        if (*((rsp + 0xa0)) < 0) {
            goto label_19;
        }
    }
    rax = xnmalloc (*((rbx + 8)), 4);
    *((rsp + 0x10)) = rax;
    rax = *((rbx + 0x10));
    *((rsp + 0x28)) = rax;
    eax = *((rsp + 0x88));
    if (rax == -1) {
        goto label_20;
    }
    eax &= 0xf000;
    if (eax == 0x8000) {
        goto label_21;
    }
label_2:
    rax = *((rbx + 8));
    *((rsp + 0x18)) = rax;
    if (rax == 0) {
        goto label_22;
    }
    *(rsp) = 0;
label_3:
    r14 = *((rsp + 0x18));
    r8 = obj_patterns;
    *((rsp + 0x40)) = ebp;
    eax = 0xfffffffe;
    *((rsp + 0x50)) = rbx;
    r10 = *((rsp + 0x10));
    r13d = 0;
    *((rsp + 0x48)) = r12;
    rbx = r14;
    while (eax == 0) {
        eax = 0xfffffffe;
        rbp = 0x0000bc84;
label_0:
        eax = -eax;
        rax = (int64_t) eax;
        if (rax >= rbx) {
            goto label_23;
        }
        r13 += rax;
        rbx -= rax;
label_1:
        eax = *(rbp);
    }
    rbp += 4;
    if (eax < 0) {
        goto label_0;
    }
    r14 = (int64_t) eax;
    if (r14 > rbx) {
        goto label_24;
    }
    r12 = r14*4;
    rbx -= r14;
    rbp += r12;
    rax = memcpy (r10, rbp, r12);
    r10 = rax;
    r10 += r12;
    goto label_1;
label_9:
    edx = 5;
    rax = dcgettext (0, "%s: invalid file type");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    *((rsp + 0x27)) = 0;
    do {
label_5:
        rax = *((rsp + 0x108));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_25;
        }
        eax = *((rsp + 0x27));
        return rax;
label_17:
        edx = 5;
        rax = dcgettext (0, "%s: fstat failed");
        r13 = rax;
        rax = errno_location ();
        rcx = r12;
        eax = 0;
        error (0, *(rax), r13);
        *((rsp + 0x27)) = 0;
    } while (1);
label_21:
    rax = *((rsp + 0xa8));
    rdi = *((rsp + 0xa0));
    rdx = 0x1fffffffffffffff;
    rcx = rax - 1;
    *(rsp) = rdi;
    edx = 0x200;
    rcx = *((rsp + 0x28));
    if (rcx > rdx) {
        rax = rdx;
    }
    if (rax > rcx) {
        rax = rcx;
    }
    if (rdi >= rax) {
        goto label_2;
    }
label_11:
    rax = *((rbx + 8));
    *((rsp + 0x18)) = rax;
    if (rax != 0) {
        goto label_3;
    }
label_8:
    rdi = *((rsp + 8));
    rax = randint_get_source ();
    r9 = rax;
    if (*(rsp) == 0) {
        goto label_26;
    }
    *((rsp + 0x18)) = 1;
    *((rsp + 8)) = 0;
label_4:
    rax = *(rsp);
    rdx = *((rbx + 8));
    *((rsp + 0x68)) = rax;
    eax = *((rbx + 0x1e));
    rax += rdx;
    if (rax == 0) {
        goto label_27;
    }
    r14d = 0;
    r13 = rsp + 0x68;
    rax = rbx;
    *(rsp) = r13;
    rbx = r14;
    r13 = r9;
    r14 = rax;
    do {
        r8d = 0;
        if (rdx > rbx) {
            rax = *((rsp + 0x10));
        }
        rbx++;
        eax = dopass (ebp, r15, r12, *((rsp + 0x10)), *((rax + rbx*4)), r13);
        if (eax != 0) {
            if (eax < 0) {
                goto label_16;
            }
            *((rsp + 0x18)) = 0;
        }
        rdx = *((r14 + 8));
        eax = *((r14 + 0x1e));
        rax += rdx;
    } while (rbx < rax);
    r9 = r13;
    rbx = r14;
label_27:
    rax = *((rsp + 0x28));
    *(rsp) = rax;
label_13:
    *((rsp + 0x28)) = 0;
    if (*(rsp) != 0) {
        rax = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        goto label_4;
    }
    eax = *((rbx + 0x18));
    if (eax != 0) {
        goto label_28;
    }
label_15:
    eax = *((rsp + 0x18));
    *((rsp + 0x27)) = al;
label_16:
    free (*((rsp + 0x10)));
    goto label_5;
label_24:
    *((rsp + 0x38)) = r13;
    r13 = r14;
    r14 = rbx;
    r8 = rbp;
    r12 = *((rsp + 0x48));
    ebp = *((rsp + 0x40));
    rbx = *((rsp + 0x50));
    if (r14 <= 1) {
        goto label_29;
    }
    rax = r14 * 3;
    if (r13 > rax) {
        goto label_29;
    }
    *((rsp + 0x40)) = ebp;
    *((rsp + 0x48)) = r12;
    r12 = r8;
    *((rsp + 0x50)) = rbx;
    rbx = *((rsp + 8));
    *((rsp + 0x58)) = r15;
    goto label_30;
label_6:
    r15 = r13 - 1;
    do {
        r13 = r15;
label_30:
        r12 += 4;
        if (r14 == r13) {
            goto label_31;
        }
        r15 = r13 - 1;
        rax = randint_genmax (rbx, r15);
    } while (r14 <= rax);
label_31:
    eax = *((r12 - 4));
    rcx = rbp + 4;
    *(rbp) = eax;
    r14--;
    if (r14 != 0) {
        goto label_6;
    }
    ebp = *((rsp + 0x40));
    r12 = *((rsp + 0x48));
    rbx = *((rsp + 0x50));
    r15 = *((rsp + 0x58));
    goto label_12;
label_29:
label_12:
    rax = *((rsp + 0x18));
    rdi = *((rsp + 0x38));
    *((rsp + 0x40)) = r12;
    r13d = 0;
    *((rsp + 0x38)) = ebp;
    rcx = rax;
    *((rsp + 0x48)) = rbx;
    r9 = rax - 1;
    r14 = rax;
    rcx -= rdi;
    rdi--;
    *((rsp + 0x50)) = r15;
    rbx = *((rsp + 0x10));
    r12 = rdi;
    r15 = rcx;
    while (rbp >= r12) {
        eax = *((rbx + r13*4));
        r12 += r9;
        r12 -= rbp;
        *((rbx + r15*4)) = eax;
        r15++;
        *((rbx + r13*4)) = 0xffffffff;
        r13++;
        if (r13 >= r14) {
            goto label_32;
        }
label_7:
    }
    *((rsp + 0x18)) = r9;
    r12 -= rbp;
    rsi -= r13;
    rax = randint_genmax (*((rsp + 8)), r15 - 1);
    esi = *((rbx + r13*4));
    r9 = *((rsp + 0x18));
    rax += r13;
    rax = rbx + rax*4;
    edi = *(rax);
    *((rbx + r13*4)) = edi;
    r13++;
    *(rax) = esi;
    if (r13 < r14) {
        goto label_7;
    }
label_32:
    ebp = *((rsp + 0x38));
    r12 = *((rsp + 0x40));
    rbx = *((rsp + 0x48));
    r15 = *((rsp + 0x50));
    goto label_8;
label_18:
    eax = isatty (ebp);
    if (eax != 0) {
        goto label_9;
    }
    eax = *((rsp + 0x88));
    eax &= 0xf000;
    goto label_10;
label_20:
    eax &= 0xf000;
    if (eax != 0x8000) {
        goto label_33;
    }
    rsi = *((rsp + 0xa0));
    *((rsp + 0x28)) = rsi;
    if (*((rbx + 0x1d)) != 0) {
        goto label_2;
    }
    rcx = *((rsp + 0xa8));
    rax = 0x1fffffffffffffff;
    rdx = rcx - 1;
    eax = 0x200;
    if (rdx > rax) {
        rcx = rax;
    }
    rax = rsi;
    __asm ("cqo");
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    if (rsi == 0) {
        goto label_34;
    }
    *(rsp) = rsi;
    if (rcx <= rsi) {
        goto label_34;
    }
label_14:
    if (rdx == 0) {
        goto label_11;
    }
    rdi = *((rsp + 0x28));
    rcx -= rdx;
    rax = 0x7fffffffffffffff;
    rax -= rdi;
    if (rax > rcx) {
        rax = rcx;
    }
    rdi += rax;
    *((rsp + 0x28)) = rdi;
    goto label_11;
label_23:
    r14 = rbx;
    ebp = *((rsp + 0x40));
    r12 = *((rsp + 0x48));
    r13 += r14;
    rbx = *((rsp + 0x50));
    *((rsp + 0x38)) = r13;
    goto label_12;
label_22:
    rdi = *((rsp + 8));
    rax = randint_get_source ();
    *((rsp + 0x18)) = 1;
    r9 = rax;
    rax = *((rsp + 0x28));
    *(rsp) = rax;
    goto label_13;
label_33:
    esi = 0;
    edx = 2;
    edi = ebp;
    rax = lseek ();
    if (rax <= 0) {
        rax = *((rsp + 0x28));
    }
    *((rsp + 0x28)) = rax;
    goto label_2;
label_19:
    edx = 5;
    rax = dcgettext (0, "%s: file has negative size");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_5;
label_34:
    *(rsp) = 0;
    goto label_14;
label_28:
    esi = 0;
    edi = ebp;
    eax = ftruncate ();
    if (eax == 0) {
        goto label_15;
    }
    eax = *((rsp + 0x88));
    eax &= 0xf000;
    if (eax != 0x8000) {
        goto label_15;
    }
    edx = 5;
    rax = dcgettext (0, "%s: error truncating");
    r13 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (0, *(rax), r13);
    goto label_16;
label_26:
    rax = *((rsp + 0x28));
    *((rsp + 0x18)) = 1;
    *(rsp) = rax;
    goto label_13;
label_25:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x33a0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x33d0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x3410 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002500 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpqdrhff3j @ 0x2500 */
 
void fcn_00002500 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpqdrhff3j @ 0x3450 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpqdrhff3j @ 0x4930 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x4510)() ();
}

/* /tmp/tmpqdrhff3j @ 0x2630 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpqdrhff3j @ 0x2950 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x2930 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpqdrhff3j @ 0x2850 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x2730 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpqdrhff3j @ 0x2750 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpqdrhff3j @ 0x2840 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpqdrhff3j @ 0x2570 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpqdrhff3j @ 0xa9a0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpqdrhff3j @ 0x7c60 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x4e00 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x7f90 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x2660 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpqdrhff3j @ 0x97f0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7aa0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9a10 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x27c0 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpqdrhff3j @ 0x9f50 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000bdd0);
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x2890 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpqdrhff3j @ 0x2810 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpqdrhff3j @ 0x25b0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpqdrhff3j @ 0x79c0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0xa7b0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7cb0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x29f0)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7a20 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x6160 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpqdrhff3j @ 0x26b0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpqdrhff3j @ 0x2940 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpqdrhff3j @ 0x4ab0 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x2890)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x84b0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x81f0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2a09)() ();
    }
    if (rdx == 0) {
        void (*0x2a09)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x8290 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a0e)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2a0e)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x8da0 */
 
int64_t dbg_isaac_seed (int64_t arg_800h, int64_t arg_808h, int64_t arg_810h, int64_t arg1) {
    rdi = arg1;
    /* void isaac_seed(isaac_state * s); */
    rdx = rdi;
    rax = rdi;
    rcx = 0x98f5704f6c44c0ab;
    r11 = 0x48fe4a0fa5a09315;
    rsi = 0x82f053db8355e0ce;
    r10 = 0xb29b2e824a595524;
    r9 = 0x8c0ea5053d4712a0;
    r8 = 0xb9f8b322c73ac862;
    r12 = 0xae985bf2cbfc89ed;
    rbx = rdi + 0x800;
    rdi = 0x647c4677a2884b7c;
    do {
        r13 = *(rax);
        rsi += *((rax + 0x20));
        rcx += *((rax + 0x38));
        r11 += *((rax + 0x28));
        r13 -= rsi;
        r12 += *((rax + 0x30));
        r13 += rdi;
        rdi = rcx;
        rdi >>= 9;
        r14 = rcx + r13;
        rcx = r13;
        r11 ^= rdi;
        rdi = *((rax + 8));
        rcx <<= 9;
        r12 ^= rcx;
        rdi -= r11;
        rdi += r8;
        r8 = r13 + rdi;
        r13 = *((rax + 0x10));
        rcx = rdi;
        rcx >>= 0x17;
        r13 -= r12;
        rcx ^= r14;
        r14 = *((rax + 0x18));
        r13 += r9;
        r9 = rdi + r13;
        rdi = r13;
        r14 -= rcx;
        rdi <<= 0xf;
        r14 += r10;
        rdi ^= r8;
        r8 = r14;
        r10 = r13 + r14;
        rsi -= rdi;
        r8 >>= 0xe;
        *(rax) = rdi;
        r13 = rsi;
        r8 ^= r9;
        rsi = r14 + rsi;
        r9 = r13;
        r11 -= r8;
        *((rax + 8)) = r8;
        r9 <<= 0x14;
        r13 += r11;
        r9 ^= r10;
        r10 = r11;
        r12 -= r9;
        r10 >>= 0x11;
        *((rax + 0x10)) = r9;
        r10 ^= rsi;
        rsi = r12;
        r11 += r12;
        rcx -= r10;
        rsi <<= 0xe;
        *((rax + 0x18)) = r10;
        rax += 0x40;
        rsi ^= r13;
        r12 += rcx;
        *((rax - 0x18)) = r11;
        *((rax - 0x20)) = rsi;
        *((rax - 0x10)) = r12;
        *((rax - 8)) = rcx;
    } while (rbx != rax);
    do {
        rax = *(rdx);
        rsi += *((rdx + 0x20));
        rcx += *((rdx + 0x38));
        r11 += *((rdx + 0x28));
        rax -= rsi;
        r12 += *((rdx + 0x30));
        rax += rdi;
        rdi = rcx;
        rdi >>= 9;
        r14 = rcx + rax;
        rcx = rax;
        r11 ^= rdi;
        rdi = *((rdx + 8));
        rcx <<= 9;
        r12 ^= rcx;
        rdi -= r11;
        rdi += r8;
        r8 = *((rdx + 0x10));
        rcx = rdi;
        r13 = rax + rdi;
        rax = *((rdx + 0x18));
        r8 -= r12;
        rcx >>= 0x17;
        r8 += r9;
        rcx ^= r14;
        r9 = rdi + r8;
        rax -= rcx;
        rdi = r8;
        rax += r10;
        rdi <<= 0xf;
        rdi ^= r13;
        r10 = r8 + rax;
        r8 = rax;
        rsi -= rdi;
        r8 >>= 0xe;
        *(rdx) = rdi;
        r8 ^= r9;
        r9 = rsi;
        rax += rsi;
        r13 = rsi;
        r11 -= r8;
        r9 <<= 0x14;
        *((rdx + 8)) = r8;
        r9 ^= r10;
        r10 = r11;
        r13 += r11;
        r12 -= r9;
        r10 >>= 0x11;
        *((rdx + 0x10)) = r9;
        r10 ^= rax;
        rsi = r12;
        r11 += r12;
        rcx -= r10;
        rsi <<= 0xe;
        *((rdx + 0x18)) = r10;
        rdx += 0x40;
        rsi ^= r13;
        r12 += rcx;
        *((rdx - 0x18)) = r11;
        *((rdx - 0x20)) = rsi;
        *((rdx - 0x10)) = r12;
        *((rdx - 8)) = rcx;
    } while (rbx != rdx);
    *((rbp + 0x810)) = 0;
    *((rbp + 0x808)) = 0;
    *((rbp + 0x800)) = 0;
    r12 = rbx;
    r13 = rbx;
    r14 = rbx;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x7dd0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29fa)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x8b10 */
 
int64_t dbg_isaac_refill (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void isaac_refill(isaac_state * s,isaac_word * result); */
    rax = *((rdi + 0x810));
    rdx = rdi;
    r8 = rsi;
    rcx = *((rdi + 0x800));
    r9 = rdi + 0x400;
    r11 = rax + 1;
    rax = rdx;
    *((rdi + 0x810)) = r11;
    r11 += *((rdi + 0x808));
    rdi = rsi;
    do {
        r10 = *(rax);
        rsi = rcx;
        rsi <<= 0x15;
        rcx ^= rsi;
        rsi = r10;
        esi &= 0x7f8;
        rcx = ~rcx;
        rcx += *((rax + 0x400));
        rbx = *((rdx + rsi));
        rbx += rcx;
        rsi = rbx;
        rsi += r11;
        *(rax) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r10 += *((rdx + rsi));
        rsi = rcx;
        *(rdi) = r10;
        rbx = *((rax + 8));
        rsi >>= 5;
        rcx ^= rsi;
        rcx += *((rax + 0x408));
        rsi = rbx;
        esi &= 0x7f8;
        r11 = *((rdx + rsi));
        r11 += rcx;
        rsi = r11;
        rsi += r10;
        r10 = rcx;
        *((rax + 8)) = rsi;
        rsi >>= 8;
        r10 <<= 0xc;
        esi &= 0x7f8;
        r10 ^= rcx;
        rbx += *((rdx + rsi));
        *((rdi + 8)) = rbx;
        r11 = *((rax + 0x10));
        r10 += *((rax + 0x410));
        rcx = r11;
        ecx &= 0x7f8;
        rsi = *((rdx + rcx));
        rcx = r10;
        rcx >>= 0x21;
        rsi += r10;
        rcx ^= r10;
        rsi += rbx;
        *((rax + 0x10)) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r11 += *((rdx + rsi));
        *((rdi + 0x10)) = r11;
        r10 = *((rax + 0x18));
        rcx += *((rax + 0x418));
        rsi = r10;
        esi &= 0x7f8;
        rbx = *((rdx + rsi));
        rbx += rcx;
        rsi = rbx;
        rsi += r11;
        rax += 0x20;
        rdi += 0x20;
        *((rax - 8)) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r11 = *((rdx + rsi));
        r11 += r10;
        *((rdi - 8)) = r11;
    } while (rax != r9);
    r8 += 0x400;
    rdi = rdx + 0x800;
    do {
        r9 = *(rax);
        rsi = rcx;
        rsi <<= 0x15;
        rcx ^= rsi;
        rsi = r9;
        esi &= 0x7f8;
        rcx = ~rcx;
        rcx += *((rax - 0x400));
        rbx = *((rdx + rsi));
        rbx += rcx;
        rsi = rbx;
        rsi += r11;
        *(rax) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r9 += *((rdx + rsi));
        rsi = rcx;
        *(r8) = r9;
        r11 = *((rax + 8));
        rsi >>= 5;
        rcx ^= rsi;
        rcx += *((rax - section..dynsym));
        rsi = r11;
        esi &= 0x7f8;
        rbx = *((rdx + rsi));
        rbx += rcx;
        rsi = rbx;
        rsi += r9;
        r9 = rcx;
        *((rax + 8)) = rsi;
        rsi >>= 8;
        r9 <<= 0xc;
        esi &= 0x7f8;
        r9 ^= rcx;
        r11 += *((rdx + rsi));
        *((r8 + 8)) = r11;
        r10 = *((rax + 0x10));
        r9 += *((rax - 0x3f0));
        rcx = r10;
        ecx &= 0x7f8;
        rsi = *((rdx + rcx));
        rcx = r9;
        rcx >>= 0x21;
        rsi += r9;
        rcx ^= r9;
        rsi += r11;
        *((rax + 0x10)) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r10 += *((rdx + rsi));
        *((r8 + 0x10)) = r10;
        r11 = *((rax + 0x18));
        rcx += *((rax - 0x3e8));
        rsi = r11;
        esi &= 0x7f8;
        rbx = *((rdx + rsi));
        rbx += rcx;
        rsi = rbx;
        rsi += r10;
        rax += 0x20;
        r8 += 0x20;
        *((rax - 8)) = rsi;
        rsi >>= 8;
        esi &= 0x7f8;
        r11 += *((rdx + rsi));
        *((r8 - 8)) = r11;
    } while (rax != rdi);
    *((rdx + 0x800)) = rcx;
    *((rdx + 0x808)) = r11;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x84f0 */
 
int64_t dbg_randint_new (int64_t arg1) {
    rdi = arg1;
    /* randint_source * randint_new(randread_source * source); */
    rbx = rdi;
    xmalloc (0x18);
    *(rax) = rbx;
    *((rax + 0x10)) = 0;
    *((rax + 8)) = 0;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x99d0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x4f60 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmpqdrhff3j @ 0x8920 */
 
uint64_t dbg_randread (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg1, int64_t arg2, uint32_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void randread(randread_source * s,void * buf,size_t size); */
    r14 = rsi;
    rbx = rdx;
    rcx = *(rdi);
    *(rsp) = rcx;
    if (rcx == 0) {
        goto label_1;
    }
    rax = errno_location ();
    r13d = 0;
    rcx = *(rsp);
    r12 = rax;
    while (rbx != 0) {
        rax = *(rbp);
        rdi = *((rbp + 0x10));
        if ((*(rax) & 0x20) == 0) {
            edx = r13d;
        }
        *(r12) = edx;
        uint64_t (*rbp + 8)() ();
        rcx = *(rbp);
        rdx = rbx;
        rdi = r14;
        esi = 1;
        rax = fread_unlocked ();
        edx = *(r12);
        r14 += rax;
        rbx -= rax;
    }
label_0:
    return rax;
label_1:
    rcx = *((rdi + 0x18));
    r12 = rdi + 0x20;
    rax = rdi + 0x838;
    r13d = 0x800;
    *(rsp) = rax;
    if (rdx <= rcx) {
        goto label_2;
    }
    do {
        rdx = rcx;
        *((rsp + 8)) = rcx;
        rsi -= rcx;
        rsi += *(rsp);
        memcpy (r14, r13, rdx);
        rcx = *((rsp + 8));
        r14 += rcx;
        rbx -= rcx;
        if ((r14b & 7) == 0) {
            goto label_3;
        }
        isaac_refill (r12, *(rsp));
        ecx = 0x800;
    } while (rbx > 0x800);
    r13 = rbx;
    goto label_4;
label_3:
    r13 = rbx;
    r15 = rbx;
    rbx = r14 + rbx;
    r13d &= 0x7ff;
    while (r15 != r13) {
        isaac_refill (r12, r14);
        r15 -= 0x800;
        if (r15 == 0) {
            goto label_5;
        }
        r14 = rbx;
        r14 -= r15;
    }
    isaac_refill (r12, *(rsp));
    ecx = 0x800;
    do {
label_4:
        *((rsp + 8)) = rcx;
        rax = memcpy (r14, *(rsp), r13);
        rcx = *((rsp + 8));
        rcx -= r13;
        *((rbp + 0x18)) = rcx;
        goto label_0;
label_5:
        *((rbp + 0x18)) = 0;
        return rax;
label_2:
        r15 = rax;
        r13 = rdx;
        r15 -= rcx;
        rax = r15 + 0x800;
        *(rsp) = rax;
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x8150 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2a04)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0xa9b4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpqdrhff3j @ 0x9a90 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x9b90 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9a30 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x8540 */
 
int64_t dbg_randint_get_source (randint_source const * s) {
    rdi = s;
    /* randread_source * randint_get_source(randint_source const * s); */
    rax = *(rdi);
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x4f00 */
 
uint64_t dbg_dir_name (void) {
    /* char * dir_name(char const * file); */
    rax = mdir_name (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x4da0 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9970 */
 
uint64_t dbg_xalignalloc (void) {
    /* void * xalignalloc(idx_t alignment,idx_t size); */
    rax = aligned_alloc ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x2970 */
 
void aligned_alloc (void) {
    __asm ("bnd jmp qword [reloc.aligned_alloc]");
}

/* /tmp/tmpqdrhff3j @ 0xa600 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x27d0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpqdrhff3j @ 0x7c90 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x9f10 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2780)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x2650 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpqdrhff3j @ 0x99f0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x4940 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmpqdrhff3j @ 0x9730 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x92c0)() ();
}

/* /tmp/tmpqdrhff3j @ 0x4e50 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000bdd0);
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x6100 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0xa8e0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpqdrhff3j @ 0x9990 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0xa860 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x4c70 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8340 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a13)() ();
    }
    if (rax == 0) {
        void (*0x2a13)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x80c0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x9de0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x2740 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpqdrhff3j @ 0x7960 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x5310 */
 
void dbg_human_readable (int64_t arg_48h, int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_1h;
    char[41] buf;
    int64_t var_10h_2;
    void ** s1;
    uint32_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    void ** var_30h;
    int64_t var_34h;
    int64_t var_38h;
    int64_t var_40h;
    uint32_t var_48h;
    int64_t var_4ch;
    int64_t var_4eh;
    int64_t var_50h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* char * human_readable(uintmax_t n,char * buf,int opts,uintmax_t from_block_size,uintmax_t to_block_size); */
    r13 = r8;
    r12 = rcx;
    rbx = rdi;
    *(rsp) = rsi;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x48)) = edx;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = edx;
    edx &= 0x20;
    *((rsp + 0x30)) = edx;
    eax &= 3;
    *((rsp + 0x10)) = eax;
    eax -= eax;
    eax &= 0xffffffe8;
    eax += 0x400;
    *((rsp + 0x34)) = eax;
    rax = localeconv ();
    r15 = *(rax);
    r14 = rax;
    rax = strlen (r15);
    rcx = *((r14 + 0x10));
    r14 = *((r14 + 8));
    rax--;
    eax = 1;
    rdi = r14;
    *((rsp + 0x38)) = rcx;
    if (rax >= 0x10) {
    }
    rax = 0x0000b053;
    if (rax >= 0x10) {
        r15 = rax;
    }
    rax = strlen (rdi);
    rax = 0x0000c303;
    if (rax > 0x10) {
        r14 = rax;
    }
    rax = *(rsp);
    rax += 0x287;
    *((rsp + 8)) = rax;
    if (r13 > r12) {
        goto label_36;
    }
    rax = r12;
    edx = 0;
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    if (rdx == 0) {
        rdx:rax = rax * rbx;
        rcx = rax;
        if (rdx !overflow 0) {
            goto label_37;
        }
    }
label_0:
    *((rsp + 0x20)) = r12;
    *(fp_stack--) = *((rsp + 0x20));
    if (r12 < 0) {
        goto label_38;
    }
label_5:
    rax = *((rsp + 0x18));
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        goto label_39;
    }
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx < 0) {
        goto label_4;
    }
label_3:
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) == 0) {
        goto label_40;
    }
label_2:
    *(fp_stack--) = *((rsp + 0x34));
    ebx = 0;
    *(fp_stack--) = fp_stack[0];
    while (ebx != 8) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_0 = fp_stack[2];
        fp_stack[2] = fp_stack[0];
        fp_stack[0] = fp_tmp_0;
        *(fp_stack--) = fp_stack[0];
        ebx++;
        fp_stack[0] *= fp_stack[2];
        fp_tmp_1 = fp_stack[3];
        fp_stack[3] = fp_stack[0];
        fp_stack[0] = fp_tmp_1;
        if (fp_stack[0] < fp_stack[3]) {
            goto label_41;
        }
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    goto label_42;
label_41:
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_stack[2] = fp_stack[0];
    fp_stack--;
label_42:
    r15 = rbp + 1;
    fp_stack[1] /= fp_stack[0];
    fp_stack++;
    rbp += 2;
    if (*((rsp + 0x10)) == 1) {
        goto label_43;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (*((rsp + 0x10)) <= 1) {
        goto label_44;
    }
    *(fp_stack--) = *(0x0000be58);
    fp_tmp_2 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_2;
    if (fp_stack[0] >= fp_stack[1]) {
        goto label_45;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_21:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x0000be54);
    }
    ecx = *((rsp + 0x10));
    if (ecx != 0) {
        goto label_46;
    }
    fp_tmp_3 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_3;
    if (fp_stack[0] != fp_stack[1]) {
        if (fp_stack[0] == fp_stack[1]) {
            goto label_47;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
    rax++;
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax >= 0) {
        goto label_48;
    }
    fp_stack[0] += *(0x0000be54);
    fp_tmp_4 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_4;
    goto label_47;
label_36:
    if (r12 == 0) {
        goto label_0;
    }
    rax = *((rsp + 0x18));
    edx = 0;
    rax = rdx:rax / r12;
    rdx = rdx:rax % r12;
    r8 = rax;
    if (rdx != 0) {
        goto label_0;
    }
    rax = rbx;
    edx = 0;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rcx = rax;
    rax = rdx * 5;
    edx = 0;
    rax += rax;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    rdx += rdx;
    edi = eax;
    if (r8 <= rdx) {
        goto label_49;
    }
    esi = 0;
    sil = (rdx != 0) ? 1 : 0;
label_20:
    r10d = *((rsp + 0x48));
    r10d &= 0x10;
    if (r10d == 0) {
        goto label_50;
    }
label_12:
    r8d = *((rsp + 0x34));
    ebx = 0;
    r11 = r8;
    if (r8 <= rcx) {
        goto label_51;
    }
label_8:
    r8 = *((rsp + 8));
    if (*((rsp + 0x10)) == 1) {
        goto label_52;
    }
label_28:
    r11d = *((rsp + 0x10));
    if (r11d == 0) {
        esi += edi;
        if (esi <= 0) {
            goto label_19;
        }
label_18:
        rcx++;
        if (r10d == 0) {
            goto label_19;
        }
        eax = *((rsp + 0x34));
        if (rax == rcx) {
            goto label_53;
        }
    }
label_19:
    rsi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        rbp--;
        rdx:rax = rax * rsi;
        rax = rcx;
        rdx >>= 3;
        rdi = rdx * 5;
        rdi += rdi;
        rax -= rdi;
        eax += 0x30;
        *(rbp) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    if ((*((rsp + 0x48)) & 4) == 0) {
        goto label_54;
    }
label_6:
    r12 = r8;
    *((rsp + 0x40)) = r8;
    r13 = 0xffffffffffffffff;
    rax = strlen (r14);
    r12 -= rbp;
    rsi = rbp;
    ecx = 0x29;
    r15 = rax;
    rax = rsp + 0x50;
    rdx = r12;
    rdi = rax;
    *((rsp + 0x10)) = rax;
    memcpy_chk ();
    *((rsp + 0x20)) = ebx;
    rbp = *((rsp + 0x40));
    rbx = r12;
    r12 = *((rsp + 0x38));
    while (al == 0) {
        rax = *((rsp + 0x10));
        if (r13 > rbx) {
            r13 = rbx;
        }
        rbx -= r13;
label_1:
        rbp -= r13;
        memcpy (rbp, rax + rbx, r13);
        if (rbx == 0) {
            goto label_55;
        }
        rbp -= r15;
        memcpy (rbp, r14, r15);
        eax = *(r12);
    }
    if (al > 0x7e) {
        goto label_56;
    }
    if (rax > rbx) {
        rax = rbx;
    }
    rbx -= rax;
    r13 = rax;
    rax = *((rsp + 0x10));
    rsi = rax + rbx;
label_7:
    r12++;
    goto label_1;
label_4:
    fp_stack[0] += *(0x0000be54);
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    if ((*((rsp + 0x48)) & 0x10) != 0) {
        goto label_2;
    }
    goto label_40;
label_39:
    fp_stack[0] += *(0x0000be54);
    *((rsp + 0x20)) = rbx;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = *((rsp + 0x20));
    if (rbx >= 0) {
        goto label_3;
    }
    goto label_4;
label_38:
    fp_stack[0] += *(0x0000be54);
    goto label_5;
label_40:
    if (*((rsp + 0x10)) != 1) {
        *(fp_stack--) = fp_stack[?];
        __asm ("fcompi st(1)");
        if (*((rsp + 0x10)) <= 1) {
            goto label_27;
        }
        *(fp_stack--) = *(0x0000be58);
        fp_tmp_5 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_5;
        if (fp_stack[0] >= fp_stack[1]) {
            goto label_57;
        }
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        eax = *((rsp + 0x4e));
        ah |= 0xc;
        *((rsp + 0x4c)) = ax;
        *(fp_stack--) = fp_stack[0];
        *((rsp + 0x20)) = fp_stack[0];
        fp_stack--;
        rax = *((rsp + 0x20));
label_24:
        *((rsp + 0x20)) = rax;
        *(fp_stack--) = *((rsp + 0x20));
        if (rax < 0) {
            fp_stack[0] += *(0x0000be54);
        }
        r8d = *((rsp + 0x10));
        if (r8d == 0) {
            fp_tmp_6 = fp_stack[1];
            fp_stack[1] = fp_stack[0];
            fp_stack[0] = fp_tmp_6;
            __asm ("fucompi st(1)");
            if (r8d != 0) {
                if (r8d == 0) {
                    goto label_27;
                }
                fp_stack++;
            } else {
                fp_stack++;
            }
            rax++;
            *((rsp + 0x10)) = rax;
            *(fp_stack--) = *((rsp + 0x10));
            if (rax < 0) {
                goto label_58;
            }
        } else {
            fp_stack[1] = fp_stack[0];
            fp_stack--;
        }
    }
label_27:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    rbx = *((rsp + 0x10));
    rcx = "%.0Lf";
    ? = fp_stack[0];
    fp_stack--;
    rdi = rbx;
    sprintf_chk ();
    ebx = 0xffffffff;
    rax = strlen (rbx);
    rdx = rax;
    r12 = rax;
label_11:
    rbp = *((rsp + 8));
    rbp -= rdx;
    memmove (rbp, *(rsp), rdx);
    r8 = rbp + r12;
label_26:
    if ((*((rsp + 0x48)) & 4) != 0) {
        goto label_6;
    }
    do {
label_54:
        if ((*((rsp + 0x48)) & 0x80) != 0) {
            if (ebx == 0xffffffff) {
                goto label_59;
            }
label_33:
            eax = *((rsp + 0x48));
            eax &= 0x100;
            esi = eax;
            esi |= ebx;
            if (esi != 0) {
                goto label_60;
            }
        }
label_13:
        rax = *((rsp + 8));
        *(rax) = 0;
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_61;
        }
        rax = rbp;
        return rax;
label_56:
        r13 = rbx;
        rsi = *((rsp + 0x10));
        ebx = 0;
        goto label_7;
label_55:
        ebx = *((rsp + 0x20));
    } while (1);
label_9:
    sil = (esi != 0) ? 1 : 0;
    esi = (int32_t) sil;
label_10:
    ebx++;
    if (r8 > r9) {
        goto label_62;
    }
    if (ebx == 8) {
        goto label_8;
    }
label_51:
    rax = rcx;
    edx = 0;
    ecx = esi;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    ecx >>= 1;
    r9 = rax;
    eax = rdx * 5;
    edx = 0;
    eax = rdi + rax*2;
    eax = edx:eax / r11d;
    edx = edx:eax % r11d;
    edx = rcx + rdx*2;
    edi = eax;
    rcx = r9;
    esi += edx;
    if (r11d > edx) {
        goto label_9;
    }
    sil = (r11d < esi) ? 1 : 0;
    esi = (int32_t) sil;
    esi += 2;
    goto label_10;
label_44:
    *(fp_stack--) = fp_stack[0];
    goto label_47;
label_46:
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    goto label_47;
label_48:
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
label_47:
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    eax = 0;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdx = 0xffffffffffffffff;
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    rdx = rax;
    *(fp_stack--) = fp_stack[?];
    if (rdx > rbp) {
        goto label_63;
    }
label_14:
    if ((*((rsp + 0x48)) & 8) != 0) {
        rax = *(rsp);
        if (*((rax + rdx - 1)) == 0x30) {
            goto label_64;
        }
        fp_stack++;
    } else {
        fp_stack++;
    }
    r12 = rdx;
    r12 -= r15;
    goto label_11;
label_37:
    r10d = *((rsp + 0x48));
    esi = 0;
    edi = 0;
    r10d &= 0x10;
    if (r10d != 0) {
        goto label_12;
    }
label_50:
    ebx = 0xffffffff;
    goto label_8;
label_59:
    rcx = *((rsp + 0x18));
    if (rcx <= 1) {
        goto label_65;
    }
    edx = *((rsp + 0x34));
    ebx = 1;
    eax = 1;
    do {
        rax *= rdx;
        if (rcx <= rax) {
            goto label_66;
        }
        ebx++;
    } while (ebx != 8);
label_66:
    esi = *((rsp + 0x48));
    eax = *((rsp + 0x48));
    eax &= 0x100;
    esi &= 0x40;
    if (esi != 0) {
label_16:
        rsi = *(rsp);
        rdi = rsi + 0x288;
        *((rsi + 0x287)) = 0x20;
        *((rsp + 8)) = rdi;
label_15:
        if (ebx == 0) {
            goto label_67;
        }
    }
    rsi = *((rsp + 8));
    r9d = *((rsp + 0x30));
    rdx = rsi + 1;
    if (r9d == 0) {
        if (ebx == 1) {
            goto label_68;
        }
    }
    rbx = (int64_t) ebx;
    rcx = obj_power_letter;
    ecx = *((rcx + rbx));
    *(rsi) = cl;
    if (eax == 0) {
        goto label_69;
    }
    r8d = *((rsp + 0x30));
    if (r8d != 0) {
        goto label_70;
    }
label_22:
    rax = rdx + 1;
    *(rdx) = 0x42;
    *((rsp + 8)) = rax;
    goto label_13;
label_43:
    rdx = 0xffffffffffffffff;
    esi = 1;
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r12 = *((rsp + 0x10));
    rcx = "%.1Lf";
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r12);
    *(fp_stack--) = fp_stack[?];
    rdx = rax;
    if (rax <= rbp) {
        goto label_14;
    }
    *(fp_stack--) = *(0x0000be5c);
    fp_stack[1] *= fp_stack[0];
label_17:
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    rdx = 0xffffffffffffffff;
    eax = 0;
    r15 = *((rsp + 0x10));
    rcx = "%.0Lf";
    esi = 1;
    rdi = r15;
    ? = fp_stack[0];
    fp_stack--;
    sprintf_chk ();
    rax = strlen (r15);
    rdx = rax;
    r12 = rax;
    goto label_11;
label_60:
    if ((*((rsp + 0x48)) & 0x40) == 0) {
        goto label_15;
    }
    goto label_16;
label_63:
    *(fp_stack--) = *(0x0000be5c);
    fp_stack[1] *= fp_stack[0];
label_23:
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(2)");
    if ((*((rsp + 0x48)) & 0x40) <= 0) {
        goto label_17;
    }
    *(fp_stack--) = *(0x0000be58);
    fp_tmp_9 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    if (fp_stack[0] >= fp_stack[2]) {
        goto label_71;
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
label_25:
    *((rsp + 0x20)) = rax;
    *(fp_stack--) = *((rsp + 0x20));
    if (rax < 0) {
        fp_stack[0] += *(0x0000be54);
    }
    edx = *((rsp + 0x10));
    if (edx == 0) {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        if (fp_stack[0] != fp_stack[1]) {
            if (fp_stack[0] == fp_stack[1]) {
                goto label_72;
            }
            fp_stack++;
        } else {
            fp_stack++;
        }
        rax++;
        *((rsp + 0x10)) = rax;
        *(fp_stack--) = *((rsp + 0x10));
        if (rax < 0) {
            goto label_73;
        }
    } else {
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
label_72:
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    goto label_17;
label_52:
    rax = rcx;
    rsi = (int64_t) esi;
    eax &= 1;
    rax += rsi;
    al = (rax != 0) ? 1 : 0;
    eax = (int32_t) al;
    eax += edi;
    if (eax > 5) {
        goto label_18;
    }
    goto label_19;
label_49:
    esi = 2;
    eax = 3;
    if (eax < 5) {
        esi = eax;
    }
    goto label_20;
label_45:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_12 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_21;
label_70:
    *((rsi + 1)) = 0x69;
    rdx = rsi + 2;
    goto label_22;
label_64:
    *(fp_stack--) = *(0x0000be5c);
    fp_stack[1] *= fp_stack[0];
    if (*((rsp + 0x10)) != 1) {
        goto label_23;
    }
    goto label_17;
label_57:
    fp_stack[0] -= fp_stack[1];
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_24;
label_71:
    fp_stack[0] -= fp_stack[2];
    fp_tmp_14 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    eax = *((rsp + 0x4e));
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    fp_tmp_15 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_15;
    rax = *((rsp + 0x20));
    __asm ("btc rax, 0x3f");
    goto label_25;
label_53:
    if (ebx == 8) {
        goto label_19;
    }
    ebx++;
    if ((*((rsp + 0x48)) & 8) == 0) {
        goto label_74;
    }
label_35:
    *((r8 - 1)) = 0x31;
    rbp = r8 - 1;
    goto label_26;
label_58:
    fp_stack[0] += *(0x0000be54);
    goto label_27;
label_73:
    fp_stack[0] += *(0x0000be54);
    fp_tmp_16 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_16;
    goto label_17;
label_68:
    *(rsi) = 0x6b;
    if (eax != 0) {
        goto label_22;
    }
label_69:
    *((rsp + 8)) = rdx;
    goto label_13;
label_62:
    if (r9 > 9) {
        goto label_8;
    }
    if (*((rsp + 0x10)) == 1) {
        goto label_75;
    }
    r12d = *((rsp + 0x10));
    if (r12d != 0) {
        goto label_76;
    }
    if (esi == 0) {
        goto label_76;
    }
label_30:
    edx = rax + 1;
    if (eax == 9) {
        goto label_77;
    }
    eax = rdx + 0x30;
label_32:
    rdi = *(rsp);
    r8 = rdi + 0x286;
    *((rdi + 0x286)) = al;
    eax = ebp;
    r8 -= rbp;
    if (ebp >= 8) {
        goto label_78;
    }
    if ((bpl & 4) != 0) {
        goto label_79;
    }
    if (eax != 0) {
        edx = *(r15);
        *(r8) = dl;
        if ((al & 2) != 0) {
            goto label_80;
        }
    }
label_29:
    esi = 0;
label_31:
    edi = 0;
    if (*((rsp + 0x10)) != 1) {
        goto label_28;
    }
    goto label_19;
label_80:
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_29;
label_75:
    edx = eax;
    edx &= 1;
    edx += esi;
    if (edx > 2) {
        goto label_30;
    }
label_76:
    if (eax != 0) {
        goto label_81;
    }
label_34:
    r8 = *((rsp + 8));
    if ((*((rsp + 0x48)) & 8) != 0) {
        goto label_31;
    }
    eax = 0x30;
    goto label_32;
label_65:
    ebx = 0;
    goto label_33;
label_78:
    rax = *(r15);
    rsi = r8 + 8;
    rdi = r15;
    rsi &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rsi;
    rdi -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_29;
    }
    eax &= 0xfffffff8;
    r9d = eax;
    eax = 0;
    do {
        edx = eax;
        eax += 8;
        r11 = *((rdi + rdx));
        *((rsi + rdx)) = r11;
    } while (eax < r9d);
    goto label_29;
label_77:
    rcx = r9 + 1;
    if (r9 == 9) {
        goto label_82;
    }
    esi = 0;
    goto label_34;
label_74:
    rax = rbp;
    *((r8 - 1)) = 0x30;
    rax = ~rax;
    r8 += rax;
    eax = ebp;
    if (ebp >= 8) {
        goto label_83;
    }
    ebp &= 4;
    if (ebp != 0) {
        goto label_84;
    }
    if (eax == 0) {
        goto label_35;
    }
    edx = *(r15);
    *(r8) = dl;
    if ((al & 2) == 0) {
        goto label_35;
    }
    edx = *((r15 + rax - 2));
    *((r8 + rax - 2)) = dx;
    goto label_35;
label_83:
    rax = *(r15);
    rcx = r8 + 8;
    rcx &= 0xfffffffffffffff8;
    *(r8) = rax;
    eax = ebp;
    rdx = *((r15 + rax - 8));
    *((r8 + rax - 8)) = rdx;
    rax = r8;
    rax -= rcx;
    r15 -= rax;
    eax += ebp;
    eax &= 0xfffffff8;
    if (eax < 8) {
        goto label_35;
    }
    eax &= 0xfffffff8;
    edx = 0;
    do {
        esi = edx;
        edx += 8;
        rdi = *((r15 + rsi));
        *((rcx + rsi)) = rdi;
    } while (edx < eax);
    goto label_35;
label_82:
    r8 = *((rsp + 8));
    goto label_29;
label_79:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_29;
label_61:
    eax = stack_chk_fail ();
label_84:
    edx = *(r15);
    *(r8) = edx;
    edx = *((r15 + rax - 4));
    *((r8 + rax - 4)) = edx;
    goto label_35;
label_67:
    rdx = *((rsp + 8));
    if (eax != 0) {
        goto label_22;
    }
    goto label_13;
label_81:
    eax += 0x30;
    goto label_32;
}

/* /tmp/tmpqdrhff3j @ 0x8480 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x7900 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9e50 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2780)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x7ba0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9000 */
 
int64_t dbg_renameatu (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    stat src_st;
    stat dst_st;
    int64_t var_fh;
    int64_t var_10h;
    int64_t var_28h;
    int64_t var_a0h;
    int64_t var_b8h;
    int64_t var_138h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int renameatu(int fd1,char const * src,int fd2,char const * dst,unsigned int flags); */
    r15d = edi;
    r14d = edx;
    r13 = rcx;
    ebx = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x138)) = rax;
    eax = 0;
    eax = renameat2 ();
    r12d = eax;
    if (eax >= 0) {
        goto label_0;
    }
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    edx = rax - 0x16;
    edx &= 0xffffffef;
    dl = (edx != 0) ? 1 : 0;
    al = (eax != 0x5f) ? 1 : 0;
    dl &= al;
    r9d = edx;
    if (dl != 0) {
        goto label_0;
    }
    if (ebx == 0) {
        goto label_3;
    }
    if (ebx != 1) {
        goto label_4;
    }
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_5;
    }
    eax = *(r8);
    if (eax == 0x4b) {
        goto label_5;
    }
    if (eax == 2) {
        goto label_6;
    }
label_1:
    r12d = 0xffffffff;
    do {
label_0:
        rax = *((rsp + 0x138));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        eax = r12d;
        return rax;
label_5:
        *(r8) = 0x11;
        r12d = 0xffffffff;
    } while (1);
label_6:
    r9d = 1;
label_3:
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    rax = strlen (rbp);
    rbx = rax;
    rax = strlen (r13);
    if (rbx != 0) {
        r8 = *(rsp);
        r9d = *((rsp + 0xf));
        if (rax == 0) {
            goto label_2;
        }
        if (*((rbp + rbx - 1)) == 0x2f) {
            goto label_8;
        }
        if (*((r13 + rax - 1)) == 0x2f) {
            goto label_8;
        }
    }
label_2:
    rcx = r13;
    edx = r14d;
    rsi = rbp;
    edi = r15d;
    eax = renameat ();
    r12d = eax;
    goto label_0;
label_8:
    rdx = rsp + 0x10;
    ecx = 0x100;
    rsi = rbp;
    edi = r15d;
    *((rsp + 0xf)) = r9b;
    *(rsp) = r8;
    eax = fstatat ();
    if (eax != 0) {
        goto label_1;
    }
    r9d = *((rsp + 0xf));
    r8 = *(rsp);
    if (r9b == 0) {
        goto label_9;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 2;
    r12d = 0xffffffff;
    goto label_0;
label_4:
    *(r8) = 0x5f;
    r12d = 0xffffffff;
    goto label_0;
label_9:
    ecx = 0x100;
    rsi = r13;
    edi = r14d;
    *(rsp) = r8;
    rdx = rsp + 0xa0;
    eax = fstatat ();
    r8 = *(rsp);
    if (eax == 0) {
        goto label_10;
    }
    if (*(r8) != 2) {
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax != 0x4000) {
        goto label_1;
    }
    goto label_2;
label_10:
    eax = *((rsp + 0xb8));
    eax &= 0xf000;
    if (eax != 0x4000) {
        *(r8) = 0x14;
        goto label_1;
    }
    eax = *((rsp + 0x28));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    *(r8) = 0x15;
    r12d = 0xffffffff;
    goto label_0;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0xa0a0 */
 
void dbg_xdectoumax (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* uintmax_t xdectoumax(char const * n_str,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoumax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmpqdrhff3j @ 0x9f90 */
 
int64_t dbg_xnumtoumax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    uintmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* uintmax_t xnumtoumax(char const * n_str,int base,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0xa0d0 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000c658;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xc658 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmpqdrhff3j @ 0x84d0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x4e40 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpqdrhff3j @ 0x9a60 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x9c20 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x4a50 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8760 */
 
uint64_t dbg_randread_new (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* randread_source * randread_new(char const * name,size_t bytes_bound); */
    if (rsi == 0) {
        goto label_2;
    }
    rbx = rdi;
    if (rdi != 0) {
        rax = fopen_safer (rdi, 0x0000c2a7);
        r13 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = xmalloc (0x1038);
        rdi = r13;
        r12 = rax;
        *(rax) = r13;
        rax = sym_randread_error;
        *((r12 + 8)) = rax;
        eax = 0x1000;
        rsi = r12 + 0x18;
        *((r12 + 0x10)) = rbx;
        if (rbp <= rax) {
            rax = rbp;
        }
        setvbuf (rdi, rsi, 0, rax);
label_1:
        rax = r12;
        return rax;
    }
    rax = xmalloc (0x1038);
    r12 = rax;
    *(rax) = 0;
    rax = sym_randread_error;
    *((r12 + 8)) = rax;
    eax = 0x800;
    r13 = r12 + 0x20;
    *((r12 + 0x10)) = 0;
    rbx = r13;
    *((r12 + 0x18)) = 0;
    if (rbp > rax) {
    }
    rbp += r13;
    if (r13 < rbp) {
        goto label_4;
    }
    goto label_5;
    do {
        rbx += rax;
label_0:
        if (rbp <= rbx) {
            goto label_5;
        }
label_4:
        rsi = rbp;
        edx = 0;
        rdi = rbx;
        rsi -= rbx;
        rax = getrandom ();
    } while (rax >= 0);
    rax = errno_location ();
    r15d = *(rax);
    r14 = rax;
    if (r15d == 4) {
        goto label_0;
    }
    rbp = *(r12);
    rdi = r12;
    edx = 0x1038;
    esi = 0x1038;
    explicit_bzero_chk ();
    free (r12);
    if (rbp != 0) {
        rpl_fclose (rbp);
    }
    *(r14) = r15d;
    r12d = 0;
    goto label_1;
label_5:
    isaac_seed (r13, rsi, rdx, rcx);
    goto label_1;
label_2:
    xmalloc (0x1038);
    *(rax) = 0;
    r12 = rax;
    rax = sym_randread_error;
    *((r12 + 8)) = rax;
    *((r12 + 0x10)) = 0;
    goto label_1;
label_3:
    r12d = 0;
    goto label_1;
}

/* /tmp/tmpqdrhff3j @ 0xa650 */
 
uint64_t dbg_fopen_safer (int64_t arg2, const char * filename) {
    rsi = arg2;
    rdi = filename;
    /* FILE * fopen_safer(char const * file,char const * mode); */
    rax = fopen (rdi, rsi);
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    eax = fileno (rax);
    while (rax != 0) {
label_0:
        rax = r12;
        return rax;
        eax = dup_safer (eax, rsi, rdx, rcx, r8);
        r13d = eax;
        if (eax < 0) {
            goto label_1;
        }
        eax = rpl_fclose (r12);
        if (eax != 0) {
            goto label_2;
        }
        rsi = rbp;
        edi = r13d;
        rax = fdopen ();
        r12 = rax;
    }
label_2:
    rax = errno_location ();
    r12d = *(rax);
    close (r13d);
    *(rbp) = r12d;
    r12d = 0;
    goto label_0;
label_1:
    rax = errno_location ();
    r12d = 0;
    r13d = *(rax);
    rpl_fclose (r12);
    *(rbp) = r13d;
    goto label_0;
}

/* /tmp/tmpqdrhff3j @ 0x2860 */
 
void setvbuf (void) {
    __asm ("bnd jmp qword [reloc.setvbuf]");
}

/* /tmp/tmpqdrhff3j @ 0x2960 */
 
void getrandom (void) {
    __asm ("bnd jmp qword [reloc.getrandom]");
}

/* /tmp/tmpqdrhff3j @ 0x2520 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpqdrhff3j @ 0xa570 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2610)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpqdrhff3j @ 0x9b40 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x86b0 */
 
uint64_t dbg_randint_all_free (int64_t arg1) {
    rdi = arg1;
    /* int randint_all_free(randint_source * s); */
    eax = randread_free (*(rdi));
    r12d = eax;
    rax = errno_location ();
    r13d = *(rax);
    rbx = rax;
    randint_free (rbp);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8490 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x83e0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a18)() ();
    }
    if (rax == 0) {
        void (*0x2a18)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x4b40 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmpqdrhff3j @ 0x9e90 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2780)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x4f20 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0xa840 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x5020)() ();
}

/* /tmp/tmpqdrhff3j @ 0x7b90 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x7aa0)() ();
}

/* /tmp/tmpqdrhff3j @ 0x8520 */
 
uint64_t dbg_randint_all_new (void) {
    /* randint_source * randint_all_new(char const * name,size_t bytes_bound); */
    rax = randread_new (rdi, rsi);
    if (rax != 0) {
        rdi = rax;
        void (*0x84f0)() ();
    }
    eax = 0;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8910 */
 
void dbg_randread_set_handler_arg ( const * handler_arg, randread_source * s) {
    rsi = handler_arg;
    rdi = s;
    /* void randread_set_handler_arg(randread_source * s, const * handler_arg); */
    *((rdi + 0x10)) = rsi;
}

/* /tmp/tmpqdrhff3j @ 0x7c70 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x8900 */
 
void dbg_randread_set_handler (void (*)() handler, randread_source * s) {
    rsi = handler;
    rdi = s;
    /* void randread_set_handler(randread_source * s,void (*)() handler); */
    *((rdi + 8)) = rsi;
}

/* /tmp/tmpqdrhff3j @ 0x2a30 */
 
void dbg_main (int32_t argc, char ** argv) {
    Options flags;
    uint32_t var_8h;
    int64_t var_13h;
    uint32_t fildes;
    uint32_t var_18h;
    void * s1;
    void * var_28h;
    void * ptr;
    int64_t var_38h;
    void * var_40h;
    int64_t var_48h;
    uint32_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    uint32_t var_68h;
    uint32_t var_6ch;
    int64_t var_6dh;
    int64_t var_6eh;
    int64_t var_78h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    xmm0 = 0;
    r15 = 0x0000c303;
    r14 = obj_long_opts;
    r13 = "fn:s:uvxz";
    r12 = 0x0000b178;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    *((rsp + 0x50)) = xmm0;
    *((rsp + 0x60)) = xmm0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, r15);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x0000bba0;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    rax = atexit ();
    *(rsp) = 0;
    *((rsp + 0x58)) = 3;
    *((rsp + 0x60)) = 0xffffffffffffffff;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_13;
        }
        if (eax > 0x80) {
            goto label_14;
        }
        if (eax <= 0x65) {
            goto label_15;
        }
        eax -= 0x66;
        if (eax > 0x1a) {
            goto label_14;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (27 cases) at 0xbba0 */
        void (*rax)() ();
        rdi = *(rsp);
        rdx = optarg;
        if (rdi != 0) {
            rsi = rdx;
            *(rsp) = rdx;
            eax = strcmp (rdi, rsi);
            rdx = *(rsp);
            if (eax != 0) {
                goto label_16;
            }
        }
        *(rsp) = rdx;
    } while (1);
    *((rsp + 0x6e)) = 1;
    goto label_0;
    *((rsp + 0x6d)) = 1;
    goto label_0;
    *((rsp + 0x6c)) = 1;
    goto label_0;
    rsi = optarg;
    if (rsi != 0) {
        _xargmatch_internal ("--remove", rsi, obj.remove_args, obj.remove_methods, 4, *(obj.argmatch_die));
        rcx = obj_remove_methods;
        eax = *((rcx + rax*4));
        *((rsp + 0x78)) = eax;
        goto label_0;
        edx = 5;
        rax = dcgettext (0, "invalid file size");
        rcx = 0x7fffffffffffffff;
        rax = xnumtoumax (*(obj.optarg), 0, 0, rcx, "cbBkKMGTPEZY0", rax);
        *((rsp + 0x70)) = rax;
        goto label_0;
        edx = 5;
        rax = dcgettext (0, "invalid number of passes");
        rdx = 0x3fffffffffffffff;
        rax = xdectoumax (*(obj.optarg), 0, rdx, r15, rax, 0);
        *((rsp + 0x58)) = rax;
        goto label_0;
        *((rsp + 0x50)) = 1;
        goto label_0;
label_15:
        if (eax == 0xffffff7d) {
            eax = 0;
            version_etc (*(obj.stdout), "shred", "GNU coreutils", *(obj.Version), "Colin Plumb", 0);
            eax = exit (0);
        }
        if (eax != 0xffffff7e) {
            goto label_14;
        }
        usage (0);
    }
    *((rsp + 0x68)) = 3;
    goto label_0;
label_13:
    r12 = *(obj.optind);
    ebp -= r12d;
    if (ebp == 0) {
        goto label_17;
    }
    rdi = *(rsp);
    rsi |= 0xffffffffffffffff;
    rax = randint_all_new ();
    *(obj.randint_source) = rax;
    if (rax == 0) {
        goto label_18;
    }
    rdi = dbg_clear_random_data;
    atexit ();
    if (ebp <= 0) {
        goto label_19;
    }
    eax = rbp - 1;
    *((rsp + 0x13)) = 1;
    r13 = rbx + r12*8;
    rax += r12;
    r15 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.";
    rax = rbx + rax*8 + 8;
    *((rsp + 8)) = rax;
    while (*(r12) == 0x2d) {
        if (*((r12 + 1)) != 0) {
            goto label_20;
        }
        eax = 0;
        r12 = randint_source;
        eax = rpl_fcntl (1, 3, rdx, rcx, r8, r9);
        if (eax < 0) {
            goto label_21;
        }
        if ((ah & 4) != 0) {
            goto label_22;
        }
        al = do_wipefd (1, rbp, r12, rsp + 0x50, r8);
label_2:
label_1:
        r13 += 8;
        free (rbp);
        if (*((rsp + 8)) == r13) {
            goto label_23;
        }
        rdx = *(r13);
        edi = 0;
        esi = 3;
        rax = quotearg_n_style_colon ();
        rax = xstrdup (rax);
        r12 = *(r13);
    }
label_20:
    eax = 0;
    r14 = randint_source;
    eax = open_safer (r12, 0x101, rdx, rcx);
    ebx = eax;
    if (eax < 0) {
        goto label_24;
    }
label_5:
    al = do_wipefd (ebx, rbp, r14, rsp + 0x50, r8);
    *(rsp) = al;
    eax = close (ebx);
    if (eax != 0) {
        goto label_25;
    }
    if (*(rsp) == 0) {
        goto label_3;
    }
    if (*((rsp + 0x68)) != 0) {
        goto label_26;
    }
    do {
label_3:
        ecx = *(rsp);
        goto label_1;
label_24:
        rax = errno_location ();
        r8 = rax;
        if (*(rax) == 0xd) {
            goto label_27;
        }
label_4:
        edx = 5;
        *(rsp) = r8;
        rax = dcgettext (0, "%s: failed to open for writing");
        r8 = *(rsp);
        rcx = rbp;
        eax = 0;
        error (0, *(r8), rax);
        *(rsp) = 0;
    } while (1);
label_22:
    edx = 5;
    rax = dcgettext (0, "%s: cannot shred append-only file descriptor");
    rcx = rbp;
    eax = 0;
    eax = error (0, 0, rax);
    eax = 0;
    goto label_2;
label_19:
    *((rsp + 0x13)) = 1;
label_23:
    eax = *((rsp + 0x13));
    eax ^= 1;
    eax = (int32_t) al;
    rdx = *((rsp + 0x78));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_28;
    }
    return;
label_25:
    edx = 5;
    rax = dcgettext (0, "%s: failed to close");
    r12 = rax;
    rax = errno_location ();
    rcx = rbp;
    eax = 0;
    error (0, *(rax), r12);
    *(rsp) = 0;
    goto label_3;
label_21:
    edx = 5;
    rax = dcgettext (0, "%s: fcntl failed");
    r12 = rax;
    rax = errno_location ();
    rcx = rbp;
    eax = 0;
    eax = error (0, *(rax), r12);
    eax = 0;
    goto label_2;
label_27:
    if (*((rsp + 0x50)) == 0) {
        goto label_4;
    }
    *(rsp) = rax;
    eax = chmod (r12, 0x80);
    r8 = *(rsp);
    if (eax != 0) {
        goto label_4;
    }
    eax = open_safer (r12, 0x101, rdx, rcx);
    r8 = *(rsp);
    ebx = eax;
    if (eax >= 0) {
        goto label_5;
    }
    goto label_4;
label_26:
    rax = xstrdup (r12);
    r14 = rax;
    rdi = rax;
    *((rsp + 0x30)) = rax;
    rax = last_component ();
    rdi = r14;
    rbx = rax;
    rax = dir_name ();
    edi = 0;
    esi = 3;
    rdx = rax;
    *((rsp + 0x40)) = rax;
    rax = quotearg_n_style_colon ();
    rax = xstrdup (rax);
    *((rsp + 0x14)) = 0xffffffff;
    *((rsp + 0x28)) = rax;
    if (*((rsp + 0x68)) == 3) {
        goto label_29;
    }
label_8:
    if (*((rsp + 0x6c)) != 0) {
        goto label_30;
    }
label_7:
    if (*((rsp + 0x68)) == 1) {
        goto label_31;
    }
    base_len (rbx);
    r14 = *((rsp + 0x30));
    *((rsp + 0x38)) = rbp;
    rcx = rax - 1;
    eax = *(rsp);
    *((rsp + 0x48)) = r13;
    r13 = rcx;
    *((rsp + 0x18)) = al;
    rax = rbx;
    rax -= r14;
    rax += r12;
    *((rsp + 0x20)) = rax;
label_9:
    rdx = r13 + 1;
    if (r13 == -1) {
        goto label_32;
    }
    memset (rbx, 0x30, rdx);
    *((rbx + r13 + 1)) = 0;
label_6:
    edx = 0xffffff9c;
    eax = renameatu (0xffffff9c, r12, edx, r14, 1);
    if (eax == 0) {
        goto label_33;
    }
    rax = errno_location ();
    if (*(rax) != 0x11) {
        goto label_34;
    }
    rbp = rbx + r13;
    while (al == 0) {
        *(rbp) = 0x30;
        rax = rbp - 1;
        if (rbx == rbp) {
            goto label_34;
        }
        esi = *(rbp);
        rax = strchr (r15, rsi);
        if (rax == 0) {
            goto label_35;
        }
        eax = *((rax + 1));
    }
    *(rbp) = al;
    goto label_6;
label_32:
    r13 = *((rsp + 0x48));
    rbp = *((rsp + 0x38));
label_31:
    eax = unlink (r12);
    if (eax != 0) {
        goto label_36;
    }
    if (*((rsp + 0x6c)) != 0) {
        goto label_37;
    }
    do {
label_12:
        ebx = *((rsp + 0x14));
        if (ebx >= 0) {
            goto label_38;
        }
label_11:
        free (*((rsp + 0x30)));
        free (*((rsp + 0x40)));
        free (*((rsp + 0x28)));
        goto label_3;
label_17:
        edx = 5;
        rax = dcgettext (0, "missing file operand");
        eax = 0;
        error (0, 0, rax);
label_14:
        usage (1);
label_30:
        edx = 5;
        rax = dcgettext (0, "%s: removing");
        rcx = rbp;
        eax = 0;
        error (0, 0, rax);
        goto label_7;
label_36:
        edx = 5;
        rax = dcgettext (0, "%s: failed to remove");
        r12 = rax;
        rax = errno_location ();
        rcx = rbp;
        eax = 0;
        eax = error (0, *(rax), r12);
        *(rsp) = 0;
    } while (1);
label_29:
    eax = 0;
    eax = open_safer (*((rsp + 0x40)), 0x10900, rdx, rcx);
    *((rsp + 0x14)) = eax;
    goto label_8;
label_33:
    while (eax == 0) {
label_10:
        if (*((rsp + 0x6c)) != 0) {
            rcx = *((rsp + 0x38));
            edx = 5;
            rsi = "%s: renamed to %s";
            if (*((rsp + 0x18)) == 0) {
                rcx = r12;
            }
            *((rsp + 0x18)) = rcx;
            rax = dcgettext (0, rsi);
            rcx = *((rsp + 0x18));
            r8 = r14;
            eax = 0;
            error (0, 0, rax);
            *((rsp + 0x18)) = 0;
        }
        memcpy (*((rsp + 0x20)), rbx, r13 + 2);
label_34:
        r13--;
        goto label_9;
        ebp = *((rsp + 0x14));
        edi = *((rsp + 0x14));
        eax = fdatasync ();
    }
    rsi = *((rsp + 0x28));
    edi = ebp;
    eax = dosync_part_0 ();
    ecx = *(rsp);
    eax = 0;
    if (eax != 0) {
        ecx = eax;
    }
    *(rsp) = cl;
    goto label_10;
label_38:
    edi = ebx;
    eax = fdatasync ();
    if (eax != 0) {
        rsi = *((rsp + 0x28));
        edi = ebx;
        eax = dosync_part_0 ();
        ecx = *(rsp);
        eax = 0;
        if (eax != 0) {
            ecx = eax;
        }
        *(rsp) = cl;
    }
    eax = close (*((rsp + 0x14)));
    if (eax == 0) {
        goto label_11;
    }
    edx = 5;
    rax = dcgettext (0, "%s: failed to close");
    r12 = rax;
    rax = errno_location ();
    rcx = *((rsp + 0x28));
    eax = 0;
    error (0, *(rax), r12);
    *(rsp) = 0;
    goto label_11;
label_37:
    edx = 5;
    rax = dcgettext (0, "%s: removed");
    rcx = rbp;
    eax = 0;
    error (0, 0, rax);
    goto label_12;
label_35:
    assert_fail (0x0000b2d6, "src/shred.c", 0x3e8, "incname");
label_28:
    stack_chk_fail ();
label_16:
    edx = 5;
    rax = dcgettext (0, "multiple random sources specified");
    eax = 0;
    error (1, 0, rax);
label_18:
    while (1) {
        rdx = *(rsp);
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r12 = rax;
        rax = errno_location ();
        rcx = r12;
        eax = 0;
        error (1, *(rax), 0x0000bdd0);
        rax = "getrandom";
        *(rsp) = rax;
    }
}

/* /tmp/tmpqdrhff3j @ 0x4510 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... FILE...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Overwrite the specified FILE(s) repeatedly, in order to make it harder\nfor even very expensive hardware probing to recover the data.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nIf FILE is -, shred standard output.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "  -f, --force    change permissions to allow writing if necessary\n  -n, --iterations=N  overwrite N times instead of the default (%d)\n      --random-source=FILE  get random bytes from FILE\n  -s, --size=N   shred this many bytes (suffixes like K, M, G accepted)\n");
    edx = 3;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -u             deallocate and remove file after overwriting\n      --remove[=HOW]  like -u but give control on HOW to delete;  See below\n  -v, --verbose  show progress\n  -x, --exact    do not round file sizes up to the next full block;\n                   this is the default for non-regular files\n  -z, --zero     add a final overwrite with zeros to hide shredding\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nDelete FILE(s) if --remove (-u) is specified.  The default is not to remove\nthe files because it is common to operate on device files like /dev/hda,\nand those files usually should not be removed.\nThe optional HOW parameter indicates how to remove a directory entry:\n'unlink' => use a standard unlink call.\n'wipe' => also first obfuscate bytes in the name.\n'wipesync' => also sync each obfuscated byte to the device.\nThe default mode is 'wipesync', but note it can be expensive.\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "CAUTION: shred assumes the file system and hardware overwrite data in place.\nAlthough this is common, many platforms operate otherwise.  Also, backups\nand mirrors may contain unremovable copies that will let a shredded file\nbe recovered later.  See the GNU coreutils manual for details.\n");
    rsi = r12;
    r12 = "shred";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000b0fe;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000b178;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000b182, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000c303;
    r12 = 0x0000b11a;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000b182, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "shred";
        printf_chk ();
        r12 = 0x0000b11a;
    }
label_5:
    r13 = "shred";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpqdrhff3j @ 0x7940 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8030 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x5020 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = rsi - 0x400;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7e60 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x29ff)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7d40 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x29f5)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x79e0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x29ea)() ();
    }
    if (rdx == 0) {
        void (*0x29ea)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x8550 */
 
int64_t dbg_randint_genmax (int64_t arg1, int64_t arg2) {
    unsigned char[8] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* randint randint_genmax(randint_source * s,randint genmax); */
    r14 = rdi;
    r13 = rsi + 1;
    rcx = *(rdi);
    r12 = *((rdi + 8));
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rbx = *((rdi + 0x10));
    r15 = rsp + 0x10;
    while (rbx >= rbp) {
        if (rbx == rbp) {
            goto label_1;
        }
label_0:
        rax = rbx;
        edx = 0;
        rax -= rbp;
        rax = rdx:rax / r13;
        rdx = rdx:rax % r13;
        rdi = rdx;
        rsi = rax;
        rbx -= rdx;
        rax = r12;
        edx = 0;
        rax = rdx:rax / r13;
        rdx = rdx:rax % r13;
        if (r12 <= rbx) {
            goto label_2;
        }
        rbx = rdi - 1;
        r12 = rdx;
    }
    rdx = rbx;
    r8d = 0;
    do {
        rdx <<= 8;
        r8++;
        rdx += 0xff;
    } while (rbp > rdx);
    rdi = rcx;
    *((rsp + 8)) = rcx;
    randread (rdi, r15, r8, rcx, r8, r9);
    rcx = *((rsp + 8));
    rdx = r15;
    do {
        eax = *(rdx);
        rbx <<= 8;
        r12 <<= 8;
        rdx++;
        rbx += 0xff;
        r12 += rax;
    } while (rbp > rbx);
    if (rbx != rbp) {
        goto label_0;
    }
label_1:
    *((r14 + 0x10)) = 0;
    *((r14 + 8)) = 0;
    do {
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r12;
        return rax;
label_2:
        *((r14 + 8)) = rax;
        r12 = rdx;
        *((r14 + 0x10)) = rsi;
    } while (1);
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7ef0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010230]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00010240]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x9db0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x9e30 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x7c40 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpqdrhff3j @ 0x9ad0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0xa6f0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x28d0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpqdrhff3j @ 0x5290 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x7980 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9cb0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpqdrhff3j @ 0x5f10 */
 
int64_t dbg_human_options (int64_t arg1, int64_t arg2, int64_t arg3) {
    char * ptr;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* strtol_error human_options(char const * spec,int * opts,uintmax_t * block_size); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
label_1:
    r12d = 0;
    if (*(rbx) == 0x27) {
        rbx++;
        r12d = 4;
    }
    r14 = obj_block_size_opts;
    eax = argmatch (rbx, obj.block_size_args, r14, 4);
    if (eax >= 0) {
        rax = (int64_t) eax;
        *(rbp) = 1;
        r12d |= *((r14 + rax*4));
        eax = 0;
        *(r13) = r12d;
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
    }
    eax = xstrtoumax (rbx, rsp, 0, rbp, "eEgGkKmMpPtTyYzZ0");
    if (eax != 0) {
        goto label_6;
    }
    ecx = *(rbx);
    edx = rcx - 0x30;
    rcx = *(rsp);
    if (dl > 9) {
        goto label_7;
    }
    goto label_2;
    do {
        edi = *((rbx + 1));
        rbx++;
        edx = rdi - 0x30;
        if (dl <= 9) {
            goto label_2;
        }
label_7:
    } while (rcx != rbx);
    if (*((rcx - 1)) == 0x42) {
        goto label_8;
    }
    r12b |= 0x80;
label_3:
    r12d |= 0x20;
label_2:
    rdx = *(rbp);
    *(r13) = r12d;
    goto label_9;
label_6:
    *(r13) = 0;
    rdx = *(rbp);
label_9:
    if (rdx != 0) {
        goto label_0;
    }
    rax = getenv ("POSIXLY_CORRECT");
    rax -= rax;
    eax &= 0x200;
    rax += 0x200;
    *(rbp) = rax;
    eax = 4;
    goto label_0;
label_4:
    rax = getenv ("BLOCK_SIZE");
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("BLOCKSIZE");
    rbx = rax;
    if (rax != 0) {
        goto label_1;
    }
    rax = getenv ("POSIXLY_CORRECT");
    if (rax == 0) {
        goto label_10;
    }
    *(rbp) = 0x200;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_8:
    r12d |= 0x180;
    if (*((rcx - 2)) != 0x69) {
        goto label_2;
    }
    goto label_3;
label_10:
    *(rbp) = 0x400;
    eax = 0;
    *(r13) = 0;
    goto label_0;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x98d0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpqdrhff3j @ 0x2760 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpqdrhff3j @ 0x4e30 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpqdrhff3j @ 0xa500 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpqdrhff3j @ 0x2590 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpqdrhff3j @ 0x92c0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000c2bd);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000c5a8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xc5a8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2950)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2950)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2950)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpqdrhff3j @ 0x9260 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x2710 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpqdrhff3j @ 0x9750 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpqdrhff3j @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x9e10 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0xa990 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpqdrhff3j @ 0x9b10 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x9ed0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2780)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpqdrhff3j @ 0x4d40 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmpqdrhff3j @ 0x2510 */
 
void getenv (void) {
    /* [15] -r-x section size 1232 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpqdrhff3j @ 0x0 */
 
void libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    eax += 0x32231e3b;
    memset (rdi, eax, rcx);
    __asm ("xlatb");
    __asm ("retf");
    *(0x7b507703167e6df9) = al;
    memset (rdi, eax, rcx);
    *(0x9de85c17c8868f8) = eax;
    rdx = 0x1c252fec;
    __asm ("int3");
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    cl += al;
    *(rax) = eax;
    *(rax) += al;
    *(rax) += al;
    cl += al;
    *(rax) = eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    eax = 0x32;
    *(rax) += al;
    *((rax + 0x32)) += bh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    *((rax - 0x17)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("stc");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("stc");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmpqdrhff3j @ 0x2550 */
 
void fdatasync (void) {
    __asm ("bnd jmp qword [reloc.fdatasync]");
}

/* /tmp/tmpqdrhff3j @ 0x2560 */
 
void unlink (void) {
    __asm ("bnd jmp qword [reloc.unlink]");
}

/* /tmp/tmpqdrhff3j @ 0x2580 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpqdrhff3j @ 0x25a0 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmpqdrhff3j @ 0x25c0 */
 
void localeconv (void) {
    __asm ("bnd jmp qword [reloc.localeconv]");
}

/* /tmp/tmpqdrhff3j @ 0x25d0 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmpqdrhff3j @ 0x25e0 */
 
void write (void) {
    __asm ("bnd jmp qword [reloc.write]");
}

/* /tmp/tmpqdrhff3j @ 0x25f0 */
 
void fread_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fread_unlocked]");
}

/* /tmp/tmpqdrhff3j @ 0x2600 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpqdrhff3j @ 0x2610 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpqdrhff3j @ 0x2620 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpqdrhff3j @ 0x2640 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpqdrhff3j @ 0x2670 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpqdrhff3j @ 0x2680 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpqdrhff3j @ 0x2690 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpqdrhff3j @ 0x26a0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpqdrhff3j @ 0x26c0 */
 
void ftruncate (void) {
    __asm ("bnd jmp qword [reloc.ftruncate]");
}

/* /tmp/tmpqdrhff3j @ 0x26d0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpqdrhff3j @ 0x26e0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpqdrhff3j @ 0x26f0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpqdrhff3j @ 0x2700 */
 
void ioctl (void) {
    __asm ("bnd jmp qword [reloc.ioctl]");
}

/* /tmp/tmpqdrhff3j @ 0x2720 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpqdrhff3j @ 0x2770 */
 
void memcpy_chk (void) {
    __asm ("bnd jmp qword [reloc.__memcpy_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x2780 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpqdrhff3j @ 0x2790 */
 
void time (void) {
    __asm ("bnd jmp qword [reloc.time]");
}

/* /tmp/tmpqdrhff3j @ 0x27a0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpqdrhff3j @ 0x27b0 */
 
void sync (void) {
    __asm ("bnd jmp qword [reloc.sync]");
}

/* /tmp/tmpqdrhff3j @ 0x27d0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpqdrhff3j @ 0x27f0 */
 
void renameat2 (void) {
    __asm ("bnd jmp qword [reloc.renameat2]");
}

/* /tmp/tmpqdrhff3j @ 0x2800 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpqdrhff3j @ 0x2820 */
 
void strcpy_chk (void) {
    __asm ("bnd jmp qword [reloc.__strcpy_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x2830 */
 
void fdopen (void) {
    __asm ("bnd jmp qword [reloc.fdopen]");
}

/* /tmp/tmpqdrhff3j @ 0x2870 */
 
void chmod (void) {
    __asm ("bnd jmp qword [reloc.chmod]");
}

/* /tmp/tmpqdrhff3j @ 0x2880 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmpqdrhff3j @ 0x28a0 */
 
void fsync (void) {
    __asm ("bnd jmp qword [reloc.fsync]");
}

/* /tmp/tmpqdrhff3j @ 0x28c0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpqdrhff3j @ 0x28d0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpqdrhff3j @ 0x28e0 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpqdrhff3j @ 0x28f0 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmpqdrhff3j @ 0x2900 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpqdrhff3j @ 0x2910 */
 
void renameat (void) {
    __asm ("bnd jmp qword [reloc.renameat]");
}

/* /tmp/tmpqdrhff3j @ 0x2920 */
 
void getpagesize (void) {
    __asm ("bnd jmp qword [reloc.getpagesize]");
}

/* /tmp/tmpqdrhff3j @ 0x2980 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpqdrhff3j @ 0x2990 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpqdrhff3j @ 0x29a0 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmpqdrhff3j @ 0x29b0 */
 
void fstatat (void) {
    __asm ("bnd jmp qword [reloc.fstatat]");
}

/* /tmp/tmpqdrhff3j @ 0x29c0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpqdrhff3j @ 0x29d0 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmpqdrhff3j @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1248 named .plt */
    __asm ("bnd jmp qword [0x0000fd58]");
}

/* /tmp/tmpqdrhff3j @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24a0 */
 
void fcn_000024a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24b0 */
 
void fcn_000024b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24c0 */
 
void fcn_000024c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24d0 */
 
void fcn_000024d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24e0 */
 
void fcn_000024e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpqdrhff3j @ 0x24f0 */
 
void fcn_000024f0 (void) {
    return __asm ("bnd jmp section..plt");
}
