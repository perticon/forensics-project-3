dopass(int param_1,long param_2,undefined8 param_3,ulong *param_4,uint param_5,undefined8 param_6,
      undefined8 param_7,long param_8)

{
  char cVar1;
  ushort uVar2;
  int iVar3;
  undefined *__src;
  __off_t _Var4;
  int *piVar5;
  ulong uVar6;
  ssize_t sVar7;
  undefined8 uVar8;
  char *__s2;
  char *pcVar9;
  time_t tVar10;
  bool bVar11;
  undefined uVar12;
  uint uVar13;
  uint uVar14;
  ulong uVar15;
  undefined4 uVar16;
  size_t __n;
  ulong uVar17;
  long in_FS_OFFSET;
  undefined8 uVar18;
  ulong local_860;
  undefined8 local_858;
  ulong local_840;
  long local_828;
  char *local_818;
  time_t local_810;
  undefined2 local_808 [2];
  undefined4 local_804;
  undefined4 local_7ff;
  undefined2 local_7fb;
  undefined local_7f9;
  char local_7f8 [656];
  undefined local_568 [656];
  undefined local_2d8 [664];
  long local_40;
  
  uVar17 = *param_4;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  getpagesize();
  if ((int)param_5 < 1) {
LAB_00103a80:
    local_840 = 0x10000;
    uVar15 = 0x10002;
  }
  else {
    uVar14 = param_5 & 0xfff;
    uVar13 = uVar14 | uVar14 << 0xc;
    cVar1 = (char)(uVar13 >> 4);
    if (((char)(uVar13 >> 8) == cVar1) && (cVar1 == (char)uVar14)) goto LAB_00103a80;
    local_840 = 0xf000;
    uVar15 = 0xf000;
  }
  __src = (undefined *)xalignalloc();
  bVar11 = 0 < (long)uVar17 && uVar17 < local_840;
  if (((0 >= (long)uVar17 || uVar17 >= local_840) && (uVar13 = rpl_fcntl(), 0 < (int)uVar13)) &&
     (uVar13 != (uVar13 | 0x4000))) {
    rpl_fcntl();
  }
  if ((*(uint *)(param_2 + 0x18) & 0xf000) != 0x2000) {
LAB_001035ed:
    uVar18 = 0x1035f9;
    _Var4 = lseek(param_1,0,0);
    if (_Var4 < 1) {
      if (_Var4 == 0) goto joined_r0x00103ba6;
      piVar5 = __errno_location();
    }
    else {
      piVar5 = __errno_location();
      *piVar5 = 0x16;
    }
    uVar18 = dcgettext(0,"%s: cannot rewind",5);
    iVar3 = *piVar5;
LAB_00103629:
    error(0,iVar3,uVar18,param_3);
LAB_00103635:
    free(__src);
    local_858 = 0xffffffff;
LAB_0010363f:
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return local_858;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  local_808[0] = 6;
  local_804 = 1;
  uVar18 = 0x103b98;
  iVar3 = ioctl(param_1,0x40086d01,local_808);
  if (iVar3 != 0) goto LAB_001035ed;
joined_r0x00103ba6:
  if ((int)param_5 < 0) {
    local_7ff = 0x646e6172;
    local_7fb = 0x6d6f;
    local_7f9 = 0;
    goto LAB_0010374a;
  }
  uVar13 = (param_5 & 0xfff) << 0xc | param_5 & 0xfff;
  uVar12 = (undefined)(uVar13 >> 4);
  uVar2 = (ushort)uVar13;
  if ((long)uVar17 < 0) {
    *__src = uVar12;
    *(ushort *)(__src + 1) = uVar2 << 8 | uVar2 >> 8;
LAB_001036be:
    __n = 3;
    do {
      uVar6 = __n * 2;
      uVar18 = 0x1036db;
      memcpy(__src + __n,__src,__n);
      __n = uVar6;
    } while (uVar6 <= uVar15 >> 1);
  }
  else {
    *__src = uVar12;
    if (uVar17 < uVar15) {
      uVar15 = uVar17;
    }
    *(ushort *)(__src + 1) = uVar2 << 8 | uVar2 >> 8;
    if (5 < uVar15) goto LAB_001036be;
    uVar6 = 3;
  }
  if (uVar6 < uVar15) {
    uVar18 = 0x103c83;
    memcpy(__src + uVar6,__src,uVar15 - uVar6);
    if ((param_5 & 0x1000) != 0) goto LAB_001036ff;
  }
  else if (((param_5 & 0x1000) != 0) && (uVar15 != 0)) {
LAB_001036ff:
    uVar6 = 0;
    do {
      __src[uVar6] = __src[uVar6] + -0x80;
      uVar6 = uVar6 + 0x200;
    } while (uVar6 < uVar15);
  }
  __sprintf_chk(&local_7ff,1,7,"%02x%02x%02x",*__src,__src[1],__src[2],uVar18);
LAB_0010374a:
  local_828 = 0;
  if (param_8 != 0) {
    uVar8 = 0x103c23;
    uVar18 = dcgettext(0,"%s: pass %lu/%lu (%s)...",5);
    error(0,0,uVar18,param_3,param_7,param_8,&local_7ff,uVar8);
    tVar10 = time((time_t *)0x0);
    local_828 = tVar10 + 5;
  }
  local_858 = 0;
  local_818 = "";
  local_810 = 0;
  local_860 = 0;
LAB_00103785:
  do {
    uVar15 = local_840;
    if (((-1 < (long)uVar17) && (uVar6 = uVar17 - local_860, uVar6 < local_840)) &&
       ((uVar6 == 0 || (uVar15 = uVar6, (long)uVar17 < (long)local_860)))) {
      iVar3 = fdatasync(param_1);
      if ((iVar3 == 0) || (iVar3 = dosync_part_0(param_1,param_3), iVar3 == 0)) {
        free(__src);
      }
      else {
        piVar5 = __errno_location();
        if (*piVar5 != 5) goto LAB_00103635;
        free(__src);
        local_858 = 1;
      }
      goto LAB_0010363f;
    }
    if ((int)param_5 < 0) {
      randread(param_6,__src,uVar15);
    }
    uVar6 = 0;
LAB_001037dc:
    do {
      sVar7 = write(param_1,__src + uVar6,uVar15 - uVar6);
      if (sVar7 < 1) {
        if ((long)uVar17 < 0) {
          if (sVar7 != 0) {
            piVar5 = __errno_location();
            iVar3 = *piVar5;
            if (iVar3 != 0x1c) {
              if ((iVar3 == 0x16) && (!bVar11)) goto LAB_00103ad0;
              uVar18 = umaxtostr(local_860 + uVar6,local_2d8);
              uVar8 = dcgettext(0,"%s: error writing at offset %s",5);
              error(0,iVar3,uVar8,param_3,uVar18);
              goto LAB_00103635;
            }
          }
          if (0x7fffffffffffffff - local_860 < uVar6) goto LAB_00103d6e;
          uVar17 = uVar6 + local_860;
          *param_4 = uVar17;
          local_860 = uVar17;
          if (param_8 != 0) goto LAB_00103d25;
          goto LAB_00103785;
        }
        piVar5 = __errno_location();
        iVar3 = *piVar5;
        if ((iVar3 != 0x16) || (bVar11)) {
          uVar18 = umaxtostr(local_860 + uVar6,local_2d8);
          uVar8 = dcgettext(0,"%s: error writing at offset %s",5);
          error(0,iVar3,uVar8,param_3,uVar18);
          if ((iVar3 != 5) || (uVar15 <= (uVar6 | 0x1ff))) goto LAB_00103635;
          uVar6 = (uVar6 | 0x1ff) + 1;
          _Var4 = lseek(param_1,local_860 + uVar6,0);
          if (_Var4 == -1) {
            uVar18 = dcgettext(0,"%s: lseek failed",5);
            error(0,*piVar5,uVar18,param_3);
            goto LAB_00103635;
          }
          local_858 = 1;
          if (uVar15 <= uVar6) break;
          goto LAB_001037dc;
        }
LAB_00103ad0:
        uVar13 = rpl_fcntl(param_1,3);
        bVar11 = true;
        if ((0 < (int)uVar13) && ((uVar13 & 0x4000) != 0)) {
          rpl_fcntl(param_1,4,uVar13 & 0xffffbfff);
        }
      }
      else {
        uVar6 = uVar6 + sVar7;
      }
    } while (uVar6 < uVar15);
    if (0x7fffffffffffffff - local_860 < uVar6) goto LAB_00103d6e;
    local_860 = uVar6 + local_860;
  } while (param_8 == 0);
  if (uVar17 == local_860) {
LAB_00103d25:
    if ((*local_818 == '\0') && (local_810 = time((time_t *)0x0), local_810 < local_828))
    goto LAB_00103785;
    __s2 = (char *)human_readable(local_860,local_568,0x1b2,1,1);
  }
  else {
    local_810 = time((time_t *)0x0);
    if (local_810 < local_828) goto LAB_00103785;
    __s2 = (char *)human_readable(local_860,local_568,0x1b2,1,1);
    iVar3 = strcmp(local_818,__s2);
    if (iVar3 == 0) goto LAB_00103785;
  }
  if ((long)uVar17 < 0) {
    uVar18 = dcgettext(0,"%s: pass %lu/%lu (%s)...%s",5);
    error(0,0,uVar18,param_3,param_7,param_8,&local_7ff,__s2);
  }
  else {
    uVar16 = 100;
    if (uVar17 != 0) {
      if (local_860 < 0x28f5c28f5c28f5d) {
        uVar16 = (undefined4)((local_860 * 100) / uVar17);
      }
      else {
        uVar16 = (undefined4)
                 (local_860 /
                 (ulong)(((long)(SUB168(SEXT816(-0x5c28f5c28f5c28f5) * SEXT816((long)uVar17) >> 0x40
                                        ,0) + uVar17) >> 6) - ((long)uVar17 >> 0x3f)));
      }
    }
    pcVar9 = (char *)human_readable(uVar17,local_2d8,0x1b0,1,1);
    if (uVar17 == local_860) {
      __s2 = pcVar9;
    }
    uVar18 = dcgettext(0,"%s: pass %lu/%lu (%s)...%s/%s %d%%",5);
    error(0,0,uVar18,param_3,param_7,param_8,&local_7ff,__s2,pcVar9,uVar16);
  }
  local_818 = local_7f8;
  __strcpy_chk(local_818,__s2,0x28c);
  local_828 = local_810 + 5;
  iVar3 = fdatasync(param_1);
  if ((iVar3 != 0) && (iVar3 = dosync_part_0(param_1,param_3), iVar3 != 0)) {
    piVar5 = __errno_location();
    if (*piVar5 != 5) goto LAB_00103635;
    local_858 = 1;
  }
  goto LAB_00103785;
LAB_00103d6e:
  uVar18 = dcgettext(0,"%s: file too large",5);
  iVar3 = 0;
  goto LAB_00103629;
}