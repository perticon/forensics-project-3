bool do_wipefd(int32_t fd, char * qname, int32_t * s, int32_t * flags) {
    // 0x3e80
    int64_t v1; // 0x3e80
    int64_t v2 = v1;
    int64_t v3; // bp-328, 0x3e80
    int64_t v4 = &v3; // 0x3e92
    __readfsqword(40);
    int32_t v5; // 0x3eeb
    if ((int32_t)function_29a0() != 0) {
        // 0x406d
        function_2630();
        function_2540();
        function_2890();
        goto lab_0x403f;
    } else {
        // 0x3ee4
        int32_t v6; // 0x3e80
        v5 = v6 & 0xf000;
        if (v5 == 0x2000) {
            // 0x434a
            if ((int32_t)function_25a0() != 0) {
                // 0x4016
                function_2630();
                function_2890();
                goto lab_0x403f;
            } else {
                goto lab_0x3efb;
            }
        } else {
            goto lab_0x3efb;
        }
    }
  lab_0x3fc4:;
    // 0x3fc4
    int64_t v7; // 0x3e80
    int64_t v8 = -((0x100000000 * v7)) >> 32; // 0x3fc6
    uint64_t v9; // 0x3e80
    if (v9 <= v8) {
        // break -> 0x4403
        goto lab_0x4403;
    }
    int64_t v10 = v8; // 0x3fd4
    int64_t v11; // 0x3e80
    int64_t v12 = v11; // 0x3fd4
    int64_t v13; // 0x3e80
    int64_t v14 = v13; // 0x3fd4
    int64_t v15; // 0x3e80
    int64_t v16 = v8 + v15; // 0x3fd4
    goto lab_0x3fd7;
  lab_0x3fd7:;
    int64_t v17 = (int64_t)*(int32_t *)v12; // 0x3fd7
    int64_t v18 = v9 - v10; // 0x3fd7
    int64_t v19 = v12; // 0x3fd7
    int64_t v20 = v14; // 0x3fd7
    int64_t v21 = v16; // 0x3fd7
    goto lab_0x3fda;
  lab_0x403f:
    // 0x403f
    if (*(int64_t *)(v4 + 264) != __readfsqword(40)) {
        // 0x4504
        return function_2660() % 2 != 0;
    }
    // 0x4056
    return *(char *)(v4 + 39) % 2 != 0;
  lab_0x3efb:;
    int64_t v32; // 0x3e80
    int64_t v33; // 0x3e80
    int64_t v34; // 0x3e80
    int64_t v35; // 0x3e80
    int64_t * v36; // 0x3e80
    char * v37; // 0x3f36
    if (v5 == 0x1000 || v5 == 0xc000) {
        // 0x4016
        function_2630();
        function_2890();
        goto lab_0x403f;
    } else {
        // 0x3f17
        int128_t v38; // 0x3e80
        int64_t v39 = v38; // 0x3f1e
        if (v39 < 0 == v5 == 0x8000) {
            // 0x4462
            function_2630();
            function_2890();
            goto lab_0x403f;
        } else {
            // 0x3f2d
            v34 = (int64_t)flags;
            v35 = (int64_t)qname;
            v36 = (int64_t *)(v34 + 8);
            v37 = xnmalloc(*v36, 4);
            int64_t v40 = *(int64_t *)(v34 + 16); // 0x3f40
            if (v40 == -1) {
                if (v5 != 0x8000) {
                    int64_t v41 = function_26d0(); // 0x444a
                    v32 = v41 < 1 ? -1 : v41;
                    goto lab_0x3f6a;
                } else {
                    // 0x437a
                    v32 = v39;
                    if (*(char *)(v34 + 29) != 0) {
                        goto lab_0x3f6a;
                    } else {
                        int64_t v42 = v2 < 0x2000000000000001 ? v2 : 512; // 0x43af
                        int64_t v43 = (v38 & 0xffffffffffffffff) % (int128_t)v42; // 0x43b8
                        v3 = v39 != 0 == v42 > v39 ? v39 : 0;
                        v33 = v39;
                        if (v43 != 0) {
                            int64_t v44 = v42 - v43; // 0x43df
                            int64_t v45 = 0x7fffffffffffffff - v39; // 0x43ec
                            int64_t v46 = v45 - v44; // 0x43ef
                            int64_t v47 = v46 < 0 == ((v46 ^ v45) & (v44 ^ v45)) < 0 == (v46 != 0) ? v44 : v45; // 0x43f2
                            v33 = v47 + v39;
                        }
                        goto lab_0x40e3;
                    }
                }
            } else {
                // 0x3f5a
                v32 = v40;
                if (v5 == 0x8000) {
                    // 0x40a0
                    v3 = v39;
                    int64_t v48 = v2 < 0x2000000000000001 ? v2 : 512; // 0x40cf
                    int64_t v49 = v48 - v40; // 0x40d3
                    v32 = v40;
                    v33 = v40;
                    if ((v49 < 0 == ((v49 ^ v48) & (v40 ^ v48)) < 0 == (v49 != 0) ? v40 : v48) > v39) {
                        goto lab_0x40e3;
                    } else {
                        goto lab_0x3f6a;
                    }
                } else {
                    goto lab_0x3f6a;
                }
            }
        }
    }
  lab_0x3f6a:;
    int64_t v50 = *v36; // 0x3f6a
    int64_t v51; // 0x3e80
    int64_t v52; // 0x3e80
    int64_t v53; // 0x3e80
    int64_t v54; // 0x3e80
    if (v50 == 0) {
        // 0x4421
        v3 = v32;
        v52 = (int64_t)randint_get_source(s);
        v51 = v35;
        goto lab_0x41ac;
    } else {
        // 0x3f7c
        v3 = 0;
        v53 = v32;
        v54 = v50;
        goto lab_0x3f84;
    }
  lab_0x40e3:;
    int64_t v55 = *v36; // 0x40e3
    v53 = v33;
    v54 = v55;
    int64_t v56 = v33; // 0x40ef
    int64_t v57 = v35; // 0x40ef
    if (v55 != 0) {
        goto lab_0x3f84;
    } else {
        goto lab_0x40f5;
    }
  lab_0x3f84:;
    int64_t v58 = (int64_t)v37; // 0x3f9e
    v17 = 0xfffffffe;
    v18 = v54;
    v19 = (int64_t)&patterns;
    v20 = v58;
    v21 = 0;
    int64_t v27; // 0x3e80
    int64_t v28; // 0x3e80
    int64_t v29; // 0x3e80
    int64_t v26; // 0x3e80
    int64_t v30; // 0x3e80
    while (true) {
      lab_0x3fda:
        // 0x3fda
        v15 = v21;
        v13 = v20;
        v9 = v18;
        int64_t v22 = v17;
        v7 = 0xfffffffe;
        v11 = (int64_t)&g37;
        if (v22 == 0) {
            goto lab_0x3fc4;
        } else {
            int64_t v23 = v19 + 4; // 0x3fde
            v7 = v22;
            v11 = v23;
            if ((int32_t)v22 < 0) {
                goto lab_0x3fc4;
            } else {
                int64_t v24 = 0x100000000 * v22;
                int64_t v25 = v24 >> 32; // 0x3fe6
                if (v9 < v25) {
                    // 0x41ee
                    v26 = v13;
                    v27 = v23;
                    v28 = v25;
                    v29 = v9;
                    if (v9 < 2 || v25 > 3 * v9) {
                        // 0x4287
                        v30 = v15 + v9;
                        goto lab_0x428c;
                    } else {
                        goto lab_0x4243;
                    }
                }
                int64_t v31 = v24 >> 30; // 0x3ff2
                v10 = v25;
                v12 = v23 + v31;
                v14 = function_2780() + v31;
                v16 = v15;
                goto lab_0x3fd7;
            }
        }
    }
  lab_0x4403:
    // 0x4403
    v30 = v15 + v9;
    goto lab_0x428c;
  lab_0x40f5:;
    int64_t v72 = (int64_t)randint_get_source(s); // 0x40fa
    int64_t v73 = v72; // 0x4107
    int64_t v74 = v57; // 0x4107
    if (v3 == 0) {
        // 0x44f1
        v3 = v56;
        v52 = v72;
        v51 = v57;
        goto lab_0x41ac;
    } else {
        goto lab_0x411b;
    }
  lab_0x41ac:
    // 0x41ac
    *(int64_t *)(v4 + 40) = 0;
    if (v3 == 0) {
        // 0x41cb
        if (*(int32_t *)(v34 + 24) != 0) {
            // 0x4498
            if ((int32_t)function_26c0() == 0) {
                goto lab_0x41d6;
            } else {
                // 0x44a9
                if ((*(int32_t *)(v4 + 136) & 0xf000) != 0x8000) {
                    goto lab_0x41d6;
                } else {
                    // 0x44c0
                    function_2630();
                    function_2540();
                    function_2890();
                    // 0x41df
                    function_2520();
                    goto lab_0x403f;
                }
            }
        } else {
            goto lab_0x41d6;
        }
    } else {
        // 0x41bc
        *(int64_t *)(v4 + 8) = *(int64_t *)(v4 + 48);
        v73 = v52;
        v74 = v51;
        goto lab_0x411b;
    }
  lab_0x428c:;
    int64_t v59 = 0x100000000 * v30 >> 32; // 0x4291
    int64_t v60 = v54 - v59;
    int64_t v61 = v59 - 1; // 0x42b4
    int64_t v62 = 0;
    uint64_t v63 = v61;
    int64_t v64; // 0x430c
    int32_t * v65; // 0x4311
    int32_t * v66; // 0x4321
    int64_t v67; // 0x4327
    while (v63 > v61) {
        // 0x42f8
        v64 = randint_genmax(s, v60 + -1 - v62);
        v65 = (int32_t *)(4 * v62 + v58);
        v66 = (int32_t *)(4 * (v64 + v62) + v58);
        *v65 = *v66;
        v67 = v62 + 1;
        *v66 = *v65;
        if (v67 >= v54) {
            // break (via goto) -> 0x4332
            goto lab_0x4332;
        }
        v62 = v67;
        v63 -= v61;
    }
    int32_t * v68 = (int32_t *)(4 * v62 + v58); // 0x42d0
    *(int32_t *)(4 * v60 + v58) = *v68;
    *v68 = -1;
    int64_t v69 = v62 + 1; // 0x42ea
    int64_t v70 = v60 + 1; // 0x42f1
    while (v69 < v54) {
        int64_t v71 = v70;
        v62 = v69;
        v63 += v60;
        while (v63 > v61) {
            // 0x42f8
            v64 = randint_genmax(s, v71 + -1 - v62);
            v65 = (int32_t *)(4 * v62 + v58);
            v66 = (int32_t *)(4 * (v64 + v62) + v58);
            *v65 = *v66;
            v67 = v62 + 1;
            *v66 = *v65;
            if (v67 >= v54) {
                // break (via goto) -> 0x4332
                goto lab_0x4332;
            }
            v62 = v67;
            v63 -= v61;
        }
        // 0x42d0
        v68 = (int32_t *)(4 * v62 + v58);
        *(int32_t *)(4 * v71 + v58) = *v68;
        *v68 = -1;
        v69 = v62 + 1;
        v70 = v71 + 1;
    }
  lab_0x4332:
    // 0x4332
    v56 = v53;
    v57 = 0x100000000 * v35 >> 32;
    goto lab_0x40f5;
  lab_0x411b:;
    int64_t v75 = v74;
    int64_t v76 = v73;
    int64_t v77 = *v36; // 0x411f
    int64_t v78 = v4 + 104; // 0x4123
    *(int64_t *)v78 = v3;
    char * v79 = (char *)(v34 + 30); // 0x4128
    if (v77 != -(int64_t)*v79) {
        // 0x4131
        v3 = v78;
        uint64_t v80 = 0;
        int32_t v81 = 0; // 0x4156
        if (v77 > v80) {
            // 0x4158
            v81 = *(int32_t *)(*(int64_t *)(v4 + 16) + 4 * v80);
        }
        int64_t v82 = v80 + 1; // 0x4161
        *(int64_t *)(v4 - 8) = *(int64_t *)(v4 + 8);
        *(int64_t *)(v4 - 16) = v82;
        int64_t v83; // bp-216, 0x3e80
        int32_t v84 = dopass(fd, (struct stat *)&v83, (char *)v75, (int64_t *)v3, v81, (int32_t *)v76, (int64_t)&g52, (int64_t)&g52); // 0x417a
        if (v84 != 0) {
            if (v84 < 0) {
                // 0x41df
                function_2520();
                goto lab_0x403f;
            }
            // 0x4187
            *(char *)(v4 + 24) = 0;
        }
        int64_t v85 = *v36; // 0x418c
        while (v82 < v85 + (int64_t)*v79) {
            // 0x4150
            v80 = v82;
            v81 = 0;
            if (v85 > v80) {
                // 0x4158
                v81 = *(int32_t *)(*(int64_t *)(v4 + 16) + 4 * v80);
            }
            // 0x4161
            v82 = v80 + 1;
            *(int64_t *)(v4 - 8) = *(int64_t *)(v4 + 8);
            *(int64_t *)(v4 - 16) = v82;
            v84 = dopass(fd, (struct stat *)&v83, (char *)v75, (int64_t *)v3, v81, (int32_t *)v76, (int64_t)&g52, (int64_t)&g52);
            if (v84 != 0) {
                if (v84 < 0) {
                    // 0x41df
                    function_2520();
                    goto lab_0x403f;
                }
                // 0x4187
                *(char *)(v4 + 24) = 0;
            }
            // 0x418c
            v85 = *v36;
        }
    }
    // 0x41a3
    v3 = *(int64_t *)(v4 + 40);
    v52 = v76;
    v51 = v75;
    goto lab_0x41ac;
  lab_0x41d6:
    // 0x41d6
    *(char *)(v4 + 39) = *(char *)(v4 + 24);
    // 0x41df
    function_2520();
    goto lab_0x403f;
  lab_0x4243:;
    int64_t v86 = v29;
    int64_t v87 = v28;
    int64_t v88 = v27;
    int64_t v89 = v26;
    int64_t v90; // 0x3e80
    int64_t v91; // 0x3e80
    int64_t v92; // 0x3e80
    if (v86 == v87) {
        goto lab_0x4260;
    } else {
        int64_t v93 = v87 - 1; // 0x424c
        uint64_t v94 = randint_genmax(s, v93); // 0x4256
        v92 = v89;
        v90 = v86;
        v91 = v93;
        if (v86 > v94) {
            goto lab_0x4260;
        } else {
            goto lab_0x4240;
        }
    }
  lab_0x4260:
    // 0x4260
    *(int32_t *)v89 = *(int32_t *)v88;
    int64_t v95 = v86 - 1; // 0x426c
    v30 = v15;
    if (v95 != 0) {
        // 0x4239
        v92 = v89 + 4;
        v90 = v95;
        v91 = v87 - 1;
        goto lab_0x4240;
    } else {
        goto lab_0x428c;
    }
  lab_0x4240:
    // 0x4240
    v26 = v92;
    v27 = v88 + 4;
    v28 = v91;
    v29 = v90;
    goto lab_0x4243;
}