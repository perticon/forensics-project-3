bool do_wipefd(int param_1,undefined8 param_2,undefined8 param_3,long param_4)

{
  int iVar1;
  uint uVar2;
  int *piVar3;
  void *pvVar4;
  undefined8 uVar5;
  ulong uVar6;
  long lVar7;
  bool bVar8;
  long lVar9;
  ulong uVar10;
  undefined1 *puVar11;
  int *piVar12;
  int *piVar13;
  int *__src;
  ulong uVar14;
  long in_FS_OFFSET;
  long local_148;
  long local_140;
  ulong local_130;
  bool local_121;
  long local_120;
  long local_118;
  long local_110;
  long local_e0;
  stat local_d8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_118 = 0;
  if (*(char *)(param_4 + 0x1c) != '\0') {
    local_118 = (ulong)*(byte *)(param_4 + 0x1e) + *(long *)(param_4 + 8);
  }
  iVar1 = fstat(param_1,&local_d8);
  if (iVar1 != 0) {
    uVar5 = dcgettext(0,"%s: fstat failed",5);
    piVar3 = __errno_location();
    error(0,*piVar3,uVar5,param_2);
    local_121 = false;
    goto LAB_0010403f;
  }
  uVar2 = local_d8.st_mode & 0xf000;
  if (uVar2 == 0x2000) {
    iVar1 = isatty(param_1);
    if (iVar1 == 0) {
      uVar2 = local_d8.st_mode & 0xf000;
      goto LAB_00103efb;
    }
  }
  else {
LAB_00103efb:
    bVar8 = uVar2 == 0x1000 || uVar2 == 0xc000;
    if (uVar2 != 0x1000 && uVar2 != 0xc000) {
      if ((uVar2 == 0x8000) && (local_d8.st_size < 0)) {
        uVar5 = dcgettext(0,"%s: file has negative size",5);
        error(0,0,uVar5,param_2);
        local_121 = bVar8;
        goto LAB_0010403f;
      }
      piVar3 = (int *)xnmalloc();
      local_120 = *(long *)(param_4 + 0x10);
      lVar9 = local_120;
      if (local_120 == -1) {
        if ((local_d8.st_mode & 0xf000) == 0x8000) {
          local_120 = local_d8.st_size;
          lVar9 = local_120;
          if (*(char *)(param_4 + 0x1d) == '\0') {
            if (0x1fffffffffffffff < local_d8.st_blksize - 1U) {
              local_d8.st_blksize = 0x200;
            }
            if ((local_d8.st_size == 0) ||
               (local_148 = local_d8.st_size, local_d8.st_blksize <= local_d8.st_size)) {
              local_148 = 0;
            }
            if (local_d8.st_size % local_d8.st_blksize != 0) {
              local_d8.st_blksize = local_d8.st_blksize - local_d8.st_size % local_d8.st_blksize;
              local_120 = 0x7fffffffffffffff - local_d8.st_size;
              if (local_d8.st_blksize < 0x7fffffffffffffff - local_d8.st_size) {
                local_120 = local_d8.st_blksize;
              }
              local_120 = local_d8.st_size + local_120;
            }
            goto LAB_001040e3;
          }
        }
        else {
          lVar9 = lseek(param_1,0,2);
          if (lVar9 < 1) {
            lVar9 = local_120;
          }
        }
      }
      else if ((local_d8.st_mode & 0xf000) == 0x8000) {
        local_148 = local_d8.st_size;
        if (0x1fffffffffffffff < local_d8.st_blksize - 1U) {
          local_d8.st_blksize = 0x200;
        }
        if (local_120 < local_d8.st_blksize) {
          local_d8.st_blksize = local_120;
        }
        if (local_d8.st_size < local_d8.st_blksize) {
LAB_001040e3:
          local_130 = *(ulong *)(param_4 + 8);
          if (local_130 != 0) goto LAB_00103f84;
          goto LAB_001040f5;
        }
      }
      local_120 = lVar9;
      local_130 = *(ulong *)(param_4 + 8);
      if (local_130 == 0) {
        uVar5 = randint_get_source();
        local_121 = true;
        local_148 = local_120;
        goto LAB_001041ac;
      }
      local_148 = 0;
LAB_00103f84:
      iVar1 = -2;
      local_110 = 0;
      puVar11 = patterns;
      uVar10 = local_130;
      piVar13 = piVar3;
      do {
        if (iVar1 == 0) {
          iVar1 = -2;
          puVar11 = patterns + 4;
LAB_00103fc4:
          uVar14 = (ulong)-iVar1;
          if (uVar10 <= uVar14) {
            local_110 = local_110 + uVar10;
            goto LAB_0010428c;
          }
          local_110 = local_110 + uVar14;
        }
        else {
          __src = (int *)((long)puVar11 + 4);
          puVar11 = (undefined1 *)__src;
          if (iVar1 < 0) goto LAB_00103fc4;
          uVar14 = (ulong)iVar1;
          if (uVar10 < uVar14) goto LAB_001041ee;
          puVar11 = (undefined1 *)(__src + uVar14);
          pvVar4 = memcpy(piVar13,__src,uVar14 * 4);
          piVar13 = (int *)((long)pvVar4 + uVar14 * 4);
        }
        uVar10 = uVar10 - uVar14;
        iVar1 = *(int *)puVar11;
      } while( true );
    }
  }
  uVar5 = dcgettext(0,"%s: invalid file type",5);
  error(0,0,uVar5,param_2);
  local_121 = false;
  goto LAB_0010403f;
LAB_001041ee:
  if ((1 < uVar10) && (uVar14 <= uVar10 * 3)) {
    do {
      if ((uVar10 == uVar14) || (uVar6 = randint_genmax(param_3), piVar12 = piVar13, uVar6 < uVar10)
         ) {
        piVar12 = piVar13 + 1;
        *piVar13 = *__src;
        uVar10 = uVar10 - 1;
        if (uVar10 == 0) goto LAB_0010428c;
      }
      uVar14 = uVar14 - 1;
      piVar13 = piVar12;
      __src = __src + 1;
    } while( true );
  }
  local_110 = local_110 + uVar10;
LAB_0010428c:
  uVar6 = 0;
  lVar9 = local_130 - local_110;
  uVar14 = local_110 - 1;
  uVar10 = uVar14;
  do {
    while (uVar10 <= uVar14) {
      uVar10 = (uVar10 + (local_130 - 1)) - uVar14;
      piVar3[lVar9] = piVar3[uVar6];
      lVar9 = lVar9 + 1;
      piVar3[uVar6] = -1;
      uVar6 = uVar6 + 1;
      if (local_130 <= uVar6) goto LAB_001040f5;
    }
    uVar10 = uVar10 - uVar14;
    lVar7 = randint_genmax();
    iVar1 = piVar3[uVar6];
    lVar7 = lVar7 + uVar6;
    piVar3[uVar6] = piVar3[lVar7];
    uVar6 = uVar6 + 1;
    piVar3[lVar7] = iVar1;
  } while (uVar6 < local_130);
LAB_001040f5:
  uVar5 = randint_get_source();
  if (local_148 == 0) {
    local_148 = local_120;
    local_121 = true;
    goto LAB_001041ac;
  }
  local_121 = true;
  local_140 = 0;
  while( true ) {
    uVar10 = *(ulong *)(param_4 + 8);
    local_e0 = local_148;
    if (*(byte *)(param_4 + 0x1e) + uVar10 != 0) {
      uVar14 = 0;
      do {
        iVar1 = 0;
        if (uVar14 < uVar10) {
          iVar1 = piVar3[uVar14];
        }
        uVar14 = uVar14 + 1;
        iVar1 = dopass(param_1,&local_d8,param_2,&local_e0,iVar1,uVar5,uVar14,local_140);
        if (iVar1 != 0) {
          local_121 = bVar8;
          if (iVar1 < 0) goto LAB_001041df;
          local_121 = false;
        }
        uVar10 = *(ulong *)(param_4 + 8);
      } while (uVar14 < *(byte *)(param_4 + 0x1e) + uVar10);
    }
    local_148 = local_120;
LAB_001041ac:
    local_120 = 0;
    if (local_148 == 0) break;
    local_140 = local_118;
  }
  if (((*(int *)(param_4 + 0x18) != 0) && (iVar1 = ftruncate(param_1,0), iVar1 != 0)) &&
     ((local_d8.st_mode & 0xf000) == 0x8000)) {
    uVar5 = dcgettext(0,"%s: error truncating",5);
    piVar13 = __errno_location();
    error(0,*piVar13,uVar5,param_2);
    local_121 = bVar8;
  }
LAB_001041df:
  free(piVar3);
LAB_0010403f:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_121;
}