unsigned char do_wipefd(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r12_6;
    int32_t ebp7;
    void** rbx8;
    void* rsp9;
    void** v10;
    void** rax11;
    void** v12;
    int1_t zf13;
    void** r15_14;
    int64_t rdi15;
    int32_t eax16;
    void* rsp17;
    unsigned char v18;
    uint32_t eax19;
    uint32_t v20;
    int64_t rdi21;
    int32_t eax22;
    uint32_t v23;
    int64_t v24;
    void** rdi25;
    void** rsi26;
    void** rax27;
    void*** rsp28;
    void** v29;
    int1_t zf30;
    void** v31;
    uint32_t v32;
    int64_t rdi33;
    void** rax34;
    void** v35;
    int1_t zf36;
    void** rax37;
    void** v38;
    int64_t rax39;
    void*** rsp40;
    unsigned char v41;
    int64_t r9_42;
    void** v43;
    void** v44;
    void** rax45;
    void** rax46;
    void** v47;
    void** v48;
    void* rax49;
    uint32_t eax50;
    int64_t rax51;
    void** v52;
    void** rbx53;
    int64_t r13_54;
    void** r14_55;
    uint32_t eax56;
    void* rax57;
    int64_t rdi58;
    int32_t eax59;
    uint32_t v60;
    uint32_t eax61;
    void** rax62;
    void** rax63;
    void** rax64;
    int32_t v65;
    void** eax66;
    void** v67;
    void** r10_68;
    void* r13_69;
    void** rbp70;
    void** v71;
    void** rbx72;
    int64_t rax73;
    void** rax74;
    void** r14_75;
    void** r12_76;
    void*** rax77;
    int32_t ebp78;
    void** r12_79;
    void** rbx80;
    void* v81;
    void** v82;
    void** r13_83;
    int32_t v84;
    void** v85;
    signed char* r9_86;
    void** r14_87;
    uint64_t rdi88;
    void** v89;
    void** rbx90;
    uint64_t rbp91;
    uint64_t r12_92;
    void** r15_93;
    void** rax94;
    void*** rax95;
    void** r13_96;
    void** r14_97;
    int32_t v98;
    void** rbp99;
    void** v100;
    void** r12_101;
    void** v102;
    void** rbx103;
    void** v104;
    void** r15_105;
    void** rax106;

    r12_6 = rsi;
    ebp7 = edi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x118);
    v10 = rdx;
    rax11 = g28;
    v12 = rax11;
    zf13 = *reinterpret_cast<signed char*>(rcx + 28) == 0;
    if (!zf13) {
    }
    r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 0x70);
    *reinterpret_cast<int32_t*>(&rdi15) = ebp7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
    eax16 = fun_29a0(rdi15, r15_14);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    if (eax16) {
        fun_2630();
        fun_2540();
        fun_2890();
        v18 = 0;
    } else {
        eax19 = v20 & 0xf000;
        if (eax19 == 0x2000) {
            *reinterpret_cast<int32_t*>(&rdi21) = ebp7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
            eax22 = fun_25a0(rdi21, r15_14);
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            if (eax22) 
                goto addr_4016_7;
            eax19 = v23 & 0xf000;
        }
        *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(eax19 == 0xc000);
        *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax19 == "2.28")) | *reinterpret_cast<unsigned char*>(&rdx));
        v18 = *reinterpret_cast<unsigned char*>(&rcx);
        if (*reinterpret_cast<unsigned char*>(&rcx)) {
            addr_4016_7:
            fun_2630();
            fun_2890();
            v18 = 0;
        } else {
            if (eax19 != 0x8000 || v24 >= 0) {
                rdi25 = *reinterpret_cast<void***>(rbx8 + 8);
                rsi26 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                rax27 = xnmalloc(rdi25, 4);
                rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                v29 = rax27;
                zf30 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 16) == 0xffffffffffffffff);
                v31 = *reinterpret_cast<void***>(rbx8 + 16);
                if (zf30) {
                    if ((v32 & 0xf000) != 0x8000) {
                        rsi26 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdx) = 2;
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi33) = ebp7;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
                        rax34 = fun_26d0(rdi33, rdi33);
                        rsp28 = rsp28 - 8 + 8;
                        if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax34) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax34 == 0)) {
                            rax34 = v31;
                        }
                        v31 = rax34;
                        goto addr_3f6a_16;
                    }
                    rsi26 = v35;
                    zf36 = *reinterpret_cast<signed char*>(rbx8 + 29) == 0;
                    v31 = rsi26;
                    if (!zf36) {
                        addr_3f6a_16:
                        rax37 = *reinterpret_cast<void***>(rbx8 + 8);
                        v38 = rax37;
                        if (!rax37) {
                            rax39 = randint_get_source(v10, rsi26, rdx);
                            rsp40 = rsp28 - 8 + 8;
                            v41 = 1;
                            r9_42 = rax39;
                            v43 = v31;
                            goto addr_41ac_19;
                        } else {
                            v43 = reinterpret_cast<void**>(0);
                            goto addr_3f84_21;
                        }
                    } else {
                        rcx = v44;
                        if (reinterpret_cast<uint64_t>(rcx + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                            rcx = reinterpret_cast<void**>(0x200);
                        }
                        rdx = reinterpret_cast<void**>(reinterpret_cast<signed char>(rsi26) % reinterpret_cast<signed char>(rcx));
                        if (!rsi26 || (v43 = rsi26, reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(rsi26))) {
                            v43 = reinterpret_cast<void**>(0);
                        }
                        if (rdx) {
                            rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) - reinterpret_cast<unsigned char>(rdx));
                            rax45 = reinterpret_cast<void**>(0x7fffffffffffffff - reinterpret_cast<unsigned char>(v31));
                            if (reinterpret_cast<signed char>(rax45) > reinterpret_cast<signed char>(rcx)) {
                                rax45 = rcx;
                            }
                            v31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) + reinterpret_cast<unsigned char>(rax45));
                            goto addr_40e3_30;
                        }
                    }
                } else {
                    if ((v32 & 0xf000) != 0x8000) 
                        goto addr_3f6a_16;
                    rax46 = v47;
                    v43 = v48;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x200;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rcx = v31;
                    if (reinterpret_cast<uint64_t>(rax46 + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                        rax46 = reinterpret_cast<void**>(0x200);
                    }
                    if (reinterpret_cast<signed char>(rax46) > reinterpret_cast<signed char>(rcx)) {
                        rax46 = rcx;
                    }
                    if (reinterpret_cast<signed char>(v48) >= reinterpret_cast<signed char>(rax46)) 
                        goto addr_3f6a_16; else 
                        goto addr_40e3_30;
                }
            } else {
                fun_2630();
                fun_2890();
            }
        }
    }
    addr_403f_38:
    rax49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rax49) {
        fun_2660();
    } else {
        eax50 = v18;
        return *reinterpret_cast<unsigned char*>(&eax50);
    }
    addr_41ac_19:
    while (v31 = reinterpret_cast<void**>(0), !!v43) {
        addr_411b_43:
        rdx = *reinterpret_cast<void***>(rbx8 + 8);
        *reinterpret_cast<uint32_t*>(&rax51) = *reinterpret_cast<unsigned char*>(rbx8 + 30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        if (rax51 + reinterpret_cast<unsigned char>(rdx)) {
            v52 = reinterpret_cast<void**>(rsp40 + 0x68);
            rbx53 = reinterpret_cast<void**>(0);
            r13_54 = r9_42;
            r14_55 = rbx8;
            do {
                r8 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rbx53)) {
                    r8 = *reinterpret_cast<void***>(v29 + reinterpret_cast<unsigned char>(rbx53) * 4);
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                }
                ++rbx53;
                rsi26 = r15_14;
                eax56 = dopass(ebp7, rsi26, r12_6, v52, r8, r13_54, rbx53, v52);
                rdx = rbx53;
                rcx = v52;
                rsp40 = rsp40 - 8 - 8 - 8 + 8 + 8 + 8;
                if (eax56) {
                    if (reinterpret_cast<int32_t>(eax56) < reinterpret_cast<int32_t>(0)) 
                        goto addr_41df_49;
                    v41 = 0;
                }
                rdx = *reinterpret_cast<void***>(r14_55 + 8);
                *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<unsigned char*>(r14_55 + 30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx53) < reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(rax57) + reinterpret_cast<unsigned char>(rdx)));
            r9_42 = r13_54;
            rbx8 = r14_55;
        }
        v43 = v31;
    }
    if (!*reinterpret_cast<void***>(rbx8 + 24) || ((rsi26 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi58) = ebp7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi58) + 4) = 0, eax59 = fun_26c0(rdi58), eax59 == 0) || (v60 & 0xf000) != 0x8000)) {
        eax61 = v41;
        v18 = *reinterpret_cast<unsigned char*>(&eax61);
    } else {
        rax62 = fun_2630();
        rax63 = fun_2540();
        rcx = r12_6;
        rdx = rax62;
        rsi26 = *reinterpret_cast<void***>(rax63);
        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
        fun_2890();
    }
    addr_41df_49:
    fun_2520(v29, rsi26, rdx, rcx, r8, v29, rsi26, rdx, rcx, r8);
    goto addr_403f_38;
    addr_40e3_30:
    rax64 = *reinterpret_cast<void***>(rbx8 + 8);
    v38 = rax64;
    if (rax64) {
        addr_3f84_21:
        r8 = reinterpret_cast<void**>(0xbc80);
        v65 = ebp7;
        eax66 = reinterpret_cast<void**>(0xfffffffe);
        v67 = rbx8;
        r10_68 = v29;
        *reinterpret_cast<int32_t*>(&r13_69) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_69) + 4) = 0;
        rbp70 = reinterpret_cast<void**>(0xbc80);
        v71 = r12_6;
        rbx72 = v38;
    } else {
        addr_40f5_57:
        rax73 = randint_get_source(v10, rsi26, rdx);
        rsp40 = rsp28 - 8 + 8;
        r9_42 = rax73;
        if (!v43) {
            v41 = 1;
            v43 = v31;
            goto addr_41ac_19;
        } else {
            v41 = 1;
            goto addr_411b_43;
        }
    }
    while (1) {
        if (!eax66) {
            eax66 = reinterpret_cast<void**>(0xfffffffe);
            rbp70 = reinterpret_cast<void**>(0xbc84);
            goto addr_3fc4_62;
        }
        rbp70 = rbp70 + 4;
        if (reinterpret_cast<signed char>(eax66) < reinterpret_cast<signed char>(0)) {
            addr_3fc4_62:
            rax74 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(-reinterpret_cast<unsigned char>(eax66))));
            if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(rbx72)) 
                break;
        } else {
            r14_75 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax66)));
            if (reinterpret_cast<unsigned char>(r14_75) > reinterpret_cast<unsigned char>(rbx72)) 
                goto addr_41ee_65;
            r12_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_75) * 4);
            rsi26 = rbp70;
            rbx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx72) - reinterpret_cast<unsigned char>(r14_75));
            rdx = r12_76;
            rbp70 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp70) + reinterpret_cast<unsigned char>(r12_76));
            rax77 = fun_2780(r10_68, rsi26, rdx, rcx, 0xbc80);
            rsp28 = rsp28 - 8 + 8;
            r10_68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax77) + reinterpret_cast<unsigned char>(r12_76));
            goto addr_3fd7_67;
        }
        r13_69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_69) + reinterpret_cast<unsigned char>(rax74));
        rbx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx72) - reinterpret_cast<unsigned char>(rax74));
        addr_3fd7_67:
        eax66 = *reinterpret_cast<void***>(rbp70);
    }
    ebp78 = v65;
    r12_79 = v71;
    rbx80 = v67;
    v81 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_69) + reinterpret_cast<unsigned char>(rbx72));
    addr_428c_70:
    v82 = r12_79;
    *reinterpret_cast<int32_t*>(&r13_83) = 0;
    *reinterpret_cast<int32_t*>(&r13_83 + 4) = 0;
    v84 = ebp78;
    v85 = rbx80;
    r9_86 = reinterpret_cast<signed char*>(v38 + 0xffffffffffffffff);
    r14_87 = v38;
    rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v38) - reinterpret_cast<uint64_t>(v81));
    rdi88 = reinterpret_cast<uint64_t>(v81) - 1;
    v89 = r15_14;
    rbx90 = v29;
    rbp91 = rdi88;
    r12_92 = rdi88;
    r15_93 = rcx;
    while (1) {
        if (rbp91 >= r12_92) {
            r12_92 = r12_92 + reinterpret_cast<uint64_t>(r9_86) - rbp91;
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r15_93) * 4) = *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4);
            ++r15_93;
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4) = reinterpret_cast<void**>(0xffffffff);
            ++r13_83;
            if (reinterpret_cast<unsigned char>(r13_83) >= reinterpret_cast<unsigned char>(r14_87)) 
                break;
        } else {
            r12_92 = r12_92 - rbp91;
            rax94 = randint_genmax(v10, reinterpret_cast<uint64_t>(r15_93 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(r13_83), rdx);
            rsp28 = rsp28 - 8 + 8;
            rsi26 = *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4);
            *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
            r9_86 = r9_86;
            rax95 = reinterpret_cast<void***>(rbx90 + (reinterpret_cast<unsigned char>(rax94) + reinterpret_cast<unsigned char>(r13_83)) * 4);
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4) = *rax95;
            ++r13_83;
            *rax95 = rsi26;
            if (reinterpret_cast<unsigned char>(r13_83) >= reinterpret_cast<unsigned char>(r14_87)) 
                break;
        }
    }
    ebp7 = v84;
    r12_6 = v82;
    rbx8 = v85;
    r15_14 = v89;
    goto addr_40f5_57;
    addr_41ee_65:
    v81 = r13_69;
    r13_96 = r14_75;
    r14_97 = rbx72;
    r8 = rbp70;
    r12_79 = v71;
    ebp78 = v65;
    rbx80 = v67;
    if (reinterpret_cast<unsigned char>(r14_97) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r13_96) > reinterpret_cast<unsigned char>(r14_97 + reinterpret_cast<unsigned char>(r14_97) * 2)) {
        v81 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v81) + reinterpret_cast<unsigned char>(r14_97));
        goto addr_428c_70;
    } else {
        v98 = ebp78;
        rbp99 = r10_68;
        v100 = r12_79;
        r12_101 = r8;
        v102 = rbx80;
        rbx103 = v10;
        v104 = r15_14;
        while (1) {
            r12_101 = r12_101 + 4;
            if (r14_97 == r13_96 || (r15_105 = r13_96 + 0xffffffffffffffff, rsi26 = r15_105, rax106 = randint_genmax(rbx103, rsi26, rdx), rsp28 = rsp28 - 8 + 8, reinterpret_cast<unsigned char>(r14_97) > reinterpret_cast<unsigned char>(rax106))) {
                *reinterpret_cast<void***>(rbp99) = *reinterpret_cast<void***>(r12_101 + 0xfffffffffffffffc);
                --r14_97;
                if (!r14_97) 
                    break;
                rbp99 = rbp99 + 4;
                r15_105 = r13_96 + 0xffffffffffffffff;
            }
            r13_96 = r15_105;
        }
        ebp78 = v98;
        r12_79 = v100;
        rbx80 = v102;
        r15_14 = v104;
        goto addr_428c_70;
    }
}