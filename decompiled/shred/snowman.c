
void** g28;

int32_t fun_29a0(int64_t rdi, void** rsi);

void** fun_2630();

void** fun_2540();

void fun_2890();

int32_t fun_25a0(int64_t rdi, void** rsi);

void** xnmalloc(void** rdi, void** rsi);

void** fun_26d0(int64_t rdi, ...);

int64_t randint_get_source(void** rdi, void** rsi, void** rdx);

void* fun_2660();

uint32_t dopass(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, void** a7, void** a8);

int32_t fun_26c0(int64_t rdi);

void fun_2520(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void*** fun_2780(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** randint_genmax(void** rdi, void** rsi, void** rdx);

unsigned char do_wipefd(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r12_6;
    int32_t ebp7;
    void** rbx8;
    void* rsp9;
    void** v10;
    void** rax11;
    void** v12;
    int1_t zf13;
    void** r15_14;
    int64_t rdi15;
    int32_t eax16;
    void* rsp17;
    unsigned char v18;
    uint32_t eax19;
    uint32_t v20;
    int64_t rdi21;
    int32_t eax22;
    uint32_t v23;
    int64_t v24;
    void** rdi25;
    void** rsi26;
    void** rax27;
    void*** rsp28;
    void** v29;
    int1_t zf30;
    void** v31;
    uint32_t v32;
    int64_t rdi33;
    void** rax34;
    void** v35;
    int1_t zf36;
    void** rax37;
    void** v38;
    int64_t rax39;
    void*** rsp40;
    unsigned char v41;
    int64_t r9_42;
    void** v43;
    void** v44;
    void** rax45;
    void** rax46;
    void** v47;
    void** v48;
    void* rax49;
    uint32_t eax50;
    int64_t rax51;
    void** v52;
    void** rbx53;
    int64_t r13_54;
    void** r14_55;
    uint32_t eax56;
    void* rax57;
    int64_t rdi58;
    int32_t eax59;
    uint32_t v60;
    uint32_t eax61;
    void** rax62;
    void** rax63;
    void** rax64;
    int32_t v65;
    void** eax66;
    void** v67;
    void** r10_68;
    void* r13_69;
    void** rbp70;
    void** v71;
    void** rbx72;
    int64_t rax73;
    void** rax74;
    void** r14_75;
    void** r12_76;
    void*** rax77;
    int32_t ebp78;
    void** r12_79;
    void** rbx80;
    void* v81;
    void** v82;
    void** r13_83;
    int32_t v84;
    void** v85;
    signed char* r9_86;
    void** r14_87;
    uint64_t rdi88;
    void** v89;
    void** rbx90;
    uint64_t rbp91;
    uint64_t r12_92;
    void** r15_93;
    void** rax94;
    void*** rax95;
    void** r13_96;
    void** r14_97;
    int32_t v98;
    void** rbp99;
    void** v100;
    void** r12_101;
    void** v102;
    void** rbx103;
    void** v104;
    void** r15_105;
    void** rax106;

    r12_6 = rsi;
    ebp7 = edi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x118);
    v10 = rdx;
    rax11 = g28;
    v12 = rax11;
    zf13 = *reinterpret_cast<signed char*>(rcx + 28) == 0;
    if (!zf13) {
    }
    r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 0x70);
    *reinterpret_cast<int32_t*>(&rdi15) = ebp7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
    eax16 = fun_29a0(rdi15, r15_14);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    if (eax16) {
        fun_2630();
        fun_2540();
        fun_2890();
        v18 = 0;
    } else {
        eax19 = v20 & 0xf000;
        if (eax19 == 0x2000) {
            *reinterpret_cast<int32_t*>(&rdi21) = ebp7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
            eax22 = fun_25a0(rdi21, r15_14);
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            if (eax22) 
                goto addr_4016_7;
            eax19 = v23 & 0xf000;
        }
        *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(eax19 == 0xc000);
        *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax19 == "2.28")) | *reinterpret_cast<unsigned char*>(&rdx));
        v18 = *reinterpret_cast<unsigned char*>(&rcx);
        if (*reinterpret_cast<unsigned char*>(&rcx)) {
            addr_4016_7:
            fun_2630();
            fun_2890();
            v18 = 0;
        } else {
            if (eax19 != 0x8000 || v24 >= 0) {
                rdi25 = *reinterpret_cast<void***>(rbx8 + 8);
                rsi26 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                rax27 = xnmalloc(rdi25, 4);
                rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                v29 = rax27;
                zf30 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 16) == 0xffffffffffffffff);
                v31 = *reinterpret_cast<void***>(rbx8 + 16);
                if (zf30) {
                    if ((v32 & 0xf000) != 0x8000) {
                        rsi26 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdx) = 2;
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi33) = ebp7;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
                        rax34 = fun_26d0(rdi33, rdi33);
                        rsp28 = rsp28 - 8 + 8;
                        if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax34) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax34 == 0)) {
                            rax34 = v31;
                        }
                        v31 = rax34;
                        goto addr_3f6a_16;
                    }
                    rsi26 = v35;
                    zf36 = *reinterpret_cast<signed char*>(rbx8 + 29) == 0;
                    v31 = rsi26;
                    if (!zf36) {
                        addr_3f6a_16:
                        rax37 = *reinterpret_cast<void***>(rbx8 + 8);
                        v38 = rax37;
                        if (!rax37) {
                            rax39 = randint_get_source(v10, rsi26, rdx);
                            rsp40 = rsp28 - 8 + 8;
                            v41 = 1;
                            r9_42 = rax39;
                            v43 = v31;
                            goto addr_41ac_19;
                        } else {
                            v43 = reinterpret_cast<void**>(0);
                            goto addr_3f84_21;
                        }
                    } else {
                        rcx = v44;
                        if (reinterpret_cast<uint64_t>(rcx + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                            rcx = reinterpret_cast<void**>(0x200);
                        }
                        rdx = reinterpret_cast<void**>(reinterpret_cast<signed char>(rsi26) % reinterpret_cast<signed char>(rcx));
                        if (!rsi26 || (v43 = rsi26, reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(rsi26))) {
                            v43 = reinterpret_cast<void**>(0);
                        }
                        if (rdx) {
                            rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) - reinterpret_cast<unsigned char>(rdx));
                            rax45 = reinterpret_cast<void**>(0x7fffffffffffffff - reinterpret_cast<unsigned char>(v31));
                            if (reinterpret_cast<signed char>(rax45) > reinterpret_cast<signed char>(rcx)) {
                                rax45 = rcx;
                            }
                            v31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) + reinterpret_cast<unsigned char>(rax45));
                            goto addr_40e3_30;
                        }
                    }
                } else {
                    if ((v32 & 0xf000) != 0x8000) 
                        goto addr_3f6a_16;
                    rax46 = v47;
                    v43 = v48;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x200;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rcx = v31;
                    if (reinterpret_cast<uint64_t>(rax46 + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                        rax46 = reinterpret_cast<void**>(0x200);
                    }
                    if (reinterpret_cast<signed char>(rax46) > reinterpret_cast<signed char>(rcx)) {
                        rax46 = rcx;
                    }
                    if (reinterpret_cast<signed char>(v48) >= reinterpret_cast<signed char>(rax46)) 
                        goto addr_3f6a_16; else 
                        goto addr_40e3_30;
                }
            } else {
                fun_2630();
                fun_2890();
            }
        }
    }
    addr_403f_38:
    rax49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rax49) {
        fun_2660();
    } else {
        eax50 = v18;
        return *reinterpret_cast<unsigned char*>(&eax50);
    }
    addr_41ac_19:
    while (v31 = reinterpret_cast<void**>(0), !!v43) {
        addr_411b_43:
        rdx = *reinterpret_cast<void***>(rbx8 + 8);
        *reinterpret_cast<uint32_t*>(&rax51) = *reinterpret_cast<unsigned char*>(rbx8 + 30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        if (rax51 + reinterpret_cast<unsigned char>(rdx)) {
            v52 = reinterpret_cast<void**>(rsp40 + 0x68);
            rbx53 = reinterpret_cast<void**>(0);
            r13_54 = r9_42;
            r14_55 = rbx8;
            do {
                r8 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rbx53)) {
                    r8 = *reinterpret_cast<void***>(v29 + reinterpret_cast<unsigned char>(rbx53) * 4);
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                }
                ++rbx53;
                rsi26 = r15_14;
                eax56 = dopass(ebp7, rsi26, r12_6, v52, r8, r13_54, rbx53, v52);
                rdx = rbx53;
                rcx = v52;
                rsp40 = rsp40 - 8 - 8 - 8 + 8 + 8 + 8;
                if (eax56) {
                    if (reinterpret_cast<int32_t>(eax56) < reinterpret_cast<int32_t>(0)) 
                        goto addr_41df_49;
                    v41 = 0;
                }
                rdx = *reinterpret_cast<void***>(r14_55 + 8);
                *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<unsigned char*>(r14_55 + 30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx53) < reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(rax57) + reinterpret_cast<unsigned char>(rdx)));
            r9_42 = r13_54;
            rbx8 = r14_55;
        }
        v43 = v31;
    }
    if (!*reinterpret_cast<void***>(rbx8 + 24) || ((rsi26 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi58) = ebp7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi58) + 4) = 0, eax59 = fun_26c0(rdi58), eax59 == 0) || (v60 & 0xf000) != 0x8000)) {
        eax61 = v41;
        v18 = *reinterpret_cast<unsigned char*>(&eax61);
    } else {
        rax62 = fun_2630();
        rax63 = fun_2540();
        rcx = r12_6;
        rdx = rax62;
        rsi26 = *reinterpret_cast<void***>(rax63);
        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
        fun_2890();
    }
    addr_41df_49:
    fun_2520(v29, rsi26, rdx, rcx, r8, v29, rsi26, rdx, rcx, r8);
    goto addr_403f_38;
    addr_40e3_30:
    rax64 = *reinterpret_cast<void***>(rbx8 + 8);
    v38 = rax64;
    if (rax64) {
        addr_3f84_21:
        r8 = reinterpret_cast<void**>(0xbc80);
        v65 = ebp7;
        eax66 = reinterpret_cast<void**>(0xfffffffe);
        v67 = rbx8;
        r10_68 = v29;
        *reinterpret_cast<int32_t*>(&r13_69) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_69) + 4) = 0;
        rbp70 = reinterpret_cast<void**>(0xbc80);
        v71 = r12_6;
        rbx72 = v38;
    } else {
        addr_40f5_57:
        rax73 = randint_get_source(v10, rsi26, rdx);
        rsp40 = rsp28 - 8 + 8;
        r9_42 = rax73;
        if (!v43) {
            v41 = 1;
            v43 = v31;
            goto addr_41ac_19;
        } else {
            v41 = 1;
            goto addr_411b_43;
        }
    }
    while (1) {
        if (!eax66) {
            eax66 = reinterpret_cast<void**>(0xfffffffe);
            rbp70 = reinterpret_cast<void**>(0xbc84);
            goto addr_3fc4_62;
        }
        rbp70 = rbp70 + 4;
        if (reinterpret_cast<signed char>(eax66) < reinterpret_cast<signed char>(0)) {
            addr_3fc4_62:
            rax74 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(-reinterpret_cast<unsigned char>(eax66))));
            if (reinterpret_cast<unsigned char>(rax74) >= reinterpret_cast<unsigned char>(rbx72)) 
                break;
        } else {
            r14_75 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax66)));
            if (reinterpret_cast<unsigned char>(r14_75) > reinterpret_cast<unsigned char>(rbx72)) 
                goto addr_41ee_65;
            r12_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_75) * 4);
            rsi26 = rbp70;
            rbx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx72) - reinterpret_cast<unsigned char>(r14_75));
            rdx = r12_76;
            rbp70 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp70) + reinterpret_cast<unsigned char>(r12_76));
            rax77 = fun_2780(r10_68, rsi26, rdx, rcx, 0xbc80);
            rsp28 = rsp28 - 8 + 8;
            r10_68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax77) + reinterpret_cast<unsigned char>(r12_76));
            goto addr_3fd7_67;
        }
        r13_69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_69) + reinterpret_cast<unsigned char>(rax74));
        rbx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx72) - reinterpret_cast<unsigned char>(rax74));
        addr_3fd7_67:
        eax66 = *reinterpret_cast<void***>(rbp70);
    }
    ebp78 = v65;
    r12_79 = v71;
    rbx80 = v67;
    v81 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_69) + reinterpret_cast<unsigned char>(rbx72));
    addr_428c_70:
    v82 = r12_79;
    *reinterpret_cast<int32_t*>(&r13_83) = 0;
    *reinterpret_cast<int32_t*>(&r13_83 + 4) = 0;
    v84 = ebp78;
    v85 = rbx80;
    r9_86 = reinterpret_cast<signed char*>(v38 + 0xffffffffffffffff);
    r14_87 = v38;
    rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v38) - reinterpret_cast<uint64_t>(v81));
    rdi88 = reinterpret_cast<uint64_t>(v81) - 1;
    v89 = r15_14;
    rbx90 = v29;
    rbp91 = rdi88;
    r12_92 = rdi88;
    r15_93 = rcx;
    while (1) {
        if (rbp91 >= r12_92) {
            r12_92 = r12_92 + reinterpret_cast<uint64_t>(r9_86) - rbp91;
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r15_93) * 4) = *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4);
            ++r15_93;
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4) = reinterpret_cast<void**>(0xffffffff);
            ++r13_83;
            if (reinterpret_cast<unsigned char>(r13_83) >= reinterpret_cast<unsigned char>(r14_87)) 
                break;
        } else {
            r12_92 = r12_92 - rbp91;
            rax94 = randint_genmax(v10, reinterpret_cast<uint64_t>(r15_93 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(r13_83), rdx);
            rsp28 = rsp28 - 8 + 8;
            rsi26 = *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4);
            *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
            r9_86 = r9_86;
            rax95 = reinterpret_cast<void***>(rbx90 + (reinterpret_cast<unsigned char>(rax94) + reinterpret_cast<unsigned char>(r13_83)) * 4);
            *reinterpret_cast<void***>(rbx90 + reinterpret_cast<unsigned char>(r13_83) * 4) = *rax95;
            ++r13_83;
            *rax95 = rsi26;
            if (reinterpret_cast<unsigned char>(r13_83) >= reinterpret_cast<unsigned char>(r14_87)) 
                break;
        }
    }
    ebp7 = v84;
    r12_6 = v82;
    rbx8 = v85;
    r15_14 = v89;
    goto addr_40f5_57;
    addr_41ee_65:
    v81 = r13_69;
    r13_96 = r14_75;
    r14_97 = rbx72;
    r8 = rbp70;
    r12_79 = v71;
    ebp78 = v65;
    rbx80 = v67;
    if (reinterpret_cast<unsigned char>(r14_97) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r13_96) > reinterpret_cast<unsigned char>(r14_97 + reinterpret_cast<unsigned char>(r14_97) * 2)) {
        v81 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v81) + reinterpret_cast<unsigned char>(r14_97));
        goto addr_428c_70;
    } else {
        v98 = ebp78;
        rbp99 = r10_68;
        v100 = r12_79;
        r12_101 = r8;
        v102 = rbx80;
        rbx103 = v10;
        v104 = r15_14;
        while (1) {
            r12_101 = r12_101 + 4;
            if (r14_97 == r13_96 || (r15_105 = r13_96 + 0xffffffffffffffff, rsi26 = r15_105, rax106 = randint_genmax(rbx103, rsi26, rdx), rsp28 = rsp28 - 8 + 8, reinterpret_cast<unsigned char>(r14_97) > reinterpret_cast<unsigned char>(rax106))) {
                *reinterpret_cast<void***>(rbp99) = *reinterpret_cast<void***>(r12_101 + 0xfffffffffffffffc);
                --r14_97;
                if (!r14_97) 
                    break;
                rbp99 = rbp99 + 4;
                r15_105 = r13_96 + 0xffffffffffffffff;
            }
            r13_96 = r15_105;
        }
        ebp78 = v98;
        r12_79 = v100;
        rbx80 = v102;
        r15_14 = v104;
        goto addr_428c_70;
    }
}

int32_t fun_28a0(int64_t rdi);

void fun_27b0(int64_t rdi);

/* dosync.part.0 */
int32_t dosync_part_0(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    void** rax6;
    int64_t r12_7;
    int64_t rdi8;
    int32_t eax9;
    int64_t rbp10;

    rax6 = fun_2540();
    *reinterpret_cast<void***>(&r12_7) = *reinterpret_cast<void***>(rax6);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
    if (*reinterpret_cast<void***>(&r12_7) == 9 || static_cast<uint32_t>(r12_7 - 21) <= 1) {
        *reinterpret_cast<int32_t*>(&rdi8) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        eax9 = fun_28a0(rdi8);
        if (eax9) {
            *reinterpret_cast<void***>(&rbp10) = *reinterpret_cast<void***>(rax6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp10) + 4) = 0;
            if (*reinterpret_cast<void***>(&rbp10) == 9 || static_cast<uint32_t>(rbp10 - 21) <= 1) {
                fun_27b0(rdi8);
                eax9 = 0;
            } else {
                fun_2630();
                fun_2890();
                *reinterpret_cast<void***>(rax6) = *reinterpret_cast<void***>(&rbp10);
                eax9 = -1;
            }
        }
    } else {
        fun_2630();
        fun_2890();
        *reinterpret_cast<void***>(rax6) = *reinterpret_cast<void***>(&r12_7);
        eax9 = -1;
    }
    return eax9;
}

int64_t fun_2640();

int64_t fun_2530(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2640();
    if (r8d > 10) {
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xbf20 + rax11 * 4) + 0xbf20;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_26f0(void** rdi, ...);

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    int64_t rax15;
    void** r8_16;
    struct s1* rbx17;
    uint32_t r15d18;
    void** rsi19;
    void** r14_20;
    int64_t v21;
    int64_t v22;
    uint32_t r15d23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x773f;
    rax8 = fun_2540();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
        fun_2530(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x10090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x87b1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            v7 = 0x77cb;
            fun_26f0((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            rax15 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax15);
        }
        *reinterpret_cast<uint32_t*>(&r8_16) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_16 + 4) = 0;
        rbx17 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d18 = rcx->f4;
        rsi19 = rbx17->f0;
        r14_20 = rbx17->f8;
        v21 = rcx->f30;
        v22 = rcx->f28;
        r15d23 = r15d18 | 1;
        rax24 = quotearg_buffer_restyled(r14_20, rsi19, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_16), r15d23, &rcx->f8, v22, v21, v7);
        if (reinterpret_cast<unsigned char>(rsi19) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx17->f0 = rsi25;
            if (r14_20 != 0x10120) {
                fun_2520(r14_20, rsi25, rsi, rdx, r8_16, r14_20, rsi25, rsi, rdx, r8_16);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx17->f8 = rax26;
            v28 = rcx->f30;
            r14_20 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d23, rsi25, v29, v28, 0x785a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2660();
        } else {
            return r14_20;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x100a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

int32_t fun_2920();

void** xalignalloc(int64_t rdi, int64_t rsi);

int32_t rpl_fcntl(int64_t rdi, void** rsi, ...);

int32_t fun_2700(int64_t rdi);

int64_t fun_2790();

void randread(int64_t rdi, void** rsi, void** rdx);

void* fun_25e0(int64_t rdi);

void** umaxtostr(void* rdi, void* rsi);

void** human_readable(void** rdi, void* rsi, void** rdx, void** rcx, void** r8, ...);

int32_t fun_2750(void** rdi, void** rsi, void** rdx, ...);

void fun_2820(void** rdi);

int32_t fun_2550(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void fun_29d0(void** rdi, void** rsi, int64_t rdx, void** rcx, ...);

uint32_t dopass(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, void** a7, void** a8) {
    int64_t r12_9;
    void** r14_10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;
    void** rax15;
    void** v16;
    int32_t eax17;
    uint32_t ebx18;
    uint32_t edx19;
    void** v20;
    int64_t rsi21;
    void** rbx22;
    void** rax23;
    void* rsp24;
    void** rbp25;
    unsigned char dl26;
    unsigned char v27;
    int64_t rdi28;
    int32_t eax29;
    int64_t rdi30;
    void** rdx31;
    int64_t rdi32;
    void** rsi33;
    void** v34;
    int32_t eax35;
    void** eax36;
    int64_t rdi37;
    void** rax38;
    uint1_t zf39;
    void** rax40;
    void* rsp41;
    void** rbx42;
    uint32_t eax43;
    uint32_t eax44;
    uint32_t edx45;
    void** r13_46;
    void** rax47;
    void** rax48;
    void* rsp49;
    void** rdi50;
    int64_t v51;
    void** rax52;
    void* rsp53;
    int64_t rax54;
    unsigned char v55;
    void** v56;
    int64_t v57;
    void** v58;
    void** r15_59;
    int64_t rdi60;
    void* rax61;
    void** rax62;
    void** r13_63;
    void** rax64;
    void** rax65;
    void** rax66;
    void** r9_67;
    int64_t rdi68;
    void** rax69;
    int64_t rdi70;
    int32_t eax71;
    int64_t rdi72;
    void** rax73;
    int64_t rax74;
    int64_t rax75;
    void* rsi76;
    void** rax77;
    int32_t eax78;
    void* rsi79;
    void** rax80;
    void* rsp81;
    void* rsp82;
    void** rax83;
    int64_t rdi84;
    int32_t eax85;
    int32_t eax86;
    void** rax87;
    void** rax88;
    void** rax89;
    void** rax90;
    void** rax91;
    int64_t rdi92;
    int32_t eax93;
    void* rsp94;
    int32_t eax95;
    void* rsp96;
    uint32_t eax97;
    void** rax98;
    void** rdx99;
    void** r12_100;
    int32_t ebp101;
    void** rbx102;
    void* rsp103;
    void** v104;
    void** rax105;
    void** v106;
    int1_t zf107;
    void** r15_108;
    int64_t rdi109;
    int32_t eax110;
    void* rsp111;
    uint32_t eax112;
    uint32_t v113;
    int64_t rdi114;
    int32_t eax115;
    uint32_t v116;
    int64_t v117;
    void** rdi118;
    void** rsi119;
    void** rax120;
    void*** rsp121;
    void** v122;
    int1_t zf123;
    void** v124;
    uint32_t v125;
    int64_t rdi126;
    void** rax127;
    void** v128;
    int1_t zf129;
    void** rax130;
    void** v131;
    int64_t rax132;
    void*** rsp133;
    int64_t r9_134;
    void** v135;
    void** v136;
    void** rax137;
    void** rax138;
    void** v139;
    void** v140;
    void* rax141;
    int64_t v142;
    int64_t rax143;
    void** v144;
    void** rbx145;
    int64_t r13_146;
    void** r14_147;
    uint32_t eax148;
    void* rax149;
    int64_t rdi150;
    int32_t eax151;
    uint32_t v152;
    void** rax153;
    void** rax154;
    void** rax155;
    int32_t v156;
    void** eax157;
    void** v158;
    void** r10_159;
    void* r13_160;
    void** rbp161;
    void** v162;
    void** rbx163;
    int64_t rax164;
    void** rax165;
    void** r14_166;
    void** r12_167;
    void*** rax168;
    int32_t ebp169;
    void** r12_170;
    void** rbx171;
    void* v172;
    void** v173;
    void** r13_174;
    int32_t v175;
    void** v176;
    signed char* r9_177;
    void** r14_178;
    uint64_t rdi179;
    void** v180;
    void** rbx181;
    uint64_t rbp182;
    uint64_t r12_183;
    void** r15_184;
    void** rax185;
    void*** rax186;
    void** r13_187;
    void** r14_188;
    int32_t v189;
    void** rbp190;
    void** v191;
    void** r12_192;
    void** v193;
    void** rbx194;
    void** v195;
    void** r15_196;
    void** rax197;
    void** rdi198;
    void** rdx199;
    void** rdi200;
    void** rax201;

    *reinterpret_cast<int32_t*>(&r12_9) = edi;
    r14_10 = *reinterpret_cast<void***>(rcx);
    v11 = rdx;
    v12 = rcx;
    v13 = r8;
    v14 = r9;
    rax15 = g28;
    v16 = rax15;
    eax17 = fun_2920();
    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8 == 0) || (ebx18 = reinterpret_cast<unsigned char>(r8) & reinterpret_cast<uint32_t>("_2.28"), edx19 = ebx18 | ebx18 << 12, *reinterpret_cast<uint32_t*>(&rcx) = edx19 >> 4, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&edx19) + 1) == *reinterpret_cast<unsigned char*>(&rcx)) && *reinterpret_cast<unsigned char*>(&rcx) == *reinterpret_cast<unsigned char*>(&edx19)) {
        v20 = reinterpret_cast<void**>(0x10000);
        *reinterpret_cast<int32_t*>(&rsi21) = 0x10002;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx22) = 0x10002;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
    } else {
        v20 = reinterpret_cast<void**>(0xf000);
        *reinterpret_cast<int32_t*>(&rsi21) = 0xf000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx22) = 0xf000;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
    }
    rax23 = xalignalloc(static_cast<int64_t>(eax17), rsi21);
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x838 - 8 + 8 - 8 + 8);
    rbp25 = rax23;
    dl26 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r14_10) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r14_10 == 0)))) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r14_10) < reinterpret_cast<unsigned char>(v20))));
    v27 = dl26;
    if (!dl26 && ((*reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&r12_9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, eax29 = rpl_fcntl(rdi28, 3), rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8), !(reinterpret_cast<uint1_t>(eax29 < 0) | reinterpret_cast<uint1_t>(eax29 == 0))) && !0)) {
        *reinterpret_cast<int32_t*>(&rdi30) = *reinterpret_cast<int32_t*>(&r12_9);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
        rpl_fcntl(rdi30, 4);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
    }
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 24)) & 0xf000) == 0x2000 && (rdx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp24) + 96), *reinterpret_cast<int32_t*>(&rdi32) = *reinterpret_cast<int32_t*>(&r12_9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0, rsi33 = reinterpret_cast<void**>(0x40086d01), *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0, v34 = reinterpret_cast<void**>(0x3b98), eax35 = fun_2700(rdi32), rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8), !eax35)) {
        eax36 = v13;
        if (reinterpret_cast<signed char>(eax36) >= reinterpret_cast<signed char>(0)) 
            goto addr_3682_8;
        goto addr_3bb0_10;
    }
    *reinterpret_cast<int32_t*>(&rdx31) = 0;
    *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
    rsi33 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi37) = *reinterpret_cast<int32_t*>(&r12_9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0;
    v34 = reinterpret_cast<void**>(0x35f9);
    rax38 = fun_26d0(rdi37);
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
    zf39 = reinterpret_cast<uint1_t>(rax38 == 0);
    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax38) < reinterpret_cast<signed char>(0)) | zf39) {
        if (!zf39) {
            rax40 = fun_2540();
            rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            rbx42 = rax40;
        } else {
            eax36 = v13;
            if (reinterpret_cast<signed char>(eax36) < reinterpret_cast<signed char>(0)) {
                addr_3bb0_10:
                goto addr_374a_15;
            } else {
                addr_3682_8:
                eax43 = reinterpret_cast<unsigned char>(eax36) & reinterpret_cast<uint32_t>("_2.28");
                eax44 = eax43 << 12 | eax43;
                edx45 = eax44 >> 4;
                if (reinterpret_cast<signed char>(r14_10) < reinterpret_cast<signed char>(0)) {
                    __asm__("rol ax, 0x8");
                    *reinterpret_cast<void***>(rbp25) = *reinterpret_cast<void***>(&edx45);
                    *reinterpret_cast<void***>(rbp25 + 1) = *reinterpret_cast<void***>(&eax44);
                    r13_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) >> 1);
                    goto addr_36be_17;
                } else {
                    *reinterpret_cast<void***>(rbp25) = *reinterpret_cast<void***>(&edx45);
                    if (reinterpret_cast<unsigned char>(rbx22) > reinterpret_cast<unsigned char>(r14_10)) {
                        rbx22 = r14_10;
                    }
                    __asm__("rol ax, 0x8");
                    *reinterpret_cast<void***>(rbp25 + 1) = *reinterpret_cast<void***>(&eax44);
                    r13_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) >> 1);
                    if (reinterpret_cast<unsigned char>(rbx22) <= reinterpret_cast<unsigned char>(5)) 
                        goto addr_3cdf_21; else 
                        goto addr_36be_17;
                }
            }
        }
    } else {
        rax47 = fun_2540();
        rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
        *reinterpret_cast<void***>(rax47) = reinterpret_cast<void**>(22);
        rbx42 = rax47;
    }
    rax48 = fun_2630();
    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
    rcx = v11;
    rsi33 = *reinterpret_cast<void***>(rbx42);
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    rdx31 = rax48;
    addr_3629_24:
    fun_2890();
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
    addr_3632_25:
    rdi50 = rbp25;
    goto addr_3635_26;
    addr_374a_15:
    v51 = 0;
    if (a8) {
        rax52 = fun_2630();
        rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8);
        rsi33 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
        rdx31 = rax52;
        rcx = v11;
        fun_2890();
        rax54 = fun_2790();
        v51 = rax54 + 5;
        r8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp53) + 0x71);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 - 8 + 8 - 8 + 8 + 8 + 8);
    }
    v55 = 0;
    v56 = reinterpret_cast<void**>(0xc303);
    v57 = 0;
    v58 = reinterpret_cast<void**>(0);
    while (1) {
        if (reinterpret_cast<signed char>(r14_10) >= reinterpret_cast<signed char>(0) && (rbx42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_10) - reinterpret_cast<unsigned char>(v58)), reinterpret_cast<unsigned char>(rbx42) < reinterpret_cast<unsigned char>(v20))) {
            if (!rbx42) 
                goto addr_3d9a_31;
            if (reinterpret_cast<signed char>(r14_10) < reinterpret_cast<signed char>(v58)) 
                goto addr_3d9a_31;
            if (reinterpret_cast<signed char>(v13) < reinterpret_cast<signed char>(0)) 
                goto addr_3b21_34; else 
                goto addr_37c2_35;
        }
        rbx42 = v20;
        if (reinterpret_cast<signed char>(v13) >= reinterpret_cast<signed char>(0)) {
            addr_37c2_35:
            r15_59 = r14_10;
            r14_10 = reinterpret_cast<void**>(0);
        } else {
            addr_3b21_34:
            randread(v14, rbp25, rbx42);
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            goto addr_37c2_35;
        }
        do {
            addr_37dc_37:
            rsi33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp25) + reinterpret_cast<unsigned char>(r14_10));
            *reinterpret_cast<int32_t*>(&rdi60) = *reinterpret_cast<int32_t*>(&r12_9);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi60) + 4) = 0;
            rdx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx42) - reinterpret_cast<unsigned char>(r14_10));
            rax61 = fun_25e0(rdi60);
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rax61) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rax61 == 0))) {
                r14_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_10) + reinterpret_cast<uint64_t>(rax61));
            } else {
                if (reinterpret_cast<signed char>(r15_59) < reinterpret_cast<signed char>(0)) {
                    if (!rax61) 
                        goto addr_3cea_41;
                    rax62 = fun_2540();
                    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                    r13_63 = *reinterpret_cast<void***>(rax62);
                    if (r13_63 == 28) 
                        goto addr_3cea_41;
                    if (!reinterpret_cast<int1_t>(r13_63 == 22)) 
                        goto addr_3c98_44;
                    if (v27) 
                        goto addr_3c98_44;
                } else {
                    rax64 = fun_2540();
                    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                    r13_63 = *reinterpret_cast<void***>(rax64);
                    if (!reinterpret_cast<int1_t>(r13_63 == 22) || v27) {
                        rax65 = umaxtostr(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(r14_10), reinterpret_cast<int64_t>(rsp24) + 0x590);
                        rax66 = fun_2630();
                        r8 = rax65;
                        rsi33 = r13_63;
                        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
                        rcx = v11;
                        rdx31 = rax66;
                        fun_2890();
                        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8 - 8 + 8);
                        if (!reinterpret_cast<int1_t>(r13_63 == 5)) 
                            goto addr_3632_25;
                        r9_67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_10) | 0x1ff);
                        if (reinterpret_cast<unsigned char>(r9_67) >= reinterpret_cast<unsigned char>(rbx42)) 
                            goto addr_3632_25;
                        r14_10 = r9_67 + 1;
                        *reinterpret_cast<int32_t*>(&rdx31) = 0;
                        *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi68) = *reinterpret_cast<int32_t*>(&r12_9);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi68) + 4) = 0;
                        rsi33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(r14_10));
                        rax69 = fun_26d0(rdi68);
                        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                        if (rax69 == 0xffffffffffffffff) 
                            goto addr_3b40_50;
                        v55 = 1;
                        if (reinterpret_cast<unsigned char>(rbx42) > reinterpret_cast<unsigned char>(r14_10)) 
                            goto addr_37dc_37; else 
                            break;
                    }
                }
                rsi33 = reinterpret_cast<void**>(3);
                *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi70) = *reinterpret_cast<int32_t*>(&r12_9);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0;
                eax71 = rpl_fcntl(rdi70, 3, rdi70, 3);
                rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                v27 = 1;
                if (!(reinterpret_cast<uint1_t>(eax71 < 0) | reinterpret_cast<uint1_t>(eax71 == 0)) && (*reinterpret_cast<int32_t*>(&rdx31) = eax71, *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx31 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx31 + 1) & 0xbf), !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax71) + 1) & 64))) {
                    rsi33 = reinterpret_cast<void**>(4);
                    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdi72) = *reinterpret_cast<int32_t*>(&r12_9);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi72) + 4) = 0;
                    rpl_fcntl(rdi72, 4, rdi72, 4);
                    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                }
            }
        } while (reinterpret_cast<unsigned char>(rbx42) > reinterpret_cast<unsigned char>(r14_10));
        rax73 = r14_10;
        rcx = v58;
        r14_10 = r15_59;
        if (reinterpret_cast<unsigned char>(0x7fffffffffffffff - reinterpret_cast<unsigned char>(rcx)) < reinterpret_cast<unsigned char>(rax73)) 
            goto addr_3d6e_56;
        r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax73) + reinterpret_cast<unsigned char>(rcx));
        v58 = r15_59;
        if (!a8) 
            continue;
        if (r14_10 == r15_59) {
            addr_3d25_59:
            if (*reinterpret_cast<void***>(v56)) 
                goto addr_3d46_60;
            rax74 = fun_2790();
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            v57 = rax74;
            if (v51 > rax74) 
                continue;
        } else {
            rax75 = fun_2790();
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            v57 = rax75;
            if (v51 > rax75) 
                continue;
            r8 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx) = 1;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            rsi76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) + 0x300);
            *reinterpret_cast<int32_t*>(&rdx31) = 0x1b2;
            *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
            rax77 = human_readable(v58, rsi76, 0x1b2, 1, 1, v58, rsi76, 0x1b2, 1, 1);
            rsi33 = rax77;
            r13_63 = rax77;
            eax78 = fun_2750(v56, rsi33, 0x1b2);
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8);
            if (!eax78) 
                continue; else 
                goto addr_3943_64;
        }
        addr_3d46_60:
        rsi79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) + 0x300);
        rax80 = human_readable(r15_59, rsi79, 0x1b2, 1, 1, r15_59, rsi79, 0x1b2, 1, 1);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
        r13_63 = rax80;
        addr_3943_64:
        if (reinterpret_cast<signed char>(r14_10) < reinterpret_cast<signed char>(0)) {
            fun_2630();
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8);
            r8 = a7;
            fun_2890();
            rcx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp81) + 0x71);
            rsp82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 - 8 + 8 + 8 + 8);
        } else {
            if (r14_10) {
                if (reinterpret_cast<unsigned char>(r15_59) > reinterpret_cast<unsigned char>(0x28f5c28f5c28f5c)) {
                }
            }
            rax83 = human_readable(r14_10, reinterpret_cast<int64_t>(rsp24) + 0x590, 0x1b0, 1, 1);
            if (r14_10 == r15_59) {
                r13_63 = rax83;
            }
            fun_2630();
            r8 = a7;
            rcx = v11;
            fun_2890();
            rsp82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        }
        rbx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp82) + 0x70);
        *reinterpret_cast<int32_t*>(&rdx31) = 0x28c;
        *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
        rsi33 = r13_63;
        fun_2820(rbx42);
        *reinterpret_cast<int32_t*>(&rdi84) = *reinterpret_cast<int32_t*>(&r12_9);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi84) + 4) = 0;
        v51 = v57 + 5;
        eax85 = fun_2550(rdi84, rsi33, 0x28c, rcx, r8, rdi84, rsi33, 0x28c, rcx, r8);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8 - 8 + 8);
        if (!eax85 || (rsi33 = v11, eax86 = dosync_part_0(*reinterpret_cast<int32_t*>(&r12_9), rsi33, 0x28c, rcx, r8, *reinterpret_cast<int32_t*>(&r12_9), rsi33, 0x28c, rcx, r8), rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8), eax86 == 0)) {
            v56 = rbx42;
            continue;
        } else {
            rax87 = fun_2540();
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax87) == 5)) 
                goto addr_3632_25;
            v56 = rbx42;
            v55 = 1;
            continue;
        }
        addr_3cea_41:
        if (reinterpret_cast<unsigned char>(0x7fffffffffffffff - reinterpret_cast<unsigned char>(v58)) < reinterpret_cast<unsigned char>(r14_10)) 
            goto addr_3d6e_56;
        rcx = v12;
        r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_10) + reinterpret_cast<unsigned char>(v58));
        v58 = r15_59;
        r14_10 = r15_59;
        *reinterpret_cast<void***>(rcx) = r15_59;
        if (a8) 
            goto addr_3d25_59;
    }
    addr_3c98_44:
    rax88 = umaxtostr(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<unsigned char>(r14_10), reinterpret_cast<int64_t>(rsp24) + 0x590);
    rax89 = fun_2630();
    rcx = v11;
    r8 = rax88;
    rsi33 = r13_63;
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    rdx31 = rax89;
    fun_2890();
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_3632_25;
    addr_3b40_50:
    rax90 = fun_2630();
    rcx = v11;
    rdx31 = rax90;
    rsi33 = *reinterpret_cast<void***>(rax64);
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    fun_2890();
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8);
    goto addr_3632_25;
    addr_3d6e_56:
    rax91 = fun_2630();
    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
    rcx = v11;
    rsi33 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    rdx31 = rax91;
    goto addr_3629_24;
    addr_3d9a_31:
    *reinterpret_cast<int32_t*>(&rdi92) = *reinterpret_cast<int32_t*>(&r12_9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi92) + 4) = 0;
    eax93 = fun_2550(rdi92, rsi33, rdx31, rcx, r8, rdi92, rsi33, rdx31, rcx, r8);
    rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
    if (!eax93 || (rsi33 = v11, eax95 = dosync_part_0(*reinterpret_cast<int32_t*>(&r12_9), rsi33, rdx31, rcx, r8, *reinterpret_cast<int32_t*>(&r12_9), rsi33, rdx31, rcx, r8), rsp94 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp94) - 8 + 8), eax95 == 0)) {
        rdi50 = rbp25;
        fun_2520(rdi50, rsi33, rdx31, rcx, r8, rdi50, rsi33, rdx31, rcx, r8);
        rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp94) - 8 + 8);
        eax97 = v55;
    } else {
        rax98 = fun_2540();
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp94) - 8 + 8);
        rdi50 = rbp25;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax98) == 5)) {
            addr_3635_26:
            fun_2520(rdi50, rsi33, rdx31, rcx, r8, rdi50, rsi33, rdx31, rcx, r8);
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            eax97 = 0xffffffff;
        } else {
            fun_2520(rdi50, rsi33, rdx31, rcx, r8, rdi50, rsi33, rdx31, rcx, r8);
            rsp96 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            eax97 = 1;
        }
    }
    rdx99 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (!rdx99) {
        return eax97;
    }
    fun_2660();
    r12_100 = rsi33;
    ebp101 = *reinterpret_cast<int32_t*>(&rdi50);
    rbx102 = rcx;
    rsp103 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp96) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x118);
    v104 = rdx99;
    rax105 = g28;
    v106 = rax105;
    zf107 = *reinterpret_cast<signed char*>(rcx + 28) == 0;
    if (!zf107) 
        goto addr_3ec0_85;
    addr_3ecd_86:
    r15_108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 0x70);
    *reinterpret_cast<int32_t*>(&rdi109) = ebp101;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0;
    eax110 = fun_29a0(rdi109, r15_108);
    rsp111 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp103) - 8 + 8);
    if (eax110) {
        fun_2630();
        fun_2540();
        fun_2890();
    } else {
        eax112 = v113 & 0xf000;
        if (eax112 == 0x2000) {
            *reinterpret_cast<int32_t*>(&rdi114) = ebp101;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi114) + 4) = 0;
            eax115 = fun_25a0(rdi114, r15_108);
            rsp111 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp111) - 8 + 8);
            if (eax115) 
                goto addr_4016_90;
            eax112 = v116 & 0xf000;
        }
        *reinterpret_cast<unsigned char*>(&rdx99) = reinterpret_cast<uint1_t>(eax112 == 0xc000);
        *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax112 == "2.28")) | *reinterpret_cast<unsigned char*>(&rdx99));
        if (*reinterpret_cast<unsigned char*>(&rcx)) {
            addr_4016_90:
            fun_2630();
            fun_2890();
        } else {
            if (eax112 != 0x8000 || v117 >= 0) {
                rdi118 = *reinterpret_cast<void***>(rbx102 + 8);
                rsi119 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0;
                rax120 = xnmalloc(rdi118, 4);
                rsp121 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp111) - 8 + 8);
                v122 = rax120;
                zf123 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx102 + 16) == 0xffffffffffffffff);
                v124 = *reinterpret_cast<void***>(rbx102 + 16);
                if (zf123) {
                    if ((v125 & 0xf000) != 0x8000) {
                        rsi119 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdx99) = 2;
                        *reinterpret_cast<int32_t*>(&rdx99 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi126) = ebp101;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi126) + 4) = 0;
                        rax127 = fun_26d0(rdi126, rdi126);
                        rsp121 = rsp121 - 8 + 8;
                        if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax127) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax127 == 0)) {
                            rax127 = v124;
                        }
                        v124 = rax127;
                        goto addr_3f6a_99;
                    }
                    rsi119 = v128;
                    zf129 = *reinterpret_cast<signed char*>(rbx102 + 29) == 0;
                    v124 = rsi119;
                    if (!zf129) {
                        addr_3f6a_99:
                        rax130 = *reinterpret_cast<void***>(rbx102 + 8);
                        v131 = rax130;
                        if (!rax130) {
                            rax132 = randint_get_source(v104, rsi119, rdx99);
                            rsp133 = rsp121 - 8 + 8;
                            r9_134 = rax132;
                            v135 = v124;
                            goto addr_41ac_102;
                        } else {
                            v135 = reinterpret_cast<void**>(0);
                            goto addr_3f84_104;
                        }
                    } else {
                        rcx = v136;
                        if (reinterpret_cast<uint64_t>(rcx + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                            rcx = reinterpret_cast<void**>(0x200);
                        }
                        rdx99 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rsi119) % reinterpret_cast<signed char>(rcx));
                        if (!rsi119 || (v135 = rsi119, reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(rsi119))) {
                            v135 = reinterpret_cast<void**>(0);
                        }
                        if (rdx99) {
                            rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) - reinterpret_cast<unsigned char>(rdx99));
                            rax137 = reinterpret_cast<void**>(0x7fffffffffffffff - reinterpret_cast<unsigned char>(v124));
                            if (reinterpret_cast<signed char>(rax137) > reinterpret_cast<signed char>(rcx)) {
                                rax137 = rcx;
                            }
                            v124 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v124) + reinterpret_cast<unsigned char>(rax137));
                            goto addr_40e3_113;
                        }
                    }
                } else {
                    if ((v125 & 0xf000) != 0x8000) 
                        goto addr_3f6a_99;
                    rax138 = v139;
                    v135 = v140;
                    *reinterpret_cast<int32_t*>(&rdx99) = 0x200;
                    *reinterpret_cast<int32_t*>(&rdx99 + 4) = 0;
                    rcx = v124;
                    if (reinterpret_cast<uint64_t>(rax138 + 0xffffffffffffffff) > 0x1fffffffffffffff) {
                        rax138 = reinterpret_cast<void**>(0x200);
                    }
                    if (reinterpret_cast<signed char>(rax138) > reinterpret_cast<signed char>(rcx)) {
                        rax138 = rcx;
                    }
                    if (reinterpret_cast<signed char>(v140) >= reinterpret_cast<signed char>(rax138)) 
                        goto addr_3f6a_99; else 
                        goto addr_40e3_113;
                }
            } else {
                fun_2630();
                fun_2890();
            }
        }
    }
    addr_403f_121:
    rax141 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v106) - reinterpret_cast<unsigned char>(g28));
    if (rax141) {
        fun_2660();
    } else {
        goto v142;
    }
    addr_41ac_102:
    while (v124 = reinterpret_cast<void**>(0), !!v135) {
        addr_411b_126:
        rdx99 = *reinterpret_cast<void***>(rbx102 + 8);
        *reinterpret_cast<uint32_t*>(&rax143) = *reinterpret_cast<unsigned char*>(rbx102 + 30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax143) + 4) = 0;
        if (rax143 + reinterpret_cast<unsigned char>(rdx99)) {
            v144 = reinterpret_cast<void**>(rsp133 + 0x68);
            rbx145 = reinterpret_cast<void**>(0);
            r13_146 = r9_134;
            r14_147 = rbx102;
            do {
                r8 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (reinterpret_cast<unsigned char>(rdx99) > reinterpret_cast<unsigned char>(rbx145)) {
                    r8 = *reinterpret_cast<void***>(v122 + reinterpret_cast<unsigned char>(rbx145) * 4);
                    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                }
                ++rbx145;
                rsi119 = r15_108;
                eax148 = dopass(ebp101, rsi119, r12_100, v144, r8, r13_146, rbx145, v144);
                rdx99 = rbx145;
                rcx = v144;
                rsp133 = rsp133 - 8 - 8 - 8 + 8 + 8 + 8;
                if (eax148) {
                    if (reinterpret_cast<int32_t>(eax148) < reinterpret_cast<int32_t>(0)) 
                        goto addr_41df_132;
                }
                rdx99 = *reinterpret_cast<void***>(r14_147 + 8);
                *reinterpret_cast<uint32_t*>(&rax149) = *reinterpret_cast<unsigned char*>(r14_147 + 30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax149) + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx145) < reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(rax149) + reinterpret_cast<unsigned char>(rdx99)));
            r9_134 = r13_146;
            rbx102 = r14_147;
        }
        v135 = v124;
    }
    if (!(!*reinterpret_cast<void***>(rbx102 + 24) || ((rsi119 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0, *reinterpret_cast<int32_t*>(&rdi150) = ebp101, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi150) + 4) = 0, eax151 = fun_26c0(rdi150), eax151 == 0) || (v152 & 0xf000) != 0x8000))) {
        rax153 = fun_2630();
        rax154 = fun_2540();
        rcx = r12_100;
        rdx99 = rax153;
        rsi119 = *reinterpret_cast<void***>(rax154);
        *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0;
        fun_2890();
    }
    addr_41df_132:
    fun_2520(v122, rsi119, rdx99, rcx, r8, v122, rsi119, rdx99, rcx, r8);
    goto addr_403f_121;
    addr_40e3_113:
    rax155 = *reinterpret_cast<void***>(rbx102 + 8);
    v131 = rax155;
    if (rax155) {
        addr_3f84_104:
        r8 = reinterpret_cast<void**>(0xbc80);
        v156 = ebp101;
        eax157 = reinterpret_cast<void**>(0xfffffffe);
        v158 = rbx102;
        r10_159 = v122;
        *reinterpret_cast<int32_t*>(&r13_160) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_160) + 4) = 0;
        rbp161 = reinterpret_cast<void**>(0xbc80);
        v162 = r12_100;
        rbx163 = v131;
    } else {
        addr_40f5_140:
        rax164 = randint_get_source(v104, rsi119, rdx99);
        rsp133 = rsp121 - 8 + 8;
        r9_134 = rax164;
        if (!v135) {
            v135 = v124;
            goto addr_41ac_102;
        } else {
            goto addr_411b_126;
        }
    }
    while (1) {
        if (!eax157) {
            eax157 = reinterpret_cast<void**>(0xfffffffe);
            rbp161 = reinterpret_cast<void**>(0xbc84);
            goto addr_3fc4_145;
        }
        rbp161 = rbp161 + 4;
        if (reinterpret_cast<signed char>(eax157) < reinterpret_cast<signed char>(0)) {
            addr_3fc4_145:
            rax165 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(-reinterpret_cast<unsigned char>(eax157))));
            if (reinterpret_cast<unsigned char>(rax165) >= reinterpret_cast<unsigned char>(rbx163)) 
                break;
        } else {
            r14_166 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax157)));
            if (reinterpret_cast<unsigned char>(r14_166) > reinterpret_cast<unsigned char>(rbx163)) 
                goto addr_41ee_148;
            r12_167 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_166) * 4);
            rsi119 = rbp161;
            rbx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx163) - reinterpret_cast<unsigned char>(r14_166));
            rdx99 = r12_167;
            rbp161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp161) + reinterpret_cast<unsigned char>(r12_167));
            rax168 = fun_2780(r10_159, rsi119, rdx99, rcx, 0xbc80, r10_159, rsi119, rdx99, rcx, 0xbc80);
            rsp121 = rsp121 - 8 + 8;
            r10_159 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax168) + reinterpret_cast<unsigned char>(r12_167));
            goto addr_3fd7_150;
        }
        r13_160 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_160) + reinterpret_cast<unsigned char>(rax165));
        rbx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx163) - reinterpret_cast<unsigned char>(rax165));
        addr_3fd7_150:
        eax157 = *reinterpret_cast<void***>(rbp161);
    }
    ebp169 = v156;
    r12_170 = v162;
    rbx171 = v158;
    v172 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_160) + reinterpret_cast<unsigned char>(rbx163));
    addr_428c_153:
    v173 = r12_170;
    *reinterpret_cast<int32_t*>(&r13_174) = 0;
    *reinterpret_cast<int32_t*>(&r13_174 + 4) = 0;
    v175 = ebp169;
    v176 = rbx171;
    r9_177 = reinterpret_cast<signed char*>(v131 + 0xffffffffffffffff);
    r14_178 = v131;
    rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v131) - reinterpret_cast<uint64_t>(v172));
    rdi179 = reinterpret_cast<uint64_t>(v172) - 1;
    v180 = r15_108;
    rbx181 = v122;
    rbp182 = rdi179;
    r12_183 = rdi179;
    r15_184 = rcx;
    while (1) {
        if (rbp182 >= r12_183) {
            r12_183 = r12_183 + reinterpret_cast<uint64_t>(r9_177) - rbp182;
            *reinterpret_cast<void***>(rbx181 + reinterpret_cast<unsigned char>(r15_184) * 4) = *reinterpret_cast<void***>(rbx181 + reinterpret_cast<unsigned char>(r13_174) * 4);
            ++r15_184;
            *reinterpret_cast<void***>(rbx181 + reinterpret_cast<unsigned char>(r13_174) * 4) = reinterpret_cast<void**>(0xffffffff);
            ++r13_174;
            if (reinterpret_cast<unsigned char>(r13_174) >= reinterpret_cast<unsigned char>(r14_178)) 
                break;
        } else {
            r12_183 = r12_183 - rbp182;
            rax185 = randint_genmax(v104, reinterpret_cast<uint64_t>(r15_184 + 0xffffffffffffffff) - reinterpret_cast<unsigned char>(r13_174), rdx99);
            rsp121 = rsp121 - 8 + 8;
            rsi119 = *reinterpret_cast<void***>(rbx181 + reinterpret_cast<unsigned char>(r13_174) * 4);
            *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0;
            r9_177 = r9_177;
            rax186 = reinterpret_cast<void***>(rbx181 + (reinterpret_cast<unsigned char>(rax185) + reinterpret_cast<unsigned char>(r13_174)) * 4);
            *reinterpret_cast<void***>(rbx181 + reinterpret_cast<unsigned char>(r13_174) * 4) = *rax186;
            ++r13_174;
            *rax186 = rsi119;
            if (reinterpret_cast<unsigned char>(r13_174) >= reinterpret_cast<unsigned char>(r14_178)) 
                break;
        }
    }
    ebp101 = v175;
    r12_100 = v173;
    rbx102 = v176;
    r15_108 = v180;
    goto addr_40f5_140;
    addr_41ee_148:
    v172 = r13_160;
    r13_187 = r14_166;
    r14_188 = rbx163;
    r8 = rbp161;
    r12_170 = v162;
    ebp169 = v156;
    rbx171 = v158;
    if (reinterpret_cast<unsigned char>(r14_188) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r13_187) > reinterpret_cast<unsigned char>(r14_188 + reinterpret_cast<unsigned char>(r14_188) * 2)) {
        v172 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v172) + reinterpret_cast<unsigned char>(r14_188));
        goto addr_428c_153;
    } else {
        v189 = ebp169;
        rbp190 = r10_159;
        v191 = r12_170;
        r12_192 = r8;
        v193 = rbx171;
        rbx194 = v104;
        v195 = r15_108;
        while (1) {
            r12_192 = r12_192 + 4;
            if (r14_188 == r13_187 || (r15_196 = r13_187 + 0xffffffffffffffff, rsi119 = r15_196, rax197 = randint_genmax(rbx194, rsi119, rdx99), rsp121 = rsp121 - 8 + 8, reinterpret_cast<unsigned char>(r14_188) > reinterpret_cast<unsigned char>(rax197))) {
                *reinterpret_cast<void***>(rbp190) = *reinterpret_cast<void***>(r12_192 + 0xfffffffffffffffc);
                --r14_188;
                if (!r14_188) 
                    break;
                rbp190 = rbp190 + 4;
                r15_196 = r13_187 + 0xffffffffffffffff;
            }
            r13_187 = r15_196;
        }
        ebp169 = v189;
        r12_170 = v191;
        rbx171 = v193;
        r15_108 = v195;
        goto addr_428c_153;
    }
    addr_3ec0_85:
    goto addr_3ecd_86;
    addr_36be_17:
    *reinterpret_cast<int32_t*>(&r15_59) = 3;
    *reinterpret_cast<int32_t*>(&r15_59 + 4) = 0;
    do {
        rdi198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp25) + reinterpret_cast<unsigned char>(r15_59));
        rdx199 = r15_59;
        r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_59) + reinterpret_cast<unsigned char>(r15_59));
        v34 = reinterpret_cast<void**>(0x36db);
        fun_2780(rdi198, rbp25, rdx199, rcx, r8);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
    } while (reinterpret_cast<unsigned char>(r15_59) <= reinterpret_cast<unsigned char>(r13_46));
    addr_36e0_166:
    r13_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v13) & reinterpret_cast<uint32_t>("2.28"));
    if (reinterpret_cast<unsigned char>(r15_59) < reinterpret_cast<unsigned char>(rbx22)) {
        v34 = reinterpret_cast<void**>(0x3c83);
        fun_2780(reinterpret_cast<unsigned char>(rbp25) + reinterpret_cast<unsigned char>(r15_59), rbp25, reinterpret_cast<unsigned char>(rbx22) - reinterpret_cast<unsigned char>(r15_59), rcx, r8);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
        if (!r13_63) {
            addr_3718_168:
            rdi200 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp24) + 0x69);
            r8 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp25))));
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            rsi33 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
            rcx = reinterpret_cast<void**>("%02x%02x%02x");
            fun_29d0(rdi200, 1, 7, "%02x%02x%02x", rdi200, 1, 7, "%02x%02x%02x");
            rdx31 = v34;
            rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 - 8 - 8 + 8 + 8 + 8);
            goto addr_374a_15;
        }
    } else {
        if (!r13_63) 
            goto addr_3718_168;
        if (!rbx22) 
            goto addr_3718_168;
    }
    *reinterpret_cast<int32_t*>(&rax201) = 0;
    *reinterpret_cast<int32_t*>(&rax201 + 4) = 0;
    do {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp25) + reinterpret_cast<unsigned char>(rax201)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp25) + reinterpret_cast<unsigned char>(rax201)) - 0x80);
        rax201 = rax201 + 0x200;
    } while (reinterpret_cast<unsigned char>(rax201) < reinterpret_cast<unsigned char>(rbx22));
    goto addr_3718_168;
    addr_3cdf_21:
    *reinterpret_cast<int32_t*>(&r15_59) = 3;
    *reinterpret_cast<int32_t*>(&r15_59 + 4) = 0;
    goto addr_36e0_166;
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xbebb);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xbeb4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xbebf);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xbeb0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gfd58 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gfd58;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24f3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2503() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2513() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2523() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2533() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2543() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t fdatasync = 0x2070;

void fun_2553() {
    __asm__("cli ");
    goto fdatasync;
}

int64_t unlink = 0x2080;

void fun_2563() {
    __asm__("cli ");
    goto unlink;
}

int64_t strncmp = 0x2090;

void fun_2573() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20a0;

void fun_2583() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20b0;

void fun_2593() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x20c0;

void fun_25a3() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x20d0;

void fun_25b3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t localeconv = 0x20e0;

void fun_25c3() {
    __asm__("cli ");
    goto localeconv;
}

int64_t fcntl = 0x20f0;

void fun_25d3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t write = 0x2100;

void fun_25e3() {
    __asm__("cli ");
    goto write;
}

int64_t fread_unlocked = 0x2110;

void fun_25f3() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x2120;

void fun_2603() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2130;

void fun_2613() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2140;

void fun_2623() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2150;

void fun_2633() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2160;

void fun_2643() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2170;

void fun_2653() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2180;

void fun_2663() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2190;

void fun_2673() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21a0;

void fun_2683() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21b0;

void fun_2693() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x21c0;

void fun_26a3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21d0;

void fun_26b3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x21e0;

void fun_26c3() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t lseek = 0x21f0;

void fun_26d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2200;

void fun_26e3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2210;

void fun_26f3() {
    __asm__("cli ");
    goto memset;
}

int64_t ioctl = 0x2220;

void fun_2703() {
    __asm__("cli ");
    goto ioctl;
}

int64_t close = 0x2230;

void fun_2713() {
    __asm__("cli ");
    goto close;
}

int64_t memcmp = 0x2240;

void fun_2723() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2250;

void fun_2733() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2260;

void fun_2743() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2270;

void fun_2753() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2280;

void fun_2763() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memcpy_chk = 0x2290;

void fun_2773() {
    __asm__("cli ");
    goto __memcpy_chk;
}

int64_t memcpy = 0x22a0;

void fun_2783() {
    __asm__("cli ");
    goto memcpy;
}

int64_t time = 0x22b0;

void fun_2793() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x22c0;

void fun_27a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t sync = 0x22d0;

void fun_27b3() {
    __asm__("cli ");
    goto sync;
}

int64_t malloc = 0x22e0;

void fun_27c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22f0;

void fun_27d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2300;

void fun_27e3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t renameat2 = 0x2310;

void fun_27f3() {
    __asm__("cli ");
    goto renameat2;
}

int64_t __freading = 0x2320;

void fun_2803() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2330;

void fun_2813() {
    __asm__("cli ");
    goto realloc;
}

int64_t __strcpy_chk = 0x2340;

void fun_2823() {
    __asm__("cli ");
    goto __strcpy_chk;
}

int64_t fdopen = 0x2350;

void fun_2833() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x2360;

void fun_2843() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2370;

void fun_2853() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x2380;

void fun_2863() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t chmod = 0x2390;

void fun_2873() {
    __asm__("cli ");
    goto chmod;
}

int64_t memmove = 0x23a0;

void fun_2883() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x23b0;

void fun_2893() {
    __asm__("cli ");
    goto error;
}

int64_t fsync = 0x23c0;

void fun_28a3() {
    __asm__("cli ");
    goto fsync;
}

int64_t __explicit_bzero_chk = 0x23d0;

void fun_28b3() {
    __asm__("cli ");
    goto __explicit_bzero_chk;
}

int64_t open = 0x23e0;

void fun_28c3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x23f0;

void fun_28d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2400;

void fun_28e3() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x2410;

void fun_28f3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2420;

void fun_2903() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t renameat = 0x2430;

void fun_2913() {
    __asm__("cli ");
    goto renameat;
}

int64_t getpagesize = 0x2440;

void fun_2923() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t exit = 0x2450;

void fun_2933() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2460;

void fun_2943() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2470;

void fun_2953() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrandom = 0x2480;

void fun_2963() {
    __asm__("cli ");
    goto getrandom;
}

int64_t aligned_alloc = 0x2490;

void fun_2973() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x24a0;

void fun_2983() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x24b0;

void fun_2993() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x24c0;

void fun_29a3() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x24d0;

void fun_29b3() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x24e0;

void fun_29c3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x24f0;

void fun_29d3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2840(int64_t rdi, ...);

void fun_2620(int64_t rdi, int64_t rsi);

void fun_2600(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

int32_t fun_2670(int64_t rdi);

struct s3 {
    signed char[40] pad40;
    signed char* f28;
    signed char* f30;
};

struct s3* stdout = reinterpret_cast<struct s3*>(0);

int64_t Version = 0xbd6c;

void version_etc(struct s3* rdi, int64_t rsi, int64_t rdx, int64_t rcx, void** r8);

int32_t fun_2930();

void usage();

int32_t optind = 0;

void** randint_all_new(int64_t rdi);

void** randint_source = reinterpret_cast<void**>(0);

void* base_len(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t renameatu(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

struct s4 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s4* fun_2690(void** rdi, ...);

int32_t fun_2560(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2710(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** quotearg_n_style_colon();

void** xstrdup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t open_safer(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2870(void** rdi, int64_t rsi, void** rdx, void** rcx);

void** last_component(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** dir_name(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void fun_26e0(int64_t rdi, void** rsi, int64_t rdx, void* rcx, void** r8);

int64_t fun_2a33(int32_t edi, void** rsi, int64_t rdx, void** rcx) {
    void** r15_5;
    void*** r13_6;
    void** rbp7;
    void** rbx8;
    void** rdi9;
    void** rax10;
    void** v11;
    void** r12_12;
    void*** rsp13;
    void** v14;
    void** r8_15;
    void** rcx16;
    int64_t rdi17;
    int32_t eax18;
    void* rsp19;
    struct s3* rdi20;
    int64_t rcx21;
    void** rax22;
    void* rsp23;
    unsigned char v24;
    void* rax25;
    void*** v26;
    unsigned char* rsp27;
    void** rax28;
    void** rcx29;
    void** rsi30;
    void** rdx31;
    void* rax32;
    void** r14_33;
    void** v34;
    void** v35;
    uint32_t eax36;
    void*** v37;
    void** r13_38;
    signed char v39;
    void** v40;
    int32_t eax41;
    unsigned char* rsp42;
    void** rax43;
    void** rbp44;
    struct s4* rax45;
    uint32_t eax46;
    int32_t v47;
    int64_t rdi48;
    int32_t eax49;
    void** v50;
    int32_t eax51;
    signed char v52;
    void** rcx53;
    int32_t eax54;
    unsigned char* rsp55;
    void** rax56;
    void** rax57;
    signed char v58;
    void** rax59;
    int64_t rdi60;
    int32_t eax61;
    unsigned char* rsp62;
    int32_t eax63;
    int64_t rdi64;
    int32_t eax65;
    void** rax66;
    void** rax67;
    void** v68;
    unsigned char* rsp69;
    void** rdx70;
    void** rax71;
    void** rax72;
    void* rsp73;
    void** r12_74;
    int32_t eax75;
    void* rsp76;
    void** rax77;
    void** rax78;
    int32_t eax79;
    void** rax80;
    void** r14_81;
    int32_t eax82;
    void*** rsp83;
    int32_t ebx84;
    void** rax85;
    signed char v86;
    int32_t eax87;
    int32_t eax88;
    unsigned char al89;
    int64_t rdi90;
    int32_t eax91;
    void** rax92;
    void** rax93;
    void** rax94;
    void** rax95;
    void** rax96;
    void** rax97;
    void** rax98;
    void** rax99;
    int32_t eax100;
    signed char v101;
    uint32_t eax102;
    int64_t rax103;
    void* rdx104;
    int64_t rax105;

    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    r15_5 = reinterpret_cast<void**>(0xc303);
    r13_6 = reinterpret_cast<void***>("fn:s:uvxz");
    *reinterpret_cast<int32_t*>(&rbp7) = edi;
    *reinterpret_cast<int32_t*>(&rbp7 + 4) = 0;
    rbx8 = rsi;
    rdi9 = *reinterpret_cast<void***>(rsi);
    rax10 = g28;
    v11 = rax10;
    __asm__("movaps [rsp+0x50], xmm0");
    __asm__("movaps [rsp+0x60], xmm0");
    set_program_name(rdi9);
    fun_2840(6, 6);
    fun_2620("coreutils", "/usr/local/share/locale");
    r12_12 = reinterpret_cast<void**>(0xbba0);
    fun_2600("coreutils", "/usr/local/share/locale");
    atexit(0x4e50, "/usr/local/share/locale", rdx, rcx);
    rsp13 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v14 = reinterpret_cast<void**>(0);
    while (*reinterpret_cast<int32_t*>(&r8_15) = 0, *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0, rcx16 = reinterpret_cast<void**>(0xf960), *reinterpret_cast<int32_t*>(&rdi17) = *reinterpret_cast<int32_t*>(&rbp7), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0, eax18 = fun_2670(rdi17), rsp19 = reinterpret_cast<void*>(rsp13 - 8 + 8), eax18 != -1) {
        if (eax18 > 0x80) 
            goto addr_312b_4;
        if (eax18 > 0x65) 
            goto addr_2b10_6;
        if (eax18 == 0xffffff7d) {
            rdi20 = stdout;
            rcx21 = Version;
            r8_15 = reinterpret_cast<void**>("Colin Plumb");
            version_etc(rdi20, "shred", "GNU coreutils", rcx21, "Colin Plumb");
            eax18 = fun_2930();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
        }
        if (eax18 != 0xffffff7e) 
            goto addr_312b_4;
        usage();
        rsp13 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    }
    r12_12 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
    *reinterpret_cast<int32_t*>(&rbp7) = *reinterpret_cast<int32_t*>(&rbp7) - *reinterpret_cast<int32_t*>(&r12_12);
    *reinterpret_cast<int32_t*>(&rbp7 + 4) = 0;
    if (!*reinterpret_cast<int32_t*>(&rbp7)) {
        fun_2630();
        fun_2890();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
    } else {
        rax22 = randint_all_new(0);
        randint_source = rax22;
        if (!rax22) {
            addr_3328_15:
            if (!v14) 
                goto addr_335f_16; else 
                goto addr_332f_17;
        } else {
            atexit(0x3460, -1, "fn:s:uvxz", 0xf960);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp7) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp7) == 0)) {
                v24 = 1;
                goto addr_2ea1_20;
            } else {
                *reinterpret_cast<int32_t*>(&rax25) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbp7 + 0xffffffffffffffff));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
                v24 = 1;
                r13_6 = reinterpret_cast<void***>(rbx8 + reinterpret_cast<unsigned char>(r12_12) * 8);
                r15_5 = reinterpret_cast<void**>("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.");
                v26 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx8 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax25) + reinterpret_cast<unsigned char>(r12_12)) * 8) + 8);
                goto addr_2da5_22;
            }
        }
    }
    addr_312b_4:
    usage();
    rsp27 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    while (1) {
        rax28 = fun_2630();
        rcx29 = rbp7;
        rsi30 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        rdx31 = rax28;
        fun_2890();
        rsp27 = rsp27 - 8 + 8 - 8 + 8;
        do {
            if (!0) {
                rax32 = base_len(rbx8, rsi30, rdx31, rcx29, r8_15);
                rsp27 = rsp27 - 8 + 8;
                r14_33 = v34;
                v35 = rbp7;
                rcx29 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax32) + 0xffffffffffffffff);
                eax36 = *reinterpret_cast<unsigned char*>(&v14);
                v37 = r13_6;
                r13_38 = rcx29;
                v39 = *reinterpret_cast<signed char*>(&eax36);
                v40 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx8) - reinterpret_cast<unsigned char>(r14_33)) + reinterpret_cast<unsigned char>(r12_12));
                while (rdx31 = r13_38 + 1, r13_38 != 0xffffffffffffffff) {
                    fun_26f0(rbx8, rbx8);
                    rsp27 = rsp27 - 8 + 8;
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(r13_38) + 1) = 0;
                    while (rcx29 = r14_33, rsi30 = r12_12, *reinterpret_cast<int32_t*>(&r8_15) = 1, *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0, eax41 = renameatu(0xffffff9c, rsi30, 0xffffff9c, rcx29, 1), rsp42 = rsp27 - 8 + 8, !!eax41) {
                        rax43 = fun_2540();
                        rsp27 = rsp42 - 8 + 8;
                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax43) == 17)) 
                            goto addr_320d_30;
                        rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(r13_38));
                        while (rsi30 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp44)))), *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0, rax45 = fun_2690(r15_5, r15_5), rsp27 = rsp27 - 8 + 8, !!rax45) {
                            eax46 = rax45->f1;
                            if (*reinterpret_cast<void***>(&eax46)) 
                                goto addr_30b1_34;
                            *reinterpret_cast<void***>(rbp44) = reinterpret_cast<void**>(48);
                            if (rbx8 == rbp44) 
                                goto addr_320d_30;
                            --rbp44;
                        }
                        goto addr_32e0_37;
                        addr_30b1_34:
                        *reinterpret_cast<void***>(rbp44) = *reinterpret_cast<void***>(&eax46);
                    }
                    if (v47 >= 0 && (*reinterpret_cast<int32_t*>(&rdi48) = v47, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi48) + 4) = 0, eax49 = fun_2550(rdi48, rsi30, 0xffffff9c, rcx29, 1), rsp42 = rsp42 - 8 + 8, !!eax49)) {
                        eax51 = dosync_part_0(v47, v50, 0xffffff9c, rcx29, 1);
                        rsp42 = rsp42 - 8 + 8;
                        *reinterpret_cast<uint32_t*>(&rcx29) = *reinterpret_cast<unsigned char*>(&v14);
                        *reinterpret_cast<int32_t*>(&rcx29 + 4) = 0;
                        if (eax51) {
                            *reinterpret_cast<uint32_t*>(&rcx29) = 0;
                            *reinterpret_cast<int32_t*>(&rcx29 + 4) = 0;
                        }
                        *reinterpret_cast<unsigned char*>(&v14) = *reinterpret_cast<unsigned char*>(&rcx29);
                    }
                    if (v52) {
                        rcx53 = v35;
                        if (!v39) {
                            rcx53 = r12_12;
                        }
                        fun_2630();
                        rcx29 = rcx53;
                        r8_15 = r14_33;
                        fun_2890();
                        rsp42 = rsp42 - 8 + 8 - 8 + 8;
                        v39 = 0;
                    }
                    rsi30 = rbx8;
                    fun_2780(v40, rsi30, r13_38 + 2, rcx29, r8_15);
                    rsp27 = rsp42 - 8 + 8;
                    addr_320d_30:
                    --r13_38;
                }
                r13_6 = v37;
                rbp7 = v35;
            }
            eax54 = fun_2560(r12_12, rsi30, rdx31, rcx29, r8_15);
            rsp55 = rsp27 - 8 + 8;
            if (eax54) {
                rax56 = fun_2630();
                rax57 = fun_2540();
                rcx29 = rbp7;
                rdx31 = rax56;
                rsi30 = *reinterpret_cast<void***>(rax57);
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                fun_2890();
                rsp55 = rsp55 - 8 + 8 - 8 + 8 - 8 + 8;
                *reinterpret_cast<unsigned char*>(&v14) = 0;
            } else {
                if (v58) {
                    rax59 = fun_2630();
                    rcx29 = rbp7;
                    rsi30 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                    rdx31 = rax59;
                    fun_2890();
                    rsp55 = rsp55 - 8 + 8 - 8 + 8;
                }
            }
            if (v47 >= 0) {
                *reinterpret_cast<int32_t*>(&rdi60) = v47;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi60) + 4) = 0;
                eax61 = fun_2550(rdi60, rsi30, rdx31, rcx29, r8_15);
                rsp62 = rsp55 - 8 + 8;
                if (eax61) {
                    rsi30 = v50;
                    eax63 = dosync_part_0(v47, rsi30, rdx31, rcx29, r8_15);
                    rsp62 = rsp62 - 8 + 8;
                    *reinterpret_cast<uint32_t*>(&rcx29) = *reinterpret_cast<unsigned char*>(&v14);
                    *reinterpret_cast<int32_t*>(&rcx29 + 4) = 0;
                    if (eax63) {
                        *reinterpret_cast<uint32_t*>(&rcx29) = 0;
                        *reinterpret_cast<int32_t*>(&rcx29 + 4) = 0;
                    }
                    *reinterpret_cast<unsigned char*>(&v14) = *reinterpret_cast<unsigned char*>(&rcx29);
                }
                *reinterpret_cast<int32_t*>(&rdi64) = v47;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi64) + 4) = 0;
                eax65 = fun_2710(rdi64, rsi30, rdx31, rcx29, r8_15);
                rsp55 = rsp62 - 8 + 8;
                if (eax65) {
                    rax66 = fun_2630();
                    rax67 = fun_2540();
                    rcx29 = v50;
                    rdx31 = rax66;
                    rsi30 = *reinterpret_cast<void***>(rax67);
                    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                    fun_2890();
                    rsp55 = rsp55 - 8 + 8 - 8 + 8 - 8 + 8;
                    *reinterpret_cast<unsigned char*>(&v14) = 0;
                }
            }
            fun_2520(v34, rsi30, rdx31, rcx29, r8_15);
            fun_2520(v68, rsi30, rdx31, rcx29, r8_15);
            fun_2520(v50, rsi30, rdx31, rcx29, r8_15);
            rsp69 = rsp55 - 8 + 8 - 8 + 8 - 8 + 8;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rcx16) = *reinterpret_cast<unsigned char*>(&v14);
                *reinterpret_cast<int32_t*>(&rcx16 + 4) = 0;
                v24 = reinterpret_cast<unsigned char>(v24 & *reinterpret_cast<unsigned char*>(&rcx16));
                while (r13_6 = r13_6 + 8, fun_2520(rbp7, rsi30, rdx31, rcx16, r8_15), rsp23 = reinterpret_cast<void*>(rsp69 - 8 + 8), v26 != r13_6) {
                    addr_2da5_22:
                    rdx70 = *r13_6;
                    rax71 = quotearg_n_style_colon();
                    rax72 = xstrdup(rax71, 3, rdx70, rcx16, r8_15);
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
                    r12_12 = *r13_6;
                    rbp7 = rax72;
                    if (*reinterpret_cast<void***>(r12_12) != 45) 
                        goto addr_2dcf_62;
                    if (*reinterpret_cast<void***>(r12_12 + 1)) 
                        goto addr_2dcf_62;
                    r12_74 = randint_source;
                    eax75 = rpl_fcntl(1, 3, 1, 3);
                    rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
                    if (eax75 < 0) {
                        rax77 = fun_2630();
                        rax78 = fun_2540();
                        rcx16 = rbp7;
                        rdx31 = rax77;
                        rsi30 = *reinterpret_cast<void***>(rax78);
                        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                        fun_2890();
                        rsp69 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8 - 8 + 8 - 8 + 8);
                        eax79 = 0;
                    } else {
                        if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax75) + 1) & 4) {
                            rax80 = fun_2630();
                            rcx16 = rbp7;
                            rsi30 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                            rdx31 = rax80;
                            fun_2890();
                            rsp69 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8 - 8 + 8);
                            eax79 = 0;
                        } else {
                            rcx16 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp76) + 80);
                            rdx31 = r12_74;
                            rsi30 = rbp7;
                            *reinterpret_cast<unsigned char*>(&eax79) = do_wipefd(1, rsi30, rdx31, rcx16, r8_15);
                            rsp69 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8);
                        }
                    }
                    v24 = reinterpret_cast<unsigned char>(v24 & *reinterpret_cast<unsigned char*>(&eax79));
                }
                goto addr_2ea1_20;
                addr_2dcf_62:
                r14_81 = randint_source;
                eax82 = open_safer(r12_12, 0x101, rdx70, rcx16, r8_15);
                rsp83 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
                ebx84 = eax82;
                if (eax82 >= 0) 
                    goto addr_2deb_70;
                rax85 = fun_2540();
                rsp83 = rsp83 - 8 + 8;
                r8_15 = rax85;
                if (*reinterpret_cast<void***>(rax85) == 13 && (v86 && (v14 = rax85, eax87 = fun_2870(r12_12, 0x80, rdx70, rcx16), rsp83 = rsp83 - 8 + 8, r8_15 = v14, !eax87))) {
                    eax88 = open_safer(r12_12, 0x101, rdx70, rcx16, r8_15);
                    rsp83 = rsp83 - 8 + 8;
                    r8_15 = v14;
                    ebx84 = eax88;
                    if (eax88 >= 0) {
                        addr_2deb_70:
                        rcx29 = reinterpret_cast<void**>(rsp83 + 80);
                        rdx31 = r14_81;
                        rsi30 = rbp7;
                        al89 = do_wipefd(ebx84, rsi30, rdx31, rcx29, r8_15);
                        *reinterpret_cast<int32_t*>(&rdi90) = ebx84;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi90) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&v14) = al89;
                        eax91 = fun_2710(rdi90, rsi30, rdx31, rcx29, r8_15);
                        rsp69 = reinterpret_cast<unsigned char*>(rsp83 - 8 + 8 - 8 + 8);
                        if (eax91) {
                            rax92 = fun_2630();
                            rax93 = fun_2540();
                            rdx31 = rax92;
                            rsi30 = *reinterpret_cast<void***>(rax93);
                            *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                            fun_2890();
                            rsp69 = rsp69 - 8 + 8 - 8 + 8 - 8 + 8;
                            *reinterpret_cast<unsigned char*>(&v14) = 0;
                            continue;
                        } else {
                            if (!*reinterpret_cast<unsigned char*>(&v14)) 
                                continue;
                            if (1) 
                                break; else 
                                continue;
                        }
                    }
                }
                v14 = r8_15;
                rax94 = fun_2630();
                r8_15 = v14;
                rdx31 = rax94;
                rsi30 = *reinterpret_cast<void***>(r8_15);
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                fun_2890();
                rsp69 = reinterpret_cast<unsigned char*>(rsp83 - 8 + 8 - 8 + 8);
                *reinterpret_cast<unsigned char*>(&v14) = 0;
            }
            rax95 = xstrdup(r12_12, rsi30, rdx31, rcx29, r8_15);
            v34 = rax95;
            rax96 = last_component(rax95, rsi30, rdx31, rcx29, r8_15);
            rbx8 = rax96;
            rax97 = dir_name(rax95, rsi30, rdx31, rcx29, r8_15);
            rsi30 = reinterpret_cast<void**>(3);
            *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
            rdx31 = rax97;
            v68 = rax97;
            rax98 = quotearg_n_style_colon();
            rax99 = xstrdup(rax98, 3, rdx31, rcx29, r8_15);
            rsp27 = rsp69 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
            v47 = -1;
            v50 = rax99;
            if (1) {
                rsi30 = reinterpret_cast<void**>(0x10900);
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                eax100 = open_safer(v68, 0x10900, rdx31, rcx29, r8_15);
                rsp27 = rsp27 - 8 + 8;
                v47 = eax100;
            }
        } while (!v101);
    }
    addr_2ea1_20:
    eax102 = static_cast<uint32_t>(v24) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax103) = *reinterpret_cast<unsigned char*>(&eax102);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    rdx104 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (!rdx104) {
        return rax103;
    }
    addr_32ff_82:
    fun_2660();
    fun_2630();
    fun_2890();
    goto addr_3328_15;
    addr_32e0_37:
    fun_26e0("p", "src/shred.c", 0x3e8, "incname", 1);
    goto addr_32ff_82;
    while (1) {
        addr_335f_16:
        addr_332f_17:
        quotearg_n_style_colon();
        fun_2540();
        fun_2890();
    }
    addr_2b10_6:
    *reinterpret_cast<uint32_t*>(&rax105) = eax18 - 0x66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax105) <= 26) {
        goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xbba0) + reinterpret_cast<uint64_t>(rax105 * 4)))) + reinterpret_cast<unsigned char>(0xbba0);
    }
}

int64_t __libc_start_main = 0;

void fun_3373() {
    __asm__("cli ");
    __libc_start_main(0x2a30, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x10008;

void fun_2500(int64_t rdi);

int64_t fun_3413() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2500(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3453() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_3463() {
    __asm__("cli ");
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2850(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2730(void** rdi, struct s3* rsi, void** rdx, void** rcx);

int32_t fun_2570(void** rdi, void** rsi, void** rdx, ...);

struct s3* stderr = reinterpret_cast<struct s3*>(0);

void fun_2950(struct s3* rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void*** a8, int64_t a9, int64_t a10, int64_t a11, void*** a12);

void fun_4513(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    struct s3* r12_7;
    void** rax8;
    struct s3* r12_9;
    void** rax10;
    struct s3* r12_11;
    void** rax12;
    void** rax13;
    struct s3* r12_14;
    void** rax15;
    struct s3* r12_16;
    void** rax17;
    struct s3* r12_18;
    void** rax19;
    struct s3* r12_20;
    void** rax21;
    struct s3* r12_22;
    void** rax23;
    int32_t eax24;
    void** r13_25;
    void** rax26;
    void** rax27;
    int32_t eax28;
    void** rax29;
    void** rax30;
    void** rax31;
    int32_t eax32;
    void** rax33;
    struct s3* r15_34;
    void** rax35;
    void** rax36;
    void** rax37;
    struct s3* rdi38;
    void** r8_39;
    void** r9_40;
    int64_t v41;
    void*** v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    void*** v46;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2630();
            fun_2850(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2630();
            fun_2730(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2630();
            fun_2730(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2630();
            fun_2730(rax12, r12_11, 5, rcx6);
            rax13 = fun_2630();
            fun_2850(1, rax13, 3, rcx6);
            r12_14 = stdout;
            rax15 = fun_2630();
            fun_2730(rax15, r12_14, 5, rcx6);
            r12_16 = stdout;
            rax17 = fun_2630();
            fun_2730(rax17, r12_16, 5, rcx6);
            r12_18 = stdout;
            rax19 = fun_2630();
            fun_2730(rax19, r12_18, 5, rcx6);
            r12_20 = stdout;
            rax21 = fun_2630();
            fun_2730(rax21, r12_20, 5, rcx6);
            r12_22 = stdout;
            rax23 = fun_2630();
            fun_2730(rax23, r12_22, 5, rcx6);
            do {
                if (1) 
                    break;
                eax24 = fun_2750("shred", 0, 5, "shred", 0, 5);
            } while (eax24);
            r13_25 = v4;
            if (!r13_25) {
                rax26 = fun_2630();
                fun_2850(1, rax26, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax27 = fun_2840(5);
                if (!rax27 || (eax28 = fun_2570(rax27, "en_", 3, rax27, "en_", 3), !eax28)) {
                    rax29 = fun_2630();
                    r13_25 = reinterpret_cast<void**>("shred");
                    fun_2850(1, rax29, "https://www.gnu.org/software/coreutils/", "shred");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_25 = reinterpret_cast<void**>("shred");
                    goto addr_4900_9;
                }
            } else {
                rax30 = fun_2630();
                fun_2850(1, rax30, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax31 = fun_2840(5);
                if (!rax31 || (eax32 = fun_2570(rax31, "en_", 3, rax31, "en_", 3), !eax32)) {
                    addr_4806_11:
                    rax33 = fun_2630();
                    fun_2850(1, rax33, "https://www.gnu.org/software/coreutils/", "shred");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_25 == "shred")) {
                        r12_2 = reinterpret_cast<void**>(0xc303);
                    }
                } else {
                    addr_4900_9:
                    r15_34 = stdout;
                    rax35 = fun_2630();
                    fun_2730(rax35, r15_34, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_4806_11;
                }
            }
            rax36 = fun_2630();
            rcx6 = r12_2;
            fun_2850(1, rax36, r13_25, rcx6);
            addr_456e_14:
            fun_2930();
        }
    } else {
        rax37 = fun_2630();
        rdi38 = stderr;
        rcx6 = r12_2;
        fun_2950(rdi38, 1, rax37, rcx6, r8_39, r9_40, v41, v42, v43, v44, v45, v46);
        goto addr_456e_14;
    }
}

void fun_4933() {
    __asm__("cli ");
    goto usage;
}

void** fun_2650(void** rdi, ...);

uint32_t fun_2720(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_4943(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2650(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2570(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_49c3_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_4a10_6; else 
                    continue;
            } else {
                rax18 = fun_2650(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_4a40_8;
                if (v16 == -1) 
                    goto addr_49fe_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_49c3_5;
            } else {
                eax19 = fun_2720(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_49c3_5;
            }
            addr_49fe_10:
            v16 = rbx15;
            goto addr_49c3_5;
        }
    }
    addr_4a25_16:
    return v12;
    addr_4a10_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_4a25_16;
    addr_4a40_8:
    v12 = rbx15;
    goto addr_4a25_16;
}

int64_t fun_4a53(void** rdi, void*** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void*** rbp6;
    int64_t rbx7;
    int32_t eax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_4a98_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            eax8 = fun_2750(rdi5, r12_4, rdx);
            if (!eax8) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_4a98_2;
    }
    return rbx7;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_4ab3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2630();
    } else {
        fun_2630();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_2890;
}

void** quote(void** rdi, ...);

void fun_4b43(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void*** v8;
    void*** r12_9;
    void** r12_10;
    int64_t v11;
    int64_t rbp12;
    void** rbp13;
    int64_t v14;
    int64_t rbx15;
    struct s3* r14_16;
    void*** v17;
    void** rax18;
    void** r15_19;
    int64_t rbx20;
    uint32_t eax21;
    void** rax22;
    struct s3* rdi23;
    int64_t v24;
    int64_t v25;
    void** rax26;
    struct s3* rdi27;
    int64_t v28;
    int64_t v29;
    struct s3* rdi30;
    signed char* rax31;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    v11 = rbp12;
    rbp13 = rsi;
    v14 = rbx15;
    r14_16 = stderr;
    v17 = rdi;
    rax18 = fun_2630();
    fun_2730(rax18, r14_16, 5, rcx);
    r15_19 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
    if (r15_19) {
        do {
            if (!rbx20 || (eax21 = fun_2720(r13_7, rbp13, r12_10, r13_7, rbp13, r12_10), !!eax21)) {
                r13_7 = rbp13;
                rax22 = quote(r15_19, r15_19);
                rdi23 = stderr;
                fun_2950(rdi23, 1, "\n  - %s", rax22, r8, r9, v24, v17, v25, v14, v11, v8);
            } else {
                rax26 = quote(r15_19, r15_19);
                rdi27 = stderr;
                fun_2950(rdi27, 1, ", %s", rax26, r8, r9, v28, v17, v29, v14, v11, v8);
            }
            ++rbx20;
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r12_10));
            r15_19 = v17[rbx20 * 8];
        } while (r15_19);
    }
    rdi30 = stderr;
    rax31 = rdi30->f28;
    if (reinterpret_cast<uint64_t>(rax31) < reinterpret_cast<uint64_t>(rdi30->f30)) {
        rdi30->f28 = rax31 + 1;
        *rax31 = 10;
        return;
    }
}

int64_t argmatch(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_4c73(int64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_4cb7_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_4d2e_4;
        } else {
            addr_4d2e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_2750(rdi15, r14_9, rdx);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_4cb0_8;
    } else {
        goto addr_4cb0_8;
    }
    return rbx16;
    addr_4cb0_8:
    rax14 = -1;
    goto addr_4cb7_3;
}

struct s5 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_4d43(void** rdi, struct s5* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2720(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

struct s6 {
    unsigned char f0;
    unsigned char f1;
};

struct s6* fun_4da3(struct s6* rdi) {
    uint32_t edx2;
    struct s6* rax3;
    struct s6* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s6*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s6*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s6*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_4e03(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2650(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int64_t file_name = 0;

void fun_4e33(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4e43(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s3* rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2580(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4e53() {
    struct s3* rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    struct s3* rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2540(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2630();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4ee3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2890();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2580(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4ee3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2890();
    }
}

int64_t mdir_name();

void xalloc_die();

void fun_4f03() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_4f23(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void* rbp6;
    void** rbx7;
    void** rax8;
    void* rax9;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp6) + 4) = 0;
    rbx7 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp6) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax8 = last_component(rdi, rsi, rdx, rcx, r8);
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(rbx7));
    while (reinterpret_cast<uint64_t>(rax9) > reinterpret_cast<uint64_t>(rbp6) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<uint64_t>(rax9) + 0xffffffffffffffff) == 47) {
        rax9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax9) + 0xffffffffffffffff);
    }
    return;
}

void** fun_27c0(void** rdi);

void*** fun_4f63(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rbp6;
    void** rbx7;
    void** rax8;
    void** r12_9;
    void* rax10;
    uint32_t ebx11;
    void** rax12;
    void*** r8_13;
    void*** rax14;
    void** rax15;
    void*** rax16;

    __asm__("cli ");
    rbp6 = rdi;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx7) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax8 = last_component(rdi, rsi, rdx, rcx, r8);
    r12_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(rbp6));
    while (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(r12_9)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp6) + reinterpret_cast<unsigned char>(r12_9) + 0xffffffffffffffff) != 47) 
            goto addr_4fe0_4;
        --r12_9;
    }
    rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_9) ^ 1);
    ebx11 = *reinterpret_cast<uint32_t*>(&rax10) & 1;
    rax12 = fun_27c0(reinterpret_cast<unsigned char>(r12_9) + reinterpret_cast<uint64_t>(rax10) + 1);
    if (!rax12) {
        addr_500d_7:
        *reinterpret_cast<int32_t*>(&r8_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_13) + 4) = 0;
    } else {
        rax14 = fun_2780(rax12, rbp6, r12_9, rcx, r8);
        r8_13 = rax14;
        if (!*reinterpret_cast<signed char*>(&ebx11)) {
            *reinterpret_cast<int32_t*>(&r12_9) = 1;
            *reinterpret_cast<int32_t*>(&r12_9 + 4) = 0;
            goto addr_4fce_10;
        } else {
            *rax14 = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_9) = 1;
            *reinterpret_cast<int32_t*>(&r12_9 + 4) = 0;
            goto addr_4fce_10;
        }
    }
    addr_4fd3_12:
    return r8_13;
    addr_4fce_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_13) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    goto addr_4fd3_12;
    addr_4fe0_4:
    rax15 = fun_27c0(r12_9 + 1);
    if (!rax15) 
        goto addr_500d_7;
    rax16 = fun_2780(rax15, rbp6, r12_9, rcx, rax15);
    r8_13 = rax16;
    goto addr_4fce_10;
}

uint32_t fun_25d0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_5023(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8, int64_t r9, int64_t a7) {
    int64_t v8;
    void** rax9;
    uint32_t eax10;
    uint32_t r12d11;
    int32_t eax12;
    void** rdx13;
    uint32_t eax14;
    int1_t zf15;
    void* rax16;
    int64_t rax17;
    void** rsi18;
    int64_t rdi19;
    uint32_t eax20;
    int64_t rdi21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    void** r13d25;
    uint32_t eax26;
    void** rax27;
    int64_t rdi28;
    uint32_t eax29;
    uint32_t ecx30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int32_t ecx35;
    int64_t rax36;

    __asm__("cli ");
    v8 = rdx;
    rax9 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax10 = fun_25d0(rdi);
        r12d11 = eax10;
        goto addr_5124_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax12 = have_dupfd_cloexec_0;
        *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        if (eax12 < 0) {
            eax14 = fun_25d0(rdi);
            r12d11 = eax14;
            if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0) || (zf15 = have_dupfd_cloexec_0 == -1, !zf15)) {
                addr_5124_3:
                rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
                if (rax16) {
                    fun_2660();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax17) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    return rax17;
                }
            } else {
                addr_51d9_9:
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi19) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = fun_25d0(rdi19, rdi19);
                if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi18) = 2, *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi21) = r12d11, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx13) = eax20 | 1, *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0, eax22 = fun_25d0(rdi21, rdi21), eax22 == 0xffffffff)) {
                    rax23 = fun_2540();
                    *reinterpret_cast<uint32_t*>(&rdi24) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                    r12d11 = 0xffffffff;
                    r13d25 = *reinterpret_cast<void***>(rax23);
                    fun_2710(rdi24, rsi18, rdx13, rcx, r8);
                    *reinterpret_cast<void***>(rax23) = r13d25;
                    goto addr_5124_3;
                }
            }
        } else {
            eax26 = fun_25d0(rdi, rdi);
            r12d11 = eax26;
            if (reinterpret_cast<int32_t>(eax26) >= reinterpret_cast<int32_t>(0) || (rax27 = fun_2540(), *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax27) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_5124_3;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                eax29 = fun_25d0(rdi28);
                r12d11 = eax29;
                if (reinterpret_cast<int32_t>(eax29) < reinterpret_cast<int32_t>(0)) 
                    goto addr_5124_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_51d9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_5089_16;
    ecx30 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx30 > 10) 
        goto addr_508d_18;
    rax31 = 1 << *reinterpret_cast<unsigned char*>(&ecx30);
    if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x502)) {
            addr_508d_18:
            if (0) {
            }
        } else {
            addr_50d5_23:
            eax32 = fun_25d0(rdi);
            r12d11 = eax32;
            goto addr_5124_3;
        }
        eax33 = fun_25d0(rdi);
        r12d11 = eax33;
        goto addr_5124_3;
    }
    if (0) {
    }
    eax34 = fun_25d0(rdi);
    r12d11 = eax34;
    goto addr_5124_3;
    addr_5089_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_508d_18;
    ecx35 = *reinterpret_cast<int32_t*>(&rsi);
    rax36 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
    if (!(*reinterpret_cast<uint32_t*>(&rax36) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax36) & 0xa0a) 
            goto addr_50d5_23;
        goto addr_508d_18;
    }
}

int32_t fun_28c0();

void fd_safer(int64_t rdi);

void fun_5293() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_28c0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2660();
    } else {
        return;
    }
}

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char* f10;
};

struct s7* fun_25c0();

void fun_2880(void** rdi, void** rsi);

void fun_2770(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_5313(uint64_t rdi, void** rsi, uint32_t edx, uint64_t rcx, uint64_t r8) {
    void** v6;
    uint64_t v7;
    uint32_t v8;
    void** rax9;
    void** v10;
    uint32_t edx11;
    uint32_t v12;
    uint32_t eax13;
    uint32_t v14;
    uint32_t v15;
    struct s7* rax16;
    void** r15_17;
    void** rax18;
    void** r14_19;
    void** rbp20;
    int1_t cf21;
    unsigned char* v22;
    void** rax23;
    void*** rsp24;
    void** v25;
    uint64_t r8_26;
    uint64_t rdx27;
    uint64_t rcx28;
    uint64_t rax29;
    uint64_t rax30;
    uint64_t rdx31;
    uint64_t rax32;
    uint64_t rdx33;
    int64_t rdi34;
    uint32_t esi35;
    uint32_t r10d36;
    uint64_t v37;
    int64_t rbx38;
    uint64_t rax39;
    int1_t zf40;
    void** rax41;
    void*** rsp42;
    void** rdx43;
    void** r12_44;
    void** rbp45;
    void** r8_46;
    void** r13_47;
    void** rax48;
    void* rsp49;
    void** r12_50;
    void** rax51;
    void** v52;
    void* rsp53;
    uint32_t v54;
    void** rbp55;
    void** rbx56;
    unsigned char* r12_57;
    void** rax58;
    void** rsi59;
    uint64_t rcx60;
    uint64_t rdx61;
    uint64_t rax62;
    uint32_t eax63;
    void** rdx64;
    uint32_t ecx65;
    void* rax66;
    void* rax67;
    uint32_t tmp32_68;
    int1_t cf69;
    void** rbp70;
    void** rax71;
    uint64_t rax72;
    int1_t zf73;
    void** rax74;
    int1_t cf75;
    int1_t below_or_equal76;
    uint64_t rax77;
    void** rax78;
    uint64_t rax79;
    int1_t zf80;
    uint64_t r8_81;
    uint64_t r11_82;
    uint64_t rdx83;
    int64_t rcx84;
    uint64_t r9_85;
    int64_t rax86;
    int32_t eax87;
    int64_t rdx88;
    int64_t rax89;
    uint32_t edx90;
    uint32_t esi91;
    uint64_t rax92;
    int64_t rax93;
    uint32_t esi94;
    int64_t rdx95;
    uint32_t eax96;
    uint64_t rax97;
    uint64_t rdx98;
    uint64_t rdi99;
    uint64_t rax100;
    int32_t eax101;
    uint64_t rax102;
    void* rcx103;
    void* rax104;
    void* rax105;
    uint32_t eax106;
    uint32_t eax107;
    uint32_t edx108;
    void* rsi109;
    uint32_t edx110;
    uint32_t edx111;
    void* rax112;
    void* rsi113;
    void* rax114;
    void* rax115;
    void* rdi116;
    uint32_t eax117;
    uint32_t r9d118;
    uint32_t eax119;
    void* rdx120;
    uint32_t edx121;
    uint32_t edx122;

    __asm__("cli ");
    v6 = rsi;
    v7 = r8;
    v8 = edx;
    rax9 = g28;
    v10 = rax9;
    edx11 = edx & 32;
    v12 = edx11;
    eax13 = edx & 3;
    v14 = eax13;
    v15 = (eax13 - (eax13 + reinterpret_cast<uint1_t>(eax13 < eax13 + reinterpret_cast<uint1_t>(edx11 < 1))) & 0xffffffe8) + 0x400;
    rax16 = fun_25c0();
    r15_17 = rax16->f0;
    rax18 = fun_2650(r15_17);
    r14_19 = rax16->f8;
    rbp20 = rax18;
    cf21 = reinterpret_cast<uint64_t>(rax18 - 1) < 16;
    v22 = rax16->f10;
    if (!cf21) {
        rbp20 = reinterpret_cast<void**>(1);
    }
    if (!cf21) {
        r15_17 = reinterpret_cast<void**>(".");
    }
    rax23 = fun_2650(r14_19);
    rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(16)) {
        r14_19 = reinterpret_cast<void**>(0xc303);
    }
    v25 = v6 + 0x287;
    if (r8 > rcx) {
        if (!rcx || (r8_26 = v7 / rcx, !!(v7 % rcx))) {
            addr_53f4_9:
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
                __asm__("fadd dword [rip+0x6736]");
            }
        } else {
            rdx27 = rdi % r8_26;
            rcx28 = rdi / r8_26;
            rax29 = rdx27 + rdx27 * 4;
            rax30 = rax29 + rax29;
            rdx31 = rax30 % r8_26;
            rax32 = rax30 / r8_26;
            rdx33 = rdx31 + rdx31;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (r8_26 <= rdx33) {
                esi35 = 2;
                if (r8_26 < rdx33) {
                    esi35 = 3;
                }
            } else {
                esi35 = 0;
                *reinterpret_cast<unsigned char*>(&esi35) = reinterpret_cast<uint1_t>(!!rdx33);
            }
            r10d36 = v8 & 16;
            if (!r10d36) 
                goto addr_599b_17; else 
                goto addr_558c_18;
        }
    } else {
        if (rcx % r8) 
            goto addr_53f4_9;
        rcx28 = rcx / r8 * rdi;
        if (__intrinsic()) 
            goto addr_53f4_9;
        esi35 = 0;
        *reinterpret_cast<uint32_t*>(&rdi34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        r10d36 = v8 & 16;
        if (r10d36) 
            goto addr_558c_18; else 
            goto addr_599b_17;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(v7) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x6756]");
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) >= reinterpret_cast<int64_t>(0)) {
            addr_5431_24:
            __asm__("fmulp st1, st0");
            if (!(*reinterpret_cast<unsigned char*>(&v8) & 16)) {
                addr_5728_25:
                if (v14 == 1) 
                    goto addr_57ba_26;
                __asm__("fld tword [rip+0x6727]");
                __asm__("fcomip st0, st1");
                if (v14 <= 1) 
                    goto addr_57ba_26;
            } else {
                addr_543e_28:
                __asm__("fild dword [rsp+0x34]");
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                __asm__("fld st0");
                goto addr_5454_29;
            }
        } else {
            goto addr_56e0_31;
        }
    } else {
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) 
            goto addr_56e0_31; else 
            goto addr_5431_24;
    }
    __asm__("fld dword [rip+0x6715]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax39) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x66cd]");
    }
    zf40 = v14 == 0;
    if (zf40) 
        goto addr_5791_39;
    __asm__("fstp st1");
    goto addr_57ba_26;
    addr_5791_39:
    __asm__("fxch st0, st1");
    __asm__("fucomip st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf40) {
            addr_57ba_26:
            __asm__("fstp tword [rsp]");
            fun_29d0(v6, 1, -1, "%.0Lf");
            *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
            rax41 = fun_2650(v6, v6);
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            rdx43 = rax41;
            r12_44 = rax41;
            goto addr_57f8_43;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax39 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x6177]");
        goto addr_57ba_26;
    } else {
        goto addr_57ba_26;
    }
    addr_57f8_43:
    rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdx43));
    fun_2880(rbp45, v6);
    rsp24 = rsp42 - 8 + 8;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) + reinterpret_cast<unsigned char>(r12_44));
    while (1) {
        if (*reinterpret_cast<unsigned char*>(&v8) & 4) {
            addr_5620_49:
            r13_47 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax48 = fun_2650(r14_19, r14_19);
            rsp49 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            r12_50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<unsigned char>(rbp45));
            r15_17 = rax48;
            rax51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 80);
            v52 = rax51;
            fun_2770(rax51, rbp45, r12_50, 41);
            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            v54 = *reinterpret_cast<uint32_t*>(&rbx38);
            rbp55 = r8_46;
            rbx56 = r12_50;
            r12_57 = v22;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rax58) = *r12_57;
                *reinterpret_cast<int32_t*>(&rax58 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&rax58)) {
                    if (reinterpret_cast<unsigned char>(r13_47) > reinterpret_cast<unsigned char>(rbx56)) {
                        r13_47 = rbx56;
                    }
                    rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(r13_47));
                    rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rax58) > 0x7e) {
                        r13_47 = rbx56;
                        rsi59 = v52;
                        *reinterpret_cast<int32_t*>(&rbx56) = 0;
                        *reinterpret_cast<int32_t*>(&rbx56 + 4) = 0;
                    } else {
                        if (reinterpret_cast<unsigned char>(rax58) > reinterpret_cast<unsigned char>(rbx56)) {
                            rax58 = rbx56;
                        }
                        rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(rax58));
                        r13_47 = rax58;
                        rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                    }
                    ++r12_57;
                }
                rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp55) - reinterpret_cast<unsigned char>(r13_47));
                fun_2780(rbp45, rsi59, r13_47, 41, r8_46);
                rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
                if (!rbx56) 
                    break;
                rbp55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) - reinterpret_cast<unsigned char>(r15_17));
                fun_2780(rbp55, r14_19, r15_17, 41, r8_46);
                rsp53 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            }
            *reinterpret_cast<uint32_t*>(&rbx38) = v54;
        }
        addr_581c_63:
        if (!(*reinterpret_cast<unsigned char*>(&v8) & 0x80)) 
            goto addr_583f_64;
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 0xffffffff) {
            rcx60 = v7;
            if (rcx60 <= 1) {
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                goto addr_582c_68;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx61) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx61) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx38) = 1;
                *reinterpret_cast<int32_t*>(&rax62) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0;
                do {
                    rax62 = rax62 * rdx61;
                    if (rcx60 <= rax62) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
                } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
                eax63 = v8 & 0x100;
                if (!(v8 & 64)) 
                    goto addr_5a08_73;
            }
        } else {
            addr_582c_68:
            eax63 = v8 & 0x100;
            if (eax63 | *reinterpret_cast<uint32_t*>(&rbx38)) {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 64)) {
                    addr_5a00_75:
                    if (!*reinterpret_cast<uint32_t*>(&rbx38)) {
                        rdx64 = v25;
                        if (eax63) {
                            addr_5a4a_77:
                            *reinterpret_cast<void***>(rdx64) = reinterpret_cast<void**>(66);
                            v25 = rdx64 + 1;
                            goto addr_583f_64;
                        } else {
                            goto addr_583f_64;
                        }
                    } else {
                        addr_5a08_73:
                        rdx64 = v25 + 1;
                        if (v12 || *reinterpret_cast<uint32_t*>(&rbx38) != 1) {
                            rbx38 = *reinterpret_cast<int32_t*>(&rbx38);
                            ecx65 = *reinterpret_cast<unsigned char*>(0xbe48 + rbx38);
                            *reinterpret_cast<void***>(v25) = *reinterpret_cast<void***>(&ecx65);
                            if (eax63) {
                                *reinterpret_cast<uint32_t*>(&r8_46) = v12;
                                *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&r8_46)) {
                                    *reinterpret_cast<void***>(v25 + 1) = reinterpret_cast<void**>(0x69);
                                    rdx64 = v25 + 2;
                                    goto addr_5a4a_77;
                                }
                            }
                        } else {
                            *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0x6b);
                            if (eax63) 
                                goto addr_5a4a_77; else 
                                goto addr_5cfb_83;
                        }
                    }
                }
            } else {
                addr_583f_64:
                *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0);
                rax66 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
                if (rax66) 
                    goto addr_5edb_85; else 
                    break;
            }
        }
        *reinterpret_cast<signed char*>(v6 + 0x287) = 32;
        v25 = v6 + 0x288;
        goto addr_5a00_75;
        addr_5cfb_83:
        v25 = rdx64;
        goto addr_583f_64;
        addr_5edb_85:
        rax67 = fun_2660();
        rsp24 = rsp24 - 8 + 8;
        addr_5ee0_87:
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 4);
        addr_5cc9_88:
        *reinterpret_cast<signed char*>(r8_46 + 0xffffffffffffffff) = 49;
        rbp45 = r8_46 + 0xffffffffffffffff;
    }
    return rbp45;
    addr_5454_29:
    while (tmp32_68 = *reinterpret_cast<uint32_t*>(&rbx38) + 1, cf69 = tmp32_68 < *reinterpret_cast<uint32_t*>(&rbx38), *reinterpret_cast<uint32_t*>(&rbx38) = tmp32_68, !cf69) {
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) 
            goto addr_5466_91;
        __asm__("fstp st1");
        __asm__("fxch st0, st2");
    }
    __asm__("fstp st2");
    __asm__("fstp st2");
    addr_5474_94:
    r15_17 = rbp20 + 1;
    __asm__("fdivrp st1, st0");
    rbp70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp20 + 2) + reinterpret_cast<uint1_t>(v12 < 1));
    if (v14 == 1) {
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_29d0(v6, 1, -1, "%.1Lf");
        rax71 = fun_2650(v6, v6);
        rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        __asm__("fld tword [rsp+0x20]");
        rdx43 = rax71;
        if (reinterpret_cast<unsigned char>(rax71) > reinterpret_cast<unsigned char>(rbp70)) {
            __asm__("fld dword [rip+0x63ad]");
            __asm__("fmul st1, st0");
            goto addr_5ab1_97;
        }
    }
    __asm__("fld tword [rip+0x69cc]");
    __asm__("fcomip st0, st1");
    if (v14 <= 1) {
        __asm__("fld st0");
        goto addr_5910_100;
    }
    __asm__("fld dword [rip+0x69b6]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
    }
    v37 = rax72;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax72) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x696e]");
    }
    zf73 = v14 == 0;
    if (zf73) 
        goto addr_54f2_107;
    __asm__("fxch st0, st1");
    goto addr_5910_100;
    addr_54f2_107:
    __asm__("fxch st0, st1");
    __asm__("fucomi st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st1");
    } else {
        if (zf73) {
            addr_5910_100:
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fstp tword [rsp]");
            fun_29d0(v6, 1, -1, "%.1Lf");
            rax74 = fun_2650(v6, v6);
            rdx43 = rax74;
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            __asm__("fld tword [rsp+0x20]");
            cf75 = reinterpret_cast<unsigned char>(rdx43) < reinterpret_cast<unsigned char>(rbp70);
            below_or_equal76 = reinterpret_cast<unsigned char>(rdx43) <= reinterpret_cast<unsigned char>(rbp70);
            if (!below_or_equal76) {
                __asm__("fld dword [rip+0x6346]");
                __asm__("fmul st1, st0");
                goto addr_5b18_112;
            } else {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 8)) {
                    __asm__("fstp st0");
                    goto addr_597a_115;
                } else {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(rdx43) + 0xffffffffffffffff) == 48) {
                        __asm__("fld dword [rip+0x6226]");
                        cf75 = v14 < 1;
                        below_or_equal76 = v14 <= 1;
                        __asm__("fmul st1, st0");
                        if (v14 == 1) {
                            goto addr_5ab1_97;
                        }
                    } else {
                        __asm__("fstp st0");
                        goto addr_597a_115;
                    }
                }
            }
        } else {
            __asm__("fstp st1");
        }
    }
    rax77 = rax72 + 1;
    v37 = rax77;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax77) >= reinterpret_cast<int64_t>(0)) {
        __asm__("fxch st0, st1");
        goto addr_5910_100;
    } else {
        __asm__("fadd dword [rip+0x6933]");
        __asm__("fxch st0, st1");
        goto addr_5910_100;
    }
    addr_5b18_112:
    __asm__("fld tword [rip+0x6342]");
    __asm__("fcomip st0, st2");
    if (below_or_equal76) {
        addr_5ab1_97:
        __asm__("fdivp st1, st0");
        __asm__("fstp tword [rsp]");
        fun_29d0(v6, 1, -1, "%.0Lf");
        rax78 = fun_2650(v6, v6);
        r15_17 = reinterpret_cast<void**>(0x53b4);
        rsp42 = rsp42 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        rdx43 = rax78;
        r12_44 = rax78;
        goto addr_57f8_43;
    } else {
        __asm__("fld dword [rip+0x6330]");
        __asm__("fxch st0, st2");
        __asm__("fcomi st0, st2");
        if (!cf75) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fsubr st2, st0");
            __asm__("fxch st0, st2");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fxch st0, st1");
            rax79 = v37;
            __asm__("btc rax, 0x3f");
        } else {
            __asm__("fstp st2");
            __asm__("fxch st0, st1");
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld st0");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            rax79 = v37;
        }
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rax79) < reinterpret_cast<int64_t>(0)) {
            __asm__("fadd dword [rip+0x62e6]");
        }
        zf80 = v14 == 0;
        if (zf80) 
            goto addr_5b76_130;
    }
    __asm__("fstp st1");
    goto addr_5ba2_132;
    addr_5b76_130:
    __asm__("fucomi st0, st1");
    __asm__("fstp st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf80) {
            addr_5ba2_132:
            __asm__("fxch st0, st1");
            goto addr_5ab1_97;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax79 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x616c]");
        __asm__("fxch st0, st1");
        goto addr_5ab1_97;
    } else {
        goto addr_5ba2_132;
    }
    addr_597a_115:
    r12_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) - reinterpret_cast<unsigned char>(r15_17));
    goto addr_57f8_43;
    addr_5466_91:
    __asm__("fstp st2");
    __asm__("fstp st2");
    goto addr_5474_94;
    addr_56e0_31:
    __asm__("fadd dword [rip+0x676e]");
    __asm__("fmulp st1, st0");
    if (*reinterpret_cast<unsigned char*>(&v8) & 16) 
        goto addr_543e_28;
    goto addr_5728_25;
    addr_599b_17:
    *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
    goto addr_559f_140;
    addr_558c_18:
    *reinterpret_cast<uint32_t*>(&r8_81) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_81) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbx38) = 0;
    r11_82 = r8_81;
    if (r8_81 > rcx28) 
        goto addr_559f_140;
    do {
        rdx83 = rcx28 % r8_81;
        *reinterpret_cast<int32_t*>(&rcx84) = reinterpret_cast<int32_t>(esi35) >> 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx84) + 4) = 0;
        r9_85 = rcx28 / r8_81;
        *reinterpret_cast<int32_t*>(&rax86) = static_cast<int32_t>(rdx83 + rdx83 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
        eax87 = static_cast<int32_t>(rdi34 + rax86 * 2);
        *reinterpret_cast<uint32_t*>(&rdx88) = eax87 % *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx88) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax89) = eax87 / *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
        edx90 = static_cast<uint32_t>(rcx84 + rdx88 * 2);
        *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax89);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        rcx28 = r9_85;
        esi91 = esi35 + edx90;
        if (*reinterpret_cast<uint32_t*>(&r11_82) > edx90) {
            esi35 = reinterpret_cast<uint1_t>(!!esi91);
        } else {
            esi35 = static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r11_82) < esi91)) + 2;
        }
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (r8_81 > r9_85) 
            break;
    } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
    goto addr_559f_140;
    if (r9_85 > 9) {
        addr_559f_140:
        r8_46 = v25;
        if (v14 == 1) {
            rax92 = rcx28;
            *reinterpret_cast<uint32_t*>(&rax93) = *reinterpret_cast<uint32_t*>(&rax92) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax93) + 4) = 0;
            if (reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!(rax93 + esi35))) + *reinterpret_cast<uint32_t*>(&rdi34)) <= reinterpret_cast<int32_t>(5)) {
                goto addr_55d8_149;
            }
        } else {
            if (v14) 
                goto addr_55d8_149;
            esi94 = esi35 + *reinterpret_cast<uint32_t*>(&rdi34);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(esi94) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(esi94 == 0)) 
                goto addr_55d8_149;
        }
    } else {
        if (v14 != 1) {
            if (v14) 
                goto addr_5d9c_154;
            if (!esi35) 
                goto addr_5d9c_154; else 
                goto addr_5d27_156;
        }
        if (reinterpret_cast<int32_t>((*reinterpret_cast<uint32_t*>(&rax89) & 1) + esi35) > reinterpret_cast<int32_t>(2)) {
            addr_5d27_156:
            *reinterpret_cast<int32_t*>(&rdx95) = static_cast<int32_t>(rax89 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax89) == 9) {
                rcx28 = r9_85 + 1;
                if (r9_85 == 9) {
                    r8_46 = v25;
                    goto addr_5d6a_160;
                } else {
                    esi35 = 0;
                    goto addr_5da4_162;
                }
            } else {
                eax96 = static_cast<uint32_t>(rdx95 + 48);
                goto addr_5d36_164;
            }
        } else {
            addr_5d9c_154:
            if (*reinterpret_cast<uint32_t*>(&rax89)) {
                eax96 = *reinterpret_cast<uint32_t*>(&rax89) + 48;
                goto addr_5d36_164;
            } else {
                addr_5da4_162:
                r8_46 = v25;
                if (*reinterpret_cast<unsigned char*>(&v8) & 8) {
                    addr_5d6c_166:
                    *reinterpret_cast<uint32_t*>(&rdi34) = 0;
                    if (v14 == 1) {
                        goto addr_55d8_149;
                    }
                } else {
                    eax96 = 48;
                    goto addr_5d36_164;
                }
            }
        }
    }
    ++rcx28;
    if (!r10d36) 
        goto addr_55d8_149;
    *reinterpret_cast<uint32_t*>(&rax97) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    if (rax97 != rcx28) {
        goto addr_55d8_149;
    }
    if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) {
        addr_55d8_149:
        rbp45 = r8_46;
    } else {
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (*reinterpret_cast<unsigned char*>(&v8) & 8) 
            goto addr_5cc9_88;
        *reinterpret_cast<signed char*>(r8_46 + 0xffffffffffffffff) = 48;
        r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp20)));
        *reinterpret_cast<uint32_t*>(&rax67) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) 
            goto addr_5e70_175; else 
            goto addr_5e3e_176;
    }
    do {
        --rbp45;
        rdx98 = __intrinsic() >> 3;
        rdi99 = rdx98 + rdx98 * 4;
        rax100 = rcx28 - (rdi99 + rdi99);
        eax101 = *reinterpret_cast<int32_t*>(&rax100) + 48;
        *reinterpret_cast<void***>(rbp45) = *reinterpret_cast<void***>(&eax101);
        rax102 = rcx28;
        rcx28 = rdx98;
    } while (rax102 > 9);
    if (!(*reinterpret_cast<unsigned char*>(&v8) & 4)) 
        goto addr_581c_63; else 
        goto addr_5620_49;
    addr_5e70_175:
    rcx103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
    *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax104) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax104) - 8);
    rax105 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rcx103));
    r15_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax105));
    eax106 = *reinterpret_cast<int32_t*>(&rax105) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
    if (eax106 < 8) 
        goto addr_5cc9_88;
    eax107 = eax106 & 0xfffffff8;
    edx108 = 0;
    do {
        *reinterpret_cast<uint32_t*>(&rsi109) = edx108;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi109) + 4) = 0;
        edx108 = edx108 + 8;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx103) + reinterpret_cast<uint64_t>(rsi109)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rsi109));
    } while (edx108 < eax107);
    goto addr_5cc9_88;
    addr_5e3e_176:
    if (*reinterpret_cast<uint32_t*>(&rbp20) & 4) 
        goto addr_5ee0_87;
    if (!*reinterpret_cast<uint32_t*>(&rax67)) 
        goto addr_5cc9_88;
    edx110 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17));
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx110);
    if (!(*reinterpret_cast<unsigned char*>(&rax67) & 2)) 
        goto addr_5cc9_88;
    edx111 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 2);
    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 2) = *reinterpret_cast<int16_t*>(&edx111);
    goto addr_5cc9_88;
    addr_5d6a_160:
    esi35 = 0;
    goto addr_5d6c_166;
    addr_5d36_164:
    *reinterpret_cast<signed char*>(v6 + 0x286) = *reinterpret_cast<signed char*>(&eax96);
    *reinterpret_cast<uint32_t*>(&rax112) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v6 + 0x286) - reinterpret_cast<unsigned char>(rbp20));
    if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) {
        rsi113 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax114) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax114) - 8);
        rax115 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rsi113));
        rdi116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax115));
        eax117 = *reinterpret_cast<int32_t*>(&rax115) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
        if (eax117 >= 8) {
            r9d118 = eax117 & 0xfffffff8;
            eax119 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rdx120) = eax119;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx120) + 4) = 0;
                eax119 = eax119 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsi113) + reinterpret_cast<int64_t>(rdx120)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi116) + reinterpret_cast<int64_t>(rdx120));
            } while (eax119 < r9d118);
            goto addr_5d6a_160;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp20) & 4) {
            *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 4);
            goto addr_5d6a_160;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rax112) && (edx121 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17)), *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx121), !!(*reinterpret_cast<unsigned char*>(&rax112) & 2))) {
                edx122 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 2) = *reinterpret_cast<int16_t*>(&edx122);
                goto addr_5d6a_160;
            }
        }
    }
}

void** fun_2510(int64_t rdi, void*** rsi);

int64_t xstrtoumax(void** rdi, ...);

int64_t fun_5f13(void** rdi, void*** rsi, int64_t* rdx) {
    void*** r13_4;
    int64_t* rbp5;
    void** rbx6;
    void* rsp7;
    void** rax8;
    void** v9;
    void** rax10;
    void** rax11;
    void** r12d12;
    int64_t rax13;
    void** rax14;
    int64_t rax15;
    void*** rsi16;
    int64_t rdx17;
    int64_t rcx18;
    int32_t edx19;
    void** rcx20;
    void** v21;
    int64_t rdi22;
    int32_t edx23;
    void** rax24;
    uint64_t rax25;
    int64_t rax26;
    void* rdx27;

    __asm__("cli ");
    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16);
    rax8 = g28;
    v9 = rax8;
    if (rdi || ((rax10 = fun_2510("BLOCK_SIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax10, !!rax10) || (rax11 = fun_2510("BLOCKSIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax11, !!rax11))) {
        r12d12 = reinterpret_cast<void**>(0);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx6) == 39)) {
            ++rbx6;
            r12d12 = reinterpret_cast<void**>(4);
        }
        rax13 = argmatch(rbx6, 0xfae0, 0xbe40, 4);
        if (*reinterpret_cast<int32_t*>(&rax13) >= 0) 
            goto addr_5f76_5;
    } else {
        rax14 = fun_2510("POSIXLY_CORRECT", rsi);
        if (!rax14) {
            *rbp5 = 0x400;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *r13_4 = reinterpret_cast<void**>(0);
            goto addr_5f8a_8;
        } else {
            *rbp5 = 0x200;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *r13_4 = reinterpret_cast<void**>(0);
            goto addr_5f8a_8;
        }
    }
    rsi16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    rax15 = xstrtoumax(rbx6, rbx6);
    if (*reinterpret_cast<int32_t*>(&rax15)) {
        *r13_4 = reinterpret_cast<void**>(0);
        rdx17 = *rbp5;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx18) + 4) = 0;
        edx19 = static_cast<int32_t>(rcx18 - 48);
        rcx20 = v21;
        if (*reinterpret_cast<unsigned char*>(&edx19) <= 9) {
            goto addr_6007_14;
        }
        do {
            if (rcx20 == rbx6) 
                break;
            *reinterpret_cast<uint32_t*>(&rdi22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            ++rbx6;
            edx23 = static_cast<int32_t>(rdi22 - 48);
        } while (*reinterpret_cast<unsigned char*>(&edx23) > 9);
        goto addr_6007_14;
        if (*reinterpret_cast<signed char*>(rcx20 + 0xffffffffffffffff) == 66) 
            goto addr_60c0_18; else 
            goto addr_5fff_19;
    }
    addr_6024_20:
    if (!rdx17) {
        rax24 = fun_2510("POSIXLY_CORRECT", rsi16);
        rax25 = reinterpret_cast<unsigned char>(rax24) - (reinterpret_cast<unsigned char>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax26) = *reinterpret_cast<uint32_t*>(&rax25) & 0x200;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
        *rbp5 = rax26 + 0x200;
        *reinterpret_cast<int32_t*>(&rax15) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    }
    addr_5f8a_8:
    rdx27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rdx27) {
        fun_2660();
    } else {
        return rax15;
    }
    addr_60c0_18:
    r12d12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | 0x180);
    if (*reinterpret_cast<signed char*>(rcx20 + 0xfffffffffffffffe) != 0x69) {
        addr_6007_14:
        rdx17 = *rbp5;
        *r13_4 = r12d12;
        goto addr_6024_20;
    }
    addr_6003_25:
    r12d12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | 32);
    goto addr_6007_14;
    addr_5fff_19:
    *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d12) | 0x80);
    goto addr_6003_25;
    addr_5f76_5:
    *rbp5 = 1;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    *r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | *reinterpret_cast<uint32_t*>(0xbe40 + *reinterpret_cast<int32_t*>(&rax13) * 4));
    goto addr_5f8a_8;
}

struct s8 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_6103(uint64_t rdi, struct s8* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_2940(void** rdi, int64_t rsi, int64_t rdx, struct s3* rcx);

struct s9 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s9* fun_26b0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_6163(void** rdi) {
    struct s3* rcx2;
    void** rbx3;
    struct s9* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2940("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2530("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_26b0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2570(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_7903(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2540();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x10220;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_7943(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_7963(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10220);
    }
    *rdi = esi;
    return 0x10220;
}

int64_t fun_7983(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x10220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s10 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_79c3(struct s10* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0x10220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s11 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s11* fun_79e3(struct s11* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s11*>(0x10220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x29ea;
    if (!rdx) 
        goto 0x29ea;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x10220;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_7a23(void** rdi, void** rsi, void** rdx, void** rcx, struct s12* r8) {
    struct s12* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s12*>(0x10220);
    }
    rax7 = fun_2540();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x7a56);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_7aa3(void** rdi, void** rsi, void*** rdx, struct s13* rcx) {
    struct s13* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s13*>(0x10220);
    }
    rax6 = fun_2540();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x7ad1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x7b2c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_7b93() {
    __asm__("cli ");
}

void** g10098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_7ba3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** rdi11;
    void** rsi12;
    void** rdx13;
    void** rcx14;
    void** r8_15;
    void** rsi16;
    void** rdx17;
    void** rcx18;
    void** r8_19;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2520(rdi6, rsi7, rdx8, rcx9, r8_10);
        } while (rbx4 != rbp5);
    }
    rdi11 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi11 != 0x10120) {
        fun_2520(rdi11, rsi12, rdx13, rcx14, r8_15);
        g10098 = reinterpret_cast<void**>(0x10120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x10090) {
        fun_2520(r12_2, rsi16, rdx17, rcx18, r8_19);
        slotvec = reinterpret_cast<void**>(0x10090);
    }
    nslots = 1;
    return;
}

void fun_7c43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7c63() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7c73(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7c93(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_7cb3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29f0;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_7d43(void** rdi, int32_t esi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x29f5;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void** fun_7dd3(int32_t edi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x29fa;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2660();
    } else {
        return rax5;
    }
}

void** fun_7e63(int32_t edi, void** rsi, void** rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x29ff;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_7ef3(void** rdi, void** rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8320]");
    __asm__("movdqa xmm1, [rip+0x8328]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8311]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2660();
    } else {
        return rax10;
    }
}

void** fun_7f93(void** rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8280]");
    __asm__("movdqa xmm1, [rip+0x8288]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8271]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2660();
    } else {
        return rax9;
    }
}

void** fun_8033(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x81e0]");
    __asm__("movdqa xmm1, [rip+0x81e8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x81c9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2660();
    } else {
        return rax3;
    }
}

void** fun_80c3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8150]");
    __asm__("movdqa xmm1, [rip+0x8158]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8146]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2660();
    } else {
        return rax4;
    }
}

void** fun_8153(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2a04;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_81f3(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x801a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8012]");
    __asm__("movdqa xmm2, [rip+0x801a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2a09;
    if (!rdx) 
        goto 0x2a09;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void** fun_8293(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7f7a]");
    __asm__("movdqa xmm1, [rip+0x7f82]");
    __asm__("movdqa xmm2, [rip+0x7f8a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2a0e;
    if (!rdx) 
        goto 0x2a0e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2660();
    } else {
        return rax9;
    }
}

void** fun_8343(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7eca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7ec2]");
    __asm__("movdqa xmm2, [rip+0x7eca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2a13;
    if (!rsi) 
        goto 0x2a13;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2660();
    } else {
        return rax6;
    }
}

void** fun_83e3(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7e2a]");
    __asm__("movdqa xmm1, [rip+0x7e32]");
    __asm__("movdqa xmm2, [rip+0x7e3a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2a18;
    if (!rsi) 
        goto 0x2a18;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2660();
    } else {
        return rax7;
    }
}

void fun_8483() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8493(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_84b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_84d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** xmalloc(int64_t rdi, void* rsi);

void fun_84f3(void** rdi, void* rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = xmalloc(24, rsi);
    *reinterpret_cast<void***>(rax3) = rdi;
    *reinterpret_cast<void***>(rax3 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax3 + 8) = reinterpret_cast<void**>(0);
    return;
}

int64_t randread_new();

int64_t fun_8523() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = randread_new();
    if (!rax1) {
        return 0;
    }
}

int64_t fun_8543(int64_t* rdi) {
    __asm__("cli ");
    return *rdi;
}

struct s14 {
    int64_t f0;
    uint64_t f8;
    uint64_t f10;
};

uint64_t fun_8553(struct s14* rdi, uint64_t rsi) {
    struct s14* r14_3;
    uint64_t r13_4;
    uint64_t rbp5;
    int64_t rcx6;
    uint64_t r12_7;
    void** rax8;
    void** v9;
    uint64_t rbx10;
    void** r15_11;
    uint64_t rdx12;
    void** r8_13;
    uint64_t rax14;
    unsigned char v15;
    uint64_t rax16;
    uint64_t rdx17;
    uint64_t rdx18;
    void* rax19;

    __asm__("cli ");
    r14_3 = rdi;
    r13_4 = rsi + 1;
    rbp5 = rsi;
    rcx6 = rdi->f0;
    r12_7 = rdi->f8;
    rax8 = g28;
    v9 = rax8;
    rbx10 = rdi->f10;
    r15_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 + 16);
    while (1) {
        if (rbx10 >= rbp5) {
            if (rbx10 == rbp5) 
                break;
        } else {
            rdx12 = rbx10;
            *reinterpret_cast<int32_t*>(&r8_13) = 0;
            *reinterpret_cast<int32_t*>(&r8_13 + 4) = 0;
            do {
                ++r8_13;
                rdx12 = (rdx12 << 8) + 0xff;
            } while (rbp5 > rdx12);
            randread(rcx6, r15_11, r8_13);
            rcx6 = rcx6;
            do {
                *reinterpret_cast<uint32_t*>(&rax14) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                rbx10 = (rbx10 << 8) + 0xff;
                r12_7 = (r12_7 << 8) + rax14;
            } while (rbp5 > rbx10);
            if (rbx10 == rbp5) 
                break;
        }
        rax16 = rbx10 - rbp5;
        rdx17 = rax16 % r13_4;
        rdx18 = r12_7 % r13_4;
        if (r12_7 <= rbx10 - rdx17) 
            goto addr_8660_10;
        rbx10 = rdx17 + 0xffffffffffffffff;
        r12_7 = rdx18;
    }
    r14_3->f10 = 0;
    r14_3->f8 = 0;
    addr_8637_13:
    rax19 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax19) {
        fun_2660();
    } else {
        return r12_7;
    }
    addr_8660_10:
    r14_3->f8 = r12_7 / r13_4;
    r12_7 = rdx18;
    r14_3->f10 = rax16 / r13_4;
    goto addr_8637_13;
}

void fun_28b0(void** rdi, void** rsi, void** rdx);

void fun_8683(void** rdi) {
    __asm__("cli ");
    fun_28b0(rdi, 24, 0xffffffffffffffff);
    goto fun_2520;
}

int32_t randread_free(int64_t rdi);

void randint_free(int64_t* rdi);

int64_t fun_86b3(int64_t* rdi) {
    int64_t rdi2;
    int32_t eax3;
    void** rax4;
    void** r13d5;
    int64_t rax6;

    __asm__("cli ");
    rdi2 = *rdi;
    eax3 = randread_free(rdi2);
    rax4 = fun_2540();
    r13d5 = *reinterpret_cast<void***>(rax4);
    randint_free(rdi);
    *reinterpret_cast<int32_t*>(&rax6) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    *reinterpret_cast<void***>(rax4) = r13d5;
    return rax6;
}

void fun_86f3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    if (!rdi) 
        goto 0x2a1d;
    quote(rdi);
    rax2 = fun_2540();
    if (*reinterpret_cast<void***>(rax2)) {
        fun_2630();
    } else {
        fun_2630();
    }
    fun_2890();
    goto 0x2a1d;
}

void** fopen_safer();

void fun_2860(void** rdi, void*** rsi);

void* fun_2960(int64_t* rdi);

void isaac_seed(int64_t* rdi);

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx);

void** fun_8763(void** rdi, void* rsi) {
    void** rax3;
    void** r12_4;
    void* rbp5;
    void** rax6;
    int64_t* r13_7;
    int64_t* rbx8;
    int64_t* rbp9;
    void** rax10;
    void** rax11;
    void* rax12;
    void** rax13;
    void** r15d14;
    void** rbp15;
    void** rcx16;
    void** r8_17;

    __asm__("cli ");
    if (!rsi) {
        rax3 = xmalloc(".34", rsi);
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0);
        r12_4 = rax3;
        *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x86f0);
        *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
    } else {
        rbp5 = rsi;
        if (!rdi) {
            rax6 = xmalloc(".34", rsi);
            r12_4 = rax6;
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x86f0);
            r13_7 = reinterpret_cast<int64_t*>(r12_4 + 32);
            *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
            rbx8 = r13_7;
            *reinterpret_cast<void***>(r12_4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<uint64_t>(rbp5) > 0x800) {
                rbp5 = reinterpret_cast<void*>(0x800);
            }
            rbp9 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp5) + reinterpret_cast<uint64_t>(r13_7));
            if (reinterpret_cast<uint64_t>(r13_7) < reinterpret_cast<uint64_t>(rbp9)) 
                goto addr_8848_7; else 
                goto addr_883e_8;
        } else {
            rax10 = fopen_safer();
            if (!rax10) {
                *reinterpret_cast<int32_t*>(&r12_4) = 0;
                *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
            } else {
                rax11 = xmalloc(".34", "rb");
                r12_4 = rax11;
                *reinterpret_cast<void***>(rax11) = rax10;
                *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x86f0);
                *reinterpret_cast<void***>(r12_4 + 16) = rdi;
                if (reinterpret_cast<uint64_t>(rbp5) <= reinterpret_cast<uint64_t>("2.28")) {
                }
                fun_2860(rax10, r12_4 + 24);
            }
        }
    }
    addr_87dd_14:
    return r12_4;
    do {
        addr_8848_7:
        rax12 = fun_2960(rbx8);
        if (reinterpret_cast<int64_t>(rax12) >= reinterpret_cast<int64_t>(0)) {
            rbx8 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbx8) + reinterpret_cast<uint64_t>(rax12));
        } else {
            rax13 = fun_2540();
            r15d14 = *reinterpret_cast<void***>(rax13);
            if (r15d14 != 4) 
                goto addr_886e_17;
        }
    } while (reinterpret_cast<uint64_t>(rbp9) > reinterpret_cast<uint64_t>(rbx8));
    addr_88a8_19:
    isaac_seed(r13_7);
    goto addr_87dd_14;
    addr_886e_17:
    rbp15 = *reinterpret_cast<void***>(r12_4);
    fun_28b0(r12_4, ".34", ".34");
    fun_2520(r12_4, ".34", ".34", rcx16, r8_17);
    if (rbp15) {
        rpl_fclose(rbp15, ".34", ".34");
    }
    *reinterpret_cast<void***>(rax13) = r15d14;
    *reinterpret_cast<int32_t*>(&r12_4) = 0;
    *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
    goto addr_87dd_14;
    addr_883e_8:
    goto addr_88a8_19;
}

struct s15 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8903(struct s15* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f8 = rsi;
    return;
}

struct s16 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8913(struct s16* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f10 = rsi;
    return;
}

struct s17 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    int64_t f10;
    void** f18;
    signed char[2079] pad2104;
    void** f838;
};

void*** isaac_refill(int64_t rdi, void** rsi, void** rdx);

void*** fun_25f0(void** rdi, int64_t rsi, void** rdx, void** rcx);

void*** fun_8923(struct s17* rdi, void** rsi, void** rdx) {
    void** r14_4;
    struct s17* rbp5;
    void** rbx6;
    int1_t zf7;
    void** v8;
    void** rcx9;
    int64_t r12_10;
    void** rax11;
    void** v12;
    void** r13_13;
    void** rdx14;
    void** r8_15;
    void** rax16;
    void** rcx17;
    void** r12_18;
    void*** rax19;
    void** edx20;
    int64_t rdi21;
    void** r8_22;
    void** r13_23;
    void** r15_24;
    void* rbx25;
    void*** rax26;

    __asm__("cli ");
    r14_4 = rsi;
    rbp5 = rdi;
    rbx6 = rdx;
    zf7 = rdi->f0 == 0;
    v8 = rdi->f0;
    if (zf7) {
        rcx9 = rdi->f18;
        r12_10 = reinterpret_cast<int64_t>(rdi) + 32;
        rax11 = reinterpret_cast<void**>(&rdi->f838);
        v12 = rax11;
        if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rcx9)) {
            r13_13 = rdx;
            v12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rcx9) + 0x800);
        } else {
            do {
                rdx14 = rcx9;
                fun_2780(r14_4, reinterpret_cast<uint64_t>(0x800 - reinterpret_cast<unsigned char>(rcx9)) + reinterpret_cast<unsigned char>(v12), rdx14, rcx9, r8_15);
                r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rcx9));
                rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(rcx9));
                if (!(*reinterpret_cast<unsigned char*>(&r14_4) & 7)) 
                    goto addr_8a18_5;
                isaac_refill(r12_10, v12, rdx14);
                *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
                *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx6) > reinterpret_cast<unsigned char>(0x800));
            goto addr_8a0f_7;
        }
    } else {
        rax16 = fun_2540();
        rcx17 = v8;
        r12_18 = rax16;
        while (rax19 = fun_25f0(r14_4, 1, rbx6, rcx17), edx20 = *reinterpret_cast<void***>(r12_18), r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<uint64_t>(rax19)), rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<uint64_t>(rax19)), !!rbx6) {
            rdi21 = rbp5->f10;
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5->f0)) & 32)) {
                edx20 = reinterpret_cast<void**>(0);
            }
            *reinterpret_cast<void***>(r12_18) = edx20;
            rbp5->f8(rdi21, 1);
            rcx17 = rbp5->f0;
        }
        goto addr_8996_13;
    }
    addr_8a60_14:
    rax19 = fun_2780(r14_4, v12, r13_13, rcx9, r8_22);
    rbp5->f18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx9) - reinterpret_cast<unsigned char>(r13_13));
    addr_8996_13:
    return rax19;
    addr_8a18_5:
    r13_23 = rbx6;
    r15_24 = rbx6;
    rbx25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rbx6));
    *reinterpret_cast<uint32_t*>(&r13_13) = *reinterpret_cast<uint32_t*>(&r13_23) & 0x7ff;
    *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    do {
        r14_4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx25) - reinterpret_cast<unsigned char>(r15_24));
        if (r15_24 == r13_13) 
            break;
        rax26 = isaac_refill(r12_10, r14_4, rdx14);
        r15_24 = r15_24 - 0x800;
    } while (r15_24);
    goto addr_8a88_17;
    isaac_refill(r12_10, v12, rdx14);
    *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    goto addr_8a60_14;
    addr_8a88_17:
    rbp5->f18 = reinterpret_cast<void**>(0);
    return rax26;
    addr_8a0f_7:
    r13_13 = rbx6;
    goto addr_8a60_14;
}

int64_t fun_8ac3(void** rdi) {
    void** r12_2;
    void** rcx3;
    void** r8_4;

    __asm__("cli ");
    r12_2 = *reinterpret_cast<void***>(rdi);
    fun_28b0(rdi, ".34", 0xffffffffffffffff);
    fun_2520(rdi, ".34", 0xffffffffffffffff, rcx3, r8_4);
    if (!r12_2) {
        return 0;
    } else {
        goto rpl_fclose;
    }
}

struct s18 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    int64_t f18;
    signed char[992] pad1024;
    int64_t f400;
    int64_t f408;
    int64_t f410;
    int64_t f418;
    signed char[992] pad2048;
    uint64_t f800;
    int64_t f808;
    int64_t f810;
};

struct s19 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    signed char[1000] pad1024;
    uint64_t f400;
};

struct s20 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
};

void fun_8b13(struct s18* rdi, struct s19* rsi) {
    struct s18* rdx3;
    struct s19* r8_4;
    uint64_t rcx5;
    struct s18* r9_6;
    int64_t r11_7;
    struct s18* rax8;
    int64_t r11_9;
    struct s19* rdi10;
    uint64_t r10_11;
    uint64_t rsi12;
    int64_t rsi13;
    uint64_t rcx14;
    uint64_t rsi15;
    uint64_t rsi16;
    int64_t rsi17;
    uint64_t r10_18;
    uint64_t rbx19;
    uint64_t rcx20;
    uint64_t rsi21;
    int64_t rsi22;
    uint64_t rsi23;
    uint64_t rsi24;
    int64_t rsi25;
    uint64_t rbx26;
    uint64_t r11_27;
    uint64_t r10_28;
    uint64_t rcx29;
    int64_t rcx30;
    uint64_t rsi31;
    uint64_t rsi32;
    int64_t rsi33;
    uint64_t r11_34;
    int64_t r10_35;
    int64_t rsi36;
    int64_t rsi37;
    uint64_t rsi38;
    uint64_t rsi39;
    int64_t rsi40;
    struct s20* r8_41;
    struct s18* rdi42;
    uint64_t r9_43;
    uint64_t rsi44;
    int64_t rsi45;
    uint64_t rcx46;
    uint64_t rsi47;
    uint64_t rsi48;
    int64_t rsi49;
    uint64_t r9_50;
    uint64_t r11_51;
    uint64_t rcx52;
    uint64_t rsi53;
    int64_t rsi54;
    uint64_t rsi55;
    uint64_t rsi56;
    int64_t rsi57;
    uint64_t r11_58;
    uint64_t r10_59;
    uint64_t r9_60;
    uint64_t rcx61;
    int64_t rcx62;
    uint64_t rsi63;
    uint64_t rsi64;
    int64_t rsi65;
    uint64_t r10_66;
    int64_t r11_67;
    int64_t rsi68;
    int64_t rsi69;
    uint64_t rsi70;
    uint64_t rsi71;
    int64_t rsi72;

    __asm__("cli ");
    rdx3 = rdi;
    r8_4 = rsi;
    rcx5 = rdi->f800;
    r9_6 = reinterpret_cast<struct s18*>(&rdi->f400);
    r11_7 = rdi->f810 + 1;
    rax8 = rdx3;
    rdi->f810 = r11_7;
    r11_9 = r11_7 + rdi->f808;
    rdi10 = rsi;
    do {
        r10_11 = rax8->f0;
        rsi12 = r10_11;
        *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<uint32_t*>(&rsi12) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
        rcx14 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + rax8->f400);
        rsi15 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi13) + rcx14 + r11_9;
        rax8->f0 = rsi15;
        rsi16 = rsi15 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi17) = *reinterpret_cast<uint32_t*>(&rsi16) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        r10_18 = r10_11 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi17);
        rdi10->f0 = r10_18;
        rbx19 = rax8->f8;
        rcx20 = (rcx14 ^ rcx14 >> 5) + rax8->f408;
        rsi21 = rbx19;
        *reinterpret_cast<uint32_t*>(&rsi22) = *reinterpret_cast<uint32_t*>(&rsi21) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
        rsi23 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi22) + rcx20 + r10_18;
        rax8->f8 = rsi23;
        rsi24 = rsi23 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&rsi24) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        rbx26 = rbx19 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi25);
        rdi10->f8 = rbx26;
        r11_27 = rax8->f10;
        r10_28 = (rcx20 << 12 ^ rcx20) + rax8->f410;
        rcx29 = r11_27;
        *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<uint32_t*>(&rcx29) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx30) + 4) = 0;
        rsi31 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx30) + r10_28 + rbx26;
        rax8->f10 = rsi31;
        rsi32 = rsi31 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi33) = *reinterpret_cast<uint32_t*>(&rsi32) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi33) + 4) = 0;
        r11_34 = r11_27 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi33);
        rdi10->f10 = r11_34;
        r10_35 = rax8->f18;
        rcx5 = (r10_28 >> 33 ^ r10_28) + rax8->f418;
        rsi36 = r10_35;
        *reinterpret_cast<uint32_t*>(&rsi37) = *reinterpret_cast<uint32_t*>(&rsi36) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi37) + 4) = 0;
        rsi38 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi37) + rcx5 + r11_34;
        rax8 = reinterpret_cast<struct s18*>(&rax8->pad1024);
        rdi10 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(rdi10) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi38;
        rsi39 = rsi38 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi40) = *reinterpret_cast<uint32_t*>(&rsi39) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi40) + 4) = 0;
        r11_9 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi40) + r10_35;
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdi10) - 8) = r11_9;
    } while (rax8 != r9_6);
    r8_41 = reinterpret_cast<struct s20*>(&r8_4->f400);
    rdi42 = reinterpret_cast<struct s18*>(&rdx3->f800);
    do {
        r9_43 = rax8->f0;
        rsi44 = r9_43;
        *reinterpret_cast<uint32_t*>(&rsi45) = *reinterpret_cast<uint32_t*>(&rsi44) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi45) + 4) = 0;
        rcx46 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x400));
        rsi47 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi45) + rcx46 + r11_9;
        rax8->f0 = rsi47;
        rsi48 = rsi47 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi49) = *reinterpret_cast<uint32_t*>(&rsi48) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi49) + 4) = 0;
        r9_50 = r9_43 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi49);
        r8_41->f0 = r9_50;
        r11_51 = rax8->f8;
        rcx52 = (rcx46 ^ rcx46 >> 5) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f8);
        rsi53 = r11_51;
        *reinterpret_cast<uint32_t*>(&rsi54) = *reinterpret_cast<uint32_t*>(&rsi53) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0;
        rsi55 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi54) + rcx52 + r9_50;
        rax8->f8 = rsi55;
        rsi56 = rsi55 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi57) = *reinterpret_cast<uint32_t*>(&rsi56) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi57) + 4) = 0;
        r11_58 = r11_51 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi57);
        r8_41->f8 = r11_58;
        r10_59 = rax8->f10;
        r9_60 = (rcx52 << 12 ^ rcx52) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f0);
        rcx61 = r10_59;
        *reinterpret_cast<uint32_t*>(&rcx62) = *reinterpret_cast<uint32_t*>(&rcx61) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx62) + 4) = 0;
        rsi63 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx62) + r9_60 + r11_58;
        rax8->f10 = rsi63;
        rsi64 = rsi63 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi65) = *reinterpret_cast<uint32_t*>(&rsi64) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
        r10_66 = r10_59 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi65);
        r8_41->f10 = r10_66;
        r11_67 = rax8->f18;
        rcx5 = (r9_60 >> 33 ^ r9_60) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3e8);
        rsi68 = r11_67;
        *reinterpret_cast<uint32_t*>(&rsi69) = *reinterpret_cast<uint32_t*>(&rsi68) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi69) + 4) = 0;
        rsi70 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi69) + rcx5 + r10_66;
        rax8 = reinterpret_cast<struct s18*>(&rax8->pad1024);
        r8_41 = reinterpret_cast<struct s20*>(reinterpret_cast<int64_t>(r8_41) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi70;
        rsi71 = rsi70 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi72) = *reinterpret_cast<uint32_t*>(&rsi71) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi72) + 4) = 0;
        r11_9 = r11_67 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi72);
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r8_41) - 8) = r11_9;
    } while (rax8 != rdi42);
    rdx3->f800 = rcx5;
    rdx3->f808 = r11_9;
    return;
}

struct s21 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    signed char[1984] pad2048;
    int64_t f800;
    int64_t f808;
    int64_t f810;
};

void fun_8da3(struct s21* rdi) {
    struct s21* rdx2;
    struct s21* rax3;
    uint64_t rcx4;
    uint64_t r11_5;
    uint64_t rsi6;
    uint64_t r10_7;
    uint64_t r9_8;
    uint64_t r8_9;
    uint64_t r12_10;
    struct s21* rbp11;
    struct s21* rbx12;
    uint64_t rdi13;
    uint64_t rsi14;
    uint64_t rcx15;
    uint64_t r13_16;
    uint64_t r11_17;
    uint64_t r12_18;
    uint64_t rdi19;
    uint64_t rcx20;
    uint64_t r13_21;
    uint64_t r14_22;
    uint64_t rsi23;
    uint64_t r11_24;
    uint64_t r12_25;
    uint64_t rsi26;
    uint64_t rcx27;
    uint64_t rax28;
    uint64_t r11_29;
    uint64_t r12_30;
    uint64_t rdi31;
    uint64_t r8_32;
    uint64_t rcx33;
    uint64_t rax34;
    uint64_t rsi35;
    uint64_t r11_36;
    uint64_t r12_37;

    __asm__("cli ");
    rdx2 = rdi;
    rax3 = rdi;
    rcx4 = 0x98f5704f6c44c0ab;
    r11_5 = 0x48fe4a0fa5a09315;
    rsi6 = 0x82f053db8355e0ce;
    r10_7 = 0xb29b2e824a595524;
    r9_8 = 0x8c0ea5053d4712a0;
    r8_9 = 0xb9f8b322c73ac862;
    r12_10 = 0xae985bf2cbfc89ed;
    rbp11 = rdi;
    rbx12 = reinterpret_cast<struct s21*>(&rdi->f800);
    rdi13 = 0x647c4677a2884b7c;
    do {
        rsi14 = rsi6 + rax3->f20;
        rcx15 = rcx4 + rax3->f38;
        r13_16 = rax3->f0 - rsi14 + rdi13;
        r11_17 = r11_5 + rax3->f28 ^ rcx15 >> 9;
        r12_18 = r12_10 + rax3->f30 ^ r13_16 << 9;
        rdi19 = rax3->f8 - r11_17 + r8_9;
        rcx20 = rdi19 >> 23 ^ rcx15 + r13_16;
        r13_21 = rax3->f10 - r12_18 + r9_8;
        r14_22 = rax3->f18 - rcx20 + r10_7;
        rdi13 = r13_21 << 15 ^ r13_16 + rdi19;
        rsi23 = rsi14 - rdi13;
        rax3->f0 = rdi13;
        r8_9 = r14_22 >> 14 ^ rdi19 + r13_21;
        r11_24 = r11_17 - r8_9;
        rax3->f8 = r8_9;
        r9_8 = rsi23 << 20 ^ r13_21 + r14_22;
        r12_25 = r12_18 - r9_8;
        rax3->f10 = r9_8;
        r10_7 = r11_24 >> 17 ^ r14_22 + rsi23;
        r11_5 = r11_24 + r12_25;
        rcx4 = rcx20 - r10_7;
        rax3->f18 = r10_7;
        rax3 = reinterpret_cast<struct s21*>(&rax3->pad2048);
        rsi6 = r12_25 << 14 ^ rsi23 + r11_24;
        r12_10 = r12_25 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 8) = rcx4;
    } while (rbx12 != rax3);
    do {
        rsi26 = rsi6 + rdx2->f20;
        rcx27 = rcx4 + rdx2->f38;
        rax28 = rdx2->f0 - rsi26 + rdi13;
        r11_29 = r11_5 + rdx2->f28 ^ rcx27 >> 9;
        r12_30 = r12_10 + rdx2->f30 ^ rax28 << 9;
        rdi31 = rdx2->f8 - r11_29 + r8_9;
        r8_32 = rdx2->f10 - r12_30 + r9_8;
        rcx33 = rdi31 >> 23 ^ rcx27 + rax28;
        rax34 = rdx2->f18 - rcx33 + r10_7;
        rdi13 = r8_32 << 15 ^ rax28 + rdi31;
        rsi35 = rsi26 - rdi13;
        rdx2->f0 = rdi13;
        r8_9 = rax34 >> 14 ^ rdi31 + r8_32;
        r11_36 = r11_29 - r8_9;
        rdx2->f8 = r8_9;
        r9_8 = rsi35 << 20 ^ r8_32 + rax34;
        r12_37 = r12_30 - r9_8;
        rdx2->f10 = r9_8;
        r10_7 = r11_36 >> 17 ^ rax34 + rsi35;
        r11_5 = r11_36 + r12_37;
        rcx4 = rcx33 - r10_7;
        rdx2->f18 = r10_7;
        rdx2 = reinterpret_cast<struct s21*>(&rdx2->pad2048);
        rsi6 = r12_37 << 14 ^ rsi35 + r11_36;
        r12_10 = r12_37 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 8) = rcx4;
    } while (rbx12 != rdx2);
    rbp11->f810 = 0;
    rbp11->f808 = 0;
    rbp11->f800 = 0;
    return;
}

int32_t fun_27f0();

int32_t fun_29b0(int64_t rdi, void** rsi, void* rdx, int64_t rcx);

int32_t fun_2910(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

int64_t fun_9003(int32_t edi, void** rsi, int32_t edx, void** rcx, int32_t r8d) {
    void** rax6;
    int32_t eax7;
    int32_t r12d8;
    void** rax9;
    void*** rsp10;
    void** r8_11;
    int64_t rax12;
    uint32_t edx13;
    uint32_t r9d14;
    void* rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    uint32_t r9d19;
    int64_t rdi20;
    int32_t eax21;
    int64_t rdi22;
    int32_t eax23;
    uint32_t r9d24;
    int64_t rdi25;
    int32_t eax26;
    uint32_t v27;
    uint32_t v28;
    int64_t rdx29;
    int64_t rdi30;
    int32_t eax31;
    uint32_t v32;
    uint32_t v33;

    __asm__("cli ");
    rax6 = g28;
    eax7 = fun_27f0();
    r12d8 = eax7;
    if (eax7 >= 0 || (rax9 = fun_2540(), rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8), r8_11 = rax9, *reinterpret_cast<void***>(&rax12) = *reinterpret_cast<void***>(rax9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0, edx13 = static_cast<uint32_t>(rax12 - 22) & 0xffffffef, *reinterpret_cast<unsigned char*>(&edx13) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!edx13)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax12) == 95)))), r9d14 = edx13, !!*reinterpret_cast<unsigned char*>(&edx13))) {
        addr_90b0_2:
        rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax15) {
            fun_2660();
        } else {
            *reinterpret_cast<int32_t*>(&rax16) = r12d8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            return rax16;
        }
    } else {
        if (!r8d) {
            addr_90f6_6:
            rax17 = fun_2650(rsi, rsi);
            rax18 = fun_2650(rcx, rcx);
            rsp10 = rsp10 - 8 + 8 - 8 + 8;
            if (!rax17) 
                goto addr_9135_7;
            r9d19 = *reinterpret_cast<unsigned char*>(&r9d14);
            if (!rax18) 
                goto addr_9135_7;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) == 47) 
                goto addr_9150_10;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff) != 47) 
                goto addr_9135_7;
        } else {
            if (r8d != 1) {
                *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(95);
                r12d8 = -1;
                goto addr_90b0_2;
            } else {
                *reinterpret_cast<int32_t*>(&rdi20) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_29b0(rdi20, rcx, rsp10 + 0xa0, 0x100);
                rsp10 = rsp10 - 8 + 8;
                r8_11 = r8_11;
                if (!eax21 || *reinterpret_cast<void***>(r8_11) == 75) {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(17);
                    r12d8 = -1;
                    goto addr_90b0_2;
                } else {
                    if (*reinterpret_cast<void***>(r8_11) != 2) 
                        goto addr_90a5_17;
                    r9d14 = 1;
                    goto addr_90f6_6;
                }
            }
        }
    }
    addr_9150_10:
    *reinterpret_cast<int32_t*>(&rdi22) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
    eax23 = fun_29b0(rdi22, rsi, rsp10 + 16, 0x100);
    rsp10 = rsp10 - 8 + 8;
    if (eax23) {
        addr_90a5_17:
        r12d8 = -1;
        goto addr_90b0_2;
    } else {
        r9d24 = *reinterpret_cast<unsigned char*>(&r9d19);
        if (!*reinterpret_cast<signed char*>(&r9d24)) {
            *reinterpret_cast<int32_t*>(&rdi25) = edx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
            eax26 = fun_29b0(rdi25, rcx, rsp10 + 0xa0, 0x100);
            if (!eax26) {
                if ((v27 & 0xf000) == 0x4000) {
                    if ((v28 & 0xf000) == 0x4000) {
                        addr_9135_7:
                        *reinterpret_cast<int32_t*>(&rdx29) = edx;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi30) = edi;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
                        eax31 = fun_2910(rdi30, rsi, rdx29, rcx);
                        r12d8 = eax31;
                        goto addr_90b0_2;
                    } else {
                        *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(21);
                        r12d8 = -1;
                        goto addr_90b0_2;
                    }
                } else {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(20);
                    goto addr_90a5_17;
                }
            } else {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r8_11) == 2) && (v32 & 0xf000) == 0x4000) {
                    goto addr_9135_7;
                }
            }
        } else {
            if ((v33 & 0xf000) == 0x4000) 
                goto addr_9135_7;
            *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(2);
            r12d8 = -1;
            goto addr_90b0_2;
        }
    }
}

int32_t dup_safer(int64_t rdi);

int64_t fun_9263(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    int32_t eax6;
    void** rax7;
    int64_t rdi8;
    void** r13d9;
    int64_t rax10;
    int64_t rax11;

    __asm__("cli ");
    if (*reinterpret_cast<uint32_t*>(&rdi) <= 2) {
        eax6 = dup_safer(rdi);
        rax7 = fun_2540();
        *reinterpret_cast<uint32_t*>(&rdi8) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        r13d9 = *reinterpret_cast<void***>(rax7);
        fun_2710(rdi8, rsi, rdx, rcx, r8);
        *reinterpret_cast<int32_t*>(&rax10) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<void***>(rax7) = r13d9;
        return rax10;
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    }
}

struct s22 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void*** f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    void*** f40;
};

void fun_2760(int64_t rdi, struct s3* rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_92c3(struct s3* rdi, void** rsi, void** rdx, void** rcx, struct s22* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void*** v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;
    void*** v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    void*** v19;
    void** rax20;
    int64_t v21;
    void*** v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    void*** v26;
    void** rax27;
    int64_t v28;
    void*** v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    void*** v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    void*** rcx37;
    int64_t r15_38;
    void*** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2950(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2950(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2630();
    fun_2950(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2760(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2630();
    fun_2950(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2760(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2630();
        fun_2950(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xc5a8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xc5a8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_9733() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s23 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_9753(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s23* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s23* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2660();
    } else {
        return;
    }
}

void fun_97f3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_9896_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_98a0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2660();
    } else {
        return;
    }
    addr_9896_5:
    goto addr_98a0_7;
}

void fun_98d3() {
    struct s3* rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2760(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2630();
    fun_2850(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2630();
    fun_2850(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2630();
    goto fun_2850;
}

int64_t fun_2970();

void fun_9973() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2970();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25b0();

void fun_9993(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_99d3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_27c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_99f3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_27c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9a13(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_27c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2810();

void fun_9a33(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2810();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9a63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2810();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9a93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25b0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9ad3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_25b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9b13(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9b43(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_25b0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9b93(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_25b0();
            if (rax5) 
                break;
            addr_9bdd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_9bdd_5;
        rax8 = fun_25b0();
        if (rax8) 
            goto addr_9bc6_9;
        if (rbx4) 
            goto addr_9bdd_5;
        addr_9bc6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_9c23(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_25b0();
            if (rax8) 
                break;
            addr_9c6a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_9c6a_5;
        rax11 = fun_25b0();
        if (rax11) 
            goto addr_9c52_9;
        if (!rbx6) 
            goto addr_9c52_9;
        if (r12_4) 
            goto addr_9c6a_5;
        addr_9c52_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_9cb3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_9d5d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_9d70_10:
                *r12_8 = 0;
            }
            addr_9d10_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_9d36_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_9d84_14;
            if (rcx10 <= rsi9) 
                goto addr_9d2d_16;
            if (rsi9 >= 0) 
                goto addr_9d84_14;
            addr_9d2d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_9d84_14;
            addr_9d36_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2810();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_9d84_14;
            if (!rbp13) 
                break;
            addr_9d84_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_9d5d_9;
        } else {
            if (!r13_6) 
                goto addr_9d70_10;
            goto addr_9d10_11;
        }
    }
}

int64_t fun_2740();

void fun_9db3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2740();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9de3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2740();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9e13() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2740();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9e33() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2740();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9e53(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_27c0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2780;
    }
}

void fun_9e93(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_27c0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2780;
    }
}

struct s24 {
    signed char[1] pad1;
    void** f1;
};

void fun_9ed3(int64_t rdi, struct s24* rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_27c0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_2780;
    }
}

void fun_9f13(void** rdi) {
    void** rax2;
    void** rax3;

    __asm__("cli ");
    rax2 = fun_2650(rdi);
    rax3 = fun_27c0(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2780;
    }
}

void fun_9f53() {
    void** rdi1;

    __asm__("cli ");
    fun_2630();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2890();
    fun_2530(rdi1);
}

uint64_t fun_9f93(void** rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void** rax5;
    int64_t rax6;
    void** rax7;
    void** r12_8;
    uint64_t v9;
    void* rax10;
    void** rax11;

    __asm__("cli ");
    rax5 = g28;
    rax6 = xstrtoumax(rdi, rdi);
    if (*reinterpret_cast<int32_t*>(&rax6)) {
        rax7 = fun_2540();
        r12_8 = rax7;
        if (*reinterpret_cast<int32_t*>(&rax6) == 1) {
            addr_a030_3:
            *reinterpret_cast<void***>(r12_8) = reinterpret_cast<void**>(75);
        } else {
            if (*reinterpret_cast<int32_t*>(&rax6) == 3) {
                *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2660();
            } else {
                return v9;
            }
        }
        rax11 = fun_2540();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_a030_3;
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(34);
    }
    quote(rdi);
    if (*reinterpret_cast<void***>(r12_8) != 22) 
        goto addr_a04c_13;
    while (1) {
        addr_a04c_13:
        if (!1) {
        }
        fun_2890();
    }
}

int64_t xnumtoumax(void** rdi, ...);

int64_t fun_a0a3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rax7;

    __asm__("cli ");
    rax7 = xnumtoumax(rdi, rdi);
    return rax7;
}

void** fun_29c0(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_28f0(void** rdi);

int64_t fun_a0d3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    struct s4* rax26;
    int64_t rax27;
    struct s4* rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    struct s4* rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_26e0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax", r8);
        do {
            fun_2660();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_a444_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_a444_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_a18d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_a195_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2540();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_29c0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_a1cb_21;
    rsi = r15_14;
    rax24 = fun_28f0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2690(r13_18), r8 = r8, rax26 == 0))) {
            addr_a1cb_21:
            r12d11 = 4;
            goto addr_a195_13;
        } else {
            addr_a209_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2690(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xc658 + rbp33 * 4) + 0xc658;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_a1cb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_a18d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_a18d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2690(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_a209_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_a195_13;
}

int64_t fun_2590();

int64_t fun_a503(void** rdi, void** rsi, void** rdx) {
    int64_t rax4;
    uint32_t ebx5;
    int64_t rax6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    rax4 = fun_2590();
    ebx5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax6 = rpl_fclose(rdi, rsi, rdx);
    if (ebx5) {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            addr_a55e_3:
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            rax7 = fun_2540();
            *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            if (rax4) 
                goto addr_a55e_3;
            rax8 = fun_2540();
            *reinterpret_cast<int32_t*>(&rax6) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax8) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    }
    return rax6;
}

uint32_t fun_27a0(void** rdi);

int32_t fun_2800(void** rdi);

int32_t rpl_fflush(void** rdi);

int64_t fun_2610(void** rdi);

int64_t fun_a573(void** rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint32_t eax4;
    int64_t rdi5;
    void** rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_27a0(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_2800(rdi);
        if (!(eax3 && (eax4 = fun_27a0(rdi), *reinterpret_cast<uint32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_26d0(rdi5, rdi5), reinterpret_cast<int1_t>(rax6 == 0xffffffffffffffff)) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2540();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2610(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2610;
}

void rpl_fseeko(void** rdi);

void fun_a603(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2800(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void** fun_28e0();

void** fun_2830(int64_t rdi, void** rsi);

void** fun_a653() {
    void** rax1;
    void** r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    void** rax6;
    void** rdi7;
    void** r13d8;
    void** rsi9;
    void** rdx10;
    void** rsi11;
    void** rdx12;
    int64_t rax13;
    void** rsi14;
    void** rsi15;
    int64_t rdi16;
    void** rax17;
    void** rax18;
    int64_t rdi19;
    void** r12d20;
    void** rdx21;
    void** rcx22;
    void** r8_23;

    __asm__("cli ");
    rax1 = fun_28e0();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_a676_2;
    eax3 = fun_27a0(rax1);
    if (eax3 > 2) 
        goto addr_a676_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_2540();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        r13d8 = *reinterpret_cast<void***>(rax6);
        rpl_fclose(rdi7, rsi9, rdx10);
        *reinterpret_cast<void***>(rax6) = r13d8;
        goto addr_a676_2;
    } else {
        rax13 = rpl_fclose(r12_2, rsi11, rdx12);
        if (!*reinterpret_cast<int32_t*>(&rax13)) {
            rsi14 = rsi15;
            *reinterpret_cast<int32_t*>(&rdi16) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
            rax17 = fun_2830(rdi16, rsi14);
            r12_2 = rax17;
            if (rax17) {
                addr_a676_2:
                return r12_2;
            }
        }
        rax18 = fun_2540();
        *reinterpret_cast<int32_t*>(&rdi19) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
        r12d20 = *reinterpret_cast<void***>(rax18);
        fun_2710(rdi19, rsi14, rdx21, rcx22, r8_23);
        *reinterpret_cast<void***>(rax18) = r12d20;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        goto addr_a676_2;
    }
}

int64_t fun_a6f3(void** rdi, int64_t rsi, int32_t edx) {
    uint32_t eax4;
    int64_t rdi5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<int64_t*>(rdi + 40) != *reinterpret_cast<int64_t*>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_27a0(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_26d0(rdi5, rdi5);
        if (rax6 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<void***>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_27e0(int64_t rdi);

signed char* fun_a773() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_27e0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2680(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_a7b3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2680(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2660();
    } else {
        return r12_7;
    }
}

void fun_a843() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_a863() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2660();
    } else {
        return rax3;
    }
}

int64_t fun_a8e3(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    int32_t r13d7;
    void** rax8;
    int64_t rax9;

    __asm__("cli ");
    rax6 = fun_2840(rdi);
    if (!rax6) {
        r13d7 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax8 = fun_2650(rax6);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax8)) {
            fun_2780(rsi, rax6, rax8 + 1, rcx, r8);
            return 0;
        } else {
            r13d7 = 34;
            if (rdx) {
                fun_2780(rsi, rax6, rdx + 0xffffffffffffffff, rcx, r8);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax9) = r13d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

void fun_a993() {
    __asm__("cli ");
    goto fun_2840;
}

void fun_a9a3() {
    __asm__("cli ");
}

void fun_a9b7() {
    __asm__("cli ");
    return;
}

void** optarg = reinterpret_cast<void**>(0);

void fun_2b30() {
    void** rdx1;
    int32_t eax2;

    rdx1 = optarg;
    if (__return_address()) {
        eax2 = fun_2750(__return_address(), rdx1, rdx1);
        if (eax2) 
            goto 0x3304;
    }
    goto 0x2ae0;
}

void fun_2b60() {
    goto 0x2ae0;
}

void fun_2be0() {
    void** rdi1;

    fun_2630();
    rdi1 = optarg;
    xnumtoumax(rdi1);
    goto 0x2ae0;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2990(int64_t rdi, void** rsi);

uint32_t fun_2980(void** rdi, void** rsi);

void fun_6395() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2630();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2630();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2650(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_6693_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_6693_22; else 
                            goto addr_6a8d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_6b4d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_6ea0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_6690_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_6690_30; else 
                                goto addr_6eb9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2650(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_6ea0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2720(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_6ea0_28; else 
                            goto addr_653c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_7000_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_6e80_40:
                        if (r11_27 == 1) {
                            addr_6a0d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_6fc8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_6647_44;
                            }
                        } else {
                            goto addr_6e90_46;
                        }
                    } else {
                        addr_700f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_6a0d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_6693_22:
                                if (v47 != 1) {
                                    addr_6be9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2650(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_6c34_54;
                                    }
                                } else {
                                    goto addr_66a0_56;
                                }
                            } else {
                                addr_6645_57:
                                ebp36 = 0;
                                goto addr_6647_44;
                            }
                        } else {
                            addr_6e74_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_700f_47; else 
                                goto addr_6e7e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_6a0d_41;
                        if (v47 == 1) 
                            goto addr_66a0_56; else 
                            goto addr_6be9_52;
                    }
                }
                addr_6701_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_6598_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_65bd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_68c0_65;
                    } else {
                        addr_6729_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_6f78_67;
                    }
                } else {
                    goto addr_6720_69;
                }
                addr_65d1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_661c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_6f78_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_661c_81;
                }
                addr_6720_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_65bd_64; else 
                    goto addr_6729_66;
                addr_6647_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_66ff_91;
                if (v22) 
                    goto addr_665f_93;
                addr_66ff_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6701_62;
                addr_6c34_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_73bb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_742b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_722f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2990(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2980(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_6d2e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_66ec_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_6d38_112;
                    }
                } else {
                    addr_6d38_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_6e09_114;
                }
                addr_66f8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_66ff_91;
                while (1) {
                    addr_6e09_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_7317_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_6d76_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_7325_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_6df7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_6df7_128;
                        }
                    }
                    addr_6da5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_6df7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_6d76_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6da5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_661c_81;
                addr_7325_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6f78_67;
                addr_73bb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_6d2e_109;
                addr_742b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_6d2e_109;
                addr_66a0_56:
                rax93 = fun_29c0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_66ec_110;
                addr_6e7e_59:
                goto addr_6e80_40;
                addr_6b4d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_6693_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_66f8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6645_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_6693_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_6b92_160;
                if (!v22) 
                    goto addr_6f67_162; else 
                    goto addr_7173_163;
                addr_6b92_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_6f67_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_6f78_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_6a3b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_68a3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_65d1_70; else 
                    goto addr_68b7_169;
                addr_6a3b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_6598_63;
                goto addr_6720_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_6e74_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_6faf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6690_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6588_178; else 
                        goto addr_6f32_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6e74_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6693_22;
                }
                addr_6faf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_6690_30:
                    r8d42 = 0;
                    goto addr_6693_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_6701_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_6fc8_42;
                    }
                }
                addr_6588_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6598_63;
                addr_6f32_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_6e90_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_6701_62;
                } else {
                    addr_6f42_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_6693_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_76f2_188;
                if (v28) 
                    goto addr_6f67_162;
                addr_76f2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_68a3_168;
                addr_653c_37:
                if (v22) 
                    goto addr_7533_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_6553_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_7000_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_708b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6693_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6588_178; else 
                        goto addr_7067_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6e74_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6693_22;
                }
                addr_708b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_6693_22;
                }
                addr_7067_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6e90_46;
                goto addr_6f42_186;
                addr_6553_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_6693_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_6693_22; else 
                    goto addr_6564_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_763e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_74c4_210;
            if (1) 
                goto addr_74c2_212;
            if (!v29) 
                goto addr_70fe_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2640();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_7631_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_68c0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_667b_219; else 
            goto addr_68da_220;
        addr_665f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_6673_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_68da_220; else 
            goto addr_667b_219;
        addr_722f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_68da_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2640();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_724d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2640();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_76c0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_7126_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_7317_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6673_221;
        addr_7173_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6673_221;
        addr_68b7_169:
        goto addr_68c0_65;
        addr_763e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_68da_220;
        goto addr_724d_222;
        addr_74c4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_751e_236;
        fun_2660();
        rsp25 = rsp25 - 8 + 8;
        goto addr_76c0_225;
        addr_74c2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_74c4_210;
        addr_70fe_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_74c4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_7126_226;
        }
        addr_7631_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_6a8d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xc04c + rax113 * 4) + 0xc04c;
    addr_6eb9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xc14c + rax114 * 4) + 0xc14c;
    addr_7533_190:
    addr_667b_219:
    goto 0x6360;
    addr_6564_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xbf4c + rax115 * 4) + 0xbf4c;
    addr_751e_236:
    goto v116;
}

void fun_6580() {
}

void fun_6738() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x6432;
}

void fun_6791() {
    goto 0x6432;
}

void fun_687e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x6701;
    }
    if (v2) 
        goto 0x7173;
    if (!r10_3) 
        goto addr_72de_5;
    if (!v4) 
        goto addr_71ae_7;
    addr_72de_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_71ae_7:
    goto 0x65b4;
}

void fun_689c() {
}

void fun_6947() {
    signed char v1;

    if (v1) {
        goto 0x68cf;
    } else {
        goto 0x660a;
    }
}

void fun_6961() {
    signed char v1;

    if (!v1) 
        goto 0x695a; else 
        goto "???";
}

void fun_6988() {
    goto 0x68a3;
}

void fun_6a08() {
}

void fun_6a20() {
}

void fun_6a4f() {
    goto 0x68a3;
}

void fun_6aa1() {
    goto 0x6a30;
}

void fun_6ad0() {
    goto 0x6a30;
}

void fun_6b03() {
    goto 0x6a30;
}

void fun_6ed0() {
    goto 0x6588;
}

void fun_71ce() {
    signed char v1;

    if (v1) 
        goto 0x7173;
    goto 0x65b4;
}

void fun_7275() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x65b4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x6598;
        goto 0x65b4;
    }
}

void fun_7692() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x6900;
    } else {
        goto 0x6432;
    }
}

void fun_9398() {
    fun_2630();
}

void fun_a27c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xa28b;
}

void fun_a34c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xa359;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xa28b;
}

void fun_a370() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_a39c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa28b;
}

void fun_a3bd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa28b;
}

void fun_a3e1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa28b;
}

void fun_a405() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa394;
}

void fun_a429() {
}

void fun_a449() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa394;
}

void fun_a465() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa394;
}

void fun_2b70() {
    goto 0x2ae0;
}

int64_t xdectoumax(void** rdi);

void fun_2c30() {
    void** rdi1;

    fun_2630();
    rdi1 = optarg;
    xdectoumax(rdi1);
    goto 0x2ae0;
}

void fun_67be() {
    goto 0x6432;
}

void fun_6994() {
    goto 0x694c;
}

void fun_6a5b() {
    goto 0x6588;
}

void fun_6aad() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x6a30;
    goto 0x665f;
}

void fun_6adf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x6a3b;
        goto 0x6460;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x68da;
        goto 0x667b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x7278;
    if (r10_8 > r15_9) 
        goto addr_69c5_9;
    addr_69ca_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x7283;
    goto 0x65b4;
    addr_69c5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_69ca_10;
}

void fun_6b12() {
    goto 0x6647;
}

void fun_6ee0() {
    goto 0x6647;
}

void fun_767f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x679c;
    } else {
        goto 0x6900;
    }
}

void fun_9450() {
}

void fun_a31f() {
    if (__intrinsic()) 
        goto 0xa359; else 
        goto "???";
}

void fun_2b80() {
    goto 0x2ae0;
}

void fun_2c70() {
    goto 0x2ae0;
}

void fun_6b1c() {
    goto 0x6ab7;
}

void fun_6eea() {
    goto 0x6a0d;
}

void fun_94b0() {
    fun_2630();
    goto fun_2950;
}

int64_t __xargmatch_internal(int64_t rdi);

void fun_2b90() {
    void** rsi1;

    rsi1 = optarg;
    if (!rsi1) 
        goto 0x2ccd;
    __xargmatch_internal("--remove");
    goto 0x2ae0;
}

void fun_67ed() {
    goto 0x6432;
}

void fun_6b28() {
    goto 0x6ab7;
}

void fun_6ef7() {
    goto 0x6a5e;
}

void fun_94f0() {
    fun_2630();
    goto fun_2950;
}

void fun_681a() {
    goto 0x6432;
}

void fun_6b34() {
    goto 0x6a30;
}

void fun_9530() {
    fun_2630();
    goto fun_2950;
}

void fun_683c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x71d0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x6701;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x6701;
    }
    if (v11) 
        goto 0x7533;
    if (r10_12 > r15_13) 
        goto addr_7583_8;
    addr_7588_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x72c1;
    addr_7583_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_7588_9;
}

struct s25 {
    signed char[24] pad24;
    int64_t f18;
};

struct s26 {
    signed char[16] pad16;
    void** f10;
};

struct s27 {
    signed char[8] pad8;
    void** f8;
};

void fun_9580() {
    int64_t r15_1;
    struct s25* rbx2;
    void** r14_3;
    struct s26* rbx4;
    void** r13_5;
    struct s27* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    struct s3* rbp10;
    int64_t v11;
    int64_t v12;
    void*** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2630();
    fun_2950(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x95a2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_95d8() {
    fun_2630();
    goto 0x95a9;
}

struct s28 {
    signed char[32] pad32;
    void*** f20;
};

struct s29 {
    signed char[24] pad24;
    int64_t f18;
};

struct s30 {
    signed char[16] pad16;
    void** f10;
};

struct s31 {
    signed char[8] pad8;
    void** f8;
};

struct s32 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_9610() {
    void*** rcx1;
    struct s28* rbx2;
    int64_t r15_3;
    struct s29* rbx4;
    void** r14_5;
    struct s30* rbx6;
    void** r13_7;
    struct s31* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s32* rbx12;
    void** rax13;
    struct s3* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2630();
    fun_2950(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x9644, __return_address(), rcx1);
    goto v15;
}

void fun_9688() {
    fun_2630();
    goto 0x964b;
}
