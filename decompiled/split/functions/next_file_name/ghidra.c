void next_file_name(void)

{
  long lVar1;
  ulong uVar2;
  undefined *puVar3;
  char cVar4;
  char cVar5;
  long lVar6;
  size_t sVar7;
  size_t sVar8;
  void *__dest;
  char *pcVar9;
  void *pvVar10;
  long lVar11;
  size_t sVar12;
  undefined8 uVar13;
  long *plVar14;
  long lVar15;
  long *plVar16;
  
  lVar11 = (long)outfile;
  pcVar9 = outfile_mid;
  sVar12 = suffix_length;
  plVar16 = sufindex_3;
  puVar3 = suffix_alphabet;
  cVar5 = suffix_auto;
  if (outfile != (void *)0x0) {
    lVar15 = suffix_length - 1;
    if (suffix_length != 0) {
      plVar14 = sufindex_3 + lVar15;
      do {
        lVar6 = *plVar14;
        lVar1 = lVar6 + 1;
        *plVar14 = lVar1;
        if ((lVar15 == 0) && (cVar5 != '\0')) {
          if (puVar3[*plVar16 + 1] == '\0') goto LAB_00104798;
          cVar5 = puVar3[lVar1];
          *pcVar9 = cVar5;
          if (cVar5 != '\0') {
            return;
          }
          *plVar14 = 0;
          *pcVar9 = *puVar3;
          break;
        }
        cVar4 = puVar3[lVar6 + 1];
        pcVar9[lVar15] = cVar4;
        if (cVar4 != '\0') {
          return;
        }
        *plVar14 = 0;
        plVar14 = plVar14 + -1;
        pcVar9[lVar15] = *puVar3;
        lVar15 = lVar15 + -1;
      } while (lVar15 != -1);
    }
    uVar13 = dcgettext(0,"output file suffixes exhausted",5);
                    /* WARNING: Subroutine does not return */
    error(1,0,uVar13);
  }
LAB_00104798:
  lVar15 = outfile_length_6;
  if (outfile_length_6 == 0) {
    sVar7 = strlen(outbase);
    sVar8 = 0;
    outbase_length_5 = sVar7;
    if (additional_suffix != (char *)0x0) {
      sVar8 = strlen(additional_suffix);
    }
    outfile_length_6 = sVar8 + sVar12 + sVar7;
    addsuf_length_4 = sVar8;
    if (outfile_length_6 + 1U < sVar7) {
LAB_00104985:
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
    __dest = (void *)xrealloc(lVar11);
    sVar12 = outbase_length_5;
    outfile = __dest;
    memcpy(__dest,outbase,outbase_length_5);
    pcVar9 = suffix_alphabet;
    plVar16 = sufindex_3;
  }
  else {
    suffix_length = sVar12 + 1;
    lVar1 = outfile_length_6 + 2;
    uVar2 = outfile_length_6 + 3;
    outfile_length_6 = lVar1;
    if (uVar2 < outbase_length_5) goto LAB_00104985;
    __dest = (void *)xrealloc(lVar11);
    plVar16 = sufindex_3;
    pcVar9 = suffix_alphabet;
    sVar12 = outbase_length_5 + 1;
    puVar3 = (undefined *)((long)__dest + outbase_length_5);
    outbase_length_5 = sVar12;
    outfile = __dest;
    *puVar3 = suffix_alphabet[*sufindex_3];
  }
  sVar7 = suffix_length;
  outfile_mid = (char *)((long)__dest + sVar12);
  pvVar10 = memset(outfile_mid,(int)*pcVar9,suffix_length);
  if (additional_suffix != (char *)0x0) {
    memcpy((void *)((long)pvVar10 + sVar7),additional_suffix,addsuf_length_4);
  }
  *(undefined *)((long)__dest + outfile_length_6) = 0;
  free(plVar16);
  lVar11 = xcalloc(sVar7,8);
  pcVar9 = numeric_suffix_start;
  sufindex_3 = (long *)lVar11;
  if (numeric_suffix_start != (char *)0x0) {
    if (lVar15 != 0) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("! widen","src/split.c",0x199,"next_file_name");
    }
    sVar12 = strlen(numeric_suffix_start);
    lVar15 = suffix_length * 8;
    memcpy((void *)((suffix_length - sVar12) + (long)outfile_mid),pcVar9,sVar12);
    if (sVar12 != 0) {
      lVar1 = sVar12 * -8;
      do {
        sVar12 = sVar12 - 1;
        *(long *)(lVar11 + lVar15 + lVar1 + sVar12 * 8) = (long)(pcVar9[sVar12] + -0x30);
      } while (sVar12 != 0);
      return;
    }
  }
  return;
}