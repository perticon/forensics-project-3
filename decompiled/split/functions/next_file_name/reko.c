Eq_3061 next_file_name[]()
{
	Eq_3061 r8[];
	Eq_28 rbp_16 = outfile;
	Eq_28 rbx_20 = suffix_length;
	if (rbp_16 != 0x00)
	{
		int64 rax_24 = rbx_20 - 1;
		if (rbx_20 != 0x00)
		{
			Eq_28 r10_28 = sufindex.3;
			byte r9b_306 = g_bF018;
			Eq_28 rdi_31 = suffix_alphabet;
			r8 = outfile_mid;
			word64 * rdx_33 = (word64) r10_28 + (rbx_20 - 1) * 0x08;
			do
			{
				int64 rcx_36 = *rdx_33;
				*rdx_33 = rcx_36 + 1;
				if (rax_24 == 0x00 && r9b_306 != 0x00)
				{
					if (*((word64) *r10_28 + ((word64) rdi_31 + 1)) == 0x00)
						goto l0000000000004798;
					Eq_3061 al_258 = *((word64) rdi_31 + (rcx_36 + 1));
					r8[0] = al_258;
					if (al_258 != 0x00)
						return r8;
					*rdx_33 = 0x00;
					r8[0] = *rdi_31;
					break;
				}
				byte cl_53 = (word64) rdi_31 + 1 + rcx_36;
				r8[rax_24].u0 = cl_53;
				if (cl_53 != 0x00)
					return r8;
				*rdx_33 = 0x00;
				r8[rax_24] = *rdi_31;
				rdx_33 -= 0x08;
				--rax_24;
			} while (rax_24 != ~0x00);
		}
		fn00000000000028A0(fn0000000000002640(0x05, "output file suffixes exhausted", null), 0x00, 0x01);
		goto l00000000000049BB;
	}
l0000000000004798:
	Eq_28 r14_105;
	Eq_28 rax_109;
	Eq_28 rbx_111;
	Eq_28 r13_107;
	Eq_2956 r12_77 = outfile_length.6;
	if (r12_77 == 0x00)
	{
		Eq_28 rax_118 = fn0000000000002660(outbase);
		Eq_28 rdi_119 = additional_suffix;
		outbase_length.5 = rax_118;
		Eq_28 rax_123 = 0x00;
		if (rdi_119 != 0x00)
			rax_123 = fn0000000000002660(rdi_119);
		addsuf_length.4 = rax_123;
		word64 rax_133 = rax_123 + (rbx_20 + rax_118);
		outfile_length.6 = rax_133;
		if (rax_118 <= (word64) rax_133.u1 + 1)
		{
			word64 rbx_436;
			word64 rbp_437;
			word64 rsi_438;
			Eq_28 rax_140 = xrealloc((word64) rax_133.u1 + 1, rbp_16, out rbx_436, out rbp_437, out rsi_438);
			Eq_28 rsi_151 = outbase;
			rbx_111 = outbase_length.5;
			outfile = rax_140;
			fn00000000000027E0(rbx_111, rsi_151, rax_140);
			r13_107 = rax_140;
			rax_109 = suffix_alphabet;
			r14_105 = sufindex.3;
			goto l0000000000004897;
		}
	}
	else
	{
		outfile_length.6 = (word64) r12_77 + 2;
		suffix_length = (word64) rbx_20 + 1;
		if ((word64) r12_77 + 3 >= outbase_length.5)
		{
			word64 rsi_100;
			word64 rbx_439;
			word64 rbp_440;
			Eq_28 rax_95 = xrealloc((word64) r12_77 + 3, rbp_16, out rbx_439, out rbp_440, out rsi_100);
			r14_105 = sufindex.3;
			Eq_28 rcx_106 = outbase_length.5;
			outfile = rax_95;
			rax_109 = suffix_alphabet;
			int64 rdx_110 = *r14_105;
			outbase_length.5 = (word64) rcx_106 + 1;
			Mem116[rax_95 + rcx_106:byte] = Mem112[rax_109 + rdx_110:byte];
			r13_107 = rax_95;
			rbx_111 = (word64) rcx_106 + 1;
l0000000000004897:
			Eq_28 rbp_160 = suffix_length;
			word64 rcx_163 = r13_107 + rbx_111;
			int32 esi_165 = (int32) *rax_109;
			outfile_mid = rcx_163;
			word64 rax_173 = fn0000000000002700(rbp_160, esi_165, rcx_163);
			Eq_28 rsi_174 = additional_suffix;
			if (rsi_174 != 0x00)
				fn00000000000027E0(addsuf_length.4, rsi_174, (word64) rbp_160 + rax_173);
			Mem187[r13_107 + Mem167[0x000000000000F2D8<p64>:word64]:byte] = 0x00;
			fn0000000000002540(r14_105);
			Eq_28 rax_192 = xcalloc(0x08, rbp_160);
			Eq_28 rbp_197 = numeric_suffix_start;
			sufindex.3 = rax_192;
			if (rbp_197 == 0x00)
				return r8;
			if (r12_77 == 0x00)
			{
				Eq_28 rax_210 = fn0000000000002660(rbp_197);
				Eq_28 r13_211 = suffix_length;
				fn00000000000027E0(rax_210, rbp_197, outfile_mid + (r13_211 - rax_210));
				Eq_3182 rdx_221 = rax_210 - 1;
				if (rax_210 == 0x00)
					return r8;
				int64 (* rcx_229)[] = (word64) rax_192 + (r13_211 << 0x03);
				do
				{
					rcx_229[rdx_221 * 0x08] = CONVERT(CONVERT(Mem232[rbp_197 + rdx_221:byte], byte, int32) - 0x30, word32, int64);
					--rdx_221;
				} while (rdx_221 >= 0x00);
				return r8;
			}
l00000000000049BB:
			fn00000000000026F0("next_file_name", 0x0199, "src/split.c", "! widen");
		}
	}
	word64 r8_435;
	word64 rdi_434;
	word64 rsi_433;
	word64 rdx_432;
	word64 rcx_431;
	xalloc_die(out rcx_431, out rdx_432, out rsi_433, out rdi_434, out r8_435);
}