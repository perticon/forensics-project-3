void next_file_name(void) {
    int64_t v1 = suffix_length; // 0x46ef
    char * v2; // 0x46e0
    int64_t * v3; // 0x46e0
    int64_t v4; // 0x46e0
    int64_t v5; // 0x46e0
    char * v6; // 0x46e0
    int64_t v7; // 0x46e0
    if (outfile == 0) {
        goto lab_0x4798;
    } else {
        if (v1 == 0) {
            // 0x4997
            function_2640();
            function_28a0();
            // 0x49bb
            function_26f0();
            return;
        }
        int64_t v8 = v1 - 1; // 0x46ff
        int64_t v9 = (int64_t)suffix_alphabet; // 0x471b
        int64_t v10 = outfile_mid; // 0x4722
        int64_t v11 = v9 + 1;
        int64_t v12 = g41 + 8 * v8;
        int64_t v13 = v8;
        int64_t * v14 = (int64_t *)v12;
        int64_t v15 = *v14; // 0x475d
        int64_t v16 = v15 + 1; // 0x4760
        *v14 = v16;
        while (*(char *)&suffix_auto == 0 || v13 != 0) {
            char v17 = *(char *)(v15 + v11); // 0x4730
            char * v18 = (char *)(v13 + v10); // 0x4735
            *v18 = v17;
            if (v17 != 0) {
                // 0x478a
                return;
            }
            // 0x473d
            *v14 = 0;
            *v18 = *(char *)&suffix_alphabet;
            if (v13 == 0) {
                // 0x4997
                function_2640();
                function_28a0();
                // 0x49bb
                function_26f0();
                return;
            }
            v12 -= 8;
            v13--;
            v14 = (int64_t *)v12;
            v15 = *v14;
            v16 = v15 + 1;
            *v14 = v16;
        }
        // 0x4771
        v7 = v12;
        v5 = v9;
        v4 = v10;
        if (*(char *)(*(int64_t *)g41 + v11) == 0) {
            goto lab_0x4798;
        } else {
            char v19 = *(char *)(v16 + v9); // 0x477b
            char * v20 = (char *)v10;
            *v20 = v19;
            v3 = v14;
            v2 = v20;
            v6 = suffix_alphabet;
            if (v19 != 0) {
                // 0x478a
                return;
            }
            goto lab_0x498a;
        }
    }
  lab_0x4798:;
    int64_t v21 = g44; // 0x4798
    int64_t v22; // 0x46e0
    int64_t v23; // 0x46e0
    int64_t v24; // 0x46e0
    if (v21 != 0) {
        // 0x4830
        g44 = v21 + 2;
        suffix_length = v1 + 1;
        v24 = v5;
        if (v21 + 3 < g43) {
            goto lab_0x4985;
        } else {
            int64_t v25 = xrealloc(); // 0x485c
            int64_t v26 = g43; // 0x4868
            outfile = v25;
            int64_t v27 = v26 + 1; // 0x4883
            g43 = v27;
            char v28 = *(char *)(*(int64_t *)g41 + (int64_t)suffix_alphabet); // 0x488e
            *(char *)(v26 + v25) = v28;
            v23 = v27;
            v22 = v25;
            goto lab_0x4897;
        }
    } else {
        int64_t v29 = function_2660(); // 0x47af
        g43 = v29;
        int64_t v30 = 0; // 0x47ca
        if (additional_suffix != 0) {
            // 0x47cc
            v30 = function_2660();
        }
        // 0x47d1
        g42 = v30;
        int64_t v31 = v29 + v1 + v30; // 0x47db
        g44 = v31;
        v24 = additional_suffix;
        if (v29 > v31 + 1) {
            goto lab_0x4985;
        } else {
            int64_t v32 = xrealloc(); // 0x47f5
            outfile = v32;
            function_27e0();
            v23 = g43;
            v22 = v32;
            goto lab_0x4897;
        }
    }
  lab_0x4985:
    // 0x4985
    xalloc_die();
    v3 = (int64_t *)v7;
    v2 = (char *)v4;
    v6 = (char *)v24;
    goto lab_0x498a;
  lab_0x498a:
    // 0x498a
    *v3 = 0;
    *v2 = *v6;
    // 0x4997
    function_2640();
    function_28a0();
    // 0x49bb
    function_26f0();
  lab_0x4897:
    // 0x4897
    outfile_mid = v22 + v23;
    function_2700();
    if (additional_suffix != 0) {
        // 0x48c4
        function_27e0();
    }
    // 0x48d4
    *(char *)(g44 + v22) = 0;
    function_2540();
    int64_t v33 = xcalloc(); // 0x48f1
    g41 = v33;
    if (numeric_suffix_start == 0) {
        // 0x478a
        return;
    }
    if (v21 != 0) {
        // 0x49bb
        function_26f0();
        return;
    }
    int64_t v34 = function_2660(); // 0x491c
    function_27e0();
    if (v34 == 0) {
        // 0x478a
        return;
    }
    int64_t v35 = v34 - 1;
    char v36 = *(char *)(v35 + numeric_suffix_start); // 0x4968
    *(int64_t *)(8 * (suffix_length - v34 + v35) + v33) = (int64_t)v36 - 48;
    int64_t v37 = v35; // 0x497a
    while (v35 != 0) {
        // 0x4968
        v35 = v37 - 1;
        v36 = *(char *)(v35 + numeric_suffix_start);
        *(int64_t *)(8 * (suffix_length - v34 + v35) + v33) = (int64_t)v36 - 48;
        v37 = v35;
    }
    // 0x478a
    return;
}