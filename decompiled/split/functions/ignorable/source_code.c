ignorable (int err)
{
  return filter_command && err == EPIPE;
}