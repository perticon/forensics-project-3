void closeout(long param_1,int param_2,int param_3,undefined8 param_4)

{
  uint uVar1;
  int iVar2;
  __pid_t _Var3;
  int *piVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  char cVar7;
  long lVar8;
  uint uVar9;
  long in_FS_OFFSET;
  uint local_4c;
  undefined local_48 [24];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    if (param_2 < 0) goto LAB_001051e1;
    iVar2 = close(param_2);
    if (iVar2 < 0) {
      uVar5 = quotearg_n_style_colon(0,3,param_4);
      piVar4 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar4,"%s",uVar5);
    }
  }
  else {
    iVar2 = rpl_fclose();
    if (iVar2 != 0) {
      piVar4 = __errno_location();
      if ((filter_command == 0) || (*piVar4 != 0x20)) {
        uVar5 = quotearg_n_style_colon(0,3,param_4);
                    /* WARNING: Subroutine does not return */
        error(1,*piVar4,"%s",uVar5);
      }
    }
    if (param_2 < 0) goto LAB_001051e1;
  }
  if (n_open_pipes != 0) {
    lVar8 = 0;
    piVar4 = open_pipes;
    do {
      if (*piVar4 == param_2) {
        n_open_pipes = n_open_pipes + -1;
        *piVar4 = open_pipes[n_open_pipes];
        break;
      }
      lVar8 = lVar8 + 1;
      piVar4 = piVar4 + 1;
    } while (lVar8 != n_open_pipes);
  }
LAB_001051e1:
  if (0 < param_3) {
    local_4c = 0;
    _Var3 = waitpid(param_3,(int *)&local_4c,0);
    if (_Var3 == -1) {
      piVar4 = __errno_location();
      if (*piVar4 != 10) {
        uVar5 = dcgettext(0,"waiting for child process",5);
                    /* WARNING: Subroutine does not return */
        error(1,*piVar4,uVar5);
      }
    }
    uVar1 = local_4c;
    lVar8 = filter_command;
    uVar9 = local_4c & 0x7f;
    if ((char)((char)uVar9 + '\x01') < '\x02') {
      if (uVar9 != 0) {
        uVar5 = dcgettext(0,"unknown status from command (0x%X)",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar5,uVar1);
      }
      cVar7 = (char)(local_4c >> 8);
      if (cVar7 != '\0') {
        uVar5 = quotearg_n_style_colon(0,3,param_4);
        uVar6 = dcgettext(0,"with FILE=%s, exit %d from command: %s",5);
                    /* WARNING: Subroutine does not return */
        error(cVar7,0,uVar6,uVar5,cVar7,lVar8);
      }
    }
    else if (uVar9 != 0xd) {
      iVar2 = sig2str(uVar9,local_48);
      if (iVar2 != 0) {
        __sprintf_chk(local_48,1,0x13,&DAT_0010a130,uVar9);
      }
      lVar8 = filter_command;
      uVar5 = quotearg_n_style_colon(0,3,param_4);
      uVar6 = dcgettext(0,"with FILE=%s, signal %s from command: %s",5);
                    /* WARNING: Subroutine does not return */
      error(uVar9 + 0x80,0,uVar6,uVar5,local_48,lVar8);
    }
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}