void closeout(struct _IO_FILE * fp, int32_t fd, int32_t pid, char * name) {
    int64_t v1 = __readfsqword(40); // 0x5131
    if (fp == NULL) {
        if (fd < 0) {
            goto lab_0x51e1;
        } else {
            // 0x520c
            if ((int32_t)function_2710() >= 0) {
                goto lab_0x519c;
            } else {
                // 0x5217
                quotearg_n_style_colon();
                function_2560();
                function_28a0();
                goto lab_0x5250;
            }
        }
    } else {
        // 0x514a
        if (rpl_fclose(fp) == 0) {
            goto lab_0x5198;
        } else {
            int64_t v2 = function_2560(); // 0x5153
            if (filter_command == 0) {
                // 0x516a
                quotearg_n_style_colon();
                function_28a0();
                goto lab_0x5198;
            } else {
                // 0x5165
                if (*(int32_t *)v2 == 32) {
                    goto lab_0x5198;
                } else {
                    // 0x516a
                    quotearg_n_style_colon();
                    function_28a0();
                    goto lab_0x5198;
                }
            }
        }
    }
  lab_0x51e1:
    if (pid >= 0 == (pid != 0)) {
        goto lab_0x5250;
    } else {
        goto lab_0x51e5;
    }
  lab_0x5198:
    if (fd < 0) {
        goto lab_0x51e1;
    } else {
        goto lab_0x519c;
    }
  lab_0x5250:
    // 0x5250
    if ((int32_t)function_28c0() == -1) {
        // 0x5350
        if (*(int32_t *)function_2560() != 10) {
            // 0x5361
            function_2640();
            function_28a0();
            function_29f0();
            quotearg_n_style_colon();
            function_2640();
            function_28a0();
        }
    }
    goto lab_0x51e5;
  lab_0x51e5:
    // 0x51e5
    if (v1 != __readfsqword(40)) {
        // 0x53ab
        function_2670();
        function_2640();
        function_28a0();
        return;
    }
  lab_0x519c:;
    int64_t v3 = n_open_pipes; // 0x519c
    if (v3 != 0) {
        int64_t v4 = (int64_t)open_pipes; // 0x51a8
        int64_t v5 = v4; // 0x51b4
        int64_t v6 = 0; // 0x51b4
        int32_t * v7 = (int32_t *)v5;
        while (*v7 != fd) {
            // 0x51c0
            v6++;
            v5 += 4;
            if (v6 == v3) {
                goto lab_0x51e1;
            }
            v7 = (int32_t *)v5;
        }
        int64_t v8 = v3 - 1; // 0x51d1
        n_open_pipes = v8;
        *v7 = *(int32_t *)(4 * v8 + v4);
    }
    goto lab_0x51e1;
}