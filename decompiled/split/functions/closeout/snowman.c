void closeout(void** rdi, void** esi, void** edx, void** rcx, ...) {
    void** rbp5;
    void** rbx6;
    void* rsp7;
    void** rax8;
    void** v9;
    int32_t eax10;
    int64_t rax11;
    void** rax12;
    int1_t zf13;
    void** rax14;
    int64_t rdi15;
    int32_t eax16;
    void** rax17;
    void* rax18;
    void*** rsi19;
    void** rdx20;
    void*** rax21;
    void** rax22;
    void** rcx23;
    void** rax24;
    void** r8_25;
    int32_t eax26;
    int64_t r8_27;
    void** rax28;
    void** v29;
    int64_t rdi30;
    int1_t zf31;
    void** rdx32;
    void** rsi33;
    void** rdi34;
    void** eax35;
    void** rax36;
    void** rax37;
    void** rsi38;
    void** r15_39;
    void** r14_40;
    uint32_t ebp41;
    int32_t ebx42;
    void** v43;
    void** v44;
    void** rax45;
    void** rax46;
    int1_t zf47;
    int64_t v48;
    void** rax49;
    int1_t zf50;
    void** rax51;
    void** r12_52;
    unsigned char v53;
    void** rsi54;
    void** r11_55;
    uint32_t eax56;
    uint32_t edi57;
    uint32_t eax58;
    void* rbx59;
    uint1_t below_or_equal60;
    uint32_t eax61;
    uint32_t edi62;
    uint32_t eax63;
    void* rbx64;
    void** rbx65;
    void** rax66;
    void** edx67;

    rbp5 = edx;
    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
    rbx6 = esi;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 72);
    rax8 = g28;
    v9 = rax8;
    if (!rdi) {
        if (reinterpret_cast<signed char>(esi) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_3;
        eax10 = fun_2710();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (eax10 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_6;
        }
    } else {
        rax11 = rpl_fclose(rdi);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax11) && ((rax12 = fun_2560(), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), zf13 = filter_command == 0, zf13) || *reinterpret_cast<void***>(rax12) != 32)) {
            rax14 = quotearg_n_style_colon();
            rcx = rax14;
            fun_28a0();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx6) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_3;
    }
    rcx = n_open_pipes;
    if (!rcx) {
        addr_51e1_3:
        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbp5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbp5 == 0))) {
            addr_5250_6:
            *reinterpret_cast<void***>(&rdi15) = rbp5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
            eax16 = fun_28c0(rdi15, reinterpret_cast<int64_t>(rsp7) + 28);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            if (eax16 == -1 && (rax17 = fun_2560(), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax17, *reinterpret_cast<void***>(rax17) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_12;
            }
        } else {
            addr_51e5_13:
            rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
            if (rax18) {
                fun_2670();
                goto addr_53b0_15;
            } else {
                return;
            }
        }
    } else {
        rsi19 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx20) = 0;
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        rax21 = rsi19;
        do {
            if (*rax21 == rbx6) 
                goto addr_51d1_19;
            ++rdx20;
            rax21 = rax21 + 4;
        } while (rdx20 != rcx);
        goto addr_51e1_3;
    }
    *reinterpret_cast<int32_t*>(&rcx) = 0;
    rbx6 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_15:
            rax22 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx23) = *reinterpret_cast<int32_t*>(&rcx);
            *reinterpret_cast<int32_t*>(&rcx23 + 4) = 0;
            fun_28a0();
        } else {
            rbp5 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rcx + 1)));
            *reinterpret_cast<unsigned char*>(&rcx + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx + 1)) {
                rbx6 = filter_command;
                rax24 = quotearg_n_style_colon();
                fun_2640();
                r8_25 = rbp5;
                *reinterpret_cast<int32_t*>(&r8_25 + 4) = 0;
                rcx = rax24;
                fun_28a0();
                goto addr_51e5_13;
            }
        }
    } else {
        if (!0) {
            rbp5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
            eax26 = sig2str(0, rbp5);
            if (eax26) {
                addr_5388_12:
                *reinterpret_cast<void***>(&r8_27) = rbx6;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_27) + 4) = 0;
                fun_29f0(rbp5, 1, 19, "%d", r8_27);
                goto addr_529f_27;
            } else {
                addr_529f_27:
                rax28 = quotearg_n_style_colon();
                fun_2640();
                r8_25 = rbp5;
                rcx = rax28;
                fun_28a0();
                goto addr_51e5_13;
            }
        }
    }
    v29 = rbx6;
    if (0) {
        *reinterpret_cast<void***>(&rdi30) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
        goto addr_543f_30;
    }
    if (rax22 || (zf31 = elide_empty_files == 0, zf31)) {
        rcx23 = outfile;
        rdx32 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx32 + 4) = 0;
        rsi33 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
        closeout(0, rsi33, rdx32, rcx23, 0, rsi33, rdx32, rcx23);
        next_file_name();
        rdi34 = outfile;
        eax35 = create(rdi34, rsi33, rdx32, rcx23, rdi34, rsi33, rdx32, rcx23);
        output_desc = eax35;
        *reinterpret_cast<void***>(&rdi30) = eax35;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
        if (reinterpret_cast<signed char>(eax35) < reinterpret_cast<signed char>(0)) {
            rax36 = quotearg_n_style_colon();
            rax37 = fun_2560();
            rsi38 = *reinterpret_cast<void***>(rax37);
            *reinterpret_cast<int32_t*>(&rsi38 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_39) = 0;
            *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
            r14_40 = reinterpret_cast<void**>(1);
            ebp41 = 1;
            ebx42 = 1;
            v43 = rsi38;
            v44 = r8_25;
            if (rax36 != 0xffffffffffffffff) 
                goto addr_552a_35;
        } else {
            addr_543f_30:
            rax45 = full_write(rdi30, 0, rax22, rcx23, rdi30, 0, rax22, rcx23);
            if (rax45 != rax22) {
                rax46 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax46) == 32) || (zf47 = filter_command == 0, zf47)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_38;
                } else {
                    addr_549d_39:
                    goto v48;
                }
            }
        }
    } else {
        addr_5498_38:
        goto addr_549d_39;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp41)) {
            rax49 = fun_26e0();
            zf50 = reinterpret_cast<int1_t>(rax49 == 0xffffffffffffffff);
            if (!zf50) {
                r14_40 = reinterpret_cast<void**>(1);
            }
            if (!zf50) {
                ebx42 = 1;
            }
        }
        rax51 = safe_read();
        r12_52 = rax51;
        if (rax51 == 0xffffffffffffffff) 
            break;
        v53 = reinterpret_cast<uint1_t>(rax51 == 0);
        addr_5535_48:
        if (reinterpret_cast<unsigned char>(r12_52) < reinterpret_cast<unsigned char>(r14_40)) {
            rsi54 = v43;
        } else {
            rsi54 = v43;
            r11_55 = v44 + 0xffffffffffffffff;
            do {
                eax56 = 1;
                *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx42));
                if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                    edi57 = *reinterpret_cast<unsigned char*>(&ebx42);
                    eax58 = cwrite(*reinterpret_cast<unsigned char*>(&edi57), rsi54, r14_40, *reinterpret_cast<unsigned char*>(&edi57), rsi54, r14_40);
                    r11_55 = r11_55;
                    rsi54 = rsi54;
                    ebp41 = eax58;
                    eax56 = eax58 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx59) = *reinterpret_cast<unsigned char*>(&ebx42);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx59) + 4) = 0;
                r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<uint64_t>(rbx59));
                below_or_equal60 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_39) <= reinterpret_cast<unsigned char>(r11_55));
                *reinterpret_cast<unsigned char*>(&ebx42) = below_or_equal60;
                if (below_or_equal60) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax56)) 
                    goto addr_5650_55;
                r12_52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_52) - reinterpret_cast<unsigned char>(r14_40));
                rsi54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi54) + reinterpret_cast<unsigned char>(r14_40));
                r14_40 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_52));
        }
        if (r12_52) {
            eax61 = 1;
            *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx42));
            if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                edi62 = *reinterpret_cast<unsigned char*>(&ebx42);
                eax63 = cwrite(*reinterpret_cast<unsigned char*>(&edi62), rsi54, r12_52, *reinterpret_cast<unsigned char*>(&edi62), rsi54, r12_52);
                ebp41 = eax63;
                eax61 = eax63 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx64) = *reinterpret_cast<unsigned char*>(&ebx42);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx64) + 4) = 0;
            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<uint64_t>(rbx64));
            *reinterpret_cast<unsigned char*>(&ebx42) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v44 == r15_39)) & *reinterpret_cast<unsigned char*>(&eax61));
            if (*reinterpret_cast<unsigned char*>(&ebx42)) 
                goto addr_567c_61;
            r14_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_40) - reinterpret_cast<unsigned char>(r12_52));
        }
    } while (!v53);
    goto addr_5650_55;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_55:
    rbx65 = r15_39 + 1;
    if (reinterpret_cast<unsigned char>(v44) > reinterpret_cast<unsigned char>(r15_39)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax66 = rbx65;
            ++rbx65;
        } while (v44 != rax66);
    }
    addr_567c_61:
    goto v29;
    addr_552a_35:
    r12_52 = rax36;
    v53 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax36) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_48;
    addr_51d1_19:
    --rcx;
    edx67 = rsi19[reinterpret_cast<unsigned char>(rcx) * 4];
    n_open_pipes = rcx;
    *rax21 = edx67;
    goto addr_51e1_3;
}