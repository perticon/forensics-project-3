Eq_28 closeout(word32 edx, Eq_88 esi, FILE * rdi, struct Eq_596 * fs, char & r8Out)
{
	Eq_28 rsi;
	Eq_28 rdx;
	ptr64 fp;
	Eq_28 rcx;
	char * r8;
	Eq_28 rax;
	word32 edx = (word32) rdx;
	Eq_88 esi = (word32) rsi;
	char * rbp_17 = (uint64) edx;
	Eq_28 r12_11 = rcx;
	int32 ebp_163 = (word32) rbp_17;
	word64 rax_26 = fs->qw0028;
	int32 eax_123 = 0x00;
	if (rdi != null)
	{
		eax_123 = (word32) rpl_fclose(rdi);
		if (eax_123 != 0x00)
		{
			Eq_28 rax_84 = fn0000000000002560();
			eax_123 = (word32) rax_84;
			if (filter_command == 0x00 || *rax_84 != 0x20)
			{
				word64 r10_595;
				quotearg_n_style_colon(rcx, 0x03, 0x00, fs, out r8, out r10_595);
				fn00000000000028A0(0xA31E, *rax_84, 0x01);
				eax_123 = 0x00;
			}
		}
		if (esi < 0x00)
		{
l00000000000051E1:
			if (ebp_163 <= 0x00)
				goto l00000000000051E5;
			goto l0000000000005250;
		}
	}
	else
	{
		if (esi < 0x00)
			goto l00000000000051E1;
		eax_123 = fn0000000000002710(esi);
		if (eax_123 < 0x00)
		{
			word64 r10_596;
			r12_11 = quotearg_n_style_colon(rcx, 0x03, 0x00, fs, out r8, out r10_596);
			fn00000000000028A0(0xA31E, *fn0000000000002560(), 0x01);
			eax_123 = 0x00;
l0000000000005250:
			Eq_28 rbx_226;
			Eq_28 rax_194;
			fn00000000000028C0();
			char * rbp_314 = rbp_17;
			if (eax_123 == ~0x00)
			{
				rax_194 = fn0000000000002560();
				if (*rax_194 != 0x0A)
				{
					fn00000000000028A0(fn0000000000002640(0x05, "waiting for child process", null), *rax_194.u0, 0x01);
					goto l0000000000005388;
				}
			}
			rbx_226.u0 = 0x00;
			if (true)
			{
				if (false)
				{
					fn00000000000028A0(fn0000000000002640(0x05, "unknown status from command (0x%X)", null), 0x00, 0x01);
					char * r8_452;
					Eq_28 rax_449 = cwrite(rax, rdx, rsi, 0x01, out r8_452);
					r8Out = r8_452;
					return rax_449;
				}
				if (false)
				{
					word64 r8_599;
					word64 r10_600;
					quotearg_n_style_colon(r12_11, 0x03, 0x00, fs, out r8_599, out r10_600);
					fn00000000000028A0(fn0000000000002640(0x05, "with FILE=%s, exit %d from command: %s", null), 0x00, 0x00);
					r8 = null;
				}
l00000000000051E5:
				Eq_28 rax_418 = rax_26 - fs->qw0028;
				if (rax_418 != 0x00)
					fn0000000000002670();
				else
				{
					r8Out = r8;
					return rax_418;
				}
			}
			if (false)
				goto l00000000000051E5;
			rbp_314 = fp - 0x48;
			rax_194.u0 = 0x00;
			if (sig2str(fp - 0x48, 0x00) == 0x00)
			{
l000000000000529F:
				word64 r8_597;
				word64 r10_598;
				quotearg_n_style_colon(r12_11, 0x03, 0x00, fs, out r8_597, out r10_598);
				fn00000000000028A0(fn0000000000002640(0x05, "with FILE=%s, signal %s from command: %s", null), 0x00, (word32) rbx_226 + 0x80);
				r8 = rbp_314;
				goto l00000000000051E5;
			}
l0000000000005388:
			fn00000000000029F0("%d", 0x13, 0x01, rbp_314);
			rbx_226 = rax_194;
			goto l000000000000529F;
		}
	}
	uint64 rcx_132 = n_open_pipes;
	if (rcx_132 != 0x00)
	{
		struct Eq_3515 * rsi_136 = open_pipes;
		uint64 rdx_138 = 0x00;
		struct Eq_3515 * rax_142 = rsi_136;
		do
		{
			eax_123 = (word32) rax_142;
			if (rax_142->a0000[0] == esi)
			{
				Eq_88 edx_160 = rsi_136[rcx_132 - 0x01];
				n_open_pipes = rcx_132 - 0x01;
				rax_142->a0000[0] = (word32) edx_160;
				break;
			}
			++rax_142;
			++rdx_138;
			eax_123 = (word32) rax_142;
		} while (rdx_138 != rcx_132);
	}
	goto l00000000000051E1;
}