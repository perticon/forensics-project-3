bool ofile_open(int32_t * files, int64_t i_check, int64_t nfiles) {
    int64_t v1 = (int64_t)files;
    int64_t v2 = 32 * i_check + v1; // 0x4f30
    int32_t * v3 = (int32_t *)(v2 + 8); // 0x4f3a
    uint32_t v4 = *v3; // 0x4f3a
    if (v4 >= 0) {
        // 0x4f42
        return false;
    }
    int64_t v5 = nfiles - 1; // 0x4f57
    int64_t v6 = i_check == 0 ? v5 : i_check - 1; // 0x4f61
    int64_t * v7 = (int64_t *)v2; // 0x4f65
    int64_t v8 = *v7; // 0x4f65
    int64_t v9 = v6; // 0x4f6f
    int64_t v10 = 0; // 0x4f6f
    int64_t v11 = v8; // 0x4f6f
    int64_t v12 = v6; // 0x4f6f
    int64_t v13 = 0; // 0x4f6f
    int64_t v14 = v8; // 0x4f6f
    if (v4 == -1) {
        goto lab_0x500b;
    } else {
        goto lab_0x4f78;
    }
  lab_0x500b:;
    int32_t v15 = create((char *)v14); // 0x500b
    int64_t v16 = v12; // 0x5012
    int32_t v17 = v15; // 0x5012
    int64_t v18 = v13; // 0x5012
    if (v15 < 0) {
        goto lab_0x4f8c;
    } else {
        goto lab_0x5018;
    }
  lab_0x4f78:;
    int32_t v19 = open_safer((char *)v11, (int32_t)&g26); // 0x4f7f
    v16 = v9;
    v17 = v19;
    v18 = v10;
    if (v19 >= 0) {
        goto lab_0x5018;
    } else {
        goto lab_0x4f8c;
    }
  lab_0x4f8c:
    // 0x4f8c
    if (*(int32_t *)function_2560() < 25) {
        int64_t v20 = v16;
        int64_t v21 = 32 * v20 + v1; // 0x4fc6
        int32_t * v22 = (int32_t *)(v21 + 8);
        while (*v22 < 0) {
            int64_t v23 = v20 == 0 ? v5 : v20 - 1; // 0x4faf
            if (v23 == i_check) {
                // 0x5050
                quotearg_n_style_colon();
                function_28a0();
                // 0x5083
                quotearg_n_style_colon();
                function_28a0();
                goto lab_0x50b6;
            }
            v20 = v23;
            v21 = 32 * v20 + v1;
            v22 = (int32_t *)(v21 + 8);
        }
        int64_t * v24 = (int64_t *)(v21 + 16); // 0x4fd0
        if (rpl_fclose((struct _IO_FILE *)*v24) != 0) {
            goto lab_0x50b6;
        } else {
            // 0x4fe6
            *v22 = -2;
            *v24 = 0;
            int64_t v25 = *v7; // 0x4ff9
            v9 = v20;
            v10 = 1;
            v11 = v25;
            v12 = v20;
            v13 = 1;
            v14 = v25;
            if (*v3 != -1) {
                goto lab_0x4f78;
            } else {
                goto lab_0x500b;
            }
        }
    } else {
        // 0x5083
        quotearg_n_style_colon();
        function_28a0();
        goto lab_0x50b6;
    }
  lab_0x5018:
    // 0x5018
    *v3 = v17;
    int64_t v26 = function_2860(); // 0x5025
    *(int64_t *)(v2 + 16) = v26;
    if (v26 == 0) {
        // 0x50e4
        quotearg_n_style_colon();
        function_2560();
        return function_28a0() % 2 != 0;
    }
    int32_t v27 = filter_pid; // 0x5037
    filter_pid = 0;
    *(int32_t *)(v2 + 24) = v27;
    // 0x4f42
    return v18 % 2 != 0;
  lab_0x50b6:
    // 0x50b6
    quotearg_n_style_colon();
    function_28a0();
    // 0x50e4
    quotearg_n_style_colon();
    function_2560();
    return function_28a0() % 2 != 0;
}