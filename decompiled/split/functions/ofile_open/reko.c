uint64 ofile_open(Eq_28 rdx, Eq_28 rsi, Eq_28 rdi, struct Eq_596 * fs, union Eq_28 & r8Out)
{
	Eq_28 r8;
	struct Eq_3663 * r13_23 = (word64) rdi + (rsi << 0x05);
	struct Eq_3667 * rbp_170 = null;
	word32 ebp_360 = 0x00;
	Eq_88 eax_32 = r13_23->t0008;
	if (eax_32 >= 0x00)
	{
l0000000000004F42:
		r8Out = r8;
		return (uint64) ebp_360;
	}
	Eq_28 rbx_103 = rsi - 1;
	if (rsi == 0x00)
		rbx_103 = rdx - 1;
	Eq_88 eax_199;
	Eq_28 rdi_168 = r13_23->t0000;
	if (eax_32 != ~0x00)
	{
l0000000000004F78:
		eax_199 = open_safer(0x0C01, rdi_168, fs);
		if (eax_199 < 0x00)
		{
l0000000000004F8C:
			Eq_28 rax_73 = fn0000000000002560();
			r8 = rax_73;
			if (*rax_73 <= 0x18)
			{
				do
				{
					rbp_170 = (word64) rdi + (rbx_103 << 0x05);
					if (rbp_170->dw0008 >= 0x00)
					{
						if ((word32) rpl_fclose(rbp_170->ptr0010) != 0x00)
							goto l00000000000050B6;
						rbp_170->dw0008 = ~0x01;
						Eq_88 eax_171 = r13_23->t0008;
						rbp_170->ptr0010 = null;
						rdi_168 = r13_23->t0000;
						rbp_170 = (struct Eq_3667 *) 0x01;
						if (eax_171 == ~0x00)
							goto l000000000000500B;
						goto l0000000000004F78;
					}
					Eq_28 rax_101 = rbx_103 - 1;
					if (rbx_103 == 0x00)
						rax_101 = rdx - 1;
					rbx_103 = rax_101;
				} while (rax_101 != rsi);
				word64 r8_501;
				word64 r10_502;
				quotearg_n_style_colon(r13_23->t0000, 0x03, 0x00, fs, out r8_501, out r10_502);
				fn00000000000028A0(0xA31E, *rax_73, 0x01);
			}
			word64 r10_496;
			word64 r8_495;
			quotearg_n_style_colon(r13_23->t0000, 0x03, 0x00, fs, out r8_495, out r10_496);
			fn00000000000028A0(0xA31E, *rax_73, 0x01);
l00000000000050B6:
			word64 r8_499;
			word64 r10_500;
			quotearg_n_style_colon(rbp_170->t0000, 0x03, 0x00, fs, out r8_499, out r10_500);
			fn00000000000028A0(0xA31E, *rax_73, 0x01);
l00000000000050E4:
			word64 r8_497;
			word64 r10_498;
			quotearg_n_style_colon(r13_23->t0000, 0x03, 0x00, fs, out r8_497, out r10_498);
			uint64 rsi_344 = (uint64) *fn0000000000002560();
			fn00000000000028A0(0xA31E, (word32) rsi_344, 0x01);
			Eq_28 r8_354;
			uint64 rax_352 = closeout((word32) 0xA31E, (word32) rsi_344, (FILE *) 0x01, fs, out r8_354);
			r8Out = r8_354;
			return rax_352;
		}
	}
	else
	{
l000000000000500B:
		eax_199 = (word32) create(rbx_103, rdi_168, fs, out r8);
		if (eax_199 < 0x00)
			goto l0000000000004F8C;
	}
	r13_23->t0008 = eax_199;
	word64 rax_210 = fn0000000000002860("a", eax_199);
	r13_23->qw0010 = rax_210;
	ebp_360 = (word32) rbp_170;
	if (rax_210 != 0x00)
	{
		word32 eax_218 = g_dwF4F8;
		g_dwF4F8 = 0x00;
		r13_23->dw0018 = eax_218;
		goto l0000000000004F42;
	}
	goto l00000000000050E4;
}