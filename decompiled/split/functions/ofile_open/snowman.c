unsigned char ofile_open(void** rdi, void** rsi, void** rdx, void** rcx) {
    struct s0* r13_5;
    struct s1* rbp6;
    void* rsp7;
    void** rbx8;
    void** r15_9;
    void** r12_10;
    void** rdi11;
    void** r14_12;
    void** eax13;
    void* rsp14;
    void** rax15;
    void* rsp16;
    void** r8_17;
    void** rax18;
    void** rdi19;
    void** v20;
    int64_t rax21;
    void** eax22;
    int32_t eax23;
    int64_t rdi24;
    int64_t rax25;
    void** rax26;
    void** rax27;
    void** rcx28;
    void** esi29;
    void** eax30;
    void** rbp31;
    void** rbx32;
    void* rsp33;
    void** rax34;
    void** v35;
    int32_t eax36;
    int64_t rax37;
    void** rax38;
    int1_t zf39;
    void** rax40;
    int32_t eax41;
    void** rax42;
    void* rax43;
    int64_t v44;
    void*** rsi45;
    void** rdx46;
    void*** rax47;
    void** rax48;
    void** rcx49;
    void** rax50;
    int32_t eax51;
    int64_t r8_52;
    void** rax53;
    void** v54;
    int64_t rdi55;
    int1_t zf56;
    void** rdx57;
    void** rsi58;
    void** rdi59;
    void** eax60;
    void** rax61;
    void** rax62;
    void** rsi63;
    void** r15_64;
    void** r14_65;
    uint32_t ebp66;
    int32_t ebx67;
    void** v68;
    void** v69;
    void** rax70;
    void** rax71;
    int1_t zf72;
    int64_t v73;
    void** rax74;
    int1_t zf75;
    void** rax76;
    void** r12_77;
    unsigned char v78;
    void** rsi79;
    void** r11_80;
    uint32_t eax81;
    uint32_t edi82;
    uint32_t eax83;
    void* rbx84;
    uint1_t below_or_equal85;
    uint32_t eax86;
    uint32_t edi87;
    uint32_t eax88;
    void* rbx89;
    void** rbx90;
    void** rax91;
    void** edx92;

    r13_5 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi) << 5) + reinterpret_cast<unsigned char>(rdi));
    *reinterpret_cast<int32_t*>(&rbp6) = 0;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    if (reinterpret_cast<signed char>(r13_5->f8) < reinterpret_cast<signed char>(0)) {
        rbx8 = rsi + 0xffffffffffffffff;
        r15_9 = rdx + 0xffffffffffffffff;
        r12_10 = rdi;
        if (!rsi) {
            rbx8 = r15_9;
        }
        rdi11 = r13_5->f0;
        r14_12 = rsi;
        if (r13_5->f8 != 0xffffffff) {
            goto addr_4f78_6;
        }
        while (eax13 = create(rdi11, rsi, rdx, rcx), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) {
            do {
                rax15 = fun_2560();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
                r8_17 = rax15;
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax15) - 23) > 1) 
                    goto addr_4f9e_9;
                do {
                    rbp6 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx8) << 5) + reinterpret_cast<unsigned char>(r12_10));
                    if (rbp6->f8 >= 0) 
                        break;
                    rax18 = rbx8 + 0xffffffffffffffff;
                    if (!rbx8) {
                        rax18 = r15_9;
                    }
                    rbx8 = rax18;
                } while (rax18 != r14_12);
                goto addr_5050_14;
                rdi19 = rbp6->f10;
                v20 = r8_17;
                rax21 = rpl_fclose(rdi19, rdi19);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax21)) 
                    goto addr_50b6_16;
                rbp6->f8 = -2;
                eax22 = r13_5->f8;
                rbp6->f10 = reinterpret_cast<void**>(0);
                rdi11 = r13_5->f0;
                *reinterpret_cast<int32_t*>(&rbp6) = 1;
                if (reinterpret_cast<int1_t>(eax22 == 0xffffffff)) 
                    break;
                addr_4f78_6:
                *reinterpret_cast<int32_t*>(&rsi) = 0xc01;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                eax13 = open_safer(rdi11, 0xc01, rdx, rcx);
                rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            } while (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0));
            break;
        }
    } else {
        addr_4f42_18:
        eax23 = *reinterpret_cast<int32_t*>(&rbp6);
        return *reinterpret_cast<unsigned char*>(&eax23);
    }
    r13_5->f8 = eax13;
    *reinterpret_cast<void***>(&rdi24) = eax13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    rax25 = fun_2860(rdi24, "a");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    r13_5->f10 = rax25;
    if (!rax25) {
        addr_50e4_20:
        rax26 = quotearg_n_style_colon();
        rax27 = fun_2560();
        rcx28 = rax26;
        esi29 = *reinterpret_cast<void***>(rax27);
        fun_28a0();
    } else {
        eax30 = filter_pid;
        filter_pid = reinterpret_cast<void**>(0);
        r13_5->f18 = eax30;
        goto addr_4f42_18;
    }
    *reinterpret_cast<uint32_t*>(&rbp31) = reinterpret_cast<uint32_t>("%s");
    *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
    rbx32 = esi29;
    *reinterpret_cast<int32_t*>(&rbx32 + 4) = 0;
    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 72);
    rax34 = g28;
    v35 = rax34;
    if (0) {
        if (reinterpret_cast<signed char>(esi29) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_24;
        eax36 = fun_2710();
        rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
        if (eax36 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_27;
        }
    } else {
        rax37 = rpl_fclose(1);
        rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax37) && ((rax38 = fun_2560(), rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8), zf39 = filter_command == 0, zf39) || *reinterpret_cast<void***>(rax38) != 32)) {
            rax40 = quotearg_n_style_colon();
            rcx28 = rax40;
            fun_28a0();
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx32) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_24;
    }
    rcx28 = n_open_pipes;
    if (!rcx28) {
        addr_51e1_24:
        if (1) {
            addr_5250_27:
            eax41 = fun_28c0("%s", reinterpret_cast<int64_t>(rsp33) + 28);
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
            if (eax41 == -1 && (rax42 = fun_2560(), rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8), rbx32 = rax42, *reinterpret_cast<void***>(rax42) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_33;
            }
        } else {
            addr_51e5_34:
            rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v35) - reinterpret_cast<unsigned char>(g28));
            if (rax43) {
                fun_2670();
                goto addr_53b0_36;
            } else {
                goto v44;
            }
        }
    } else {
        rsi45 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx46) = 0;
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        rax47 = rsi45;
        do {
            if (*rax47 == rbx32) 
                goto addr_51d1_40;
            ++rdx46;
            rax47 = rax47 + 4;
        } while (rdx46 != rcx28);
        goto addr_51e1_24;
    }
    *reinterpret_cast<int32_t*>(&rcx28) = 0;
    rbx32 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx32 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_36:
            rax48 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx49) = *reinterpret_cast<int32_t*>(&rcx28);
            *reinterpret_cast<int32_t*>(&rcx49 + 4) = 0;
            fun_28a0();
        } else {
            *reinterpret_cast<uint32_t*>(&rbp31) = *reinterpret_cast<unsigned char*>(&rcx28 + 1);
            *reinterpret_cast<unsigned char*>(&rcx28 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx28 + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx28 + 1)) {
                rbx32 = filter_command;
                rax50 = quotearg_n_style_colon();
                fun_2640();
                *reinterpret_cast<uint32_t*>(&r8_17) = *reinterpret_cast<uint32_t*>(&rbp31);
                *reinterpret_cast<int32_t*>(&r8_17 + 4) = 0;
                rcx28 = rax50;
                fun_28a0();
                goto addr_51e5_34;
            }
        }
    } else {
        if (!0) {
            rbp31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 32);
            eax51 = sig2str(0, rbp31);
            if (eax51) {
                addr_5388_33:
                *reinterpret_cast<void***>(&r8_52) = rbx32;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
                fun_29f0(rbp31, 1, 19, "%d", r8_52);
                goto addr_529f_48;
            } else {
                addr_529f_48:
                rax53 = quotearg_n_style_colon();
                fun_2640();
                r8_17 = rbp31;
                rcx28 = rax53;
                fun_28a0();
                goto addr_51e5_34;
            }
        }
    }
    v54 = rbx32;
    if (0) {
        *reinterpret_cast<void***>(&rdi55) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
        goto addr_543f_51;
    }
    if (rax48 || (zf56 = elide_empty_files == 0, zf56)) {
        rcx49 = outfile;
        rdx57 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
        rsi58 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
        closeout(0, rsi58, rdx57, rcx49, 0, rsi58, rdx57, rcx49);
        next_file_name();
        rdi59 = outfile;
        eax60 = create(rdi59, rsi58, rdx57, rcx49, rdi59, rsi58, rdx57, rcx49);
        output_desc = eax60;
        *reinterpret_cast<void***>(&rdi55) = eax60;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
        if (reinterpret_cast<signed char>(eax60) < reinterpret_cast<signed char>(0)) {
            rax61 = quotearg_n_style_colon();
            rax62 = fun_2560();
            rsi63 = *reinterpret_cast<void***>(rax62);
            *reinterpret_cast<int32_t*>(&rsi63 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_64) = 0;
            *reinterpret_cast<int32_t*>(&r15_64 + 4) = 0;
            r14_65 = reinterpret_cast<void**>(1);
            ebp66 = 1;
            ebx67 = 1;
            v68 = rsi63;
            v69 = r8_17;
            if (rax61 != 0xffffffffffffffff) 
                goto addr_552a_56;
        } else {
            addr_543f_51:
            rax70 = full_write(rdi55, 0, rax48, rcx49, rdi55, 0, rax48, rcx49);
            if (rax70 != rax48) {
                rax71 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax71) == 32) || (zf72 = filter_command == 0, zf72)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_59;
                } else {
                    addr_549d_60:
                    goto v73;
                }
            }
        }
    } else {
        addr_5498_59:
        goto addr_549d_60;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp66)) {
            rax74 = fun_26e0();
            zf75 = reinterpret_cast<int1_t>(rax74 == 0xffffffffffffffff);
            if (!zf75) {
                r14_65 = reinterpret_cast<void**>(1);
            }
            if (!zf75) {
                ebx67 = 1;
            }
        }
        rax76 = safe_read();
        r12_77 = rax76;
        if (rax76 == 0xffffffffffffffff) 
            break;
        v78 = reinterpret_cast<uint1_t>(rax76 == 0);
        addr_5535_69:
        if (reinterpret_cast<unsigned char>(r12_77) < reinterpret_cast<unsigned char>(r14_65)) {
            rsi79 = v68;
        } else {
            rsi79 = v68;
            r11_80 = v69 + 0xffffffffffffffff;
            do {
                eax81 = 1;
                *reinterpret_cast<unsigned char*>(&ebp66) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp66) | *reinterpret_cast<unsigned char*>(&ebx67));
                if (*reinterpret_cast<unsigned char*>(&ebp66)) {
                    edi82 = *reinterpret_cast<unsigned char*>(&ebx67);
                    eax83 = cwrite(*reinterpret_cast<unsigned char*>(&edi82), rsi79, r14_65, *reinterpret_cast<unsigned char*>(&edi82), rsi79, r14_65);
                    r11_80 = r11_80;
                    rsi79 = rsi79;
                    ebp66 = eax83;
                    eax81 = eax83 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx84) = *reinterpret_cast<unsigned char*>(&ebx67);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx84) + 4) = 0;
                r15_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_64) + reinterpret_cast<uint64_t>(rbx84));
                below_or_equal85 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_64) <= reinterpret_cast<unsigned char>(r11_80));
                *reinterpret_cast<unsigned char*>(&ebx67) = below_or_equal85;
                if (below_or_equal85) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax81)) 
                    goto addr_5650_76;
                r12_77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_77) - reinterpret_cast<unsigned char>(r14_65));
                rsi79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi79) + reinterpret_cast<unsigned char>(r14_65));
                r14_65 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_77));
        }
        if (r12_77) {
            eax86 = 1;
            *reinterpret_cast<unsigned char*>(&ebp66) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp66) | *reinterpret_cast<unsigned char*>(&ebx67));
            if (*reinterpret_cast<unsigned char*>(&ebp66)) {
                edi87 = *reinterpret_cast<unsigned char*>(&ebx67);
                eax88 = cwrite(*reinterpret_cast<unsigned char*>(&edi87), rsi79, r12_77, *reinterpret_cast<unsigned char*>(&edi87), rsi79, r12_77);
                ebp66 = eax88;
                eax86 = eax88 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx89) = *reinterpret_cast<unsigned char*>(&ebx67);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx89) + 4) = 0;
            r15_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_64) + reinterpret_cast<uint64_t>(rbx89));
            *reinterpret_cast<unsigned char*>(&ebx67) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v69 == r15_64)) & *reinterpret_cast<unsigned char*>(&eax86));
            if (*reinterpret_cast<unsigned char*>(&ebx67)) 
                goto addr_567c_82;
            r14_65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_65) - reinterpret_cast<unsigned char>(r12_77));
        }
    } while (!v78);
    goto addr_5650_76;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_76:
    rbx90 = r15_64 + 1;
    if (reinterpret_cast<unsigned char>(v69) > reinterpret_cast<unsigned char>(r15_64)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax91 = rbx90;
            ++rbx90;
        } while (v69 != rax91);
    }
    addr_567c_82:
    goto v54;
    addr_552a_56:
    r12_77 = rax61;
    v78 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax61) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_69;
    addr_51d1_40:
    --rcx28;
    edx92 = rsi45[reinterpret_cast<unsigned char>(rcx28) * 4];
    n_open_pipes = rcx28;
    *rax47 = edx92;
    goto addr_51e1_24;
    addr_4f9e_9:
    addr_5083_89:
    v20 = r8_17;
    quotearg_n_style_colon();
    fun_28a0();
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
    addr_50b6_16:
    quotearg_n_style_colon();
    r8_17 = v20;
    fun_28a0();
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    goto addr_50e4_20;
    addr_5050_14:
    quotearg_n_style_colon();
    r8_17 = r8_17;
    fun_28a0();
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
    goto addr_5083_89;
}