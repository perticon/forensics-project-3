void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002950(fn0000000000002640(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000571E;
	}
	fn0000000000002880(fn0000000000002640(0x05, "Usage: %s [OPTION]... [FILE [PREFIX]]\n", null), 0x01);
	fn0000000000002770(stdout, fn0000000000002640(0x05, "Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is 'x'.\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002950(fn0000000000002640(0x05, "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with '-n'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            '\\0' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with '-n r/...'\n", null), 0x01, stdout);
	fn0000000000002770(stdout, fn0000000000002640(0x05, "      --verbose           print a diagnostic just before each\n                            output file is opened\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n", null));
	fn0000000000002770(stdout, fn0000000000002640(0x05, "\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like 'l' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n", null));
	struct Eq_4456 * rbx_225 = fp - 0xB8 + 16;
	do
	{
		Eq_28 rsi_227 = rbx_225->qw0000;
		++rbx_225;
	} while (rsi_227 != 0x00 && fn00000000000027A0(rsi_227, 0xA133) != 0x00);
	ptr64 r13_240 = rbx_225->qw0008;
	if (r13_240 != 0x00)
	{
		fn0000000000002880(fn0000000000002640(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_28 rax_327 = fn0000000000002870(null, 0x05);
		if (rax_327 == 0x00 || fn0000000000002570(0x03, "en_", rax_327) == 0x00)
			goto l00000000000059BE;
	}
	else
	{
		fn0000000000002880(fn0000000000002640(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_28 rax_269 = fn0000000000002870(null, 0x05);
		if (rax_269 == 0x00 || fn0000000000002570(0x03, "en_", rax_269) == 0x00)
		{
			fn0000000000002880(fn0000000000002640(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000059FB:
			fn0000000000002880(fn0000000000002640(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000571E:
			fn0000000000002930(edi);
		}
		r13_240 = 0xA133;
	}
	fn0000000000002770(stdout, fn0000000000002640(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000059BE:
	fn0000000000002880(fn0000000000002640(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000059FB;
}