bool cwrite(bool new_file_flag, char * bp, int64_t bytes) {
    int32_t v1; // 0x53e0
    if (!new_file_flag) {
        // 0x54a8
        v1 = output_desc;
    } else {
        if (((int64_t)bp || bytes) == 0) {
            // 0x53fb
            if (*(char *)&elide_empty_files != 0) {
                // 0x549d
                return true;
            }
        }
        // 0x5408
        closeout(NULL, output_desc, filter_pid, (char *)outfile);
        next_file_name();
        int32_t v2 = create((char *)outfile); // 0x542e
        output_desc = v2;
        v1 = v2;
        if (v2 < 0) {
            // 0x54b0
            quotearg_n_style_colon();
            function_2560();
            return function_28a0() % 2 != 0;
        }
    }
    // 0x543f
    if (full_write(v1, (int32_t *)bp, bytes) == bytes) {
        // 0x549d
        return true;
    }
    // 0x544f
    if (*(int32_t *)function_2560() != 32) {
        // 0x5468
        quotearg_n_style_colon();
        function_28a0();
        // 0x549d
        return true;
    }
    // 0x545c
    if (filter_command != 0) {
        // 0x549d
        return false;
    }
    // 0x5468
    quotearg_n_style_colon();
    function_28a0();
    // 0x549d
    return true;
}