uint32_t cwrite(unsigned char dil, void** rsi, void** rdx, ...) {
    int64_t v4;
    int64_t rbx5;
    int64_t rdi6;
    int1_t zf7;
    void** rcx8;
    void** rdx9;
    void** rsi10;
    void** rdi11;
    void** eax12;
    void** rax13;
    void** rax14;
    void** rsi15;
    uint64_t r15_16;
    void** r14_17;
    uint32_t ebp18;
    int32_t ebx19;
    void** v20;
    uint64_t v21;
    uint64_t r8_22;
    void** rax23;
    void** rax24;
    uint32_t eax25;
    int1_t zf26;
    void** rax27;
    int1_t zf28;
    void** rax29;
    void** r12_30;
    unsigned char v31;
    void** rsi32;
    uint64_t r11_33;
    uint32_t eax34;
    uint32_t edi35;
    uint32_t eax36;
    int64_t rbx37;
    uint1_t below_or_equal38;
    uint32_t eax39;
    uint32_t edi40;
    uint32_t eax41;
    int64_t rbx42;
    uint64_t rbx43;
    uint64_t rax44;

    v4 = rbx5;
    if (!dil) {
        *reinterpret_cast<void***>(&rdi6) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        goto addr_543f_3;
    }
    if (reinterpret_cast<unsigned char>(rsi) | reinterpret_cast<unsigned char>(rdx) || (zf7 = elide_empty_files == 0, zf7)) {
        rcx8 = outfile;
        rdx9 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        rsi10 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
        closeout(0, rsi10, rdx9, rcx8);
        next_file_name();
        rdi11 = outfile;
        eax12 = create(rdi11, rsi10, rdx9, rcx8);
        output_desc = eax12;
        *reinterpret_cast<void***>(&rdi6) = eax12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) {
            rax13 = quotearg_n_style_colon();
            rax14 = fun_2560();
            rsi15 = *reinterpret_cast<void***>(rax14);
            *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
            r14_17 = reinterpret_cast<void**>(1);
            ebp18 = 1;
            ebx19 = 1;
            v20 = rsi15;
            v21 = r8_22;
            if (rax13 != 0xffffffffffffffff) 
                goto addr_552a_8;
        } else {
            addr_543f_3:
            rax23 = full_write(rdi6, rsi, rdx, rcx8);
            if (rax23 != rdx) {
                rax24 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax24) == 32) || (eax25 = 0, zf26 = filter_command == 0, zf26)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_11;
                } else {
                    addr_549d_12:
                    return eax25;
                }
            }
        }
    } else {
        addr_5498_11:
        eax25 = 1;
        goto addr_549d_12;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp18)) {
            rax27 = fun_26e0();
            zf28 = reinterpret_cast<int1_t>(rax27 == 0xffffffffffffffff);
            if (!zf28) {
                r14_17 = reinterpret_cast<void**>(1);
            }
            if (!zf28) {
                ebx19 = 1;
            }
        }
        rax29 = safe_read();
        r12_30 = rax29;
        if (rax29 == 0xffffffffffffffff) 
            break;
        v31 = reinterpret_cast<uint1_t>(rax29 == 0);
        addr_5535_21:
        if (reinterpret_cast<unsigned char>(r12_30) < reinterpret_cast<unsigned char>(r14_17)) {
            rsi32 = v20;
        } else {
            rsi32 = v20;
            r11_33 = v21 - 1;
            do {
                eax34 = 1;
                *reinterpret_cast<unsigned char*>(&ebp18) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp18) | *reinterpret_cast<unsigned char*>(&ebx19));
                if (*reinterpret_cast<unsigned char*>(&ebp18)) {
                    edi35 = *reinterpret_cast<unsigned char*>(&ebx19);
                    eax36 = cwrite(*reinterpret_cast<unsigned char*>(&edi35), rsi32, r14_17);
                    r11_33 = r11_33;
                    rsi32 = rsi32;
                    ebp18 = eax36;
                    eax34 = eax36 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx37) = *reinterpret_cast<unsigned char*>(&ebx19);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx37) + 4) = 0;
                r15_16 = r15_16 + rbx37;
                below_or_equal38 = reinterpret_cast<uint1_t>(r15_16 <= r11_33);
                *reinterpret_cast<unsigned char*>(&ebx19) = below_or_equal38;
                if (below_or_equal38) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax34)) 
                    goto addr_5650_28;
                r12_30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_30) - reinterpret_cast<unsigned char>(r14_17));
                rsi32 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi32) + reinterpret_cast<unsigned char>(r14_17));
                r14_17 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_30));
        }
        if (r12_30) {
            eax39 = 1;
            *reinterpret_cast<unsigned char*>(&ebp18) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp18) | *reinterpret_cast<unsigned char*>(&ebx19));
            if (*reinterpret_cast<unsigned char*>(&ebp18)) {
                edi40 = *reinterpret_cast<unsigned char*>(&ebx19);
                eax41 = cwrite(*reinterpret_cast<unsigned char*>(&edi40), rsi32, r12_30);
                ebp18 = eax41;
                eax39 = eax41 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx42) = *reinterpret_cast<unsigned char*>(&ebx19);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx42) + 4) = 0;
            r15_16 = r15_16 + rbx42;
            *reinterpret_cast<unsigned char*>(&ebx19) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v21 == r15_16)) & *reinterpret_cast<unsigned char*>(&eax39));
            if (*reinterpret_cast<unsigned char*>(&ebx19)) 
                goto addr_567c_34;
            r14_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_17) - reinterpret_cast<unsigned char>(r12_30));
        }
    } while (!v31);
    goto addr_5650_28;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_28:
    rbx43 = r15_16 + 1;
    if (v21 > r15_16) {
        do {
            cwrite(1, 0, 0);
            rax44 = rbx43;
            ++rbx43;
        } while (v21 != rax44);
    }
    addr_567c_34:
    goto v4;
    addr_552a_8:
    r12_30 = rax13;
    v31 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_21;
}