undefined8 cwrite(char param_1,ulong param_2,ulong param_3)

{
  ulong uVar1;
  int *piVar2;
  undefined8 uVar3;
  
  if (param_1 != '\0') {
    if (((param_2 | param_3) == 0) && (elide_empty_files != '\0')) {
      return 1;
    }
    closeout(0,output_desc,filter_pid,outfile);
    next_file_name();
    output_desc = create(outfile);
    if (output_desc < 0) {
      uVar3 = quotearg_n_style_colon(0,3,outfile);
      piVar2 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar2,"%s",uVar3);
    }
  }
  uVar1 = full_write(output_desc,param_2,param_3);
  if (uVar1 == param_3) {
    return 1;
  }
  piVar2 = __errno_location();
  if ((*piVar2 == 0x20) && (filter_command != 0)) {
    return 0;
  }
  uVar3 = quotearg_n_style_colon(0,3,outfile);
                    /* WARNING: Subroutine does not return */
  error(1,*piVar2,"%s",uVar3);
}