void bytes_split(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** r14_7;
    void** r13_8;
    uint32_t ebp9;
    int32_t ebx10;
    void** v11;
    void** v12;
    void** r12_13;
    unsigned char v14;
    void** rax15;
    int1_t zf16;
    void** rax17;
    void** rsi18;
    void** r11_19;
    uint32_t eax20;
    uint32_t edi21;
    uint32_t eax22;
    void* rbx23;
    uint1_t below_or_equal24;
    uint32_t eax25;
    uint32_t edi26;
    uint32_t eax27;
    void* rbx28;
    void** rbx29;
    void** rax30;

    *reinterpret_cast<int32_t*>(&r15_6) = 0;
    *reinterpret_cast<int32_t*>(&r15_6 + 4) = 0;
    r14_7 = rdi;
    r13_8 = rdi;
    ebp9 = 1;
    ebx10 = 1;
    v11 = rsi;
    v12 = r8;
    if (rcx != 0xffffffffffffffff) {
        r12_13 = rcx;
        v14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(rdx));
        goto addr_5535_3;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp9)) {
            rax15 = fun_26e0();
            zf16 = reinterpret_cast<int1_t>(rax15 == 0xffffffffffffffff);
            if (!zf16) {
                r14_7 = r13_8;
            }
            if (!zf16) {
                ebx10 = 1;
            }
        }
        rax17 = safe_read();
        r12_13 = rax17;
        if (rax17 == 0xffffffffffffffff) 
            break;
        v14 = reinterpret_cast<uint1_t>(rax17 == 0);
        addr_5535_3:
        if (reinterpret_cast<unsigned char>(r12_13) < reinterpret_cast<unsigned char>(r14_7)) {
            rsi18 = v11;
        } else {
            rsi18 = v11;
            r11_19 = v12 + 0xffffffffffffffff;
            do {
                eax20 = 1;
                *reinterpret_cast<unsigned char*>(&ebp9) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp9) | *reinterpret_cast<unsigned char*>(&ebx10));
                if (*reinterpret_cast<unsigned char*>(&ebp9)) {
                    edi21 = *reinterpret_cast<unsigned char*>(&ebx10);
                    eax22 = cwrite(*reinterpret_cast<unsigned char*>(&edi21), rsi18, r14_7);
                    r11_19 = r11_19;
                    rsi18 = rsi18;
                    ebp9 = eax22;
                    eax20 = eax22 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx23) = *reinterpret_cast<unsigned char*>(&ebx10);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx23) + 4) = 0;
                r15_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_6) + reinterpret_cast<uint64_t>(rbx23));
                below_or_equal24 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_6) <= reinterpret_cast<unsigned char>(r11_19));
                *reinterpret_cast<unsigned char*>(&ebx10) = below_or_equal24;
                if (below_or_equal24) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax20)) 
                    goto addr_5650_18;
                r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) - reinterpret_cast<unsigned char>(r14_7));
                rsi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi18) + reinterpret_cast<unsigned char>(r14_7));
                r14_7 = r13_8;
            } while (reinterpret_cast<unsigned char>(r13_8) <= reinterpret_cast<unsigned char>(r12_13));
        }
        if (r12_13) {
            eax25 = 1;
            *reinterpret_cast<unsigned char*>(&ebp9) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp9) | *reinterpret_cast<unsigned char*>(&ebx10));
            if (*reinterpret_cast<unsigned char*>(&ebp9)) {
                edi26 = *reinterpret_cast<unsigned char*>(&ebx10);
                eax27 = cwrite(*reinterpret_cast<unsigned char*>(&edi26), rsi18, r12_13);
                ebp9 = eax27;
                eax25 = eax27 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx28) = *reinterpret_cast<unsigned char*>(&ebx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx28) + 4) = 0;
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_6) + reinterpret_cast<uint64_t>(rbx28));
            *reinterpret_cast<unsigned char*>(&ebx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v12 == r15_6)) & *reinterpret_cast<unsigned char*>(&eax25));
            if (*reinterpret_cast<unsigned char*>(&ebx10)) 
                goto addr_567c_24;
            r14_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_7) - reinterpret_cast<unsigned char>(r12_13));
        }
    } while (!v14);
    goto addr_5650_18;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_18:
    rbx29 = r15_6 + 1;
    if (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(r15_6)) {
        do {
            cwrite(1, 0, 0);
            rax30 = rbx29;
            ++rbx29;
        } while (v12 != rax30);
    }
    addr_567c_24:
    return;
}