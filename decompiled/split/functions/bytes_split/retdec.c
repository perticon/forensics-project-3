void bytes_split(int64_t n_bytes, char * buf, uint64_t bufsize, uint64_t initial_read, uint64_t max_files) {
    int64_t v1 = 1; // 0x5524
    int64_t v2 = 1; // 0x5524
    int64_t v3 = n_bytes; // 0x5524
    int64_t v4 = 0; // 0x5524
    int64_t v5; // 0x54f0
    int64_t v6; // 0x54f0
    int64_t v7; // 0x54f0
    int64_t v8; // 0x54f0
    int64_t v9; // 0x54f0
    bool v10; // 0x54f0
    if (initial_read == -1) {
        goto lab_0x55df;
    } else {
        // 0x552a
        v9 = 1;
        v8 = 1;
        v5 = initial_read;
        v6 = n_bytes;
        v7 = 0;
        v10 = initial_read < bufsize;
        goto lab_0x5535;
    }
  lab_0x55b0:;
    // 0x55b0
    int64_t v11; // 0x54f0
    int64_t v12 = v11;
    int64_t v13; // 0x54f0
    int64_t v14 = v13;
    int64_t v15; // 0x54f0
    int64_t v16 = v15;
    int64_t v17; // 0x54f0
    int64_t v18 = v17;
    int64_t v19; // 0x54f0
    uint64_t v20 = v19;
    v1 = v20;
    v2 = v18;
    v3 = v14;
    int64_t v21 = v12; // 0x55b3
    int64_t v22; // 0x54f0
    if (v16 != 0) {
        uint64_t v23 = v20 % 256;
        char v24 = 1; // 0x55bd
        int64_t v25 = v23 | v18; // 0x55bd
        if ((char)(v18 || v20) != 0) {
            bool v26 = cwrite(v20 % 2 != 0, (char *)v22, v16); // 0x5616
            v25 = v26 ? 0xffffffff : 0;
            v24 = v26 ? -2 : 1;
        }
        int64_t v27 = v12 + v23; // 0x55c2
        unsigned char v28 = v24 & (char)(v27 == max_files); // 0x55cd
        if (v28 != 0) {
            // 0x567c
            return;
        }
        // 0x55d5
        v1 = v28;
        v2 = v25;
        v3 = v14 - v16;
        v21 = v27;
    }
    // 0x55d8
    v4 = v21;
    int64_t v29 = v4; // 0x55dd
    if (v10) {
        goto lab_0x5650;
    } else {
        goto lab_0x55df;
    }
  lab_0x55df:;
    int64_t v30 = v3;
    int64_t v31 = v1;
    int64_t v32 = v31; // 0x55e2
    int64_t v33 = v30; // 0x55e2
    if ((char)v2 == 0) {
        int64_t v34 = function_26e0(); // 0x5632
        v32 = v34 != -1 ? 1 : v31 & 0xffffffff;
        v33 = v34 != -1 ? n_bytes : v30;
    }
    int64_t v35 = safe_read(0, buf, bufsize); // 0x55f0
    if (v35 == -1) {
        // 0x568b
        quotearg_n_style_colon();
        function_2560();
        function_28a0();
        return;
    }
    // 0x5602
    v9 = v32;
    v8 = v2;
    v5 = v35;
    v6 = v33;
    v7 = v4;
    v10 = v35 == 0;
    goto lab_0x5535;
  lab_0x5535:;
    int64_t v36 = v7;
    int64_t v37 = v6;
    int64_t v38 = v5;
    int64_t v39 = v8;
    int64_t v40 = v9;
    int64_t v41 = (int64_t)buf;
    v19 = v40;
    v17 = v39;
    v22 = v41;
    v15 = v38;
    v13 = v37;
    v11 = v36;
    if (v38 < v37) {
        goto lab_0x55b0;
    } else {
        int64_t v42 = v38; // 0x5548
        int64_t v43 = v36; // 0x5548
        int64_t v44 = v37;
        int64_t v45 = v41;
        uint64_t v46 = v40;
        uint64_t v47 = v46 % 256;
        int64_t v48 = v47 | v39; // 0x557c
        if ((char)(v39 || v46) != 0) {
            // 0x557e
            v48 = cwrite(v46 % 2 != 0, (char *)v45, v44) ? 0xffffffff : 0;
        }
        // 0x5550
        v43 += v47;
        v29 = v43;
        while (v43 <= max_files - 1) {
            int64_t v49 = v48;
            v42 -= v44;
            int64_t v50 = v44 + v45; // 0x5569
            v19 = 1;
            v17 = v49;
            v22 = v50;
            v15 = v42;
            v13 = n_bytes;
            v11 = v43;
            if (v42 < n_bytes) {
                goto lab_0x55b0;
            }
            v44 = n_bytes;
            v45 = v50;
            v46 = 1;
            v47 = v46 % 256;
            v48 = v47 | v49;
            if ((char)(v49 || v46) != 0) {
                // 0x557e
                v48 = cwrite(v46 % 2 != 0, (char *)v45, v44) ? 0xffffffff : 0;
            }
            // 0x5550
            v43 += v47;
            v29 = v43;
        }
        goto lab_0x5650;
    }
  lab_0x5650:
    // 0x5650
    if (v29 >= max_files) {
        // 0x567c
        return;
    }
    int64_t v51 = v29 + 1;
    cwrite(true, NULL, 0);
    int64_t v52 = v51; // 0x567a
    while (v51 != max_files) {
        // 0x5660
        v51 = v52 + 1;
        cwrite(true, NULL, 0);
        v52 = v51;
    }
}