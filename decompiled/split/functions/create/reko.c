uint64 create(Eq_28 rbx, Eq_28 rdi, struct Eq_596 * fs, union Eq_28 & r8Out)
{
	ptr64 fp;
	word64 qwLocB0;
	word64 qwLocB8;
	ui32 dwLocA0;
	Eq_28 r8;
	Eq_28 rdx;
	Eq_28 rsi;
	Eq_88 r13d_308;
	word32 rax_32_32_438;
	Eq_28 r13_159;
	Eq_28 r12_11 = rdi;
	word64 rax_17 = fs->qw0028;
	if (filter_command != 0x00)
	{
		Eq_28 rax_24 = fn0000000000002520("SHELL");
		r13_159 = rax_24;
		word32 eax_36 = (word32) 0xA018;
		rax_32_32_438 = SLICE(0xA018, word32, 32);
		if (rax_24 == 0x00)
			r13_159.u0 = 0xA018;
		fn00000000000025E0();
		if (eax_36 != 0x00)
		{
l0000000000004E92:
			fn00000000000028A0(fn0000000000002640(0x05, "failed to set FILE environment variable", null), *fn0000000000002560(), 0x01);
			goto l0000000000004EBE;
		}
		if (g_bF2E2 != 0x00)
			goto l0000000000004C98;
l0000000000004A56:
		Eq_88 dwLocB8_730 = (word32) qwLocB8;
		Eq_88 dwLocB4_727 = SLICE(qwLocB8, word32, 32);
		Mem56 = Mem18;
		word32 eax_45 = fn0000000000002720(fp - 0xB8);
		if (eax_45 != 0x00)
			goto l0000000000004E66;
		fn00000000000029D0();
		if (eax_45 != 0x00)
		{
			if (eax_45 != ~0x00)
			{
				int32 eax_425 = fn0000000000002710(dwLocB8_730);
				uint64 rax_442 = SEQ(rax_32_32_438, eax_425);
				if (eax_425 == 0x00)
				{
					uint64 rdx_429 = n_open_pipes;
					g_dwF4F8 = eax_45;
					struct Eq_3515 * rdi_433 = open_pipes;
					if (rdx_429 == open_pipes_alloc)
					{
						struct Eq_3515 * rax_445 = x2nrealloc(rax_442, 0x04, &open_pipes_alloc, rdi_433);
						rdx_429 = n_open_pipes;
						open_pipes = rax_445;
						rdi_433 = rax_445;
					}
					n_open_pipes = rdx_429 + 1;
					rdi_433[rdx_429] = (struct Eq_3515) dwLocB4_727;
					r13d_308 = dwLocB4_727;
l0000000000004AC2:
					if (rax_17 - fs->qw0028 != 0x00)
					{
l0000000000004DA3:
						fn0000000000002670();
					}
					else
					{
						r8Out = r8;
						return (uint64) r13d_308;
					}
				}
l0000000000004EEA:
				fn00000000000028A0(fn0000000000002640(0x05, "failed to close input pipe", null), *fn0000000000002560(), 0x01);
				Eq_28 r8_708;
				uint64 rax_704 = ofile_open(rdx, rsi, 0x01, fs, out r8_708);
				r8Out = r8_708;
				return rax_704;
			}
l0000000000004EBE:
			fn00000000000028A0(fn0000000000002640(0x05, "fork system call failed", null), *fn0000000000002560(), 0x01);
			goto l0000000000004EEA;
		}
		uint64 rbx_55;
		for (rbx_55 = 0x00; rbx_55 < n_open_pipes; ++rbx_55)
		{
			if (fn0000000000002710(open_pipes[rbx_55]) != 0x00)
			{
				fn00000000000028A0(fn0000000000002640(0x05, "closing prior pipe", null), *fn0000000000002560(), 0x01);
				goto l0000000000004DA3;
			}
		}
		r12_11 = (uint64) fn0000000000002710(dwLocB4_727);
		Eq_28 rax_100 = fn0000000000002560();
		word32 eax_117 = (word32) rax_100;
		rbx = rax_100;
		if ((word32) r12_11 != 0x00)
		{
			fn00000000000028A0(fn0000000000002640(0x05, "closing output pipe", null), *rax_100, 0x01);
			goto l0000000000004DCC;
		}
		if (dwLocB8_730 != 0x00)
		{
			fn00000000000026A0();
			if (eax_117 != 0x00)
			{
l0000000000004E42:
				fn00000000000028A0(fn0000000000002640(0x05, "moving input pipe", null), *rbx, 0x01);
l0000000000004E66:
				fn00000000000028A0(fn0000000000002640(0x05, "failed to create pipe", null), *fn0000000000002560(), 0x01);
				goto l0000000000004E92;
			}
			if (fn0000000000002710(dwLocB8_730) != 0x00)
			{
				fn00000000000028A0(fn0000000000002640(0x05, "closing input pipe", null), *rax_100, 0x01);
				rdi.u0 = 0x01;
l0000000000004D08:
				quotearg_style(rdi, 0x04, fs, out r8);
				fn0000000000002950(fn0000000000002640(0x05, "creating file %s\n", null), 0x01, stdout);
l0000000000004BDD:
				Eq_88 eax_281 = open_safer(0x41, r12_11, fs);
				Mem295 = Mem18;
				r13d_308 = eax_281;
				if (eax_281 < 0x00)
					goto l0000000000004AC2;
				if (fn00000000000029B0(fp - 0xB8, eax_281) != 0x00)
				{
l0000000000004E03:
					word64 r8_888;
					quotearg_style(r12_11, 0x04, fs, out r8_888);
					fn00000000000028A0(fn0000000000002640(0x05, "failed to stat %s", null), *fn0000000000002560(), 0x01);
					goto l0000000000004E42;
				}
				if (g_qwF308 == qwLocB0 && g_qwF300 == qwLocB8)
				{
l0000000000004DCC:
					word64 r8_889;
					r12_11 = quotearg_style(r12_11, 0x04, fs, out r8_889);
					fn00000000000028A0(fn0000000000002640(0x05, "%s would overwrite input; aborting", null), 0x00, 0x01);
					goto l0000000000004E03;
				}
				if (fn00000000000026D0(0x00, eax_281) == 0x00 || (dwLocA0 & 0xF000) != 0x8000)
					goto l0000000000004AC2;
				word64 r8_890;
				word64 r10_891;
				r13_159 = quotearg_n_style_colon(r12_11, 0x03, 0x00, fs, out r8_890, out r10_891);
				Eq_28 rax_358 = fn0000000000002640(0x05, "%s: error truncating", null);
				fn00000000000028A0(rax_358, *fn0000000000002560(), 0x01);
				r12_11 = rax_358;
l0000000000004C98:
				word64 r10_892;
				quotearg_n_style_colon(r12_11, 0x03, 0x00, fs, out r8, out r10_892);
				fn0000000000002950(fn0000000000002640(0x05, "executing with FILE=%s\n", null), 0x01, stdout);
				rax_32_32_438 = 0x00;
				goto l0000000000004A56;
			}
		}
		fn0000000000002530(null, &g_tF460, 0x02);
		r12_11 = filter_command;
		last_component(r13_159);
		fn0000000000002990();
		fn00000000000028A0(fn0000000000002640(0x05, "failed to run command: \"%s -c %s\"", null), *rax_100, 0x01);
		r8 = r12_11;
		rdi.u0 = 0x01;
	}
	if (g_bF2E2 == 0x00)
		goto l0000000000004BDD;
	goto l0000000000004D08;
}