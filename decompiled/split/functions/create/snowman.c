void** create(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void** r12_5;
    void* rsp6;
    void** rax7;
    void** v8;
    int1_t zf9;
    void** rax10;
    void** rdx11;
    void** rsi12;
    void** r13_13;
    int32_t eax14;
    void* rsp15;
    int1_t zf16;
    void* rsp17;
    int32_t* rsp18;
    void* rsp19;
    void** rax20;
    void** rdx21;
    int64_t* rsp22;
    void** rax23;
    void** rax24;
    void** rax25;
    void* rsp26;
    void* rsp27;
    void** r8_28;
    void* rsp29;
    void** v30;
    void* rsp31;
    void** v32;
    void** r12_33;
    void** rax34;
    unsigned char v35;
    void** rcx36;
    void** edx37;
    void*** rsi38;
    void*** rax39;
    void** rax40;
    uint32_t ebp41;
    void** rax42;
    int1_t zf43;
    void** r14_44;
    int32_t ebx45;
    void** rax46;
    void** rsi47;
    void** v48;
    void** r11_49;
    void** v50;
    uint32_t eax51;
    uint32_t edi52;
    uint32_t eax53;
    void* rbx54;
    void** r15_55;
    uint1_t below_or_equal56;
    uint32_t eax57;
    uint32_t edi58;
    uint32_t eax59;
    void* rbx60;
    void** rbx61;
    void** rax62;
    void** rax63;
    void** rax64;
    void** rdi65;
    void** eax66;
    void** r13d67;
    int32_t eax68;
    int1_t zf69;
    int64_t v70;
    int1_t zf71;
    int64_t v72;
    int64_t rdi73;
    int32_t eax74;
    uint32_t v75;
    void** rax76;
    void** rax77;
    void** rax78;
    void** rdi79;
    int32_t eax80;
    void** eax81;
    void** rbx82;
    int1_t zf83;
    int32_t eax84;
    int1_t below_or_equal85;
    int32_t eax86;
    int32_t v87;
    int32_t eax88;
    int32_t eax89;
    void** rax90;
    void** rax91;
    int1_t zf92;
    void** rax93;
    void* rax94;
    int32_t eax95;
    void** rax96;
    void** rax97;
    void** rdx98;
    void** rsi99;
    void** rdx100;
    int1_t zf101;
    void*** rdi102;
    void*** rax103;
    void** v104;
    void** v105;
    struct s2* r13_106;
    void** rbx107;
    void** r15_108;
    void** rdi109;
    void** r14_110;
    void** eax111;
    void* rsp112;
    void** rax113;
    struct s3* rbp114;
    void** rax115;
    void** rdi116;
    int64_t rax117;
    void** eax118;
    int64_t v119;
    int64_t rdi120;
    int64_t rax121;
    void** rax122;
    void** rax123;
    void** esi124;
    void** eax125;
    void** rbp126;
    void** rbx127;
    void* rsp128;
    void** rax129;
    void** v130;
    int32_t eax131;
    int64_t rax132;
    void** rax133;
    int1_t zf134;
    void** rax135;
    int32_t eax136;
    void** rax137;
    void* rax138;
    int64_t v139;
    void** rdx140;
    void** rax141;
    void** rcx142;
    void** rax143;
    int32_t eax144;
    int64_t r8_145;
    void** rax146;
    int64_t rdi147;
    int1_t zf148;
    void** rdx149;
    void** rsi150;
    void** rdi151;
    void** eax152;
    void** rax153;
    void** rsi154;
    void** rax155;
    void** rax156;
    int1_t zf157;
    int64_t v158;

    r12_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0xa0);
    rax7 = g28;
    v8 = rax7;
    zf9 = filter_command == 0;
    if (zf9) 
        goto addr_4bd0_2;
    rax10 = fun_2520("SHELL", rsi, rdx, rcx);
    *reinterpret_cast<int32_t*>(&rdx11) = 1;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    rsi12 = r12_5;
    r13_13 = rax10;
    if (!rax10) {
        r13_13 = reinterpret_cast<void**>("/bin/sh");
    }
    eax14 = fun_25e0("FILE", rsi12, 1, rcx);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
    if (!eax14) {
        zf16 = verbose == 0;
        if (!zf16) 
            goto addr_4c98_7; else 
            goto addr_4a56_8;
    }
    addr_4e92_9:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4ebe_10;
    addr_4da3_11:
    fun_2670();
    rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    addr_4da8_12:
    rax20 = fun_2640();
    rdx21 = rax20;
    fun_28a0();
    rsp22 = reinterpret_cast<int64_t*>(rsp18 - 2 + 2 - 2 + 2);
    addr_4dcc_13:
    rax23 = quotearg_style(4, r12_5, rdx21, rcx);
    r12_5 = rax23;
    rax24 = fun_2640();
    rcx = r12_5;
    rdx21 = rax24;
    fun_28a0();
    rsp22 = rsp22 - 1 + 1 - 1 + 1 - 1 + 1;
    addr_4e03_14:
    rax25 = quotearg_style(4, r12_5, rdx21, rcx);
    fun_2640();
    fun_2560();
    rcx = rax25;
    fun_28a0();
    rsp26 = reinterpret_cast<void*>(rsp22 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1);
    addr_4e42_15:
    fun_2640();
    fun_28a0();
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
    addr_4e66_16:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4e92_9;
    addr_5050_17:
    quotearg_n_style_colon();
    r8_28 = r8_28;
    fun_28a0();
    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8 - 8 + 8);
    addr_5083_18:
    v30 = r8_28;
    quotearg_n_style_colon();
    fun_28a0();
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8 - 8 + 8);
    addr_50b6_19:
    quotearg_n_style_colon();
    r8_28 = v30;
    fun_28a0();
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
    goto addr_50e4_20;
    addr_568b_21:
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_567c_23:
    goto v32;
    addr_552a_24:
    r12_33 = rax34;
    v35 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax34) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_25;
    addr_51d1_26:
    --rcx36;
    edx37 = rsi38[reinterpret_cast<unsigned char>(rcx36) * 4];
    n_open_pipes = rcx36;
    *rax39 = edx37;
    goto addr_51e1_27;
    addr_4f9e_28:
    goto addr_5083_18;
    addr_4d77_29:
    rax40 = fun_2640();
    r12_5 = rax40;
    fun_2560();
    fun_28a0();
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4da3_11;
    do {
        addr_55df_30:
        if (!*reinterpret_cast<unsigned char*>(&ebp41)) {
            rax42 = fun_26e0();
            zf43 = reinterpret_cast<int1_t>(rax42 == 0xffffffffffffffff);
            if (!zf43) {
                r14_44 = reinterpret_cast<void**>(1);
            }
            if (!zf43) {
                ebx45 = 1;
            }
        }
        rax46 = safe_read();
        r12_33 = rax46;
        if (rax46 == 0xffffffffffffffff) 
            goto addr_568b_21;
        v35 = reinterpret_cast<uint1_t>(rax46 == 0);
        addr_5535_25:
        if (reinterpret_cast<unsigned char>(r12_33) < reinterpret_cast<unsigned char>(r14_44)) {
            rsi47 = v48;
        } else {
            rsi47 = v48;
            r11_49 = v50 + 0xffffffffffffffff;
            do {
                eax51 = 1;
                *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx45));
                if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                    edi52 = *reinterpret_cast<unsigned char*>(&ebx45);
                    eax53 = cwrite(*reinterpret_cast<unsigned char*>(&edi52), rsi47, r14_44, *reinterpret_cast<unsigned char*>(&edi52), rsi47, r14_44);
                    r11_49 = r11_49;
                    rsi47 = rsi47;
                    ebp41 = eax53;
                    eax51 = eax53 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx54) = *reinterpret_cast<unsigned char*>(&ebx45);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx54) + 4) = 0;
                r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_55) + reinterpret_cast<uint64_t>(rbx54));
                below_or_equal56 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_55) <= reinterpret_cast<unsigned char>(r11_49));
                *reinterpret_cast<unsigned char*>(&ebx45) = below_or_equal56;
                if (below_or_equal56) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax51)) 
                    goto addr_5650_44;
                r12_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_33) - reinterpret_cast<unsigned char>(r14_44));
                rsi47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi47) + reinterpret_cast<unsigned char>(r14_44));
                r14_44 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_33));
        }
        if (r12_33) {
            eax57 = 1;
            *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx45));
            if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                edi58 = *reinterpret_cast<unsigned char*>(&ebx45);
                eax59 = cwrite(*reinterpret_cast<unsigned char*>(&edi58), rsi47, r12_33, *reinterpret_cast<unsigned char*>(&edi58), rsi47, r12_33);
                ebp41 = eax59;
                eax57 = eax59 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx60) = *reinterpret_cast<unsigned char*>(&ebx45);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx60) + 4) = 0;
            r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_55) + reinterpret_cast<uint64_t>(rbx60));
            *reinterpret_cast<unsigned char*>(&ebx45) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v50 == r15_55)) & *reinterpret_cast<unsigned char*>(&eax57));
            if (*reinterpret_cast<unsigned char*>(&ebx45)) 
                goto addr_567c_23;
            r14_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_44) - reinterpret_cast<unsigned char>(r12_33));
        }
    } while (!v35);
    addr_5650_44:
    rbx61 = r15_55 + 1;
    if (reinterpret_cast<unsigned char>(v50) > reinterpret_cast<unsigned char>(r15_55)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax62 = rbx61;
            ++rbx61;
        } while (v50 != rax62);
        goto addr_567c_23;
    }
    while (1) {
        addr_4d08_54:
        rax63 = quotearg_style(4, rdi, rdx, rcx);
        rax64 = fun_2640();
        rdi65 = stdout;
        rcx = rax63;
        fun_2950(rdi65, 1, rax64, rcx, r8_28);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8);
        while (1) {
            *reinterpret_cast<int32_t*>(&rdx21) = 0x1b6;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            eax66 = open_safer(r12_5, 65, 0x1b6, rcx);
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            r13d67 = eax66;
            if (reinterpret_cast<signed char>(eax66) < reinterpret_cast<signed char>(0)) 
                goto addr_4ac2_56;
            eax68 = fun_29b0();
            rsp22 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            if (eax68) 
                goto addr_4e03_14;
            zf69 = gf308 == v70;
            if (!zf69) 
                goto addr_4c2d_59;
            zf71 = in_stat_buf == v72;
            if (zf71) 
                goto addr_4dcc_13;
            addr_4c2d_59:
            *reinterpret_cast<void***>(&rdi73) = r13d67;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
            eax74 = fun_26d0(rdi73);
            rsp19 = reinterpret_cast<void*>(rsp22 - 1 + 1);
            if (!eax74) 
                goto addr_4ac2_56;
            if ((v75 & 0xf000) != 0x8000) 
                goto addr_4ac2_56;
            rax76 = quotearg_n_style_colon();
            r13_13 = rax76;
            fun_2640();
            fun_2560();
            fun_28a0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4c98_7:
            rax77 = quotearg_n_style_colon();
            r12_5 = rax77;
            rax78 = fun_2640();
            rdi79 = stdout;
            rcx = r12_5;
            *reinterpret_cast<int32_t*>(&rsi12) = 1;
            *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
            rdx11 = rax78;
            fun_2950(rdi79, 1, rdx11, rcx, r8_28);
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4a56_8:
            eax80 = fun_2720(rsp15, rsi12, rdx11, rcx);
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            if (eax80) 
                goto addr_4e66_16;
            eax81 = fun_29d0(rsp15, rsi12, rdx11, rcx);
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
            if (eax81) 
                goto addr_4a71_64;
            rbx82 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rbx82 + 4) = 0;
            zf83 = n_open_pipes == 0;
            if (!zf83) {
                do {
                    eax84 = fun_2710();
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                    if (eax84) 
                        goto addr_4d77_29;
                    ++rbx82;
                    below_or_equal85 = reinterpret_cast<unsigned char>(n_open_pipes) <= reinterpret_cast<unsigned char>(rbx82);
                } while (!below_or_equal85);
            }
            eax86 = fun_2710();
            *reinterpret_cast<int32_t*>(&r12_5) = eax86;
            *reinterpret_cast<int32_t*>(&r12_5 + 4) = 0;
            fun_2560();
            rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8);
            if (*reinterpret_cast<int32_t*>(&r12_5)) 
                goto addr_4da8_12;
            if (v87) {
                eax88 = fun_26a0();
                rsp26 = reinterpret_cast<void*>(rsp18 - 2 + 2);
                if (eax88) 
                    goto addr_4e42_15;
                eax89 = fun_2710();
                rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                if (eax89) 
                    break;
            }
            fun_2530();
            r12_5 = filter_command;
            rax90 = last_component(r13_13, 0xf460);
            fun_2990(r13_13, rax90, "-c", r12_5);
            rax91 = fun_2640();
            r8_28 = r12_5;
            rcx = r13_13;
            rdx = rax91;
            *reinterpret_cast<int32_t*>(&rdi) = 1;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_28a0();
            rsp6 = reinterpret_cast<void*>(rsp18 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2);
            addr_4bd0_2:
            zf92 = verbose == 0;
            if (!zf92) 
                goto addr_4d08_54;
        }
        rax93 = fun_2640();
        *reinterpret_cast<int32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        rdx = rax93;
        fun_28a0();
        rsp6 = reinterpret_cast<void*>(rsp18 - 2 + 2 - 2 + 2);
    }
    addr_4ac2_56:
    rax94 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rax94) {
        return r13d67;
    }
    addr_4a71_64:
    if (eax81 == 0xffffffff) {
        addr_4ebe_10:
        fun_2640();
        fun_2560();
        fun_28a0();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_4eea_76;
    } else {
        eax95 = fun_2710();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
        if (eax95) {
            addr_4eea_76:
            rax96 = fun_2640();
            rax97 = fun_2560();
            rdx98 = rax96;
            rsi99 = *reinterpret_cast<void***>(rax97);
            *reinterpret_cast<int32_t*>(&rsi99 + 4) = 0;
            fun_28a0();
        } else {
            rdx100 = n_open_pipes;
            zf101 = rdx100 == open_pipes_alloc;
            filter_pid = eax81;
            rdi102 = open_pipes;
            if (zf101) {
                rax103 = x2nrealloc();
                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                rdx100 = n_open_pipes;
                open_pipes = rax103;
                rdi102 = rax103;
            }
            n_open_pipes = rdx100 + 1;
            rdi102[reinterpret_cast<unsigned char>(rdx100) * 4] = v104;
            r13d67 = v105;
            goto addr_4ac2_56;
        }
    }
    r13_106 = reinterpret_cast<struct s2*>((reinterpret_cast<unsigned char>(rsi99) << 5) + 1);
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    if (reinterpret_cast<signed char>(r13_106->f8) < reinterpret_cast<signed char>(0)) {
        rbx107 = rsi99 + 0xffffffffffffffff;
        r15_108 = rdx98 + 0xffffffffffffffff;
        if (!rsi99) {
            rbx107 = r15_108;
        }
        rdi109 = r13_106->f0;
        r14_110 = rsi99;
        if (r13_106->f8 != 0xffffffff) {
            goto addr_4f78_86;
        }
        while (eax111 = create(rdi109, rsi99, rdx98, rcx), rsp112 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8), reinterpret_cast<signed char>(eax111) < reinterpret_cast<signed char>(0)) {
            do {
                rax113 = fun_2560();
                rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp112) - 8 + 8);
                r8_28 = rax113;
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax113) - 23) > 1) 
                    goto addr_4f9e_28;
                do {
                    rbp114 = reinterpret_cast<struct s3*>((reinterpret_cast<unsigned char>(rbx107) << 5) + 1);
                    if (rbp114->f8 >= 0) 
                        break;
                    rax115 = rbx107 + 0xffffffffffffffff;
                    if (!rbx107) {
                        rax115 = r15_108;
                    }
                    rbx107 = rax115;
                } while (rax115 != r14_110);
                goto addr_5050_17;
                rdi116 = rbp114->f10;
                v30 = r8_28;
                rax117 = rpl_fclose(rdi116, rdi116);
                rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax117)) 
                    goto addr_50b6_19;
                rbp114->f8 = -2;
                eax118 = r13_106->f8;
                rbp114->f10 = reinterpret_cast<void**>(0);
                rdi109 = r13_106->f0;
                if (reinterpret_cast<int1_t>(eax118 == 0xffffffff)) 
                    break;
                addr_4f78_86:
                rsi99 = reinterpret_cast<void**>(0xc01);
                *reinterpret_cast<int32_t*>(&rsi99 + 4) = 0;
                eax111 = open_safer(rdi109, 0xc01, rdx98, rcx);
                rsp112 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            } while (reinterpret_cast<signed char>(eax111) < reinterpret_cast<signed char>(0));
            break;
        }
    } else {
        addr_4f42_95:
        goto v119;
    }
    r13_106->f8 = eax111;
    *reinterpret_cast<void***>(&rdi120) = eax111;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi120) + 4) = 0;
    rax121 = fun_2860(rdi120, "a");
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp112) - 8 + 8);
    r13_106->f10 = rax121;
    if (!rax121) {
        addr_50e4_20:
        rax122 = quotearg_n_style_colon();
        rax123 = fun_2560();
        rcx36 = rax122;
        esi124 = *reinterpret_cast<void***>(rax123);
        fun_28a0();
    } else {
        eax125 = filter_pid;
        filter_pid = reinterpret_cast<void**>(0);
        r13_106->f18 = eax125;
        goto addr_4f42_95;
    }
    *reinterpret_cast<uint32_t*>(&rbp126) = reinterpret_cast<uint32_t>("%s");
    *reinterpret_cast<int32_t*>(&rbp126 + 4) = 0;
    rbx127 = esi124;
    *reinterpret_cast<int32_t*>(&rbx127 + 4) = 0;
    rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 72);
    rax129 = g28;
    v130 = rax129;
    if (0) {
        if (reinterpret_cast<signed char>(esi124) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_27;
        eax131 = fun_2710();
        rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
        if (eax131 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_102;
        }
    } else {
        rax132 = rpl_fclose(1);
        rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax132) && ((rax133 = fun_2560(), rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8), zf134 = filter_command == 0, zf134) || *reinterpret_cast<void***>(rax133) != 32)) {
            rax135 = quotearg_n_style_colon();
            rcx36 = rax135;
            fun_28a0();
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx127) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_27;
    }
    rcx36 = n_open_pipes;
    if (!rcx36) {
        addr_51e1_27:
        if (1) {
            addr_5250_102:
            eax136 = fun_28c0("%s", reinterpret_cast<int64_t>(rsp128) + 28);
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
            if (eax136 == -1 && (rax137 = fun_2560(), rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8), rbx127 = rax137, *reinterpret_cast<void***>(rax137) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_108;
            }
        } else {
            addr_51e5_109:
            rax138 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v130) - reinterpret_cast<unsigned char>(g28));
            if (rax138) {
                fun_2670();
                goto addr_53b0_111;
            } else {
                goto v139;
            }
        }
    } else {
        rsi38 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx140) = 0;
        *reinterpret_cast<int32_t*>(&rdx140 + 4) = 0;
        rax39 = rsi38;
        do {
            if (*rax39 == rbx127) 
                goto addr_51d1_26;
            ++rdx140;
            rax39 = rax39 + 4;
        } while (rdx140 != rcx36);
        goto addr_51e1_27;
    }
    *reinterpret_cast<int32_t*>(&rcx36) = 0;
    rbx127 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx127 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_111:
            rax141 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx142) = *reinterpret_cast<int32_t*>(&rcx36);
            *reinterpret_cast<int32_t*>(&rcx142 + 4) = 0;
            fun_28a0();
        } else {
            *reinterpret_cast<uint32_t*>(&rbp126) = *reinterpret_cast<unsigned char*>(&rcx36 + 1);
            *reinterpret_cast<unsigned char*>(&rcx36 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx36 + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx36 + 1)) {
                rbx127 = filter_command;
                rax143 = quotearg_n_style_colon();
                fun_2640();
                *reinterpret_cast<uint32_t*>(&r8_28) = *reinterpret_cast<uint32_t*>(&rbp126);
                *reinterpret_cast<int32_t*>(&r8_28 + 4) = 0;
                rcx36 = rax143;
                fun_28a0();
                goto addr_51e5_109;
            }
        }
    } else {
        if (!0) {
            rbp126 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp128) + 32);
            eax144 = sig2str(0, rbp126);
            if (eax144) {
                addr_5388_108:
                *reinterpret_cast<void***>(&r8_145) = rbx127;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_145) + 4) = 0;
                fun_29f0(rbp126, 1, 19, "%d", r8_145);
                goto addr_529f_122;
            } else {
                addr_529f_122:
                rax146 = quotearg_n_style_colon();
                fun_2640();
                r8_28 = rbp126;
                rcx36 = rax146;
                fun_28a0();
                goto addr_51e5_109;
            }
        }
    }
    v32 = rbx127;
    if (0) {
        *reinterpret_cast<void***>(&rdi147) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi147) + 4) = 0;
        goto addr_543f_125;
    }
    if (rax141 || (zf148 = elide_empty_files == 0, zf148)) {
        rcx142 = outfile;
        rdx149 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx149 + 4) = 0;
        rsi150 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi150 + 4) = 0;
        closeout(0, rsi150, rdx149, rcx142, 0, rsi150, rdx149, rcx142);
        next_file_name();
        rdi151 = outfile;
        eax152 = create(rdi151, rsi150, rdx149, rcx142, rdi151, rsi150, rdx149, rcx142);
        output_desc = eax152;
        *reinterpret_cast<void***>(&rdi147) = eax152;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi147) + 4) = 0;
        if (reinterpret_cast<signed char>(eax152) < reinterpret_cast<signed char>(0)) {
            rax34 = quotearg_n_style_colon();
            rax153 = fun_2560();
            rsi154 = *reinterpret_cast<void***>(rax153);
            *reinterpret_cast<int32_t*>(&rsi154 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_55) = 0;
            *reinterpret_cast<int32_t*>(&r15_55 + 4) = 0;
            r14_44 = reinterpret_cast<void**>(1);
            ebp41 = 1;
            ebx45 = 1;
            v48 = rsi154;
            v50 = r8_28;
            if (rax34 == 0xffffffffffffffff) 
                goto addr_55df_30; else 
                goto addr_552a_24;
        } else {
            addr_543f_125:
            rax155 = full_write(rdi147, 0, rax141, rcx142, rdi147, 0, rax141, rcx142);
            if (rax155 != rax141) {
                rax156 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax156) == 32) || (zf157 = filter_command == 0, zf157)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_132;
                } else {
                    addr_549d_133:
                    goto v158;
                }
            }
        }
    } else {
        addr_5498_132:
        goto addr_549d_133;
    }
}