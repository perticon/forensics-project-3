int create(char *param_1)

{
  long lVar1;
  int iVar2;
  __pid_t _Var3;
  int iVar4;
  char *__path;
  char *__arg;
  undefined8 uVar5;
  undefined8 uVar6;
  int *piVar7;
  ulong uVar8;
  long in_FS_OFFSET;
  undefined local_b8 [8];
  __ino_t local_b0;
  uint local_a0;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (filter_command == 0) {
    if (verbose != '\0') {
      uVar6 = quotearg_style(4,param_1);
      uVar5 = dcgettext(0,"creating file %s\n",5);
      __fprintf_chk(stdout,1,uVar5,uVar6);
    }
    iVar2 = open_safer(param_1,0x41,0x1b6);
    if (-1 < iVar2) {
      iVar4 = fstat(iVar2,(stat *)local_b8);
      if (iVar4 != 0) {
        uVar6 = quotearg_style(4,param_1);
        uVar5 = dcgettext(0,"failed to stat %s",5);
        piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar7,uVar5,uVar6);
      }
      if ((in_stat_buf._8_8_ == local_b0) &&
         (in_stat_buf._0_8_ == CONCAT44(local_b8._4_4_,local_b8._0_4_))) {
        uVar6 = quotearg_style(4,param_1);
        uVar5 = dcgettext(0,"%s would overwrite input; aborting",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar5,uVar6);
      }
      iVar4 = ftruncate(iVar2,0);
      if ((iVar4 != 0) && ((local_a0 & 0xf000) == 0x8000)) {
        uVar6 = quotearg_n_style_colon(0,3,param_1);
        uVar5 = dcgettext(0,"%s: error truncating",5);
        piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar7,uVar5,uVar6);
      }
    }
  }
  else {
    __path = getenv("SHELL");
    if (__path == (char *)0x0) {
      __path = "/bin/sh";
    }
    iVar2 = setenv("FILE",param_1,1);
    if (iVar2 != 0) {
      uVar6 = dcgettext(0,"failed to set FILE environment variable",5);
      piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar7,uVar6);
    }
    if (verbose != '\0') {
      uVar6 = quotearg_n_style_colon(0,3,param_1);
      uVar5 = dcgettext(0,"executing with FILE=%s\n",5);
      __fprintf_chk(stdout,1,uVar5,uVar6);
    }
    iVar2 = pipe((int *)local_b8);
    if (iVar2 != 0) {
      uVar6 = dcgettext(0,"failed to create pipe",5);
      piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar7,uVar6);
    }
    _Var3 = fork();
    if (_Var3 == 0) {
      uVar8 = 0;
      if (n_open_pipes != 0) {
        do {
          iVar2 = close(*(int *)(open_pipes + uVar8 * 4));
          if (iVar2 != 0) {
            uVar6 = dcgettext(0,"closing prior pipe",5);
            piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
            error(1,*piVar7,uVar6);
          }
          uVar8 = uVar8 + 1;
        } while (uVar8 < n_open_pipes);
      }
      iVar2 = close(local_b8._4_4_);
      piVar7 = __errno_location();
      if (iVar2 != 0) {
        uVar6 = dcgettext(0,"closing output pipe",5);
                    /* WARNING: Subroutine does not return */
        error(1,*piVar7,uVar6);
      }
      if (local_b8._0_4_ != 0) {
        iVar2 = dup2(local_b8._0_4_,0);
        if (iVar2 != 0) {
          uVar6 = dcgettext(0,"moving input pipe",5);
                    /* WARNING: Subroutine does not return */
          error(1,*piVar7,uVar6);
        }
        iVar2 = close(local_b8._0_4_);
        if (iVar2 != 0) {
          uVar6 = dcgettext(0,"closing input pipe",5);
                    /* WARNING: Subroutine does not return */
          error(1,*piVar7,uVar6);
        }
      }
      sigprocmask(2,(sigset_t *)oldblocked,(sigset_t *)0x0);
      lVar1 = filter_command;
      __arg = (char *)last_component(__path);
      execl(__path,__arg,&DAT_0010a0de,lVar1,0);
      uVar6 = dcgettext(0,"failed to run command: \"%s -c %s\"",5);
                    /* WARNING: Subroutine does not return */
      error(1,*piVar7,uVar6,__path,lVar1);
    }
    if (_Var3 == -1) {
      uVar6 = dcgettext(0,"fork system call failed",5);
      piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar7,uVar6);
    }
    iVar2 = close(local_b8._0_4_);
    if (iVar2 != 0) {
      uVar6 = dcgettext(0,"failed to close input pipe",5);
      piVar7 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar7,uVar6);
    }
    filter_pid = _Var3;
    if (n_open_pipes == open_pipes_alloc) {
      open_pipes = x2nrealloc(open_pipes,&open_pipes_alloc,4);
    }
    lVar1 = n_open_pipes * 4;
    n_open_pipes = n_open_pipes + 1;
    *(int *)(open_pipes + lVar1) = local_b8._4_4_;
    iVar2 = local_b8._4_4_;
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}