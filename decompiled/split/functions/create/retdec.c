int32_t create(char * name) {
    int64_t v1 = __readfsqword(40); // 0x49ef
    int64_t v2 = (int64_t)name; // 0x4a0a
    int64_t v3; // 0x49e0
    int64_t v4; // 0x49e0
    if (filter_command == 0) {
        goto lab_0x4bd0;
    } else {
        int64_t v5 = function_2520(); // 0x4a17
        if ((int32_t)function_25e0() != 0) {
            goto lab_0x4e92;
        } else {
            int64_t v6 = v5 == 0 ? (int64_t)"/bin/sh" : v5; // 0x4a38
            v3 = v6;
            v4 = v6;
            if (*(char *)&verbose != 0) {
                goto lab_0x4c98;
            } else {
                goto lab_0x4a56;
            }
        }
    }
  lab_0x4b24:;
    int64_t v7 = function_2710(); // 0x4b28
    function_2560();
    int64_t v8; // 0x49e0
    int32_t v9; // 0x49e0
    if ((int32_t)v7 != 0) {
        // 0x4da8
        function_2640();
        function_28a0();
        goto lab_0x4dcc;
    } else {
        if (v9 == 0) {
            goto lab_0x4b67;
        } else {
            // 0x4b48
            if ((int32_t)function_26a0() != 0) {
                // 0x4e42
                function_2640();
                function_28a0();
                goto lab_0x4e66;
            } else {
                // 0x4b57
                if ((int32_t)function_2710() != 0) {
                    // 0x4ce0
                    function_2640();
                    function_28a0();
                    v8 = v7 & 0xffffffff;
                    goto lab_0x4d08;
                } else {
                    goto lab_0x4b67;
                }
            }
        }
    }
  lab_0x4bd0:;
    int64_t v10 = v2; // 0x4bd7
    v8 = v2;
    if (*(char *)&verbose != 0) {
        goto lab_0x4d08;
    } else {
        goto lab_0x4bdd;
    }
  lab_0x4d08:
    // 0x4d08
    quotearg_style();
    function_2640();
    function_2950();
    v10 = v8;
    goto lab_0x4bdd;
  lab_0x4bdd:;
    int32_t v11 = open_safer((char *)v10, 65); // 0x4bec
    int32_t result = v11; // 0x4bf6
    if (v11 < 0) {
        goto lab_0x4ac2;
    } else {
        // 0x4bfc
        if ((int32_t)function_29b0() != 0) {
            goto lab_0x4e03;
        } else {
            // 0x4c0e
            int64_t v12; // 0x49e0
            if (g45 != v12) {
                goto lab_0x4c2d;
            } else {
                // 0x4c1c
                if (in_stat_buf == (int64_t)v9) {
                    goto lab_0x4dcc;
                } else {
                    goto lab_0x4c2d;
                }
            }
        }
    }
  lab_0x4e92:
    // 0x4e92
    function_2640();
    function_2560();
    function_28a0();
    goto lab_0x4ebe;
  lab_0x4ac2:
    // 0x4ac2
    if (v1 == __readfsqword(40)) {
        // 0x4ad9
        return result;
    }
    // 0x4da3
    function_2670();
    // 0x4da8
    function_2640();
    function_28a0();
    goto lab_0x4dcc;
  lab_0x4ebe:
    // 0x4ebe
    function_2640();
    function_2560();
    function_28a0();
    // 0x4eea
    function_2640();
    function_2560();
    return function_28a0();
  lab_0x4c98:
    // 0x4c98
    quotearg_n_style_colon();
    function_2640();
    function_2950();
    v3 = v4;
    goto lab_0x4a56;
  lab_0x4a56:;
    // 0x4a56
    int64_t v13; // 0x49e0
    if ((int32_t)function_2720() != 0) {
        goto lab_0x4e66;
    } else {
        int32_t v14 = function_29d0(); // 0x4a6d
        switch (v14) {
            case 0: {
                // 0x4af0
                v13 = v3;
                int64_t v15 = 0; // 0x4afa
                if (n_open_pipes == 0) {
                    goto lab_0x4b24;
                } else {
                    while ((int32_t)function_2710() == 0) {
                        int64_t v16 = v15 + 1; // 0x4b17
                        v15 = v16;
                        if (n_open_pipes <= v16) {
                            goto lab_0x4b24;
                        }
                    }
                    // 0x4d77
                    function_2640();
                    function_2560();
                    function_28a0();
                    // 0x4da3
                    function_2670();
                    // 0x4da8
                    function_2640();
                    function_28a0();
                    goto lab_0x4dcc;
                }
            }
            case -1: {
                goto lab_0x4ebe;
            }
            default: {
                // 0x4a7a
                if ((int32_t)function_2710() != 0) {
                    // 0x4eea
                    function_2640();
                    function_2560();
                    return function_28a0();
                }
                // 0x4a8a
                filter_pid = v14;
                int64_t v17 = n_open_pipes; // 0x4aa5
                if (n_open_pipes == open_pipes_alloc) {
                    int64_t v18 = x2nrealloc(); // 0x4d5c
                    v17 = n_open_pipes;
                    *(int64_t *)&open_pipes = v18;
                }
                // 0x4aab
                n_open_pipes = v17 + 1;
                goto lab_0x4ac2;
            }
        }
    }
  lab_0x4e03:
    // 0x4e03
    quotearg_style();
    function_2640();
    function_2560();
    function_28a0();
    // 0x4e42
    function_2640();
    function_28a0();
    goto lab_0x4e66;
  lab_0x4e66:
    // 0x4e66
    function_2640();
    function_2560();
    function_28a0();
    goto lab_0x4e92;
  lab_0x4c2d:
    // 0x4c2d
    result = v11;
    if ((int32_t)function_26d0() == 0) {
        goto lab_0x4ac2;
    } else {
        // 0x4c3f
        result = v11;
        if ((v9 & (int32_t)&g28) != 0x8000) {
            goto lab_0x4ac2;
        } else {
            int64_t v19 = quotearg_n_style_colon(); // 0x4c5d
            function_2640();
            function_2560();
            function_28a0();
            v4 = v19;
            goto lab_0x4c98;
        }
    }
  lab_0x4dcc:
    // 0x4dcc
    quotearg_style();
    function_2640();
    function_28a0();
    goto lab_0x4e03;
  lab_0x4b67:
    // 0x4b67
    function_2530();
    v2 = filter_command;
    last_component((char *)v13);
    function_2990();
    function_2640();
    function_28a0();
    goto lab_0x4bd0;
}