void main(int param_1,undefined8 *param_2)

{
  uint uVar1;
  byte bVar2;
  bool bVar3;
  byte bVar4;
  int iVar5;
  int iVar6;
  int iVar7;
  int iVar8;
  int iVar9;
  int iVar10;
  undefined8 uVar11;
  ushort **ppuVar12;
  byte *pbVar13;
  ulong uVar14;
  size_t sVar15;
  undefined *puVar16;
  undefined *puVar17;
  void *pvVar18;
  ulong uVar19;
  size_t sVar20;
  __off_t _Var21;
  long lVar22;
  int *piVar23;
  undefined8 uVar24;
  long lVar25;
  undefined *puVar26;
  undefined8 *puVar27;
  char *pcVar28;
  undefined *puVar29;
  ulong uVar30;
  ulong uVar31;
  ulong uVar32;
  byte *pbVar33;
  byte *pbVar34;
  long in_FS_OFFSET;
  bool bVar35;
  bool bVar36;
  bool bVar37;
  ulong local_168;
  undefined *local_160;
  ulong local_158;
  undefined8 *local_150;
  ulong local_148;
  ulong local_110;
  ulong local_100;
  sigaction local_f8;
  undefined local_58 [24];
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_168 = 0;
  infile = "-";
  outbase = "x";
  local_158 = 0;
  local_148 = 0;
  iVar10 = 0;
  iVar8 = eolchar;
  pbVar34 = additional_suffix;
  uVar14 = local_168;
  iVar9 = 0;
LAB_00102b0b:
  local_168 = uVar14;
  additional_suffix = pbVar34;
  eolchar = iVar8;
  iVar5 = optind;
  if (optind == 0) {
    iVar5 = 1;
  }
  uVar11 = getopt_long(param_1,param_2,"0123456789C:a:b:del:n:t:ux");
  pbVar33 = optarg;
  iVar6 = (int)uVar11;
  if (iVar6 != -1) {
    if (0x83 < iVar6) {
switchD_00102b56_caseD_a:
      iVar7 = usage();
switchD_00102b56_caseD_0:
      iVar8 = eolchar;
      pbVar34 = additional_suffix;
      if (iVar10 != 0) {
        if (iVar10 != 4) goto LAB_00103bc6;
        if ((iVar9 == 0) || (iVar9 == iVar5)) {
          if ((0x1999999999999999 < local_168) ||
             (uVar14 = (long)iVar7 + local_168 * 10, iVar9 = iVar5, uVar14 < local_168)) {
            uVar11 = umaxtostr(local_168,local_58);
            uVar24 = dcgettext(0,"line count option -%s%c... is too large",5);
                    /* WARNING: Subroutine does not return */
            error(1,0,uVar24,uVar11,iVar6);
          }
          goto LAB_00102b0b;
        }
      }
      iVar10 = 4;
      uVar14 = (long)iVar7;
      iVar9 = iVar5;
      goto LAB_00102b0b;
    }
    if (iVar6 < 0x30) {
      if (iVar6 == -0x83) {
        version_etc(stdout,"split","GNU coreutils",Version,"Torbjorn Granlund","Richard M. Stallman"
                    ,0,uVar11);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar6 == -0x82) {
        usage(0);
        goto LAB_00102b79;
      }
      goto switchD_00102b56_caseD_a;
    }
    iVar7 = iVar6 + -0x30;
    iVar8 = eolchar;
    pbVar34 = additional_suffix;
    uVar14 = local_168;
    switch(iVar7) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      goto switchD_00102b56_caseD_0;
    default:
      goto switchD_00102b56_caseD_a;
    case 0x13:
      if (iVar10 == 0) {
        uVar11 = dcgettext(0,"invalid number of bytes",5);
        uVar14 = xdectoumax(optarg,1,0x7fffffffffffffff,multipliers_7,uVar11,0);
        iVar10 = 2;
        iVar8 = eolchar;
        pbVar34 = additional_suffix;
        goto LAB_00102b0b;
      }
      break;
    case 0x31:
      uVar11 = dcgettext(0,"invalid suffix length",5);
      suffix_length = xdectoumax(optarg,0,0x1fffffffffffffff,"",uVar11,0);
      iVar8 = eolchar;
      pbVar34 = additional_suffix;
      goto LAB_00102b0b;
    case 0x32:
      if (iVar10 == 0) {
        uVar11 = dcgettext(0,"invalid number of bytes",5);
        uVar14 = xdectoumax(optarg,1,0x7fffffffffffffff,multipliers_7,uVar11,0);
        iVar10 = 1;
        iVar8 = eolchar;
        pbVar34 = additional_suffix;
        goto LAB_00102b0b;
      }
      break;
    case 0x34:
    case 0x48:
      pcVar28 = "0123456789abcdef";
      if (iVar6 == 100) {
        pcVar28 = "0123456789";
      }
      suffix_alphabet = pcVar28;
      if (optarg != (byte *)0x0) {
        sVar15 = strlen((char *)optarg);
        sVar20 = strspn((char *)pbVar33,pcVar28);
        if (sVar15 != sVar20) {
          uVar11 = quote(pbVar33);
          pcVar28 = "%s: invalid start value for hexadecimal suffix";
          if (iVar6 != 100) goto LAB_00103143;
          uVar24 = dcgettext(0,"%s: invalid start value for numerical suffix",5);
          goto LAB_0010314d;
        }
        while ((iVar8 = eolchar, pbVar34 = additional_suffix, numeric_suffix_start = pbVar33,
               *pbVar33 == 0x30 && (pbVar33[1] != 0))) {
          pbVar33 = pbVar33 + 1;
          optarg = pbVar33;
        }
      }
      goto LAB_00102b0b;
    case 0x35:
      elide_empty_files = '\x01';
      goto LAB_00102b0b;
    case 0x3c:
      if (iVar10 == 0) {
        uVar11 = dcgettext(0,"invalid number of lines",5);
        uVar14 = xdectoumax(optarg,1,0xffffffffffffffff,"",uVar11,0);
        iVar10 = 3;
        iVar8 = eolchar;
        pbVar34 = additional_suffix;
        goto LAB_00102b0b;
      }
      break;
    case 0x3e:
      if (iVar10 == 0) {
        ppuVar12 = __ctype_b_loc();
        pbVar34 = optarg;
        do {
          optarg = pbVar34;
          pbVar33 = optarg;
          pbVar34 = optarg + 1;
        } while ((*(byte *)((long)*ppuVar12 + (ulong)*optarg * 2 + 1) & 0x20) != 0);
        iVar10 = strncmp((char *)optarg,"r/",2);
        if (iVar10 == 0) {
          optarg = pbVar33 + 2;
          iVar10 = 7;
        }
        else {
          iVar8 = strncmp((char *)pbVar33,"l/",2);
          iVar10 = 5;
          if (iVar8 == 0) {
            optarg = pbVar33 + 2;
            iVar10 = 6;
          }
        }
        pbVar33 = (byte *)strchr((char *)optarg,0x2f);
        if (pbVar33 == (byte *)0x0) {
          uVar11 = dcgettext(0,"invalid number of chunks",5);
          uVar14 = xdectoumax(optarg,1,0xffffffffffffffff,"",uVar11,0);
          iVar8 = eolchar;
          pbVar34 = additional_suffix;
        }
        else {
          uVar11 = dcgettext(0,"invalid number of chunks",5);
          uVar14 = xdectoumax(pbVar33 + 1,1,0xffffffffffffffff,"",uVar11,0);
          iVar8 = eolchar;
          pbVar34 = additional_suffix;
          if (pbVar33 != optarg) {
            *pbVar33 = 0;
            uVar11 = dcgettext(0,"invalid chunk number",5);
            local_158 = xdectoumax(optarg,1,uVar14,"",uVar11,0);
            iVar8 = eolchar;
            pbVar34 = additional_suffix;
          }
        }
        goto LAB_00102b0b;
      }
      break;
    case 0x44:
      iVar8 = (int)(char)*optarg;
      if (*optarg == 0) {
        uVar11 = dcgettext(0,"empty record separator",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar11);
      }
      if (optarg[1] != 0) {
        iVar8 = strcmp((char *)optarg,"\\0");
        if (iVar8 != 0) {
          uVar11 = quote(pbVar33);
          uVar24 = dcgettext(0,"multi-character separator %s",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar24,uVar11);
        }
        iVar8 = 0;
      }
      pbVar34 = additional_suffix;
      if ((-1 < eolchar) && (eolchar != iVar8)) {
        uVar11 = dcgettext(0,"multiple separator characters specified",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar11);
      }
      goto LAB_00102b0b;
    case 0x45:
      unbuffered = '\x01';
      goto LAB_00102b0b;
    case 0x50:
      verbose = 1;
      goto LAB_00102b0b;
    case 0x51:
      filter_command = optarg;
      goto LAB_00102b0b;
    case 0x52:
      uVar11 = dcgettext(0,"invalid IO block size",5);
      local_148 = xdectoumax(optarg,1,0x7ffffffffffffffe,multipliers_7,uVar11,0);
      iVar8 = eolchar;
      pbVar34 = additional_suffix;
      goto LAB_00102b0b;
    case 0x53:
      goto switchD_00102b56_caseD_53;
    }
LAB_00103bc6:
    pcVar28 = "cannot split in more than one way";
    goto LAB_00102b9f;
  }
LAB_00102b79:
  if ((local_158 != 0) && (filter_command != (byte *)0x0)) {
    pcVar28 = "--filter does not process a chunk extracted to stdout";
    goto LAB_00102b9f;
  }
  if (iVar10 == 0) {
    local_168 = 1000;
    iVar10 = 3;
  }
  else if (local_168 == 0) {
    uVar11 = quote(&DAT_0010a2bf);
    uVar24 = dcgettext(0,"invalid number of lines",5);
                    /* WARNING: Subroutine does not return */
    error(0,0,"%s: %s",uVar24,uVar11);
  }
  if (eolchar < 0) {
    eolchar = 10;
  }
  uVar1 = iVar10 - 5;
  if (numeric_suffix_start == (byte *)0x0) {
    uVar14 = local_168 - 1;
    if (2 < uVar1) goto LAB_001030c1;
  }
  else {
    suffix_auto = 0;
    if (2 < uVar1) {
LAB_001030c1:
      uVar31 = suffix_length;
      if (suffix_length == 0) goto LAB_00103bd7;
      goto LAB_001030d1;
    }
    uVar14 = local_168 - 1;
    iVar9 = xstrtoumax(numeric_suffix_start,0,10,&local_100,"");
    if (((iVar9 == 0) && (!CARRY8(local_168,local_100))) && (local_100 < local_168)) {
      uVar14 = uVar14 + local_100;
    }
  }
  uVar31 = 0;
  sVar15 = strlen(suffix_alphabet);
  do {
    uVar31 = uVar31 + 1;
    bVar36 = sVar15 <= uVar14;
    uVar14 = uVar14 / sVar15;
  } while (bVar36);
  suffix_auto = 0;
  if (suffix_length == 0) {
LAB_00103bd7:
    suffix_length = uVar31;
    if (uVar31 < 2) {
      suffix_length = 2;
    }
  }
  else {
    if (suffix_length < uVar31) {
      uVar11 = dcgettext(0,"the suffix length needs to be at least %lu",5);
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar11,uVar31);
    }
LAB_001030d1:
    suffix_auto = 0;
  }
  iVar9 = optind;
  if (optind < param_1) {
    lVar25 = (long)optind;
    infile = (char *)param_2[lVar25];
    iVar9 = optind + 1;
    if (param_1 <= optind + 1) goto LAB_001031e4;
    outbase = (char *)param_2[lVar25 + 1];
    optind = optind + 2;
    iVar9 = optind;
    if (param_1 <= optind) goto LAB_001031e4;
    uVar11 = quote(param_2[lVar25 + 2]);
    pcVar28 = "extra operand %s";
    goto LAB_00103143;
  }
LAB_001031e4:
  optind = iVar9;
  if (numeric_suffix_start != (byte *)0x0) {
    sVar15 = strlen((char *)numeric_suffix_start);
    pcVar28 = "numerical suffix start value is too large for the suffix length";
    if (suffix_length < sVar15) {
LAB_00102b9f:
      uVar11 = dcgettext(0,pcVar28,5);
                    /* WARNING: Subroutine does not return */
      error(0,0,uVar11);
    }
  }
  pcVar28 = infile;
  iVar9 = strcmp(infile,"-");
  if ((iVar9 != 0) && (iVar9 = fd_reopen(0,pcVar28,0,0), iVar9 < 0)) {
    uVar11 = quotearg_style(4,infile);
    uVar24 = dcgettext(0,"cannot open %s for reading",5);
    piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar23,uVar24,uVar11);
  }
  iVar9 = fstat(0,(stat *)in_stat_buf);
  if (iVar9 != 0) {
    uVar11 = quotearg_n_style_colon(0,3,infile);
    piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar23,"%s",uVar11);
  }
  if ((local_148 == 0) && (local_148 = 0x20000, in_stat_buf._56_8_ - 0x20000 < 0x1ffffffffffe0001))
  {
    local_148 = in_stat_buf._56_8_;
  }
  getpagesize();
  uVar31 = 0xffffffffffffffff;
  uVar14 = 0x7fffffffffffffff;
  puVar16 = (undefined *)xalignalloc();
  if (uVar1 < 2) {
    uVar14 = 0;
    _Var21 = lseek(0,0,1);
    if (_Var21 < 0) {
      piVar23 = __errno_location();
      if (*piVar23 == 0x1d) {
        *piVar23 = 0;
      }
      goto LAB_001040de;
    }
    while (lVar25 = safe_read(0,puVar16 + uVar14,local_148 - uVar14), lVar25 != 0) {
      if (lVar25 == -1) goto LAB_001040de;
      uVar14 = uVar14 + lVar25;
      if (local_148 <= uVar14) {
        if (in_stat_buf._48_8_ != 0) {
          lVar25 = uVar14 + _Var21;
          if (((in_stat_buf._24_4_ & 0xd000) != 0x8000) ||
             (lVar22 = in_stat_buf._48_8_, in_stat_buf._48_8_ < lVar25)) {
            lVar22 = lseek(0,0,2);
            if (lVar22 < 0) goto LAB_001040de;
            if (lVar25 != lVar22) {
              _Var21 = lseek(0,lVar25,0);
              if (_Var21 < 0) goto LAB_001040de;
              if (lVar22 < lVar25) {
                lVar22 = lVar25;
              }
            }
          }
          uVar14 = uVar14 + (lVar22 - lVar25);
          if (uVar14 != 0x7fffffffffffffff) break;
        }
        piVar23 = __errno_location();
        *piVar23 = 0x4b;
LAB_001040de:
        uVar11 = quotearg_n_style_colon(0,3,infile);
        uVar24 = dcgettext(0,"%s: cannot determine file size",5);
        piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar23,uVar24,uVar11);
      }
    }
    if ((long)uVar14 < 0) goto LAB_001040de;
    uVar31 = local_148;
    if ((long)uVar14 < (long)local_148) {
      uVar31 = uVar14;
    }
    if (uVar14 < local_168) {
      uVar14 = local_168;
    }
    if ((long)local_168 < 0) {
      uVar11 = umaxtostr(local_168,local_58);
      uVar11 = quote(uVar11);
      uVar24 = dcgettext(0,"invalid number of chunks",5);
                    /* WARNING: Subroutine does not return */
      error(1,0x4b,"%s: %s",uVar24,uVar11);
    }
  }
  if (filter_command != (byte *)0x0) {
    sigemptyset((sigset_t *)newblocked);
    sigaction(0xd,(sigaction *)0x0,&local_f8);
    if (local_f8.__sigaction_handler != 1) {
      sigaddset((sigset_t *)newblocked,0xd);
    }
    sigprocmask(0,(sigset_t *)newblocked,(sigset_t *)oldblocked);
  }
  switch(iVar10) {
  case 2:
    bVar36 = false;
    uVar14 = 0;
    uVar31 = 0;
    lVar25 = 0;
    local_150 = (undefined8 *)0x0;
    do {
      uVar19 = safe_read(0,puVar16,local_148);
      if (uVar19 == 0xffffffffffffffff) {
        uVar11 = quotearg_n_style_colon(0,3,infile);
        piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar23,"%s",uVar11);
      }
      puVar17 = puVar16;
      if (uVar19 == 0) {
        if (uVar31 != 0) {
          cwrite(lVar25 == 0,local_150,uVar31);
        }
        free(local_150);
        break;
      }
      do {
        lVar22 = lVar25 + uVar31;
        uVar30 = local_168 - lVar22;
        if (uVar19 < uVar30) {
          uVar30 = 0;
          pvVar18 = memrchr(puVar17,eolchar,uVar19);
          local_160 = (undefined *)0x0;
          if (uVar31 == 0) goto LAB_001034c1;
LAB_00103429:
          if ((pvVar18 != (void *)0x0) || (lVar25 == 0)) {
            cwrite(lVar25 == 0,local_150,uVar31);
            lVar25 = lVar22;
            uVar14 = local_148;
            if (local_148 < uVar31) {
              local_150 = (undefined8 *)xrealloc();
            }
            goto LAB_001034c1;
          }
LAB_00103442:
          if (bVar36) {
            puVar29 = puVar17;
            if (local_160 == (undefined *)0x0) {
LAB_001045d3:
              uVar30 = uVar19;
              if (uVar19 == 0) goto LAB_00103550;
            }
            else if (uVar30 == 0) {
              if (local_160 != (undefined *)0x0) goto LAB_00103550;
              goto LAB_001045d3;
            }
LAB_0010350b:
            if (uVar14 - uVar31 < uVar30) {
              bVar35 = CARRY8(uVar14,local_148);
              uVar14 = uVar14 + local_148;
              if (bVar35) {
                    /* WARNING: Subroutine does not return */
                xalloc_die();
              }
              local_150 = (undefined8 *)xrealloc(local_150,uVar14);
            }
            uVar19 = uVar19 - uVar30;
            pvVar18 = (void *)((long)local_150 + uVar31);
            uVar31 = uVar31 + uVar30;
            memcpy(pvVar18,puVar17,uVar30);
            puVar29 = puVar17 + uVar30;
            goto LAB_00103550;
          }
          bVar35 = lVar25 == 0;
          if (local_160 == (undefined *)0x0) {
            lVar25 = lVar25 + uVar19;
            cwrite(bVar35,puVar17,uVar19);
            break;
          }
          uVar19 = uVar19 - uVar30;
          puVar29 = puVar17 + uVar30;
          cwrite(bVar35,puVar17,uVar30);
          lVar25 = 0;
        }
        else {
          local_160 = puVar17 + (uVar30 - 1);
          pvVar18 = memrchr(puVar17,eolchar,uVar30);
          if (uVar31 != 0) goto LAB_00103429;
LAB_001034c1:
          if (pvVar18 == (void *)0x0) {
            uVar31 = 0;
            goto LAB_00103442;
          }
          bVar36 = lVar25 == 0;
          lVar22 = (long)pvVar18 + (1 - (long)puVar17);
          lVar25 = lVar25 + lVar22;
          uVar19 = uVar19 - lVar22;
          puVar29 = puVar17 + lVar22;
          cwrite(bVar36,puVar17,lVar22);
          puVar17 = puVar29;
          if (local_160 == (undefined *)0x0) {
            bVar36 = uVar19 != 0;
            uVar31 = 0;
            uVar30 = uVar19;
            if (bVar36) goto LAB_0010350b;
            bVar36 = true;
            goto LAB_00103475;
          }
          bVar36 = uVar30 - lVar22 != 0;
          if (bVar36) {
            uVar31 = 0;
            uVar30 = uVar30 - lVar22;
            goto LAB_0010350b;
          }
          bVar36 = true;
          uVar31 = 0;
LAB_00103550:
          if (local_160 != (undefined *)0x0) {
            bVar36 = false;
            lVar25 = 0;
          }
        }
LAB_00103475:
        puVar17 = puVar29;
      } while (uVar19 != 0);
    } while( true );
  case 3:
  case 4:
    uVar14 = 0;
    local_160 = (undefined *)0x1;
    do {
      lVar25 = safe_read(0,puVar16,local_148);
      iVar10 = eolchar;
      if (lVar25 == -1) {
        uVar11 = quotearg_n_style_colon(0,3,infile);
        piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar23,"%s",uVar11);
      }
      puVar26 = puVar16 + lVar25;
      *puVar26 = (char)eolchar;
      puVar29 = puVar16;
      puVar17 = puVar16;
      while (puVar17 = (undefined *)rawmemchr(puVar17,iVar10), puVar26 != puVar17) {
        uVar14 = uVar14 + 1;
        puVar17 = puVar17 + 1;
        if (local_168 <= uVar14) {
          uVar14 = 0;
          cwrite(local_160,puVar29,(long)puVar17 - (long)puVar29);
          local_160 = (undefined *)0x1;
          puVar29 = puVar17;
          iVar10 = eolchar;
        }
      }
      if (puVar26 != puVar29) {
        cwrite(local_160,puVar29,(long)puVar26 - (long)puVar29);
        local_160 = (undefined *)0x0;
      }
    } while (lVar25 != 0);
    break;
  case 5:
    if (local_158 == 0) {
      bytes_split(uVar14 / local_168,puVar16,local_148,uVar31);
    }
    else {
      if ((local_168 < local_158) || (uVar14 < local_168)) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("k && n && k <= n && n <= file_size","src/split.c",0x3e4,"bytes_chunk_extract"
                     );
      }
      uVar19 = (local_158 - 1) * (uVar14 / local_168);
      if (local_168 != local_158) {
        uVar14 = local_158 * (uVar14 / local_168);
      }
      if (uVar19 < uVar31) {
        uVar30 = uVar31 - uVar19;
        FUN_00102890(puVar16,puVar16 + uVar19,uVar30);
      }
      else {
        uVar30 = 0xffffffffffffffff;
        _Var21 = lseek(0,uVar19 - uVar31,1);
        if (_Var21 < 0) {
          uVar11 = quotearg_n_style_colon(0,3,infile);
          piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar23,"%s",uVar11);
        }
      }
      for (; (long)uVar19 < (long)uVar14; uVar19 = uVar19 + uVar31) {
        if (uVar30 == 0xffffffffffffffff) {
          uVar30 = safe_read(0,puVar16,local_148);
          if (uVar30 == 0xffffffffffffffff) {
            uVar11 = quotearg_n_style_colon(0,3,infile);
            piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
            error(1,*piVar23,"%s",uVar11);
          }
          if (uVar30 == 0) break;
        }
        uVar31 = uVar30;
        if (uVar14 - uVar19 <= uVar30) {
          uVar31 = uVar14 - uVar19;
        }
        uVar30 = full_write(1,puVar16,uVar31);
        if ((uVar31 != uVar30) &&
           ((piVar23 = __errno_location(), filter_command == (byte *)0x0 || (*piVar23 != 0x20)))) {
          uVar11 = quotearg_n_style_colon(0,3,&DAT_0010a210);
                    /* WARNING: Subroutine does not return */
          error(1,*piVar23,"%s",uVar11);
        }
        uVar30 = 0xffffffffffffffff;
      }
    }
    break;
  case 6:
    if ((local_168 < local_158) || (uVar14 < local_168)) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("n && k <= n && n <= file_size","src/split.c",0x363,"lines_chunk_split");
    }
    uVar19 = uVar14 / local_168;
    if (local_158 < 2) {
      local_160 = (undefined *)0x0;
      uVar32 = uVar19 - 1;
      uVar30 = 1;
    }
    else {
      uVar30 = local_158 - 1;
      uVar32 = uVar19 * uVar30 - 1;
      local_160 = (undefined *)uVar32;
      if (uVar32 < uVar31) {
        uVar31 = uVar31 - uVar32;
        FUN_00102890(puVar16,puVar16 + uVar32,uVar31);
      }
      else {
        lVar25 = uVar32 - uVar31;
        uVar31 = 0xffffffffffffffff;
        _Var21 = lseek(0,lVar25,1);
        if (_Var21 < 0) {
          uVar11 = quotearg_n_style_colon(0,3,infile);
          piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar23,"%s",uVar11);
        }
      }
    }
    bVar36 = false;
    bVar35 = true;
    while ((long)local_160 < (long)uVar14) {
      if ((uVar31 == 0xffffffffffffffff) &&
         (uVar31 = safe_read(0,puVar16,local_148), uVar31 == 0xffffffffffffffff)) {
        uVar11 = quotearg_n_style_colon(0,3,infile);
        piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar23,"%s",uVar11);
      }
      if (uVar31 == 0) break;
      local_110 = uVar31;
      if (uVar14 - (long)local_160 <= uVar31) {
        local_110 = uVar14 - (long)local_160;
      }
      puVar29 = puVar16 + local_110;
      puVar17 = puVar16;
      bVar36 = false;
      while (uVar31 = 0xffffffffffffffff, puVar29 != puVar17) {
        uVar31 = uVar32 - (long)local_160;
        if ((long)uVar31 < 0) {
          uVar31 = 0;
        }
        if (local_110 < uVar31) {
          uVar31 = local_110;
        }
        pvVar18 = memchr(puVar17 + uVar31,eolchar,local_110 - uVar31);
        bVar37 = pvVar18 != (void *)0x0;
        puVar26 = puVar29;
        if (bVar37) {
          puVar26 = (undefined *)((long)pvVar18 + 1);
        }
        lVar25 = (long)puVar26 - (long)puVar17;
        if (uVar30 == local_158) {
          lVar22 = full_write(1,puVar17,lVar25);
          if (lVar25 != lVar22) {
            uVar11 = dcgettext(0,"write error",5);
            piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
            error(1,*piVar23,"%s",uVar11);
          }
        }
        else if (local_158 == 0) {
          cwrite(bVar35,puVar17,lVar25);
        }
        local_160 = (undefined *)((long)local_160 + lVar25);
        bVar35 = bVar37;
        while ((((long)uVar32 < (long)local_160 || (bVar3 = bVar36, bVar35)) &&
               (bVar3 = (bool)((bVar35 ^ 1U) & puVar29 == puVar26), !bVar3))) {
          uVar30 = uVar30 + 1;
          if (local_158 != 0 && local_158 < uVar30) goto LAB_00103900;
          uVar32 = uVar32 + uVar19;
          if (local_168 == uVar30) {
            uVar32 = uVar14 - 1;
          }
          if ((long)uVar32 < (long)local_160) {
            if (local_158 == 0) {
              cwrite(1,0,0);
            }
          }
          else {
            bVar35 = false;
          }
        }
        local_110 = local_110 - lVar25;
        puVar17 = puVar26;
        bVar36 = bVar3;
        bVar35 = bVar37;
      }
    }
    uVar14 = (uVar30 + 1) - (ulong)!bVar36;
    if (local_158 == 0) {
      for (; uVar14 <= local_168; uVar14 = uVar14 + 1) {
        cwrite(1,0,0);
      }
    }
    break;
  case 7:
    local_150 = (undefined8 *)0x0;
    if (local_158 == 0) {
      uVar14 = 0;
      local_150 = (undefined8 *)xnmalloc(local_168,0x20);
      puVar27 = local_150;
      do {
        next_file_name();
        uVar14 = uVar14 + 1;
        uVar11 = xstrdup();
        *(undefined4 *)(puVar27 + 1) = 0xffffffff;
        *puVar27 = uVar11;
        puVar27[2] = 0;
        *(undefined4 *)(puVar27 + 3) = 0;
        puVar27 = puVar27 + 4;
      } while (local_168 != uVar14);
    }
    local_160 = (undefined *)0x0;
    uVar14 = 1;
    bVar2 = 0;
    bVar35 = false;
    bVar36 = false;
    while( true ) {
      lVar25 = safe_read(0,puVar16,local_148);
      if (lVar25 == -1) {
        uVar11 = quotearg_n_style_colon(0,3,infile);
        piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
        error(1,*piVar23,"%s",uVar11);
      }
      if (lVar25 == 0) break;
      puVar17 = puVar16 + lVar25;
      puVar29 = puVar16;
      while (puVar29 != puVar17) {
        sVar15 = (long)puVar17 - (long)puVar29;
        pvVar18 = memchr(puVar29,eolchar,sVar15);
        bVar37 = pvVar18 != (void *)0x0;
        puVar26 = puVar17;
        if (bVar37) {
          sVar15 = (long)(undefined *)((long)pvVar18 + 1) - (long)puVar29;
          puVar26 = (undefined *)((long)pvVar18 + 1);
        }
        if (local_158 == 0) {
          bVar4 = ofile_open(local_150,local_160,local_168);
          bVar2 = bVar2 | bVar4;
          piVar23 = __errno_location();
          puVar27 = local_150 + (long)local_160 * 4;
          if (unbuffered == '\0') {
            sVar15 = fwrite_unlocked(puVar29,sVar15,1,(FILE *)puVar27[2]);
            if (sVar15 == 1) {
LAB_00103930:
              if ((*piVar23 != 0x20) || (filter_command == (byte *)0x0)) {
                bVar35 = true;
              }
            }
            else if ((*piVar23 != 0x20) || (filter_command == (byte *)0x0)) {
              uVar11 = quotearg_n_style_colon(0,3,*puVar27);
                    /* WARNING: Subroutine does not return */
              error(1,*piVar23,"%s",uVar11);
            }
          }
          else {
            sVar20 = full_write(*(undefined4 *)(puVar27 + 1),puVar29,sVar15);
            if (sVar20 == sVar15) goto LAB_00103930;
            if ((filter_command == (byte *)0x0) || (*piVar23 != 0x20)) {
              uVar11 = quotearg_n_style_colon(0,3,*puVar27);
                    /* WARNING: Subroutine does not return */
              error(1,*piVar23,"%s",uVar11);
            }
          }
          if (bVar2 != 0) {
            iVar10 = rpl_fclose();
            if (iVar10 != 0) {
              uVar11 = quotearg_n_style_colon(0,3,*puVar27);
                    /* WARNING: Subroutine does not return */
              error(1,*piVar23,"%s",uVar11);
            }
            puVar27[2] = 0;
            *(undefined4 *)(puVar27 + 1) = 0xfffffffe;
          }
          puVar29 = puVar26;
          if ((bVar37) &&
             (local_160 = (undefined *)((long)local_160 + 1), (undefined *)local_168 == local_160))
          {
            if (!bVar35) goto LAB_00103f4e;
            local_160 = (undefined *)0x0;
            bVar36 = bVar35;
            bVar35 = false;
          }
        }
        else {
          if (uVar14 == local_158) {
            if (unbuffered == '\0') {
              sVar15 = fwrite_unlocked(puVar29,sVar15,1,stdout);
              if (sVar15 != 1) {
                clearerr_unlocked(stdout);
                uVar11 = dcgettext(0,"write error",5);
                piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
                error(1,*piVar23,"%s",uVar11);
              }
            }
            else {
              sVar20 = full_write(1,puVar29,sVar15);
              if (sVar20 != sVar15) {
                uVar11 = dcgettext(0,"write error",5);
                piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
                error(1,*piVar23,"%s",uVar11);
              }
            }
          }
          puVar29 = puVar26;
          if (bVar37) {
            uVar31 = uVar14 + 1;
            bVar37 = local_168 != uVar14;
            uVar14 = 1;
            if (bVar37) {
              uVar14 = uVar31;
            }
          }
        }
      }
    }
    if (local_158 == 0) {
      if (bVar36) {
LAB_00103f4e:
      }
      else {
        local_168._0_4_ = (int)local_160;
      }
      uVar14 = 0;
      puVar27 = local_150;
      do {
        if (((ulong)(long)(int)local_168 <= uVar14) && (elide_empty_files == '\0')) {
          ofile_open(local_150,uVar14,local_168);
        }
        if (-1 < *(int *)(puVar27 + 1)) {
          closeout(puVar27[2],*(int *)(puVar27 + 1),*(undefined4 *)(puVar27 + 3),*puVar27);
        }
        uVar14 = uVar14 + 1;
        *(undefined4 *)(puVar27 + 1) = 0xfffffffe;
        puVar27 = puVar27 + 4;
      } while (local_168 != uVar14);
    }
    break;
  default:
    bytes_split(local_168,puVar16,local_148,0xffffffffffffffff,0);
  }
LAB_00103900:
  iVar10 = close(0);
  if (iVar10 != 0) {
    uVar11 = quotearg_n_style_colon(0,3,infile);
    piVar23 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar23,"%s",uVar11);
  }
  closeout(0,output_desc,filter_pid,outfile);
                    /* WARNING: Subroutine does not return */
  exit(0);
switchD_00102b56_caseD_53:
  pbVar34 = optarg;
  pbVar13 = (byte *)last_component();
  iVar8 = eolchar;
  if (pbVar33 != pbVar13) {
    uVar11 = quote(pbVar33);
    pcVar28 = "invalid suffix %s, contains directory separator";
LAB_00103143:
    uVar24 = dcgettext(0,pcVar28,5);
LAB_0010314d:
                    /* WARNING: Subroutine does not return */
    error(0,0,uVar24,uVar11);
  }
  goto LAB_00102b0b;
}