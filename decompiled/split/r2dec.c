#include <stdint.h>

/* /tmp/tmpb028xga7 @ 0x45f0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpb028xga7 @ 0x6040 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000b2a7;
        rdx = 0x0000b298;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000b29f;
        rdx = 0x0000b2a1;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000b2a3;
    rdx = 0x0000b29c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x9990 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x2820 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpb028xga7 @ 0x6120 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2a00)() ();
    }
    rdx = 0x0000b300;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xb300 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000b2ab;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000b2a1;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000b32c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xb32c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000b29f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000b2a1;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000b29f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000b2a1;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000b42c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xb42c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000b52c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xb52c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000b2a1;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000b29f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000b29f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000b2a1;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpb028xga7 @ 0x2a00 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 29762 named .text */
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x7540 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2a05)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x2a05 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a0a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2550 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpb028xga7 @ 0x2a10 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a15 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a1a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a1f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a24 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a29 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a2e */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a33 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2a38 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x46e0 */
 
uint64_t dbg_next_file_name (void) {
    /* void next_file_name(); */
    rbp = outfile;
    rbx = suffix_length;
    if (rbp == 0) {
        goto label_2;
    }
    rax = rbx - 1;
    if (rbx == 0) {
        goto label_3;
    }
    r10 = *(0x0000f2c0);
    r9d = *(obj.suffix_auto);
    rdi = suffix_alphabet;
    r8 = outfile_mid;
    rdx = r10 + rax*8;
    while (rax != 0) {
label_0:
        ecx = *((rdi + rcx + 1));
        *((r8 + rax)) = cl;
        if (cl != 0) {
            goto label_1;
        }
        *(rdx) = 0;
        ecx = *(rdi);
        rdx -= 8;
        *((r8 + rax)) = cl;
        rax--;
        if (rax == -1) {
            goto label_3;
        }
        rcx = *(rdx);
        rsi = rcx + 1;
        *(rdx) = rsi;
    }
    if (r9b == 0) {
        goto label_0;
    }
    rax = *(r10);
    if (*((rdi + rax + 1)) == 0) {
        goto label_2;
    }
    eax = *((rdi + rsi));
    *(r8) = al;
    if (al == 0) {
        goto label_4;
    }
    do {
label_1:
        r12 = rbx;
        r13 = rbx;
        r14 = rbx;
        return rax;
label_2:
        r12 = *(0x0000f2d8);
        if (r12 == 0) {
            rax = strlen (*(obj.outbase));
            rdi = additional_suffix;
            *(0x0000f2d0) = rax;
            r13 = rax;
            eax = 0;
            if (rdi != 0) {
                rax = strlen (rdi);
            }
            rbx += r13;
            *(0x0000f2c8) = rax;
            rax += rbx;
            rsi = rax + 1;
            *(0x0000f2d8) = rax;
            if (r13 > rsi) {
                goto label_5;
            }
            rdi = rbp;
            rax = xrealloc ();
            rbx = *(0x0000f2d0);
            rdi = rax;
            *(obj.outfile) = rax;
            r13 = rax;
            memcpy (rdi, *(obj.outbase), rbx);
            rax = suffix_alphabet;
            r14 = *(0x0000f2c0);
        } else {
            rbx++;
            rax = r12 + 2;
            rsi = r12 + 3;
            *(0x0000f2d8) = rax;
            *(obj.suffix_length) = rbx;
            if (rsi < *(0x0000f2d0)) {
                goto label_5;
            }
            rdi = rbp;
            rax = xrealloc ();
            r14 = *(0x0000f2c0);
            rcx = *(0x0000f2d0);
            r13 = rax;
            *(obj.outfile) = rax;
            rax = suffix_alphabet;
            rdx = *(r14);
            rbx = rcx + 1;
            *(0x0000f2d0) = rbx;
            edx = *((rax + rdx));
            *((r13 + rcx)) = dl;
        }
        rbp = suffix_length;
        rcx = r13 + rbx;
        esi = *(rax);
        rdi = rcx;
        *(obj.outfile_mid) = rcx;
        memset (rdi, rsi, rbp);
        rsi = additional_suffix;
        if (rsi != 0) {
            memcpy (rax + rbp, rsi, *(0x0000f2c8));
        }
        rax = *(0x0000f2d8);
        *((r13 + rax)) = 0;
        free (r14);
        rdi = rbp;
        esi = 8;
        rax = xcalloc ();
        rbp = numeric_suffix_start;
        *(0x0000f2c0) = rax;
        rbx = rax;
    } while (rbp == 0);
    if (r12 != 0) {
        goto label_6;
    }
    rax = strlen (rbp);
    r13 = suffix_length;
    r12 = rax;
    r13 <<= 3;
    rdi -= rax;
    rdi += *(obj.outfile_mid);
    memcpy (r13, rbp, rax);
    rdx = r12 - 1;
    if (r12 == 0) {
        goto label_1;
    }
    r12 = -r12;
    rax = r13 + r12*8;
    rcx = rbx + rax;
    do {
        eax = *((rbp + rdx));
        eax -= 0x30;
        rax = (int64_t) eax;
        *((rcx + rdx*8)) = rax;
        rdx--;
    } while (rdx >= 0);
    return rax;
label_5:
    xalloc_die ();
label_4:
    *(rdx) = 0;
    eax = *(rdi);
    *(r8) = al;
label_3:
    edx = 5;
    rax = dcgettext (0, "output file suffixes exhausted");
    eax = 0;
    error (1, 0, rax);
label_6:
    return assert_fail ("! widen", "src/split.c", 0x199, "next_file_name");
}

/* /tmp/tmpb028xga7 @ 0x49e0 */
 
int64_t dbg_create (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1fh, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1) {
    int64_t var_8h;
    int64_t var_bp_10h;
    int[2] fd_pair;
    int64_t var_ch;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_38h_2;
    int64_t var_sp_8h;
    int64_t fildes;
    int64_t var_18h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_38h;
    int64_t var_98h;
    rdi = arg1;
    /* int create(char const * name); */
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    if (*(obj.filter_command) == 0) {
        goto label_4;
    }
    rax = getenv ("SHELL");
    edx = 1;
    rsi = r12;
    rdi = "FILE";
    r13 = rax;
    rax = "/bin/sh";
    if (rax == 0) {
        r13 = rax;
    }
    eax = setenv (rdi, rsi, rdx);
    if (eax != 0) {
        goto label_5;
    }
    if (*(obj.verbose) != 0) {
        goto label_6;
    }
label_1:
    rdi = rsp;
    eax = pipe ();
    if (eax != 0) {
        goto label_7;
    }
    eax = fork ();
    ebx = eax;
    if (eax != 0) {
        if (eax == 0xffffffff) {
            goto label_8;
        }
        eax = close (*(rsp));
        if (eax != 0) {
            goto label_9;
        }
        rdx = n_open_pipes;
        *(obj.filter_pid) = ebx;
        rdi = open_pipes;
        if (rdx == *(obj.open_pipes_alloc)) {
            goto label_10;
        }
label_3:
        rax = rdx + 1;
        *(obj.n_open_pipes) = rax;
        eax = *((rsp + 4));
        *((rdi + rdx*4)) = eax;
        r13d = *((rsp + 4));
label_0:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_11;
        }
        eax = r13d;
        return rax;
    }
    ebx = 0;
    if (*(obj.n_open_pipes) == 0) {
        goto label_12;
    }
    do {
        rax = open_pipes;
        eax = close (*((rax + rbx*4)));
        if (eax != 0) {
            goto label_13;
        }
        rbx++;
    } while (*(obj.n_open_pipes) > rbx);
label_12:
    eax = close (*((rsp + 4)));
    r12d = eax;
    rax = errno_location ();
    rbx = rax;
    if (r12d != 0) {
        goto label_14;
    }
    edi = *(rsp);
    if (edi != 0) {
        esi = 0;
        eax = dup2 ();
        if (eax != 0) {
            goto label_15;
        }
        eax = close (*(rsp));
        if (eax != 0) {
            goto label_16;
        }
    }
    edx = 0;
    rsi = obj_oldblocked;
    edi = 2;
    sigprocmask ();
    r12 = filter_command;
    rdi = r13;
    rax = last_component ();
    r8d = 0;
    rdx = 0x0000a0de;
    rdi = r13;
    rsi = rax;
    rcx = r12;
    eax = 0;
    execl ();
    edx = 5;
    rax = dcgettext (0, "failed to run command: \"%s -c %s\");
    r8 = r12;
    rcx = r13;
    eax = 0;
    rax = error (1, *(rbx), rax);
label_4:
    if (*(obj.verbose) != 0) {
        goto label_17;
    }
label_2:
    eax = 0;
    eax = open_safer (r12, 0x41, 0x1b6, rcx);
    r13d = eax;
    if (eax < 0) {
        goto label_0;
    }
    eax = fstat (eax, rsp);
    if (eax != 0) {
        goto label_18;
    }
    rax = *((rsp + 8));
    if (*(0x0000f308) == rax) {
        rax = *(rsp);
        if (*(obj.in_stat_buf) == rax) {
            goto label_19;
        }
    }
    esi = 0;
    edi = r13d;
    eax = ftruncate ();
    if (eax == 0) {
        goto label_0;
    }
    eax = *((rsp + 0x18));
    eax &= loc.data_start;
    if (eax != 0x8000) {
        goto label_0;
    }
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s: error truncating");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    rax = error (1, *(rax), r12);
label_6:
    rdx = r12;
    edi = 0;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "executing with FILE=%s\n");
    rdi = stdout;
    rcx = r12;
    esi = 1;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_16:
    edx = 5;
    rax = dcgettext (0, "closing input pipe");
    eax = 0;
    rax = error (1, *(rbx), rax);
label_17:
    rsi = rdi;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "creating file %s\n");
    rdi = stdout;
    rcx = r13;
    esi = 1;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_2;
label_10:
    edx = 4;
    rsi = obj_open_pipes_alloc;
    rax = x2nrealloc ();
    rdx = n_open_pipes;
    *(obj.open_pipes) = rax;
    rdi = rax;
    goto label_3;
label_13:
    edx = 5;
    rax = dcgettext (0, "closing prior pipe");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_11:
    stack_chk_fail ();
label_14:
    edx = 5;
    rax = dcgettext (0, "closing output pipe");
    eax = 0;
    error (1, *(rbx), rax);
label_19:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s would overwrite input; aborting");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_18:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to stat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_15:
    edx = 5;
    rax = dcgettext (0, "moving input pipe");
    eax = 0;
    error (1, *(rbx), rax);
label_7:
    edx = 5;
    rax = dcgettext (0, "failed to create pipe");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_5:
    edx = 5;
    rax = dcgettext (0, "failed to set FILE environment variable");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_8:
    edx = 5;
    rax = dcgettext (0, "fork system call failed");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_9:
    edx = 5;
    rax = dcgettext (0, "failed to close input pipe");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmpb028xga7 @ 0x4f20 */
 
uint64_t dbg_ofile_open (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1fh, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, void ** arg1, uint32_t arg2, int64_t arg3) {
    int64_t var_8h_2;
    int64_t var_ch;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_38h_2;
    int64_t errname;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool ofile_open(of_t * files,size_t i_check,size_t nfiles); */
    r13 = rsi;
    r13 <<= 5;
    r13 += rdi;
    ebp = 0;
    eax = *((r13 + 8));
    if (eax >= 0) {
label_2:
        eax = ebp;
        return eax;
    }
    rbx = rsi - 1;
    r15 = rdx - 1;
    r12 = rdi;
    if (rsi == 0) {
        rbx = r15;
    }
    rdi = *(r13);
    r14 = rsi;
    if (eax == 0xffffffff) {
        goto label_3;
    }
label_0:
    eax = 0;
    eax = open_safer (rdi, 0xc01, rdx, rcx);
    if (eax >= 0) {
        goto label_4;
    }
label_1:
    rax = errno_location ();
    r8 = rax;
    eax = *(rax);
    eax -= 0x17;
    if (eax <= 1) {
        goto label_5;
    }
    goto label_6;
    do {
        rax = rbx - 1;
        if (rbx == 0) {
            rax = r15;
        }
        rbx = rax;
        if (rax == r14) {
            goto label_7;
        }
label_5:
        rbp <<= 5;
        rbp += r12;
        eax = *((rbp + 8));
    } while (eax < 0);
    *((rsp + 8)) = r8;
    eax = rpl_fclose (*((rbp + 0x10)));
    if (eax != 0) {
        goto label_8;
    }
    *((rbp + 8)) = 0xfffffffe;
    eax = *((r13 + 8));
    *((rbp + 0x10)) = 0;
    rdi = *(r13);
    if (eax != 0xffffffff) {
        goto label_0;
    }
label_3:
    eax = create (rdi, rsi, rdx, rcx, r8, r9);
    if (eax < 0) {
        goto label_1;
    }
label_4:
    *((r13 + 8)) = eax;
    rsi = 0x0000a114;
    edi = eax;
    rax = fdopen ();
    *((r13 + 0x10)) = rax;
    if (rax != 0) {
        eax = filter_pid;
        *(obj.filter_pid) = 0;
        *((r13 + 0x18)) = eax;
        goto label_2;
label_7:
        rdx = *(r13);
        esi = 3;
        edi = 0;
        *((rsp + 8)) = r8;
        rax = quotearg_n_style_colon ();
        r8 = *((rsp + 8));
        rcx = rax;
        eax = 0;
        error (1, *(r8), 0x0000a31e);
label_6:
        rdx = *(r13);
        esi = 3;
        edi = 0;
        *((rsp + 8)) = r8;
        rax = quotearg_n_style_colon ();
        r8 = *((rsp + 8));
        rcx = rax;
        eax = 0;
        error (1, *(r8), 0x0000a31e);
label_8:
        rdx = *(rbp);
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r8 = *((rsp + 8));
        rcx = rax;
        eax = 0;
        error (1, *(r8), 0x0000a31e);
    }
    rdx = *(r13);
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
}

/* /tmp/tmpb028xga7 @ 0x5120 */
 
int64_t dbg_closeout (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1fh, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, void ** arg1, int64_t arg2, void ** arg3, int64_t arg4) {
    char[19] signame;
    int64_t var_ch;
    int64_t wstatus;
    int64_t var_20h_2;
    int64_t var_38h_2;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void closeout(FILE * fp,int fd,pid_t pid,char const * name); */
    r12 = rcx;
    ebx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_4;
    }
    eax = rpl_fclose (rdi);
    if (eax != 0) {
        rax = errno_location ();
        r13 = rax;
        if (*(obj.filter_command) != 0) {
            if (*(rax) == 0x20) {
                goto label_5;
            }
        }
        rdx = r12;
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        rcx = rax;
        eax = 0;
        error (1, *(r13), 0x0000a31e);
    }
label_5:
    if (ebx < 0) {
        goto label_6;
    }
label_0:
    rcx = n_open_pipes;
    if (rcx == 0) {
        goto label_6;
    }
    rsi = open_pipes;
    edx = 0;
    rax = rsi;
    while (*(rax) != ebx) {
        rdx++;
        rax += 4;
        if (rdx == rcx) {
            goto label_6;
        }
    }
    rcx--;
    edx = *((rsi + rcx*4));
    *(obj.n_open_pipes) = rcx;
    *(rax) = edx;
    do {
label_6:
        if (ebp > 0) {
            goto label_7;
        }
label_1:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_8;
        }
        return rax;
label_4:
    } while (esi < 0);
    eax = close (ebx);
    if (eax >= 0) {
        goto label_0;
    }
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_7:
    *((rsp + 0x1c)) = 0;
    eax = waitpid (ebp, rsp + 0x1c, 0);
    if (eax == 0xffffffff) {
        goto label_9;
    }
label_2:
    ecx = *((rsp + 0x1c));
    ebx = *((rsp + 0x1c));
    ebx &= 0x7f;
    eax = rbx + 1;
    if (al <= 1) {
        goto label_10;
    }
    if (ebx == 0xd) {
        goto label_1;
    }
    rbp = rsp + 0x20;
    eax = sig2str (ebx, rbp, rdx, rcx);
    if (eax != 0) {
        goto label_11;
    }
label_3:
    rdx = r12;
    edi = 0;
    esi = 3;
    r13 = filter_command;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "with FILE=%s, signal %s from command: %s");
    r9 = r13;
    r8 = rbp;
    rcx = r12;
    eax = 0;
    error (rbx + 0x80, 0, rax);
    goto label_1;
label_10:
    if (ebx != 0) {
        goto label_12;
    }
    ebp = (int32_t) ch;
    ch &= 0xff;
    if (ch == 0) {
        goto label_1;
    }
    rdx = r12;
    edi = 0;
    esi = 3;
    rbx = filter_command;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "with FILE=%s, exit %d from command: %s");
    r9 = rbx;
    r8d = ebp;
    rcx = r12;
    eax = 0;
    error (ebp, 0, rax);
    goto label_1;
label_9:
    rax = errno_location ();
    rbx = rax;
    if (*(rax) == 0xa) {
        goto label_2;
    }
    edx = 5;
    rax = dcgettext (0, "waiting for child process");
    eax = 0;
    rax = error (1, *(rbx), rax);
label_11:
    r8d = ebx;
    rcx = 0x0000a130;
    rdi = rbp;
    eax = 0;
    edx = 0x13;
    esi = 1;
    sprintf_chk ();
    goto label_3;
label_8:
    stack_chk_fail ();
label_12:
    edx = 5;
    *((rsp + 0xc)) = ecx;
    rax = dcgettext (0, "unknown status from command (0x%X)");
    ecx = *((rsp + 0xc));
    eax = 0;
    rax = error (1, 0, rax);
}

/* /tmp/tmpb028xga7 @ 0x53e0 */
 
int64_t dbg_cwrite (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_1fh, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, void ** arg2, int64_t arg3) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    rsi = arg2;
    rdx = arg3;
    /* _Bool cwrite(_Bool new_file_flag,char const * bp,size_t bytes); */
    rbx = rdx;
    if (dil == 0) {
        goto label_0;
    }
    rax = rsi;
    rax |= rdx;
    if (rax == 0) {
        if (*(obj.elide_empty_files) != 0) {
            goto label_1;
        }
    }
    closeout (0, *(obj.output_desc), *(obj.filter_pid), *(obj.outfile), r8, r9);
    next_file_name ();
    eax = create (*(obj.outfile), rsi, rdx, rcx, r8, r9);
    *(obj.output_desc) = eax;
    edi = eax;
    if (eax < 0) {
        goto label_2;
    }
    do {
        rax = full_write (rdi, rbp, rbx);
        if (rax != rbx) {
            rax = errno_location ();
            rbx = rax;
            if (*(rax) == 0x20) {
                eax = 0;
                if (*(obj.filter_command) != 0) {
                    goto label_3;
                }
            }
            rdx = outfile;
            esi = 3;
            edi = 0;
            rax = quotearg_n_style_colon ();
            rcx = rax;
            eax = 0;
            error (1, *(rbx), 0x0000a31e);
        }
label_1:
        eax = 1;
label_3:
        return rax;
label_0:
        edi = output_desc;
    } while (1);
label_2:
    rdx = outfile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
}

/* /tmp/tmpb028xga7 @ 0x54f0 */
 
int64_t dbg_bytes_split (int64_t arg_8h, uint32_t arg_10h, int64_t arg_18h, uint32_t arg_1fh, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1, uint32_t arg2, uint32_t arg3, uint32_t arg4, int64_t arg5) {
    int64_t var_10h;
    uint32_t var_20h;
    uint32_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void bytes_split(uintmax_t n_bytes,char * buf,size_t bufsize,size_t initial_read,uintmax_t max_files); */
    r15d = 0;
    r14 = rdi;
    r13 = rdi;
    ebx = 1;
    *((rsp + 0x20)) = rsi;
    *((rsp + 0x28)) = rdx;
    *((rsp + 0x10)) = r8;
    if (rcx == -1) {
        goto label_3;
    }
    r12 = rcx;
    rsp + 0x1f = (rcx < rdx) ? 1 : 0;
label_1:
    if (r12 < r14) {
        goto label_4;
    }
    rax = *((rsp + 0x10));
    rsi = *((rsp + 0x20));
    r11 = rax - 1;
    while (bpl == 0) {
label_0:
        ebx = (int32_t) bl;
        r15 += rbx;
        bl = (r15 <= r11) ? 1 : 0;
        if (r15 > r11) {
            if (al != 0) {
                goto label_5;
            }
        }
        r12 -= r14;
        rsi += r14;
        r14 = r13;
        if (r13 > r12) {
            goto label_6;
        }
        eax = 1;
        bpl |= bl;
    }
    edi = (int32_t) bl;
    *((rsp + 8)) = r11;
    *(rsp) = rsi;
    eax = cwrite (rdi, rsi, r14, rcx, r8, r9);
    r11 = *((rsp + 8));
    rsi = *(rsp);
    eax ^= 1;
    goto label_0;
label_4:
    rsi = *((rsp + 0x20));
label_6:
    if (r12 == 0) {
        goto label_7;
    }
    eax = 1;
    bpl |= bl;
    while (1) {
        ebx = (int32_t) bl;
        r15 += rbx;
        bl = (*((rsp + 0x10)) == r15) ? 1 : 0;
        bl &= al;
        if (bl != 0) {
            goto label_8;
        }
        r14 -= r12;
label_7:
        if (*((rsp + 0x1f)) != 0) {
            goto label_5;
        }
label_3:
        if (bpl == 0) {
            goto label_9;
        }
label_2:
        rax = safe_read (0, *((rsp + 0x20)), *((rsp + 0x28)));
        r12 = rax;
        if (rax == -1) {
            goto label_10;
        }
        rsp + 0x1f = (rax == 0) ? 1 : 0;
        goto label_1;
        edi = (int32_t) bl;
        eax = cwrite (rdi, rsi, r12, rcx, r8, r9);
        eax ^= 1;
    }
label_9:
    rsi = r14;
    edi = 0;
    edx = 1;
    rax = lseek ();
    eax = 1;
    if (rax != -1) {
        r14 = r13;
    }
    if (rax != -1) {
        ebx = eax;
    }
    goto label_2;
label_5:
    rbx = r15 + 1;
    if (*((rsp + 0x10)) <= r15) {
        goto label_8;
    }
    do {
        cwrite (1, 0, 0, rcx, r8, r9);
        rax = rbx;
        rbx++;
    } while (*((rsp + 0x10)) != rax);
label_8:
    return rax;
label_10:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
}

/* /tmp/tmpb028xga7 @ 0x4620 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___TMC_END__;
    rax = obj___TMC_END__;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x4650 */
 
int64_t register_tm_clones (void) {
    rdi = obj___TMC_END__;
    rsi = obj___TMC_END__;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x4690 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002510 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpb028xga7 @ 0x2510 */
 
void fcn_00002510 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpb028xga7 @ 0x46d0 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpb028xga7 @ 0x9e30 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpb028xga7 @ 0x7a90 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x5b50 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmpb028xga7 @ 0x7dc0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x2670 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpb028xga7 @ 0x8c10 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x78d0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x2560 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpb028xga7 @ 0x8e30 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x2800 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpb028xga7 @ 0x9370 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000a31e);
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x2640 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpb028xga7 @ 0x28a0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpb028xga7 @ 0x2850 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpb028xga7 @ 0x25c0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpb028xga7 @ 0x77f0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x99d0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7ae0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2a10)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7850 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x5f90 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpb028xga7 @ 0x26c0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpb028xga7 @ 0x2570 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpb028xga7 @ 0x2940 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpb028xga7 @ 0x8020 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2a29)() ();
    }
    if (rdx == 0) {
        void (*0x2a29)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x80c0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a2e)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2a2e)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7c00 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x2a1a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7f80 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2a24)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x9e44 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpb028xga7 @ 0x8eb0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x8fb0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x8e50 */
 
uint64_t xrealloc (int32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x8d90 */
 
uint64_t dbg_xalignalloc (void) {
    /* void * xalignalloc(idx_t alignment,idx_t size); */
    rax = aligned_alloc ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x2970 */
 
void aligned_alloc (void) {
    __asm ("bnd jmp qword [reloc.aligned_alloc]");
}

/* /tmp/tmpb028xga7 @ 0x5de0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2810)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpb028xga7 @ 0x8df0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x7ac0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x9330 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27e0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x2660 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpb028xga7 @ 0x8e10 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x8b50 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x86e0)() ();
}

/* /tmp/tmpb028xga7 @ 0x5ba0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a31e);
    } while (1);
}

/* /tmp/tmpb028xga7 @ 0x5f30 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x5af0 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x9d70 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpb028xga7 @ 0x8db0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x9cf0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x8170 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a33)() ();
    }
    if (rax == 0) {
        void (*0x2a33)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x8400 */
 
int64_t dbg_str2sig (int64_t arg1, int64_t arg2) {
    char * endp;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    /* int str2sig(char const * signame,int * signum); */
    r13 = rsi;
    r12 = rdi;
    rbp = 0x0000f0a4;
    ebx = 0;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = *(rdi);
    eax -= 0x30;
    if (eax > 9) {
        goto label_3;
    }
    goto label_4;
    do {
        ebx++;
        rbp += 0xc;
        if (ebx == 0x23) {
            goto label_5;
        }
label_3:
        eax = strcmp (rbp, r12);
    } while (eax != 0);
    rdx = rbx * 3;
    rax = obj_numname_table;
    eax = *((rax + rdx*4));
    do {
label_2:
        r8d = *((rax + rdx*4));
        r8d >>= 0x1f;
label_0:
        *(r13) = eax;
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        eax = r8d;
        return rax;
label_4:
        rax = strtol (rdi, rsp, 0xa);
        rdx = *(rsp);
        if (*(rdx) != 0) {
            goto label_1;
        }
    } while (rax <= 0x40);
    do {
label_1:
        eax = 0xffffffff;
        r8d = 0xffffffff;
        goto label_0;
label_5:
        eax = libc_current_sigrtmin ();
        eax = libc_current_sigrtmax ();
        ebx = eax;
        if (ebp > 0) {
            eax = strncmp (r12, 0x0000b668, 5);
            if (eax == 0) {
                goto label_7;
            }
        }
    } while (ebx <= 0);
    eax = strncmp (r12, "RTMAX", 5);
    if (eax != 0) {
        goto label_1;
    }
    rax = strtol (r12 + 5, rsp, 0xa);
    rdx = *(rsp);
    if (*(rdx) != 0) {
        goto label_1;
    }
    ebp -= ebx;
    rbp = (int64_t) ebp;
    if (rax < rbp) {
        goto label_1;
    }
    if (rax > 0) {
        goto label_1;
    }
    eax += ebx;
    goto label_2;
label_7:
    rax = strtol (r12 + 5, rsp, 0xa);
    rdx = rax;
    rax = *(rsp);
    if (*(rax) != 0) {
        goto label_1;
    }
    if (rdx < 0) {
        goto label_1;
    }
    eax = ebx;
    eax -= ebp;
    rax = (int64_t) eax;
    if (rdx > rax) {
        goto label_1;
    }
    eax = rbp + rdx;
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7ef0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x9200 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x2790 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpb028xga7 @ 0x7790 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x82b0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x7730 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x9270 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27e0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x79d0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x94c0 */
 
void dbg_xdectoumax (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* uintmax_t xdectoumax(char const * n_str,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoumax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmpb028xga7 @ 0x93b0 */
 
int64_t dbg_xnumtoumax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    uintmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* uintmax_t xnumtoumax(char const * n_str,int base,uintmax_t min,uintmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x94f0 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000ba18;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xba18 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmpb028xga7 @ 0x8300 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x5d60 */
 
uint64_t dbg_fd_reopen (int64_t arg1, int64_t arg4, int64_t oflag, char * path) {
    rdi = arg1;
    rcx = arg4;
    rdx = oflag;
    rsi = path;
    /* int fd_reopen(int desired_fd,char const * file,int flags,mode_t mode); */
    eax = 0;
    eax = open (rsi, edx, ecx);
    r12d = eax;
    if (ebp != eax) {
        if (eax >= 0) {
            goto label_0;
        }
    }
    eax = r12d;
    return eax;
label_0:
    esi = ebp;
    edi = eax;
    eax = dup2 ();
    ebx = eax;
    rax = errno_location ();
    r12d = ebx;
    r13d = *(rax);
    close (r12d);
    eax = r12d;
    *(rbp) = r13d;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x85a0 */
 
uint64_t dbg_sig2str (int64_t arg_4h, int64_t arg_5h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int sig2str(int signum,char * signame); */
    rax = obj_numname_table;
    edx = 0;
    rcx = rax;
    ebx = edi;
    while (*(rcx) != ebx) {
        edx++;
        rcx += 0xc;
        if (edx == 0x23) {
            goto label_2;
        }
    }
    rdx *= 3;
    eax = strcpy (rbp, rax + rdx*4 + 4);
    eax = 0;
    do {
label_1:
        return rax;
label_2:
        eax = libc_current_sigrtmin ();
        r12d = eax;
        eax = libc_current_sigrtmax ();
        if (r12d > ebx) {
            goto label_3;
        }
        if (eax < ebx) {
            goto label_3;
        }
        edx = eax;
        edx -= r12d;
        edx >>= 1;
        edx += r12d;
        if (edx >= ebx) {
            goto label_4;
        }
        edx = 0x58;
        *(rbp) = 0x414d5452;
        r12d = eax;
        *((rbp + 4)) = dx;
label_0:
        eax = ebx;
        eax -= r12d;
    } while (eax == 0);
    rdi = rbp + 5;
    r8d = eax;
    eax = 0;
    rdx = 0xffffffffffffffff;
    rcx = 0x0000b674;
    esi = 1;
    eax = sprintf_chk ();
    eax = 0;
    return rax;
label_4:
    ecx = 0x4e;
    *(rbp) = 0x494d5452;
    *((rbp + 4)) = cx;
    goto label_0;
label_3:
    eax = 0xffffffff;
    goto label_1;
}

/* /tmp/tmpb028xga7 @ 0x2590 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmpb028xga7 @ 0x2920 */
 
void libc_current_sigrtmin (void) {
    __asm ("bnd jmp qword [reloc.__libc_current_sigrtmin]");
}

/* /tmp/tmpb028xga7 @ 0x2960 */
 
void libc_current_sigrtmax (void) {
    __asm ("bnd jmp qword [reloc.__libc_current_sigrtmax]");
}

/* /tmp/tmpb028xga7 @ 0x29f0 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmpb028xga7 @ 0x5b90 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpb028xga7 @ 0x8e80 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x9040 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x5c50 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2620)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpb028xga7 @ 0x8f60 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x82c0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x8210 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2a38)() ();
    }
    if (rax == 0) {
        void (*0x2a38)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x92b0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27e0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x5eb0 */
 
uint64_t dbg_full_write (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t full_write(int fd, const * buf,size_t count); */
    if (rdx == 0) {
        goto label_0;
    }
    r12d = edi;
    rbx = rdx;
    r13d = 0;
    while (rax != -1) {
        if (rax == 0) {
            goto label_1;
        }
        r13 += rax;
        rbp += rax;
        rbx -= rax;
        if (rbx == 0) {
            goto label_2;
        }
        rax = safe_write (r12d, rbp, rbx);
    }
    do {
label_2:
        rax = r13;
        return rax;
label_1:
        errno_location ();
        *(rax) = 0x1c;
        rax = r13;
        return rax;
label_0:
        r13d = 0;
    } while (1);
}

/* /tmp/tmpb028xga7 @ 0x9a60 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x9a80)() ();
}

/* /tmp/tmpb028xga7 @ 0x79c0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x78d0)() ();
}

/* /tmp/tmpb028xga7 @ 0x8320 */
 
uint64_t dbg_safe_read (int64_t arg1, uint32_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_read(int fd,void * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = read (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (rbx > 0x7ff00000) {
        if (eax != 0x16) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x7aa0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x2a40 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    uintmax_t n_start;
    sigaction act;
    char[21] buffer;
    void ** s;
    void ** var_10h;
    void ** ptr;
    uint32_t var_20h;
    char * s2;
    char * var_30h;
    size_t var_38h;
    size_t var_40h;
    uint32_t s1;
    uint32_t var_56h;
    uint32_t var_57h;
    size_t * n;
    int64_t var_68h;
    struct sigaction * oldact;
    int64_t var_110h;
    int64_t var_128h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r14 = obj_longopts;
    r13 = "0123456789C:a:b:del:n:t:ux";
    r12 = 0x0000a1b3;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x128)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000b6d1);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = 0x0000b080;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rax = 0x0000a210;
    *(rsp) = 0;
    *(obj.infile) = rax;
    rax = 0x0000a3bd;
    *(obj.outbase) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 8)) = 0;
    do {
label_0:
        r15d = optind;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        eax = 1;
        edi = ebp;
        if (r15d == 0) {
            r15d = eax;
        }
        r8d = 0;
        eax = getopt_long ();
        r8d = eax;
        if (eax != 0xffffffff) {
            if (eax > 0x83) {
                goto label_45;
            }
            if (eax > 0x2f) {
                eax = rax - 0x30;
                if (eax > 0x53) {
                    goto label_45;
                }
                edx = eax;
                rdx = *((r12 + rdx*4));
                rdx += r12;
                /* switch table (84 cases) at 0xb080 */
                eax = void (*rdx)() ();
            }
            if (eax == 0xffffff7d) {
                goto label_46;
            }
            if (eax != 0xffffff7e) {
                goto label_45;
            }
            usage (0, rsi, rdx, rcx, r8, r9);
        }
        if (*((rsp + 0x10)) == 0) {
            goto label_47;
        }
        if (*(obj.filter_command) == 0) {
            goto label_47;
        }
        edx = 5;
label_2:
        rax = dcgettext (0, "--filter does not process a chunk extracted to stdout");
        eax = 0;
        error (0, 0, rax);
label_45:
        rax = usage (1, rsi, rdx, rcx, r8, r9);
        ecx = *((rsp + 8));
        if (ecx != 0) {
            if (ecx != 4) {
                goto label_48;
            }
            ecx = *((rsp + 0x18));
            if (ecx == 0) {
                goto label_49;
            }
            if (ecx == r15d) {
                goto label_49;
            }
        }
        rax = (int64_t) eax;
        *((rsp + 0x18)) = r15d;
        *(rsp) = rax;
        *((rsp + 8)) = 4;
    } while (1);
    rsi = "0123456789abcdef";
    rax = "0123456789";
    r15 = optarg;
    if (r8d == 0x64) {
        rsi = rax;
    }
    *((rsp + 0x30)) = r8d;
    *(obj.suffix_alphabet) = rsi;
    *((rsp + 0x28)) = rsi;
    if (r15 == 0) {
        goto label_0;
    }
    rax = strlen (r15);
    *((rsp + 0x38)) = rax;
    rax = strspn (r15, *((rsp + 0x28)));
    r8d = *((rsp + 0x30));
    if (*((rsp + 0x38)) == rax) {
        goto label_50;
    }
    goto label_51;
    do {
        if (*((r15 + 1)) == 0) {
            goto label_52;
        }
        r15++;
        *(obj.optarg) = r15;
label_50:
    } while (*(r15) == 0x30);
label_52:
    *(obj.numeric_suffix_start) = r15;
    goto label_0;
    if (*((rsp + 8)) != 0) {
        goto label_48;
    }
    edx = 5;
    rax = dcgettext (0, "invalid number of bytes");
    rdx = 0x7fffffffffffffff;
    rax = xdectoumax (*(obj.optarg), 1, rdx, "bEGKkMmPTYZ0", rax, 0);
    *((rsp + 8)) = 1;
    *(rsp) = rax;
    goto label_0;
    edx = 5;
    rax = dcgettext (0, "invalid suffix length");
    rdx = 0x1fffffffffffffff;
    rax = xdectoumax (*(obj.optarg), 0, rdx, 0x0000b6d1, rax, 0);
    *(obj.suffix_length) = rax;
    goto label_0;
    if (*((rsp + 8)) != 0) {
        goto label_48;
    }
    edx = 5;
    rax = dcgettext (0, "invalid number of bytes");
    rdx = 0x7fffffffffffffff;
    rax = xdectoumax (*(obj.optarg), 1, rdx, "bEGKkMmPTYZ0", rax, 0);
    *((rsp + 8)) = 2;
    *(rsp) = rax;
    goto label_0;
    *(obj.elide_empty_files) = 1;
    goto label_0;
    if (*((rsp + 8)) != 0) {
        goto label_48;
    }
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = optarg;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        *(obj.optarg) = rax;
        r15 = rax;
        rax++;
        edx = *(r15);
    }
    eax = strncmp (r15, 0x0000a273, 2);
    if (eax == 0) {
        goto label_53;
    }
    eax = strncmp (r15, 0x0000a276, 2);
    *((rsp + 8)) = 5;
    if (eax == 0) {
        r15 += 2;
        *((rsp + 8)) = 6;
        *(obj.optarg) = r15;
    }
label_1:
    rax = strchr (*(obj.optarg), 0x2f);
    edx = 5;
    rsi = "invalid number of chunks";
    edi = 0;
    r15 = rax;
    if (rax == 0) {
        goto label_54;
    }
    rax = dcgettext (rdi, rsi);
    rdx |= 0xffffffffffffffff;
    rax = xdectoumax (r15 + 1, 1, rdx, 0x0000b6d1, rax, 0);
    *(rsp) = rax;
    if (r15 == *(obj.optarg)) {
        goto label_0;
    }
    *(r15) = 0;
    edx = 5;
    rax = dcgettext (0, "invalid chunk number");
    rax = xdectoumax (*(obj.optarg), 1, *(rsp), 0x0000b6d1, rax, 0);
    *((rsp + 0x10)) = rax;
    goto label_0;
    if (*((rsp + 8)) != 0) {
        goto label_48;
    }
    edx = 5;
    rax = dcgettext (0, "invalid number of lines");
    rdx |= 0xffffffffffffffff;
    rax = xdectoumax (*(obj.optarg), 1, rdx, 0x0000b6d1, rax, 0);
    *((rsp + 8)) = 3;
    *(rsp) = rax;
    goto label_0;
    r15 = optarg;
    rdi = optarg;
    rax = last_component ();
    if (r15 != rax) {
        goto label_55;
    }
    *(obj.additional_suffix) = r15;
    goto label_0;
    edx = 5;
    rax = dcgettext (0, "invalid IO block size");
    rdx = 0x7ffffffffffffffe;
    rax = xdectoumax (*(obj.optarg), 1, rdx, "bEGKkMmPTYZ0", rax, 0);
    *((rsp + 0x20)) = rax;
    goto label_0;
    rax = optarg;
    *(obj.filter_command) = rax;
    goto label_0;
    *(obj.verbose) = 1;
    goto label_0;
    *(obj.unbuffered) = 1;
    goto label_0;
    r15 = optarg;
    eax = *(r15);
    if (al == 0) {
        goto label_56;
    }
    if (*((r15 + 1)) != 0) {
        eax = strcmp (r15, 0x0000a2be);
        if (eax != 0) {
            goto label_57;
        }
        eax = 0;
    }
    edx = eolchar;
    if (edx >= 0) {
        if (edx != eax) {
            goto label_58;
        }
    }
    *(obj.eolchar) = eax;
    goto label_0;
label_46:
    eax = 0;
    version_etc (*(obj.stdout), "split", "GNU coreutils", *(obj.Version), "Torbjorn Granlund", "Richard M. Stallman");
    exit (0);
label_53:
    r15 += 2;
    *((rsp + 8)) = 7;
    *(obj.optarg) = r15;
    goto label_1;
label_49:
    rdx = 0x1999999999999999;
    rcx = *(rsp);
    if (rcx > rdx) {
        goto label_59;
    }
    rdx = rcx * 0xa;
    rax = (int64_t) eax;
    rax += rdx;
    if (rax < rcx) {
        goto label_59;
    }
    *(rsp) = rax;
    *((rsp + 0x18)) = r15d;
    goto label_0;
label_47:
    if (*((rsp + 8)) != 0) {
        goto label_60;
    }
    *(rsp) = 0x3e8;
    *((rsp + 8)) = 3;
    do {
        if (*(obj.eolchar) < 0) {
            *(obj.eolchar) = 0xa;
        }
        eax = *((rsp + 8));
        rdi = numeric_suffix_start;
        r12d = rax - 5;
        if (rdi == 0) {
            goto label_61;
        }
        *(obj.suffix_auto) = 0;
        if (r12d <= 2) {
            goto label_62;
        }
label_23:
        r14 = suffix_length;
        if (r14 == 0) {
            goto label_24;
        }
label_25:
        *(obj.suffix_auto) = 0;
label_22:
        eax = optind;
        if (eax >= ebp) {
            goto label_63;
        }
        rcx = (int64_t) eax;
        edx = rax + 1;
        rsi = rcx*8;
        rcx = *((rbx + rcx*8));
        *(obj.optind) = edx;
        *(obj.infile) = rcx;
        if (edx >= ebp) {
            goto label_63;
        }
        rdx = *((rbx + rsi + 8));
        eax += 2;
        *(obj.optind) = eax;
        *(obj.outbase) = rdx;
        if (eax >= ebp) {
            goto label_63;
        }
        rax = quote (*((rbx + rsi + 0x10)), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
label_29:
        rax = dcgettext (0, "extra operand %s");
label_32:
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        usage (1, rsi, rdx, rcx, r8, r9);
label_54:
        rax = dcgettext (rdi, rsi);
        rdx |= 0xffffffffffffffff;
        rax = xdectoumax (*(obj.optarg), 1, rdx, 0x0000b6d1, rax, 0);
        *(rsp) = rax;
        goto label_0;
label_60:
    } while (*(rsp) != 0);
    rax = quote (0x0000a2bf, rsi, rdx, rcx, r8);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "invalid number of lines");
    r8 = rbx;
    rcx = rax;
    eax = 0;
    error (0, 0, "%s: %s");
    usage (1, rsi, rdx, rcx, r8, r9);
label_63:
    rdi = numeric_suffix_start;
    if (rdi == 0) {
        goto label_64;
    }
    rax = strlen (rdi);
    edx = 5;
    rsi = "numerical suffix start value is too large for the suffix length";
    if (rax > *(obj.suffix_length)) {
        goto label_2;
    }
label_64:
    rbp = infile;
    eax = strcmp (rbp, 0x0000a210);
    if (eax != 0) {
        eax = fd_reopen (0, rbp, 0, 0);
        if (eax < 0) {
            goto label_65;
        }
    }
    eax = fstat (0, obj.in_stat_buf);
    if (eax != 0) {
        goto label_66;
    }
    if (*((rsp + 0x20)) == 0) {
        rcx = Scrt1.o;
        rax = 0x1ffffffffffe0000;
        rdx = rcx - 0x20000;
        eax = 0x20000;
        if (rdx <= rax) {
            rax = rcx;
        }
        *((rsp + 0x20)) = rax;
    }
    eax = getpagesize ();
    rcx = *((rsp + 0x20));
    r13 |= 0xffffffffffffffff;
    rdi = (int64_t) eax;
    rsi = rcx + 1;
    rax = xalignalloc ();
    *((rsp + 0x48)) = rax;
    if (r12d <= 1) {
        goto label_67;
    }
label_28:
    if (*(obj.filter_command) != 0) {
        r12 = obj_newblocked;
        rdi = r12;
        sigemptyset ();
        sigaction (0xd, 0, rsp + 0x70);
        if (*((rsp + 0x70)) != 1) {
            esi = 0xd;
            rdi = r12;
            sigaddset ();
        }
        rdx = obj_oldblocked;
        rsi = r12;
        edi = 0;
        sigprocmask ();
    }
    eax = *((rsp + 8));
    eax -= 2;
    if (eax > 5) {
        goto label_68;
    }
    rdx = 0x0000b1d0;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (6 cases) at 0xb1d0 */
    r15d = 0;
    r13 = *(rsp);
    *((rsp + 8)) = bpl;
    r14 = r15;
label_20:
    rax = safe_read (0, *((rsp + 0x48)), *((rsp + 0x20)));
    *(rsp) = rax;
    rax++;
    if (rax == 0) {
        goto label_69;
    }
    r15 = *((rsp + 0x48));
    rbx = *(rsp);
    ebp = eolchar;
    rbx += r15;
    r12 = r15;
    *(rbx) = bpl;
    do {
label_3:
        esi = ebp;
        rdi = r15;
        rax = rawmemchr ();
        if (rbx == rax) {
            goto label_70;
        }
        r14++;
        r15 = rax + 1;
    } while (r13 > r14);
    edi = *((rsp + 8));
    rdx = r15;
    r14d = 0;
    rdx -= r12;
    r12 = r15;
    cwrite (rdi, r12, rdx, rcx, r8, r9);
    *((rsp + 8)) = 1;
    ebp = eolchar;
    goto label_3;
    *((rsp + 0x10)) = 0;
    r12d = 0;
    r15d = 0;
    ebp = 0;
    *((rsp + 0x18)) = 0;
    do {
label_15:
        rax = safe_read (0, *((rsp + 0x48)), *((rsp + 0x20)));
        rbx = rax;
        if (rax == -1) {
            goto label_71;
        }
        if (rax == 0) {
            goto label_72;
        }
        r14 = *((rsp + 0x48));
        goto label_73;
label_4:
        rax = r14 + r13 - 1;
        rdx = r13;
        rdi = r14;
        *((rsp + 8)) = rax;
        rax = memrchr ();
        r8 = rax;
        if (r15 == 0) {
            goto label_13;
        }
label_5:
        dil = (rbp == 0) ? 1 : 0;
        if (r8 != 0) {
            goto label_74;
        }
        if (dil != 0) {
            goto label_74;
        }
label_19:
        if (*((rsp + 0x10)) != 0) {
            goto label_75;
        }
        edi = 0;
        dil = (rbp == 0) ? 1 : 0;
        if (*((rsp + 8)) == 0) {
            goto label_76;
        }
        rbx -= r13;
        r14 += r13;
        cwrite (rdi, r14, r13, rcx, r8, r9);
        ebp = 0;
label_6:
    } while (rbx == 0);
label_73:
    r13 = *(rsp);
    rax = rbp + r15;
    esi = eolchar;
    *((rsp + 0x28)) = rax;
    r13 -= rax;
    if (r13 <= rbx) {
        goto label_4;
    }
    rdx = rbx;
    rdi = r14;
    r13d = 0;
    rax = memrchr ();
    *((rsp + 8)) = 0;
    r8 = rax;
    if (r15 != 0) {
        goto label_5;
    }
label_13:
    if (r8 == 0) {
        goto label_77;
    }
    r8 -= r14;
    r15 = r8 + 1;
    dil = (rbp == 0) ? 1 : 0;
    rbp += r15;
    rbx -= r15;
    r14 += r15;
    cwrite (0, r14, r15, rcx, r8, r9);
    if (*((rsp + 8)) == 0) {
        goto label_78;
    }
    r13 -= r15;
    rsp + 0x10 = (r13 != 0) ? 1 : 0;
    if (r13 == 0) {
        goto label_79;
    }
    r15d = 0;
label_14:
    rax = r12;
    rax -= r15;
    if (rax < r13) {
        r12 += *((rsp + 0x20));
        if (r12 < 0) {
            goto label_80;
        }
        rdi = *((rsp + 0x18));
        rsi = r12;
        rax = xrealloc ();
        *((rsp + 0x18)) = rax;
    }
    rax = *((rsp + 0x18));
    rbx -= r13;
    r14 += r13;
    r15 += r13;
    eax = memcpy (rax + r15, r14, r13);
label_43:
    ecx = *((rsp + 0x10));
    eax = 0;
    if (*((rsp + 8)) != 0) {
        ecx = eax;
    }
    eax = 0;
    if (*((rsp + 8)) != 0) {
    }
    *((rsp + 0x10)) = cl;
    goto label_6;
    *((rsp + 0x18)) = 0;
    if (*((rsp + 0x10)) == 0) {
        goto label_81;
    }
label_37:
    *((rsp + 8)) = 0;
    r13d = 1;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    *((rsp + 0x56)) = 0;
    do {
label_7:
        rax = safe_read (0, *((rsp + 0x48)), *((rsp + 0x20)));
        if (rax == -1) {
            goto label_82;
        }
        if (rax == 0) {
            goto label_83;
        }
        rcx = *((rsp + 0x48));
        r12 = rcx + rax;
    } while (rcx == r12);
    goto label_84;
label_8:
    if (r13 == rax) {
        goto label_85;
    }
label_12:
    if (r15b != 0) {
        rax = r13 + 1;
        r13d = 1;
        if (*(rsp) == r13) {
            r13 = rax;
            goto label_10;
        }
    }
label_10:
    if (r12 == rbx) {
        goto label_7;
    }
label_84:
    r14 = r12;
    r14 -= rbp;
    rax = memchr (rbp, *(obj.eolchar), r14);
    if (rax == 0) {
        goto label_86;
    }
    rbx = rax + 1;
    r15d = 1;
    r14 = rbx;
    r14 -= rbp;
label_11:
    rax = *((rsp + 0x10));
    if (rax != 0) {
        goto label_8;
    }
    al = ofile_open (*((rsp + 0x18)), *((rsp + 8)), *(rsp), rcx, r8, r9);
    rax = errno_location ();
    r10 = *((rsp + 8));
    *((rsp + 0x40)) = rax;
    r10 <<= 5;
    r10 += *((rsp + 0x18));
    if (*(obj.unbuffered) != 0) {
        goto label_87;
    }
    rcx = *((r10 + 0x10));
    edx = 1;
    rsi = r14;
    rdi = rbp;
    *((rsp + 0x38)) = r10;
    rax = fwrite_unlocked ();
    r10 = *((rsp + 0x38));
    r11 = *((rsp + 0x40));
    rax--;
    if (rax == 0) {
        goto label_16;
    }
    if (*(r11) == 0x20) {
        if (*(obj.filter_command) != 0) {
            goto label_17;
        }
    }
    rdx = *(r10);
    esi = 3;
    edi = 0;
    *(rsp) = r11;
    rax = quotearg_n_style_colon ();
    r11 = *(rsp);
    rcx = rax;
    eax = 0;
    error (1, *(r11), 0x0000a31e);
    rax = *(rsp);
    rcx = *((rsp + 0x10));
    if (rax < rcx) {
        goto label_88;
    }
    if (rax > rbp) {
        goto label_88;
    }
    rax = rbp;
    edx = 0;
    rax = *(rdx:rax) / rsp;
    rdx = *(rdx:rax) % rsp;
    rbx = rax;
    rax = *((rsp + 0x10));
    if (rax <= 1) {
        goto label_89;
    }
    r12 = rax - 1;
    rax = rbx;
    rax *= r12;
    r15 = rax - 1;
    r14 = r15;
    if (r13 <= r15) {
        goto label_90;
    }
    r13 -= r15;
    memmove (*((rsp + 0x48)), rdi + r15, r13);
    *((rsp + 8)) = r15;
label_38:
    *((rsp + 0x57)) = 0;
    *((rsp + 0x30)) = 1;
    *((rsp + 0x18)) = rbp;
    rbp = *((rsp + 0x10));
label_41:
    rcx = *((rsp + 8));
    if (*((rsp + 0x18)) <= rcx) {
        goto label_91;
    }
    if (r13 == -1) {
        goto label_92;
    }
label_40:
    if (r13 == 0) {
        goto label_91;
    }
    rax = *((rsp + 0x18));
    rax -= *((rsp + 8));
    *((rsp + 0x57)) = 0;
    r8 = *((rsp + 0x48));
    if (rax <= r13) {
        r13 = rax;
    }
    r10 = r8;
    r10 += r13;
    *((rsp + 0x58)) = r13;
    r13 = r8;
    r8 = rbx;
    rbx = *(rsp);
    r15 = r10;
label_27:
    rax |= 0xffffffffffffffff;
    if (r15 == r13) {
        goto label_93;
    }
    edx = 0;
    rax = r14;
    rax -= *((rsp + 8));
    rcx = *((rsp + 0x58));
    __asm ("cmovs rax, rdx");
    esi = eolchar;
    *((rsp + 0x28)) = r8;
    if (rax > rcx) {
        rax = rcx;
    }
    rcx -= rax;
    rax = memchr (r13 + rax, rsi, rcx);
    r8 = *((rsp + 0x28));
    r9 = rax;
    if (rax == 0) {
        goto label_94;
    }
    *((rsp + 0x56)) = 1;
    r9++;
label_30:
    rax = r9;
    rax -= r13;
    *((rsp + 0x28)) = rax;
    if (r12 == rbp) {
        goto label_95;
    }
    if (rbp == 0) {
        goto label_96;
    }
label_31:
    r13 = *((rsp + 8));
    r13 += *((rsp + 0x28));
    *((rsp + 8)) = r13;
    ecx = *((rsp + 0x56));
    do {
label_9:
        if (r13 <= r14) {
            if (cl == 0) {
                goto label_97;
            }
        }
        eax = ecx;
        eax ^= 1;
        dl = (r15 == r9) ? 1 : 0;
        al &= dl;
        if (al != 0) {
            goto label_98;
        }
        r12++;
        dl = (rbp != 0) ? 1 : 0;
        al = (r12 > rbp) ? 1 : 0;
        if ((dl & al) != 0) {
            goto label_21;
        }
        r14 += r8;
        if (rbx == r12) {
            rax = *((rsp + 0x18));
            r14 = rax - 1;
        }
        if (r13 <= r14) {
            goto label_99;
        }
    } while (rbp != 0);
    *((rsp + 0x40)) = r8;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = cl;
    cwrite (1, 0, 0, rcx, r8, r9);
    ecx = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    r8 = *((rsp + 0x40));
    goto label_9;
    if (*((rsp + 0x10)) != 0) {
        goto label_100;
    }
    rax = rbp;
    edx = 0;
    rax = rdx:rax / r8;
    rdx = rdx:rax % r8;
    bytes_split (rax, *((rsp + 0x48)), *((rsp + 0x20)), r13, *(rsp), r9);
label_21:
    eax = close (0);
    if (eax != 0) {
        goto label_101;
    }
    closeout (0, *(obj.output_desc), *(obj.filter_pid), *(obj.outfile), r8, r9);
    exit (0);
label_16:
    if (*(r11) == 0x20) {
        if (*(obj.filter_command) != 0) {
            goto label_17;
        }
    }
    *((rsp + 0x30)) = 1;
label_17:
    if (*((rsp + 0x28)) != 0) {
        goto label_102;
    }
label_18:
    if (r15b == 0) {
        goto label_10;
    }
    rax = *((rsp + 8));
    if (*(rsp) != rax) {
        goto label_10;
    }
    if (*((rsp + 0x30)) == 0) {
        goto label_103;
    }
    *((rsp + 8)) = 0;
    eax = *((rsp + 0x30));
    *((rsp + 0x30)) = 0;
    *((rsp + 0x56)) = al;
    goto label_10;
label_86:
    rbx = r12;
    r15d = 0;
    goto label_11;
label_85:
    if (*(obj.unbuffered) != 0) {
        goto label_104;
    }
    rcx = stdout;
    edx = 1;
    rsi = r14;
    rdi = rbp;
    rax = fwrite_unlocked ();
    rax--;
    if (rax == 0) {
        goto label_12;
    }
    rdi = stdout;
    clearerr_unlocked ();
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_74:
    edi = (int32_t) dil;
    *((rsp + 0x30)) = r8;
    cwrite (rdi, *((rsp + 0x18)), r15, rcx, r8, r9);
    r12 = *((rsp + 0x20));
    r8 = *((rsp + 0x30));
    if (*((rsp + 0x20)) < r15) {
        goto label_105;
    }
    rbp = *((rsp + 0x28));
    goto label_13;
label_78:
    rsp + 0x10 = (rbx != 0) ? 1 : 0;
    eax = *((rsp + 0x10));
    r15d = 0;
    if (al == 0) {
        goto label_106;
    }
label_44:
    r13 = rbx;
    goto label_14;
label_76:
    rbp += rbx;
    cwrite (rdi, r14, rbx, rcx, r8, r9);
    goto label_15;
label_87:
    *((rsp + 0x38)) = r10;
    rax = full_write (*((r10 + 8)), rbp, r14);
    r10 = *((rsp + 0x38));
    r11 = *((rsp + 0x40));
    if (rax == r14) {
        goto label_16;
    }
    if (*(obj.filter_command) == 0) {
        goto label_107;
    }
    if (*(r11) == 0x20) {
        goto label_17;
    }
label_107:
    rdx = *(r10);
    esi = 3;
    edi = 0;
    *(rsp) = r11;
    rax = quotearg_n_style_colon ();
    r11 = *(rsp);
    rcx = rax;
    eax = 0;
    error (1, *(r11), 0x0000a31e);
label_102:
    *((rsp + 0x40)) = r11;
    *((rsp + 0x38)) = r10;
    eax = rpl_fclose (*((r10 + 0x10)));
    r10 = *((rsp + 0x38));
    r11 = *((rsp + 0x40));
    if (eax != 0) {
        goto label_108;
    }
    *((r10 + 0x10)) = 0;
    *((r10 + 8)) = 0xfffffffe;
    goto label_18;
label_77:
    r15d = 0;
    goto label_19;
label_104:
    rax = full_write (1, rbp, r14);
    if (rax == r14) {
        goto label_12;
    }
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_105:
    rdi = *((rsp + 0x18));
    rsi = r12;
    rax = xrealloc ();
    rbp = *((rsp + 0x28));
    r8 = *((rsp + 0x30));
    *((rsp + 0x18)) = rax;
    goto label_13;
label_70:
    if (rbx != r12) {
        edi = *((rsp + 8));
        rbx -= r12;
        cwrite (rdi, r12, rbx, rcx, r8, r9);
        *((rsp + 8)) = 0;
    }
    if (*(rsp) != 0) {
        goto label_20;
    }
    goto label_21;
label_99:
    ecx = 0;
    goto label_9;
label_48:
    edx = 5;
    rsi = "cannot split in more than one way";
    goto label_2;
label_24:
    eax = 2;
    if (r14 < rax) {
        r14 = rax;
    }
    *(obj.suffix_length) = r14;
    goto label_22;
label_61:
    rax = *(rsp);
    r13 = rax - 1;
    if (r12d > 2) {
        goto label_23;
    }
label_26:
    r14d = 0;
    rax = strlen (*(obj.suffix_alphabet));
    rcx = rax;
    rax = r13;
    do {
        rsi = r13;
        edx = 0;
        r14++;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
    } while (rcx <= rsi);
    rax = suffix_length;
    *(obj.suffix_auto) = 0;
    if (rax == 0) {
        goto label_24;
    }
    if (r14 <= rax) {
        goto label_25;
    }
    edx = 5;
    rax = dcgettext (0, "the suffix length needs to be at least %lu");
    rcx = r14;
    eax = 0;
    error (1, 0, rax);
label_62:
    rax = *(rsp);
    r13 = rax - 1;
    eax = xstrtoumax (rdi, 0, 0xa, rsp + 0x68, 0x0000b6d1);
    if (eax != 0) {
        goto label_26;
    }
    rax = *((rsp + 0x68));
    rcx = *(rsp);
    dl = (rcx > rax) ? 1 : 0;
    rcx += rax;
    if (rcx < 0) {
        goto label_26;
    }
    if (dl == 0) {
        goto label_26;
    }
    r13 += rax;
    goto label_26;
label_98:
    *((rsp + 0x57)) = al;
label_97:
    eax = *((rsp + 0x56));
    rcx = *((rsp + 0x28));
    r13 = r9;
    *((rsp + 0x30)) = al;
    goto label_27;
label_67:
    edx = 1;
    esi = 0;
    edi = 0;
    r12d = 0;
    rax = lseek ();
    rbx = rax;
    if (rax < 0) {
        goto label_109;
    }
    r13 = *((rsp + 0x20));
    r14 = *((rsp + 0x48));
    while (rax != 0) {
        if (rax == -1) {
            goto label_42;
        }
        rbp = r12 + rax;
        r12 = rbp;
        if (rbp >= r13) {
            goto label_110;
        }
        rdx -= r12;
        rax = safe_read (0, r14 + r12, r13);
    }
label_36:
    if (r12 < 0) {
        goto label_42;
    }
    rax = *((rsp + 0x20));
    if (rax > r12) {
        rax = r12;
    }
    r13 = rax;
    rax = *(rsp);
    if (r12 < rax) {
    }
    if (rax >= 0) {
        goto label_28;
    }
    rax = umaxtostr (rax, rsp + 0x110, rdx);
    rax = quote (rax, rsi, rdx, rcx, r8);
    edx = 5;
    rbx = rax;
    rax = dcgettext (0, "invalid number of chunks");
    r8 = rbx;
    rcx = rax;
    eax = 0;
    error (1, 0x4b, "%s: %s");
label_55:
    rax = quote (r15, rsi, rdx, rcx, r8);
    edx = 5;
    rsi = "invalid suffix %s, contains directory separator";
    r12 = rax;
    goto label_29;
label_68:
    rcx |= 0xffffffffffffffff;
    bytes_split (*(rsp), *((rsp + 0x48)), *((rsp + 0x20)), rcx, 0, r9);
    goto label_21;
label_72:
    if (r15 != 0) {
        goto label_111;
    }
label_39:
    free (*((rsp + 0x18)));
    goto label_21;
label_94:
    *((rsp + 0x56)) = 0;
    r9 = r15;
    goto label_30;
label_96:
    edi = *((rsp + 0x30));
    *((rsp + 0x40)) = r8;
    *((rsp + 0x38)) = r9;
    cwrite (rdi, r13, *((rsp + 0x28)), rcx, r8, r9);
    r8 = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    goto label_31;
label_51:
    *(rsp) = r8d;
    rax = quote (r15, rsi, rdx, rcx, r8);
    r8d = *(rsp);
    edx = 5;
    rsi = "%s: invalid start value for hexadecimal suffix";
    r12 = rax;
    if (r8d != 0x64) {
        goto label_29;
    }
    rax = dcgettext (0, "%s: invalid start value for numerical suffix");
    rdx = rax;
    goto label_32;
label_95:
    *((rsp + 0x38)) = r8;
    *((rsp + 0x30)) = r9;
    rax = full_write (1, r13, rax);
    r9 = *((rsp + 0x30));
    r8 = *((rsp + 0x38));
    if (*((rsp + 0x28)) == rax) {
        goto label_31;
    }
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_83:
    if (*((rsp + 0x10)) != 0) {
        goto label_21;
    }
    if (*((rsp + 0x56)) != 0) {
        goto label_103;
    }
    rbx = *((rsp + 8));
label_34:
    rbp = *((rsp + 0x18));
    r13 = *(rsp);
    r12d = 0;
    goto label_112;
label_33:
    esi = *((rbp + 8));
    if (esi >= 0) {
        closeout (*((rbp + 0x10)), rsi, *((rbp + 0x18)), *(rbp), r8, r9);
    }
    r12++;
    *((rbp + 8)) = 0xfffffffe;
    rbp += 0x20;
    if (r13 == r12) {
        goto label_21;
    }
label_112:
    if (rbx > r12) {
        goto label_33;
    }
    if (*(obj.elide_empty_files) != 0) {
        goto label_33;
    }
    ofile_open (*((rsp + 0x18)), r12, r13, rcx, r8, r9);
    goto label_33;
label_103:
    rbx = *(rsp);
    goto label_34;
label_100:
    rax = *(rsp);
    rcx = *((rsp + 0x10));
    if (rax < rcx) {
        goto label_113;
    }
    if (rax > rbp) {
        goto label_113;
    }
    rsi = *(rsp);
    rax = rbp;
    edx = 0;
    rcx = *((rsp + 0x10));
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    rdx = rcx - 1;
    rdx *= rax;
    rbx = rdx;
    if (rsi != rcx) {
        rcx *= rax;
    }
    if (r13 <= rdx) {
        goto label_114;
    }
    r14 = *((rsp + 0x48));
    r13 -= rdx;
    memmove (r14, r14 + rdx, r13);
label_35:
    if (rbp <= rbx) {
        goto label_21;
    }
    while (rax != 0) {
        rax = rbp;
        rsi = r14;
        edi = 1;
        rax -= rbx;
        if (rax <= r13) {
            r13 = rax;
        }
        rax = full_write (rdi, rsi, r13);
        if (r13 != rax) {
            rax = errno_location ();
            if (*(obj.filter_command) != 0) {
                if (*(rax) == 0x20) {
                    goto label_115;
                }
            }
            r12 = rax;
            rdx = 0x0000a210;
            esi = 3;
            edi = 0;
            rax = quotearg_n_style_colon ();
            rcx = rax;
            eax = 0;
            error (1, *(r12), 0x0000a31e);
        }
label_115:
        rbx += r13;
        r13 |= 0xffffffffffffffff;
        goto label_35;
        rax = safe_read (0, r14, *((rsp + 0x20)));
        r13 = rax;
        if (rax == -1) {
            goto label_116;
        }
    }
    goto label_21;
label_110:
    r13 = Scrt1.o;
    if (r13 == 0) {
        goto label_117;
    }
    eax = Scrt1.o;
    r12 += rbx;
    eax &= 0xd000;
    if (eax == 0x8000) {
        if (r13 >= r12) {
            goto label_118;
        }
    }
    edx = 2;
    esi = 0;
    edi = 0;
    rax = lseek ();
    r13 = rax;
    if (rax < 0) {
        goto label_42;
    }
    if (r12 != rax) {
        edx = 0;
        edi = 0;
        rsi = r12;
        rax = lseek ();
        if (rax < 0) {
            goto label_42;
        }
        if (r13 >= r12) {
            r13 = r12;
            goto label_118;
        }
    }
label_118:
    rax = 0x7fffffffffffffff;
    r13 -= r12;
    r12 = rbp + r13;
    if (r12 != rax) {
        goto label_36;
    }
label_117:
    errno_location ();
    *(rax) = 0x4b;
label_42:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s: cannot determine file size");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_81:
    ebp = 0;
    rax = xnmalloc (*(rsp), 0x20);
    *((rsp + 0x18)) = rax;
    rbx = rax;
    do {
        next_file_name ();
        rbp++;
        rbx += 0x20;
        rax = xstrdup (*(obj.outfile));
        *((rbx - 0x18)) = 0xffffffff;
        *((rbx - 0x20)) = rax;
        *((rbx - 0x10)) = 0;
        *((rbx - 8)) = 0;
    } while (*(rsp) != rbp);
    goto label_37;
label_89:
    *((rsp + 8)) = 0;
    r14 = rbx - 1;
    r12d = 1;
    goto label_38;
label_111:
    dil = (rbp == 0) ? 1 : 0;
    cwrite (0, *((rsp + 0x18)), r15, rcx, r8, r9);
    goto label_39;
label_90:
    rsi = r15;
    edx = 1;
    edi = 0;
    rsi -= r13;
    r13 |= 0xffffffffffffffff;
    rax = lseek ();
    *((rsp + 8)) = r15;
    if (rax >= 0) {
        goto label_38;
    }
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_114:
    rsi = rdx;
    edi = 0;
    edx = 1;
    rsi -= r13;
    r13 |= 0xffffffffffffffff;
    rax = lseek ();
    r14 = *((rsp + 0x48));
    if (rax >= 0) {
        goto label_35;
    }
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_92:
    rax = safe_read (0, *((rsp + 0x48)), *((rsp + 0x20)));
    r13 = rax;
    if (rax != -1) {
        goto label_40;
    }
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    rax = error (1, *(rax), 0x0000a31e);
label_93:
    r13 = rax;
    rbx = r8;
    goto label_41;
label_80:
    xalloc_die ();
label_108:
    rdx = *(r10);
    esi = 3;
    edi = 0;
    *(rsp) = r11;
    rax = quotearg_n_style_colon ();
    r11 = *(rsp);
    rcx = rax;
    eax = 0;
    error (1, *(r11), 0x0000a31e);
label_66:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_65:
    rsi = infile;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot open %s for reading");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_91:
    r12 -= 0xffffffffffffffff;
    if (*((rsp + 0x10)) == 0) {
        goto label_119;
    }
    goto label_21;
    do {
        r12++;
        cwrite (1, 0, 0, rcx, r8, r9);
label_119:
    } while (*(rsp) >= r12);
    goto label_21;
label_113:
    assert_fail ("k && n && k <= n && n <= file_size", "src/split.c", 0x3e4, "bytes_chunk_extract");
label_116:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_82:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_69:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_109:
    rax = errno_location ();
    if (*(rax) != 0x1d) {
        goto label_42;
    }
    *(rax) = 0;
    goto label_42;
label_101:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_79:
    *((rsp + 0x10)) = 1;
    r15d = 0;
    goto label_43;
label_88:
    assert_fail ("n && k <= n && n <= file_size", "src/split.c", 0x363, "lines_chunk_split");
label_57:
    rax = quote (r15, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "multi-character separator %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_58:
    edx = 5;
    rax = dcgettext (0, "multiple separator characters specified");
    eax = 0;
    error (1, 0, rax);
label_56:
    edx = 5;
    rax = dcgettext (0, "empty record separator");
    eax = 0;
    error (1, 0, rax);
label_71:
    rdx = infile;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x0000a31e);
label_59:
    *((rsp + 8)) = r8d;
    rax = umaxtostr (*(rsp), rsp + 0x110, rdx);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "line count option -%s%c... is too large");
    r8d = *((rsp + 8));
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_75:
    if (*((rsp + 8)) == 0) {
        goto label_120;
    }
    if (r13 != 0) {
        goto label_14;
    }
    if (*((rsp + 8)) != 0) {
        goto label_43;
    }
label_120:
    if (rbx != 0) {
        goto label_44;
    }
    goto label_43;
label_106:
    *((rsp + 0x10)) = 1;
    goto label_6;
}

/* /tmp/tmpb028xga7 @ 0x56c0 */
 
int64_t dbg_usage (char * arg_8h, int64_t arg_10h, char * arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, char * arg_60h, int64_t arg_68h, int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE [PREFIX]]\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is 'x'.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with '-n'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            '\\0' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with '-n r/...'\n");
    ecx = 2;
    esi = 1;
    rdi = stdout;
    rdx = rax;
    eax = 0;
    fprintf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --verbose           print a diagnostic just before each\n                            output file is opened\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like 'l' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n");
    rsi = r12;
    r12 = "split";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000a139;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000a1b3;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000a1bd, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000b6d1;
    r12 = 0x0000a155;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000a1bd, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "split";
        printf_chk ();
        r12 = 0x0000a155;
    }
label_5:
    r13 = "split";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpb028xga7 @ 0x7770 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x7e60 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x9a80 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = rsi - 0x400;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7c90 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x2a1f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x7b70 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2a15)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x8390 */
 
uint64_t dbg_safe_write (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_write(int fd, const * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = write (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (eax == 0x16) {
        if (rbx <= 0x7ff00000) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x7810 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x2a0a)() ();
    }
    if (rdx == 0) {
        void (*0x2a0a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x7d20 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000f630]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000f640]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x91d0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x9250 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x7a70 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x8ef0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x82e0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpb028xga7 @ 0x5e30 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x28e0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpb028xga7 @ 0x5ce0 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x77b0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x90d0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpb028xga7 @ 0x8cf0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpb028xga7 @ 0x27b0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpb028xga7 @ 0x2880 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpb028xga7 @ 0x5b80 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpb028xga7 @ 0x9920 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpb028xga7 @ 0x25a0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpb028xga7 @ 0x86e0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000b68b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000b978;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xb978 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2950)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2950)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2950)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpb028xga7 @ 0x8680 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x2710 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpb028xga7 @ 0x8b70 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpb028xga7 @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpb028xga7 @ 0x9230 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x9e20 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpb028xga7 @ 0x8f30 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x92f0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x27e0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpb028xga7 @ 0x2520 */
 
void getenv (void) {
    /* [15] -r-x section size 1248 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpb028xga7 @ 0x2530 */
 
void sigprocmask (void) {
    __asm ("bnd jmp qword [reloc.sigprocmask]");
}

/* /tmp/tmpb028xga7 @ 0x2540 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpb028xga7 @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func fini, func init, func main, char ** ubp_av) {
    rsi = argc;
    r8 = fini;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    edx = 3;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += dil;
    *(0x2800403d) += cl;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *((rdi + 0x18)) += bh;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += bl;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += bl;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) !overflow 0) {
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        if (*(rax) overflow 0) {
            goto label_0;
        }
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += dl;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += eax;
        *(rax) += al;
        eax += 0;
    }
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
label_0:
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *((rcx + 0x7e)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) > 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) ^= rax;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x31)) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmpb028xga7 @ 0x2580 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpb028xga7 @ 0x25b0 */
 
void sigaction (void) {
    __asm ("bnd jmp qword [reloc.sigaction]");
}

/* /tmp/tmpb028xga7 @ 0x25d0 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmpb028xga7 @ 0x25e0 */
 
void setenv (void) {
    __asm ("bnd jmp qword [reloc.setenv]");
}

/* /tmp/tmpb028xga7 @ 0x25f0 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmpb028xga7 @ 0x2600 */
 
void write (void) {
    __asm ("bnd jmp qword [reloc.write]");
}

/* /tmp/tmpb028xga7 @ 0x2610 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpb028xga7 @ 0x2620 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpb028xga7 @ 0x2630 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpb028xga7 @ 0x2650 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpb028xga7 @ 0x2680 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpb028xga7 @ 0x2690 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpb028xga7 @ 0x26a0 */
 
void dup2 (void) {
    __asm ("bnd jmp qword [reloc.dup2]");
}

/* /tmp/tmpb028xga7 @ 0x26b0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpb028xga7 @ 0x26d0 */
 
void ftruncate (void) {
    __asm ("bnd jmp qword [reloc.ftruncate]");
}

/* /tmp/tmpb028xga7 @ 0x26e0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpb028xga7 @ 0x26f0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpb028xga7 @ 0x2700 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpb028xga7 @ 0x2720 */
 
void pipe (void) {
    __asm ("bnd jmp qword [reloc.pipe]");
}

/* /tmp/tmpb028xga7 @ 0x2730 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmpb028xga7 @ 0x2740 */
 
void memchr (void) {
    __asm ("bnd jmp qword [reloc.memchr]");
}

/* /tmp/tmpb028xga7 @ 0x2750 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmpb028xga7 @ 0x2760 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpb028xga7 @ 0x2770 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpb028xga7 @ 0x2780 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmpb028xga7 @ 0x27a0 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpb028xga7 @ 0x27c0 */
 
void sigemptyset (void) {
    __asm ("bnd jmp qword [reloc.sigemptyset]");
}

/* /tmp/tmpb028xga7 @ 0x27d0 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmpb028xga7 @ 0x27e0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpb028xga7 @ 0x27f0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpb028xga7 @ 0x2810 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpb028xga7 @ 0x2830 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpb028xga7 @ 0x2840 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpb028xga7 @ 0x2860 */
 
void fdopen (void) {
    __asm ("bnd jmp qword [reloc.fdopen]");
}

/* /tmp/tmpb028xga7 @ 0x2870 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpb028xga7 @ 0x2890 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmpb028xga7 @ 0x28b0 */
 
void memrchr (void) {
    __asm ("bnd jmp qword [reloc.memrchr]");
}

/* /tmp/tmpb028xga7 @ 0x28c0 */
 
void waitpid (void) {
    __asm ("bnd jmp qword [reloc.waitpid]");
}

/* /tmp/tmpb028xga7 @ 0x28d0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpb028xga7 @ 0x28e0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpb028xga7 @ 0x28f0 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmpb028xga7 @ 0x2900 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpb028xga7 @ 0x2910 */
 
void getpagesize (void) {
    __asm ("bnd jmp qword [reloc.getpagesize]");
}

/* /tmp/tmpb028xga7 @ 0x2930 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpb028xga7 @ 0x2950 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpb028xga7 @ 0x2980 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpb028xga7 @ 0x2990 */
 
void execl (void) {
    __asm ("bnd jmp qword [reloc.execl]");
}

/* /tmp/tmpb028xga7 @ 0x29a0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpb028xga7 @ 0x29b0 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmpb028xga7 @ 0x29c0 */
 
void sigaddset (void) {
    __asm ("bnd jmp qword [reloc.sigaddset]");
}

/* /tmp/tmpb028xga7 @ 0x29d0 */
 
void fork (void) {
    __asm ("bnd jmp qword [reloc.fork]");
}

/* /tmp/tmpb028xga7 @ 0x29e0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpb028xga7 @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1264 named .plt */
    __asm ("bnd jmp qword [0x0000ed58]");
}

/* /tmp/tmpb028xga7 @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24a0 */
 
void fcn_000024a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24b0 */
 
void fcn_000024b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24c0 */
 
void fcn_000024c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24d0 */
 
void fcn_000024d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24e0 */
 
void fcn_000024e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x24f0 */
 
void fcn_000024f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpb028xga7 @ 0x2500 */
 
void fcn_00002500 (void) {
    return __asm ("bnd jmp section..plt");
}
