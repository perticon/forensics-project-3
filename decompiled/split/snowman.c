
void** output_desc = reinterpret_cast<void**>(0xff);

signed char elide_empty_files = 0;

void** outfile = reinterpret_cast<void**>(0);

void** filter_pid = reinterpret_cast<void**>(0);

void closeout(void** rdi, void** esi, void** edx, void** rcx, ...);

void next_file_name();

void** create(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** quotearg_n_style_colon();

void** fun_2560();

void** fun_28a0();

void** full_write(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** filter_command = reinterpret_cast<void**>(0);

void** fun_26e0();

void** safe_read();

uint32_t cwrite(unsigned char dil, void** rsi, void** rdx, ...) {
    int64_t v4;
    int64_t rbx5;
    int64_t rdi6;
    int1_t zf7;
    void** rcx8;
    void** rdx9;
    void** rsi10;
    void** rdi11;
    void** eax12;
    void** rax13;
    void** rax14;
    void** rsi15;
    uint64_t r15_16;
    void** r14_17;
    uint32_t ebp18;
    int32_t ebx19;
    void** v20;
    uint64_t v21;
    uint64_t r8_22;
    void** rax23;
    void** rax24;
    uint32_t eax25;
    int1_t zf26;
    void** rax27;
    int1_t zf28;
    void** rax29;
    void** r12_30;
    unsigned char v31;
    void** rsi32;
    uint64_t r11_33;
    uint32_t eax34;
    uint32_t edi35;
    uint32_t eax36;
    int64_t rbx37;
    uint1_t below_or_equal38;
    uint32_t eax39;
    uint32_t edi40;
    uint32_t eax41;
    int64_t rbx42;
    uint64_t rbx43;
    uint64_t rax44;

    v4 = rbx5;
    if (!dil) {
        *reinterpret_cast<void***>(&rdi6) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        goto addr_543f_3;
    }
    if (reinterpret_cast<unsigned char>(rsi) | reinterpret_cast<unsigned char>(rdx) || (zf7 = elide_empty_files == 0, zf7)) {
        rcx8 = outfile;
        rdx9 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        rsi10 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
        closeout(0, rsi10, rdx9, rcx8);
        next_file_name();
        rdi11 = outfile;
        eax12 = create(rdi11, rsi10, rdx9, rcx8);
        output_desc = eax12;
        *reinterpret_cast<void***>(&rdi6) = eax12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) {
            rax13 = quotearg_n_style_colon();
            rax14 = fun_2560();
            rsi15 = *reinterpret_cast<void***>(rax14);
            *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
            r14_17 = reinterpret_cast<void**>(1);
            ebp18 = 1;
            ebx19 = 1;
            v20 = rsi15;
            v21 = r8_22;
            if (rax13 != 0xffffffffffffffff) 
                goto addr_552a_8;
        } else {
            addr_543f_3:
            rax23 = full_write(rdi6, rsi, rdx, rcx8);
            if (rax23 != rdx) {
                rax24 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax24) == 32) || (eax25 = 0, zf26 = filter_command == 0, zf26)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_11;
                } else {
                    addr_549d_12:
                    return eax25;
                }
            }
        }
    } else {
        addr_5498_11:
        eax25 = 1;
        goto addr_549d_12;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp18)) {
            rax27 = fun_26e0();
            zf28 = reinterpret_cast<int1_t>(rax27 == 0xffffffffffffffff);
            if (!zf28) {
                r14_17 = reinterpret_cast<void**>(1);
            }
            if (!zf28) {
                ebx19 = 1;
            }
        }
        rax29 = safe_read();
        r12_30 = rax29;
        if (rax29 == 0xffffffffffffffff) 
            break;
        v31 = reinterpret_cast<uint1_t>(rax29 == 0);
        addr_5535_21:
        if (reinterpret_cast<unsigned char>(r12_30) < reinterpret_cast<unsigned char>(r14_17)) {
            rsi32 = v20;
        } else {
            rsi32 = v20;
            r11_33 = v21 - 1;
            do {
                eax34 = 1;
                *reinterpret_cast<unsigned char*>(&ebp18) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp18) | *reinterpret_cast<unsigned char*>(&ebx19));
                if (*reinterpret_cast<unsigned char*>(&ebp18)) {
                    edi35 = *reinterpret_cast<unsigned char*>(&ebx19);
                    eax36 = cwrite(*reinterpret_cast<unsigned char*>(&edi35), rsi32, r14_17);
                    r11_33 = r11_33;
                    rsi32 = rsi32;
                    ebp18 = eax36;
                    eax34 = eax36 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx37) = *reinterpret_cast<unsigned char*>(&ebx19);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx37) + 4) = 0;
                r15_16 = r15_16 + rbx37;
                below_or_equal38 = reinterpret_cast<uint1_t>(r15_16 <= r11_33);
                *reinterpret_cast<unsigned char*>(&ebx19) = below_or_equal38;
                if (below_or_equal38) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax34)) 
                    goto addr_5650_28;
                r12_30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_30) - reinterpret_cast<unsigned char>(r14_17));
                rsi32 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi32) + reinterpret_cast<unsigned char>(r14_17));
                r14_17 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_30));
        }
        if (r12_30) {
            eax39 = 1;
            *reinterpret_cast<unsigned char*>(&ebp18) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp18) | *reinterpret_cast<unsigned char*>(&ebx19));
            if (*reinterpret_cast<unsigned char*>(&ebp18)) {
                edi40 = *reinterpret_cast<unsigned char*>(&ebx19);
                eax41 = cwrite(*reinterpret_cast<unsigned char*>(&edi40), rsi32, r12_30);
                ebp18 = eax41;
                eax39 = eax41 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx42) = *reinterpret_cast<unsigned char*>(&ebx19);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx42) + 4) = 0;
            r15_16 = r15_16 + rbx42;
            *reinterpret_cast<unsigned char*>(&ebx19) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v21 == r15_16)) & *reinterpret_cast<unsigned char*>(&eax39));
            if (*reinterpret_cast<unsigned char*>(&ebx19)) 
                goto addr_567c_34;
            r14_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_17) - reinterpret_cast<unsigned char>(r12_30));
        }
    } while (!v31);
    goto addr_5650_28;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_28:
    rbx43 = r15_16 + 1;
    if (v21 > r15_16) {
        do {
            cwrite(1, 0, 0);
            rax44 = rbx43;
            ++rbx43;
        } while (v21 != rax44);
    }
    addr_567c_34:
    goto v4;
    addr_552a_8:
    r12_30 = rax13;
    v31 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_21;
}

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
};

struct s1 {
    void** f0;
    signed char[7] pad8;
    int32_t f8;
    signed char[4] pad16;
    void** f10;
};

int64_t rpl_fclose(void** rdi, ...);

void** open_safer(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_2860(int64_t rdi, int64_t rsi);

void** g28;

int32_t fun_2710();

void** n_open_pipes = reinterpret_cast<void**>(0);

int32_t fun_28c0(int64_t rdi, void* rsi);

void** fun_2640();

void fun_2670();

void*** open_pipes = reinterpret_cast<void***>(0);

int32_t sig2str(int64_t rdi, void** rsi);

void fun_29f0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

unsigned char ofile_open(void** rdi, void** rsi, void** rdx, void** rcx) {
    struct s0* r13_5;
    struct s1* rbp6;
    void* rsp7;
    void** rbx8;
    void** r15_9;
    void** r12_10;
    void** rdi11;
    void** r14_12;
    void** eax13;
    void* rsp14;
    void** rax15;
    void* rsp16;
    void** r8_17;
    void** rax18;
    void** rdi19;
    void** v20;
    int64_t rax21;
    void** eax22;
    int32_t eax23;
    int64_t rdi24;
    int64_t rax25;
    void** rax26;
    void** rax27;
    void** rcx28;
    void** esi29;
    void** eax30;
    void** rbp31;
    void** rbx32;
    void* rsp33;
    void** rax34;
    void** v35;
    int32_t eax36;
    int64_t rax37;
    void** rax38;
    int1_t zf39;
    void** rax40;
    int32_t eax41;
    void** rax42;
    void* rax43;
    int64_t v44;
    void*** rsi45;
    void** rdx46;
    void*** rax47;
    void** rax48;
    void** rcx49;
    void** rax50;
    int32_t eax51;
    int64_t r8_52;
    void** rax53;
    void** v54;
    int64_t rdi55;
    int1_t zf56;
    void** rdx57;
    void** rsi58;
    void** rdi59;
    void** eax60;
    void** rax61;
    void** rax62;
    void** rsi63;
    void** r15_64;
    void** r14_65;
    uint32_t ebp66;
    int32_t ebx67;
    void** v68;
    void** v69;
    void** rax70;
    void** rax71;
    int1_t zf72;
    int64_t v73;
    void** rax74;
    int1_t zf75;
    void** rax76;
    void** r12_77;
    unsigned char v78;
    void** rsi79;
    void** r11_80;
    uint32_t eax81;
    uint32_t edi82;
    uint32_t eax83;
    void* rbx84;
    uint1_t below_or_equal85;
    uint32_t eax86;
    uint32_t edi87;
    uint32_t eax88;
    void* rbx89;
    void** rbx90;
    void** rax91;
    void** edx92;

    r13_5 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi) << 5) + reinterpret_cast<unsigned char>(rdi));
    *reinterpret_cast<int32_t*>(&rbp6) = 0;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    if (reinterpret_cast<signed char>(r13_5->f8) < reinterpret_cast<signed char>(0)) {
        rbx8 = rsi + 0xffffffffffffffff;
        r15_9 = rdx + 0xffffffffffffffff;
        r12_10 = rdi;
        if (!rsi) {
            rbx8 = r15_9;
        }
        rdi11 = r13_5->f0;
        r14_12 = rsi;
        if (r13_5->f8 != 0xffffffff) {
            goto addr_4f78_6;
        }
        while (eax13 = create(rdi11, rsi, rdx, rcx), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) {
            do {
                rax15 = fun_2560();
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
                r8_17 = rax15;
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax15) - 23) > 1) 
                    goto addr_4f9e_9;
                do {
                    rbp6 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx8) << 5) + reinterpret_cast<unsigned char>(r12_10));
                    if (rbp6->f8 >= 0) 
                        break;
                    rax18 = rbx8 + 0xffffffffffffffff;
                    if (!rbx8) {
                        rax18 = r15_9;
                    }
                    rbx8 = rax18;
                } while (rax18 != r14_12);
                goto addr_5050_14;
                rdi19 = rbp6->f10;
                v20 = r8_17;
                rax21 = rpl_fclose(rdi19, rdi19);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax21)) 
                    goto addr_50b6_16;
                rbp6->f8 = -2;
                eax22 = r13_5->f8;
                rbp6->f10 = reinterpret_cast<void**>(0);
                rdi11 = r13_5->f0;
                *reinterpret_cast<int32_t*>(&rbp6) = 1;
                if (reinterpret_cast<int1_t>(eax22 == 0xffffffff)) 
                    break;
                addr_4f78_6:
                *reinterpret_cast<int32_t*>(&rsi) = 0xc01;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                eax13 = open_safer(rdi11, 0xc01, rdx, rcx);
                rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            } while (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0));
            break;
        }
    } else {
        addr_4f42_18:
        eax23 = *reinterpret_cast<int32_t*>(&rbp6);
        return *reinterpret_cast<unsigned char*>(&eax23);
    }
    r13_5->f8 = eax13;
    *reinterpret_cast<void***>(&rdi24) = eax13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    rax25 = fun_2860(rdi24, "a");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    r13_5->f10 = rax25;
    if (!rax25) {
        addr_50e4_20:
        rax26 = quotearg_n_style_colon();
        rax27 = fun_2560();
        rcx28 = rax26;
        esi29 = *reinterpret_cast<void***>(rax27);
        fun_28a0();
    } else {
        eax30 = filter_pid;
        filter_pid = reinterpret_cast<void**>(0);
        r13_5->f18 = eax30;
        goto addr_4f42_18;
    }
    *reinterpret_cast<uint32_t*>(&rbp31) = reinterpret_cast<uint32_t>("%s");
    *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
    rbx32 = esi29;
    *reinterpret_cast<int32_t*>(&rbx32 + 4) = 0;
    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 72);
    rax34 = g28;
    v35 = rax34;
    if (0) {
        if (reinterpret_cast<signed char>(esi29) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_24;
        eax36 = fun_2710();
        rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
        if (eax36 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_27;
        }
    } else {
        rax37 = rpl_fclose(1);
        rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax37) && ((rax38 = fun_2560(), rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8), zf39 = filter_command == 0, zf39) || *reinterpret_cast<void***>(rax38) != 32)) {
            rax40 = quotearg_n_style_colon();
            rcx28 = rax40;
            fun_28a0();
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx32) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_24;
    }
    rcx28 = n_open_pipes;
    if (!rcx28) {
        addr_51e1_24:
        if (1) {
            addr_5250_27:
            eax41 = fun_28c0("%s", reinterpret_cast<int64_t>(rsp33) + 28);
            rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
            if (eax41 == -1 && (rax42 = fun_2560(), rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8), rbx32 = rax42, *reinterpret_cast<void***>(rax42) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_33;
            }
        } else {
            addr_51e5_34:
            rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v35) - reinterpret_cast<unsigned char>(g28));
            if (rax43) {
                fun_2670();
                goto addr_53b0_36;
            } else {
                goto v44;
            }
        }
    } else {
        rsi45 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx46) = 0;
        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
        rax47 = rsi45;
        do {
            if (*rax47 == rbx32) 
                goto addr_51d1_40;
            ++rdx46;
            rax47 = rax47 + 4;
        } while (rdx46 != rcx28);
        goto addr_51e1_24;
    }
    *reinterpret_cast<int32_t*>(&rcx28) = 0;
    rbx32 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx32 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_36:
            rax48 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx49) = *reinterpret_cast<int32_t*>(&rcx28);
            *reinterpret_cast<int32_t*>(&rcx49 + 4) = 0;
            fun_28a0();
        } else {
            *reinterpret_cast<uint32_t*>(&rbp31) = *reinterpret_cast<unsigned char*>(&rcx28 + 1);
            *reinterpret_cast<unsigned char*>(&rcx28 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx28 + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx28 + 1)) {
                rbx32 = filter_command;
                rax50 = quotearg_n_style_colon();
                fun_2640();
                *reinterpret_cast<uint32_t*>(&r8_17) = *reinterpret_cast<uint32_t*>(&rbp31);
                *reinterpret_cast<int32_t*>(&r8_17 + 4) = 0;
                rcx28 = rax50;
                fun_28a0();
                goto addr_51e5_34;
            }
        }
    } else {
        if (!0) {
            rbp31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp33) + 32);
            eax51 = sig2str(0, rbp31);
            if (eax51) {
                addr_5388_33:
                *reinterpret_cast<void***>(&r8_52) = rbx32;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_52) + 4) = 0;
                fun_29f0(rbp31, 1, 19, "%d", r8_52);
                goto addr_529f_48;
            } else {
                addr_529f_48:
                rax53 = quotearg_n_style_colon();
                fun_2640();
                r8_17 = rbp31;
                rcx28 = rax53;
                fun_28a0();
                goto addr_51e5_34;
            }
        }
    }
    v54 = rbx32;
    if (0) {
        *reinterpret_cast<void***>(&rdi55) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
        goto addr_543f_51;
    }
    if (rax48 || (zf56 = elide_empty_files == 0, zf56)) {
        rcx49 = outfile;
        rdx57 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
        rsi58 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
        closeout(0, rsi58, rdx57, rcx49, 0, rsi58, rdx57, rcx49);
        next_file_name();
        rdi59 = outfile;
        eax60 = create(rdi59, rsi58, rdx57, rcx49, rdi59, rsi58, rdx57, rcx49);
        output_desc = eax60;
        *reinterpret_cast<void***>(&rdi55) = eax60;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
        if (reinterpret_cast<signed char>(eax60) < reinterpret_cast<signed char>(0)) {
            rax61 = quotearg_n_style_colon();
            rax62 = fun_2560();
            rsi63 = *reinterpret_cast<void***>(rax62);
            *reinterpret_cast<int32_t*>(&rsi63 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_64) = 0;
            *reinterpret_cast<int32_t*>(&r15_64 + 4) = 0;
            r14_65 = reinterpret_cast<void**>(1);
            ebp66 = 1;
            ebx67 = 1;
            v68 = rsi63;
            v69 = r8_17;
            if (rax61 != 0xffffffffffffffff) 
                goto addr_552a_56;
        } else {
            addr_543f_51:
            rax70 = full_write(rdi55, 0, rax48, rcx49, rdi55, 0, rax48, rcx49);
            if (rax70 != rax48) {
                rax71 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax71) == 32) || (zf72 = filter_command == 0, zf72)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_59;
                } else {
                    addr_549d_60:
                    goto v73;
                }
            }
        }
    } else {
        addr_5498_59:
        goto addr_549d_60;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp66)) {
            rax74 = fun_26e0();
            zf75 = reinterpret_cast<int1_t>(rax74 == 0xffffffffffffffff);
            if (!zf75) {
                r14_65 = reinterpret_cast<void**>(1);
            }
            if (!zf75) {
                ebx67 = 1;
            }
        }
        rax76 = safe_read();
        r12_77 = rax76;
        if (rax76 == 0xffffffffffffffff) 
            break;
        v78 = reinterpret_cast<uint1_t>(rax76 == 0);
        addr_5535_69:
        if (reinterpret_cast<unsigned char>(r12_77) < reinterpret_cast<unsigned char>(r14_65)) {
            rsi79 = v68;
        } else {
            rsi79 = v68;
            r11_80 = v69 + 0xffffffffffffffff;
            do {
                eax81 = 1;
                *reinterpret_cast<unsigned char*>(&ebp66) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp66) | *reinterpret_cast<unsigned char*>(&ebx67));
                if (*reinterpret_cast<unsigned char*>(&ebp66)) {
                    edi82 = *reinterpret_cast<unsigned char*>(&ebx67);
                    eax83 = cwrite(*reinterpret_cast<unsigned char*>(&edi82), rsi79, r14_65, *reinterpret_cast<unsigned char*>(&edi82), rsi79, r14_65);
                    r11_80 = r11_80;
                    rsi79 = rsi79;
                    ebp66 = eax83;
                    eax81 = eax83 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx84) = *reinterpret_cast<unsigned char*>(&ebx67);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx84) + 4) = 0;
                r15_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_64) + reinterpret_cast<uint64_t>(rbx84));
                below_or_equal85 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_64) <= reinterpret_cast<unsigned char>(r11_80));
                *reinterpret_cast<unsigned char*>(&ebx67) = below_or_equal85;
                if (below_or_equal85) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax81)) 
                    goto addr_5650_76;
                r12_77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_77) - reinterpret_cast<unsigned char>(r14_65));
                rsi79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi79) + reinterpret_cast<unsigned char>(r14_65));
                r14_65 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_77));
        }
        if (r12_77) {
            eax86 = 1;
            *reinterpret_cast<unsigned char*>(&ebp66) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp66) | *reinterpret_cast<unsigned char*>(&ebx67));
            if (*reinterpret_cast<unsigned char*>(&ebp66)) {
                edi87 = *reinterpret_cast<unsigned char*>(&ebx67);
                eax88 = cwrite(*reinterpret_cast<unsigned char*>(&edi87), rsi79, r12_77, *reinterpret_cast<unsigned char*>(&edi87), rsi79, r12_77);
                ebp66 = eax88;
                eax86 = eax88 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx89) = *reinterpret_cast<unsigned char*>(&ebx67);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx89) + 4) = 0;
            r15_64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_64) + reinterpret_cast<uint64_t>(rbx89));
            *reinterpret_cast<unsigned char*>(&ebx67) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v69 == r15_64)) & *reinterpret_cast<unsigned char*>(&eax86));
            if (*reinterpret_cast<unsigned char*>(&ebx67)) 
                goto addr_567c_82;
            r14_65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_65) - reinterpret_cast<unsigned char>(r12_77));
        }
    } while (!v78);
    goto addr_5650_76;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_76:
    rbx90 = r15_64 + 1;
    if (reinterpret_cast<unsigned char>(v69) > reinterpret_cast<unsigned char>(r15_64)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax91 = rbx90;
            ++rbx90;
        } while (v69 != rax91);
    }
    addr_567c_82:
    goto v54;
    addr_552a_56:
    r12_77 = rax61;
    v78 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax61) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_69;
    addr_51d1_40:
    --rcx28;
    edx92 = rsi45[reinterpret_cast<unsigned char>(rcx28) * 4];
    n_open_pipes = rcx28;
    *rax47 = edx92;
    goto addr_51e1_24;
    addr_4f9e_9:
    addr_5083_89:
    v20 = r8_17;
    quotearg_n_style_colon();
    fun_28a0();
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
    addr_50b6_16:
    quotearg_n_style_colon();
    r8_17 = v20;
    fun_28a0();
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    goto addr_50e4_20;
    addr_5050_14:
    quotearg_n_style_colon();
    r8_17 = r8_17;
    fun_28a0();
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
    goto addr_5083_89;
}

void bytes_split(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** r14_7;
    void** r13_8;
    uint32_t ebp9;
    int32_t ebx10;
    void** v11;
    void** v12;
    void** r12_13;
    unsigned char v14;
    void** rax15;
    int1_t zf16;
    void** rax17;
    void** rsi18;
    void** r11_19;
    uint32_t eax20;
    uint32_t edi21;
    uint32_t eax22;
    void* rbx23;
    uint1_t below_or_equal24;
    uint32_t eax25;
    uint32_t edi26;
    uint32_t eax27;
    void* rbx28;
    void** rbx29;
    void** rax30;

    *reinterpret_cast<int32_t*>(&r15_6) = 0;
    *reinterpret_cast<int32_t*>(&r15_6 + 4) = 0;
    r14_7 = rdi;
    r13_8 = rdi;
    ebp9 = 1;
    ebx10 = 1;
    v11 = rsi;
    v12 = r8;
    if (rcx != 0xffffffffffffffff) {
        r12_13 = rcx;
        v14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(rdx));
        goto addr_5535_3;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp9)) {
            rax15 = fun_26e0();
            zf16 = reinterpret_cast<int1_t>(rax15 == 0xffffffffffffffff);
            if (!zf16) {
                r14_7 = r13_8;
            }
            if (!zf16) {
                ebx10 = 1;
            }
        }
        rax17 = safe_read();
        r12_13 = rax17;
        if (rax17 == 0xffffffffffffffff) 
            break;
        v14 = reinterpret_cast<uint1_t>(rax17 == 0);
        addr_5535_3:
        if (reinterpret_cast<unsigned char>(r12_13) < reinterpret_cast<unsigned char>(r14_7)) {
            rsi18 = v11;
        } else {
            rsi18 = v11;
            r11_19 = v12 + 0xffffffffffffffff;
            do {
                eax20 = 1;
                *reinterpret_cast<unsigned char*>(&ebp9) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp9) | *reinterpret_cast<unsigned char*>(&ebx10));
                if (*reinterpret_cast<unsigned char*>(&ebp9)) {
                    edi21 = *reinterpret_cast<unsigned char*>(&ebx10);
                    eax22 = cwrite(*reinterpret_cast<unsigned char*>(&edi21), rsi18, r14_7);
                    r11_19 = r11_19;
                    rsi18 = rsi18;
                    ebp9 = eax22;
                    eax20 = eax22 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx23) = *reinterpret_cast<unsigned char*>(&ebx10);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx23) + 4) = 0;
                r15_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_6) + reinterpret_cast<uint64_t>(rbx23));
                below_or_equal24 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_6) <= reinterpret_cast<unsigned char>(r11_19));
                *reinterpret_cast<unsigned char*>(&ebx10) = below_or_equal24;
                if (below_or_equal24) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax20)) 
                    goto addr_5650_18;
                r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) - reinterpret_cast<unsigned char>(r14_7));
                rsi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi18) + reinterpret_cast<unsigned char>(r14_7));
                r14_7 = r13_8;
            } while (reinterpret_cast<unsigned char>(r13_8) <= reinterpret_cast<unsigned char>(r12_13));
        }
        if (r12_13) {
            eax25 = 1;
            *reinterpret_cast<unsigned char*>(&ebp9) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp9) | *reinterpret_cast<unsigned char*>(&ebx10));
            if (*reinterpret_cast<unsigned char*>(&ebp9)) {
                edi26 = *reinterpret_cast<unsigned char*>(&ebx10);
                eax27 = cwrite(*reinterpret_cast<unsigned char*>(&edi26), rsi18, r12_13);
                ebp9 = eax27;
                eax25 = eax27 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx28) = *reinterpret_cast<unsigned char*>(&ebx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx28) + 4) = 0;
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_6) + reinterpret_cast<uint64_t>(rbx28));
            *reinterpret_cast<unsigned char*>(&ebx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v12 == r15_6)) & *reinterpret_cast<unsigned char*>(&eax25));
            if (*reinterpret_cast<unsigned char*>(&ebx10)) 
                goto addr_567c_24;
            r14_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_7) - reinterpret_cast<unsigned char>(r12_13));
        }
    } while (!v14);
    goto addr_5650_18;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_18:
    rbx29 = r15_6 + 1;
    if (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(r15_6)) {
        do {
            cwrite(1, 0, 0);
            rax30 = rbx29;
            ++rbx29;
        } while (v12 != rax30);
    }
    addr_567c_24:
    return;
}

void closeout(void** rdi, void** esi, void** edx, void** rcx, ...) {
    void** rbp5;
    void** rbx6;
    void* rsp7;
    void** rax8;
    void** v9;
    int32_t eax10;
    int64_t rax11;
    void** rax12;
    int1_t zf13;
    void** rax14;
    int64_t rdi15;
    int32_t eax16;
    void** rax17;
    void* rax18;
    void*** rsi19;
    void** rdx20;
    void*** rax21;
    void** rax22;
    void** rcx23;
    void** rax24;
    void** r8_25;
    int32_t eax26;
    int64_t r8_27;
    void** rax28;
    void** v29;
    int64_t rdi30;
    int1_t zf31;
    void** rdx32;
    void** rsi33;
    void** rdi34;
    void** eax35;
    void** rax36;
    void** rax37;
    void** rsi38;
    void** r15_39;
    void** r14_40;
    uint32_t ebp41;
    int32_t ebx42;
    void** v43;
    void** v44;
    void** rax45;
    void** rax46;
    int1_t zf47;
    int64_t v48;
    void** rax49;
    int1_t zf50;
    void** rax51;
    void** r12_52;
    unsigned char v53;
    void** rsi54;
    void** r11_55;
    uint32_t eax56;
    uint32_t edi57;
    uint32_t eax58;
    void* rbx59;
    uint1_t below_or_equal60;
    uint32_t eax61;
    uint32_t edi62;
    uint32_t eax63;
    void* rbx64;
    void** rbx65;
    void** rax66;
    void** edx67;

    rbp5 = edx;
    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
    rbx6 = esi;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 72);
    rax8 = g28;
    v9 = rax8;
    if (!rdi) {
        if (reinterpret_cast<signed char>(esi) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_3;
        eax10 = fun_2710();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (eax10 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_6;
        }
    } else {
        rax11 = rpl_fclose(rdi);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax11) && ((rax12 = fun_2560(), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), zf13 = filter_command == 0, zf13) || *reinterpret_cast<void***>(rax12) != 32)) {
            rax14 = quotearg_n_style_colon();
            rcx = rax14;
            fun_28a0();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx6) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_3;
    }
    rcx = n_open_pipes;
    if (!rcx) {
        addr_51e1_3:
        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbp5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbp5 == 0))) {
            addr_5250_6:
            *reinterpret_cast<void***>(&rdi15) = rbp5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
            eax16 = fun_28c0(rdi15, reinterpret_cast<int64_t>(rsp7) + 28);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            if (eax16 == -1 && (rax17 = fun_2560(), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax17, *reinterpret_cast<void***>(rax17) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_12;
            }
        } else {
            addr_51e5_13:
            rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
            if (rax18) {
                fun_2670();
                goto addr_53b0_15;
            } else {
                return;
            }
        }
    } else {
        rsi19 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx20) = 0;
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        rax21 = rsi19;
        do {
            if (*rax21 == rbx6) 
                goto addr_51d1_19;
            ++rdx20;
            rax21 = rax21 + 4;
        } while (rdx20 != rcx);
        goto addr_51e1_3;
    }
    *reinterpret_cast<int32_t*>(&rcx) = 0;
    rbx6 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_15:
            rax22 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx23) = *reinterpret_cast<int32_t*>(&rcx);
            *reinterpret_cast<int32_t*>(&rcx23 + 4) = 0;
            fun_28a0();
        } else {
            rbp5 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rcx + 1)));
            *reinterpret_cast<unsigned char*>(&rcx + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx + 1)) {
                rbx6 = filter_command;
                rax24 = quotearg_n_style_colon();
                fun_2640();
                r8_25 = rbp5;
                *reinterpret_cast<int32_t*>(&r8_25 + 4) = 0;
                rcx = rax24;
                fun_28a0();
                goto addr_51e5_13;
            }
        }
    } else {
        if (!0) {
            rbp5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
            eax26 = sig2str(0, rbp5);
            if (eax26) {
                addr_5388_12:
                *reinterpret_cast<void***>(&r8_27) = rbx6;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_27) + 4) = 0;
                fun_29f0(rbp5, 1, 19, "%d", r8_27);
                goto addr_529f_27;
            } else {
                addr_529f_27:
                rax28 = quotearg_n_style_colon();
                fun_2640();
                r8_25 = rbp5;
                rcx = rax28;
                fun_28a0();
                goto addr_51e5_13;
            }
        }
    }
    v29 = rbx6;
    if (0) {
        *reinterpret_cast<void***>(&rdi30) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
        goto addr_543f_30;
    }
    if (rax22 || (zf31 = elide_empty_files == 0, zf31)) {
        rcx23 = outfile;
        rdx32 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx32 + 4) = 0;
        rsi33 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
        closeout(0, rsi33, rdx32, rcx23, 0, rsi33, rdx32, rcx23);
        next_file_name();
        rdi34 = outfile;
        eax35 = create(rdi34, rsi33, rdx32, rcx23, rdi34, rsi33, rdx32, rcx23);
        output_desc = eax35;
        *reinterpret_cast<void***>(&rdi30) = eax35;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
        if (reinterpret_cast<signed char>(eax35) < reinterpret_cast<signed char>(0)) {
            rax36 = quotearg_n_style_colon();
            rax37 = fun_2560();
            rsi38 = *reinterpret_cast<void***>(rax37);
            *reinterpret_cast<int32_t*>(&rsi38 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_39) = 0;
            *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
            r14_40 = reinterpret_cast<void**>(1);
            ebp41 = 1;
            ebx42 = 1;
            v43 = rsi38;
            v44 = r8_25;
            if (rax36 != 0xffffffffffffffff) 
                goto addr_552a_35;
        } else {
            addr_543f_30:
            rax45 = full_write(rdi30, 0, rax22, rcx23, rdi30, 0, rax22, rcx23);
            if (rax45 != rax22) {
                rax46 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax46) == 32) || (zf47 = filter_command == 0, zf47)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_38;
                } else {
                    addr_549d_39:
                    goto v48;
                }
            }
        }
    } else {
        addr_5498_38:
        goto addr_549d_39;
    }
    do {
        if (!*reinterpret_cast<unsigned char*>(&ebp41)) {
            rax49 = fun_26e0();
            zf50 = reinterpret_cast<int1_t>(rax49 == 0xffffffffffffffff);
            if (!zf50) {
                r14_40 = reinterpret_cast<void**>(1);
            }
            if (!zf50) {
                ebx42 = 1;
            }
        }
        rax51 = safe_read();
        r12_52 = rax51;
        if (rax51 == 0xffffffffffffffff) 
            break;
        v53 = reinterpret_cast<uint1_t>(rax51 == 0);
        addr_5535_48:
        if (reinterpret_cast<unsigned char>(r12_52) < reinterpret_cast<unsigned char>(r14_40)) {
            rsi54 = v43;
        } else {
            rsi54 = v43;
            r11_55 = v44 + 0xffffffffffffffff;
            do {
                eax56 = 1;
                *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx42));
                if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                    edi57 = *reinterpret_cast<unsigned char*>(&ebx42);
                    eax58 = cwrite(*reinterpret_cast<unsigned char*>(&edi57), rsi54, r14_40, *reinterpret_cast<unsigned char*>(&edi57), rsi54, r14_40);
                    r11_55 = r11_55;
                    rsi54 = rsi54;
                    ebp41 = eax58;
                    eax56 = eax58 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx59) = *reinterpret_cast<unsigned char*>(&ebx42);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx59) + 4) = 0;
                r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<uint64_t>(rbx59));
                below_or_equal60 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_39) <= reinterpret_cast<unsigned char>(r11_55));
                *reinterpret_cast<unsigned char*>(&ebx42) = below_or_equal60;
                if (below_or_equal60) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax56)) 
                    goto addr_5650_55;
                r12_52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_52) - reinterpret_cast<unsigned char>(r14_40));
                rsi54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi54) + reinterpret_cast<unsigned char>(r14_40));
                r14_40 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_52));
        }
        if (r12_52) {
            eax61 = 1;
            *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx42));
            if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                edi62 = *reinterpret_cast<unsigned char*>(&ebx42);
                eax63 = cwrite(*reinterpret_cast<unsigned char*>(&edi62), rsi54, r12_52, *reinterpret_cast<unsigned char*>(&edi62), rsi54, r12_52);
                ebp41 = eax63;
                eax61 = eax63 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx64) = *reinterpret_cast<unsigned char*>(&ebx42);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx64) + 4) = 0;
            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<uint64_t>(rbx64));
            *reinterpret_cast<unsigned char*>(&ebx42) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v44 == r15_39)) & *reinterpret_cast<unsigned char*>(&eax61));
            if (*reinterpret_cast<unsigned char*>(&ebx42)) 
                goto addr_567c_61;
            r14_40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_40) - reinterpret_cast<unsigned char>(r12_52));
        }
    } while (!v53);
    goto addr_5650_55;
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_5650_55:
    rbx65 = r15_39 + 1;
    if (reinterpret_cast<unsigned char>(v44) > reinterpret_cast<unsigned char>(r15_39)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax66 = rbx65;
            ++rbx65;
        } while (v44 != rax66);
    }
    addr_567c_61:
    goto v29;
    addr_552a_35:
    r12_52 = rax36;
    v53 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax36) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_48;
    addr_51d1_19:
    --rcx;
    edx67 = rsi19[reinterpret_cast<unsigned char>(rcx) * 4];
    n_open_pipes = rcx;
    *rax21 = edx67;
    goto addr_51e1_3;
}

void** fun_2520(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_25e0(int64_t rdi, void** rsi, void** rdx, void** rcx);

signed char verbose = 0;

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** stdout = reinterpret_cast<void**>(0);

void fun_2950(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

int32_t fun_29b0();

int64_t gf308 = 0;

int64_t in_stat_buf = 0;

int32_t fun_26d0(int64_t rdi);

int32_t fun_2720(void* rdi, void** rsi, void** rdx, void** rcx);

void** fun_29d0(void* rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_26a0();

void fun_2530();

void** last_component(void** rdi, int64_t rsi);

void fun_2990(void** rdi, void** rsi, int64_t rdx, void** rcx);

void** open_pipes_alloc = reinterpret_cast<void**>(0);

void*** x2nrealloc();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
};

struct s3 {
    signed char[8] pad8;
    int32_t f8;
    signed char[4] pad16;
    void** f10;
};

void** create(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void** r12_5;
    void* rsp6;
    void** rax7;
    void** v8;
    int1_t zf9;
    void** rax10;
    void** rdx11;
    void** rsi12;
    void** r13_13;
    int32_t eax14;
    void* rsp15;
    int1_t zf16;
    void* rsp17;
    int32_t* rsp18;
    void* rsp19;
    void** rax20;
    void** rdx21;
    int64_t* rsp22;
    void** rax23;
    void** rax24;
    void** rax25;
    void* rsp26;
    void* rsp27;
    void** r8_28;
    void* rsp29;
    void** v30;
    void* rsp31;
    void** v32;
    void** r12_33;
    void** rax34;
    unsigned char v35;
    void** rcx36;
    void** edx37;
    void*** rsi38;
    void*** rax39;
    void** rax40;
    uint32_t ebp41;
    void** rax42;
    int1_t zf43;
    void** r14_44;
    int32_t ebx45;
    void** rax46;
    void** rsi47;
    void** v48;
    void** r11_49;
    void** v50;
    uint32_t eax51;
    uint32_t edi52;
    uint32_t eax53;
    void* rbx54;
    void** r15_55;
    uint1_t below_or_equal56;
    uint32_t eax57;
    uint32_t edi58;
    uint32_t eax59;
    void* rbx60;
    void** rbx61;
    void** rax62;
    void** rax63;
    void** rax64;
    void** rdi65;
    void** eax66;
    void** r13d67;
    int32_t eax68;
    int1_t zf69;
    int64_t v70;
    int1_t zf71;
    int64_t v72;
    int64_t rdi73;
    int32_t eax74;
    uint32_t v75;
    void** rax76;
    void** rax77;
    void** rax78;
    void** rdi79;
    int32_t eax80;
    void** eax81;
    void** rbx82;
    int1_t zf83;
    int32_t eax84;
    int1_t below_or_equal85;
    int32_t eax86;
    int32_t v87;
    int32_t eax88;
    int32_t eax89;
    void** rax90;
    void** rax91;
    int1_t zf92;
    void** rax93;
    void* rax94;
    int32_t eax95;
    void** rax96;
    void** rax97;
    void** rdx98;
    void** rsi99;
    void** rdx100;
    int1_t zf101;
    void*** rdi102;
    void*** rax103;
    void** v104;
    void** v105;
    struct s2* r13_106;
    void** rbx107;
    void** r15_108;
    void** rdi109;
    void** r14_110;
    void** eax111;
    void* rsp112;
    void** rax113;
    struct s3* rbp114;
    void** rax115;
    void** rdi116;
    int64_t rax117;
    void** eax118;
    int64_t v119;
    int64_t rdi120;
    int64_t rax121;
    void** rax122;
    void** rax123;
    void** esi124;
    void** eax125;
    void** rbp126;
    void** rbx127;
    void* rsp128;
    void** rax129;
    void** v130;
    int32_t eax131;
    int64_t rax132;
    void** rax133;
    int1_t zf134;
    void** rax135;
    int32_t eax136;
    void** rax137;
    void* rax138;
    int64_t v139;
    void** rdx140;
    void** rax141;
    void** rcx142;
    void** rax143;
    int32_t eax144;
    int64_t r8_145;
    void** rax146;
    int64_t rdi147;
    int1_t zf148;
    void** rdx149;
    void** rsi150;
    void** rdi151;
    void** eax152;
    void** rax153;
    void** rsi154;
    void** rax155;
    void** rax156;
    int1_t zf157;
    int64_t v158;

    r12_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0xa0);
    rax7 = g28;
    v8 = rax7;
    zf9 = filter_command == 0;
    if (zf9) 
        goto addr_4bd0_2;
    rax10 = fun_2520("SHELL", rsi, rdx, rcx);
    *reinterpret_cast<int32_t*>(&rdx11) = 1;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    rsi12 = r12_5;
    r13_13 = rax10;
    if (!rax10) {
        r13_13 = reinterpret_cast<void**>("/bin/sh");
    }
    eax14 = fun_25e0("FILE", rsi12, 1, rcx);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
    if (!eax14) {
        zf16 = verbose == 0;
        if (!zf16) 
            goto addr_4c98_7; else 
            goto addr_4a56_8;
    }
    addr_4e92_9:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4ebe_10;
    addr_4da3_11:
    fun_2670();
    rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    addr_4da8_12:
    rax20 = fun_2640();
    rdx21 = rax20;
    fun_28a0();
    rsp22 = reinterpret_cast<int64_t*>(rsp18 - 2 + 2 - 2 + 2);
    addr_4dcc_13:
    rax23 = quotearg_style(4, r12_5, rdx21, rcx);
    r12_5 = rax23;
    rax24 = fun_2640();
    rcx = r12_5;
    rdx21 = rax24;
    fun_28a0();
    rsp22 = rsp22 - 1 + 1 - 1 + 1 - 1 + 1;
    addr_4e03_14:
    rax25 = quotearg_style(4, r12_5, rdx21, rcx);
    fun_2640();
    fun_2560();
    rcx = rax25;
    fun_28a0();
    rsp26 = reinterpret_cast<void*>(rsp22 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1);
    addr_4e42_15:
    fun_2640();
    fun_28a0();
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
    addr_4e66_16:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4e92_9;
    addr_5050_17:
    quotearg_n_style_colon();
    r8_28 = r8_28;
    fun_28a0();
    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8 - 8 + 8);
    addr_5083_18:
    v30 = r8_28;
    quotearg_n_style_colon();
    fun_28a0();
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8 - 8 + 8);
    addr_50b6_19:
    quotearg_n_style_colon();
    r8_28 = v30;
    fun_28a0();
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
    goto addr_50e4_20;
    addr_568b_21:
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_567c_23:
    goto v32;
    addr_552a_24:
    r12_33 = rax34;
    v35 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax34) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_25;
    addr_51d1_26:
    --rcx36;
    edx37 = rsi38[reinterpret_cast<unsigned char>(rcx36) * 4];
    n_open_pipes = rcx36;
    *rax39 = edx37;
    goto addr_51e1_27;
    addr_4f9e_28:
    goto addr_5083_18;
    addr_4d77_29:
    rax40 = fun_2640();
    r12_5 = rax40;
    fun_2560();
    fun_28a0();
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4da3_11;
    do {
        addr_55df_30:
        if (!*reinterpret_cast<unsigned char*>(&ebp41)) {
            rax42 = fun_26e0();
            zf43 = reinterpret_cast<int1_t>(rax42 == 0xffffffffffffffff);
            if (!zf43) {
                r14_44 = reinterpret_cast<void**>(1);
            }
            if (!zf43) {
                ebx45 = 1;
            }
        }
        rax46 = safe_read();
        r12_33 = rax46;
        if (rax46 == 0xffffffffffffffff) 
            goto addr_568b_21;
        v35 = reinterpret_cast<uint1_t>(rax46 == 0);
        addr_5535_25:
        if (reinterpret_cast<unsigned char>(r12_33) < reinterpret_cast<unsigned char>(r14_44)) {
            rsi47 = v48;
        } else {
            rsi47 = v48;
            r11_49 = v50 + 0xffffffffffffffff;
            do {
                eax51 = 1;
                *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx45));
                if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                    edi52 = *reinterpret_cast<unsigned char*>(&ebx45);
                    eax53 = cwrite(*reinterpret_cast<unsigned char*>(&edi52), rsi47, r14_44, *reinterpret_cast<unsigned char*>(&edi52), rsi47, r14_44);
                    r11_49 = r11_49;
                    rsi47 = rsi47;
                    ebp41 = eax53;
                    eax51 = eax53 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx54) = *reinterpret_cast<unsigned char*>(&ebx45);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx54) + 4) = 0;
                r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_55) + reinterpret_cast<uint64_t>(rbx54));
                below_or_equal56 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_55) <= reinterpret_cast<unsigned char>(r11_49));
                *reinterpret_cast<unsigned char*>(&ebx45) = below_or_equal56;
                if (below_or_equal56) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax51)) 
                    goto addr_5650_44;
                r12_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_33) - reinterpret_cast<unsigned char>(r14_44));
                rsi47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi47) + reinterpret_cast<unsigned char>(r14_44));
                r14_44 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_33));
        }
        if (r12_33) {
            eax57 = 1;
            *reinterpret_cast<unsigned char*>(&ebp41) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp41) | *reinterpret_cast<unsigned char*>(&ebx45));
            if (*reinterpret_cast<unsigned char*>(&ebp41)) {
                edi58 = *reinterpret_cast<unsigned char*>(&ebx45);
                eax59 = cwrite(*reinterpret_cast<unsigned char*>(&edi58), rsi47, r12_33, *reinterpret_cast<unsigned char*>(&edi58), rsi47, r12_33);
                ebp41 = eax59;
                eax57 = eax59 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx60) = *reinterpret_cast<unsigned char*>(&ebx45);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx60) + 4) = 0;
            r15_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_55) + reinterpret_cast<uint64_t>(rbx60));
            *reinterpret_cast<unsigned char*>(&ebx45) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v50 == r15_55)) & *reinterpret_cast<unsigned char*>(&eax57));
            if (*reinterpret_cast<unsigned char*>(&ebx45)) 
                goto addr_567c_23;
            r14_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_44) - reinterpret_cast<unsigned char>(r12_33));
        }
    } while (!v35);
    addr_5650_44:
    rbx61 = r15_55 + 1;
    if (reinterpret_cast<unsigned char>(v50) > reinterpret_cast<unsigned char>(r15_55)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax62 = rbx61;
            ++rbx61;
        } while (v50 != rax62);
        goto addr_567c_23;
    }
    while (1) {
        addr_4d08_54:
        rax63 = quotearg_style(4, rdi, rdx, rcx);
        rax64 = fun_2640();
        rdi65 = stdout;
        rcx = rax63;
        fun_2950(rdi65, 1, rax64, rcx, r8_28);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8 - 8 + 8);
        while (1) {
            *reinterpret_cast<int32_t*>(&rdx21) = 0x1b6;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            eax66 = open_safer(r12_5, 65, 0x1b6, rcx);
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            r13d67 = eax66;
            if (reinterpret_cast<signed char>(eax66) < reinterpret_cast<signed char>(0)) 
                goto addr_4ac2_56;
            eax68 = fun_29b0();
            rsp22 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            if (eax68) 
                goto addr_4e03_14;
            zf69 = gf308 == v70;
            if (!zf69) 
                goto addr_4c2d_59;
            zf71 = in_stat_buf == v72;
            if (zf71) 
                goto addr_4dcc_13;
            addr_4c2d_59:
            *reinterpret_cast<void***>(&rdi73) = r13d67;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
            eax74 = fun_26d0(rdi73);
            rsp19 = reinterpret_cast<void*>(rsp22 - 1 + 1);
            if (!eax74) 
                goto addr_4ac2_56;
            if ((v75 & 0xf000) != 0x8000) 
                goto addr_4ac2_56;
            rax76 = quotearg_n_style_colon();
            r13_13 = rax76;
            fun_2640();
            fun_2560();
            fun_28a0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4c98_7:
            rax77 = quotearg_n_style_colon();
            r12_5 = rax77;
            rax78 = fun_2640();
            rdi79 = stdout;
            rcx = r12_5;
            *reinterpret_cast<int32_t*>(&rsi12) = 1;
            *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
            rdx11 = rax78;
            fun_2950(rdi79, 1, rdx11, rcx, r8_28);
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4a56_8:
            eax80 = fun_2720(rsp15, rsi12, rdx11, rcx);
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
            if (eax80) 
                goto addr_4e66_16;
            eax81 = fun_29d0(rsp15, rsi12, rdx11, rcx);
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
            if (eax81) 
                goto addr_4a71_64;
            rbx82 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rbx82 + 4) = 0;
            zf83 = n_open_pipes == 0;
            if (!zf83) {
                do {
                    eax84 = fun_2710();
                    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
                    if (eax84) 
                        goto addr_4d77_29;
                    ++rbx82;
                    below_or_equal85 = reinterpret_cast<unsigned char>(n_open_pipes) <= reinterpret_cast<unsigned char>(rbx82);
                } while (!below_or_equal85);
            }
            eax86 = fun_2710();
            *reinterpret_cast<int32_t*>(&r12_5) = eax86;
            *reinterpret_cast<int32_t*>(&r12_5 + 4) = 0;
            fun_2560();
            rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8);
            if (*reinterpret_cast<int32_t*>(&r12_5)) 
                goto addr_4da8_12;
            if (v87) {
                eax88 = fun_26a0();
                rsp26 = reinterpret_cast<void*>(rsp18 - 2 + 2);
                if (eax88) 
                    goto addr_4e42_15;
                eax89 = fun_2710();
                rsp18 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                if (eax89) 
                    break;
            }
            fun_2530();
            r12_5 = filter_command;
            rax90 = last_component(r13_13, 0xf460);
            fun_2990(r13_13, rax90, "-c", r12_5);
            rax91 = fun_2640();
            r8_28 = r12_5;
            rcx = r13_13;
            rdx = rax91;
            *reinterpret_cast<int32_t*>(&rdi) = 1;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_28a0();
            rsp6 = reinterpret_cast<void*>(rsp18 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2);
            addr_4bd0_2:
            zf92 = verbose == 0;
            if (!zf92) 
                goto addr_4d08_54;
        }
        rax93 = fun_2640();
        *reinterpret_cast<int32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        rdx = rax93;
        fun_28a0();
        rsp6 = reinterpret_cast<void*>(rsp18 - 2 + 2 - 2 + 2);
    }
    addr_4ac2_56:
    rax94 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rax94) {
        return r13d67;
    }
    addr_4a71_64:
    if (eax81 == 0xffffffff) {
        addr_4ebe_10:
        fun_2640();
        fun_2560();
        fun_28a0();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_4eea_76;
    } else {
        eax95 = fun_2710();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
        if (eax95) {
            addr_4eea_76:
            rax96 = fun_2640();
            rax97 = fun_2560();
            rdx98 = rax96;
            rsi99 = *reinterpret_cast<void***>(rax97);
            *reinterpret_cast<int32_t*>(&rsi99 + 4) = 0;
            fun_28a0();
        } else {
            rdx100 = n_open_pipes;
            zf101 = rdx100 == open_pipes_alloc;
            filter_pid = eax81;
            rdi102 = open_pipes;
            if (zf101) {
                rax103 = x2nrealloc();
                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                rdx100 = n_open_pipes;
                open_pipes = rax103;
                rdi102 = rax103;
            }
            n_open_pipes = rdx100 + 1;
            rdi102[reinterpret_cast<unsigned char>(rdx100) * 4] = v104;
            r13d67 = v105;
            goto addr_4ac2_56;
        }
    }
    r13_106 = reinterpret_cast<struct s2*>((reinterpret_cast<unsigned char>(rsi99) << 5) + 1);
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    if (reinterpret_cast<signed char>(r13_106->f8) < reinterpret_cast<signed char>(0)) {
        rbx107 = rsi99 + 0xffffffffffffffff;
        r15_108 = rdx98 + 0xffffffffffffffff;
        if (!rsi99) {
            rbx107 = r15_108;
        }
        rdi109 = r13_106->f0;
        r14_110 = rsi99;
        if (r13_106->f8 != 0xffffffff) {
            goto addr_4f78_86;
        }
        while (eax111 = create(rdi109, rsi99, rdx98, rcx), rsp112 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8), reinterpret_cast<signed char>(eax111) < reinterpret_cast<signed char>(0)) {
            do {
                rax113 = fun_2560();
                rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp112) - 8 + 8);
                r8_28 = rax113;
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax113) - 23) > 1) 
                    goto addr_4f9e_28;
                do {
                    rbp114 = reinterpret_cast<struct s3*>((reinterpret_cast<unsigned char>(rbx107) << 5) + 1);
                    if (rbp114->f8 >= 0) 
                        break;
                    rax115 = rbx107 + 0xffffffffffffffff;
                    if (!rbx107) {
                        rax115 = r15_108;
                    }
                    rbx107 = rax115;
                } while (rax115 != r14_110);
                goto addr_5050_17;
                rdi116 = rbp114->f10;
                v30 = r8_28;
                rax117 = rpl_fclose(rdi116, rdi116);
                rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax117)) 
                    goto addr_50b6_19;
                rbp114->f8 = -2;
                eax118 = r13_106->f8;
                rbp114->f10 = reinterpret_cast<void**>(0);
                rdi109 = r13_106->f0;
                if (reinterpret_cast<int1_t>(eax118 == 0xffffffff)) 
                    break;
                addr_4f78_86:
                rsi99 = reinterpret_cast<void**>(0xc01);
                *reinterpret_cast<int32_t*>(&rsi99 + 4) = 0;
                eax111 = open_safer(rdi109, 0xc01, rdx98, rcx);
                rsp112 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            } while (reinterpret_cast<signed char>(eax111) < reinterpret_cast<signed char>(0));
            break;
        }
    } else {
        addr_4f42_95:
        goto v119;
    }
    r13_106->f8 = eax111;
    *reinterpret_cast<void***>(&rdi120) = eax111;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi120) + 4) = 0;
    rax121 = fun_2860(rdi120, "a");
    rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp112) - 8 + 8);
    r13_106->f10 = rax121;
    if (!rax121) {
        addr_50e4_20:
        rax122 = quotearg_n_style_colon();
        rax123 = fun_2560();
        rcx36 = rax122;
        esi124 = *reinterpret_cast<void***>(rax123);
        fun_28a0();
    } else {
        eax125 = filter_pid;
        filter_pid = reinterpret_cast<void**>(0);
        r13_106->f18 = eax125;
        goto addr_4f42_95;
    }
    *reinterpret_cast<uint32_t*>(&rbp126) = reinterpret_cast<uint32_t>("%s");
    *reinterpret_cast<int32_t*>(&rbp126 + 4) = 0;
    rbx127 = esi124;
    *reinterpret_cast<int32_t*>(&rbx127 + 4) = 0;
    rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 72);
    rax129 = g28;
    v130 = rax129;
    if (0) {
        if (reinterpret_cast<signed char>(esi124) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_27;
        eax131 = fun_2710();
        rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
        if (eax131 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_102;
        }
    } else {
        rax132 = rpl_fclose(1);
        rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax132) && ((rax133 = fun_2560(), rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8), zf134 = filter_command == 0, zf134) || *reinterpret_cast<void***>(rax133) != 32)) {
            rax135 = quotearg_n_style_colon();
            rcx36 = rax135;
            fun_28a0();
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx127) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_27;
    }
    rcx36 = n_open_pipes;
    if (!rcx36) {
        addr_51e1_27:
        if (1) {
            addr_5250_102:
            eax136 = fun_28c0("%s", reinterpret_cast<int64_t>(rsp128) + 28);
            rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8);
            if (eax136 == -1 && (rax137 = fun_2560(), rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp128) - 8 + 8), rbx127 = rax137, *reinterpret_cast<void***>(rax137) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_108;
            }
        } else {
            addr_51e5_109:
            rax138 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v130) - reinterpret_cast<unsigned char>(g28));
            if (rax138) {
                fun_2670();
                goto addr_53b0_111;
            } else {
                goto v139;
            }
        }
    } else {
        rsi38 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx140) = 0;
        *reinterpret_cast<int32_t*>(&rdx140 + 4) = 0;
        rax39 = rsi38;
        do {
            if (*rax39 == rbx127) 
                goto addr_51d1_26;
            ++rdx140;
            rax39 = rax39 + 4;
        } while (rdx140 != rcx36);
        goto addr_51e1_27;
    }
    *reinterpret_cast<int32_t*>(&rcx36) = 0;
    rbx127 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx127 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_111:
            rax141 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx142) = *reinterpret_cast<int32_t*>(&rcx36);
            *reinterpret_cast<int32_t*>(&rcx142 + 4) = 0;
            fun_28a0();
        } else {
            *reinterpret_cast<uint32_t*>(&rbp126) = *reinterpret_cast<unsigned char*>(&rcx36 + 1);
            *reinterpret_cast<unsigned char*>(&rcx36 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx36 + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx36 + 1)) {
                rbx127 = filter_command;
                rax143 = quotearg_n_style_colon();
                fun_2640();
                *reinterpret_cast<uint32_t*>(&r8_28) = *reinterpret_cast<uint32_t*>(&rbp126);
                *reinterpret_cast<int32_t*>(&r8_28 + 4) = 0;
                rcx36 = rax143;
                fun_28a0();
                goto addr_51e5_109;
            }
        }
    } else {
        if (!0) {
            rbp126 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp128) + 32);
            eax144 = sig2str(0, rbp126);
            if (eax144) {
                addr_5388_108:
                *reinterpret_cast<void***>(&r8_145) = rbx127;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_145) + 4) = 0;
                fun_29f0(rbp126, 1, 19, "%d", r8_145);
                goto addr_529f_122;
            } else {
                addr_529f_122:
                rax146 = quotearg_n_style_colon();
                fun_2640();
                r8_28 = rbp126;
                rcx36 = rax146;
                fun_28a0();
                goto addr_51e5_109;
            }
        }
    }
    v32 = rbx127;
    if (0) {
        *reinterpret_cast<void***>(&rdi147) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi147) + 4) = 0;
        goto addr_543f_125;
    }
    if (rax141 || (zf148 = elide_empty_files == 0, zf148)) {
        rcx142 = outfile;
        rdx149 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx149 + 4) = 0;
        rsi150 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi150 + 4) = 0;
        closeout(0, rsi150, rdx149, rcx142, 0, rsi150, rdx149, rcx142);
        next_file_name();
        rdi151 = outfile;
        eax152 = create(rdi151, rsi150, rdx149, rcx142, rdi151, rsi150, rdx149, rcx142);
        output_desc = eax152;
        *reinterpret_cast<void***>(&rdi147) = eax152;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi147) + 4) = 0;
        if (reinterpret_cast<signed char>(eax152) < reinterpret_cast<signed char>(0)) {
            rax34 = quotearg_n_style_colon();
            rax153 = fun_2560();
            rsi154 = *reinterpret_cast<void***>(rax153);
            *reinterpret_cast<int32_t*>(&rsi154 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_55) = 0;
            *reinterpret_cast<int32_t*>(&r15_55 + 4) = 0;
            r14_44 = reinterpret_cast<void**>(1);
            ebp41 = 1;
            ebx45 = 1;
            v48 = rsi154;
            v50 = r8_28;
            if (rax34 == 0xffffffffffffffff) 
                goto addr_55df_30; else 
                goto addr_552a_24;
        } else {
            addr_543f_125:
            rax155 = full_write(rdi147, 0, rax141, rcx142, rdi147, 0, rax141, rcx142);
            if (rax155 != rax141) {
                rax156 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax156) == 32) || (zf157 = filter_command == 0, zf157)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_132;
                } else {
                    addr_549d_133:
                    goto v158;
                }
            }
        }
    } else {
        addr_5498_132:
        goto addr_549d_133;
    }
}

void** suffix_length = reinterpret_cast<void**>(0);

/* sufindex.3 */
void** sufindex_3 = reinterpret_cast<void**>(0);

unsigned char suffix_auto = 1;

void** suffix_alphabet = reinterpret_cast<void**>(5);

void** outfile_mid = reinterpret_cast<void**>(0);

struct s4 {
    signed char[1] pad1;
    void** f1;
    signed char[1] pad3;
    void** f3;
};

/* outfile_length.6 */
struct s4* outfile_length_6 = reinterpret_cast<struct s4*>(0);

/* outbase_length.5 */
void** outbase_length_5 = reinterpret_cast<void**>(0);

void xalloc_die();

void** xrealloc(void** rdi, void** rsi, void** rdx, void** rcx);

void** outbase = reinterpret_cast<void**>(0);

void** fun_2660(void** rdi, ...);

void** additional_suffix = reinterpret_cast<void**>(0);

/* addsuf_length.4 */
void** addsuf_length_4 = reinterpret_cast<void**>(0);

void fun_27e0(void** rdi, void** rsi, void** rdx, void** rcx);

void* fun_2700(void** rdi, ...);

void fun_2540(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** xcalloc(void** rdi, int64_t rsi, void** rdx);

void** numeric_suffix_start = reinterpret_cast<void**>(0);

void fun_26f0(void** rdi, void** rsi, void** rdx, void** rcx);

struct s5 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
};

struct s6 {
    signed char[8] pad8;
    int32_t f8;
    signed char[4] pad16;
    void** f10;
};

void next_file_name() {
    void** rbp1;
    int64_t* rsp2;
    int64_t v3;
    int64_t rbx4;
    void** rbx5;
    void** rax6;
    void** r10_7;
    uint32_t r9d8;
    void** rdi9;
    void** r8_10;
    void** rdx11;
    void** rcx12;
    void** rsi13;
    uint32_t ecx14;
    uint32_t ecx15;
    struct s4* r12_16;
    void** rsi17;
    int1_t cf18;
    void** rax19;
    void* rsp20;
    void** r14_21;
    void** rcx22;
    void** r13_23;
    void** rax24;
    void** rdx25;
    void** rbx26;
    uint32_t edx27;
    void** rdi28;
    void** rax29;
    void** rax30;
    struct s4* rax31;
    void** rsi32;
    void** rax33;
    void** rsi34;
    uint32_t eax35;
    uint32_t eax36;
    void** rbp37;
    void** rcx38;
    void** rdx39;
    void* rax40;
    void* rsp41;
    void** rsi42;
    struct s4* rax43;
    void** rax44;
    void** rcx45;
    void** rdx46;
    void** rdi47;
    void** rax48;
    void** r13_49;
    void** tmp64_50;
    void** rdx51;
    int64_t* rcx52;
    int1_t cf53;
    void** r12_54;
    void* rsp55;
    void** rax56;
    void** v57;
    int1_t zf58;
    void** rax59;
    void** rdx60;
    void** rsi61;
    void** r13_62;
    int32_t eax63;
    void* rsp64;
    int1_t zf65;
    void* rsp66;
    int32_t* rsp67;
    void* rsp68;
    void** rax69;
    void** rdx70;
    int64_t* rsp71;
    void** rax72;
    void** rax73;
    void** rax74;
    void* rsp75;
    void* rsp76;
    void* rsp77;
    void** v78;
    void* rsp79;
    void** v80;
    void** r12_81;
    void** rax82;
    unsigned char v83;
    void** rcx84;
    void** edx85;
    void*** rsi86;
    void*** rax87;
    void** rax88;
    uint32_t ebp89;
    void** rax90;
    int1_t zf91;
    void** r14_92;
    int32_t ebx93;
    void** rax94;
    void** rsi95;
    void** v96;
    void** r11_97;
    void** v98;
    uint32_t eax99;
    uint32_t edi100;
    uint32_t eax101;
    void* rbx102;
    void** r15_103;
    uint1_t below_or_equal104;
    uint32_t eax105;
    uint32_t edi106;
    uint32_t eax107;
    void* rbx108;
    void** rbx109;
    void** rax110;
    void** rax111;
    void** rax112;
    void** rdi113;
    void** eax114;
    int32_t eax115;
    int1_t zf116;
    int64_t v117;
    int1_t zf118;
    int64_t v119;
    int64_t rdi120;
    int32_t eax121;
    uint32_t v122;
    void** rax123;
    void** rax124;
    void** rax125;
    void** rdi126;
    int32_t eax127;
    void** eax128;
    int1_t zf129;
    int32_t eax130;
    int1_t below_or_equal131;
    int32_t eax132;
    int32_t v133;
    int32_t eax134;
    int32_t eax135;
    void** rax136;
    void** rax137;
    int1_t zf138;
    void** rax139;
    void* rax140;
    int32_t eax141;
    void** rax142;
    void** rax143;
    void** rdx144;
    void** rsi145;
    void** rdx146;
    int1_t zf147;
    void*** rdi148;
    void*** rax149;
    void** v150;
    struct s5* r13_151;
    void** rbx152;
    void** r15_153;
    void** rdi154;
    void** r14_155;
    void** eax156;
    void* rsp157;
    void** rax158;
    struct s6* rbp159;
    void** rax160;
    void** rdi161;
    int64_t rax162;
    void** eax163;
    int64_t v164;
    int64_t rdi165;
    int64_t rax166;
    void** rax167;
    void** rax168;
    void** esi169;
    void** eax170;
    void** rbp171;
    void** rbx172;
    void* rsp173;
    void** rax174;
    void** v175;
    int32_t eax176;
    int64_t rax177;
    void** rax178;
    int1_t zf179;
    void** rax180;
    int32_t eax181;
    void** rax182;
    void* rax183;
    int64_t v184;
    void** rdx185;
    void** rax186;
    void** rcx187;
    void** rax188;
    int32_t eax189;
    int64_t r8_190;
    void** rax191;
    int64_t rdi192;
    int1_t zf193;
    void** rdx194;
    void** rsi195;
    void** rdi196;
    void** eax197;
    void** rax198;
    void** rsi199;
    void** rax200;
    void** rax201;
    int1_t zf202;
    int64_t v203;

    rbp1 = outfile;
    rsp2 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8);
    v3 = rbx4;
    rbx5 = suffix_length;
    if (!rbp1) 
        goto addr_4798_2;
    rax6 = rbx5 + 0xffffffffffffffff;
    if (!rbx5) {
        addr_4997_4:
        fun_2640();
        fun_28a0();
        rsp2 = rsp2 - 1 + 1 - 1 + 1;
        goto addr_49bb_5;
    } else {
        r10_7 = sufindex_3;
        r9d8 = suffix_auto;
        rdi9 = suffix_alphabet;
        r8_10 = outfile_mid;
        rdx11 = r10_7 + reinterpret_cast<unsigned char>(rax6) * 8;
        do {
            rcx12 = *reinterpret_cast<void***>(rdx11);
            rsi13 = rcx12 + 1;
            *reinterpret_cast<void***>(rdx11) = rsi13;
            if (rax6) 
                goto addr_4730_8;
            if (*reinterpret_cast<signed char*>(&r9d8)) 
                break;
            addr_4730_8:
            ecx14 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi9) + reinterpret_cast<unsigned char>(rcx12) + 1);
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(rax6)) = *reinterpret_cast<signed char*>(&ecx14);
            if (*reinterpret_cast<signed char*>(&ecx14)) 
                goto addr_478a_10;
            *reinterpret_cast<void***>(rdx11) = reinterpret_cast<void**>(0);
            ecx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi9));
            rdx11 = rdx11 - 8;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(rax6)) = *reinterpret_cast<signed char*>(&ecx15);
            --rax6;
        } while (rax6 != 0xffffffffffffffff);
        goto addr_4997_4;
    }
    if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi9) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r10_7)) + 1)) {
        addr_4798_2:
        r12_16 = outfile_length_6;
        if (r12_16) {
            ++rbx5;
            rsi17 = reinterpret_cast<void**>(&r12_16->f3);
            cf18 = reinterpret_cast<unsigned char>(rsi17) < reinterpret_cast<unsigned char>(outbase_length_5);
            outfile_length_6 = reinterpret_cast<struct s4*>(&r12_16->pad3);
            suffix_length = rbx5;
            if (cf18) {
                addr_4985_14:
                xalloc_die();
                rsp2 = rsp2 - 1 + 1;
            } else {
                rax19 = xrealloc(rbp1, rsi17, rdx11, rcx12);
                rsp20 = reinterpret_cast<void*>(rsp2 - 1 + 1);
                r14_21 = sufindex_3;
                rcx22 = outbase_length_5;
                r13_23 = rax19;
                outfile = rax19;
                rax24 = suffix_alphabet;
                rdx25 = *reinterpret_cast<void***>(r14_21);
                rbx26 = rcx22 + 1;
                outbase_length_5 = rbx26;
                edx27 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rdx25));
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_23) + reinterpret_cast<unsigned char>(rcx22)) = *reinterpret_cast<signed char*>(&edx27);
                goto addr_4897_16;
            }
        } else {
            rdi28 = outbase;
            rax29 = fun_2660(rdi28);
            rsp2 = rsp2 - 1 + 1;
            rdi9 = additional_suffix;
            outbase_length_5 = rax29;
            *reinterpret_cast<int32_t*>(&rax30) = 0;
            *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
            if (rdi9) {
                rax30 = fun_2660(rdi9);
                rsp2 = rsp2 - 1 + 1;
            }
            rbx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rax29));
            addsuf_length_4 = rax30;
            rax31 = reinterpret_cast<struct s4*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<unsigned char>(rbx5));
            rsi32 = reinterpret_cast<void**>(&rax31->f1);
            outfile_length_6 = rax31;
            if (reinterpret_cast<unsigned char>(rax29) > reinterpret_cast<unsigned char>(rsi32)) 
                goto addr_4985_14;
            rax33 = xrealloc(rbp1, rsi32, rdx11, rcx12);
            rbx26 = outbase_length_5;
            rsi34 = outbase;
            outfile = rax33;
            r13_23 = rax33;
            fun_27e0(rax33, rsi34, rbx26, rcx12);
            rsp20 = reinterpret_cast<void*>(rsp2 - 1 + 1 - 1 + 1);
            r14_21 = sufindex_3;
            goto addr_4897_16;
        }
    } else {
        eax35 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi9) + reinterpret_cast<unsigned char>(rsi13));
        *reinterpret_cast<void***>(r8_10) = *reinterpret_cast<void***>(&eax35);
        if (*reinterpret_cast<void***>(&eax35)) 
            goto addr_478a_10;
    }
    *reinterpret_cast<void***>(rdx11) = reinterpret_cast<void**>(0);
    eax36 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi9));
    *reinterpret_cast<void***>(r8_10) = *reinterpret_cast<void***>(&eax36);
    goto addr_4997_4;
    addr_4897_16:
    rbp37 = suffix_length;
    rcx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23) + reinterpret_cast<unsigned char>(rbx26));
    outfile_mid = rcx38;
    rdx39 = rbp37;
    rax40 = fun_2700(rcx38, rcx38);
    rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
    rsi42 = additional_suffix;
    if (rsi42) {
        rdx39 = addsuf_length_4;
        fun_27e0(reinterpret_cast<int64_t>(rax40) + reinterpret_cast<unsigned char>(rbp37), rsi42, rdx39, rcx38);
        rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
    }
    rax43 = outfile_length_6;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_23) + reinterpret_cast<uint64_t>(rax43)) = 0;
    fun_2540(r14_21, rsi42, rdx39, rcx38, r8_10);
    rax44 = xcalloc(rbp37, 8, rdx39);
    rsp2 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8 - 8 + 8);
    rbp1 = numeric_suffix_start;
    sufindex_3 = rax44;
    if (!rbp1) {
        addr_478a_10:
        return;
    } else {
        if (r12_16) {
            addr_49bb_5:
            rcx45 = reinterpret_cast<void**>("next_file_name");
            *reinterpret_cast<int32_t*>(&rdx46) = 0x199;
            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
            rdi47 = reinterpret_cast<void**>("! widen");
            fun_26f0("! widen", "src/split.c", 0x199, "next_file_name");
        } else {
            rax48 = fun_2660(rbp1, rbp1);
            r13_49 = suffix_length;
            tmp64_50 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_49) - reinterpret_cast<unsigned char>(rax48)) + reinterpret_cast<unsigned char>(outfile_mid));
            fun_27e0(tmp64_50, rbp1, rax48, rcx38);
            rdx51 = rax48 + 0xffffffffffffffff;
            if (rax48) {
                rcx52 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rax44) + ((reinterpret_cast<unsigned char>(r13_49) << 3) + -reinterpret_cast<unsigned char>(rax48) * 8));
                do {
                    rcx52[reinterpret_cast<unsigned char>(rdx51)] = *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp1) + reinterpret_cast<unsigned char>(rdx51)) - 48;
                    cf53 = reinterpret_cast<unsigned char>(rdx51) < reinterpret_cast<unsigned char>(1);
                    --rdx51;
                } while (!cf53);
                return;
            }
        }
    }
    r12_54 = reinterpret_cast<void**>("! widen");
    rsp55 = reinterpret_cast<void*>(rsp2 - 1 + 1 - 1 - 1 - 1 - 20);
    rax56 = g28;
    v57 = rax56;
    zf58 = filter_command == 0;
    if (zf58) 
        goto addr_4bd0_31;
    rax59 = fun_2520("SHELL", "src/split.c", 0x199, "next_file_name");
    *reinterpret_cast<int32_t*>(&rdx60) = 1;
    *reinterpret_cast<int32_t*>(&rdx60 + 4) = 0;
    rsi61 = reinterpret_cast<void**>("! widen");
    r13_62 = rax59;
    if (!rax59) {
        r13_62 = reinterpret_cast<void**>("/bin/sh");
    }
    eax63 = fun_25e0("FILE", "! widen", 1, "next_file_name");
    rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8 - 8 + 8);
    if (!eax63) {
        zf65 = verbose == 0;
        if (!zf65) 
            goto addr_4c98_36; else 
            goto addr_4a56_37;
    }
    addr_4e92_38:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp66 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4ebe_39;
    addr_4da3_40:
    fun_2670();
    rsp67 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8);
    addr_4da8_41:
    rax69 = fun_2640();
    rdx70 = rax69;
    fun_28a0();
    rsp71 = reinterpret_cast<int64_t*>(rsp67 - 2 + 2 - 2 + 2);
    addr_4dcc_42:
    rax72 = quotearg_style(4, r12_54, rdx70, rcx45);
    r12_54 = rax72;
    rax73 = fun_2640();
    rcx45 = r12_54;
    rdx70 = rax73;
    fun_28a0();
    rsp71 = rsp71 - 1 + 1 - 1 + 1 - 1 + 1;
    addr_4e03_43:
    rax74 = quotearg_style(4, r12_54, rdx70, rcx45);
    fun_2640();
    fun_2560();
    rcx45 = rax74;
    fun_28a0();
    rsp75 = reinterpret_cast<void*>(rsp71 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1);
    addr_4e42_44:
    fun_2640();
    fun_28a0();
    rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8 - 8 + 8);
    addr_4e66_45:
    fun_2640();
    fun_2560();
    fun_28a0();
    rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4e92_38;
    addr_5050_46:
    quotearg_n_style_colon();
    r8_10 = r8_10;
    fun_28a0();
    rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 - 8 + 8);
    addr_5083_47:
    v78 = r8_10;
    quotearg_n_style_colon();
    fun_28a0();
    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 - 8 + 8);
    addr_50b6_48:
    quotearg_n_style_colon();
    r8_10 = v78;
    fun_28a0();
    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
    goto addr_50e4_49;
    addr_568b_50:
    quotearg_n_style_colon();
    fun_2560();
    fun_28a0();
    addr_567c_52:
    goto v80;
    addr_552a_53:
    r12_81 = rax82;
    v83 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax82) < reinterpret_cast<unsigned char>("%s"));
    goto addr_5535_54;
    addr_51d1_55:
    --rcx84;
    edx85 = rsi86[reinterpret_cast<unsigned char>(rcx84) * 4];
    n_open_pipes = rcx84;
    *rax87 = edx85;
    goto addr_51e1_56;
    addr_4f9e_57:
    goto addr_5083_47;
    addr_4d77_58:
    rax88 = fun_2640();
    r12_54 = rax88;
    fun_2560();
    fun_28a0();
    rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp66) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_4da3_40;
    do {
        addr_55df_59:
        if (!*reinterpret_cast<unsigned char*>(&ebp89)) {
            rax90 = fun_26e0();
            zf91 = reinterpret_cast<int1_t>(rax90 == 0xffffffffffffffff);
            if (!zf91) {
                r14_92 = reinterpret_cast<void**>(1);
            }
            if (!zf91) {
                ebx93 = 1;
            }
        }
        rax94 = safe_read();
        r12_81 = rax94;
        if (rax94 == 0xffffffffffffffff) 
            goto addr_568b_50;
        v83 = reinterpret_cast<uint1_t>(rax94 == 0);
        addr_5535_54:
        if (reinterpret_cast<unsigned char>(r12_81) < reinterpret_cast<unsigned char>(r14_92)) {
            rsi95 = v96;
        } else {
            rsi95 = v96;
            r11_97 = v98 + 0xffffffffffffffff;
            do {
                eax99 = 1;
                *reinterpret_cast<unsigned char*>(&ebp89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp89) | *reinterpret_cast<unsigned char*>(&ebx93));
                if (*reinterpret_cast<unsigned char*>(&ebp89)) {
                    edi100 = *reinterpret_cast<unsigned char*>(&ebx93);
                    eax101 = cwrite(*reinterpret_cast<unsigned char*>(&edi100), rsi95, r14_92, *reinterpret_cast<unsigned char*>(&edi100), rsi95, r14_92);
                    r11_97 = r11_97;
                    rsi95 = rsi95;
                    ebp89 = eax101;
                    eax99 = eax101 ^ 1;
                }
                *reinterpret_cast<uint32_t*>(&rbx102) = *reinterpret_cast<unsigned char*>(&ebx93);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx102) + 4) = 0;
                r15_103 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_103) + reinterpret_cast<uint64_t>(rbx102));
                below_or_equal104 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_103) <= reinterpret_cast<unsigned char>(r11_97));
                *reinterpret_cast<unsigned char*>(&ebx93) = below_or_equal104;
                if (below_or_equal104) 
                    continue;
                if (*reinterpret_cast<signed char*>(&eax99)) 
                    goto addr_5650_73;
                r12_81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_81) - reinterpret_cast<unsigned char>(r14_92));
                rsi95 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi95) + reinterpret_cast<unsigned char>(r14_92));
                r14_92 = reinterpret_cast<void**>(1);
            } while (reinterpret_cast<unsigned char>(1) <= reinterpret_cast<unsigned char>(r12_81));
        }
        if (r12_81) {
            eax105 = 1;
            *reinterpret_cast<unsigned char*>(&ebp89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&ebp89) | *reinterpret_cast<unsigned char*>(&ebx93));
            if (*reinterpret_cast<unsigned char*>(&ebp89)) {
                edi106 = *reinterpret_cast<unsigned char*>(&ebx93);
                eax107 = cwrite(*reinterpret_cast<unsigned char*>(&edi106), rsi95, r12_81, *reinterpret_cast<unsigned char*>(&edi106), rsi95, r12_81);
                ebp89 = eax107;
                eax105 = eax107 ^ 1;
            }
            *reinterpret_cast<uint32_t*>(&rbx108) = *reinterpret_cast<unsigned char*>(&ebx93);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx108) + 4) = 0;
            r15_103 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_103) + reinterpret_cast<uint64_t>(rbx108));
            *reinterpret_cast<unsigned char*>(&ebx93) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v98 == r15_103)) & *reinterpret_cast<unsigned char*>(&eax105));
            if (*reinterpret_cast<unsigned char*>(&ebx93)) 
                goto addr_567c_52;
            r14_92 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_92) - reinterpret_cast<unsigned char>(r12_81));
        }
    } while (!v83);
    addr_5650_73:
    rbx109 = r15_103 + 1;
    if (reinterpret_cast<unsigned char>(v98) > reinterpret_cast<unsigned char>(r15_103)) {
        do {
            cwrite(1, 0, 0, 1, 0, 0);
            rax110 = rbx109;
            ++rbx109;
        } while (v98 != rax110);
        goto addr_567c_52;
    }
    while (1) {
        addr_4d08_83:
        rax111 = quotearg_style(4, rdi47, rdx46, rcx45);
        rax112 = fun_2640();
        rdi113 = stdout;
        rcx45 = rax111;
        fun_2950(rdi113, 1, rax112, rcx45, r8_10);
        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8 - 8 + 8 - 8 + 8);
        while (1) {
            *reinterpret_cast<int32_t*>(&rdx70) = 0x1b6;
            *reinterpret_cast<int32_t*>(&rdx70 + 4) = 0;
            eax114 = open_safer(r12_54, 65, 0x1b6, rcx45);
            rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
            if (reinterpret_cast<signed char>(eax114) < reinterpret_cast<signed char>(0)) 
                goto addr_4ac2_85;
            eax115 = fun_29b0();
            rsp71 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8);
            if (eax115) 
                goto addr_4e03_43;
            zf116 = gf308 == v117;
            if (!zf116) 
                goto addr_4c2d_88;
            zf118 = in_stat_buf == v119;
            if (zf118) 
                goto addr_4dcc_42;
            addr_4c2d_88:
            *reinterpret_cast<void***>(&rdi120) = eax114;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi120) + 4) = 0;
            eax121 = fun_26d0(rdi120);
            rsp68 = reinterpret_cast<void*>(rsp71 - 1 + 1);
            if (!eax121) 
                goto addr_4ac2_85;
            if ((v122 & 0xf000) != 0x8000) 
                goto addr_4ac2_85;
            rax123 = quotearg_n_style_colon();
            r13_62 = rax123;
            fun_2640();
            fun_2560();
            fun_28a0();
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4c98_36:
            rax124 = quotearg_n_style_colon();
            r12_54 = rax124;
            rax125 = fun_2640();
            rdi126 = stdout;
            rcx45 = r12_54;
            *reinterpret_cast<int32_t*>(&rsi61) = 1;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            rdx60 = rax125;
            fun_2950(rdi126, 1, rdx60, rcx45, r8_10);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8 - 8 + 8);
            addr_4a56_37:
            eax127 = fun_2720(rsp64, rsi61, rdx60, rcx45);
            rsp76 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
            if (eax127) 
                goto addr_4e66_45;
            eax128 = fun_29d0(rsp64, rsi61, rdx60, rcx45);
            rsp66 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp76) - 8 + 8);
            if (eax128) 
                goto addr_4a71_93;
            rbx5 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rbx5 + 4) = 0;
            zf129 = n_open_pipes == 0;
            if (!zf129) {
                do {
                    eax130 = fun_2710();
                    rsp66 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp66) - 8 + 8);
                    if (eax130) 
                        goto addr_4d77_58;
                    ++rbx5;
                    below_or_equal131 = reinterpret_cast<unsigned char>(n_open_pipes) <= reinterpret_cast<unsigned char>(rbx5);
                } while (!below_or_equal131);
            }
            eax132 = fun_2710();
            *reinterpret_cast<int32_t*>(&r12_54) = eax132;
            *reinterpret_cast<int32_t*>(&r12_54 + 4) = 0;
            fun_2560();
            rsp67 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp66) - 8 + 8 - 8 + 8);
            if (*reinterpret_cast<int32_t*>(&r12_54)) 
                goto addr_4da8_41;
            if (v133) {
                eax134 = fun_26a0();
                rsp75 = reinterpret_cast<void*>(rsp67 - 2 + 2);
                if (eax134) 
                    goto addr_4e42_44;
                eax135 = fun_2710();
                rsp67 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
                if (eax135) 
                    break;
            }
            fun_2530();
            r12_54 = filter_command;
            rax136 = last_component(r13_62, 0xf460);
            fun_2990(r13_62, rax136, "-c", r12_54);
            rax137 = fun_2640();
            r8_10 = r12_54;
            rcx45 = r13_62;
            rdx46 = rax137;
            *reinterpret_cast<int32_t*>(&rdi47) = 1;
            *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0;
            fun_28a0();
            rsp55 = reinterpret_cast<void*>(rsp67 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2);
            addr_4bd0_31:
            zf138 = verbose == 0;
            if (!zf138) 
                goto addr_4d08_83;
        }
        rax139 = fun_2640();
        *reinterpret_cast<int32_t*>(&rdi47) = 1;
        *reinterpret_cast<int32_t*>(&rdi47 + 4) = 0;
        rdx46 = rax139;
        fun_28a0();
        rsp55 = reinterpret_cast<void*>(rsp67 - 2 + 2 - 2 + 2);
    }
    addr_4ac2_85:
    rax140 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v57) - reinterpret_cast<unsigned char>(g28));
    if (!rax140) {
        goto v3;
    }
    addr_4a71_93:
    if (eax128 == 0xffffffff) {
        addr_4ebe_39:
        fun_2640();
        fun_2560();
        fun_28a0();
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp66) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_4eea_105;
    } else {
        eax141 = fun_2710();
        rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp66) - 8 + 8);
        if (eax141) {
            addr_4eea_105:
            rax142 = fun_2640();
            rax143 = fun_2560();
            rdx144 = rax142;
            rsi145 = *reinterpret_cast<void***>(rax143);
            *reinterpret_cast<int32_t*>(&rsi145 + 4) = 0;
            fun_28a0();
        } else {
            rdx146 = n_open_pipes;
            zf147 = rdx146 == open_pipes_alloc;
            filter_pid = eax128;
            rdi148 = open_pipes;
            if (zf147) {
                rax149 = x2nrealloc();
                rsp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8);
                rdx146 = n_open_pipes;
                open_pipes = rax149;
                rdi148 = rax149;
            }
            n_open_pipes = rdx146 + 1;
            rdi148[reinterpret_cast<unsigned char>(rdx146) * 4] = v150;
            goto addr_4ac2_85;
        }
    }
    r13_151 = reinterpret_cast<struct s5*>((reinterpret_cast<unsigned char>(rsi145) << 5) + 1);
    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    if (reinterpret_cast<signed char>(r13_151->f8) < reinterpret_cast<signed char>(0)) {
        rbx152 = rsi145 + 0xffffffffffffffff;
        r15_153 = rdx144 + 0xffffffffffffffff;
        if (!rsi145) {
            rbx152 = r15_153;
        }
        rdi154 = r13_151->f0;
        r14_155 = rsi145;
        if (r13_151->f8 != 0xffffffff) {
            goto addr_4f78_115;
        }
        while (eax156 = create(rdi154, rsi145, rdx144, rcx45), rsp157 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8), reinterpret_cast<signed char>(eax156) < reinterpret_cast<signed char>(0)) {
            do {
                rax158 = fun_2560();
                rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp157) - 8 + 8);
                r8_10 = rax158;
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax158) - 23) > 1) 
                    goto addr_4f9e_57;
                do {
                    rbp159 = reinterpret_cast<struct s6*>((reinterpret_cast<unsigned char>(rbx152) << 5) + 1);
                    if (rbp159->f8 >= 0) 
                        break;
                    rax160 = rbx152 + 0xffffffffffffffff;
                    if (!rbx152) {
                        rax160 = r15_153;
                    }
                    rbx152 = rax160;
                } while (rax160 != r14_155);
                goto addr_5050_46;
                rdi161 = rbp159->f10;
                v78 = r8_10;
                rax162 = rpl_fclose(rdi161, rdi161);
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax162)) 
                    goto addr_50b6_48;
                rbp159->f8 = -2;
                eax163 = r13_151->f8;
                rbp159->f10 = reinterpret_cast<void**>(0);
                rdi154 = r13_151->f0;
                if (reinterpret_cast<int1_t>(eax163 == 0xffffffff)) 
                    break;
                addr_4f78_115:
                rsi145 = reinterpret_cast<void**>(0xc01);
                *reinterpret_cast<int32_t*>(&rsi145 + 4) = 0;
                eax156 = open_safer(rdi154, 0xc01, rdx144, rcx45);
                rsp157 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
            } while (reinterpret_cast<signed char>(eax156) < reinterpret_cast<signed char>(0));
            break;
        }
    } else {
        addr_4f42_124:
        goto v164;
    }
    r13_151->f8 = eax156;
    *reinterpret_cast<void***>(&rdi165) = eax156;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi165) + 4) = 0;
    rax166 = fun_2860(rdi165, "a");
    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp157) - 8 + 8);
    r13_151->f10 = rax166;
    if (!rax166) {
        addr_50e4_49:
        rax167 = quotearg_n_style_colon();
        rax168 = fun_2560();
        rcx84 = rax167;
        esi169 = *reinterpret_cast<void***>(rax168);
        fun_28a0();
    } else {
        eax170 = filter_pid;
        filter_pid = reinterpret_cast<void**>(0);
        r13_151->f18 = eax170;
        goto addr_4f42_124;
    }
    *reinterpret_cast<uint32_t*>(&rbp171) = reinterpret_cast<uint32_t>("%s");
    *reinterpret_cast<int32_t*>(&rbp171 + 4) = 0;
    rbx172 = esi169;
    *reinterpret_cast<int32_t*>(&rbx172 + 4) = 0;
    rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 72);
    rax174 = g28;
    v175 = rax174;
    if (0) {
        if (reinterpret_cast<signed char>(esi169) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_56;
        eax176 = fun_2710();
        rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8);
        if (eax176 < 0) {
            quotearg_n_style_colon();
            fun_2560();
            fun_28a0();
            rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_5250_131;
        }
    } else {
        rax177 = rpl_fclose(1);
        rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax177) && ((rax178 = fun_2560(), rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8), zf179 = filter_command == 0, zf179) || *reinterpret_cast<void***>(rax178) != 32)) {
            rax180 = quotearg_n_style_colon();
            rcx84 = rax180;
            fun_28a0();
            rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<signed char>(rbx172) < reinterpret_cast<signed char>(0)) 
            goto addr_51e1_56;
    }
    rcx84 = n_open_pipes;
    if (!rcx84) {
        addr_51e1_56:
        if (1) {
            addr_5250_131:
            eax181 = fun_28c0("%s", reinterpret_cast<int64_t>(rsp173) + 28);
            rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8);
            if (eax181 == -1 && (rax182 = fun_2560(), rsp173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp173) - 8 + 8), rbx172 = rax182, *reinterpret_cast<void***>(rax182) != 10)) {
                fun_2640();
                fun_28a0();
                goto addr_5388_137;
            }
        } else {
            addr_51e5_138:
            rax183 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v175) - reinterpret_cast<unsigned char>(g28));
            if (rax183) {
                fun_2670();
                goto addr_53b0_140;
            } else {
                goto v184;
            }
        }
    } else {
        rsi86 = open_pipes;
        *reinterpret_cast<int32_t*>(&rdx185) = 0;
        *reinterpret_cast<int32_t*>(&rdx185 + 4) = 0;
        rax87 = rsi86;
        do {
            if (*rax87 == rbx172) 
                goto addr_51d1_55;
            ++rdx185;
            rax87 = rax87 + 4;
        } while (rdx185 != rcx84);
        goto addr_51e1_56;
    }
    *reinterpret_cast<int32_t*>(&rcx84) = 0;
    rbx172 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rbx172 + 4) = 0;
    if (1) {
        if (0) {
            addr_53b0_140:
            rax186 = fun_2640();
            *reinterpret_cast<int32_t*>(&rcx187) = *reinterpret_cast<int32_t*>(&rcx84);
            *reinterpret_cast<int32_t*>(&rcx187 + 4) = 0;
            fun_28a0();
        } else {
            *reinterpret_cast<uint32_t*>(&rbp171) = *reinterpret_cast<unsigned char*>(&rcx84 + 1);
            *reinterpret_cast<unsigned char*>(&rcx84 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx84 + 1) & 0xff);
            if (*reinterpret_cast<unsigned char*>(&rcx84 + 1)) {
                rbx172 = filter_command;
                rax188 = quotearg_n_style_colon();
                fun_2640();
                *reinterpret_cast<uint32_t*>(&r8_10) = *reinterpret_cast<uint32_t*>(&rbp171);
                *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
                rcx84 = rax188;
                fun_28a0();
                goto addr_51e5_138;
            }
        }
    } else {
        if (!0) {
            rbp171 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp173) + 32);
            eax189 = sig2str(0, rbp171);
            if (eax189) {
                addr_5388_137:
                *reinterpret_cast<void***>(&r8_190) = rbx172;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_190) + 4) = 0;
                fun_29f0(rbp171, 1, 19, "%d", r8_190);
                goto addr_529f_151;
            } else {
                addr_529f_151:
                rax191 = quotearg_n_style_colon();
                fun_2640();
                r8_10 = rbp171;
                rcx84 = rax191;
                fun_28a0();
                goto addr_51e5_138;
            }
        }
    }
    v80 = rbx172;
    if (0) {
        *reinterpret_cast<void***>(&rdi192) = output_desc;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi192) + 4) = 0;
        goto addr_543f_154;
    }
    if (rax186 || (zf193 = elide_empty_files == 0, zf193)) {
        rcx187 = outfile;
        rdx194 = filter_pid;
        *reinterpret_cast<int32_t*>(&rdx194 + 4) = 0;
        rsi195 = output_desc;
        *reinterpret_cast<int32_t*>(&rsi195 + 4) = 0;
        closeout(0, rsi195, rdx194, rcx187, 0, rsi195, rdx194, rcx187);
        next_file_name();
        rdi196 = outfile;
        eax197 = create(rdi196, rsi195, rdx194, rcx187, rdi196, rsi195, rdx194, rcx187);
        output_desc = eax197;
        *reinterpret_cast<void***>(&rdi192) = eax197;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi192) + 4) = 0;
        if (reinterpret_cast<signed char>(eax197) < reinterpret_cast<signed char>(0)) {
            rax82 = quotearg_n_style_colon();
            rax198 = fun_2560();
            rsi199 = *reinterpret_cast<void***>(rax198);
            *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
            fun_28a0();
            *reinterpret_cast<int32_t*>(&r15_103) = 0;
            *reinterpret_cast<int32_t*>(&r15_103 + 4) = 0;
            r14_92 = reinterpret_cast<void**>(1);
            ebp89 = 1;
            ebx93 = 1;
            v96 = rsi199;
            v98 = r8_10;
            if (rax82 == 0xffffffffffffffff) 
                goto addr_55df_59; else 
                goto addr_552a_53;
        } else {
            addr_543f_154:
            rax200 = full_write(rdi192, 0, rax186, rcx187, rdi192, 0, rax186, rcx187);
            if (rax200 != rax186) {
                rax201 = fun_2560();
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax201) == 32) || (zf202 = filter_command == 0, zf202)) {
                    quotearg_n_style_colon();
                    fun_28a0();
                    goto addr_5498_161;
                } else {
                    addr_549d_162:
                    goto v203;
                }
            }
        }
    } else {
        addr_5498_161:
        goto addr_549d_162;
    }
}

int64_t fun_2650();

int64_t fun_2550(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2650();
    if (r8d > 10) {
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb300 + rax11 * 4) + 0xb300;
    }
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s7* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    int64_t rax15;
    void** r8_16;
    struct s8* rbx17;
    uint32_t r15d18;
    void** rsi19;
    void** r14_20;
    int64_t v21;
    int64_t v22;
    uint32_t r15d23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x756f;
    rax8 = fun_2560();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
        fun_2550(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xf090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7981]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            v7 = 0x75fb;
            fun_2700((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            rax15 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax15);
        }
        *reinterpret_cast<uint32_t*>(&r8_16) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_16 + 4) = 0;
        rbx17 = reinterpret_cast<struct s8*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d18 = rcx->f4;
        rsi19 = rbx17->f0;
        r14_20 = rbx17->f8;
        v21 = rcx->f30;
        v22 = rcx->f28;
        r15d23 = r15d18 | 1;
        rax24 = quotearg_buffer_restyled(r14_20, rsi19, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_16), r15d23, &rcx->f8, v22, v21, v7);
        if (reinterpret_cast<unsigned char>(rsi19) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx17->f0 = rsi25;
            if (r14_20 != 0xf520) {
                fun_2540(r14_20, rsi25, rsi, rdx, r8_16, r14_20, rsi25, rsi, rdx, r8_16);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx17->f8 = rax26;
            v28 = rcx->f30;
            r14_20 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d23, rsi25, v29, v28, 0x768a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2670();
        } else {
            return r14_20;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xf248;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s9 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s9* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s9* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xb2a3);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xb29c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xb2a7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xb298);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t ged58 = 0;

void fun_2033() {
    __asm__("cli ");
    goto ged58;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2503() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2513() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2523() {
    __asm__("cli ");
    goto getenv;
}

int64_t sigprocmask = 0x2040;

void fun_2533() {
    __asm__("cli ");
    goto sigprocmask;
}

int64_t free = 0x2050;

void fun_2543() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2553() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2563() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2573() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2583() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x20a0;

void fun_2593() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20b0;

void fun_25a3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t sigaction = 0x20c0;

void fun_25b3() {
    __asm__("cli ");
    goto sigaction;
}

int64_t reallocarray = 0x20d0;

void fun_25c3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20e0;

void fun_25d3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t setenv = 0x20f0;

void fun_25e3() {
    __asm__("cli ");
    goto setenv;
}

int64_t clearerr_unlocked = 0x2100;

void fun_25f3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t write = 0x2110;

void fun_2603() {
    __asm__("cli ");
    goto write;
}

int64_t textdomain = 0x2120;

void fun_2613() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2130;

void fun_2623() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2140;

void fun_2633() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2150;

void fun_2643() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2160;

void fun_2653() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2170;

void fun_2663() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2180;

void fun_2673() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2190;

void fun_2683() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21a0;

void fun_2693() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x21b0;

void fun_26a3() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x21c0;

void fun_26b3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x21d0;

void fun_26c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x21e0;

void fun_26d3() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t lseek = 0x21f0;

void fun_26e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2200;

void fun_26f3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2210;

void fun_2703() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2220;

void fun_2713() {
    __asm__("cli ");
    goto close;
}

int64_t pipe = 0x2230;

void fun_2723() {
    __asm__("cli ");
    goto pipe;
}

int64_t strspn = 0x2240;

void fun_2733() {
    __asm__("cli ");
    goto strspn;
}

int64_t memchr = 0x2250;

void fun_2743() {
    __asm__("cli ");
    goto memchr;
}

int64_t read = 0x2260;

void fun_2753() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x2270;

void fun_2763() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2280;

void fun_2773() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x2290;

void fun_2783() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x22a0;

void fun_2793() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x22b0;

void fun_27a3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x22c0;

void fun_27b3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t sigemptyset = 0x22d0;

void fun_27c3() {
    __asm__("cli ");
    goto sigemptyset;
}

int64_t strtol = 0x22e0;

void fun_27d3() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x22f0;

void fun_27e3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2300;

void fun_27f3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2310;

void fun_2803() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2320;

void fun_2813() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2330;

void fun_2823() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2340;

void fun_2833() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2350;

void fun_2843() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2360;

void fun_2853() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x2370;

void fun_2863() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x2380;

void fun_2873() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2390;

void fun_2883() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x23a0;

void fun_2893() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x23b0;

void fun_28a3() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x23c0;

void fun_28b3() {
    __asm__("cli ");
    goto memrchr;
}

int64_t waitpid = 0x23d0;

void fun_28c3() {
    __asm__("cli ");
    goto waitpid;
}

int64_t open = 0x23e0;

void fun_28d3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x23f0;

void fun_28e3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2400;

void fun_28f3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2410;

void fun_2903() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t getpagesize = 0x2420;

void fun_2913() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t __libc_current_sigrtmin = 0x2430;

void fun_2923() {
    __asm__("cli ");
    goto __libc_current_sigrtmin;
}

int64_t exit = 0x2440;

void fun_2933() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2450;

void fun_2943() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2460;

void fun_2953() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t __libc_current_sigrtmax = 0x2470;

void fun_2963() {
    __asm__("cli ");
    goto __libc_current_sigrtmax;
}

int64_t aligned_alloc = 0x2480;

void fun_2973() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x2490;

void fun_2983() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t execl = 0x24a0;

void fun_2993() {
    __asm__("cli ");
    goto execl;
}

int64_t iswprint = 0x24b0;

void fun_29a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x24c0;

void fun_29b3() {
    __asm__("cli ");
    goto fstat;
}

int64_t sigaddset = 0x24d0;

void fun_29c3() {
    __asm__("cli ");
    goto sigaddset;
}

int64_t fork = 0x24e0;

void fun_29d3() {
    __asm__("cli ");
    goto fork;
}

int64_t __ctype_b_loc = 0x24f0;

void fun_29e3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2500;

void fun_29f3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2870(int64_t rdi, ...);

void fun_2630(int64_t rdi, int64_t rsi);

void fun_2610(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void** infile = reinterpret_cast<void**>(0);

int32_t eolchar = -1;

void** fun_28b0(void** rdi, int64_t rsi, void** rdx, void** rcx);

void** xnmalloc(void** rdi, int64_t rsi, void** rdx, void** rcx);

int64_t xstrdup(void** rdi, int64_t rsi, void** rdx, void** rcx);

void fun_2930();

struct s10 {
    signed char[1] pad1;
    void** f1;
};

struct s10* fun_2740(void** rdi, int64_t rsi, void** rdx, void** rcx);

signed char unbuffered = 0;

int64_t fun_2840(void** rdi, void** rsi, int64_t rdx, void** rcx);

void fun_25f0(void** rdi, void** rsi, int64_t rdx, void** rcx);

void** quote(void** rdi, ...);

void** umaxtostr(void** rdi, void* rsi, void** rdx, void** rcx);

void fun_2890(void** rdi, void*** rsi, void** rdx);

int32_t xstrtoumax(void** rdi);

int32_t fun_27a0(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fd_reopen();

void** gf338 = reinterpret_cast<void**>(0);

int32_t fun_2910();

void** xalignalloc(int64_t rdi, void** rsi, void** rdx);

int32_t usage();

void** optarg = reinterpret_cast<void**>(0);

void** xdectoumax(void** rdi, ...);

int32_t optind = 0;

int64_t fun_2680(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t Version = 0xb245;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

void** fun_26b0(void** rdi, ...);

void** gf330 = reinterpret_cast<void**>(0);

uint32_t gf318 = 0;

void fun_27c0(int64_t rdi, void** rsi, void** rdx);

void fun_25b0(int64_t rdi);

void fun_29c0(int64_t rdi, int64_t rsi, void* rdx);

void fun_2a43(int32_t edi, void** rsi) {
    void** r14_3;
    void** r13_4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** r12_8;
    void*** rsp9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** rax15;
    void*** rsp16;
    int64_t rdx17;
    uint32_t eax18;
    void** rdx19;
    void** rsi20;
    void** v21;
    void** rax22;
    void*** rsp23;
    void** r15_24;
    unsigned char dil25;
    void** rcx26;
    void** r8_27;
    void*** rsp28;
    unsigned char dil29;
    void** rsi30;
    void** rdi31;
    int1_t zf32;
    void** rax33;
    int64_t rsi34;
    void** v35;
    void** rdx36;
    void** rax37;
    void** rax38;
    int32_t edi39;
    void** rsi40;
    unsigned char v41;
    void** v42;
    void** rax43;
    void** rax44;
    void** rax45;
    void** rcx46;
    void** rax47;
    void** rax48;
    void** rax49;
    void** rdx50;
    void** rax51;
    void** rdi52;
    int64_t rax53;
    unsigned char v54;
    void** rdi55;
    void** r10_56;
    int64_t rax57;
    void** r11_58;
    void* rsp59;
    void** rax60;
    void** rcx61;
    void** rdx62;
    void* rsp63;
    void** rsi64;
    void** rax65;
    void** rax66;
    int32_t eax67;
    void* rsp68;
    void** edx69;
    void** esi70;
    int1_t zf71;
    uint32_t eax72;
    int64_t rsi73;
    struct s10* rax74;
    int1_t zf75;
    void** rax76;
    int64_t rax77;
    void** rax78;
    int1_t zf79;
    unsigned char al80;
    void** rax81;
    void* rsp82;
    void** r10_83;
    int1_t zf84;
    int64_t rdi85;
    void** rax86;
    int1_t zf87;
    int64_t rax88;
    int1_t zf89;
    void** rax90;
    void** rax91;
    void** rcx92;
    void** rax93;
    void** rax94;
    void** rax95;
    void** rax96;
    void** rax97;
    int1_t zf98;
    void** rax99;
    void** rax100;
    void** rax101;
    void** rax102;
    void** v103;
    void** rax104;
    int64_t rsi105;
    void** rcx106;
    struct s10* rax107;
    void** r9_108;
    void** rax109;
    void** rax110;
    uint32_t edi111;
    uint32_t ecx112;
    uint32_t eax113;
    unsigned char al114;
    void** rax115;
    int1_t zf116;
    void** esi117;
    void** edx118;
    void** rdi119;
    void** rax120;
    void** rdi121;
    void** rax122;
    uint32_t edi123;
    void** rax124;
    void** rsi125;
    void** r15_126;
    unsigned char dil127;
    uint32_t eax128;
    int1_t zf129;
    void** tmp64_130;
    int1_t cf131;
    void** rax132;
    void** rcx133;
    void** rdx134;
    void** rax135;
    void** rax136;
    void** rax137;
    void** rax138;
    void** rax139;
    void** rdi140;
    void** rax141;
    void** rcx142;
    void** rax143;
    void** rsi144;
    void** rax145;
    void** rdi146;
    int32_t eax147;
    void** v148;
    void** rdi149;
    void** rax150;
    int1_t below_or_equal151;
    int32_t eax152;
    int32_t eax153;
    int32_t eax154;
    void** rcx155;
    void** rax156;
    int32_t eax157;
    void** rcx158;
    void** rsi159;
    void** rax160;
    void** rax161;
    void** r13_162;
    void** rax163;
    void** rbp164;
    void** rax165;
    void** rax166;
    void** rax167;
    void** rax168;
    void* rsp169;
    void* rsp170;
    void** rdi171;
    void** rax172;
    int64_t rdi173;
    int64_t rax174;
    void** rdi175;
    int64_t rcx176;
    void** rdi177;
    void** rax178;
    void** rdi179;
    int1_t zf180;
    void** rdi181;
    void** rax182;
    int32_t eax183;
    void** rax184;
    int1_t zf185;
    int1_t sf186;
    int64_t rax187;
    int64_t rax188;
    int64_t rcx189;
    void* rsi190;
    int32_t eax191;
    void** rdi192;
    void** rax193;
    void** rax194;
    void** rax195;
    void** r13_196;
    uint32_t eax197;
    void** r12_198;
    void** rax199;
    void** rax200;
    int1_t zf201;
    void* rsp202;
    void* rsp203;
    int64_t v204;
    int64_t rax205;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>(0xe8e0);
    r13_4 = reinterpret_cast<void**>("0123456789C:a:b:del:n:t:ux");
    *reinterpret_cast<int32_t*>(&rbp5) = edi;
    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
    rbx6 = rsi;
    rdi7 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi7);
    fun_2870(6, 6);
    fun_2630("coreutils", "/usr/local/share/locale");
    r12_8 = reinterpret_cast<void**>(0xb080);
    fun_2610("coreutils", "/usr/local/share/locale");
    atexit(0x5ba0, "/usr/local/share/locale");
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v10 = reinterpret_cast<void**>(0);
    infile = reinterpret_cast<void**>("-");
    outbase = reinterpret_cast<void**>("x");
    v11 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&v12) = 0;
    v13 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&v14) = 0;
    goto addr_2b0b_2;
    addr_40d3_3:
    rax15 = fun_2560();
    rsp16 = rsp16 - 8 + 8;
    *reinterpret_cast<void***>(rax15) = reinterpret_cast<void**>(75);
    goto addr_40de_4;
    addr_2b4d_5:
    *reinterpret_cast<uint32_t*>(&rdx17) = eax18;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rdx17 * 4)))) + reinterpret_cast<unsigned char>(r12_8);
    while (1) {
        addr_33d4_6:
        rdx19 = v13;
        rsi20 = v21;
        rax22 = safe_read();
        rsp23 = rsp23 - 8 + 8;
        rbx6 = rax22;
        if (rax22 == 0xffffffffffffffff) 
            goto addr_453e_7;
        if (!rax22) {
            if (r15_24) {
                rsi20 = v12;
                rdx19 = r15_24;
                dil25 = reinterpret_cast<uint1_t>(rbp5 == 0);
                cwrite(dil25, rsi20, rdx19, dil25, rsi20, rdx19);
                rsp23 = rsp23 - 8 + 8;
            }
            fun_2540(v12, rsi20, rdx19, rcx26, r8_27);
            rsp28 = rsp23 - 8 + 8;
            goto addr_3900_12;
        } else {
            r14_3 = v21;
            goto addr_347e_14;
        }
        addr_3a65_15:
        rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(rbx6));
        cwrite(dil29, r14_3, rbx6, dil29, r14_3, rbx6);
        rsp23 = rsp23 - 8 + 8;
        continue;
        while (1) {
            addr_3533_16:
            rsi30 = r14_3;
            rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(r13_4));
            r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r13_4));
            rdi31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v12) + reinterpret_cast<unsigned char>(r15_24));
            r15_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_24) + reinterpret_cast<unsigned char>(r13_4));
            fun_27e0(rdi31, rsi30, r13_4, rcx26);
            rsp23 = rsp23 - 8 + 8;
            while (1) {
                addr_3550_17:
                *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<unsigned char*>(&v11);
                *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                zf32 = v14 == 0;
                if (!zf32) {
                    *reinterpret_cast<uint32_t*>(&rcx26) = 0;
                    *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                }
                if (!zf32) {
                    rbp5 = reinterpret_cast<void**>(0);
                }
                *reinterpret_cast<unsigned char*>(&v11) = *reinterpret_cast<unsigned char*>(&rcx26);
                while (1) {
                    if (!rbx6) 
                        goto addr_33d4_6;
                    addr_347e_14:
                    rax33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r15_24));
                    *reinterpret_cast<int32_t*>(&rsi34) = eolchar;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi34) + 4) = 0;
                    v35 = rax33;
                    r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(rax33));
                    if (reinterpret_cast<unsigned char>(r13_4) <= reinterpret_cast<unsigned char>(rbx6)) {
                        rdx36 = r13_4;
                        v14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r13_4) + 0xffffffffffffffff);
                        rax37 = fun_28b0(r14_3, rsi34, rdx36, rcx26);
                        rsp23 = rsp23 - 8 + 8;
                        r8_27 = rax37;
                        if (!r15_24) 
                            goto addr_34c1_24;
                    } else {
                        rdx36 = rbx6;
                        *reinterpret_cast<int32_t*>(&r13_4) = 0;
                        *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
                        rax38 = fun_28b0(r14_3, rsi34, rdx36, rcx26);
                        rsp23 = rsp23 - 8 + 8;
                        v14 = reinterpret_cast<void**>(0);
                        r8_27 = rax38;
                        if (!r15_24) 
                            goto addr_34c1_24;
                    }
                    *reinterpret_cast<unsigned char*>(&edi39) = reinterpret_cast<uint1_t>(rbp5 == 0);
                    if (r8_27) 
                        goto addr_3a10_27;
                    if (*reinterpret_cast<unsigned char*>(&edi39)) 
                        goto addr_3a10_27; else 
                        goto addr_3442_29;
                    addr_344d_30:
                    dil29 = reinterpret_cast<uint1_t>(rbp5 == 0);
                    if (!v14) 
                        goto addr_3a65_15;
                    rsi40 = r14_3;
                    rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(r13_4));
                    r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r13_4));
                    cwrite(dil29, rsi40, r13_4, dil29, rsi40, r13_4);
                    rsp23 = rsp23 - 8 + 8;
                    *reinterpret_cast<int32_t*>(&rbp5) = 0;
                    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
                    continue;
                    addr_45e1_32:
                    *reinterpret_cast<unsigned char*>(&v11) = 1;
                    continue;
                    while (1) {
                        addr_4179_33:
                        v14 = reinterpret_cast<void**>(0);
                        r14_3 = rbx6 + 0xffffffffffffffff;
                        *reinterpret_cast<uint32_t*>(&r12_8) = 1;
                        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                        while (1) {
                            addr_3759_34:
                            v41 = 0;
                            *reinterpret_cast<unsigned char*>(&v42) = 1;
                            v12 = rbp5;
                            rbp5 = v11;
                            while (1) {
                                if (reinterpret_cast<signed char>(v12) <= reinterpret_cast<signed char>(v14)) 
                                    goto addr_4361_36;
                                if (r13_4 == 0xffffffffffffffff) 
                                    goto addr_425a_38; else 
                                    goto addr_3787_39;
                                addr_42ab_40:
                                r13_4 = rax43;
                                rbx6 = r8_27;
                                continue;
                                addr_4278_41:
                                rax44 = quotearg_n_style_colon();
                                r12_8 = rax44;
                                fun_2560();
                                rax43 = fun_28a0();
                                rsp28 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8;
                                goto addr_42ab_40;
                                while (1) {
                                    addr_35a5_42:
                                    rax45 = safe_read();
                                    rsp28 = rsp28 - 8 + 8;
                                    if (rax45 != 0xffffffffffffffff) {
                                        if (!rax45) 
                                            goto addr_3ed9_44;
                                        rcx46 = v21;
                                        r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx46) + reinterpret_cast<unsigned char>(rax45));
                                        if (rcx46 == r12_8) 
                                            continue;
                                        rbp5 = rcx46;
                                        goto addr_3608_47;
                                    }
                                    addr_43e6_48:
                                    quotearg_n_style_colon();
                                    fun_2560();
                                    fun_28a0();
                                    quotearg_n_style_colon();
                                    fun_2560();
                                    fun_28a0();
                                    rsp16 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                    addr_444c_50:
                                    rax47 = fun_2560();
                                    rsp16 = rsp16 - 8 + 8;
                                    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax47) == 29)) {
                                        *reinterpret_cast<void***>(rax47) = reinterpret_cast<void**>(0);
                                    }
                                    addr_40de_4:
                                    rax48 = quotearg_n_style_colon();
                                    rax49 = fun_2640();
                                    r12_8 = rax49;
                                    fun_2560();
                                    rcx46 = rax48;
                                    rdx50 = r12_8;
                                    fun_28a0();
                                    *reinterpret_cast<int32_t*>(&rbp5) = 0;
                                    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
                                    rax51 = xnmalloc(v10, 32, rdx50, rcx46);
                                    rsp28 = rsp16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                    v12 = rax51;
                                    rbx6 = rax51;
                                    do {
                                        next_file_name();
                                        rdi52 = outfile;
                                        ++rbp5;
                                        rbx6 = rbx6 + 32;
                                        rax53 = xstrdup(rdi52, 32, rdx50, rcx46);
                                        rsp28 = rsp28 - 8 + 8 - 8 + 8;
                                        *reinterpret_cast<int32_t*>(rbx6 + 0xffffffffffffffe8) = -1;
                                        *reinterpret_cast<int64_t*>(rbx6 + 0xffffffffffffffe0) = rax53;
                                        *reinterpret_cast<int64_t*>(rbx6 + 0xfffffffffffffff0) = 0;
                                        *reinterpret_cast<int32_t*>(rbx6 + 0xfffffffffffffff8) = 0;
                                    } while (v10 != rbp5);
                                    v14 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r13_4) = 1;
                                    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
                                    *reinterpret_cast<unsigned char*>(&v35) = 0;
                                    *reinterpret_cast<unsigned char*>(&v42) = 0;
                                    v54 = 0;
                                    continue;
                                    addr_43b3_56:
                                    quotearg_n_style_colon();
                                    fun_2560();
                                    fun_28a0();
                                    rsp28 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8;
                                    goto addr_43e6_48;
                                    while (1) {
                                        addr_3ae3_57:
                                        rdi55 = *reinterpret_cast<void***>(r10_56 + 16);
                                        rax57 = rpl_fclose(rdi55, rdi55);
                                        rsp28 = rsp28 - 8 + 8;
                                        r10_56 = r10_56;
                                        r11_58 = r11_58;
                                        if (*reinterpret_cast<int32_t*>(&rax57)) {
                                            while (1) {
                                                v10 = r11_58;
                                                quotearg_n_style_colon();
                                                r11_58 = v10;
                                                fun_28a0();
                                                rsp59 = reinterpret_cast<void*>(rsp28 - 8 + 8 - 8 + 8);
                                                addr_42eb_59:
                                                rax60 = quotearg_n_style_colon();
                                                fun_2560();
                                                rcx61 = rax60;
                                                rdx62 = reinterpret_cast<void**>("%s");
                                                fun_28a0();
                                                rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8);
                                                addr_431e_60:
                                                rsi64 = infile;
                                                rax65 = quotearg_style(4, rsi64, rdx62, rcx61);
                                                r13_4 = rax65;
                                                rax66 = fun_2640();
                                                r12_8 = rax66;
                                                fun_2560();
                                                fun_28a0();
                                                rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp63) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                                while (1) {
                                                    addr_4361_36:
                                                    r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_8) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_8) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(v41 < 1)))))));
                                                    if (!v11) {
                                                        while (reinterpret_cast<unsigned char>(v10) >= reinterpret_cast<unsigned char>(r12_8)) {
                                                            ++r12_8;
                                                            cwrite(1, 0, 0, 1, 0, 0);
                                                            rsp28 = rsp28 - 8 + 8;
                                                        }
                                                    }
                                                    while (1) {
                                                        addr_3900_12:
                                                        eax67 = fun_2710();
                                                        rsp68 = reinterpret_cast<void*>(rsp28 - 8 + 8);
                                                        if (eax67) 
                                                            goto addr_4465_65;
                                                        rcx46 = outfile;
                                                        edx69 = filter_pid;
                                                        esi70 = output_desc;
                                                        closeout(0, esi70, edx69, rcx46);
                                                        fun_2930();
                                                        rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8);
                                                        while (1) {
                                                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r11_58) == 32) || (zf71 = filter_command == 0, zf71)) {
                                                                *reinterpret_cast<unsigned char*>(&v42) = 1;
                                                            }
                                                            while (1) {
                                                                if (*reinterpret_cast<unsigned char*>(&v35)) 
                                                                    goto addr_3ae3_57;
                                                                addr_3950_70:
                                                                if (*reinterpret_cast<signed char*>(&r15_24) && (++v14, v10 == v14)) {
                                                                    if (!*reinterpret_cast<unsigned char*>(&v42)) 
                                                                        goto addr_3f4e_72;
                                                                    v14 = reinterpret_cast<void**>(0);
                                                                    eax72 = *reinterpret_cast<unsigned char*>(&v42);
                                                                    *reinterpret_cast<unsigned char*>(&v42) = 0;
                                                                    v54 = *reinterpret_cast<unsigned char*>(&eax72);
                                                                }
                                                                while (1) {
                                                                    if (r12_8 == rbx6) 
                                                                        goto addr_35a5_42;
                                                                    rbp5 = rbx6;
                                                                    addr_3608_47:
                                                                    *reinterpret_cast<int32_t*>(&rsi73) = eolchar;
                                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi73) + 4) = 0;
                                                                    r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_8) - reinterpret_cast<unsigned char>(rbp5));
                                                                    rax74 = fun_2740(rbp5, rsi73, r14_3, rcx46);
                                                                    rsp28 = rsp28 - 8 + 8;
                                                                    if (!rax74) {
                                                                        rbx6 = r12_8;
                                                                        *reinterpret_cast<int32_t*>(&r15_24) = 0;
                                                                        *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                                                    } else {
                                                                        rbx6 = reinterpret_cast<void**>(&rax74->f1);
                                                                        *reinterpret_cast<int32_t*>(&r15_24) = 1;
                                                                        *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                                                        r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(rbp5));
                                                                    }
                                                                    if (!v11) 
                                                                        break;
                                                                    if (r13_4 == v11) {
                                                                        zf75 = unbuffered == 0;
                                                                        if (!zf75) {
                                                                            rax76 = full_write(1, rbp5, r14_3, rcx46);
                                                                            rsp28 = rsp28 - 8 + 8;
                                                                            if (rax76 != r14_3) 
                                                                                goto addr_3b3e_82;
                                                                        } else {
                                                                            rcx46 = stdout;
                                                                            rax77 = fun_2840(rbp5, r14_3, 1, rcx46);
                                                                            rsp28 = rsp28 - 8 + 8;
                                                                            if (rax77 - 1) 
                                                                                goto addr_39d1_84;
                                                                        }
                                                                    }
                                                                    if (!*reinterpret_cast<signed char*>(&r15_24)) 
                                                                        continue;
                                                                    rax78 = r13_4 + 1;
                                                                    zf79 = v10 == r13_4;
                                                                    *reinterpret_cast<int32_t*>(&r13_4) = 1;
                                                                    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
                                                                    if (zf79) 
                                                                        continue;
                                                                    r13_4 = rax78;
                                                                }
                                                                al80 = ofile_open(v12, v14, v10, rcx46);
                                                                *reinterpret_cast<unsigned char*>(&v35) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&v35) | al80);
                                                                rax81 = fun_2560();
                                                                rsp82 = reinterpret_cast<void*>(rsp28 - 8 + 8 - 8 + 8);
                                                                r10_83 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v14) << 5) + reinterpret_cast<unsigned char>(v12));
                                                                zf84 = unbuffered == 0;
                                                                if (!zf84) {
                                                                    *reinterpret_cast<void***>(&rdi85) = *reinterpret_cast<void***>(r10_83 + 8);
                                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi85) + 4) = 0;
                                                                    rax86 = full_write(rdi85, rbp5, r14_3, rcx46);
                                                                    rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp82) - 8 + 8);
                                                                    r10_56 = r10_83;
                                                                    r11_58 = rax81;
                                                                    if (rax86 == r14_3) 
                                                                        break;
                                                                    zf87 = filter_command == 0;
                                                                    if (zf87) 
                                                                        goto addr_3ab3_91;
                                                                    if (*reinterpret_cast<void***>(r11_58) != 32) 
                                                                        goto addr_3ab3_91;
                                                                } else {
                                                                    rcx46 = *reinterpret_cast<void***>(r10_83 + 16);
                                                                    rax88 = fun_2840(rbp5, r14_3, 1, rcx46);
                                                                    rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp82) - 8 + 8);
                                                                    r10_56 = r10_83;
                                                                    r11_58 = rax81;
                                                                    if (!(rax88 - 1)) 
                                                                        break;
                                                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r11_58) == 32)) 
                                                                        goto addr_36bf_95;
                                                                    zf89 = filter_command == 0;
                                                                    if (zf89) 
                                                                        goto addr_36bf_95;
                                                                }
                                                            }
                                                        }
                                                        addr_36bf_95:
                                                        v10 = r11_58;
                                                        quotearg_n_style_colon();
                                                        r11_58 = v10;
                                                        fun_28a0();
                                                        rsp28 = rsp28 - 8 + 8 - 8 + 8;
                                                        if (reinterpret_cast<unsigned char>(v10) < reinterpret_cast<unsigned char>(v11)) 
                                                            goto addr_44a5_97;
                                                        if (reinterpret_cast<unsigned char>(v10) > reinterpret_cast<unsigned char>(rbp5)) 
                                                            goto addr_44a5_97;
                                                        rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) / reinterpret_cast<unsigned char>(v10));
                                                        if (reinterpret_cast<unsigned char>(v11) <= reinterpret_cast<unsigned char>(1)) 
                                                            goto addr_4179_33;
                                                        r12_8 = v11 + 0xffffffffffffffff;
                                                        r15_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) * reinterpret_cast<unsigned char>(r12_8) + 0xffffffffffffffff);
                                                        r14_3 = r15_24;
                                                        if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(r15_24)) 
                                                            goto addr_3740_101;
                                                        r13_4 = reinterpret_cast<void**>(0xffffffffffffffff);
                                                        rax90 = fun_26e0();
                                                        rsp28 = rsp28 - 8 + 8;
                                                        v14 = r15_24;
                                                        if (reinterpret_cast<signed char>(rax90) >= reinterpret_cast<signed char>(0)) 
                                                            goto addr_3759_34;
                                                        rax91 = quotearg_n_style_colon();
                                                        r12_8 = rax91;
                                                        fun_2560();
                                                        rcx92 = r12_8;
                                                        fun_28a0();
                                                        r13_4 = reinterpret_cast<void**>(0xffffffffffffffff);
                                                        rax93 = fun_26e0();
                                                        rsp28 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                                        r14_3 = v21;
                                                        if (reinterpret_cast<signed char>(rax93) >= reinterpret_cast<signed char>(0)) {
                                                            while (reinterpret_cast<signed char>(rbp5) > reinterpret_cast<signed char>(rbx6)) {
                                                                if (1) {
                                                                    rax94 = safe_read();
                                                                    rsp28 = rsp28 - 8 + 8;
                                                                    r13_4 = rax94;
                                                                    if (rax94 == 0xffffffffffffffff) 
                                                                        goto addr_43b3_56;
                                                                    if (!rax94) 
                                                                        goto addr_405a_109;
                                                                }
                                                                rax95 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) - reinterpret_cast<unsigned char>(rbx6));
                                                                if (reinterpret_cast<unsigned char>(rax95) <= reinterpret_cast<unsigned char>(r13_4)) {
                                                                    r13_4 = rax95;
                                                                }
                                                                rax96 = full_write(1, r14_3, r13_4, rcx92);
                                                                rsp28 = rsp28 - 8 + 8;
                                                                if (r13_4 != rax96 && ((rax97 = fun_2560(), rsp28 = rsp28 - 8 + 8, zf98 = filter_command == 0, zf98) || *reinterpret_cast<void***>(rax97) != 32)) {
                                                                    r12_8 = rax97;
                                                                    rax99 = quotearg_n_style_colon();
                                                                    rcx92 = rax99;
                                                                    fun_28a0();
                                                                    rsp28 = rsp28 - 8 + 8 - 8 + 8;
                                                                }
                                                                rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(r13_4));
                                                                r13_4 = reinterpret_cast<void**>(0xffffffffffffffff);
                                                            }
                                                            continue;
                                                            addr_405a_109:
                                                            continue;
                                                        }
                                                        rax100 = quotearg_n_style_colon();
                                                        r12_8 = rax100;
                                                        fun_2560();
                                                        fun_28a0();
                                                        rsp28 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8;
                                                        addr_425a_38:
                                                        rax101 = safe_read();
                                                        rsp28 = rsp28 - 8 + 8;
                                                        r13_4 = rax101;
                                                        if (reinterpret_cast<int1_t>(rax101 == 0xffffffffffffffff)) 
                                                            goto addr_4278_41;
                                                        addr_3787_39:
                                                        if (!r13_4) 
                                                            break;
                                                        rax102 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(v14));
                                                        v41 = 0;
                                                        if (reinterpret_cast<unsigned char>(rax102) > reinterpret_cast<unsigned char>(r13_4)) 
                                                            goto addr_37ab_117;
                                                        r13_4 = rax102;
                                                        addr_37ab_117:
                                                        r10_56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) + reinterpret_cast<unsigned char>(r13_4));
                                                        v103 = r13_4;
                                                        r13_4 = v21;
                                                        r8_27 = rbx6;
                                                        rbx6 = v10;
                                                        r15_24 = r10_56;
                                                        while (rax43 = reinterpret_cast<void**>(0xffffffffffffffff), r15_24 != r13_4) {
                                                            rax104 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) - reinterpret_cast<unsigned char>(v14));
                                                            if (reinterpret_cast<signed char>(rax104) < reinterpret_cast<signed char>(0)) {
                                                                rax104 = reinterpret_cast<void**>(0);
                                                            }
                                                            *reinterpret_cast<int32_t*>(&rsi105) = eolchar;
                                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi105) + 4) = 0;
                                                            if (reinterpret_cast<unsigned char>(rax104) > reinterpret_cast<unsigned char>(v103)) {
                                                                rax104 = v103;
                                                            }
                                                            rcx106 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v103) - reinterpret_cast<unsigned char>(rax104));
                                                            rax107 = fun_2740(reinterpret_cast<unsigned char>(r13_4) + reinterpret_cast<unsigned char>(rax104), rsi105, rcx106, rcx106);
                                                            rsp28 = rsp28 - 8 + 8;
                                                            r8_27 = r8_27;
                                                            if (!rax107) {
                                                                v54 = 0;
                                                                r9_108 = r15_24;
                                                            } else {
                                                                v54 = 1;
                                                                r9_108 = reinterpret_cast<void**>(&rax107->f1);
                                                            }
                                                            rax109 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_108) - reinterpret_cast<unsigned char>(r13_4));
                                                            v35 = rax109;
                                                            if (r12_8 == rbp5) {
                                                                v42 = r9_108;
                                                                rax110 = full_write(1, r13_4, rax109, rcx106);
                                                                rsp28 = rsp28 - 8 + 8;
                                                                r9_108 = v42;
                                                                r8_27 = r8_27;
                                                                if (v35 != rax110) 
                                                                    goto addr_3ea6_129;
                                                            } else {
                                                                if (!rbp5) {
                                                                    edi111 = *reinterpret_cast<unsigned char*>(&v42);
                                                                    cwrite(*reinterpret_cast<unsigned char*>(&edi111), r13_4, v35);
                                                                    rsp28 = rsp28 - 8 + 8;
                                                                    r8_27 = r8_27;
                                                                    r9_108 = r9_108;
                                                                }
                                                            }
                                                            r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<unsigned char>(v35));
                                                            v14 = r13_4;
                                                            ecx112 = v54;
                                                            while (reinterpret_cast<signed char>(r13_4) > reinterpret_cast<signed char>(r14_3) || *reinterpret_cast<unsigned char*>(&ecx112)) {
                                                                eax113 = ecx112 ^ 1;
                                                                al114 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax113) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_24 == r9_108)));
                                                                if (al114) 
                                                                    goto addr_3cba_135;
                                                                ++r12_8;
                                                                if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rbp5)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_8) > reinterpret_cast<unsigned char>(rbp5)))) 
                                                                    goto addr_3900_12;
                                                                r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r8_27));
                                                                if (rbx6 == r12_8) {
                                                                    r14_3 = v12 + 0xffffffffffffffff;
                                                                }
                                                                if (reinterpret_cast<signed char>(r13_4) <= reinterpret_cast<signed char>(r14_3)) {
                                                                    ecx112 = 0;
                                                                } else {
                                                                    if (rbp5) 
                                                                        continue;
                                                                    *reinterpret_cast<unsigned char*>(&v42) = *reinterpret_cast<unsigned char*>(&ecx112);
                                                                    cwrite(1, 0, 0);
                                                                    rsp28 = rsp28 - 8 + 8;
                                                                    ecx112 = *reinterpret_cast<unsigned char*>(&v42);
                                                                    r9_108 = r9_108;
                                                                    r8_27 = r8_27;
                                                                }
                                                            }
                                                            addr_3cbe_143:
                                                            *reinterpret_cast<uint32_t*>(&rax102) = v54;
                                                            r13_4 = r9_108;
                                                            v103 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v103) - reinterpret_cast<unsigned char>(v35));
                                                            *reinterpret_cast<unsigned char*>(&v42) = *reinterpret_cast<unsigned char*>(&rax102);
                                                            continue;
                                                            addr_3cba_135:
                                                            v41 = al114;
                                                            goto addr_3cbe_143;
                                                        }
                                                        goto addr_42ab_40;
                                                        addr_3ea6_129:
                                                        rax115 = fun_2640();
                                                        r12_8 = rax115;
                                                        fun_2560();
                                                        rcx46 = r12_8;
                                                        fun_28a0();
                                                        rsp28 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8;
                                                        addr_3ed9_44:
                                                        if (v11) 
                                                            continue;
                                                        if (v54) {
                                                            addr_3f4e_72:
                                                            rbx6 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(v10)));
                                                        } else {
                                                            rbx6 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v14)));
                                                        }
                                                        rbp5 = v12;
                                                        r13_4 = v10;
                                                        *reinterpret_cast<uint32_t*>(&r12_8) = 0;
                                                        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                                                        do {
                                                            if (reinterpret_cast<unsigned char>(rbx6) <= reinterpret_cast<unsigned char>(r12_8) && (zf116 = elide_empty_files == 0, zf116)) {
                                                                ofile_open(v12, r12_8, r13_4, rcx46);
                                                                rsp28 = rsp28 - 8 + 8;
                                                            }
                                                            esi117 = *reinterpret_cast<void***>(rbp5 + 8);
                                                            if (reinterpret_cast<signed char>(esi117) >= reinterpret_cast<signed char>(0)) {
                                                                edx118 = *reinterpret_cast<void***>(rbp5 + 24);
                                                                rdi119 = *reinterpret_cast<void***>(rbp5 + 16);
                                                                rcx46 = *reinterpret_cast<void***>(rbp5);
                                                                closeout(rdi119, esi117, edx118, rcx46);
                                                                rsp28 = rsp28 - 8 + 8;
                                                            }
                                                            ++r12_8;
                                                            *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0xfffffffe);
                                                            rbp5 = rbp5 + 32;
                                                        } while (r13_4 != r12_8);
                                                    }
                                                }
                                                addr_3b3e_82:
                                                rax120 = fun_2640();
                                                r12_8 = rax120;
                                                fun_2560();
                                                rcx26 = r12_8;
                                                rdx36 = reinterpret_cast<void**>("%s");
                                                fun_28a0();
                                                rsp23 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8;
                                                goto addr_3b71_152;
                                                addr_39d1_84:
                                                rdi121 = stdout;
                                                fun_25f0(rdi121, r14_3, 1, rcx46);
                                                rax122 = fun_2640();
                                                fun_2560();
                                                rcx26 = rax122;
                                                edi39 = 1;
                                                fun_28a0();
                                                rsp23 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                                addr_3a10_27:
                                                edi123 = *reinterpret_cast<unsigned char*>(&edi39);
                                                rdx36 = r15_24;
                                                v42 = r8_27;
                                                cwrite(*reinterpret_cast<unsigned char*>(&edi123), v12, rdx36, *reinterpret_cast<unsigned char*>(&edi123), v12, rdx36);
                                                rsp23 = rsp23 - 8 + 8;
                                                r12_8 = v13;
                                                r8_27 = v42;
                                                if (reinterpret_cast<unsigned char>(v13) < reinterpret_cast<unsigned char>(r15_24)) {
                                                    addr_3b71_152:
                                                    rax124 = xrealloc(v12, r12_8, rdx36, rcx26);
                                                    rsp23 = rsp23 - 8 + 8;
                                                    rbp5 = v35;
                                                    r8_27 = v42;
                                                    v12 = rax124;
                                                } else {
                                                    rbp5 = v35;
                                                }
                                                addr_34c1_24:
                                                if (!r8_27) {
                                                    *reinterpret_cast<int32_t*>(&r15_24) = 0;
                                                    *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                                    addr_3442_29:
                                                    if (!*reinterpret_cast<unsigned char*>(&v11)) 
                                                        goto addr_344d_30;
                                                } else {
                                                    r8_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_27) - reinterpret_cast<unsigned char>(r14_3));
                                                    rsi125 = r14_3;
                                                    r15_126 = r8_27 + 1;
                                                    dil127 = reinterpret_cast<uint1_t>(rbp5 == 0);
                                                    rdx36 = r15_126;
                                                    rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r15_126));
                                                    rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(r15_126));
                                                    r14_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r15_126));
                                                    cwrite(dil127, rsi125, rdx36, dil127, rsi125, rdx36);
                                                    rsp23 = rsp23 - 8 + 8;
                                                    if (!v14) {
                                                        *reinterpret_cast<unsigned char*>(&v11) = reinterpret_cast<uint1_t>(!!rbx6);
                                                        eax128 = *reinterpret_cast<unsigned char*>(&v11);
                                                        *reinterpret_cast<int32_t*>(&r15_24) = 0;
                                                        *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                                        if (!*reinterpret_cast<signed char*>(&eax128)) 
                                                            goto addr_45e1_32; else 
                                                            goto addr_3a5d_157;
                                                    } else {
                                                        r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) - reinterpret_cast<unsigned char>(r15_126));
                                                        zf129 = r13_4 == 0;
                                                        *reinterpret_cast<unsigned char*>(&v11) = reinterpret_cast<uint1_t>(!zf129);
                                                        if (zf129) 
                                                            goto addr_4498_159;
                                                        *reinterpret_cast<int32_t*>(&r15_24) = 0;
                                                        *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                                        goto addr_350b_161;
                                                    }
                                                }
                                                addr_45b6_162:
                                                if (!v14) {
                                                    addr_45d3_163:
                                                    if (!rbx6) 
                                                        goto addr_45dc_164;
                                                } else {
                                                    if (r13_4) {
                                                        addr_350b_161:
                                                        if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_8) - reinterpret_cast<unsigned char>(r15_24)) >= reinterpret_cast<unsigned char>(r13_4)) 
                                                            goto addr_3533_16; else 
                                                            goto addr_3516_166;
                                                    } else {
                                                        if (v14) 
                                                            goto addr_3550_17; else 
                                                            goto addr_45d3_163;
                                                    }
                                                }
                                                addr_3a5d_157:
                                                r13_4 = rbx6;
                                                goto addr_350b_161;
                                                addr_3516_166:
                                                tmp64_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(v13));
                                                cf131 = reinterpret_cast<unsigned char>(tmp64_130) < reinterpret_cast<unsigned char>(r12_8);
                                                r12_8 = tmp64_130;
                                                if (!cf131) 
                                                    goto addr_3521_168;
                                                xalloc_die();
                                                rsp28 = rsp23 - 8 + 8;
                                                continue;
                                                addr_44a5_97:
                                                fun_26f0("n && k <= n && n <= file_size", "src/split.c", 0x363, "lines_chunk_split");
                                                quote(r15_24, r15_24);
                                                fun_2640();
                                                fun_28a0();
                                                fun_2640();
                                                fun_28a0();
                                                fun_2640();
                                                fun_28a0();
                                                rsp23 = rsp28 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                                addr_453e_7:
                                                rax132 = quotearg_n_style_colon();
                                                fun_2560();
                                                rcx133 = rax132;
                                                rdx134 = reinterpret_cast<void**>("%s");
                                                fun_28a0();
                                                rsp9 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
                                                addr_4571_173:
                                                *reinterpret_cast<int32_t*>(&v14) = *reinterpret_cast<int32_t*>(&r8_27);
                                                rax135 = umaxtostr(v10, rsp9 + 0x110, rdx134, rcx133);
                                                r12_8 = rax135;
                                                rax136 = fun_2640();
                                                *reinterpret_cast<int32_t*>(&r8_27) = *reinterpret_cast<int32_t*>(&v14);
                                                *reinterpret_cast<int32_t*>(&r8_27 + 4) = 0;
                                                rcx26 = r12_8;
                                                rdx36 = rax136;
                                                fun_28a0();
                                                rsp23 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
                                                goto addr_45b6_162;
                                            }
                                            addr_3ab3_91:
                                            v10 = r11_58;
                                            rax137 = quotearg_n_style_colon();
                                            r11_58 = v10;
                                            rcx46 = rax137;
                                            fun_28a0();
                                            rsp28 = rsp28 - 8 + 8 - 8 + 8;
                                        } else {
                                            *reinterpret_cast<void***>(r10_56 + 16) = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<void***>(r10_56 + 8) = reinterpret_cast<void**>(0xfffffffe);
                                            goto addr_3950_70;
                                        }
                                    }
                                }
                            }
                            addr_3740_101:
                            r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) - reinterpret_cast<unsigned char>(r15_24));
                            fun_2890(v21, reinterpret_cast<unsigned char>(v21) + reinterpret_cast<unsigned char>(r15_24), r13_4);
                            rsp28 = rsp28 - 8 + 8;
                            v14 = r15_24;
                        }
                    }
                }
                addr_45dc_164:
                continue;
                addr_4465_65:
                rax138 = quotearg_n_style_colon();
                r12_8 = rax138;
                fun_2560();
                fun_28a0();
                rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp68) - 8 + 8 - 8 + 8 - 8 + 8);
                addr_4498_159:
                *reinterpret_cast<unsigned char*>(&v11) = 1;
                *reinterpret_cast<int32_t*>(&r15_24) = 0;
                *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
            }
            addr_3521_168:
            rax139 = xrealloc(v12, r12_8, rdx36, rcx26);
            rsp23 = rsp23 - 8 + 8;
            v12 = rax139;
        }
    }
    while (1) {
        addr_2b93_175:
        while (1) {
            addr_2b9f_176:
            fun_2640();
            fun_28a0();
            rsp9 = rsp9 - 8 + 8 - 8 + 8;
            goto addr_2bb4_177;
            addr_3bc6_178:
            continue;
            while (1) {
                addr_3c01_179:
                rdi140 = suffix_alphabet;
                *reinterpret_cast<int32_t*>(&r14_3) = 0;
                *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
                rax141 = fun_2660(rdi140, rdi140);
                rsp9 = rsp9 - 8 + 8;
                rcx142 = rax141;
                rax143 = r13_4;
                do {
                    rsi144 = rax143;
                    ++r14_3;
                    rdx62 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax143) % reinterpret_cast<unsigned char>(rcx142));
                    rax143 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax143) / reinterpret_cast<unsigned char>(rcx142));
                } while (reinterpret_cast<unsigned char>(rcx142) <= reinterpret_cast<unsigned char>(rsi144));
                rax145 = suffix_length;
                suffix_auto = 0;
                if (!rax145) 
                    goto addr_3bd7_182;
                if (reinterpret_cast<unsigned char>(r14_3) <= reinterpret_cast<unsigned char>(rax145)) 
                    goto addr_30d1_184;
                fun_2640();
                *reinterpret_cast<int32_t*>(&rdi146) = 1;
                *reinterpret_cast<int32_t*>(&rdi146 + 4) = 0;
                fun_28a0();
                rsp9 = rsp9 - 8 + 8 - 8 + 8;
                addr_3c6a_186:
                r8_27 = reinterpret_cast<void**>(0xb6d1);
                r13_4 = v10 + 0xffffffffffffffff;
                eax147 = xstrtoumax(rdi146);
                rsp9 = rsp9 - 8 + 8;
                if (eax147) 
                    continue;
                if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(v148)) < reinterpret_cast<unsigned char>(v10)) 
                    continue;
                if (reinterpret_cast<unsigned char>(v10) <= reinterpret_cast<unsigned char>(v148)) 
                    continue;
                r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) + reinterpret_cast<unsigned char>(v148));
                continue;
                while (1) {
                    addr_31e4_190:
                    rdi149 = numeric_suffix_start;
                    if (!rdi149) 
                        goto addr_320e_191;
                    rax150 = fun_2660(rdi149, rdi149);
                    rsp9 = rsp9 - 8 + 8;
                    below_or_equal151 = reinterpret_cast<unsigned char>(rax150) <= reinterpret_cast<unsigned char>(suffix_length);
                    *reinterpret_cast<int32_t*>(&rdx62) = 5;
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    if (!below_or_equal151) 
                        goto addr_2b9f_176;
                    addr_320e_191:
                    rbp5 = infile;
                    eax152 = fun_27a0(rbp5, "-", rdx62, rcx142);
                    rsp63 = reinterpret_cast<void*>(rsp9 - 8 + 8);
                    if (!eax152) 
                        goto addr_323e_193;
                    *reinterpret_cast<int32_t*>(&rcx61) = 0;
                    *reinterpret_cast<int32_t*>(&rcx61 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx62) = 0;
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    eax153 = fd_reopen();
                    rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                    if (eax153 < 0) 
                        goto addr_431e_60;
                    addr_323e_193:
                    eax154 = fun_29b0();
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                    if (eax154) 
                        goto addr_42eb_59;
                    if (!v13) {
                        rcx155 = gf338;
                        rdx62 = rcx155 + 0xfffffffffffe0000;
                        *reinterpret_cast<int32_t*>(&rax156) = 0x20000;
                        *reinterpret_cast<int32_t*>(&rax156 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rdx62) <= reinterpret_cast<unsigned char>(0x1ffffffffffe0000)) {
                            rax156 = rcx155;
                        }
                        v13 = rax156;
                    }
                    eax157 = fun_2910();
                    rcx158 = v13;
                    r13_4 = reinterpret_cast<void**>(0xffffffffffffffff);
                    rbp5 = reinterpret_cast<void**>(0x7fffffffffffffff);
                    rsi159 = rcx158 + 1;
                    rax160 = xalignalloc(static_cast<int64_t>(eax157), rsi159, rdx62);
                    rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8);
                    v21 = rax160;
                    if (*reinterpret_cast<uint32_t*>(&r12_8) > 1) 
                        goto addr_32b8_200;
                    *reinterpret_cast<uint32_t*>(&r12_8) = 0;
                    *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                    rax161 = fun_26e0();
                    rsp16 = rsp16 - 8 + 8;
                    rbx6 = rax161;
                    if (reinterpret_cast<signed char>(rax161) < reinterpret_cast<signed char>(0)) 
                        goto addr_444c_50;
                    r13_162 = v13;
                    r14_3 = v21;
                    do {
                        rsi159 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_3) + reinterpret_cast<unsigned char>(r12_8));
                        rdx62 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_162) - reinterpret_cast<unsigned char>(r12_8));
                        rax163 = safe_read();
                        rsp16 = rsp16 - 8 + 8;
                        if (!rax163) 
                            break;
                        if (rax163 == 0xffffffffffffffff) 
                            goto addr_40de_4;
                        rbp164 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(rax163));
                        r12_8 = rbp164;
                    } while (reinterpret_cast<unsigned char>(rbp164) < reinterpret_cast<unsigned char>(r13_162));
                    goto addr_405f_206;
                    addr_3d32_207:
                    if (reinterpret_cast<signed char>(r12_8) < reinterpret_cast<signed char>(0)) 
                        goto addr_40de_4;
                    rax165 = v13;
                    rbp5 = r12_8;
                    if (reinterpret_cast<signed char>(rax165) > reinterpret_cast<signed char>(r12_8)) {
                        rax165 = r12_8;
                    }
                    r13_4 = rax165;
                    if (reinterpret_cast<unsigned char>(r12_8) < reinterpret_cast<unsigned char>(v10)) {
                        rbp5 = v10;
                    }
                    if (reinterpret_cast<signed char>(v10) >= reinterpret_cast<signed char>(0)) 
                        goto addr_32b8_200;
                    rax166 = umaxtostr(v10, rsp16 + 0x110, rdx62, rcx158);
                    rax167 = quote(rax166, rax166);
                    rbx6 = rax167;
                    fun_2640();
                    fun_28a0();
                    rax168 = quote(r15_24, r15_24);
                    rsp169 = reinterpret_cast<void*>(rsp16 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    r12_8 = rax168;
                    while (1) {
                        fun_2640();
                        fun_28a0();
                        usage();
                        rsp170 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp169) - 8 + 8 - 8 + 8 - 8 + 8);
                        while (1) {
                            fun_2640();
                            rdi171 = optarg;
                            rax172 = xdectoumax(rdi171, rdi171);
                            rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp170) - 8 + 8 - 8 + 8);
                            v10 = rax172;
                            while (1) {
                                addr_2b0b_2:
                                *reinterpret_cast<int32_t*>(&r15_24) = optind;
                                *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                rcx142 = r14_3;
                                rdx62 = r13_4;
                                *reinterpret_cast<int32_t*>(&rdi173) = *reinterpret_cast<int32_t*>(&rbp5);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi173) + 4) = 0;
                                if (!*reinterpret_cast<int32_t*>(&r15_24)) {
                                    *reinterpret_cast<int32_t*>(&r15_24) = 1;
                                    *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0;
                                }
                                rax174 = fun_2680(rdi173, rbx6, rdx62, rcx142);
                                rsp9 = rsp9 - 8 + 8;
                                *reinterpret_cast<int32_t*>(&r8_27) = *reinterpret_cast<int32_t*>(&rax174);
                                *reinterpret_cast<int32_t*>(&r8_27 + 4) = 0;
                                if (*reinterpret_cast<int32_t*>(&rax174) == -1) 
                                    goto addr_2b79_220;
                                if (*reinterpret_cast<int32_t*>(&rax174) <= 0x83) {
                                    if (*reinterpret_cast<int32_t*>(&rax174) <= 47) {
                                        if (*reinterpret_cast<int32_t*>(&rax174) == 0xffffff7d) {
                                            rdi175 = stdout;
                                            rcx176 = Version;
                                            version_etc(rdi175, "split", "GNU coreutils", rcx176, "Torbjorn Granlund", "Richard M. Stallman", 0, rax174);
                                            fun_2930();
                                            optarg = r15_24 + 2;
                                            rdi177 = optarg;
                                            rax178 = fun_26b0(rdi177, rdi177);
                                            rsp170 = reinterpret_cast<void*>(rsp9 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                            if (!rax178) 
                                                break;
                                            fun_2640();
                                            rdi179 = rax178 + 1;
                                            xdectoumax(rdi179, rdi179);
                                            rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp170) - 8 + 8 - 8 + 8);
                                            zf180 = rax178 == optarg;
                                            if (zf180) 
                                                continue;
                                            *reinterpret_cast<void***>(rax178) = reinterpret_cast<void**>(0);
                                            fun_2640();
                                            rdi181 = optarg;
                                            rax182 = xdectoumax(rdi181, rdi181);
                                            rsp9 = rsp9 - 8 + 8 - 8 + 8;
                                            v10 = rax182;
                                            continue;
                                        } else {
                                            if (*reinterpret_cast<int32_t*>(&rax174) == 0xffffff7e) 
                                                goto addr_2b72_230;
                                        }
                                    } else {
                                        eax18 = static_cast<uint32_t>(rax174 - 48);
                                        if (eax18 <= 83) 
                                            goto addr_2b4d_5;
                                    }
                                }
                                addr_2bb4_177:
                                eax183 = usage();
                                rsp9 = rsp9 - 8 + 8;
                                if (!*reinterpret_cast<int32_t*>(&v14)) {
                                    addr_2be4_232:
                                    *reinterpret_cast<int32_t*>(&v12) = *reinterpret_cast<int32_t*>(&r15_24);
                                    v10 = reinterpret_cast<void**>(static_cast<int64_t>(eax183));
                                    *reinterpret_cast<int32_t*>(&v14) = 4;
                                    continue;
                                } else {
                                    if (*reinterpret_cast<int32_t*>(&v14) != 4) 
                                        goto addr_3bc6_178;
                                    if (!*reinterpret_cast<int32_t*>(&v12)) 
                                        goto addr_3033_235;
                                    if (*reinterpret_cast<int32_t*>(&v12) != *reinterpret_cast<int32_t*>(&r15_24)) 
                                        goto addr_2be4_232;
                                }
                                addr_3033_235:
                                rdx134 = reinterpret_cast<void**>(0x1999999999999999);
                                rcx133 = v10;
                                if (reinterpret_cast<unsigned char>(rcx133) > reinterpret_cast<unsigned char>(0x1999999999999999)) 
                                    goto addr_4571_173;
                                rdx134 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx133) * 10);
                                rax184 = reinterpret_cast<void**>(static_cast<int64_t>(eax183) + reinterpret_cast<unsigned char>(rdx134));
                                if (reinterpret_cast<unsigned char>(rax184) < reinterpret_cast<unsigned char>(rcx133)) 
                                    goto addr_4571_173;
                                v10 = rax184;
                                *reinterpret_cast<int32_t*>(&v12) = *reinterpret_cast<int32_t*>(&r15_24);
                            }
                        }
                        addr_2b79_220:
                        if (1) 
                            goto addr_306a_239;
                        zf185 = filter_command == 0;
                        if (!zf185) 
                            goto addr_2b93_175;
                        addr_306a_239:
                        if (*reinterpret_cast<int32_t*>(&v14)) {
                            if (!v10) 
                                break;
                        } else {
                            v10 = reinterpret_cast<void**>(0x3e8);
                            *reinterpret_cast<int32_t*>(&v14) = 3;
                        }
                        sf186 = eolchar < 0;
                        if (sf186) {
                            eolchar = 10;
                        }
                        *reinterpret_cast<int32_t*>(&rax187) = *reinterpret_cast<int32_t*>(&v14);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax187) + 4) = 0;
                        rdi146 = numeric_suffix_start;
                        *reinterpret_cast<uint32_t*>(&r12_8) = static_cast<uint32_t>(rax187 - 5);
                        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
                        if (!rdi146) {
                            r13_4 = v10 + 0xffffffffffffffff;
                            if (*reinterpret_cast<uint32_t*>(&r12_8) <= 2) 
                                goto addr_3c01_179;
                        } else {
                            suffix_auto = 0;
                            if (*reinterpret_cast<uint32_t*>(&r12_8) <= 2) 
                                goto addr_3c6a_186;
                        }
                        r14_3 = suffix_length;
                        if (!r14_3) {
                            addr_3bd7_182:
                            if (reinterpret_cast<unsigned char>(r14_3) < reinterpret_cast<unsigned char>(2)) {
                                r14_3 = reinterpret_cast<void**>(2);
                            }
                            suffix_length = r14_3;
                        } else {
                            addr_30d1_184:
                            suffix_auto = 0;
                        }
                        *reinterpret_cast<int32_t*>(&rax188) = optind;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax188) + 4) = 0;
                        if (*reinterpret_cast<int32_t*>(&rax188) >= *reinterpret_cast<int32_t*>(&rbp5)) 
                            goto addr_31e4_190;
                        rcx189 = *reinterpret_cast<int32_t*>(&rax188);
                        *reinterpret_cast<int32_t*>(&rdx62) = static_cast<int32_t>(rax188 + 1);
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        rsi190 = reinterpret_cast<void*>(rcx189 * 8);
                        rcx142 = *reinterpret_cast<void***>(rbx6 + rcx189 * 8);
                        optind = *reinterpret_cast<int32_t*>(&rdx62);
                        infile = rcx142;
                        if (*reinterpret_cast<int32_t*>(&rdx62) >= *reinterpret_cast<int32_t*>(&rbp5)) 
                            goto addr_31e4_190;
                        rdx62 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rsi190) + 8);
                        eax191 = *reinterpret_cast<int32_t*>(&rax188) + 2;
                        optind = eax191;
                        outbase = rdx62;
                        if (eax191 >= *reinterpret_cast<int32_t*>(&rbp5)) 
                            goto addr_31e4_190;
                        rdi192 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rsi190) + 16);
                        rax193 = quote(rdi192);
                        rsp169 = reinterpret_cast<void*>(rsp9 - 8 + 8);
                        r12_8 = rax193;
                        continue;
                        addr_2b72_230:
                        usage();
                        rsp9 = rsp9 - 8 + 8;
                        goto addr_2b79_220;
                    }
                    rax194 = quote("0", "0");
                    rbx6 = rax194;
                    rax195 = fun_2640();
                    r8_27 = rbx6;
                    rcx142 = rax195;
                    rdx62 = reinterpret_cast<void**>("%s: %s");
                    fun_28a0();
                    usage();
                    rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                    continue;
                    addr_405f_206:
                    r13_196 = gf330;
                    if (!r13_196) 
                        goto addr_40d3_3;
                    eax197 = gf318;
                    r12_198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<unsigned char>(rbx6));
                    if ((eax197 & 0xd000) != 0x8000 || reinterpret_cast<signed char>(r13_196) < reinterpret_cast<signed char>(r12_198)) {
                        *reinterpret_cast<int32_t*>(&rdx62) = 2;
                        *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rsi159) = 0;
                        *reinterpret_cast<int32_t*>(&rsi159 + 4) = 0;
                        rax199 = fun_26e0();
                        rsp16 = rsp16 - 8 + 8;
                        r13_196 = rax199;
                        if (reinterpret_cast<signed char>(rax199) < reinterpret_cast<signed char>(0)) 
                            goto addr_40de_4;
                        if (r12_198 == rax199) 
                            goto addr_40b8_259;
                    } else {
                        addr_40b8_259:
                        r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp164) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_196) - reinterpret_cast<unsigned char>(r12_198)));
                        if (!reinterpret_cast<int1_t>(r12_8 == 0x7fffffffffffffff)) 
                            goto addr_3d32_207; else 
                            goto addr_40d3_3;
                    }
                    *reinterpret_cast<int32_t*>(&rdx62) = 0;
                    *reinterpret_cast<int32_t*>(&rdx62 + 4) = 0;
                    rsi159 = r12_198;
                    rax200 = fun_26e0();
                    rsp16 = rsp16 - 8 + 8;
                    if (reinterpret_cast<signed char>(rax200) < reinterpret_cast<signed char>(0)) 
                        goto addr_40de_4;
                    if (reinterpret_cast<signed char>(r13_196) >= reinterpret_cast<signed char>(r12_198)) 
                        goto addr_40b8_259;
                    r13_196 = r12_198;
                    goto addr_40b8_259;
                }
            }
        }
    }
    addr_32b8_200:
    zf201 = filter_command == 0;
    if (!zf201) {
        r12_8 = reinterpret_cast<void**>(0xf3e0);
        fun_27c0(0xf3e0, rsi159, rdx62);
        rsp202 = reinterpret_cast<void*>(rsp16 - 8 + 8);
        fun_25b0(13);
        rsp203 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp202) - 8 + 8);
        if (v204 != 1) {
            fun_29c0(0xf3e0, 13, reinterpret_cast<int64_t>(rsp202) + 0x70);
            rsp203 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp203) - 8 + 8);
        }
        fun_2530();
        rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp203) - 8 + 8);
    }
    *reinterpret_cast<uint32_t*>(&rax205) = *reinterpret_cast<int32_t*>(&v14) - 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax205) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax205) > 5) {
        *reinterpret_cast<int32_t*>(&r8_27) = 0;
        *reinterpret_cast<int32_t*>(&r8_27 + 4) = 0;
        bytes_split(v10, v21, v13, 0xffffffffffffffff, 0);
        rsp28 = rsp16 - 8 + 8;
        goto addr_3900_12;
    } else {
        goto *reinterpret_cast<int32_t*>(0xb1d0 + rax205 * 4) + 0xb1d0;
    }
}

int64_t __libc_start_main = 0;

void fun_45f3() {
    __asm__("cli ");
    __libc_start_main(0x2a40, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xf008;

void fun_2510(int64_t rdi);

int64_t fun_4693() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2510(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_46d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2880(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2770(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2570(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_56c3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** rax13;
    void** rdi14;
    void** r8_15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** r12_24;
    void** rax25;
    int32_t eax26;
    void** r13_27;
    void** rax28;
    void** rax29;
    int32_t eax30;
    void** rax31;
    void** rax32;
    void** rax33;
    int32_t eax34;
    void** rax35;
    void** r15_36;
    void** rax37;
    void** rax38;
    void** rax39;
    void** rdi40;
    void** r8_41;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2640();
            fun_2880(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2640();
            fun_2770(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2640();
            fun_2770(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2640();
            fun_2770(rax12, r12_11, 5, rcx6);
            rax13 = fun_2640();
            rdi14 = stdout;
            fun_2950(rdi14, 1, rax13, 2, r8_15);
            r12_16 = stdout;
            rax17 = fun_2640();
            fun_2770(rax17, r12_16, 5, 2);
            r12_18 = stdout;
            rax19 = fun_2640();
            fun_2770(rax19, r12_18, 5, 2);
            r12_20 = stdout;
            rax21 = fun_2640();
            fun_2770(rax21, r12_20, 5, 2);
            r12_22 = stdout;
            rax23 = fun_2640();
            fun_2770(rax23, r12_22, 5, 2);
            r12_24 = stdout;
            rax25 = fun_2640();
            fun_2770(rax25, r12_24, 5, 2);
            do {
                if (1) 
                    break;
                eax26 = fun_27a0("split", 0, 5, "sha512sum");
            } while (eax26);
            r13_27 = v4;
            if (!r13_27) {
                rax28 = fun_2640();
                fun_2880(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax29 = fun_2870(5);
                if (!rax29 || (eax30 = fun_2570(rax29, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax30)) {
                    rax31 = fun_2640();
                    r13_27 = reinterpret_cast<void**>("split");
                    fun_2880(1, rax31, "https://www.gnu.org/software/coreutils/", "split");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_27 = reinterpret_cast<void**>("split");
                    goto addr_5ab8_9;
                }
            } else {
                rax32 = fun_2640();
                fun_2880(1, rax32, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax33 = fun_2870(5);
                if (!rax33 || (eax34 = fun_2570(rax33, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax34)) {
                    addr_59be_11:
                    rax35 = fun_2640();
                    fun_2880(1, rax35, "https://www.gnu.org/software/coreutils/", "split");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_27 == "split")) {
                        r12_2 = reinterpret_cast<void**>(0xb6d1);
                    }
                } else {
                    addr_5ab8_9:
                    r15_36 = stdout;
                    rax37 = fun_2640();
                    fun_2770(rax37, r15_36, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_59be_11;
                }
            }
            rax38 = fun_2640();
            rcx6 = r12_2;
            fun_2880(1, rax38, r13_27, rcx6);
            addr_571e_14:
            fun_2930();
        }
    } else {
        rax39 = fun_2640();
        rdi40 = stderr;
        rcx6 = r12_2;
        fun_2950(rdi40, 1, rax39, rcx6, r8_41);
        goto addr_571e_14;
    }
}

struct s11 {
    unsigned char f0;
    unsigned char f1;
};

struct s11* fun_5af3(struct s11* rdi) {
    uint32_t edx2;
    struct s11* rax3;
    struct s11* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s11*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s11*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s11*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_5b53(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2660(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int64_t file_name = 0;

void fun_5b83(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_5b93(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2580(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_5ba3() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2560(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2640();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_5c33_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_28a0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2580(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_5c33_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_28a0();
    }
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    void** f90;
};

int32_t fun_27f0(struct s12* rdi);

int32_t fun_2830(struct s12* rdi);

int32_t rpl_fflush(struct s12* rdi);

int64_t fun_2620(struct s12* rdi);

int64_t fun_5c53(struct s12* rdi) {
    int32_t eax2;
    int32_t eax3;
    void** rax4;
    int32_t eax5;
    void** rax6;
    void** r12d7;
    int64_t rax8;

    __asm__("cli ");
    eax2 = fun_27f0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2830(rdi);
        if (!(eax3 && (fun_27f0(rdi), rax4 = fun_26e0(), reinterpret_cast<int1_t>(rax4 == 0xffffffffffffffff)) || (eax5 = rpl_fflush(rdi), eax5 == 0))) {
            rax6 = fun_2560();
            r12d7 = *reinterpret_cast<void***>(rax6);
            rax8 = fun_2620(rdi);
            if (r12d7) {
                *reinterpret_cast<void***>(rax6) = r12d7;
                *reinterpret_cast<int32_t*>(&rax8) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            }
            return rax8;
        }
    }
    goto fun_2620;
}

int32_t fun_28d0(int64_t rdi, int64_t rsi, int64_t rdx);

void fd_safer(int64_t rdi);

void fun_5ce3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t v4;
    int64_t rdx5;
    void** rax6;
    int32_t eax7;
    int64_t rdi8;
    void* rdx9;

    __asm__("cli ");
    v4 = rdx;
    *reinterpret_cast<int32_t*>(&rdx5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    rax6 = g28;
    if (*reinterpret_cast<unsigned char*>(&rsi) & 64) {
        *reinterpret_cast<int32_t*>(&rdx5) = *reinterpret_cast<int32_t*>(&v4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
    }
    eax7 = fun_28d0(rdi, rsi, rdx5);
    *reinterpret_cast<int32_t*>(&rdi8) = eax7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    fd_safer(rdi8);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2670();
    } else {
        return;
    }
}

int64_t fun_5d63(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    int64_t rsi5;
    int64_t rdx6;
    int32_t eax7;
    int64_t rax8;
    int32_t eax9;
    void** rax10;
    void** r13d11;
    int64_t rax12;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi5) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx6) = ecx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
    eax7 = fun_28d0(rsi, rsi5, rdx6);
    if (edi == eax7 || eax7 < 0) {
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        eax9 = fun_26a0();
        rax10 = fun_2560();
        r13d11 = *reinterpret_cast<void***>(rax10);
        fun_2710();
        *reinterpret_cast<int32_t*>(&rax12) = eax9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        *reinterpret_cast<void***>(rax10) = r13d11;
        return rax12;
    }
}

void rpl_fseeko(struct s12* rdi);

void fun_5de3(struct s12* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2830(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_5e33(struct s12* rdi, int64_t rsi, int32_t edx) {
    void** rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        fun_27f0(rdi);
        rax4 = fun_26e0();
        if (rax4 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax4;
            *reinterpret_cast<uint32_t*>(&rax5) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
        return rax5;
    }
}

int64_t safe_write(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_5eb3(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;
    void** rax10;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    } else {
        r12d5 = edi;
        rbp6 = rsi;
        rbx7 = rdx;
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            rax9 = safe_write(rdi8, rbp6, rbx7);
            if (rax9 == -1) 
                break;
            if (!rax9) 
                goto addr_5f10_6;
            r13_4 = r13_4 + rax9;
            rbp6 = rbp6 + rax9;
            rbx7 = rbx7 - rax9;
        } while (rbx7);
    }
    return r13_4;
    addr_5f10_6:
    rax10 = fun_2560();
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(28);
    return r13_4;
}

struct s13 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_5f33(uint64_t rdi, struct s13* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_2940(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s14 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s14* fun_26c0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_5f93(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s14* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2940("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2550("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_26c0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2570(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_7733(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2560();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0xf620;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_7773(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf620);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_7793(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf620);
    }
    *rdi = esi;
    return 0xf620;
}

int64_t fun_77b3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xf620);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s15 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_77f3(struct s15* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s15*>(0xf620);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s16 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s16* fun_7813(struct s16* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s16*>(0xf620);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x2a0a;
    if (!rdx) 
        goto 0x2a0a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xf620;
}

struct s17 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_7853(void** rdi, void** rsi, void** rdx, void** rcx, struct s17* r8) {
    struct s17* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s17*>(0xf620);
    }
    rax7 = fun_2560();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x7886);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s18 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_78d3(void** rdi, void** rsi, void*** rdx, struct s18* rcx) {
    struct s18* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s18*>(0xf620);
    }
    rax6 = fun_2560();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x7901);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x795c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_79c3() {
    __asm__("cli ");
}

void** gf098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_79d3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** rdi11;
    void** rsi12;
    void** rdx13;
    void** rcx14;
    void** r8_15;
    void** rsi16;
    void** rdx17;
    void** rcx18;
    void** r8_19;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2540(rdi6, rsi7, rdx8, rcx9, r8_10);
        } while (rbx4 != rbp5);
    }
    rdi11 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi11 != 0xf520) {
        fun_2540(rdi11, rsi12, rdx13, rcx14, r8_15);
        gf098 = reinterpret_cast<void**>(0xf520);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xf090) {
        fun_2540(r12_2, rsi16, rdx17, rcx18, r8_19);
        slotvec = reinterpret_cast<void**>(0xf090);
    }
    nslots = 1;
    return;
}

void fun_7a73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7a93() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7aa3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7ac3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_7ae3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2a10;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2670();
    } else {
        return rax6;
    }
}

void** fun_7b73(void** rdi, int32_t esi, void** rdx, void** rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2a15;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2670();
    } else {
        return rax7;
    }
}

void** fun_7c03(int32_t edi, void** rsi) {
    void** rax3;
    struct s7* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x2a1a;
    rcx4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2670();
    } else {
        return rax5;
    }
}

void** fun_7c93(int32_t edi, void** rsi, void** rdx) {
    void** rax4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2a1f;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2670();
    } else {
        return rax6;
    }
}

void** fun_7d23(void** rdi, void** rsi, uint32_t edx) {
    struct s7* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x78f0]");
    __asm__("movdqa xmm1, [rip+0x78f8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x78e1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2670();
    } else {
        return rax10;
    }
}

void** fun_7dc3(void** rdi, uint32_t esi) {
    struct s7* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7850]");
    __asm__("movdqa xmm1, [rip+0x7858]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x7841]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2670();
    } else {
        return rax9;
    }
}

void** fun_7e63(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x77b0]");
    __asm__("movdqa xmm1, [rip+0x77b8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x7799]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2670();
    } else {
        return rax3;
    }
}

void** fun_7ef3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7720]");
    __asm__("movdqa xmm1, [rip+0x7728]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7716]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2670();
    } else {
        return rax4;
    }
}

void** fun_7f83(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2a24;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2670();
    } else {
        return rax6;
    }
}

void** fun_8023(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x75ea]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x75e2]");
    __asm__("movdqa xmm2, [rip+0x75ea]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2a29;
    if (!rdx) 
        goto 0x2a29;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2670();
    } else {
        return rax7;
    }
}

void** fun_80c3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    void** rcx6;
    struct s7* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x754a]");
    __asm__("movdqa xmm1, [rip+0x7552]");
    __asm__("movdqa xmm2, [rip+0x755a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2a2e;
    if (!rdx) 
        goto 0x2a2e;
    rcx7 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2670();
    } else {
        return rax9;
    }
}

void** fun_8173(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x749a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7492]");
    __asm__("movdqa xmm2, [rip+0x749a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2a33;
    if (!rsi) 
        goto 0x2a33;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2670();
    } else {
        return rax6;
    }
}

void** fun_8213(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x73fa]");
    __asm__("movdqa xmm1, [rip+0x7402]");
    __asm__("movdqa xmm2, [rip+0x740a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2a38;
    if (!rsi) 
        goto 0x2a38;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2670();
    } else {
        return rax7;
    }
}

void fun_82b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_82c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_82e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8303(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_2750(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_8323(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2750(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_2560();
        if (*reinterpret_cast<void***>(rax9) == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 22)) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_2600(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_8393(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2600(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_2560();
        if (*reinterpret_cast<void***>(rax9) == 4) 
            continue;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 22)) 
            break;
        if (rbx6 <= 0x7ff00000) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_27d0(void** rdi, signed char** rsi, int64_t rdx);

int32_t fun_2920(void** rdi, void** rsi);

int32_t fun_2960(void** rdi, void** rsi);

int64_t fun_8403(void** rdi, int32_t* rsi, void** rdx, void** rcx) {
    int32_t* r13_5;
    void** r12_6;
    void** rbp7;
    int64_t rbx8;
    signed char** rsp9;
    void** rax10;
    void** v11;
    void** rdi12;
    int32_t eax13;
    int64_t rax14;
    signed char* v15;
    int32_t r8d16;
    void* rax17;
    int64_t rax18;
    int32_t eax19;
    int64_t rbp20;
    int32_t eax21;
    int32_t eax22;
    int32_t eax23;
    int64_t rax24;
    signed char* v25;
    int64_t rax26;
    signed char* v27;

    __asm__("cli ");
    r13_5 = rsi;
    r12_6 = rdi;
    rbp7 = reinterpret_cast<void**>("HUP");
    *reinterpret_cast<int32_t*>(&rbx8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
    rsp9 = reinterpret_cast<signed char**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rax10 = g28;
    v11 = rax10;
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48) > 9) {
        do {
            rdi12 = rbp7;
            eax13 = fun_27a0(rdi12, r12_6, rdx, rcx);
            rsp9 = rsp9 - 1 + 1;
            if (!eax13) 
                break;
            *reinterpret_cast<int32_t*>(&rbx8) = *reinterpret_cast<int32_t*>(&rbx8) + 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
            rbp7 = rbp7 + 12;
        } while (*reinterpret_cast<int32_t*>(&rbx8) != 35);
        goto addr_84d0_4;
    } else {
        rax14 = fun_27d0(rdi, rsp9, 10);
        if (*v15) 
            goto addr_84bc_7;
        if (rax14 <= 64) 
            goto addr_846d_9; else 
            goto addr_84bc_7;
    }
    *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(0xf0a0 + (rbx8 + rbx8 * 2) * 4);
    addr_846d_9:
    r8d16 = *reinterpret_cast<int32_t*>(&rax14) >> 31;
    addr_8474_11:
    *r13_5 = *reinterpret_cast<int32_t*>(&rax14);
    rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax17) {
        fun_2670();
    } else {
        *reinterpret_cast<int32_t*>(&rax18) = r8d16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        return rax18;
    }
    addr_84d0_4:
    eax19 = fun_2920(rdi12, r12_6);
    *reinterpret_cast<int32_t*>(&rbp20) = eax19;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp20) + 4) = 0;
    eax21 = fun_2960(rdi12, r12_6);
    rsp9 = rsp9 - 1 + 1 - 1 + 1;
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp20) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp20) == 0) || (eax22 = fun_2570(r12_6, "RTMIN", 5, rcx), rsp9 = rsp9 - 1 + 1, !!eax22)) {
        if (reinterpret_cast<uint1_t>(eax21 < 0) | reinterpret_cast<uint1_t>(eax21 == 0) || ((eax23 = fun_2570(r12_6, "RTMAX", 5, rcx), rsp9 = rsp9 - 1 + 1, !!eax23) || ((rax24 = fun_27d0(r12_6 + 5, rsp9, 10), !!*v25) || (rax24 < static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp20) - eax21) || !reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(rax24 < 0) | reinterpret_cast<uint1_t>(rax24 == 0)))))) {
            addr_84bc_7:
            *reinterpret_cast<int32_t*>(&rax14) = -1;
            r8d16 = -1;
            goto addr_8474_11;
        } else {
            *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(&rax24) + eax21;
            goto addr_846d_9;
        }
    } else {
        rax26 = fun_27d0(r12_6 + 5, rsp9, 10);
        if (*v27) 
            goto addr_84bc_7;
        if (rax26 < 0) 
            goto addr_84bc_7;
        if (rax26 > static_cast<int64_t>(eax21 - *reinterpret_cast<int32_t*>(&rbp20))) 
            goto addr_84bc_7;
        *reinterpret_cast<int32_t*>(&rax14) = static_cast<int32_t>(rbp20 + rax26);
        goto addr_846d_9;
    }
}

void fun_2590(void** rdi, int64_t rsi);

int64_t fun_85a3(void** rdi, void** rsi) {
    int64_t rdx3;
    int32_t* rcx4;
    void** rbp5;
    int32_t ebx6;
    int64_t rax7;
    int32_t eax8;
    int32_t r12d9;
    int32_t eax10;
    int64_t r8_11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rdx3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    rcx4 = reinterpret_cast<int32_t*>(0xf0a0);
    rbp5 = rsi;
    ebx6 = *reinterpret_cast<int32_t*>(&rdi);
    do {
        if (*rcx4 == ebx6) 
            break;
        *reinterpret_cast<int32_t*>(&rdx3) = *reinterpret_cast<int32_t*>(&rdx3) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        rcx4 = rcx4 + 3;
    } while (*reinterpret_cast<int32_t*>(&rdx3) != 35);
    goto addr_85f0_4;
    fun_2590(rbp5, 0xf0a0 + (rdx3 + rdx3 * 2) * 4 + 4);
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    addr_85e3_6:
    return rax7;
    addr_85f0_4:
    eax8 = fun_2920(rdi, rsi);
    r12d9 = eax8;
    eax10 = fun_2960(rdi, rsi);
    if (r12d9 > ebx6 || eax10 < ebx6) {
        *reinterpret_cast<int32_t*>(&rax7) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        goto addr_85e3_6;
    } else {
        if ((eax10 - r12d9 >> 1) + r12d9 >= ebx6) {
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0x494d5452);
            *reinterpret_cast<int16_t*>(rbp5 + 4) = 78;
        } else {
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0x414d5452);
            r12d9 = eax10;
            *reinterpret_cast<int16_t*>(rbp5 + 4) = 88;
        }
        *reinterpret_cast<int32_t*>(&rax7) = ebx6 - r12d9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            *reinterpret_cast<int32_t*>(&r8_11) = *reinterpret_cast<int32_t*>(&rax7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_11) + 4) = 0;
            fun_29f0(rbp5 + 5, 1, -1, "%+d", r8_11);
            return 0;
        }
    }
}

int32_t dup_safer();

int64_t fun_8683(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_2560();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_2710();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s19 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_27b0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_86e3(void** rdi, void** rsi, void** rdx, void** rcx, struct s19* r8, void** r9) {
    void** r12_7;
    void** rax8;
    void** rax9;
    void** r13_10;
    void** r12_11;
    void** rax12;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2950(rdi, 1, "%s %s\n", rdx, rcx);
    } else {
        r9 = rcx;
        fun_2950(rdi, 1, "%s (%s) %s\n", rsi, rdx, rdi, 1, "%s (%s) %s\n", rsi, rdx);
    }
    rax8 = fun_2640();
    fun_2950(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6);
    fun_27b0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2640();
    fun_2950(rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6);
    fun_27b0(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r13_10 = r8->f8;
        r12_11 = r8->f0;
        rax12 = fun_2640();
        fun_2950(rdi, 1, rax12, r12_11, r13_10, rdi, 1, rax12, r12_11, r13_10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xb978 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xb978;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_8b53() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s20 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_8b73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s20* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s20* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2670();
    } else {
        return;
    }
}

void fun_8c13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_8cb6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_8cc0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2670();
    } else {
        return;
    }
    addr_8cb6_5:
    goto addr_8cc0_7;
}

void fun_8cf3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_27b0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2640();
    fun_2880(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2640();
    fun_2880(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2640();
    goto fun_2880;
}

int64_t fun_2970();

void fun_8d93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2970();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25c0();

void fun_8db3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2800(void** rdi);

void fun_8df3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2800(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8e13(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2800(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8e33(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2800(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2850();

void fun_8e53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2850();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_8e83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2850();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8eb3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_8ef3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_25c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8f33(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8f63(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_25c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8fb3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_25c0();
            if (rax5) 
                break;
            addr_8ffd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_8ffd_5;
        rax8 = fun_25c0();
        if (rax8) 
            goto addr_8fe6_9;
        if (rbx4) 
            goto addr_8ffd_5;
        addr_8fe6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_9043(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_25c0();
            if (rax8) 
                break;
            addr_908a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_908a_5;
        rax11 = fun_25c0();
        if (rax11) 
            goto addr_9072_9;
        if (!rbx6) 
            goto addr_9072_9;
        if (r12_4) 
            goto addr_908a_5;
        addr_9072_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_90d3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_917d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_9190_10:
                *r12_8 = 0;
            }
            addr_9130_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_9156_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_91a4_14;
            if (rcx10 <= rsi9) 
                goto addr_914d_16;
            if (rsi9 >= 0) 
                goto addr_91a4_14;
            addr_914d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_91a4_14;
            addr_9156_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2850();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_91a4_14;
            if (!rbp13) 
                break;
            addr_91a4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_917d_9;
        } else {
            if (!r13_6) 
                goto addr_9190_10;
            goto addr_9130_11;
        }
    }
}

int64_t fun_2790();

void fun_91d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9203() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9233() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9253() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9273(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2800(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

void fun_92b3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2800(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

struct s21 {
    signed char[1] pad1;
    void** f1;
};

void fun_92f3(int64_t rdi, struct s21* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2800(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_27e0;
    }
}

void fun_9333(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2660(rdi);
    rax3 = fun_2800(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

void fun_9373() {
    void** rdi1;

    __asm__("cli ");
    fun_2640();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_28a0();
    fun_2550(rdi1);
}

uint64_t fun_93b3(void** rdi, int32_t esi, uint64_t rdx, uint64_t rcx) {
    void** rax5;
    int32_t eax6;
    void** rax7;
    void** r12_8;
    uint64_t v9;
    void* rax10;
    void** rax11;

    __asm__("cli ");
    rax5 = g28;
    eax6 = xstrtoumax(rdi);
    if (eax6) {
        rax7 = fun_2560();
        r12_8 = rax7;
        if (eax6 == 1) {
            addr_9450_3:
            *reinterpret_cast<void***>(r12_8) = reinterpret_cast<void**>(75);
        } else {
            if (eax6 == 3) {
                *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
            }
        }
    } else {
        if (v9 >= rdx && v9 <= rcx) {
            rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
            if (rax10) {
                fun_2670();
            } else {
                return v9;
            }
        }
        rax11 = fun_2560();
        r12_8 = rax11;
        if (v9 > 0x3fffffff) 
            goto addr_9450_3;
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(34);
    }
    quote(rdi);
    if (*reinterpret_cast<void***>(r12_8) != 22) 
        goto addr_946c_13;
    while (1) {
        addr_946c_13:
        if (!1) {
        }
        fun_28a0();
    }
}

void xnumtoumax();

void fun_94c3() {
    __asm__("cli ");
    xnumtoumax();
    return;
}

void*** fun_29e0(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_28f0(void** rdi);

int64_t fun_94f3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    void** rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void*** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    void** rax26;
    int64_t rax27;
    void** rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    void** rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void**>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_26f0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_2670();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_9864_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<unsigned char>(rcx9));
                } while (!__intrinsic());
            }
            addr_9864_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_95ad_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_95b5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2560();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_29e0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_95eb_21;
    rsi = r15_14;
    rax24 = fun_28f0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_26b0(r13_18), r8 = r8, rax26 == 0))) {
            addr_95eb_21:
            r12d11 = 4;
            goto addr_95b5_13;
        } else {
            addr_9629_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_26b0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void**>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xba18 + rbp33 * 4) + 0xba18;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_95eb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_95ad_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_95ad_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_26b0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_9629_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_95b5_13;
}

int64_t fun_25a0();

int64_t fun_9923(void** rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_25a0();
    ebx3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_997e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2560();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_997e_3;
            rax6 = fun_2560();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

signed char* fun_2820(int64_t rdi);

signed char* fun_9993() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2820(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2690(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_99d3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2690(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2670();
    } else {
        return r12_7;
    }
}

void fun_9a63() {
    __asm__("cli ");
}

uint32_t fun_25d0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_9a83(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_25d0(rdi);
        r12d10 = eax9;
        goto addr_9b84_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_25d0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_9b84_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2670();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_9c39_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_25d0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_25d0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2560();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_2710();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_9b84_3;
                }
            }
        } else {
            eax22 = fun_25d0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2560(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_9b84_3;
            } else {
                eax25 = fun_25d0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_9b84_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_9c39_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_9ae9_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_9aed_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_9aed_18:
            if (0) {
            }
        } else {
            addr_9b35_23:
            eax28 = fun_25d0(rdi);
            r12d10 = eax28;
            goto addr_9b84_3;
        }
        eax29 = fun_25d0(rdi);
        r12d10 = eax29;
        goto addr_9b84_3;
    }
    if (0) {
    }
    eax30 = fun_25d0(rdi);
    r12d10 = eax30;
    goto addr_9b84_3;
    addr_9ae9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_9aed_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_9b35_23;
        goto addr_9aed_18;
    }
}

int32_t setlocale_null_r();

int64_t fun_9cf3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2670();
    } else {
        return rax3;
    }
}

int64_t fun_9d73(int64_t rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    int32_t r13d6;
    void** rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = fun_2870(rdi);
    if (!rax5) {
        r13d6 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax7 = fun_2660(rax5);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax7)) {
            fun_27e0(rsi, rax5, rax7 + 1, rcx);
            return 0;
        } else {
            r13d6 = 34;
            if (rdx) {
                fun_27e0(rsi, rax5, rdx + 0xffffffffffffffff, rcx);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_9e23() {
    __asm__("cli ");
    goto fun_2870;
}

void fun_9e33() {
    __asm__("cli ");
}

void fun_9e47() {
    __asm__("cli ");
    return;
}

void** fun_2730(void** rdi, void** rsi);

void fun_2bfc() {
    void** rsi1;
    void** r15_2;
    int32_t r8d3;
    void** rax4;
    void** rax5;
    int32_t r8d6;

    rsi1 = reinterpret_cast<void**>("0123456789abcdef");
    r15_2 = optarg;
    if (r8d3 == 100) {
        rsi1 = reinterpret_cast<void**>("0123456789");
    }
    suffix_alphabet = rsi1;
    if (!r15_2) 
        goto 0x2b0b;
    rax4 = fun_2660(r15_2);
    rax5 = fun_2730(r15_2, rsi1);
    if (rax4 == rax5) {
        while (*reinterpret_cast<void***>(r15_2) == 48 && *reinterpret_cast<void***>(r15_2 + 1)) {
            ++r15_2;
            optarg = r15_2;
        }
        numeric_suffix_start = r15_2;
        goto 0x2b0b;
    } else {
        quote(r15_2, r15_2);
        if (r8d6 != 100) 
            goto 0x3143;
        fun_2640();
        goto 0x314d;
    }
}

void fun_2c84() {
    int32_t v1;
    void** rdi2;

    if (v1) 
        goto 0x3bc6;
    fun_2640();
    rdi2 = optarg;
    xdectoumax(rdi2, rdi2);
    goto 0x2b0b;
}

void fun_2cdb() {
    void** rdi1;
    void** rax2;

    fun_2640();
    rdi1 = optarg;
    rax2 = xdectoumax(rdi1);
    suffix_length = rax2;
    goto 0x2b0b;
}

void fun_2d76() {
    elide_empty_files = 1;
    goto 0x2b0b;
}

void fun_2d9e() {
    void** rax1;
    void** r15_2;
    int64_t rdx3;
    void* rcx4;
    void** rcx5;
    int32_t eax6;
    void** rcx7;
    int32_t eax8;

    do {
        optarg = rax1;
        r15_2 = rax1;
        ++rax1;
        *reinterpret_cast<uint32_t*>(&rdx3) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_2));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    } while (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx4) + rdx3 * 2 + 1) & 32);
    eax6 = fun_2570(r15_2, "r/", 2, rcx5);
    if (!eax6) 
        goto 0x301b;
    eax8 = fun_2570(r15_2, "l/", 2, rcx7);
    if (eax8) 
        goto 0x2e08;
    optarg = r15_2 + 2;
}

void fun_2eab() {
    int32_t v1;
    void** rdi2;

    if (v1) 
        goto 0x3bc6;
    fun_2640();
    rdi2 = optarg;
    xdectoumax(rdi2, rdi2);
    goto 0x2b0b;
}

void fun_2efc() {
    void** r15_1;
    int64_t rsi2;
    void** rax3;

    r15_1 = optarg;
    rax3 = last_component(r15_1, rsi2);
    if (r15_1 != rax3) 
        goto 0x3dad;
    additional_suffix = r15_1;
    goto 0x2b0b;
}

void fun_2f20() {
    void** rdi1;

    fun_2640();
    rdi1 = optarg;
    xdectoumax(rdi1, rdi1);
    goto 0x2b0b;
}

void** fun_2780(void** rdi, int64_t rsi, void** rdx);

void fun_3329() {
    void** r13_1;
    unsigned char v2;
    void** r14_3;
    void** rdx4;
    void** v5;
    void** rax6;
    void** v7;
    void** r15_8;
    void** v9;
    int32_t ebp10;
    void** rbx11;
    void** r12_12;
    int64_t rsi13;
    void** rax14;
    uint32_t edi15;
    void** rsi16;
    uint32_t edi17;

    r13_1 = reinterpret_cast<void**>(__return_address());
    v2 = 1;
    r14_3 = reinterpret_cast<void**>(0);
    do {
        rdx4 = v5;
        rax6 = safe_read();
        v7 = rax6;
        if (!(rax6 + 1)) 
            goto 0x4419;
        r15_8 = v9;
        ebp10 = eolchar;
        rbx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v7) + reinterpret_cast<unsigned char>(r15_8));
        r12_12 = r15_8;
        *reinterpret_cast<void***>(rbx11) = *reinterpret_cast<void***>(&ebp10);
        while (*reinterpret_cast<int32_t*>(&rsi13) = ebp10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0, rax14 = fun_2780(r15_8, rsi13, rdx4), rbx11 != rax14) {
            ++r14_3;
            r15_8 = rax14 + 1;
            if (reinterpret_cast<unsigned char>(r13_1) > reinterpret_cast<unsigned char>(r14_3)) 
                continue;
            edi15 = v2;
            rsi16 = r12_12;
            *reinterpret_cast<int32_t*>(&r14_3) = 0;
            *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
            rdx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_8) - reinterpret_cast<unsigned char>(r12_12));
            r12_12 = r15_8;
            cwrite(*reinterpret_cast<unsigned char*>(&edi15), rsi16, rdx4);
            v2 = 1;
            ebp10 = eolchar;
        }
        if (rbx11 != r12_12) {
            edi17 = v2;
            cwrite(*reinterpret_cast<unsigned char*>(&edi17), r12_12, reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(r12_12));
            v2 = 0;
        }
    } while (v7);
    goto 0x3900;
}

void fun_33be() {
}

void fun_3572() {
    int64_t v1;

    if (!v1) 
        goto 0x4123; else 
        goto "???";
}

void fun_38d3() {
    int64_t v1;
    void** v2;
    void** rbp3;
    void* rdx4;
    void** v5;
    int64_t rbp6;
    void* r13_7;
    void** v8;
    void*** r13_9;
    int64_t rbp10;
    void** v11;
    void** v12;
    void** r13_13;

    if (v1) {
        if (reinterpret_cast<unsigned char>(__return_address()) < reinterpret_cast<unsigned char>(v2) || reinterpret_cast<unsigned char>(__return_address()) > reinterpret_cast<unsigned char>(rbp3)) {
            fun_26f0("k && n && k <= n && n <= file_size", "src/split.c", 0x3e4, "bytes_chunk_extract");
        } else {
            rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5 + 0xffffffffffffffff) * (reinterpret_cast<uint64_t>(rbp6) / reinterpret_cast<unsigned char>(__return_address())));
            if (__return_address() != v5) {
            }
            if (reinterpret_cast<uint64_t>(r13_7) <= reinterpret_cast<uint64_t>(rdx4)) 
                goto 0x4203;
            fun_2890(v8, reinterpret_cast<unsigned char>(v8) + reinterpret_cast<uint64_t>(rdx4), reinterpret_cast<int64_t>(r13_9) - reinterpret_cast<uint64_t>(rdx4));
        }
    } else {
        bytes_split(reinterpret_cast<uint64_t>(rbp10) / reinterpret_cast<unsigned char>(__return_address()), v11, v12, r13_13, __return_address());
    }
}

uint32_t fun_2760(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_29a0(int64_t rdi, void** rsi);

uint32_t fun_2980(void** rdi, void** rsi);

void fun_61c5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void*** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2640();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2640();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2660(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_64c3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_64c3_22; else 
                            goto addr_68bd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_697d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_6cd0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_64c0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_64c0_30; else 
                                goto addr_6ce9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2660(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_6cd0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2760(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_6cd0_28; else 
                            goto addr_636c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_6e30_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_6cb0_40:
                        if (r11_27 == 1) {
                            addr_683d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_6df8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_6477_44;
                            }
                        } else {
                            goto addr_6cc0_46;
                        }
                    } else {
                        addr_6e3f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_683d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_64c3_22:
                                if (v47 != 1) {
                                    addr_6a19_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2660(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_6a64_54;
                                    }
                                } else {
                                    goto addr_64d0_56;
                                }
                            } else {
                                addr_6475_57:
                                ebp36 = 0;
                                goto addr_6477_44;
                            }
                        } else {
                            addr_6ca4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_6e3f_47; else 
                                goto addr_6cae_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_683d_41;
                        if (v47 == 1) 
                            goto addr_64d0_56; else 
                            goto addr_6a19_52;
                    }
                }
                addr_6531_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_63c8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_63ed_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_66f0_65;
                    } else {
                        addr_6559_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_6da8_67;
                    }
                } else {
                    goto addr_6550_69;
                }
                addr_6401_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_644c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_6da8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_644c_81;
                }
                addr_6550_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_63ed_64; else 
                    goto addr_6559_66;
                addr_6477_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_652f_91;
                if (v22) 
                    goto addr_648f_93;
                addr_652f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6531_62;
                addr_6a64_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_71eb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_725b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_705f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_29a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2980(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_6b5e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_651c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_6b68_112;
                    }
                } else {
                    addr_6b68_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_6c39_114;
                }
                addr_6528_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_652f_91;
                while (1) {
                    addr_6c39_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_7147_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_6ba6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_7155_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_6c27_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_6c27_128;
                        }
                    }
                    addr_6bd5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_6c27_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_6ba6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6bd5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_644c_81;
                addr_7155_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6da8_67;
                addr_71eb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_6b5e_109;
                addr_725b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_6b5e_109;
                addr_64d0_56:
                rax93 = fun_29e0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93 + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_651c_110;
                addr_6cae_59:
                goto addr_6cb0_40;
                addr_697d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_64c3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_6528_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6475_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_64c3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_69c2_160;
                if (!v22) 
                    goto addr_6d97_162; else 
                    goto addr_6fa3_163;
                addr_69c2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_6d97_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_6da8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_686b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_66d3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_6401_70; else 
                    goto addr_66e7_169;
                addr_686b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_63c8_63;
                goto addr_6550_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_6ca4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_6ddf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_64c0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_63b8_178; else 
                        goto addr_6d62_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6ca4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_64c3_22;
                }
                addr_6ddf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_64c0_30:
                    r8d42 = 0;
                    goto addr_64c3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_6531_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_6df8_42;
                    }
                }
                addr_63b8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_63c8_63;
                addr_6d62_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_6cc0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_6531_62;
                } else {
                    addr_6d72_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_64c3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_7522_188;
                if (v28) 
                    goto addr_6d97_162;
                addr_7522_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_66d3_168;
                addr_636c_37:
                if (v22) 
                    goto addr_7363_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_6383_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_6e30_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_6ebb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_64c3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_63b8_178; else 
                        goto addr_6e97_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_6ca4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_64c3_22;
                }
                addr_6ebb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_64c3_22;
                }
                addr_6e97_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6cc0_46;
                goto addr_6d72_186;
                addr_6383_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_64c3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_64c3_22; else 
                    goto addr_6394_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_746e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_72f4_210;
            if (1) 
                goto addr_72f2_212;
            if (!v29) 
                goto addr_6f2e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2650();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_7461_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_66f0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_64ab_219; else 
            goto addr_670a_220;
        addr_648f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_64a3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_670a_220; else 
            goto addr_64ab_219;
        addr_705f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_670a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2650();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_707d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2650();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_74f0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_6f56_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_7147_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_64a3_221;
        addr_6fa3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_64a3_221;
        addr_66e7_169:
        goto addr_66f0_65;
        addr_746e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_670a_220;
        goto addr_707d_222;
        addr_72f4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_734e_236;
        fun_2670();
        rsp25 = rsp25 - 8 + 8;
        goto addr_74f0_225;
        addr_72f2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_72f4_210;
        addr_6f2e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_72f4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_6f56_226;
        }
        addr_7461_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_68bd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb42c + rax113 * 4) + 0xb42c;
    addr_6ce9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb52c + rax114 * 4) + 0xb52c;
    addr_7363_190:
    addr_64ab_219:
    goto 0x6190;
    addr_6394_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb32c + rax115 * 4) + 0xb32c;
    addr_734e_236:
    goto v116;
}

void fun_63b0() {
}

void fun_6568() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x6262;
}

void fun_65c1() {
    goto 0x6262;
}

void fun_66ae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x6531;
    }
    if (v2) 
        goto 0x6fa3;
    if (!r10_3) 
        goto addr_710e_5;
    if (!v4) 
        goto addr_6fde_7;
    addr_710e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_6fde_7:
    goto 0x63e4;
}

void fun_66cc() {
}

void fun_6777() {
    signed char v1;

    if (v1) {
        goto 0x66ff;
    } else {
        goto 0x643a;
    }
}

void fun_6791() {
    signed char v1;

    if (!v1) 
        goto 0x678a; else 
        goto "???";
}

void fun_67b8() {
    goto 0x66d3;
}

void fun_6838() {
}

void fun_6850() {
}

void fun_687f() {
    goto 0x66d3;
}

void fun_68d1() {
    goto 0x6860;
}

void fun_6900() {
    goto 0x6860;
}

void fun_6933() {
    goto 0x6860;
}

void fun_6d00() {
    goto 0x63b8;
}

void fun_6ffe() {
    signed char v1;

    if (v1) 
        goto 0x6fa3;
    goto 0x63e4;
}

void fun_70a5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x63e4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x63c8;
        goto 0x63e4;
    }
}

void fun_74c2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x6730;
    } else {
        goto 0x6262;
    }
}

void fun_87b8() {
    fun_2640();
}

void fun_969c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x96ab;
}

void fun_976c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x9779;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x96ab;
}

void fun_9790() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_97bc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x96ab;
}

void fun_97dd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x96ab;
}

void fun_9801() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x96ab;
}

void fun_9825() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x97b4;
}

void fun_9849() {
}

void fun_9869() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x97b4;
}

void fun_9885() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x97b4;
}

void fun_2d1f() {
    int32_t v1;
    void** rdi2;

    if (v1) 
        goto 0x3bc6;
    fun_2640();
    rdi2 = optarg;
    xdectoumax(rdi2, rdi2);
    goto 0x2b0b;
}

void fun_2d82(void** rdi, void** rsi, void** rdx, void** rcx) {
    int32_t v5;

    if (v5) 
        goto 0x3bc6;
    fun_29e0(rdi, rsi, rdx, rcx);
    goto 0x2da7;
}

void fun_2f65() {
    void** rax1;

    rax1 = optarg;
    filter_command = rax1;
    goto 0x2b0b;
}

void fun_65ee() {
    goto 0x6262;
}

void fun_67c4() {
    goto 0x677c;
}

void fun_688b() {
    goto 0x63b8;
}

void fun_68dd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x6860;
    goto 0x648f;
}

void fun_690f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x686b;
        goto 0x6290;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x670a;
        goto 0x64ab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x70a8;
    if (r10_8 > r15_9) 
        goto addr_67f5_9;
    addr_67fa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x70b3;
    goto 0x63e4;
    addr_67f5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_67fa_10;
}

void fun_6942() {
    goto 0x6477;
}

void fun_6d10() {
    goto 0x6477;
}

void fun_74af() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x65cc;
    } else {
        goto 0x6730;
    }
}

void fun_8870() {
}

void fun_973f() {
    if (__intrinsic()) 
        goto 0x9779; else 
        goto "???";
}

void fun_2f78() {
    verbose = 1;
    goto 0x2b0b;
}

void fun_694c() {
    goto 0x68e7;
}

void fun_6d1a() {
    goto 0x683d;
}

void fun_88d0() {
    fun_2640();
    goto fun_2950;
}

void fun_2f84() {
    unbuffered = 1;
    goto 0x2b0b;
}

void fun_661d() {
    goto 0x6262;
}

void fun_6958() {
    goto 0x68e7;
}

void fun_6d27() {
    goto 0x688e;
}

void fun_8910() {
    fun_2640();
    goto fun_2950;
}

void fun_2f90() {
    void** r15_1;
    int32_t eax2;
    void** rdx3;
    void** rcx4;
    int32_t eax5;
    int32_t edx6;

    r15_1 = optarg;
    eax2 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_1));
    if (!*reinterpret_cast<signed char*>(&eax2)) 
        goto 0x451a;
    if (*reinterpret_cast<void***>(r15_1 + 1)) {
        eax5 = fun_27a0(r15_1, "\\0", rdx3, rcx4);
        if (eax5) 
            goto 0x44c4;
        eax2 = 0;
    }
    edx6 = eolchar;
    if (edx6 >= 0) {
        if (edx6 != eax2) 
            goto 0x44f6;
    }
    eolchar = eax2;
    goto 0x2b0b;
}

void fun_664a() {
    goto 0x6262;
}

void fun_6964() {
    goto 0x6860;
}

void fun_8950() {
    fun_2640();
    goto fun_2950;
}

void fun_666c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x7000;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x6531;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x6531;
    }
    if (v11) 
        goto 0x7363;
    if (r10_12 > r15_13) 
        goto addr_73b3_8;
    addr_73b8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x70f1;
    addr_73b3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_73b8_9;
}

struct s22 {
    signed char[8] pad8;
    void** f8;
};

void fun_89a0() {
    void** r13_1;
    struct s22* rbx2;
    void** r12_3;
    void*** rbx4;
    void** rax5;
    void** rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2640();
    fun_2950(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_89f8() {
    fun_2640();
    goto 0x89c9;
}

struct s23 {
    signed char[8] pad8;
    void** f8;
};

void fun_8a30() {
    void** r13_1;
    struct s23* rbx2;
    void** r12_3;
    void*** rbx4;
    void** rax5;
    void** rbp6;
    int64_t v7;

    r13_1 = rbx2->f8;
    r12_3 = *rbx4;
    rax5 = fun_2640();
    fun_2950(rbp6, 1, rax5, r12_3, r13_1, rbp6, 1, rax5, r12_3, r13_1);
    goto v7;
}

void fun_8aa8() {
    fun_2640();
    goto 0x8a6b;
}
